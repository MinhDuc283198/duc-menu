<?php 
$string['register_form_title'] = 'Register for the course now!';
$string['register_form_description'] = 'Master Korean will contact you as soon as you complete the registration information below.';
$string['register_form_name'] = 'Full Name:';
$string['register_form_phone'] = 'Phone Number:';
$string['register_form_email'] = 'Email:';
$string['register_form_submit'] = 'Register now!';
$string['register_form_admin_title'] = 'Customer Courses Register!';
$string['register_form_index'] = 'Index!';