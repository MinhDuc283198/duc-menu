<?php

$string['register_form_title'] = 'Đăng ký mua khóa học ngay!';
$string['register_form_description'] = 'Master Korean sẽ liên hệ ngay sau khi bạn hoàn tất thông tin đăng ký dưới đây.';
$string['register_form_name'] = 'Họ Tên:';
$string['register_form_phone'] = 'SĐT:';
$string['register_form_email'] = 'Email:';
$string['register_form_submit'] = 'Đăng ký ngay!';
$string['register_form_admin_title'] = 'Người dùng đăng ký khóa học!';
$string['register_form_index'] = 'STT';