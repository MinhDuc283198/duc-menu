<?php

$string['register_form_title'] = '강좌 구매 등록 하세요!';
$string['register_form_description'] = '아래 등록 정보를 입력하시면 Master Korean는 연락 드리겠습니다.';
$string['register_form_name'] = '이름:';
$string['register_form_phone'] = '전화번호:';
$string['register_form_email'] = '이메일 주소:';
$string['register_form_submit'] = '지금 등록하세요!';
$string['register_form_admin_title'] = '강좌회원가입!';
$string['register_form_index'] = '숫자 순서';