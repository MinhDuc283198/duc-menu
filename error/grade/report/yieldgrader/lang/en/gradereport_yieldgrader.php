<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradereport_grader', language 'en'
 *
 * @package   gradereport_grader
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Final Grader Report';

$string['grade_setting'] = 'Grade Setting';
$string['excelldown'] = 'Excell File Download';
$string['hrdexport'] = 'HRD Export';
$string['num'] = 'no.';
$string['belong'] = '교육지명번호';
$string['rank'] = 'NEIS번호';
$string['username'] = 'Student ID';
$string['name'] = 'Name';
$string['per'] = 'Percentage';
$string['final_grade'] = 'Final Grade';

$string['gradeopen'] = 'Whether to publish credits';
$string['open'] = 'Open';
$string['private'] = 'Private';
$string['editdate'] = 'Settings Modified';
$string['norecord'] = 'No records';