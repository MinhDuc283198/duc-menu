<?php
require_once('../../../config.php');

$courseid = required_param('id', PARAM_INT); // course id
$gradeids = optional_param_array('gradeid', array(), PARAM_RAW); // course id
$showgrades = required_param('showgrades', PARAM_INT); // course id
if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('nocourseid');
}

$lmsdata_class = $DB->get_record('lmsdata_class',array('courseid'=>$courseid));
$g_item = $DB->get_record('grade_items',array('courseid'=>$courseid, 'itemtype'=>'course'));

if($gradeids){
    foreach($gradeids as $gid => $grade){
        $grade_yn = $DB->set_field('grade_grades', 'finalgrade', $grade, array('userid' => $gid,'itemid'=>$g_item->id));    
    }
}

$DB->set_field('course', 'showgrades', $showgrades, array('id' => $courseid));
//$DB->set_field('lmsdata_class', 'gradesettingdate', time(), array('id' => $lmsdata_class->id));

redirect($CFG->wwwroot.'/grade/report/yieldgrader/index.php?id='.$courseid);