<?php

define('AJAX_SCRIPT', true);

require_once('../config.php');

$phone = required_param('phone', PARAM_TEXT);
$name = required_param('name', PARAM_TEXT);
$email = required_param('email', PARAM_TEXT);
$classid = required_param('classid', PARAM_INT);

$data->phone = $phone;
$data->name = $name;
$data->email = $email;
$data->classid = $classid;
$data->created_at = time();


$DB->insert_record('customer_register', $data);

return response_success();


