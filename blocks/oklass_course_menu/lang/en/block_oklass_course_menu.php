<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'oklass_course_menu', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   oklass_course_menu
 * @copyright Jinotech {@link http://jinotech.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Course home';
$string['title'] = 'Course home'; 

$string['courseinfo'] = 'Course information';
$string['participantlist'] = 'Participant list';
$string['syllabus'] = 'Syllabus ';

$string['gradeandattend'] = 'Grade/Attend';
$string['learningstats'] = 'Learning Stats';
$string['progressstats'] = 'Progress Stats';
$string['attendonline'] = 'Online attendance book';
$string['attendoffline'] = 'Offline attendance book';
$string['gradebook'] = 'Grade book';
$string['objection'] = 'Objection';

$string['sendnotification'] = 'Send notification';
$string['sendmessage'] = 'Send message';
$string['sendmail'] = 'Send mail';
$string['settingnotification'] = 'Send notification';

$string['coursemanage'] = 'Course management';
$string['coursesetting'] = 'Course setting';
$string['groupsetting'] = 'Group setting';
$string['groupparticipant'] = 'Group participant';
$string['questionbank'] = 'Question bank';
$string['courselog'] = 'Course log';

$string['changerole'] = 'Show student screen';
$string['backrole'] = 'Return to role';
$string['teamediting'] = 'Team management';
$string['teammember'] = 'Team Member';

$string['messageview'] = 'View Message';

$string['offline_attendance'] = 'Online Attendance'; 
$string['courseqna'] = 'Course Q&A';