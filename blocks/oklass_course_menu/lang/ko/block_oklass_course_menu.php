<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'oklass_course_menu', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   oklass_course_menu
 * @copyright Jinotech {@link http://jinotech.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = '강의실 홈';
$string['title'] = '강의실 홈'; 

$string['courseinfo'] = '강의정보';
$string['participantlist'] = '참여자 목록';
$string['syllabus'] = '강의계획서';

$string['gradeandattend'] = '성적/출석';
$string['learningstats'] = '학습활동현황';
$string['progressstats'] = '진도현황';
$string['attendonline'] = '온라인출석부';
$string['attendoffline'] = '오프라인출석부';
$string['gradebook'] = '성적부';
$string['objection'] = '이의신청';

$string['sendnotification'] = '알림보내기';
$string['sendmessage'] = '메시지보내기';
$string['sendmail'] = '메일보내기';
$string['settingnotification'] = '알림설정';

$string['coursemanage'] = '강의관리';
$string['coursesetting'] = '강의설정';
$string['groupsetting'] = '그룹설정';
$string['groupparticipant'] = '그룹참여자';
$string['questionbank'] = '문제은행';
$string['courselog'] = '강의기록';

$string['changerole'] = '학습자 화면 보기';
$string['backrole'] = '과정운영자 화면 보기';
$string['teamediting'] = '팀 관리';
$string['teammember'] = '팀원';

$string['messageview'] = '메시지보기';

$string['offline_attendance'] = '온라인 출석부'; 
$string['courseqna'] = '과정Q&A'; 