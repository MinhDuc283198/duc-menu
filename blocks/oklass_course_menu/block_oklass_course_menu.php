<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * JINO Site main menu block.
 *
 * @package    oklass_course_menu
 * @copyright  Jinotech (http://jinotech.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_oklass_course_menu extends block_list {

    public function init() {
        global $CFG,$COURSE;
        $this->title = html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/course/view.php?id=" . $COURSE->id));
        $this->title .= get_string('title', 'block_oklass_course_menu');
        $this->title .= html_writer::end_tag('a');
    }

    public function applicable_formats() {
        return array('all' => true);
    }
    public function instance_allow_multiple() {
        return true;
    }

    public function get_content() {
        global $CFG, $DB, $USER, $PAGE, $OUTPUT, $COURSE;
        
        $PAGE->requires->js('/blocks/oklass_course_menu/oklass_course_menu.js');
        $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
        

        if ($this->content !== null) {
            return $this->content;
        }
        if($COURSE->format == 'site'){
            return true;
        }

        //$this->title = '';

        $this->content = new stdClass;
        $this->content->items = array();
        $this->content->icons = array();

        $currentlang = current_language();

        $menu_html = '';
        $menu_html .= html_writer::start_tag('p', array('class' => 'down'));
        $menu_html .= get_string('courseinfo', 'block_oklass_course_menu');
        $menu_html .= html_writer::end_tag('p');

        $sub_html = '';
        $sub_html .= html_writer::start_tag('ul');
        
        //여기에 section0에 대한 학습활동 가져오기 시작    
        $section = course_get_format($COURSE)->get_section(0);
        $sequences = explode(',',$section->sequence);
        foreach ($sequences as $moduleid) {
            
            $sql = "SELECT cm.id,cm.instance,mo.name
                  FROM {course_modules} cm 
                  JOIN {modules} mo ON mo.id = cm.module
                  WHERE cm.id=:moduleid and cm.visible=1 and mo.visible=1 and mo.name!=:lcmsprogress";

            if($module = $DB->get_record_sql($sql, array('moduleid' => $moduleid, 'lcmsprogress'=>'lcmsprogress'))){
                if($mod = $DB->get_record($module->name,array('id'=>$module->instance))){
                    $url = new moodle_url($CFG->wwwroot.'/mod/'.$module->name.'/view.php',array('id'=>$module->id,'popup'=>1));
                    if($COURSE->format == 'oklass_mooc'){
                        $url = 'javascript:window.open(\''.$url.'\',\'menuopen\',\'width=1200, height=800,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no\');void(0);';
                    }
                    $sub_html .= html_writer::start_tag('li');
                    $sub_html .= html_writer::start_tag('a', array('href' => $url));
                    $sub_html .= $mod->name;
                    $sub_html .= html_writer::end_tag('a');
                    $sub_html .= html_writer::end_tag('li');
                }
            }
        }
        
        //section0 학습활동 가져오기 끝
        /*        
        $sub_html .= html_writer::start_tag('li');
        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/jinoboard/list.php?id=7", 'target'=>'_blank'));
        $sub_html .= get_string('courseqna', 'block_oklass_course_menu');
        $sub_html .= html_writer::end_tag('a');
        $sub_html .= html_writer::end_tag('li');        
        */
        
        if (has_capability('moodle/course:manageactivities', $context)){
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/course/report/statistics/progress.php?id=" . $COURSE->id));
            $sub_html .= get_string('progressstats', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
        }
        
        if (has_capability('moodle/course:manageactivities', $context) && $COURSE->format != 'oklass_mooc') {
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/course/report/statistics/modules.php?id=" . $COURSE->id));
            $sub_html .= get_string('learningstats', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
        }

        
        //참여자 목록은 과정에 관리 권한이 있는 사용자만 볼 수 있게 처리
        if (has_capability('moodle/course:manageactivities', $context)) {
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/okcourse/userindex.php?id=" . $COURSE->id));
            $sub_html .= get_string('participantlist', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
        }
        
        $sub_html .= html_writer::start_tag('li');
        if (has_capability('moodle/course:manageactivities', $context)) {
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/grade/report/yieldgrader/index.php?id=" . $COURSE->id));
        }else{
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/grade/report/user/index.php?id=" . $COURSE->id));
        }
        $sub_html .= get_string('gradebook', 'block_oklass_course_menu');
        $sub_html .= html_writer::end_tag('a');
        $sub_html .= html_writer::end_tag('li');
        
        $sub_html .= html_writer::start_tag('li');
        if (has_capability('moodle/course:manageactivities', $context)) {
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/course_objection_application/index.php?id=" . $COURSE->id));
            $sub_html .= get_string('objection', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
        }else{
            $sql = "SELECT * FROM {course} c JOIN {lmsdata_class} lc ON c.id = lc.courseid WHERE c.id = :courseid";
            $course_objection = $DB->get_record_sql($sql,array("courseid"=>$COURSE->id));
            
            $now_time = strtotime(date('Y-m-d H:i:s'));
            if($course_objection->appealstart<=$now_time){
                if($course_objection->appealend>=$now_time){
                    $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/course_objection_application/index.php?id=" . $COURSE->id));
                    $sub_html .= get_string('objection', 'block_oklass_course_menu');
                    $sub_html .= html_writer::end_tag('a');
                    $sub_html .= html_writer::end_tag('li');
                }
            }

        }
      
        
        
        $sub_html .= html_writer::end_tag('ul');

        $menu_html .= $sub_html;
        $this->content->items[] = $menu_html;
        
        if(!$lmsdata_class = $DB->get_record('lmsdata_class',array('courseid'=>$COURSE->id))){
            $lmsdata_class = $DB->get_record('lmsdata_course',array('courseid'=>$COURSE->id));
        }
        
        if(has_capability('moodle/course:manageactivities', $context) && $lmsdata_class->notice == 1){
            $menu_html = '';
            $menu_html .= html_writer::start_tag('p', array('class' => 'down'));
            $menu_html .= get_string('sendnotification', 'block_oklass_course_menu');
            $menu_html .= html_writer::end_tag('p');

            $sub_html = '';
            $sub_html .= html_writer::start_tag('ul');
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/sendmessage/index.php?id=" . $COURSE->id));
            $sub_html .= get_string('sendmessage', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
            
            /*
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/message/index.php?viewing=course_" . $COURSE->id));
            $sub_html .= get_string('messageview', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
            */
            
            $sub_html .= html_writer::start_tag('li'); 
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/sendmail/index.php?id=" . $COURSE->id));
            $sub_html .= get_string('sendmail', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
            $sub_html .= html_writer::end_tag('ul');

            $menu_html .= $sub_html;
            $this->content->items[] = $menu_html;
        }
        /* 알림 끝 */
        
if (has_capability('moodle/course:manageactivities', $context)) {
        $menu_html = '';
        $menu_html .= html_writer::start_tag('p', array('class' => 'down'));
        $menu_html .= get_string('coursemanage', 'block_oklass_course_menu');
        $menu_html .= html_writer::end_tag('p');    

        $sub_html = '';
        $sub_html .= html_writer::start_tag('ul');
        
        /*
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/okmanage/index.php?id=" . $COURSE->id));
            $sub_html .= get_string('coursesetting', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
*/
//            $sub_html .= html_writer::start_tag('li');
//            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/okteam/index.php?id=" . $COURSE->id));
//            $sub_html .= get_string('teamediting', 'block_oklass_course_menu');
//            $sub_html .= html_writer::end_tag('a'); 
//            $sub_html .= html_writer::end_tag('li'); 
        
//        $sub_html .= html_writer::start_tag('li');
//        $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/local/okteam/overview.php?id=" . $COURSE->id));
//        $sub_html .= get_string('teammember', 'block_oklass_course_menu');
//        $sub_html .= html_writer::end_tag('a'); 
//        $sub_html .= html_writer::end_tag('li'); 
 
            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/question/edit.php?courseid=" . $COURSE->id));
            $sub_html .= get_string('questionbank', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');

            $sub_html .= html_writer::start_tag('li');
            $sub_html .= html_writer::start_tag('a', array('href' => $CFG->wwwroot . "/report/log/index.php?id=" . $COURSE->id));
            $sub_html .= get_string('courselog', 'block_oklass_course_menu');
            $sub_html .= html_writer::end_tag('a');
            $sub_html .= html_writer::end_tag('li');
        
        
            $sub_html .= html_writer::end_tag('ul');
            $menu_html .= $sub_html;
            $this->content->items[] = $menu_html;
        }
        
        $button_html = '';
        $context = context_course::instance($COURSE->id);
        //학생 화면 보기 버튼
        if (has_capability('moodle/role:switchroles', $context)) {
            $link = html_writer::tag('a',get_string('changerole', 'block_oklass_course_menu'), array('href' => $CFG->wwwroot .'/course/switchrole.php?id=' . $COURSE->id . "&sesskey=" . sesskey() . "&switchrole=5&returnurl=".$PAGE->url));
            $button_html .= html_writer::tag('div',$link, array('class' => 'screen'));
        }
        //원래 역활로 돌아가기 버튼
        if (is_role_switched($COURSE->id)) {
             $link = html_writer::tag('a', get_string('backrole', 'block_oklass_course_menu'), array('href' => $CFG->wwwroot .'/course/switchrole.php?id=' . $COURSE->id . "&sesskey=" . sesskey() . "&switchrole=0&returnurl=".$PAGE->url));
            $button_html .= html_writer::tag('div',$link, array('class' => 'screen'));
        }
        $this->content->items[] = $button_html;
        
        

        $this->content->footer = '';

        return $this->content;
    }

}
