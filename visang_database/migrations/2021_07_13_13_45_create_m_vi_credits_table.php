<?php

$table = new xmldb_table('m_vi_credits');
$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('employer_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
$table->add_field('credit_point', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
$table->add_field('expire_date', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
$table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
$table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_key('foreign', XMLDB_KEY_FOREIGN, array('employer_id'),' m_vi_employers','id');