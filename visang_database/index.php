<?php 

require('../config.php');

require_login(0, false);

global $CFG, $DB;

if (!admin() && !app_prod()) 
{
    echo 'Permission!';
    die;
}

$dbman = $DB->get_manager(); // Loads ddl manager and xmldb classes.

// create migration table manager
$table = new xmldb_table('visang_migrations');
$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('filename', XMLDB_TYPE_TEXT, null, null, null, null, null);
$table->add_field('created_at', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

if (!$dbman->table_exists($table)) {
    $dbman->create_table($table); 
}
$migration_table = $table->getName();
//create migration

$files = glob('./migrations' . '/*.php');

foreach ($files as $file) {

    require($file);

    if (!$dbman->table_exists($table)) {

        $dbman->create_table($table);

        $data_migration->filename = $file;

        $data_migration->created_at = time();

        $DB->insert_record($migration_table, $data_migration);

        echo $file . '<br>';
    }
}