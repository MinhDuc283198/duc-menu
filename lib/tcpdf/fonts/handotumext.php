<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='HCRDotumExt';
$up=-75;
$ut=50;
$dw=500;
$diff='';
$originalsize=1725428;
$enc='';
$file='handotumext.z';
$ctg='handotumext.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-657 -371 4084 1377]','ItalicAngle'=>0,'Ascent'=>1070,'Descent'=>-231,'Leading'=>0,'CapHeight'=>886,'XHeight'=>886,'StemV'=>70,'StemH'=>30,'AvgWidth'=>882,'MaxWidth'=>4137,'MissingWidth'=>500);
$cw=array(0=>500,65535=>500);
// --- EOF ---
