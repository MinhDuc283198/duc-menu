<!doctype html>
<html lang="en">
<?php include('../config.php'); ?>
<head>
    <?php echo get_visang_config('javascript_header'); ?>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Giảm giá 30% tất cả các khóa học | Free giáo trình | Ưu đãi chỉ trong tháng 8</title>
    <meta name="description" content="Hỗ trợ mùa dịch, Master Korean giảm giá 30% toàn bộ khóa học (trừ khóa Siêu vỡ lòng chỉ 99k), tặng kèm sách giáo trình tương ứng với khóa học."/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .banner {
            width: 100%;
        }
        .bg-gray {
            background-color: rgb(246, 246, 246);
        }
        .gift-item {
            display: flex;
            align-items: center;
        }
        @media screen and (max-width: 600px) {
            iframe {
                width: 100% !important;
                height: auto;
            }
        }
        .benefit-item {
            display: flex;
            align-items: center;
        }
        .benefit-item img {
            width: 35px;
            height: auto;

        }
        .feedback-item img {
            width: 135px;
            height: auto;
        }
        h3 {
            color: rgb(72, 91, 198);
            font-weight: bold;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

</head>
<body>
<?php echo get_visang_config('javascript_body'); ?>
    <?php if(isPC()) { ?>
        <a href="#register">
            <img class="banner lazy" data-src="/lading_page/img/landingpage.webp" alt="">
        </a>
    <?php } else { ?>
        <a href="#register">
            <img class="banner lazy" data-src="/lading_page/img/landingpage-mobile.webp" alt="">
        </a>
    <?php } ?>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-6">
                <h3>Dù bạn ở trình độ nào, Master Korean cũng có khóa học phù hợp cho bạn</h3>
            </div>
            <div class="col-md-6">
                <p>Master Korean được học viên công nhận có thư viện khóa học đa dạng. Đồng thời đáp ứng đầy đủ nhu cầu của cộng đồng học tiếng Hàn như:
                    Khóa học dành người mới bắt đầu, Giao tiếp cơ bản, Sơ cấp, Trung cấp, Luyện thi TOPIK, Tiếng Hàn Business,...
                </p>
            </div>
        </div>
        <div class="course mt-5">
            <h4>Các khóa học Tiếng Hàn Visang</h4>
            <div class="row mt-3">
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=104">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Thumbnail-Beginner-courses-web.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6"
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=129">
                        <img class="img-fluid lazy" data-src="/lading_page/img/mastertalk-nhập-môn314x177.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=110">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Thumbnail Tiếng Hàn Sơ cấp 1-314x177.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=111">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Thumbnail Tiếng Hàn Sơ cấp 2-314x177..webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=112">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Thumbnail Trung cap 1-314x177..webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=113">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Thumbnail Trung cap 2-314x177..webp" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="course mt-5">
            <h4>Các khóa ôn thi TOPIK & Tiếng Hàn Business</h4>
            <div class="row mt-3">
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=94">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Thumbnail TOPIK-314x177..webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6"
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=120">
                        <img class="img-fluid lazy" data-src="/lading_page/img/eps-topik-314x177.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=130">
                        <img class="img-fluid lazy" data-src="/lading_page/img/cam-nang-eps-topik-314x177.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=119">
                        <img class="img-fluid lazy" data-src="/lading_page/img/master-pattern-314x177.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/detail.php?id=118">
                        <img class="img-fluid lazy" data-src="/lading_page/img/master-key-314x177.webp" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-6">
                    <a href="https://www.masterkorean.vn/local/course/list.php?id=5">
                        <img class="img-fluid lazy" data-src="/lading_page/img/Tieng Han Business co Ok-314x177.webp" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray">
        <h3 class="text-center mt-5 pt-5">
            Hãy cũng tìm hiểu về Master Korean nhé!
        </h3>
        <div class="text-center pt-3">
            <div class="iframe-content">
                <img class="lazy" style="max-width: 100%" data-src="/lading_page/img/Screenshot_1.png" alt="">
            </div>
            <div class="mt-4">
                <a href="#register" class="btn btn-danger mb-5" style="font-size: 25px">ĐĂNG KÝ NGAY</a>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <h3 class="text-center">Tại sao bạn nên lựa chọn Master Korean?</h3>
        <p class="mt-3 text-center">
           <strong> Master Korean</strong> là Hệ thống khóa học tiếng Hàn online số 1 Việt Nam, được cung cấp bởi <strong> Visang </strong> Việt Nam - tập đoàn giáo dục Hàn Quốc với hơn 20 năm kinh nghiệm.
        </p>
        <div class="row">
            <div class="col-md-4 p-5">
                <img class="img-fluid lazy" data-src="/lading_page/img/icon lịch - 488x485px.webp" alt="">
                <p class="text-center mt-3">
                    Có thể giao tiếp cơ bản bằng tiếng Hàn trong <b> 30 ngày</b>
                </p>
            </div>
            <div class="col-md-4 p-5">
                <img class="img-fluid lazy" data-src="/lading_page/img/icon topik-512x512px.webp" alt="">
                <p class="text-center mt-3">
                    Hỗ trợ tư vấn xây dựng lộ trình học
                    <b> đạt TOPIK nhanh nhất</b>
                </p>
            </div>
            <div class="col-md-4 p-5">
                <img class="img-fluid lazy" data-src="/lading_page/img/icon laptop-512x512px.webp" alt="">
                <p class="text-center mt-3">
                    Học online mọi lúc mọi nơi,
                    <b> không lo dịch bệnh</b>
                </p>
            </div>
        </div>
    </div>
    <div class="bg-gray" >
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col-md-7">
                    <h3>
                        Hãy mua khóa học ngay để nhận được
                        những ưu đãi hấp dẫn nhất tháng 9 nhé!
                    </h3>
                    <div class="gift-item mt-3">
                        <img class="img-gift mr-3 lazy" width="35px" data-src="/lading_page/img/icon quà tặng-90x90px.webp" alt="">
                        <p class="gift-content mb-0"> <b style="color: rgb(238, 99, 23)">Giảm 30%</b>  tất cả các khóa học của Master Korean (trừ Khóa học siêu vỡ lòng) </p>
                    </div>
                    <div class="gift-item mt-3">
                        <img class="img-gift mr-3 lazy" width="35px" data-src="/lading_page/img/icon quà tặng-90x90px.webp" alt="">
                        <p class="gift-content mb-0"> <b style="color: rgb(238, 99, 23)">Được trợ giảng hỗ trợ 1:1 </b> trong 3 buổi đầu tiên để học tập hiệu quả nhất</p>
                    </div>
                    <div class="gift-item mt-3">
                        <img class="img-gift mr-3 lazy" width="35px" data-src="/lading_page/img/icon quà tặng-90x90px.webp" alt="">
                        <p class="gift-content mb-0"> <b style="color: rgb(238, 99, 23)">Giảm 50% </b> khóa học dành cho người mới bắt đầu
                            <a href="https://www.masterkorean.vn/local/course/detail.php?id=126">(truy cập tại đây)</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-5" id="register">
                    <?php include('../common/course_register.php');  ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt-5">
        <h3 class="text-center">Điều gì khiến khóa học của Master Korean hiệu quả?</h3>
        <div class="row mt-3">
            <div class="col-md-6 benefit-item">
                <img class="mr-3 lazy" data-src="/lading_page/img/icon-like-60x61px.webp" alt="">
                <div>
                    <p class="mb-0"><b>Thương hiệu uy tín</b></p>
                    <p>Đến từ Visang Education - thương hiệu giáo dục, xuất bản hàng đầu Hàn Quốc với hơn 20 năm kinh nghiệm.</p>
                </div>
            </div>
            <div class="col-md-6 benefit-item">
                <img class="mr-3 lazy" data-src="/lading_page/img/icon-like-60x61px.webp" alt="">
                <div>
                    <p class="mb-0"><b>Bài giảng chất lượng xuất sắc</b></p>
                    <p>Chương trình học được xây dựng có hệ thống, dựa trên các giáo trình tự biên soạn và đội ngũ giảng viên Thạc sĩ,
                        Tiến sĩ chuyên môn cao từ Visang Education.</p>
                </div>
            </div>
            <div class="col-md-6 benefit-item">
                <img class="mr-3 lazy" data-src="/lading_page/img/icon-like-60x61px.webp" alt="">
                <div>
                    <p class="mb-0"><b>Học tiếng Hàn qua tình huống</b></p>
                    <p>Master Korean là thương hiệu đào tạo tiếng Hàn duy nhất đưa các video tình huống từ các diễn viên Hàn Quốc vào bài giảng,
                        giúp học viên hiểu rõ các chủ đề, ngữ pháp, từ vựng qua các đoạn video ngắn, từ đó có thể nắm chắc, tư duy ngôn ngữ như người bản xứ.</p>
                </div>
            </div>
            <div class="col-md-6 benefit-item">
                <img class="mr-3 lazy" data-src="/lading_page/img/icon-like-60x61px.webp" alt="">
                <div>
                    <p class="mb-0"><b>Cách học dễ dàng</b></p>
                    <p>Chỉ cần một thiết bị có kết nối internet, học viên có thể học Tiếng Hàn với Master Korean ở mọi lúc, mọi nơi.</p>
                </div>
            </div>
            <div class="col-md-6 benefit-item">
                <img class="mr-3 lazy" data-src="/lading_page/img/icon-like-60x61px.webp" alt="">
                <div>
                    <p class="mb-0"><b>Hỗ trợ tận tình</b></p>
                    <p>Được tư vấn trực tiếp 1:1 với đội ngũ trợ giảng lâu năm của Master Korean.</p>
                </div>
            </div>
            <div class="col-md-6 benefit-item">
                <img class="mr-3 lazy" data-src="/lading_page/img/icon-like-60x61px.webp" alt="">
                <div>
                    <p class="mb-0"><b>Tìm kiếm việc làm dễ dàng</b></p>
                    <p>Được kết nối trực tiếp với trang Master Korean Jobs, giúp học viên dễ dàng tìm kiếm và nhận được đề xuất
                        việc làm chất lượng từ các Công ty Hàn Quốc.</p>
                </div>
            </div>
        </div>

        <div class="text-center mt-5 mb-5">
            <h3>Feedback của học viên về Master Korean</h3>
            <div class="row mt-5">
                <div class="col-md-2">

                </div>

                <div class="col-md-4 ">
                    <div class="feedback-item text-center p-2 shadow rounded">
                        <img class="mb-5 mt-5 lazy" data-src="/lading_page/img/thủy nguyễn-276x329.webp" alt="">
                        <p class="mb-4">"Từ một người có trình độ trung cấp 1 tiếng Hàn giành được TOPIK cấp 5 trong kỳ thi năng lực tiếng Hàn..."</p>
                        <p class="mt-5 mb-0"><b>Thủy Nguyễn</b></p>
                        <p class="mb-5">Khóa Trung cấp 1</p>
                    </div>
                </div>

                <div class="col-md-4 ">
                    <div class="feedback-item text-center p-2 shadow rounded">
                        <img class="mb-5 mt-5 lazy" data-src="/lading_page/img/nhung trần-315x329px.webp" alt="">
                        <p class="mb-4">"Nhờ có Master Korean, tôi có thể dễ dàng ôn thi TOPIK tại nhà. Những tips mà cô Thúy giảng dạy thực sự hữu
                            ích và giúp tôi tăng điểm bài thi TOPIK..."
                        </p>
                        <p class="mt-5 mb-0"><b>Nhung Trần</b></p>
                        <p class="mb-5">Khóa Luyện thi TOPIK II</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div style="background: rgb(72, 91, 198);" class="text-center">
        <h2 class="pt-5 pb-5" style="color: white">Vui lòng đăng ký thông tin để nhận ưu đãi đặc biệt từ Master Korean Việt Nam nhé!</h2>
        <a href="#register" class="btn btn-danger mb-5" style="font-size: 25px">ĐĂNG KÝ NGAY</a>
    </div>
<a href="https://zalo.me/2282433133049043336" style="position: fixed; right: 15px; bottom: 15px">
    <img style="width: 70px" class="lazy"  data-src="/theme/oklassedu/pix/home/mobile/icon/zalo-chat-icon.png" alt="">
</a>
<?php if (isPC()) { echo get_visang_config('javascript_footer'); }; ?>
<script>

    !function(window){
        var $q = function(q, res){
                if (document.querySelectorAll) {
                    res = document.querySelectorAll(q);
                } else {
                    var d=document
                        , a=d.styleSheets[0] || d.createStyleSheet();
                    a.addRule(q,'f:b');
                    for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
                        l[b].currentStyle.f && c.push(l[b]);

                    a.removeRule(0);
                    res = c;
                }
                return res;
            }
            , addEventListener = function(evt, fn){
                window.addEventListener
                    ? this.addEventListener(evt, fn, false)
                    : (window.attachEvent)
                    ? this.attachEvent('on' + evt, fn)
                    : this['on' + evt] = fn;
            }
            , _has = function(obj, key) {
                return Object.prototype.hasOwnProperty.call(obj, key);
            }
        ;

        function loadImage (el, fn) {
            var img = new Image()
                , src = el.getAttribute('data-src');
            img.onload = function() {
                if (!! el.parent)
                    el.parent.replaceChild(img, el)
                else
                    el.src = src;

                fn? fn() : null;
            }
            img.src = src;
        }

        function elementInViewport(el) {
            var rect = el.getBoundingClientRect()

            return (
                rect.top    >= 0
                && rect.left   >= 0
                && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
            )
        }

        var images = new Array()
            , query = $q('img.lazy')
            , processScroll = function(){
                for (var i = 0; i < images.length; i++) {
                    if (elementInViewport(images[i])) {
                        loadImage(images[i], function () {
                            images.splice(i, i);
                        });
                    }
                };
            }
        ;
        // Array.prototype.slice.call is not callable under our lovely IE8
        for (var i = 0; i < query.length; i++) {
            images.push(query[i]);
        };

        processScroll();
        addEventListener('scroll',processScroll);

    }(this);

    $('.iframe-content img').click(function(){
        console.log(1);
        let text = `<iframe width="840" height="472" src="https://www.youtube.com/embed/WKnbT83ycK8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
        $('.iframe-content').html(text);
    });
</script>
</body>
</html>