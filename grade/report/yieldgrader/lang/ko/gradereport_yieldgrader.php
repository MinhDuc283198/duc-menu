<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradereport_grader', language 'en'
 *
 * @package   gradereport_grader
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = '최종 성적';

$string['grade_setting'] = '성적 설정';
$string['excelldown'] = '엑셀 파일 다운로드';
$string['hrdexport'] = 'HRD 내보내기';
$string['num'] = '번호';
$string['belong'] = '교육지명번호';
$string['rank'] = 'NEIS번호';
$string['username'] = '아이디';
$string['name'] = '이름';
$string['per'] = '백분률';
$string['final_grade'] = '최종 성적';

$string['gradeopen'] = '성적 공개 여부';
$string['open'] = '공개';
$string['private'] = '비공개';
$string['editdate'] = '성적 수정일';
$string['norecord'] = '기록 없음';