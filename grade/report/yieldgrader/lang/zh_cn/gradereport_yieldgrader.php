<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$string['pluginname'] = 'Final Grader Report';

$string['grade_setting'] = 'Grade Setting';
$string['excelldown'] = 'Excell File Download';
$string['hrdexport'] = 'HRD Export';
$string['num'] = 'no.';
$string['belong'] = '교육지명번호';
$string['rank'] = 'NEIS번호';
$string['username'] = 'Student ID';
$string['name'] = 'Name';
$string['per'] = 'Percentage';
$string['final_grade'] = 'Final Grade';

$string['gradeopen'] = 'Whether to publish credits';
$string['open'] = 'Open';
$string['private'] = 'Private';
$string['editdate'] = 'Settings Modified';
$string['norecord'] = 'No records';