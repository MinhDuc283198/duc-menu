<?php
require_once('../../../config.php');
require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->dirroot . '/user/renderer.php');
require_once($CFG->dirroot . '/grade/lib.php');
require_once($CFG->dirroot . '/grade/report/yieldgrader/lib.php');
require_once("$CFG->libdir/excellib.class.php");

$courseid = required_param('id', PARAM_INT);        // course id
$page = optional_param('page', 1, PARAM_INT);   // active page
$edit = optional_param('edit', -1, PARAM_BOOL); // sticky editting mode
$perpage = optional_param('perpage', 10, PARAM_INT);
$excell = optional_param('excell', 0, PARAM_INT);


if (isset($graderreportsifirst)) {
    $SESSION->gradereport['filterfirstname'] = $graderreportsifirst;
}
if (isset($graderreportsilast)) {
    $SESSION->gradereport['filtersurname'] = $graderreportsilast;
}

$PAGE->set_url(new moodle_url('/grade/report/yieldgrader/index.php', array('id' => $courseid)));
$PAGE->requires->yui_module('moodle-gradereport_grader-gradereporttable', 'Y.M.gradereport_grader.init', null, null, true);

// basic access checks
if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('nocourseid');
}
$lmsdata_class = $DB->get_record('lmsdata_class', array('courseid' => $courseid));
require_login($course);
$context = context_course::instance($course->id);

require_capability('gradereport/yieldgrader:view', $context);
require_capability('moodle/grade:viewall', $context);

// return tracking object
$gpr = new grade_plugin_return(array('type' => 'report', 'plugin' => 'yieldgrader', 'courseid' => $courseid, 'page' => $page));

// last selected report session tracking
if (!isset($USER->grade_last_report)) {
    $USER->grade_last_report = array();
}
$USER->grade_last_report[$course->id] = 'grader';

// Build editing on/off buttons

if (!isset($USER->gradeediting)) {
    $USER->gradeediting = array();
}

if (has_capability('moodle/grade:edit', $context)) {
    if (!isset($USER->gradeediting[$course->id])) {
        $USER->gradeediting[$course->id] = 0;
    }

    if (($edit == 1) and confirm_sesskey()) {
        $USER->gradeediting[$course->id] = 1;
    } else if (($edit == 0) and confirm_sesskey()) {
        $USER->gradeediting[$course->id] = 0;
    }

    // page params for the turn editting on
    $options = $gpr->get_options();
    $options['sesskey'] = sesskey();

    if ($USER->gradeediting[$course->id]) {
        $options['edit'] = 0;
        $string = get_string('turneditingoff');
    } else {
        $options['edit'] = 1;
        $string = get_string('turneditingon');
    }
    $buttons = new single_button(new moodle_url('index.php', $options), $string, 'get');
} else {
    $USER->gradeediting[$course->id] = 0;
    $buttons = '';
}

$gradeserror = array();

// Handle toggle change request
if (!is_null($toggle) && !empty($toggle_type)) {
    set_user_preferences(array('grade_report_show' . $toggle_type => $toggle));
}

// Perform actions
if (!empty($target) && !empty($action) && confirm_sesskey()) {
    grade_report_grader::do_process_action($target, $action, $courseid);
}

$reportname = get_string('pluginname', 'gradereport_yieldgrader');

// Do this check just before printing the grade header (and only do it once).
grade_regrade_final_grades_if_required($course);

$offset = ($page - 1) * $perpage;
$sql = "select u.*,lu.*,gg.finalgrade,gg.rawgrademax,gg.id as gradeid from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "left join {lmsdata_user} lu on lu.userid = u.id "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "left join {grade_items} gi on gi.courseid = c.instanceid and gi.itemtype = 'course' "
        . "left join {grade_grades} gg on gg.itemid = gi.id and gg.finalgrade is not null and gg.userid = u.id "
        . "where ra.contextid = :contextid ";
$where = '';
$param = array('contextid' => $context->id,'rolename'=>'student', 'contextlevel' => CONTEXT_COURSE);
if ($search) {
    $where = ' and  ((u.firstname like :searchtxt1 or u.lastname like :searchtxt2 or u. firstname||lastname like :searchtxt3) or u.username like :searchtxt4 )';
    $param['searchtxt1'] = $param['searchtxt2'] = $param['searchtxt3'] = $param['searchtxt4'] = '%' . $search . '%';
}

$users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param, $offset, $perpage);
$countsql = "select count(u.id) from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "where ra.contextid = :contextid ";
$param = array('contextid' => $context->id, 'rolename' => 'student', 'contextlevel' => CONTEXT_COURSE);
$usercount = $DB->count_records_sql($countsql . $where . ' order by u.username asc', $param);
$total_pages = ceil($usercount / $perpage);
$num = $usercount - $offset;

if($excell != 1){
// Print header
print_grade_page_head($COURSE->id, 'report', 'yieldgrader', $reportname, false, $buttons);

$renderer = $PAGE->get_renderer('core_user');
$url = $CFG->wwwroot.'/grade/report/yieldgrader/index.php?id='.$courseid;
echo $renderer->user_search($url, '', '', $num,$usercount , get_string('allusers','grades'));

//show warnings if any
foreach ($warnings as $warning) {
    echo $OUTPUT->notification($warning);
}


$displayaverages = true;
if ($numusers == 0) {
    $displayaverages = false;
}
?>
<form id="grade_edit_form" method="post" action="grade_edit.php?id=<?php echo $courseid; ?>">
    <div>
        <div class="edit_grade_on" style="display: none;">
            <div>
                <label for="showgrades01"><?php echo get_string('gradeopen','gradereport_yieldgrader'); ?></label>
                <input type="radio" id="showgrades01" name="showgrades" <?php if ($course->showgrades == 1) {
    echo 'checked';
    } ?> value="1"> <label for="showgrades02"><?php echo get_string('open','gradereport_yieldgrader'); ?></label>
                <input type="radio" id="showgrades02" name="showgrades" <?php if ($course->showgrades == 0) {
    echo 'checked';
} ?> value="0" > <?php echo get_string('private','gradereport_yieldgrader'); ?>
            </div>
            <!--<div><?php echo get_string('editdate','gradereport_yieldgrader'); ?> : <?php echo ($lmsdata_class->gradesettingdate == 0) ? get_string('norecord','gradereport_yieldgrader') : date('Y-m-d h:i', $lmsdata_class->gradesettingdate); ?></div>-->
        </div>
        <input type="button" onclick="toggle_edit()" title="<?php echo get_string('grade_setting', 'gradereport_yieldgrader'); ?>" value="<?php echo get_string('grade_setting', 'gradereport_yieldgrader'); ?>" />
    <input type="button" onclick="location.href='<?php echo $CFG->wwwroot.'/grade/report/yieldgrader/index.php?id='.$courseid.'&excell=1'; ?>'" title="<?php echo get_string('excelldown', 'gradereport_yieldgrader'); ?>" value="<?php echo get_string('excelldown', 'gradereport_yieldgrader'); ?>" />
    </div>
    <table class="generaltable">
        <caption class="hidden-caption"><?php echo get_string('hrd:grade','local_lmsdata'); ?></caption>
        <thead>
            <tr>
                <th scope="row"><?php echo get_string('num', 'gradereport_yieldgrader'); ?></th>
                <th scope="row"><?php echo get_string('username', 'gradereport_yieldgrader'); ?></th>
                <th scope="row"><?php echo get_string('name', 'gradereport_yieldgrader'); ?></th>
                <th scope="row"><?php echo get_string('email'); ?></th>
                <th scope="row"><?php echo get_string('per', 'gradereport_yieldgrader'); ?></th>
                <th scope="row"><?php echo get_string('final_grade', 'gradereport_yieldgrader'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            // 로그를 위해
                $targetids = array();
            foreach ($users as $user) {
                $targetids[] = $user->id;
                $percentage = ($user->finalgrade / $user->rawgrademax) * 100;

		    ?>
                <tr>
                    <td><?php echo $num--; ?></td>
                    <td><?php echo $user->username; ?></td>
                    <td><?php echo $user->lastname; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo (!is_number($percentage, 1) && !($user->finalgrade > 0) && empty($user->finalgrade))?'-': number_format($percentage, 1) . '%'; ?></td>
                    <td>
                        <span class="edit_grade_on" style="display: none;"><input type="text" title="search" size="5" name="gradeid[<?php echo $user->gradeid ?>]" value="<?php echo $user->finalgrade; ?>"></span>
                        <span class="edit_grade_off"><?php echo number_format($user->finalgrade, get_config('core', 'grade_export_decimalpoints')); ?></span>
                        /
    <?php echo number_format($user->rawgrademax, get_config('core', 'grade_export_decimalpoints')); ?>
                    </td>
                </tr>
<?php } 
?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7"><input type="submit" style="display: none;" class="edit_grade_on" value="<?php echo get_string('submit'); ?>"></td>
            </tr>
        </tfoot>
</form>
</table>
<?php
    $page_params = array();
    $page_params['id'] = $courseid;
    $page_params['perpage'] = $perpage;
    $page_params['search'] = $search;
    $page_params['searchfield'] = $searchfield;
    echo '<div class="table-footer-area">';
    yieldgrader_get_paging_bar($CFG->wwwroot . "/grade/report/yieldgrader/index.php", $page_params, $total_pages, $page);
    echo '</div>';
    
$event = \gradereport_grader\event\grade_report_viewed::create(
                array(
                    'context' => $context,
                    'courseid' => $courseid,
                )
);
$event->trigger();
?>
<script type="text/javascript">
    var toggle = 0;
    $('#grade_edit_form').submit(function () {
        if (toggle == 0) {
            return false;
        } else {

            return true;
        }
    });

    function toggle_edit() {
        if (toggle == 0) {
            $('.edit_grade_on').show();
            $('.edit_grade_off').hide();
            toggle = 1;
        } else {
            $('.edit_grade_on').hide();
            $('.edit_grade_off').show();
            toggle = 0;
        }
    }
    
</script>
<?php
echo $OUTPUT->footer();
} else {
    $fields = array(
                get_string('username', 'gradereport_yieldgrader'),
                get_string('name', 'gradereport_yieldgrader'),
                get_string('email'),
                get_string('per', 'gradereport_yieldgrader'),
                get_string('final_grade', 'gradereport_yieldgrader')
    );

    $filename = $course->fullname . '_grades.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col++, $fieldname);
    }

    $row = 1;

 

    foreach ($users as $user) {
                        $percentage = ($user->finalgrade / $user->rawgrademax) * 100;

        $col = 0;
        $worksheet[0]->write($row, $col++, $user->username);
        $worksheet[0]->write($row, $col++, $user->lastname);
        $worksheet[0]->write($row, $col++, $user->email);
        $worksheet[0]->write($row, $col++, (!is_number($percentage, 1))?'-': number_format($percentage, 1) . '%');
        $worksheet[0]->write($row, $col++, number_format($user->finalgrade, get_config('core', 'grade_export_decimalpoints')).'/'.number_format($user->rawgrademax, get_config('core', 'grade_export_decimalpoints')));
        $row++;
    }
    $workbook->close();
    die;
}