<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * oklass_lcms course format.  Display the whole course as "oklass_lcms" made of modules.
 *
 * @package format_oklass_lcms
 * @copyright 2006 The Open University
 * @author N.D.Freear@open.ac.uk, and others.
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');

// Horrible backwards compatible parameter aliasing..
if ($topic = optional_param('topic', 0, PARAM_INT)) {
    $url = $PAGE->url;
    $url->param('section', $topic);
    debugging('Outdated topic param passed to course/view.php', DEBUG_DEVELOPER);
    redirect($url);
}
// End backwards-compatible aliasing..

$context = context_course::instance($course->id);

$sql = 'SELECT lco.*, lc.learningstart, lc.learningend FROM {lmsdata_class} lc JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid WHERE lc.courseid = :courseid';
$courseinfo = $DB->get_record_sql($sql, array('courseid'=>$course->id));

$sql = 'SELECT SUM(grademax) as totalgrade FROM {grade_items} WHERE courseid = :courseid AND itemmodule = :itemmodule';
$quiz_grade = $DB->get_record_sql($sql, array('courseid'=>$course->id, 'itemmodule'=>'quiz'));

$sql = 'SELECT SUM(grademax) as totalgrade FROM {grade_items} WHERE courseid = :courseid AND itemmodule = :itemmodule';
$assign_grade = $DB->get_record_sql($sql, array('courseid'=>$course->id, 'itemmodule'=>'assign'));

$sql = 'SELECT SUM(scale) as totalgrade FROM {forum} WHERE course = :course';
$forum_grade = $DB->get_record_sql($sql, array('course'=>$course->id));

$total_grade = $DB->get_record('grade_items', array('courseid'=>$course->id, 'itemtype'=>'course'));

$min_progress = $DB->get_record('lcmsprogress',array('course'=>$course->id));
$min_grade = $DB->get_record('course_completion_criteria',array('course'=>$course->id, 'criteriatype'=>6));

if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// make sure all sections are created
$course = course_get_format($course)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));

$text = '';

    $text .= html_writer::start_tag("div", array("class" => "course-content"));
    $text .= html_writer::start_tag("ul", array("class" => "media"));
    
    /****** 강의 정보 *****/
    $evaluation_standard = '';
    
    //최소 진도율이 있을 때
    if($min_progress->completionprogress > 0){
        $evaluation_standard .= get_string('minprogress', 'format_oklass_mooc',$min_progress->completionprogress);
    }
    
    //최소 이수 점수가 있을 때
    if($min_grade->gradepass > 0){
        if($min_progress->completionprogress > 0) $evaluation_standard .= ' & ';
        $evaluation_standard .= get_string('mingrade', 'format_oklass_mooc',(int)$min_grade->gradepass);
    }
    
    if($min_grade->gradepass > 0 && $total_grade->grademax > 0){
        $evaluation_standard .= '(';
        $evaluation_standard .= get_string('totalgrade', 'format_oklass_mooc',(int)$total_grade->grademax);
        
        $grade_element = array();
        if($min_progress->grade > 0){
            $grade_element[] = get_string('grade:progress', 'format_oklass_mooc',(int)$min_progress->grade);
        }
        if($quiz_grade->totalgrade > 0){
            $grade_element[] = get_string('grade:quiz', 'format_oklass_mooc',(int)$quiz_grade->totalgrade);
        }
        if($forum_grade->totalgrade > 0){
            $grade_element[] = get_string('grade:forum', 'format_oklass_mooc',(int)$forum_grade->totalgrade);
        }
        if($assign_grade->totalgrade > 0){
            $grade_element[] = get_string('grade:assign', 'format_oklass_mooc',(int)$assign_grade->totalgrade);
        }
        
        $evaluation_standard .= implode(', ', $grade_element);
        $evaluation_standard .= ')';
    }
    
    if(empty($evaluation_standard)){
        $evaluation_standard = get_string('notevaluationstandard', 'format_oklass_mooc');
    }

    $learningperiod = '';
    if(isset($courseinfo->learningstart) && isset($courseinfo->learningend)){
        $learningperiod = date('Y-m-d', $courseinfo->learningstart)." ~ ".date('Y-m-d', $courseinfo->learningend);
    } else {
        $learningperiod = get_string('notlearningdate', 'format_oklass_mooc');
    }

    $text .= html_writer::start_tag("li", array("class" => "section main span5"));
    $text .= html_writer::tag("h3", get_string('section0name', 'format_oklass_mooc'),array("class" => "sectionname"));
    $text .= html_writer::start_tag("div", array("class" => "course-info"));
    $text .= html_writer::tag("p", get_string('trainingperiod', 'format_oklass_mooc'),array("class" => "title"));
    $text .= html_writer::tag("p", $learningperiod, array("class" => "orange text"));
    $text .= html_writer::tag("p", get_string('evaluationstandard', 'format_oklass_mooc'),array("class" => "title"));
    $text .= html_writer::tag("p", "$evaluation_standard",array("class" => "text"));
    $text .= html_writer::end_tag("div");
    $text .= html_writer::end_tag("li");
    
    /****** 강의 알람상황 *****/
    $text .= html_writer::start_tag("li", array("class" => "section main span7"));
    $text .= html_writer::tag("h3", get_string('lecturenotice', 'format_oklass_mooc'),array("class" => "sectionname"));
    $text .= html_writer::start_tag("ul", array("class" => "section img-text"));
    
    $notices = format_oklass_lcms_course_notices($course);
    if(!empty($notices)){
        $text .= $notices;
    } else {
        $text .= '알림사항이 없습니다.';
    }
    
//    $text .= html_writer::start_tag("li", array("class" => "activity"));
//    $text .= html_writer::start_tag("div", array("class" => "activityinstance"));
//    $text .= html_writer::start_tag("a", array("href" => "#"));
//    $text .= html_writer::tag("img", "" ,array("src" => "/theme/image.php/oklassedu/jinotechboard/1498177032/icon", "class" => "iconlarge activityicon"));
//    $text .= html_writer::tag("span", "강의 공지사항" ,array("class" => "instancename"));
//    $text .= html_writer::tag("span", "(마지막 접속 후 게시물 수 : 1)" ,array("class" => "explain gray"));
//    $text .= html_writer::end_tag("a");
//    $text .= html_writer::end_tag("div");
//    $text .= html_writer::end_tag("li");
    
    
    $text .= html_writer::end_tag("ul");
    $text .= html_writer::end_tag("li");
    
    
    /*     * **진도율**** */

    $sql = " SELECT l.id, l.contents, cm.id as cmid, cm.section FROM {lcms} l JOIN {course_modules} cm ON cm.instance = l.id JOIN {modules} mo ON mo.id = cm.module "
            . "where l.course = :courseid and l.type = 'html' and mo.name = 'lcms' and cm.visible = 1 and mo.visible = 1";
    $lcmses = $DB->get_records_sql($sql, array('courseid' => $course->id));
    $totalcount = 0;
    $myprogress = 0;
    $myplaytime = 0;
    $myplaypage = 0;
    $mycount = 0;
    $myaverage = 0;
    $playtime = '00:00';
    $secprogress = array();

    if (!empty($lcmses)) {
        foreach ($lcmses as $lcms) {
            
            $totalcount++;

            //진도율 가져오기
            $track = $DB->get_record_select('lcms_track', 'lcms=:lcmsid and userid=:userid', array('lcmsid' => $lcms->cmid, 'userid' => $USER->id));
            if ($track && $track->progress && $track->progress >= 0) {
                $myprogress += $track->progress;
                $myplaypage += $track->playpage;
            }
            $mycount++;
        }
    }
//    $myaverage = round($myprogress / $mycount);
    //소수점2째자리에서 버림
    if($mycount > 0){
        $myaverage = floor(($myprogress/$mycount) * 100) / 100;
    }

    $text .= html_writer::start_tag("li", array("class" => "section main clear progress-info"));
    $text .= html_writer::start_tag("div", array("class" => "top"));
    $text .= html_writer::start_tag("div", array("class" => "l_content"));
    $text .= html_writer::tag("strong", get_string('startdate', 'format_oklass_mooc'));
    $text .= html_writer::tag("span", date('Y-m-d', $course->startdate));
    $text .= "<br />";
    //$text .= html_writer::tag("strong", get_string('learningtime', 'format_oklass_mooc'));
    //$text .= html_writer::tag("span", $playtime, array("class" => "time"));
    $text .= html_writer::end_tag("div");
    $text .= html_writer::start_tag("div", array("class" => "r_content"));
    $text .= html_writer::tag("span", get_string('refresh', 'format_oklass_mooc'), array("class" => "refresh"));
    $text .= html_writer::end_tag("div");
    $text .= html_writer::start_tag("div", array("class" => "bottom"));
    $text .= html_writer::tag("div", html_writer::tag("p", "", array("class" => "bar")), array("class" => "bars"));
    $text .= html_writer::tag("strong", get_string('progress', 'format_oklass_mooc') . $myaverage . "%", array("class" => "percent"));
    $text .= html_writer::end_tag("div");
    $text .= html_writer::end_tag("div");
    $text .= html_writer::start_tag("div", array('style'=>'padding-top:10px;'));
    $text .= html_writer::tag("strong", '진도율을 확인하시려면 (미완료 / 진도율 %)를 클릭하여 주시기 바랍니다.' , array("class" => "text_info"));
    $text .= html_writer::end_tag("div");

    $text .= html_writer::end_tag("li");
    /*     * **진도율**** */
    
    echo $text;

$renderer = $PAGE->get_renderer('format_oklass_lcms');

if (!empty($displaysection)) {
    $renderer->print_single_section_page($course, null, null, null, null, $displaysection);
} else {
    $renderer->print_multiple_section_page($course, null, null, null, null);
}

// Include course format js module
$PAGE->requires->js('/course/format/oklass_lcms/format.js');
$PAGE->requires->js('/course/format/oklass_lcms/lcms.js');
