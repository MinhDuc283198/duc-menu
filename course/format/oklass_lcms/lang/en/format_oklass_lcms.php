<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_oklass_lcms', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_oklass_lcms
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['currentsection'] = 'This topic';
$string['editsection'] = 'Edit topic';
$string['editsectionname'] = 'Edit topic name';
$string['deletesection'] = 'Delete topic';
$string['newsectionname'] = 'New name for topic {$a}';
$string['sectionname'] = 'Topic';
$string['pluginname'] = 'oklass_lcms format';
$string['section0name'] = 'Course Info';
$string['page-course-view-oklass_lcms'] = 'Any course main page in oklass_lcms format';
$string['page-course-view-oklass_lcms-x'] = 'Any course page in oklass_lcms format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';

$string['complete']='complete';
$string['incomplete']='Incomplete';
$string['learningtime']='Learning time';
$string['allprogress']='Total progress';
$string['progress']='Progress';
$string['startdate']='Start date';

$string['notevaluationstandard']='There is no evaluation criteria.';
$string['notlearningdate']='The Learning period is not set.';
$string['minprogress']='Minimum progress rate over {$a}%';
$string['mingrade']='Grade {$a} points or more';
$string['totalgrade']='Total Grade {$a} Point = ';
$string['grade:progress']='Progress {$a}%';
$string['grade:quiz']='Exam {$a}%';
$string['grade:forum']='Discussion Participation {$a}%';
$string['grade:assign']='Assignment {$a}%';

$string['trainingperiod'] = 'Training period';
$string['evaluationstandard'] = 'Evaluation standard';
$string['lecturenotice'] = 'Lecture notice';
$string['refresh'] = 'refresh';
$string['learn'] = 'Learn';
