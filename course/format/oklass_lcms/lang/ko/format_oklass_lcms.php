<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_oklass_lcms', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_oklass_lcms
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['currentsection'] = 'This topic';
$string['editsection'] = 'Edit topic';
$string['editsectionname'] = 'Edit topic name';
$string['deletesection'] = 'Delete topic';
$string['newsectionname'] = 'New name for topic {$a}';
$string['sectionname'] = 'Topic';
$string['pluginname'] = '강의콘텐츠형';
$string['section0name'] = '강의정보';
$string['page-course-view-oklass_lcms'] = 'Any course main page in oklass_lcms format';
$string['page-course-view-oklass_lcms-x'] = 'Any course page in oklass_lcms format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';

$string['complete']='완료';
$string['incomplete']='미완료';
$string['learningtime']='학습시간';
$string['allprogress']='전체 진도율';
$string['progress']='진도율';
$string['startdate']='시작일';

$string['notevaluationstandard']='평가기준이 없습니다.';
$string['notlearningdate']='교육기간이 설정되어있지 않습니다.';
$string['minprogress']='최소 진도율 {$a}% 이상';
$string['mingrade']='총점 {$a}점 이상';
$string['totalgrade']='총점 {$a}점 = ';
$string['grade:progress']='진도율 {$a}%';
$string['grade:quiz']='시험 {$a}%';
$string['grade:forum']='토론참여 {$a}%';
$string['grade:assign']='과제 {$a}%';

$string['trainingperiod'] = '교육기간';
$string['evaluationstandard'] = '평가기준';
$string['lecturenotice'] = '강의알림사항';
$string['refresh'] = '새로고침';
$string['learn'] = '학습하기';
