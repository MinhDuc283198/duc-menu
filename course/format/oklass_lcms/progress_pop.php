<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->dirroot. '/mod/okmedia/lib.php');

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$lcmsid= optional_param('lcmsid', 0, PARAM_INT);
$sql = 'SELECT ld.* FROM {lcms_duration} ld  
join {lcms} l on l.contents = ld.contents 
join {course_modules} cm on cm.instance = l.id 
WHERE cm.id = :lcmsid order by ld.page asc';
$durations = $DB->get_records_sql($sql,array('lcmsid'=>$lcmsid));

$sql = 'select l.id, l.name '
        . 'from {lcms} l join {course_modules} cm on cm.instance = l.id '
        . 'join {lcms_contents} lc on lc.id = l.contents where cm.id = :lcmsid';
$lcms = $DB->get_record_sql($sql,array('lcmsid'=>$lcmsid));

$popupwidth = !$lcms->popupwidth? '1280':$lcms->popupwidth;
$popupheight = !$lcms->popupheight? '900':$lcms->popupheight;

?>  
<style>
    .pop_wrap {
    float:left;width:100%;
    margin-top: -10px;
}
#id_duration{
    position:relative;margin-top:15px;width:100%;height:10px;background:#ddd;
}
.pop_wrap .poshtmls{
    margin-top:0;position:absolute;top:0;height:10px;background:#00abcd;
}
#id_duration .balloon{
    font-size: 12px; height: 33px; width: 75px; top:14px;
}
</style>
<div id="popwrap">
    <h3 id="pop_title">
        <?php echo $lcms->name;?>
        <img class="close r_content" src="/theme/oklassedu/pix/images/close.png" alt="" />
    </h3>

    <div id="pop_content">
            <table class="table table-condensed">
            <thead>
                <tr>
                    <th class="w50px">페이지</th>
                    <th class="w100px">제목</th>
                    <th class="none">학습현황</th>
                    <th class="w50px">진도율</th>
                    <th class="w50px">학습하기</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                
                foreach($durations as $duration){
                    $html = '';
                    //진도체크하는 페이지만 학습현황과 진도율 표시함.
                    if($duration->progress == 1){
$query = 'select positionfrom,positionto from {lcms_playtime} where lcmsid = :lcmsid and userid = :userid and positionpage = :positionpage and positionfrom < positionto order by positionfrom, positionto';
$plays = $DB->get_records_sql($query,array('lcmsid'=> $lcmsid,'userid'=>$USER->id,'positionpage'=>$duration->page));

$poses = array();
$playtime = 0;
$start = -1;
$end = -1;
foreach($plays as $play){
    if ($start == -1) {       
        $start = $play->positionfrom;
        $end = $play->positionto;
    } else {
        if (($start <= $play->positionfrom && $play->positionfrom <= $end) && ($end <= $play->positionto)) {
            $end = $play->positionto;
        } else if ($end < $play->positionfrom) {
            $start = $play->positionfrom;
            $end = $play->positionto;
        }
    }
    $poses[$start] = $end;
    $playtime += $end-$start;
}

$poshtml = '';
$start = $end = 0;
$progress = floor(($playtime/$duration->duration) * 100);
if($progress > 100) $progress = 100;
if($progress > 0){
foreach($poses as $key=>$val){
    $start = ($key/$duration->duration) * 100;
    $end = ($val/$duration->duration) * 100 - $start;
    $sptm = okmedia_time_from_seconds($key);    
    $eptm = okmedia_time_from_seconds($val);
    $splaytime = 'S: ' . $sptm->h . ':' . $sptm->m . ':' . $sptm->s;
    $eplaytime = 'E: ' . $eptm->h . ':' . $eptm->m . ':' . $eptm->s;
    $posgrade = html_writer::tag("div",$splaytime.'<br/>'.$eplaytime,array('class'=>'balloon'));
    $poshtml .= html_writer::tag("div" , $posgrade, array("class"=>"poshtmls", "data-start"=>$key, "data-end"=>$val, 'style'=>'left:'.$start.'%;width:'.$end.'%'));
}
//학습한 구간 표시
//$html .= html_writer::start_tag('li' , array("id"=>"durationbar1"));
$html .= html_writer::start_div('pop_wrap');
$html .= html_writer::start_tag('div',array("id"=>"id_duration","style"=>"margin-top:-15px"));
$html .= $poshtml;
$html .= html_writer::end_tag("div");
$html .= html_writer::end_tag("div");
} else {
    $html .= html_writer::tag('div','학습한 데이터가 없습니다.',array('style'=>'text-align:center;'));
    
}
$progresstxt = $progress.'%';
                    }else{
                         $html .= html_writer::tag('div','-',array('style'=>'text-align:center;'));
                         $progresstxt = '-';
                    }

//$html .= html_writer::end_tag("li");
                    
                ?>
                <tr style="height: 60px;">
                    <td><?php echo $duration->page ?></td>
                    <td><?php echo $duration->title ?></td>
                    <td><?php echo $html ?></td>
                    <td><?php echo $progresstxt; ?></td>
                    <td><input type="button" value="학습하기" class="btn brd orange" onclick="goto_seek(<?php echo $lcmsid;?>,<?php echo $duration->page;?>);"/></td>
                   
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>


</div>

<script>
    function goto_seek(id, page){
        var url = "<?php echo $CFG->wwwroot;?>/mod/lcms/package.php?id="+id+"&redirect=1&inpopup=1&page="+page;
        var wh = "width=<?php echo $popupwidth;?>px,height=<?php echo $popupheight;?>px,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no,fullscreen=yes";
        window.open(url,'',wh);
    }
</script>
