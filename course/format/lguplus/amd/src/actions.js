// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Various actions on modules and sections in the editing mode - hiding, duplicating, deleting, etc.
 *
 * @module     format_lguplus/actions
 * @package    format_lguplus
 * @copyright  2018 Jinhoon Jang
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      3.3
 */
define(['jquery', 'core/notification', 'core/str', 'core/url'],
    function($, notification, str, url) {
        var SELECTOR = {
            ACTIVITYSEL: 'select.select-activity',
            ACTIONAREA: '.add-activity-actions',
            ACTIVITYACTION: 'button.add-activity-action',
            SECTIONIDATTR: 'data-section'
        };

        /**
         * Wrapper for Y.Moodle.core_course.util.cm.getId
         *
         * @param {JQuery} element
         * @returns {Integer}
         */
        var getSectionId = function(element) {
            return $(element).attr(SELECTOR.SECTIONIDATTR);
        };
        
        var getSelectedModule = function(section) {
            return $(SELECTOR.ACTIVITYSEL + '[' + SELECTOR.SECTIONIDATTR + '=' + section + ']').val();
        };
        
        var addActivity = function(course, section, module) {
            
            if(module === "0") {
                str.get_strings([
                    {key: 'pleaseselectactivity', component: 'format_lguplus'},
                    {key: 'addactivity', component: 'format_lguplus'}
                ]).done(function(strings) {
                    notification.alert(
                        strings[1],
                        strings[0]
                    );
                });
            } else {

                var form = document.createElement("form");
                var parm = new Array();
                var input = new Array();

                form.action = "/course/modedit.php";
                form.method = "post";

                parm.push( ['add', module] );
                parm.push( ['section', section] );
                parm.push( ['course', course] );
                parm.push( ['return', 0] );
                parm.push( ['sr', 0] );


                for (var i = 0; i < parm.length; i++) {
                    input[i] = document.createElement("input");
                    input[i].setAttribute("type", "hidden");
                    input[i].setAttribute('name', parm[i][0]);
                    input[i].setAttribute("value", parm[i][1]);
                    form.appendChild(input[i]);
                }
                document.body.appendChild(form);
                form.submit();
            }
        };

        return /** @alias module:format_lguplus/actions */ {

            init: function(courseid) {
                $( document ).ready(function() {
                    $(SELECTOR.ACTIONAREA + ' ' + SELECTOR.ACTIVITYACTION).click(function(event){
                        var section = getSectionId(event.target);
                        var module = getSelectedModule(section);
                        addActivity(courseid, section, module);
                    });
                });
            }
        };
    });