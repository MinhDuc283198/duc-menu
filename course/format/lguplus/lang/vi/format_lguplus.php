<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_lguplus', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_lguplus
 * @copyright 2018 Jinhoon Jang
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addsections'] = 'Thêm chủ đề';
$string['currentsection'] = 'Chủ đề hiện tại';
$string['editsection'] = 'Sửa chủ đề';
$string['editsectionname'] = 'Sửa tên chủ đề';
$string['deletesection'] = 'Xóa chủ đề';
$string['newsectionname'] = 'Đổi tên chủ đề {$a}';
$string['sectionname'] = 'Chủ đề';
$string['pluginname'] = 'LGU + Định dạng';
$string['section0name'] = 'Tổng quát';
$string['sectionNname'] = 'Chủ đề {$a}';
$string['page-course-view-topics'] = 'Áp dụng chủ đề cho trang chính Khóa học';
$string['page-course-view-topics-x'] = 'Áp dụng chủ đề cho mọi trang Khóa học';
$string['hidefromothers'] = 'Ẩn chủ đề';
$string['showfromothers'] = 'Hiện chủ đề';
$string['courselist'] = 'Danh sách khóa học';
$string['courseintro'] = 'Giới thiệu khóa học';
$string['activitiestouse'] = 'Các hoạt động';
$string['activitiestouse_help'] = '';
$string['addepisode'] = 'Thêm sự kiện';
$string['addactivity'] = 'Thêm hoạt động';
$string['selectactivity'] = 'Chọn hoạt động';
$string['pleaseselectactivity'] = 'Vui lòng chọn hoạt động!';
$string['learningterm'] = 'Thời gian học';
$string['relearningterm'] = 'Thời gian ôn tập';
$string['recentlearning'] = 'Lịch sử học tập';
$string['norecentlearning'] = 'Không có lịch sử học tập';
$string['progress'] = 'Tiến độ';
$string['success'] = 'Hoàn thành';
$string['notsuccess'] = 'Chưa hoàn thành';
$string['days'] = 'Ngày';
$string['text'] = 'Vui lòng chọn một khóa học để tham gia.';