<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_lguplus', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_lguplus
 * @copyright 2018 Jinhoon Jang
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addsections'] = 'Add topics';
$string['currentsection'] = 'This topic';
$string['editsection'] = 'Edit topic';
$string['editsectionname'] = 'Edit topic name';
$string['deletesection'] = 'Delete topic';
$string['newsectionname'] = 'New name for topic {$a}';
$string['sectionname'] = 'Topic';
$string['pluginname'] = 'LGU+ format';
$string['section0name'] = 'General';
$string['sectionNname'] = 'Topic {$a}';
$string['page-course-view-topics'] = 'Any course main page in topics format';
$string['page-course-view-topics-x'] = 'Any course page in topics format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';
$string['courselist'] = 'Course List';
$string['courseintro'] = 'Course Introduction';
$string['activitiestouse'] = 'Activities to use';
$string['activitiestouse_help'] = '';
$string['addepisode'] = 'Add an episode';
$string['addactivity'] = 'Add an activity';
$string['selectactivity'] = 'Select an activity';
$string['pleaseselectactivity'] = 'Please select a activity!';
$string['learningterm'] = 'learningterm';
$string['relearningterm'] = 'relearningterm';
$string['recentlearning'] = 'recentlearning';
$string['norecentlearning'] = 'norecentlearning';
$string['progress'] = 'progress';
$string['success'] = 'success';
$string['notsuccess'] = 'notsuccess';
$string['days'] = 'days';
$string['text'] = 'Please select a course to take.';