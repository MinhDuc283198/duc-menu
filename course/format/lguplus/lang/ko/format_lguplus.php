<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_lguplus', language 'ko', branch 'MOODLE_20_STABLE'
 *
 * @package   format_lguplus
 * @copyright 2018 Jinhoon Jang
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addsections'] = '새 차수 추가';
$string['currentsection'] = '현재 차수';
$string['editsection'] = '편집';
$string['editsectionname'] = '이름 편집';
$string['deletesection'] = '삭제';
$string['newsectionname'] = '{$a}차수의 새 이름';
$string['sectionname'] = '차수';
$string['pluginname'] = '재능교육 포맷';
$string['section0name'] = '일반';
$string['sectionNname'] = '{$a}차';
$string['page-course-view-topics'] = 'Any course main page in topics format';
$string['page-course-view-topics-x'] = 'Any course page in topics format';
$string['hidefromothers'] = '차수 숨기기';
$string['showfromothers'] = '차수 보이기';
$string['courselist'] = '강좌 목록';
$string['courseintro'] = '강좌 소개';
$string['activitiestouse'] = '사용할 활동';
$string['activitiestouse_help'] = '';
$string['addepisode'] = '차시 추가';
$string['addactivity'] = '학습활동 추가';
$string['selectactivity'] = '학습활동 선택';
$string['pleaseselectactivity'] = '학습활동을 선택하세요!';
$string['learningterm'] = '수강기간';
$string['relearningterm'] = '복습 기간';
$string['recentlearning'] = '최근학습보기';
$string['norecentlearning'] = '학습이력이 없습니다';
$string['progress'] = '진도율';
$string['success'] = '완료';
$string['notsuccess'] = '미완료';
$string['days'] = '일';
$string['text'] = '수강할 강의를 선택해주세요.';