// Javascript functions for Topics course format

$(document).ready(function () {
    $('.frontvideo').click("click", $.proxy(function (event) {
        event.preventDefault();
        var contentid = $(event.currentTarget).parent().attr('data-id');
        var contenturl = $(event.currentTarget).attr('conurl');
//        var browser = utils.check_mobile();
        var popup;
        var _isApp = sessionStorage.getItem("app");
        //news.viewcountup(contentid);
        var url = "openPopup," + contenturl + "&type=" + 0 + "&typeid=" + 0 + ",resizable=no,width=750px,height=565px,scrollbars=yes";
        if (_isApp == "Y") {
            utils.openPopup(contenturl + "&type=" + 0 + "&typeid=" + 0);
            window.parent.postMessage(url, "*");
        } else {
            window.open(contenturl + "&type=" + 0 + "&typeid=" + 0, 'mediaopen', 'resizable=no,width=750px,height=565px,scrollbars=yes');
        }
//                            if (!browser) {
//                                window.open(contenturl + "&type=" + 0 + "&typeid=" + 0, 'mediaopen', 'resizable=no,width=729px,height=1000px');
//                            } else if (browser) {
////                                window.parent.postMessage(url, "*");
//                                window.open(contenturl + "&type=" + 0 + "&typeid=" + 0, 'mediaopen', 'resizable=no,width=729px,height=1000px');
//                            }

    }, this));
    $('.c-hash-area span').click("click", $.proxy(function (event) {
        var hash_text = $(event.currentTarget).text();
        search_submit(hash_text);
    }, this));
    function search_submit(searchtext) {
        post_to_url('/local/search/index.php', {'text': searchtext})
    }
    function post_to_url(path, params, method) {
        method = method || "post"; // Set method to post by default, if not specified.
        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);
        for (var key in params) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
        document.body.appendChild(form);
        form.submit();
    }
});

M.course = M.course || {};

M.course.format = M.course.format || {};

/**
 * Get sections config for this format
 *
 * The section structure is:
 * <ul class="topics">
 *  <li class="section">...</li>
 *  <li class="section">...</li>
 *   ...
 * </ul>
 *
 * @return {object} section list configuration
 */
M.course.format.get_config = function () {
    return {
        container_node: 'ul',
        container_class: 'topics',
        section_node: 'li',
        section_class: 'section'
    };
}

/**
 * Swap section
 *
 * @param {YUI} Y YUI3 instance
 * @param {string} node1 node to swap to
 * @param {string} node2 node to swap with
 * @return {NodeList} section list
 */
M.course.format.swap_sections = function (Y, node1, node2) {
    var CSS = {
        COURSECONTENT: 'course-content',
        SECTIONADDMENUS: 'section_add_menus'
    };

    var sectionlist = Y.Node.all('.' + CSS.COURSECONTENT + ' ' + M.course.format.get_section_selector(Y));
    // Swap menus.
    sectionlist.item(node1).one('.' + CSS.SECTIONADDMENUS).swap(sectionlist.item(node2).one('.' + CSS.SECTIONADDMENUS));
}

/**
 * Process sections after ajax response
 *
 * @param {YUI} Y YUI3 instance
 * @param {array} response ajax response
 * @param {string} sectionfrom first affected section
 * @param {string} sectionto last affected section
 * @return void
 */
M.course.format.process_sections = function (Y, sectionlist, response, sectionfrom, sectionto) {
    var CSS = {
        SECTIONNAME: 'sectionname'
    },
            SELECTORS = {
                SECTIONLEFTSIDE: '.left .section-handle .icon'
            };

    if (response.action == 'move') {
        // If moving up swap around 'sectionfrom' and 'sectionto' so the that loop operates.
        if (sectionfrom > sectionto) {
            var temp = sectionto;
            sectionto = sectionfrom;
            sectionfrom = temp;
        }

        // Update titles and move icons in all affected sections.
        var ele, str, stridx, newstr;

        for (var i = sectionfrom; i <= sectionto; i++) {
            // Update section title.
            var content = Y.Node.create('<span>' + response.sectiontitles[i] + '</span>');
            sectionlist.item(i).all('.' + CSS.SECTIONNAME).setHTML(content);
            // Update move icon.
            ele = sectionlist.item(i).one(SELECTORS.SECTIONLEFTSIDE);
            str = ele.getAttribute('alt');
            stridx = str.lastIndexOf(' ');
            newstr = str.substr(0, stridx + 1) + i;
            ele.setAttribute('alt', newstr);
            ele.setAttribute('title', newstr); // For FireFox as 'alt' is not refreshed.
        }
    }
}
