<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the lguplus course format.
 *
 * @package format_lguplus
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since Moodle 2.3
 */
defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/course/format/renderer.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');

/**
 * Basic renderer for lguplus format.
 *
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_lguplus_renderer extends format_section_renderer_base {

    public $coursecomplete = array();

    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_lguplus_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {
        global $PAGE, $COURSE;

        $o = '';
        if ($PAGE->user_is_editing()) {
            $o .= html_writer::start_div('c-list');
            $o .= html_writer::start_div('add-episode_new_box');
            $o .= html_writer::tag('h3', get_string('courselist', 'theme_oklassedu'));
            $straddepisode = get_string('addepisode', 'format_lguplus');
            $urladdepisode = new moodle_url('/course/changenumsections.php', array('courseid' => $COURSE->id,
                'insertsection' => 0,
                'sesskey' => sesskey(),
                'sectionreturn' => 0,
                'numsections' => 1));
            $o .= html_writer::start_span('add-episode');
            $o .= html_writer::tag('a', $straddepisode, array('href' => $urladdepisode));
            $o .= html_writer::end_span();
            $o .= html_writer::end_div();
            $o .= html_writer::end_div();
        }
        $o .= html_writer::start_tag('div', array('class' => "course-content"));
        $o .= html_writer::start_tag('ul', array('class' => "newweeks"));
        return $o;
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        $o = '';
        $o .= html_writer::end_tag('ul');
        $o .= html_writer::end_tag('div');
        return $o;
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return get_string('topicoutline');
    }

    /**
     * Generate the section title, wraps it in a link to the section page if page is to be displayed on a separate page
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title($section, $course) {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section));
    }

    /**
     * Generate the section title to be displayed on the section page, without a link
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title_without_link($section, $course) {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section, false));
    }

    /**
     * Generate the edit control items of a section
     *
     * @param stdClass $course The course entry from DB
     * @param stdClass $section The course_section entry from DB
     * @param bool $onsectionpage true if being printed on a section page
     * @return array of edit control items
     */
    protected function section_edit_control_items($course, $section, $onsectionpage = false) {
        global $PAGE;

        if (!$PAGE->user_is_editing()) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $controls = array();
        if ($section->section && has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) {  // Show the "light globe" on/off.
                $url->param('marker', 0);
                $markedthistopic = get_string('markedthistopic');
                $highlightoff = get_string('highlightoff');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marked',
                    'name' => $highlightoff,
                    'pixattr' => array('class' => '', 'alt' => $markedthistopic),
                    'attr' => array('class' => 'editing_highlight', 'title' => $markedthistopic,
                        'data-action' => 'removemarker'));
            } else {
                $url->param('marker', $section->section);
                $markthistopic = get_string('markthistopic');
                $highlight = get_string('highlight');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marker',
                    'name' => $highlight,
                    'pixattr' => array('class' => '', 'alt' => $markthistopic),
                    'attr' => array('class' => 'editing_highlight', 'title' => $markthistopic,
                        'data-action' => 'setmarker'));
            }
        }

        $parentcontrols = parent::section_edit_control_items($course, $section, $onsectionpage);

        // If the edit key exists, we are going to insert our controls after it.
        if (array_key_exists("edit", $parentcontrols)) {
            $merged = array();
            // We can't use splice because we are using associative arrays.
            // Step through the array and merge the arrays.
            foreach ($parentcontrols as $key => $action) {
                $merged[$key] = $action;
                if ($key == "edit") {
                    // If we have come to the edit key, merge these controls here.
                    $merged = array_merge($merged, $controls);
                }
            }

            return $merged;
        } else {
            return array_merge($controls, $parentcontrols);
        }
    }

    private function course_intro($courseid) {
        global $PAGE, $DB, $USER;

        $o = '';

//        $courseinfo = local_lmsdata_get_course_info($courseid);
        $course_info_sql = " SELECT * "
                . "FROM {course} co "
                . "left JOIN {lmsdata_class} lc on lc.courseid = co.id "
                . "WHERE co.id = :courseid ";
        $courseinfo = $DB->get_record_sql($course_info_sql, array('courseid' => $courseid));
        $this->coursecomplete = format_lguplus_get_course_progress($courseid);
        $category = $DB->get_record('course_categories', array('visible' => 1, 'id' => $courseinfo->category), '*');
        $category_path = array_filter(explode('/', $category->path));
        $i = 0;
        $category_text = '';
        if ($category_path) {
            foreach ($category_path as $cp) {
                //$category_name = $DB->get_record('course_categories', array('visible' => 1, 'id' => $cp), 'name');
                $category_name = $DB->get_record_sql("select cc.id, cc.name, ccl.en_name, ccl.vi_name from {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid WHERE cc.id = ?", array($cp));
                switch (current_language()) {
                    case 'ko' :
                        $category_text = $category_name->name;
                        break;
                    case 'en' :
                        $category_text = $category_name->en_name;
                        break;
                    case 'vi' :
                        $category_text = $category_name->vi_name;
                        break;
                }
                //$category_text .= $category_name->name;
                $i++;
                if ($i != count($category_path)) {
                    $category_text .= ' > ';
                }
            }
        }
        $lastlearningsql = "select id,objecttable,contextinstanceid,objectid 
                            from moodle.m_logstore_standard_log 
                            where  target = ? and action = ? and userid = ? and courseid = ?
                            order by id desc limit 1 ";
        $lastlearning = $DB->get_record_sql($lastlearningsql, array("target" => "course_module", "action" => "viewed", 'userid' => $USER->id, 'courseid' => $courseid));

        if ($lastlearning) {
            // Get data from parameter.
            if (is_object($lastlearning->objectid)) {
                $instanceid = $lastlearning->objectid->id;
            } else {
                $instanceid = (int)$lastlearning->objectid;
            }
            $pagetable = '{' . $lastlearning->objecttable . '}';
            $iscourse = $DB->get_record_sql("
                    SELECT c.*
                      FROM $pagetable instance
                      JOIN {course} c ON c.id = instance.course
                     WHERE instance.id = ?", array($instanceid));

            if($iscourse){
                $lastmodsql = " select * from moodle.m_$lastlearning->objecttable where id = ?";
                $lastmod = $DB->get_record_sql($lastmodsql, array('id' => $lastlearning->objectid));
                list ($course, $cm) = get_course_and_cm_from_instance($lastlearning->objectid, $lastlearning->objecttable);
            }else{
                $lastmodule = $DB->get_record($lastlearning->objecttable,array('id'=>$lastlearning->objectid));
                if($lastmodule){
                    $course = get_course($lastmodule->course);
                }
            }

        if ($lastlearning) {
            $lastmodule = $DB->get_record($lastlearning->objecttable, array('id' => $lastlearning->objectid));
        }
        if ($lastmodule) {
            $course = get_course($lastmodule->course);
        }

        }
        $okmediamodule = $DB->get_field('modules', 'id', array('name' => 'okmedia'));

        $clickaction = ["onclick" => $cm->onclick, 'class' => 'btns'];
        $o .= html_writer::start_div('tp-crs-info');
        $o .= html_writer::start_div('tp');
        switch (current_language()) {
            case 'ko' :
                $coruse_name_lang = $courseinfo->title;
                break;
            case 'en' :
                $coruse_name_lang = $courseinfo->en_title;
                break;
            case 'vi' :
                $coruse_name_lang = $courseinfo->vi_title;
                break;
        }
        $o .= html_writer::tag('h3', html_writer::tag('span', $category_text) . ($courseinfo->title == null ? " Master Course -" . $courseinfo->fullname : $coruse_name_lang));
        $o .= html_writer::start_tag('p', array('class' => 'dt'));
        $enrolusersql = "select ue.timestart,ue.timeend
                        from {enrol} e
                        join {user_enrolments} ue on e.id = ue.enrolid and e.enrol = ?
                        where e.courseid = ? and ue.userid = ?";
        $enroluser = $DB->get_record_sql($enrolusersql,array('manual',$courseid,$USER->id));
        $adminsql = "select u.id from {user} u JOIN {lmsdata_user} lu ON u.id = lu.userid WHERE u.id = :uid AND lu.usergroup=:usergroup";
        $adminuser = $DB->get_record_sql($adminsql,array('uid'=>$USER->id,'usergroup'=>'ad'));

        if ($enroluser->timestart && !$adminuser) {
            $courseinfo->reviewperiod;
            $enddate = strtotime(date('Y-m-d', $enroluser->timeend) ." -$courseinfo->reviewperiod"."days");
            $o .= html_writer::tag('span', get_string('learningterm', 'format_lguplus') . ': ' . date('Y-m-d', $enroluser->timestart) . '~' . date('Y-m-d', $enddate));
        } else {
            //$o .= html_writer::tag('span', get_string('learningterm', 'format_lguplus') . ': ' . date('Y-m-d', $courseinfo->learningstart) . '~' . date('Y-m-d', $courseinfo->learningend));
            $o .= html_writer::tag('span', get_string('learningterm', 'format_lguplus') . ': ' . $courseinfo->courseperiod. get_string('days', 'format_lguplus'));
        }
         
        
        if($courseinfo->reviewperiod != 0){
            $o .= html_writer::tag('span', get_string('relearningterm', 'format_lguplus') . ': ' . $courseinfo->reviewperiod . get_string('days', 'format_lguplus'));
        }
        $o .= html_writer::end_tag('p');
        $o .= html_writer::end_div();
        $o .= html_writer::start_div('bt');
        $courseprogress = $this->coursecomplete['course']->courseprogress;
        if ($lastlearning) {
            $o .= html_writer::tag('a', get_string('recentlearning', 'format_lguplus'), $clickaction);

            $csquery = "select * from {course_sections} where course=:mcid and section = :section ORDER BY section ASC";
            $cslsit = $DB->get_record_sql($csquery, array('mcid' => $courseid, 'section' => $cm->sectionnum));
            $titleclass = '';
            if($iscourse){
                if(!empty($cslsit->name)){
                    $titleclass = $cslsit->name.' ';
                }else{
                    $titleclass = $cm->sectionnum .get_string('class2','local_course').' ';
                }
            }else{
                $titleclass = get_string('text', 'format_lguplus');
            }
            $o .= html_writer::tag('span', $titleclass) . html_writer::tag('span', $lastmod->name, array('class' => 't-blue'));
            $o .= html_writer::start_div('f-r');
            $o .= html_writer::tag('p', get_string('progress', 'format_lguplus') . html_writer::tag('strong', "$courseprogress%"));
            $o .= html_writer::start_div('bar-event', array('data-num' => "$courseprogress%"));
            $o .= html_writer::tag('span');
        } else {
            $o .= html_writer::tag('a', get_string('norecentlearning', 'format_lguplus'), array('href' => '#', 'class' => 'btns'));
            $o .= html_writer::start_div('f-r');
            $o .= html_writer::tag('span');
        }
        $o .= html_writer::end_div();
        $o .= html_writer::end_div();
        $o .= html_writer::end_div();
        $o .= html_writer::end_div();

        return $o;
    }

    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE, $DB;
        echo $this->course_intro($course->id);
        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $okmediamodule = $DB->get_field('modules', 'id', array('name' => 'okmedia'));
        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);

        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
//        echo $this->course_activity_clipboard($course, 0);
        //echo $this->course_intro($course->id);
        // Now the list of sections..
        echo $this->start_section_list();

        $numsections = course_get_format($course)->get_last_section_number();

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) {
                continue;
            }
            if ($section > $numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            if (!$PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                // Display section summary only.
                echo $this->section_summary($thissection, $course, null);
            } else {
                // 섹션 헤더 표시
                echo $this->section_header($thissection, $course, false, 0);
                if ($thissection->uservisible) {
                    //섹션 완료여부 표시
                    echo $this->visang_course_section_cm_list($course, $thissection, 0);
                    echo $this->course_section_add_cm_control($course, $section, 0);
                }
                echo $this->section_footer();
            }
        }

        if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
            // Print stealth sections if present.
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section <= $numsections or empty($modinfo->sections[$section])) {
                    // this is not stealth section or it is empty
                    continue;
                }
                echo $this->stealth_section_header($section);
                echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                echo $this->stealth_section_footer();
            }

            echo $this->end_section_list();

            //echo $this->change_number_sections($course, 0);
        } else {
            echo $this->end_section_list();
        }
    }

    /**
     * Output the html for a multiple section page
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections (argument not used)
     * @param array $mods (argument not used)
     * @param array $modnames (argument not used)
     * @param array $modnamesused (argument not used)
     */
    public function print_multiple_section_page_editing($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);
        //echo $this->course_intro($course->id);
        // Now the list of sections..
        echo $this->start_section_list();
        $menuitems = theme_oklassv2_get_coursemenu_items($course->id);
        ?>
        <li class="section-0   main clearfix">
            <div class="section-header">
                <h3 class="sectionname"><?php echo get_string('common', "theme_oklassedu"); ?></h3>
            </div>
            <div class="activities">
        <?php foreach ($menuitems as $menuitem) { ?>
                    <span class="activityinstance <?php echo $menuitem->visible == 1 ? 'visible' : 'not-visible' ?>">
                        <span class="instancename"><a href="<?php echo $menuitem->url ?>"><?php echo get_string($menuitem->name, 'theme_oklassedu') ?></a></span>
                        <span class="action-<?php echo $menuitem->action->class ?>"><a href="<?php echo $menuitem->action->url ?>"><?php echo $menuitem->action->text ?></a></span>
                    </span>
        <?php } ?>
            </div>
        </li>
        <?php
        $numsections = course_get_format($course)->get_last_section_number();

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) {
                continue;
            }
            if ($section > $numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            if (!$PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                // Display section summary only.
                echo $this->section_summary($thissection, $course, null);
            } else {
                echo $this->section_header($thissection, $course, false, 0);
                if ($thissection->uservisible) {
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    echo $this->course_section_add_cm_control($course, $section, 0);
                }
                echo $this->section_footer();
            }
        }

        if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
            // Print stealth sections if present.
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section <= $numsections or empty($modinfo->sections[$section])) {
                    // this is not stealth section or it is empty
                    continue;
                }
                echo $this->stealth_section_header($section);
                echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                echo $this->stealth_section_footer();
            }

            echo $this->end_section_list();

            //echo $this->change_number_sections($course, 0);
        } else {
            echo $this->end_section_list();
        }

        // 학습활동 추가 이벤트 등록
        $PAGE->requires->js_call_amd('format_lguplus/actions', 'init', array($course->id));
    }

    /**
     * Generate the display of the header part of a section before
     * course modules are included
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @param bool $onsectionpage true if being printed on a single-section page
     * @param int $sectionreturn The section to return to after an action
     * @return string HTML to output.
     */
    protected function section_header($section, $course, $onsectionpage, $sectionreturn = null) {
        global $PAGE, $DB, $USER;

        $o = '';
        $currenttext = '';
        $sectionstyle = '';

        if ($section->section != 0) {
            // Only in the non-general sections.
            if (!$section->visible) {
                $sectionstyle = ' hidden';
            }
            if (course_get_format($course)->is_section_current($section)) {
                $sectionstyle = ' current';
            }
        }

        $o .= html_writer::start_tag('li', array('id' => 'section-' . $section->section,
                    'class' => 'section main clearfix' . $sectionstyle, 'role' => 'region',
                    'aria-label' => get_section_name($course, $section)));

        // Create a span that contains the section title to be used to create the keyboard section move menu.
        $o .= html_writer::tag('span', get_section_name($course, $section), array('class' => 'hidden sectionname'));

        $o .= html_writer::start_tag('div', array('class' => 'content'));

        // When not on a section page, we display the section titles except the general section if null
        $hasnamenotsecpg = (!$onsectionpage && ($section->section != 0 || !is_null($section->name)));

        // When on a section page, we only display the general section title, if title is not the default one
        $hasnamesecpg = ($onsectionpage && ($section->section == 0 && !is_null($section->name)));

        $classes = ' accesshide';
        if ($hasnamenotsecpg || $hasnamesecpg) {
            $classes = '';
        }
        $sectionname = html_writer::tag('span', $this->section_title($section, $course));
        $sectionname .= html_writer::start_tag("div", array('class' => "f-r"));
        if ($this->coursecomplete[$section->section]->sectioncomplete && $this->coursecomplete[$section->section]->completecount ) {
            $sectionname .= html_writer::tag("span", date("Y-m-d", $this->coursecomplete[$section->section]->completetime), array('class' => "dt"));
            $sectionname .= html_writer::tag("a", get_string('success', 'format_lguplus'), array('class' => "btns complete"));
        }
        $sectionname .= html_writer::end_tag("div");
        if (course_can_delete_section($course, $section) && $PAGE->user_is_editing()) {
            if (get_string_manager()->string_exists('deletesection', 'format_' . $course->format)) {
                $strdelete = get_string('deletesection', 'format_' . $course->format);
            } else {
                $strdelete = get_string('deletesection');
            }
            $url = new moodle_url('/course/editsection.php', array(
                'id' => $section->id,
                'sr' => $sectionreturn,
                'delete' => 1,
                'sesskey' => sesskey()));

            $sectionname .= html_writer::start_span('delete_section');

            $authority_sql = " SELECT ra.userid FROM {context} ct
            JOIN {role_assignments} ra ON ra.contextid = ct.id AND ra.userid = :userid
            JOIN {role} r ON r.id = ra.roleid AND (r.shortname = 'manager' OR r.shortname = 'editingteacher' OR r.shortname = 'teacher')
            WHERE ct.contextlevel = :contextlevel AND ct.instanceid = :courseid ";
            $authority_check = $DB->get_record_sql($authority_sql, array('contextlevel' => CONTEXT_COURSE, 'userid' => $USER->id, 'courseid' => $course->id));
            if ($authority_check || is_siteadmin()) {
                $sectionname .= html_writer::tag('a', $strdelete, array('href' => $url));
            }

            $sectionname .= html_writer::end_span();
            $sectionname .= html_writer::end_div();
        }
        $deleteurl = '/course/editsection.php?id=' . $section->id . '&sr&delete=1&sesskey=' . sesskey();
        $o .= html_writer::start_div('section-header');
        $o .= $this->output->heading($sectionname, 3, 'sectionname' . $classes);




        return $o;
    }

    /**
     * Renders HTML for the menus to add activities and resources to the current course
     *
     * @param stdClass $course
     * @param int $section relative section number (field course_sections.section)
     * @param int $sectionreturn The section to link back to
     * @param array $displayoptions additional display options, for example blocks add
     *     option 'inblock' => true, suggesting to display controls vertically
     * @return string
     */
    function course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array()) {

        // 학습활동이 이미 추가 되어 있는 경우
        $modinfo = get_fast_modinfo($course);
//        if($modinfo->sections[$section]) {
//            return '';
//        }
//        $vertical = !empty($displayoptions['inblock']);
        // check to see if user can add menus and there are modules to add
        if (!has_capability('moodle/course:manageactivities', context_course::instance($course->id)) || !$this->page->user_is_editing() || !($modnames = get_module_types_names()) || empty($modnames) || !($activitiestouse = get_config('format_lguplus', 'activitiestouse')) || empty($activitiestouse)) {
            return '';
        }

        $activities = explode(',', $activitiestouse);

        $output = '';

        $output .= html_writer::start_div('add-activity-actions');
        $output .= html_writer::start_tag('select', array('class' => 'select-activity', 'data-section' => $section));
        $output .= html_writer::tag('option', get_string('selectactivity', 'format_lguplus'), array('value' => '0'));
        foreach ($activities as $activity) {
            $output .= html_writer::tag('option', $modnames[$activity], array('value' => $activity));
        }
        $output .= html_writer::end_tag('select');
        $output .= html_writer::tag('button', get_string('addactivity', 'format_lguplus'), array('class' => 'add-activity-action', 'data-section' => $section));
        $output .= html_writer::end_div();

        return $output;
    }

    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        // Can we view the section in question?
        if (!($sectioninfo = $modinfo->get_section_info($displaysection))) {
            // This section doesn't exist
            print_error('unknowncoursesection', 'error', null, $course->fullname);
            return;
        }

        if (!$sectioninfo->uservisible) {
            if (!$course->hiddensections) {
                echo $this->start_section_list();
                echo $this->section_hidden($displaysection, $course->id);
                echo $this->end_section_list();
            }
            // Can't view this section.
            return;
        }

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, $displaysection);
        $thissection = $modinfo->get_section_info(0);
        if ($thissection->summary or ! empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
            echo $this->start_section_list();
            echo $this->section_header($thissection, $course, true, $displaysection);
            echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
//            echo $this->course_section_add_cm_control($course, 0, $displaysection);
            echo $this->section_footer();
            echo $this->end_section_list();
        }

        // Start single-section div
        echo html_writer::start_tag('div', array('class' => 'single-section'));

        // The requested section page.
        $thissection = $modinfo->get_section_info($displaysection);

        // Title with section navigation links.
        $sectionnavlinks = $this->get_nav_links($course, $modinfo->get_section_info_all(), $displaysection);
        $sectiontitle = '';
        $sectiontitle .= html_writer::start_tag('div', array('class' => 'section-navigation navigationtitle'));
//        $sectiontitle .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'mdl-left'));
//        $sectiontitle .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'mdl-right'));
        // Title attributes
        $classes = 'sectionname';
        if (!$thissection->visible) {
            $classes .= ' dimmed_text';
        }
        $sectionname = html_writer::tag('span', $this->section_title_without_link($thissection, $course));
        $sectiontitle .= $this->output->heading($sectionname, 3, $classes);

        $sectiontitle .= html_writer::end_tag('div');
        echo $sectiontitle;

        // Now the list of sections..
        echo $this->start_section_list();

        echo $this->section_header($thissection, $course, true, $displaysection);
        // Show completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();

        echo $this->visang_course_section_cm_list($course, $thissection, $displaysection);
        echo $this->course_section_add_cm_control($course, $displaysection, $displaysection);
        echo $this->section_footer();
        echo $this->end_section_list();

        // Display section bottom navigation.
        $sectionbottomnav = '';
        $sectionbottomnav .= html_writer::start_tag('div', array('class' => 'section-navigation mdl-bottom'));
//        $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'mdl-left'));
//        $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'mdl-right'));
        $sectionbottomnav .= html_writer::tag('div', $this->section_nav_selection($course, $sections, $displaysection), array('class' => 'mdl-align'));
        $sectionbottomnav .= html_writer::end_tag('div');
        echo $sectionbottomnav;

        // Close single-section div.
        echo html_writer::end_tag('div');

        // 학습활동 추가 이벤트 등록
        $PAGE->requires->js_call_amd('format_lguplus/actions', 'init', array($course->id));
    }

    public function visang_course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = array()) {
        global $USER;

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // check if we are currently in the process of moving a module with JavaScript disabled
        $ismoving = $this->page->user_is_editing() && ismoving($course->id);
        if ($ismoving) {
            $movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', array('class' => 'movetarget'));
            $strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
        }

        // Get the list of modules visible to user (excluding the module being moved if there is one)
        $moduleshtml = array();
        if (!empty($modinfo->sections[$section->section])) {
            foreach ($modinfo->sections[$section->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];

                if ($ismoving and $mod->id == $USER->activitycopy) {
                    // do not display moving mod
                    continue;
                }

                if ($modulehtml = $this->visang_course_section_cm_list_item($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
                    $moduleshtml[$modnumber] = $modulehtml;
                }
            }
        }

        $sectionoutput = '';
        if (!empty($moduleshtml) || $ismoving) {
            foreach ($moduleshtml as $modnumber => $modulehtml) {
                if ($ismoving) {
                    $movingurl = new moodle_url('/course/mod.php', array('moveto' => $modnumber, 'sesskey' => sesskey()));
                    $sectionoutput .= html_writer::tag('li', html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)), array('class' => 'movehere'));
                }

                $sectionoutput .= $modulehtml;
            }

            if ($ismoving) {
                $movingurl = new moodle_url('/course/mod.php', array('movetosection' => $section->id, 'sesskey' => sesskey()));
                $sectionoutput .= html_writer::tag('li', html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)), array('class' => 'movehere'));
            }
        }

        // Always output the section module list.
        $output .= html_writer::tag('ul', $sectionoutput, array('class' => 'section img-text'));

        return $output;
    }

    public function visang_course_section_cm_list_item($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
        $output = '';
        if ($modulehtml = $this->courserenderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
            $modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
            $output .= html_writer::tag('li', $modulehtml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
        }
        return $output;
    }

}
