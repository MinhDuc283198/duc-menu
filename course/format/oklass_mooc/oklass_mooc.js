$(document).ready(function () {
    /* 
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    
    // 기본값으로 펼침
    $(".media_list .listname").show();
    $('.media_list .listname').each(function () {
        if ($(this).hasClass("on")) {
            $(this).find(" ~ li").each(function () {
                if ($(this).hasClass("listitem")) {
                    $(this).show();
                }
            });   
        }
    });

    /** media list events **/
    $(".media_list .listname").toggle(function () {
        $(this).removeClass("on");
        $(this).find(" ~ li").each(function () {
            if ($(this).hasClass("listitem")) {
                $(this).hide();
            } else {
                return false;
            }
        });
    }, function () {
        $(this).addClass("on");
        $(this).find(" ~ li").each(function () {
            if ($(this).hasClass("listitem")) {
                $(this).show();
            } else {
                return false;
            }
        });
    });
    /** media list events **/

    /** page refresh event **/
    $(".refresh").click(function () {
        location.reload();
    });
    /** page refresh event **/
    var percent = $(".percent").text().replace("진도율", "");
    $(".bars .bar").width(percent);
    /**  **/
});