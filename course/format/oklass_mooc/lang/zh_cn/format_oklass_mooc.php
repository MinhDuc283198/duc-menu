<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$string['addsection'] = '添加项目';
$string['addsubsection'] = '添加详细项目';
$string['backtocourse'] = 'Back to course \'{$a}\'';
$string['backtosection'] = 'Back to \'{$a}\'';
$string['cancelmoving'] = 'Cancel moving \'{$a}\'';
$string['confirmdelete'] = '"是否删除该项目？
所有活动内容和详细项目也会被删除。"';
$string['currentsection'] = '本周';
$string['deletesection'] = '删除周';
$string['displaycontent'] = '显示内容';
$string['editsectionname'] = '修改项目名称';
$string['hidefromothers'] = '隐藏主题';
$string['mergeup'] = '与以上合并';
$string['newsectionname'] = 'New name for section {$a}';
$string['page-course-view-oklass_mooc'] = 'Any course main page in oklass_mooc format';
$string['page-course-view-oklass_mooc-x'] = 'Any course page in oklass_mooc format';
$string['pluginname'] = 'Media Format';
$string['removemarker'] = '现在进行中，不显示。';
$string['section0name'] = '课程信息';
$string['sectionname'] = 'Topic';
$string['setmarker'] = '以现在进行中显示。';
$string['showcollapsed'] = '显示折叠状态';
$string['showexpanded'] = '显示打开状态';
$string['showfromothers'] = '显示主题';
$string['trainingperiod'] = 'Training period';
$string['evaluationstandard'] = 'Evaluation standard';
$string['lecturenotice'] = 'Lecture notice';
$string['refresh'] = 'refresh';
$string['learn'] = 'Learn';
$string['lastlearn'] = '최근 학습창으로 이동';

$string['complete']='完成';
$string['incomplete']='未完成';
$string['learningtime']='学习时间';
$string['allprogress']='Total progress';
$string['progress']='进度率';
$string['startdate']='开始日期';