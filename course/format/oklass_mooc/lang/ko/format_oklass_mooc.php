<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English language file for oklass_mooc format
 *
 * @package    format_oklass_mooc
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addsection'] = '항목 추가';
$string['addsubsection'] = '부항목 추가';
$string['backtocourse'] = 'Back to course \'{$a}\'';
$string['backtosection'] = 'Back to \'{$a}\'';
$string['cancelmoving'] = 'Cancel moving \'{$a}\'';
$string['confirmdelete'] = '"해당 항목을 삭제하시겠습니까?
모든 활동 내용과 세부 항목들 또한 삭제됩니다."';
$string['currentsection'] = '이번 주';
$string['deletesection'] = '주 삭제';
$string['displaycontent'] = '내용 표시';
$string['editsectionname'] = '항목명 수정';
$string['hidefromothers'] = '주제 감추기';
$string['mergeup'] = '상위와 통합';
$string['newsectionname'] = '{$a} 섹션의 새로운 이름';
$string['page-course-view-oklass_mooc'] = 'Any course main page in oklass_mooc format';
$string['page-course-view-oklass_mooc-x'] = 'Any course page in oklass_mooc format';
$string['pluginname'] = '미디어형식';
$string['removemarker'] = '현재 진행 중으로 표시하지 않음';
$string['section0name'] = '강의정보';
$string['trainingperiod'] = '교육기간';
$string['evaluationstandard'] = '평가기준';
$string['lecturenotice'] = '강의알림사항';
$string['refresh'] = '새로고침';
$string['learn'] = '학습하기';
$string['lastlearn'] = '최근 학습창으로 이동';

$string['sectionname'] = 'Topic';
$string['setmarker'] = '현재 진행 중으로 표시';
$string['showcollapsed'] = '접은 상태 표시';
$string['showexpanded'] = '펼친 상태 표시';
$string['showfromothers'] = 'Show topic';
$string['comp'] = '주제 표시';

$string['complete']='완료';
$string['incomplete']='미완료';
$string['learningtime']='학습시간';
$string['allprogress']='전체 진도율';
$string['progress']='진도율';
$string['startdate']='시작일';

$string['notevaluationstandard']='평가기준이 없습니다.';
$string['notlearningdate']='교육기간이 설정되어있지 않습니다.';
$string['minprogress']='최소 진도율 {$a}% 이상';
$string['mingrade']='총점 {$a}점 이상';
$string['totalgrade']='총점 {$a}점 = ';
$string['grade:progress']='진도율 {$a}%';
$string['grade:quiz']='시험 {$a}%';
$string['grade:forum']='토론참여 {$a}%';
$string['grade:assign']='과제 {$a}%';
