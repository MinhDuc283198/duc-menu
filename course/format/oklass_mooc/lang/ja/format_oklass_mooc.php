<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$string['addsection'] = '項目の追加';
$string['addsubsection'] = '詳細項目の追加';
$string['backtocourse'] = 'Back to course \'{$a}\'';
$string['backtosection'] = 'Back to \'{$a}\'';
$string['cancelmoving'] = 'Cancel moving \'{$a}\'';
$string['confirmdelete'] = '"その項目を削除しますか？
すべての活動内容と詳細項目も削除されます。"';
$string['currentsection'] = '今週';
$string['deletesection'] = '週削除';
$string['displaycontent'] = '内容の表示';
$string['editsectionname'] = '項目名の変更';
$string['hidefromothers'] = 'テーマを隠す';
$string['mergeup'] = '上位と統合';
$string['newsectionname'] = 'New name for section {$a}';
$string['page-course-view-oklass_mooc'] = 'Any course main page in oklass_mooc format';
$string['page-course-view-oklass_mooc-x'] = 'Any course page in oklass_mooc format';
$string['pluginname'] = 'Media Format';
$string['removemarker'] = '現在進行中で表示しない';
$string['section0name'] = '講義情報';
$string['sectionname'] = 'Topic';
$string['setmarker'] = '現在進行中で表示';
$string['showcollapsed'] = '折りたたんだ状態で表示';
$string['showexpanded'] = '広げた状態で表示';
$string['showfromothers'] = 'トピックの表示';
$string['trainingperiod'] = 'Training period';
$string['evaluationstandard'] = 'Evaluation standard';
$string['lecturenotice'] = 'Lecture notice';
$string['refresh'] = 'refresh';
$string['learn'] = 'Learn';
$string['lastlearn'] = '최근 학습창으로 이동';

$string['complete']='完了';
$string['incomplete']='未完了';
$string['learningtime']='学習時間';
$string['allprogress']='Total progress';
$string['progress']='進度率';
$string['startdate']='開始日';