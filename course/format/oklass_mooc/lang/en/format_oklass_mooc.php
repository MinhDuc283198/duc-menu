<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English language file for oklass_mooc format
 *
 * @package    format_oklass_mooc
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addsection'] = 'Add section';
$string['addsubsection'] = 'Add subsection';
$string['backtocourse'] = 'Back to course \'{$a}\'';
$string['backtosection'] = 'Back to \'{$a}\'';
$string['cancelmoving'] = 'Cancel moving \'{$a}\'';
$string['confirmdelete'] = 'Are you sure you want to delete this section? All activities and subsections will also be deleted.';
$string['currentsection'] = 'This section';
$string['deletesection'] = 'Delete section';
$string['displaycontent'] = 'Display content';
$string['editsectionname'] = 'Edit section name';
$string['hidefromothers'] = 'Hide topic';
$string['mergeup'] = 'Merge with parent';
$string['newsectionname'] = 'New name for section {$a}';
$string['page-course-view-oklass_mooc'] = 'Any course main page in oklass_mooc format';
$string['page-course-view-oklass_mooc-x'] = 'Any course page in oklass_mooc format';
$string['pluginname'] = 'Media Format';
$string['removemarker'] = 'Do not mark as current';
$string['section0name'] = 'Course Information';
$string['sectionname'] = 'Topic';
$string['setmarker'] = 'Mark as current';
$string['showcollapsed'] = 'Show collapsed';
$string['showexpanded'] = 'Show expanded';
$string['showfromothers'] = 'Show topic';
$string['trainingperiod'] = 'Training period';
$string['evaluationstandard'] = 'Evaluation standard';
$string['lecturenotice'] = 'Lecture notice';
$string['refresh'] = 'refresh';
$string['learn'] = 'Learn';
$string['lastlearn'] = '최근 학습창으로 이동';

$string['complete']='complete';
$string['incomplete']='Incomplete';
$string['learningtime']='Learning time';
$string['allprogress']='Total progress';
$string['progress']='Progress';
$string['startdate']='Start date';

$string['notevaluationstandard']='There is no evaluation criteria.';
$string['notlearningdate']='The Learning period is not set.';
$string['minprogress']='Minimum progress rate over {$a}%';
$string['mingrade']='Grade {$a} points or more';
$string['totalgrade']='Total Grade {$a} Point = ';
$string['grade:progress']='Progress {$a}%';
$string['grade:quiz']='Exam {$a}%';
$string['grade:forum']='Discussion Participation {$a}%';
$string['grade:assign']='Assignment {$a}%';