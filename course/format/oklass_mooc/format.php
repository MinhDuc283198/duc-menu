<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package    format_oklass_mooc
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/course/format/oklass_mooc/lib.php');

$context = context_course::instance($course->id);

$sql = 'SELECT lco.*, lc.learningstart, lc.learningend
        FROM {lmsdata_class} lc
        JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
        WHERE lc.courseid = :courseid';
$courseinfo = $DB->get_record_sql($sql, array('courseid'=>$course->id));

$sql = 'SELECT SUM(grademax) as totalgrade FROM {grade_items} WHERE courseid = :courseid AND itemmodule = :itemmodule';
$quiz_grade = $DB->get_record_sql($sql, array('courseid'=>$course->id, 'itemmodule'=>'quiz'));

$sql = 'SELECT SUM(grademax) as totalgrade FROM {grade_items} WHERE courseid = :courseid AND itemmodule = :itemmodule';
$assign_grade = $DB->get_record_sql($sql, array('courseid'=>$course->id, 'itemmodule'=>'assign'));

$sql = 'SELECT SUM(scale) as totalgrade FROM {forum} WHERE course = :course';
$forum_grade = $DB->get_record_sql($sql, array('course'=>$course->id));

$total_grade = $DB->get_record('grade_items', array('courseid'=>$course->id, 'itemtype'=>'course'));

$min_progress = $DB->get_record('lcmsprogress',array('course'=>$course->id));
$min_grade = $DB->get_record('course_completion_criteria',array('course'=>$course->id, 'criteriatype'=>6));

if (($marker >= 0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// make sure section 0 is created
course_create_sections_if_missing($course, 0);

$renderer = $PAGE->get_renderer('format_oklass_mooc');
if (($deletesection = optional_param('deletesection', 0, PARAM_INT)) && confirm_sesskey()) {
    $renderer->confirm_delete_section($course, $displaysection, $deletesection);
} else if ($PAGE->user_is_editing()) {
    $renderer->display_section($course, $displaysection, $displaysection);
} else {

    $modinfo = get_fast_modinfo($course);

    $text = '';

    $text .= html_writer::start_tag("div", array("class" => "course-content"));
    $text .= html_writer::start_tag("ul", array("class" => "media"));
    
    /****** 강의 정보 *****/
    $evaluation_standard = '';
    
    //최소 진도율이 있을 때
    if($min_progress->completionprogress > 0){
        $evaluation_standard .= get_string('minprogress', 'format_oklass_mooc',$min_progress->completionprogress);
    }
    
    //최소 이수 점수가 있을 때
    if($min_grade->gradepass > 0){
        if($min_progress->completionprogress > 0) $evaluation_standard .= ' & ';
        $evaluation_standard .= get_string('mingrade', 'format_oklass_mooc',(int)$min_grade->gradepass);
    }
    
    if($min_grade->gradepass > 0 && $total_grade->grademax > 0){
        $evaluation_standard .= '(';
        $evaluation_standard .= get_string('totalgrade', 'format_oklass_mooc',(int)$total_grade->grademax);
        
        $grade_element = array();
        if($min_progress->grade > 0){
            $grade_element[] = get_string('grade:progress', 'format_oklass_mooc',(int)$min_progress->grade);
        }
        if($quiz_grade->totalgrade > 0){
            $grade_element[] = get_string('grade:quiz', 'format_oklass_mooc',(int)$quiz_grade->totalgrade);
        }
        if($forum_grade->totalgrade > 0){
            $grade_element[] = get_string('grade:forum', 'format_oklass_mooc',(int)$forum_grade->totalgrade);
        }
        if($assign_grade->totalgrade > 0){
            $grade_element[] = get_string('grade:assign', 'format_oklass_mooc',(int)$assign_grade->totalgrade);
        }
        
        $evaluation_standard .= implode(', ', $grade_element);
        $evaluation_standard .= ')';
    }
    
    if(empty($evaluation_standard)){
        $evaluation_standard = get_string('notevaluationstandard', 'format_oklass_mooc');
    }

    $learningperiod = '';
    if(isset($courseinfo->learningstart) && isset($courseinfo->learningend)){
        $learningperiod = date('Y-m-d', $courseinfo->learningstart)." ~ ".date('Y-m-d', $courseinfo->learningend);
    } else {
        $learningperiod = get_string('notlearningdate', 'format_oklass_mooc');
    }

    $text .= html_writer::start_tag("li", array("class" => "section main span5"));
    $text .= html_writer::tag("h3", get_string('section0name', 'format_oklass_mooc'),array("class" => "sectionname"));
    $text .= html_writer::start_tag("div", array("class" => "course-info"));
    $text .= html_writer::tag("p", get_string('trainingperiod', 'format_oklass_mooc'),array("class" => "title"));
    $text .= html_writer::tag("p", $learningperiod, array("class" => "blue text"));
    $text .= html_writer::tag("p", get_string('evaluationstandard', 'format_oklass_mooc'),array("class" => "title"));
    $text .= html_writer::tag("p", "$evaluation_standard",array("class" => "text"));
    $text .= html_writer::end_tag("div");
    $text .= html_writer::end_tag("li");
    
    /****** 강의 알람상황 *****/
    $text .= html_writer::start_tag("li", array("class" => "section main span7"));
    $text .= html_writer::tag("h3", get_string('lecturenotice', 'format_oklass_mooc'),array("class" => "sectionname"));
    $text .= html_writer::start_tag("ul", array("class" => "section img-text"));
    
    $notices = format_oklass_mooc_course_notices($course);
    if(!empty($notices)){
        $text .= $notices;
    } else {
        $text .= '강의알림사항이 없습니다.';
        $text .= '<br>';
        $text .= '(7일이내 제출해야 할 과제, 퀴즈, 토론 활동이 표시됩니다.)';
    }
    
//    $text .= html_writer::start_tag("li", array("class" => "activity"));
//    $text .= html_writer::start_tag("div", array("class" => "activityinstance"));
//    $text .= html_writer::start_tag("a", array("href" => "#"));
//    $text .= html_writer::tag("img", "" ,array("src" => "/theme/image.php/oklassedu/jinotechboard/1498177032/icon", "class" => "iconlarge activityicon"));
//    $text .= html_writer::tag("span", "강의 공지사항" ,array("class" => "instancename"));
//    $text .= html_writer::tag("span", "(마지막 접속 후 게시물 수 : 1)" ,array("class" => "explain gray"));
//    $text .= html_writer::end_tag("a");
//    $text .= html_writer::end_tag("div");
//    $text .= html_writer::end_tag("li");
    
    
    $text .= html_writer::end_tag("ul");
    $text .= html_writer::end_tag("li");
    
    
    /** **진도율**** */

//    $sql = " SELECT l.id, l.progress, cm.id as cmid, cm.section FROM {okmedia} l JOIN {course_modules} cm ON cm.instance = l.id JOIN {modules} mo ON mo.id = cm.module "
//            . "where l.course = :courseid and mo.name = 'okmedia' and cm.visible = 1 and mo.visible = 1";
//    $lcmses = $DB->get_records_sql($sql, array('courseid' => $course->id));
//    $myprogress = 0;
//    $myplaytime = 0;
//    $mycount = 0;
//    $playtime = '00:00';
//    $secprogress = array();
//
//    if (!empty($lcmses)) {
//        foreach ($lcmses as $lcms) {
//
//            //부모 section id 값 가져오기
//            $query = "select s.id from {course_sections} s 
//            join {course_format_options} fo 
//            on fo.value = s.section and fo.courseid = s.course  
//            where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and fo.sectionid = :sectionid";
//            $param = array('courseid' => $course->id, 'sectionid' => $lcms->section);
//            $parentid = $DB->get_field_sql($query, $param);
//
//            if (!$secprogress[$parentid]['count']) {
//                $secprogress[$parentid]['total'] = 0;
//                $secprogress[$parentid]['count'] = 0;
//            }
//
//            if (!$secprogress[$lcms->section]) {
//                $secprogress[$lcms->section]['count'] = 0;
//                $secprogress[$lcms->section]['total'] = 0;
//            }
//
//            //진도율 가져오기
//            if($lcms->progress == 1){
//            $track = $DB->get_record_select('okmedia_track', 'okmediaid=:mediaid and userid=:userid', array('mediaid' => $lcms->cmid, 'userid' => $USER->id));
//                if ($track && $track->progress && $track->progress >= 0) {
//                    $myprogress += $track->progress;
//                    $myplaytime += $track->playtime;
//                    if ($track->timeview > $timeview)
//                        $timeview = $track->timeview;
//                    $secprogress[$parentid]['total'] += $track->progress;
//                    $secprogress[$lcms->section]['total'] = $track->progress;
//                }
//                $secprogress[$parentid]['count'] ++;
//                $secprogress[$lcms->section]['count'] ++;
//                $mycount++;
//            }
//        }
//    }
//    if ($mycount == 0)
//        $mycount = 1;
////    $myaverage = round($myprogress / $mycount);
//    //소수점2째자리에서 버림
//    $myaverage = floor(($myprogress/$mycount) * 100) / 100;
//
//    $time_h = sprintf('%02d', floor($myplaytime / 3600));
//    $time_m = sprintf('%02d', floor(($myplaytime % 3600) / 60));
//    $time_s = sprintf('%02d', $myplaytime - ($time_h * 3600) - ($time_m * 60));
//
//    $playtime = $time_h . ':' . $time_m . ':' . $time_s;
//    
//    //가장 최근 학습 중이던 섹션을 찾아서 이동
//    $sql = 'select cm.id,cm.course,cm.section from {course_modules} cm join {okmedia_track} ot on ot.okmediaid = cm.id where cm.course = :courseid and ot.userid = :userid '
//            . 'order by ot.timeview desc limit 1';
//    $lastsection = $DB->get_record_sql($sql,array('courseid'=>$course->id,'userid'=>$USER->id));
//    if($lastsection){
//        $laststudyurl = format_oklass_mooc_shared_media_url($course->id, $lastsection->section, 'player');
//    }
//
//    $text .= html_writer::start_tag("li", array("class" => "section main clear progress-info"));
//    $text .= html_writer::start_tag("div", array("class" => "top"));
//    $text .= html_writer::start_tag("div", array("class" => "l_content"));
//    $text .= html_writer::tag("strong", get_string('startdate', 'format_oklass_mooc'));
//    $text .= html_writer::tag("span", date('Y-m-d', $course->startdate));
//    $text .= "<br />";
//    $text .= html_writer::tag("strong", get_string('learningtime', 'format_oklass_mooc'));
//    $text .= html_writer::tag("span", $playtime, array("class" => "time"));
//    $text .= html_writer::end_tag("div");
//    if($laststudyurl){
//        $text .= html_writer::start_tag("div", array("class" => "btn orange"));
//        $text .= html_writer::tag('a', get_string('lastlearn', 'format_oklass_mooc'), array('href' => $laststudyurl));
//        $text .= html_writer::end_tag("div");
//    }
//    $text .= html_writer::start_tag("div", array("class" => "r_content"));
//    $text .= html_writer::tag("span", get_string('refresh', 'format_oklass_mooc'), array("class" => "refresh"));
//    $text .= html_writer::end_tag("div");
//    $text .= html_writer::start_tag("div", array("class" => "bottom"));
//    $text .= html_writer::tag("div", html_writer::tag("p", "", array("class" => "bar")), array("class" => "bars"));
//    $text .= html_writer::tag("strong", get_string('progress', 'format_oklass_mooc') . $myaverage . "%", array("class" => "percent"));
//    $text .= html_writer::end_tag("div");
//    $text .= html_writer::end_tag("div");
//    
//
//    $text .= html_writer::end_tag("li");
    /*     * **진도율**** */
    
    /***activity 현황***/
    $mod_sql_from = 'SELECT 
                        cm.id, cm.section, 
                        mo.name,
                        cfo.value,
                        CASE WHEN cmc.completionstate IS NULL THEN 0 ELSE cmc.completionstate END AS iscompletion
                    FROM {course_modules} cm
                    JOIN {course_format_options} cfo ON cfo.sectionid = cm.section AND cfo.name = :parent
                    JOIN {modules} mo ON mo.id = cm.module
                    LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid ';
    $check_mods = array('okmedia', 'vpl', 'assign', 'hvp');
    list($mod_sql_in, $mod_params) = $DB->get_in_or_equal($check_mods, SQL_PARAMS_NAMED, 'md');
    $mod_params['parent'] = 'parent';
    $mod_params['userid'] = $USER->id;
    $mod_params['courseid'] = $course->id;
    
    $mod_sql_where[] = ' cm.course = :courseid';
    $mod_sql_where[] = ' mo.name '.$mod_sql_in;
    $mod_sql_where =  ' WHERE '.implode(' AND ',$mod_sql_where);
    
    $completion_mods = $DB->get_records_sql($mod_sql_from.$mod_sql_where, $mod_params);
    $completion_data = format_oklass_mooc_complete_mod($completion_mods);


    /*     * **리스트**** */
    $text .= html_writer::start_tag("li", array("class" => "section main clear"));
    
    $text .= html_writer::start_tag('div', array('class' => 'study-title-area'));
    $text .= html_writer::tag("h3", get_string('learn', 'format_oklass_mooc') ,array("class" => "sectionname"));
    $text .= html_writer::start_tag("div", array("class" => "activity-data"));
    $text .= format_oklass_mooc_complete_print($completion_data, true);
    $text .= html_writer::end_tag("div");
    $text .= html_writer::end_tag("div");
    
    $text .= html_writer::start_tag("div", array("class" => "content nopadding"));
    $text .= html_writer::start_tag("ul", array("class" => "media_list"));

    foreach ($modinfo->get_section_info_all() as $section => $thissection) {
        if ($section == 0) {
            continue;
        }

        $query = "select count(*) from {course_format_options} where format='oklass_mooc' and name = 'parent' and value != 0 and courseid = :courseid and sectionid = :sectionid";
        $param = array('courseid' => $course->id, 'sectionid' => $thissection->id);
        $optioncount = $DB->count_records_sql($query, $param);
        if ($optioncount == 0) {
//            print_object($thissection->section);
            $query = "select count(*) from {course_format_options} fo 
            join {course_sections} s on s.id = fo.sectionid 
            where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and fo.value = :section";
            $param = array('courseid' => $course->id, 'section' => $section);
            $sectioncount = $DB->count_records_sql($query, $param);
            $sectionclass = ($sectioncount > 0) ? 'listname on' : ' header';
            $secporgresscount = $secprogress[$thissection->id]['count'];
            if ($secporgresscount == 0) {
                $secporgresscount = 1;
            }
//            $mysecprogress = round($secprogress[$thissection->id]['total'] / $secporgresscount);
            //소수점 2째자리에서 버림
            $mysecprogress = floor(($secprogress[$thissection->id]['total']/$secporgresscount) * 100) / 100;

            $text .= html_writer::start_tag("li", array("class" => $sectionclass));
            $text .= html_writer::start_tag("div", array("class" => "listname-title"));
            $text .= get_section_name($course, $section);
            $text .= html_writer::end_tag("div");
            $section_completion_data = format_oklass_mooc_complete_mod($completion_mods, $thissection->section);
            $text .= format_oklass_mooc_complete_print($section_completion_data, true, true);
            if ($secprogress[$thissection->id]['count'] > 0) {
                $text .= html_writer::start_tag("strong", array("class" => "r_content perprogress"));
                $text .= get_string('progress', 'format_oklass_mooc') . "&nbsp;";
                $text .= html_writer::tag("span", $mysecprogress . '%', array("class" => "orange"));
                $text .= html_writer::end_tag("strong");
            }
            $text .= html_writer::end_tag("li");
        } else {
            //소수점 2째자리에서 버림
            $secprogress[$thissection->id]['total'] = floor(($secprogress[$thissection->id]['total']) * 100) / 100;
            $share_mediaurl = format_oklass_mooc_shared_media_url($course->id, $thissection->id, 'player');
            $sectionname = html_writer::tag('a', get_section_name($course, $section), array('href' => $share_mediaurl));
            if (!$secprogress[$thissection->id]['total']) {
                $secprogress[$thissection->id]['total'] = 0;
            }
            if ($secprogress[$thissection->id]['total'] < 100) {
                $seccompleted = get_string('incomplete', 'format_oklass_mooc') . '(' . $secprogress[$thissection->id]['total'] . '%)';
                $compclass = 'incomplete';
            } else {
                $seccompleted = get_string('complete', 'format_oklass_mooc') . '(' . $secprogress[$thissection->id]['total'] . '%)';
                $compclass = '';
            }
            $text .= html_writer::start_tag("li", array("class" => "listitem"));
            if ($secprogress[$thissection->id]['count'] > 0) {
                $text .= html_writer::tag("span", $seccompleted, array("class" => "r_content ".$compclass));
            }
            $text .= $sectionname;
            $section_completion_data = format_oklass_mooc_complete_mod($completion_mods, $thissection->id, true);
            $text .= format_oklass_mooc_complete_print1($section_completion_data);
            $text .= html_writer::end_tag("li");
        }
    }
    $text .= html_writer::end_tag("ul");

    /*     * **리스트**** */

    $text .= html_writer::end_tag("li");
    $text .= html_writer::end_tag("ul");

    $text .= html_writer::end_tag("div");

    echo $text;


    echo '<script type="text/javascript">
        $(document).ready(function(){
            var per = $(".progressbar .txper").text();
            $(".progressbar .barchart .per").width(per);
            //새로고침
            $(".right_cont .reload").click(function(){
                    window.location.reload()
            });';
    echo ' }); 
         </script>';
} // is not editing end
// Include course format js module
$PAGE->requires->js('/course/format/oklass_mooc/format.js');
$PAGE->requires->js('/course/format/oklass_mooc/oklass_mooc.js');
$PAGE->requires->string_for_js('confirmdelete', 'format_oklass_mooc');
$PAGE->requires->js_init_call('M.course.format.init_oklass_mooc');



