<?php

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once $CFG->dirroot.'/login/lib.php';
require_once $CFG->dirroot.'/local/lmsdata/lib.php';

//userid로 자동 로그인 처리 필요...

$v = required_param('v', PARAM_RAW);
$decrypted = base64_decode($v);
list($id,$sectionid, $userid, $mode) = explode('||', $decrypted);

if(!$userid){
    require_login();
    die();
}

$username = $DB->get_field('user','username',array('id'=>$userid));

if ($user) {

/// Let's get them all set up.
    complete_user_login($user);

    //user 언어 체크
    $sql = "select c.code, c.langcode from {lmsdata_corporation} c 
            join {lmsdata_user} u on u.corporationcode = c.code 
            where u.userid = :userid";
    if(!$user->lang){
        if($userinfo = $DB->get_record_sql($sql,array('userid'=>$user->id))){
            $langcode = strtolower($userinfo->langcode);
            if($langcode == 'zh') {
                $langcode = 'zh_cn';
            }
            $user->lang = $langcode;
        }
    }
}

if($mode == 'player'){
    redirect($CFG->wwwroot.'/mod/okmedia/viewer/mooc.php?v='.$v);
}



