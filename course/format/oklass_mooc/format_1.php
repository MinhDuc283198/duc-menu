<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package    format_oklass_mooc
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/course/format/oklass_mooc/lib.php');

$context = context_course::instance($course->id);

if (($marker >= 0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// make sure section 0 is created
course_create_sections_if_missing($course, 0);

$renderer = $PAGE->get_renderer('format_oklass_mooc');
if (($deletesection = optional_param('deletesection', 0, PARAM_INT)) && confirm_sesskey()) {
    $renderer->confirm_delete_section($course, $displaysection, $deletesection);
} else if ($PAGE->user_is_editing()) {
    $renderer->display_section($course, $displaysection, $displaysection);
} else {
    $sectionkey = optional_param('sec', 0, PARAM_INT);
    $keyid = optional_param('keyid', 0, PARAM_INT);

    $query = "select cs.section from {course_format_options} fo "
            . "join {course_sections} cs on cs.id = fo.sectionid "
            . "where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid order by cs.section asc";
    $param = array('courseid' => $course->id);
    $children = $DB->get_records_sql($query, $param);

    $sec_keys = array_keys($children);
    $sectionnum = $children[$sec_keys[$sectionkey]]->section;
    if ($sectionnum) {
        $sequence = array_filter(explode(',', $DB->get_field('course_sections', 'sequence', array('course' => $course->id, 'section' => $sectionnum))));
        $course = $DB->get_record('course', array('id' => $course->id), '*', MUST_EXIST);

        $renderer->display_first_section($course);

        $section_count = $DB->count_records('course_sections', array('course' => $course->id));

        list($videos, $count) = format_oklass_mooc_video($course->id, $sectionnum);

        if ($count > 0) {

            foreach ($sequence as $key => $id) {
                if (isset($videos[$id])) {
                    $video_sequence[$id] = $id;
                    unset($sequence[$key]);
                }
            }
            $keys = array_keys($video_sequence);
            $returnurl = course_get_url($course);

            $current_video = $keys[$keyid];

            $prev_key = $keyid - 1;
            $prev_section = $sectionkey - 1;
            $next_key = $keyid + 1;
            $next_section = $sectionkey + 1;

            $cm = get_coursemodule_from_id('okmedia', $current_video);
            if (!$okmedia = $DB->get_record('okmedia', array('id' => $cm->instance))) {
                print_error('course module is incorrect');
            }


            echo html_writer::tag('h2', $okmedia->name, array('class' => 'page_title'));



            echo html_writer::start_tag('div', array('style' => 'clear:both;', "class" => "mooc_video"));  // video start


            $contents = $DB->get_record('lmsmedia_contents', array('id' => $okmedia->contents));
            $lcms_context = context_module::instance($cm->id);

            add_to_log($course->id, "okmedia", "view", "viewer/embed.php?id=$cm->id", "$okmedia->id", $cm->id);

            $trackcount = $DB->count_records('okmedia_track', array('okmediaid' => $cm->id, 'userid' => $USER->id));
            $track = $DB->get_record('okmedia_track', array('okmediaid' => $cm->id, 'userid' => $USER->id));

            $last = get_string('firststudy', 'okmedia');
            //학습진도율
            $rate = '0';
            $totaltime = '00:00:00';
            if ($trackcount > 0) {
                //최종학습일
                $last = date('Y-m-d H:i:s', $track->timeview);
                $rate = $track->progress;
                //총학습시간
                $total = okmedia_time_from_seconds($track->playtime);
                $totaltime = $total->h . ':' . $total->m . ':' . $total->s;
            }

            echo html_writer::start_tag('div', array('class' => 'video_info_group'));
            echo html_writer::tag('span', get_string('laststudy', 'lcms', $last), array('class' => 'detail-laststudy'));
            echo html_writer::tag('span', get_string('studyrate', 'lcms', $rate), array('class' => 'detail-studyrate'));
            echo html_writer::tag('span', get_string('totalstudytime', 'lcms', $totaltime), array('class' => 'detail-totalstudytime'));
            echo html_writer::end_tag("div");


            echo html_writer::start_tag('div', array('class' => 'buttons'));  // button group
            /* Print Next Button */
            if (isset($keys[$prev_key])) {
                echo html_writer::tag('button', 'Prev Video', array('onclick' => "location.href='$returnurl&sec=$sectionnum&keyid=$prev_key'"));
            }
            if (isset($keys[$next_key])) {
                echo html_writer::tag('button', 'Next Video', array('onclick' => "location.href='$returnurl&sec=$sectionnum&keyid=$next_key'"));
            }
            //echo "<br>";

            /* Print Next Button */
            if (isset($sec_keys[$sectionkey + 1])) {
                echo html_writer::tag('button', 'Next Section >', array('class' => 'right', 'onclick' => "location.href='$returnurl&sec=$next_section'"));
            } else {
                //echo html_writer::tag('button', 'Last Section' ,array('class'=>'right'));
            }

            if (isset($sec_keys[$sectionkey - 1])) {
                echo html_writer::tag('button', ' < Prev Section', array('class' => 'left', 'onclick' => "location.href='$returnurl&sec=$prev_section'"));
            } else {
                //echo html_writer::tag('button', 'First Section',array('class'=>'right'));
            }
            echo html_writer::end_tag("div");

            /* Print Next Button */
            if (!$current_video) {
                echo html_writer::tag('div', 'Empty Video', array('class' => 'empty_video'));
            } else {




                echo html_writer::start_tag('div', array('class' => 'detail-progress'));
                echo html_writer::start_tag('div', array('class' => 'detail-contents-area'));


                $viewer_url = $CFG->wwwroot . '/mod/okmedia/viewer/embed.php?id=' . $cm->id;

                $attfile .= '<iframe id="vod_viewer" title="vod" src="' . $viewer_url . '"></iframe>';
                echo html_writer::tag('div', $attfile, array('class' => ""));

                echo html_writer::end_tag('div');
            }

            echo html_writer::end_tag('div');
        }

        $mod_key = array_keys(array_flip($sequence));
        $mod_count = count($mod_key);
        if ($mod_count > 0) {
            $module_url = array();
            $module_name = array();
            $module_mname = array();
            $modulelist = '';
            $i = 0;
            foreach ($mod_key as $key => $mid) {
                $mod_cm = $DB->get_record('course_modules', array('id' => $mid));
                $module = $DB->get_record('modules', array('id' => $mod_cm->module));
                $module_name[$i] = $DB->get_field($module->name, 'name', array('id' => $mod_cm->instance));
                $module_mname[$i] = $module->name;
                $module_url[$i] = $CFG->wwwroot . '/mod/' . $module->name . '/view.php?iframe=1&id=' . $mid;
                $modulelist .= '<span class="circle" onclick="modluenavi(\'' . $module_name[$i] . '\',\'' . $module_mname[$i] . '\',\'' . $module_url[$i] . '\',' . $i . ');">'
                        . '<img src="/theme/oklasscompany/pix_plugins/mod/' . $module->name . '/icon.png" alt="icon01" title="icon01"/></span>';
                $i++;
            }
            $module_iframe = '<iframe id="module" name="module_iframe" style="width:100%; height:500px;" title="module" src="' . $module_url[0] . '"></iframe>';

            //video icon
            echo html_writer::start_tag('div', array('class' => 'icons', 'id' => 'modulen_navidiv'));
            echo '<div class="left">&nbsp;</div>';
            echo '<div class="center">' . $modulelist . '</div>';
            if ($module_url[1]) {
                echo '<div class="right"><div onclick="modluenavi(\'' . $module_name[1] . '\',\'' . $module_mname[1] . '\',\'' . $module_url[1] . '\',1);">
            <span class="">' . $module_name[1] . '</span>
            <span class="circle"><img src="/theme/oklasscompany/pix_plugins/mod/' . $module_mname[1] . '/icon.png" alt="icon01" title="icon01"/></span>
            <span class="arrow_right">&gt;</span>
            </div>
        ';
            } else {
                echo '<div class="right">&nbsp;</div>';
            }

            echo html_writer::end_tag("div");
            //video icon
            echo html_writer::end_tag('div'); // video end

            echo html_writer::tag('div', $module_iframe, array('class' => "module_iframe"));
            ?>
            <style>
                iframe#module {
                    background-image:url('<?php echo $CFG->wwwroot; ?>/course/format/oklass_mooc/pix/default.gif');
                    background-size: 150px;
                    background-repeat:no-repeat;
                    background-position:center
                }
            </style>
            <script language='javascript'>
                var srcs = <?php echo json_encode($module_url); ?>;
                var names = <?php echo json_encode($module_name); ?>;
                var mnames = <?php echo json_encode($module_mname); ?>;
                var max = <?php echo $mod_count - 1; ?>;
                var int = 0;
                function modluenavi(name, mname, src, num) {
                    $('iframe#module').attr('src', src);
                    if (num > 0) {
                        int = num - 1;
                        $('#modulen_navidiv .left').html('<div  onclick="modluenavi(\'' + names[int] + '\',\'' + mnames[int] + '\',\'' + srcs[int] + '\',' + int + ')"><span class="arrow_left">&lt;</span><span class="circle"><img src="/theme/oklasscompany/pix_plugins/mod/' + mnames[int] + '/icon.png" alt="icon01" title="icon01" /></span><span class="">' + names[int] + '</span></div>');
                    } else {
                        $('#modulen_navidiv .left').html('<div>&nbsp;</div>');
                    }
                    if (max > num) {
                        int = num + 1;
                        $('#modulen_navidiv .right').html('<div onclick="modluenavi(\'' + names[int] + '\',\'' + mnames[int] + '\',\'' + srcs[int] + '\',' + int + ')"><span class="">' + names[int] + '</span><span class="circle"><img src="/theme/oklasscompany/pix_plugins/mod/' + mnames[int] + '/icon.png" alt="icon01" title="icon01" /></span><span class="arrow_left">&gt;</span>');
                    } else {
                        $('#modulen_navidiv .right').html('<div>&nbsp;</div>');
                    }
                }
            </script>
            <?php
        }
    } else {
        'No Section';
    }
} // is not editing end
// Include course format js module
$PAGE->requires->js('/course/format/oklass_mooc/format.js');
$PAGE->requires->string_for_js('confirmdelete', 'format_oklass_mooc');
$PAGE->requires->js_init_call('M.course.format.init_oklass_mooc');
