<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/local/media/lib.php');
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/local/media/playerlib.php');

$v = required_param('v', PARAM_RAW);
$decrypted = base64_decode($v);
list($id, $sectionid, $userid, $mode) = explode('||', $decrypted);

if (!$course = $DB->get_record('course', array('id' => $id))) {
    print_error('course is misconfigured');
}

$context = context_course::instance($course->id, MUST_EXIST);
require_login($course);
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();

//section 가져오기
$modinfo = get_fast_modinfo($course);

//section에 등록된 동영상 가져오기 (한 섹션에 동영상은 하나만 등록 가능)
$sql = " SELECT l.*, cm.id as cmid FROM {okmedia} l JOIN {course_modules} cm ON cm.instance = l.id "
        . "JOIN {modules} mo ON mo.id = cm.module  where mo.name = 'okmedia' and cm.visible = 1 and mo.visible = 1 and cm.section = :sectionid";
$okmedia = $DB->get_record_sql($sql,array('sectionid'=>$sectionid),0,1);

if($okmedia){
    //사내 사외 여부 체크함.
    local_media_inout_check($okmedia->contents);
}
  
//현재 섹션명
$currentsection = $DB->get_record('course_sections',array('id'=>$sectionid));
$currenttitle = get_section_name($course, $currentsection);

//현재 섹션의 학습활동 정보 가져오기
$sequence = explode(',',$currentsection->sequence);
$modules = array();
$modcount = 0;
foreach($sequence as $seq){
    $mod_cm = $DB->get_record('course_modules', array('id' => $seq));
    $module = $DB->get_record('modules', array('id' => $mod_cm->module));
    if($mod_cm && $module && $module->name != 'okmedia'){
        $modules[$seq] = array();
        $modules[$seq]['name'] = $DB->get_field($module->name, 'name', array('id' => $mod_cm->instance));
        $modules[$seq]['url'] = $CFG->wwwroot . '/mod/' . $module->name . '/view.php?iframe=1&id=' . $mod_cm->id;
        $modules[$seq]['urlencode'] = base64_encode($modules[$seq]['url']);
        
        if($modcount == 0){
            $firstmodkey = $seq;
            $firstmodname = $modules[$seq]['name'];
            $firstmodurl = $modules[$seq]['url'];
        }
        $modcount++;
    }
}

//부모 section id 값 가져오기
$query = "select s.id from {course_sections} s 
join {course_format_options} fo 
on fo.value = s.section and fo.courseid = s.course  
where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and fo.sectionid = :sectionid";
$param = array('courseid'=>$course->id,'sectionid'=>$sectionid);
$parentid = $DB->get_field_sql($query, $param);

//이전 section과 다음 section 가져오기
$query = "SELECT s.section, s.id FROM {course_sections} s 
join {course_format_options} fo 
on fo.sectionid = s.id and fo.courseid = s.course 
WHERE fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and section < :section 
order by s.section desc limit 1";
$param = array('courseid'=>$course->id, 'section'=>$currentsection->section);
if($prevsection = $DB->get_record_sql($query, $param)){
    if($prevsection->id){
        $prevlink = format_oklass_mooc_shared_media_url_link($course->id, $prevsection->id, 'player');
    }
}

$query = "SELECT s.section, s.id FROM {course_sections} s 
join {course_format_options} fo 
on fo.sectionid = s.id and fo.courseid = s.course 
WHERE fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and section > :section 
order by s.section asc limit 1";
$param = array('courseid'=>$course->id, 'section'=>$currentsection->section);
if($nextsection = $DB->get_record_sql($query, $param)){
    if($nextsection->id){
        $nextlink = format_oklass_mooc_shared_media_url_link($course->id, $nextsection->id, 'player');
    }
}


$html = "";
$html .= html_writer::start_tag("div" ,array("id"=>"pop_wrap" ,"class"=>"autoheight mooc"));
/***title***/
$html .= html_writer::start_tag("div" ,array( "class"=>"pop_title"));
$html .= html_writer::start_tag("h2");
$html .= html_writer::tag("span" ,"" ,array("class" => "menu" ,"title"=>"menu"));
$html .= $currenttitle;
$html .= html_writer::end_tag("h2");
if($okmedia){
    $html .= html_writer::start_tag("p" ,array( "class"=>"popclose",'id'=>'popclose'));
    $html .= html_writer::tag("span",get_string('close_text','local_media') ,array("class" => "close_text"));
    $html .= html_writer::tag("img","" ,array( "alt"=>"close" ,"title"=>"close" ,"src"=>"/theme/oklasscompany/pix/images/close.png"));
    $html .= html_writer::end_tag("p");
}else{
    $html .= html_writer::start_tag("p" ,array( "class"=>"popclose",'id'=>'popclose2'));
    $html .= html_writer::tag("img","" ,array( "alt"=>"close" ,"title"=>"close" ,"src"=>"/theme/oklasscompany/pix/images/close.png"));
    $html .= html_writer::end_tag("p");
}
$html .= html_writer::end_tag("div");
/***title***/

/***contents***/
$html .= html_writer::start_tag("div" ,array("id"=>"contents" ,"class"=>"mooc"));

/**menu**/
$html .= html_writer::start_tag("div",array("class"=>"aside"));
$html .= html_writer::start_tag("ul",array("class"=>"lgb"));

foreach ($modinfo->get_section_info_all() as $section => $thissection) {
        if($section == 0){
            continue;
        }
        
        $query = "select count(*) from {course_format_options} where format='oklass_mooc' and name = 'parent' and value != 0 and courseid = :courseid and sectionid = :sectionid";
        $param = array('courseid'=>$course->id,'sectionid'=>$thissection->id);
        $optioncount = $DB->count_records_sql($query,$param);
        
        if($optioncount == 0){
            if($childcount > 0){
                $html .= html_writer::end_tag("ul");
                $html .= html_writer::end_tag("li");
            }
            $query = "select count(*) from {course_format_options} fo 
            join {course_sections} s on s.id = fo.sectionid 
            where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and fo.value = :section";
            $param = array('courseid'=>$course->id,'section'=>$section);
            $sectioncount = $DB->count_records_sql($query,$param);
            $sectionclass = ($sectioncount > 0)? '':' nochildsection';
            $parenton = ($parentid == $thissection->id)? 'on':'';
            $html .= html_writer::start_tag("li" ,array("class"=>$parenton.$sectionclass));
            $html .= html_writer::tag("h3" ,get_section_name($course, $section));
            $childcount = 0;
        }else{
            if($childcount == 0){
                $html .= html_writer::start_tag("ul");
            }
            $selectedon = ($sectionid == $thissection->id)? 'on':'';
            $share_mediaurl = format_oklass_mooc_shared_media_url_link($course->id, $thissection->id, 'player');
            $html .= html_writer::tag("li" ,html_writer::tag("a",get_section_name($course, $section) ,array("href"=>$share_mediaurl)) ,array("class"=>$selectedon));
            $childcount++;
        }
        
        
}

$html .= html_writer::end_tag("ul");
$html .= html_writer::end_tag("div");
/**menu**/

if($okmedia){
    $html .= html_writer::start_tag("div" ,array("class"=>"video_group mooc"));
    $viewer_url = $CFG->wwwroot . '/mod/okmedia/viewer/embed.php?id=' . $okmedia->cmid;
    $attfile = '<iframe id="vod_viewer" title="vod" src="' . $viewer_url . '"></iframe>';
    $html .= html_writer::tag('div', $attfile, array('class' => ""));
    $html .= html_writer::end_tag("div");
}

$html .= html_writer::start_tag('div', array('class' => 'buttons'));  // button group
if($prevlink){
    $html .= html_writer::tag('button', 'Prev', array('onclick' => 'location.href="'.$prevlink.'"', 'class'=>'prev'));
}
if($nextlink){
    $html .= html_writer::tag('button', 'Next', array('onclick' => 'location.href="'.$nextlink.'"', 'class'=>'next'));
}
$html .= html_writer::end_tag('div');

if($modules){
    /**tab header**/
    $html .= html_writer::start_tag("div" ,array("class"=>"tab_group full"));
    $html .= html_writer::start_tag("div" ,array("class"=>"tab"));

    foreach($modules as $key => $mod){
    /**모듈명 가져오기 **/
        $selectedon = ($firstmodkey == $key)? 'on':'';
        $html .= html_writer::start_tag('a',array('href'=>'javascript:goto_modurl('.$key.',"'.$mod['url'].'");'));
        $html .= html_writer::start_tag("div" ,array("class"=>"my my_course ".$selectedon,"id"=>"mod_tab_".$key));
        $html .= html_writer::tag("h3",$mod['name']);
        $html .= html_writer::end_tag("div");
        $html .= html_writer::end_tag("a");
    }

    $html .= html_writer::end_tag("div");
    $html .= html_writer::end_tag("div");
    /**tab header**/

    /**tab contents**/
    $html .= html_writer::start_tag("div" ,array("class"=>"con tab_con activity"));
    $html .= html_writer::start_tag("div" ,array("class"=>"course_con"));
    $attfile = '<iframe class="mod_viewer" id="mod_viewer_'.$firstmodkey.'" title="mod" src="' . $firstmodurl . '">';
    $attfile .= '</iframe>';
    $html .= html_writer::tag('div', $attfile, array('class' => ""));
    $html .= html_writer::end_tag("div");
    $html .= html_writer::end_tag("div");
    /**tab contents**/
}

$html .= html_writer::end_tag("div");
$html .= html_writer::end_tag("div");
/***contents***/
$html .= html_writer::end_tag("div");

$html .= html_writer::tag('div',html_writer::tag('div','<img src="loading.gif">',array('style'=>'position: absolute; top: 50%; left: 47%;')), 
        array('id' => 'loading', 'style' => 'position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1200; background-color:#757b8c;opacity:0.7;display:none;'));

echo $html;
echo $OUTPUT->footer();
 ?>
<script type="text/javascript">
    $(document).ready(function(){
        
        $('#popclose2').click(function(){
            self.close();
        });
        
        /**menu click event**/
        $("#pop_wrap .menu").click(function(){
            $("#contents.mooc .aside").addClass("show");

            if($("#popbg").length == 0){
                $("#pop_wrap").append("<div id='popbg'></div>")
                $("#popbg").fadeIn().css({top:"0px" ,left: "0px"}).click(function(){
                    $("#contents.mooc .aside").removeClass("show");
                    $(this).fadeOut();
                });  
                $("#popbg, #contents.mooc .aside").css("min-height",jQuery('body').prop("scrollHeight"));
            }else{
                $("#popbg").fadeIn().css({top:"0px" ,left: "0px"});
            }

        });
        /**menu click event**/
        
        /**lnb 2depth click event**/
        $(".aside > ul > li h3").click(function(){
            if(!$(this).parent().hasClass("on")){
                $(this).parent().addClass("on"); 
            }else{
                $(this).parent().removeClass("on"); 
            }
            $("#popbg").css("height",$(".aside").height());
        });
         /**lnb 2depth click event**/
         //iframe 로드될 때 세로 사이즈 변경
        $('.mod_viewer').load(function(){
           $(this).height($(this).contents().find('body').height() + 30);
           endloading();
        });
    });  
    
    function goto_modurl(key,url){
        startloading();
        //var gotourl = 'loading.php?url='+url;
        $('.mod_viewer').attr({'src':url,'id':'mod_viewer_'+key});
    }
    
    function startloading() {
        $('#loading').show();
    }
    
    function endloading() {
        $('#loading').hide();
    }
</script>


