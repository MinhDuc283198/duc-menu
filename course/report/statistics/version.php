<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015042001;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2014050800;        // Requires this Moodle version
$plugin->component = 'coursereport_statistics';   // Full name of the plugin (used for diagnostics)