<?php

require('../../../config.php');
require_once $CFG->dirroot . '/course/report/statistics/lib.php';
require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT); // course id
$excell = optional_param('excell', 0, PARAM_INT);

require_login();

$context = context_course::instance($id);
$PAGE->set_context($context);


$course = get_course($id);

$PAGE->set_url('/course/report/statistics/index.php');
$PAGE->set_course($course);
$PAGE->set_pagelayout('incourse');
$strplural = get_string("pluginname", "coursereport_statistics") . " - ".get_string('board_write_by_type','coursereport_statistics');

$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$sql = "select u.* from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "where ra.contextid = :contextid order by u.username asc";
$users = $DB->get_records_sql($sql, array('contextid' => $context->id, 'rolename' => 'student'));

$cmsql = "select cm.id, cm.instance , mod.name from {course_modules} cm "
        . "join {modules} mod on mod.id = cm.module and (mod.name = 'jinotechboard' or mod.name = 'teamboard' or mod.name = 'jinoforum') "
        . "where cm.course = :course";
$cms = $DB->get_records_sql($cmsql, array('course' => $id));
if (!$excell) {
    echo $OUTPUT->header();

//tab
    $row[] = new tabobject('board_read', "$CFG->wwwroot/course/report/statistics/index.php?id=" . $id, get_string('board_period_read','coursereport_statistics'));
    $row[] = new tabobject('board_write', "$CFG->wwwroot/course/report/statistics/write.php?id=" . $id, get_string('board_period_write','coursereport_statistics'));
    $row[] = new tabobject('type_board_read', "$CFG->wwwroot/course/report/statistics/board_read_type.php?id=" . $id, get_string('board_read_by_type','coursereport_statistics'));
    $row[] = new tabobject('type_board_write', "$CFG->wwwroot/course/report/statistics/board_write_type.php?id=" . $id, get_string('board_write_by_type','coursereport_statistics'));
    $row[] = new tabobject('board_comment', "$CFG->wwwroot/course/report/statistics/board_comment.php?id=" . $id, get_string('board_comment_read_by_type','coursereport_statistics'));
    $rows[] = $row;
    $currenttab = 'type_board_write';
    print_tabs($rows, $currenttab);

    
    echo '<div style="width:100%; overflow-y:hidden; overflow-x:scroll;white-space:nowrap;">'
    		. '<table id="statistics_table_jb">';
    echo "<tr><th width=10%>".get_string('user:name','coursereport_statistics')."</th><th width=10%>".get_string('user:hakbun','coursereport_statistics')."</th>";

    foreach ($cms as $cm) {
        $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
        $modulename = get_string('modulename', $cm->name);
        $activityicon = $OUTPUT->pix_icon('icon', $modulename, $cm->name, array('class' => 'icon'));
        echo '<th><a href="' . $CFG->wwwroot . '/mod/' . $cm->name . '/view.php?id=' . $cm->id . '">' . $activityicon . $mod->name . '</a></th>';
    }
    echo "</tr><tr>";
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        foreach ($users as $user) {
            echo "<td>" . fullname($user) . "</td>";
            echo "<td>" . $user->username . "</td>";
            foreach ($cms as $cm) {
                switch ($cm->name) {
                    case "jinotechboard" :
                    case "teamboard" :
                        $sql = "select count(*) "
                                . "from {" . $cm->name . "_contents} w "
                                . "join {" . $cm->name . "} b on b.id = :instance and b.id = w.board and b.course = :courseid "
                                . "where w.userid = :userid";
                        $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                        echo "<td>" . $DB->count_records_sql($sql, $param) . "</td>";
                        break;
                    case "jinoforum" :
                        $sql = "select count(*) "
                                . "from {jinoforum_posts} w "
                                . "join {jinoforum_discussions} d on d.id = w.discussion "
                                . "join {jinoforum} f on  f.id = :instance and f.id = d.jinoforum and f.course = :courseid "
                                . "where w.userid = :userid";
                        $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                        echo "<td>" . $DB->count_records_sql($sql, $param) . "</td>";
                        break;
                }
            }
            echo "</tr>";
        }
    } else if (has_capability('coursereport/statistics:isstudent', $context)) {
        $user = $USER;

        echo "<td>" . fullname($user) . "</td>";
        echo "<td>" . $user->username . "</td>";        
        foreach ($cms as $cm) {
            switch ($cm->name) {
                case "jinotechboard" :
                case "teamboard" :
                    $sql = "select count(*) "
                            . "from {" . $cm->name . "_contents} w "
                            . "join {" . $cm->name . "} b on b.id = :instance and b.id = w.board and b.course = :courseid "
                            . "where w.userid = :userid";
                    $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                    echo "<td>" . $DB->count_records_sql($sql, $param) . "</td>";
                    break;
                case "jinoforum" :
                    $sql = "select count(*) "
                            . "from {jinoforum_posts} w "
                            . "join {jinoforum_discussions} d on d.id = w.discussion "
                            . "join {jinoforum} f on  f.id = :instance and f.id = d.jinoforum and f.course = :courseid "
                            . "where w.userid = :userid";
                    $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                    echo "<td>" . $DB->count_records_sql($sql, $param) . "</td>";
                    break;
            }
        }
         echo "</tr>";
    }
    echo "</table>"
    . "</div></br>";
    $url = "board_write_type.php?id=$id&excell=1";
    echo '<button onclick="location.href=\'' . $url . '\'" class="red-form">'.get_string('download').'</button>';
    echo $OUTPUT->footer();
} else {
     $fields = array(
        get_string('user:name','coursereport_statistics'),
    	get_string('user:hakbun','coursereport_statistics') 
    );
     
    foreach ($cms as $cm) {
        $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
        $modulename = get_string('modulename', $cm->name);
        $fields[] = $mod->name;
    }
    $filename = 'b_w_t_statistics_'.$id.'.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        foreach ($users as $user) {
            $col = 0;
            $worksheet[0]->write($row, $col++, fullname($user));
            $worksheet[0]->write($row, $col++, $user->username);                       
            foreach ($cms as $cm) {
                switch ($cm->name) {
                    case "jinotechboard" :
                    case "teamboard" :
                        $sql = "select count(*) "
                                . "from {" . $cm->name . "_contents} w "
                                . "join {" . $cm->name . "} b on b.id = :instance and b.id = w.board and b.course = :courseid "
                                . "where w.userid = :userid";
                        $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                        $worksheet[0]->write($row, $col++, $DB->count_records_sql($sql, $param));
                        break;
                    case "jinoforum" :
                        $sql = "select count(*) "
                                . "from {jinoforum_posts} w "
                                . "join {jinoforum_discussions} d on d.id = w.discussion "
                                . "join {jinoforum} f on  f.id = :instance and f.id = d.jinoforum and f.course = :courseid "
                                . "where w.userid = :userid";
                        $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                        $worksheet[0]->write($row, $col++, $DB->count_records_sql($sql, $param));
                        break;
                }
            }
            $row++;
        }
    } else if (has_capability('coursereport/statistics:isstudent', $context)) {
        $user = $USER;
        $worksheet[0]->write($row, $col++, fullname($user));
        $worksheet[0]->write($row, $col++, $user->username);        
        foreach ($cms as $cm) {
            switch ($cm->name) {
                case "jinotechboard" :
                case "teamboard" :
                    $sql = "select count(*) "
                            . "from {" . $cm->name . "_contents} w "
                            . "join {" . $cm->name . "} b on b.id = :instance and b.id = w.board and b.course = :courseid "
                            . "where w.userid = :userid";
                    $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                    $worksheet[0]->write($row, $col++, $DB->count_records_sql($sql, $param));
                    break;
                case "jinoforum" :
                    $sql = "select count(*) "
                            . "from {jinoforum_posts} w "
                            . "join {jinoforum_discussions} d on d.id = w.discussion "
                            . "join {jinoforum} f on  f.id = :instance and f.id = d.jinoforum and f.course = :courseid "
                            . "where w.userid = :userid";
                    $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                    $worksheet[0]->write($row, $col++, $DB->count_records_sql($sql, $param));
                    break;
            }
        }
    }
    $workbook->close();
    die;
}

