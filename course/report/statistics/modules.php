<?php
require('../../../config.php');
require_once $CFG->dirroot . '/course/report/statistics/lib.php';
require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT); // course id 
$search = optional_param('search', '', PARAM_CLEAN);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 'all', PARAM_CLEAN);
$excell = optional_param('excell', 0, PARAM_INT);

$offset = ($page - 1) * $perpage;

require_login();


$context = context_course::instance($id);
$PAGE->set_context($context);


$course = get_course($id);

$PAGE->set_url('/course/report/statistics/modules.php?id=' . $id);
$PAGE->set_course($course);
$PAGE->set_pagelayout('course');
$strplural = get_string('learningstats', 'block_oklass_course_menu');

$PAGE->navbar->add(get_string('gradeandattend', 'block_oklass_course_menu'));
$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$PAGE->requires->jquery();

$sql = "select u.* from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "join {enrol} e on e.courseid = c.instanceid  "
        . "join {user_enrolments} ue on ue.enrolid = e.id and ue.userid = ra.userid  "
        . "where ra.contextid = :contextid and ue.status != 1";
$where = '';
$param = array('contextid' => $context->id, 'rolename' => 'student', 'contextlevel' => CONTEXT_COURSE);
if ($search) {
    $where = ' and  ((u.firstname like :searchtxt1 or u.lastname like :searchtxt2 or u. firstname||lastname like :searchtxt3) or u.username like :searchtxt4 )';
    $param['searchtxt1'] = $param['searchtxt2'] = $param['searchtxt3'] = $param['searchtxt4'] = '%' . $search . '%';
}
if (!$excell) {
    $users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param, $offset, $perpage);
} else {
    $users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param);
}
$countsql = "select count(u.id) from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "join {enrol} e on e.courseid = c.instanceid  "
        . "join {user_enrolments} ue on ue.enrolid = e.id and ue.userid = ra.userid  "
        . "where ra.contextid = :contextid and ue.status != 1";
$usercount = $DB->count_records_sql($countsql . $where . ' order by u.username asc', $param);
$headsql = "select cm.id, cm.instance, mo.name, ctx.id as contextid from {course_modules} cm "
        . "join {modules} mo on mo.id = cm.module and mo.name != 'lcms' and mo.name != 'okmedia' and mo.visible = 1 "
        . "join {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
        . "where cm.course = :course and cm.section = :section ";
$sql_select = "SELECT mc.id, mc.fullname, mc.shortname , mc.format as courseformat 
 ,(SELECT COUNT(*) FROM {role_assignments} ra JOIN {role} ro ON ra.roleid = ro.id JOIN {context} ctx ON ra.contextid = ctx.id JOIN {course} co ON ctx.instanceid = co.id AND contextlevel = :contextlevel1 WHERE co.id = mc.id AND ro.id = 5) as student 
 ,(SELECT COUNT(*) FROM {role_assignments} ra JOIN {role} ro ON ra.roleid = ro.id JOIN {context} ctx ON ra.contextid = ctx.id JOIN {course} co ON ctx.instanceid = co.id AND contextlevel = :contextlevel2 WHERE co.id = mc.id AND ro.id = 41) as auditor 
 ,(SELECT COUNT(cm.id) from {course_modules} cm join {modules} mo on mo.id = cm.module and (mo.name = 'lcms' or mo.name = 'okmedia') WHERE cm.course = mc.id) as lcmscount 
 ,(SELECT COUNT(cm.id) from {course_modules} cm  join {modules} mo on mo.id = cm.module and mo.name != 'lcms' and mo.name != 'okmedia' WHERE cm.course = mc.id) as modcount 
        FROM {course} mc WHERE mc.id = :courseid";
$param = array('courseid' => $id, 'contextlevel1' => CONTEXT_COURSE, 'contextlevel2' => CONTEXT_COURSE);
$course_data = $DB->get_record_sql($sql_select, $param);
$section_cnt = $DB->count_records_sql('select COUNT(section) FROM m_course_sections where course = :id', array('id' => $id));
$sections_sql = "select DISTINCT cs.id,cs.name,cs.section ,
	(select count(*) from {course_modules} cm 
	JOIN {modules} mo ON mo.id = cm.module AND mo.name != 'lcms' and mo.name != 'okmedia' 
	where cm.section = cs.id ) AS colspan 
	from {course_sections} cs
	where cs.visible = 1 and cs.course = :id
	order by cs.section asc";
$count_sections = $DB->get_record_sql('select value from {course_format_options} where name = \'numsections\' and courseid = :id', array('id' => $id));
$course_sections = $DB->get_records_sql($sections_sql, array('id' => $id), 1, $count_sections->value);

if (!$excell) {
    echo $OUTPUT->header();
    $url = "modules.php?id=$id&excell=1";
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        ?>
        <h3><?php echo get_string('lcms_edu', 'coursereport_statistics'); ?></h3>
        <table class="generaltable">
            <caption class="hidden-caption"><?php echo get_string('lcms_edu', 'coursereport_statistics'); ?></caption>
            <thead>
                <tr>
                    <th scope="row"><?php echo get_string('student', 'coursereport_statistics'); ?></th>
                    <!--<th scope="row"><?php echo get_string('auditor', 'coursereport_statistics'); ?></th>-->
                    <th scope="row"><?php echo get_string('sectioncnt', 'coursereport_statistics'); ?></th>
                    <th scope="row"><?php echo get_string('lcmscnt', 'coursereport_statistics'); ?></th>
                    <th scope="row"><?php echo get_string('modcnt', 'coursereport_statistics'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="col"><?php echo $course_data->student; ?></td>
                    <!--<td scope="col"><?php echo $course_data->auditor; ?></td>-->
                    <td scope="col"><?php echo $section_cnt - 1; ?></td>
                    <td scope="col"><?php echo $course_data->lcmscount; ?></td>
                    <td scope="col"><?php echo $course_data->modcount; ?></td>
                </tr>
            </tbody>
        </table>

        <h3><?php echo get_string('learningstats', 'block_oklass_course_menu'); ?></h3>
        <?php
    } else {
        echo '<h3>' . get_string('my_edu', 'coursereport_statistics') . '</h3>';
    }
    ?>
    <form class="table-search-option stat_form">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <input type="hidden" name="type" value="all">
        <div class="select_colume"> 
            <span style="font-weight: bold; cursor: pointer;" onclick="$('input[name=type]').val('all'); $('form.stat_form').submit();"><?php echo get_string('all', 'coursereport_statistics'); ?></span>
            | <span style="font-weight: bold;  cursor: pointer;" onclick="$('input[name=type]').val('read'); $('form.stat_form').submit();"><?php echo get_string('view', 'coursereport_statistics'); ?></span>
            | <span style="font-weight: bold;  cursor: pointer;" onclick="$('input[name=type]').val('write'); $('form.stat_form').submit();"><?php echo get_string('submit', 'coursereport_statistics'); ?></span>
    <!--                    | <span style="font-weight: bold; cursor: pointer;" onclick="$('input[name=type]').val('comment'); $('form.stat_form').submit();"><?php echo get_string('comment', 'coursereport_statistics'); ?></span>-->
        </div>
        <div class="stat_search_area">
            <input type="text" title="search" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('searchplaceholder', 'coursereport_statistics'); ?>">
            <input type="submit" value="<?php echo get_string('search', 'local_jinoboard'); ?>" class="board-search"/>
            <input type="button" onclick="location.href = '<?php echo $url; ?>'" class="red-form" value="<?php echo get_string('download'); ?>" />
        </div>
    </form>
    <?php
    echo '<table class="generaltable stat_table">';
    echo '<caption class="hidden-caption">' . get_string('stat_edu', 'coursereport_statistics') . '</caption><thead>'
    . '<tr>';
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        echo '<th scope="row" rowspan="3">' . get_string('no', 'coursereport_statistics') . '</th>';
    }
    echo '<th scope="row" rowspan="3">' . get_string('user:name', 'coursereport_statistics') . '</th>'
    . '<th scope="row" rowspan="3">' . get_string('user:hakbun', 'coursereport_statistics') . '</th>';
    $rows1 = array();
    $rows2 = array();
    $rows3 = array();
    foreach ($course_sections as $course_section) {
        echo '<th scope="row" colspan="' . $course_section->colspan . '">';
        $title = get_section_name($course_data->id, $course_section);
        echo $title;
        echo '</th>';
        $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
        foreach ($heads as $cm) {
            $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
            $modulename = get_string('modulename', $cm->name);
            $activityicon = $OUTPUT->pix_icon('icon', $modulename, $cm->name, array('class' => 'icon', 'title' => $mod->name, 'alt' => $mod->name));
            $rows1[] = '<th scope="row"><a href="' . $CFG->wwwroot . '/mod/' . $cm->name . '/view.php?id=' . $cm->id . '">' . $activityicon . '</a></th>';
            $row2 = '<th scope="row">';
            if ($type == 'all' || $type == 'read') {
                $row2 .= '<span active="off" class="stat_span view_span" onclick="list_active(this,' . "'" . 'view_span' . "'" . ')">' . get_string('view', 'coursereport_statistics') . '</span>';
            }
            if ($type == 'all' || $type == 'write') {
                $row2 .= '<span active="off" class="stat_span write_span" onclick="list_active(this,' . "'" . 'write_span' . "'" . ')">' . get_string('submit', 'coursereport_statistics') . '</span>';
            }
            if ($cm->name == 'jinotechboard') {
                if ($type == 'all' || $type == 'comment') {
                    $row2 .= '<span active="off" class="stat_span comment_span" onclick="list_active(this,' . "'" . 'comment_span' . "'" . ')">' . get_string('comment', 'coursereport_statistics') . '</span>';
                }
            }
            $row2 .= '</th>';
            $rows2[] = $row2;
        }
        if (!$heads) {
            $rows1[] = '<th scope="row">&nbsp;</th>';
            $rows2[] = '<th scope="row">&nbsp;</th>';
        }
    }
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        $num = $usercount - $offset;
        // 로그를 위해
        $targetids = array();
        foreach ($users as $user) {
            $targetids[] = $user->id;
            $row3 = "<tr><td>" . $num-- . "</td><td>" . fullname($user) . "</td>";
            $row3 .= "<td>" . $user->username . "</td>";
            foreach ($course_sections as $course_section) {
                $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
                foreach ($heads as $cm) {
                    $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
                    $read_cnt = $DB->get_field_sql('select read_count from {lmsdata_log} where courseid = :courseid and modname = :modname and write_count = 0 and cmid = :cmid and userid = :userid', array('courseid' => $id, 'modname' => $cm->name, 'userid' => $user->id, 'cmid' => $cm->id));
                    $write_cnt = $DB->get_field_sql('select write_count from {lmsdata_log} where courseid = :courseid and modname = :modname and read_count = 0 and cmid = :cmid and userid = :userid', array('courseid' => $id, 'modname' => $cm->name, 'userid' => $user->id, 'cmid' => $cm->id));
                    $row3 .= '<td>';
                    if ($type == 'all' || $type == 'read') {
                        $row3 .= ($read_cnt) ? '<span class="stat_span view_span_col">' . $read_cnt . '</span>' : '<span class="stat_span view_span_col">0</span>';
                    }
                    if ($type == 'all' || $type == 'write') {
                        $row3 .= ($write_cnt) ? '<span class="stat_span write_span_col">' . $write_cnt . '</span>' : '<span class="stat_span write_span_col">0</span>';
                    }
                    if ($cm->name == 'jinotechboard') {
                        $comcnt = '';
                        $sql = "select count(*) "
                                . "from {" . $cm->name . "_comments} c "
                                . "join {" . $cm->name . "} b on  b.id = :instance and b.id = c.board and b.course = :courseid "
                                . "where c.userid = :userid";
                        $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                        $comcnt = $DB->count_records_sql($sql, $param);
                        if ($type == 'all' || $type == 'comment') {
                            $row3 .= '<span class="stat_span comment_span_col">' . $comcnt . '</span>';
                        }
                    }
                    $row3 .= '</td>';
                }
                if (!$heads) {
                    $row3 .= '<td>-</td>';
                }
            }
            $row3 .= "</tr>";
            $rows3[] = $row3;
        }
    } else if (has_capability('coursereport/statistics:isstudent', $context)) {
        $user = $USER;
        $row3 = "<tr><td>" . fullname($user) . " </td>";
        $row3 .= "<td>" . $user->username . "</td>";
        foreach ($course_sections as $course_section) {
            $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
            foreach ($heads as $cm) {
                $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
                $read_cnt = $DB->get_field_sql('select read_count from {lmsdata_log} where courseid = :courseid and modname = :modname and write_count = 0 and cmid = :cmid and userid = :userid', array('courseid' => $id, 'modname' => $cm->name, 'userid' => $user->id, 'cmid' => $cm->id));
                $write_cnt = $DB->get_field_sql('select write_count from {lmsdata_log} where courseid = :courseid and modname = :modname and read_count = 0 and cmid = :cmid and userid = :userid', array('courseid' => $id, 'modname' => $cm->name, 'userid' => $user->id, 'cmid' => $cm->id));
                $row3 .= '<td>';
                if ($type == 'all' || $type == 'read') {
                    $row3 .= ($read_cnt) ? '<span class="stat_span view_span_col">' . $read_cnt . '</span>' : '<span class="stat_span view_span_col">-</span>';
                }
                if ($type == 'all' || $type == 'write') {
                    $row3 .= ($write_cnt) ? '<span class="stat_span write_span_col">' . $write_cnt . '</span>' : '<span class="stat_span write_span_col">-</span>';
                }
                if ($cm->name == 'jinotechboard') {
                    $comcnt = '';
                    $sql = "select count(*) "
                            . "from {" . $cm->name . "_comments} c "
                            . "join {" . $cm->name . "} b on  b.id = :instance and b.id = c.board and b.course = :courseid "
                            . "where c.userid = :userid";
                    $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                    $comcnt = $DB->count_records_sql($sql, $param);
                    if ($type == 'all' || $type == 'comment') {
                        $row3 .= '<span class="stat_span comment_span_col">' . $comcnt . '</span>';
                    }
                }
                $row3 .= '</td>';
            }
            if (!$heads) {
                $row3 .= '<td>-</td>';
            }
        }
        $row3 .= "</tr>";
        $rows3[] = $row3;
    }
    echo '</tr><tr>' . implode('', $rows1) . '</tr><tr>' . implode('', $rows2) . "</tr></thead><tbody>";
    if ($users) {
        foreach ($rows3 as $row) {
            echo $row;
        }
    } else {
        $colspan = count($rows1) + 3;
        echo"<tr><td colspan='" . $colspan . "'>" . get_string('nouser','coursereport_statistics') . "</td></tr>";
    }
    echo "</tbody></table>";

    $params = array('id' => $id, 'search' => $search, 'type' => $type, 'perpage' => $perpage);
    $total_page = ceil($usercount / $perpage);
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        echo '<div class="table-footer-area">';
        coursereport_get_paging_bar($CFG->wwwroot . '/course/report/statistics/modules.php', $params, $total_page, $page);
        echo '</div>';
    }
    ?> 

    <script type="text/javascript">
        function list_active(obj, cls) {
            if ($(obj).attr('active') == 'off') {
                $('.' + cls).attr('active', 'on');
                switch (cls) {
                    case 'view_span':
                        $('.view_span_col').attr('style', '');
                        $('.write_span_col').attr('style', 'display:none !important;');
                        $('.comment_span_col').attr('style', 'display:none !important;');

                        $('.write_span').attr('active', 'off');
                        $('.comment_span').attr('active', 'off');
                        break;
                    case 'write_span':
                        $('.view_span_col').attr('style', 'display:none !important;');
                        $('.write_span_col').attr('style', '');
                        $('.comment_span_col').attr('style', 'display:none !important;');

                        $('.view_span').attr('active', 'off');
                        $('.comment_span').attr('active', 'off');
                        break;
                    case 'comment_span':
                        $('.view_span_col').attr('style', 'display:none !important;');
                        $('.write_span_col').attr('style', 'display:none !important;');
                        $('.comment_span_col').attr('style', '');

                        $('.write_span').attr('active', 'off');
                        $('.view_span').attr('active', 'off');
                        break;
                }
            } else {
                $('.view_span').attr('active', 'off');
                $('.write_span').attr('active', 'off');
                $('.comment_span').attr('active', 'off');
                $('.view_span_col').attr('style', '');
                $('.write_span_col').attr('style', '');
                $('.comment_span_col').attr('style', '');
            }
        }
    </script>
    <?php
    echo $OUTPUT->footer();
} else {
    $filename = 'statistics_' . $id . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');

    $worksheet[0]->write(0, 0, get_string('stat_course', 'coursereport_statistics'));

    $worksheet[0]->write(1, 0, get_string('student', 'coursereport_statistics'));
    $worksheet[0]->write(1, 1, get_string('auditor', 'coursereport_statistics'));
    $worksheet[0]->write(1, 2, get_string('sectioncnt', 'coursereport_statistics'));
    $worksheet[0]->write(1, 3, get_string('lcmscnt', 'coursereport_statistics'));
    $worksheet[0]->write(1, 4, get_string('modcnt', 'coursereport_statistics'));

    $worksheet[0]->write(2, 0, $course_data->student);
    $worksheet[0]->write(2, 1, $course_data->auditor);
    $worksheet[0]->write(2, 2, $section_cnt);
    $worksheet[0]->write(2, 3, $course_data->lcmscount);
    $worksheet[0]->write(2, 4, $course_data->modcount);

    $worksheet[0]->write(3, 0, get_string('stat_edu', 'coursereport_statistics'));

    $worksheet[0]->write(4, 0, get_string('name', 'coursereport_statistics'));
    $worksheet[0]->write(4, 1, get_string('usernumber', 'coursereport_statistics'));
    $col = 2;
    $col2 = 2;
    $rows1 = array();
    foreach ($course_sections as $course_section) {
        $title = get_section_name($course_data->id, $course_section);
        $worksheet[0]->write(4, $col, $title);
        if ($course_section->colspan) {
            $col += $course_section->colspan * 3;
        } else {
            $col += 3;
        }
        $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
        foreach ($heads as $cm) {
            $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
            $rows1[] = $mod->name;
        }
        if (!$heads) {
            $rows1[] = '-';
        }
    }
    foreach ($rows1 as $row => $val) {
        $worksheet[0]->write(5, $col2, $val);
        $worksheet[0]->write(6, $col2, get_string('view', 'coursereport_statistics'));
        $worksheet[0]->write(6, $col2 + 1, get_string('write', 'coursereport_statistics'));
        $worksheet[0]->write(6, $col2 + 2, get_string('comment', 'coursereport_statistics'));
        $col2 += 3;
    }


    $row = 7;
    foreach ($users as $user) {
        $col3 = 0;
        $worksheet[0]->write($row, $col3++, fullname($user));
        $worksheet[0]->write($row, $col3++, $user->username);
        foreach ($course_sections as $course_section) {
            $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
            foreach ($heads as $cm) {
                $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
                $read_cnt = $DB->get_field_sql('select read_count from {lmsdata_log} where courseid = :courseid and modname = :modname and write_count = 0 and cmid = :cmid and userid = :userid', array('courseid' => $id, 'modname' => $cm->name, 'userid' => $user->id, 'cmid' => $cm->id));
                $write_cnt = $DB->get_field_sql('select write_count from {lmsdata_log} where courseid = :courseid and modname = :modname and read_count = 0 and cmid = :cmid and userid = :userid', array('courseid' => $id, 'modname' => $cm->name, 'userid' => $user->id, 'cmid' => $cm->id));
                if (!$read_cnt) {
                    $read_cnt = 0;
                }
                if (!$write_cnt) {
                    $write_cnt = 0;
                }
                $comcnt = 0;
                if ($cm->name == 'jinotechboard') {
                    $sql = "select count(*) "
                            . "from {" . $cm->name . "_comments} c "
                            . "join {" . $cm->name . "} b on  b.id = :instance and b.id = c.board and b.course = :courseid "
                            . "where c.userid = :userid";
                    $param = array('courseid' => $id, 'userid' => $user->id, 'instance' => $cm->instance);
                    $comcnt = $DB->count_records_sql($sql, $param);
                }
                $worksheet[0]->write($row, $col3++, $read_cnt);
                $worksheet[0]->write($row, $col3++, $write_cnt);
                $worksheet[0]->write($row, $col3++, $comcnt);
            }
            if (!$heads) {
                $worksheet[0]->write($row, $col3++, 0);
                $worksheet[0]->write($row, $col3++, 0);
                $worksheet[0]->write($row, $col3++, 0);
            }
        }
        $rows3[] = $row3;
        $row++;
    }




    $workbook->close();
    die;
}
