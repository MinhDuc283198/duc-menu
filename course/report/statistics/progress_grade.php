<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once $CFG->libdir . '/formslib.php';
require_once $CFG->dirroot.'/grade/lib.php';
require_once($CFG->dirroot.'/lib/grade/grade_grade.php');
require_once($CFG->dirroot.'/lib/grade/grade_item.php');
 
$id = required_param('id', PARAM_INT);    // course id

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);

require_course_login($course, true);
$context = context_course::instance($course->id);
$PAGE->set_context($context);

//권한이 있는 사람만 성적부 반영이 가능함.
if (!has_capability('coursereport/statistics:isteacher', $context)) {
    error(get_string('noauth','coursereport_statistics'));
    die();
}

//성적부 아이템 만들기
$itemid = 0;

if(!$itemid = $DB->get_field('grade_items', 'id', array('courseid' => $course->id, 'idnumber' => COURSEREPORT_STATICS_PROGRESS_GRADE_ITEM_IDNUMBER))) {
    $data = new stdClass();
    $data->id =  0;
    $data->courseid =  $course->id;
    $data->itemtype = 'manual';
    $data->itemname = get_string('studyprogress', 'coursereport_statistics');
    $data->iteminfo = '';
    $data->idnumber = COURSEREPORT_STATICS_PROGRESS_GRADE_ITEM_IDNUMBER;
    $data->gradetype = 1;
    $data->grademax = 100;
    $data->grademin =  0;
    $data->gradepass = 0;
    $data->display = 1;
    $data->decimals = 0;
    $data->weightoverride = 0;
    $data->aggregationcoef = 0;
    $data->aggregationcoef2 = 0;

    $grade_item = new grade_item(array('id'=>0, 'courseid'=>$course->id));

    grade_item::set_properties($grade_item, $data);

    $grade_item->outcomeid = null;
    $itemid = $grade_item->insert();
}


//학생 데이터만 가져오기
$roleobjs = $DB->get_records('role', array('shortname' => 'student'));
$roles = array_keys($roleobjs);
list($sql_in, $sql_params) = $DB->get_in_or_equal($roles, SQL_PARAMS_NAMED, 'roleid');

$sql_select = "SELECT  ur.* ";
$sql_from = " FROM {user} ur
              JOIN (
                SELECT userid 
                FROM {role_assignments} 
                WHERE contextid = :contextid AND roleid $sql_in
                GROUP BY userid 
                ) ra ON ra.userid = ur.id ";

$sql_conditions = array('ur.deleted = :deleted');

$sql_params['contextid'] = $context->id;
$sql_params['deleted'] = 0;

$sql_where = ' WHERE '.implode(' AND ', $sql_conditions);
$sql_orderby = ' ORDER BY firstname, lastname ASC ';

$users = $DB->get_records_sql($sql_select.$sql_from.$sql_where.$sql_orderby, $sql_params);

if($users){
$userids = array_keys($users);
list($sql, $params) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED, 'userid');

//등록된 동영상 및 이러닝 콘텐츠 수 가져오기 (section 0에 있는 것은 제외)
$sql = "select count(*) from {course_modules} m 
join {modules} mo on mo.id = m.module 
join {course_sections} s on s.id = m.section 
where (mo.name='okmedia' or mo.name='lcms') and s.section > 0 and m.visible = 1 and mo.visible = 1 and m.course = :courseid";

if(!$totalcount = $DB->count_records_sql($sql,array('courseid'=>$course->id))){
    $totalcount = 1;
}

//USER별 진도율 가져오기
$sql = "select tt.* from (
(select t.id,t.userid, t.progress from {okmedia_track} t 
join {course_modules} m on m.id = t.okmediaid 
join {modules} mo on mo.id = m.module 
join {course_sections} s on s.id = m.section 
where mo.name = 'okmedia' and mo.visible = 1 and s.section > 0 and m.visible = 1 and m.course = :courseid1)
union all 
(select t.id,t.userid, t.progress from {lcms_track} t 
join {course_modules} m on m.id = t.lcms  
join {modules} mo on mo.id = m.module 
join {course_sections} s on s.id = m.section 
where mo.name = 'lcms' and mo.visible = 1 and s.section > 0 and  m.visible = 1 and m.course = :courseid2)
) tt";

$tracks = $DB->get_records_sql($sql,array('courseid1'=>$course->id,'courseid2'=>$course->id));

//진도율 합산
$grades = array();
foreach($tracks as $track) {
    $userid = $track->userid;
    $progress = $track->progress;    
    if(!$grades[$userid]) {
        $grades[$userid] = 0;
    }
    
    $grades[$userid] += $progress;
    
}

$count = 0;
foreach($users as $user) {
    $userid = $user->id;
    $gradegrade = grade_grade::fetch(array('itemid' => $itemid, 'userid' => $userid));
    $mygrade = round($grades[$userid]/$totalcount);
    if(empty($gradegrade)) {
        $gradegrade = new grade_grade();
        $gradegrade->itemid = $itemid;
        $gradegrade->userid = $userid;
        $gradegrade->finalgrade = $mygrade;
        $gradegrade->rawgrademax = 100;
        $gradegrade->rawgrademin = 0;
        $gradegrade->timecreated = time();
        $gradegrade->timemodified = time();
        $gradegrade->insert();
    }else {
        $gradegrade->finalgrade = $mygrade;
        $gradegrade->timemodified = time();
        $gradegrade->update();
    }
        $count++;
}
}

$gradeitem = grade_item::fetch(array('itemtype' => 'course', 'courseid' => $course->id));
$gradeitem->regrade_final_grades();

if(!$count){
    $count = 0;
}

$returnvalue = new stdClass();
$returnvalue->status = 'success';
$returnvalue->count = $count;
$returnvalue->text = get_string('progress:alert2', 'coursereport_statistics', $returnvalue);

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);