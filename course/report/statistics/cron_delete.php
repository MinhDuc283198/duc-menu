<?php
define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->libdir . '/clilib.php');

cron_setup_user();

ini_set('display_errors', '1');
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

$all_read_histories = $DB->get_recordset_sql('select * from {lmsdata_log} where read_count != 0 order by id asc');
/* read delete */
/*
$cnt = 1;
foreach($all_read_histories as $history){
    $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and cmid = :cmid and section = :section and read_count != 0 order by read_count desc"; 
    $get_equal_histories = $DB->get_records_sql($sql,array('courseid'=>$history->courseid,'userid'=>$history->userid,'cmid'=>$history->cmid,'section'=>$history->section));
    $top_count = 0;
    $prev_cnt = 0;
    echo $cnt."\r\n";
    echo "[";
    foreach($get_equal_histories as $equal_history){
        echo $equal_history->id."(".$top_count.")";
        if($top_count > $equal_history->read_count || $prev_cnt >= $equal_history->read_count){
            echo "DELETE : ".$equal_history->id."\r\n";
            $DB->delete_records('lmsdata_log',array('id'=>$equal_history->id));
            $prev_cnt = 0;
        } else {
            $top_count = $equal_history->read_count;
            $prev_cnt = $equal_history->read_count;
        }
    }
    echo "]";
   
    $cnt++;
}
 */
/* write delete */
$all_write_histories = $DB->get_recordset_sql('select * from {lmsdata_log} where write_count != 0 order by id asc');
$cnt = 1;
foreach($all_write_histories as $history){
    $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and cmid = :cmid and section = :section and write_count != 0 order by write_count desc"; 
    $get_equal_histories = $DB->get_records_sql($sql,array('courseid'=>$history->courseid,'userid'=>$history->userid,'cmid'=>$history->cmid,'section'=>$history->section));
    $top_count = 0;
    $prev_cnt = 0;
    echo $cnt."\r\n";
    echo "[";
    foreach($get_equal_histories as $equal_history){
        echo $equal_history->id."(".$top_count.")";
        if($top_count > $equal_history->write_count || $prev_cnt >= $equal_history->write_count){
            echo "DELETE : ".$equal_history->id."\r\n";
            $DB->delete_records('lmsdata_log',array('id'=>$equal_history->id));
            $prev_cnt = 0;
        } else {
            $top_count = $equal_history->write_count;
            $prev_cnt = $equal_history->write_count;
        }
    }
    echo "]";
   
    $cnt++;
}