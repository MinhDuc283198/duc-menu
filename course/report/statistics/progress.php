<?php
require('../../../config.php');
require_once $CFG->dirroot . '/course/report/statistics/lib.php';
require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT); // course id
$search = optional_param('search', '', PARAM_CLEAN);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 'all', PARAM_CLEAN);
$excell = optional_param('excell', 0, PARAM_INT);

$offset = ($page - 1) * $perpage;

require_login();


$context = context_course::instance($id);
$PAGE->set_context($context);


$course = get_course($id);

$PAGE->set_url('/course/report/statistics/progress.php?id=' . $id);
$PAGE->set_course($course);
$PAGE->set_pagelayout('course');
$strplural = get_string('progressstats', 'block_oklass_course_menu');

$PAGE->navbar->add(get_string('gradeandattend', 'block_oklass_course_menu'));
$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$PAGE->requires->jquery();

$sql = "select u.* from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "where ra.contextid = :contextid ";
$where = '';
$param = array('contextid' => $context->id, 'rolename' => 'student', 'contextlevel' => CONTEXT_COURSE);
if ($search) {
    $where = ' and  ((u.firstname like :searchtxt1 or u.lastname like :searchtxt2 or u. firstname||lastname like :searchtxt3) or u.username like :searchtxt4 )';
    $param['searchtxt1'] = $param['searchtxt2'] = $param['searchtxt3'] = $param['searchtxt4'] = '%' . $search . '%';
}
if (!$excell) {
    $users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param, $offset, $perpage);
} else {
    $users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param);
}
$sql_select = "SELECT mc.id, mc.fullname, mc.shortname , mc.format as courseformat 
        FROM {course} mc 
        WHERE mc.id = :courseid";
$param2 = array('courseid' => $id, 'contextlevel1' => CONTEXT_COURSE, 'contextlevel2' => CONTEXT_COURSE);
$course_data = $DB->get_record_sql($sql_select, $param2);
$countsql = "select count(u.id) from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "where ra.contextid = :contextid ";
$usercount = $DB->count_records_sql($countsql . $where . ' order by u.username asc', $param);
$headsql = "select cm.id, cm.instance, mo.name, ctx.id as contextid from {course_modules} cm "
        . "join {modules} mo on mo.id = cm.module and (mo.name = 'lcms' or mo.name = 'okmedia') and mo.visible = 1 "
        . "join {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
        . "where cm.course = :course and cm.section = :section ";
$section_cnt = $DB->get_field('course_format_options', 'value', array('courseid' => $course_data->id, 'format' => $course_data->courseformat, 'name' => 'numsections'));
$sections_sql = "select DISTINCT cs.id,cs.name,cs.section , 
	(select count(*) from {course_modules} cm 
	JOIN {modules} mo ON mo.id = cm.module AND (mo.name = 'lcms' or mo.name = 'okmedia')  
	where cm.section = cs.id ) AS colspan 
	from {course_sections} cs 
	where cs.visible = 1 and cs.course = :id 
        and (select count(*) from {course_modules} cm 
	JOIN {modules} mo ON mo.id = cm.module AND (mo.name = 'lcms' or mo.name = 'okmedia')  
	where cm.section = cs.id ) > 0 
	order by cs.section asc";
$course_sections = $DB->get_records_sql($sections_sql, array('id' => $id));
if (!$excell) {
    echo $OUTPUT->header();
    $url = "progress.php?id=$id&excell=1";
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        ?>
        <h3><?php echo get_string('progressstats', 'block_oklass_course_menu'); ?></h3>
        <form class="table-search-option stat_form">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="stat_search_area">
                <input type="text" title="search" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('searchplaceholder', 'coursereport_statistics'); ?>">
                <input type="submit" value="<?php echo get_string('search'); ?>" class="board-search"/>
            </div>
        </form>
        <div class="stat_downbtn">
            <input type="button" value="<?php print_string('exceldownload', 'coursereport_statistics'); ?>" onclick="location.href = '<?php echo $url; ?>'"/>
        </div>
        <?php
    } else {
        echo '<h3>' . get_string('my_edu1', 'coursereport_statistics') . '</h3>';
    }

    echo '<table class="generaltable stat_table"><caption class="hidden-caption">학습현황</capiton><thead>'
    . '<tr>';
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        echo '<th rowspan="2" scope="row">' . get_string('no', 'coursereport_statistics') . '</th>';
    }
    echo '<th rowspan="2" scope="row">' . get_string('user:name', 'coursereport_statistics') . '</th>'
    . '<th rowspan="2" scope="row">' . get_string('user:hakbun', 'coursereport_statistics') . '</th>';
    $rows1 = array();
    $rows3 = array();
    foreach ($course_sections as $course_section) {
        if ($course_section->section != '0') {
            $title = get_section_name($COURSE->id, $course_section);
            echo '<th scope="row" colspan="' . $course_section->colspan . '">' . $title . '</th>';
            $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
            foreach ($heads as $cm) {
                $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
                $modulename = get_string('modulename', $cm->name);
                $activityicon = $OUTPUT->pix_icon('icon', $modulename, $cm->name, array('class' => 'icon', 'title' => $mod->name, 'alt' => $mod->name));
                $rows1[] = '<th scope="row"><a href="' . $CFG->wwwroot . '/mod/' . $cm->name . '/view.php?id=' . $cm->id . '">' . $activityicon . '</a></th>';
            }
            if (!$heads) {
                $rows1[] = '<th scope="row">&nbsp;</th>';
            }
        }
    }

    if (has_capability('coursereport/statistics:isteacher', $context)) {
        $num = $usercount - $offset;
        $targetids = array();
        foreach ($users as $user) {
            $targetids[] = $user->id;
            $row3 = "<tr><td>" . $num-- . "</td><td>" . fullname($user) . " </td>";
            $row3 .= "<td>" . $user->username . "</td>";
            foreach ($course_sections as $course_section) {
                if ($course_section->section != '0') {
                    $lcmssql = "SELECT cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress "
                            . "FROM {course_modules} cm "
                            . "JOIN {modules} mo on mo.id = cm.module and mo.name = 'okmedia' and mo.visible = 1 "
                            . "JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
                            . "JOIN {okmedia} lc on lc.id = cm.instance "
                            . "left JOIN {okmedia_track} lt on lt.okmediaid = lc.id and lt.userid = :userid "
                            . "WHERE cm.course = :course and cm.section = :section ";
                    $lcmsdata = $DB->get_records_sql($lcmssql, array('course' => $id, 'userid' => $user->id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));

                    foreach ($lcmsdata as $lcms) {
                        $mod = $DB->get_record($lcms->name, array('id' => $lcms->instance));

                        $row3 .= '<td>';

                        $progress = ($lcms->progress) ? $lcms->progress . "%" : "-";

                        $row3 .= $progress;
                        $row3 .= '</td>';
                    }
                    if (!$lcmsdata) {
                        $row3 .= '<td>-</td>';
                    }
                }
            }
            $row3 .= "</tr>";
            $rows3[] = $row3;
        }
    } else if (has_capability('coursereport/statistics:isstudent', $context)) {
        $user = $USER;
        $row3 = "<tr><td>" . fullname($user) . " </td>";
        $row3 .= "<td>" . $user->username . "</td>";
        foreach ($course_sections as $course_section) {
            if ($course_section->section != '0') {
                $lcmssql = "SELECT cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress "
                        . "FROM {course_modules} cm "
                        . "JOIN {modules} mo on mo.id = cm.module and mo.name = 'okmedia' and mo.visible = 1 "
                        . "JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
                        . "JOIN {okmedia} lc on lc.id = cm.instance "
                        . "left JOIN {okmedia_track} lt on lt.okmediaid = lc.id and lt.userid = :userid "
                        . "WHERE cm.course = :course and cm.section = :section ";
                $lcmsdata = $DB->get_records_sql($lcmssql, array('course' => $id, 'userid' => $user->id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));

                foreach ($lcmsdata as $lcms) {
                    $mod = $DB->get_record($lcms->name, array('id' => $lcms->instance));

                    $row3 .= '<td>';
                    $progress = ($lcms->progress) ? $lcms->progress . "%" : "-";

                    $row3 .= $progress;
                    $row3 .= '</td>';
                }
                if (!$lcmsdata) {
                    $row3 .= '<td>-</td>';
                }
            }
        }
        $row3 .= "</tr>";
        $rows3[] = $row3;
    }
    echo '</tr><tr>' . implode('', $rows1) . '</tr><tr>' . implode('', $rows2) . "</tr></thead><tbody>";
    if ($users) {
        foreach ($rows3 as $row) {
            echo $row;
        }
    }else{
        $colspan = count($rows1) + 3;
        echo"<tr><td colspan='" . $colspan . "'>" . get_string('nouser', 'coursereport_statistics') . "</td></tr>";
    }
    echo "</tbody></table>";

    $params = array('id' => $id, 'search' => $search, 'type' => $type, 'perpage' => $perpage);
    $total_page = ceil($usercount / $perpage);
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        echo '<div class="table-footer-area">';
        coursereport_get_paging_bar($CFG->wwwroot . '/course/report/statistics/progress.php', $params, $total_page, $page);
        echo '</div>';
    }
    ?>

    <script type="text/javascript">
        function list_active(obj, cls) {
            if ($(obj).attr('active') == 'off') {
                $('.' + cls).attr('active', 'on');
                switch (cls) {
                    case 'view_span':
                        $('.view_span_col').attr('style', '');
                        $('.write_span_col').attr('style', 'display:none;');
                        $('.comment_span_col').attr('style', 'display:none;');

                        $('.write_span').attr('active', 'off');
                        $('.comment_span').attr('active', 'off');
                        break;
                    case 'write_span':
                        $('.view_span_col').attr('style', 'display:none;');
                        $('.write_span_col').attr('style', '');
                        $('.comment_span_col').attr('style', 'display:none;');

                        $('.view_span').attr('active', 'off');
                        $('.comment_span').attr('active', 'off');
                        break;
                    case 'comment_span':
                        $('.view_span_col').attr('style', 'display:none;');
                        $('.write_span_col').attr('style', 'display:none;');
                        $('.comment_span_col').attr('style', '');

                        $('.write_span').attr('active', 'off');
                        $('.view_span').attr('active', 'off');
                        break;
                }
            } else {
                $('.view_span').attr('active', 'off');
                $('.write_span').attr('active', 'off');
                $('.comment_span').attr('active', 'off');
                $('.view_span_col').attr('style', '');
                $('.write_span_col').attr('style', '');
                $('.comment_span_col').attr('style', '');
            }
        }

        function progress_grading() {
            if (confirm('<?php print_string('progress:alert1', 'coursereport_statistics'); ?>')) {
                $.ajax({
                    url: '<?php echo $CFG->wwwroot . '/course/report/statistics/progress_grade.php'; ?>',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: <?php echo $id ?>
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            alert(data.text);
                        }
                    }
                });
            }
        }
    </script>
    <?php
    echo $OUTPUT->footer();
} else {
    $filename = 'statistics_' . $id . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');

    $worksheet[0]->write(0, 0, get_string('stat_course', 'coursereport_statistics'));

    $worksheet[0]->write(1, 0, get_string('student', 'coursereport_statistics'));
    $worksheet[0]->write(1, 1, get_string('auditor', 'coursereport_statistics'));
    $worksheet[0]->write(1, 2, get_string('sectioncnt', 'coursereport_statistics'));
    $worksheet[0]->write(1, 3, get_string('lcmscnt', 'coursereport_statistics'));
    $worksheet[0]->write(1, 4, get_string('modcnt', 'coursereport_statistics'));

    $worksheet[0]->write(2, 0, $course_data->student);
    $worksheet[0]->write(2, 1, $course_data->auditor);
    $worksheet[0]->write(2, 2, $section_cnt);
    $worksheet[0]->write(2, 3, $course_data->lcmscount);
    $worksheet[0]->write(2, 4, $course_data->modcount);

    $worksheet[0]->write(3, 0, get_string('stat_edu', 'coursereport_statistics'));

    $worksheet[0]->write(4, 0, get_string('name', 'coursereport_statistics'));
    $worksheet[0]->write(4, 1, get_string('usernumber', 'coursereport_statistics'));
    $col = 2;
    $col2 = 2;
    $rows1 = array();
    foreach ($course_sections as $course_section) {
        $worksheet[0]->write(4, $col, $course_section->name);
        if ($course_section->colspan) {
            $col += $course_section->colspan * 1;
        } else {
            $col += 1;
        }
        $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
        foreach ($heads as $cm) {
            $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
            $rows1[] = $mod->name;
        }
        if (!$heads) {
            $rows1[] = ' ';
        }
    }
    foreach ($rows1 as $row => $val) {
        $worksheet[0]->write(5, $col2, $val);
        $col2 += 1;
    }


    $row = 6;
    foreach ($users as $user) {
        $col3 = 0;
        $worksheet[0]->write($row, $col3++, fullname($user));
        $worksheet[0]->write($row, $col3++, $user->username);
        foreach ($course_sections as $course_section) {
            $lcmssql = "SELECT cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress "
                    . "FROM {course_modules} cm "
                    . "JOIN {modules} mo on mo.id = cm.module and mo.name = 'okmedia' and mo.visible = 1 "
                    . "JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
                    . "JOIN {okmedia} lc on lc.id = cm.instance "
                    . "JOIN {okmedia_track} lt on lt.okmediaid = lc.id and lt.userid = :userid "
                    . "WHERE cm.course = :course and cm.section = :section ";
            $lcmsdata = $DB->get_records_sql($lcmssql, array('course' => $id, 'userid' => $user->id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));

            foreach ($lcmsdata as $lcms) {
                $mod = $DB->get_record($lcms->name, array('id' => $lcms->instance));
                $progress = ($lcms->progress) ? $lcms->progress . "%" : "-";

                $worksheet[0]->write($row, $col3++, $progress);
            }
            if (!$lcmsdata) {
                $col3 += 1;
            }
        }
        $rows3[] = $row3;
        $row++;
    }




    $workbook->close();
    die;
}
