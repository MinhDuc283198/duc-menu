<?php
define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->libdir . '/clilib.php');

cron_setup_user();

ini_set('display_errors', '1');
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

$all_read_histories = $DB->get_recordset_sql('select * from {lmsdata_log} where read_count != 0 order by id asc');
/* read delete */
$n = 1;
foreach($all_read_histories as $history){
    $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and cmid = :cmid and section = :section and read_count != 0 order by read_count desc"; 
    $get_equal_histories = $DB->get_records_sql($sql,array('courseid'=>$history->courseid,'userid'=>$history->userid,'cmid'=>$history->cmid,'section'=>$history->section));
    $top_count = 0;
    $prev_cnt = 0;
    $cnt = 1;
    $read_count = 0;
    $prev_count = 0;
    $total = 0;
    echo $n."\r\n";
    foreach($get_equal_histories as $equal_history){
        if($cnt == 2){
            $read_count = $equal_history->read_count;
            $DB->delete_records('lmsdata_log',array('id'=>$equal_history->id));
            $total = $read_count + $prev_count;
            echo "[".$prev_id."]/[".$total."]";
            $DB->update_record('lmsdata_log',array('id'=>$prev_id,'read_count'=>$total));
            echo "SUMSUM♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡\r\n";
        }
        if($cnt == 1){
            $prev_id = $equal_history->id;
            $prev_count = $equal_history->read_count;
        }
        $cnt++;
    }
   $n++;
}