<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->libdir . '/clilib.php');

cron_setup_user();

ini_set('display_errors', '1');
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);

$cron_start = time();
$year = get_config('moodle', 'haxa_year');
$term = trim(get_config('moodle', 'haxa_term'),"0");

$yesterday = strtotime('last day midnight', $cron_start);  // 어제 자정
$today = strtotime('today midnight', $cron_start);         // 오늘 자정

if (!$DB->get_record('lmsdata_log_cron', array('starttime' => $today))) {
    
 /** Jinotech Board Write Count * */
 $sql = "select c.id as courseid , c.startdate , c.format , u.id as userid ,b.id as boardid , cm.id as cmid , count(w.id) as write_cnt 
from {course} c 
join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
join {role_assignments} ra on ra.contextid = ct.id 
join {user} u on u.id = ra.userid  
join {jinotechboard_contents} w on w.userid = u.id and w.timecreated >= :yesterday and w.timecreated < :today
join {jinotechboard} b on b.id = w.board and b.course = c.id 
join {course_modules} cm on cm.course = c.id and cm.instance = b.id 
join {modules} module on module.id = cm.module and module.name = 'jinotechboard' 
group by c.id, c.startdate , c.format ,u.id , b.id , cm.id ";
    $param = array('yesterday' => $yesterday, 'today' => $today);
    $write_datas = $DB->get_recordset_sql($sql, $param);
    $cnt = 0;
    $upcnt = 0;
    foreach ($write_datas as $write_data) {
        $weekstarts = array();
        $weekstarts[] = strtotime('last sunday', $write_data->startdate);

        /* 강의별로 현재 주차가 다름으로 강의를 구해온 후에 강의정보를 바탕으로 현재 주차를 구한다 section = 주차 */
        $section_cnt = $DB->get_field('course_format_options', 'value', array('courseid' => $write_data->courseid, 'format' => $write_data->format, 'name' => 'numsections'));
        $current_section = 0;
        for ($i = 1; $i <= $section_cnt; $i++) {
            $weekstarts[$i] = strtotime('next sunday', $weekstarts[$i - 1]);
            if ($weekstarts[$i - 1] <= $cron_start && $weekstarts[$i] > $cron_start) {
                $current_section = $i;
            }
        }

        $data = new stdClass();
        $data->courseid = $write_data->courseid;
        $data->userid = $write_data->userid;
        $data->write_count = $write_data->write_cnt;
        $data->section = $current_section;
        $data->modname = 'jinotechboard';
        $data->cmid = $write_data->cmid;
        $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and modname = 'jinotechboard' and cmid = :cmid and write_count != 0 and section = :section";
        $log = $DB->get_record_sql($sql, array('courseid'=>$write_data->courseid,'userid'=> $write_data->userid,'cmid'=>$write_data->cmid,'section'=>$current_section));
        if ($log && $log->id) {
            $data->id = $log->id;
            $data->write_count = $data->write_count + $log->write_count;
            $DB->update_record('lmsdata_log', $data);  
        } else {
            $DB->insert_record('lmsdata_log', $data);
        }
    }


    /** Jinotech Board Read Count * */
$sql = "select c.id as courseid , c.startdate , c.format , u.id as userid ,b.id as boardid ,cm.id as cmid , count(r.id) as read_cnt 
from {course} c 
join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
join {role_assignments} ra on ra.contextid = ct.id 
join {user} u on u.id = ra.userid  
join {jinotechboard_read} r on r.userid = u.id and r.firstread >= :yesterday and r.firstread < :today 
join {jinotechboard} b on b.id = r.jinotechboardid and b.course = c.id 
join {course_modules} cm on cm.course = c.id and cm.instance = b.id 
join {modules} module on module.id = cm.module and module.name = 'jinotechboard' 
group by c.id, c.startdate , c.format ,u.id , b.id , cm.id";
    $param = array('yesterday' => $yesterday, 'today' => $today);
    $read_datas = $DB->get_recordset_sql($sql, $param);
    foreach ($read_datas as $read_data) {

        $weekstarts = array();
        $weekstarts[] = strtotime('last sunday', $read_data->startdate);

        $current_section = 0;

        $section_cnt = $DB->get_field('course_format_options', 'value', array('courseid' => $read_data->courseid, 'format' => $read_data->format, 'name' => 'numsections'));

        for ($i = 1; $i <= $section_cnt; $i++) {
            $weekstarts[$i] = strtotime('next sunday', $weekstarts[$i - 1]);
            if ($weekstarts[$i - 1] <= $cron_start && $weekstarts[$i] > $cron_start) {
                $current_section = $i;
            }
        }

        $data = new stdClass();
        $data->courseid = $read_data->courseid;
        $data->userid = $read_data->userid;
        $data->read_count = $read_data->read_cnt;
        $data->section = $current_section;
        $data->modname = 'jinotechboard';
        $data->cmid = $read_data->cmid;
        unset($log);
        $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and modname = 'jinotechboard' and cmid = :cmid and read_count != 0 and section = :section";
        $log = $DB->get_record_sql($sql, array('courseid'=>$data->courseid,'userid'=> $data->userid,'cmid'=>$data->cmid,'section'=>$current_section));
        if ($log && $log->id) {
            $data->id = $log->id;
            $data->read_count = $data->read_count + $log->read_count;
            $DB->update_record('lmsdata_log', $data);
        } else {
            $DB->insert_record('lmsdata_log', $data);
        }
    }
    $delete_sql = "delete from {jinotechboard_read} where firstread < :today";
    $DB->execute($delete_sql,array('today'=>$today));

    /** Module Read Count * */
    //If You want used university class add this join {lmsdata_class} yc on yc.course = c.id and yc.year = :year and yc.term = :term 
    $sql = "select mo.name , c.id as courseid , c.startdate , c.format , u.id as userid ,cm.id as cmid , count(logs.id) as read_cnt  
from {course} c 
join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
join {role_assignments} ra on ra.contextid = ct.id 
join {user} u on u.id = ra.userid  
join {course_modules} cm on cm.course = c.id 
join {modules} mo on mo.id = cm.module and (mo.name != 'jinotechboard' and mo.name != 'lcms' and mo.name != 'okmedia') 
join {logstore_standard_log} logs on logs.contextlevel = 70 and logs.contextinstanceid = cm.id and logs.courseid = c.id and logs.component = concat('mod_',mo.name) and logs.action = 'viewed' and logs.userid = u.id and logs.timecreated >= :yesterday and logs.timecreated < :today 
group by mo.name, c.id, c.startdate , c.format ,u.id , cm.id";
    $param = array('yesterday' => $yesterday, 'today' => $today, 'year' => $year, 'term' => $term);
    $read_datas = $DB->get_recordset_sql($sql, $param);
    foreach ($read_datas as $read_data) {

        $weekstarts = array();
        $weekstarts[] = strtotime('last sunday', $read_data->startdate);

        $current_section = 0;

        $section_cnt = $DB->get_field('course_format_options', 'value', array('courseid' => $read_data->courseid, 'format' => $read_data->format, 'name' => 'numsections'));

        for ($i = 1; $i <= $section_cnt; $i++) {
            $weekstarts[$i] = strtotime('next sunday', $weekstarts[$i - 1]);
            if ($weekstarts[$i - 1] <= $cron_start && $weekstarts[$i] > $cron_start) {
                $current_section = $i;
            }
        }

        $data = new stdClass();
        $data->courseid = $read_data->courseid;
        $data->userid = $read_data->userid;
        $data->read_count = $read_data->read_cnt;
        $data->section = $current_section;
        $data->modname = $read_data->name;
        $data->cmid = $read_data->cmid;
        unset($log);
        $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and modname = :modname and cmid = :cmid and read_count != 0 and section = :section";
        $log = $DB->get_record_sql($sql, array('courseid'=>$data->courseid,'userid'=> $data->userid,'modname'=>$data->modname,'cmid'=>$data->cmid,'section'=>$current_section));
        if ($log && $log->id) {
            $data->id = $log->id;
            $data->read_count = $data->read_count + $log->read_count;
            $DB->update_record('lmsdata_log', $data);
        } else {
            $DB->insert_record('lmsdata_log', $data);
        }
    }
    
    
    
        /** Module Write Count * */
    //If You want used university class add this join {lmsdata_class} yc on yc.course = c.id and yc.year = :year and yc.term = :term 
    $sql = "select mo.name , c.id as courseid , c.startdate , c.format , u.id as userid ,cm.id as cmid , count(logs.id) as write_count  
from {course} c 
join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
join {role_assignments} ra on ra.contextid = ct.id 
join {user} u on u.id = ra.userid  
join {course_modules} cm on cm.course = c.id 
join {modules} mo on mo.id = cm.module and (mo.name = 'assign' or mo.name = 'choice' or mo.name = 'quiz') 
join {logstore_standard_log} logs on logs.contextlevel = 70 and logs.contextinstanceid = cm.id and logs.courseid = c.id and logs.component = concat('mod_',mo.name) and (logs.action = 'submitted' or logs.action = 'started') and logs.userid = u.id and logs.timecreated >= :yesterday and logs.timecreated < :today 
group by mo.name, c.id, c.startdate , c.format ,u.id , cm.id";
    $param = array('yesterday' => $yesterday, 'today' => $today);
    $write_datas = $DB->get_recordset_sql($sql, $param);
    foreach ($write_datas as $write_data) {

        $weekstarts = array();
        $weekstarts[] = strtotime('last sunday', $write_data->startdate);

        $current_section = 0;

        $section_cnt = $DB->get_field('course_format_options', 'value', array('courseid' => $write_data->courseid, 'format' => $write_data->format, 'name' => 'numsections'));

        for ($i = 1; $i <= $section_cnt; $i++) {
            $weekstarts[$i] = strtotime('next sunday', $weekstarts[$i - 1]);
            if ($weekstarts[$i - 1] <= $cron_start && $weekstarts[$i] > $cron_start) {
                $current_section = $i;
            }
        }

        $data = new stdClass();
        $data->courseid = $write_data->courseid;
        $data->userid = $write_data->userid;
        $data->write_count = $write_data->write_count;
        $data->section = $current_section;
        $data->modname = $write_data->name;
        $data->cmid = $write_data->cmid;
        unset($log);
        $sql = "select * from {lmsdata_log} where courseid = :courseid and userid = :userid and modname = :modname and cmid = :cmid and write_count != 0 and section = :section";
        $log = $DB->get_record_sql($sql, array('courseid'=>$data->courseid,'userid'=> $data->userid,'modname'=>$data->modname,'cmid'=>$data->cmid,'section'=>$current_section));
        if ($log && $log->id) {
            $data->id = $log->id;
            $data->write_count = $data->write_count + $log->write_count;
            $DB->update_record('lmsdata_log', $data);
        } else {
            $DB->insert_record('lmsdata_log', $data);
        }
        echo 'Run...\r\n';
    }
      
    $cron_end = time();
    echo $yesterday.'-'.$today;
    $DB->insert_record('lmsdata_log_cron', array('starttime' => $today, 'endtime' => $cron_end));
    echo 'End';
} else {
    error_log(date("Y-m-d h:i:s 중복된 크론 실행", time()) . "\r\n", 3, 'cron_error.txt');
}
