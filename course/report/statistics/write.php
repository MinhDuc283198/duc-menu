<?php

require('../../../config.php');
require_once $CFG->dirroot . '/course/report/statistics/lib.php';
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT); // course id
$excell = optional_param('excell', 0, PARAM_INT);

require_login();

$context = context_course::instance($id);
$PAGE->set_context($context);

$course = get_course($id);
$PAGE->set_url('/course/report/statistics/write.php?id='.$id);
$PAGE->set_course($course);
$PAGE->set_pagelayout('incourse');
$strplural = get_string("pluginname", "coursereport_statistics") . " - " . get_string('board_period_write','coursereport_statistics');

$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);


$sql = "select u.* from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "where ra.contextid = :contextid order by u.username asc";
$users = $DB->get_records_sql($sql, array('contextid' => $context->id, 'rolename' => 'student'));


$section_cnt = $DB->get_field('course_format_options', 'value', array('courseid' => $id, 'format' => $course->format, 'name' => 'numsections'));


$weekstarts = array();
$weekstarts[] = strtotime('last sunday', $course->startdate);
for ($i = 1; $i <= $section_cnt; $i++) {
    $weekstarts[$i] = strtotime('next sunday', $weekstarts[$i - 1]);
}

if (!$excell) {
    echo $OUTPUT->header();

//tab
    $row[] = new tabobject('board_read', "$CFG->wwwroot/course/report/statistics/index.php?id=" . $id, get_string('board_period_read','coursereport_statistics'));
    $row[] = new tabobject('board_write', "$CFG->wwwroot/course/report/statistics/write.php?id=" . $id, get_string('board_period_write','coursereport_statistics'));
    $row[] = new tabobject('type_board_read', "$CFG->wwwroot/course/report/statistics/board_read_type.php?id=" . $id, get_string('board_read_by_type','coursereport_statistics'));
    $row[] = new tabobject('type_board_write', "$CFG->wwwroot/course/report/statistics/board_write_type.php?id=" . $id, get_string('board_write_by_type','coursereport_statistics'));
    $row[] = new tabobject('board_comment', "$CFG->wwwroot/course/report/statistics/board_comment.php?id=" . $id, get_string('board_comment_read_by_type','coursereport_statistics'));
    $rows[] = $row;
    $currenttab = 'board_write';
    print_tabs($rows, $currenttab);
   
    echo '<div style="width:100%; overflow-y:hidden; overflow-x:scroll;white-space:nowrap;">';
    echo '<table><tr><th width=10%>'.get_string('user:name','coursereport_statistics').'</th><th width=10%>'.get_string('user:hakbun','coursereport_statistics').'</th>';
    foreach ($weekstarts as $key => $value) {
        $current_week = date("Y-m-d", $value);
        $next_week = ($key >= $section_cnt) ? "" : date("Y-m-d", strtotime('next sunday', $value));
        $weekstr = get_string('weeks','coursereport_statistics',($key + 1));
        $th = str_replace(get_string('weeks','coursereport_statistics',($section_cnt + 1)), get_string('etc','coursereport_statistics'), '<th><span title="' . $current_week . '~' . $next_week . '">' . $weekstr."</span></th>");
        echo $th;
    }
    echo "</tr><tr>";
    if (has_capability('coursereport/statistics:isteacher', $context)) {
        foreach ($users as $user) {
            echo "<td>" . fullname($user)."</td>";
            echo "<td>" . $user->username."</td>";
            foreach ($weekstarts as $key => $value) {
                $next_week = ($key >= $section_cnt) ? 9421593200 : strtotime('next sunday', $value);
                echo "<td>" . (get_board_writes($id, 1, $user->id, $value, $next_week) + get_board_writes($id, 2, $user->id, $value, $next_week) + get_jinoforum_writes($id, $user->id, $value, $next_week)) . "</td>";
            }
            echo "</tr>";
        }
    } else if (has_capability('coursereport/statistics:isstudent', $context)) {
        $user = $USER;
        echo "<td>" . fullname($user)."</td>";
        echo "<td>" . $user->username."</td>";
        foreach ($weekstarts as $key => $value) {
            $next_week = ($key >= $section_cnt) ? 9421593200 : strtotime('next sunday', $value);
            echo "<td>" . (get_board_writes($id, 1, $user->id, $value, $next_week) + get_board_writes($id, 2, $user->id, $value, $next_week) + get_jinoforum_writes($id, $user->id, $value, $next_week)) . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
    echo "</div></br>";
    $url = "write.php?id=$id&excell=1";
    echo '<button onclick="location.href=\'' . $url . '\'" class="red-form">'.get_string('download').'</button>';
    echo $OUTPUT->footer();
} else {
    $name = get_string('name');
    $fields = array(get_string('user:name','coursereport_statistics'), get_string('user:hakbun','coursereport_statistics'));
    foreach ($weekstarts as $key => $value) {
        $current_week = date("Y-m-d", $value);
        $next_week = ($key >= $section_cnt) ? "" : date("Y-m-d", strtotime('next sunday', $value));
        $weekstr = get_string('weeks','coursereport_statistics',($key + 1));
        $fields[] = str_replace(get_string('weeks','coursereport_statistics',($section_cnt + 1)), get_string('etc','coursereport_statistics'),  $weekstr);
    }

    $filename = 'boardwrite_statistics_'.$id.'.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    if (has_capability('coursereport/statistics:isteacher', $context)) {
    foreach ($users as $user) {
        $col = 0;
        $worksheet[0]->write($row, $col++, fullname($user));
        $worksheet[0]->write($row, $col++, $user->username);
        foreach ($weekstarts as $key => $value) {
            $next_week = ($key >= $section_cnt) ? 9421593200 : strtotime('next sunday', $value);
            $worksheet[0]->write($row, $col++, (get_board_writes($id, 1, $user->id, $value, $next_week) + get_board_writes($id, 2, $user->id, $value, $next_week) + get_jinoforum_writes($id, $user->id, $value, $next_week)));
        }
        $row++;
    }
    } else if (has_capability('coursereport/statistics:isstudent', $context)) {
        $user = $USER;
         $col = 0;
        $worksheet[0]->write($row, $col++, fullname($user));
        $worksheet[0]->write($row, $col++, $user->username);
        foreach ($weekstarts as $key => $value) {
            $next_week = ($key >= $section_cnt) ? 9421593200 : strtotime('next sunday', $value);
            $worksheet[0]->write($row, $col++, (get_board_writes($id, 1, $user->id, $value, $next_week) + get_board_writes($id, 2, $user->id, $value, $next_week) + get_jinoforum_writes($id, $user->id, $value, $next_week)));
        }
    }
    $workbook->close();
    die;
}
?>
