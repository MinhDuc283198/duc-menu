<?php

define('COURSEREPORT_STATICS_PROGRESS_GRADE_ITEM_IDNUMBER', 'oklass_mediaandlcms_progress');

function statistics_report_extend_navigation($reportnav, $course, $context) {
 //   $url = new moodle_url('/course/report/statistics/index.php', array('id' => $course->id));
 //   $reportnav->add(get_string('statistics', 'coursereport_statistics'), $url);
}

/**
 * 
 * @global type $DB
 * @param type $courseid
 * @param type $case
 * @param type $userid
 * @param type $current_section
 * @param type $sec_data
 */
function get_board_reads($courseid, $userid, $sec) {
    global $DB;
    $sql = "select sum(read_count) as cnt from {lmsdata_log} where courseid = :courseid and userid = :userid and modname in ('jinoforum','jinotechboard','teamboard') and section = :sec";
    $param = array('courseid' => $courseid, 'userid' => $userid , 'sec' =>$sec);
    $cnt = $DB->get_record_sql($sql , $param);
    return (!empty($cnt->cnt))?$cnt->cnt:0;
}


function get_board_writes($courseid, $case, $userid, $value, $next_week) {
    global $DB;
    switch ($case) {
        case 1 : $table = "jinotechboard";
            break;
        case 2 : $table = "teamboard";
            break;
    }

    $sql = "select count(*) "
            . "from {" . $table . "_contents} w "
            . "join {" . $table . "} b on  b.id = w.board and b.course = :courseid "
            . "where w.userid = :userid and w.timecreated >= :sec_start and w.timecreated < :sec_end";
    $param = array('courseid' => $courseid, 'userid' => $userid,'sec_start'=>$value, 'sec_end'=>$next_week);

    return $DB->count_records_sql($sql, $param);
}
function get_jinoforum_writes($courseid, $userid, $value, $next_week) {
    global $DB;
    
    $sql = "select count(*) "
            . "from {jinoforum_posts} w "
            . "join {jinoforum_discussions} d on d.id = w.discussion "
            . "join {jinoforum} f on f.id = d.jinoforum and f.course = :courseid "            
            . "where w.userid = :userid and w.created >= :sec_start and w.created < :sec_end";
    $param = array('courseid' => $courseid, 'userid' => $userid,'sec_start'=>$value, 'sec_end'=>$next_week);
    return $DB->count_records_sql($sql, $param);
}

function coursereport_get_paging_bar($url, $params, $total_pages, $current_page, $max_nav = 10) {
    
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }
    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = "page=";
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . "?page=";
    }

    echo '<div class="board-breadcrumbs" >';

    if ($current_page > 1) {
        echo '<span class="board-nav-prev"><a class="prev" href="' . $url . ($current_page - 1) . '"><</a></span>';
    } else {
        echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    echo '<ul>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="current"><a href="#">' . $i . '</a></li>';
        } else {
            echo '<li><a href="' . $url . '' . $i . '">' . $i . '</a></li>';
        }
    }
    echo '</ul>';
    if ($current_page < $total_pages) {
        echo '<span class="board-nav-next"><a class="next" href="' . $url . ($current_page + 1) . '">></a></span>';
    } else {
        echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
    }

    echo '</div>';
}
