<?php
$string['pluginname'] = "강의 통계";
$string['statistics'] = "강의 통계";

$string['statistics:activities'] = "학습현황";
$string['statistics:board'] = "게시판 통계";


$string['board_period_read'] = '기간별 읽기';
$string['board_period_write'] = '기간별 쓰기';
$string['board_read_by_type'] = '게시판별 읽기'; 
$string['board_write_by_type'] = '게시판별 쓰기';
$string['board_comment_read_by_type'] = '게시판별 댓글 쓰기';

$string['weeks'] = '{$a} 주차';
$string['etc'] = '기타';
$string['user:name'] = '이름';
$string['user:hakbun'] = '아이디';

$string['stat_course'] = '강의실 운영 현황';

$string['student'] = '학습자';
$string['auditor'] = '청강생';
$string['sectioncnt'] = '일차/토픽 수';
$string['lcmscnt'] = '동영상 수';
$string['modcnt'] = '학습 활동/자원 수';

$string['stat_edu'] = '학습 현황';
$string['my_edu'] = '나의 학습 현황';
$string['my_edu1'] = '나의 진도 현황';

$string['searchplaceholder'] = '아이디/이름을 입력하세요.'; 

$string['all'] = '전체';
$string['view'] = '조회';
$string['submit'] = '제출';
$string['write'] = '쓰기';
$string['comment'] = '댓글';
$string['no'] = '번호';
$string['name'] = '이름';
$string['usernumber'] = '아이디';

$string['lcms_edu'] = '강의운영 현황';
$string['lcms_edu1'] = '동영상 학습현황';

$string['nouser'] = '등록된 학습자가 없습니다.';
$string['noauth'] = '권한이 없습니다.';

$string['studyprogress'] = '학습 진도율';
$string['progress:alert1'] = '성적에 반영하시겠습니까?';
$string['progress:alert2'] = '{$a->count} 명의 사용자를 처리하였습니다.';

$string['exceldownload'] = '엑셀다운로드';
