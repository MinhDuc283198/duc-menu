<?php

$string['pluginname'] = "Course Statistics";
$string['statistics'] = "Course Statistics";

$string['statistics:activities'] = "Activities Statistics";
$string['statistics:board'] = "Board Statistics";

$string['board_period_read'] = 'Read(Period)';
$string['board_period_write'] = 'Write(Period)'; 
$string['board_read_by_type'] = 'Read';
$string['board_write_by_type'] = 'write';
$string['board_comment_read_by_type'] = 'Comments write';
$string['weeks'] = '{$a} Weeks';
$string['etc'] = 'etc';
$string['user:name'] = 'Name';
$string['user:hakbun'] = 'Student No';


$string['stat_course'] = 'Course Education Report';

$string['student'] = 'Student';
$string['auditor'] = 'Auditor';
$string['sectioncnt'] = 'Section Count';
$string['lcmscnt'] = 'Content Count';
$string['modcnt'] = 'Module Count';

$string['stat_edu'] = 'Education Report';
$string['my_edu'] = 'My Educations';
$string['my_edu1'] = 'My Progress';

$string['searchplaceholder'] = 'Enter Student number or name'; 

$string['all'] = 'All';
$string['view'] = 'View';
$string['submit'] = 'Submit';
$string['write'] = 'Write';
$string['comment'] = 'Reply';
$string['no'] = 'No.';
$string['name'] = 'Name';
$string['usernumber'] = 'Student Number';

$string['lcms_edu'] = 'Contents Report';
$string['lcms_edu1'] = 'Progress Report';

$string['nouser'] = 'Empty User';
$string['noauth'] = 'No auth';

$string['studyprogress'] = 'Study Progress';
$string['progress:alert1'] = 'Will you reflect on grades?';
$string['progress:alert2'] = '{$a->count} users have been processed.';

$string['exceldownload'] = 'Excel download';