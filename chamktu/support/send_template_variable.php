<?php 
/**
 * 발송템플릿 양식 다운로드
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/phpexcel/PHPExcel.php');
require_once($CFG->libdir.'/phpexcel/PHPExcel/IOFactory.php');

if (!is_siteadmin($USER)) {
    redirect($CFG->wwwroot);
}

$filename = 'send_template_variable.xlsx';
$filepath = 'send_template_variable.xlsx';

$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
    
}

$maxRow = $objWorksheet->getHighestRow();

for ($i = 2; $i <= $maxRow; $i++) {
    $varname  = $objWorksheet->getCell('D' . $i)->getValue();
    if(!$DB->get_record('lmsdata_sendvariable', array('varname'=>trim($varname)))){
        $data = new stdClass();
        $data->tbname = $objWorksheet->getCell('A' . $i)->getValue();
        $data->fieldname  = $objWorksheet->getCell('B' . $i)->getValue();
        $data->fieldtype = $objWorksheet->getCell('C' . $i)->getValue();
        $data->varname  = $varname;
        $data->description = $objWorksheet->getCell('E' . $i)->getValue();

        print_object($data);
        
        $DB->insert_record('lmsdata_sendvariable',$data);
    
        echo $varname . '등록 완료\n';
    }else{
        echo $varname . '이미 등록되어있음.\n';
    }


}

echo '전체 등록 완료';
