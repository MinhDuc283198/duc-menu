<?php
/**
 * 발송템플릿 관리
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once("$CFG->libdir/excellib.class.php");
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/support/send_template.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';

$search_val = optional_param('search_val', '', PARAM_RAW);
$type = optional_param('type', '', PARAM_RAW);
$classtype2 = optional_param('classtype', '', PARAM_RAW);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

$PAGE->set_context($context);
$PAGE->set_url('/chamktu/support/send_template.php');

$query = "SELECT * from {lmsdata_sendtemplate} ";

$conditions = array();
$params = array();

$value = $search_val;

if($type){
    $conditions[] = 'type = :type';
    $params['type'] = $type;
}

if($classtype2){
    $conditions[] = 'classtype = :classtype';
    $params['classtype'] = $classtype2;
}

if($search_val) {   
    //$value1 = $value;
    $value2 = $value;
    //$conditions[] = $DB->sql_like('contents',':value1');
    $conditions[] = $DB->sql_like('title',':value2');
    //$params['value1'] = '%'.$value1.'%';
    $params['value2'] = '%'.$value2.'%';    
}

$where = '';
if(!empty($conditions)) {
    $where = ' WHERE '.implode(' AND ', $conditions);
}

$sort = ' order by id desc';

$totalcount = $DB->count_records_sql("SELECT count(*) from {lmsdata_sendtemplate} ".$where.$sort, $params);
$datas = $DB->get_records_sql($query.$where.$sort, $params, ($currpage - 1) * $perpage, $perpage);
$datas1 = $DB->get_records_sql($query.$where.$sort, $params);
if (!$excel) {
?>
<?php include_once('../inc/header.php'); ?>
<div id="contents">
    <?php include_once('../inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title">발송템플릿관리</h3>
        <div class="page_navbar"><a href="./evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > <strong>발송템플릿관리</strong></div>
        
        <form name="search_form" class="search_area" id="course_search">
            <input type="hidden" name="page" value="1" />
<!--            <select name="type">
                <option value="">종류선택</option>
                <option value="sms" <?php echo ($type == 'sms')? 'selected':'';?>>SMS</option>
                <option value="email" <?php echo ($type == 'email')? 'selected':'';?>>email</option>
            </select>-->
            <select name="classtype">
                <option value="">구분선택</option>
                <option value="0" <?php echo ($classtype2 == 0)? 'selected':'';?>>전체</option>
                <option value="1" <?php echo ($classtype2 == 1)? 'selected':'';?>>필수과정</option>
                <option value="2" <?php echo ($classtype2 == 2)? 'selected':'';?>>선택과정</option>
                <option value="3" <?php echo ($classtype2 == 3)? 'selected':'';?>>자격인증</option>                
            </select>
            <input type="text" name="search_val" value="<?php echo $value;?>" class="w_300" placeholder="제목 검색"/>
            <input type="submit" class="blue_btn" id="search" value="검색"/>
            <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
            </div>
        </form> <!-- Search Form End -->
            <div style="float:left;">
                <input type="button" class="" value="발송내역" onclick="javascript:location.href = 'send_template_list.php?type=1';"/> 
            </div>
<!--            <div style="float:right;">
                <input type="hidden" value="0" name="excel" >
                <a href="send_template.php?excel=1"><input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>"/></a>                  
            </div>-->
        <table cellspacing="0" cellpadding="0">
            <tr>
                <th style="width:5%;"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th style="width:10%;"><?php echo get_string('contents_kinds','local_lmsdata'); ?></th>
                <th style="width:10%;"><?php echo get_string('gubun','local_lmsdata'); ?></th>
                <th><?php echo get_string('target', 'local_lmsdata'); ?></th>
                <th><?php echo get_string('sms:subject', 'local_lmsdata'); ?></th>
                <th style="width:10%;"><?php echo get_string('sms:sendtime','local_lmsdata'); ?></th>
                <th style="width:10%;"><?php echo get_string('isused', 'local_lmsdata'); ?></th>
            </tr>
            
            <?php
                if($totalcount>0){
                    $startnum = $totalcount - ($currpage - 1) * $perpage;
                    $count = 0;
                    foreach($datas as $data){
                        
                        $sendtimes = array('수동','수강신청','교육시작일로부터','교육종료일로부터', '교육종료일로부터','평가시작일로부터','평가종료일로부터','이의제기신청일로부터');
                        $sendtime = $sendtimes[$data->sendtime]; 
                        
                        $sendtime .= ($data->sendtime != 0 && $data->sendtime != 1 && $data->sendtime != 3)? '<br/>'.$data->senddate.'일후':'';
                        $sendtime .= ($data->sendtime == 3)? '<br/>'.$data->senddate.'일전':'';
                        
                        $classtypes = array('전체','필수과정','선택과정','자격인증');
                        $classtype = $classtypes[$data->classtype]; 
                        
                        $isused = ($data->isused)? get_string('siteadmin_use','local_lmsdata') : get_string('siteadmin_nouse','local_lmsdata');

                        echo '<tr>

                        <td class="number">'.$data->id.'</td>
                            
                        <td>'.$data->type.'</td>
                            
                        <td>'.$classtype.'</td>
                            
                        <td>'.$data->sendtarget.'</td>

                        <td style="text-align:left;"><a href="send_template_write.php?id='.$data->id.'&page='.$currpage.'&search_val='.$search_val.'">'.$data->title.'</a></td>

                        <td>'.$sendtime.'</td>

                        <td>'.$isused.'</td>


                        </tr>';
                        $count++;
                    }

                }else{
                    echo '<tr><td colspan="10">발송 템플릿이 없습니다.</td></tr>';
                }
            ?>
        </table>
        
        <div id="btn_area">
            <input type="button" value="템플릿 등록" onclick="location.href = 'send_template_write.php'" class="blue_btn" style="float:right;"/>
        </div>
            <?php
                $page_params = array();
                
                if($search_val) {
                    $page_params['search_val'] = $search_val;
                }
                $page_params['type'] = $type;
                $page_params['classtype'] = $classtype2;

                print_paging_navbar($totalcount, $currpage, $perpage, 'send_template.php', $page_params);
            ?>
            <!-- Breadcrumbs End -->
    </div> <!-- Table Footer Area End -->
</div>

<?php include_once('../inc/footer.php'); 
} else {
$fields = array(
        '번호',
        '종류',
        '구분',
        '대상',
        '제목',
        '발송시간',
        '사용여부'
    );

 $date = date('Y-m-d', time());
    $filename = 'send_template_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
   foreach ($datas1 as $data) {
       $classtypes = array('전체','필수과정','선택과정','자격인증');
       $classtype = $classtypes[$data->classtype]; 
       $sendtimes = array('수동','수강신청','교육시작일로부터','교육종료일로부터(전)','교육종료일로부터(후)','평가시작일로부터','평가종료일로부터','이의제기신청일로부터');
       $sendtime = $sendtimes[$data->sendtime]; 
       $sendtime .= ($data->sendtime != 0 && $data->sendtime != 1 && $data->sendtime != 3)? $data->senddate.'일후':'';
       $sendtime .= ($data->sendtime == 3)? $data->senddate.'일전':'';
        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $data->type);
        $worksheet[0]->write($row, $col++, $classtype);
        $worksheet[0]->write($row, $col++, $data->sendtarget);
        $worksheet[0]->write($row, $col++, $data->title);
        $worksheet[0]->write($row, $col++, $sendtime);
        $worksheet[0]->write($row, $col++, ($data->isused)? get_string('siteadmin_use','local_lmsdata') : get_string('siteadmin_nouse','local_lmsdata'));
        
        $row++;
    }
    
    $workbook->close();
    
    die;
    }
  ?>  
 <script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }

</script>