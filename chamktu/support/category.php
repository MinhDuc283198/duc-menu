<?php //
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/category.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$category_number = optional_param('number', 0, PARAM_INT);

include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('category_manage', 'local_lmsdata'); ?></h3>

        <div class="page_navbar">
            <a href="./evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
            <strong>카테고리관리</strong>
        </div>  
        <div class="main-category-table">
            <table class="generaltable">
                <thead>
                    <tr>
                        <th style="width:5%;">번호</th>
                        <th style="width:10%;">형식</th>
                        <th style="width:16%">이름</th>
<!--                        <th style="">URL</th>-->
                        <!--<th style="width:24%">권한</th>-->
                        <th style="width:10%;">생성일</th>                    
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = 'select * from {lmsdata_category} where depth < 3 order by parent asc , depth asc, step asc';
                    $categorys = $DB->get_records_sql($query);
                    $num = 1;
                    foreach ($categorys as $category) {
                        $lang = $DB->get_field('lmsdata_category_name','name',array('categoryid'=>$category->id,'lang'=> current_language()));
                        switch ($category->type) {
                            case 1: 
                                $link = '';
                                $type_txt = '상위카테고리';
                                $target = '';
                                break;
                            case 2: 
                                $link = (preg_match('/http/i', $category->url))?$category->url:$CFG->wwwroot.$category->url;
                                $type_txt = '하위카테고리';
                                if($category->ispopup == 2){
                                    $target = 'target="_blank"';
                                } else {
                                    $target ='';
                                }
                                break;
                         }
                        ?>
                        <tr>
                            <td><?php echo $num++; ?></td>
                            <td><?php echo $type_txt; ?></td>
                            <td style="text-align:left; padding-left:15px;">
                                <a href="category_add.php?id=<?php echo $category->id; ?>" <?php if($category->depth == 3) echo 'style="padding-left:20px;"'?> title="Edit">
                                <?php 
                                if($category->type == 2){
                                    echo '<span class="child_icon">'.++$category->step.'.</span> '; 
                                } else {
                                ?>
                                <i class="fa <?php echo $category->icon; ?>" aria-hidden="true"></i> <?php } 
                                echo $lang; ?> 
                                </a>
                            </td>
<!--                            <td><?php echo ($category->type == 1) ? '상위 카테고리는 URL이 없습니다.' : $category->url. '<a href="'.$link.'" '.$target.' class="red">[Link]</a>'; ?></td>
                            <td>
                                <?php
//                                $usergroups = $DB->get_records('category_apply', array('categoryid' => $category->id), '', 'usergroup');
//                                foreach ($usergroups as $usergroup) {
//                                    echo '<div class="category_usergroups ' . $usergroup->usergroup . '">' . get_string('role:'.$usergroup->usergroup,'local_lmsdata') . '</div>';
//                                }
                                ?>
                            </td>-->
                            <td><?php echo date('Y-m-d', $category->timecreated); ?></td>
                        </tr>
                    <?php
                        $sql = 'SELECT * FROM {lmsdata_category} where sub_parent=:sub_parent ORDER BY step ASC';
                        if($third_categorys = $DB->get_records_sql($sql, array('sub_parent'=>$category->id))){
                           foreach($third_categorys as $third_category){
                               $lang = $DB->get_field('lmsdata_category_name','name',array('categoryid'=>$third_category->id,'lang'=> current_language()));
                               $link = (preg_match('/http/i', $category->url))?$category->url:$CFG->wwwroot.$category->url;
                                $type_txt = '하위카테고리';
                                if($category->ispopup == 2){
                                    $target = 'target="_blank"';
                                } else {
                                    $target ='';
                                } 
                    ?>
                        <tr> 
                            <td><?php echo $num++; ?></td>
                            <td><?php echo $type_txt; ?></td>
                            <td style="text-align:left; padding-left:15px;">
                                <a href="category_add.php?id=<?php echo $third_category->id; ?>" style="padding-left:20px;" title="Edit">
                                <span class="child_icon"><?php echo ++$third_category->step;?></span>
                                <?php echo $lang; ?>
                                </a>
                            </td>
                            <td><?php // echo $third_category->url. '<a href="'.$link.'" '.$target.' class="red">[Link]</a>'; ?></td>
<!--                            <td>
                                <?php
//                                $usergroups = $DB->get_records('category_apply', array('categoryid' => $third_category->id), '', 'usergroup');
//                                foreach ($usergroups as $usergroup) {
//                                    echo '<div class="category_usergroups ' . $usergroup->usergroup . '">' . get_string('role:'.$usergroup->usergroup,'local_lmsdata') . '</div>';
//                                }
                                ?>
                            </td>-->
                            <td><?php echo date('Y-m-d', $third_category->timecreated); ?></td>
                            
                        </tr>
                    <?php
                           }
                        }
                    }
                    if (!$categorys) {
                        echo '<tr><td align="center" colspan="6">등록된 카테고리가 없습니다</td></tr>';
                    }
                    ?>
                </tbody>
            </table>
        <div id="btn_area">
            <input type="button" value="카테고리 추가" onclick="location.href = 'category_add.php'" class="blue_btn" style="float:right;"/>
        </div>
    </div>
</div><!--Content End-->
</div> <!--Contents End-->

<?php include_once ($CFG->dirroot . '/chamktu/inc/footer.php'); ?>

<script type="text/javascript">

</script>
