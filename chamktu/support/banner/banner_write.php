<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
                    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/banner/banner.php');
    redirect(get_login_url());
}


$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
include_once('../../inc/header.php');

$mod = optional_param('mod', "", PARAM_TEXT);
$temp->itemid = $banner->id;



$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);


?>
  <?php include_once('../../inc/sidebar_support.php');?>
    <div id="content">
        <h3 class="page_title">배너 등록</h3>        
        <div class="page_navbar"><a href="../evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
                                 <a href="./banner.php"><?php echo get_string('banner_manage','local_lmsdata'); ?></a> > 
                                 <strong>배너 등록</strong></div>
    <form action="banner_submit.php" id="banner_submit" enctype="multipart/form-data" method="POST">
        <table>
            <tr>
                <td  style="background-color: #F2F2F2">배너명</td>
                <td style="text-align:left;" colspan="3"><input type="text" name="name" style="width:70%;"></td>
            </tr>
            <tr>
                    <td class="field_title" style="background-color: #F2F2F2">첨부파일</td>
                    <td class="field_value number" colspan="3">                                
                        <?php
                        if ($temp->itemid) {
                            $output = '';
                            $cnt = 0;
                            foreach ($files as $file) {
                                $filename = $file->get_filename();
                                $mimetype = $file->get_mimetype();
                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';                                
                                
                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $id . '/' . $filename);
                                if ($file->get_filesize() > 0) {
                                    $fileobj[$cnt] = '<input type="hidden" name="file_del" value="0"/><img src="'.$path.'">';
                                    $fileobj[$cnt] .= "<div style='float:left;' id ='file'><a href=\"$path\">$iconimage</a>";
                                    $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
                                    $cnt++;
                                }
                            }
                        }
                            for ($n = 0; $n <= 0; $n++) {
                            ?>
                            <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile" id="uploadfile" style=" float:left;"/> 

                                <?php
                                if (isset($fileobj[$n])) {
                                    echo $fileobj[$n];
                                }
                                echo "</div>";
                            }
                            ?>
                             <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                    </td>
             </tr>
             
             <tr>
                 <td  style="background-color: #F2F2F2">링크</td>
                 <td colspan="3" style="text-align:left;"><input type="text" name="linkurl" value="" style="width:70%;"></td>
             </tr>
             <tr>
                 <td  style="background-color: #F2F2F2">순서</td>
                 <td colspan="3" style="text-align:left;"><input type="text" name="target" value=""></td>
             </tr>
             <tr>
                 <td  style="background-color: #F2F2F2">사용여부</td>
                 <td colspan="3" style="text-align:left;">
                     <input type="radio" name="isused" value="1" checked="checked">사용&nbsp;&nbsp;&nbsp; 
                     <input type="radio" name="isused" value="0">미사용
                 </td>
             </tr>
            <input type="hidden" name="mod" value="<?php echo $mod?>">
        </table>
        <div clas="btn_area">
            <a href="<?php echo $CFG->wwwroot.'/chamktu/support/banner/banner.php';?>"><input type="button" id="notice_list" class="blue_btn" value="목록" style="float: left; border: 1px solid #999" /></a>

            <input type="submit" id="add_user" class="blue_btn" value="저장" style="float: right; margin: 0 10px 0 0" />

        </div> <!-- Bottom Button Area -->
    </form>
    </div> 
    <?php include_once ('../../inc/footer.php'); ?>