<?php
require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/board/board_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/support/banner/lib.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$banner = lmsdata_banner_get_banner_content($id);

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
$fileobj = lmsdata_banner_get_file($context->id, 'banner', $id, 1);
$temp->itemid = $banner->id;

include_once('../../inc/header.php');
?>
<div id="contents">
    <?php include_once('../../inc/sidebar_support.php');?>
    <div id="content">
        <h3 class="page_title">배너 수정</h3>
        <div class="page_navbar"><a href="./notice.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > <a href="./banner.php"><?php echo get_string('banner_management','local_lmsdata'); ?></a></div>
        
        <form action="banner_submit.php" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="mod" value="edit"/>
            <table class="detail">
                <colgroup>
                    <col width="200px" />
                    <col width="/" />
                </colgroup>
                <tr>
                    <td class="field_title">배너명</td>
                    <td class="field_value">
                        <input type="text" name="name" value="<?php echo $banner-> name;?>"
                    </td>
                </tr>
                <tr>
                    <td class="field_title">첨부파일</td>
                    <td class="field_value">
                        <?php echo $fileobj ?>
                        <?php echo $banner-> filename;?>
                        <input type="file" name="uploadfile" id="uploadfile"/> 
                        <input type="hidden" class="" name="file_id" value="<?php
                        if ($temp->itemid > 0) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                        ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="field_title">링크</td>
                    <td class="field_value">
                        <input type="text" name="linkurl" value="<?php echo $banner-> linkurl;?>"
                    </td>
                </tr>
                <tr>
                    <td class="field_title">순서</td>
                    <td class="field_value"><input type="text" name="target" value="<?php echo $banner-> target;?>"></td>
                </tr>
                <tr>
                    <td class="field_title">사용여부</td>
                    <td class="field_value">
                        <?php if ($banner->isused == 1) { ?>
                        <input type="radio" name="isused" value="1" checked="checked">사용&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0">미사용</td>
                            <?php 
                            } else {?>
                        <input type="radio" name="isused" value="1" >사용&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0" checked="checked">미사용</td>
                   </td>
                </tr>
                            <?php }?>            
            </table>
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <div id="btn_area">
                        <input type="submit" id="add_user" class="blue_btn btn-area-right" value="저장" />
                        <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
            </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->  

<?php include_once ('../../inc/footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#banner_list').click(function () {
            location.href = "./banner.php";
        });
    });
</script>