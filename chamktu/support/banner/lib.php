<?php

function lmsdata_banner($page,$perpage){
    global $DB; 
                $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    $sql="select * from {lmsdata_banner} lb
          left join (SELECT itemid FROM {files} WHERE component = 'local_lmsdata' AND filesize >0) f 
          ON lb.id = f.itemid
          order by isused desc , target desc, id desc";
    
return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function lmsdata_banner_get_banner_count() {
    global $DB;    
    $params = array();

        
 
    return $DB->count_records_select("lmsdata_banner", $params);
}

function print_paging_navbar($totalcount, $page, $perpage, $baseurl, $params = null, $maxdisplay = 18) {
    global $CFG;
    $pagelinks = array();
   
    $lastpage = 1;
    if($totalcount > 0) {
        $lastpage = ceil($totalcount / $perpage);
    }
   
    if($page > $lastpage) {
        $page = $lastpage;
    }
           
    if ($page > round(($maxdisplay/3)*2)) {
        $currpage = $page - round($maxdisplay/2);
        if($currpage > ($lastpage - $maxdisplay)) {
            if(($lastpage - $maxdisplay) > 0){
                $currpage = $lastpage - $maxdisplay;
            }
        }
    } else {
        $currpage = 1;
    }
   
   
   
    if($params == null) {
        $params = array();
    }
   
    $prevlink = '';
    if ($page > 1) {
        $params['page'] = $page - 1;
        $prevlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/>', array('class'=>'next'));
    } else {
        $prevlink = '<a href="#" class="next"><img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/></a>';
    }
   
    $nextlink = '';
     if ($page < $lastpage) {
        $params['page'] = $page + 1;
        $nextlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/>', array('class'=>'prev'));
    } else {
        $nextlink = '<a href="#" class="prev"><img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/></a>';
    }
   
   
    echo '<div class="pagination">';
   
    $pagelinks[] = $prevlink;
   
    if ($currpage > 1) {
        $params['page'] = 1;
        $firstlink = html_writer::link(new moodle_url($baseurl, $params), 1);
       
        $pagelinks[] = $firstlink;
        if($currpage > 2) {
            $pagelinks[] = '...';
        }
    }
   
    $displaycount = 0;
    while ($displaycount <= $maxdisplay and $currpage <= $lastpage) {
        if ($page == $currpage) {
            $pagelinks[] = '<strong>'.$currpage.'</strong>';
        } else {
            $params['page'] = $currpage;
            $pagelink = html_writer::link(new moodle_url($baseurl, $params), $currpage);
            $pagelinks[] = $pagelink;
        }
       
        $displaycount++;
        $currpage++;
    }
   
    if ($currpage - 1 < $lastpage) {
        $params['page'] = $lastpage;
        $lastlink = html_writer::link(new moodle_url($baseurl, $params), $lastpage);
       
        if($currpage != $lastpage) {
            $pagelinks[] = '...';
        }
        $pagelinks[] = $lastlink;
    }
   
    $pagelinks[] = $nextlink;
   
   
    echo implode('&nbsp;', $pagelinks);
   
    echo '</div>';
}

function lmsdata_banner_get_banner_content($id){
    global $DB;
    
    return $DB->get_record('lmsdata_banner',array('id'=>$id));
    
}

function lmsdata_banner_get_file($contextid, $filearea, $itemid=0, $type, $on = '') {
    global $CFG, $OUTPUT;
    
    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_lmsdata', $filearea, $itemid, "", false);
    
    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_lmsdata/banner/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            if($type == 1){
            $fileobj = '<img class="small '.$on.'" src="'.$path.'">';//클래스가 small 이거나 on일때 이미지 경로 나오게 
            }
        }
    }
    return $fileobj;
}

/*
 * 받은 파라미터로 studytype을 리턴한다
 * classtype : lmsdata_course classtype을 파라미터로 받음 
 * return : $studytype
 */
function studytype ($classtype) {
    
    $classtypes = explode(',',$classtype);
    $studytype = '';

    foreach ($classtypes as $classtype) {
        if($classtype == 1) {
            $studytype .= ', 영재원';
        } else if ($classtype == 2) {
            $studytype .=', 일반인';
        } else if ($classtype == 3) {
            $studytype .=', 영재센터';
        }
    }
    $studytype = trim(substr($studytype, 1));
    
    return $studytype;
}