<?php
/*
 * 코드 등록 기능
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$mod = optional_param('mod', "", PARAM_TEXT);
$id = optional_param('id', "", PARAM_TEXT);
$category = get_code_category();


include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php
            if ($category->depth == 1 || $mod == "top") {
                echo '대분류 등록';
            } else {
                echo '소분류 등록';
            }
            ?></h3>

        <div class="page_navbar">
            <a>운영관리</a> > 
            <a href="<?php echo $CFG->wwwroot . '/chamktu/support/code_manage.php'; ?>" >코드관리</a> > 
            <strong><?php
                if ($category->depth == 1 || $mod == "top") {
                    echo '대분류 등록';
                } else {
                    echo '소분류 등록';
                }
                ?>
            </strong>
        </div>
        <div>
            <form id="frm_code_submit" name="f" action="code_manage_submit.php">

                <table>
                    <tr>
                        <td bgcolor="#f2f2f2">코드분류</td><?php if ($mod == 'under') {
                    ?>   
                            <td style="text-align:left">
                                <select id="parentid" name="parentid" onclick="code_select()">
                                    <?php
                                    foreach ($category as $cate) {
                                    ?>
                                    <option value="<?php echo $cate->id ?>"><?php echo $cate->code_category ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        <input type="hidden" name="code_category" id="code_category" value="">
                        <input type="hidden" name="depth" value="2">
                    <?php } else {
                        ?><td style="text-align:left"><input type="text" id="code_category" name="code_category" size="10" maxlength="10" required="required">--->영문10자이내로 작성하세요. 예)grades</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td bgcolor="#f2f2f2">코드</td><td style="text-align:left"><input type="text" id="code" name="code" size="10" maxlength="10" required="required">--->동일분류에서는 중복불가, 영문10자이내로 작성하세요 예)010</td>
                    </tr>
                    <tr>
                        <td bgcolor="#f2f2f2">코드명</td><td style="text-align:left"><input type="text" name="codename" size="50" maxlength="100">예)학점</td>
                    </tr>
                    <tr>
                        <td bgcolor="#f2f2f2">영문코드명</td><td style="text-align:left"><input type="text" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');" name="english_codename" size="50" maxlength="100">예)grades</td>
                    </tr>
                    <tr>
                        <?php
                        if ($mod == 'top') {
                            echo '<td bgcolor="#f2f2f2">순서</td><td style="text-align:left"><input type="text" name="step" size="1" value="0" maxlength="10" readonly="readonly"></td><input type="hidden" name="depth" value="1">';
                        } else {
                            ?>
                            <td bgcolor="#f2f2f2">순서</td><td style="text-align:left"><input type="text" name="step" size="1" value="1" maxlength="10"></td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" name="status" value="1" checked="checked">승인&nbsp;&nbsp;&nbsp; <input type="radio" name="status" value="0">불가</td>
                    </tr>
                    <input type="hidden" name="mod" value="add">                
                </table>
                <div id="btn_area">
                    <input type="submit" id="code_submit"  class="blue_btn" value="<?php echo get_string('save', 'local_lmsdata'); ?>" style="float:right" /> 
                    <input type="button" id="code_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                </div><!--Btn Area End-->        
            </form>
        </div>
    </div>
</div>
<script>
    var target = document.getElementById("parentid");
    var cc = target.options[target.selectedIndex].text
    $('#code_category').val(cc);


    $(document).ready(function () {
        $('#code_list').click(function () {
            location.href = "./code_manage.php";
        });
        $('#code_submit').click(function () {
            $('#frm_code_submit').submit();
        });


        //  $("#parentid").val($("#code_category option:selected").val());



        $('#frm_code_submit').submit(function (event) {
            var title = $("#code_category").val();
            if (title.trim() == '') {
                alert("코드분류 입력해 주세요");
                return false;
            }
            ;

            var content = $("#code").val();
            if (content.trim() == '') {
                alert("코드를 입력해 주세요");
                return false;
            }
            ;

        });
    });
    /**
     * 소분류 등록시 순서 리턴
     * @returns {undefined}
     */
    function code_select(){        
        var parentid = $("select[name=parentid]").val();

        $.ajax({
            url: 'code_manage_ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                parentid: parentid
            },
            success: function (data, textStatus, jqXHR) {
                $("input[name='step']").val(data.codecount);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }
</script>