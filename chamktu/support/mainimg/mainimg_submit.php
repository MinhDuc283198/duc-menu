<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

if (!isloggedin()) {
    echo '<script type="text/javascript">
            alert("로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.");
            document.location.href="' . $SITECFG->wwwroot . '/main/user/login.php";
        </script>';
}

$id = optional_param('id', 0, PARAM_INT);
$name = required_param('name', PARAM_TEXT);
$mod = optional_param('mod', "", PARAM_TEXT);
$totalpage = optional_param('totalpage', 0, PARAM_TEXT);
$file_id = optional_param('file_id', 0, PARAM_INT);
$file_del = optional_param('file_del', 0, PARAM_INT);
$isused = optional_param('isused', 0, PARAM_INT);
$target = optional_param('target', 0, PARAM_INT);

$newdata = new object();

$newdata->id = $id;
$newdata->name = $name;
$newdata->totalpage = $totalpage;
$newdata->target = $target;
$newdata->isused = $isused;

$context = context_system::instance();
if ($mod == "edit") {
    $newdata->id = $id;
    $DB->update_record('lmsdata_mainimg', $newdata);

    $itemid = $id;
} else if ($mod == "add") {

   $temp = new stdClass();
   $temp= $DB->insert_record('lmsdata_mainimg', $newdata);
    $id = $temp;
}

if ($file_id != 0) {
    
    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($mod == 'edit' && !($_FILES['uploadfile']['name']==null || $_FILES['uploadfile']['name']=='')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'mainimg', $itemid, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }
 

if (!empty($_FILES['uploadfile']['tmp_name'])) {
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_lmsdata',
                'filearea' => 'mainimg',
                'itemid' => $id,
                'filepath' => '/',
                'filename' => $_FILES['uploadfile']['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
            );
            $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile']['tmp_name']);
        }}

redirect($SITECFG->wwwroot . '/chamktu/support/mainimg/mainimg.php');
?>
