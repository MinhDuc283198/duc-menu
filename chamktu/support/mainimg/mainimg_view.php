<?php
require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/banner/banner.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/support/banner/lib.php');
include_once('../../inc/header.php');

$id = optional_param('id', 0, PARAM_INT);

$banner = $DB->get_record('lmsdata_mainimg', array('id' => $id));

if ($banner->id) {
        //$fileobj = lmsdata_banner_get_file($context->id, 'banner', $id, 1);
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'mainimg', $id, "", false);

    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/mainimg/' . $id . '/' . $filename);
        if ($file->get_filesize() > 0) {
            //if($type == 1){
            $fileobj = '<img class="small '.$on.'" src="'.$path.'">';//클래스가 small 이거나 on일때 이미지 경로 나오게 
            //}
        }
    }
}
?>
<div id="contents">
    <?php include_once ('../../inc/sidebar_support.php'); ?>

    <div id="content">
        <h3 class="page_title">메인 상단이미지 관리</h3>
        <div class="frm_popup">
            <table cellspadding="0" cellspacing="0" class="detail">
                <tr>
                    <td class="field_title">제목</td>
                    <td class="field_value">
                        <?php echo $banner-> name; ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title">첨부파일</td>
                    <td class="field_value">
                            <?php echo $fileobj; ?>
                            <input type="hidden" name="file_del" value="0"/>
                        </td>
                </tr>
                <tr>
                    <td class="field_title">순서</td>
                    <td class="field_value">
                        <?php echo $banner->target; ?></td>
                </tr>
                <tr>
                    <td class="field_title">사용여부</td>
                    <td class="field_value">
                        <?php if ($banner->isused == 1) {
                            echo "사용";
                        } else {
                            echo "미사용";
                        } ?></td>
                </tr>
            </table>
            <div id="btn_area">
                <input type="button" id="banner_write"  class="blue_btn" value="<?php echo get_string('edit', 'local_lmsdata'); ?>" style="float:right" />
                <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
            </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->


<?php include_once ('../../inc/footer.php'); ?>

<script type='text/javascript'>
    $(document).ready(function(){
        $('#banner_list').click(function(){
            location.href = "./mainimg.php";
        });
    $('#banner_write').click(function(){
        location.href = '<?php echo($CFG->wwwroot); ?>/chamktu/support/mainimg/mainimg_edit.php?id=<?php echo($banner->id); ?>';
        });    
    });
</script>   