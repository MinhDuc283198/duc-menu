<?php
require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/banner/banner.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/support/banner/lib.php');
include_once('../../inc/header.php');

$currpage = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$id = optional_param('id', 0, PARAM_INT);
//$totalcount = lmsdata_banner_get_banner_count();
$totalcount = $DB->count_records_select("lmsdata_mainimg", $params);

$offset = 0;
if ($page != 0) {
        $offset = $page * $perpage;
}
$sql="select * from {lmsdata_mainimg} lb
      left join (SELECT itemid FROM {files} WHERE component = 'local_lmsdata' AND filesize >0) f 
      ON lb.id = f.itemid
      order by isused desc , target desc, id desc";
    
    
//$banners = lmsdata_banner($currpage-1,$perpage);
$banners = $DB->get_records_sql($sql, $params, $offset, $perpage);
?>
<div id="contents">
    <?php include_once('../../inc/sidebar_support.php');?>
<div id="content">
    <h3 class="page_title">메인 상단이미지 관리</h3>
    <div class="page_navbar"><a href="../evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > <strong>메인 상단이미지 관리</strong></div>
      <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/chamktu/support/mainimg/mainimg_delete.php'>
          <table>
                <caption class="hidden-caption">메인 상단이미지 관리</caption>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck1" class="chkbox" /></th>
                        <th scope="row" width="5%">No.</th>
                        <th scope="row" width="45%">제목</th>
                        <th scope="row" width="10%">첨부파일</th>
                        <th scope="row" width="10%">사용여부</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $offset = 0;
                    if ($currpage != 0) {
                        $offset = ($currpage - 1) * $perpage;
                    }
                    $num = $totalcount - $offset;
                    if($totalcount){
                    foreach ($banners as $banner) {
                            $fs = get_file_storage();
                            $files = $fs->get_area_files($context->id, 'local_lmsdata', 'mainimg', $banner->itemid, "", false);
                        ?>
                        <tr>
                            <td class="chkbox">
                                <input class="chkbox" type="checkbox" title="check" id="chkbox1" name="bannerid<?php echo $banner->id; ?>" value="<?php echo $banner->id; ?>" />
                            </td>
                            <td class="number">
                                <?php echo $num; ?>
                            </td>   
                            <td>
                                <a href='<?php echo $CFG->wwwroot ?>/chamktu/support/mainimg/mainimg_view.php?mod=edit&id=<?php echo $banner->id ?>'><?php echo $banner -> name?></a>
                            </td>   
                                <?php
                                    if ($banner->id) {
                                    $output = '';
                                    $fileobj = '';
                                    foreach ($files as $file) {
                                        $filename = $file->get_filename();
                                        $mimetype = $file->get_mimetype();
                                        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';                                

                                        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/mainimg/' . $banner->itemid . '/' . $filename);
                                        if ($file->get_filesize() > 0) {
                                            $fileobj = '<img class=\'small\' src="'.$path.'">';

                                        }
                                    }
                                }
                                ?>
                            <td>
                                <?php 
                                  if($banner->id){
                                     echo $fileobj;
                                   }
                                ?>
                            </td>
             
                            <td>
                                    <?php if(($banner->isused)==1){
                                      echo '사용';
                                     }else{
                                       echo '미사용';
                                    }?>         
                            </td>
                        </tr>
                        <?php 
                         $num--;
                         } 
                      }else if (!$totalcount) { ?>
                        <tr><td colspan="6"><span>등록된 메인 상단이미지가 없습니다.</span></td></tr>
                      <?php } ?>
                </tbody>
            </table>
            <div id="btn_area">
                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/chamktu/support/mainimg/mainimg_write.php?mod=add"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>"  />
            </div>
        </form>
    <?php
print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . '/chamktu/support/mainimg/mainimg.php');?>
</div>
</div>
 <?php include_once ('../../inc/footer.php'); ?>
<script>
//     $(function() {
//        $("#allcheck1").click(function() {
//            var chk = $("#allcheck1").is(":checked");`
//
//            if (chk) {
//                $(".chkbox input").each(function() {
//                    this.checked = true;
//                });
//            } else {
//                $(".chkbox input").each(function() {
//                    this.checked = false;
//                });
//            }
//        });
        $(function() {
            $("#allcheck1").click(function() {
            var chk = $("#allcheck1").is(":checked");

            if (chk) {
                $(".chkbox input").each(function() {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function() {
                    this.checked = false;
                });
            }
        });
        
        $("#delete_button").click(function() {
            if (!$(".chkbox").is(":checked")) {
                alert('삭제할 번호의 체크박스를 선택하세요.');
                return false;
            }
            
            if(confirm("<?php echo get_string('delete_confirm', 'local_lmsdata');?>")){
              $('#form').submit();
               }
             });
         });
      
    
    </script>

