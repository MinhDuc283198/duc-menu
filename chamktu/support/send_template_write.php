<?php
/**
 * 발송템플릿 등록
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/sms_write.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);
$search_val = optional_param('search_val', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);

if ($id) {
    if (!$data = $DB->get_record('lmsdata_sendtemplate', array('id' => $id))) {
        redirect('send_template.php');
    }
} else {
    //데이터 없을 경우 미리 선언
    $data = new stdClass();
    $data->type = 'email';
    $data->classtype = 0;
    $data->title = '';
    $data->sendtarget ='학습자';
    $data->sendtime = 0;
    $data->isused = 1;
    $data->contents = '';
    $data->link = '';
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js'
);

include_once('../inc/header.php');
?>
<div id="contents">
<?php include_once('../inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title">발송템플릿 등록</h3>
        <div class="page_navbar"><a href="/chamktu/support/evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > <a href="./send_template.php">발송템플릿관리</a> > <strong>발송템플릿 등록<strong></div>

        <form name="write_form"  method="POST" enctype="multipart/form-data" action="./send_template_submit.php">
            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <input type="hidden" name="page" value="<?php echo $page; ?>"/>
            <input type="hidden" name="search_val" value="<?php echo $search_val; ?>"/>
            <table cellpadding="0" cellspacing="0" class="detail">

                <tbody>
                    <tr>
                        <td class="field_title">제목</td>
                        <td class="field_value">
                            <input type="text" name="title" value="<?php echo $data->title;?>" size="100"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">종류</td>
                        <td class="field_value">
<!--                            <input type="radio" name="type" value="sms" <?php echo ($data->type == 'sms')? 'checked':'';?>/> SMS-->
                            <input type="radio" name="type" value="email" <?php echo ($data->type == 'email')? 'checked':'';?>/> Email
                            <input type="radio" name="type" value="push" <?php echo ($data->type == 'push')? 'checked':'';?>/> Push
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">과정형식</td>
                        <td class="field_value">
                            <input type="radio" name="classtype" value="0" <?php echo ($data->classtype == 0)? 'checked':'';?>/> 전체
                            <input type="radio" name="classtype" value="1" <?php echo ($data->classtype == 1)? 'checked':'';?>/> 필수과정
                            <input type="radio" name="classtype" value="2" <?php echo ($data->classtype == 2)? 'checked':'';?>/> 선택과정
                            <input type="radio" name="classtype" value="3" <?php echo ($data->classtype == 3)? 'checked':'';?>/> 자격인증                            
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">발송시점</td>
                        <td class="field_value">
                            <select name="sendtime">
                                <?php
                                $sendtimes = array('수동', '수강신청일', '교육시작일로부터', '교육종료일로부터(전)', '교육종료일로부터(후)', '평가시작일로부터', '평가종료일로부터','이의제기신청일로부터');
                                foreach ($sendtimes as $key => $val) {
                                    $selected = '';
                                    if($key == $data->sendtime){
                                        $selected = 'selected';
                                    }
                                    echo '<option value="' . $key . '" '.$selected.'>' . $val . '</option>';
                                }
                                ?>                   
                            </select>
                            <span id="id_senddatearea" style="display:none;">
                                <input type="text" name="senddate" value="<?php echo ($data->senddate)? $data->senddate : 0;?>" size="3"/> 일후
                            </span>
                             <span id="id_senddatearea1" style="display:none;">
                                <input type="text" name="senddate" value="<?php echo ($data->senddate)? $data->senddate : 0;?>" size="3"/> 일전
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">발송대상</td>
                        <td class="field_value">
                            <input type="radio" name="sendtarget" value="학습자" <?php echo ($data->sendtarget == '학습자')? 'checked':'';?>/> 학습자
                            <input type="radio" name="sendtarget" value="미이수자" <?php echo ($data->sendtarget == '미이수자')? 'checked':'';?>/> 미이수자
                            <input type="radio" name="sendtarget" value="강사" <?php echo ($data->sendtarget == '강사')? 'checked':'';?>/> 강사
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">사용여부</td>
                        <td class="field_value">
                            <input type="radio" name="isused" value="1" <?php echo ($data->isused == 1)? 'checked':'';?>/> 사용
                            <input type="radio" name="isused" value="0" <?php echo ($data->isused == 0)? 'checked':'';?>/> 사용안함
                        </td>
                    </tr>
                    <tr  id="id_linkarea" style="display:none;">
                        <td class="field_title">link</td>
                        <td class="field_value">
                            <input type="text" name="link" value="<?php echo $data->link;?>" size="100"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">내용</td>
                        <td class="field_value">
                            <textarea style="width: 98%" id="editor" name="contents" ><?php echo $data->contents; ?></textarea>	
                        </td>
                    </tr>
                </tbody>

            </table>

            <div id="btn_area">
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_lmsdata'); ?>" onclick="return update_data();" />
                <input type="button" id="notice_list" class="normal_btn" onclick="location.href = 'send_template.php?page=<?php echo $page; ?>&search_val=<?php echo $search_val; ?>'" value="<?php echo get_string('list2', 'local_lmsdata'); ?>"  />
            </div> <!-- Bottom Button Area -->

        </form>
    </div>
</div>
<?php include_once('../inc/footer.php'); ?>

<script type="text/javascript">
    
    $(document).ready(function () { 
        
//        if($('input[name=type]:checked').val() != 'sms'){
//            if (!CKEDITOR.instances.editor) {
//                call_ckeditor();
//            }
//        }
        /*
        $('input[type=radio][name=type]').on('click', function(){
            $('#id_linkarea').show();
            $('#id_linkarea').hide();
        });
        */
<?php       
if($data->type == "push"){
?>
    $('#id_linkarea').show();
<?php
}
?>

        $('input[type=radio][name=type]').on('click', function(){
         if( ( $('input:radio[name=type][value=push]').is(':checked') ) == true){
             $('#id_linkarea').show();
         }else{
             $('#id_linkarea').hide();
         }
        });
       
        if($('select[name=sendtime] option:selected').val() == 2 || $('select[name=sendtime] option:selected').val() == 4 || $('select[name=sendtime] option:selected').val() == 5 || $('select[name=sendtime] option:selected').val() == 6 || $('select[name=sendtime] option:selected').val() == 7){
                $('#id_senddatearea').show();
                $('#id_senddatearea1').hide();
        }
        else if($('select[name=sendtime] option:selected').val() == 3){
                 $('#id_senddatearea1').show();
                 $('#id_senddatearea').hide();
        }
    
//        $('input[name=type]').click(function(){
//            if($(this).val() != 'sms'){
//                if (!CKEDITOR.instances.editor) {
//                    call_ckeditor();
//                }
//            }else{
//                if (CKEDITOR.instances.editor) {
//                    CKEDITOR.instances.editor.destroy();
//                }
//            }
//        });
//        
        $('select[name=sendtime]').change(function(){
            if($(this).val() == 2 || $(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6 || $(this).val() == 7 ){
                $('#id_senddatearea').show();
                $('#id_senddatearea1').hide();
            }else if($(this).val() == 3){
                 $('#id_senddatearea').hide();
                 $('#id_senddatearea1').show();                 
            }else{
                $('#id_senddatearea').hide();
                $('#id_senddatearea1').hide();
            }
        });
    
    });
    /**
     * 양식 데이터 저장
     * @returns {Boolean}
     */
    function update_data() {
        

        var frm = $('form[name=write_form]');
        if ($.trim(frm.find('input[name=title]').val()) == '') {
            alert('제목을 입력하세요.');
            return false;
        }
        if ($.trim(frm.find('textarea[name=contents]').val()) == '') {
            //alert(frm.find('textarea[name=contents]').val());
            //return false;
        }
    }
    
    function call_ckeditor(){
        var editor = CKEDITOR.replace('editor', {
            language: '<?php echo current_language(); ?>',
            filebrowserBrowseUrl: '../js/ckfinder-2.4/ckfinder.html',
            filebrowserImageBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Flash',
            filebrowserUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKFinder.setupCKEditor(editor, '../');
    }

</script>
