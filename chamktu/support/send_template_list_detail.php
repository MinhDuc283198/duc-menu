<?php
/**
 * 발송내역 상세보기
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/send_template.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);
$seq = optional_param('seq', 0, PARAM_INT);
//검색용 파라미터
//if ($type == 1) {
    $title = 'MAIL';
    $sql_select = "select * ";
    $sql_from = " FROM Balsong_Mail_Tran_Detail bmt join Balsong_Mail_Result bmr on bmt.Result_CD = bmr.Result_CD "; 
    $sql_where = " WHERE bmt.Mail_Tran_Seq = $seq ";
    $sql_orderby = " order by bmt.Seq desc ";    
//}else {
//    $title = 'SMS';
//    $sql_select = "select * ";
//    $sql_from = " FROM Balsong_Mobile ";
//    $sql_orderby = " order by seqno desc ";
//}

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo $title ?>발송내역</h3>
            <div style="float:left;">
                <?php if($type == 1){ ?>
                <input type="button" class="orange_btn" value="메일" id="mail" onclick="javascript:location.href = 'send_template_list.php?type=1';"/>
                <!--<input type="button" class="" value="SMS" id="sms" onclick="javascript:location.href = 'send_template_list.php?type=2';"/>--> 
                <?php }else{?>
                <input type="button" class="" value="메일" id="mail" onclick="javascript:location.href = 'send_template_list.php?type=1';"/>
                <!--<input type="button" class="orange_btn" value="SMS" id="sms" onclick="javascript:location.href = 'send_template_list.php?type=2';"/>--> 
                <?php }?>
            </div>
            <div style="float:right;">
                <a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/send_template_list.php?type=1"><input type="button" class="" value="<?php echo get_string('return', 'local_courselist'); ?>" /></a>
            </div>
    <div>
            <table >
        <?php //if ($type == 1) { ?>
                        <thead>
                            <tr>
                                <th scope="row" width="3%">seq</th>
                                <th scope="row" width="3%">Mail_Tran_Seq</th>
                                <th scope="row" width="5%">Dest_Mail</th>
                                <th scope="row" width="5%">Company_NM</th>
                                <th scope="row" width="5%">Person_NM</th>
                                <th scope="row" width="5%">etc1</th>
                                <th scope="row" width="5%">etc2</th>
                                <th scope="row" width="5%">etc3</th>
                                <th scope="row" width="5%">etc4</th>
                                <th scope="row" width="5%">etc5</th>
                                <th scope="row" width="5%">etc6</th>
                                <th scope="row" width="5%">etc7</th>
                                <th scope="row" width="5%">etc8</th>
                                <th scope="row" width="5%">etc9</th>
                                <th scope="row" width="5%">etc10</th>
                                <th scope="row" width="5%">Result_NM</th>
                            </tr>
                        </thead>
                        <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="16"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                //$startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                        <tr>
                            <td class="field_value"><?php echo $course->seq?></td>
                            <td class="field_value"><?php echo $course->mail_tran_seq?></td>
                            <td class="field_value"><?php echo $course->dest_mail?></td>
                            <td class="field_value"><?php echo $course->company_nm?></td>
                            <td class="field_value"><?php echo $course->person_nm?></td>
                            <td class="field_value"><?php echo $course->etc1?></td>
                            <td class="field_value"><?php echo $course->etc2?></td>
                            <td class="field_value"><?php echo $course->etc3?></td>
                            <td class="field_value"><?php echo $course->etc4?></td>
                            <td class="field_value"><?php echo $course->etc5?></td>
                            <td class="field_value"><?php echo $course->etc6?></td>
                            <td class="field_value"><?php echo $course->etc7?></td>
                            <td class="field_value"><?php echo $course->etc8?></td>
                            <td class="field_value"><?php echo $course->etc9?></td>
                            <td class="field_value"><?php echo $course->etc10?></td>
                            <td class="field_value"><?php echo $course->result_nm?></td>
                        </tr>
                        <?php
                }
            }
            ?>    
        </table><!--Table End-->
    </div><!--Content End-->    
</div> <!--Contents End-->
<?php include_once ('../inc/footer.php'); ?>
