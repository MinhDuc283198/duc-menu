<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/main_menu_add.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);

if ($id) {
    $menu = $DB->get_record('main_menu', array('id' => $id));
}
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_support.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo empty($courseid) ? get_string('add_menu', 'local_lmsdata') : get_string('edit', 'local_lmsdata'); ?></h3>
        <div class="page_navbar"><a href="./evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> >  <a href="./main_menu.php"><?php echo get_string('menu_manage', 'local_lmsdata'); ?></a> > <strong><?php echo empty($courseid) ? get_string('add_menu', 'local_lmsdata') : get_string('edit', 'local_lmsdata'); ?></strong></div>
        <form name="" action="main_menu_add.execute.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="current_step" value="<?php
            if (isset($menu)) {
                echo $menu->step;
            }
            ?>" >
                   <?php
                   if (isset($menu)) {
                       ?>
                <input type="hidden" name="edit" value="<?php echo $menu->id; ?>">
                <?php if ($menu->type == 1 || $menu->type == 2) { ?>
                    <input type="hidden" name="type" value="<?php echo $menu->type; ?>">
                <?php
                }
            }
            ?>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr>
                        <td class="field_title"><span class="required red">*</span> 메뉴명</td>
                        <td class="field_value">
                            <?php
                            $languages = get_string_manager()->get_list_of_translations(); // Get languages for quick search later 
                            /** 한국어가 먼저 나오는 버전 시작 * */
                            $arr_lang = array();
                            foreach ($languages as $k => $v) {
                                $indexnumber = 2;
                                $lang = ($id) ? $DB->get_field('main_menu_name', 'name', array('menuid' => $id, 'lang' => $k)) : '';
                                if ($k == 'ko') {
                                    $arr_lang[1] = '<p>' . '<input type="text" value="' . $lang . '" name="name[' . $k . ']"> ' . $v . '</p>';
                                } else {
                                    $arr_lang[$indexnumber] = '<p>' . '<input type="text" value="' . $lang . '" name="name[' . $k . ']"> ' . $v . '</p>';
                                    $indexnumber++;
                                }
                            }

                            for ($i = 1; $i <= count($languages); $i++) {
                                echo $arr_lang[$i];
                            }
                            /** 한국어가 먼저 나오는 버전 끝 * */
                            /** 영어가 먼저 나오는 버전 시작
                              foreach ($languages as $k => $v) {
                              $lang = ($id) ? $DB->get_field('main_menu_name', 'name', array('menuid' => $id, 'lang' => $k)) : '';
                              echo '<p>' . '<input type="text" value="' . $lang . '" name="name[' . $k . ']"> ' . $v . '</p>';
                              }
                              영어가 먼저 나오는 버전 끝 * */
                            ?>
                        </td>
                    </tr>
                    <tr class="">
                        <td class="field_title"><span class="required red">*</span> 필수여부</td>
                        <td class="field_value">
                            <select class="w_90" name="required">
                                <option value="1">필수</option>
                                <option <?php
                                if (isset($menu) && $menu->required == 2) {
                                    echo 'selected';
                                }
                                ?> value="2">선택</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="">
                        <td class="field_title"><span class="required red">*</span> 종류</td>
                        <td class="field_value">
<?php if (!isset($menu)) { ?>
                                <select class="w_90 select_type" name="type" onchange="change_type()">
                                    <option value="1">상위메뉴</option>
                                    <option value="2">하위메뉴</option>
                                    <option value="3">링크</option>
                                    <option value="4">팝업</option>
                                </select>
                                <p>상위메뉴는 하위메뉴를 추가하기위한 메뉴이며 링크,팝업은 하위메뉴를 추가 할 수 없습니다.</p>
                                <?php
                            } else {
                                switch ($menu->type) {
                                    case 1: echo '상위메뉴';
                                        break;
                                    case 2: echo '하위메뉴';
                                        break;
                                    case 3:
                                        echo '<select class="w_90 select_type" name="type" onchange="change_type()">
                                                <option value="3" selected>링크</option>
                                                <option value="4">팝업</option>
                                            </select>';
                                        break;
                                    case 4:
                                        echo '<select class="w_90 select_type" name="type" onchange="change_type()">
                                                <option value="3">링크</option>
                                                <option value="4" selected>팝업</option>
                                            </select>';
                                        break;
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    <tr class="parent_menu" style="display:none;">
                        <td class="field_title"><span class="required red">*</span> 상위메뉴</td>
                        <td class="field_value">
                            <?php
                            $parents = $DB->get_records('main_menu', array('type' => 1));
                            if ($parents) {
                                echo '<select class="w_90" name="parent" onchange="sub_parents(); parent_change();">';
                                foreach ($parents as $parent) {
                                    $lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $parent->id, 'lang' => current_language()));
                                    ?>
                        <option <?php
                        if (isset($menu) && $menu->parent == $parent->id) {
                            echo 'selected';
                        }
                        ?> value="<?php echo $parent->id; ?>"><?php echo $lang; ?></option>
                            <?php
                        }
                        echo '</select>';
                        echo '<select class="w_120" name="sub_parent" onchange="parent_change()">';
                        echo '<option value="0">해당사항없음</option>';
                        echo '</select>';
                    } else {
                        ?>
                    <p>등록된 상위메뉴가 없습니다. 상위메뉴를 등록 후 하위메뉴를 추가해주세요.</p>
                    <?php } ?>
                </td>
                </tr>
                <tr class="child_type" style="display:none;">
                    <td class="field_title"><span class="required red">*</span> 하위메뉴 종류</td>
                    <td class="field_value">
                        <select class="w_90" name="ispopup">
                            <option value="1">링크</option>
                            <option value="2" <?php
                            if (isset($menu) && $menu->ispopup == 2) {
                                echo 'selected';
                            }
                            ?>>팝업</option>
                        </select>
                    </td>
                </tr>
                <tr class="">
                    <td class="field_title"><span class="required red">*</span> 위치</td>
                    <td class="field_value">
                        <select class="w_160" name="step">
                            <?php
                            if (isset($menu)) {
                                ?>
                                <option value="">유지</option>
<?php } ?>
                            <option value="0">맨 앞</option>
                            <option value="-1">맨 뒤</option>
                        </select>
                    </td>
                </tr>
                <tr class="url_tr">
                    <td class="field_title">URL</td>
                    <td class="field_value">
                        <input type="text" class="w_300" name="url" <?php
                        if (isset($menu)) {
                            echo 'value="' . $menu->url . '"';
                        }
                        ?> placeholder="ex)http://www.naver.com">
                        <p class="description">http가 포함되지 않으면 <?php echo $CFG->wwwroot; ?>가 자동으로 포함됩니다. /login/index.php 식으로 입력해주세요.</p>
                    </td>
                </tr>
<!--                <tr class="icon_tr"> 
                    <td class="field_title">아이콘</td>
                    <td class="field_value">
                        <input type="text" name="icon" <?php
                if (isset($menu)) {
                    echo 'value="' . $menu->icon . '"';
                }
                ?> placeholder="ex)fa-address-book-o">
                        <p class="description">참조 <a style="color:rgb(85, 26, 139);" target="_blank" href="http://fontawesome.io/icons/">http://fontawesome.io/icons/</a> => 원하는 아이콘을 클릭 후 예재 i class="fa fa-address-book-o" 중 <strong>fa-address-book-o</strong></p>
                    </td>
                </tr>-->
                <tr>
                    <td class="field_title">사용여부</td>
                    <td class="field_value"> 
                        <select name="isused" class="w_90">
                            <option value="1">사용</option>
                            <option value="2" <?php
                            if (isset($menu) && $menu->isused == 2) {
                                echo 'selected';
                            }
                            ?>>미사용</option>
                        </select>
                    </td>
                </tr>
<!--                <tr>
                    <td class="field_title">권한</td>
                    <td class="field_value"> 
                <?php
//$query = 'select distinct usergroup from {lmsdata_user}';
//$usergroups = $DB->get_records_sql($query);
                ?>
                        <select name="usergroup[]" class="w_90" multiple="">
                <?php
//foreach ($usergroups as $usergroup => $val) {
//    if(!isset($menu)){
//        $selected = 'selected';
//    } else {
//        $group = $DB->get_field('main_menu_apply', 'id', array('menuid' => $menu->id, 'usergroup' => $usergroup));
//        if($group){
//            $selected = 'selected';
//        } else {
//            $selected = '';
//        }
//    }
//    echo '<option '.$selected.'>' . $usergroup . '</option>';
//}
                ?>
                        </select>
                    </td>
                </tr>-->
                </tbody>
            </table>

            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" />
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="location.href = '<?php echo $CFG->wwwroot; ?>/chamktu/support/main_menu_delete.php?delete=<?php echo $id; ?>'"/>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="location.href = 'main_menu.php';"/>
            </div>
        </form><!--Search Area2 End-->
    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    var sub_parent = 0;

<?php
if (isset($menu) && $menu->sub_parent != 0) {
    ?>
        sub_parent = <?php echo $menu->sub_parent; ?>;
    <?php
}
if (isset($menu)) {
    ?>
        change_type();
<?php } ?>
    $(document).ready(function () {
        sub_parents();
    });

    function change_type() {
        var type = $('.select_type option:selected').val();
        if (!type) {
            var type = $('input[name=type]').val();
        }
        var parent = $('select[name=parent] option:selected').val();
        var currentstep = $('input[name=current_step]').val();
        switch (type) {
            case '1':

                $('.parent_menu').hide();
//                $('.icon_tr').show();
                $('.child_type').hide();
                break;
            case '2':

                $('.parent_menu').show();
//                $('.icon_tr').hide();
                $('.child_type').show();
                break;
            case '3':

                $('.parent_menu').hide();
//                $('.icon_tr').show();
                $('.child_type').hide();
                break;
            case '4':

                $('.parent_menu').hide();
//                $('.icon_tr').show();
                $('.child_type').hide();
                break;
        }
        $.ajax({
            url: '/chamktu/support/main_menu_step.ajax.php',
            method: 'POST',
            async: false,
            data: {
                type: type,
                parent: parent,
                currentstep: currentstep,
                first: 1
            },
            success: function (data) {
                $('select[name=step]').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
    function parent_change() {
        var type = $('.select_type option:selected').val();
        if (!type) {
            var type = $('input[name=type]').val();
        }
        var parent = $('select[name=parent] option:selected').val();
        var s_parent = $('select[name=sub_parent] option:selected').val();
        console.log(s_parent);
        var currentstep = $('input[name=current_step]').val();
        $.ajax({
            url: '/chamktu/support/main_menu_step.ajax.php',
            method: 'POST',
            async: false,
            data: {
                type: type,
                parent: parent,
                currentstep: currentstep,
                sub_parent: s_parent
            },
            success: function (data) {
                $('select[name=step]').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }

    function sub_parents() {
        var parent = $('select[name=parent] option:selected').val();
        $.ajax({
            url: '/chamktu/support/main_menu_sub.ajax.php',
            method: 'POST',
            async: false,
            data: {
                parent: parent,
                sub_parent: sub_parent
            },
            success: function (data) {
                $('select[name=sub_parent]').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
</script>
<?php
include_once ('../inc/footer.php');
