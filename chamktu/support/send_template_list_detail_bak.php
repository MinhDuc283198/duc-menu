<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/send_template.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);

//검색용 파라미터
if ($type == 1) {
    $title = 'MAIL';
    $sql_select = "select * ";
    $sql_from = " FROM Balsong_Mail_Tran bm join Balsong_Mail_Tran_Detail bmt on bm.Seq = bmt.Mail_Tran_Seq    ";
    $sql_orderby = " order by bm.Seq desc ";    
}else {
    $title = 'SMS';
    $sql_select = "select * ";
    $sql_from = " FROM Balsong_Mobile ";
    $sql_orderby = " order by seqno desc ";
}

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);

?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo $title ?>발송내역</h3>
            <div style="float:left;">
                <input type="button" class="" value="메일" onclick="javascript:location.href = 'send_template_list.php?type=1';"/> 
                <input type="button" class="" value="SMS" onclick="javascript:location.href = 'send_template_list.php?type=2';"/> 
            </div>
        <?php if ($type == 1) { ?>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="15%">수신메일</th>
                    <th scope="row">수신자 상호명</th>
                    <th scope="row" width="5%">수신자</th>
                    <th scope="row" width="5%">사용자 임의값1</th>
                    <th scope="row" width="5%">사용자 임의값2</th>
                    <th scope="row" width="5%">사용자 임의값3</th>
                    <th scope="row" width="5%">사용자 임의값4</th>
                    <th scope="row" width="5%">사용자 임의값5</th>
                    <th scope="row" width="5%">사용자 임의값6</th>
                    <th scope="row" width="5%">사용자 임의값7</th>
                    <th scope="row" width="5%">사용자 임의값8</th>
                    <th scope="row" width="5%">사용자 임의값9</th>
                    <th scope="row" width="5%">사용자 임의값10</th>
                    <th scope="row" width="5%">상태</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                    <tr>
<!--                        <td><?php// echo $startnum--; ?></td>
                        <td><?php //echo $course->subject?></td>
                        <td><a href='<?php //echo $CFG->wwwroot ?>/chamktu/support/send_template_list_detail.php'><?php echo $course->mail_text?></a></td>
                        <td><?php //echo $course->person_nm?></td>
                        <td><?php //echo $course->dest_mail?></td>
                        <td><?php //echo $course->done_date?></td>
                        <td><?php //echo $course->total_ea?></td>
                        <td><?php //echo $course->fail_ea?></td>
                        <td><?php //echo $course->sender_nm?></td>
                        <td><?php //echo $course->done_date ? '발송완료' : '발송대기' ?></td>-->
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $course->dest_mail?></td>
                        <td><?php echo $course->company_nm?></td>
                        <td><?php echo $course->person_nm?></td>
                        <td><?php echo $course->etc1?></td>
                        <td><?php echo $course->etc2?></td>
                        <td><?php echo $course->etc3?></td>
                        <td><?php echo $course->etc4?></td>
                        <td><?php echo $course->etc5?></td>
                        <td><?php echo $course->etc6?></td>
                        <td><?php echo $course->etc7?></td>
                        <td><?php echo $course->etc8?></td>
                        <td><?php echo $course->etc9?></td>
                        <td><?php echo $course->etc10?></td>
                        <td><?php echo $course->status?></td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <?php }else{ ?>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%">내용</th>
                    <th scope="row" width="7%">수신번호</th>
                    <th scope="row" width="5%">발송일</th>
                    <th scope="row" width="5%">수신인원</th>
                    <th scope="row" width="5%">수신실패인원</th>
                    <th scope="row" width="10%">발송자</th>
                    <th scope="row" width="5%">상태</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                    <tr>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $course->msg_contents?></td>
                        <td><?php echo $course->dest_no?></td>
                        <td><?php echo $course->mobsend_time?></td>
                        <td><?php echo $course->total_ea?></td>
                        <td><?php echo $course->fail_ea?></td>
                        <td><?php echo $course->sender_nm?></td>
                        <td><?php echo $course->mobsend_time?'발송완료':'발송대기'?></td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <?php }
        print_paging_navbar($count_courses, $currpage, $perpage, "send_template_list.php?type=$type", $page_params);

        $query_string = '';
        if (!empty($excel_params)) {
            $query_array = array();
            foreach ($excel_params as $key => $value) {
                $query_array[] = urlencode($key) . '=' . urlencode($value);
            }
            $query_string = '?' . implode('&', $query_array);
        }
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->
<?php include_once ('../inc/footer.php'); ?>
