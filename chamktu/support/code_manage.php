<?php
/**
 * 코드관리 리스트 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/code_manage.php');
    redirect(get_login_url());
}
$context = context_system::instance(); 
require_capability('moodle/site:config', $context);

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);
$searcharea = optional_param('searcharea', '', PARAM_TEXT);

/**
 * 리스트 호출 함수
 * @global type $DB
 * @param type $page
 * @param type $perpage
 * @param type $searchtext
 * @param type $searcharea
 * @return type
 */
function get_top_list($page,$perpage,$searchtext,$searcharea){
    global $DB;
        $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    $where = "";
     if(!empty($searcharea) && !empty($searchtext)){
        $where= "where codename like '%$searchtext%'and code_category='$searcharea'";
    }else if(!empty($searcharea)){
        $where= "where code_category='$searcharea'";
    }else if(!empty($searchtext)){
        $where= "where codename like '%$searchtext%'";
    }

    $sql="select * from {lmsdata_code} $where order by parentid asc,step asc";
    return $DB->get_records_sql($sql,$params,$offset,$perpage);
}

$top_list= get_top_list($page-1,$perpage,$searchtext,$searcharea);


include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title">코드관리</h3>
         <div class="page_navbar">
            <a href="./popup.php"> <?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
            
            <strong>코드관리</strong>
        </div>
        <form name="course_search" id="course_search" class="search_area" action="code_manage.php" method="get">
            <input type="hidden" name="page" value="1" />
            <input type="hidden" name="coursetype" value="<?php echo $coursetype;?>" />
            <select title="category01" name="cata1" id="course_search_cata1" onchange="cata1_changed(this);"  class="w_160">
                <option value="0">영역선택</option>
                <?php
                $catagories = $DB->get_records('course_categories', array('visible'=>1, 'parent'=>2), 'sortorder', 'id, idnumber, name');

                foreach($catagories as $catagory) {
                    $selected = '';
                    if($catagory->id == $cata1) {
                        $selected = ' selected';
                    }
                    echo '<option value="'.$catagory->id.'"'.$selected.'> '.$catagory->name.'</option>';
                }
                ?>
            </select>
            <select title="category02" name="searcharea" id="course_search_cata1" class="w_160">
                <option value="0">분류선택</option>
                <?php
                $catagories = $DB->get_records_sql("select code_category from {lmsdata_code}");
                foreach($catagories as $catagory) {
                    $selected = '';
                    if($catagory->code_category == $searcharea) {
                        $selected = ' selected';
                    }
                    echo '<option value="'.$catagory->code_category.'"'.$selected.'> '.$catagory->code_category.'</option>';
                }
                ?>
            </select>
            <div style="float:center;">
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="코드명을 입력하세요."  class="search-text"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search','local_lmsdata'); ?>"/>          
            </div>
        </form><!--Search Area2 End-->

        <form method="post" id="formid" action="<?php echo $CFG->wwwroot ?>/chamktu/support/code_manage_del.php">
        <div>
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                     <th>일련번호</th>
                     <th>분류</th>
                     <th>코드</th>
                     <th>코드명/영문</th>
                     <th>순서</th>
                     <th>사용</th>
                     <th>날짜</th>
                    </tr>
            </thead>
            <tbody>
                <?php                 
                foreach($top_list as $top){
                    ?>
                <tr>
                    <td class="chkbox"><input type="checkbox" title="check"  name="codeid<?php echo $top->id; ?>" value="<?php echo $top->id; ?>" /></td>
                    <td><?php echo $top->id?></td>
                    <td><?php echo $top->code_category?></td>
                    <td><?php echo $top->code?></td>
            <td style="text-align:left"><?php if($top->depth==2){
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        }?><a href="<?php echo'/chamktu/support/code_manage_edit.php?id='.$top->id.'&page='.$page.'&perpage='.$perpage.'&searcharea='.$searcharea.'&searchtext='.$searchtext?>"><?php echo $top->codename?> / <?php echo $top->english_codename?></a></td>
                    <td><?php echo $top->step?></td>
                    <?php if($top->status){
                        echo '<td>승인</td>';
                    }else{
                        echo '<td>불가</td>';
                    }?>                    
                    <td><?php echo date("Y-m-d", $top->timecreated)?></td>
                </tr>
              
                <?php
                }
                
 		?>
                
                
             </tbody>
            </table>
            
        </div>
        <div style="float:left;">
            <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/course/management.php"' value="분류변경" style="float: center;" />
        </div>
        <div id="btn_area">
            <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/chamktu/support/code_manage_add.php?mod=top"' value="대분류등록" style="float: right;" />
            <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/chamktu/support/code_manage_add.php?mod=under"' value="소분류등록" style="float: center;" />
            <input type="button" id="code_delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left;"/>
        </div>
    </form>
        <div class="pagination">
        <?php

    $totalcount= $DB->count_records_select("lmsdata_code");
        print_paging_navbar($totalcount, $page, $perpage, $CFG->wwwroot . '/chamktu/support/code_manage.php', array("searcharea" => $searcharea,"searchtext"=>$searchtext));
        ?>
        </div>
        </div>
</div>
        
        <?php include_once ($CFG->dirroot . '/chamktu/inc/footer.php'); ?>

<script>
    $(function() {
        $("#allcheck").click(function() {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function() {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function() {
                    this.checked = false;
                });
            }
        });
        /**
         * 코드삭제
         */
        $("#code_delete_button").click(function() {
            if(confirm("<?php echo get_string('delete_confirm', 'local_lmsdata');?>")){
                $('#formid').submit();
            }
        });
    });
</script>