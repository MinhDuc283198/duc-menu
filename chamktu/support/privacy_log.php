<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once dirname(dirname (__FILE__)).'/lib/paging.php';
require_once dirname(dirname (__FILE__)).'/lib.php';
require_once dirname(dirname (__FILE__)).'/lib/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage     = optional_param('page', 1, PARAM_INT);
$perpage      = optional_param('perpage', 20, PARAM_INT);
$search       = optional_param('search', 'username', PARAM_RAW);
$searchtext   = optional_param('searchtext', '', PARAM_TEXT);
$excel = optional_param('excel', 0, PARAM_INT);

$sql = 'select * from {jei_privacy_log} order by id desc';
if (!$excel) {
    $logs = $DB->get_records_sql($sql, null,($currpage - 1) * $perpage, $perpage);
} else {
    $logs = $DB->get_records_sql($sql);
}
$sql2 = 'select mu.username, mu.firstname, mu.lastname from {user} mu '
        . 'join {jei_privacy_log} pl on pl.userid = mu.id';
$username = $DB->get_record_sql($sql2);
$sql3 = 'select count(*) from m_jei_privacy_log';

$count = $DB->count_records_sql($sql3);

$type_arr = array(1 => '조회', 2 => '등록', 3 => '수정', 4 => '삭제');

if (!$excel) {
    include_once (dirname(dirname (__FILE__)).'/inc/header.php'); 
?>
<div id="contents">
    <?php include_once  (dirname(dirname (__FILE__)).'/inc/sidebar_support.php');?>    
    <div id="content">
        <h3 class="page_title"><?php echo '개인정보 조회 로그'; ?></h3><br/>        
        <div style="float:right;">
            <input type="submit" class="blue_btn" style="margin-bottom:10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="privacy_log_excel();"/>                 
        </div>
        <form name="log_search" id="log_search" action="privacy_log.php" method="get">
            <input type="hidden" name="excel" value="0">
        </form>
        <table>
            <caption class="hidden-caption"><?php echo get_string('admin_management', 'local_lmsdata'); ?></caption>
            <thead>
            <tr>
                <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th scope="row" width="10%"><?php echo get_string('user_id', 'local_lmsdata'); ?></th>
                <th scope="row" width="10%"><?php echo get_string('name','local_lmsdata'); ?></th>
                <th scope="row" width="*"><?php echo 'URL'; ?></th>
                <th scope="row"><?php echo 'IP 주소'; ?></th>
                <th scope="row"><?php echo '접속 시간'; ?></th>
                <th scope="row" width="7%"><?php echo '구분'; ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
                if ($count === 0) {
            ?>
                <tr>
                    <td colspan="6">접속 기록이 없습니다.</td>
                </tr>
            <?php
                } else {                                
                $number = $count - (($currpage - 1) * $perpage);
                
                foreach($logs as $log){                    
            ?>
                <tr>
                    <td><?php echo $number--;?></td>                    
                    <td><?php echo $username->username; ?></td>
                    <td><?php echo fullname($username);?></td>
                    <td><?php echo $log->url;?></td>
                    <td><?php echo $log->ipaddr;?></td>
                    <td><?php echo date('Y-m-d H:i:s', $log->timecreated);?></td>
                    <td><?php echo $type_arr[$log->url_type];?></td>
                </tr>
            <?php                
                    }
                }
            ?>
            </tbody>
        </table><!--Table End-->
        <?php
            if (($count / $perpage) > 1) {
                print_paging_navbar($count, $currpage, $perpage, 'privacy_log.php');
            }
        ?>    
        
    </div><!--Content End-->
    
</div> <!--Contents End-->

 <?php include_once ('../inc/footer.php');
 } else {

    $fields = array(
        '번호',
        '아이디',
        '이름',
        'URL',
        'IP 주소',
        '접속 시간',
        '구분'
    );

    $date = date('Y-m-d', time());
    $filename = '개인정보 조회 로그_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($logs as $log) {

        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $username->username);
        $worksheet[0]->write($row, $col++, fullname($username));
        $worksheet[0]->write($row, $col++, $log->url);
        $worksheet[0]->write($row, $col++, $log->ipaddr);
        $worksheet[0]->write($row, $col++, date('Y-m-d H:i:s', $log->timecreated));
        $worksheet[0]->write($row, $col++, $type_arr[$log->url_type]);
 
        $row++;
    }

    $workbook->close();
    die;
 }
 ?>
<script type="text/javascript">
    function privacy_log_excel() {
        $("input[name='excel']").val(1);
        $("#log_search").submit();
        $("input[name='excel']").val(0);
    }
</script>

