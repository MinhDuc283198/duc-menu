<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$menu_number = optional_param('number', 0, PARAM_INT);

include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('menu_manage', 'local_lmsdata'); ?></h3>

        <div class="page_navbar">
            <a href="./evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
            <strong><?php echo get_string('menu_manage', 'local_lmsdata'); ?></strong>
        </div>
        <div class="main-menu-table">
            <table class="generaltable">
                <thead>
                    <tr>
                        <th style="width:5%;">번호</th>
                        <th style="width:10%;">형식</th>
                        <th style="width:16%">이름</th>
                        <th style="">URL</th>
                        <!--<th style="width:24%">권한</th>-->
                        <th style="width:10%;">생성일</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = 'select * from {main_menu} where depth < 3 order by parent asc , depth asc, step asc';
                    $menus = $DB->get_records_sql($query);
                    $num = 1;
                    foreach ($menus as $menu) {
                        $lang = $DB->get_field('main_menu_name','name',array('menuid'=>$menu->id,'lang'=> current_language()));
                        switch ($menu->type) {
                            case 1: 
                                $link = '';
                                $type_txt = '상위메뉴';
                                $target = '';
                                break;
                            case 2: 
                                $link = (preg_match('/http/i', $menu->url))?$menu->url:$CFG->wwwroot.$menu->url;
                                $type_txt = '하위메뉴';
                                if($menu->ispopup == 2){
                                    $target = 'target="_blank"';
                                } else {
                                    $target ='';
                                }
                                break;
                            case 3: 
                                $link = $menu->url;
                                $type_txt = '링크';
                                $target ='';
                                break;
                            case 4: 
                                $link = $menu->url;
                                $type_txt = '팝업';
                                $target = 'target="_blank"';
                                break;
                        }
                        ?>
                        <tr>
                            <td><?php echo $num++; ?></td>
                            <td><?php echo $type_txt; ?></td>
                            <td style="text-align:left; padding-left:15px;">
                                <a href="main_menu_add.php?id=<?php echo $menu->id; ?>" <?php if($menu->depth == 3) echo 'style="padding-left:20px;"'?> title="Edit">
                                <?php 
                                if($menu->type == 2){
                                    echo '<span class="child_icon">'.++$menu->step.'.</span> '; 
                                } else {
                                ?>
                                <i class="fa <?php echo $menu->icon; ?>" aria-hidden="true"></i> <?php } 
                                echo $lang; ?>
                                </a>
                            </td>
                            <td><?php echo $menu->url. '<a href="'.$link.'" '.$target.' class="red">[Link]</a>'; ?></td>
<!--                            <td>
                                <?php
//                                $usergroups = $DB->get_records('main_menu_apply', array('menuid' => $menu->id), '', 'usergroup');
//                                foreach ($usergroups as $usergroup) {
//                                    echo '<div class="menu_usergroups ' . $usergroup->usergroup . '">' . get_string('role:'.$usergroup->usergroup,'local_lmsdata') . '</div>';
//                                }
                                ?>
                            </td>-->
                            <td><?php echo date('Y-m-d', $menu->timecreated); ?></td>
                        </tr>
                    <?php
                        $sql = 'SELECT * FROM {main_menu} where sub_parent=:sub_parent ORDER BY step ASC';
                        if($third_menus = $DB->get_records_sql($sql, array('sub_parent'=>$menu->id))){
                           foreach($third_menus as $third_menu){
                               $lang = $DB->get_field('main_menu_name','name',array('menuid'=>$third_menu->id,'lang'=> current_language()));
                               $link = (preg_match('/http/i', $menu->url))?$menu->url:$CFG->wwwroot.$menu->url;
                                $type_txt = '하위메뉴';
                                if($menu->ispopup == 2){
                                    $target = 'target="_blank"';
                                } else {
                                    $target ='';
                                } 
                    ?>
                        <tr>
                            <td><?php echo $num++; ?></td>
                            <td><?php echo $type_txt; ?></td>
                            <td style="text-align:left; padding-left:15px;">
                                <a href="main_menu_add.php?id=<?php echo $third_menu->id; ?>" style="padding-left:20px;" title="Edit">
                                <span class="child_icon"><?php echo ++$third_menu->step;?></span>
                                <?php echo $lang; ?>
                                </a>
                            </td>
                            <td><?php echo $third_menu->url. '<a href="'.$link.'" '.$target.' class="red">[Link]</a>'; ?></td>
<!--                            <td>
                                <?php
//                                $usergroups = $DB->get_records('main_menu_apply', array('menuid' => $third_menu->id), '', 'usergroup');
//                                foreach ($usergroups as $usergroup) {
//                                    echo '<div class="menu_usergroups ' . $usergroup->usergroup . '">' . get_string('role:'.$usergroup->usergroup,'local_lmsdata') . '</div>';
//                                }
                                ?>
                            </td>-->
                            <td><?php echo date('Y-m-d', $third_menu->timecreated); ?></td>
                        </tr>
                    <?php
                           }
                        }
                    }
                    if (!$menus) {
                        echo '<tr><td align="center" colspan="6">등록된 메뉴가 없습니다</td></tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div id="btn_area">
            <input type="button" value="메뉴 추가" onclick="location.href = 'main_menu_add.php'" class="blue_btn" style="float:right;"/>
        </div>        
    </div>
</div><!--Content End-->
</div> <!--Contents End-->

<?php include_once ($CFG->dirroot . '/chamktu/inc/footer.php'); ?>

<script type="text/javascript">

</script>
