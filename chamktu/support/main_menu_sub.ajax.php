<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$parent = optional_param('parent',0, PARAM_INT);
$sub_parent = optional_param('sub_parent',0, PARAM_INT);
?>

<option value="0">해당사항없음</option>
<?php 
    $menus = $DB->get_records('main_menu',array('depth'=>2,'parent'=>$parent),'step asc');
    foreach($menus as $menu){
         $lang = $DB->get_field('main_menu_name','name',array('menuid'=>$menu->id,'lang'=> current_language()));
?>
    <option value="<?php echo $menu->id;?>" <?php if($sub_parent != 0 && $menu->id == $sub_parent){ echo 'selected'; } ?>><?php echo $lang;?></option>
<?php } ?>