<?php
/**
 * 발송내역 리스트
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/send_template.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$page = optional_param('page', 1, PARAM_INT);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);
$excel = optional_param('excel', 0, PARAM_INT);
$search_option = optional_param('search_option', 'search_option_all', PARAM_TEXT);

/*
if ($type == 1) {
    $title = 'MAIL';
    $sql_select = "select * ";
    //$sql_from = " FROM Balsong_Mail_Tran bm join Balsong_Mail_Tran_Detail bmt on bm.Seq = bmt.Mail_Tran_Seq ";
    $sql_from = " FROM {mailsend_history}";
    $sql_where = " WHERE subject like '%$searchtext%' ";
    $sql_orderby = " order by id desc ";    
}
 */

if($type == 1){
    $title = 'MAIL';
    if($search_option != "search_option_all"){
        if($search_option == "search_option_mail"){
            $sql = "SELECT *, 'mail' AS type FROM m_mailsend_history where subject like '%$searchtext%' order by timecreated desc ";
        }else if($search_option == "search_option_push"){
             $sql = "SELECT id,userid,subject,TEXT,targets,timecreated,done_date,send_status,'push' AS type  FROM m_pushsend_history where subject like '%$searchtext%' order by timecreated desc ";
        }
    }else{
        $sql = "SELECT *, 'mail' AS type FROM m_mailsend_history where subject like '%$searchtext%'  UNION all SELECT id,userid,subject,TEXT,targets,timecreated,done_date,send_status,'push' AS type  FROM  m_pushsend_history where subject like '%$searchtext%' order by timecreated desc ";
    }
    
}

//else {
//    $title = 'SMS';
//    $sql_select = "select * ";
//    $sql_from = " FROM Balsong_Mobile ";
//    $sql_where = " WHERE msg_contents like '%$searchtext%' ";
//    $sql_orderby = " order by seqno desc ";
//}

$courses = $DB->get_records_sql($sql, $params, ($currpage - 1) * $perpage, $perpage);
$courses1 = $DB->get_records_sql($sql, $params);

//$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from. $sql_where , $params);
$count_courses_mail = 0;
$count_courses_push = 0;

if($search_option != "search_option_all"){
    if($search_option == "search_option_mail"){
        $count_courses_mail = $DB->count_records_sql("SELECT COUNT(*)  FROM {mailsend_history}  WHERE subject like '%$searchtext%'", $params);
    }else if($search_option == "search_option_push"){
        $count_courses_push = $DB->count_records_sql("SELECT COUNT(*)  FROM {pushsend_history}  WHERE subject like '%$searchtext%'" , $params);
    }
}else{
    $count_courses_mail = $DB->count_records_sql("SELECT COUNT(*)  FROM {mailsend_history}  WHERE subject like '%$searchtext%'", $params);
    $count_courses_push = $DB->count_records_sql("SELECT COUNT(*)  FROM {pushsend_history}  WHERE subject like '%$searchtext%'" , $params);
}

$count_courses = $count_courses_mail + $count_courses_push;
//$count_courses = 10;
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);

if (!$excel) {
?>
<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title">발송내역</h3>
        <div class="page_navbar"><a href="./evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > <a href="./send_template.php">발송템플릿관리</a> > <strong>발송내역</strong></div>
        <?php if ($type == 1) {?>
            <form name="course_search" id="course_search" class="search_area" action="send_template_list.php" method="get">
                
                <input type="hidden" title="page" name="page" value="1" />
               <div style="float:center;">
                   <select name="search_option" title="category" class="w_160">
                        <option value="search_option_all" <?php if($search_option== 'search_option_all') echo 'selected';?>>전체</option>
                        <option value="search_option_mail" <?php if($search_option == 'search_option_mail') echo 'selected';?>>mail</option>
                        <option value="search_option_push" <?php if($search_option == 'search_option_push') echo 'selected';?>>push</option>
                    </select>
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="제목을 입력하세요."  class="search-text"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search','local_lmsdata'); ?>"/>          
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="float:right;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->
         <?php }else{ ?>
            <form name="course_search" id="course_search" class="search_area" action="send_template_list.php" method="get">
               <input type="hidden" title="page" name="page" value="1" />
               <input type="hidden" title="type" name="type" value="2" />
               <div style="float:center;">
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="내용을 입력하세요."  class="search-text"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search','local_lmsdata'); ?>"/>          
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="float:right;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->
         <?php } ?>
            <div style="float:left;">
                <?php if($type == 1){ ?>
                <input type="button" class="orange_btn" value="발송템플릿 리스트" id="mail" onclick="javascript:location.href = 'send_template.php';"/>
                <!--<input type="button" class="" value="SMS" id="sms" onclick="javascript:location.href = 'send_template_list.php?type=2';"/>--> 
                <?php }else{?>
                <input type="button" class="" value="발송템플릿 리스트" id="mail" onclick="javascript:location.href = 'send_template.php?';"/>
                <!--<input type="button" class="orange_btn" value="SMS" id="sms" onclick="javascript:location.href = 'send_template_list.php?type=2';"/>--> 
                <?php }?>
            </div>
           
        <?php if ($type == 1) { ?>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="5%">종류</th>
                    <th scope="row" width="15%">제목</th>
                    <th scope="row" width="10%">발송 일시</th>
                    <th scope="row" width="5%">발송 대상</th>
                    <th scope="row" width="5%">발송 여부</th>                    
                    <!--<th scope="row" width="5%">실패</th>-->
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo '발송 내역이 없습니다.'; ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {   
                    $send_status_text="";
                    if($course->send_status == 0){
                        $send_status_text = "발송 대기";
                    }else if($course->send_status == 1){
                        $send_status_text = "발송 성공";
                    }else{
                        $send_status_text = "발송 실패";
                    }
                    ?>
                    <tr>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $course->type; ?></td>
                        <td><?php echo $course->subject?></a></td>
                        <td><?php echo date('Y.m.d\  H:i',$course->done_date)?></td>
                        <td><?php echo $course->targets?></td>
                        <td><?php echo $send_status_text?></td>
<!--                    <td><?php echo $course->targets?></td>
                        <td><?php echo $course->total_ea?></td>
                        <td><?php echo $course->sent_ea?></td>-->                        
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <?php }else{ ?>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%">내용</th>
                    <th scope="row" width="7%">수신번호</th>
                    <th scope="row" width="5%">발송일</th>
                    <th scope="row" width="5%">상태</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                    <tr>
                        <td><?php echo $startnum--; ?></td>
<!--                        <td><a href='<?php echo $CFG->wwwroot ?>/chamktu/support/send_sms_list_detail.php?seq=<?php echo $course->seqno ?>'><?php echo $course->msg_contents?></a></td>-->
                        <td><?php echo $course->msg_contents?></td>
                        <td><?php echo $course->dest_no?></td>
                        <td><?php echo $course->mobsend_time?></td>
                        <td><?php echo $course->mobsend_time?'발송완료':'발송대기'?></td>
                    </tr>
                    <?php
                }
            }
            
            ?>    
        </table><!--Table End-->
        <?php }
        
        print_paging_navbar($count_courses, $currpage, $perpage, "send_template_list.php?type=$type", array("searchtext"=>$searchtext,"search_option"=>$search_option));
        $query_string = '';
        if (!empty($excel_params)) {
            $query_array = array();
            foreach ($excel_params as $key => $value) {
                $query_array[] = urlencode($key) . '=' . urlencode($value);
            }
            $query_string = '?' . implode('&', $query_array);
        }
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->

<?php include_once ('../inc/footer.php'); 
} else {
    if($type==1){
$fields = array(
        '번호',
        '제목',
        '발송일시',
        '대상',
        '성공 여부',        
    );

 $date = date('Y-m-d', time());
    $filename = '메일발송내역_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
   foreach ($courses1 as $course) {
        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $course->subject);
        $worksheet[0]->write($row, $col++, date('Y.m.d\  H:i',$course->done_date));
        $worksheet[0]->write($row, $col++, $course->targets);
        $worksheet[0]->write($row, $col++, $course->send_status?'발송완료':'발송대기');        
        $row++;
    }
    $workbook->close();
    die;
}
//else{
//   $fields = array(
//        '번호',
//        '내용',
//        '수신번호',
//        '발송일',
//        '상태',
//    );
//
// $date = date('Y-m-d', time());
//    $filename = 'SMS발송내역_' . $date . '.xls';
//
//    $workbook = new MoodleExcelWorkbook('-');
//    $workbook->send($filename);
//
//    $worksheet = array();
//
//    $worksheet[0] = $workbook->add_worksheet('');
//    $col = 0;
//    foreach ($fields as $fieldname) {
//        $worksheet[0]->write(0, $col, $fieldname);
//        $col++;
//    }
//
//    $row = 1;
//   foreach ($courses1 as $course) {
//        $col = 0;
//        $worksheet[0]->write($row, $col++, $row);
//        $worksheet[0]->write($row, $col++, $course->msg_contents);
//        $worksheet[0]->write($row, $col++, substr($course->dest_no, 0, 3) . '-' . substr($course->dest_no, 3, 4) . '-' . substr($course->dest_no, 7, 4));
//        $worksheet[0]->write($row, $col++, $course->mobsend_time);
//        $worksheet[0]->write($row, $col++, $course->mobsend_time?'발송완료':'발송대기');
//
//        $row++;
//    }
//     $workbook->close();
//    die; 
//}
    //}
}
?>
<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }

</script>  