<?php

require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin

require_login(0, false);

$names = required_param_array('name', PARAM_RAW);
$required = required_param('required', PARAM_INT);
$type = required_param('type', PARAM_INT);
$step = optional_param('step', '', PARAM_RAW);
$isused = required_param('isused', PARAM_INT);
$url = optional_param('url', '', PARAM_RAW);
$parent = optional_param('parent', 0, PARAM_INT);
$icon = optional_param('icon', '', PARAM_RAW);
//$usergroups = required_param_array('usergroup', PARAM_RAW);
$current_step = optional_param('current_step', 0, PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);
$ispopup = optional_param('ispopup', 1, PARAM_INT);
$sub_parent = optional_param('sub_parent',0, PARAM_INT);

/* To Do Edit 
 * 1.step rule define. 
 * 2. make edit
 * 3. test 
 */

$data = new stdClass();

if (!$edit) {
    if($sub_parent != 0){
        $data->depth = '3';
        $data->parent = $parent;
        $data->sub_parent = $sub_parent;
    } else if ($type != 2) {
        $data->depth = '1';
    } else {
        $data->depth = '2';
        $data->parent = $parent;
    }
    $data->type = $type;
    if ($step == -1) {  // 스텝 0 = 맨 앞
        if ($type == 2) {
            $max = $DB->get_field_sql('select max(step) from {main_menu} where parent = :parent and depth = :depth', array('depth' => $data->depth, 'parent' => $parent));
        } else {
            $max = $DB->get_field_sql('select max(step) from {main_menu} where depth = :depth', array('depth' => $data->depth));
        }
        $data->step = ($max) ? $max + 1 : 0;
    } else {
        $data->step = $step;
        if ($type == 2) {
            $DB->execute("update {main_menu} set step = step + 1 where parent = ? and depth = ? and step >= ?", array($parent, $data->depth, $step));
        } else {
            $DB->execute("update {main_menu} set step = step + 1 where depth = ? and step >= ?", array($data->depth, $step));
        }
    }

    $data->icon = $icon;
    $data->required = $required;
    $data->url = $url;
    $data->userid = $USER->id;
    $data->isused = $isused;
    $data->ispopup = $ispopup;
    $data->timecreated = time();

    $new_menu = $DB->insert_record('main_menu', $data);
    if ($type != 2) {
        $DB->set_field('main_menu', 'parent', $new_menu, array('id' => $new_menu));
    }

    foreach ($names as $lang => $name) {
        $name_languge = new stdClass();
        $name_languge->menuid = $new_menu;
        $name_languge->lang = $lang;
        $name_languge->name = $name;
        $name_languge->timemodified = time();
        $name_by_languge = $DB->insert_record('main_menu_name', $name_languge);
    }
    $usergroups = array('0'=>'rs', '1'=>'pr',  '2'=>'ad');
    foreach ($usergroups as $usergroup => $val) {
        $menu_usergroup = new stdClass();
        $menu_usergroup->menuid = $new_menu;
        $menu_usergroup->usergroup = $val;
        $menu_usergroup->timecreated = time();
        $menu_usergroup->timemodified = time();
        $name_by_languge = $DB->insert_record('main_menu_apply', $menu_usergroup);
    }


    redirect('main_menu.php');
} else {
    $menu = $DB->get_record('main_menu', array('id' => $edit));
    $data->id = $edit;
    
    if($sub_parent != 0){
        $data->depth = '3';
        $data->parent = $parent;
    } else if ($type != 2) {
        $data->depth = '1';
    } else {
        $data->depth = '2';
        $data->parent = $parent;
    }
    
    $data->sub_parent = $sub_parent;

    $current_groups = $DB->get_records('main_menu_apply', array('menuid' => $edit));
    
    
    
//    $ug = array(); // make new apply and change key,value
//    foreach ($usergroups as $k => $v) {
//        $ug[$v] = $k;
//        if ($group_apply = $DB->get_record('main_menu_apply', array('menuid' => $edit, 'usergroup' => $v))) {
//            continue;
//        } else {
//            $menu_usergroup = new stdClass();
//            $menu_usergroup->menuid = $edit;
//            $menu_usergroup->usergroup = $v;
//            $menu_usergroup->timecreated = time();
//            $menu_usergroup->timemodified = time();
//            $name_by_languge = $DB->insert_record('main_menu_apply', $menu_usergroup);
//        }
//    }
//    /* Delete Undefine Usergroups */
//    foreach ($current_groups as $k => $v) {
//        if (!isset($ug[$v->usergroup])) {
//            $DB->delete_records('main_menu_apply', array('menuid' => $edit, 'usergroup' => $v->usergroup));
//        }
//    }
    
    if ($step != '') {
        if ($menu->type == 2) {
            $current_step = $menu->step;
            $max = $DB->get_field_sql('select max(step) from {main_menu} where parent = :parent and depth = :depth and sub_parent = :sub_parent', array('depth' => $data->depth, 'parent' => $parent, 'sub_parent'=>$sub_parent));
            
            if($step == -1) {  // 스텝 0 = 맨 앞   -1 맨뒤
                $data->step = ($max) ? $max + 1 : 0;
            } else {
                $DB->execute("update {main_menu} set step = step + 1 where parent = ? and depth = ? and step >= ? and step <= ?", array($parent, $menu->depth, $step, $menu->step));
                $data->parent = $parent;
                $data->step = $step;
            }
        } else {
            $DB->execute("update {main_menu} set step = step - 1 where depth = ? and step >= ?", array($menu->parent, $menu->depth, $menu->step));
            if ($step == -1) {
                $max = $DB->get_field_sql('select max(step) from {main_menu} where depth = :depth', array('depth' => $menu->depth));
                $data->step = ($max) ? $max + 1 : 0;
            } else {
                $DB->execute("update {main_menu} set step = step + 1 where depth = ? and step >= ?", array($menu->parent, $menu->depth, $step));
            }
            $data->step = $step;
        }
    }


    $data->icon = $icon;
    $data->required = $required;
    $data->url = $url;
    $data->type = $type;
    $data->userid = $USER->id;
    $data->ispopup = $ispopup;
    $data->isused = $isused;
    $data->timemodified = time();
    $DB->update_record('main_menu', $data);


    foreach ($names as $lang => $name) {
        if (!$name) {
            $DB->delete_records('main_menu_name', array('menuid' => $edit, 'lang' => $lang));
        } else if (!$lang = $DB->get_record('main_menu_name', array('menuid' => $edit, 'lang' => $lang))) {
            $name_languge = new stdClass();
            $name_languge->menuid = $edit;
            $name_languge->lang = $lang;
            $name_languge->name = $name;
            $name_languge->timemodified = time();
            $name_by_languge = $DB->insert_record('main_menu_name', $name_languge);
        } else {
            $name_languge = new stdClass();
            $name_languge->id = $lang->id;
            $name_languge->name = $name;
            $name_languge->timemodified = time();
            $name_by_languge = $DB->update_record('main_menu_name', $name_languge);
        }
    }



    redirect('main_menu_add.php?id=' . $edit);
}