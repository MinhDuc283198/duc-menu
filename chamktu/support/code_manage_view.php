<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance(); 
require_capability('moodle/site:config', $context);

$id = required_param('id',PARAM_TEXT);

$view = get_code_view($id);

include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php if($view->depth==1){
            echo '대분류';
        }else{
            echo '소분류';
        }?> 코드</h3>

        <div class="page_navbar">
            <a>운영관리</a> > 
            <a href="<?php echo $CFG->dirroot . '/support/code_manage.php'; ?>" >코드관리</a> > 
            <strong><?php if($view->depth==1){
            echo '대분류 코드';
        }else{
            echo '소분류 코드';
        }?> </strong>
        </div>
        <div>
        <form id="frm_code_submit" action="code_manage_submit.php">
        <table>
                <tr>
                    <td bgcolor="#f2f2f2">코드분류</td><td style="text-align:left"><input type="text" value="<?php echo $view->code_category?>" readonly="readonly" name="code_category" size="10" maxlength="20">--->영문10자이내로 작성하세요. 예)BANK</td>
                </tr>
                <tr>
                    <td bgcolor="#f2f2f2">코드</td><td style="text-align:left"><input type="text" value="<?php echo $view->code?>" readonly="readonly"  name="code" size="10" maxlength="20">--->동일분류에서는 중복불가, 영문10자이내로 작성하세요 예)010</td>
                </tr>
                <tr>
                    <td bgcolor="#f2f2f2">코드명</td><td style="text-align:left"><input type="text" value="<?php echo $view->codename?>" readonly="readonly" name="codename" size="50" maxlength="100">예)국민은행</td>
                </tr>
                <tr>
                    <td bgcolor="#f2f2f2">영문코드명</td><td style="text-align:left"><input type="text" value="<?php echo $view->english_codename?>" readonly="readonly" name="english_codename" size="50" maxlength="100">예)kukmin</td>
                </tr>
                <tr>

                    <td bgcolor="#f2f2f2">순서</td><td style="text-align:left"><input type="text" value="<?php echo $view->step?>" readonly="readonly" name="step" size="1" value="1" maxlength="10"></td>

                </tr>
                <tr>
                    <?php if($view->status){
                        ?>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" name="status" value="1" checked="checked">승인&nbsp;&nbsp;&nbsp; <input type="radio" disabled="disabled" name="status" value="0">불가</td>
                    <?php
                    }else{?>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" disabled="disabled" name="status" value="1" >승인&nbsp;&nbsp;&nbsp; <input type="radio" name="status" value="0" checked="checked">불가</td>
                
                </tr>
                    <?php
                    
                    
            
                    }?>
                            
            </table>
            <div id="btn_area">
                <?php
                if($view->depth==1){
                    ?>
                <input type="button" id="code_add"  class="blue_btn" value="소분류 등록" style="float:right" />
                <?php
                }
                ?>
                        
                        <input type="button" id="code_edit"  class="blue_btn" value="수정하기" style="float:center" />
                        <input type="button" id="code_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                    </div><!--Btn Area End-->        
        </form>
            </div>
    </div>
</div>
        <script>
            $(document).ready(function () {
        $('#code_list').click(function () {
            location.href = "./code_manage.php";
        });
        $('#code_edit').click(function () {
            location.href = "./code_manage_edit.php?id="+<?php echo $id?>;
        });
        $('#code_add').click(function () {
            location.href = "./code_manage_add.php?id="+<?php echo $id?>;
        });
    }); 
            </script>
            