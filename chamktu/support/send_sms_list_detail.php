<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/send_template.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);
$seq = optional_param('seq', 0, PARAM_INT);
//검색용 파라미터
//if ($type == 1) {
//    $title = 'MAIL';
//    $sql_select = "select * ";
//    $sql_from = " FROM Balsong_Mail_Tran bm join Balsong_Mail_Tran_Detail bmt on bm.Seq = bmt.Mail_Tran_Seq    ";
//    $sql_orderby = " order by bm.Seq desc ";    
//}else {
    $title = 'SMS';
    $sql_select = "select * ";
    $sql_from = " FROM Balsong_Mobile ";
    $sql_where = " WHERE seqno = $seq ";
    $sql_orderby = " order by seqno desc ";
//}

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby);
//$courses = $DB->get_record_sql('Balsong_Mail_Tran', array('seq' => $seq));
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo $title ?>발송내역</h3>
            <div style="float:left;">
                <input type="button" class="" value="메일" onclick="javascript:location.href = 'send_template_list.php?type=1';"/> 
                <input type="button" class="" value="SMS" onclick="javascript:location.href = 'send_template_list.php?type=2';"/> 
            </div>
            <div style="float:right;">
                <input type="button" class="red-form" value="<?php echo get_string('return', 'local_courselist'); ?>" onclick="javascript:history.back()"/>
            </div>
        <div >
            <table >
        <?php //if ($type == 1) { ?>
                        
        </table><!--Table End-->
        <?php //}else{ ?>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%">seqno</th>
                    <th scope="row" width="5%">dest_no</th>
                    <th scope="row" width="7%">call_back</th>
                    <th scope="row" width="7%">msg_contents</th>
                    <th scope="row" width="5%">msg_instm</th>
                    <th scope="row" width="5%">sendreq_time</th>
                    <th scope="row" width="5%">mobsend_time</th>
                    <th scope="row" width="5%">repmsg_recvtm</th>
                    <th scope="row" width="5%">status_code</th>
                    <th scope="row" width="5%">mob_company</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                //$startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                <tr>
                    <td><?php echo $course->seqno?></td>
                    <td><?php echo $course->dest_no?></td>
                    <td><?php echo $course->call_back?></td>
                    <td><?php echo $course->msg_contents?></td>
                    <td><?php echo $course->msg_instm?></td>
                    <td><?php echo $course->sendreq_time?></td>
                    <td><?php echo $course->mobsend_time?></td>
                    <td><?php echo $course->repmsg_recvtm?></td>
                    <td><?php echo $course->status_code?></td>
                    <td><?php echo $course->mob_company?></td>
                </tr>
                  <?php
                }
            }
            ?>  
        </table><!--Table End-->
        <?php 
         //print_paging_navbar($count_courses, $currpage, $perpage, "send_template_list.php?type=$type", $page_params);
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->
<?php include_once ('../inc/footer.php'); ?>
