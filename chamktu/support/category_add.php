<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/category_add.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);

if ($id) {
    $category = $DB->get_record('lmsdata_category', array('id' => $id));
}
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_support.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo empty($id) ? get_string('add_category', 'local_lmsdata') : get_string('edit_category', 'local_lmsdata'); ?></h3>
        <div class="page_navbar"><a href="./evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
                                 <a href="./category.php"><?php echo get_string('category_manage', 'local_lmsdata'); ?></a> >             
                                 <strong><?php echo empty($id) ? get_string('add_category', 'local_lmsdata') : get_string('edit_category', 'local_lmsdata'); ?></strong></div>
        <form name="" action="category_add.execute.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="current_step" value="<?php if(isset($category)){ echo $category->step; }?>" >
            <?php
            if (isset($category)) {
            ?>
            <input type="hidden" name="edit" value="<?php echo $category->id; ?>">
            <?php if($category->type == 1 ||  $category->type == 2){ ?>
            <input type="hidden" name="type" value="<?php echo $category->type; ?>">
            <?php } } ?>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr>
                        <td class="field_title"><span class="required red">*</span>카테고리명</td>
                        <td class="field_value">
                            <?php
                            $languages = get_string_manager()->get_list_of_translations(); // Get languages for quick search later 
                            foreach ($languages as $k => $v) {
                                $lang = ($id) ? $DB->get_field('lmsdata_category_name', 'name', array('categoryid' => $id, 'lang' => $k)) : '';
                                echo '<p>' . '<input type="text" value="' . $lang . '" name="name[' . $k . ']">' . $v . '</p>';
                            }
                            ?>
                        </td>
                    </tr>
<!--                    <tr class="">
                        <td class="field_title"><span class="required red">*</span>필수여부</td>
                        <td class="field_value">
                            <select class="w_90" name="required">
                                <option value="1">필수</option>
                                <option <?php if (isset($category) && $category->required == 2) {
                                echo 'selected';
                            } ?> value="2">선택</option>
                            </select>
                        </td>
                    </tr>-->
                    <tr class="">
                        <td class="field_title"><span class="required red">*</span>종류</td>
                        <td class="field_value">
                            <?php if(!isset($category)){ ?>
                            <select class="w_90 select_type" name="type" onchange="change_type()">
                                <option value="1">상위카테고리</option>
                                <option value="2">하위카테고리</option>                               
                            </select>
                            <p>상위카테고리는 하위카테고리를 추가하기위한 카테고리입니다.</p>
                            <?php 
                            } else { 
                                switch($category->type){
                                    case 1: echo '상위카테고리'; break;
                                    case 2: echo '하위카테고리'; break;                                   
                                }
                            } 
                            ?>
                        </td>
                    </tr>
                    <tr class="parent_category" style="display:none;">
                        <td class="field_title"><span class="required red">*</span>상위카테고리</td>
                        <td class="field_value">
                <?php
                $parents = $DB->get_records('lmsdata_category', array('type' => 1));
                if ($parents) {
                    echo '<select class="w_90" name="parent" onchange="sub_parents(); parent_change();">';
                    foreach ($parents as $parent) {
                        $lang = $DB->get_field('lmsdata_category_name', 'name', array('categoryid' => $parent->id, 'lang' => current_language()));
                        ?>
                        <option <?php if (isset($category) && $category->parent == $parent->id) {
                            echo 'selected';
                        } ?> value="<?php echo $parent->id; ?>"><?php echo $lang; ?></option>
                <?php
                    }
                    echo '</select>';
                    echo '<select class="w_120" name="sub_parent" onchange="parent_change()">';
                        echo '<option value="0">해당사항없음</option>';
                    echo '</select>';
                } else {
                ?>
                    <p>등록된 상위카테고리가 없습니다. 상위카테고리를 등록 후 하위카테고리를 추가해주세요.</p>
                <?php } ?>
                </td>
                </tr>               
                <tr class="">
                    <td class="field_title"><span class="required red">*</span>위치</td>
                    <td class="field_value">
                        <select class="w_160" name="step">
<?php
if (isset($category)) {
    ?>
                                <option value="">유지</option>
<?php } ?>
                            <option value="0">맨 앞</option>
                            <option value="-1">맨 뒤</option>
                        </select>
                    </td>
                </tr>
<!--                <tr class="url_tr" style="display:none;">
                    <td class="field_title">URL</td>
                    <td class="field_value">
                        <input type="text" class="w_300" name="url" <?php if (isset($category)) { echo 'value="'.$category->url.'"'; }   ?> placeholder="ex)http://www.naver.com">
                        <p class="description">http가 포함되지 않으면 <?php echo $CFG->wwwroot; ?>가 자동으로 포함됩니다. /login/index.php 식으로 입력해주세요.</p>
                    </td>
                </tr>-->
<!--                <tr class="icon_tr"> 
                    <td class="field_title">아이콘</td>
                    <td class="field_value">
                        <input type="text" name="icon" <?php if (isset($category)) { echo 'value="'.$category->icon.'"'; }   ?> placeholder="ex)fa-address-book-o">
                        <p class="description">참조 <a style="color:rgb(85, 26, 139);" target="_blank" href="http://fontawesome.io/icons/">http://fontawesome.io/icons/</a> => 원하는 아이콘을 클릭 후 예제 i class="fa fa-address-book-o" 중 <strong>fa-address-book-o</strong></p>
                    </td>
                </tr>-->
                <tr>
                    <td class="field_title">사용여부</td>
                    <td class="field_value"> 
                        <select name="isused" class="w_90">
                            <option value="1">사용</option>
                            <option value="2" <?php if (isset($category) && $category->isused == 2) { echo 'selected'; } ?>>미사용</option>
                        </select>
                    </td>
                </tr>
<!--                <tr>
                    <td class="field_title">권한</td>
                    <td class="field_value"> 
<?php
//$query = 'select distinct usergroup from {lmsdata_user}';
//$usergroups = $DB->get_records_sql($query);
?>
                        <select name="usergroup[]" class="w_90" multiple="">
<?php
//foreach ($usergroups as $usergroup => $val) {
//    if(!isset($category)){
//        $selected = 'selected';
//    } else {
//        $group = $DB->get_field('category_apply', 'id', array('categoryid' => $category->id, 'usergroup' => $usergroup));
//        if($group){
//            $selected = 'selected';
//        } else {
//            $selected = '';
//        }
//    }
//    echo '<option '.$selected.'>' . $usergroup . '</option>';
//}
?>
                        </select>
                    </td>
                </tr>-->
                </tbody>
            </table>

            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" />
                <!--<input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="location.href='<?php echo $CFG->wwwroot;?>/chamktu/support/category_delete.php?delete=<?php echo $id;?>'"/>-->
                <?php  if($data->required == '1'){ echo get_string('siteadmin_necessary', 'local_lmsdata'); } else { ?><a href="#" class="normal_btn" onclick="if(confirm('해당 상위카테고리 삭제 시 연결된 하위카테고리도 모두 삭제됩니다. 삭제 하시겠습니까?')){ location.href='<?php echo $CFG->wwwroot;?>/chamktu/support/category_delete.php?delete=<?php echo $id;?>';  }"><?php echo get_string('delete', 'local_lmsdata'); ?></a><?php } ?>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="location.href = 'category.php';"/>
                
                
            </div>
        </form><!--Search Area2 End-->
    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
var sub_parent = 0;
        
<?php
if(isset($category) && $category->sub_parent != 0){
?>
sub_parent = <?php echo $category->sub_parent;?>;
<?php
}
if (isset($category)) { ?>
        change_type();
<?php } ?>
    $(document).ready(function(){
        sub_parents();
    });
    
    function change_type() {
        var type = $('.select_type option:selected').val();           
        if(!type){
            var type = $('input[name=type]').val();
        }
        var parent = $('select[name=parent] option:selected').val();
        var currentstep = $('input[name=current_step]').val();
        switch (type) {
            case '1':
                $('.url_tr').hide();
                $('.parent_category').hide();
//                $('.icon_tr').show();
                $('.child_type').hide();
                break;
            case '2':
                $('.url_tr').show();
                $('.parent_category').show();
//                $('.icon_tr').hide();
                $('.child_type').show();
                break;
           
        }
        $.ajax({
            url: '/chamktu/support/category_step.ajax.php',
            method: 'POST',
            async: false,
            data: {
                type: type,
                parent: parent,
                currentstep: currentstep,
                first: 1
            },
            success: function (data) {
                $('select[name=step]').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
    function parent_change(){
        var type = $('.select_type option:selected').val();           
        if(!type){
            var type = $('input[name=type]').val();
        }
        var parent = $('select[name=parent] option:selected').val();
        var s_parent = $('select[name=sub_parent] option:selected').val();
        console.log(s_parent);
        var currentstep = $('input[name=current_step]').val();
        $.ajax({
            url: '/chamktu/support/category_step.ajax.php',
            method: 'POST',
            async: false,
            data: {
                type: type,
                parent: parent,
                currentstep: currentstep,
                sub_parent: s_parent
            },
            success: function (data) {
                $('select[name=step]').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
    
    function sub_parents(){
        var parent = $('select[name=parent] option:selected').val();
        $.ajax({
            url: '/chamktu/support/category_sub.ajax.php',
            method: 'POST',
            async: false,
            data: {
                parent: parent,
                sub_parent: sub_parent
            },
            success: function (data) {
                $('select[name=sub_parent]').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
</script>
<?php
include_once ('../inc/footer.php');
