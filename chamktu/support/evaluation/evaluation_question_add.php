<?php
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once dirname(dirname(dirname(__FILE__))) . '/lib/paging.php';
require_once dirname(dirname(dirname(__FILE__))) . '/lib.php';

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
  $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/evaluation_course/evaluation_list.php');
  redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$formid = required_param('formid', PARAM_INT);
$categoryid = optional_param('categoryid', 0, PARAM_INT);
?>
<link rel="stylesheet" type="text/css" href="styles.css" >
<?php include_once (dirname(dirname(dirname(__FILE__))) . '/inc/header.php'); ?>
<div id="contents">
    <?php
    include_once (dirname(dirname(dirname(__FILE__))) . '/inc/sidebar_support.php');
    $orders = $DB->get_records('lmsdata_evaluation_questions', array('formid' => $formid, 'category' => $categoryid), 'sortorder asc', 'sortorder');
    $used = "&nbsp;" . get_string('used_order', 'local_lmsdata') . " : ";
    $max = 0;
    foreach ($orders as $order) {
	if ($max < $order->sortorder) {
	  $max = $order->sortorder;
	}
	$used .= $order->sortorder . ",";
    }
    $max++;
    ?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('add_question', 'local_lmsdata'); ?></h3>        
        <div class="page_navbar"><a href="./survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> >
		<a href="./survey_form.php">설문관리</a> > 
		<a href="./survey_form.php"><?php echo get_string('survey_form', 'local_lmsdata'); ?></a> >
		<a href="./evaluation_categories.php?formid=<?php echo $formid; ?>"><?php echo get_string('prev_sample', 'local_lmsdata'); ?></a> >
		<strong><?php echo get_string('add_question', 'local_lmsdata'); ?></strong>
        </div> 
        <form id="question_add_form" action="evaluation_question_submit.php" method="post" enctype="multipart/form-data"> 
            <input type="hidden" name="formid" value="<?php echo $formid; ?>">
            <input type="hidden" name="categoryid" value="<?php echo $categoryid; ?>">
            <input type="hidden" name="mode" value="add">
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr>
                        <td class="field_title"><?php echo get_string('question_name', 'local_lmsdata'); ?><span class="red"> * </span></td>
                        <td class="field_value">
                            <input type="text" name="title" value="<?php echo $max; ?>. " size="80" class="form_text" placeholder="<?php echo get_string('question_name', 'local_lmsdata'); ?>" required="true">
                        </td>
                    </tr>
			  <?php
//                    echo
//                    '<tr>
//                        <td class="field_title">'.get_string('header','local_lmsdata').'</td>
//                        <td class="field_value">
//                            <textarea name="contents" class="form_textarea" placeholder="'.get_string('header','local_lmsdata').'"></textarea>
//                        </td>
//                    </tr>';
			  ?>
                    <tr>
                        <td class="field_title" id="qtype" rowspan="2"><?php echo get_string('qtype', 'local_lmsdata'); ?><span class="red"> * </span></td>
                        <td class="field_value">
                            <input name="qtype" type="radio" value="1" checked=""> <?php echo get_string('qtype1', 'local_lmsdata'); ?>
                            <input name="qtype" type="radio" value="2"> <?php echo get_string('qtype2', 'local_lmsdata'); ?> 
                            <input name="qtype" type="radio" value="3"> <?php echo get_string('qtype3', 'local_lmsdata'); ?> 
                            <input name="qtype" type="radio" value="4"> <?php echo get_string('qtype4', 'local_lmsdata'); ?> 
                            <input name="qtype" type="radio" value="5"> <?php echo get_string('qtype5', 'local_lmsdata'); ?> 
                            <input name="qtype" type="radio" value="6"> <?php echo get_string('qtype6', 'local_lmsdata'); ?> 
                        </td>
                    </tr>
                    <tr id="answers">
                        <td class="left" style="padding:0 0 15px 15px;">
                            &nbsp;
                            <div id="answer_texts">
                                <input type="text" id="answer1" required="true" ired size="50" maxlength="120"  name="answers[]" class="answer_text form_text" placeholder="<?php echo get_string('qnumber', 'local_lmsdata', 1); ?>">
                                <input type="text" id="answer2" required="true" size="50" maxlength="120"  name="answers[]" class="answer_text form_text" placeholder="<?php echo get_string('qnumber', 'local_lmsdata', 2); ?>">
                                <input type="text" id="answer3" required="true"  size="50" maxlength="120"  name="answers[]" class="answer_text form_text" placeholder="<?php echo get_string('qnumber', 'local_lmsdata', 3); ?>">
                                <input type="text" id="answer4" required="true"  size="50" maxlength="120"  name="answers[]" class="answer_text form_text" placeholder="<?php echo get_string('qnumber', 'local_lmsdata', 4); ?>">
                                <input type="text" id="answer5" required="true"  size="50" maxlength="120"  name="answers[]" class="answer_text form_text" placeholder="<?php echo get_string('qnumber', 'local_lmsdata', 5); ?>">
                            </div>
                            <div id="answer_texts_etc">
                                <input type="text" id="answeretc" class="form_text" size="50" maxlength="120" value="기타" name="answers_etc" placeholder="<?php echo get_string('etc_string', 'local_lmsdata'); ?>" required="false">
                            </div>
                            <div class="btns">
                                <input type="button" class="gray_btn_small" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="delete_select()"/>
                                <input type="button" class="gray_btn_small" style="float:left; margin-right: 10px;" value="<?php echo get_string('etc_string', 'local_lmsdata'); ?>" onclick="add_select()"/>
                            </div>
                        </td>
                    </tr>
                    <tr id="answers_5">
                        <td class="left" style="padding:0 0 15px 15px;">
                            &nbsp;
                            <div id="answer5_text">
                                <input type="text" id="answer5_1"  size="50" maxlength="120" required="true" name="answers[]" class="answer_text_5 form_text" value="<?php echo get_string('answer1', 'local_lmsdata'); ?>" disabled>
                                <input type="text" id="answer5_2"  size="50" maxlength="120" required="true" name="answers[]" class="answer_text_5 form_text" value="<?php echo get_string('answer2', 'local_lmsdata'); ?>" disabled>
                                <input type="text" id="answer5_3"  size="50" maxlength="120" required="true" name="answers[]" class="answer_text_5 form_text" value="<?php echo get_string('answer3', 'local_lmsdata'); ?>" disabled>
                                <input type="text" id="answer5_4"  size="50" maxlength="120" required="true" name="answers[]" class="answer_text_5 form_text" value="<?php echo get_string('answer4', 'local_lmsdata'); ?>" disabled>
                                <input type="text" id="answer5_5"  size="50" maxlength="120" required="true" name="answers[]" class="answer_text_5 form_text" value="<?php echo get_string('answer5', 'local_lmsdata'); ?>" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr id="expression">
                        <td class="field_title"><?php echo get_string('selectxy', 'local_lmsdata'); ?><span class="red"> * </span></td>
                        <td class="field_value">
				    <?php echo get_string('x', 'local_lmsdata'); ?> <input name="expression" type="radio" value="1" checked="">
				    <?php echo get_string('y', 'local_lmsdata'); ?> <input name="expression" type="radio" value="2">
                        </td>
                    </tr> 
                    <tr>
                        <td class="field_title"><?php echo get_string('selectrequire', 'local_lmsdata'); ?><span class="red"> * </span></td>
                        <td class="field_value">
				    <?php echo get_string('required', 'local_lmsdata'); ?> <input name="required" type="radio" value="1" checked="">
				    <?php echo get_string('select', 'local_lmsdata'); ?> <input name="required" type="radio" value="2" >
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><label for="form_sortorder"><?php echo get_string('order', 'local_lmsdata'); ?><span class="red"> * </span></label></td>
                        <td class="field_value">
                            <input type="text" name="sortorder" id="form_sortorder" value="<?php echo $max; ?>" placeholder="ex) 1" size="2" required="true" />
				    <?php
				    echo rtrim($used, ',');
				    ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="btn_area">
                <input type="button"  onclick="validateRequiredFields();" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" />
                <input type="button" class="red_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="location.href = 'evaluation_categories.php?formid=<?php echo $formid; ?>';"/>
            </div>
        </form>
        <script>
          var value = '';
          function delete_select() {
              var number = $('.answer_text').length;
              $('#answers input[id=answer' + number + ']').remove(); //수정
          }
          function add_select() {
              var number = $('.answer_text').length;
              number++;
              $('#answer_texts').append('<input type="text" id="answer' + number + '" name="answers[]" size="50" required="true" maxlength="120" class="answer_text form_text" placeholder="<?php echo get_string('qtype2', 'local_lmsdata'); ?> ' + number + '번 문구">');
          }
          $('input[name=qtype]').click(function () {
              value = $('input[name=qtype]:checked').val();
              switch (value) {
                  case '1':
                  case '2':
                      $('#expression').show();
                      $('#answers').show();
                      $('#answers_5').hide();
                      $('#qtype').prop('rowspan', '2');
                      $('input[name=expression]').prop('disabled', false);
                      $('.answer_text').prop('disabled', false);
                      $('.answer_text_5').prop('disabled', true);
                      break;
                  case '5':
                      $('#expression').show();
                      $('#answers_5').show();
                      $('#answers').hide();
                      $('#qtype').prop('rowspan', '2');
                      $('input[name=expression]').prop('disabled', false);
                      $('.answer_text_5').prop('disabled', false);
                      $('.answer_text').prop('disabled', true);
                      break;
                  default :
                      $('#expression').hide();
                      $('#answers').hide();
                      $('#answers_5').hide();
                      $('input[name=expression]').prop('disabled', true);
                      $('.answer_text').prop('disabled', true);
                      $('.answer_text_5').prop('disabled', true);
                      $('#qtype').prop('rowspan', '1');
                      break;
              }
          });
          function validateRequiredradios() {
              if ($('input[name=expression]:checked').val() == '' || $('input[name=expression]:checked').val() < 1 || $('input[name=expression]:checked').val() == undefined) {
                  return false;
              } else if ($('input[name=required]:checked').val() == '' || $('input[name=required]:checked').val() < 1 || $('input[name=required]:checked').val() == undefined) {
                  return false;
              } else {
                  return true;
              }
          }
          function validateRequiredFields() {
              if ($("#answeretc").val() != '' && $("#answeretc").val() != undefined && $("#answeretc").val().length < 1) {
                  $("#answeretc").remove();
              }
              var returnval = validateRequiredradios();
              if (returnval) {
                  $('.detail input[type="text"], textarea, select:visible:first').each(function (i, requiredField) {
                      if ($(requiredField).attr('id') != 'answeretc' && $(requiredField).val() == '' && $(requiredField).attr('disabled') != 'disabled' && returnval) {
                          alert('필수여부를 체크해주세요.');
                          $(requiredField).focus();
                          returnval = false;
                      }
                  });
              }
              if (returnval) {
                  $('#question_add_form').submit();
              }
          }
        </script>
    </div><!--Content End-->
</div>
<?php include_once ('../../inc/footer.php'); ?>
