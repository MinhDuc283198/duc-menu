<?php
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once dirname(dirname(dirname(__FILE__))) . '/lib/paging.php';
require_once dirname(dirname(dirname(__FILE__))) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/evaluation_course/evaluation_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$hakyear = optional_param('hakyear', '', PARAM_RAW); //학년
$search = optional_param('search', 1, PARAM_INT);
$classobject = optional_param('term', 0, PARAM_RAW);
$year = optional_param('year', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);

// 현재 년도, 학기
//if(!$year) {
//    $year = get_config('moodle', 'haxa_year');
//}

$page_params = array();
$params = array(
    'contextlevel' => CONTEXT_COURSE,
);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js',
    $CFG->wwwroot . '/theme/oklassedu/javascript/theme.js'
);
?>

<?php include_once (dirname(dirname(dirname(__FILE__))) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(dirname(__FILE__))) . '/inc/sidebar_support.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo get_string('lectureevaluation', 'local_lmsdata'); ?></h3>
        <!--<div class="page_navbar"><a href="../category_list.php"><?php echo get_string('course_management', 'local_lmsdata'); ?></a> > <a href="../evaluation/evaluation_form.php"><?php echo get_string('evaluation_questionnaire', 'local_lmsdata'); ?></a> > <?php echo get_string('lectureevaluation', 'local_lmsdata'); ?></div>-->
        <div class="page_navbar"><a href="../../manage/index.php">관리자페이지</a> > <a href="../evaluation/evaluation_result_list.php">운영관리</a> > <?php echo get_string('lectureevaluation', 'local_lmsdata'); ?></div>
        <form name="" id="course_search" class="search_area" action="evaluation_result_list.php" method="get">
            <input type="hidden" name="page" value="1" />
            <select  title="year" name="year" class="w_160">
                <option value=""><?php echo get_string('all', 'local_lmsdata'); ?></option>
                <?php
                $year_arr = lmsdata_get_years();
                foreach ($year_arr as $tg_year) {
                    $selected = "";
                    if ($tg_year == $year) {
                        $selected = "selected";
                    }
                    echo '<option value="' . $tg_year . '"  ' . $selected . '>' . get_string('year', 'local_lmsdata', $tg_year) . '</option>';
                }
                ?>
            </select>
            <select title="term" name="term" class="w_160">
                <option value="0" <?php echo $classobject == 0 ? 'selected' : '' ?>><?php echo get_string('all', 'local_lmsdata'); ?></option>
                <?php
                $object_arr = lmsdata_get_object();

                foreach ($object_arr as $object_key => $tg_object) {
                    $selected = "";
                    if ($object_key == $classobject) {
                        $selected = "selected";
                    }
                    echo '<option value="' . $object_key . '"  ' . $selected . '>' . $tg_object . '</option>';
                }
                ?>
            </select> 
            <br/>
            <select title="search" name="search" class="w_160">
                <option value="0" <?php echo!empty($search) == 0 ? 'selected' : '' ?> ><?php echo get_string('all', 'local_lmsdata'); ?></option>
                <option value="1" <?php echo!empty($search) == 1 ? 'selected' : '' ?> ><?php echo get_string('course_code', 'local_lmsdata'); ?></option>
                <option value="2" <?php echo!empty($search) == 2 ? 'selected' : '' ?>>강의명</option>
            </select> 
            <input title="search" type="text" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('search_placeholder', 'local_lmsdata'); ?>"  class="search-text"/>
            <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
        </form><!--Search Area2 End-->

        <table>
            <caption class="hidden-caption">강의평가</caption>
            <tr>
                <th scope="row"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th scope="row"><?php echo get_string('course_code', 'local_lmsdata'); ?></th>
                <th scope="row"><?php echo get_string('course_name', 'local_lmsdata'); ?></th>
                <th scope="row"><?php echo get_string('eval_period', 'local_lmsdata'); ?></th>
                <th scope="row">작성자수</th>
                <th scope="row"><?php echo get_string('viewresult', 'local_lmsdata'); ?></th>
            </tr>
            <?php
            $join = "";
            $lcon = "";
            $params = array('type' => 1);
            if ($cata3 || $cata2 || $cata1)
                $join = "join {course_categories} cc on cc.path like :cata and cc.path like '%/'||c.category  ";
            if ($cata3) {
                $params['cata'] = '/' . $cata1 . '/' . $cata2 . '/' . $cata3;
            } else if ($cata2) {
                $params['cata'] = '/' . $cata1 . '/' . $cata2 . '/%';
            } else if ($cata1) {
                $params['cata'] = '/' . $cata1 . '/%';
            }
            if ($year) {
                $lcon .= ' and lc.classyear = :year ';
                $params['year'] = $year;
            }
            if ($classobject) {
                $lcon .= ' and ( lco.classobject = :classobject or lco.classtype = :classtype ) ';
                $params['classobject'] = $classobject;
                $params['classtype'] = $classobject;
            }

            if (!empty($searchtext)) {
                $params['searchtxt'] = '%' . $searchtext . '%';
//                $params['searchtxt2'] = '%'.$searchtext.'%';
                $params['searchtxt3'] = '%' . $searchtext . '%';
                switch ($search) {
                    case 0:
//                         $lcon .= ' and (lc.kor_lec_name like :searchtxt or lc.eng_lec_name like :searchtxt2 or lc.subject_id like :searchtxt3) '; 
                        $lcon .= ' and (c.fullname like :searchtxt or lco.coursecd like :searchtxt3) ';
                        break;
                    case 1:
                        $lcon .= ' and lco.coursecd like :searchtxt ';
                        break;
                    case 2:
//                        $lcon .= ' and (lc.kor_lec_name like :searchtxt or lc.eng_lec_name like :searchtxt2) '; 
                        $lcon .= ' and (c.fullname like :searchtxt) ';
                        break;
                    default:
//                        $lcon .= ' and (lc.kor_lec_name like :searchtxt or lc.eng_lec_name like :searchtxt2 or lc.subject_id like :searchtxt3) '; 
                        $lcon .= ' and (c.fullname like :searchtxt or lco.coursecd like :searchtxt3) ';
                        break;
                }
            }
            $offset = ($currpage - 1) * $perpage;
            $sql = 'select e.*, c.fullname, lco.coursecd, c.fullname '
                    . 'from {lmsdata_evaluation} e '
                    . ' join {course} c on c.id = e.course '
                    . ' join {lmsdata_class} lc on lc.courseid = c.id '
                    . ' join {lmsdata_course} lco on lc.parentcourseid = lco.id '
                    . $join
                    . ' where e.type = :type ' . $lcon
                    . 'order by e.id desc';
            $evaluations = $DB->get_records_sql($sql, $params, $offset, $perpage);
            $cnt = 1;
            foreach ($evaluations as $evaluation) {
//                print_object($evsluation);
                ?>
                <tr>
                    <td><?php echo $cnt++; ?></td>
                    <td><?php echo empty($evaluation->coursecd) ? '-' : $evaluation->coursecd; ?></td>
                    <td><?php echo $evaluation->fullname; ?></td> 
                    <td><?php echo date("Y-m-d", $evaluation->timestart) . " ~ " . date("Y-m-d", $evaluation->timeend); ?></td>
                    <td><?php echo $DB->count_records('lmsdata_evaluation_submits', array('evaluation' => $evaluation->id, 'completion' => 1)); ?></td>
                    <td>
                        <?php
                        if ($DB->count_records('lmsdata_evaluation_submits', array('evaluation' => $evaluation->id, 'completion' => 1)) > 0) {
                            ?>
                                <!--<input type="button" class="gray_btn_small" onclick="window.open('<?php // echo $CFG->wwwroot . "/local/evaluation/answers.php?id=" . $evaluation->id;  ?>', 'answers', '')" value="<?php // echo get_string('viewresult', 'local_lmsdata');  ?>">-->
                            <input type="button" class="gray_btn_small" onclick="window.open('<?php echo $CFG->wwwroot . "/chamktu/support/evaluation/evaluation_result.php?id=" . $evaluation->id; ?>', 'answers', '')" value="<?php echo get_string('viewresult', 'local_lmsdata'); ?>">
                            <!--<input type="button" class="gray_btn_small" id="opener_result"  value="<?php echo get_string('viewresult', 'local_lmsdata'); ?>">-->
                            <!--<input type="button" class="gray_btn_small" onclick="opener_result(<?php echo $evaluation->id; ?>)"  value="<?php echo get_string('viewresult', 'local_lmsdata'); ?>">-->
                            <?php
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            if (empty($evaluations)) {
                echo '<tr><td colspan="7">' . get_string('Explanation', 'local_lmsdata') . '</td></tr>';
            }
            ?>

        </table><!--Table End-->

        <!--        <div id="btn_area">
                    <div style="float:right;">
                        <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('add_evaluation', 'local_lmsdata'); ?>" onclick="location.href='evaluation_list_add.php'"/> 
                    </div>
                </div>-->
        <?php
        if (($evaluations_cnt / $perpage) > 1) {
            print_paging_navbar_script($evaluations_cnt, $currpage, $perpage, 'javascript:cata_page(:page);');
        }
        ?>

    </div><!--Content End-->

</div> <!--Contents End-->

<script>
//opener_result

    /**
     function opener_result(evaluationid) {
     alert(evaluationid);
     var tag = $("<div id='select_user'></div>");
     $.ajax({
     //            url: 'evaluation_result.php',
     //            url: '<?php // echo $CFG->wwwroot . "/local/evaluation/answers.php?id=" . $evaluation->id;  ?>',
     url: '<?php echo $CFG->wwwroot . "/local/evaluation/answers.php"; ?>',
     method: 'POST',
     data: {
     id : evaluationid
     },
     success: function (data) {
     tag.html(data).dialog({
     title: '강의평가 보기',
     modal: true,
     width: 800,
     maxHeight: getWindowSize().height - 20,
     buttons: [{id: 'close',
     text: '닫기',
     disable: true,
     click: function () {
     $(this).dialog("close");
     }}],
     open: function () {
     var t = $(this).parent();
     var w = $(window);
     var s = getWindowSize();
     
     var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
     if (x < 0)
     x = 0;
     var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
     if (y < 0)
     y = 0;
     t.offset({
     top: y,
     left: x
     });
     },
     close: function () {
     $('#select_user').remove();
     $(this).dialog('destroy').remove();
     }
     }).dialog('open');
     }
     });
     }
     **/

//          function opener_result(evaluationid) {
//               var tag = $("<div id='select_user'></div>");
//               tag.dialog("open");
//            $.ajax({
//                url: "<?php echo $CFG->wwwroot . '/local/evaluation/answers.php'; ?>",
//                autoOpen : false,
//                data: {
//                id : evaluationid
//            },
//                success: function (result) {
//                    tag.html(result);
//                }
//            });
//          }

    $("#opener_result").click(function () {
        var tag = $("<div id='select_user'></div>");
        tag.dialog("open");
        $.ajax({
            url: "<?php echo $CFG->wwwroot . '/local/evaluation/answers.php'; ?>",
            success: function (result) {
                tag.html(result);
            }
        });
    });
</script>

<?php include_once ('../../inc/footer.php'); ?>
