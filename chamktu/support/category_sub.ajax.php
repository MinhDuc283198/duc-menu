<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$parent = optional_param('parent',0, PARAM_INT);
$sub_parent = optional_param('sub_parent',0, PARAM_INT);
?>

<option value="0">해당사항없음</option>
<?php 
    $categorys = $DB->get_records('lmsdata_category',array('depth'=>2,'parent'=>$parent),'step asc');
    foreach($categorys as $category){
         $lang = $DB->get_field('lmsdata_category_name','name',array('categoryid'=>$category->id,'lang'=> current_language()));
?>
    <option value="<?php echo $category->id;?>" <?php if($sub_parent != 0 && $category->id == $sub_parent){ echo 'selected'; } ?>><?php echo $lang;?></option>
<?php } ?> 