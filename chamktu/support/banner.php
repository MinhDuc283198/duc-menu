<?php
require(dirname(dirname(dirname(__FILE__))).'/config.php');
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/board/board_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
include_once('../inc/header.php');

$id = optional_param('id', 0, PARAM_INT);
$banners = lmsdata_banner();
?>
    <?php include_once('../inc/sidebar_support.php');?>
<div id="content">
    <h3 class="page_title"><?php echo get_string('banner', 'local_lmsdata'); ?></h3>
    <div class="page_navbar"><a href="./notice.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > <a href="./banner.php"><?php echo get_string('banner','local_lmsdata'); ?></a></div>
      <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/'>
            <table>
                <caption class="hidden-caption">배너관리</caption>
                <thead>
                    <tr>
                        <th scope="row" width="45%">배너명</th>
                        <th scope="row" width="10%">첨부파일</th>
                        <th scope="row" width="15%">링크</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($banners as $banner) {
                            $fs = get_file_storage();
                            $files = $fs->get_area_files($context->id, 'local_lmsdata', 'attachment', true, 'timemodified', false);  
                        ?>
                        <tr>
                            <td><?php echo $banner -> name?></td>                               
                        <?php
                        if ($banner->id) {
                            $output = '';
                            foreach ($files as $file) {
                                $filename = $file->get_filename();
                                $mimetype = $file->get_mimetype();
                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';                                
                                
                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $book->itemid . '/' . $filename);
                                if ($file->get_filesize() > 0) {
                                    $fileobj = '<img class=\'small\' src="'.$path.'">';

                                }
                            }
                        }
                            ?>
                            
                            <td>
                                <?php 
                                if($banner->id){
                                    echo $fileobj;
                                }
                                ?>
                            </td>
                            <td><?php echo $banner->linkurl?></td> 
                        </tr>
                        <?php 
                         } 
                        ?>
                </tbody>
            </table>
            <div id="btn_area">
                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/chamktu/board/rebooks_write.php?mod=add"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" />
            </div>
        </form>
</div>
