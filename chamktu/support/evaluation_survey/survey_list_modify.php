<?php
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once dirname(dirname(dirname(__FILE__))) . '/lib/paging.php';
require_once dirname(dirname(dirname(__FILE__))) . '/lib.php';

// Check for valid admin user - no guest autologin

require_login(0, false);

$id = required_param('id', PARAM_INT);

$evaluation = $DB->get_record('lmsdata_evaluation', array('id' => $id));

if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/evaluation/evaluation_add.php');
    redirect(get_login_url());
}

$context = context_system::instance();
require_capability('moodle/site:config', $context);

include_once (dirname(dirname(dirname(__FILE__))) . '/inc/header.php');
?>
<div id="contents">
    <?php include_once (dirname(dirname(dirname(__FILE__))) . '/inc/sidebar_support.php'); ?>

    <div id="content">
        <h3 class="page_title">설문 수정</h3>
        <div class="page_navbar"><a href="./survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> >
            <a href="../evaluation/survey_form.php">설문관리</a> > 
            <a href="./survey_list.php"><?php echo get_string('survey', 'local_lmsdata'); ?></a> >
            <strong>설문 수정</strong></div>

        <form id="evaluation_add_form" action="submit.php" method="post" enctype="multipart/form-data"> 
            <div>
                <table cellpadding="0" cellspacing="0" class="detail">
                    <input type="hidden" name="userid" value="<?php echo $USER->id; ?>" />
                    <input type="hidden" name="mode" value="modify" />
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <tbody>
                        <tr>
                            <td class="field_title"><label for="form_form"><?php echo get_string('force', 'local_evaluation'); ?></label></td>
                            <td class="field_value">
                                <input type="checkbox" value="1" name="compulsion" <?php if ($evaluation->compulsion == 1) echo 'checked'; ?> />
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><label for="form_content">설문양식</label></td>
                            <td class="field_value">
                                <?php
                                if (!empty($evaluation->formid)) {
                                    $formname = $DB->get_field('lmsdata_evaluation_forms', 'title', array('id' => $evaluation->formid));
                                }
                                echo $formname;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><label for="form_content">설문기간</label></td>
                            <td class="field_value">
                                <input type="text" title="설문기간" name="starttime" id="timestart" class="w_120" value="<?php echo empty($evaluation->timestart) ? date('Y-m-d', time()) : date('Y-m-d', $evaluation->timestart); ?>" placeholder="yyyy-mm-dd"/>
                                ~ 
                                <input type="text" title="설문기간" name="endtime" id="timeend" class="w_120" value="<?php echo empty($evaluation->timeend) ? date('Y-m-d', time()) : date('Y-m-d', $evaluation->timeend); ?>" placeholder="yyyy-mm-dd"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><label for="form_content">대상선택</label></td>
                            <td class="field_value">                                        
                                <?php
                                $targets = array('10' => '임직원', '20' => '멘토', '30' => '교육생',);
                                $tars = array();
                                if (!empty($evaluation->targets)) {
                                    $tars = explode(',', $evaluation->targets);
                                }
                                foreach ($targets as $key => $val) {
                                    echo '<input type="checkbox" name ="targets[]" ' . (in_array($key, $tars) ? 'checked' : '') . ' value="' . $key . '"/> <label>' . $val . '</label>';
                                }
                                ?>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" />
                <?php if (empty($submits)) { ?>
                    <input type="button" class="red_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="location.href = 'survey_delete.php?id=<?php echo $id; ?>';"/>
<?php } ?>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="location.href = 'survey_list.php';"/>
            </div>
        </form><!--Search Area2 End-->
    </div><!--Content End-->

</div> <!--Contents End--> 

<!-- 양식선택 다이얼로그 시작 -->
<div id="form_search_dialog">
</div>
<!-- 양식선택 다이얼로그 끝 -->

<script>
    $(document).ready(function () {
        $("#timestart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#timeend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#timeend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#timestart").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
    function form_selete(id, name) {
        $('#form_search_dialog').dialog('close');
        $('input[name=formid]').val(id);
        $('input[name=formname]').val(name);
    }
    $('#evaluation_add_form').submit(function () {
        var no_val;
        $('input[required]').each(function (i, requiredField) {
            if ($(requiredField).val() == '') {
                alert($(requiredField).attr('message'));
                no_val = true;
            }
        });
        if (no_val == true) {
            return false;
        }
        if ($('input[name="targets[]"]:checked').val() == '' || $('input[name="targets[]"]:checked').val() == undefined) {
            alert('설문대상을 지정해주세요.');
            return false;
        }
    });
    $(function () {
        $("#course_search_dialog").css('zIndex', 100);
        $("#course_search_dialog").dialog({
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            autoOpen: false,
            hide: {
                duration: 1000,
            },
            buttons: {
                "<?php echo get_string('close', 'local_lmsdata'); ?>": function () {
                    $(this).dialog("close");
                }
            }
        });

        // 다이얼로그 오픈
        $("#opener_course").click(function () {
            $("#course_search_dialog").dialog("open");
        });
        // 다이얼로그 검색버튼 클릭
        $("#searchbtn_course").click(function () {

        });
    });
    $(function () {
        $("#form_search_dialog").css('zIndex', 100);
        $("#form_search_dialog").dialog({
            maxWidth: 800,
            maxHeight: 500,
            width: 800,
            height: 500,
            autoOpen: false,
            hide: {
                duration: 1000,
            },
            buttons: {
                "<?php echo get_string('close', 'local_lmsdata'); ?>": function () {
                    $(this).dialog("close");
                }
            }
        });

        // 다이얼로그 오픈
        $("#opener_form").click(function () {
            $("#form_search_dialog").dialog("open");
            $.ajax({url: "get_form.ajax.php",
                success: function (result) {
                    $("#form_search_dialog").html(result);
                }
            });
        });
        // 다이얼로그 검색버튼 클릭
        $("#searchbtn_form").click(function () {

        });
    });
</script>
<?php
include_once ('../../inc/footer.php');
