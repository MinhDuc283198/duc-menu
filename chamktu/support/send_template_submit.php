<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
/**
 * 발송템플릿 submit
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/support/send_template_submit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->libdir . '/datalib.php');

global $DB, $USER;

//발송 템플릿 폼 데이터 받음.
$id = optional_param('id', 0, PARAM_INT);
$type = required_param('type', PARAM_RAW);
//$component = required_param('component', PARAM_RAW); //고유아이디 없어도 될 것으로 판단하여 주석
$classtype = optional_param('classtype', 0, PARAM_INT);
$title = optional_param('title', '', PARAM_TEXT);
$sendtarget = required_param('sendtarget', PARAM_RAW);
$sendtime = optional_param('sendtime', 0, PARAM_INT);
$senddate = optional_param('senddate', 0, PARAM_INT);
$isused = optional_param('isused', 1, PARAM_INT);
$contents = optional_param('contents', '', PARAM_RAW);
$userid = $USER->id;
$link = optional_param('link', '', PARAM_TEXT);

$search_val = optional_param('search_val', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);

if(!$id){
    
    //새로운 데이터 저장
    $data = new stdClass();
    $data->type = $type;
    //$data->component = $component; 
    $data->classtype = $classtype;
    $data->title = $title;
    $data->sendtarget =$sendtarget;
    $data->sendtime = $sendtime;
    $data->senddate = $senddate;
    $data->isused = $isused;
    $data->contents = $contents;
    $data->userid = $userid;
    $data->timecreated = time();
    $data->link = $link;
    
    $newid = $DB->insert_record('lmsdata_sendtemplate',$data);
    
    redirect('send_template.php');
}else{
    
    //기존 데이터를 불러옴
    if($data = $DB->get_record('lmsdata_sendtemplate', array('id'=>$id))){
        
        $data->type = $type;
        //$data->component = $component; 
        $data->classtype = $classtype;
        $data->title = $title;
        $data->sendtarget =$sendtarget;
        $data->sendtime = $sendtime;
        $data->senddate = $senddate;
        $data->isused = $isused;
        $data->contents = $contents;
        $data->userid = $userid;
        $data->timeupdated = time();
        $data->link = $link;
        
        $DB->update_record('lmsdata_sendtemplate',$data);
            
    }
    
    redirect('send_template_write.php?id='.$id.'&page='.$page.'&search_val='.$search_val);
}

?>
