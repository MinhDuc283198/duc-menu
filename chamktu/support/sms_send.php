<?php

// Written at Louisiana State University

require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

 $CFG->debug = (E_ALL | E_STRICT);
$CFG->debugdisplay = 1;
error_reporting($CFG->debug);
ini_set('display_errors', '1');
ini_set('log_errors', '1');

require_once($CFG->dirroot . '/enrol/externallib.php');
require_once($CFG->dirroot . '/chamktu/support/smslib.php');
require_once($CFG->dirroot . '/chamktu/support/smsconfig.php');

global $CFG, $DB;

//require_login();
$smssend = new stdClass();
$smssend->contents = optional_param('contents', '', PARAM_TEXT);
$sendd = optional_param('sendd', '', PARAM_ALPHANUMEXT);
$sendh = optional_param('sendh', 0, PARAM_ALPHANUM);
$sendm = optional_param('sendm', 0, PARAM_ALPHANUM);
$now_send = optional_param('now_send', 0, PARAM_INT);
$sphone = optional_param('sphone', '', PARAM_RAW);
$mailto_type = optional_param('mailto_type', 1, PARAM_INT);
$smssend->subject = optional_param('subject', '', PARAM_RAW);
$mailto = optional_param('mailto', '', PARAM_RAW);

//시간처리
$sendds = explode('-', $sendd);
$sy = $sendds[0];
$sm = $sendds[1];
$sd = $sendds[2];

if ($now_send) {
    $smssend->sendtime = time();
    $smssend->schedule_type = 0;
} else {
    $smssend->sendtime = mktime($sendh, $sendm, 0, $sm, $sd, $sy);
    $smssend->schedule_type = 1;
}

$smssend->sender = optional_param('fullname', '', PARAM_RAW);
$smssend->userid = $USER->id;
$smssend->callback = str_replace("-", "", $sphone);
$smssend->timecreated = time();

$msg_no = set_smssend($smssend);
if(!$msg_no){
    echo 'Set Sms Not Working';
    die();
}
$newemail = $DB->insert_record('lmsdata_sms', $smssend);

//전체 대상일 경우와 지정일 경우 구분하여 발송자 리스트 처리
if ($mailto_type == 2) {
    if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $smssend->callback) || preg_match('/^[0-9]{10,11}$/',$smssend->callback)) {
        //전체발송
        //전체 회원데이터를 얻는다.
        $query = 'select u.* from {user} u
            JOIN {lmsdata_user} ui on ui.userid = u.id';
        $users = $DB->get_records_sql($query);
        $count = 0;
        foreach ($users as $user) {
            if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $user->phone2) || preg_match('/^[0-9]{10,11}$/',$user->phone2)) {

                $user->phone2 = str_replace("-", "", $user->phone2);

                $smssend_user->sms = $newemail;
                $smssend_user->userid = $user->id;
                $smssend_user->phone = $user->phone2;
                $smssend_user->fullname = fullname($user);
                $smssend_user->timecreated = time();

                send_sms($user, $smssend,$msg_no);
                $DB->insert_record('lmsdata_sms_data', $smssend_user);

                $count++;
            }
        }
    }
} else {
    //발송대상 목록을 분해한다. 
    if ($mailto) {

        $maillists = json_decode($mailto, true);

        $count = 0;
        foreach ($maillists as $key => $mails) {

            $mail = explode(';', $mails);

            if ($mail[0] == 'user') {

                $query = 'select u.* from {user} u
                   JOIN {lmsdata_user} ui on ui.userid = u.id
                   where u.id=:id';
                $user = $DB->get_record_sql($query, array('id' => $mail[1]));
                $rcount = $DB->count_records_sql('select count(*) from {lmsdata_sms_data} where sms=:sms and userid=:userid', array('sms' => $newemail, 'userid' => $mail[1]));

                if ($rcount == 0) {
                    if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $user->phone2) || preg_match('/^[0-9]{10,11}$/',$user->phone2)) {

                        $user->phone2 = str_replace("-", "", $user->phone2);
                        $smssend_user = new stdClass();
                        $smssend_user->sms = $newemail;
                        $smssend_user->userid = $user->id;
                        $smssend_user->phone = $user->phone2;
                        $smssend_user->fullname = fullname($user);
                        $smssend_user->timecreated = time();


                        $now = date('Ymdhis', time());
                        $send_date = date('Ymdhis', $send_date);

                        $userinfo = $user->firstname . $user->lastname . '^' . $user->phone2;

                        send_sms($user, $smssend,$msg_no);
                        $DB->insert_record('lmsdata_sms_data', $smssend_user);
                        $count++;
                    }
                }
            } else if ($mail[0] == 'role') {

                if ($mail[1] == 'admin') {
                    $roles = $DB->get_records('role_assignments', array('roleid' => 1));
                } else if ($mail[1] == 'manager') {
                    $roles = $DB->get_records('role_assignments', array('roleid' => 2));
                } else if ($mail[1] == 'teacher') {
                    $roles = $DB->get_records('role_assignments', array('roleid' => 3));
                } else if ($mail[1] == 'student') {
                    $roles = $DB->get_records('role_assignments', array('roleid' => 5));
                }

                foreach ($roles as $role) {

                    $rcount = $DB->count_records_sql('select count(*) from {lmsdata_sms_data} where sms=:sms and userid=:userid', array('sms' => $newemail, 'userid' => $role->userid));

                    if ($rcount == 0) {

                        $query = 'select u.* from {user} u
                            JOIN {lmsdata_user} ui on ui.userid = u.id
                            where u.id=:id';
                        $user = $DB->get_record_sql($query, array('id' => $role->userid));

                        if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $user->phone2) || preg_match('/^[0-9]{10,11}$/',$user->phone2)) {

                            $user->phone2 = str_replace("-", "", $user->phone2);

                            $smssend_user->sms = $newemail;
                            $smssend_user->userid = $user->id;
                            $smssend_user->phone = $user->phone2;
                            $smssend_user->fullname = fullname($user);
                            $smssend_user->timecreated = time();

                            send_sms($user, $smssend,$msg_no);
                            $DB->insert_record('lmsdata_sms_data', $smssend_user);
                            $count++;
                        }
                    }
                }
            } else if ($mail[0] == 'course') {
                //코스에 유저가 있어야 함..
                $params = array('instanceid' => $mail[1], 'contextlevel' => 50);

                $query = 'select mu.* 
                          from {context} mc
                          join {role_assignments} ra on ra.contextid = mc.id
                          join {user} mu on mu.id = ra.userid
                          join {lmsdata_user} lu on lu.userid = mu.id
                          where instanceid = :instanceid and contextlevel = :contextlevel';
                $users = $DB->get_records_sql($query, $params);

                $count = 0;
                foreach ($users as $user) {

                    $rcount = $DB->count_records_sql('select count(*) from {lmsdata_sms_data} where sms=:sms and userid=:userid', array('sms' => $newemail, 'userid' => $user->id));

                    if ($rcount == 0) {
                        if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $user->phone2) || preg_match('/^[0-9]{10,11}$/',$user->phone2)) {

                            $user->phone2 = str_replace("-", "", $user->phone2);

                            $smssend_user->sms = $newemail;
                            $smssend_user->userid = $user->id;
                            $smssend_user->phone = $user->phone2;
                            $smssend_user->fullname = fullname($user);
                            $smssend_user->timecreated = time();

                            send_sms($user, $smssend,$msg_no);
                            $DB->insert_record('lmsdata_sms_data', $smssend_user);
                            $count++;
                        }
                    }
                }
            }
        }
    }
}

header('Location: sms_state.php?id=' . $newemail);
