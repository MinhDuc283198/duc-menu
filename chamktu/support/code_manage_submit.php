<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
/**
 * 코드 submit
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
//로그인 안되어있을때 로그인페이지로 이동.
if (!isloggedin()) {//로그인 되어있는지 확인
    echo '<script type="text/javascript">
            alert("로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.");
            document.location.href="' . $SITECFG->wwwroot . '/main/user/login.php";
        </script>';
}
//파라미터 가져옴
$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$code_category = optional_param('code_category',"",PARAM_TEXT);
$depth = required_param('depth', PARAM_INT); // 1 : 대분류,  2: 소분류
$code = required_param('code', PARAM_TEXT); // 코드값
$codename = optional_param('codename', "", PARAM_TEXT); // 코드이름
$english_codename = optional_param('english_codename', "", PARAM_TEXT); // 영어 코드 이름
$step = optional_param('step', 0, PARAM_INT); //소순서
$status = optional_param('status', 0, PARAM_INT); //
$parentid = optional_param('parentid', 0, PARAM_INT); //
$page = optional_param('page',0,PARAM_INT);
$perpage = optional_param('perpage', 0, PARAM_INT);
$searchtext = optional_param('searchtext','',PARAM_TEXT);
$searcharea = optional_param('searcharea','',PARAM_TEXT);



if($depth==2){
    $code_category=get_code_category2($parentid);
}






//배열을 새로 만듬
$newdata = new stdClass();//stdClass()와 object()의 차이

//값을 지정
$newdata->id = $id;
$newdata->code_category = $code_category;
$newdata->depth = $depth;
$newdata->code = $code;
$newdata->codename = $codename;
$newdata->english_codename = $english_codename;
$newdata->step = $step;
$newdata->status = $status;
$newdata->parentid =$parentid;


   if ($mod == "edit") {//수정시
    $newdata->timemodified = time();
    $DB->update_record('lmsdata_code', $newdata);//table을 update

    redirect($CFG->wwwroot . '/chamktu/support/code_manage.php?searchtext='.$searchtext.'&searcharea='.$searcharea.'&page='.$page.'&perpage='.$perpage);
} else if ($mod == "add") {//등록시
    if($depth==1){
    $ccck = code_category_check($code_category);
    if($ccck){
    echo '<script type="text/javascript">
            alert("코드분류는 중복될 수 없습니다.");
            document.location.href="' . $SITECFG->wwwroot . '/chamktu/support/code_manage_add.php?mod=top";
        </script>';
    return false;
}
}
if($depth==2){
    $ccc = code_check($code,$code_category);
    if($ccc){
    echo '<script type="text/javascript">
            alert("대분류 안에서 코드값은 중복될 수 없습니다.");
            document.location.href="' . $SITECFG->wwwroot . '/chamktu/support/code_manage_add.php?id='.$parentid.'";
        </script>';
    return false;
}
}

   $newdata->timecreated = time();
   $newdata->timemodified = time();   

   $temp = new stdClass();
   $temp->id = $DB->insert_record('lmsdata_code', $newdata);//table에 insert
   $temp->parentid = $temp->id;
   //$sql= "select * from {lmsdata_code} where id=$temp";
   if($depth==1){
   //$sql="update lmsdata_code set parentid=".$temp.""." where id=".$temp."";
   $DB->update_record('lmsdata_code',$temp);
   }


}
redirect($CFG->wwwroot . '/chamktu/support/code_manage.php?searchtext='.$searchtext.'&searcharea='.$searcharea.'&page=1');//목록페이지로 돌아감
?>