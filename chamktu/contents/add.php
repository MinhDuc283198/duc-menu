<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/chamktu/contents/lib.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/notices_write.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->libdir . '/filestorage/file_storage.php');
require_once($CFG->libdir . '/filestorage/stored_file.php');
require_once($CFG->libdir . '/filelib.php');

$context = context_system::instance();

$id = optional_param("id", 0, PARAM_INT);
$mod = optional_param("mod", "", PARAM_TEXT);

$temp = new stdclass();
$transcodingurl = get_config('moodle', 'transcodingurl');
$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/contents/category.js'
);
$fileNameString = '';
?>
<?php include_once('../inc/header.php'); ?>
<link rel="stylesheet" type="text/css" href="./styles/common.css" />
<div id="contents">
    <?php include_once('../inc/sidebar_contents.php'); ?>
    <div id="content">
        <h3 class="page_title">콘텐츠 등록</h3>
        <div class="page_navbar"><a href="index.php"><?php echo get_string('lcms_management', 'local_lmsdata'); ?></a> > 
            <a href="./index.php"><?php echo get_string('lcms_management', 'local_lmsdata'); ?></a> >
            <strong>콘텐츠 등록</strong>
        </div>
        <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="<?php echo './add_submit.php?id=' . $id . '&type=' . $type; ?>" method="POST">
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr>
                        <td class="field_title"><label for="con_name"><?php echo get_string('title', 'local_lmsdata'); ?> <span class="red">*</span></label></td>
                        <td class="field_value">
                            <input type="text" id="con_name" class="w_300" name ="con_name" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">카테고리 <span class="red">*</span></td> <!--운영관리>카테고리관리에 등록된 값-->
                        <td class="field_value" colspan="3">
                            <select title="category01" name="category1" id="content_cata1" onchange="con_cata1_changed(this);"  class="w_160">
                                <option value="0"> - 선택하세요 -</option>
                                <?php
                                $catagories = siteadmin_get_content_category(1);
                                foreach ($catagories as $category) {
                                    echo '<option value="' . $category->id . '"> ' . $category->name . '</option>';
                                }
                                ?>
                            </select>
                            <select title="category02" name="category2" id="content_cata2" class="w_160">
                                <option value="0"> - 선택하세요 -</option>
                            </select>
                        </td>
                    </tr>
                <input type="hidden" title="radio" name="con_type" value="video" >
                <?php if ($mod != 'ref') { ?>
                    <input type="hidden" name="ref_con_type" value="" />
                <?php } else { ?>
                    <input type="hidden" name="con_type" value="ref" />
                    <input type="hidden" name="mode" value="ref" />
                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <?php } ?>
                <tr>
                    <td class="field_title"><?php echo get_string('contents_visibility', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <input type="radio" title="radio" name="share_yn" checked="checked" value="Y"> <?php echo get_string('contents_open', 'local_lmsdata'); ?>
                        <input type="radio" title="radio" name="share_yn" value="N"> <?php echo get_string('contents_private', 'local_lmsdata'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo '교육콘텐츠 여부'; ?></td>
                    <td class="field_value">
                        <input type="radio" title="radio" name="onlymain" checked="checked" value="1"> <?php echo '교육+과정 콘텐츠'; ?>
                        <input type="radio" title="radio" name="onlymain" value="0"> <?php echo '과정 콘텐츠 전용'; ?>
                    </td>
                </tr>            
                <tr>
                    <td class="field_title"><?php echo get_string('contents_fileformat', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <input type="radio" title="radio" name="con_type" value="video" onchange="load_con_form($('#con_form'), './html/write_' + this.value + '.php', 'video');" checked><?php echo get_string('contents_video', 'local_lmsdata'); ?>
                        <input type="radio" title="radio" name="con_type" value="embed" onchange="load_con_form($('#con_form'), './html/write_' + this.value + '.php', 'embed');"> <?php echo get_string('contents_externalcontent', 'local_lmsdata'); ?> 
                    </td>
                </tr>
                <tr> 
                    <td class="field_title"><?php echo get_string('contents_explanation', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <textarea name="con_des" title="con_des" class="ckeditor" id="con_des" rows="7" ></textarea>		  	
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('contents_attachments', 'local_lmsdata'); ?> <span class="red">*</span></td>
                    <td class="field_value number" id="con_form">
                    </td>
                </tr>
                <tr class = "thumbnailcl">                
                    <td class="field_title"> 썸네일 <span class="red">*</span><td>
                        <div class="fileBox" > 
                            <label for="captionFile" class="blue_btn" id="thumbnail_btn" style='float:left'>썸네일</label> 
                            <input type="file" title="썸네일" name="script" size="100" value="" id="captionFile" class="uploadBtn" style="display:none">  
                            <input type="text" class="fileName" readonly="readonly" size="100" value="<?php echo $fileNameString; ?>"> 
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn_area">
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('board_save', 'local_lmsdata'); ?>"  />
                <input type="button" onclick="location.href = 'index.php'" class="normal_btn" value="<?php echo get_string('board_list', 'local_lmsdata'); ?>"  />
            </div> <!-- Bottom Button Area -->

        </form>
    </div>
</div>
<?php include_once('../inc/footer.php'); ?>


<script type="text/javascript">
    var enableconname = 0;
    $(document).ready(function () {

        $('#con_name').focusout(function () {
            var conname = $(this).val();
            if (conname) {
                $.ajax({
                    url: '<?php echo $CFG->wwwroot . "/chamktu/contents/get_contents_name.ajax.php" ?>',
                    method: 'POST',
                    data: {
                        conname: conname,
                        type: 1
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            enableconname = data.result;
                            console.log(data.result);
                        }
                    }
                });
            }
        });

<?php //if(empty($type)){ $type = "embed"; }   ?>
        //load_con_form($('#con_form'), './html/write_<?php echo $type; ?>.php');
        var uploadFile = $('.fileBox input[type=file]');
        $('#frm_popup_submit').submit(function () {
            if ($('input[name=con_name]').val() == '') {
                alert('제목을 입력하세요.');
                return false;
            }
            if (enableconname > 0) {
                alert('다른 제목을 입력해주세요.');
                return false;
            }

            if ($('#content_cata1 option:selected').val() == 0) {
                alert('상위 카테고리를 선택하세요.');
                return false;
            }

            if ($('#content_cata2 option:selected').val() == 0) {
                alert('하위 카테고리를 선택하세요.');
                return false;
            }
            //비디오 업로드 일때 체크
            if ($('input:radio[name=con_type]:checked').val() == 'video') {
                if ($(this).find('input:hidden[name=filename]').val() == '') {
                    alert("비디오 파일을 업로드해주세요");
                    return false;
                }
            }
            if ($('.fileName').val() == '') {
                alert("썸네일을 등록해주세요")
                return false;
            }
            //확장자 체크
            thumbnail_check();

            // 유튜브나 기타 등등 업로드때 체크
//        if($('input:radio[name=con_type]:checked').val() == 'embed') {
//            if($(this).find('input:text[name=emb_code]').val() == '') {
//                embed_check();
//            }
//        }
        });

        uploadFile.on('change', function () {
            if (window.FileReader) {
                var filename = $(this)[0].files[0].name;
            } else {
                var filename = $(this).val().split('/').pop().split('\\').pop();
            }
            $(this).siblings('.fileName').val(filename);
        });

        var url = './html/write_video.php';

        $('#con_form').load(url, function () {
            var $load = $(this);
            //파일선택시 선택한 폼 로드
            $(this).find('input:radio[name=file_type]').click(function () {
                var fl_num = $(this).val();
                for (var i = 1; i <= 2; i++) {
                    $load.find('#file_type' + i).addClass('display_none');
                }
                $load.find('#file_type' + fl_num).removeClass('display_none');
            });

        });

    });

    function remove_file() {
        $("a[name='file_link']").remove();
        $("input[name='remove_button']").remove();
        $("input[name='file_del']").val(1);

    }

    function cc_mark_change(val) {
        switch (val) {
            case 1:
                $('#cc_type1').show();
                $('#cc_type2').hide();
                $('#cc_type3').hide();
                break;
            case 2:
                $('#cc_type1').hide();
                $('#cc_type2').show();
                $('#cc_type3').hide();
                break;
            case 3:
                $('#cc_type1').hide();
                $('#cc_type2').hide();
                $('#cc_type3').show();
                break;
        }
    }
    function load_con_form(frm, url, type) {
        var transinform = '<?php echo $transcodingurl; ?>';
        if (!transinform && url == './html/write_video.php') {
            alert('transcodingurl을 입력해주세요');
            $("input[name='con_type'][value='embed']").prop('checked', true);
            url = './html/write_embed.php';
        }

        //유튜브 일경우 썸네일 안보이게 처리
//    if(type == 'embed') {
//        $('.thumbnailcl').css('display', 'none');
//    } else { 
//        $('.thumbnailcl').css('display', '');
//    }

        frm.load(url, function () {
            var $load = $(this);

            //파일선택시 선택한 폼 로드
            $(this).find('input:radio[name=file_type]').click(function () {
                var fl_num = $(this).val();
                for (var i = 1; i <= 2; i++) {
                    $load.find('#file_type' + i).addClass('display_none');
                }
                $load.find('#file_type' + fl_num).removeClass('display_none');
            });

        });

    }
//썸네일 확장자 체크
    function thumbnail_check() {
        var ext = $('.fileName').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1 && $('input[name=uploadfile]').val() != "") {
            alert('gif,png,jpg,jpeg 파일만 업로드 할수 있습니다.');
            return false;
        }
    }
</script>