<?php
/**
 * 콘텐츠 관리 리스트 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/chamktu/contents/lib.php');
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/course_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';

$userid = optional_param('userid', 0, PARAM_INT);
$ctype = optional_param('ctype', "all", PARAM_RAW);
$public = optional_param('public', "all", PARAM_RAW);

$search = optional_param('search', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$page_num = optional_param('page_num', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

$category1 = optional_param('category1', 0, PARAM_INT); // 부모 카테고리
$category2 = optional_param('category2', 0, PARAM_INT); // 상세 카테고리

$context = context_system::instance();

require_login();

$PAGE->set_context($context);
$PAGE->set_url('/chamktu/contents/index.php');

$like = '';
if (!empty($search)) {
    $like = "and " . $DB->sql_like('title', ':search', false);
}
$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/contents/category.js'
);
/*
  $sql = "select count(id) from {jinoboard_contents} where board = :board " . $like . " and isnotice = 0 order by ref DESC, step ASC";
  $totalcount = $DB->count_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'));
  $total_pages = jinoboard_get_total_pages($totalcount, $perpage);
 */
if (!$excel) {
    include_once('../inc/header.php');
    ?>
    <div id="contents">
        <?php include_once('../inc/sidebar_contents.php'); ?>
        <?php
//                $test = $DB->get_records('lmsdata_category');
//                print_object($test);
        ?>
        <div id="content">
            <h3 class="page_title"><?php echo get_string('contents_contentslist', 'local_lmsdata'); ?></h3>
            <div class="page_navbar"><?php echo get_string('lcms_management', 'local_lmsdata'); ?> > <strong><?php echo get_string('lcms_management', 'local_lmsdata'); ?></strong></div>

                    <!--        <form id="frm_notices_search" class="search_area" action="<?php echo $CFG->wwwroot . '/chamktu/contents/add_once_submit.php' ?>">
                                과정코드이름:<input type="text" name="con_name">
                                과정차시:<input type="number" name="classnum">
                                과정차수(페이지수):<input type="number" name="classpage">            
                                <input type="hidden" name="page" value="<?php echo $page ?>">
                                <input type="submit" class="search_btn" id="search" value="콘텐츠 일괄 업로드">
                            </form>-->
            <!-- 검색폼 시작 -->

            <form id="frm_content_search" class="search_area">
                <input type="hidden" title="page_num" name="page_num" value="1" />
                <input type="hidden" name="type" value="<?php echo $type; ?>">
                <select name="perpage" title="page" class="w_160" onchange="this.form.submit();">
                    <?php
                    $nums = array(10, 20, 30, 50);
                    foreach ($nums as $num) {
                        $selected = ($num == $perpage) ? 'selected' : '';

                        echo '<option value="' . $num . '" ' . $selected . '>' . get_string('showperpage', 'local_jinoboard', $num) . '</option>';
                    }
                    ?>
                </select>
                <select title="category01" name="category1" id="content_cata1" onchange="con_cata1_changed2(this);"  class="w_160">
                    <option value="0"> - 선택하세요 -</option>
                    <?php
                    $catagories = siteadmin_get_content_category(1);
                    foreach ($catagories as $category) {
                        if ($category->id == $category1) {
                            $selected = ' selected="selected" ';
                        } else {
                            $selected = '';
                        }
                        echo '<option value="' . $category->id . ' " ' . $selected . '> ' . $category->name . '</option>';
                    }
                    ?>
                </select>
                <select title="category02" name="category2" id="content_cata2" class="w_160">
                    <option value="0"> - 선택하세요 -</option>
                    <?php
                    if ($category2) {
                        $catagories_detail = siteadmin_get_content_category(2, $category1);
                        foreach ($catagories_detail as $category) {
                            if ($category->id == $category2) {
                                $selected = 'selected';
                            } else {
                                $selected = '';
                            }
                            echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                        }
                    } else if($category1) {
                         $catagories_detail = siteadmin_get_content_category(2, $category1);
                        foreach ($catagories_detail as $category) {
                            echo '<option value="' . $category->id . '" > ' . $category->name . '</option>';
                        }
                    }
                    ?>
                </select>
    <!--                <select name="target" title="target" class="w_160">
            <option value=""><?php echo get_string('contents_all', 'local_lmsdata'); ?></option>
                <?php
                $catagories_detail = siteadmin_get_content_category(2, $category1);
                foreach ($catagories_detail as $category) {
                    if ($category->id == $category2) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
                    echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                }
                ?>
        </select>-->
                <input type="text" title="search" name="search"  id="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('input', 'local_jinoboard'); ?>">
                <input type="submit" class="search_btn" id="searchbtn" value="<?php echo get_string('search', 'local_jinoboard'); ?>">
            </form>
            <!--input type="button" value="<?php //echo get_string('contents_contenthistory', 'local_lmsdata')      ?>" onclick="location.href = 'history.php'" class="blue_btn" style="float:right;"/-->
            <!-- 검색폼 종료 -->
            <form id="delete_form" action="deletes.php" method="POST">
                <table cellspacing="0" cellpadding="0">
                    <caption class="hidden-caption">콘텐츠 리스트</caption>
                    <tr>
                        <th scope="row" style="width:5%;"><input type="checkbox" title="check" id="allcheck" style="margin: 0 !important;"/></th>
                        <th scope="row" style="width:5%;"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                        <th scope="row" style="width:5%;">상위<?php echo get_string('category', 'local_lmsdata'); ?></th>
                        <th scope="row" style="width:5%;">하위<?php echo get_string('category', 'local_lmsdata'); ?></th> 
                        <th scope="row"><?php echo get_string('title', 'local_lmsdata'); ?></th>
                        <th scope="row" style="width:15%;"><?php echo get_string('contents_kinds', 'local_lmsdata'); ?></th>
                        <th scope="row" style="width:10%;"><?php echo get_string('contents_visibility', 'local_lmsdata'); ?></th>
                        <th scope="row" style="width:10%;"><?php echo get_string('update_date', 'local_lmsdata'); ?></th>
                    </tr>
                    <?php
                    $datas = get_contents_list($userid, $category1, $category2, $search, $page_num, $perpage);
                    foreach ($datas->files as $file) {
                        if (!$file->gname) {
                            $file->gname = '-';
                        }
                        switch ($file->con_type) {
                            case "word" : $type_txt = get_string('document', 'local_repository');
                                break;
                            case "html" :
                            case "html2" : $type_txt = get_string('html', 'local_repository');
                                break;
                            case "video" : $type_txt = get_string('video', 'local_repository');
                                break;
                            case "embed" : $type_txt = get_string('embed', 'local_repository');
                                break;
                            default : $type_txt = "-";
                                break;
                        }
                        ?>
                        <tr>
                            <td style="width:5%;"><input type="checkbox" title="check" name="check[<?php echo $file->conid; ?>]" class="check_delete" style="margin: 0 !important;"/></td>
                            <td style="width:5%;"><?php echo $datas->num--; ?></td>
                            <td style="width:15%;">
                                <?php
                                $cataname = '';
                                $catas2 = siteadmin_get_content_category(1);
                                foreach ($catas2 as $cata2) {
                                    if ($cata2->id == $file->parent) {
                                        $cataname .= $cata2->name;
                                    }
                                }
                                echo $cataname;
                                ?>
                            </td>
                            <td style="width:15%;">
                                <?php
                                $cataname2 = '';
                                $catas = siteadmin_get_content_category(2); //카테고리 이름, 번호 구하는 함수
                                foreach ($catas as $cata) {
                                    if ($cata->id == $file->category) {
                                        $cataname2 = $cata->name;
                                    }
                                }
                                echo $cataname2;
                                ?>
                            </td>
                            <td>
                                <a href="detail.php?id=<?php echo $file->id ?>&userid=<?php echo $userid; ?>">
                                    <?php echo $file->con_name; ?>
                                </a>
                            </td>
                            <td style="width:15%;"><?php echo $type_txt; ?></td>
                            <td style="width:10%;">
                                <?php
                                $public = (!$file->share_yn || $file->share_yn == 'N') ? get_string('n', 'local_repository') : get_string('y', 'local_repository');
                                echo $public;
                                ?>
                            </td>
                            <td style="width:10%;"><?php echo date('Y.m.d', $file->reg_dt); ?></td>
                        </tr>   
                        <?php
                    }
                    if (empty($datas->total_count)) {
                        ?>
                        <tr>
                            <td colspan="8"><?php echo get_string('nocontents', 'local_repository'); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <div class="btn_area">
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                    <!--input type="button" value="<?php //echo get_string('contents_filebatchregistration', 'local_lmsdata')      ?>" onclick="location.href = 'excell_add.php'" class="blue_btn" style="float:right;"/-->
                    <input type="button" value="<?php echo get_string('contents_registration', 'local_lmsdata') ?>" onclick="location.href = 'add.php'" class="blue_btn" style="float:right;"/>
                    <input type="button" id="delete_notice" class="normal_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left" />
                </div>    
            </form>
            <?php
            $page_params = array();
            $page_params['category2'] = $category2;
            $page_params['category1'] = $category1;
            $page_params['perpage'] = $perpage;
            $page_params['search'] = $search;
            if (($datas->total_count / $perpage) > 1) {
                print_paging_navbar_script($datas->total_count, $page_num, $perpage, 'javascript:cata_page(:page);');
            }
            ?>
            <!-- Breadcrumbs End -->
        </div> <!-- Table Footer Area End -->
    </div>
    </div>
    <?php
} else {
    $chkdatas = get_contents($userid, $category2, $search, $page_num, $perpage);
    $datas = get_contents($userid, $category2, $search, $page_num, $chkdatas->total_count);
    $fields = array(
        '번호',
        '상위 카테고리',
        '하위 카테고리',
        '제목',
        '종류',
        '공개여부',
        '등록일'
    );

    $date = date('Y-m-d', time());
    $filename = '콘텐츠리스트_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($datas->files as $file) {
        $public = (!$file->share_yn || $file->share_yn == 'N') ? get_string('n', 'local_repository') : get_string('y', 'local_repository');
        switch ($file->con_type) {
            case "word" : $type_txt = get_string('document', 'local_repository');
                break;
            case "html" :
            case "html2" : $type_txt = get_string('html', 'local_repository');
                break;
            case "video" : $type_txt = get_string('video', 'local_repository');
                break;
            case "embed" : $type_txt = get_string('embed', 'local_repository');
                break;
            default : $type_txt = "-";
                break;
        }
        $catas2 = siteadmin_get_content_category(1);
        foreach ($catas2 as $cata2) {
            if ($cata2->id == $file->parent) {
                $categrytxt2 = $cata2->name;
            }
        }
        $catas = siteadmin_get_content_category(2);
        foreach ($catas as $cata) {
            if ($cata->id == $file->category) {
                $categrytxt = $cata->name;
            }
        }

        $col = 0;
        $worksheet[0]->set_column(1, 1, 15, array('v_align' => 'top')); // 상위 카테고리
        $worksheet[0]->set_column(1, 2, 25, array('v_align' => 'top')); // 하위 카테고리
        $worksheet[0]->set_column(1, 3, 25, array('v_align' => 'top')); // 제목
        $worksheet[0]->set_column(1, 4, 25, array('v_align' => 'top')); // 종류

        $worksheet[0]->write($row, $col++, $datas->num--);
        $worksheet[0]->write($row, $col++, $categrytxt2);
        $worksheet[0]->write($row, $col++, $categrytxt);
        $worksheet[0]->write($row, $col++, $file->con_name);
        $worksheet[0]->write($row, $col++, $type_txt);
        $worksheet[0]->write($row, $col++, $public);
        $worksheet[0]->write($row, $col++, date('Y.m.d', $file->reg_dt));

        $row++;
    }

    $workbook->close();
    die;
}
?>
<script>
    /**
     * 페이징 기능
     * @param {type} page
     * @returns {undefined}
     */
    function cata_page(page) {
        $('input[name=page_num]').val(page);
        $('#frm_content_search').submit();
    }

    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content",
            header: "h3",
            active: false
        });
        $("#accordion").accordion("option", "icons", null);
    });
//	$('#accordion input[type="checkbox"]').click(function(e) {
//		e.stopPropagation();
//	});

    $(document).ready(function () {
        /**
         * 체크박스 all 이벤트
         */
        $('#allcheck').click(function () {
            if ($('#allcheck').is(":checked")) {
                $(".check_delete").each(function () {
                    this.checked = true;
                });
            } else {
                $(".check_delete").each(function () {
                    this.checked = false;
                });
            }
        });
        /**
         * 삭제 기능
         */
        $('#delete_notice').click(function () {
            if (confirm("삭제 하시겠습니까? 선택된 콘텐츠와 연관된 모든 파일과 학습활동도 삭제됩니다.")) {
                $('#delete_form').submit();
            }
        });
        /**
         * 검색기능
         */
        $('#searchbtn').click(function () {
//            var content_cata1 = $('#content_cata1').val();
//            var content_cata2 = $('#content_cata2').val();
////            if (content_cata2 == 0) {
////                $('#content_cata1').val(0);
////                alert('세부 카테고리도 선택해주세요.');
////                return false;
////            }
//            var search = $('#search').val();
//            var page_num = $('input[name=page_num]').val();

//            location.href = "./notice.php?searchfield=" + searchfield + "&searchvalue=" + searchval + "&timestart=" + timestart + "&timeend=" + timeend;
        });
    });
    /**
     * 엑셀다운로드
     
     * @returns {undefined}     */
    function course_list_excel() {
//        $("input[name='excel']").val(1);
//        $("#course_search").submit();
//        $("input[name='excel']").val(0);
        var url = 'index.php?excel=1';
        document.location.href = url;
    }
</script>
<?php include_once('../inc/footer.php'); ?>
