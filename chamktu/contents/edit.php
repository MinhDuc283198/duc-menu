<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/chamktu/contents/lib.php');
?>
<link rel="stylesheet" type="text/css" href="./styles/common.css" />
<?php
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/notices_write.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->libdir . '/filestorage/file_storage.php');
require_once($CFG->libdir . '/filestorage/stored_file.php');
require_once($CFG->libdir . '/filelib.php');

$context = context_system::instance();
$nav = array('top' => 'site', 'left' => 'board', 'sub' => 'notice');

$id = optional_param("id", 0, PARAM_INT);
$ref = optional_param("ref", 0, PARAM_INT);
$mod = optional_param("mode", "edit", PARAM_TEXT);

$temp = new stdclass();

$sql = "select "
        . "rep.id repid , rep.referencecnt , "
        . "con.id as con_id,con.cc_type,con.cc_mark,con.author,con.teacher,con.con_name,con.con_type,con.con_des,"
        . "con.update_dt,con.share_yn,con.data_dir,con.embed_type,con.embed_code, con.category, con.onlymain, "
        . "rep_group.name as gname "
        . "from {lcms_repository} rep "
        . "join {lcms_contents} con on con.id= rep.lcmsid "
        . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
        . "where rep.id= :id";

$data = $DB->get_record_sql($sql, array('id' => $id));

$fs = get_file_storage();
$files = $fs->get_area_files(1, 'local_repository', 'thumbnail', $data->con_id, "", false);
foreach ($files as $file) {
    $fileNameString = $file->get_filename();
}

if (!$ref) {
    $type = $data->con_type;
} else {
    $type = $data->ref_con_type;
}
$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/contents/category.js'
);
?>
<?php include_once('../inc/header.php'); ?>
<div id="contents">
    <?php include_once('../inc/sidebar_contents.php'); ?>
    <div id="content">
        <h3 class="page_title">콘텐츠 수정</h3>
        <div class="page_navbar"><a href="index.php"><?php echo get_string('lcms_management', 'local_lmsdata'); ?></a> > 
            <a href="./index.php"><?php echo get_string('lcms_management', 'local_lmsdata'); ?></a> >
            <strong>콘텐츠 수정</strong></div>
        <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="<?php echo './add_submit.php?mode=edit&id=' . $id . '&type=' . $type; ?>" method="POST">
            <table cellpadding="0" cellspacing="0" class="detail">

                <tbody>
                    <tr>
                        <td class="field_title"><?php echo get_string('title', 'local_lmsdata'); ?> <span class="red">*</span></td>
                        <td class="field_value">
                            <input type="text" class="w_300" id ="con_name" name ="con_name" value="<?php echo (!empty($data->con_name)) ? $data->con_name : ""; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">카테고리 <span class="red">*</span></td> <!--운영관리>카테고리관리에 등록된 값-->
                        <td class="field_value" colspan="3">
                            <select title="category01" name="category1" id="content_cata1" onchange="con_cata1_changed(this);"  class="w_160">
                                <option value="0"> - 선택하세요 -</option>
                                <?php
                                $catagories = siteadmin_get_content_category(1);
                                $parentid = $DB->get_field('lmsdata_category', 'parent', array('id' => $data->category));
                                foreach ($catagories as $category) {
                                    if ($category->id == $parentid) {
                                        $selected = 'selected';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                                }
                                ?>
                            </select>
                            <select title="category02" name="category2" id="content_cata2" class="w_160">
                                <option value="0"> - 선택하세요 -</option>
                                <?php
                                $catagories = siteadmin_get_content_category(2, $parentid);
                                foreach ($catagories as $category) {
                                    if ($category->id == $data->category) {
                                        $selected = 'selected';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('contents_visibility', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="radio" name="share_yn" <?php
                            if ($data->share_yn == 'Y') {
                                echo 'checked';
                            }
                            ?> value="Y"> <?php echo get_string('contents_open', 'local_lmsdata'); ?>
                            <input type="radio" name="share_yn" <?php
                            if ($data->share_yn == 'N') {
                                echo 'checked';
                            }
                            ?> value="N"> <?php echo get_string('contents_private', 'local_lmsdata'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo '교육콘텐츠 여부'; ?></td>
                        <td class="field_value">
                            <input type="radio" name="onlymain" <?php
                            if ($data->onlymain == 1) {
                                echo 'checked';
                            }
                            ?> value="1"> <?php echo '교육+과정 콘텐츠'; ?>
                            <input type="radio" name="onlymain" <?php
                            if ($data->onlymain == 0) {
                                echo 'checked';
                            }
                            ?> value="0"> <?php echo '과정 콘텐츠 전용'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo '개선과정 컨텐츠'; ?></td>
                        <td class="field_value">
                            <input type="radio" name="better" value="1"> <?php echo '예'; ?>
                            <input type="radio" name="better"checked value="0"> <?php echo '아니오'; ?>
                        </td>
                    </tr>
                <input type="hidden" name="con_type" value="<?php echo $type; ?>">
                <?php
                switch ($type) {
                    case "word" : $type_txt = get_string('document', 'local_repository');
                        break;
                    case "html" :
                    case "html2" : $type_txt = get_string('html', 'local_repository');
                        break;
                    case "video" : $type_txt = get_string('video', 'local_repository');
                        break;
                    case "embed" : $type_txt = get_string('embed', 'local_repository');
                        break;
                    default : $type_txt = "<?php echo get_string('contents_attachments', 'local_lmsdata'); ?>";
                        break;
                }
                ?>
                <tr>
                    <td class="field_title"><?php echo get_string('contents_fileformat', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <?php echo $type_txt; ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('contents_explanation', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <textarea style="width: 98%" id="editor" name="con_des" ><?php echo (!empty($data->con_des)) ? $data->con_des : "" ?></textarea>			  	
                    </td>
                </tr>
                <tr>
                    <td class="field_title" id="rowspan_td" rowspan="1"><?php echo get_string('contents_attachments', 'local_lmsdata'); ?> <span class="red">*</span></td>
                    <td class="field_value number">
                        <input type="hidden" name="stay_file" value="0">
                        <input type="checkbox" name="stay_file" onclick="file_change()" value="1">
                        <?php echo get_string('filechange', 'local_repository'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_value number" id="con_form" style="display: none;" ></td>
                </tr>
                <tr class="thumbnailcl">
                    <td class="field_title"> 썸네일 <span class="red">*</span><td>
                        <div class="fileBox" > 
                            <label for="captionFile" class="blue_btn" id="thumbnail_btn" style='float:left'>썸네일</label> 
                            <input type="file" title="썸네일" name="script" size="100" value="" id="captionFile" class="uploadBtn" style="display:none">  
                            <input type="text" class="fileName" readonly="readonly" size="100" style="float:left;" value="<?php echo $fileNameString; ?>"> 
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="btn_area">
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('board_save', 'local_lmsdata'); ?>"  />
                <input type="button" onclick="location.href = 'index.php'" class="normal_btn" value="<?php echo get_string('board_list', 'local_lmsdata'); ?>"  />
            </div> <!-- Bottom Button Area -->

        </form>
    </div>
</div>
<?php include_once('../inc/footer.php'); ?>

<script type="text/javascript">
    var enableconname = 0;

    var filetype = '<?php echo $type ?>';
//    if(filetype == 'embed') {
//        $('.thumbnailcl').css('display', 'none');
//    }

    $('#frm_popup_submit').submit(function () {
        
        if ($('input[name=con_name]').val() == '') {
            alert('제목을 입력하세요.');
            return false;
        }

        if (enableconname > 0) {
            alert('다른 제목을 입력해주세요.');
            return false;
        }

        if ($('#content_cata1 option:selected').val() == 0) {
            alert('상위 카테고리를 선택하세요.');
            return false;
        }

        if ($('#content_cata2 option:selected').val() == 0) {
            alert('하위 카테고리를 선택하세요.');
            return false;
        }

        if (filetype != 'embed') {
            if ($('#con_form').attr('style') == '') {
                if ($(this).find('input:hidden[name=filename]').val() == '') {
                    alert("비디오 파일을 업로드해주세요");
                    return false;
                }
            }
            if ($('.fileName').val() == '') {
                alert("썸네일을 등록해주세요")
                return false;
            }

            if (thumbnail_check() == false) {
                return false;
            } else {
                return true;
            }
        }
    });
    var editor = CKEDITOR.replace('editor', {
        language: '<?php echo current_language(); ?>',
        filebrowserBrowseUrl: '../js/ckfinder-2.4/ckfinder.html',
        filebrowserImageBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Flash',
        filebrowserUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
    CKFinder.setupCKEditor(editor, '../');

    function file_change() {
        if ($('input[name=stay_file]').is(':checked')) {
            $('#rowspan_td').attr('rowspan', 2);
            $('#con_form').attr('style', '');
        } else {
            $('#rowspan_td').attr('rowspan', 1);
            $('#con_form').attr('style', 'display:none;');
        }
    }
    $(document).ready(function () {
        load_con_form($('#con_form'), './html/write_<?php echo $type; ?>.php?con_id=<?php echo $data->con_id ?>&id=<?php echo $id; ?>&userid=<?php echo $USER->id; ?>&wwwroot=<?php echo $CFG->wwwroot; ?>&embed_code=<?php echo $data->embed_code; ?>&mode=edit');

        $('#con_name').focusout(function () {
            var conname = $(this).val();
            var repid = '<?php echo $data -> repid ;?>';
            if (conname) {
                $.ajax({
                    url: '<?php echo $CFG->wwwroot . "/chamktu/contents/get_contents_name.ajax.php" ?>',
                    method: 'POST',
                    data: {
                        conname: conname,
                        repid: repid,
                        type: 2
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            enableconname = data.result;
                            console.log(data.result);
                        }
                    }
                });
            }
        });
    });
    function remove_file() {
        $("a[name='file_link']").remove();
        $("input[name='remove_button']").remove();
        $("input[name='file_del']").val(1);

    }
    function cc_mark_change(val) {
        switch (val) {
            case 1:
                $('#cc_type1').show();
                $('#cc_type2').hide();
                $('#cc_type3').hide();
                break;
            case 2:
                $('#cc_type1').hide();
                $('#cc_type2').show();
                $('#cc_type3').hide();
                break;
            case 3:
                $('#cc_type1').hide();
                $('#cc_type2').hide();
                $('#cc_type3').show();
                break;
        }
    }
    function load_con_form(frm, url) {

        frm.load(url, function () {

            var $load = $(this);

            //파일선택시 선택한 폼 로드
            $(this).find('input:radio[name=file_type]').click(function () {
                var fl_num = $(this).val();
                for (var i = 1; i <= 2; i++) {
                    $load.find('#file_type' + i).addClass('display_none');
                }
                $load.find('#file_type' + fl_num).removeClass('display_none');
            });

        });
    }
//썸네일 확장자 체크
    function thumbnail_check() {
        var ext = $('.fileName').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1 && $('input[name=uploadfile]').val() != "") {
            alert('gif,png,jpg,jpeg 파일만 업로드 할수 있습니다.');
            return false;
        } else {
            return true;
        }
    }
</script>