<?php

/**
 * 과정검색 ajax
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$con_name = required_param('conname', PARAM_TEXT);
$type = optional_param('type', 0, PARAM_INT);
$returnvalue = new stdClass();
$conname = '';
if ($type == 1) {
    $sqlconname = "select count(rep.id) "
            . "from {lcms_repository} rep "
            . "join {lcms_contents} con on con.id= rep.lcmsid "
            . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
            . "where con.con_name = :con_name";
    $conname = $DB->count_records_sql($sqlconname, array('con_name' => $con_name));
    $returnvalue->status = 'success';
} else if ($type == 2) {
    $repid = optional_param('repid', 0, PARAM_INT);
    $sqlconname = "select count(rep.id) "
            . "from {lcms_repository} rep "
            . "join {lcms_contents} con on con.id= rep.lcmsid "
            . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
            . "where con.con_name = :con_name and rep.id != :repid";
    $conname = $DB->count_records_sql($sqlconname, array('con_name' => $con_name, 'repid' => $repid));
    $returnvalue->status = 'success';
}
$returnvalue->result = $conname;

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
