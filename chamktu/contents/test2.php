<!DOCTYPE html>
<html>
    <head>
        <title>Flowplayer quick start</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="./viewer/jquery-ui-1.10.3.custom.css" />
        <script type="text/javascript" src="./viewer/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="./viewer/jquery-ui-1.10.3.custom.min.js"></script>
        <link rel="stylesheet" href="//releases.flowplayer.org/7.0.4/skin/skin.css">
        <script src="./viewer/flowplayer7/flowplayer.min.js"></script>
        <script src="./viewer/flowplayer7/flowplayer.speed-menu.js"></script>
        <!--<script src="https://player.vimeo.com/api/player.js"></script>-->
    </head>
<body>
    <div id="flowplayer" style="width:700px; height: 400px;">
    <div class="buttons">
        <span>0.75x</span>
        <span class="active">1x</span>
        <span>1.25x</span>
        <span>1.5x</span>
    </div>
  </div>
    
    
<script>
$('#flowplayer').flowplayer({
    clip: {
        sources: [
            { 
                type: "video/mp4",
                src:  "http://cdn.carrotenglish.com/imooc/cafeteria/CKC1609/mp4/CKC160936.mp4" 
            }
        ]
    },
    speeds: [0.75,1,1.25,1.5,2.0],
});

</script>
</body>
</html>