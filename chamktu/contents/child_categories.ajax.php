<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once($CFG->dirroot.'/chamktu/contents/lib.php');

$pid = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$catagories = siteadmin_get_content_category(2, $pid);
if($catagories) {
    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories;
    foreach ($catagories as $catagory) {
        $returnvalue->selecttag .= '<option value="'.$catagory->id.'">'.$catagory->name.'</option>';
    }
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);