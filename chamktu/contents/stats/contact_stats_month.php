<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);

require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname (__FILE__)).'/lib/paging.php';
require_once dirname(dirname (__FILE__)).'/lib.php';



// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/stats/contact_stats_month.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage     = optional_param('page', 1, PARAM_INT);
$perpage      = optional_param('perpage', 31, PARAM_INT);
$year         = optional_param('year', 0, PARAM_INT);
$mon         = optional_param('mon', 0, PARAM_INT);

// 현재 년도, 학기
if(!$year) {
    $year = get_config('moodle', 'haxa_year'); 
}
if(!$mon){
    $date = date("Y-m");
    $mon = date("m");
}else{
    $date = $year.'-'.$mon;
} 

$sql_select  = "SELECT FROM_UNIXTIME(`timecreated`,'%d') as hour, COUNT(*) AS count ";

$sql_from    = " FROM {logstore_standard_log}  ";

$page_params = array();
$sql_where = '';
$conditions = array('action = :isloggedin');
$param['isloggedin'] = 'loggedin';

    $date_start =  strtotime($date.' 00:00:00');
    
    $last_day = date('t',$date_start);    
    $date_end =  strtotime($date.'-'.$last_day .' 23:59:59');
    $conditions[] = "(timecreated > :date_start AND timecreated < :date_end)";
    $param['date_start'] = $date_start;
    $param['date_end'] = $date_end;
    
if($conditions) $sql_where = ' WHERE '.implode(' AND ',$conditions);
$sql_groupby = " GROUP BY FROM_UNIXTIME(`timecreated`,'%d')";

$contact_stats = $DB->get_records_sql($sql_select.$sql_from.$sql_where.$sql_groupby, $param, ($currpage-1)*$perpage, $perpage);
//$count_contact_stats = $DB->count_records_sql("SELECT COUNT(*) ".$sql_from.$sql_where, $param);

$js = array(
    $CFG->wwwroot.'/chamktu/manage/course_list.js'
);
?>

<?php include_once ('../inc/header.php');?>
<div id="contents">
    <?php include_once ('../inc/sidebar_stats.php');?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('stats_loginstats', 'local_lmsdata'); ?></h3>
        <p class="page_sub_title"> <?php echo get_string('stats_longtext1', 'local_lmsdata'); ?></p>
        <div class="siteadmin_tabs">
            <a href="contact_stats_day.php"><p class="black_btn"><?php echo get_string('stats_daily', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_month.php"><p class="black_btn black_btn_selected"><?php echo get_string('stats_monthly', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_year.php"><p class="black_btn"><?php echo get_string('stats_yearly', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_term.php"><p class="black_btn"><?php echo get_string('stats_periodsearch', 'local_lmsdata'); ?></p></a>
        </div>
        <div class="page_navbar"><a href="./contact_stats_month.php"><?php echo get_string('stats_management', 'local_lmsdata'); ?></a> > <strong><?php echo get_string('stats_loginstats', 'local_lmsdata'); ?></strong></div>
        
        <div class="down_area">
             <input type="submit" onclick="course_all_excel(); return false;" class="red_btn" value="<?php echo get_string('stats_longtext4', 'local_lmsdata'); ?> (*.xls)" style="float:left;"/>
             <p style="float:left; margin: 4px 0 0 10px;"><?php echo get_string('stats_longtext2', 'local_lmsdata'); ?></p>
        </div> <!--Down Area End-->
        
        <form name="" id="course_search" class="search_area" action="contact_stats_month.php" method="get">
            <input type="hidden" name="page" value="1" />
            
            <select name="year" class="w_80">
                <option value="0"> - <?php echo get_string('contents_now', 'local_lmsdata'); ?> -</option>
                <?php 
                $years = lmsdata_get_years();
                foreach($years as $v=>$y) {
                    $selected = '';
                    if($v == $year) {
                        $selected = ' selected';
                    }
                    echo '<option value="'.$v.'"'.$selected.'> '.$y.'</option>';
                }
                ?>
            </select>
            <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="mon" class="w_80">
                <option value="0"> - <?php echo get_string('contents_now', 'local_lmsdata'); ?> -</option>
                <?php 
                $mons = lmsdata_get_mons($mon);
                echo $mons;
                ?>
            </select>
            <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
            <input type="submit" class="blue_btn" value="<?php echo get_string('stats_search', 'local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/>          
        </form><!--Search Area2 End-->
        <table>
            <tr>
                <th style='width:20%;'><?php echo get_string('stats_accesstime', 'local_lmsdata'); ?></th>
                <th style='width:20%;'><?php echo get_string('stats_accesscount', 'local_lmsdata'); ?></th>
                <th style='width:50%;'><?php echo get_string('stats_accesschat', 'local_lmsdata'); ?></th>
                <th style='width:10%;'><?php echo get_string('stats_accesslog', 'local_lmsdata'); ?></th>
            </tr>
            <?php
            if(!$contact_stats) { ?>
            <tr>
                <td colspan="24">통계내용이 없습니다.</td>
            </tr>
            <?php } else {
     foreach($contact_stats as $contact_stat) {         
         $a2 = $contact_stat->count;
         if($a1<$a2){
             $a1 = $a2;
         }         
     }
            foreach($contact_stats as $contact_stat) {
            ?>
            <tr>
                <td style='width:20%;'><?php echo $contact_stat->hour.get_string('contents_day', 'local_lmsdata'); ?></td>
                <td style='width:20%;'><?php echo $contact_stat->count.'명'; ?></td>
                <td style='width:50%;'><div style="width: <?php echo ($contact_stat->count/$a1)*100?>%; height: 50%;background-color: #a1a1a8;"></div></td>
                <td style='width:10%;'><?php echo get_string('stats_view', 'local_lmsdata'); ?></td>
            </tr>
            <?php }} ?>
        </table><!--Table End-->
        <?php
         print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        ?>
    </div><!--Content End-->
    
</div> <!--Contents End-->
<script type="text/javascript">
    function course_edit_popup(id) {
        var tag = $("<div></div>");
        $.ajax({
          url: '<?php echo $SITECFG->wwwroot.'/chamktu/stats/course_form.php'; ?>',
          data: {
              parent: $('[name=parent]').val(),
              category: $('[name=category]').val(),
              id: id
          },
          success: function(data) {
            tag.html(data).dialog({
                title: '<?php echo get_string('stats_learningactivitystatus', 'local_lmsdata'); ?>',
                modal: true,
                width: 600,
                maxHeight: getWindowSize().height - 20,
                close: function () {
                    $( this ).dialog('destroy').remove()
                }
            }).dialog('open');
          }
        });
    }
        function course_all_excel() {
        <?php
        $query_string = '';
        if(!empty($param)) {
            $query_array = array();
            foreach($param as $key=>$value) {
                $query_array[] = urlencode( $key ) . '=' . urlencode( $value );
            }
            $query_string = '?'.implode('&', $query_array);
        }
        ?>
        var url = "course_all.excel.php<?php echo $query_string; ?>";
        
        document.location.href = url;
    }
</script>
 <?php include_once ('../inc/footer.php');?>

