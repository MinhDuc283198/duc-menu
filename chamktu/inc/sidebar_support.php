<?php
$submenu = basename($_SERVER['PHP_SELF'], '.php');
?>
<div id="sidebar">
    <div class="menu_title"><h2><?php echo get_string('site_management', 'local_lmsdata'); ?></h2></div>
    <ul class="submenu">
        <li<?php echo starts_with($submenu, 'evaluation') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/evaluation/survey_form.php">설문관리</a>
            <?php
            if (starts_with($submenu, 'survey') || starts_with($submenu, 'evaluation')) {
                ?>
                <ul>
                    <li<?php echo (starts_with($submenu, 'survey_form') || $submenu == "evaluation_add" || starts_with($submenu, 'evaluation_categor') || starts_with($submenu, 'evaluation_modify')|| starts_with($submenu, 'evaluation_question_')) ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/evaluation/survey_form.php"><?php echo get_string('survey_form', 'local_lmsdata'); ?></a></li> 
                    <li<?php echo (starts_with($submenu, 'survey_list') || $submenu == "survey_result" )? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/evaluation_survey/survey_list.php"><?php echo get_string('survey', 'local_lmsdata'); ?></a></li>
                </ul>
            <?php } ?>
        </li>
        <li<?php echo starts_with($submenu, 'popup') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/popup.php"><?php echo get_string('popup_manage', 'local_lmsdata'); ?></a></li>
        <li<?php echo starts_with($submenu, 'infadmin') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/users/infadmin.php"><?php echo get_string('admin_management', 'local_lmsdata'); ?></a></li>
        <li<?php echo starts_with($submenu, 'main_menu') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/main_menu.php">메뉴관리</a></li>
        <!--<li<?php echo starts_with($submenu, 'code_manage') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/code_manage.php">코드관리</a></li>-->
        <li<?php echo starts_with($submenu, 'send_template') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/send_template.php">발송템플릿관리</a></li>
        <li<?php echo starts_with($submenu, 'banner') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/banner/banner.php">배너관리</a></li>
        <li<?php echo starts_with($submenu, 'mainimg') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/mainimg/mainimg.php">메인 상단이미지 관리</a></li>
        <!--<li<?php echo starts_with($submenu, 'footer') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/admin/settings.php?section=theme_oklassedu_footer">푸터관리</a></li>-->
        <li<?php echo starts_with($submenu, 'category') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/category.php">카테고리관리</a></li>
        <li<?php echo starts_with($submenu, 'privacy_log') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/privacy_log.php">개인정보 조회 로그</a></li>
    </ul>

    <?php include_once ('../inc/nav.php'); ?>

</div><!--Sidebar End-->
