<?php
/**
* Sidebar for siteadmin
*
* @package newoklass
* @copyright Copyright (c) 2016 (oklass)
* @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/
$submenu = trim(basename($_SERVER['PHP_SELF']), '.php');
$eval_on = false;
?>
<div id="sidebar">
    <div class="menu_title"><h2><?php echo get_string('enrol_management', 'local_lmsdata'); ?></h2></div>
    <ul class="submenu">
        <li<?php echo starts_with($submenu, 'edu_enrolment_list') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/enrol/edu_enrolment_list.php">수강신청목록</a>
        <li<?php echo starts_with($submenu, 'edu_cancel_list') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/enrol/edu_cancel_list.php">취소신청목록</a>
        <li<?php echo starts_with($submenu, 'edu_course') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/enrol/edu_course.php"><?php echo get_string('stats_allcourselist', 'local_lmsdata'); ?></a>
        <li<?php echo starts_with($submenu, 'edu_point') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/enrol/edu_point_new.php">포인트현황</a>
    </ul>
 
<?php include_once dirname(dirname(__FILE__)) . '/inc/nav.php'; ?>

</div><!--Sidebar Manage End-->