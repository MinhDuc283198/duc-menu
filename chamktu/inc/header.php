<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
include_once (dirname(__FILE__) . '/lib.php');
include_once (dirname (dirname(__FILE__)) . '/lib.php');
require_once dirname(dirname (__FILE__)).'/lib/lib.php';
$separator = explode(DIRECTORY_SEPARATOR, getcwd());
$topmenu = array_pop($separator);

// 관리자관리는 user폴더(회원관리)에 있는 'infadmin'으로 시작하는 php들 
// 실제 화면에서는 운영관리 메뉴에 들어있음 
// => 세부 설정을 하기 위한 소스
// starts_with($topmenu_self, 'infadmin') true or false로 판단하여 selected 추가 설정하기
$separator_self = explode(DIRECTORY_SEPARATOR, $_SERVER['REQUEST_URI']);
$topmenu_self = array_pop($separator_self);

$coursecontext = context_system::instance();   // SYSTEM context.
$PAGE->set_context($coursecontext);

if (!is_siteadmin($USER)) {
    redirect($CFG->wwwroot);
}
$adminlist = edu_control_admincategory();

privacy_log_search(); //개인정보 조회 로그 함수

?>
<!doctype html>
<html dir="ltr" lang="<?php echo current_language(); ?>" xml:lang="<?php echo current_language(); ?>">
    <head>
        <title><?php echo $SITE->fullname; ?></title> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <?php //echo $OUTPUT->standard_head_html(); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/chamktu/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/chamktu/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/chamktu/css/loading.css">
        <script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/chamktu/js/lib/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/chamktu/js/lib/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/chamktu/js/lib/jquery.ui.datepicker_lang.js"></script>
        <script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/chamktu/js/loading.js"></script>
        <script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/chamktu/js/common.js"></script>
        <?php
        if (isset($js) && is_array($js)) {
            foreach ($js as $j) {
                echo '<script type="text/javascript" src="' . $j . '"></script>';
            }
        }
        ?>
    </head>
    <body>
        <div class="wrap">

            <div id="header">
                <div class="logout">
                    <a class="cursor" onclick="location.href = '<?php echo $CFG->wwwroot . '/login/logout.php?sesskey=' . $USER->sesskey; ?>'"><?php echo get_string('logout', 'local_lmsdata'); ?></a>
                    <a class="cursor"  onclick="location.href = '<?php echo $CFG->wwwroot; ?>'" ><?php echo get_string('golms', 'local_lmsdata'); ?></a>
                </div> 
                <h1 class="logo">
                    <a href="<?php echo $CFG->wwwroot . '/chamktu/manage/index.php'; ?>" style="color: black;">
                        <img src="/theme/oklassedu/pix/images/logo.png" alt="JEI 재능그룹" /> 
                    </a>
                </h1>
                <ul class="gnb" style="float:left;">
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/manage/edu_course.php"<?php echo ($topmenu == 'manage'&& !(starts_with($topmenu_self, 'index'))) ? ' class="selected"' : ''; ?>><?php echo get_string('course_management', 'local_lmsdata'); ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/enrol/edu_enrolment_list.php"<?php echo ($topmenu == 'enrol') ? ' class="selected"' : ''; ?>><?php echo get_string('enrol_management', 'local_lmsdata'); ?></a></li>
                    <?php if($adminlist =='super'){?>
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/users/info.php"<?php echo ($topmenu == 'users' && !(starts_with($topmenu_self, 'infadmin'))) ? ' class="selected"' : ''; ?>><?php echo get_string('user_management', 'local_lmsdata'); ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/board/board_list.php"<?php echo ($topmenu == 'board') ? ' class="selected"' : ''; ?>><?php echo get_string('board_management', 'local_lmsdata'); ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/support/evaluation/survey_form.php"<?php echo ($topmenu == 'support' ||starts_with($topmenu_self, 'survey') || starts_with($topmenu_self, 'infadmin') || starts_with($topmenu_self, 'mainimg') || starts_with($topmenu_self, 'banner')) ? ' class="selected"' : ''; ?>><?php echo get_string('site_management', 'local_lmsdata'); ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/contact_stats_day.php"<?php echo ($topmenu == 'stats') ? ' class="selected"' : ''; ?>><?php echo get_string('stats_management', 'local_lmsdata'); ?></a></li>
                    <?php } ?>
                    <li><a href="<?php echo $CFG->wwwroot; ?>/chamktu/contents/index.php"<?php echo ($topmenu == 'contents') ? ' class="selected"' : ''; ?>><?php echo get_string('lcms_management', 'local_lmsdata'); ?></a></li>
                </ul> <!--GNB End-->
            </div> <!--Header End-->
