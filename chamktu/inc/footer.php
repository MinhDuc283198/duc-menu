<div class="loading_modal"></div>
<div id="footer">	
    <div class="group">
        <div class="logo">
            <img src="/theme/oklassedu/pix/images/logo.png" alt="JEL재능교육"/>
        </div>
        <div class="info">
            <p>주)재능교육  |  대표자 : 박종우  |  사업자 번호 : 204-81-03990  |  주소 : 서울특별시 종로구 창경궁로 293</p>
            <p class="tel">전화번호 : 1588-1132  |  이메일 : answer@jei.com</p>
            <p class="copy">Copyright © JEI. All rights reserved.</p>
        </div>    
    </div>	
</div> <!-- Footer End -->