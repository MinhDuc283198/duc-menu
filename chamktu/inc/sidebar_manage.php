<?php
/**
* Sidebar for siteadmin
*
* @package newoklass
* @copyright Copyright (c) 2016 (oklass)
* @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/
$submenu = trim(basename($_SERVER['PHP_SELF']), '.php');
$eval_on = false;
?>
<div id="sidebar">
    <div class="menu_title"><h2><?php echo get_string('course_management', 'local_lmsdata'); ?></h2></div>
    <ul class="submenu">
        <li<?php echo starts_with($submenu, 'edu_course') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/manage/edu_course.php">과정관리</a>
        <li<?php echo starts_with($submenu, 'edu_class') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/manage/edu_class.php">기수관리</a>
        <li<?php echo starts_with($submenu, 'qna') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/manage/qna.php">과정Q&A</a></li>
        <li<?php echo starts_with($submenu, 'qbank') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/question/edit.php?courseid=1" target="_blank">문제은행</a></li>
    </ul>

<?php include_once dirname(dirname(__FILE__)) . '/inc/nav.php'; ?>

</div><!--Sidebar Manage End-->
