<?php
require_once dirname(dirname (dirname (__FILE__))).'/config.php';
$submenu =  basename($_SERVER['PHP_SELF'], '.php');
?>
<div id="sidebar">
    <div class="menu_title"><h2><?php echo get_string('board_management', 'local_lmsdata'); ?></h2></div>
    <ul class="submenu">
        <li<?php echo starts_with($submenu, 'board') ? ' class="selected"' : ''; ?>><a href="../board/board_list.php"><?php echo get_string('siteadmin_boardmanagement', 'local_lmsdata'); ?></a></li>
        <li<?php echo starts_with($submenu, 'rebooks') ? ' class="selected"' : ''; ?>><a href="../board/rebooks_list.php"><?php echo get_string('recommend_book', 'local_lmsdata');?></a> 
    </ul>
    
    <?php include_once ('../inc/nav.php');?>

</div><!--Sidebar End-->
