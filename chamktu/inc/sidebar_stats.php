<?php
/**
* Sidebar for siteadmin
*
* @package newoklass
* @copyright Copyright (c) 2016 (oklass)
* @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/
$submenu = trim(basename($_SERVER['PHP_SELF']), '.php');
$eval_on = false;
?>
<div id="sidebar">
    <div class="menu_title"><h2><?php echo get_string('stats_management', 'local_lmsdata'); ?></h2></div>
    <ul class="submenu">
                <!--<li<?php echo starts_with($submenu, 'edu_keris_results') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/edu_keris_results.php">사이트 통계</a></li>-->
        <li<?php echo starts_with($submenu, 'contact_stats') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/contact_stats_day.php"><?php echo get_string('stats_loginstats', 'local_lmsdata'); ?></a></li>
        <li <?php echo starts_with($submenu, 'edu_operation_status') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/edu_operation_status.php">교육과정 통계</a></li>
        <li <?php echo starts_with($submenu, 'contents_report') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/contents_report.php">콘텐츠 통계</a></li>
        <li<?php echo starts_with($submenu, 'stats') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/stats_search.php">검색어 통계</a></li>
        <li<?php echo starts_with($submenu, 'menu') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/menu_stats.php">메뉴 이용 통계</a></li>
<!--        <li<?php echo starts_with($submenu, 'edu_sales_status') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/edu_sales_status.php">매출정산</a></li>
        <li <?php echo starts_with($submenu, 'edu_operation_status') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/edu_operation_status.php">운영현황보고</a></li>
        <li <?php echo starts_with($submenu, 'edu_keris_report') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/edu_keris_report.php">KERIS보고</a></li>-->
        <!--<li<?php echo starts_with($submenu, 'edu_training_results') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/edu_training_results.php">연수결과보고</a></li>-->
        
        <!--<li <?php echo starts_with($submenu, 'activity') ? ' class="selected"' : ''; ?>><a href="<?php echo $CFG->wwwroot; ?>/chamktu/stats/">교육청결과보고</a></li>-->        
    </ul>

<?php include_once dirname(dirname(__FILE__)) . '/inc/nav.php'; ?>

</div><!--Sidebar Manage End-->