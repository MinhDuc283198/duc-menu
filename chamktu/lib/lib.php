<?php
function admin_println($message) {
    echo '<div style="width:500px">';
    echo $message;
    echo '</div><br/>';
    admin_flushdata();
}

function admin_flushdata() {
    while (ob_get_level() > 0) {
        ob_end_flush();
    }

    flush();

    ob_start();     
} 
            
/**
 * //연수이력 등록 API
 * @global type $DB
 * @param type $application
 * @return type
 */
function chamktu_applications_set_training_history($application){
    global $DB;
    
    $sql = 'SELECT lc.*, lco.coursename, cc.name as categoryname, cod.codename as gradename, mu.lastname as teachername
            FROM {lmsdata_course_applications} lca
            JOIN {lmsdata_class} lc ON lc.id = lca.courseid
            JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
            JOIN {course} mc ON mc.id = lco.courseid
            JOIN {course_categories} cc ON cc.id = mc.category
            JOIN {lmsdata_code} cod ON cod.id = lco.coursegrade
            JOIN {lmsdata_course_teacher} lct ON lct.courseid = lco.courseid
            JOIN {lmsdata_user} lu ON lu.id = lct.lmsuserid
            JOIN {user} mu ON mu.id = lu.userid
            WHERE lca.id = :id ORDER BY lct.best DESC LIMIT 1';
    
    $courseinfo = $DB->get_record_sql($sql, array('id'=>$application->id));
    
    $userinfo = $DB->get_record('lmsdata_user',array('userid'=>$application->userid));
    
    $post_data = array();
    
    $post_data['id'] = $courseinfo->courseid;
    $post_data['applicationid'] = $application->id;
    $post_data['userid'] = $userinfo->userid;
    $post_data['userno'] = $userinfo->userno;
    $post_data['coursetype'] = $courseinfo->categoryname;
    $post_data['coursename'] = $courseinfo->coursename;
    $post_data['classnum'] = $courseinfo->classnum;
    $post_data['classtype'] = $courseinfo->classtype;
    $post_data['coursegrade'] = $courseinfo->gradename;
    $post_data['learningstart'] = date('Y-m-d H:i:s',$courseinfo->learningstart);
    $post_data['learningent'] = date('Y-m-d H:i:s',$courseinfo->learningend);
    $post_data['teachername'] = $courseinfo->teachername;
    $post_data['completionstatus'] = $application->completionstatus;
    $post_data['paymentstatus'] = $application->paymentstatus;
    $post_data['olddata'] = 0;
    
//    if($_SERVER['HTTP_HOST'] == 'campus.eduhope.net'){
//        $curl = curl_init('http://222.234.1.235:8282/EduAuth/tr/insertRemoteTraining.do'); 
//    } else {
//        $curl = curl_init('http://117.52.98.183:8480/EduAuth/tr/insertRemoteTraining.do'); 
//    }
//    curl_setopt($curl, CURLOPT_FAILONERROR, true); 
//    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
//    curl_setopt($curl, CURLOPT_POST, 1);
//    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
//    curl_setopt($curl, CURLOPT_POSTFIELDSIZE, 0);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
//    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   
//
//    $output = curl_exec($curl);
//    $output = json_decode($output);
    $output = '';
    return $output;
}

/**
 * 연수이력 수정
 * @global type $DB
 * @param type $applicationid
 * @return type
 */
function local_course_applications_update_training_history($applicationid){
    global $DB;
    
    $application_data = $DB->get_record_sql("select lca.id, lca.userid, lco.coursename, cc.name as categoryname, cod.codename as gradename, lc.learningstart, lc.learningend, lca.completionstatus, lca.paymentstatus
        ,(select group_concat(u.lastname) from m_lmsdata_course_teacher lct join m_lmsdata_user lu on lu.id = lct.lmsuserid join m_user u on u.id = lu.userid where courseid = lco.courseid ) as teachername
        from {lmsdata_course_applications} lca 
        join {lmsdata_class} lc on lca.courseid = lc.id
        join {lmsdata_course} lco on lc.parentcourseid = lco.id 
        JOIN {course} mc ON mc.id = lco.courseid
        JOIN {course_categories} cc ON cc.id = mc.category
        JOIN {lmsdata_code} cod ON cod.id = lco.coursegrade
        where lca.id = $applicationid ");
    
    $post_data = array();
    
    $post_data['applicationid'] = $applicationid;
    $post_data['userid'] = $application_data->userid;
    $post_data['coursetype'] = $application_data->categoryname;
    $post_data['coursename'] = $application_data->coursename;
    $post_data['coursegrade'] = $application_data->gradename;
    $post_data['learningstart'] = date('Y-m-d H:i:s',$application_data->learningstart);
    $post_data['learningent'] = date('Y-m-d H:i:s',$application_data->learningend);
    $post_data['teachername'] = $application_data->teachername;
    $post_data['completionstatus'] = $application_data->completionstatus;
    $post_data['paymentstatus'] = $application_data->paymentstatus;
    
//    if($_SERVER['HTTP_HOST'] == 'campus.eduhope.net'){
//        $curl = curl_init('http://222.234.1.235:8282/EduAuth/tr/updateRemoteTraining.do'); 
//    } else {
//        $curl = curl_init('http://117.52.98.183:8480/EduAuth/tr/updateRemoteTraining.do'); 
//    }
//    curl_setopt($curl, CURLOPT_FAILONERROR, true); 
//    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
//    curl_setopt($curl, CURLOPT_POST, 1);
//    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
//    curl_setopt($curl, CURLOPT_POSTFIELDSIZE, 0);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
//    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   
//
//    $output = curl_exec($curl);
//    $output = json_decode($output);
    $output = '';
    return $output;
}

/**
 * 연수이력 일괄 수정
 * @param type $courseid
 * @param type $coursetype
 * @param type $coursename
 * @param type $coursegrade
 * @param type $learningstart
 * @param type $learningent
 * @param type $teachername
 * @return type
 */
function local_course_applications_all_update_training_history($courseid, $coursetype, $coursename, $coursegrade, $learningstart, $learningent, $teachername){
    $post_data = array();
    
    $post_data['id'] = $courseid;
    $post_data['coursetype'] = $coursetype;
    $post_data['coursename'] = $coursename;
    $post_data['coursegrade'] = $coursegrade;
    $post_data['learningstart'] = date('Y-m-d H:i:s',$learningstart);
    $post_data['learningent'] = date('Y-m-d H:i:s',$learningent);
    $post_data['teachername'] = $teachername;
    
//    if($_SERVER['HTTP_HOST'] == 'campus.eduhope.net'){
//        $curl = curl_init('http://222.234.1.235:8282/EduAuth/tr/updateAllRemoteTraining.do'); 
//    } else {
//        $curl = curl_init('http://117.52.98.183:8480/EduAuth/tr/updateAllRemoteTraining.do'); 
//    }
//    curl_setopt($curl, CURLOPT_FAILONERROR, true); 
//    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
//    curl_setopt($curl, CURLOPT_POST, 1);
//    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
//    curl_setopt($curl, CURLOPT_POSTFIELDSIZE, 0);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
//    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   

//    $output = curl_exec($curl);
//    $output = json_decode($output);
    $output = '';
    return $output;
}


/**
 * //sms 발송 함수
 * @global type $DB
 * @param type $content
 * @param type $phone
 */
function chamktu_balsong_sms($content, $phone){
    global $DB;

    $content = htmlspecialchars_decode($content);
    
    if(mb_strlen($content) > 90 && mb_strlen($content) <= 1000){
        $msg_type = 'M';
    } else if(mb_strlen($content) <= 90){
        $msg_type = 'S';
    }
    if(time() >= 1512054000){
        $sql = "INSERT INTO Balsong_Mobile (msg_type, dest_no, call_back, msg_contents, sendreq_time, bill_id) VALUES ('$msg_type', '$phone', '0226709465 ', '$content', now(), '101197502163')";
    } else {
        $sql = "INSERT INTO Balsong_Mobile (msg_type, dest_no, call_back, msg_contents, sendreq_time) VALUES ('$msg_type', '$phone', '0226709465 ', '$content', now())";
    }

    $DB->execute($sql,array());
}
/**
 * 이메일 발송 함수
 * @global type $CFG
 * @global type $DB
 * @param type $title
 * @param type $content
 * @param type $application
 */
function eduhope_balsong_mail($title, $content, $application){
    global $CFG, $DB;
    
    $db_host = 'localhost';
    $db_id = $CFG->dbuser;
    $db_pw = $CFG->dbpass;
    $db_name = $CFG->dbname;
    $conn = mysqli_connect($db_host, $db_id, $db_pw, $db_name);

    $sql = "INSERT INTO Balsong_Mail_Tran (User_ID, User_PW, Send_Date, Subject, Mail_Text, Sender_NM, Sender_Email) VALUES ('ck0000000004', '0226709300', now(), '$title', '$content', '참교육 연수원', 'chamcampus@gmail.com')";

    $result = mysqli_query($conn, $sql);

    $select = 'SELECT max(seq) as last_seq FROM Balsong_Mail_Tran';

    $query = mysqli_query($conn, $select);

    $last_seq = mysqli_fetch_assoc($query);
    
        $username = $DB->get_field('user', 'lastname', array('id'=>$application->userid));
        $sql = "INSERT INTO Balsong_Mail_Tran_Detail (Mail_Tran_Seq, Dest_Mail, Company_NM, Person_NM) VALUES ('".$last_seq['last_seq']."', '$application->email', '', '$username')"; 

        $result = mysqli_query($conn, $sql);


    $sql = "UPDATE Balsong_Mail_Tran SET Send_Status=1 WHERE Seq='".$last_seq['last_seq']."'";

    $result = mysqli_query($conn, $sql);
}
/**
 * //발송 템플릿 변수 가져오는 함수
 * @global type $DB
 * @param type $data
 * @return type
 */
function admin_sendvarsetting($data){
    
    global $DB;
    
    $vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
    $vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
    $datavars = array();
    
    
    //passgrade 가져오기
    $sql = 'select sum(grademax) as avggrade from {grade_items} where courseid = :courseid and itemmodule = :itemmodule and gradetype = 1';
    //진도율
    $passgrade_progress = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'lcmsprogress'));
    //과제
    $passgrade_assign = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'assign'));
    //토론
    $passgrade_forum = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'forum'));
    //과제
    $passgrade_quiz = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'quiz'));
    
    //통과성적
    $sql = 'select gradepass from {course_completion_criteria} where course = :courseid and criteriatype = 6;';
    $passgrade = $DB->get_field_sql($sql, array('courseid'=>$data['courseid']));
    
    //이수진도율
    $sql = 'select ROUND(avg(completionprogress)) from {lcmsprogress} where course = :courseid and id = (select id from {lcmsprogress} where course = :courseid2 order by timemodified desc limit 1)';
    $passprogress = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'courseid2'=>$data['courseid']));
    
    //나의 진도율
    $sql = 'select progress from {lcmsprogress_grades} lg 
            join {lcmsprogress} l on l.id = lg.lcmsprogress 
            where l.course = :courseid and lg.userid = :userid';
    $myprogress = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'userid'=>$data['userid']));
    
    //나의 성적
    $sql = 'select avg(gg.finalgrade) as avggrade from {grade_items} gi 
            join {grade_grades} gg on gg.itemid = gi.id 
            where gi.courseid = :courseid and gi.itemmodule = :itemmodule and gg.userid = :userid';
    //진도율
    $mygrade_progress = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'lcmsprogress', 'userid'=>$data['userid']));    
    //과제
    $mygrade_assign = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'assign', 'userid'=>$data['userid']));
    //토론
    $mygrade_forum = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'forum', 'userid'=>$data['userid']));
    //퀴즈
    $mygrade_quiz = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemmodule'=>'quiz', 'userid'=>$data['userid']));
    //최종성적
    $sql = 'select avg(gg.finalgrade) as avggrade from {grade_items} gi 
            join {grade_grades} gg on gg.itemid = gi.id 
            where gi.courseid = :courseid and gi.itemtype = :itemtype and gg.userid = :userid';
    $mygrade = $DB->get_field_sql($sql, array('courseid'=>$data['courseid'], 'itemtype'=>'course', 'userid'=>$data['userid']));
    
    $sql_data = $DB->get_record_sql("select lca.id, lca.paymentdate, lca.birthday, lca.locationnumber, lca.neiscode, lca.paymentstatus, lca.price, lca.timecreated
                                    , lc.gradeviewend, lc.gradeviewstart, lc.classyear, lc.classnum, lc.evaluationend, lc.evaluationstart, lc.learningend, lc.learningstart
                                    , lco.coursename, u.username, u.lastname, lsc.consignmentplace 
                                    from m_lmsdata_course_applications lca 
                                    join m_lmsdata_class lc on lc.id = lca.courseid
                                    join m_lmsdata_course lco on lc.parentcourseid = lco.id
                                    join m_user u on lca.userid = u.id 
                                    join m_lmsdata_schedule lsc on lsc.id = lc.scheduleid 
                                    where lca.userid = :userid and lca.id = :id ",array('userid'=>$data['userid'],'id'=>$data['applicationid']));
    foreach ($sql_data as $key => $val){
        $data[$key] = $val;
    }
    
    foreach($vars as $key => $val){
        $datavars[$key] = trim($data[$key]);
        switch($vartypes[$key]){
            case 'date':
                $datavars[$key] = date('Y년 m월 d일',$datavars[$key]);
                break;
            case 'getstring':
                if($datavars[$key])
                $datavars[$key] = get_string($datavars[$key],'local_lmsdata');
                break;
            case 'number':
                $datavars[$key] = number_format($datavars[$key]);
                break;
            case 'text':
                $datavars[$key] = $datavars[$key];
                break;
            case 'passrate':
                $passrates = array();
                if(!empty($passgrade_progress)){
                    $passrates[] = '진도율 '.round($passgrade_progress).'%';
                }
                if(!empty($passgrade_assign)){
                    $passrates[] = '과제 '.round($passgrade_assign).'%';
                }
                if(!empty($passgrade_forum)){
                    $passrates[] = '토론 '.round($passgrade_forum).'%';
                }
                if(!empty($passgrade_quiz)){
                    $passrates[] = '온라인 시험 '.round($passgrade_quiz).'%';
                }
                
                $datavars[$key] = ($passrates)? implode(', ',$passrates):'-';
                
                break;
            case 'myprogress':
                $datavars[$key] = ($myprogress)? $myprogress.'%':'0%';
                break;
            case 'passgrade':
                $datavars[$key] = ($passgrade)? $passgrade.'점':'0점';
                break;
            case 'passprogress':
                $datavars[$key] = ($passprogress)? $passprogress.'%':'0%';
                break;
            case 'mygrade':
                $mygrades = array();
                if(!empty($passgrade_progress)){
                    if(empty($mygrade_progress)) $mygrade_progress = 0;
                    $mygrades[] = '진도율 '.round($mygrade_progress).'점';
                }
                if(!empty($passgrade_assign)){
                    if(empty($mygrade_assign)) $mygrade_assign = 0;
                    $mygrades[] = '과제 '.round($mygrade_assign).'점';
                }
                if(!empty($passgrade_forum)){
                    if(empty($mygrade_forum)) $mygrade_forum = 0;
                    $mygrades[] = '토론 '.round($mygrade_forum).'점';
                }
                if(!empty($passgrade_quiz)){
                    if(empty($mygrade_quiz)) $mygrade_quiz = 0;
                    $mygrades[] = '온라인 시험 '.round($mygrade_quiz).'점';
                }
                if(!empty($passgrade)){
                    if(empty($mygrade)) $mygrade = 0;
                    $mygrades[] = '총점 '.round($mygrade).'점';
                }
                
                $datavars[$key] = ($mygrades)? implode(', ',$mygrades):'-';
                
                break;
            
        }
    }
    $data['title'] = str_replace($vars, $datavars, $data['title']);
    $data['contents'] = str_replace($vars, $datavars, $data['contents']);
    return $data;
    
}
/**
 * (19.05.22)
 * 이메일 템플릿 발송전 DB에 넣는 함수
 */
function reday_to_send_email($user,$subject,$message){
    global $CFG, $DB, $USER;
     if($USER->id == ""){
         $USER->id = 2;
     }
     $mailhs = new stdClass();
     $mailhs->userid = $USER->id;
     $mailhs->subject = $subject;
     $mailhs->text = $message;
     $mailhs->targets = $user->id; //메일 받을사람 user id
     //발송여부(send_status)를 0으로 insert //향후 다른페이지에서 0인 컬럼을 찾아서 메일을 보내고 1로 수정해야함.
     $mailhs->send_status = 0;
     $mailhs->timecreated = time();//생성날짜
     $return = $DB->insert_record('mailsend_history',$mailhs);
     return $return;
}

/**
 * (19.06.03)
 * push 템플릿 발송전 DB에 넣는 함수
 */
function reday_to_send_push($user,$subject,$message,$link=""){
    global $CFG, $DB, $USER;
     if($link == ""){
         $link = $CFG->wwwroot;
     }   
     if($USER->id == ""){
         $USER->id = 2;
     }
     $mailhs = new stdClass();
     $mailhs->userid = $USER->id;
     $mailhs->subject = $subject;
     $mailhs->text = $message;
     $mailhs->targets = $user->id; //메일 받을사람 user id
     //발송여부(send_status)를 0으로 insert //향후 다른페이지에서 0인 컬럼을 찾아서 메일을 보내고 1로 수정해야함.
     $mailhs->send_status = 0;
     $mailhs->link = $link;
     $mailhs->timecreated = time();//생성날짜
     $return = $DB->insert_record('pushsend_history',$mailhs);
     //local_notification_add($mailhs->userid,$mailhs->targets,$mailhs->subject,$mailhs->text,$mailhs->text,"","","");
     return $return;
}

/**
 * 이메일 템플릿 발송 status 처리하는 함수
 * @global type $CFG
 * @global type $DB
 * @param type $seq
 * @return type
 */
//function eduhope_balsong_mail_update($seq){
//    global $CFG, $DB;
//    
//    $db_host = 'localhost';
//    $db_id = $CFG->dbuser;
//    $db_pw = $CFG->dbpass;
//    $db_name = $CFG->dbname;
//    $conn = mysqli_connect($db_host, $db_id, $db_pw, $db_name);
//    $sql = "UPDATE Balsong_Mail_Tran SET Send_Status=1 WHERE Seq='".$seq."'";
//
//    $result = mysqli_query($conn, $sql);
//    return $result;
//}

/**
 * 개인정보 조회 로그를 DB에 넣는 함수
 * 
 */

function privacy_log_search(){
    global $CFG, $DB, $USER;
    
    $sql = "select * from {config} where name in ('search_log_url','create_log_url', 'update_log_url', 'delete_log_url')";
    $settings = $DB->get_records_sql($sql);
    $setting = $DB->get_record_sql($sql);
    $userid = $USER->id;   
    $ip = $_SERVER['REMOTE_ADDR'];
    $url = $_SERVER["PHP_SELF"];
    
    $privacy = new stdClass();
    $privacy->userid = $userid;
    $privacy->url = $_SERVER['REQUEST_URI']; //접속한 url
    $privacy->ipaddr = $ip; //접속한 사용자의 ip주소
    $privacy->timecreated = time();

    foreach ($settings as $setting) {
        $search = explode(',',$setting->value);
        //조회, 등록, 수정, 삭제 url 구분
        if($setting->name == 'search_log_url') {
        $privacy->url_type = 1; //조회
        } else if($setting->name == 'create_log_url') {
            $privacy->url_type = 2; //등록
        } else if($setting->name == 'update_log_url') {
            $privacy->url_type = 3; //수정
        } else {
            $privacy->url_type = 4; //삭제
        }
        foreach($search as $sea){
            if($sea == $url) {
                $DB->insert_record('jei_privacy_log', $privacy);
            }
        }
    }
} 