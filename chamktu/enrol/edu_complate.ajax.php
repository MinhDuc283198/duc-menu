<?php
/**
 * 이수변경 ajax
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');

$id = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$applications = $DB->get_record('lmsdata_course_applications', array('id' => $id));

$applications->completionstatus = 1;
$applications->adminchange = 1;
$applications->timemodified = time();

$DB->update_record('lmsdata_course_applications', $applications);
chamktu_applications_set_training_history($applications);

$returnvalue->status = 'success';

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
