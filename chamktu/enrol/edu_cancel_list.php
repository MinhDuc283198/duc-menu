<?php
/**
 * 취소신청 목록 리스트
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$classobject = optional_param('classobject', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$discounttype = optional_param('discounttype', '', PARAM_RAW);
$paymentstatus = optional_param('paymentstatus', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);
//검색용 파라미터

if ($classyear) {
    $sql_where[] = " lc.classyear = :classyear ";
    $params['classyear'] = $classyear;
}
if ($classnum) {
    $sql_where[] = " lc.classnum = :classnum ";
    $params['classnum'] = $classnum;
}
if ($discounttype) {
    if ($discounttype == 'group') {
        $sql_where[] = " lca.discounttype = :discounttype ";
        $params['discounttype'] = $discounttype;
    } else {
        $sql_where[] = " lca.discounttype != :discounttype ";
        $params['discounttype'] = 'group';
    }
}
if ($paymentstatus) {
    $sql_where[] = " lac.status = :paymentstatus ";
    $params['paymentstatus'] = $paymentstatus;
}
if (!empty($searchtext)) {
    $like = array();
    $like[] = $DB->sql_like('u.lastname', ':lastname');
    $like[] = $DB->sql_like('co.fullname', ':fullname');
    $params['lastname'] = '%' . $searchtext . '%';
    $params['fullname'] = '%' . $searchtext . '%';
    $sql_where[] = '( ' . implode(' OR ', $like) . ' )';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

//$sql_select = "SELECT lca.*, lac.status as lacstatus, lac.timecreated as time, lac.refund, lac.reason ,lc.classyear,lc.classnum, co.fullname, u.lastname ";
$sql_select = "SELECT lac.id, lca.id lcaid, lca.usertypecode, lac.status as lacstatus, lac.timecreated as time, lac.reason, "
        . "lc.classyear, lc.classnum, co.fullname, u.lastname, u.firstname, u.phone1, u.phone2,u.username, u.id uid ";
$sql_from = "FROM {lmsdata_applications_cancel} lac "
        . "join {lmsdata_course_applications} lca on lca.id = lac.applicationid "
        . "join {lmsdata_class} lc on lc.courseid = lac.courseid "
        . "join {course} co on co.id = lc.courseid "
        . "join {user} u on u.id = lac.userid ";

$sql_orderby = " order by lac.id desc";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

        <div id="content">
            <h3 class="page_title">취소신청목록</h3>
            <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <strong>취소신청목록</strong></div>
            <form name="course_search" id="course_search" class="search_area" action="edu_cancel_list.php" method="get">
                <input type="hidden" name="page" value="1" />
                <input type="hidden" name="excel" value="0" />
                <div style="float:left;">
                    <select title="class" name="coursename" id="coursename" class="w_200">
                        <option value="0">과정명</option>
                        <?php
                        $datassql = 'select lco.id, lco.coursename '
                                . 'from {lmsdata_course} lco '
                                . 'join {lmsdata_class} lc on lc.parentcourseid = lco.id '
                                . 'join {lmsdata_course_applications} lca on lca.courseid = lc.courseid '; // $classyear $classnum
//                                . 'where lco.isused != 1 and lc.classyear = :lcclassyear and lc.classnum = :lcclassnum';
                        $datas = $DB->get_records_sql($datassql);
                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->id == $coursename) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->id . '"' . $selected . '> ' . $data->coursename . '</option>';
                        }
                        ?>
                    </select>
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="year_changed(this, 'lmsdata_class', 1);">
                        <option value="0">년도</option>
                        <?php
//                        $yearRange = 10;
//                        $currentYear = date('Y');
//                        $maxYear = ($currentYear + $yearRange);
                        $max = $DB->get_field_sql("SELECT max(classyear) FROM {lmsdata_class}");
                        $min = $DB->get_field_sql("SELECT min(classyear) FROM {lmsdata_class} where classyear != 0 ");

                        foreach (range($min, $max) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select> 
                    <select title="class" name="classnum" id="classnum" class="w_160" >
                        <option value="0">기수</option>
                        <?php
                        if ($classyear) {
                            if ($classtype) {
                                $classnum_where = " and classtype = $classtype";
                            }
                            if ($classyear) {
                                $classnum_where .= " and classyear = $classyear";
                            }
                            $datas = $DB->get_records_sql('select classnum from {lmsdata_class} where classnum != 0' . $classnum_where);

                            foreach ($datas as $data) {
                                $selected = '';
                                if ($data->classnum == $classnum) {
                                    $selected = ' selected';
                                }
                                echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                            }
                        }
                        ?>
                    </select>
                    <!--            </div>
                                <div>
                                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="과정명 또는 신청자명을 입력하세요."  class="search-text"/>-->
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
                </div>
            </form><!--Search Area2 End-->
            <div style="float:right;">
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
            </div>
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                        <th scope="row" width="5%">년도</th>
                        <th scope="row" width="5%">기수</th>
                        <th scope="row" width="15%">과정명</th>
                        <th scope="row" width="10%">신청자</th>
                        <th scope="row" width="5%">취소신청일</th>
                        <th scope="row" width="5%">사용자구분</th>
                        <th scope="row" width="5%">취소승인</th>                    
                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="13">신청내역이 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    $type_arrs = array('10' => '임직원', '20' => '멘토', '30' => '교육생');
                    $startnum = $count_courses - (($currpage - 1) * $perpage);
//                print_object($courses);die();
                    foreach ($courses as $course) {
                        ?>
                        <tr>
                            <td><?php echo $startnum--; ?></td>                    
                            <td><?php echo $course->classyear . '년' ?></td>
                            <td><?php echo $course->classnum ? $course->classnum . '기' : '-' ?></td>
                            <td><a href="<?php echo $CFG->wwwroot . '/chamktu/enrol/edu_cancel_list.detail.php?id=' . $course->id; ?>">
                                    <?php echo $course->fullname ?></a></td>
                            <td><?php echo $course->firstname . $course->lastname ?></td>
                            <td><?php echo date('Y-m-d', $course->time) ?></td>
                            <td><?php echo!$course->usertypecode ? '-' : $type_arrs[$course->usertypecode]; ?></td><!--user-->
                            <td>
                                <?php
                                if ($course->lacstatus == 'complete') {
                                    echo '승인완료';
                                } else {
                                    echo '승인취소';
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
            <?php
            if (($count_courses / $perpage) > 1) {
                print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            }
            ?>            
        </div><!--Content End-->    
    </div> <!--Contents End-->


    <?php
    include_once ('../inc/footer.php');
} else {
    $fields = array(
        '번호',
        '년도',
        '기수',
        '과정명',
        '아이디',
        '신청자',
        '전화번호',
        '취소신청일',
        '사용자구분',
        '취소승인'
    );

    $date = date('Y-m-d', time());
    $filename = '취소신청목록_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($courses as $course) {
        $type_arrs = array('10' => '임직원', '20' => '멘토', '30' => '교육생');
        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $course->classyear . '년');
        $worksheet[0]->write($row, $col++, $course->classnum . '기');
        $worksheet[0]->write($row, $col++, $course->fullname);
        $worksheet[0]->write($row, $col++, $course->username);
        $worksheet[0]->write($row, $col++, $course->lastname);
        $worksheet[0]->write($row, $col++, substr($course->phone1, 0, 3) . '-' . substr($course->phone1, 3, 4) . '-' . substr($course->phone1, 7, 4));

        $worksheet[0]->write($row, $col++, date('Y-m-d', $course->time));
        $worksheet[0]->write($row, $col++, !$course->usertypecode ? '-' : $type_arrs[$course->usertypecode]);
        if ($course->lacstatus == 'complete') {
            $lacstatus = '승인완료';
        } else {
            $lacstatus = '승인취소';
        }
        $worksheet[0]->write($row, $col++, $lacstatus);
        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">

    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    /**
     * 결제취소 기능
     * @param {type} id
     * @param {type} paymentmeans
     * @returns {undefined}
     */

    function refund_cancel(id, paymentmeans) {
        var paymentmeans = paymentmeans;
        if (confirm("결제취소 하시겠습니까?") == true) {
            if (paymentmeans == "card") {
                window.open('refund_cancel.php?id=' + id, 'cancel', 'width=650,height=450');
            } else {
                $.ajax({
                    url: 'edu_cancel.ajax.php',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            alert('취소되었습니다.');
                            location.reload();
                        }
                    }
                });
            }
        }
    }
    /**
     * 엑셀다운로드
     
     * @returns {undefined}     */
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $("#course_search").submit();
        $("input[name='excel']").val(0);
    }

</script>    
