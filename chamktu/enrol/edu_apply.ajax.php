<?php
/**
 * 수강신청 관리자 등록 ajax
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');

$id = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$applications = $DB->get_record('lmsdata_course_applications', array('id' => $id));

$lmsdata_class = $DB->get_record('lmsdata_class',array('id' => $applications->courseid));

$applications->status = 'apply';
$applications->paymentstatus = 'complete';
$applications->timemodified = time();

$data = $DB->get_record('course',array('id'=>$lmsdata_class->courseid));
$user = $DB->get_record('lmsdata_user',array('userid'=>$applications->userid));
$user->roleid = 5;
set_assign_user($data, $user);

$DB->update_record('lmsdata_course_applications', $applications);
chamktu_applications_set_training_history($applications);

$returnvalue->status = 'success';

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
