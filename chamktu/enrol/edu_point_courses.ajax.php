<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once $CFG->dirroot.'/local/jeipoint/lib.php';

$pid = required_param('id', PARAM_INT);

$idnumbers = array(
                1 => JEL_COURSE_CATEGORY_TRUST,
                2 => JEL_COURSE_CATEGORY_YEAR
            );

$idnumber = $idnumbers[$pid];

$returnvalue = new stdClass();

$course_sql = 'SELECT 
                        lc.id, lc.coursename AS value
                FROM {lmsdata_course} lc
                JOIN {course} co ON co.id = lc.courseid 
                JOIN {course_categories} cc ON cc.id = co.category
                WHERE cc.idnumber = :idnumber ';

$courses = $DB->get_records_sql($course_sql, array('idnumber' => $idnumber));

if($courses) {
    $returnvalue->status = 'success';
    $returnvalue->courses = $courses;
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);