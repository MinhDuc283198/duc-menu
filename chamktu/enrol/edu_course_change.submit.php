<?php
/**
 * 과정변경 기능 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once($CFG->dirroot . '/chamktu/lib.php');
$courseid = required_param('courseid', PARAM_INT);
$id = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$data = $DB->get_record('lmsdata_course_applications', array('id'=>$id));
$user = $DB->get_record('lmsdata_user',array('userid'=>$data->userid));
$course = $DB->get_record('course', array('id'=>$data->courseid));
$temp_class = $DB->get_record('lmsdata_class', array('id'=>$data->courseid));
$class = $DB->get_record('lmsdata_class', array('id'=>$courseid));

set_unassign_user($temp_class->courseid, $data->userid, 5);

$data->courseid = $class->id;
$DB->update_record('lmsdata_course_applications',$data);
$course = $DB->get_record('course', array('id'=>$class->courseid));

$user->roleid = 5;
set_assign_user($course, $user);

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);