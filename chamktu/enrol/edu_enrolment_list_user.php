<?php
/**
 * 사용자 검색 팝업 화면 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
$ids = optional_param_array('id', array(), PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$userid = optional_param('userid', 0, PARAM_INT);

if (!empty($searchtext)) {
    $sql_where[] = $DB->sql_like('u.username', ':username');
    $params['username'] = '%' . $searchtext . '%';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}
$users = $DB->get_records_sql('select lu.*, u.id, u.username, u.lastname, u.firstname from {user} u join {lmsdata_user} lu on u.id = lu.userid ' . $sql_where, $params, ($currpage - 1) * $perpage, $perpage);
$count_users = $DB->count_records_sql("select count(*) from {user} u join {lmsdata_user} lu on u.id = lu.userid " . $sql_where . $orderby, $params);
$arr = array('10' => '임직원', '20' => '멘토', '30' => '교육생');
?>

<div class="popup_content" id="course_teacher">
    <h2>아이디검색</h2>
    <form id="frm_course_teacher" name="frm_course_teacher" onsubmit="return false;">
        <div style="float:left;">
            <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="아이디를 입력하세요."  class="search-text"/>
            <input type="button" class="blue_btn" value="검색" onclick="teacher_search_name();"/>
        </div>
        <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <!--<th width="10%">선택</th>-->
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th width="25%">아이디</th>
                    <th width="40%">이름</th>
                    <th width="25%">회원구분</th>
                    <th width="10%">선택</th>
                </tr>
                <?php
                $startnum = $count_users - (($currpage - 1) * $perpage);
                foreach ($users as $user) {
                    echo '<tr>';
//                echo '<td><input type="checkbox" onclick="select()" name="id" value="'.$user->id.'"'.$disabled.$checked.'/></td>';
                    echo '<td>' . $startnum-- . '</td>';
                    echo '<td>' . $user->username . '</td>';
                    echo '<td>' . $user->firstname . $user->lastname . '<input type="hidden" name="id_' . $user->id . '_name" value="' . $user->lastname . '"/></td>';
                    echo '<td>' . $arr[$user->usertypecode] . '</td>';
                    echo '<td><input type="button" value="선택" onclick="submit_user(' . $user->userid . ', ' . $user->usertypecode . ')"></td>';
                    echo '<input type="hidden" name="id_' . $user->id . '_lastname" value="' . $user->lastname . '"/>'
                    . '<input type="hidden" name="id_' . $user->id . '_username" value="' . $user->username . '"/>'
                    . '<input type="hidden" name="id_' . $user->id . '_birthday" value="' . $user->birthday . '"/>'
                    . '<input type="hidden" name="id_' . $user->id . '_phone" value="' . $user->phone1 . '"/>'
                    . '<input type="hidden" name="id_' . $user->id . '_usertypecode" value="' . $user->usertypecode . '"/>'
                    . '<input type="hidden" name="id_' . $user->id . '_userid" value="' . $user->id . '"/>';

                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>        
    </form>
    <?php
    if (($count_users / $perpage) > 1) {
        print_paging_navbar_script($count_users, $currpage, $perpage, 'javascript:class_students_search(:page);', 10);
    }
    ?>
</div>
<script>

    function submit_user(userid, usertypecode) {
        if ($("#usertype").val() == usertypecode / 10) {
            set_user_info(userid, usertypecode);
        } else if ($("#usertype").val() == 4) {
            if (usertypecode / 10 == 1 || usertypecode / 10 == 2) {
                set_user_info(userid, usertypecode);
            } else {
                alert('해당 과정에 신청할 수 없는 사용자입니다.');
            }
        } else if ($("#usertype").val() == 5) {
            if (usertypecode / 10 == 2 || usertypecode / 10 == 3) {
                set_user_info(userid, usertypecode);
            } else {
                alert('해당 과정에 신청할 수 없는 사용자입니다.');
            }
        } else if ($("#usertype").val() == 6) {
            if (usertypecode / 10 == 1 || usertypecode / 10 == 3) {
                set_user_info(userid, usertypecode);
            } else {
                alert('해당 과정에 신청할 수 없는 사용자입니다.');
            }
        } else if ($("#usertype").val() == 7) {
            set_user_info(userid, usertypecode);
        } else {
            alert('해당 과정에 신청할 수 없는 사용자입니다.');
        }
    }

    function set_user_info(userid, usertypecode) {
        $('input[name=lastname]').val($('input[name=id_' + userid + '_lastname]').val());
        $('input[name=lastname]').prop('disabled', true);
        $('input[name=username]').val($('input[name=id_' + userid + '_username]').val());
        $('input[name=username]').prop('disabled', true);
        $('input[name=phone]').val($('input[name=id_' + userid + '_phone]').val());
        $('input[name=phone]').prop('disabled', true);
        $('input[name=userid]').val($('input[name=id_' + userid + '_userid]').val());
        $('input[name=usertypecode]').val($('input[name=id_' + userid + '_usertypecode]').val());
        $('#select_user').dialog("close");
    }

    /**
     * 페이지이동
     * @param {type} page
     * @returns {undefined}
     */
    function class_students_search(page) {

        $.ajax({
            url: 'edu_enrolment_list_user.php',
            method: 'POST',
            data: {
                'page': page,
                'searchtext': $('#frm_course_teacher').find('input[name=searchtext]').val(),
            },
            success: function (data) {
                $("#course_teacher").parent().html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
    /**
     * 검색어 넘기는 함수
     * @returns {undefined}
     */
    function teacher_search_name() {
        $.ajax({
            url: 'edu_enrolment_list_user.php',
            method: 'POST',
            data: {
                'page': <?php echo $currpage; ?>,
                'searchtext': $('#frm_course_teacher').find('input[name=searchtext]').val()
            },
            success: function (data) {
                $("#course_teacher").parent().html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
    /**
     * 엔터입력 submit
     
     * @type type     */
    $("input[name=searchtext]").keypress(function (e) {
        if (e.keyCode == 13) {
            teacher_search_name();
        }
    });
</script>   