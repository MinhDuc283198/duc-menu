<?php
/**
 * 수강신청목록 상세보기
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');
require_once($CFG->dirroot . '/chamktu/enrol/lib.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
$coursetype = optional_param('coursetype', 1, PARAM_INT); //1:직무 2:위탁
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);

if (!empty($id)) {
    $sql_select = "SELECT *, lca.id lcaid,  lca.courseid lcacourseid, lca.timecreated, lca.email lcaemail, u.email uemail, lca.price lcaprice, lc.courseid as lccid, u.id getuserid"
            . ", (select codename from {lmsdata_code} where lco.coursegrade = id ) as coursegrade ";
    $sql_from = " FROM {lmsdata_course_applications} lca "
            . "join {lmsdata_class} lc on lc.courseid=lca.courseid "
            . "join {lmsdata_course} lco on lco.id=lc.parentcourseid "
            . "join {course} co on co.id = lc.courseid "
            . "join {user} u on u.id = lca.userid ";
    $sql_where = " where lca.id = $id";

    //$sql_orderby = " order by lg.id desc";
    $data = $DB->get_record_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);

    $type_arrs = array(
        1 => '임직원 강의',
        2 => '멘토 강의',
        3 => '교육생 강의',
        4 => '임직원, 멘토 강의',
        5 => '멘토, 교육생 강의',
        6 => '임직원, 교육생 강의',
        7 => '전체 강의'
    );
    $cancle = $DB->get_record('lmsdata_applications_cancel', array('courseid' => $data->lcacourseid, 'userid' => $data->userid));
}

$getpointsql = 'SELECT course, SUM(point) lecturepoint FROM {okmedia} WHERE course = :course GROUP BY course';
$getpoint = $DB->get_record_sql($getpointsql, array('course' => $data->lcacourseid));
//$sql = 'select progress from {lcmsprogress_grades} lg '
//        . 'join {lcmsprogress} lc on lc.id = lg.lcmsprogress '
//        . 'where lc.course = :courseid '
//        . 'and lg.userid = :userid';
//$progress = $DB->get_record_sql($sql, array('courseid' => $data->lccid, 'userid' => $data->userid));
//$progress1=substr($progress->progress,0,5);
//$progress1 = sprintf("%2.2f" ,$progress->progress);

$sql1 = 'select sum(finalgrade) as final from {grade_grades} gg '
        . 'join {grade_items} gi on gg.itemid = gi.id '
        . 'where gg.userid = :userid '
        . 'and gi.courseid = :courseid '
        . 'and gi.itemmodule = :itemmodule';

$quiz = $DB->get_record_sql($sql1, array('userid' => $data->userid, 'courseid' => $data->lccid, 'itemmodule' => 'quiz'));
$assign = $DB->get_record_sql($sql1, array('userid' => $data->userid, 'courseid' => $data->lccid, 'itemmodule' => 'assign'));
$forum = $DB->get_record_sql($sql1, array('userid' => $data->userid, 'courseid' => $data->lccid, 'itemmodule' => 'forum'));
//$quiz = substr($quiz->final,0,5);
//$assign = substr($assign->final,0,5);
//$forum = substr($forum->final,0,5);
$quiz1 = sprintf("%2.2f", $quiz->final);
$assign1 = sprintf("%2.2f", $assign->final);
$forum1 = sprintf("%2.2f", $forum->final);

$sql2 = 'select sum(finalgrade) as final from {grade_grades} gg '
        . 'join {grade_items} gi on gg.itemid = gi.id '
        . 'where gg.userid = :userid '
        . 'and gi.courseid = :courseid '
        . 'and gi.itemtype = :itemtype';
$total1 = $DB->get_record_sql($sql2, array('userid' => $data->userid, 'courseid' => $data->lccid, 'itemtype' => 'course'));
$total = sprintf("%2.2f", $total1->final);

$certinum = edu_enrolment_certinum($data->lcaid, $data->userid, $data->lccid);

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);

//// 하드코딩 부분
//if ($data->completionstatus == 0) {
//    $restudy_date = strtotime("+1 week 23:59:59", $data->learningend);
//} else {
//    $restudy_date = strtotime("+6 month 23:59:59", $data->learningend);
//}

if (!empty($data->bankcode)) {
    $bankname = $DB->get_record('inicode', array('type' => 'bank', 'code' => $data->bankcode));
    $data->bankname = $bankname->name;
}
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title">수강신청 상세보기</h3>
        <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <a href="./edu_enrolment_list.php">수강신청목록</a> > <strong>수강신청 상세보기</strong></div>
        <form name="" id="course_search"  method="post" enctype="multipart/form-data">            
            <h4 class="table_title" style="margin-top: 50px;margin-left: 30px;">사용자정보</h4>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr style="height: 45px">
                        <td class="field_title">신청자</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $data->lastname . $data->firstname; ?></span>
                        </td>
                        <td class="field_title">아이디</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $data->username; ?></span>
                        </td>
                    </tr>                    
                    <tr style="height: 45px">
                        <td class="field_title">연락처</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $data->phone ? substr($data->phone, 0, 3) . '-' . substr($data->phone, 3, 4) . '-' . substr($data->phone, 7, 4) : '-' ?></span>
                        </td>
                        <td class="field_title">이메일</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo empty($data->uemail) ? $data->lcaemail : $data->uemail; ?></span>
                        </td>
                    </tr> 
                </tbody>
            </table>
            <h4 class="table_title" style="margin-top: 230px;margin-left: 30px;">수강신청정보</h4>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr style="height: 45px">
                        <td class="field_title">년도</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $data->classyear . '년'; ?></span>
                        </td>
                        <td class="field_title">기수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $data->classnum . '기'; ?></span>
                        </td>
                    </tr>

                    <tr style="height: 45px">
                        <td class="field_title">과정명</td>
                        <td class="field_value" colspan="3">
                            <span class="text_field"><a href="<?php echo $CFG->wwwroot ?>/course/view.php?id=<?php echo $data->lccid ?>" target="_blank"><?php echo $data->fullname; ?></a></span>
                        </td>
                    </tr>     
                    <tr style="height: 45px"> <!--수강형식, 상세-->
                        <td class="field_title">수강형식</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo empty($data->classobject) ? $type_arrs[$data->classtype] : $type_arrs[$data->classobject]; ?></span>
                        </td>
                        <td class="field_title">포인트</td>
                        <td class="field_value" >                            
                            <span class="text_field"><?php echo empty($getpoint->lecturepoint) ? '0 P' : $getpoint->lecturepoint . ' P'; ?></span>
                        </td>
                    </tr> 
                    <tr style="height: 45px"> <!--시작, 끝-->
                        <td class="field_title">수강신청 기간</td>
                        <td class="field_value">
                            <span class="text_field"><?php
                                if ($data->enrolmentstart) {
                                    echo date('Y-m-d', $data->enrolmentstart) . ' ~ ' . date('Y-m-d', $data->enrolmentend);
                                } else {
                                    echo '-';
                                }
                                ?></span>
                        </td>
                        <td class="field_title">접수시간</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo date('Y-m-d', $data->timecreated); ?></span>
                        </td>
                    </tr>     
                </tbody>
            </table>
            <h4 class="table_title" style="margin-top: 230px;margin-left: 30px;">학습 및 평가정보</h4>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr style="height: 45px">
                        <td class="field_title">학습기간</td>
                        <td class="field_value" colspan="3">
<!--                            <span class="text_field"><?php //echo date('y-m-d',$data->studystart);      ?> ~ <?php //echo date('y-m-d',$data->studyend);      ?></span>-->
                            <span class="text_field"><?php echo date('y-m-d', $data->learningstart); ?> ~ <?php echo date('y-m-d', $data->learningend); ?>
                        </td>
<!--                        <td class="field_title">재학습기간</td>
                        <td class="field_value">
                            <span class="text_field"><?php //echo date('y-m-d',$data->restudystart);      ?> ~ <?php //echo date('y-m-d',$data->restudyend);      ?></span>
                            <span class="text_field"><?php echo date('y-m-d', $data->learningend); ?> ~ <?php echo date('y-m-d', $restudy_date); ?></span>
                        </td>-->
                    </tr>

                    <tr style="height: 45px">
                        <td class="field_title">이수여부</td>
                        <td class="field_value">
                            <span class="text_field">
                                <?php
                                $ifcomplete = $DB->get_record('course_completions', array('userid' => $data->getuserid, 'course' => $data->lccid));
                                echo!empty($ifcomplete->timecompleted) ? '이수' : '미이수';
                                ?>
                            </span>
                        </td>
                        <td class="field_title">이수번호</td>
                        <td class="field_value" >                            
                            <span class="text_field"><?php echo $certinum ?></span>
                        </td>
                    </tr>

                    <?php
                    $getprogresssql = 'SELECT cm.id, cmc.completionstate, cmc.id AS cid, cmc.userid, cm.course 
                                                                        FROM {course_modules} cm 
                                                                        LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid 
                                                                        WHERE cm.course = :courseno and cmc.completionstate is not null and cm.module != :module';
                    $getprogress = $DB->get_records_sql($getprogresssql, array('userid' => $data->userid, 'courseno' => $data->lcacourseid, 'module' => 16));
                    $getprogresssql_all = 'SELECT cm.id, cmc.completionstate, cmc.id AS cid, cmc.userid, cm.course 
                                                                            FROM {course_modules} cm 
                                                                            LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid 
                                                                            WHERE cm.course = :courseno and cm.module != :module';
                    $getprogress_all = $DB->get_records_sql($getprogresssql_all, array('userid' => $data->userid, 'courseno' => $data->lcacourseid, 'module' => 16));
                    if ($getprogress_all) {
                        $setprogress = round((count($getprogress) / count($getprogress_all)) * 100, 2);
                    } else {
                        $setprogress = 0;
                    }
                    ?>

                    <tr style="height: 45px">
                        <td class="field_title">학습진도율</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo!empty($setprogress) ? $setprogress . ' %' : '0 %' ?></span>
                        </td>
                        <td class="field_title">성적처리일</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo date('Y-m-d', $data->gradeviewend); ?></span>
                        </td>
                    </tr>
<!--                    <tr style="height: 45px">
                        <td class="field_title">과제점수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $assign1; ?></span>
                        </td>
                        <td class="field_title">토론점수</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $forum1; ?></span>
                        </td>
                    </tr>
                    <tr style="height: 45px">
                        <td class="field_title">시험점수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $quiz1 ?></span>
                        </td>
                        <td class="field_title">총점</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $total ?></span>
                        </td>
                    </tr>-->
                </tbody>
            </table>
            <div id="btn_area">
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_enrolment_list.php';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            return false;
    }
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }

    /**
     * 강제 이수처리기능
     
     * @param {type} id
     * @returns {undefined}     */
    function change_complate(id) {
        if (confirm("이수처리 하시겠습니까? 이수변경시 다시 되돌릴 수 없습니다.") == true) {
            $.ajax({
                url: 'edu_complate.ajax.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function (data) {
                    if (data.status == 'success') {
                        alert('변경되었습니다.');
                        location.reload();
                    }
                }
            });
        }
    }
</script>
<?php
include_once ('../inc/footer.php');
