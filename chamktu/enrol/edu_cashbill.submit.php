<?php
/**
 * 현금영수증 발급 submit 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$page = optional_param('page', 0, PARAM_INT);
$customerName = optional_param('customerName', '', PARAM_RAW);
$email = optional_param('email', '', PARAM_RAW);
$hp = optional_param('hp', '', PARAM_RAW);
$orderNumber = optional_param('orderNumber', '', PARAM_RAW);
$itemName = optional_param('itemName', '', PARAM_RAW);
$userid = optional_param('userid', 0, PARAM_INT);
$application = optional_param('application', 0, PARAM_INT);
$tradeUsage = optional_param('tradeUsage', '', PARAM_RAW);
$taxationType = optional_param('taxationType', '', PARAM_RAW);
$identityNum = optional_param('identityNum', '', PARAM_RAW);
$supplyCost = optional_param('supplyCost', 0, PARAM_INT);
$tax = optional_param('tax', 0, PARAM_INT);
$serviceFee = optional_param('serviceFee', 0, PARAM_INT);
$totalAmount = optional_param('totalAmount', 0, PARAM_INT);

  /**
  * 팜빌 현금영수증 API PHP SDK Example
  *
  * PHP SDK 연동환경 설정방법 안내 : blog.linkhub.co.kr/584
  * 업테이트 일자 : 2017-05-22
  * 연동기술지원 연락처 : 1600-8539 / 070-4304-2991~2
  * 연동기술지원 이메일 : code@linkhub.co.kr
  *
  * <테스트 연동개발 준비사항>
  * 1) 19, 22번 라인에 선언된 링크아이디(LinkID)와 비밀키(SecretKey)를
  *    링크허브 가입시 메일로 발급받은 인증정보를 참조하여 변경합니다.
  * 2) 팝빌 개발용 사이트(test.popbill.com)에 연동회원으로 가입합니다.
  */

  require_once '../../../popbill/Popbill/PopbillCashbill.php';

  // 링크 아이디
  $LinkID = 'CHAMCAMPUS';

  // 발급받은 비밀키. 유출에 주의하시기 바랍니다.
  $SecretKey = 'hi0u17QGKkIkF2A+XLiJ5PDpT12KPRPJmXNFlaPNBSE=';

  // 통신방식 기본은 CURL , curl 사용에 문제가 있을경우 STREAM 사용가능.
  // STREAM 사용시에는 allow_fopen_url = on 으로 설정해야함.
  define('LINKHUB_COMM_MODE','CURL');

  $CashbillService = new CashbillService($LinkID, $SecretKey);

  // 연동환경 설정값, 개발용(true), 상업용(false)
  $CashbillService->IsTest(false);


  /**
  * 1건의 현금영수증을 즉시발행합니다.
  * - 발행일 기준 오후 5시 이전에 발행된 현금영수증은 다음날 오후 2시에 국세청
  *   전송결과를 확인할 수 있습니다.
  * - 현금영수증 국세청 전송 정책에 대한 정보는 "[현금영수증 API 연동매뉴얼]
  *   > 1.4. 국세청 전송정책"을 참조하시기 바랍니다.
  * - 취소현금영수증 작성방법 안내 - http://blog.linkhub.co.kr/702
  */

    //include '../../../popbill/CashbillExample/common.php';
  // 팝빌 회원 사업자번호, '-' 제외 10자리
	$testCorpNum = '1078206408';

  // 문서관리번호, 사업자별로 중복없이 1~24자리 영문, 숫자, '-', '_' 조합으로 구성
	$mgtKey = $application.'_'.time().'_'.$userid;

  // 메모
	$memo = '현금영수증 즉시발행';


	// 현금영수증 객체 생성
	$Cashbill = new Cashbill();

  // [필수] 현금영수증 문서관리번호,
	$Cashbill->mgtKey = $mgtKey;

  // [필수] 거래유형, (승인거래, 취소거래) 중 기재
	$Cashbill->tradeType = '승인거래';

  // [취소 현금영수증 발행시 필수] 원본 현금영수증 국세청 승인번호
  // 국세청 승인번호는 GetInfo API의 ConfirmNum 항목으로 확인할 수 있습니다.
  // $Cashbill->orgConfirmNum = '';

  // [필수] 거래처 식별번호, 거래유형에 따라 작성
  // 소득공제용 - 주민등록/휴대폰/카드번호 기재가능
  // 지출증빙용 - 사업자번호/주민등록/휴대폰/카드번호 기재가능
	$Cashbill->identityNum = $identityNum;

  // [필수] 과세, 비과세 중 기재
	$Cashbill->taxationType = $taxationType;

  // [필수] 공급가액, ','콤마 불가 숫자만 가능
	$Cashbill->supplyCost = $supplyCost;

  // [필수] 세액, ','콤마 불가 숫자만 가능
	$Cashbill->tax = $tax;

  // [필수] 봉사료, ','콤마 불가 숫자만 가능
  $Cashbill->serviceFee = $serviceFee;

  // [필수] 거래금액, ','콤마 불가 숫자만 가능
	$Cashbill->totalAmount = $totalAmount;

  // [필수] 소득공제용, 지출증빙용 중 기재
	$Cashbill->tradeUsage = $tradeUsage;


  // [필수] 발행자 사업자번호
	$Cashbill->franchiseCorpNum = $testCorpNum;

  // 발행자 상호
	$Cashbill->franchiseCorpName = '참교육 원격연수원';

  // 발행자 대표자 성명
	$Cashbill->franchiseCEOName = '조창익';

  // 발행자 주소
	$Cashbill->franchiseAddr = '서울특별시 서대문구 경기대로 82 광산빌딩 4층';

  // 발항자 연락처
	$Cashbill->franchiseTEL = '02-2670-9465';



  // 고객명
  $Cashbill->customerName = $customerName;

  // 상품명
	$Cashbill->itemName = $itemName;

  // 주문번호
	$Cashbill->orderNumber = $orderNumber;

  // 고객 메일주소
	$Cashbill->email = $email;

  // 고객 휴대폰 번호
	$Cashbill->hp = $hp;
        

  // 발행시 알림문자 전송여부
	$Cashbill->smssendYN = false;

	try {
		$result = $CashbillService->RegistIssue($testCorpNum, $Cashbill, $memo);
		$code = $result->code;
		$message = $result->message;
	}
	catch(PopbillException $pe) {
		$code = $pe->getCode();
		$message = $pe->getMessage();
	}
        
        if($code == 1){
            if($tradeUsage == '소득공제용'){
                $cshrtype = 0;
            } else {
                $cshrtype = 1;
            }
            $new = new stdClass();
            
            $new->identitynum = $identityNum;
            $new->tradetype = '승인거래';
            $new->taxationtype = $taxationType;
            $new->supplycost = $supplyCost;
            $new->tax = $tax;
            $new->servicefee = $serviceFee;
            $new->price = $totalAmount;
            $new->tradeusage = $cshrtype;
            $new->customername = $customerName;
            $new->ordernumber = $orderNumber;
            $new->email = $email;
            $new->hp = $hp;
            $new->application = $application;
            $new->itemname = $itemName;
            $new->userid = $userid;
            
            $DB->insert_record('lmsdata_receipt',$new);
            
            $applicationinfo = $DB->get_record('lmsdata_course_applications', array('id'=>$application));
            $applicationinfo->cshrresult = 1;
            $applicationinfo->cshrtype = $cshrtype;
            
            $DB->update_record('lmsdata_course_applications', $applicationinfo);
        }

?>
<script>
    alert('<?php echo $message;?>');
    location.href='<?php echo $CFG->wwwroot?>/chamktu/enrol/edu_enrolment_list.php?page=<?php echo $page;?>';
</script>
</html>
