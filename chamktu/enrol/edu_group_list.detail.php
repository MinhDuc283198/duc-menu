<?php
/**
 * 단체신청목록 상세 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin

require_login(0, false);
$coursetype = optional_param('coursetype', 1, PARAM_INT); //1:직무 2:위탁
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);

if (!empty($id)) {
    $sql = " SELECT lg.*, ls.classyear, ls.classnum
        ,(select sum(price) from {lmsdata_course_applications} lca join {lmsdata_groupapply} lg1 on lca.userid = lg1.userid and lca.courseid = lg1.classid where lca.groupcode = lg.groupcode) as totalprice
                    FROM {lmsdata_grouplist} lg join {lmsdata_schedule} ls on ls.id=lg.scheduleid 
                    WHERE lg.id = :id  order by lg.id desc";
    $params = array('id' => $id);
    $groupinfo = $DB->get_record_sql($sql, $params);


    $lists_sql = "select lga.*, co.id coid, co.fullname, lg.groupcode, u.lastname, u.username, lg.id lgid, lc.courseid, lca.price, lca.id lcaid  ";
    $lists_count_sql = "select count(*) ";

    $lists_from = " from {lmsdata_groupapply} lga 
        join {lmsdata_grouplist} lg on lg.id = lga.groupid 
        JOIN {lmsdata_course_applications} lca ON lca.groupcode = lg.groupcode and lga.userid = lca.userid and lca.status != 'cancel'
        JOIN {lmsdata_class} lc  on lc.id = lca.courseid
        JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
        JOIN {course} co ON co.id = lco.courseid
        JOIN {user} u ON lga.userid = u.id
        where lg.id = $id and lga.status !=2 ";
    $lists = $DB->get_records_sql($lists_sql . $lists_from);
    $count_lists = $DB->count_records_sql($lists_count_sql . $lists_from);

    $arr = array(1 => get_string('individual_payment', 'local_group_application'), 2 => get_string('bill_payment', 'local_group_application'), 3 => get_string('administrative_room_settlement', 'local_group_application'));

    $context = context_system::instance();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_group_application', "group_application", $id, 'id');
    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_group_application/group_application/' . $id . '/' . $filename);
        if ($file->get_filesize() > 0) {
            $fileobj = "<span id ='file'><a href=\"$path\">$iconimage</a>";
            $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</span>';
        }
    }
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title">단체신청 상세보기</h3>

        <form name="" id="course_search"  method="post" enctype="multipart/form-data">
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>

                    <tr style="height: 45px">
                        <td class="field_title">년도</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $groupinfo->classyear . '년'; ?></span>
                        </td>
                        <td class="field_title">기수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $groupinfo->classnum . '기'; ?></span>
                        </td>
                    </tr>

                    <tr style="height: 45px">
                        <td class="field_title">신청인원</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $groupinfo->usercount . '명'; ?></span>
                        </td>
                        <td class="field_title">사용인원</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $count_lists . '명'; ?></span>
                        </td>
                    </tr> 
                    <tr style="height: 45px">
                        <td class="field_title">대표자명</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $groupinfo->repusername; ?></span>
                        </td>
                        <td class="field_title">코드번호</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $groupinfo->groupcode; ?></span>
                        </td>
                    </tr>     
                    <tr style="height: 45px">
                        <td class="field_title">발행일</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo date('Y-m-d', $groupinfo->timecreated) ?></span>
                        </td>
                        <td class="field_title">신청서</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $fileobj; ?></span>
                        </td>
                    </tr>
                        <tr style="height: 45px">
                            <td class="field_title">결제방법</td>
                            <td class="field_value">
                                <span class="text_field"><?php echo $arr[$groupinfo->paymentmethod]; ?></span>
                            </td>
                            <td class="field_title">총결제금액</td>
                            <td class="field_value">
                                <span class="text_field"><?php echo number_format($groupinfo->totalprice) . '원' ?></span>
                            </td>
                        </tr>
                        <tr style="height: 45px" >
                            <td class="field_title">결제여부</td>
                            <td class="field_value" colspan="3">
                                <span class="text_field">
                                <?php if($groupinfo->status != 2) { ?>
                                    <input type="button"  value="결제승인" onclick="admin_payment('<?php echo $id; ?>')"/>
                                <?php }else { echo '결제완료' ;} ?></span>
                            </td>
                        </tr>
                </tbody>
            </table>
            <br><br><br><br><br>            
            <table style="margin-top: 40px; ">
                <thead>
                    <tr>
                        <!--<th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>-->
                        <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                        <th scope="row" width="30%">과정명</th>
                        <th scope="row" width="15%">결제금액</th>
                        <th scope="row" width="15%">이름(아이디)</th>
                        <th scope="row" width="15%">사용일</th>
                        <th scope="row" width="10%">승인</th>
                    </tr>
                </thead>
                <?php if ($count_lists === 0) { ?>
                    <tr>
                        <td colspan="10"><?php echo get_string('empty_data', 'local_group_application'); ?></td>
                    </tr>
                    <?php
                } else {
                    $startnum = $count_lists - (($currpage - 1) * $perpage);
                    foreach ($lists as $list) {
                        ?>
                        <tr>
                            <!--<td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $class->lcid; ?>"/></td>-->
                            <td><?php echo $startnum--; ?></td>
                            <td><?php echo $list->fullname ?></td>
                            <td><?php echo number_format($list->price) . '원' ?></td>
                            <td>
                                <a href="<?php echo $CFG->wwwroot . '/chamktu/enrol/edu_enrolment_list.detail.php?id=' . $list->lcaid; ?>">
                                    <?php echo $list->lastname . ' (' . $list->username . ')' ?></a>
                            </td>
                            <td><?php echo date('Y-m-d', $list->timecreated) ?></td>
                            <td>
                                <?php if (!$list->status) { ?>
                                    <input type="button" class="normal_btn"  value="승인" onclick="apply('<?php echo $list->id; ?>', '<?php echo $list->courseid; ?>')"/>
                                <?php
                                } else {
                                    echo "승인완료";
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_group_list.php';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    /**
     * 그룹신청 승인기능
     * @param {type} id
     * @param {type} courseid
     * @returns {undefined}
     */
    function apply(id, courseid) {
//        var course_list =[];
//        course_list.push(id) ;
        if (confirm("승인하시겠습니까?") == true) {
            $.ajax({
                url: 'edu_group_apply.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    courseid: courseid
                },
                success: function (data, textStatus, jqXHR) {
                    alert('승인되었습니다.');
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //alert(jqXHR.responseText);
                }
            });
        }
    }
    /**
     * 그룹신청 결제승인기능
     * @param {type} id
     * @returns {undefined}
     */
    function admin_payment(id){
        if (confirm("결제승인하시겠습니까?") == true) {
            $.ajax({
                url: 'edu_group_payment.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'success') {
                        alert('승인되었습니다.');
                        location.reload();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //alert(jqXHR.responseText);
                }
            });
        }
    }
</script>
<?php
include_once ('../inc/footer.php');
