<?php

/**
 * 템플릿발송기능 ajax페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->dirroot . '/chamktu/lib/lib.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');

$templateid = required_param('templateid', PARAM_INT);
$enrolmentids = required_param_array('enrolmentid', PARAM_INT);

$returnvalue = new stdClass();
$main = 0;
foreach ($enrolmentids as $enrolmentid){
    $applications = $DB->get_record('lmsdata_course_applications',array('id'=>$enrolmentid));
    $courseid = $DB->get_field('lmsdata_class', 'courseid', array('id' => $applications->courseid));
    $sendtemplate = $DB->get_record('lmsdata_sendtemplate',array('id'=>$templateid));
    $data['courseid'] = $courseid;
    $data['applicationid'] = $applications->id;
    $data['userid'] = $applications->userid;
    $data['contents'] =$sendtemplate->contents;
    $data['title'] =$sendtemplate->title;    
    $data = admin_sendvarsetting($data);
    $subject =  $data['title']; 
    $massage =$data['contents'];
    $link = $sendtemplate->link; 
    $user = $DB->get_record('user',array('id'=>$applications->userid));

    
   if($sendtemplate->type == 'email'){
        $returnval = reday_to_send_email($user,$subject,$massage);
    }
    if($sendtemplate->type == 'push'){ 
        $returnval = reday_to_send_push($user,$subject,$massage,$link);
    }    
    
   }
   if(!empty($returnval)){
        $returnvalue->status = 'success';   
   }
    
//@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);