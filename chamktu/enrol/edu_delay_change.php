<?php
/**
 * 연수 연기신청 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$id = optional_param('id', 0, PARAM_INT);

//검색용 파라미터

$sql_select = "SELECT lad.*, lc.parentcourseid, lc.id lcid, lc.classyear, lc.classnum, co.fullname, u.lastname, lca.id lcaid, ls.classyear lsclassyear, ls.classnum lsclassnum, ls.id lsid ";
$sql_from = " FROM {lmsdata_applications_delay} lad "
        . "join {lmsdata_course_applications} lca on lca.id=lad.applicationid "
        . "join {lmsdata_class} lc on lc.id=lca.courseid "
        . "join {course} co on co.id = lc.courseid "
        . "join {user} u on u.id = lca.userid "
        . "join {lmsdata_schedule} ls on lad.scheduleid = ls.id where lad.id = $id ";
$sql_orderby = " order by lad.id desc";

$courses = $DB->get_record_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$arr = array(0 => '신청', 1 => '승인', 2 => '이월완료');
?>

<div class="popup_content">
    <form id="frm_delay" name="frm_delay"  method="post" action="edu_delay_change.ajax.php" >
        <input type="hidden" name="delayid" value="<?php echo $courses->id;?>" />
        <input type="hidden" name="type" value="2" />
    <table cellpadding="0" cellspacing="0" class="detail">
        <tbody>
            <tr style="height: 45px">
                <td class="field_title">년도</td>
                <td class="field_value" >
                    <span class="text_field"><?php echo $courses->classyear . '년'; ?></span>
                </td>
                <td class="field_title">기수</td>
                <td class="field_value">
                    <span class="text_field"><?php echo $courses->classnum . '기'; ?></span>
                </td>
            </tr>                    
            <tr style="height: 45px">
                <td class="field_title">과정명</td>
                <td class="field_value">
                    <span class="text_field"><?php echo $courses->fullname; ?></span>
                </td>
                <td class="field_title">신청자</td>
                <td class="field_value" >
                    <span class="text_field"><?php echo $courses->lastname; ?></span>
                </td>
            </tr> 
            <tr style="height: 45px">
                <td class="field_title">사유</td>
                <td class="field_value" colspan="3">
                    <span class="text_field"><?php echo $courses->reason; ?></span>
                </td>
            </tr> 
            <?php
            $today = time();
            $sql = "SELECT lc.*, co.fullname ";
            $from = " FROM {lmsdata_class} lc "
                    . " join {course} co on lc.courseid = co.id "
                    . " where lc.classtype = 1 and lc.parentcourseid = $courses->parentcourseid and lc.learningend > $today and lc.id != $courses->lcid ";
//            $orderby = " order by lad.id desc";
            
            $delay_classnums = $DB->get_records_sql($sql . $from);
            ?>
            <tr style="height: 45px">
                <td class="field_title">변경기수</td>
                <td class="field_value" colspan="3">
                    <select title="class" name="classid" id="classid" class="w_160"> 
                        <option value="0">기수</option>
                        <?php
                        foreach ($delay_classnums as $delay_classnum) {
                            echo '<option value="' . $delay_classnum->id . '"' . $selected . '> ' . $delay_classnum->classnum . '기</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr> 
        </tbody>
    </table>
    </form>
</div> <!--Contents End-->

