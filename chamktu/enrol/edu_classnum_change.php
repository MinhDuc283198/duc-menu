<?php
/**
 * 과정변경 팝업 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

?>

<div class="popup_content" id="course_teacher">
    <form id="frm_course_teacher" name="frm_course_teacher" onsubmit="return false;">
        <table cellpadding="0" cellspacing="0" class="detail">
            <tbody>
                <tr>
                    <td class="field_title">수강형식</td>
                    <td class="field_value" colspan="3">
                        <?php
                        $type_arrs = array(1 => '직무교육', 2 => '위탁교육');
                        $checked = 'checked';
                        foreach ($type_arrs as $key => $val) {
                            $radiobutton .= '<input type="radio" name="classtype" ' . $checked . ' value="' . $key . '" onclick="radio_check()">' . $val;
                            $checked = '';
                        }
                        echo $radiobutton;
                        ?>

                    </td>
                </tr>
                <tr>
                    <td class="field_title">년도</td>
                    <td class="field_value">
                        <select title="year" name="classyear" id="classyear" onchange="classnumchange(1);" class="w_160">

                            <?php
                            $yearRange = 10;
                            $currentYear = date('Y');
                            $maxYear = ($currentYear + $yearRange);
                            echo '<option value="0" ' . $disabled . '>년도</option>';
                            foreach (range($currentYear, $maxYear) as $year) {
                                $selected = '';
                                if ($year == $course->classyear) {
                                    $selected = 'selected';
                                }
                                echo '<option value="' . $year . '"' . $selected . ' ' . $disabled . '>' . $year . '년</option>';
                            }
                            ?>
                        </select> 
                    </td>
                    
                    <td class="field_title" id="classnum_select1">기수</td>
                    <td class="field_value" id="classnum_select2">
                        <select title="classnum_select" name="classnum_select" id="classnum_select" class="w_160" onchange="classnumchange(2)">
                            <option value="0">기수선택</option>
                        </select>
                    </td>

                    <td class="field_title" id="consignment_select1">위탁선택</td>
                    <td class="field_value" id="consignment_select2">
                        <select title="consignment_select" name="consignment_select" id="consignment_select" class="w_160" onchange="classnumchange(2)" >
                            <option value="0">위탁선택</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="field_title" >과정선택</td>
                    <td class="field_value"  colspan="3">
                        <select title="course_select" name="course_select" id="course_select" class="w_160" >
                            <option value="0">과정선택</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $(document).ready(function () {
        $('#consignment_select1').hide();
        $('#consignment_select2').hide();
    });
    /**
     * 과정변경 select박스 change ajax
     * @param {type} type
     * @returns {undefined}
     */
    function classnumchange(type) {
        var year = $("select[name=classyear]").val();        
        var classtype = $('input:radio[name="classtype"]:checked').val();
        var categories = null;
        if(classtype == 1){
            var sel1 = $('#classnum_select');
            var id = $("select[name=classnum_select]").val();
        }else{
            var sel1 = $('#consignment_select');
            var id = $("select[name=consignment_select]").val();
        }
        
        $.ajax({
            url: 'edu_course_change.php',
            type: 'POST',
            dataType: 'json',
            data: {
                year: year,
                type: type,                
                classtype: classtype,
                id: id,
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 'success') {
                    categories = data.categories;
                }
                if (type == 1) {
                    cata_clean_select(sel1);
                    if(classtype == 1){
                        classnum_add_select_options(sel1, categories);
                    }else{
                        schedule_add_select_options(sel1, categories);
                    }
                }else{
                    cata_clean_select($('#course_select'));
                    course_add_select_options($('#course_select'), categories);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }
    /**
     * select box직무 차수변경
     * @param {type} sel
     * @param {type} options
     * @returns {undefined}
     */
    function classnum_add_select_options(sel, options) {
        $.each(options, function (i, option) {
            sel.append($('<option>', { 
                value: option.id,
                text : option.classnum+'기' 
            }));
        });
    }
    /**
     * select box위탁 위탁변경
     * @param {type} sel
     * @param {type} options
     * @returns {undefined}
     */
    function schedule_add_select_options(sel, options) {
        $.each(options, function (i, option) {
            sel.append($('<option>', { 
                value: option.id,
                text : option.consignmenttitle
            }));
        });
    }
    /**
     * select box 과정 변경
     * @param {type} sel
     * @param {type} options
     * @returns {undefined}
     */
    function course_add_select_options(sel, options) {
        $.each(options, function (i, option) {
            sel.append($('<option>', { 
                value: option.courseid,
                text : option.coursename 
            }));
        });
    }
    /**
     * 직무, 위탁 라디오버튼 변경
     * @returns {undefined}
     */
    function radio_check() {
        var classtype = $('input:radio[name="classtype"]:checked').val();
        var sel1 = $('#classnum_select');        
        cata_clean_select(sel1);
        $("select[name=classyear]").val(0);
        $("select[name=course_select]").val(0);
        
        if (classtype == 1) {
            $('#consignment_select1').hide();
            $('#consignment_select2').hide();
            $('#classnum_select1').show();
            $('#classnum_select2').show();
        }else {
            $('#classnum_select1').hide();
            $('#classnum_select2').hide();
            $('#consignment_select1').show();
            $('#consignment_select2').show();
        }
    }
</script>   