<?php
/**
 * 연수 연기신청 목록 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$classtype = optional_param('classtype', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$paymentmethod = optional_param('paymentmethod', 0, PARAM_INT);

//검색용 파라미터

if ($classyear) {
    $sql_where[] = " lc.classyear = :classyear ";
    $params['classyear'] = $classyear;
}
if ($classnum) {
    $sql_where[] = " lc.classnum = :classnum ";
    $params['classnum'] = $classnum;
}

if (!empty($searchtext)) {
    $like = array();
    $like[] = $DB->sql_like('u.lastname', ':lastname');
    $like[] = $DB->sql_like('co.fullname', ':fullname');
    $params['lastname'] = '%' . $searchtext . '%';
    $params['fullname'] = '%' . $searchtext . '%';
    $sql_where[] = ' ( ' . implode(' OR ', $like) . ') ';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lad.*, lc.parentcourseid, lc.classyear, lc.classnum, co.fullname, u.lastname, lca.id lcaid, ls.classyear lsclassyear, ls.classnum lsclassnum, ls.id lsid ";
$sql_from = " FROM {lmsdata_applications_delay} lad "
        . "join {lmsdata_course_applications} lca on lca.id=lad.applicationid "
        . "join {lmsdata_class} lc on lc.id=lca.courseid "
        . "join {course} co on co.id = lc.courseid "
        . "join {user} u on u.id = lca.userid "
        . "join {lmsdata_schedule} ls on lad.scheduleid = ls.id ";
$sql_orderby = " order by lad.id desc";

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);


$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(0 => '신청', 1 => '승인', 2 => '이월완료', 3=>'취소');
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title">연기신청목록</h3>
        <form name="course_search" id="course_search" class="search_area" action="edu_delay_list.php" method="get">
            <input type="hidden" name="page" value="1" />
            <div style="float:left;">
                <select title="year" name="classyear" id="classyear" class="w_160" onchange="year_changed(this, 'lmsdata_class', 1);">
                    <option value="0">년도</option>
                    <?php
                    $yearRange = 1;
                    $currentYear = date('Y');
                    $maxYear = ($currentYear + $yearRange);

                    foreach (range(2017, $maxYear) as $year) {
                        $selected = '';
                        if ($year == $classyear) {
                            $selected = 'selected';
                        }
                        echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                    }
                    ?>
                </select> 
                <select title="class" name="classnum" id="classnum" class="w_160">
                    <option value="0">기수</option>
                    <?php
                    if ($classyear) {
                        if ($classyear)
                            $classnum_where .= " where lc.classyear = $classyear";
                        $datas = $DB->get_records_sql($sql_select . $sql_from . $classnum_where);

                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->classnum == $classnum) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                        }
                    }
                    ?>
                </select>

            </div>
            <br><br><br>

            <div style="float:left;">
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="과정명 또는 신청자명을 입력하세요."  class="search-text"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
            </div>
        </form><!--Search Area2 End-->

        <table>
            <thead>
                <tr>
                    <!--<th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>-->
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                    <th scope="row" width="5%">년도</th>
                    <th scope="row" width="5%">기수</th>
                    <th scope="row" width="15%">과정명</th>
                    <th scope="row" width="5%">연기희망년도</th>
                    <th scope="row" width="5%">연기희망기수</th>
                    <th scope="row" width="15%">이월 과정명</th>
                    <th scope="row" width="10%">신청자</th>                    
                    <th scope="row" width="10%">신청일</th>
                    <th scope="row" width="5%">연기상태</th>
                    <th scope="row" width="10%">관리</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="12">신청내역이 없습니다.</td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    $disable = '';
                    $newcourse = $DB->get_record_sql("select co.fullname from {lmsdata_schedule} ls "
                            . "join {lmsdata_class} lc on lc.scheduleid = ls.id "
                            . "join {course} co on co.id = lc.courseid "
                            . "where ls.id = $course->lsid and lc.parentcourseid = $course->parentcourseid ");
                    if (!$newcourse || $course->status == 1)
                        $disable = "disabled";
                    ?>
                    <tr>
                    <input type="hidden" name="status" value="<?php echo $course->status ?>">
                    <!--<td><input type="checkbox" <?php echo $disable ?> class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>-->
                    <td><?php echo $startnum--; ?></td>                    
                    <td><?php echo $course->classyear . '년' ?></td>
                    <td><?php echo $course->classnum ? $course->classnum . '기' : '-' ?></td>
                    <td><a href="javascript:delay_detail('<?php echo $course->id; ?>')" ><?php echo $course->fullname ?></a></td>
                    <td><?php echo $course->lsclassyear . '년' ?></td>
                    <td><?php echo $course->lsclassnum ? $course->lsclassnum . '기' : '-' ?></td>
                    <td><a href="javascript:delay_detail('<?php echo $course->id; ?>')" ><?php echo $newcourse->fullname ? $newcourse->fullname : '해당 기수에 과정이 생성되지 않았습니다.' ?></a></td>
                    <td><?php echo $course->lastname ?></td>                    
                    <td><?php echo date('Y-m-d', $course->timecreated) ?></td>
                    <td><?php echo $arr[$course->status] ?></td>
                    <td>
                        <?php if ($course->status == 0) { ?>
                            <input type="button" class="normal_btn" value="승인" onclick="delay_apply('<?php echo $course->id; ?>', 1)"/>
                            <input type="button" class="normal_btn" value="취소" onclick="delay_apply('<?php echo $course->id; ?>', 3)"/>
                        <?php } ?>
                        <?php if ($course->status == 1) { ?>
                            <input type="button" class="normal_btn" value="이월" onclick="delay_change('<?php echo $course->id; ?>')"/>
                        <?php }
                        ?>      
                    </td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <div id="btn_area">            
            <div style="float:right;">

            </div>
        </div>
        <?php
        print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->

<?php include_once ('../inc/footer.php'); ?>

<script type="text/javascript">
    /**
     * 승인, 이월, 취소 함수
     * @param {type} delayid
     * @param {type} type
     * @returns {undefined}
     */
    function delay_apply(delayid, type) {
        if (type == 1) {
            var text = '승인하시겠습니까?';
        } else if (type == 2) {
            var text = '이월하시겠습니까?';
        } else {
            var text = '취소하시겠습니까?';
        }

        if (confirm(text) == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/chamktu/enrol/edu_delay_change.ajax.php" ?>',
                method: 'POST',
                data: {
                    delayid: delayid,
                    type: type
                },
                success: function (data) {
                    if (data.status == 'success') {
                        location.reload();
                    }
                }
            });
        }
    }
    /**
     * 이월 처리 ajax
     * @param {type} id
     * @returns {undefined}     
     */
    function delay_change(id) {
        var tag = $("<div id='cashbill'></div>");
        var title = '수강이월';

        $.ajax({
            url: 'edu_delay_change.php',
            method: 'POST',
            data: {
                id: id,
                page: '<?php echo $currpage; ?>'
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: title,
                    modal: true,
                    width: 800,
                    maxHeight: getWindowSize().height - 20,
                    buttons: [
                        {id: 'save',
                            text: '이월',
                            click: function () {
                                if (confirm("이월하시겠습니까?") == true) {
                                    if ($('#frm_delay select[name=classid]').val() == '0') {
                                        alert('기수를 선택해주세요');
                                        return false;
                                    }
                                    $('#frm_delay').submit();
                                    $(this).dialog("close");
                                }
                            }},
                        {id: 'close',
                            text: '닫기',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    open: function () {
                        var t = $(this).parent();
                        var w = $(window);
                        var s = getWindowSize();

                        var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                        if (x < 0)
                            x = 0;
                        var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                        if (y < 0)
                            y = 0;
                        t.offset({
                            top: y,
                            left: x
                        });
                    },
                    close: function () {
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }
    /**
    * 연기 신청 상세보기 popup

     * @param {type} id
     * @returns {undefined}     */
    function delay_detail(id) {
        var tag = $("<div id='delay_detail'></div>");

        $.ajax({
            url: 'edu_delay_detail.php',
            method: 'POST',
            data: {
                id: id
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: '연기신청 상세보기',
                    modal: true,
                    width: 800,
                    maxHeight: getWindowSize().height - 20,
                    buttons: [
                        {id: 'close',
                            text: '닫기',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    open: function () {
                        var t = $(this).parent();
                        var w = $(window);
                        var s = getWindowSize();

                        var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                        if (x < 0)
                            x = 0;
                        var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                        if (y < 0)
                            y = 0;
                        t.offset({
                            top: y,
                            left: x
                        });
                    },
                    close: function () {
                        $('#frm_course_select').remove();
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }

</script>    
