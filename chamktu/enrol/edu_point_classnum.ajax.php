<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once $CFG->dirroot.'/local/jeipoint/lib.php';

$parentcourseid = required_param('parentcourseid', PARAM_INT);
$year = required_param('year', PARAM_INT);

$returnvalue = new stdClass();

$classnum_sql = 'SELECT 
                    classnum AS id, 
                    classnum AS value 
               FROM {lmsdata_class} 
               WHERE parentcourseid = :parentcourseid AND classyear = :classyear ORDER BY classnum ';

$classnum = $DB->get_records_sql($classnum_sql, array('parentcourseid' => $parentcourseid, 'classyear' => $year));

if($classnum) {
    $returnvalue->status = 'success';
    $returnvalue->classnum = $classnum;
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);