//과정유형 변경 시 이벤트
function point_pointtype_changed(sel) {
    var pointCourseName = $('#pointcoursename');
    point_clean_select(pointCourseName);

    if ($(sel).val() == 0) {
        return;
    }
    
    var courses = point_get_courses($(sel).val());
    if (courses !== null) {
        point_add_select_options(pointCourseName, courses);
    }
}

// 과정유형 변경 시 값을 가져옴
function point_get_courses(pid) {
    var courses = null;
    $.ajax({
        url: '/chamktu/enrol/edu_point_courses.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                courses = data.courses;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return courses;
}

//과정명 변경 시 이벤트
function point_pointcourse_changed(sel) {
    var pointClassYear = $('#pointclassyear');
    point_clean_select(pointClassYear);

    if ($(sel).val() == 0) {
        return;
    }
    
    var years = point_get_years($(sel).val());
    if (years !== null) {
        point_add_select_options(pointClassYear, years);
    }
}

// 과정명 변경 시 값을 가져옴
function point_get_years(pid) {
    var years = null;
    $.ajax({
        url: '/chamktu/enrol/edu_point_year.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                years = data.years;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return years;
}

//년도 변경 시 이벤트
function point_pointyear_changed(sel) {
    var pointClassNum = $('#pointclassnum');
    point_clean_select(pointClassNum);

    if ($(sel).val() == 0) {
        return;
    }
    
    var classnum = point_get_classnum($(sel).val());
    if (classnum !== null) {
        point_add_select_options(pointClassNum, classnum);
    }
}

// 년도 변경 시 값을 가져옴
function point_get_classnum(pid) {
    var years = null;
    $.ajax({
        url: '/chamktu/enrol/edu_point_classnum.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            parentcourseid : $('#pointcoursename').val(),
            year: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                classnum = data.classnum;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return classnum;
}

//select box 변경시 옵션 리스트 출력
function point_add_select_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.value
        }));
    });
}
//select box 초기화
function point_clean_select(sel) {
    $(sel[0].options).each(function () {
        if ($(this).val() != 0) {
            $(this).remove();
        }
        ;
    });
}

function popup_point(sel) {
    var tag = $("<div id='course_list'></div>");
    var userid = $(sel).data('userid');
    var courseid = $(sel).data('courseid');
    $.ajax({
      url:"edu_point_history.php?userid="+userid+"&courseid="+courseid,
      method: 'POST',
      success: function(data) {
        tag.html(data).dialog({
            title: '교육 이력 관리',
            modal: true,
            width: 900,
            resizable: false,
            height: 700,
            close: function () {
                $('#course_list').remove();
                $( this ).dialog('destroy').remove()
            }
        }).dialog('open');
      }
    });
}
function cata_page(page) {
    $('[name=page]').val(page);
    $('#course_search').submit();
}

function getWindowSize() {
    var myWidth = 0;
    var myHeight = 0;

    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if (document.documentElement &&
            (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    return {"width": myWidth, "height": myHeight}
}
