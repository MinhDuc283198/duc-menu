<?php
/**
 * 단체신청목록 리스트
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$classtype = optional_param('classtype', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$paymentmethod = optional_param('paymentmethod', 0, PARAM_INT);

//검색용 파라미터

if ($classyear) {
    $sql_where[] = " ls.classyear = :classyear ";
    $params['classyear'] = $classyear;
}
if ($classnum) {
    $sql_where[] = " ls.classnum = :classnum ";
    $params['classnum'] = $classnum;
}
if ($paymentmethod) {
    $sql_where[] = " lg.paymentmethod = :paymentmethod ";
    $params['paymentmethod'] = $paymentmethod;
}
if (!empty($searchtext)) {
    $like = array();
    $like[] = $DB->sql_like('lg.repusername', ':repusername');
    $like[] = $DB->sql_like('lg.groupcode', ':groupcode');
    $params['repusername'] = '%' . $searchtext . '%';
    $params['groupcode'] = '%' . $searchtext . '%';
    $sql_where[] = ' ' . implode(' OR ', $like);
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lg.*, ls.classyear, ls.classnum"
        . ", (select count(id) from {lmsdata_groupapply} where groupid = lg.id ) applycount ";
$sql_from = " FROM {lmsdata_grouplist} lg join {lmsdata_schedule} ls on ls.id=lg.scheduleid ";

$sql_orderby = " order by lg.id desc";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => get_string('individual_payment', 'local_group_application'), 2 => get_string('bill_payment', 'local_group_application'), 3 => get_string('administrative_room_settlement', 'local_group_application'));
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title">단체신청목록</h3>
        <form name="course_search" id="course_search" class="search_area" action="edu_group_list.php" method="get">
            <input type="hidden" name="page" value="1" />
            <div style="float:left;">
                <select title="year" name="classyear" id="classyear" class="w_160" onchange="year_changed(this, 'lmsdata_class',1);">
                    <option value="0">년도</option>
                    <?php
                    $yearRange = 1;
                    $currentYear = date('Y');
                    $maxYear = ($currentYear + $yearRange);

                    foreach (range(2017, $maxYear) as $year) {
                        $selected = '';
                        if ($year == $classyear) {
                            $selected = 'selected';
                        }
                        echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                    }
                    ?>
                </select> 
                <select title="class" name="classnum" id="classnum" class="w_160">
                    <option value="0">기수</option>
                    <?php
                    if ($classyear) {
                        if ($classyear)
                            $classnum_where .= " where classyear = $classyear";
                        $datas = $DB->get_records_sql($sql_select . $sql_from . $classnum_where);

                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->classnum == $classnum) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                        }
                    }
                    ?>
                </select>
                <select title="category01" name="paymentmethod" id="course_search_cata1" onchange="cata1_changed(this);"  class="w_160">
                    <option value="0">결제방법</option>
                    <?php
                    foreach ($arr as $key => $val) {
                        $selected = '';
                        if ($key == $paymentmethod) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $key . '"' . $selected . '> ' . $val . '</option>';
                    }
                    ?>
                </select>   
            </div>
            <br><br><br>

            <div style="float:left;">
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="코드번호 또는 대표자명 입력하세요."  class="search-text"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
            </div>
        </form><!--Search Area2 End-->
        <div style="float:right;">                            
            <input type="submit" class="blue_btn" style="" value="단체변경신청 목록" onclick="javascript:location.href='edu_group_list_change.php'"/> 
        </div>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                    <th scope="row" width="5%">년도</th>
                    <th scope="row" width="5%">기수</th>
                    <th scope="row" width="10%">결제방법</th>
                    <th scope="row" width="15%">코드번호</th>
                    <th scope="row" width="10%">대표자명</th>                                        
                    <th scope="row" width="10%">예정인원수</th>
                    <th scope="row" width="10%">신청인원수</th>
                    <th scope="row" width="10%">발급일</th>
                    <th scope="row" width="5%">상태</th>
                </tr>
            </thead>
<?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="15"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    $course->status == 1 ? $style = 'text-decoration:line-through' : $style = '';
                    ?>
                    <tr>
                        <td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>
                        <td><?php echo $startnum--; ?></td>                    
                        <td><?php echo $course->classyear . '년' ?></td>
                        <td><?php echo $course->classnum ? $course->classnum . '기' : '-' ?></td>
                        <td><?php echo $course->paymentmethod ? $arr[$course->paymentmethod] : '-'; ?></td>
                        <td style="<?php echo $style; ?>"><a href="<?php echo $CFG->wwwroot . '/chamktu/enrol/edu_group_list.detail.php?id=' . $course->id; ?>"><?php echo $course->groupcode ?></a></td>
                        <td><?php echo $course->repusername; ?></td>
                        <td><?php echo $course->usercount . '명' ?></td>
                        <td><?php echo $course->applycount . '명' ?></td>
                        <td><?php echo date('Y-m-d', $course->timecreated) ?></td>
                        <td><?php echo $course->status == 1 ? '취소' : ($course->status == 2 ? '완료':'신청') ?></td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
<input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="취소" onclick="group_delete()"/>
        <?php
        print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        ?>            
    </div><!--Content End-->
    
</div> <!--Contents End-->

<?php include_once ('../inc/footer.php'); ?>

<script>
    /**
     * 단체신청을 취소하는 기능
     * @returns {Boolean}
     */
    function group_delete() {
        var course_list = [];
        if (!$(".courseid").is(":checked")) {
            alert('기수를 선택하세요.');
            return false;
        }
        $("input[name=courseid]:checked").each(function () {
            course_list.push($(this).val());
        });

        if (confirm("단체등록을 취소하시겠습니까? 해당코드의 수강신청이 취소됩니다.") == true) {
            $.ajax({
                url: 'edu_group_list_cancle.php',
                method: 'POST',
                data: {
                    ids: course_list,
                },
                success: function (data) {
                    location.reload();
                }
            });
        }
    }
</script>
