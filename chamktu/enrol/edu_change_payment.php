<?php
/**
 * 결제정보 변경 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
$id = required_param('id', PARAM_INT);
$data = $DB->get_record('lmsdata_course_applications', array('id'=>$id));
?>

<div class="popup_content" id="course_teacher">
    <form id="change_payment" name="change_payment" onsubmit="return false;">
        <table cellpadding="0" cellspacing="0" class="detail">                
                <tbody>
                    <tr style="height: 45px">
                        <td class="field_title">결제방법</td>
                        <td class="field_value" >
                            <select name="paymentmeans">
                                <option <?php if($data->paymentmeans=="") echo 'selected'; ?> value="">없음</option>
                                <option <?php if($data->paymentmeans=="card") echo 'selected'; ?> value="card">카드결제</option>
                                <option <?php if($data->paymentmeans=="bank") echo 'selected'; ?> value="bank">실시간계좌이체</option>
                                <option <?php if($data->paymentmeans=="vbank") echo 'selected'; ?> value="vbank">가상계좌결제</option>
                                <option <?php if($data->paymentmeans=="unbank") echo 'selected'; ?> value="unbank">무통장입금</option>
                            </select>
                        </td>
                        <td class="field_title">결제금액</td>
                        <td class="field_value">
                            <input type="text" name ="price" value="<?php echo $data->price; ?>" >원
                        </td>
                    </tr>
                    <tr style="height: 45px">
                        <td class="field_title">할인정보</td>
                        <td class="field_value" >
                            <select name="discounttype">
                                <option <?php if($data->discounttype=="") echo 'selected'; ?> value="">없음</option>
                                <option <?php if($data->discounttype=="group") echo 'selected'; ?> value="group">그룹할인</option>
                                <option <?php if($data->discounttype=="member") echo 'selected'; ?> value="member">영재원할인</option>
                            </select>
                            <span class="text_field"><?php echo $arr1[$data->discounttype]; ?></span>
                        </td>
                        <td class="field_title">결제번호(거래번호)</td>
                        <td class="field_value">
                            <input type="text" name ="paymentid"  value="<?php echo $data->paymentid; ?>"  >
                        </td>
                    </tr>
                    <tr style="height: 45px">
                            <td class="field_title">카드사</td>
                            <td class="field_value">
                                <select name="cardcode">
                                    <option value="">없음</option>                                    
                                <?php 
                                $cards= $DB->get_records_sql("select code, name from {inicode} where type = 'card' ");
                                
                                foreach ($cards as $card){
                                    $selected = '';
                                    if($data->cardcode == $card->code) $selected = 'selected';
                                ?>
                                    <option <?php echo $selected ?> value="<?php echo $card->code ?>"><?php echo $card->name ?></option>
                                <?php } ?>
                                </select>
                            </td>
                            <td class="field_title">카드번호</td>
                            <td class="field_value">
                                <input type="text" name ="cardnumber"  value="<?php echo $data->cardnumber; ?>" >
                            </td>
                    </tr>
                    <tr style="height: 45px">
                            <td class="field_title">입금은행</td>
                            <td class="field_value">
                                <select name="bankcode">
                                    <option value="">없음</option>                                    
                                <?php 
                                $banks = $DB->get_records_sql("select code, name from {inicode} where type = 'bank' ");
                                
                                foreach ($banks as $bank){
                                    $selected = '';
                                    if($data->bankcode == $bank->code) $selected = 'selected';
                                ?>
                                    <option <?php echo $selected ?> value="<?php echo $bank->code ?>"><?php echo $bank->name ?></option>
                                <?php } ?>
                                </select>
                            </td>
                            <td class="field_title">계좌번호</td>
                            <td class="field_value">
                                <input type="text" name ="account" value="<?php echo $data->account; ?>">
                            </td>
                    </tr>
                    <tr style="height: 45px">
                    <td class="field_title">결제일시</td>
                    <td class="field_value" colspan="3">
                        <input type="text" title="시간" <?php echo $date_disabled; ?> name="timecreated" id="timecreated" class="w_120" value="<?php echo $data->timecreated ? date('Y-m-d', $data->timecreated) : '' ?>" placeholder="클릭하세요"/>                            
                    </td>
                    </tr> 
                </tbody>
            </table>
    </form>
</div>
<script>
    $(document).ready(function () {
        $("#timecreated").datepicker({
            dateFormat: "yy-mm-dd",
        });
        $("#timemodified").datepicker({
            dateFormat: "yy-mm-dd",
        });
    });
</script>   