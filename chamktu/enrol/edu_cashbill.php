<?php
/**
 * 현금영수증 발급 팝업 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$id = optional_param('id', 0, PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);

$sql = 'SELECT lca.*, lco.coursename, lc.classnum, lc.classyear FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc ON lc.id = lca.courseid
        JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
        WHERE lca.id = :id';

$application = $DB->get_record_sql($sql, array('id'=>$id));
$userinfo = $DB->get_record('user', array('id'=>$application->userid));
?>

<div class="popup_content">
    <h2>과정선택</h2>
    <form id="frm_cashbill" name="frm_cashbill" method="post" action="edu_cashbill.submit.php">
        <table cellpadding="0" cellspacing="0">
            <input type="hidden" name="page" value="<?php echo $page;?>" />
            <input type="hidden" name="customerName" value="<?php echo $userinfo->lastname;?>" />
            <input type="hidden" name="email" value="<?php echo $application->email;?>" />
            <input type="hidden" name="hp" value="<?php echo $application->phone;?>" />
            <input type="hidden" name="orderNumber" value="<?php echo $application->ordernumber;?>" />
            <input type="hidden" name="itemName" value="<?php echo $application->coursename.' '.$application->classyear.'년 '.$application->classnum.'기';?>" />
            <input type="hidden" name="userid" value="<?php echo $userinfo->id;?>" />
            <input type="hidden" name="application" value="<?php echo $application->id;?>" />
            <input type="hidden" name="totalAmount" value="<?php echo $application->price;?>" />
            <input type="hidden" name="taxationType" value="비과세" />
            <input type="hidden" name="supplyCost" value="<?php echo $application->price;?>">
            <input type="hidden" name="tax" value="0">
            <input type="hidden" name="serviceFee" value="0">
            <tbody>
                <tr>
                    <td>상품명</td>
                    <td class="left"><?php echo $application->coursename.' '.$application->classyear.'년 '.$application->classnum.'기';?></td>
                </tr>
                <tr>
                    <td>현금영수증 타입</td>
                    <td class="left">
                        <input type="radio" name="tradeUsage" checked value="소득공제용"> 소득공제용
                        <input type="radio" name="tradeUsage" value="지출증빙용"> 지출증빙용
                    </td>
                </tr>
                <tr>
                    <td>식별번호</td>
                    <td class="left"><input type="text" name="identityNum" value="<?php echo $application->phone;?>"> 예 : (전화번호, 주민등록번호, 사업자번호)(-없이)</td>
                </tr>
                <tr>
                    <td>금액</td>
                    <td class="left"><?php echo number_format($application->price).'원';?></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $('#frm_cashbill').submit(function(){
        var sum = parseInt($('input[name=supplyCost]').val()) + parseInt($('input[name=tax]').val()) + parseInt($('input[name=serviceFee]').val());
        if(sum != <?php echo $application->price;?>){
            alert('공급가액, 세액, 봉사료의 합이 금액과 일치하지 않습니다.')
            return false;
        }
    });
</script>