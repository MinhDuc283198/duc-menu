<?php
/**
 * 학교검색 팝업 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
$context = context_system::instance();

$search = optional_param("search_val", '', PARAM_RAW); // 검색내용
$page = optional_param("page", 1, PARAM_INT); // 페이지
$perpage = 10;
$offset = 0;

if ($page != 0) {
    $offset = ($page-1) * $perpage;
}

$db_host = '222.234.1.238';
$db_id = 'eduhope';
$db_pw = 'eduhope';
$db_name = 'eduhope';
$conn = mysqli_connect($db_host, $db_id, $db_pw, $db_name);

$sql = 'select * from tb_school';
$count_sql = 'select count(*) from tb_school';

$where = " where USE_YN = 'Y' ";

$orderby = ' order by SCHOOL_NM asc';

$limit = " limit $offset, $perpage";

if(!empty($search)){
    $where .= " AND SCHOOL_NM like '%$search%'";
}

$query = mysqli_query($conn, $sql.$where);
$count = mysqli_num_rows($query);

$query = mysqli_query($conn, $sql.$where.$orderby.$limit);
//echo $count;

?>  
    
    
    <div class="popup_content" id="course_teacher">
        <h2>학교검색</h2>
        <form id="frm_course_teacher" name="frm_course_teacher" onsubmit="return false;">
            <input type="text" class="search" name="search_val" value="<?php echo $search;?>">
            <input type="button" class="btn gray h30" name="search_btn" value="검색"  onclick="teacher_search_name();">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <th>학교명</th>
                    <th>학교코드</th>
                    <th>학교주소</th>
                    <th>선택</th>
                </thead>
                <?php
                if(!empty($search)){
                ?>
                <tbody>
                    <?php
                    while($result = mysqli_fetch_assoc($query)){
                    ?>
                    <tr>
                        <td><?php echo (!empty($result['SCHOOL_NM']))?$result['SCHOOL_NM']:'-';?></td>
                        <td><?php echo (!empty($result['SCHOOL_CODE']))?$result['SCHOOL_CODE']:'-';?></td>
                        <td class="text-left"><?php echo (!empty($result['ADDRESS']))?$result['ADDRESS']:'-';?></td>
                        <td><input type="button" value="선택" onclick="set_school('<?php echo $result['SCHOOL_NM'];?>', '<?php echo $result['SCHOOL_CODE'];?>')"></td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <?php
                } else {
                    echo '<tr><td colspan="4">검색어를 입력해주세요</td></tr>';
                }
                ?>
            </table>
        </form>
        <?php
    print_paging_navbar_script($count_users, $page, $perpage, 'javascript:class_students_search(:page);', 10);
    ?>
    </div>
<script>
    /**
     * 학교 선택 클릭
     * @param {type} name
     * @param {type} code
     * @returns {undefined}
     */
    function set_school(name, code){
        $('input[name=school]').val(name);
        $('#select_user').dialog( "close" );
    }
    /**
     * 페이지 이동 함수
     * @param {type} page
     * @returns {undefined}
     */
    function class_students_search(page) {

        $.ajax({
            url: 'schoolsearch.php',
            method: 'POST',
            data: {
                'page': page,
                'search_val': $('#frm_course_teacher').find('input[name=search_val]').val(),
            },
            success: function (data) {
                $("#course_teacher").parent().html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
    /**
     * 검색 기능
     * @returns {undefined}
     */
    function teacher_search_name() {
        $.ajax({
            url: 'schoolsearch.php',
            method: 'POST',
            data: {
                'page': <?php echo $page; ?>,
                'search_val': $('#frm_course_teacher').find('input[name=search_val]').val()
            },
            success: function (data) {
                $("#course_teacher").parent().html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
    $("input[name=search_val]").keypress(function (e) {
        if (e.keyCode == 13) {
            teacher_search_name();
        }
    });
    
</script>