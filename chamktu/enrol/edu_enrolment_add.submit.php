<?php

/**
 * 수강신청목록 데이터 저장
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');

$lmsdata = new stdClass();

foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}
foreach ($neis as $neiss) {
    $lmsdata->neiscode .= $neiss;
}
foreach ($no as $nos) {
    $lmsdata->locationnumber .= $nos;
}

$lmsdata->timemodified = strtotime($lmsdata->timecreated);
$lmsdata->timecreated = time();

$courseconfig = get_config('moodlecourse');

$lmsdata->price = '0';
$lmsdata->paymentstatus = 'complete';

$resultmessage = '';
if (!$id) {
    $alreadyenrol = $DB->get_record('lmsdata_course_applications', array('courseid' => $lmsdata->courseid, 'userid' => $lmsdata->userid));

    if ($alreadyenrol && $alreadyenrol->status == 'apply') {
        if ($alreadyenrol->status == 'apply') {
            echo '<script type="text/javascript">alert("이미 수강신청이 된 차수입니다."); location.href="' . $CFG->wwwroot . '/chamktu/enrol/edu_enrolment_list.php";</script>';
        } else if ($alreadyenrol->status == 'cancel') {
            $applicationid = $DB->update_record('lmsdata_course_applications', $lmsdata);
            $application = $DB->get_record('lmsdata_course_applications', array('id' => $applicationid));
            chamktu_applications_set_training_history($application);
            //    $lmsdata_class = $DB->get_record('lmsdata_class',array('id'=>$lmsdata->courseid));
            $lmsdata_class = $DB->get_record('lmsdata_class', array('courseid' => $lmsdata->courseid));
            $data = $DB->get_record('course', array('id' => $lmsdata_class->courseid));
            $user = $DB->get_record('lmsdata_user', array('userid' => $lmsdata->userid));

            $user->roleid = 5;
            set_assign_user($data->id, $user);
            echo '<script type="text/javascript">alert("수강신청이 완료되었습니다."); location.href="' . $CFG->wwwroot . '/chamktu/enrol/edu_enrolment_list.php";</script>';
        }
    } else {
        $applicationid = $DB->insert_record('lmsdata_course_applications', $lmsdata);
        $application = $DB->get_record('lmsdata_course_applications', array('id' => $applicationid));
        chamktu_applications_set_training_history($application);
        //    $lmsdata_class = $DB->get_record('lmsdata_class',array('id'=>$lmsdata->courseid));
        $lmsdata_class = $DB->get_record('lmsdata_class', array('courseid' => $lmsdata->courseid));
        $data = $DB->get_record('course', array('id' => $lmsdata_class->courseid));
        $user = $DB->get_record('lmsdata_user', array('userid' => $lmsdata->userid));

        $user->roleid = 5;
        set_assign_user($data->id, $user);
        echo '<script type="text/javascript">alert("수강신청이 완료되었습니다."); location.href="' . $CFG->wwwroot . '/chamktu/enrol/edu_enrolment_list.php";</script>';
    }
}
