<?php
require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/report/statistics/lib.php';
require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->libdir/excellib.class.php");


$id = required_param('id', PARAM_INT); // course id
$search = optional_param('search', '', PARAM_CLEAN);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 15, PARAM_INT);
$type = optional_param('type', 'all', PARAM_CLEAN);
$excell = optional_param('excell', 0, PARAM_INT);
$offset = ($page - 1) * $perpage;

require_login();


$context = context_course::instance($id);
$PAGE->set_context($context);


$course = get_course($id);
$sql = "select u.* from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "where ra.contextid = :contextid ";
$where = '';
$param = array('contextid' => $context->id, 'rolename' => 'student', 'contextlevel' => CONTEXT_COURSE);
if ($search) {
    $where = ' and  ((u.firstname like :searchtxt1 or u.lastname like :searchtxt2 or u. firstname||lastname like :searchtxt3) or u.username like :searchtxt4 )';
    $param['searchtxt1'] = $param['searchtxt2'] = $param['searchtxt3'] = $param['searchtxt4'] = '%' . $search . '%';
}
if (!$excell) {
    $users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param, $offset, $perpage);
} else {
    $users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param);
}
$countsql = "select count(u.id) from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
        . "where ra.contextid = :contextid ";
$usercount = $DB->count_records_sql($countsql . $where . ' order by u.username asc', $param);
$headsql = "select cm.id, cm.instance, mo.name, ctx.id as contextid from {course_modules} cm "
        . "join {modules} mo on mo.id = cm.module and (mo.name = 'lcms' or mo.name = 'okmedia') and mo.visible = 1 "
        . "join {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
        . "where cm.course = :course and cm.section = :section ";
$sql_select = "SELECT mc.id, mc.fullname, mc.shortname , mc.format as courseformat 
     , mu.firstname, mu.lastname, mu.USERNAME 
     , ca.name as category_name 
 ,(SELECT COUNT(*) FROM {role_assignments} ra JOIN {role} ro ON ra.roleid = ro.id JOIN {context} ctx ON ra.contextid = ctx.id JOIN {course} co ON ctx.instanceid = co.id AND contextlevel = :contextlevel1 WHERE co.id = mc.id AND ro.id = 5) as student 
 ,(SELECT COUNT(*) FROM {role_assignments} ra JOIN {role} ro ON ra.roleid = ro.id JOIN {context} ctx ON ra.contextid = ctx.id JOIN {course} co ON ctx.instanceid = co.id AND contextlevel = :contextlevel2 WHERE co.id = mc.id AND ro.id = 41) as auditor 
 ,(SELECT COUNT(cm.id) from {course_modules} cm join {modules} mo on mo.id = cm.module and mo.name = 'okmedia' WHERE cm.course = mc.id) as lcmscount 
 ,(SELECT COUNT(cm.id) from {course_modules} cm  join {modules} mo on mo.id = cm.module and mo.name != 'assign' WHERE cm.course = mc.id) as modcount 
        FROM {course} mc 
        JOIN {user} mu
        LEFT JOIN {lmsdata_class} lcs ON lcs.courseid = mc.id 
        LEFT JOIN {course_categories} ca ON ca.id = mc.category 
        LEFT JOIN {lmsdata_user} lc ON lc.userid = mu.id WHERE mc.id = :courseid";
$param2 = array('courseid' => $id, 'contextlevel1' => CONTEXT_COURSE, 'contextlevel2' => CONTEXT_COURSE);
$course_data = $DB->get_record_sql($sql_select, $param2);

$section_cnt = $DB->count_records_sql('select COUNT(section) FROM m_course_sections where course = :id', array('id' => $id));
$sections_sql = "select DISTINCT cs.id,cs.name,cs.section , 
	(select count(*) from {course_modules} cm 
	JOIN {modules} mo ON mo.id = cm.module AND (mo.name = 'lcms' or mo.name = 'okmedia')  
	where cm.section = cs.id ) AS colspan 
	from {course_sections} cs
	where cs.visible = 1 and cs.course = :id
	order by cs.section asc";
//$course_sections = $DB->get_records_sql($sections_sql, array('id' => $id));
$course_sections = $DB->get_records_sql($sections_sql, array('id' => $id), 1, $section_cnt);


$course_info_select = "SELECT mc.id, mc.fullname, mc.shortname
     , yc.classyear,  yc.learningstart, yc.learningend
     , ca.name as category_name
     , mu.firstname, mu.lastname
     , mu.username
     ,(SELECT COUNT(*) 
        FROM {role_assignments} ra
        JOIN {role} ro ON ra.roleid = ro.id
        JOIN {context} ctx ON ra.contextid = ctx.id
        JOIN {course} co ON ctx.instanceid = co.id AND contextlevel = :contextlevel2
        WHERE co.id = mc.id
        AND ro.id = 5) as student ";

$course_info_from = " FROM {course} mc 
JOIN {lmsdata_class} yc ON yc.courseid = mc.id 
JOIN {course_categories} ca ON ca.id = mc.category 
LEFT JOIN {lmsdata_user} lu ON lu.userno = 0
LEFT JOIN {user} mu ON lu.userid = mu.id";

$course_info_params = array(
    'coursetype' => $coursetype,
    'contextlevel1' => CONTEXT_COURSE,
    'contextlevel2' => CONTEXT_COURSE,
    'id' => $id
);
$course_info_where = ' where mc.id = :id ';
$courses_info = $DB->get_records_sql($course_info_select . $course_info_from . $course_info_where, $course_info_params);

if (!$excell) {
    $js = array(
        $CFG->wwwroot . '/chamktu/manage/course_list.js'
    );
    include_once ('../inc/header.php');
    ?>
    <?php
    $url = "edu_course_progress.php?id=$id&excell=1";
    ?>        
    <script type="text/javascript" src= "<?php echo $CFG->wwwroot ?>/theme/javascript.php?theme=oklasscampus&amp;rev=-1&amp;type=head"></script>
    <div id="contents">
        <?php include_once ('../inc/sidebar_enrol.php'); ?>
        <div id="content">
            <h3 class="page_title">진도현황</h3>
            <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <a href="./edu_course.php"><?php echo get_string('stats_allcourselist', 'local_lmsdata'); ?></a> > <strong>진도현황</strong></div>
            <?php foreach ($courses_info as $course) { ?>
                <table cellpadding="0" cellspacing="0" class="detail">
                    <tbody>
                        <tr>
                            <td class="field_title"><?php echo get_string('stats_years', 'local_lmsdata'); ?></td>
                            <td class="field_value" style="width: 30%;"><?php echo $course->classyear ?></td>
                            <td class="field_title"><?php echo get_string('stats_classification', 'local_lmsdata'); ?></td>
                            <td class="field_value" style="width: 30%;"><?php echo $course->category_name ?></td>
                        </tr>
                        <tr>
                            <td class="field_title"><?php echo get_string('stats_coursename', 'local_lmsdata'); ?></td>
                            <td class="field_value" style="width: 30%;"><?php echo $course->fullname ?></td>
                            <td class="field_title"><?php echo get_string('stats_learningperiod', 'local_lmsdata'); ?></td>
                            <td scope="col" class="field_value" style="width: 30%;"><?php echo $date1 = date("Y-m-d", $course->learningstart) . ' ~ ' . date("Y-m-d", $course->learningend); ?></td>
                        </tr>
        <!--                    <tr>
                            <td scope="row" class="field_title"><?php echo get_string('teacher', 'local_lmsdata'); ?></td>
                            <td scope="col" class="field_value" style="width: 30%;"><?php echo $course->firstname . $course->lastname; ?></td>
                            <td class="field_title"><?php echo get_string('stats_student', 'local_lmsdata'); ?></td>
                            <td class="field_value" style="width: 30%;"><?php echo $course->student . '명' ?></td>
                        </tr>-->
                    </tbody>
                </table>
            <?php } ?>
            <table class="generaltable">
                <caption class="hidden-caption"><?php echo get_string('lcms_edu', 'coursereport_statistics'); ?></caption>
                <thead>
                    <tr>
                        <th scope="row"><?php echo get_string('student', 'coursereport_statistics'); ?></th>
                        <!--<th scope="row"><?php echo get_string('auditor', 'coursereport_statistics'); ?></th>-->
                        <th scope="row"><?php echo get_string('sectioncnt', 'coursereport_statistics'); ?></th>
                        <th scope="row"><?php echo get_string('lcmscnt', 'coursereport_statistics'); ?></th>
                        <th scope="row"><?php echo get_string('modcnt', 'coursereport_statistics'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="col"><?php echo $course_data->student; ?></td>
                        <!--<td scope="col"><?php echo $course_data->auditor; ?></td>-->
                        <td scope="col"><?php echo $section_cnt - 1; ?></td>
                        <td scope="col"><?php echo $course_data->lcmscount; ?></td>
                        <td scope="col"><?php echo $course_data->modcount; ?></td>
                    </tr>
                </tbody>
            </table>       

            <form  id="course_search" class="table-search-option stat_form">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" title="page" name="page" value="1" />
                <div class="stat_search_area">
                    <input type="text" title="search" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('searchplaceholder', 'coursereport_statistics'); ?>">
                    <input type="submit" value="<?php echo get_string('search', 'local_jinoboard'); ?>" class="board-search"/>
                    <input type="button" onclick="location.href = '<?php echo $url; ?>'" class="red-form" value="<?php echo get_string('download'); ?>" />
                </div>
            </form>

            <?php
            echo '<div class="no-overflow">';
            echo '<table class="generaltable stat_table"><caption class="hidden-caption">' . get_string('studyrate', 'local_lmsdata') . '<thead>'
            . '<tr>';
            echo '<th scope="row" rowspan="2">' . get_string('no', 'coursereport_statistics') . '</th>';
            echo '<th scope="row" rowspan="2">' . get_string('user:name', 'coursereport_statistics') . '</th>'
            . '<th scope="row" rowspan="2">' . get_string('user:hakbun', 'coursereport_statistics') . '</th>';
            $rows1 = array();
            $rows3 = array();
            foreach ($course_sections as $course_section) {
                if ($course_section->section != '0') {
//print_object($title);
                    $title = get_section_name($id, $course_section);
                    echo '<th scope="row" colspan="' . $course_section->colspan . '">' . $title . '</th>';
                    $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
                    foreach ($heads as $cm) {

                        $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
                        $modulename = get_string('modulename', $cm->name);
                        $activityicon = $OUTPUT->pix_icon('icon', $modulename, $cm->name, array('class' => 'icon', 'title' => $mod->name, 'alt' => $mod->name));
                        $rows1[] = '<th scope="row">' . $activityicon . '</th>';
                    }
                    if (!$heads) {
                        $rows1[] = '<th scope="row">&nbsp;</th>';
                    }
                }
            }
            $num = $usercount - $offset;
            foreach ($users as $user) {
                $row3 = "<tr><td>" . $num-- . "</td><td>" . fullname($user) . " </td>";
                $row3 .= "<td>" . $user->username . "</td>";

                foreach ($course_sections as $course_section) {
                    if ($course_section->section != '0') {

                        $lcmssql =//                            "select lcms.* from "
//                            . "(select cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress from {course_modules} cm "
//                            . "JOIN {modules} mo on mo.id = cm.module and mo.name = 'lcms' and mo.visible = 1 "
//                            . "JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel1 "
//                            . "JOIN {lcms} lc on lc.id = cm.instance "
//                            . "LEFT JOIN {lcms_track} lt on lt.lcms = lc.id and lt.userid = :userid1 "
//                            . "where cm.course = :course1 and cm.section = :section1 "
//                            . "union "
                                "select cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress
                            from {course_modules} cm 
                            JOIN {modules} mo on mo.id = cm.module and mo.name = 'okmedia' and mo.visible = 1 
                            JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel2 
                            JOIN {okmedia} lc on lc.id = cm.instance 
                            LEFT JOIN {okmedia_track} lt on lt.okmediaid = lc.id and lt.userid = :userid2 
                            where cm.section = :section2 ";
                        $lcmsdata = $DB->get_records_sql($lcmssql, array('course2' => $id, 'userid2' => $user->id,
                            'contextlevel2' => CONTEXT_MODULE, 'section2' => $course_section->id));

                        foreach ($lcmsdata as $lcms) {

                            $row3 .= '<td>';

                            $progress = ($lcms->progress) ? $lcms->progress . "%" : "-";

                            $row3 .= $progress;
                            $row3 .= '</td>';
                        }
                        if (!$lcmsdata) {

                            $row3 .= '<td>-</td>';
                        }
                    }
                }
                $row3 .= "</tr>";
                $rows3[] = $row3;
            }
            echo '</tr><tr>' . implode('', $rows1) . '</tr><tr>' . implode('', $rows2) . "</tr></thead><tbody>";
            if (!$rows3) {
                ?>
                <tr>
                    <td colspan="15">학습자가 없습니다.</td>
                </tr>
            <?php
            } else {
                foreach ($rows3 as $row) {
                    echo $row;
                }
            }
            echo "</tbody></table>";
            echo "</div>";


            $params = array('id' => $id, 'search' => $search, 'type' => $type, 'perpage' => $perpage);
            $total_page = ceil($usercount / $perpage);
            if (($usercount / $perpage) > 1) {
                print_paging_navbar_script($usercount, $page, $perpage, 'javascript:cata_page(:page);');
            }
            ?>


        </div>
        <script type="text/javascript">
            function cata_page(page) {
                $('[name=page]').val(page);
                $('#course_search').submit();
            }
            function list_active(obj, cls) {
                if ($(obj).attr('active') == 'off') {
                    $('.' + cls).attr('active', 'on');
                    switch (cls) {
                        case 'view_span':
                            $('.view_span_col').attr('style', '');
                            $('.write_span_col').attr('style', 'display:none;');
                            $('.comment_span_col').attr('style', 'display:none;');

                            $('.write_span').attr('active', 'off');
                            $('.comment_span').attr('active', 'off');
                            break;
                        case 'write_span':
                            $('.view_span_col').attr('style', 'display:none;');
                            $('.write_span_col').attr('style', '');
                            $('.comment_span_col').attr('style', 'display:none;');

                            $('.view_span').attr('active', 'off');
                            $('.comment_span').attr('active', 'off');
                            break;
                        case 'comment_span':
                            $('.view_span_col').attr('style', 'display:none;');
                            $('.write_span_col').attr('style', 'display:none;');
                            $('.comment_span_col').attr('style', '');

                            $('.write_span').attr('active', 'off');
                            $('.view_span').attr('active', 'off');
                            break;
                    }
                } else {
                    $('.view_span').attr('active', 'off');
                    $('.write_span').attr('active', 'off');
                    $('.comment_span').attr('active', 'off');
                    $('.view_span_col').attr('style', '');
                    $('.write_span_col').attr('style', '');
                    $('.comment_span_col').attr('style', '');
                }
            }
        </script>
        <?php
        include_once ('../inc/footer.php');
    } else {
        $param = array('contextid' => $context->id, 'rolename' => 'student', 'contextlevel' => CONTEXT_COURSE);
        $countsql = "select count(u.id) from {role_assignments} ra "
                . "join {user} u on u.id = ra.userid "
                . "join {role} r on ra.roleid = r.id and r.shortname = :rolename "
                . "where ra.contextid = :contextid ";
        $usercount = $DB->count_records_sql($countsql . ' order by u.username asc', $param);

        $filename = '진도현황_' . $id . '.xls';

        $workbook = new MoodleExcelWorkbook('-');
        $workbook->send($filename);

        $worksheet = array();

        $worksheet[0] = $workbook->add_worksheet('');

        $worksheet[0]->write(0, 0, '전체 과정 운영현황');

        $worksheet[0]->write(1, 0, get_string('student', 'coursereport_statistics'));
//    $worksheet[0]->write(1, 1, get_string('auditor', 'coursereport_statistics'));
        $worksheet[0]->write(1, 1, get_string('sectioncnt', 'coursereport_statistics'));
        $worksheet[0]->write(1, 2, get_string('lcmscnt', 'coursereport_statistics'));
        $worksheet[0]->write(1, 3, get_string('modcnt', 'coursereport_statistics'));

        $worksheet[0]->write(2, 0, $usercount);
//    $worksheet[0]->write(2, 1, $course_data->auditor);
        $worksheet[0]->write(2, 1, $section_cnt - 1);
        $worksheet[0]->write(2, 2, $course_data->lcmscount);
        $worksheet[0]->write(2, 3, $course_data->modcount);

        $worksheet[0]->write(3, 0, '진도현황');

        $worksheet[0]->write(4, 0, get_string('name', 'coursereport_statistics'));
        $worksheet[0]->write(4, 1, get_string('usernumber', 'coursereport_statistics'));
        $col = 2;
        $col2 = 2;
        $rows1 = array();
        foreach ($course_sections as $course_section) {
            $title = get_section_name($id, $course_section);
            $worksheet[0]->write(4, $col, $title);
            if ($course_section->colspan) {
                $col += $course_section->colspan * 1;
            } else {
                $col += 1;
            }
            $heads = $DB->get_records_sql($headsql, array('course' => $id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id));
            foreach ($heads as $cm) {
                $mod = $DB->get_record($cm->name, array('id' => $cm->instance));
                $rows1[] = $mod->name;
            }
            if (!$heads) {
                $rows1[] = '-';
            }
        }
        foreach ($rows1 as $row => $val) {
            $worksheet[0]->write(5, $col2, $val);
            $col2 += 1;
        }


        $row = 6;
        foreach ($users as $user) {
            $col3 = 0;
            $worksheet[0]->write($row, $col3++, fullname($user));
            $worksheet[0]->write($row, $col3++, $user->username);
            foreach ($course_sections as $course_section) {
                $lcmssql =//                    "select lcms.* from "
//                    . "(select cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress from {course_modules} cm "
//                    . "JOIN {modules} mo on mo.id = cm.module and mo.name = 'lcms' and mo.visible = 1 "
//                    . "JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel "
//                    . "JOIN {lcms} lc on lc.id = cm.instance "
//                    . "JOIN {lcms_track} lt on lt.lcms = lc.id and lt.userid = :userid "
//                    . "where cm.course = :course and cm.section = :section "
//                    . "union "
                        "select cm.id, cm.instance, mo.name, ctx.id as contextid , lc.type , lt.playtime,lt.progress"
                        . " from {course_modules} cm "
                        . "JOIN {modules} mo on mo.id = cm.module and mo.name = 'okmedia' and mo.visible = 1 "
                        . "JOIN {context} ctx on ctx.instanceid = cm.id and ctx.contextlevel = :contextlevel2 "
                        . "JOIN {okmedia} lc on lc.id = cm.instance "
                        . "LEFT JOIN {okmedia_track} lt on lt.okmediaid = lc.id and lt.userid = :userid2 "
                        . "where cm.section = :section2 ";


                $lcmsdata = $DB->get_records_sql($lcmssql, array('course' => $id, 'userid' => $user->id, 'contextlevel' => CONTEXT_MODULE, 'section' => $course_section->id, 'course2' => $id, 'userid2' => $user->id, 'contextlevel2' => CONTEXT_MODULE, 'section2' => $course_section->id));

                foreach ($lcmsdata as $lcms) {
//                print_object($lcms);
//                die();
                    $mod = $DB->get_record($lcms->name, array('id' => $lcms->instance));
                    $progress = ($lcms->progress) ? $lcms->progress . "%" : "-";

                    $worksheet[0]->write($row, $col3++, $progress);
                }
                if (!$lcmsdata) {
                    $worksheet[0]->write($row, $col3++, "-");
//                $col3 += 1;
                }
            }
            $rows3[] = $row3;
            $row++;
        }




        $workbook->close();
        die;
    }
