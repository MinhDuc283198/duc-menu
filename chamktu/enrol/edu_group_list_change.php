<?php
/**
 * 단체변경신청 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);

$courses = $DB->get_records_sql("SELECT lgc.*, u.lastname FROM {lmsdata_group_change} lgc join {user} u on lgc.userid = u.id order by lgc.id desc, lgc.timecreated desc ", $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . "FROM {lmsdata_group_change} lgc join {user} u on lgc.userid = u.id  " . $sql_where, $params);

$arr = array(1 => '변경', 2 => '삭제');
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title">단체변경신청 목록</h3>

        <table>
            <thead>
                <tr>
                    <!--<th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>-->
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                    <th scope="row" width="5%">구분</th>                    
                    <th scope="row" width="20%">제목</th>
                    <th scope="row" width="50%">신청내용</th>
                    <th scope="row" width="10%">신청자</th>
                    <th scope="row" width="10%">신청일</th>
                </tr>
            </thead>
<?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10">신청내용이 없습니다.</td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                    <tr>
                        <!--<td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>-->
                        <td><?php echo $startnum--; ?></td>                    
                        <td><?php echo $arr[$course->status];?></td>                        
                        <td><?php echo $course->title ?></td>
                        <td><?php echo $course->contents; ?></td>
                        <td><?php echo $course->lastname ?></td>
                        <td><?php echo date('Y-m-d', $course->timecreated) ?></td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <div id="btn_area">            
            <div style="float:right;">
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="단체신청목록" onclick="javascript:location.href='edu_group_list.php';"/> 
            </div>
        </div>
        <?php
        print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->

<?php include_once ('../inc/footer.php'); ?>
