<?php
/**
 * 연수 연기신청목록 상세페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$id = optional_param('id', 0, PARAM_INT);

//검색용 파라미터

$sql_select = "SELECT lad.*, lc.parentcourseid, lc.classyear, lc.classnum, co.fullname, u.lastname, lca.id lcaid, ls.classyear lsclassyear, ls.classnum lsclassnum, ls.id lsid ";
$sql_from = " FROM {lmsdata_applications_delay} lad "
        . "join {lmsdata_course_applications} lca on lca.id=lad.applicationid "
        . "join {lmsdata_class} lc on lc.id=lca.courseid "
        . "join {course} co on co.id = lc.courseid "
        . "join {user} u on u.id = lca.userid "
        . "join {lmsdata_schedule} ls on lad.scheduleid = ls.id where lad.id = $id ";
$sql_orderby = " order by lad.id desc";

$courses = $DB->get_record_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$arr = array(0 => '신청', 1 => '승인', 2 => '이월완료');
?>

<div class="popup_content">
    <table cellpadding="0" cellspacing="0" class="detail">
        <tbody>
            <tr style="height: 45px">
                <td class="field_title">년도</td>
                <td class="field_value" >
                    <span class="text_field"><?php echo $courses->classyear . '년'; ?></span>
                </td>
                <td class="field_title">기수</td>
                <td class="field_value">
                    <span class="text_field"><?php echo $courses->classnum . '기'; ?></span>
                </td>
            </tr>                    
            <tr style="height: 45px">
                <td class="field_title">과정명</td>
                <td class="field_value">
                    <span class="text_field"><?php echo $courses->fullname; ?></span>
                </td>
                <td class="field_title">신청자</td>
                <td class="field_value" >
                    <span class="text_field"><?php echo $courses->lastname; ?></span>
                </td>
            </tr> 
            <tr style="height: 45px">
                <td class="field_title">사유</td>
                <td class="field_value" colspan="3">
                    <span class="text_field"><?php echo $courses->reason; ?></span>
                </td>
            </tr> 
        </tbody>
    </table>
</div> <!--Contents End-->

