<?php

/* 단체 승인기능 ajax
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/chamktu/lib.php');

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id  = required_param('id', PARAM_INT);
$courseid  = required_param('courseid', PARAM_INT);

$returnvalue = new stdClass();

$groupapply = $DB->get_record('lmsdata_groupapply',array('id'=>$id));
$grouplist = $DB->get_record('lmsdata_grouplist',array('id'=>$groupapply->groupid));
$course = $DB->get_record('course',array('id'=>$courseid));
$user = $DB->get_record('lmsdata_user',array('userid'=>$groupapply->userid));
$applications = $DB->get_record('lmsdata_course_applications',array('userid'=>$groupapply->userid, 'courseid' => $groupapply->classid, 'groupcode' => $grouplist->groupcode));
$user->roleid = 5;
if(set_assign_user($course, $user)){    
    $groupapply->status = 1;
    $applications->status = 'apply';
    $applications->paymentstatus = 'complete';
    $DB->update_record('lmsdata_groupapply', $groupapply);
    $DB->update_record('lmsdata_course_applications', $applications);
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);


