<?php

require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib.php';
include_once (dirname(dirname(__FILE__)) . '/inc/lib.php');
require_once("$CFG->libdir/excellib.class.php");
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/enrol/course_all.php');
    redirect(get_login_url());
}
$context = context_system::instance();

$year = optional_param('year', date('Y'), PARAM_INT);
$searchtext = optional_param('course_name', '', PARAM_RAW);

$sql_select = "SELECT 
                    mc.id mcid, mc.shortname,
                    yc.classyear, yc.classnum,
                    lco.coursename,
                    ca.name as category_name,
                    (SELECT COUNT(*) 
                        FROM {role_assignments} ra
                        JOIN {role} ro ON ra.roleid = ro.id
                        JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel
                        WHERE ctx.instanceid = mc.id AND ro.archetype = :archetype
                    ) AS student ";

$sql_from = ' FROM {course} mc 
              JOIN {lmsdata_class} yc ON yc.courseid = mc.id
              JOIN {lmsdata_course} lco ON lco.id = yc.parentcourseid
              JOIN {course_categories} ca ON ca.id = mc.category ';


$conditions = array();
$page_params = array();
$param = array(
    'contextlevel' => CONTEXT_COURSE,
    'archetype' => 'student'
);
if (!empty($searchtext)) {
    $conditions[] = $DB->sql_like('lco.coursename', ':course_name');
    $param['course_name'] = '%' . $searchtext . '%';
}


if ($year) {
    $conditions[] = " yc.classyear = :year ";
    $param['year'] = $year;
}

$sql_where = '';
if ($conditions) {
    $sql_where = ' WHERE ' . implode(' AND ', $conditions);
}
$orderby = "ORDER BY yc.learningstart DESC ";

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $param);

$fields = array(
    '년도',
    '기수',
    '과정명',
    '수강생',
    '이수자',
    '미이수자',
    '강의평가 평균(참여 수)'
);

$filename = get_string('stats_allcourselist', 'local_lmsdata') . '.xls';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);

$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');
$col = 0;
foreach ($fields as $fieldname) {
    $worksheet[0]->write(0, $col, $fieldname);
    $col++;
}
$row = 1;
$worksheet[0]->set_column(2, 2, 40, array('v_align' => 'top'));
foreach ($courses as $course) {
    $col = 0;

    $average_sql = 'SELECT SUM(lce.stars) total, COUNT(lce.userid) num, (SUM(lce.stars) / COUNT(lce.userid)) average, 
                            lce.courseid, co.fullname  
                            FROM {lmsdata_contents_evaluation} lce
                            JOIN {course} co ON lce.courseid = co.id
                            WHERE courseid = :courseid
                            GROUP BY courseid';
    $average = $DB->get_record_sql($average_sql, array('courseid' => $course->mcid));

    $complete_sql = 'SELECT count(ra.id) 
                            FROM {role_assignments} ra 
                            JOIN {role} ro ON ra.roleid = ro.id 
                            JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel2 
                            JOIN {course_completions} cc ON cc.course = :courseid1 AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                            WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype';
    $complete = $DB->count_records_sql($complete_sql, array('contextlevel2' => CONTEXT_COURSE, 'courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'archetype' => 'student'));



    $worksheet[0]->write($row, $col++, $course->classyear);
    $worksheet[0]->write($row, $col++, $course->classnum);
    $worksheet[0]->write($row, $col++, $course->coursename);
    $worksheet[0]->write($row, $col++, $course->student);

    $numcomplete = empty($complete) ? 0 : $complete;
    $worksheet[0]->write($row, $col++, $numcomplete);

    $notcomplete = ($course->student - $numcomplete);
    $worksheet[0]->write($row, $col++, $notcomplete);

    $evaluationresult = empty($average->average) ? '-' : number_format($average->average, 2, '.', '') . '(' . $average->num . ')';
    $worksheet[0]->write($row, $col++, $evaluationresult);
    $row++;
}
$workbook->close();
die;
