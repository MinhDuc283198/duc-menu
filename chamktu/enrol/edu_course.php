<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
include_once (dirname(dirname(__FILE__)) . '/inc/lib.php');
include_once (dirname(dirname(dirname(__FILE__))) . '/lib/completionlib.php');
if (!is_siteadmin($USER)) {
    redirect($CFG->wwwroot);
}


// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/enrol/edu_course.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$year = optional_param('year', date('Y'), PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);

$sql_select = "SELECT 
                    mc.id, mc.shortname,
                    yc.classyear, yc.classnum,
                    lco.coursename,
                    ca.name as category_name,
                    (SELECT COUNT(*) 
                        FROM {role_assignments} ra
                        JOIN {role} ro ON ra.roleid = ro.id
                        JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel
                        WHERE ctx.instanceid = mc.id AND ro.archetype = 'student'
                    ) AS student";

$sql_from = ' FROM {course} mc 
              JOIN {lmsdata_class} yc ON yc.courseid = mc.id
              JOIN {lmsdata_course} lco ON lco.id = yc.parentcourseid
              JOIN {course_categories} ca ON ca.id = mc.category ';

$conditions = array();
$page_params = array();
$param = array(
    'contextlevel' => CONTEXT_COURSE
);
if (!empty($searchtext)) {
    $conditions[] = $DB->sql_like('lco.coursename', ':course_name');
    $param['course_name'] = '%' . $searchtext . '%';
}


if ($year) {
    $conditions[] = " yc.classyear = :year ";
    $param['year'] = $year;
}

$sql_where = '';
if ($conditions) {
    $sql_where = ' WHERE ' . implode(' AND ', $conditions);
}
$orderby = "ORDER BY yc.learningstart DESC ";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $param, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $param);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once ('../inc/header.php'); ?>
<div id="contents">
    <?php include_once ('../inc/sidebar_enrol.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('stats_allcourselist', 'local_lmsdata'); ?></h3>
        <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <strong><?php echo get_string('stats_allcourselist', 'local_lmsdata'); ?></strong></div>
        <p class="page_sub_title"> <?php echo get_string('stats_longtext5', 'local_lmsdata'); ?></p><br/>
<!--        <p class="explain"><?php echo get_string('allcourse', 'local_lmsdata'); ?></p>-->

        <div class="down_area">
            <input type="submit" onclick="course_all_excel(); return false;" class="red_btn" value="<?php echo get_string('stats_longtext4', 'local_lmsdata'); ?> " style="float:left;"/>
            <p style="float:left; margin: 4px 0 0 10px;"></p>
        </div> <!--Down Area End-->

        <form name="" id="course_search" class="search_area" action="edu_course.php" method="get">
            <input type="hidden" name="page" value="1" />
            <label><?php echo get_string('stats_years', 'local_lmsdata'); ?> &nbsp;&nbsp;</label>
            <select name="year" title="year" class="w_260" style="margin:5px 20px 5px 0;">
                <?php
                $years = lmsdata_get_years();
                foreach ($years as $v => $y) {
                    $selected = '';
                    if ($v == $year) {
                        $selected = ' selected';
                    }
                    echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                }
                ?>
            </select>
            <br>
            <!--<label><?php echo get_string('stats_classification', 'local_lmsdata'); ?> &nbsp;&nbsp</label>-->
            <input type="text" name="searchtext" title="serch" value="<?php echo $searchtext; ?>" placeholder="과정명을 입력하세요 " class="w_260" style="color: #8E9094; margin:0 0 5px 15px;" />
            <input type="submit" class="blue_btn" value="<?php echo get_string('stats_search', 'local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/>          
        </form><!--Search Area2 End-->
        <!--<p style="float:left; color:red; margin-bottom: 5px;"><?php echo get_string('stats_longtext3', 'local_lmsdata'); ?></p>-->

        <table>
            <caption class="hidden-caption"><?php echo get_string('coursenow', 'local_lmsdata'); ?></caption>
            <thead>
                <tr>
                    <th scope="row"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row"><?php echo get_string('stats_years', 'local_lmsdata'); ?></th>
                    <th scope="row"><?php echo '기수'; ?></th>
                    <th scope="row"><?php echo get_string('course_name', 'local_lmsdata'); ?></th>
                    <th scope="row"><?php echo get_string('stats_student', 'local_lmsdata'); ?></th>
                    <th scope="row"><?php echo '이수자'; ?></th>
                    <th scope="row"><?php echo '미이수자'; ?></th>
                    <th scope="row"><?php echo '강의평가 평균(참여 수)'; ?></th>
                    <th scope="row"><?php echo get_string('stats_learningactivity', 'local_lmsdata'); ?></th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="24"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);

                foreach ($courses as $course) {
                    $submitted_count = lmsdata_get_submission_assign_count($course);

                    $sub_name = "";

                    $complete_sql = 'SELECT count(ra.id) 
                                                                    FROM {role_assignments} ra 
                                                                    JOIN {role} ro ON ra.roleid = ro.id 
                                                                    JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel2 
                                                                    JOIN {course_completions} cc ON cc.course = :courseid1 AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                                                    WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype';
                    $complete = $DB->count_records_sql($complete_sql, array('contextlevel2' => CONTEXT_COURSE, 'courseid1' => $course->id, 'courseid2' => $course->id, 'archetype' => 'student'));

                    $average_sql = 'SELECT SUM(lce.stars) total, COUNT(lce.userid) num, (SUM(lce.stars) / COUNT(lce.userid)) average, 
                                                                    lce.courseid, co.fullname  
                                                                    FROM {lmsdata_contents_evaluation} lce
                                                                    JOIN {course} co ON lce.courseid = co.id
                                                                    WHERE courseid = :courseid
                                                                    GROUP BY courseid';
                    $average = $DB->get_record_sql($average_sql, array('courseid' => $course->id));
                    ?>
                    <tr>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $course->classyear; ?></td>
                        <td><?php echo $course->classnum; ?></td>
                        <td class="left"><a href="<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->id; ?>"><?php echo $course->coursename; ?></a></td>
                        <td><?php echo $course->student; ?></td>
                        <td><?php echo $complete; ?></td>
                        <td><?php echo ($course->student - $complete); ?></td>
                        <td><?php
                            $evaluationresult = empty($average->average) ? '-' : number_format($average->average, 2, '.', '') . '(' . $average->num . ')';
                            echo $evaluationresult;
                            ?></td>
                        <td><input type="submit" id="" class="orange_btn_small" value="<?php echo get_string('stats_learninghistory', 'local_lmsdata'); ?>" onclick="document.location.href = 'edu_course_history.php?id=<?php echo $course->id ?>'; return false;">
                            <input type="submit" id="" class="orange_btn_small" value="<?php echo get_string('stats_learningprogress', 'local_lmsdata'); ?>" onclick="document.location.href = 'edu_course_progress.php?id=<?php echo $course->id ?>'; return false;">
                            <?php
                            $completion = new completion_info($course);
                            if ($completion->has_criteria()) {
                                ?>
                                <input type="submit" id="" class="orange_btn_small" value="이수완료" onclick="document.location.href = '<?php echo $CFG->wwwroot . '/report/completion/index.php?course=' . $course->id ?>'; return false;">
            <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table><!--Table End-->
            <?php
            if (($count_courses / $perpage) > 1) {
                print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            }
            ?>
        </div><!--Content End-->

    </div> <!--Contents End-->
    <script type="text/javascript">
        function course_edit_popup(id) {
            var tag = $("<div></div>");
            $.ajax({
                url: '<?php echo $SITECFG->wwwroot . '/chamktu/enrol/course_form.php'; ?>',
                data: {
                    parent: $('[name=parent]').val(),
                    category: $('[name=category]').val(),
                    id: id
                },
                success: function (data) {
                    tag.html(data).dialog({
                        title: '<?php echo get_string('stats_learningactivitystatus', 'local_lmsdata'); ?>',
                        modal: true,
                        width: 600,
                        maxHeight: getWindowSize().height - 20,
                        close: function () {
                            $(this).dialog('destroy').remove()
                        }
                    }).dialog('open');
                }
            });
        }
        function course_all_excel() {
    <?php
    $query_string = '';
    if (!empty($param)) {
        $query_array = array();
        foreach ($param as $key => $value) {
            $query_array[] = urlencode($key) . '=' . urlencode($value);
        }
        $query_string = '?' . implode('&', $query_array);
    }
    ?>
            var url = "edu_course_all.excel.php<?php echo $query_string; ?>";

            document.location.href = url;
        }
    </script>
    <?php
    include_once ('../inc/footer.php');

    