<?php
/**
 * 취소신청리스트 상세페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
$coursetype = optional_param('coursetype', 1, PARAM_INT); //1:직무 2:위탁
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);

if (!empty($id)) {
    $sql_select = "SELECT *, lac.timecreated lactimecreated, lac.status lacstatus, lca.timecreated  lcatimecreated";

    $sql_from = " FROM {lmsdata_applications_cancel} lac "
            . "join {lmsdata_course_applications} lca on lca.id=lac.applicationid "
            . "join {lmsdata_class} lc on lc.courseid=lca.courseid "
            . "join {lmsdata_course} lco on lco.id=lc.parentcourseid "
            . "join {course} co on co.id = lc.courseid "
            . "join {user} u on u.id = lca.userid ";
    $sql_where = " where lac.id = :id";
    //$sql_orderby = " order by lg.id desc";
    $data = $DB->get_record_sql($sql_select . $sql_from . $sql_where . $sql_orderby, array('id' => $id), ($currpage - 1) * $perpage, $perpage);

    $type_arrs = array(
        1 => '임직원 강의', 
        2 => '멘토 강의', 
        3 => '교육생 강의', 
        4 => '임직원, 멘토 강의', 
        5 => '멘토, 교육생 강의', 
        6 => '임직원, 교육생 강의', 
        7 => '전체 강의'
        );
    $cancle = $DB->get_record('lmsdata_applications_cancel', array('courseid' => $data->courseid, 'userid' => $data->userid));
}

$getpointsql = 'SELECT course, SUM(point) lecturepoint FROM {okmedia} WHERE course = :course GROUP BY course';
$getpoint = $DB->get_record_sql($getpointsql, array('course' => $data->lcacourseid));

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
//$arr1 = array('group' => '그룹할인', 'member' => '영재원할인');
//$arr2 = array('card' => '카드결제', 'bank' => '실시간계좌이체', 'vbank' => '가상계좌결제', 'unbank' => '무통장입금');
//$arr3 = array('stand' => '환불신청', 'complete' => '환불완료');
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title">취소신청 상세보기</h3>
        <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <a href="./edu_cancel_list.php">취소신청목록<a> > <strong>취소신청 상세보기</strong></div>
        <form name="" id="course_search"  method="post" enctype="multipart/form-data">            
            <h4 class="table_title" style="margin-top: 50px;margin-left: 30px;">사용자정보</h4>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr style="height: 45px">
                        <td class="field_title">신청자</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $data->lastname; ?></span>
                        </td>
                        <td class="field_title">아이디</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $data->username; ?></span>
                        </td>
                    </tr>                    
                    <tr style="height: 45px">
                        <td class="field_title">연락처</td>
                        <td class="field_value">
                            <span class="text_field">
                                <?php echo $data->phone ? substr($data->phone, 0, 3) . '-' . substr($data->phone, 3, 4) . '-' . substr($data->phone, 7, 4) : '-' ?></span>
                        </td>
                        <td class="field_title">이메일</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $data->email; ?></span>
                        </td>
                    </tr> 
                    <tr style="height: 45px">
                        <td class="field_title">생년월일</td>
                        <td class="field_value" colspan="3">
                            <span class="text_field"><?php echo $data->birthday ? substr($data->birthday, 0, 4) . '/' . substr($data->birthday, 4, 2) . '/' . substr($data->birthday, 6, 2) : '-' ?></span>
                        </td>
                </tbody>
            </table>
            <h4 class="table_title" style="margin-top: 230px;margin-left: 30px;">수강신청정보</h4>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <tr style="height: 45px">
                        <td class="field_title">년도</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $data->classyear . '년'; ?></span>
                        </td>
                        <td class="field_title">기수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $data->classnum . '기'; ?></span>
                        </td>
                    </tr>

                    <tr style="height: 45px">
                        <td class="field_title">과정명</td>
                        <td class="field_value" colspan="3">
                            <span class="text_field"><?php echo $data->fullname; ?></span>
                        </td>
                    </tr>     
                    <tr style="height: 45px">
                        <td class="field_title">수강형식</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $type_arrs[$data->classobject]; ?></span>
                        </td>
                        <td class="field_title">포인트</td>
                        <td class="field_value" >                            
                            <span class="text_field"><?php echo empty($getpoint->lecturepoint)? '0 P' : $getpoint->lecturepoint . ' P'; ?></span>
                        </td>
                    </tr> 
                    <tr style="height: 45px">
                        <td class="field_title">수강신청 기간</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo date('Y-m-d', $data->enrolmentstart) . ' ~ ' . date('Y-m-d', $data->enrolmentend) ?></span>
                        </td>
                        <td class="field_title">접수시간</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo date('Y-m-d', $data->timecreated); ?></span>
                        </td>
                    </tr>     
                </tbody>
            </table>

            <div id="btn_area">
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_cancel_list.php';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    //숫자만
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            return false;
    }
    //문자열 제거
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }

</script>
<?php
include_once ('../inc/footer.php');
