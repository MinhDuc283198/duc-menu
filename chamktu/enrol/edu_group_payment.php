<?php
/* 단체신청 결제 완료 처리해주는 ajax
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id  = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

if($grouplist = $DB->get_record('lmsdata_grouplist',array('id'=>$id))){
    $grouplist->status = 2;
    if($DB->update_record('lmsdata_grouplist', $grouplist)){
        $returnvalue->status = 'success';
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);


