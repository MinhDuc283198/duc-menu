<?php
/**
 * 결제정보변경 ajax
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once($CFG->dirroot . '/chamktu/lib.php');

$id = required_param('id', PARAM_INT);

$returnvalue = new stdClass();
$change = new stdClass();
$arrays = explode('&', $_REQUEST['data']);

//print_object('2'. unserialize($_REQUEST['data']));
$data = $DB->get_record('lmsdata_course_applications', array('id'=>$id));
foreach ($arrays as $array){

    $a1 = explode('=', $array);
    $a = $a1[0];
    $b = $a1[1];
    $data->$a = $b;
}
$data->timecreated = strtotime($data->timecreated);

$DB->update_record('lmsdata_course_applications',$data);


@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);