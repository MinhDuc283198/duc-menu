<?php
/**
 * 수강신청목록 리스트
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$coursename = optional_param('coursename', 0, PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);
//검색용 파라미터

$sql_where[] = " lca.paymentstatus != :paymentstatus ";
$params['paymentstatus'] = "cancel";
$sql_where[] = " lca.status != :status ";
$params['status'] = "cancel";

if ($classyear) {
    $sql_where[] = " lc.classyear = :classyear ";
    $params['classyear'] = $classyear;
}
if ($classnum) {
    $sql_where[] = " lc.classnum = :classnum ";
    $params['classnum'] = $classnum;
}
if (!empty($searchtext)) {
    $like[] = $DB->sql_like('u.lastname', ':lastname');
    $params['lastname'] = '%' . $searchtext . '%';
    $like[] = $DB->sql_like('u.firstname', ':firstname');
    $params['firstname'] = '%' . $searchtext . '%';
    $like[] = $DB->sql_like('lca.school', ':school');
    $params['school'] = '%' . $searchtext . '%';
    $like[] = $DB->sql_like('u.username', ':username');
    $params['username'] = '%' . $searchtext . '%';
    $like[] = $DB->sql_like('lca.phone', ':phone');
    $params['phone'] = '%' . $searchtext . '%';
    $sql_where[] = ' (' . implode(' OR ', $like) . ') ';
}
if ($coursename) {
    $sql_where[] = " lco.id = :coursename ";
    $params['coursename'] = $coursename;
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

// lmsdata_schedule 있음
//$sql_select_schedule = "SELECT lca.*, lc.classyear,lc.classnum,lc.courseid as mcourseid, co.fullname, u.lastname, u.username,lco.coursename, ls.consignmenttitle,
// lco.classtype ";
//$sql_from_schedule = " FROM {lmsdata_course_applications} lca "
//        . "join {lmsdata_class} lc on lc.id=lca.courseid "
//        . "JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id "
//        . "left JOIN {lmsdata_schedule} ls ON lc.scheduleid = ls.id "
//        . "join {course} co on co.id = lc.courseid "
//        . "join {user} u on u.id = lca.userid";

$sql_select = "SELECT lca.*, lc.classyear, lc.classnum, lc.courseid as mcourseid, 
                        co.fullname, u.lastname, u.username, u.firstname, lco.coursename, lca.completionstatus, u.phone1, u.phone2, u.id uid";
$sql_from = ' FROM {lmsdata_course_applications} lca 
                   JOIN {lmsdata_class} lc ON lc.courseid = lca.courseid 
                   JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id 
                   JOIN {course} co ON co.id = lc.courseid 
                  JOIN {context} ctx ON ctx.instanceid = co.id AND ctx.contextlevel = :contextlevel 
                  JOIN {user} u ON u.id = lca.userid 
                  JOIN {role_assignments} ra ON ra.userid = u.id AND ra.contextid = ctx.id 
                  JOIN {role} ro ON ro.id = ra.roleid AND ro.archetype = :archetype';

$sql_orderby = " order by lca.id desc";

$params['archetype'] = 'student';
$params['contextlevel'] = CONTEXT_COURSE;
if (!$excel) {
    $courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
} else {
    $courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params);
}

$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);

$query = "SELECT * from {lmsdata_sendtemplate} where sendtime = 0 and isused = 1 and (type = 'email' or type = 'sms' or type = 'push') ";
$totalcount = $DB->count_records_sql("SELECT count(*) from {lmsdata_sendtemplate} " . $where . $sort, $params);
$sendtemplate = $DB->get_records_sql($query);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

        <div id="content">
            <h3 class="page_title">수강신청목록</h3>
            <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <strong>수강신청목록</strong></div>
            <form name="course_search" id="course_search" class="search_area" action="edu_enrolment_list.php" method="get">
                <input type="hidden" name="page" value="1" />
                <input type="hidden" name="excel" value="0" />
                <div style="float:left;">
                    <select title="class" name="coursename" id="coursename" class="w_200">
                        <option value="0">과정명</option>
                        <?php
                        $datassql = 'select lco.id, lco.coursename, lca.userid getuserid '
                                . 'from {lmsdata_course} lco '
                                . 'join {lmsdata_class} lc on lc.parentcourseid = lco.id '
                                . 'join {lmsdata_course_applications} lca on lca.courseid = lc.courseid '; // $classyear $classnum
//                                . 'where lco.isused != 1 and lc.classyear = :lcclassyear and lc.classnum = :lcclassnum';
                        $datas = $DB->get_records_sql($datassql);
                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->id == $coursename) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->id . '"' . $selected . '> ' . $data->coursename . '</option>';
                        }
                        ?>
                    </select>
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="year_changed(this, 'lmsdata_class', 1);">
                        <option value="0">년도</option>
                        <?php
//                        $yearRange = 10;
//                        $currentYear = date('Y');
//                        $maxYear = ($currentYear + $yearRange);
                        $max = $DB->get_field_sql("SELECT max(classyear) FROM {lmsdata_class}");
                        $min = $DB->get_field_sql("SELECT min(classyear) FROM {lmsdata_class} where classyear != 0 ");

                        foreach (range($min, $max) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select> 
                    <select title="class" name="classnum" id="classnum" class="w_160" >
                        <option value="0">기수</option>
                        <?php
                        if ($classyear) {
                            if ($classtype) {
                                $classnum_where = " and classtype = $classtype";
                            }
                            if ($classyear) {
                                $classnum_where .= " and classyear = $classyear";
                            }
                            $datas = $DB->get_records_sql('select classnum from {lmsdata_class} where classnum != 0' . $classnum_where);

                            foreach ($datas as $data) {
                                $selected = '';
                                if ($data->classnum == $classnum) {
                                    $selected = ' selected';
                                }
                                echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                            }
                        }
                        ?>
                    </select>


                </div>
                <br><br><br>

                <div style="float:left;">
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="이름/아이디/전화번호를 입력하세요."  class="search-text"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>
                </div>
            </form><!--Search Area2 End-->
            <div style="float:right;">
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
            </div>
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="3%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>
                        <th scope="row" width="3%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                        <th scope="row" width="10%">수강신청일</th>
                        <th scope="row" width="6%">년도</th>
                        <th scope="row" width="7%">기수</th>
                        <th scope="row" width="30%">과정명</th>
                        <th scope="row" width="6%">신청자<br/>(아이디)</th>
                        <th scope="row" width="12%">전화번호</th>
                        <th scope="row" width="6%">이수여부</th>
                        <th scope="row" width="6%">사용자 구분</th>
                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="20">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    $type_arrs = array('10' => '임직원', '20' => '멘토', '30' => '교육생');
                    $startnum = $count_courses - (($currpage - 1) * $perpage);
                    foreach ($courses as $course) {
                        ?>
                        <tr>
                            <td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>
                            <td><?php echo $startnum--; ?></td>                    
                            <td><?php echo date('Y-m-d', $course->timecreated) ?></td>
                            <td><?php echo $course->classyear . '년' ?></td>
                            <td><?php echo $course->classnum ? $course->classnum . '기' : '-' ?></td>
                            <td><a href="<?php echo $CFG->wwwroot . '/chamktu/enrol/edu_enrolment_list.detail.php?id=' . $course->id; ?>">
                                    <?php echo $course->coursename ?></a></td>
                            <td><?php echo $course->lastname . $course->firstname; ?><br/>(<?php echo $course->username ?>)</td>
                            <td>
                                <?php
                                $phonenum = '';
                                if ($course->phone) {
                                    $phonenum = $course->phone;
                                } else if ($course->phone1) {
                                    $phonenum = $course->phone1;
                                } else {
                                    $phonenum = $course->phone2;
                                }
                                //핸드폰 번호 양식이 다 다르게 출력되어서 여러번 체크함 
                                if (strlen($phonenum) == 10) { //번호에서 0이 빠져있을때
                                    $phonenum = '0' . $phonenum;
                                }
                                if (strlen($phonenum) > 11) { //010-0000-0000 형식일때
                                    $chkphonenum = $phonenum;
                                } else if (strlen($phonenum) == 11) { // 01000000000 형식일때
                                    $chkphonenum = substr($phonenum, 0, 3) . '-' . substr($phonenum, 3, 4) . '-' . substr($phonenum, 7, 4);
                                }
                                echo ($chkphonenum) ? $chkphonenum : '-';
                                ?>
                            </td>
                            <td><?php
                                $ifcomplete = $DB->get_record('course_completions', array('userid' => $course->uid, 'course' => $course->mcourseid));
                                echo!empty($ifcomplete->timecompleted) ? '이수' : '미이수';
                                ?></td>
                            <td><?php
                                $selectuser = $DB->get_record('lmsdata_user', array('userid' => $course->uid));
                                if ($selectuser->usertypecode !== $course->usertypecode) {
                                    echo $type_arrs[$selectuser->usertypecode];
                                } else {
                                    echo $type_arrs[$course->usertypecode];
                                }
                                ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
            <div style="float:left;">
                <select name="send_template" id="send_template" style="float: left; width: 100px;">
                    <option value="0">선택</option>
                    <?php
                    foreach ($sendtemplate as $sendtemplates) {
                        echo "<option value='$sendtemplates->id'>[$sendtemplates->type]$sendtemplates->title</option>";
                    }
                    ?>
                </select>
                <button class="enter" onclick="send_template()">발송</button>
            </div>
            <div id="btn_area">            
                <div style="float:right;">
                    <input type="submit" class="blue_btn" style="margin-right: 10px;" value="수강신청" onclick="javascript:location.href = 'edu_enrolment_list_add.php';"/> 
                </div>
            </div>
            <?php
            if (($count_courses / $perpage) > 1) {
                print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            }
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $fields = array(
        '번호',
        '년도',
        '기수',
        '과정명',
        '아이디',
        '신청자',
        '전화번호',
        '이수여부',
        '학습진도율',
        '과제점수',
        '토론점수',
        '시험점수',
        '총점'
    );

    $date = date('Y-m-d', time());
    $filename = '수강신청목록_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($courses as $course) {

        //나의 진도율
        $sql = 'select avg(progress) from {lcmsprogress_grades} lg 
                join {lcmsprogress} l on l.id = lg.lcmsprogress 
                where l.course = :courseid and lg.userid = :userid';
        $myprogress = $DB->get_field_sql($sql, array('courseid' => $course->mcourseid, 'userid' => $course->userid));

        //나의 성적
        $sql = 'select ROUND(avg(gg.finalgrade)) as avggrade from {grade_items} gi 
                join {grade_grades} gg on gg.itemid = gi.id 
                where gi.courseid = :courseid and gi.itemmodule = :itemmodule and gg.userid = :userid';

        //과제
        $mygrade_assign = $DB->get_field_sql($sql, array('courseid' => $course->mcourseid, 'itemmodule' => 'assign', 'userid' => $course->userid));
        //토론
        $mygrade_forum = $DB->get_field_sql($sql, array('courseid' => $course->mcourseid, 'itemmodule' => 'forum', 'userid' => $course->userid));
        //퀴즈
        $mygrade_quiz = $DB->get_field_sql($sql, array('courseid' => $course->mcourseid, 'itemmodule' => 'quiz', 'userid' => $course->userid));
        //최종성적
        $sql = 'select ROUND(avg(gg.finalgrade)) as avggrade from {grade_items} gi 
                join {grade_grades} gg on gg.itemid = gi.id 
                where gi.courseid = :courseid and gi.itemtype = :itemtype and gg.userid = :userid';
        $mygrade = $DB->get_field_sql($sql, array('courseid' => $course->mcourseid, 'itemtype' => 'course', 'userid' => $course->userid));

        $myprogress = floor($myprogress * 100) / 100;
        $mygrade_assign = floor($mygrade_assign * 100) / 100;
        $mygrade_forum = floor($mygrade_forum * 100) / 100;
        $mygrade_quiz = floor($mygrade_quiz * 100) / 100;
        $mygrade = floor($mygrade * 100) / 100;

        $phonenum = '';
        if ($course->phone) {
            $phonenum = $course->phone;
        } else if ($course->phone1) {
            $phonenum = $course->phone1;
        } else {
            $phonenum = $course->phone2;
        }

        if (strlen($phonenum) == 10) { //번호에서 0이 빠져있을때
            $phonenum = '0' . $phonenum;
        }
        if (strlen($phonenum) > 12) { //010-0000-0000 형식일때
            $chkphonenum = $phonenum;
        } else if (strlen($phonenum) == 11) { // 01000000000 형식일때
            $chkphonenum = substr($phonenum, 0, 3) . '-' . substr($phonenum, 3, 4) . '-' . substr($phonenum, 7, 4);
        }

        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $course->classyear . '년');
        $worksheet[0]->write($row, $col++, $course->classnum . '기');
        $worksheet[0]->write($row, $col++, $course->coursename);
        $worksheet[0]->write($row, $col++, $course->username);
        $worksheet[0]->write($row, $col++, $course->lastname);
        $worksheet[0]->write($row, $col++, $chkphonenum ? $chkphonenum : '-');

        $ifcomplete = $DB->get_record('course_completions', array('userid' => $course->uid, 'course' => $course->mcourseid));
        $worksheet[0]->write($row, $col++, !empty($ifcomplete->timecompleted) ? '이수' : '미이수');
        $worksheet[0]->write($row, $col++, $myprogress);
        $worksheet[0]->write($row, $col++, $mygrade_assign);
        $worksheet[0]->write($row, $col++, $mygrade_forum);
        $worksheet[0]->write($row, $col++, $mygrade_quiz);
        $worksheet[0]->write($row, $col++, $mygrade);
        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">

    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    /**
     * 현금영수증 발급기능
     * @param {type} id
     * @returns {undefined}
     */
    function cashbill(id) {
        alert('준비중입니다.');
        return false;

        var tag = $("<div id='cashbill'></div>");
        var title = '현금영수증 발급';

        $.ajax({
            url: 'edu_cashbill.php',
            method: 'POST',
            data: {
                id: id,
                page: '<?php echo $currpage; ?>'
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: title,
                    modal: true,
                    width: 800,
                    maxHeight: getWindowSize().height - 20,
                    buttons: [
                        {id: 'save',
                            text: '발급',
                            click: function () {
                                if (!$('#frm_cashbill input[name=identityNum]').val()) {
                                    alert('식별번호를 입력해주세요');
                                    return false;
                                }
                                if (!$('#frm_cashbill input[name=supplyCost]').val()) {
                                    alert('공급가액을 입력해주세요');
                                    return false;
                                }
                                if (!$('#frm_cashbill input[name=tax]').val()) {
                                    alert('세액을 입력해주세요');
                                    return false;
                                }
                                if (!$('#frm_cashbill input[name=serviceFee]').val()) {
                                    alert('봉사료를 입력해주세요');
                                    return false;
                                }
                                $('#frm_cashbill').submit();
                            }},
                        {id: 'close',
                            text: '닫기',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    open: function () {
                        var t = $(this).parent();
                        var w = $(window);
                        var s = getWindowSize();

                        var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                        if (x < 0)
                            x = 0;
                        var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                        if (y < 0)
                            y = 0;
                        t.offset({
                            top: y,
                            left: x
                        });
                    },
                    close: function () {
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }
    /**
     * 템플릿 저장된 메일 발송
     
     * @returns {Boolean}     */
    function send_template() {
        var templateid = $("#send_template option:selected").val();

        var enrolmentid = [];

        if (!$(".courseid").is(":checked")) {
            alert('해당 내역을 선택하세요.');
            return false;
        }
        if (templateid == '0') {
            alert('템플릿을 선택하세요.');
            return false;
        }
        $("input[name=courseid]:checked").each(function () {
            enrolmentid.push($(this).val());
        });

        $.ajax({
            url: 'send_template.ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                templateid: templateid,
                enrolmentid: enrolmentid,
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 'success') {
                    alert('발송되었습니다.');
                    //console.log(data);
                }
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
                console.log(jqXHR);
            }
        });
    }
    /**
     * 엑셀다운로드
     
     * @returns {undefined}     */
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $("#course_search").submit();
        $("input[name='excel']").val(0);

//        var url = "edu_enrolment_list.php?excel=1";
//
//        document.location.href = url;
    }
</script>    
