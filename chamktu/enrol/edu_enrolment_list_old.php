<?php
/**
 * 과거 수강신청목록 리스트
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$paymentstatus = optional_param('paymentstatus', '', PARAM_RAW);
$classnum = optional_param('classnum', 0, PARAM_INT);
$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$discounttype = optional_param('discounttype', '', PARAM_RAW);
$neisyn = optional_param('neisyn', '', PARAM_RAW);
$placeyn = optional_param('placeyn', '', PARAM_RAW);
$receiptyn = optional_param('receiptyn', '', PARAM_RAW);

if ($paymentstatus) {
    $sql_where[] = " lao.paymentstatus = :paymentstatus ";
    $params['paymentstatus'] = $paymentstatus;
}
if ($discounttype) {
    $sql_where[] = " lao.discounttype = :discounttype ";
    $params['discounttype'] = $discounttype;
}
if ($neisyn) {
    if ($neisyn == 'Y') {
        $sql_where[] = " lao.neiscode != '' ";
    } else {
        $sql_where[] = " (lao.neiscode = '' or  lao.neiscode is null )";
    }
}
if ($placeyn) {
    if ($placeyn == 'Y') {
        $sql_where[] = " lao.locationnumber != '' ";
    } else {
        $sql_where[] = " (lao.locationnumber = '' or  lao.locationnumber is null )";
    }
}
if ($receiptyn) {
    if ($receiptyn == 'Y') {
        $sql_where[] = " lao.cshrresult != '' ";
    } else {
        $sql_where[] = " (lao.cshrresult = '' or  lao.cshrresult is null )";
    }
}

if (!empty($searchtext)) {
    $like = array();
    $like[] = $DB->sql_like('lg.repusername', ':repusername');
    $like[] = $DB->sql_like('lg.groupcode', ':groupcode');
    $params['repusername'] = '%' . $searchtext . '%';
    $params['groupcode'] = '%' . $searchtext . '%';
    $sql_where[] = ' ' . implode(' OR ', $like);
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lao.*, co.fullname, u.lastname ";
$sql_from = " FROM {lmsdata_applications_old} lao "
        . "join {lmsdata_course} lco on lco.coursecd = lao.coursecd "
        . "join {course} co on co.id = lco.courseid "
        . "join {user} u on u.id = lao.userid";

$sql_orderby = " order by lao.id desc";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => get_string('individual_payment', 'local_group_application'), 2 => get_string('bill_payment', 'local_group_application'), 3 => get_string('administrative_room_settlement', 'local_group_application'));
$arr1 = array('stand' => '대기', 'complete' => '결제완료', 'cancel' => '결제취소', 'fail' => '결제실패');
$arr2 = array('card' => '카드결제', 'bank' => '실시간계좌이체', 'vbank' => '가상계좌결제', 'unbank' => '무통장입금');
$arr3 = array('member' => '영재원할인', 'group' => '단체할인');
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>
    
    <div id="content">
        <h3 class="page_title">이전수강신청목록</h3>
        <form name="course_search" id="course_search" class="search_area" action="edu_enrolment_list_old.php" method="get">
            <input type="hidden" name="page" value="1" />
            <div style="float:left;">
                <select title="category01" name="paymentstatus" onchange="cata1_changed(this);"  class="w_160">
                    <option value="0">결제상태</option>
                    <?php
                    foreach ($arr1 as $key => $val) {
                        $selected = '';
                        if ($key == $paymentstatus) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $key . '"' . $selected . '> ' . $val . '</option>';
                    }
                    ?>
                </select>
                <select title="category01" name="discounttype" onchange="cata1_changed(this);"  class="w_160">
                    <option value="0">할인정보</option>
                    <?php
                    foreach ($arr3 as $key => $val) {
                        $selected = '';
                        if ($key == $discounttype) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $key . '"' . $selected . '> ' . $val . '</option>';
                    }
                    ?>
                </select>
                <select title="category01" name="neisyn" class="w_160">
                    <option value="0">NEIS 유무</option>
                    <option value="Y" <?php echo $neisyn == 'Y' ? 'selected' : '' ?>>Y</option>
                    <option value="N" <?php echo $neisyn == 'N' ? 'selected' : '' ?>>N</option>
                </select>
                <select title="category01" name="placeyn" class="w_160">
                    <option value="0">지명번호 유무</option>
                    <option value="Y" <?php echo $placeyn == 'Y' ? 'selected' : '' ?>>Y</option>
                    <option value="N" <?php echo $placeyn == 'N' ? 'selected' : '' ?>>N</option>
                </select>
<!--                <select title="category01" name="receiptyn" class="w_160">
                    <option value="0">현금영수증 발급유무</option>
                    <option value="Y" <?php echo $receiptyn == 'Y' ? 'selected' : '' ?>>Y</option>
                    <option value="N" <?php echo $receiptyn == 'N' ? 'selected' : '' ?>>N</option>
                </select>-->

            </div>
            <br><br><br>

            <div style="float:left;">
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="과정명 또는 신청자명을 입력하세요."  class="search-text"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
            </div>
        </form><!--Search Area2 End-->
            <div style="float:left;">
                <input type="submit" class="" value="현재수강이력" onclick="javascript:location.href = 'edu_enrolment_list.php';"/> 
            </div>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%">수강신청일</th>
                    <th scope="row" width="20%">과정명</th>
                    <th scope="row" width="5%">결제방법</th>
                    <th scope="row" width="5%">결제금액</th>
                    <th scope="row" width="10%">신청자</th>
                    <th scope="row" width="5%">결제상태</th>
                    <th scope="row" width="5%">할인정보</th>
                    <th scope="row" width="10%">NEIS 개인정보</th>
                    <th scope="row" width="15%">지명번호</th>
                    <!--<th scope="row" width="5%">현금영수증</th>-->
                    
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="20">데이터가 없습니다.</td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                    <tr>
                        <td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>
                        <td><?php echo $startnum--; ?></td>                    
                        <td><?php echo date('Y-m-d',$course->timecreated) ?></td>
                        <td><?php echo $course->fullname ?></td>
                        <td><?php echo $arr2[$course->paymentmeans] ?></td>
                        <td><?php echo number_format($course->price) . '원' ?></td>
                        <td><a href="<?php echo $CFG->wwwroot . '/chamktu/enrol/edu_enrolment_list.detail.php?id=' . $course->id; ?>">
                                <?php echo $course->lastname ?></a></td>
                        <td><?php echo $arr1[$course->paymentstatus] ?></td>
                        <td><?php echo $arr3[$course->discounttype] ?></td>
                        <td><?php echo $course->neiscode ?></td>
                        <td><?php echo $course->locationnumber ?></td>
<!--                        <td>
                            <?php if ($course->paymentmeans != 'card' && $course->paymentstatus == 'complete') { ?>
                                <?php if (empty($course->cshrresult)) echo '<input type="button" onclick="cashbill(\'' . $course->id . '\')" value="발급">';
                                else echo '발급완료'; ?>
                            <?php } ?>
                        </td>-->
                        
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <?php
        print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        ?>            
    </div><!--Content End-->
</div> <!--Contents End-->

<?php include_once ('../inc/footer.php'); ?>

