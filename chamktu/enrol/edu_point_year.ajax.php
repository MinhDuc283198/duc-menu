<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once $CFG->dirroot.'/local/jeipoint/lib.php';

$pid = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$year_sql = 'SELECT 
                    classyear AS id, 
                    classyear AS value 
               FROM {lmsdata_class} 
               WHERE parentcourseid = :parentcourseid GROUP BY classyear ORDER BY classyear ';

$years = $DB->get_records_sql($year_sql, array('parentcourseid' => $pid));

if($years) {
    $returnvalue->status = 'success';
    $returnvalue->years = $years;
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);