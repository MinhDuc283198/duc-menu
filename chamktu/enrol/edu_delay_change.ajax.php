<?php

/**
 * 수강연기 ajax페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');
$id = required_param('delayid', PARAM_INT);
$type = required_param('type', PARAM_INT);

$returnvalue = new stdClass();

//    $check = $DB->get_record("lmsdata_applications_delay",array('id'=>$id));
//    
//    $returnvalue->message = get_string('empty_case','local_lmsdata');

$delay = $DB->get_record_sql("select lad.*, lc.courseid course
    , (select courseid from m_lmsdata_class lc2 where lc2.scheduleid = lad.scheduleid and lc2.parentcourseid = lc.parentcourseid) newcourse
    from m_lmsdata_applications_delay lad 
    join m_lmsdata_class lc on lc.id = lad.courseid where lad.id = $id ");
$applications_status = 'delay';
if ($type == 1) {
    $delay_status = 1;
    //기존과정 유저 유보로 변경
    $enrol = $DB->get_record('enrol', array('courseid' => $delay->course, 'enrol' => 'manual'));
    $user_enrolments = $DB->get_record('user_enrolments', array('enrolid' => $enrol->id, 'userid' => $delay->userid));
    $user_enrolments->status = 1;
    $DB->update_record('user_enrolments', $user_enrolments);
} else if ($type == 2) {
    $classid = required_param('classid', PARAM_INT);
    $lmsdata_class = $DB->get_record('lmsdata_class', array('id' => $classid));

    $delay_status = 2;
    //새로운과정에 유저추가
    $user->roleid = 5;
    $user->shortname == 'student';
    $user->userid = $delay->userid;
    set_assign_user($lmsdata_class->courseid, $user);

    //새로운과정에 히스토리 복사
    $okmedia_track_datas = $DB->get_records_sql("select ot.* 
        ,(select com2.id from m_course_modules com2 join m_okmedia ok2 on ok2.id = com2.instance where ok2.contents = ok.contents and com2.course = $lmsdata_class->courseid) okmediaid
        from m_okmedia_track ot
        join m_course_modules com on ot.okmediaid = com.id
        join m_okmedia ok on ok.id = com.instance where com.course = $delay->course and ot.userid = $delay->userid ");
    foreach ($okmedia_track_datas as $okmedia_track_data) {
        $DB->insert_record('okmedia_track', $okmedia_track_data);
    }

    //새로운과정에 진도율 복사
    $okmedia_playtime_datas = $DB->get_records_sql("select ot.* 
        ,(select com2.id from m_course_modules com2 join m_okmedia ok2 on ok2.id = com2.instance where ok2.contents = ok.contents and com2.course = $lmsdata_class->courseid) okmediaid
        from m_okmedia_playtime ot
        join m_course_modules com on ot.okmediaid = com.id
        join m_okmedia ok on ok.id = com.instance where com.course = $delay->course and ot.userid = $delay->userid ");
    foreach ($okmedia_playtime_datas as $okmedia_playtime_data) {
        $DB->insert_record('okmedia_playtime', $okmedia_playtime_data);
    }

    //lcmsprogress 복사
    if ($lcmsprogress = $DB->get_record("lcmsprogress", array('course' => $delay->course))) {
        if ($newprogress = $DB->get_record("lcmsprogress", array('course' => $lmsdata_class->courseid))) {
//            $lcmsprogress->course = $lmsdata_class->courseid;
//            $lcmsprogressid = $DB->insert_record('lcmsprogress', $lcmsprogress);
            $lcmsprogress_grades = $DB->get_records("lcmsprogress_grades", array('lcmsprogress' => $lcmsprogress->id, 'userid' => $delay->userid));
            foreach ($lcmsprogress_grades as $lcmsprogress_grade) {
                $lcmsprogress_grade->lcmsprogress = $newprogress->id;
                $DB->insert_record('lcmsprogress_grades', $lcmsprogress_grade);
            }
        }
    }

    //기존의 수강신청정보를 이월된 과정에 맞게 복사
    if ($application = $DB->get_record('lmsdata_course_applications', array('id' => $delay->applicationid))) {
        $application->courseid = $lmsdata_class->id;
        $application->status = 'apply';
        $DB->insert_record('lmsdata_course_applications', $application);
    }
} else {
    $applications_status = 'apply';
    $delay_status = 3;
}

//신청테이블 상태값 변경
$application = $DB->get_record('lmsdata_course_applications', array('id' => $delay->applicationid));
$application->status = $applications_status;
if ($DB->update_record('lmsdata_course_applications', $application)) {
    local_course_applications_update_training_history($application->id);
}

//연기신청 상태값 변경
$delay = $DB->get_record("lmsdata_applications_delay", array('id' => $id));
$delay->timemodified = time();
$delay->status = $delay_status;
$DB->update_record('lmsdata_applications_delay', $delay);
$returnvalue->status = 'success';
if ($type == 2) {
    redirect("edu_delay_list.php");
}
@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
