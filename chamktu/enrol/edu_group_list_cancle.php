<?php
/* 
 * 단체 신청을 취소하는 ajax페이지
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$ids  = required_param_array('ids', PARAM_INT);

foreach ($ids as $id){
    
    $group = $DB->get_record('lmsdata_grouplist', array('id' => $id));
    $applys = $DB->get_records('lmsdata_course_applications', array('groupcode' => $group->groupcode));
    foreach ($applys as $apply){
        $apply->status = 'cancel';
        if($DB->update_record('lmsdata_course_applications',$apply)){
            local_course_applications_update_training_history($apply->id);
        }
    }
    $group->status = 1;
    $DB->update_record('lmsdata_grouplist', $group);
}
