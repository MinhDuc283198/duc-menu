<?php
/**
 * 이수번호 생성 함수
 * @global type $DB
 * @param type $id
 * @param type $userid
 * @param type $courseid
 * @return string
 */
function edu_enrolment_certinum($id,$userid,$courseid){
    global $DB;
    
    //이수증에 넣을 데이터 가져옴
    $sql = "select lco.coursename,lc.courseid  ,lca.userid as userid,lca.id, "
            . "lc.id as lcid, lco.classtype, lc.classyear, lc.classnum, "
            . "lc.certificatepre,ls.consignmentplace ,lc.classtype as classtype2 ";
    $from = "
        from {lmsdata_course_applications} lca 
        join {lmsdata_class} lc on lc.courseid=lca.courseid 
        join {lmsdata_course} lco on lco.id = lc.parentcourseid  
        join {lmsdata_schedule} ls on ls.id = lc.scheduleid  ";
     $where = "where lc.courseid = $courseid "
             . "and lca.userid = $userid ";
    $cert = $DB->get_record_sql($sql . $from .$where);
    

        if ($cert->classtype == 1) {
        $coursetype = get_string('cer:classtype1', 'local_mypage') . '-';
    } else if ($cert->classtype == 2) {
        $coursetype = get_string('cer:classtype2', 'local_mypage') . '-';
    } else if ($cert->classtype == 3) {
        $coursetype = get_string('cer:classtype3', 'local_mypage') . '-';
    }

    $prefix = $cert->certificatepre;
    // 최대 8자리 숫자 lmsdata_course_application 아이디번호와 앞에 0을 합쳐 8자리 숫자를 만듬
        $defaultnum = '00000000';
        $index = strlen($cert->id);
        $number = substr($defaultnum, 0, 8 - $index);
        $indexnum =  $number . $cert->id;
        $documentid = '';
        if ($prefix) {
            $documentid = $prefix . '-' . $coursetype . $cert->classyear . '-' . $cert->classnum . '-' . $indexnum; //증서번호;    
        } else {
            $documentid = get_string('cer:chamedu', 'local_mypage') . '-' . $coursetype . $cert->classyear . '-' . $cert->classnum . '-' . $indexnum;
        }
    return $documentid;
}
 
?>
