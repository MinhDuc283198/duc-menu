<?php
/**
 * 재능 특화 신임과정 포인트 목록
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
require_once $CFG->dirroot . '/local/jeipoint/lib.php';
require_once $CFG->libdir . '/excellib.class.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/enrol/edu_point_new.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$page = optional_param('page', 1, PARAM_INT); //현재페이지
$perpage = optional_param('perpage', 20, PARAM_INT); //한페이지에 보여줄 로우수
$excel = optional_param('excel', 0, PARAM_INT);
$pointtype = optional_param('pointtype', 0, PARAM_INT); //과정유형
$courseid = optional_param('pointcoursename', 0, PARAM_INT); //lmsdata_course->id
$classyear = optional_param('pointclassyear', 0, PARAM_INT); //년도
$classnum = optional_param('pointclassnum', '', PARAM_RAW); //기수
$qualified = optional_param('qualified', 0, PARAM_INT); //자격충족여부
$usertypecode = optional_param('usertypecode', 0, PARAM_INT); //사용자 구분
$searchtext = optional_param('searchtext', '', PARAM_TEXT); //검색값
//데이터 가져오기

$sql = 'SELECT 
            lju.id, lju.userid, lju.courseid, lju.totalpoint, lju.sumpoint, lju.pointtype, lju.losttime,
            ur.lastname, ur.username,
            lu.usertypecode, lu.phone1,
            lc.classyear, lc.classnum,
            lco.coursename ';

$sql_from = 'FROM {local_jeipoint_user} lju 
             JOIN {user} ur ON ur.id = lju.userid
             JOIN {lmsdata_user} lu ON ur.id=lu.userid 
             JOIN {lmsdata_class} lc ON lc.courseid = lju.courseid 
             JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid ';


$params = array();
$where = array();

$where[] = ' lju.visible = :visible ';
$params['visible'] = 1;
//과정유형 조건
if (!empty($pointtype)) {
    $where[] = ' lju.pointtype = :pointtype ';
    $params['pointtype'] = $pointtype - 1;
}

//과정명 - lmsdata_course->id
if (!empty($courseid)) {
    $where[] = ' lco.id = :courseid ';
    $params['courseid'] = $courseid;
}

//년도
if (!empty($classyear)) {
    $where[] = ' lc.classyear = :classyear ';
    $params['classyear'] = $classyear;
}

//기수
if (!empty($classnum)) {
    $where[] = ' lc.classnum = :classnum ';
    $params['classnum'] = $classnum;
}

//자격충족여부
if (!empty($qualified)) {
    if ($qualified == 1) {
        $where[] = ' lju.totalpoint <= lju.sumpoint ';
    } else {
        $where[] = ' lju.totalpoint > lju.sumpoint ';
    }
}

//사용자 구분
if (!empty($usertypecode)) {
    $where[] = ' lu.usertypecode = :usertypecode ';
    $params['usertypecode'] = $usertypecode;
}

//검색어 조건
if ($searchtext) {
    $where[] = ' (' . $DB->sql_like('ur.lastname', ':lastname') . ' OR ' . $DB->sql_like('ur.username', ':username') . ') ';
    $params['lastname'] = '%' . $searchtext . '%';
    $params['username'] = '%' . $searchtext . '%';
}

$order = ' ORDER BY totalpoint > sumpoint DESC, losttime ASC ';

if (!empty($where)) {
    $where = ' WHERE ' . implode(' AND ', $where);
} else {
    $where = '';
}

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js',
    $CFG->wwwroot . '/chamktu/enrol/edu_point.js',
);

$utype_arr = array(
    10 => JEI_USER_TYPECODE_10,
    20 => JEI_USER_TYPECODE_20,
    30 => JEI_USER_TYPECODE_30,
);

if (!$excel) {

    $offset = ($page - 1) * $perpage;
    $users = $DB->get_records_sql($sql . $sql_from . $where . $order, $params, $offset, $perpage);
    $count_sql = "SELECT count(lju.id) ";
    $total_count = $DB->count_records_sql($count_sql . $sql_from . $where, $params);
    $num = $total_count - $offset;

    include_once (dirname(dirname(__FILE__)) . '/inc/header.php');
    ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

        <div id="content">
            <h3 class="page_title">포인트 현황</h3>
            <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <strong>포인트현황</strong></div>

            <form name="" id="course_search" class="search_area" action="edu_point_new.php" method="get">
                <input type="hidden" title="page" name="page" value="1" />
                <input type="hidden" title="excel" name="excel" value="0" />
                <div style="float:left;">
                    <select title="pointtype" name="pointtype" id="pointtype" class="w_160" onchange="point_pointtype_changed(this);">
                        <option value="0">과정유형</option>
                        <option value="1" <?php echo (!empty($pointtype) && $pointtype == 1) ? 'selected' : ''; ?>>신임과정</option>
                        <option value="2" <?php echo (!empty($pointtype) && $pointtype == 2) ? 'selected' : ''; ?>>연차별과정</option>
                    </select>
                    <select title="pointcoursename" name="pointcoursename" id="pointcoursename" class="w_200" onchange="point_pointcourse_changed(this);">
                        <option value="0">과정명</option>
                        <?php
                        if (!empty($courseid)) {
                            $idnumbers = array(
                                1 => JEL_COURSE_CATEGORY_TRUST,
                                2 => JEL_COURSE_CATEGORY_YEAR
                            );

                            $idnumber = $idnumbers[$pointtype];
                            $course_sql = 'SELECT 
                                                        lc.id, lc.coursename AS value
                                                FROM {lmsdata_course} lc
                                                JOIN {course} co ON co.id = lc.courseid 
                                                JOIN {course_categories} cc ON cc.id = co.category
                                                WHERE cc.idnumber = :idnumber ';

                            $courses = $DB->get_records_sql($course_sql, array('idnumber' => $idnumber));
                            if (!empty($courses)) {
                                foreach ($courses AS $cid => $corusename) {
                                    $selected = '';
                                    if ($cid == $courseid) {
                                        $selected = 'selected';
                                    }
                                    echo '<option value="' . $cid . '" ' . $selected . '>' . $corusename->value . '</option>';
                                }
                            }
                        }
                        ?>
                    </select>
                    <select title="pointclassyear" name="pointclassyear" id="pointclassyear" class="w_120" onchange="point_pointyear_changed(this);">
                        <option value="0">년도</option>
    <?php
    if (!empty($classyear)) {
        $year_sql = 'SELECT 
                                                    classyear AS id, 
                                                    classyear AS value 
                                               FROM {lmsdata_class} 
                                               WHERE parentcourseid = :parentcourseid GROUP BY classyear ORDER BY classyear ';

        $years = $DB->get_records_sql($year_sql, array('parentcourseid' => $courseid));
        if (!empty($years)) {
            foreach ($years AS $yid => $year) {
                $selected = '';
                if ($yid == $classyear) {
                    $selected = 'selected';
                }
                echo '<option value="' . $yid . '" ' . $selected . '>' . $year->value . '</option>';
            }
        }
    }
    ?>
                    </select> 
                    <select title="pointclassnum" name="pointclassnum" id="pointclassnum" class="w_160" >
                        <option value="0">기수</option>
    <?php
    if (!empty($classnum)) {
        $classnum_sql = 'SELECT 
                                                    classnum AS id, 
                                                    classnum AS value 
                                               FROM {lmsdata_class} 
                                               WHERE parentcourseid = :parentcourseid AND classyear = :classyear ORDER BY classnum ';

        $classnums = $DB->get_records_sql($classnum_sql, array('parentcourseid' => $courseid, 'classyear' => $classyear));
        if (!empty($years)) {
            foreach ($classnums AS $cnid => $csnum) {
                $selected = '';
                if ($cnid == $classnum) {
                    $selected = 'selected';
                }
                echo '<option value="' . $cnid . '" ' . $selected . '>' . $csnum->value . '</option>';
            }
        }
    }
    ?>
                    </select>
                    </br>
                    <select title="qualified" name="qualified" id="qualified" class="w_160" >
                        <option value="0">자격충족여부</option>
                        <option value="1" <?php echo (!empty($qualified) && $qualified == 1) ? 'selected' : ''; ?>>자격충족</option>
                        <option value="2" <?php echo (!empty($qualified) && $qualified == 2) ? 'selected' : ''; ?>>자격미달</option>
                    </select>
                    <select title="usertypecode" name="usertypecode" id="usertypecode" class="w_160" >
                        <option value="0">구분</option>
                        <option value="10" <?php echo (!empty($usertypecode) && $usertypecode == 10) ? 'selected' : ''; ?>>임직원</option>
                        <option value="20" <?php echo (!empty($usertypecode) && $usertypecode == 20) ? 'selected' : ''; ?>>멘토</option>
                        <option value="30" <?php echo (!empty($usertypecode) && $usertypecode == 30) ? 'selected' : ''; ?>>교육생</option>
                    </select>
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="이름/아이디를 입력하세요."  class="search-text"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
                </div>
            </form><!--Search Area2 End-->
            <div style="float:right;">
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
            </div>
            <table>
                <caption class="hidden-caption">회원관리</caption>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                        <th scope="row">유형</th>
                        <th scope="row">과정명</th>
                        <th scope="row">년도</th>
                        <th scope="row">기수</th>
                        <th scope="row">구분</th>
                        <th scope="row">이름</th>
                        <th scope="row">아이디</th>
                        <th scope="row">획득포인트</th>
                        <th scope="row">목표포인트</th>
                        <th scope="row">자격미달일</th>
                        <th scope="row">내역</th>
                    </tr>
                </thead>
                <tbody>
    <?php
    if ($total_count > 0) {
        foreach ($users as $user) {
            $pointtype = ($user->pointtype == 1) ? '연차별과정' : '신임과정';

            if ($user->totalpoint > $user->sumpoint) {
                $lostdate = date('Y-m-d', $user->losttime);
            } else {
                $lostdate = '-';
            }

            $userid = $user->userid;
            $courseid = $user->courseid;
            ?>
                            <tr>
                                <td width="5%"><?php echo $num--; ?></td>
                                <td><?php echo $pointtype; ?></td>
                                <td><?php echo $user->coursename; ?></td>
                                <td><?php echo $user->classyear; ?></td>
                                <td><?php echo $user->classnum; ?></td>
                                <td><?php echo $utype_arr[$user->usertypecode]; ?></td>
                                <td><?php echo $user->lastname; ?></td>
                                <td><?php echo $user->username; ?></td>
                                <td><?php echo $user->sumpoint; ?></td>
                                <td><?php echo $user->totalpoint; ?></td>
                                <td><?php echo $lostdate; ?></td>
                                <td><input type="button" name="" value="내역" data-userid ="<?php echo $userid; ?>" data-courseid ="<?php echo $courseid; ?>" onclick="popup_point(this)" /></td>
                            </tr>
            <?php
        }
    } else {
        ?>
                        <tr>
                            <td colspan="11">등록된 사용자가 없습니다.</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table><!--Table End-->

                    <?php
                    if (($total_count / $perpage) > 1) {
                        print_paging_navbar_script($total_count, $page, $perpage, 'javascript:cata_page(:page);');
                    }
                    ?>


        </div><!--Content End-->

    </div> <!--Contents End-->
    <script type="text/javascript">
        function course_list_excel() {
            $("input[name='excel']").val(1);
            $("#course_search").submit();
        }
    </script>    
    <?php
    include_once ('../inc/footer.php');
} else {
    $users = $DB->get_records_sql($sql . $sql_from . $where . $order, $params);
    $fields = array(
        '유형',
        '과정명',
        '년도',
        '기수',
        '구분',
        '이름',
        '아이디',
        '전화번호',
        '획득포인트',
        '목표포인트',
        '자격미달일',
    );

    $filename = '포인트현황(' . date('Ymd') . ').xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($users as $user) {
        $pointtype = ($user->pointtype == 1) ? '연차별과정' : '신임과정';

        if ($user->totalpoint > $user->sumpoint) {
            $lostdate = date('Y-m-d', $user->losttime);
        } else {
            $lostdate = '-';
        }

        $userid = $user->userid;
        $courseid = $user->courseid;

        $col = 0;
        $worksheet[0]->write($row, $col++, $pointtype);
        $worksheet[0]->write($row, $col++, $user->coursename);
        $worksheet[0]->write($row, $col++, $user->classyear);
        $worksheet[0]->write($row, $col++, $user->classnum);
        $worksheet[0]->write($row, $col++, $utype_arr[$user->usertypecode]);
        $worksheet[0]->write($row, $col++, $user->lastname);
        $worksheet[0]->write($row, $col++, $user->username);
        $worksheet[0]->write($row, $col++, $user->phone1);
        $worksheet[0]->write($row, $col++, $user->sumpoint);
        $worksheet[0]->write($row, $col++, $user->totalpoint);
        $worksheet[0]->write($row, $col++, $lostdate);
        $row++;
    }

    $workbook->close();
    die;
}
?>
