<?php

/**
 * 과정변경 ajax 과정 반환 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';


$type = required_param('type', PARAM_INT);
$year = required_param('checkyear', PARAM_INT);

$returnvalue = new stdClass();

if ($type == 1) {
    $catagories_sql = 'select lc.classnum, lc.classyear, lc.id, lco.classobject lcoclassobject
                                    FROM {lmsdata_class} lc
                                    JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                                    where lc.classyear =:lcclassyear
                                    order by classnum';
    $catagories = $DB->get_records_sql($catagories_sql, array('lcclassyear' => $year));
    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories;
} else if ($type == 2) {
    $identid = required_param('identid', PARAM_INT);

    $cata_sql = 'select id, classyear, classnum FROM {lmsdata_class} WHERE id= :lcid';
    $catacontent = $DB->get_records_sql($cata_sql, array('lcid' => $identid));

    $catagories_sql = 'select lc.*, lco.coursename '
            . 'from {lmsdata_class} lc '
            . 'join {lmsdata_course} lco on lc.parentcourseid = lco.id '
            . 'where lc.classyear = :lcyear and lc.classnum = :lcclassnum';
    $params = array(
        'lcyear' => $catacontent[$identid]->classyear,
        'lcclassnum' => $catacontent[$identid]->classnum
    );

    $catagories = $DB->get_records_sql($catagories_sql, $params);


    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories;
} else if ($type == 3) {
    $courseid = required_param('courseid', PARAM_INT);
    $catagories_sql = 'select lc.*, lco.coursename, lco.classobject '
            . 'from {lmsdata_class} lc '
            . 'join {lmsdata_course} lco on lc.parentcourseid = lco.id '
            . 'where lc.classyear = :lcyear and lc.courseid = :lccourseid';

    $catagories = $DB->get_record_sql($catagories_sql, array('lcyear' => $year, 'lccourseid' => $courseid));
    
    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories;
}

//@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
