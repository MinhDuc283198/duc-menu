<?php
require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname (__FILE__)).'/lib/paging.php';
require_once dirname(dirname (__FILE__)).'/lib.php';
$PAGE->set_context($context);

$userid    = required_param('userid',  PARAM_INT);
$courseid  = required_param('courseid', PARAM_INT);

$sql_select = ' SELECT * FROM {local_jeipoint} WHERE userid = :userid AND courseid = :courseid  ORDER BY timecreated DESC ';

$points = $DB->get_records_sql($sql_select, array('userid'=>$userid, 'courseid'=>$courseid));

?>
<table class="table">
    <colgroup>
        <col width="45%" />
        <col width="/" />
        <col width="/" />
    </colgroup>
    <thead>
        <tr>
            <th>지급내용</th> 
            <th>지급(차감)일</th>
            <th>지급(차감)포인트</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($points)) {
            foreach ($points AS $point) {
                echo '<tr>';
                echo '<td class="text-left">' . $point->title . '</td>';
                echo '<td>' . date('Y.m.d', $point->timecreated) . '</td>';
                echo '<td>' . $point->point . 'P</td>';
                echo '</tr>';
            }
        } else {
            echo '<tr><td colspan="3">포인트 내역이 없습니다.</td></tr>';
        }
        ?>
    </tbody>
</table>