<?php
/**
 * 결제 취소 ajax
 */

require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');

$id = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$refund = $DB->get_record('lmsdata_course_applications', array('id' => $id));
$lmscourse = $DB->get_record('lmsdata_class', array('id' => $refund->courseid));
$refund->status = 'cancel';
$refund->paymentstatus = 'cancel';
$refund->timemodified = time();

if($refund->groupcode){
    $group = $DB->get_record('lmsdata_grouplist', array('groupcode' => $refund->groupcode));
    $groupapply = $DB->get_record('lmsdata_groupapply', array('groupid' => $group->id,'userid'=>$refund->userid));
    $groupapply->status = 2;
    $DB->update_record('lmsdata_groupapply', $groupapply);
}

if ($refund_cancel = $DB->get_record('lmsdata_applications_cancel', array('applicationid' => $id))) {
    $refund_cancel->status = 'complete';
    $refund_cancel->timemodified = time();
    $DB->update_record('lmsdata_applications_cancel', $refund_cancel);

}else{
    $refund_cancel->courseid = $refund->courseid;
    $refund_cancel->userid = $refund->userid;
    $refund_cancel->status = 'complete';
    $refund_cancel->timecreated =  time();    
    $refund_cancel->applicationid = $refund->id;
    $refund_cancel->refund = $refund->price;
    $refund_cancel->reason = '관리자취소';    
    $DB->insert_record('lmsdata_applications_cancel', $refund_cancel);
}

    if (set_unassign_user($lmscourse->courseid, $refund->userid, 5)) {
        $returnvalue->status = 'success';
        if($DB->update_record('lmsdata_course_applications',$refund)){
            local_course_applications_update_training_history($refund->id);
        }
    }
@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
