<?php
/**
 * 수강신청 관리자 등록
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
//require_once $CFG->dirroot.'/local/courselist/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/enrol/edu_enrolment_list_add.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);


if (!empty($id)) {
    $course_sql = " SELECT co.id coid, co.fullname, lc.*, lco.classobject lcoclassobject
                    FROM {course} co
                    JOIN {lmsdata_class} lc ON co.id = lc.courseid 
                    JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                    WHERE lc.id = :id ";
    $params = array('id' => $id);
    $course = $DB->get_record_sql($course_sql, $params);

    $context = context_course::instance($course->coid);

    $date_disabled = 'disabled';
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_enrol.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo empty($id) ? '수강신청' : '수강신청수정'; ?></h3>
        <div class="page_navbar"><a href="./edu_enrolment_list.php">수강관리</a> > <a href="./edu_enrolment_list.php">수강신청목록</a> > <strong><?php echo empty($id) ? '수강신청' : '수강신청수정'; ?></strong></div>
        <form name="" id="course_search" action="edu_enrolment_add.submit.php" method="post" enctype="multipart/form-data">
            <?php
            if (!empty($course)) {
                echo '<input type="hidden" name="id" value="' . $id . '" />';
            }
            ?>
            <table cellpadding="0" cellspacing="0" class="detail">                
                <tbody>
                    <tr>
                        <td class="field_title">년도</td>
                        <td class="field_value">
                            <select title="year" name="classyear" id="classyear" onchange="classnumchange(1);" class="w_160">

                                <?php
                                $max = $DB->get_field_sql("SELECT max(classyear) FROM {lmsdata_class}");
                                $min = $DB->get_field_sql("SELECT min(classyear) FROM {lmsdata_class}");
//                                $yearRange = 10;
                                $currentYear = date('Y');
//                                $maxYear = ($currentYear + $yearRange);
                                echo '<option value="0" ' . $disabled . '>년도</option>';
                                foreach (range($currentYear, $max) as $year) {
                                    $selected = '';
                                    if ($year == $course->classyear) {
                                        $selected = 'selected';
                                    }
                                    echo '<option value="' . $year . '"' . $selected . ' ' . $disabled . '>' . $year . '년</option>';
                                }
                                ?>
                            </select> 
                        </td>
                <input type="hidden" title="price" name="status" value="apply" class="w_120"/>
                <td class="field_title" id="classnum_select1">기수</td>
                <td class="field_value" id="classnum_select2">
                    <select title="classnum_select" name="classnum_select" id="classnum_select" class="w_160" onchange="classnumchange(2)">
                        <option value="0">기수선택</option>
                    </select>
                </td>
                </tr>
                <tr>
                    <td class="field_title" >과정선택</td>
                    <td class="field_value"  colspan="3">
                        <select title="course_select" name="courseid" id="course_select" class="w_160" onchange="courseidselect()" >
                            <option value="0">과정선택</option>
                        </select>
                        <input type="hidden" id="usertype" value=""/>
                    </td>
                </tr>
                <tr>
                    <td class="field_title">아이디</td>
                    <td class="field_value"  colspan="3">
                        <input type="hidden" title="userid" name="userid" class="w_120"/>
                        <input type="hidden" title="usertypecode" name="usertypecode" class="w_120"/>
                        <input type="text" title="username" name="username" class="w_120"/>
                        <input type="button" value="아이디검색" onclick="select_user()">
                    </td>
                </tr>
                <tr>
                    <td class="field_title">이름</td>
                    <td class="field_value">
                        <input type="text" title="lastname" name="lastname" class="w_120"/>
                    </td>
                    <td class="field_title">휴대폰번호</td>
                    <td class="field_value">
                        <input type="text" title="phone" name="phone" class="w_200" placeholder="(-)를 제외하여 입력하세요"/>
                    </td>
                </tr>
                </tbody>
            </table>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" onclick="return course_create_submit_check()"/>
                <?php if (!empty($id)) { ?>
                    <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="course_delete('<?php echo $id; ?>')"/>
                <?php } ?>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_enrolment_list.php';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    /**
     * 학교검색 기능
     * @type type
     */
    $('input[name=schoolselect]').click(function () {
        var host = $(location).attr('host');
        if (host.indexOf('eduhope.net') != -1) {
            var tag = $("<div id='select_user'></div>");
            $.ajax({
                url: 'schoolsearch.php',
                method: 'POST',
                data: {},
                success: function (data) {
                    tag.html(data).dialog({
                        title: '학교검색',
                        modal: true,
                        width: 800,
                        maxHeight: getWindowSize().height - 20,
                        buttons: [{id: 'close',
                                text: '닫기',
                                disable: true,
                                click: function () {
                                    $(this).dialog("close");
                                }}],
                        open: function () {
                            var t = $(this).parent();
                            var w = $(window);
                            var s = getWindowSize();

                            var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                            if (x < 0)
                                x = 0;
                            var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                            if (y < 0)
                                y = 0;
                            t.offset({
                                top: y,
                                left: x
                            });
                        },
                        close: function () {
                            $('#select_user').remove();
                            $(this).dialog('destroy').remove()
                        }
                    }).dialog('open');
                }
            });
        } else {
            alert('본 기능은 실서버에서만 사용가능합니다.');
        }
    });
    $(document).ready(function () {
//        $('#consignment_select1').hide();
//        $('#consignment_select2').hide();
        $("#timecreated").datepicker({
            dateFormat: "yy-mm-dd",
        });
        $("#birthday").datepicker({
            dateFormat: "yy-mm-dd",
        });

        console.log($('input[name="userid"]').val());
        if ($('input[name="userid"]').val() === '') {
            $('input[name="username"]').prop('value', '');
            $('input[name="lastname"]').prop('value', '');
            $('input[name="birthday"]').prop('value', '');
            $('input[name="phone"]').prop('value', '');
        }

    });


    //neis코드 입력받으면 다음칸으로 자동포커스
    $('input[name="neis[]"]').keyup(function (e) {
        if (e.keyCode >= 48 && e.keyCode <= 90) {
            $(this).val(String.fromCharCode(e.keyCode));
            $(this).next().focus();
        } else {
            if (e.keyCode == 8) {
                $(this).prev().focus();
            }
        }
        neis_chk = false;
    });
    /**
     * 사용자 검색기능
     * @returns {undefined}
     */
    function select_user() {
        var tag = $("<div id='select_user'><input type='hidden' id='usertype' value=' " + $("#usertype").val() + " '/></div>");

        $.ajax({
            url: 'edu_enrolment_list_user.php',
            method: 'POST',
            data: {},
            success: function (data) {
                tag.html(data).dialog({
                    title: '아이디검색',
                    modal: true,
                    width: 800,
                    maxHeight: getWindowSize().height - 20,
                    buttons: [{id: 'close',
                            text: '닫기',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    open: function () {
                        var t = $(this).parent();
                        var w = $(window);
                        var s = getWindowSize();

                        var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                        if (x < 0)
                            x = 0;
                        var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                        if (y < 0)
                            y = 0;
                        t.offset({
                            top: y,
                            left: x
                        });
                    },
                    close: function () {
                        $('#select_user').remove();
                        $(this).dialog('destroy').remove();
                    }
                }).dialog('open');
            }
        });
    }
    /**
     * 기수, selectbox 선택시 
     * @param {type} type
     * @returns {undefined}
     */
    function classnumchange(type) {
        var checkyear = $("select[name=classyear]").val();
//        var classobject; // 기수대상자
//        
//        var allcheckedlength = $("input:checkbox[name='classobject']").length;
//        var checkedlength = $("input:checkbox[name='classobject']:checked").length;
//        if (checkedlength > 0 && allcheckedlength === checkedlength) {
//            classobject = 3;
//        } else{
//            classobject = $('input:checkbox[name="classobject"]:checked').val();
//        }

        var categories = null;
//        if (classobject == 1) {
        var sel1 = $('#classnum_select');
        var identid = $("select[name=classnum_select]").val();
//        } else {
//            var sel1 = $('#consignment_select');
//            var id = $("select[name=consignment_select]").val();
//        }

        $.ajax({
            url: 'edu_course_change.php',
            method: 'POST',
            dataType: 'json',
            data: {
                checkyear: checkyear,
                type: type,
//                classobject: classobject,
                identid: identid
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 'success') {
                    categories = data.categories;
                }
                console.log(categories);
                if (type == 1) { // 연도 선택
                    cata_clean_select(sel1);
//                    if (classobject == 1) {
                    classnum_add_select_options(sel1, categories);
//                    } else {
//                        schedule_add_select_options(sel1, categories);
//                    }
                } else { // 기수 선택
                    cata_clean_select($('#course_select'));
                    course_add_select_options($('#course_select'), categories);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }
    function classnum_add_select_options(sel, options) {
        $.each(options, function (i, option) {
            sel.append($('<option>', {
                value: option.id,
                text: option.classnum + '기'
            }));
        });
    }
    function schedule_add_select_options(sel, options) {
        $.each(options, function (i, option) {
            sel.append($('<option>', {
                value: option.id,
                text: option.consignmenttitle
            }));
        });
    }
    function course_add_select_options(sel, options) {
        $.each(options, function (i, option) {
            sel.append($('<option>', {
                value: option.courseid,
                text: option.coursename
            }));
        });
    }
    /**
     * 라디오버튼 val 체크
     * @returns {undefined}
     */
    function radio_check() {
        var classobject = $('input:radio[name="classobject"]:checked').val();
        var sel1 = $('#classnum_select');
        cata_clean_select(sel1);
        $("select[name=classyear]").val(0);
        $("select[name=courseid]").val(0);

    }
    /**
     * text 박스 숫자만 입력
     * @param {type} event
     * @returns {undefined|Boolean}
     */
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            return false;
    }
    /**
     * 문자열 제거 함수
     * @param {type} event
     * @returns {undefined}
     */
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }

    /**
     * 밸리데이션 체크
     * @returns {Boolean}
     */
    function course_create_submit_check() {
//        var classobject = $('input:radio[name="classobject"]:checked').val();

        if ($("select[name=classyear]").val() == '0') {
            alert("년도를 선택하세요.");
            return false;
        }
        if ($("select[name=course_select]").val() == '0') {
            alert("과정을 선택하세요.");
            return false;
        }
        if ($.trim($("input[name='username']").val()) == '') { // username을 lastname로 바꾸기
            alert("아이디를 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='lastname']").val()) == '') { // username을 lastname로 바꾸기
            alert("이름을 입력하세요.");
            return false;
        }
    }

    function courseidselect() {
        var coursename = $("#course_select").val();
        var checkyear = $("select[name=classyear]").val();
        $.ajax({
            url: 'edu_course_change.php',
            method: 'POST',
            dataType: 'json',
            data: {
                courseid: coursename,
                checkyear: checkyear,
                type: 3
            },
            success: function (data, textStatus, jqXHR) {
                 console.log(data);
                if (data.status == 'success') {
                    categories = data.categories;
                }
                console.log(categories);
                $("#usertype").attr('value',categories.classobject);
                console.log($("#usertype").val());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });
    }

</script>
<?php
include_once ('../inc/footer.php');
