<?php

require(dirname(dirname(__FILE__)) . '/config.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');


$sql = "select concat(lca.id,'_',ls.id) as id, lc.courseid, lca.userid, lca.status, lco.coursename, lc.classtype, lc.classyear, lc.classnum, lc.learningstart, lc.learningend, 
    lc.evaluationstart, lc.evaluationend, lca.price, lca.paymentstatus, u.lastname, u.username, lca.phone, lca.email ,lca.neiscode, lca.locationnumber, 
    lca.birthday, lc.gradeviewstart, lc.gradeviewend, 
    ls.type, ls.title, ls.contents  
from {lmsdata_course_applications} lca 
join {user} u on u.id = lca.userid 
join {lmsdata_class} lc on lc.id = lca.courseid 
join {lmsdata_course} lco on lco.id = lc.parentcourseid 
join {lmsdata_sendtemplate} ls on (ls.classtype = lc.classtype or ls.classtype = 0) ";
$orderby = 'order by lca.id asc';

$where = 'where ls.isused = 1 ';

$datas = $DB->get_records_sql($sql.$where.$where2.$orderby, $params2);


$values = array();

foreach($datas as $key => $data){
    $values[$key] = (array)$data;
}
$vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1,'fieldname'=>'learningstart'),'','fieldname, fieldtype');
$vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1,'fieldname'=>'learningstart'),'','fieldname, varname');

foreach($values as $data){
//    print_object($data);
    $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    
    //여기서 SMS, 메일 보내기 함수 실행
    
}
//print_object($datas);
die;