<?php 
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/phpexcel/PHPExcel.php');
require_once($CFG->libdir.'/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');

if (!is_siteadmin($USER)) {
    redirect($CFG->wwwroot);
}

$filename = 'course_info.xlsx';
$filepath = 'course_info.xlsx';

$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
    
}

$maxRow = $objWorksheet->getHighestRow();

$courseconfig = get_config('moodlecourse');
//$role = $DB->get_record('role', array('shortname' => 'teacher'), 'id, shortname');

//주제분류
$categories = array();
$categories['A'] = '교과';
$categories['B'] = '특별활동및재량';
$categories['C'] = '생활지도';
$categories['D'] = '교직역량';
$categories['E'] = '정보화';
$categories['F'] = '학교및학급운영';
$categories['G'] = '교직행정실무';
$categories['H'] = '국가정책';
$categories['I'] = '어문학';
$categories['J'] = '기타';

//개발서버, 실서버 반영 시 변경
$parentidnumber = 'oklass_regular_etc';

if(!$parent_category = $DB->get_record('course_categories', array('idnumber'=>$parentidnumber))){
    $parent = $DB->get_field('course_categories','id',array('idnumber'=>'oklass_regular'));
    $newparentcategory = new stdClass();
    $newparentcategory->name = '기타';
    $newparentcategory->idnumber = $parentidnumber;
    $newparentcategory->description = '';
    $newparentcategory->parent = $parent; // if $parent = 0, the new category will be a top-level category
    $newparentcategory->sortorder = 999;
    $parent_category = coursecat::create($newparentcategory);
}

for ($i = 2; $i <= $maxRow; $i++) {
    $coursecd  = $objWorksheet->getCell('A' . $i)->getValue();
    if(!$lmsdata = $DB->get_record('lmsdata_course', array('coursecd'=>trim($coursecd)))){
        
        $lmsdata = new stdClass();
        $lmsdata->coursecd = $objWorksheet->getCell('A' . $i)->getValue(); //코스코드
        $lmsdata->classtype  = $objWorksheet->getCell('B' . $i)->getValue(); //자율(F),직무(W)구분
        $lmsdata->coursetype = $objWorksheet->getCell('C' . $i)->getValue(); //주제분류
        $lmsdata->coursegrade = $objWorksheet->getCell('D' . $i)->getValue(); //학점
        $lmsdata->coursename = $objWorksheet->getCell('E' . $i)->getValue(); //과정명
        $lmsdata->intro = $objWorksheet->getCell('F' . $i)->getValue(); //과정개요
        $lmsdata->subjects = $objWorksheet->getCell('G' . $i)->getValue(); //학습대상
        $lmsdata->objectives = $objWorksheet->getCell('H' . $i)->getValue(); //학습목표
        $lmsdata->contents = $objWorksheet->getCell('I' . $i)->getValue(); //학습내용
        $lmsdata->week = $objWorksheet->getCell('K' . $i)->getValue(); //주
        $lmsdata->day = $objWorksheet->getCell('L' . $i)->getValue(); //일
        $lmsdata->hours = $objWorksheet->getCell('M' . $i)->getValue(); //시간
        $lmsdata->learningtime = $objWorksheet->getCell('N' . $i)->getValue(); //총학습시간
        $lmsdata->memberdiscount = $objWorksheet->getCell('Q' . $i)->getValue(); //조합원할인율
        
        //자율, 직무 구분
        $lmsdata->classtype = ($lmsdata->classtype == 'F')? '3':'1';
        if(trim($lmsdata->coursegrade) == '') $lmsdata->coursegrade = 0;
        
        if($lmsdata->coursegrade){
            if($coursegrade = $DB->get_field('lmsdata_code','id',array('code'=>$lmsdata->coursegrade, 'code_category'=>'grades'))){
                $lmsdata->coursegrade = $coursegrade;
            }
        }
        
        $lmsdata->intro = str_replace(chr(10),'<br/>',$lmsdata->intro);
        $lmsdata->subjects = str_replace(chr(10),'<br/>',$lmsdata->subjects);
        $lmsdata->objectives = str_replace(chr(10),'<br/>',$lmsdata->objectives);
        $lmsdata->contents = str_replace(chr(10),'<br/>',$lmsdata->contents);
        
        $catename = $categories[$lmsdata->coursetype];
        
        if(!$category = $DB->get_record('course_categories',array('name'=>$catename))){
            $newcategory = new stdClass();
            $newcategory->name = $catename;
            $newcategory->description = '';
            $newcategory->parent = $parent_category->id; // if $parent = 0, the new category will be a top-level category
            $newcategory->sortorder = 999;
            $category = coursecat::create($newcategory);
        }
        
        $lmsdata->coursearea = $parent_category->id;
        $lmsdata->coursetype = $category->id;        
        
        //무들 데이터
        $data = new stdClass();
        $data->requested = 1;
        $data->fullname = $lmsdata->coursename;
        $data->shortname = $lmsdata->coursename . time();
        $data->startdate = time();
        $data->summary = $lmsdata->objectives;
        $data->summaryformat = 1;
        $data->category = $category->id;
        $data->format = $courseconfig->format;
        $data->newsitems = $courseconfig->newsitems;
        $data->showgrades = $courseconfig->showgrades;
        $data->showreports = $courseconfig->showreports;
        $data->maxbytes = $courseconfig->maxbytes;
        $data->groupmode = $courseconfig->groupmode;
        $data->groupmodeforce = $courseconfig->groupmodeforce;
        $data->visible = $courseconfig->visible;
        $data->lang = $courseconfig->lang;
        $data->enablecompletion = $courseconfig->enablecompletion;
        $course = create_course($data);
        $courseid = $course->id;

        //기본블럭설정
        edu_blocks_add_default_course_blocks($course);

        $lmsdata->courseid = $course->id;
        $lmsdatacourse = $DB->insert_record('lmsdata_course', $lmsdata);
    
        echo $coursecd . '등록 완료\n';
    }else{
        
        $lmsdata->coursegrade = $objWorksheet->getCell('D' . $i)->getValue(); //학점
        if(trim($lmsdata->coursegrade) == '') $lmsdata->coursegrade = 0;
        
        $lmsdata->intro = $objWorksheet->getCell('F' . $i)->getValue(); //과정개요
        $lmsdata->subjects = $objWorksheet->getCell('G' . $i)->getValue(); //학습대상
        $lmsdata->objectives = $objWorksheet->getCell('H' . $i)->getValue(); //학습목표
        $lmsdata->contents = $objWorksheet->getCell('I' . $i)->getValue(); //학습내용
        
        $lmsdata->intro = str_replace(chr(10),'<br/>',$lmsdata->intro);
        $lmsdata->subjects = str_replace(chr(10),'<br/>',$lmsdata->subjects);
        $lmsdata->objectives = str_replace(chr(10),'<br/>',$lmsdata->objectives);
        $lmsdata->contents = str_replace(chr(10),'<br/>',$lmsdata->contents);
        
        if($lmsdata->coursegrade){
            if($coursegrade = $DB->get_field('lmsdata_code','id',array('code'=>$lmsdata->coursegrade, 'code_category'=>'grades'))){
                $lmsdata->coursegrade = $coursegrade;
            }
        }
        $DB->update_record('lmsdata_course', $lmsdata);
        
        echo $coursecd . '업데이트 \n';
    }


}

echo '전체 등록 완료';
