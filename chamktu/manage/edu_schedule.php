<?php
/**
 * 교육일정관리 리스트 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/edu_course.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classtype = optional_param('classtype', 1, PARAM_INT); //1:직무 2:위탁
$classyear = optional_param('classyear', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);

//검색용 파라미터

$sql_where[] = " classtype = :classtype ";
$params['classtype'] = $classtype;

if ($classyear) {
    $sql_where[] = " classyear = :classyear ";
    $params['classyear'] = $classyear;
}
if ($classnum) {
    $sql_where[] = " classnum = :classnum ";
    $params['classnum'] = $classnum;
}
if (!empty($searchtext)) {
    $sql_where[] = $DB->sql_like('consignmenttitle', ':consignmenttitle');
    $params['consignmenttitle'] = '%' . $searchtext . '%';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}
$orderby = ' order by id desc ';
$courses = $DB->get_records_sql("select * from {lmsdata_schedule} " . $sql_where . $orderby, $params);
$count_courses = $DB->count_records_sql("select count(*) from {lmsdata_schedule} " . $sql_where . $orderby, $params);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);

$offline = array(0 => '없음', 1 => '있음');
$class_arr = array(1=>'영재원 일정', 2=>'일반인 일정', 3=>'청소년 일정');
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo $class_arr[$classtype]; ?></h3>
        <form name="course_search" id="course_search" class="search_area" action="edu_schedule.php" method="get">
            <input type="hidden" name="page" value="1" />
            <input type="hidden" name="classtype" value="<?php echo $classtype; ?>" />
            <div style="float:left;">
                <select title="year" name="classyear" id="classyear" class="w_120" onchange="year_changed(this, 'lmsdata_class', <?php echo $classtype; ?>);">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2017, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select> 
                <?php if ($classtype == 1) { ?>
                    <select title="class" name="classnum" id="classnum" class="w_160">
                    <option value="0">기수</option>
                    <?php
                    if ($classyear) {
                        if ($classtype)
                            $classnum_where = " and classtype = $classtype";
                        if ($classyear)
                            $classnum_where .= " and classyear = $classyear";
                        $datas = $DB->get_records_sql('select classnum from {lmsdata_class} where classnum != 0 and classnum < 100 ' . $classnum_where);

                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->classnum == $classnum) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                        }
                    }
                    ?>
                </select>
                <?php } ?>
                <?php if ($classtype == 2) { ?>
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="제목을 입력하세요."  class="search-text"/>
                <?php } ?>
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>
            </div>
        </form><!--Search Area2 End-->

        <table>
            <thead>
                <tr>
                    <th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%">년도</th>
                    <?php if ($classtype == 1) { ?>
                        <th scope="row" width="10%">기수</th>
                        <th scope="row" width="15%">수강신청기간</th>
                        <th scope="row" width="15%">교육기간</th>
                        <!--<th scope="row" width="5%">교육기간</th>-->
                        <th scope="row" width="15%">과정수</th>
                        <th scope="row" width="15%">집합교육</th>
                    <?php } else { ?>
                        <th scope="row" width="40%">제목</th>
                        <th scope="row" width="20%">게시기간</th>
                        <th scope="row" width="10%">등록일</th>
                    <?php } ?>
                    <th scope="row" width="10%">관리</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    $count_course = $DB->count_records('lmsdata_class', array('classnum' => $course->classnum), 'id');
                    ?>
                    <tr>
                        <td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $course->classyear . '년' ?></td>
                        <?php if ($classtype == 1) { ?>
                            <td><?php echo $course->classnum . '기' ?></td>
                            <td><a href="<?php echo $CFG->wwwroot . '/chamktu/manage/edu_schedule.detail.php?classtype=' . $classtype . '&id=' . $course->id; ?>">
                                    <?php echo date('Y-m-d', $course->enrolmentstart) . ' ~ ' . date('Y-m-d', $course->enrolmentend) ?></a></td>
                            <td><?php echo date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend) ?></td>
                            <!--<td><?php echo ceil(($course->learningend - $course->learningstart) / 86400 / 7) . '주' ?></td>-->
                            <td><?php echo $count_course ?></td>
                            <td><?php echo $offline[$course->offlineyn]; ?></td>
                        <?php } else { ?>
                            <td><a href="<?php echo $CFG->wwwroot . '/chamktu/manage/edu_schedule.detail.php?classtype=' . $classtype . '&id=' . $course->id; ?>">
                                    <?php echo $course->consignmenttitle ?></a></td>
                            <td><?php echo date('Y-m-d', $course->postingstart) . ' ~ ' . date('Y-m-d', $course->postingend) ?></td>
                            <td><?php echo date('Y-m-d', $course->timeupdated) ?></td>

                        <?php } ?>
                        <td><input type="button" class="normal_btn" value="수정" onclick="javascript:location.href = 'edu_schedule_add.php?classtype=<?php echo $classtype ?>&id=<?php echo $course->id ?>&classnum=<?php echo $course->classnum ?>&classyear=<?php echo $course->classyear ?>';"/>
                            <input type="button" class="normal_btn" value="과정추가" onclick="schedule_course_add('<?php echo $course->id ?>')"/></td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="schedule_delete()"/>
        <div id="btn_area">            
            <div style="float:right;">
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="일정등록" onclick="javascript:location.href = 'edu_schedule_add.php?classtype=<?php echo $classtype; ?>';"/> 
            </div>
        </div>
        <?php
        print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');

        $query_string = '';
        if (!empty($excel_params)) {
            $query_array = array();
            foreach ($excel_params as $key => $value) {
                $query_array[] = urlencode($key) . '=' . urlencode($value);
            }
            $query_string = '?' . implode('&', $query_array);
        }
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->

<?php include_once ('../inc/footer.php'); ?>

<script type="text/javascript">
    /**
     * 일정삭제
     * @returns {Boolean}
     */
    function schedule_delete() {
        var course_list = [];
        if (!$(".courseid").is(":checked")) {
            alert('일정을 선택하세요.');
            return false;
        }
        $("input[name=courseid]:checked").each(function () {
            course_list.push($(this).val());
        });

        if (confirm("<?php echo get_string('deletecoursecheck'); ?>") == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/chamktu/manage/edu_class_delete.php" ?>',
                method: 'POST',
                data: {
                    scheduleids: course_list,
                    type: 2
                },
                success: function (data) {
                    document.location.href = "<?php echo $CFG->wwwroot . "/chamktu/manage/edu_schedule.php?classtype=$classtype" ?>";
                }
            });
        }
    }
    /**
    * 교육일정 과정추가

     * @param {type} id
     * @returns {undefined}     */
    function schedule_course_add(id) {
        var tag = $("<div id='course_add_dialog'></div>");
        var title = '과정선택';

        $.ajax({
            url: 'edu_schedule_course_add.php',
            method: 'POST',
            data: {
                id: id,
                classtype:<?php echo $classtype; ?>
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: title,
                    modal: true,
                    width: 800,
                    maxHeight: getWindowSize().height - 20,
                    buttons: [{id: 'save',
                            text: '추가',
                            click: function () {
                                if ($('#frm_course_select input:checkbox[name=id]:checked').length == 0) {
                                    alert('과정을 선택해주세요');
                                    return false;
                                }
                                var course_list = [];
                                $("#frm_course_select input:checkbox[name=id]:checked").each(function () {
                                    course_list.push($(this).val());
                                    var id = $(this).val();
                                });
                                document.location.href = "edu_schedule_course_sync.php?id="+id+"&classtype="+<?php echo $classtype; ?>+"&course_list="+course_list;
                            }},
                        {id: 'close',
                            text: '닫기',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    open: function () {
                        var t = $(this).parent();
                        var w = $(window);
                        var s = getWindowSize();

                        var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                        if (x < 0)
                            x = 0;
                        var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                        if (y < 0)
                            y = 0;
                        t.offset({
                            top: y,
                            left: x
                        });
                    },
                    close: function () {
                        $('#frm_course_select').remove();
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }

</script>    
