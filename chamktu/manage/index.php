<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

//$sql_select = "SELECT lca.*, lc.classyear,lc.classnum, lc.classtype, co.fullname, u.lastname ";
//$sql_from = " FROM {lmsdata_course_applications} lca "
//        . "join {lmsdata_class} lc on lc.id=lca.courseid "
//        . "join {course} co on co.id = lc.courseid "
//        . "join {user} u on u.id = lca.userid";
//$sql_select = "SELECT lca.*, lc.classyear,lc.classnum,  u.lastname, u.username, lco.coursename, ls.consignmenttitle, lc.classtype ";
$sql_select_ongoing = "SELECT lc.*, lco.coursename, lco.classtype, lco.classobject, lc.classnum, lco.coursearea, lco.coursetype, lco.roughprocess, lco.detailprocess ";
$sql_select_soon = "SELECT lc.*, lco.coursename, lco.classtype, lco.classobject, lc.classnum, lco.coursearea, lco.coursetype, lco.roughprocess, lco.detailprocess, "
	  . "(select count(*) from {lmsdata_course_applications} where courseid = lc.courseid and status = 'apply' ) as student ";

//$sql_from = " FROM {lmsdata_course_applications} lca "
//        . "JOIN {lmsdata_class} lc on lc.courseid=lca.courseid "
//        . "JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id "
//        . "JOIN {course} co on co.id = lc.courseid "
//        . "JOIN {user} u on u.id = lca.userid ";
////        . "where lco.classtype != :lcoclasstype";

$sql_from = " FROM {lmsdata_class} lc "
	  . "JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id "
	  . "JOIN {course} co on co.id = lc.courseid ";
//        . "JOIN {user} u on u.id = lca.userid "


$sql_orderby = " order by lc.timecreated desc";
$sql_where_ongoing = " where lc.learningstart < :learningstart and lc.learningend > :learningend ";
$sql_where_soon = " where lc.learningstart > :learningstart ";
//$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, 0, 5);
$page_ongoing = optional_param('ongoingpage', 1, PARAM_INT);
$page_soon = optional_param('soonpage', 1, PARAM_INT);
$page_qna = optional_param('qnapage', 1, PARAM_INT);
$page_course = optional_param('coursepage', 1, PARAM_INT);
$perpage_ongoing = optional_param('ongoingper', 6, PARAM_INT);
$perpage_soon = optional_param('soonper', 6, PARAM_INT);
$perpage_qna = optional_param('qnaper', 6, PARAM_INT);
$perpage_course = optional_param('courseper', 6, PARAM_INT);
$pageeven = optional_param('pageeven', '', PARAM_TEXT);

$courses_ongoing = $DB->get_records_sql($sql_select_ongoing . $sql_from . $sql_where_ongoing . $sql_orderby, array('learningstart' => time(), 'learningend' => time()), ($page_ongoing - 1) * $perpage_ongoing, $perpage_ongoing);
$courses_soon = $DB->get_records_sql($sql_select_soon . $sql_from . $sql_where_soon . $sql_orderby, array('learningstart' => time()), ($page_soon - 1) * $perpage_soon, $perpage_soon);
$count_courses_ongoing = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where_ongoing, array('learningstart' => time(), 'learningend' => time()));
$count_courses_soon = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where_soon, array('learningstart' => time()));



// 진도율 확인하기
//global $DB, $COURSE;

$course_info_sql = ' SELECT lcl.courseid as classcourse, lcl.classyear, lcl.classnum, lcl.learningstart, lcl.learningend, pct.id as pcontextid, lco.coursename, lco.completestandard, '
	  . '(SELECT count(*) '
	  . 'FROM {okmedia} om '
	  . 'LEFT JOIN {okmedia_track} ot ON ot.okmediaid = om.id AND ot.userid = :userid1 '
	  . 'WHERE ot.userid IS NOT NULL AND om.course = lcl.courseid) count, '
	  . '(SELECT sum(ot.progress) '
	  . 'FROM {okmedia} om '
	  . 'LEFT JOIN {okmedia_track} ot ON ot.okmediaid = om.id AND ot.userid = :userid2 '
	  . 'WHERE ot.userid IS NOT NULL AND om.course = lcl.courseid) progress '
	  . 'FROM {lmsdata_class} lcl '
	  . 'JOIN {lmsdata_course} lco ON lco.id = lcl.parentcourseid '
	  . 'JOIN {context} pct ON pct.instanceid = lco.courseid AND pct.contextlevel = :contextlevel '
	  . 'WHERE lcl.courseid= :courseid ';

//$course_info = $DB->get_record_sql($course_info_sql,array('courseid'=>$COURSE->id,'contextlevel'=>CONTEXT_COURSE, 'userid1'=>$USER->id, 'userid2'=>$USER->id));

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>

<form id="paging_oper" action="./index.php" method="get">
    <input type="hidden" name="ongoingpage" value="<?php echo $page_ongoing ?>" />
    <input type="hidden" name="soonpage" value="<?php echo $page_soon ?>" />
    <input type="hidden" name="qnapage" value="<?php echo $page_qna ?>" />
    <input type="hidden" name="coursepage" value="<?php echo $page_course ?>" />
    <input type="hidden" name="ongoingpere" value="<?php echo $perpage_ongoing ?>" />
    <input type="hidden" name="soonper" value="<?php echo $perpage_soon ?>" />
    <input type="hidden" name="qnaper" value="<?php echo $perpage_qna ?>" />
    <input type="hidden" name="courseper" value="<?php echo $perpage_course ?>" />
    <input type="hidden" name="pageeven" value="<?php echo $pageeven ?>" />
</form>

<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title" id="ongoing"><a href="../manage/edu_class.php">운영 중인 과정(<?php echo $count_courses_ongoing; ?>)</a></h3>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="20%">과정명</th>
                    <th scope="row" width="10%">기수</th>
                    <th scope="row" width="10%">시작일</th>
                    <th scope="row" width="10%">종료일</th>
                    <th scope="row" width="10%">평균진도율</th>
                </tr>
            </thead>
		<?php if ($count_courses_ongoing === 0) { ?>
  		<tr>
  		    <td colspan="5">운영 중인 과정이 없습니다.</td>
  		</tr>
		  <?php
		} else {
		  foreach ($courses_ongoing as $course) {
		    $totalModule = $DB->count_records('course_modules', array('course' => $course->courseid, 'visible' => 1));
		    ?>
    		<tr>
    		    <td><a href="<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->courseid; ?>">
				    <?php echo $course->coursename ?></a></td>
    		    <td><?php echo $course->classnum; ?></td>
    		    <td><?php echo date('Y-m-d', $course->learningstart) ?></td>
    		    <td><?php echo date('Y-m-d', $course->learningend) ?></td>
    		    <td>
				<?php
				if ($totalModule == 0) {
				  echo '-';
				} else {
				  $roleCnt = $DB->count_records_sql('SELECT COUNT(*)
                                                                   FROM {role_assignments} ra
                                                                   JOIN {context} co ON co.id = ra.contextid AND co.contextlevel = :conlevel AND co.instanceid = :courseid', array('conlevel' => CONTEXT_COURSE, 'courseid' => $course->courseid));
				  if ($roleCnt == 0) {
				    echo '-';
				  } else {
				    $completionCnt = $DB->count_records_sql('SELECT COUNT(*)
                                                                             FROM {course_modules} cm
                                                                             JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id 
                                                                             WHERE cm.course = :courseid AND cmc.completionstate = :state', array('courseid' => $course->courseid, 'state' => 1));
				    $progress = round($completionCnt / ($totalModule * $roleCnt), 2) * 100;
				    echo $progress . '%';
				  }
				}
				?>
    		    </td>
    		</tr>
    <?php
  }
}
?>    
        </table><!--Table End-->
		<?php
		if (($count_courses_ongoing / $perpage_ongoing) > 1) {
		  print_paging_navbar_script($count_courses_ongoing, $page_ongoing, $perpage_ongoing, 'javascript:cata_ongoingpage(:page);');
		}
		?>
        <h3 class="page_title" id="soon"><a href="../manage/edu_class.php">운영 예정 과정</a></h3>
        <table>
            <thead>
                <tr>
                    <th scope="row" width="20%">과정명</th>
                    <th scope="row" width="10%">기수</th>
                    <th scope="row" width="10%">시작일</th>
                    <th scope="row" width="10%">종료일</th>
                    <th scope="row" width="10%">신청자수</th>

                </tr>
            </thead>
<?php if ($count_courses_soon === 0) { ?>
  		<tr>
  		    <td colspan="5">운영 예정인 과정이 없습니다.</td>
  		</tr>
  <?php
} else {
  foreach ($courses_soon as $course) {
    ?>
    		<tr>
    		    <td><a href="<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->courseid; ?>">
    <?php echo $course->coursename; ?></a></td>
    		    <td><?php echo $course->classnum; ?></td>
    		    <td><?php echo date('Y-m-d', $course->learningstart); ?></td>
    		    <td><?php echo date('Y-m-d', $course->learningend); ?></td>
    		    <td><?php echo $course->student; ?></td>
    		</tr>
    <?php
  }
}
?>    
        </table><!--Table End-->
		<?php
		if (($count_courses_soon / $perpage_soon ) > 1) {
		  print_paging_navbar_script($count_courses_soon, $page_soon, $perpage_soon, 'javascript:cata_soonpage(:page);');
		}
		?>
	  <?php
//        $sql_select = "SELECT lg.*, ls.classyear, ls.classnum"
//                . ", (select count(id) from {lmsdata_groupapply} where groupid = lg.id ) applycount ";
//        $sql_from = " FROM {lmsdata_grouplist} lg join {lmsdata_schedule} ls on ls.id=lg.scheduleid ";
//
//        $sql_orderby = " order by lg.timecreated desc";
//        $courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, 0, 5);
//        
//        $sql_select = "SELECT lad.*, lc.parentcourseid, lc.classyear, lc.classnum, co.fullname, u.lastname, lca.id lcaid, ls.classyear lsclassyear, ls.classnum lsclassnum, ls.id lsid ";
//        $sql_from = " FROM {lmsdata_applications_delay} lad "
//                . "join {lmsdata_course_applications} lca on lca.id=lad.applicationid "
//                . "join {lmsdata_class} lc on lc.id=lca.courseid "
//                . "join {course} co on co.id = lc.courseid "
//                . "join {user} u on u.id = lca.userid "
//                . "join {lmsdata_schedule} ls on lad.scheduleid = ls.id ";
//        $sql_orderby = " order by lad.timecreated desc";
//
//        $courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, 0, 5);
//        $count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
//        $sql_select = "SELECT lca.*, lac.status as lacstatus, lac.timecreated as time, lac.refund, lac.reason ,lc.classyear,lc.classnum, co.fullname, u.lastname ";
//        $sql_from = " FROM {lmsdata_applications_cancel} lac "
//                . "join {lmsdata_course_applications} lca on lca.id=lac.applicationid "
//                . "join {lmsdata_class} lc on lc.id=lac.courseid "
//                . "join {course} co on co.id = lc.courseid "
//                . "join {user} u on u.id = lac.userid";
//
//        $sql_orderby = " order by lac.timecreated desc";
//        $courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, 0, 5);
//        $count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
//        
	  ?>

        <h3 class="page_title" style="margin-top: 20px;" id="qna"><a href="../../local/jinoboard/list.php?id=2">1:1 질문과 답변</a></h3>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <th style="width:15%;"><?php echo get_string('datecreated', 'local_lmsdata'); ?></th>
                <th><?php echo get_string('title', 'local_lmsdata'); ?></th>
                <th style="width:15%;"><?php echo get_string('author', 'local_lmsdata'); ?></th>

            </tr>
<?php
$sql = "select jc.* 
                    from {jinoboard_contents} jc
                    where jc.board = 2 
                    order by ref DESC, step ASC ";
$sql_boardcount = "select count(jc.id)  
                    from {jinoboard_contents} jc
                    where jc.board = 2 
                    order by ref DESC, step ASC ";
$contents = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'), ($page_qna - 1) * $perpage_qna, $perpage_qna);
$contents_count = $DB->count_records_sql($sql_boardcount, array('board' => $board->id, 'search' => '%' . $search . '%'), 0, $perpage_qna);

foreach ($contents as $content) {
  $list_num++;
  if ($content->step) {
    $step_left_len = $content->lev * 20;
    $step_left = 'style="margin-left:' . $step_left_len . 'px;"';
  } else {
    $step_left = '';
  }
  echo '<tr>';

  $postuser = $DB->get_record('user', array('id' => $content->userid));
  $fullname = fullname($postuser);
  $userdate = userdate($content->timecreated);
  echo "<td>" . date("Y-m-d", $content->timecreated) . "</td>";
  echo "";
  echo "<td style='text-align:left; padding-left:20px;'><a href='" . $CFG->wwwroot . "/local/jinoboard/detail_uncore.php?id=" . $content->id . "&page=1&perpage=10&list_num=" . $list_num . "&search=" . $search . "&board=2&searchfield=" . $searchfield . "' " . $step_left . ">" . $content->title . $filecheck . "</a></td>";
  echo '<td>' . $fullname . '</td>';
}

if (empty($contents)) {
  echo '<tr>
                        <td colspan="3">데이터가 없습니다.</td>
                      </tr>';
}
?>
        </table>
		<?php
		if (($contents_count / $perpage_qna) > 1) {
		  print_paging_navbar_script($contents_count, $page_qna, $perpage_qna, 'javascript:cata_qnapage(:page);');
		}
		?>


<?php
$board = $DB->get_record('jinoboard', array('type' => '5'));
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_url('/local/jinoboard/index.php');

//        $like = '';
//        if (!empty($search)) {
//            $like = "and " . $DB->sql_like('title', ':search', false);
//        }
//        $sql = "select count(jcb.id) from {jinotechboard_contents} jcb 
//join {jinotechboard} bo on jcb.board = bo.id and bo.type = 2 " . $like . " and isnotice = 0 order by ref DESC, step ASC";
//        $totalcount = $DB->count_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'));
//        $perpages = define_perpages();
?>



        <h3 class="page_title" style="margin-top: 20px;" id="course"><a href="../manage/qna.php">과정Q&amp;A</a></h3>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <th style="width:15%;"><?php echo get_string('datecreated', 'local_lmsdata'); ?></th>
                <th style="width:25%;">과정명</th>
                <th><?php echo get_string('title', 'local_lmsdata'); ?></th>
                <th style="width:15%;"><?php echo get_string('author', 'local_lmsdata'); ?></th>

            </tr>
<?php
//            $offset = 0;
//            if ($page != 0) {
//                $offset = ($page - 1) * $perpage;
//            }
//            $num = $totalcount - $offset;
$sql = "select jcb.*,co.fullname from {jinotechboard_contents} jcb 
                        join {jinotechboard} bo on jcb.board = bo.id and bo.type = 2 
                        join {lmsdata_class} lc on lc.courseid = jcb.course
                        join {lmsdata_course} lco on lc.parentcourseid = lco.id
                        join {course} co on lco.courseid = co.id " . $like . " and isnotice = 0 order by ref DESC, step ASC";
$countcourse_sql = "select count(jcb.id) 
                        from {jinotechboard_contents} jcb 
                        join {jinotechboard} bo on jcb.board = bo.id and bo.type = 2 
                        join {lmsdata_class} lc on lc.courseid = jcb.course
                        join {lmsdata_course} lco on lc.parentcourseid = lco.id
                        join {course} co on lco.courseid = co.id " . $like . " and isnotice = 0 order by ref DESC, step ASC";
$contents = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'), 0, 5);
$contents_countcourse = $DB->count_records_sql($countcourse_sql, array('board' => $board->id, 'search' => '%' . $search . '%'), ($page_course - 1) * $perpage_course, $perpage_course);

if ($contents_countcourse > 0) {
  foreach ($contents as $content) {
    echo '<tr>';
    $cm = $DB->get_record_sql("select cm.id from {modules} m join {course_modules} cm on cm.module = m.id and cm.instance = $content->board and cm.course = $content->course  ");
    $context = context_module::instance($cm->id);
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'mod_jinotechboard', 'attachment', $content->id, 'timemodified', false);
    if (count($files) > 0) {
	$filecheck = '<img src="../img/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '"  class="icon-attachment">';
    } else {
	$filecheck = "";
    }

    if ($content->step) {
	$step_left_len = $content->step * 20;
	$step_left = 'style="margin-left:' . $step_left_len . 'px;"';
    } else {
	$step_left = '';
    }


    $postuser = $DB->get_record('user', array('id' => $content->userid));
    $fullname = fullname($postuser);
    $userdate = userdate($content->timecreated);
    echo "<td>" . date("Y-m-d", $content->timecreated) . "</td>";
    echo "<td>" . $content->fullname . "</td>";
    echo "<td style='text-align:left; padding-left:20px;'><a href='qna_view.php?id=" . $content->id . "' " . $step_left . ">" . $content->title . "</a>" . $filecheck . "</td>";
    echo '<td>' . $fullname . '</td>';

    echo "</tr>";
    $contents_countcourse--;
  }
} else {
  echo "<tr><td colspan='4' align='center'>" . get_string('nodata', 'local_lmsdata') . "</td></tr>";
}
?>
        </table>
		<?php
		if (($contents_countcourse / $perpage_course) > 1) {
		  print_paging_navbar_script($contents_countcourse, $page_course, $perpage_course, 'javascript:cata_page(:page);');
		}
		?>

    </div><!--Content End-->
</div> <!--Contents End-->
<?php include_once ('../inc/footer.php'); ?>
<script>
  function cata_ongoingpage(page) {
      $('[name=ongoingpage]').val(page);
      $('[name=pageeven]').val('ongoing');
      $('#paging_oper').submit();
  }
  function cata_soonpage(page) {
      $('[name=soonpage]').val(page);
      $('[name=pageeven]').val('soon');
      $('#paging_oper').submit();
  }
  function cata_qnapage(page) {
      $('[name=qnapage]').val(page);
      $('[name=pageeven]').val('qna');
      $('#paging_oper').submit();
  }
  function cata_page(page) {
      $('[name=coursepage]').val(page);
      $('[name=pageeven]').val('course');
      $('#paging_oper').submit();
  }

  $("document").ready(function () {
      event = $('[name=pageeven]').val();
      if (event) {
          $('html,body').animate({
              scrollTop: $("#" + event).offset().top
          }, 500);
    }
  });

  function delay_apply(delayid, type) {
      if (type == 1) {
          var text = '승인하시겠습니까?';
      } else if (type == 2) {
          var text = '이월하시겠습니까?';
      } else {
          var text = '취소하시겠습니까?';
      }

      if (confirm(text) == true) {
          $.ajax({
              url: '<?php echo $CFG->wwwroot . "/chamktu/enrol/edu_delay_change.ajax.php" ?>',
              method: 'POST',
              data: {
                  delayid: delayid,
                  type: type
              },
              success: function (data) {
                  if (data.status == 'success') {
                      location.reload();
                  }
              }
          });
      }
  }
  function delay_change(id) {
      var tag = $("<div id='cashbill'></div>");
      var title = '수강이월';

      $.ajax({
          url: 'edu_delay_change.php',
          method: 'POST',
          data: {
              id: id,
              page: '<?php echo $currpage; ?>'
          },
          success: function (data) {
              tag.html(data).dialog({
                  title: title,
                  modal: true,
                  width: 800,
                  maxHeight: getWindowSize().height - 20,
                  buttons: [
                      {id: 'save',
                          text: '이월',
                          click: function () {
                              if (confirm("이월하시겠습니까?") == true) {
                                  if ($('#frm_delay select[name=classid]').val() == '0') {
                                      alert('기수를 선택해주세요');
                                      return false;
                                  }
                                  $('#frm_delay').submit();
                                  $(this).dialog("close");
                              }
                          }},
                      {id: 'close',
                          text: '닫기',
                          disable: true,
                          click: function () {
                              $(this).dialog("close");
                          }}],
                  open: function () {
                      var t = $(this).parent();
                      var w = $(window);
                      var s = getWindowSize();

                      var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                      if (x < 0)
                          x = 0;
                      var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                      if (y < 0)
                          y = 0;
                      t.offset({
                          top: y,
                          left: x
                      });
                  },
                  close: function () {
                      $(this).dialog('destroy').remove()
                  }
              }).dialog('open');
          }
      });
  }
  function delay_detail(id) {
      var tag = $("<div id='delay_detail'></div>");

      $.ajax({
          url: '../enrol/edu_delay_detail.php',
          method: 'POST',
          data: {
              id: id
          },
          success: function (data) {
              tag.html(data).dialog({
                  title: '연기신청 상세보기',
                  modal: true,
                  width: 800,
                  maxHeight: getWindowSize().height - 20,
                  buttons: [
                      {id: 'close',
                          text: '닫기',
                          disable: true,
                          click: function () {
                              $(this).dialog("close");
                          }}],
                  open: function () {
                      var t = $(this).parent();
                      var w = $(window);
                      var s = getWindowSize();

                      var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                      if (x < 0)
                          x = 0;
                      var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                      if (y < 0)
                          y = 0;
                      t.offset({
                          top: y,
                          left: x
                      });
                  },
                  close: function () {
                      $('#frm_course_select').remove();
                      $(this).dialog('destroy').remove()
                  }
              }).dialog('open');
          }
      });
  }
</script>