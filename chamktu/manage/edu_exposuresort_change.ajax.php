<?php

/**
 * 과정 sort 순서 변경 ajax
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
$exposuresort = required_param('exposuresort', PARAM_INT);
$id  = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$lmsdata_course = $DB->get_record('lmsdata_course',array('id'=>$id));
$lmsdata_course->exposuresort = $exposuresort;
if($DB->update_record('lmsdata_course',$lmsdata_course)){
    $returnvalue->status = 'success';            
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

?>
