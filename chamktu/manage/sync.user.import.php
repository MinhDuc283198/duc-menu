<?php
ini_set('display_errors', '1');
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/local/haksa/lib.php';
require_once $CFG->dirroot . '/chamktu/manage/synclib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/sync.user.import.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$tab = 0;

$js = array('/chamktu/manage/sync.js');
include_once ('../inc/header.php');
?>

<div id="contents">
    <?php include_once ('../inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo get_string('synchronization', 'local_lmsdata'); ?></h3>
        <p class="page_sub_title"> <?php echo get_string('msg4','local_lmsdata'); ?></p>

        <div class="content_navigation">
            <?php
            $tabs = siteadmin_get_sync_tabs();
            foreach ($tabs AS $i => $t) {
                $css_class = $t['class'];
                if ($t['page'] == 'user') {
                    $css_class .= ' ' . $css_class . '_selected';
                    $tab = $i;
                }
                echo '<a href="sync.php?tab=' . $i . '"><p class="' . $css_class . '">' . $t['text'] . '</p></a>';
            }
            ?>
        </div><!--Content Navigation End-->

        <h4 class="page_sub_title"><?php echo get_string('user_sync','local_lmsdata'); ?></h4>
        <div class="extra_information">
            <p>학사시스템에서 사용자를 동기화하는 중입니다.</p>
            <p><?php echo get_string('wait_complete','local_lmsdata'); ?></p>
        </div>

        <div class="course_imported">
            <?php
            siteadmin_println('&nbsp;');

            $count = 0;
            $timesync = time();

            $existingusers = $DB->get_records_menu('user', array('deleted' => 0), '', 'username, id');

            $count_created = 0;

            $sql_st1 = "SELECT 
                USERNAME AS username, 
                USERNAME AS password, 
                USERNAME_KOR AS user_nm,
                EMAIL AS email,
                TEL_NO AS phone1, 
                HP_NO AS phone2,

                ' ' AS idnumber,
                'manual' AS auth,
                0 AS suspended,
                0 AS maildisplay,
                1 AS mailformat,
                0 AS maildigest,
                1 AS autosubscribe,
                1 AS trackforums,
                99 AS timezone,
                ' ' AS calendartype,
                0 AS descriptiontrust,
                ' ' AS description,
                1 AS descriptionformat,
                1 AS mnethostid,
                1 AS confirmed,
                0 AS timemodified,
                ' ' AS address,

                USERNAME_ENG AS eng_name,
                USERGROUP_CD,
                USERGROUP,
                UNIV_CD AS univ_cd, 
                UNIV AS univ,
                MAJOR_CD AS major_cd, 
                MAJOR AS major,
                NATION_CD,
                0 AS b_temp,
                0 AS b_mobile,
                0 AS b_email,
                0 AS b_tel,
                0 AS b_univ,
                0 AS b_major,
                ' ' AS ehks,
                ' ' AS edhs,
                ' ' AS domain,
                ' ' AS hyhg,
                ' ' AS persg,
                ' ' AS psosok,
                SEX AS sex,	
                HYEAR AS hyear,
                EXAN_NUMB AS ex_num 
            FROM V_HAK_USER_INFO";
            
            $conn = siteadmin_sync_db_connect();
            $rs = odbc_exec ($conn, $sql_st1);
            // text가 잘려나오지 않도록 1 MB로 설정
            odbc_longreadlen($rs, 1048576);
            while($row = odbc_fetch_array($rs)) {
                $usernew = (object) array_change_key_case($row, CASE_LOWER);
                

                $usernew->user_nm = siteadmin_sync_encode($usernew->user_nm);
                $usernew->eng_name = siteadmin_sync_encode($usernew->eng_name);
                $usernew->usergroup = siteadmin_sync_encode($usernew->usergroup);
//                echo '<pre>';
//                                print_r($usernew);
//                                echo '</pre>';
//                die();
                if($usernew->usergroup == '학부생'){
                    $usernew->usergroup = 'rs';
                } else if ($usernew->usergroup == '직원'){
                    $usernew->usergroup = 'pr';
                } else if($usernew->usergroup == '교원'){
                    $usernew->usergroup = 'pr';
                } else if($usernew->usergroup == '조교'){
                    $usernew->usergroup = 'ad';
                }
                
                $usernew->univ = siteadmin_sync_encode($usernew->univ);
                $usernew->major = siteadmin_sync_encode($usernew->major);
                
                $userspnm = siteadmin_split_fullname($usernew->user_nm);
                $usernew->firstname = $userspnm['firstname'];
                $usernew->lastname = $userspnm['lastname'];
                $usernew->timemodified = $timesync;
                $usernew->timecreated = $usernew->timemodified;
                
                $usernew->email = siteadmin_sync_validate_email($usernew->email);
                $usernew->phone1 = siteadmin_sync_validate_phonenumber($usernew->phone1);
                $usernew->phone2 = siteadmin_sync_validate_phonenumber($usernew->phone2);
                if(empty($usernew->eng_name)) {
                    $usernew->eng_name = $usernew->user_nm;
                }
                
                if(empty($usernew->hyear)) {
                    $usernew->hyear = 0;
                }
                
//                $lang = 'ko';
//                if(!empty($usernew->nation_cd) && trim($usernew->nation_cd) != '410') {
//                    $lang = 'en';
//                } 
//                $usernew->lang = $lang;
                // nation_cd
                $usernew->lang = 'ko';
                
                if($usernew->username != $usernew->ex_num && $usernew->usergroup == 'rs'){
                    $ex_user = $DB->get_record('user',array('username'=>$usernew->ex_num));
                    if($ex_user){
                        $DB->update_record('user',array('id'=>$ex_user->id,'username'=>$usernew->username));
                        $existingusers[$usernew->username] = $ex_user->id;
                    }
                } 
                    //수험번호
                    $userid = local_haksa_create_user($usernew, $existingusers);                
                    if ($userid > 0) {
                        $usernew->userid = $userid;
                        siteadmin_insert_or_update_lmsuserdata($usernew);

                        $count_created += 1;
                        siteadmin_println($count_created . '. 아이디:' . $usernew->username . ', 이름:' . $usernew->firstname.' '.$usernew->lastname);
                    }
            }
            odbc_free_result($rs);
            siteadmin_sync_db_close($conn);
            
            $history = new stdClass();
            $history->timestart = $timesync;
            $history->timeend = time();
            $history->usercount = $count_created;
            $DB->insert_record('haksa_user_history', $history);
            ?>
        </div>

        <div class="extra_information">
            <p><?php echo $count_created; ?> 명의 사용자를 동기화했습니다.</p>
        </div>
        <div id="btn_area">
            <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="location.href = 'sync.php?tab=<?php echo $tab; ?>'"/>
        </div>
        <?php
        siteadmin_scroll_down();
        ?>
    </div><!--Content End-->

</div> <!--Contents End-->

<?php
include_once ('../inc/footer.php');
