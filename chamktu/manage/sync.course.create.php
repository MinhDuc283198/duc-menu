<?php
/*
  create or replace FUNCTION max_course_shortname
  RETURN NUMBER
  IS max_shortname NUMBER(11,0);
  BEGIN
  SELECT MAX(TO_NUMBER(shortname)) INTO max_shortname
  FROM m_course
  WHERE LENGTH(TRIM(TRANSLATE(shortname, ' +-.0123456789',' '))) IS NULL;

  RETURN (max_shortname);
  END max_course_shortname;
 */

ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once $CFG->dirroot . '/local/haksa/lib.php';
require_once $CFG->dirroot . '/chamktu/manage/synclib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/course_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);


$year = optional_param('year', 0, PARAM_INT);
$term = optional_param('term', 0, PARAM_INT);

if ($year == 0) {
    $year = get_config('moodle', 'haxa_year');
}
if ($term == 0) {
    $term = get_config('moodle', 'haxa_term');
}

$tab = 0;

$haksa = $DB->get_record('haksa', array('year' => $year, 'term' => $term));

$js = array('/chamktu/manage/sync.js',
    '/chamktu/js/lib/jquery.numeric.min.js');
include_once ('../inc/header.php');
?>

<div id="contents">
    <?php include_once ('../inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo get_string('synchronization', 'local_lmsdata'); ?></h3>
        <p class="page_sub_title"> <?php echo get_string('msg4','local_lmsdata'); ?></p>

        <div class="content_navigation">
            <?php
            $tabs = siteadmin_get_sync_tabs();
            foreach ($tabs AS $i => $t) {
                $css_class = $t['class'];
                if ($t['page'] == 'course') {
                    $css_class .= ' ' . $css_class . '_selected';
                    $tab = $i;
                }
                echo '<a href="sync.php?tab=' . $i . '"><p class="' . $css_class . '">' . $t['text'] . '</p></a>';
            }
            ?>
        </div><!--Content Navigation End-->

        <?php
        if ((empty($year) || empty($term)) && $tabs[$tab]['page'] != 'config') {
            end($tabs);         // move the internal pointer to the end of the array
            $key = key($tabs);
            ?>
            <div class="extra_information"><?php echo get_string('msg5','local_lmsdata'); ?></div>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="sync_goto_config('<?php echo $key; ?>')"/>
            </div>
            <?php
        } else if ($haksa == false) {
            ?>
            <div class="extra_information">
                <p>먼저 학사시스템에서 <?php echo $year; ?> <?php echo get_string('year2','local_lmsdata'); ?> <?php echo $terms[$term]; ?> 강의를 가져와야 합니다.</p>
            </div>
            <div id="btn_area">
                <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="location.href = 'sync.php?tab=<?php echo $tab; ?>&year=<?php echo $year; ?>&term=<?php echo $term; ?>'"/>
            </div>
            <?php
        } else {
            ?>
            <h4 class="page_sub_title">강의 동기화</h4>
            <?php
            $terms = siteadmin_get_terms_sync();

            $leccdstart = optional_param('leccdstart', 0, PARAM_INT);

            if ($leccdstart > 0) {
                ?>
                <div class="extra_information">
                    <p>LMS에 <?php echo $year; ?> <?php echo get_string('year2','local_lmsdata'); ?> <?php echo $terms[$term]; ?> 강의를 생성/업데이트 중입니다.</p>
                    <p><?php echo get_string('wait_complete','local_lmsdata'); ?></p>
                </div>

                <div class="course_imported">
                    <?php
                    siteadmin_flushdata();

                    $strtimestart = required_param('timestart', PARAM_RAW_TRIMMED);

                    $timestart = strtotime($strtimestart);
                    $timeend = 0;
                    $timeregstart = 0;
                    $timeregend = 0;
                    $timemodified = time();

                    $timecreatestart = time();
                    $count_created = 0;
                    $count_updated = 0;
                    $count_deleted = 0;

                    $userids = $DB->get_records_menu('user', array('deleted' => 0), '', 'username, id');

// 강의 업데이트 시작
                    // 강의 이름, 언어, 시작일 업데이트 한다.
                    $haksa_classes = $DB->get_records_sql("
SELECT cl.id,
       cl.kor_lec_name,
       cl.eng_lec_name,
       cl.courseid,
       cl.prof_cd,
       cl.gubun
FROM {haksa_class} cl
WHERE cl.courseid != 0
  AND cl.YEAR = :year
  AND cl.TERM = :term
  AND cl.DELETED = :deleted", array('year' => $year, 'term' => $term, 'deleted' => 0));

                    foreach ($haksa_classes AS $haksa_class) {
                        $mdl_class = $DB->get_record('course', array('id' => $haksa_class->courseid));
                        if ($mdl_class !== false) {
                            $mdl_class->fullname = $haksa_class->kor_lec_name;

                            /*
                             * 강의 시작/종료, 강의 등록 시작/종료는 업데이트 안하도록 변경
                             * 2015. 9. 4
                             */
                            // 시작일
                            //$mdl_class->startdate     = $timestart;

                            $DB->update_record('course', $mdl_class);

                            //// lmsdata_class 업데이트
                            $lmsdata_class = $DB->get_record('lmsdata_class', array('course' => $mdl_class->id));
                            if ($lmsdata_class) {
                                $lmsdata_class->kor_lec_name = $haksa_class->kor_lec_name;
                                $lmsdata_class->eng_lec_name = $haksa_class->eng_lec_name;
                                $lmsdata_class->bunban = $haksa_class->bb;
                                $lmsdata_class->timemodified = $timemodified;

                                $prof_cd = clean_param($haksa_class->prof_cd, PARAM_USERNAME);
                                if (isset($userids[$prof_cd])) {
                                    $lmsdata_class->prof_userid = $userids[$prof_cd];
                                }

                                $DB->update_record('lmsdata_class', $lmsdata_class);
                                
                            }

                            $count_updated++;
                            //siteadmin_println('강의(<a href="'.$CFG->wwwroot.'/course/view.php?id='.$mdl_class->id.'">'.$mdl_class->fullname.'</a>)를 업데이트했습니다.');
                        } 
//                        else {
//                            // 2015. 9. 7. 교수가 강의를 삭제한 경우 다시 생성되지 않음. UIC1808-01, UIC1804-01, UIC1804-02
//                            // 다시 생성되도록 courseid를 0로 설정
//                            $DB->set_field('haksa_class', 'courseid', 0, array('id' => $haksa_class->id));
//                        }
                    }
// 강의 업데이트 끝
// 새로운 강의 생성 시작
                    $categories = array();
                    siteadmin_get_course_categories($categories);
                    // 생성할 강의 가져오기
                    $haksa_classes_new = $DB->get_records_sql("
SELECT cl.id AS class_id,
       cl.KOR_LEC_NAME AS fullname,
       cl.SUMMARY,
       1 AS summaryformat,
       1 AS visible,
       'topics' AS FORMAT,
       16 AS numsections,
       0 AS hiddensections,
       0 AS coursedisplay,
       'oklasscampus' AS theme,
       '' AS lang,
       'gregorian' AS calendartype,
       5 AS newsitems,
       1 AS showgrades,
       0 AS showreports,
       0 AS maxbytes,
       0 AS enablecompletion,
       1 AS enrol_guest_status_0,
       0 AS groupmode,
       0 AS groupmodeforce,
       0 AS defaultgroupingid,
       '' AS role_1,
       '' AS role_2,
       '' AS role_3,
       '' AS role_4,
       '' AS role_5,
       '' AS role_6,
       '' AS role_7,
       '' AS role_8,
       0 AS id,
       cl.YEAR,
       cl.TERM,
       cl.LEC_CD,
       cl.PROF_CD,
       cl.KOR_LEC_NAME,
       cl.ENG_LEC_NAME,
       cl.UNIV_CD,
       cl.UNIV,
       cl.MAJOR_CD,
       cl.MAJOR,
       cl.BB,
       cl.HAKJUM,
       cl.GUBUN,
       0 AS timeend,
       0 AS timeregstart,
       0 AS timeregend,
       '0' AS isnonformal,
       0 AS timemodified,
       cl.CATA1,
       cl.CATA2,
       cl.CATA3
FROM {haksa_class} cl
WHERE cl.courseid = 0
  AND cl.YEAR = :year
  AND cl.TERM = :term
  AND cl.DELETED = :deleted
ORDER BY cl.CATA3, cl.KOR_LEC_NAME", array('year' => $year, 'term' => $term, 'deleted' => 0));

                    foreach ($haksa_classes_new AS $haksa_class) {
                        $haksa_class->shortname     = $leccdstart++;
                        $haksa_class->startdate = $timestart;
                        $haksa_class->timemodified = $timemodified;

                        if (empty($haksa_class->summary)) {
                            $haksa_class->summary = '';
                        }

                        $path = siteadmin_get_category_path($haksa_class);
                        $haksa_class->category = siteadmin_find_or_create_category($path, $categories);

                        $haksa_class->lang = 'ko';
                        if($haksa_class->gubun == 2) {
                            $haksa_class->lang = 'en';
                        }
                        // Create Course
                        $course = siteadmin_create_course($haksa_class);
                        
                        //new_course_create_activity_jinotechboard($course, 1);
                        //new_course_create_activity_jinotechboard($course, 2);
                        //new_course_create_activity_jinotechboard($course, 3);

                        // Update haksa_class->courseid
                        $DB->set_field('haksa_class', 'courseid', $course->id, array('id' => $haksa_class->class_id));
                        
                        // Insert lmsdata_class table
                        $lmsdata_class = new stdClass();
                        $lmsdata_class->course = $course->id;
                        $lmsdata_class->subject_id = $haksa_class->lec_cd;
                        $lmsdata_class->category = $haksa_class->category;
                        $lmsdata_class->kor_lec_name = $haksa_class->kor_lec_name;
                        $lmsdata_class->eng_lec_name = $haksa_class->eng_lec_name;
                        $lmsdata_class->prof_userid = 0;
                        $lmsdata_class->year = $haksa_class->year;
                        $lmsdata_class->term = $haksa_class->term;
                        $lmsdata_class->ohakkwa_cd = $haksa_class->major_cd;
                        $lmsdata_class->hakjum = $haksa_class->hakjum;
                        $lmsdata_class->timestart = $timestart;
                        $lmsdata_class->timeend = $timeend;
                        $lmsdata_class->timeregstart = $timeregstart;
                        $lmsdata_class->timeregend = $timeregend;
                        $lmsdata_class->isnonformal = $haksa_class->isnonformal;
                        $lmsdata_class->gubun = $haksa_class->gubun;
                        $lmsdata_class->timemodified = $haksa_class->timemodified;
                        $lmsdata_class->ohakkwa = $haksa_class->major;
                        $lmsdata_class->domain = $haksa_class->univ;
                        $lmsdata_class->bunban = $haksa_class->bb;

                        $prof_cd = clean_param($haksa_class->prof_cd, PARAM_USERNAME);
                        if (isset($userids[$prof_cd])) {
                            $lmsdata_class->prof_userid = $userids[$prof_cd];
                        } else {
                            //$userid = $useryscec->id;
                            //siteadmin_println('Could not found user: '.$conew->prof_cd.', '.$conew->shortname, $logfile);
                        }

                        $DB->insert_record('lmsdata_class', $lmsdata_class);
                        
                        // local/courselis/classes/observer.php의 course_created 함수에서 lmsdata_class 테이블에 넣는 것을 막기위해
                        // lmsdata_class 에 넣은 후 이벤트를 발생시킨다.
                        // Trigger a course created event.
                        $event = \core\event\course_created::create(array(
                                    'objectid' => $course->id,
                                    'context' => context_course::instance($course->id),
                                    'other' => array('shortname' => $course->shortname,
                                        'fullname' => $course->fullname)
                        ));
                        $event->trigger();

                        $count_created++;
                        
                        siteadmin_println('강의(<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $course->id . '">' . $course->fullname . '</a>)를 생성했습니다.');
                    }
// 새로운 강의 생성 끝
// 지워진 강의 삭제 시작
                    $deleted_classes = $DB->get_records_sql("SELECT id, courseid
                FROM {haksa_class} cl
WHERE cl.LEC_CD IS NOT NULL
  AND cl.YEAR = :year
  AND cl.TERM = :term
  AND cl.DELETED = :deleted", array('year' => $year, 'term' => $term, 'deleted' => 1));

                    foreach ($deleted_classes as $deleted_class) {
                        $course = $DB->get_record('course', array('id' => $deleted_class->courseid));
                        if ($course !== false) {
                            if (delete_course($course->id, false)) {
                                $DB->set_field('haksa_class', 'courseid', 0, array('id' => $deleted_class->id));
                                // lmsdata_class 테이블은 지우지 않아도 이벤트에 의해
                                // // local/courselis/classes/observer.php 의 course_deleted 함수에서 지워짐
                                // $DB->delete_records('lmsdata_class', array('course' => $course->id));

                                $count_deleted++;
                                siteadmin_println('강의(' . $course->fullname . ')를 삭제했습니다.');
                            }
                        }
                    }
// 지워진 강의 삭제 끝


                    fix_course_sortorder();
                    cache_helper::purge_by_event('changesincourse');

                    $timecreateend = time();

                    $haksa->timecreatecourse = $timecreatestart;
                    $DB->update_record('haksa', $haksa);
                    ?>
                </div>

                <div class="extra_information">
                    <p><?php echo $count_created; ?> 개의 강의를 생성했습니다.</p>
                    <p><?php echo $count_updated; ?> 개의 강의를 업데이트했습니다.</p>
                    <p><?php echo $count_deleted; ?> 개의 강의가 삭제되었습니다.</p>
                </div>
                <div id="btn_area">
                    <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="location.href = 'sync.php?tab=<?php echo $tab; ?>&year=<?php echo $year; ?>&term=<?php echo $term; ?>'"/>
                </div>
                <?php
                siteadmin_scroll_down();
            } else {

                $timestart = get_config('moodle', 'sync_startdate_'.$year.'_'.$term);
                $timeend = "";
                
                $leccdstart = siteadmin_sync_get_next_shortname();

                // 이전에 만든 강의가 있는지 <?php echo get_string('okay','local_lmsdata'); 
                if($timestart == '1970-01-01' || empty($timestart)) {
                    if ($maxid = $DB->get_field_sql('SELECT MAX(id) FROM {lmsdata_class} WHERE year = :year AND term = :term', array('year' => $year, 'term' => $term))) {
                        $class = $DB->get_record('lmsdata_class', array('id' => $maxid));
                        $timestart = strftime('%Y-%m-%d', $class->timestart);
                        $timeend = strftime('%Y-%m-%d', $class->timeend);
                    }
                }
                ?>
                <div class="extra_information">
                    <p>LMS에 <?php echo $year; ?> <?php echo get_string('year2','local_lmsdata'); ?> <?php echo $terms[$term]; ?> 강의를 생성/업데이트 합니다.</p>
                    <p>학사시스템에서 지워진 강의는 삭제됩니다.</p>
                </div>

                <form id="sync_course_create" class="sync_area" method="POST">
                    <input type="hidden" name="tab" value="<?php echo $tab; ?>" />
                    <input type="hidden" name="year" value="<?php echo $year; ?>" />
                    <input type="hidden" name="term" value="<?php echo $term; ?>" />
                    <input type="hidden" name="leccdstart" value="<?php echo $leccdstart; ?>" />
                    <label style="margin-right: 60px;"><font color="#F00A0D" size="3px;"><strong>*</strong></font> 강의 시작일</label>
                    <input type="text" name="timestart" id="timestart" placeholder="YYYY-MM-DD" value="<?php echo $timestart; ?>" /> <br/>
                </form>

                <div id="btn_area">
                    <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="생성" onclick="sync_course_create_submit()"/>
                    <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('cancle','local_lmsdata'); ?>" onclick="location.href = 'sync.php?tab=<?php echo $tab; ?>&year=<?php echo $year; ?>&term=<?php echo $term; ?>'"/>
                </div>
                <?php
            }
        }
        ?>
    </div><!--Content End-->

</div> <!--Contents End-->

<script type="text/javascript">
    function sync_course_create_submit() {
        if ($.trim($("input[name='timestart']").val()) == '') {
            alert("강의 시작일을 입력하세요.");
            return false;
        }

        $('#sync_course_create').submit();
    }

    $(document).ready(function () {
        $("#timestart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#timeend").datepicker("option", "minDate", selectedDate);
            }
        });
    });
</script>

<?php
include_once ('../inc/footer.php');
