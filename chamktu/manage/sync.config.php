<h4 class="page_sub_title"><?php echo get_string('setting_currentterm','local_lmsdata'); ?></h4>
<form class="sync_area" id="frm_sync_config" method="POST">
    <label style="margin-right: 60px;"><font color="#F00A0D" size="3px;"><strong>*</strong></font><?php echo get_string('year','local_lmsdata',' '); ?></label>
    <select title="year" class="w_90" onchange="#" name="year">
        <?php
        $years = siteadmin_get_years();
        foreach($years as $v=>$y) {
            $selected = '';
            if($v == $year) {
                $selected = ' selected';
            }
            echo '<option value="'.$v.'"'.$selected.'> '. get_string('year','local_lmsdata',$y) . '</option>';
        }
        ?>
    </select>

    <br>

    <label style="margin-right: 60px;"><font color="#F00A0D" size="3px;"><strong>*</strong></font> <?php echo get_string('stats_terms','local_lmsdata'); ?></label>
    <select title="term" class="w_90" onchange="#" name="term">
        <?php 
        $terms = siteadmin_get_terms();
        foreach($terms as $v=>$t) {
            $selected = '';
            if($v == $term) {
                $selected = ' selected';
            }
            echo '<option value="'.$v.'"'.$selected.'> '.$t.'</option>';
        }
        ?>
    </select>
</form><!--Sync Area End-->

<div id="btn_area">
    <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="sync_set_config()"/> 
    <p class="ps"><?php echo get_string('msg7','local_lmsdata'); ?></p>
</div>