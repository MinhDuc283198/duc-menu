<?php
/**
 * 강사 추가 팝업 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
$ids = optional_param_array('id', array(), PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
if(!empty($searchtext)) {
    $sql_where[] = $DB->sql_like('u.lastname', ':lastname');
    $params['lastname'] = '%'.$searchtext.'%';    
}
if(!empty($sql_where)) {
    $sql_where = ' WHERE '.implode(' and ', $sql_where);
}else {
    $sql_where = '';
}
$users = $DB->get_records_sql('select * from {user} u join {lmsdata_user} lu on u.id = lu.userid '.$sql_where,$params, ($currpage-1)*$perpage, $perpage);
$count_users = $DB->count_records_sql("select count(*) from {user} u join {lmsdata_user} lu on u.id = lu.userid " . $sql_where . $orderby, $params);
$arr = array('10'=>'일반회원','20'=>'일반교사','30'=>'영재원');
?>

<div class="popup_content" id="course_teacher">
    <h2>강사 추가</h2>
    <form id="frm_course_teacher" name="frm_course_teacher" onsubmit="return false;">
            <div style="float:left;">
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="이름을 입력하세요."  class="search-text"/>
                <input type="button" class="blue_btn" value="검색" onclick="teacher_search_name();"/>
            </div>
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th width="10%">선택</th>
                <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th width="25%">아이디</th>
                <th width="40%">이름</th>
                <th width="25%">영재원구분</th>
            </tr>
            <?php
            $startnum = $count_users - (($currpage - 1) * $perpage);
            foreach($users as $user) {
                $disabled = '';
                $checked = '';
                if(in_array($user->id, $ids)) {
                    $disabled = ' disabled';
                    $checked = ' checked';
                }
                echo '<tr>';
                echo '<td><input type="checkbox" name="id" value="'.$user->id.'"'.$disabled.$checked.'/></td>';
                echo '<td>'.$startnum--.'</td>';
                echo '<td>'.$user->username.'</td>';
                echo '<td>'.$user->lastname.'<input type="hidden" name="id_'.$user->id.'_name" data-group="'.$user->usergroup.'" value="'.$user->lastname.'"/></td>';
                echo '<td>'.$arr[$user->usertypecode].'</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>        
    </form>
<?php
    print_paging_navbar_script($count_users, $currpage, $perpage, 'javascript:class_students_search(:page);',10);
?>
</div>
<script>
    /**
     * 페이지 이동 함수
     * @param {type} page
     * @returns {undefined}
     */
    function class_students_search(page) {
        var js_array = <?php echo json_encode($ids)?>;
        $.ajax({
            url: 'edu_teacher_select.php',
            method: 'POST',
            data: { 
                'id' : js_array,
                'page': page,
                'searchtext': $('#frm_course_teacher').find('input[name=searchtext]').val(),
            },
            success: function(data) {
                $("#course_teacher").parent().html(data);
            },
            error: function(jqXHR, textStatus, errorThrown ) {
        //console.log(jqXHR.responseText);
            }
        });
    }
    /**
     * 검색 결과 리턴
     * @returns {undefined}
     */
    function teacher_search_name() {
        $.ajax({
            url: 'edu_teacher_select.php',
            method: 'POST',
            data: { 
                'ids' : <?php echo $ids; ?>,
                'page': <?php echo $currpage; ?>,
                'searchtext': $('#frm_course_teacher').find('input[name=searchtext]').val()
            },
            success: function(data) {
                $("#course_teacher").parent().html(data);
            },
            error: function(jqXHR, textStatus, errorThrown ) {
        //console.log(jqXHR.responseText);
            }
        });
    }
    $("input[name=searchtext]").keypress(function(e) { 
        if (e.keyCode == 13){
            teacher_search_name();
        }    
    });
</script>   