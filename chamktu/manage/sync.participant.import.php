<?php
//ini_set('display_errors', '1');
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/local/haksa/lib.php';
require_once $CFG->dirroot . '/chamktu/manage/synclib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/sync.participant.import.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);


$year = optional_param('year', 0, PARAM_INT);
$term = optional_param('term', 0, PARAM_INT);

if ($year == 0) {
    $year = get_config('moodle', 'haxa_year');
}
if ($term == 0) {
    $term = get_config('moodle', 'haxa_term');
}

$tab = 0;

$haksa = $DB->get_record('haksa', array('year' => $year, 'term' => $term));
if ($haksa == false) {
    $haksa = new stdClass();
    $haksa->year = $year;
    $haksa->term = $term;
    $haksa->timesynccourse = 0;
    $haksa->timesyncparticipant = 0;
    $haksa->timecreatecourse = 0;
    $haksa->timeassignparticipant = 0;

    $haksa->id = $DB->insert_record('haksa', $haksa);
}

$js = array('/chamktu/manage/sync.js');
include_once ('../inc/header.php');
?>

<div id="contents">
    <?php include_once ('../inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo get_string('synchronization', 'local_lmsdata'); ?></h3>
        <p class="page_sub_title"> <?php echo get_string('msg4','local_lmsdata'); ?></p>

        <div class="content_navigation">
            <?php
            $tabs = siteadmin_get_sync_tabs();
            foreach ($tabs AS $i => $t) {
                $css_class = $t['class'];
                if ($t['page'] == 'participant') {
                    $css_class .= ' ' . $css_class . '_selected';
                    $tab = $i;
                }
                echo '<a href="sync.php?tab=' . $i . '"><p class="' . $css_class . '">' . $t['text'] . '</p></a>';
            }
            ?>
        </div><!--Content Navigation End-->

        <?php
        if ((empty($year) || empty($term)) && $tabs[$tab]['page'] != 'config') {
            end($tabs);         // move the internal pointer to the end of the array
            $key = key($tabs);
            ?>
            <div class="extra_information"><?php echo get_string('msg5','local_lmsdata'); ?></div>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="sync_goto_config('<?php echo $key; ?>')"/>
            </div>
            <?php
        } else {
            $terms = siteadmin_get_terms_sync();
            ?>
            <h4 class="page_sub_title">강의 참여자 동기화</h4>
            <div class="extra_information">
                <p>학사시스템에서 <?php echo $year; ?> <?php echo get_string('year2','local_lmsdata'); ?> <?php echo $terms[$term]; ?> 강의 참여자를 가져오는 중입니다.</p>
                <p><?php echo get_string('wait_complete','local_lmsdata'); ?></p>
            </div>

            <div class="course_imported">
                <?php
                siteadmin_flushdata();

                $param = array('year'=>$year, 'term'=>$term);
                
                $sql_existingprofs = "SELECT p.id,
                    CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(p.UNIV_CD, '-'), p.MAJOR_CD), '-'), p.LEC_CD), '-'), p.BB), '-'), p.prof_cd) AS prof_key
            FROM {haksa_class_professor} p
            WHERE p.year = :year
              AND p.term = :term";

                // 이전에 가져온 교수 데이터
                $profs = $DB->get_records_sql_menu($sql_existingprofs, array('year' => $year, 'term' => $term));

                $sql_prof = "SELECT 
                                YEAR,
                                TERM,
                                TRIM(UNIV_CD) AS UNIV_CD,
                                UNIV,
                                TRIM(MAJOR_CD) AS MAJOR_CD,
                                MAJOR,
                                TRIM(LEC_CD) AS LEC_CD,
                                TRIM(BB) AS BB,
                                PROF_CD,
                                PROF_NM AS PROF_NAME,
                                DELETED
                            FROM V_HAK_PROF_INFO 
                            WHERE YEAR = :year 
                              AND TERM = :term";
                
                // 교수
                $conn = siteadmin_sync_db_connect();
                $rs = odbc_prepare ($conn, $sql_prof);
                // article_text가 잘려나오지 않도록 1M로 설정
                odbc_longreadlen($rs, 1048576);
                $success = odbc_execute($rs, $param);
                if($success) {
                    $count_prof = 0;
                    $count_prof_deleted = 0;
                    while ($row = odbc_fetch_array($rs)) {
                        $prof = (object) array_change_key_case($row, CASE_LOWER);
                        // 비어있는 것들은 무시
                        if(empty($prof->univ_cd) || empty($prof->major_cd) || empty($prof->lec_cd) || empty($prof->bb)) {
                            continue;
                        }
                        
                        $prof->prof_name = siteadmin_sync_encode($prof->prof_name);
                        $prof->univ = siteadmin_sync_encode($prof->univ);
                        $prof->major = siteadmin_sync_encode($prof->major);

                        // 없는 경우에만 넣어야 함.
                        $prof_key = $prof->univ_cd.'-'.$prof->major_cd.'-'.$prof->lec_cd.'-'.$prof->bb.'-'.$prof->prof_cd;
                        if (in_array($prof_key, $profs)) {
                            $prof_id = array_search($prof_key, $profs);
                            if ($record = $DB->get_record('haksa_class_professor', array('id' => $prof_id))) {
                                $prof->id = $record->id;
                                $prof->haksa = $haksa->id;
                                $DB->update_record('haksa_class_professor', $prof);
                            }
                            unset($profs[$prof_id]);
                        } else {
                            $prof->haksa = $haksa->id;
                            $DB->insert_record('haksa_class_professor', $prof);

                            $count_prof += 1;

                            siteadmin_println('교수 추가(강의코드:' .$prof->univ_cd.'-'.$prof->major_cd.'-'. $prof->lec_cd .'-'.$prof->bb. ', 교번:' . $prof->prof_cd . ', 이름:' . $prof->prof_name . ')');
                        }
                    }
                    odbc_free_result($rs);
                    siteadmin_sync_db_close($conn);
                    
                    siteadmin_println('');
                    
                    // 삭제된 교수 처리
                    foreach ($profs as $deleted_prof_id => $prof_key) {
                        if ($deleted_prof = $DB->get_record('haksa_class_professor', array('id' => $deleted_prof_id))) {
                            // deleted = 1 로 만듬.
                            if ($deleted_prof->deleted == 0) {
                                $deleted_prof->deleted = 1;
                                $DB->update_record('haksa_class_professor', $deleted_prof);

                                $count_prof_deleted += 1;

                                siteadmin_println('교수 삭제(강의코드:' . $deleted_prof->univ_cd.'-'.$deleted_prof->major_cd.'-'. $deleted_prof->lec_cd .'-'.$deleted_prof->bb . ', 교번:' . $deleted_prof->prof_cd . ', 이름:' . $deleted_prof->prof_name . ')');
                            }
                        }
                    }
                    siteadmin_println('');
                } else {
                    siteadmin_println('Error(Professor): '.odbc_errormsg($conn));
                }

                
                

                $sql_existingstuds = "SELECT s.id, 
                    CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(s.UNIV_CD, '-'), s.MAJOR_CD), '-'), s.LEC_CD), '-'), s.BB), '-'), s.hakbun) AS stud_key
                                        FROM {haksa_class_student} s
                                        WHERE s.year = :year
                                          AND s.term = :term ";

                // 이전에 가져온 학생 데이터
                $studs = $DB->get_records_sql_menu($sql_existingstuds, array('year' => $year, 'term' => $term));

                $sql_stud = "SELECT 
                                YEAR,
                                TERM,
                                TRIM(UNIV_CD) AS UNIV_CD,
                                UNIV,
                                TRIM(MAJOR_CD) AS MAJOR_CD,
                                MAJOR,
                                TRIM(LEC_CD) AS LEC_CD,
                                TRIM(BB) AS BB,
                                HAKBUN,
                                USER_NM AS NAME,
                                DELETED
                            FROM V_HAK_STUD_INFO 
                            WHERE YEAR = :year
                              AND TERM = :term";
                
                // 학생
                $conn = siteadmin_sync_db_connect();
                $rs = odbc_prepare ($conn, $sql_stud);
                // article_text가 잘려나오지 않도록 1M로 설정
                odbc_longreadlen($rs, 1048576);
                $success = odbc_execute($rs, $param);
                if($success) {
                    $count_stud = 0;
                    $count_stud_deleted = 0;
                    while ($row = odbc_fetch_array($rs)) {
                        $stud = (object) array_change_key_case($row, CASE_LOWER);
                        // 비어있는 것들은 무시
                        if(empty($stud->univ_cd) || empty($stud->major_cd) || empty($stud->lec_cd) || empty($stud->bb)) {
                            continue;
                        }
                        
                        $stud->name = siteadmin_sync_encode($stud->name);
                        $stud->univ = siteadmin_sync_encode($stud->univ);
                        $stud->major = siteadmin_sync_encode($stud->major);

                        // 없는 경우에만 넣어야 함.
                        $stud_key = $stud->univ_cd.'-'.$stud->major_cd.'-'.$stud->lec_cd.'-'.$stud->bb.'-'.$stud->hakbun;
                        if (in_array($stud_key, $studs)) {
                            $stud_id = array_search($stud_key, $studs);
                            if ($record = $DB->get_record('haksa_class_student', array('id' => $stud_id))) {
                                $stud->id = $record->id;
                                $stud->haksa = $haksa->id;
                            }
                            
                            unset($studs[$stud_id]);
                        } else {
                            $stud->haksa = $haksa->id;
                            $DB->insert_record('haksa_class_student', $stud);

                            $count_stud += 1;

                            siteadmin_println('학생 추가(강의코드:' . $stud->univ_cd.'-'.$stud->major_cd.'-'.$stud->lec_cd.'-'.$stud->bb . ', 학번:' . $stud->hakbun . ', 이름:' . $stud->name . ')');
                        }
                    }
                    odbc_free_result($rs);
                    siteadmin_sync_db_close($conn);
                    
                    siteadmin_println('');

                    // 삭제된 학생 처리
                    foreach ($studs as $deleted_stud_id => $stud_key) {
                        if ($deleted_stud = $DB->get_record('haksa_class_student', array('id' => $deleted_stud_id))) {
                            // deleted = 1 로 만듬.
                            if ($deleted_stud->deleted == 0) {
                                $deleted_stud->deleted = 1;
                                $DB->update_record('haksa_class_student', $deleted_stud);

                                $count_stud_deleted += 1;

                                siteadmin_println('학생 삭제(강의코드:' . $deleted_stud->univ_cd.'-'.$deleted_stud->major_cd.'-'.$deleted_stud->lec_cd.'-'.$deleted_stud->bb . ', 학번:' . $deleted_stud->hakbun . ', 이름:' . $deleted_stud->name . ')');
                            }
                        }
                    }
                } else {
                    siteadmin_println('Error(Student): '.odbc_errormsg($conn));
                }

                $haksa->timesyncparticipant = time();
                $DB->update_record('haksa', $haksa);
                ?>
            </div>

            <div class="extra_information">
                <p><?php echo $count_prof; ?> 명의 교수, <?php echo $count_stud; ?> 명의 학생 참여자를 가져왔습니다.</p>
                <p><?php echo $count_prof_deleted; ?> 명의 교수, <?php echo $count_stud_deleted; ?> 명의 학생 참여자가 삭제되었습니다.</p>
            </div>
            <div id="btn_area">
                <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>" onclick="location.href = 'sync.php?tab=<?php echo $tab; ?>&year=<?php echo $year; ?>&term=<?php echo $term; ?>'"/>
            </div>
            <?php
            siteadmin_scroll_down();
        }
        ?>
    </div><!--Content End-->

</div> <!--Contents End-->

<?php
include_once ('../inc/footer.php');
