<?php
/**
 * 교육일정 등록 submit 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');

$lmsdata = new stdClass();
$classtype = required_param('classtype', PARAM_INT);

foreach ($_REQUEST as $key => $val){
    $$key = $val;
    
    if(strpos($key, 'start')){
        $lmsdata->$key = strtotime($val);
    }else if(strpos($key, 'end')){
        $lmsdata->$key = strtotime($val) + 86399;
    }else{
        $lmsdata->$key = $val;
    }
}
if($classtype == 2){
    $lmsdata->classnum = '';  
}
$lmsdata->timeupdated = time();
$lmsdata->classtype = $classtype;
$lmsdata->logo1 = $_FILES['logo1']['name'];
$lmsdata->logo2 = $_FILES['logo2']['name'];
$courseconfig = get_config('moodlecourse');

if(!$id) {
    $lmsdata->timecreated = time();

    $scheduleid = $DB->insert_record('lmsdata_schedule', $lmsdata);    
}else{
    $lmsdata_classes = $DB->get_records('lmsdata_class', array('scheduleid'=>$id));
    foreach ($lmsdata_classes as $lmsdata_class) {
        $lmsdata->id = $lmsdata_class->id;
        $DB->update_record('lmsdata_class', $lmsdata);
    }
    $lmsdata->id = $id;
    $scheduleid = $id;
    $DB->update_record('lmsdata_schedule', $lmsdata);
    $lmsdata_schedule = $DB->get_record('lmsdata_schedule',array('id'=>$id));
}

$context = context_system::instance();

if($classtype == 1){
    edu_courselist_file_upload($file_del, $context->id, 'bannerimg', $id, $scheduleid);
}else{
    edu_courselist_file_upload($file_del_1, $context->id, 'logo1', $id, $scheduleid);
    edu_courselist_file_upload($file_del_2, $context->id, 'logo2', $id, $scheduleid);
}
edu_courselist_file_upload($file_del, $context->id, 'certificateimg', $id, $scheduleid);
    
if($id){
    redirect($CFG->wwwroot . "/chamktu/manage/edu_schedule_add.php?classtype=$classtype&id=$id&classnum=$lmsdata_schedule->classnum&classyear=$lmsdata_schedule->classyear");
}else{
    redirect($CFG->wwwroot . '/chamktu/manage/edu_schedule.php?classtype='.$classtype);
}
