<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');


$year = required_param('year', PARAM_INT); 
$term = required_param('term', PARAM_INT); 

set_config('haxa_year', $year);
set_config('haxa_term', $term);

echo '현재학기 설정을 완료했습니다.';