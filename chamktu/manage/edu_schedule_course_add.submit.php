<?php
/**
 * 교육일정 과정추가 submit 페이지
 */
ini_set('display_errors', '1');
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');

$CFG->debug = (E_ALL | E_STRICT);
$CFG->debugdisplay = 1;
error_reporting($CFG->debug);
$CFG->debugdisplay = 1;

$lmsdata = new stdClass();
$returnvalue = new stdClass();

foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}
$success = true;

foreach ($course_list as $course_lists) {
    $count++;
    
    $lmsdata_schedule = $DB->get_record('lmsdata_schedule', array('id' => $id));
    $lmsdata_course = $DB->get_record('lmsdata_course', array('id' => $course_lists));

    $data = new stdClass();
    $data = $DB->get_record('course', array('id' => $lmsdata_course->courseid));
    $data->fullname = $data->fullname . ' ' . $lmsdata_schedule->classyear . '년 ' . $lmsdata_schedule->classnum . '기';
    $data->shortname = '[class_' . $data->fullname . ']' . time();
    $course = create_course($data);

    if ($course) {
        //템플릿복사        
        if ($lmsdata_course) {
            edu_import_template($course->id, $lmsdata_course->courseid);
        }
        //강사정보 복사
        edu_import_teacher($course->id, $lmsdata_course->courseid);        
       
    }else{
        $success = false;
    }
    if($success) {
        $lmsdata_schedule->courseid = $course->id;
        $lmsdata_schedule->parentcourseid = $course_lists;
        $lmsdata_schedule->scheduleid = $id;
        $lmsdata_schedule->timecreated = time();

        $classid = $DB->insert_record('lmsdata_class', $lmsdata_schedule);
        $returnvalue->status = 'success';
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
