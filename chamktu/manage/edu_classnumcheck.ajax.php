<?php

/**
 * 차수 체크 ajax후 다음차수를 반환
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
$classyear = optional_param('classyear', 0, PARAM_INT);
$classnum  = optional_param('classnum', 0, PARAM_INT);
$type  = optional_param('type', 0, PARAM_INT);
$classtype  = optional_param('classtype', 0, PARAM_INT);

$returnvalue = new stdClass();
if($type == 1){
    $classnum = $DB->get_record_sql('select * from {lmsdata_schedule} where classyear =:classyear and classtype = :classtype order by classnum desc',array('classyear'=>$classyear, 'classtype'=>$classtype));
    $returnvalue->status = 'success';
    $returnvalue->classnum = $classnum->classnum+1;
}else{
    if($DB->get_record('lmsdata_schedule',array('classyear'=>$classyear, 'classnum'=>$classnum, 'classtype'=>$classtype))){
        $returnvalue->status = 'false';
    }else{
        $returnvalue->status = 'success';
    }
}


@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

?>
