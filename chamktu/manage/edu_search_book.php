<?php
/**
 * 추천도서 팝업 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$ids = optional_param_array('id', array(), PARAM_INT);

$books = $DB->get_records('lmsdata_rebooks', array(),'','*');
?>

<div class="popup_content">
    <h2>도서 추가</h2>
    <form id="frm_course_teacher" name="frm_course_teacher" action="edu_search_book.php" method="POST" onsubmit="return false;">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th>도서명</th>
                <th>저자</th>
                <th>출판사</th>
                <th>판매가</th>                
            </tr>
            <?php
            foreach($books as $book) {
                $disabled = '';
                $checked = '';
                if(in_array($book->id, $ids)) {
                    $disabled = ' disabled';
                    $checked = ' checked';
                }
                echo '<tr>';
                echo '<td><input type="checkbox" name="id" value="'.$book->id.'"'.$disabled.$checked.'/></td>';
                echo '<td>'.$book->name.'<input type="hidden" name="id_'.$book->id.'_name" value="'.$book->name.'"/></td>';
                echo '<td>'.$book->author.'</td>';
                echo '<td>'.$book->publisher.'</td>';
                echo '<td>'.$book->price.'</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </form>
</div>
