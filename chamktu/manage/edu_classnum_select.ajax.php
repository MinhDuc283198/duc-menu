<?php
/**
 * 정기연수 위탁연수 차수 반환 
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$year = required_param('year', PARAM_INT);
$type = required_param('type', PARAM_INT);
$classtype = required_param('classtype', PARAM_INT);

$returnvalue = new stdClass();

    if($type == 1){
        $catagories = $DB->get_records('lmsdata_schedule', array('classtype'=>$classtype, 'classyear'=>$year), 'classnum', '*');
        $returnvalue->status = 'success';
        foreach ($catagories as $catagory){
            if($classtype == 1){
                $catagory->value = $catagory->classnum;
                $catagory->text = $catagory->classnum.'기';
            }else{
                $catagory->value = $catagory->id;
                $catagory->text = $catagory->consignmenttitle;
            }
        }
        $returnvalue->categories = $catagories;
    }else if($type == 2){
        $date = new stdClass();
        $classnum = required_param('classnum', PARAM_INT);
        $catagories = $DB->get_record('lmsdata_schedule', array('classtype'=>$classtype, 'classyear'=>$year, 'classnum'=>$classnum));        
        
        foreach ($catagories as $key => $val){         
            if(strpos($key, 'start') || strpos($key, 'end') ){
                $date->$key = date('Y-m-d', $val);
            }
            if($key =="id"){
                $date->schedule = $val;
            }
        }
        $returnvalue->status = 'success';
        $returnvalue->categories = $date;
    }

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);