<?php

/**
 * 과정 인기, 추천 변경 ajax
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$courseids  = required_param_array('courseid', PARAM_INT);
$type  = required_param('type', PARAM_INT);

$returnvalue = new stdClass();

foreach ($courseids as $courseid){
    if($type == 1){    
        if($course = $DB->get_record_sql('select * from {lmsdata_course} where id = :id and mainexposure like :mainexposure',array('id'=>$courseid, 'mainexposure'=>'%'.$mainexposure.'%'))){
            $returnvalue->status = 'false';
        }else{
            $course = $DB->get_record('lmsdata_course',array('id'=>$courseid));
            $course->mainexposure = $course->mainexposure.",$mainexposure";
            if($DB->update_record('lmsdata_course',$course)){
                $returnvalue->status = 'success';            
            }
        }
    }else {
        $course = $DB->get_record('lmsdata_course',array('id'=>$courseid));
        $explode = explode(',', $course->mainexposure);        
        unset($explode[array_search($mainexposure, $explode)]);
        $course->mainexposure = implode(',', $explode);
        if($DB->update_record('lmsdata_course',$course)){
            $returnvalue->status = 'success';            
        }
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

?>
