<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$type = required_param('type', PARAM_INT);

$returnvalue = new stdClass();

if ($type == 1) {
    $classyear = required_param('classyear', PARAM_INT);
    $classnum = required_param('classnum', PARAM_INT);

    $getcoursenamesql = 'select lco.id, lco.coursename, lc.classnum '
            . 'from {lmsdata_course} lco '
            . 'join {lmsdata_class} lc on lco.id = lc.parentcourseid '
            . 'where isused != 1 and lc.classyear = :lcclassyear and lc.classnum = :lcclassnum';
    $catagories = $DB->get_records_sql($getcoursenamesql, array('lcclassyear' => $classyear, 'lcclassnum' => $classnum));
    if ($catagories) {
        $returnvalue->status = 'success';
        $returnvalue->categories = $catagories;
    } else {
        $returnvalue->status = 'error';
        $returnvalue->message = get_string('empty_case', 'local_lmsdata');
    }
} else {
    $classyear = required_param('classyear', PARAM_INT);
    $classtype = required_param('classtype', PARAM_INT);
    $classnum = required_param('classnum', PARAM_INT);

    $getcoursenamesql = 'select lco.id, lco.coursename, lc.classnum '
            . 'from {lmsdata_course} lco '
            . 'join {lmsdata_class} lc on lco.id = lc.parentcourseid '
            . 'where isused != 1 and lc.classyear = :lcclassyear and (lco.classtype = :lcoclasstype or lco.classobject = :lcoclassobject) and lc.classnum = :lcclassnum';
    $catagories = $DB->get_records_sql($getcoursenamesql, array('lcclassyear' => $classyear, 'lcoclasstype' => $classtype, 'lcoclassobject' => $classtype, 'lcclassnum' => $classnum));
    if ($catagories) {
        $returnvalue->status = 'success';
        $returnvalue->categories = $catagories;
    } else {
        $returnvalue->status = 'error';
        $returnvalue->message = get_string('empty_case', 'local_lmsdata');
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
?>