<?php
/**
 * 기수 생성 및 수정 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
//require_once $CFG->dirroot.'/local/courselist/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/course_list_add.php');
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT); // lmsdata_class->id
$mod = optional_param('mod', '', PARAM_RAW);


// lmsdata가 없어서 임시로
$content_lmsdata = $DB->get_records('lmsdata_class', array());

$course = '';
$coursecategories = $DB->get_record('course_categories', array('idnumber' => 'certification_course'));

// lmsdata가 없어서 임시로
//if (!empty($content_lmsdata)) {
if (!empty($id)) {
    $course_sql = " SELECT co.id coid, co.fullname, lc.*, lco.coursename, co.category, lco.roughprocess, lco.coursetype, lco.roughprocess, lco.detailprocess, 
        ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, ca.parent caparent, ca2.parent ca2parent, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber 
                        FROM {course} co 
                        JOIN {lmsdata_class} lc ON co.id = lc.courseid                          
                        JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid 
                        JOIN {course_categories} ca on ca.id = co.category 
                        JOIN {course_categories} ca2 ON ca.parent = ca2.id 
                        WHERE lc.id = :id ";
    $params = array('id' => $id);
    $course = $DB->get_record_sql($course_sql, $params);

    $context = context_course::instance($course->coid);
//    $fileobj = edu_courselist_get_file($context->id, 'certificateimg');
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo empty($id) ? '기수등록' : '기수수정'; ?></h3>
        <div class="page_navbar"><a href="./edu_course.php"><?php echo get_string('course_management', 'local_lmsdata'); ?></a> > 
            <a href="./edu_class.php">기수관리</a> > 
            <strong><?php echo empty($id) ? '기수등록' : '기수수정'; ?></strong></div>
        <form name="" id="course_search" action="edu_class_add.submit.php" method="post" enctype="multipart/form-data">
            <?php
//            if ($mod == 'copy') {
//                $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, ca.parent caparent, ca2.parent ca2parent, lc.*                        
//                            FROM {course} co
//                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
//                            JOIN {course_categories} ca on ca.id = co.category 
//                            JOIN {course_categories} ca2 ON ca.parent = ca2.id
//                            where lc.classtype =:lcclasstype and ca2.name =:ca2name and lc.id =:icid
//                            order by lc.roughprocess asc, lc.coursetype asc";
//                $params_select_sql = array(
//                    'lcclasstype' => $course->classtype,
//                    'ca2name' => $course->occasional,
//                    'icid' => $course->coid
//                );
//
//                $course_select = $DB -> get_record_sql($course_select_sql, $params_select_sql);
//            }
            ?>
            <?php
            // lmsdata가 없어서 임시로
            if (!empty($course)) {
                echo '<input type="hidden" name="id" value="' . $id . '" />';
                echo '<input type="hidden" name="courseid" value="' . $course->courseid . '" />';
            }

            echo '<input type="hidden" id="idcerti" name="idcerti" value="' . $coursecategories->id . '" />';
            echo '<input type="hidden" name="mod" value="' . $mod . '" />';
            ?>
            <table cellpadding="0" cellspacing="0" class="detail">                
                <tbody>
                    <tr id="table_class">
                        <td class="field_title">년도 / 기수 <span class="red">*</span></td>
                        <td class="field_value" colspan="3">
                            <select title="year" name="classyear" id="classyear" class="w_160">

                                <?php
                                $yearRange = 6;
                                $currentYear = date('Y');
                                $maxYear = ($currentYear + $yearRange);
                                echo '<option value="0" ' . $disabled . '>년도선택</option>';
                                foreach (range($currentYear, $maxYear) as $year) {
                                    $selected = '';
                                    if ($year == $course->classyear) {
                                        $selected = 'selected';
                                    }
                                    echo '<option value="' . $year . '"' . $selected . '" ' . $disabled . '>' . $year . '년 </option>';
                                    $selected = '';
                                }
                                ?>
                            </select> 
                            <input type="text" title="기수선택" name="classnum" placeholder="기수선택" size="20" value="<?php echo $course->classnum; ?>"/>                            
                        </td>
                    </tr>
                    <tr id='table_courseselect1'>
                        <td class="field_title">과정선택 <span class="red">*</span></td>
                        <td class="field_value" colspan="3">
                            <?php
                            if ($mod != 'edit') {
                                if ($mod == 'copy') {
                                    ?>
                                    <input type="hidden" readonly="" name="roughprocess" value="<?php echo empty($course->roughprocess) ? $course->coursearea : $course->roughprocess; ?>"/>
                                    <input type="hidden" readonly="" name="detailprocess" value="<?php echo $course->detailprocess; ?>"/>
                                <?php } ?>

                                <select title="class" name="cata1" id="course_search_class1" onchange="selectbox_check()" class="w_1000">
                                    <option value="0">과정선택</option>
                                    <?php
                                    $catagories = $DB->get_records('course_categories', array('visible' => 1, 'parent' => 2), 'sortorder', 'id, idnumber, name');
                                    foreach ($catagories as $catagory) {
                                        $selected = '';
                                        if ($catagory->id == $course->roughprocess) {
                                            $selected = ' selected';
                                        }
                                        echo '<option value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                                    }
                                    ?>
                                </select>
                            <?php } else { ?>                            
                                <input type="hidden" readonly="" name="roughprocess" value="<?php echo empty($course->roughprocess) ? $course->coursearea : $course->roughprocess; ?>"/>
                                <input type="hidden" readonly="" name="detailprocess" value="<?php echo $course->detailprocess; ?>"/>
                                <input type="text" readonly="" name="cata1" size="100" value="<?php echo $course->fullname ?>"/>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr id='table_courseselect2' hidden="hidden">
                        <td class="field_title">세부과정선택 <span class="red">*</span></td>
                        <td class="field_value" colspan="3">
                            <?php if ($mod != 'edit') { ?>
                                <select title="class" name="catatotal2" id="course_search_class2" onchange="selectbox_check2()" class="w_1000">
                                    <option value="0">
                                        세부과정선택
                                    </option>
                                </select>
                            <?php } else { ?>                            
                                <input type="hidden" readonly="" name="detailprocess" value="<?php echo empty($course->detailprocess) ? $course->coursetype : $course->detailprocess; ?>"/>
                                <input type="text" readonly="" name="cata1" size="100" value="<?php echo $course->fullname ?>"/>
                            <?php } ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="field_title">수강신청기간 </td>
                        <td colspan="3" class="field_value">

                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="enrolmentstart" id="enrolmentstart" class="w_120" value="<?php echo $course->enrolmentstart ? date('Y-m-d', $course->enrolmentstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="enrolmentend" id="enrolmentend" class="w_120" value="<?php echo $course->enrolmentend ? date('Y-m-d', $course->enrolmentend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" id="courseptitle">교육기간 <span class="red">*</span></td>
                        <td class="field_value" id="coursepvalue">

                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="learningstart" id="learningstart" class="w_120" value="<?php echo $course->learningstart ? date('Y-m-d', $course->learningstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="learningend" id="learningend" class="w_120" value="<?php echo $course->learningend ? date('Y-m-d', $course->learningend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                        <td class="field_title">성적조회기간 <span class="red">*</span></td>
                        <td class="field_value">

                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="gradeviewstart" id="gradeviewstart" class="w_120" value="<?php echo $course->gradeviewstart ? date('Y-m-d', $course->gradeviewstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="gradeviewend" id="gradeviewend" class="w_120" value="<?php echo $course->gradeviewend ? date('Y-m-d', $course->gradeviewend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                    </tr>
                    <tr id='table_time3'>
                        <td class="field_title">평가기간 <span class="red">*</span></td>
                        <td class="field_value">

                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="evaluationstart" id="evaluationstart" class="w_120" value="<?php echo $course->evaluationstart ? date('Y-m-d', $course->evaluationstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="evaluationend" id="evaluationend" class="w_120" value="<?php echo $course->evaluationend ? date('Y-m-d', $course->evaluationend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                        <td class="field_title">이의신청기간 <span class="red">*</span></td>
                        <td class="field_value">

                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="appealstart" id="appealstart" class="w_120" value="<?php echo $course->appealstart ? date('Y-m-d', $course->appealstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" <?php echo $date_disabled; ?> name="appealend" id="appealend" class="w_120" value="<?php echo $course->appealend ? date('Y-m-d', $course->appealend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">수강제한</td>
                        <td class="field_value">
                            <?php
                            if ($course->classlimit) {
                                $check = 'checked';
                            }
                            ?>
                            <input type="checkbox" class="classlimit_check" name="classlimit_check" <?php echo $check ?> onclick="checkbox_check()"/>
                            <input type="text" title="수강제한" id="classlimit"  name="classlimit" size="10" value="<?php echo $course->classlimit; ?>" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/>
                        </td>
                        <td class="field_title">집합교육</td>                        
                        <td class="field_value">                            
                            <input type="radio" name="offlineyn" value="1" <?php if ($course->offlineyn == 1) echo 'checked'; ?>> 있음 
                            <input type="radio" name="offlineyn" value="0" <?php if ($course->offlineyn == 0) echo 'checked'; ?>> 없음                     
                        </td>
                    </tr>
                    <?php
//                    echo '<tr>
//                        <td class="field_title">이수증 배경이미지</td>
//                        <td class="field_value">
//                            echo $fileobj; 
//                            <input type="hidden" name="file_del" value="0"/>
//                            <input type="file" title="file" name="certificateimg" placeholder="" size="40" value=" /// echo $course->sampleurl; /// "/>
//                        </td>
//                        <td class="field_title">이수증 일련번호</td>                        
//                        <td class="field_value">
//                            <input type="text" title="일련번호" name="certificatepre" placeholder="" size="40" value=" /// echo $course->certificatepre; /// "/>                            
//                        </td>
//                    </tr>';
//                    
                    ?>


                </tbody>
            </table>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" onclick="return course_create_submit_check()"/>
                <?php if (!empty($id)) { ?>
                    <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="course_delete('<?php echo $id; ?>')"/>
                <?php } ?>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_class.php';"/>
            </div>

            <div id="btnidarea"></div>

        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    var sort2;
    var categorynumber;
    var ifalready = 0;
    /**
     * selectbox에 숫자키만 입력
     * @param {type} event
     * @returns {Boolean|undefined}
     */
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            return false;
    }
    /**
     * 문자열 삭제
     * @param {type} event
     * @returns {undefined}
     */
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39) {
            return;
        } else {
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
        }
    }


    /**
     * 변경된 옵션 넣어주는 함수
     * @param {type} sel
     * @param {type} options
     * @returns {undefined}
     */
    function date_value_change(sel, options) {
        $.each(options, function (i, option) {
            $("input[name='" + i + "']").val(option);
        });
    }
    function select_options(sel, options) {
        $.each(options, function (i, option) {
            if (options[i].fullname == "<?php echo $course->coursename; ?>") {
                sel.append($('<option>', {
                    value: option.value,
                    text: option.text,
                    selected: "selected"
                }));
            } else {
                sel.append($('<option>', {
                    value: option.value,
                    text: option.text
                }));
            }
        });
    }
    /**
     * 벨리데이터 체크 함수
     * @returns {Boolean}
     */
    function course_create_submit_check() {

        if ($("select[name=classyear]").val() == '0') {
            alert("년도를 선택하세요.");
            return false;
        }
        if ($("input[name=classnum]").val() == '0' || $("input[name=classnum]").val() == '' || $("input[name=classnum]").val() == undefined) {
            alert("기수를 입력하세요.");
            return false;
        }
        if ($("input[name=classnum]").val().match(/[^0-9]/)) {
            alert("기수는 숫자만 가능합니다.");
            return false;
        }
        if ($("select[name=cata1]").val() == '0' || $("select[name=catatotal2]").val() == '0') {
            alert("과정을 선택하세요.");
            return false;
        }

        if (!date_check()) {
            return false;
        }
        if (!date_able_check()) {
            return false;
        }
        selectbox_check2();
    }
    /**
     * date 박스 체크
     * @returns {Boolean}
     */
    function date_check() {
        if ($.trim($("input[name='learningstart']").val()) == '' || $.trim($("input[name='learningend']").val()) == '') {
            alert("교육기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='gradeviewstart']").val()) == '' || $.trim($("input[name='gradeviewend']").val()) == '') {
            alert("성적조회기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='evaluationstart']").val()) == '' || $.trim($("input[name='evaluationend']").val()) == '') {
            alert("평가기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='appealstart']").val()) == '' || $.trim($("input[name='appealend']").val()) == '') {
            alert("이의신청기간을 입력하세요.");
            return false;
        }
        return true;
    }
    function date_able_check() {
        var enrolmentstart = $.trim($("input[name='enrolmentstart']").val());
        var enrolmentend = $.trim($("input[name='enrolmentend']").val());
        var learningstart = $.trim($("input[name='learningstart']").val());
        var learningend = $.trim($("input[name='learningend']").val());
        var learningStime = $.trim($("select[name='learningStime']").val());
        var learningStimes = $.trim($("select[name='learningStimes']").val());
        var learningEtime = $.trim($("select[name='learningEtime']").val());
        var learningEtimes = $.trim($("select[name='learningEtimes']").val());
        var gradeviewstart = $.trim($("input[name='gradeviewstart']").val());
        var gradeviewend = $.trim($("input[name='gradeviewend']").val());
        var evaluationstart = $.trim($("input[name='evaluationstart']").val());
        var evaluationend = $.trim($("input[name='evaluationend']").val());
        var appealstart = $.trim($("input[name='appealstart']").val());
        var appealend = $.trim($("input[name='appealend']").val());
        if ((enrolmentstart != '' && enrolmentend == '') || (enrolmentstart == '' && enrolmentend != '')) {
            // 신청기간 지정을 완벽히 안한 경우
            alert("수강신청기간을 입력하세요.");
            return false;
        } else {

            // 신청기간을 제대로 입력한 경우에만 사용
            if (enrolmentstart != '' && enrolmentend != '') {
                if (learningstart > gradeviewstart) {
                    alert("성적조회기간은 교육 시작일 이전으로 설정할 수 없습니다.");
                    return false;
                }
            }

            // 공통
            if (learningstart > gradeviewstart) {
                alert("성적조회기간은 교육 시작일 이전으로 설정할 수 없습니다.");
                return false;
            } else if (learningstart > evaluationstart) {
                alert("평가기간은 교육 시작일 이전으로 설정할 수 없습니다.");
                return false;
            } else if (learningstart > appealstart) {
                alert("이의신청기간은 교육 시작일 이전으로 설정할 수 없습니다.");
                return false;
            } else if (gradeviewstart > appealend) {
                alert("이의신청기간은 성적 조회기간 이전으로 설정할 수 없습니다.");
                return false;
            } else if (evaluationstart > appealstart) {
                alert("평가기간 시작일 이전에 이의신청기간을 설정할 수 없습니다.");
                return false;
            } else if (evaluationstart > gradeviewstart) {
                alert("평가기간 시작일 이전에 성적조회기간을 설정할 수 없습니다.");
                return false;
            }
        }
        return true;
    }
    /**
     * class 삭제기능
     * @param {type} id
     * @returns {undefined}
     */
    function course_delete(id) {
        var course_list = [];
        course_list.push(id);
        if (confirm("<?php echo get_string('deletecoursecheck'); ?>") == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/chamktu/manage/edu_class_delete.php" ?>',
                method: 'POST',
                data: {
                    classids: course_list,
                    type: 1
                },
                success: function (data) {
                    document.location.href = "<?php echo $CFG->wwwroot . "/chamktu/manage/edu_class.php" ?>";
                }
            });
        }
    }


    $(document).ready(function () {
        categorynumber = $("input[name='roughprocess']").val();
        if (<?php echo ($mod == 'copy') ? 'true' : 'false'; ?>) {
            selectbox_check();
        } else {
            if (<?php echo ($mod == 'edit') ? 'true' : 'false'; ?> && (categorynumber == $("#idcerti").val())) {
                selectbox_check();
            }
            $("#table_courseselect2").hide();
        }
        var valclassyear = "<?php echo empty($course->classyear) ? '' : "$course->classyear"; ?>";
        var classsyear;
        if (valclassyear.length > 0) {
            sort2 = '';
            classsyear = "<?php echo empty($course->classyear) ? '' : $course->classyear; ?>";
            if ("<?php echo $course->occasional; ?>" != '') {
                sort2 = "<?php echo empty($course->occasional) ? '' : $course->occasional; ?>";
            } else {
                sort2 = "<?php echo empty($course->category) ? '' : $course->category; ?>";
            }
        }
        $("select[name=classyear]").prop('value', classsyear);
        if ($("#table_courseselect1").val() === 0) {
            $("#table_courseselect2").hide();
        } else {
            $('.course_search_class2 option[value=' + sort2 + ']').attr('selected', 'selected');
            $("select[name=catatotal2]").attr('value', sort2);
        }

//        $("#postingstart").datepicker({
//            dateFormat: "yy-mm-dd",
//            onClose: function (selectedDate) {
//                $("#postingend").datepicker("option", "minDate", selectedDate);
//            }
//        });
//        $("#postingend").datepicker({
//            dateFormat: "yy-mm-dd",
//            onClose: function (selectedDate) {
//                $("#postingstart").datepicker("option", "maxDate", selectedDate);
//            }
//        });
        $("#enrolmentstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#enrolmentend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#enrolmentend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#enrolmentstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#learningstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#learningend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#learningend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#learningstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#gradeviewstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#gradeviewend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#gradeviewend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#gradeviewstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#evaluationstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#evaluationend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#evaluationend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#evaluationstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#appealstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#appealend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#appealend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#appealstart").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
    /**
     * 수강제한 체크박스
     
     * @returns {undefined}         */
    function checkbox_check() {
        if ($(".classlimit_check").is(":checked")) {
            $('#classlimit').show();
        } else {
            $('#classlimit').hide();
        }
    }
    /**
     * 수강형식 및 수시여부 라디오버튼 
     
     * @returns {undefined}         */

    function radio_check() {

        if ('<?php echo $mod ?>' != 'edit') {
            $("select[name=classyear]").val(0); // 년도선택으로 돌아가는 소스부분
            selectbox_check();
        }
    }


    /**
     * 셀렉트박스 숨김/보여주기 설정
     
     * @returns {undefined}         */
    function selectbox_check() {
        if (categorynumber && categorynumber == $("#idcerti").val() && ifalready != 1) { // db 시간이 있는 경우
            var entirestart = new Date('<?php echo $course->learningstart ? date('Y-m-d H:i', $course->learningstart) : '' ?>');
            var entireend = new Date('<?php echo $course->learningstart ? date('Y-m-d H:i', $course->learningend) : '' ?>');
            var starthour = entirestart.getHours();
            var startminute = entirestart.getMinutes();
            var endhour = entireend.getHours();
            var endminute = entireend.getMinutes();
            $('#courseptitle').html('시험기간 <span class="red">*</span>');
            var starttime = '<select title="시간" <?php echo $date_disabled; ?> name="learningStime" id="learningStime" class="w_120" value="' + starthour + '" placeholder="클릭하세요">';
            for (var i = 0; i < 24; i++) {
                if (i == starthour) {
                    starttime += '<option value="' + i + '" selected>' + i + '시</option>';
                } else {
                    starttime += '<option value="' + i + '">' + i + '시</option>';
                }
            }
            starttime += '</select>';
            starttime += '<select title="분" <?php echo $date_disabled; ?> name="learningStimes" id="learningStimes" class="w_120" value="' + startminute + '" placeholder="클릭하세요">';
            for (var i = 0; i < 60; i += 10) {
                if (i == startminute) {
                    starttime += '<option value="' + i + '" selected>' + i + '분</option>';
                } else {
                    starttime += '<option value="' + i + '">' + i + '분</option>';
                }
            }
            starttime += '</select> ';
            var endtime = '<select  title="시간" <?php echo $date_disabled; ?> name="learningEtime" id="learningEtime" class="w_120" value="' + endhour + '" placeholder="클릭하세요">';
            for (var i = 0; i < 24; i++) {
                if (i == endhour) {
                    endtime += '<option  value="' + i + '" selected>' + i + '시</option>';
                } else {
                    endtime += '<option  value="' + i + '">' + i + '시</option>';
                }
            }
            endtime += '</select>';
            endtime += '<select title="분" <?php echo $date_disabled; ?> name="learningEtimes" id="learningEtimes" class="w_120" value="' + endminute + '" placeholder="클릭하세요">';
            for (var i = 0; i < 60; i += 10) {
                if (i == endminute) {
                    endtime += '<option value="' + i + '" selected>' + i + '분</option>';
                } else {
                    endtime += '<option value="' + i + '">' + i + '분</option>';
                }
            }
            endtime += '</select>';
            $('#learningstart').after(starttime);
            $('#learningend').after(endtime);
            ifalready = 1;
        } else if ($("#course_search_class1").val() == $("#idcerti").val() && ifalready != 1) {
            var entirestart = new Date('<?php echo $course->learningstart ? date('Y-m-d H:i', $course->learningstart) : '' ?>');
            var entireend = new Date('<?php echo $course->learningstart ? date('Y-m-d H:i', $course->learningend) : '' ?>');
            var starthour = entirestart.getHours();
            var startminute = entirestart.getMinutes();
            var endhour = entireend.getHours();
            var endminute = entireend.getMinutes();
            $('#courseptitle').html('시험기간 <span class="red">*</span>');
            var starttime = '<select title="시간" <?php echo $date_disabled; ?> name="learningStime" id="learningStime" class="w_120" value="' + starthour + '" placeholder="클릭하세요">';
            for (var i = 0; i < 24; i++) {
                if (i == starthour) {
                    starttime += '<option value="' + i + '" selected>' + i + '시</option>';
                } else {
                    starttime += '<option value="' + i + '">' + i + '시</option>';
                }
            }
            starttime += '</select>';
            starttime += '<select title="분" <?php echo $date_disabled; ?> name="learningStimes" id="learningStimes" class="w_120" value="' + startminute + '" placeholder="클릭하세요">';
            for (var i = 0; i < 60; i += 10) {
                if (i == startminute) {
                    starttime += '<option value="' + i + '" selected>' + i + '분</option>';
                } else {
                    starttime += '<option value="' + i + '">' + i + '분</option>';
                }
            }
            starttime += '</select> ';
            var endtime = '<select  title="시간" <?php echo $date_disabled; ?> name="learningEtime" id="learningEtime" class="w_120" value="' + endhour + '" placeholder="클릭하세요">';
            for (var i = 0; i < 24; i++) {
                if (i == endhour) {
                    endtime += '<option  value="' + i + '" selected>' + i + '시</option>';
                } else {
                    endtime += '<option  value="' + i + '">' + i + '시</option>';
                }
            }
            endtime += '</select>';
            endtime += '<select title="분" <?php echo $date_disabled; ?> name="learningEtimes" id="learningEtimes" class="w_120" value="' + endminute + '" placeholder="클릭하세요">';
            for (var i = 0; i < 60; i += 10) {
                if (i == endminute) {
                    endtime += '<option value="' + i + '" selected>' + i + '분</option>';
                } else {
                    endtime += '<option value="' + i + '">' + i + '분</option>';
                }
            }
            endtime += '</select>';
            $('#learningstart').after(starttime);
            $('#learningend').after(endtime);
            ifalready = 1;
        } else {
            $('#table_courseselect2').show();
            $('#courseptitle').html('교육기간  <span class="red">*</span>');
            $('#coursepvalue').find('select').remove();
            $('#coursepvalue').find('option').remove();
            ifalready = 2;
        }

        if ($("#course_search_class1").val() == 0) {
            $('#table_courseselect2').hide();
        } else {
            $('#table_courseselect2').show();
        }
        course_select2();
        return false;
    }


    /**
     * 수강형식에 따른 과정 선택 함수
     * @returns {undefined}
     */
    function course_select2() {

        //전체 갯수
        //$("input:checkbox[name='classtype']").length;
        //선택된 갯수
        //$("input:checkbox[name='classtype']:checked").length;
        var val1;
        if ($("#course_search_class1").val()) {
            val1 = $("#course_search_class1").val();
        } else {
            val1 = categorynumber;
        }
        var classid = '<?php echo $id; ?>';
        var categories = null;
        var sel1 = $('#course_search_class2'); // 과정선택

        $.ajax({
            url: 'edu_course_select.ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                classid: classid,
                selectvalue: val1
            },
            success: function (data) {
                if (data.status == 'success') {
//                    console.log(data.optionvalue);
                    sel1.html(data.optionvalue);
                }
            },
            error: function (request, status, error) {
                console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
            }
        });
    }

    function selectbox_check2() {
        var coid = $('#course_search_class2').val();
        $("#coid").attr('value', coid);
        var val1 = $("#course_search_class1").val();
        if (!coid || !val1) {
            $("#course_search_class1").prop('value', '');
            $("#course_search_class1").prop('selected', '');
        } else {
            $.ajax({
                url: 'edu_class_add.getid.php',
                type: 'POST',
                dataType: 'html',
                data: {
                    selectvalue: val1,
                    coid: coid
                },
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    $("#btnidarea").html(data);
                },
                error: function (request, status, error) {
                    console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
                }
            });
        }
    }


</script>
<?php
include_once ('../inc/footer.php');
