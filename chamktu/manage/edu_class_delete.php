<?php

/* 기수 삭제 기능 페이지
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/chamktu/lib.php');

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$type  = required_param('type', PARAM_INT);

if($type == 1){
    $classids  = required_param_array('classids', PARAM_INT);
    foreach ($classids as $classid){
        $class = $DB->get_record('lmsdata_class', array('id' => $classid), '*', MUST_EXIST);
        $DB->delete_records('lmsdata_class', array('id'=>$classid));

        $course = $DB->get_record('course', array('id' => $class->courseid), '*', MUST_EXIST);        
        delete_course($course,FALSE);
    }
}else if($type == 2){
    $scheduleids  = required_param_array('scheduleids', PARAM_INT);
    foreach ($scheduleids as $scheduleid){
        $classes = $DB->get_records('lmsdata_class', array('scheduleid' => $scheduleid));
        foreach ($classes as $class){
            $DB->delete_records('lmsdata_class', array('id'=>$class->id));
            $course = $DB->get_record('course', array('id' => $class->courseid), '*', MUST_EXIST);
            delete_course($course, FALSE);
        }
        $DB->delete_records('lmsdata_schedule', array('id'=>$scheduleid));
    }
}