<?php

require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$selectvalue = required_param('selectvalue', PARAM_INT);
$coid = required_param('coid', PARAM_INT);

$returnvalue = '';
$course_select_sql = '';
$params = '';

if ($selectvalue == 7) {
    $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, ca.parent caparent, ca2.parent ca2parent, lc.*                        
                            FROM {course} co
                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                            JOIN {course_categories} ca on ca.id = co.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            where ca2.name ='필수과정' and lc.id =:icid
                            order by lc.roughprocess asc, lc.detailprocess asc";

    $params = array(
        'icid' => $coid
    );
} else if($selectvalue == 8) {
    $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, ca.parent caparent, ca2.parent ca2parent, lc.*                        
                            FROM {course} co
                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                            JOIN {course_categories} ca on ca.id = co.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            where ca2.name ='교과과정' and lc.id =:icid
                            order by lc.roughprocess asc, lc.detailprocess asc";

    $params = array(
        'icid' => $coid
    );
} else{
    $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, ca.parent caparent, ca2.parent ca2parent, lc.*                        
                            FROM {course} co
                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                            JOIN {course_categories} ca on ca.id = co.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            where ca.name ='자격인증' and lc.id =:icid
                            order by lc.roughprocess asc, lc.detailprocess asc";

    $params = array(
        'icid' => $coid
    );
    
}

$catagories = $DB->get_record_sql($course_select_sql, $params);

$returnvalue = '<input type="hidden" name="coid" value="'.$catagories->coid.'">
<input type="hidden" name="caparent" value="'.$catagories->caparent.'">
<input type="hidden" name="caid" value="'.$catagories->caid.'">
<input type="hidden" name="cata2" value="'.$catagories->ca2id.'">
<input type="hidden" name="fullname" value="'.$catagories->fullname.'">';

echo $returnvalue;