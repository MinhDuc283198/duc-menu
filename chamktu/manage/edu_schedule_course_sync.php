<?php
/**
 * 일정에 과정 추가시 템플릿 복사 페이지
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/classes/template_import_ui.php');

//ini_set('display_errors', '1');
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);
//ini_set('max_input_time', -1);
//ini_set('default_socket_timeout',3600);
// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/edu_schedule_course_sync.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$tab = 0;

$js = array('/chamktu/manage/sync.js');
include_once ('../inc/header.php');

foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}
$course_list = explode(',', $course_list);
?>

<div id="contents">
    <?php include_once ('../inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo $classtype == 1 ? '교육일정 과정추가' : '위탁교육일정 과정추가'; ?></h3>
        <p class="page_sub_title">강의추가시 템플릿이 복사 됩니다.</p>
        <div class="content_navigation"></div>
        <h4 class="page_sub_title">강의생성</h4>
        <div class="extra_information">
            <p>과정을 생성중입니다.</p>
            <p><?php echo get_string('wait_complete', 'local_lmsdata'); ?></p>
        </div>

        <div class="course_imported">
            <?php
            siteadmin_println('&nbsp;');

            $count = 0;
            $timesync = time();

            $count_created = '';
            foreach ($course_list as $course_lists) {
                $lmsdata_course = $DB->get_record('lmsdata_course', array('id' => $course_lists));
                $lmsdata_schedule = $DB->get_record('lmsdata_schedule', array('id' => $id));
                $lmsdata_class = $DB->get_record('lmsdata_class', array('scheduleid' => $id, 'parentcourseid' => $course_lists));

                if (!$lmsdata_class) {
                    $data = new stdClass();
                    $data = $DB->get_record('course', array('id' => $lmsdata_course->courseid));
                    if ($classtype == 1) {
                        $data->fullname = $data->fullname . ' ' . $lmsdata_schedule->classyear . '년 ' . $lmsdata_schedule->classnum . '기';
                    } else {
                        $data->fullname = $data->fullname . ' ' . $lmsdata_schedule->classyear . '년 ' . $lmsdata_schedule->consignmenttitle;
                    }

                    $data->shortname = '[class_' . $data->fullname . ']' . time();

                    siteadmin_println($data->fullname . ' 강의를 생성중입니다.');

                    //강의실 생성
                    $course = create_course($data);
                    
                    
                    $lmsdata_schedule->courseid = $course->id;
                    $lmsdata_schedule->parentcourseid = $course_lists;
                    $lmsdata_schedule->scheduleid = $id;
                    $lmsdata_schedule->templateyn = 'N';
                    $lmsdata_schedule->timecreated = time();
                    $lmsdata_schedule->timeupdated = time();

                    $classid = $DB->insert_record('lmsdata_class', $lmsdata_schedule);
                }

                //템플릿복사
                if (!$lmsdata_class || $lmsdata_class->templateyn == 'N') {
                    if (edu_import_template($course->id, $lmsdata_course->courseid)) {
                        siteadmin_println($data->fullname . ' 강의가 생성되었습니다.');
                        $count_created++;
                        if ($lmsdata_schedule->evaluationstart && $lmsdata_schedule->evaluationend) {
                            //퀴즈 데이터 날짜 변경       
                            $quizs = $DB->get_records('quiz', array('course' => $course->id));
                            foreach ($quizs as $quiz) {
                                $quiz->timeopen = $lmsdata_schedule->evaluationstart;
                                $quiz->timeclose = $lmsdata_schedule->evaluationend;
                                $DB->update_record('quiz', $quiz);
                            }
                        }
                        //게시글 복사
                        edu_import_board($course->id, $lmsdata_course->courseid, $lmsdata->learningstart);
                        //강사정보 복사
                        edu_import_teacher($course->id, $lmsdata_course->courseid);
                        $lmsdata->id = $classid;
                        $lmsdata->templateyn = 'Y';
                        $DB->update_record('lmsdata_class', $lmsdata);
                    }
                }
            }
            ?>
        </div>

        <div class="extra_information">
            <?php if ($count_created) { ?>
                <p><?php echo $count_created; ?> 개의 강의를 추가 했습니다.</p>
            <?php } else { ?>
                <p>이미 생성된 강의 입니다.</p>
            <?php } ?>
        </div>
        <div id="btn_area">
            <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('okay', 'local_lmsdata'); ?>" onclick="location.href = 'edu_schedule.php?classtype=<?php echo $classtype; ?>'"/>
        </div>
        <?php
        siteadmin_scroll_down();
        ?>
    </div><!--Content End-->
</div> <!--Contents End-->

<?php
include_once ('../inc/footer.php');
