<?php

/**
 * 과정검색 ajax
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$selectvalue = required_param('selectvalue', PARAM_INT);
$classid = optional_param('classid', 0, PARAM_INT);

$returnvalue = new stdClass();
$course_select_sql = '';
$params = '';
if ($selectvalue == 7) {
    $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, 
                            ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, 
                            ca2.idnumber ca2idnumber, lc.*                        
                            FROM {course} co
                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                            JOIN {course_categories} ca on ca.id = co.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            where ca2.idnumber =:idnumber
                            order by lc.roughprocess asc, lc.detailprocess asc, lc.timeupdated desc";

    $params = array(
        'idnumber' => 'required_course'
    );
} else if ($selectvalue == 8) {
    $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, 
                            ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, 
                            ca2.idnumber ca2idnumber, lc.*                       
                            FROM {course} co
                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                            JOIN {course_categories} ca on ca.id = co.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            where ca.idnumber =:idnumber
                            order by lc.roughprocess asc, lc.detailprocess asc, lc.timeupdated desc";

    $params = array(
        'idnumber' => 'selection_course'
    );
} else {
    $course_select_sql = "SELECT co.id coid, co.fullname, ca.id caid, ca2.id ca2id, 
                            ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, 
                            ca2.idnumber ca2idnumber, lc.*                        
                            FROM {course} co
                            JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                            JOIN {course_categories} ca on ca.id = co.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            where ca.idnumber =:idnumber
                            order by lc.roughprocess asc, lc.detailprocess asc, lc.timeupdated desc";

    $params = array(
        'idnumber' => 'certification_course'
    );
}
$catagories = $DB->get_records_sql($course_select_sql, $params);

$returnvalue->status = 'success';

$optionvalue = '<option value=0>세부과정선택</option>';
$class;
foreach ($catagories as $catagory) {
    if ($classid) {
        $class = $DB->get_record('lmsdata_class', array('id' => $classid));
    }
    $catagory->value = $catagory->id;
    $catagory->text = '[' . $catagory->ca2name . '-' . $catagory->ca1name . '] ' . $catagory->fullname;
    if ($class->parentcourseid == $catagory->value) {
        $optionvalue .= '<option value="' . $catagory->value . '" selected>' . $catagory->text . '</option>';
    } else {
        $optionvalue .= '<option value="' . $catagory->value . '">' . $catagory->text . '</option>';
    }
}
$returnvalue->optionvalue = $optionvalue;

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
