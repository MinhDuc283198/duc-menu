<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/manage/qna_view.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);
    
    require_once dirname(dirname(__FILE__)) . '/lib/paging.php';    
    require_once $CFG->dirroot . '/local/jinoboard/lib.php';
    require_once($CFG->libdir . '/filestorage/file_storage.php');
    require_once($CFG->libdir . '/filestorage/stored_file.php');
    require_once($CFG->libdir . '/filelib.php');
    
    
    $contentId        = optional_param('id', 0, PARAM_INT);
    $type = optional_param('type', 5, PARAM_INT);
    
    
    
    $contentsRS = $DB->get_record_sql("select jcb.*,co.fullname,lc.courseid from {jinotechboard_contents} jcb 
join {jinotechboard} bo on jcb.board = bo.id and bo.type = 2 
join {lmsdata_class} lc on lc.courseid = jcb.course
join {lmsdata_course} lco on lc.parentcourseid = lco.id
join {course} co on lco.courseid = co.id left join {user} u ON u.id=jcb.userid "
            . " where jcb.id=:id", array('id' => $contentId));    
    
    $postuser = $DB->get_record('user', array('id' => $contentsRS->userid));
    // 파일 다운로드
    $cm = $DB->get_record_sql("select cm.id from {modules} m join {course_modules} cm on cm.module = m.id and cm.instance = $contentsRS->board and cm.course = $contentsRS->courseid  ");
    $context = context_module::instance($cm->id);

//첨부파일영역
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_jinotechboard', 'attachment', $contentId, 'id', false);
$attachments = "";
if (count($files) > 0) {
    $type = '';
    $attfile = '';

    if ($CFG->enableportfolios)
        $canexport = $USER->id == $content->userid;
    if ($canexport) {
        require_once($CFG->libdir . '/portfoliolib.php');
    }
    foreach ($files as $file) {

        $filepath = $file->get_filepath();
        if (empty($filepath)) {
            $filepath = '/';
        }

        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $attfile .= '<li>';
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_jinotechboard/attachment/' . $contentId . $filepath . $filename);
        if ($file->get_filesize() > 0) {
            $attfile .= "<a href=\"$path\">$iconimage</a> ";
            $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
        } else {
            $alertstring = get_string('deletedfile', 'jinotechboard');
            $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . $iconimage . '</a>';
            $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . s($filename) . "</a>";
        }
        $attfile .= '</li>';
    }

    $attachments .= $attfile;

    $output .= html_writer::start_tag('div', array('class' => 'detail-attachment-area'));
    $output .= html_writer::tag('span', get_string('attachment', 'jinotechboard'), array('class' => "detail-attachment-title"));
    $output .= html_writer::tag('ul', $attachments, array('class' => "detail-attachment"));
    $output .= html_writer::end_tag('div');
}
    $page = new stdClass();
    $page->title = get_string('admin_question','local_lmsdata');   
?>

    
<?php include_once('../inc/header.php');?>
<div id="contents">
    <?php include_once('../inc/sidebar_manage.php');?>
    <div id="content">
        <form action="qna_del.php" method="post">
            <input type="hidden" name="check_list[]" value="<?php echo $contentsRS->id;?>">
<h3 class="page_title">과정Q&A</h3>
<div class="page_navbar"><a href="./edu_course.php"><?php echo get_string('course_management', 'local_lmsdata'); ?></a> > <a href="./qna.php"><?php echo get_string('Q_A', 'local_lmsdata'); ?> </a> ><strong> 상세보기</strong></div>
<div class="detail-title-area">
    
    <span class="detail-title">
        <?php echo $contentsRS->title ?>
    </span>
    <br/>
    <?php
        $fullname = fullname($postuser);
    ?>
    <span class="detail-date area-right"><?php echo date('Y-m-d',$contentsRS->timecreated).' '.$fullname.' 작성';?></span>
</div>
<div class="detail-contents">
    <?php echo $contentsRS->contents; ?>
</div>
<?php echo $output;?>

<div clas="btn_area">
   <input type="button" id="qna_list" class="gray_btn" value="<?php echo get_string('list2','local_lmsdata'); ?>" style="float: left" />
<input type="submit" id="deleste_qna" class="gray_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: right" />
<input type="button" id="qna_update" class="blue_btn" value="<?php echo get_string('edit','local_lmsdata'); ?>" style="float: right; margin: 0 10px 0 0" />
<input type="button" id="comment_qna" class="blue_btn" value="답글" style="float: right; margin: 0 10px 0 0" />
</div> <!-- Bottom Button Area -->
</form>
    </div>
</div>

  
<?php 
    include_once '../inc/footer.php';
?>


<script type="text/javascript">
      $(document).ready(function () {
		                
//                 $('#delete_qna').click(function() {
//                      var del_list =[];
//                      del_list.push(<?php echo $contentId?>);
//                    $.ajax({
//                        url : "./qna_del.php",
//                        type: "post",
//                        data : {
//                            data : del_list                            
//                        },
//                        async: false,
//                        success: function(data){
//                           location.href = "./qna.php";
//                        },
//                        error:function(e){
//                            console.log(e.responseText);
//                        }
//                    }); 
//                });
                
                
                $('#qna_update').click(function() {
                       location.href = "./qna_write.php?mod=edit&type=5&id=<?php echo $contentId?>";
                });
                
                $('#qna_list').click(function() {
                   location.href = "./qna.php";
                });
                
                $('#comment_qna').click(function() {
                       location.href = "./qna_write.php?mod=reply&type=5&id=<?php echo $contentId?>";
                });
        });
</script>