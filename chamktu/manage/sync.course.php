<?php
//require_once $CFG->dirroot.'/local/haksa/config.php';
//require_once $CFG->dirroot.'/local/haksa/lib.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once $CFG->dirroot . '/chamktu/manage/synclib.php';
require_once $CFG->dirroot . '/local/haksa/lib.php';

$year = optional_param('year', $year, PARAM_INT);
$term = optional_param('term', $term, PARAM_INT);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', 2, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);

$haksa = $DB->get_record('haksa', array('year' => $year, 'term' => $term));

?>

<h4 class="page_sub_title">강의 동기화</h4>

<form name="" id="course_search" class="search_area" action="sync.php" method="get">
    <input type="hidden" name="page" value="1" />
    <input type="hidden" name="tab" value="<?php echo $tab; ?>" />

    <label style="margin-right: 5px;"><font color="#F00A0D" size="3px;"><strong>*</strong></font> <?php echo get_string('year','local_lmsdata',' '); ?></label>
    <select class="w_90" onchange="#" name="year">
        <?php
        $years = siteadmin_get_years();
        foreach ($years as $v => $y) {
            $selected = '';
            if ($v == $year) {
                $selected = ' selected';
            }
            echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
        }
        ?>
    </select>

    <label style="margin-right: 5px;"><font color="#F00A0D" size="3px;"><strong>*</strong></font> <?php echo get_string('stats_terms','local_lmsdata'); ?></label>
    <select class="w_90" onchange="#" name="term">
        <?php
        $terms = siteadmin_get_terms_sync();
        foreach ($terms as $v => $t) {
            $selected = '';
            if ($v == $term) {
                $selected = ' selected';
            }
            echo '<option value="' . $v . '"' . $selected . '> ' . $t . '</option>';
        }
        ?>
    </select>
    <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('okay','local_lmsdata'); ?>"/>

    <br/>
    <!--label style="margin-right: 40px;">&nbsp;</label-->
    <input type="radio" name="search" value="2"<?php if ($search == 2) { ?> checked<?php } ?>/>과목코드
    <input type="radio" name="search" value="3"<?php if ($search == 3) { ?> checked<?php } ?>/>과목명
    <input type="text" name="searchtext" value="<?php echo $searchtext; ?>" placeholder=" <?php echo get_string('search','local_lmsdata'); ?> " class="w_260" style="color: #8E9094; margin:0 0 5px 15px;" />
    <input type="submit" class="blue_btn" value="<?php echo get_string('search','local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/>
</form><!--Search Area2 End-->

<?php
if ($haksa === false || $haksa->timesynccourse == 0) {
    $terms = siteadmin_get_terms_sync();
    ?>
    <div class="extra_information">
        <p>학사시스템에서 <?php echo $year; ?> <?php echo get_string('year2','local_lmsdata'); ?> <?php echo $terms[$term]; ?> 강의를 가져오지 않았습니다.</p>
        <p>지금 가져오려면 "강의 가져오기" 버튼을 클릭하세요.</p>
    </div>
    <div id="btn_area">
        <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="강의 가져오기" onclick="location.href = 'sync.course.import.php?year=<?php echo $year; ?>&term=<?php echo $term; ?>'"/>
    </div>
    <?php
} else {
    $sql_where = "";

    $conditions = array('hc.year = :year', 'hc.term = :term', 'hc.deleted = :deleted');
    $params = array('year' => $year, 'term' => $term, 'deleted' => 0);
    if (!empty($searchtext)) {
        switch ($search) {
//            case 1: // 강의코드
//                $conditions[] = $DB->sql_like('hc.hakno', ':hakno');
//                $params['hakno'] = '%' . $searchtext . '%';
//                break;
            case 2: // 과목코드
                $conditions[] = $DB->sql_like('hc.lec_cd', ':lec_cd');
                $params['lec_cd'] = '%' . $searchtext . '%';
                break;
            case 3; // 강의명
                $conditions[] = $DB->sql_like('hc.kor_lec_name', ':kor_lec_name');
                $params['kor_lec_name'] = '%' . $searchtext . '%';
                break;
            default:
                break;
        }
    }

    if (!empty($conditions)) {
        $sql_where = ' WHERE ' . implode(' AND ', $conditions);
    }
    $sql_orderby = ' ORDER BY hc.kor_lec_name';

    $classes = $DB->get_records_sql('SELECT hc.id, hc.univ
     , hc.major
     , hc.kor_lec_name
     , hc.prof_name
     , hc.lec_cd
FROM {haksa_class} hc' . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
    $count_classes = $DB->count_records_sql('SELECT COUNT(*) FROM {haksa_class} hc ' . $sql_where, $params);
    ?>

    <table>
        <thead>
            <tr>
                <th><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th>과목코드</th>
                <th>과목명</th>
                <th><?php echo get_string('prof_1', 'local_lmsdata'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($count_classes) {
                $startnum = $count_classes - (($currpage - 1) * $perpage);
                foreach ($classes as $class) {
                    ?>
                    <tr>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $class->lec_cd; ?></td>
                        <td class="left"><?php echo $class->kor_lec_name; ?></td>
                        <td><?php echo $class->prof_name; ?></td>
                    </tr>
                    <?php
                }
            } else {
                echo '<tr><td colspan="8">'.get_string("nodata","local_lmsdata").'</td></tr>';
            }
            ?>
        </tbody>
    </table>

    <div id="btn_area">
        <input type="button" class="blue_btn" style="float:left; margin-right: 10px;" value="강의 가져오기" onclick="location.href = 'sync.course.import.php?year=<?php echo $year; ?>&term=<?php echo $term; ?>'"/>
        <input class="red_btn" value="강의 생성" id="create_course" style="float:right; margin-left: 10px;" type="button" onclick="location.href = 'sync.course.create.php?year=<?php echo $year; ?>&term=<?php echo $term; ?>'" />
    </div>

    <?php
    print_paging_navbar_script($count_classes, $currpage, $perpage, 'javascript:sync_goto_page(:page);');
}
