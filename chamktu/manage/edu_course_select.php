<?php
/**
 * 과정선택 팝업 페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$ids = optional_param_array('id', array(), PARAM_INT);



$sql_select  = "SELECT mc.id, ca.name ca1name, ca2.name ca2name, mc.fullname, lc.id lcid, lc.mainexposure ";

$sql_from    = " FROM {course} mc
                 JOIN {lmsdata_course} lc ON lc.courseid = mc.id and lc.classtype = 1
                 JOIN {course_categories} ca ON ca.id = mc.category 
                 left JOIN {course_categories} ca2 ON ca.parent = ca2.id";


$courses = $DB->get_records_sql($sql_select.$sql_from.$sql_where.$sql_orderby, $params, ($currpage-1)*$perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) ".$sql_from.$sql_where, $params);
?>

<div class="popup_content">
    <h2>과정선택</h2>
    <form id="frm_course_select" name="frm_course_select" onsubmit="return false;">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th width="15%">선택</th>
                <th width="15%">번호</th>
                <th width="40%">과정명</th>
                <th width="15%">영역</th>
                <th width="15%">분류</th>                
            </tr>
            <?php
            foreach($courses as $course) {
                $disabled = '';
                $checked = '';
                if(in_array($course->lcid, $ids)) {
                    $disabled = ' disabled';
                    $checked = ' checked';
                }
                echo '<tr>';
                echo '<td><input type="checkbox" name="id" value="'.$course->lcid.'"'.$disabled.$checked.'/></td>';
                echo '<td>'.$count_courses.'</td>';
                echo '<td>'.$course->fullname.'<input type="hidden" name="id_'.$course->lcid.'_name" value="'.$course->fullname.'"/></td>';
                echo '<td>'.$course->ca1name.'</td>';
                echo '<td>'.$course->ca2name.'</td>';                
                echo '</tr>';
                $count_courses--;
            }
            ?>
            </tbody>
        </table>
    </form>
</div>
