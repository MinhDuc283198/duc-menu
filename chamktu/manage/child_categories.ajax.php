<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$pid = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$getclasssql = 'select lc.classyear '
        . 'from {lmsdata_class} lc join {lmsdata_course} lco on lco.id = lc.parentcourseid where (lco.classtype = :lcoclasstype or lco.classobject = :lcoclassobject)';

//$catagories = $DB->get_records('course_categories', array('visible'=>1, 'parent'=>$pid), 'sortorder', 'id, idnumber, name');
$catagories = $DB->get_records_sql($getclasssql, array('lcoclasstype' => $pid, 'lcoclassobject' => $pid));
if($catagories) {
    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories;
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);