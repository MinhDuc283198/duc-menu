<?php
/**
 * select 박스 선택시 위탁처를 가져오는 ajax 파일
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$pid = required_param('id', PARAM_INT);

$returnvalue = new stdClass();


$catagories = $DB->get_records_sql("select consignmenttitle from {lmsdata_schedule} where classtype = 2 and consignmenttitle != '' and classyear = $pid ");
if($catagories) {
    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories;
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);