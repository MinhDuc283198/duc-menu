<?php
/**
 * 교육일정 추가,수정 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/course_list_add.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$classtype = required_param('classtype', PARAM_INT);

if (!empty($id)) {
    $course_sql = " SELECT *
                        
                    FROM {lmsdata_schedule}
                    WHERE id = :id ";
    $params = array('id' => $id);
    $course = $DB->get_record_sql($course_sql, $params);
    if ($classtype == 1) {
        $fileobj = edu_courselist_get_file($context->id, 'bannerimg', $id);
    } else {
        $fileobj1 = edu_courselist_get_file($context->id, 'logo1', $id, 1);
        $fileobj2 = edu_courselist_get_file($context->id, 'logo2', $id, 2);
    }
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo $classtype == 1 ? ($id ? '교육 일정수정' : '교육 일정등록') : ($id ? '위탁교육 일정수정' : '위탁교육 일정등록'); ?></h3>

        <form name="" id="course_search" action="edu_schedule_add.submit.php?id=<?php echo $id; ?>&classtype=<?php echo $classtype; ?>" method="post" enctype="multipart/form-data">

            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>
                    <?php if ($classtype == 2) { ?>
                        <tr>
                            <td class="field_title">제목</td>                        
                            <td class="field_value" colspan="3">
                                <input type="text" title="제목" name="consignmenttitle" placeholder="" size="150" value="<?php echo $course->consignmenttitle; ?>"/>                            
                            </td>
                        </tr>

                        <tr>
                            <td class="field_title">위탁처</td>                        
                            <td class="field_value">
                                <input type="text" title="위탁처" name="consignmentplace" placeholder="" size="20" value="<?php echo $course->consignmentplace; ?>"/>                            
                            </td>
                            <td class="field_title">본인부담금</td>                        
                            <td class="field_value">
                                <input type="text" title="본인부담금" name="deductible" placeholder="" size="20" value="<?php echo $course->deductible; ?>" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/> 원
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
    <!--                        <td class="field_title">과정선택</td>
                        <td class="field_value" >
                            <select id="course_select" name="course_select[]" multiple style="width: 300px;">
    
                        <?php
                        if ($id) {
                            $disabled = '';
                            $disabled = 'disabled';
                            if ($course_selects = $DB->get_records_sql('SELECT lco.id, co.fullname FROM {lmsdata_class} lc join {lmsdata_course} lco on lc.parentcourseid = lco.id join {course} co on co.id = lc.courseid where classyear = :classyear and classnum = :classnum', array('classyear' => $classyear, 'classnum' => $classnum))) {
                                foreach ($course_selects as $course_select) {
                                    echo '<option value="' . $course_select->id . '">' . $course_select->fullname . '</option>';
                                }
                            }
                        }
                        ?>
                            </select>
                        <?php if (!$id) {
                            ?>
                                            <input type="button" value="추가" class="orange_btn" onclick="course_multiple_select();"/>
                                            <input type="button" value="제거" class="orange_btn" onclick="remove_multiple_select();"/>
                        <?php } ?>
                        </td>-->
                        <td class="field_title">년도 / 기수</td>
                        <td class="field_value">
                            <select title="year" name="classyear" id="classyear" class="w_160" onchange="classnum_check(1)">
                                <?php
                                $yearRange = 10;
                                $currentYear = date('Y');
                                $maxYear = ($currentYear + $yearRange);
                                echo '<option ' . $disabled . ' value="0">년도</option>';
                                foreach (range($currentYear, $maxYear) as $year) {
                                    $selected = '';
                                    if ($year == $course->classyear) {
                                        $selected = 'selected';
                                    }
                                    echo '<option ' . $disabled . ' value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                                }
                                ?>
                            </select> 
                            <?php $classnum_desc = $DB->get_record_sql("select classnum from {lmsdata_schedule} where classtype = $classtype and classyear = $currentYear and openyn != 1 order by classnum desc ");
                            ?>
                            <?php if ($classtype == 1) { ?>
                                <input type="number" title="기수" name="classnum" <?php echo $disabled; ?> size="10" value="<?php echo $course->classnum ? $course->classnum : $classnum_desc->classnum + 1; ?>"onchange="classnum_check(2)" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/>기
                            <?php } ?>
                        </td>
                        <td class="field_title">공개여부</td>
                        <td class="field_value">
                            <input type="radio" name="openyn" value="0" <?php if ($course->openyn == 0) echo 'checked'; ?>> 공개
                            <input type="radio" name="openyn" value="1" <?php if ($course->openyn == 1) echo 'checked'; ?>> 비공개
                            <br>
                            <?php if ($classtype == 1) echo '* 비공개일정의 기수는 100이상으로 등록하세요'; ?>
                        </td>
                    </tr>

                    <tr id='table_time1'>
                        <td class="field_title">게시기간</td>
                        <td class="field_value">
                            <input type="text" title="시간" name="postingstart" id="postingstart" class="w_120" value="<?php echo $course->postingstart ? date('Y-m-d', $course->postingstart) : ''; ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" name="postingend" id="postingend" class="w_120" value="<?php echo $course->postingend ? date('Y-m-d', $course->postingend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                        <td class="field_title">수강신청기간</td>
                        <td class="field_value">

                            <input type="text" title="시간" name="enrolmentstart" id="enrolmentstart" class="w_120" value="<?php echo $course->enrolmentstart ? date('Y-m-d', $course->enrolmentstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" name="enrolmentend" id="enrolmentend" class="w_120" value="<?php echo $course->enrolmentend ? date('Y-m-d', $course->enrolmentend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                    </tr>
                    <tr id='table_time2'>
                        <td class="field_title">교육기간</td>
                        <td class="field_value">

                            <input type="text" title="시간" name="learningstart" id="learningstart" class="w_120" value="<?php echo $course->learningstart ? date('Y-m-d', $course->learningstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" name="learningend" id="learningend" class="w_120" value="<?php echo $course->learningend ? date('Y-m-d', $course->learningend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                        <td class="field_title">성적조회기간</td>
                        <td class="field_value">

                            <input type="text" title="시간" name="gradeviewstart" id="gradeviewstart" class="w_120" value="<?php echo $course->gradeviewstart ? date('Y-m-d', $course->gradeviewstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" name="gradeviewend" id="gradeviewend" class="w_120" value="<?php echo $course->gradeviewend ? date('Y-m-d', $course->gradeviewend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>
                    </tr>
                    <tr id='table_time3'>
                        <td class="field_title">평가기간</td>
                        <td class="field_value">

                            <input type="text" title="시간" name="evaluationstart" id="evaluationstart" class="w_120" value="<?php echo $course->evaluationstart ? date('Y-m-d', $course->evaluationstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" name="evaluationend" id="evaluationend" class="w_120" value="<?php echo $course->evaluationend ? date('Y-m-d', $course->evaluationend) : '' ?>" placeholder="클릭하세요"/> 
                            <br>[평가 종료일은 교육 종료일 +7일로 설정하세요.]
                        </td>
                        <td class="field_title">수강제한</td>
                        <td class="field_value">
                            <input type="text" title="수강제한" id="classlimit"  name="classlimit" size="10" value="<?php echo $course->classlimit; ?>" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)'/>
                            <br>[수강신청 제한 인원 0일경우 무제한]
                        </td>
<!--                        <td class="field_title">이의신청기간</td>
                        <td class="field_value">

                            <input type="text" title="시간" name="appealstart" id="appealstart" class="w_120" value="<?php echo $course->appealstart ? date('Y-m-d', $course->appealstart) : '' ?>" placeholder="클릭하세요"/> ~ 
                            <input type="text" title="시간" name="appealend" id="appealend" class="w_120" value="<?php echo $course->appealend ? date('Y-m-d', $course->appealend) : '' ?>" placeholder="클릭하세요"/> 
                        </td>-->
                    </tr>
                    <?php if ($classtype == 2) { ?>

                        <tr>
                            <td class="field_title">내용</td>
                            <td class="field_value" colspan="3"> 
                                <textarea name="consignmenttext" title="내용" class="ckeditor" id="subjects" rows="5" ><?php echo!empty($course->consignmenttext) ? $course->consignmenttext : ''; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">로고1</td>
                            <td class="field_value">
                                <input type="file" title="file" name="logo1" placeholder="" size="40" value=""/>
                                <?php echo $fileobj1; ?>
                                <input type="hidden" name="file_del_1" value="0"/>
                            </td>
                            <td class="field_title">로고2</td>
                            <td class="field_value">
                                <input type="file" title="file" name="logo2" placeholder="" size="40" value=""/>
                                <?php echo $fileobj2; ?>
                                <input type="hidden" name="file_del_2" value="0"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">과정신청수제한</td>
                            <td class="field_value"> 
                                <input type="text" title="과정신청수제한" id="application_limit"  name="application_limit" size="10" value="<?php echo $course->application_limit; ?>" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/>
                                * 0일 경우에는 무제한
                            </td>
                            <td class="field_title">과정신청자수 보이기</td>
                            <td class="field_value">
                                <input type="radio" name="counthide" value="0" <?php if ($course->counthide == 0) echo 'checked'; ?>> 공개
                                <input type="radio" name="counthide" value="1" <?php if ($course->counthide == 1) echo 'checked'; ?>> 비공개
                                <br>
                                <?php if ($classtype == 1) echo '* 비공개일정의 기수는 100이상으로 등록하세요'; ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="field_title">이수증 배경이미지</td>
                        <td class="field_value">
                            <input type="file" title="file" name="certificateimg" placeholder="" size="40" value=""/>
                        </td>
                        <td class="field_title">이수증 일련번호</td>                        
                        <td class="field_value">
                            <input type="text" title="일련번호" name="certificatepre" placeholder="" size="40" value=""/>                            
                        </td>
                    </tr>

                </tbody>
            </table>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" onclick="return course_create_submit_check()"/>
                <?php if (!empty($id)) { ?>
                    <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="course_delete('<?php echo $courseid; ?>')"/>
                <?php } ?>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_schedule.php?classtype=<?php echo $classtype; ?>';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    /**
     * 이미지 등록 삭제 
     * @returns {undefined}
     */
    function remove_file_1() {
        $("span[name='file_link_1']").remove();
        $("input[name='remove_button_1']").remove();
        $("input[name='file_del_1']").val(1);
    }
    function remove_file_2() {
        $("span[name='file_link_2']").remove();
        $("input[name='remove_button_2']").remove();
        $("input[name='file_del_2']").val(1);
    }
    /**
     * 벨리데이션 체크
     * @type type
     */
    $(document).ready(function () {
        $("select[name=classyear]").val(<?php echo $currentYear; ?>);

        $("#postingstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#postingend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#postingend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#postingstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#enrolmentstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#enrolmentend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#enrolmentend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#enrolmentstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#learningstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#learningend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#learningend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#learningstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#gradeviewstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#gradeviewend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#gradeviewend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#gradeviewstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#evaluationstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#evaluationend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#evaluationend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#evaluationstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("#appealstart").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#appealend").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#appealend").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#appealstart").datepicker("option", "maxDate", selectedDate);
            }
        });
        $("input[name=bannerimg").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["gif", "jpg", "jpeg", "png"]) == -1) {
                    alert("gif, jpg, jpeg, png 파일만 업로드 해주세요.");
                    $(this).val("");
                    return;
                }
            }
        });
        $("input[name=certificateimg").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["gif", "jpg", "jpeg", "png"]) == -1) {
                    alert("gif, jpg, jpeg, png 파일만 업로드 해주세요.");
                    $(this).val("");
                    return;
                }
            }
        });
        $("input[name=logo1").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["gif", "jpg", "jpeg", "png"]) == -1) {
                    alert("gif, jpg, jpeg, png 파일만 업로드 해주세요.");
                    $(this).val("");
                    return;
                }
            }
        });
        $("input[name=logo2").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["gif", "jpg", "jpeg", "png"]) == -1) {
                    alert("gif, jpg, jpeg, png 파일만 업로드 해주세요.");
                    $(this).val("");
                    return;
                }
            }
        });
    });
    /**
     * 숫자만 작성가능
     * @param {type} event
     * @returns {undefined|Boolean}
     */
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            return false;
    }
    /**
     * 문자열 삭제
     * @param {type} event
     * @returns {undefined}
     */
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
    /**
     * 밸리데이션체크
     * @returns {Boolean}
     */
    function course_create_submit_check() {

        if ($('input:radio[name="openyn"]:checked').val() == '1' && $("input[name='classnum']").val() < 100) {
            alert("비공개일정의 기수는 100이상을 입력하세요");
            return false;
        }
        if ($("select[name=classyear]").val() == '0') {
            alert("년도를 선택하세요.");
            return false;
        }
        if (<?php echo $classtype ?> == 1) {
            if ($.trim($("input[name='classnum']").val()) == '') {
                alert("기수를 입력하세요.");
                return false;
            }
        }

        if ($.trim($("input[name='postingstart']").val()) == '' || $.trim($("input[name='postingend']").val()) == '') {
            alert("게시기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='enrolmentstart']").val()) == '' || $.trim($("input[name='enrolmentend']").val()) == '') {
            alert("수강신청기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='learningstart']").val()) == '' || $.trim($("input[name='learningend']").val()) == '') {
            alert("교육기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='gradeviewstart']").val()) == '' || $.trim($("input[name='gradeviewend']").val()) == '') {
            alert("성적조회기간을 입력하세요.");
            return false;
        }
        if ($.trim($("input[name='evaluationstart']").val()) == '' || $.trim($("input[name='evaluationend']").val()) == '') {
            alert("평가기간을 입력하세요.");
            return false;
        }
    }

    /**
     * 일정삭제
     * @param {type} courseid
     * @returns {undefined}
     */
    function course_delete(courseid) {
        if (confirm("<?php echo get_string('deletecoursecheck'); ?>") == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/chamktu/manage/course_delete.execute.php" ?>',
                method: 'POST',
                data: {
                    id: courseid,
                },
                success: function (data) {
                    document.location.href = "<?php echo $CFG->wwwroot . "/chamktu/manage/edu_schedule.php?classtype=" . $classtype; ?>";
                }
            });
        }
    }
    /**
    * 신규 차수 체크

     * @param {type} type
     * @returns {undefined}     */
    function classnum_check(type) {
        var classnum = $("input[name='classnum']").val();
        var year = $("select[name=classyear]").val();

        $.ajax({
            url: 'edu_classnumcheck.ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                type: type,
                classyear: year,
                classnum: classnum,
                classtype: <?php echo $classtype; ?>
            },
            success: function (data, textStatus, jqXHR) {
                if (type == 1) {
                    $("input[name='classnum']").val(data.classnum);
                } else {
                    if (data.status == 'false') {
                        alert('해당기수가 존재합니다.');
                        return false;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }

//    function course_multiple_select_submit() {
//        var teacher_type = '#course_select';
//        var selCerts = $(teacher_type);
//        $("#frm_course_select input:checkbox[name=id]:checked").each(function () {
//            var id = $(this).val();
//            if ($(teacher_type + " option[value='" + id + "']").length == 0) {
//                var name = $("#frm_course_select input[name=" + "id_" + id + "_name" + "]").val();
//                selCerts.append('<option value="' + id + '">' + name + '</option>');
//            }
//        });
//        return true;
//    }
//    function remove_multiple_select() {
//        var teacher_type = '#course_select';
//        $(teacher_type + " option:selected").each(
//                function () {
//                    $(this).remove();
//                });
//    }

</script>
<?php
include_once ('../inc/footer.php');
