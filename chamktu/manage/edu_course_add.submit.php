<?php

/**
 * 과정 submit 파일
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/chamktu/lib/lib.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');

$submittype = required_param('type', PARAM_INT);
$classobject = required_param('classtype', PARAM_INT);
$classtype = required_param('classtype', PARAM_INT);

if ($submittype == 1) {
    $lmsdata = new stdClass();
    foreach ($_REQUEST as $key => $val) {
        $$key = $val;
        $lmsdata->$key = $val;
    }
    $mainexposure = implode(',', $mainexposure);

    $lmsdata->mainexposure = $mainexposure;
    $lmsdata->roughprocess = $cata1;
    $lmsdata->detailprocess = empty($cata2) ? $cata1 : $cata2;
    $lmsdata->coursearea = $cata1;
    $lmsdata->coursetype = empty($cata2) ? $cata1 : $cata2;
    $lmsdata->classobject = ($lmsdata->classobject != $classtype) ? $classtype : $classtype; // 멘토 / 임직원 / 교육생 인지 구분
    $lmsdata->classtype = ($lmsdata->classobject != $classtype) ? $classtype : $classtype; // 멘토 / 임직원 / 교육생 인지 구분
    $lmsdata->timecreated = time();
    $lmsdata->timeupdated = time();
    $lmsdata->subjects = $target;
    $lmsdata->completestandard = $completestandard; // 이수추적 가능하게끔 하는 변수, admin/설정 페이지에서 설정할 수 있음

    if ($lmsdata->roughprocess == 12) {
        $lmsdata->intro = '';
        $lmsdata->subjects = $lmsdata->testrange;
        $lmsdata->contents = $lmsdata->testcontent;
        $lmsdata->objectives = $lmsdata->testarea;
    }

    if (!empty($coursecd)) {
        $lmsdata->coursecd = $coursecd;
    }

//    $lmsdata->notice = 1;
    $courseconfig = get_config('moodlecourse');

    if (!$courseid) {
        $data = new stdClass();
        $data->classobject = ($data->classobject != $classtype) ? $classtype : $classtype; // 멘토 / 임직원 / 교육생 인지 구분
        $data->fullname = $lmsdata->coursename;
        $data->shortname = $lmsdata->coursename . time();
        $data->startdate = time();
        $data->summary = $intro;
        $data->summaryformat = 1;
        $data->category = empty($cata2) ? $cata1 : $cata2; // 카테고리

        $data->format = 'lguplus';
        $data->newsitems = $courseconfig->newsitems;
        $data->showgrades = $courseconfig->showgrades;
        $data->showreports = $courseconfig->showreports;
        $data->maxbytes = $courseconfig->maxbytes;
        $data->groupmode = $courseconfig->groupmode;
        $data->groupmodeforce = $courseconfig->groupmodeforce;
        $data->visible = $courseconfig->visible;
        $data->lang = $courseconfig->lang;
        $data->enablecompletion = $courseconfig->enablecompletion;

        $course = create_course($data);
        $courseid = $course->id;

        //기본블럭설정
        edu_blocks_add_default_course_blocks($course);
        
        // 과정 qna 추가
        $newcourse = $DB->get_record('course', array('id' => $courseid));
        new_course_create_activity_jinotechboard($newcourse, 2);
        
        $lmsdata->courseid = $course->id;
        $lmsdatacourse = $DB->insert_record('lmsdata_course', $lmsdata);
    } else {
        $data = $DB->get_record('course', array('id' => $courseid));
        $fullname = $data->fullname;
        $shortname = $data->shortname;
        $shname = substr($shortname, strlen($fullname));

        if ($data->fullname != $lmsdata->coursename) {
            $data->fullname = $lmsdata->coursename;
            $data->shortname = $lmsdata->coursename . $shname;
        }

        if (!empty($intro)) {
            $data->summary = $intro;
        }

        $data->category = empty($cata2) ? $cata1 : $cata2;

        $data_lms = $DB->get_record('lmsdata_course', array('courseid' => $courseid));
        $lmsdata->id = $data_lms->id;
        $data_lmsclass = $DB->get_records('lmsdata_class', array('parentcourseid' => $data_lms->id));

        if ($data_lmsclass) {
            foreach ($data_lmsclass as $clas) {
                $coursedata = $DB->get_record('course', array('id' => $clas->courseid));

                $coursedata->fullname = $lmsdata->coursename;
                $coursedata->shortname = $lmsdata->coursename . time();

                if (!empty($intro)) {
                    $coursedata->summary = $intro;
                }

                if (!empty($coursedata) && !empty($coursedata->id)) {
                    $DB->update_record('course', $coursedata);
                }
            }
        }

        update_course($data);

        $update_yn = $DB->update_record('lmsdata_course', $lmsdata);
    }
    $context = context_course::instance($courseid);
    edu_courselist_file_upload($file_del, $context->id, 'thumbnailimg', $id);
} else {
    
}
if ($id) {
    if ($submittype == 1) {
//        redirect($CFG->wwwroot . '/chamktu/manage/edu_course_add.php?id=' . $id);
        redirect($CFG->wwwroot . '/chamktu/manage/edu_course.php');
    }
} else {
    redirect($CFG->wwwroot . '/chamktu/manage/edu_course.php');
}