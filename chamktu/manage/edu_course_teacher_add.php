<?php
/**
 * 강사 정보 입력 페이지(과정등록시 강사가 추가 되어야함)
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/edu_course_add.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$courseid = optional_param('id', 0, PARAM_INT);
$coursetype = optional_param('coursetype', 1, PARAM_INT); //0:교과, 1:비교과, 2:이러닝

if (!empty($courseid)) {
    $teachers = $DB->get_records_sql('SELECT ct.*, u.lastname, lu.userid, u.username FROM {lmsdata_course_teacher} ct '
            . 'join {lmsdata_user} lu on ct.lmsuserid = lu.id '
            . 'join {user} u on lu.userid = u.id '
            . ' where ct.courseid = :courseid and ct.type = 1 '
            , array('courseid' => $courseid));

    $context = context_course::instance($courseid);
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo '강사정보'; ?></h3>
        <?php if ($courseid) { ?>
            <div class="clear">
                <input type="button" value="과정등록" onclick="javascript:location.href = 'edu_course_add.php?id=<?php echo $courseid; ?>';" class="normal_btn" />
                <input type="button" value="강사정보" onclick="javascript:location.href = 'edu_course_teacher_add.php?id=<?php echo $courseid; ?>';" class="blue_btn" />    
            </div>
        <?php } ?>

        <form name="" id="course_search" action="edu_course_add.submit.php?id=<?php echo $courseid; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" readonly="" name="type" value="2"/>
            <?php
            if (!empty($course)) {
                echo '<input type="hidden" name="courseid" value="' . $courseid . '" />';
            }
            foreach ($teachers as $teacher) {
                $fileobj = edu_courselist_get_file($context->id, 'teacherimg', $teacher->id);
                $fs = get_file_storage();
                $context = context_course::instance($courseid);
                $files = $fs->get_area_files($context->id, 'local_courselist', 'teacherimg', $teacher->id, 'id');
                $fileobj = '';
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();
                    $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_courselist/teacherimg/' . $teacher->id . '/' . $filename);
                    if ($file->get_filesize() > 0) {
                        $fileobj = "<span id ='file' name='file_link_$teacher->id' ><a href=\"$path\" >$iconimage</a>";
                        $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                        $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_$teacher->id' value='삭제' onclick='remove_file($teacher->id)'/></span>";
                    }
                }
                ?>
                <table cellpadding="0" cellspacing="0" class="detail">
                    <tbody>

                        <tr >
                            <td class="field_title">아이디</td>
                            <td class="field_value">
                                <input type="text" name="userid" readonly="" value="<?php echo $teacher->username; ?>">
                                <input type="hidden" name="teacherids[]" readonly="" value="<?php echo $teacher->id; ?>">
                            </td>
                            <td class="field_title">이름</td>
                            <td class="field_value">
                                <input type="text" name="username" readonly="" value="<?php echo $teacher->lastname; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">사진등록</td>
                            <td class="field_value">
                                <div id="file_area">
                                    <input type="file" title="file" id="nt_img_real" name="teacherimg_<?php echo $teacher->id; ?>" placeholder="" size="40" value=""/>    
                                    <?php echo $fileobj; ?>
                                    <input type="hidden" name="file_del_<?php echo $teacher->id; ?>" value="0"/>
                                </div>                            
                            </td>
                            <td class="field_title">대표강사</td>
                            <td class="field_value">
                                <input type="radio" name="best" value="<?php echo $teacher->id; ?>" <?php echo $teacher->best == 1 ? 'checked' : '' ?> onclick="radio_check()">
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">이력사항</td>
                            <td class="field_value" colspan="3"> 
                                <textarea name="teacherhistory_<?php echo $teacher->id; ?>" title="이력사항" class="ckeditor" id="teacherhistory" rows="5" ><?php echo!empty($teacher->teacherhistory) ? $teacher->teacherhistory : ''; ?></textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            <?php } ?>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" onclick="return course_create_submit_check()"/>
                <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_course.php';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    /**
     * 파일 삭제버튼
     * @param {type} id
     * @returns {undefined}
     */
    function remove_file(id) {
        $("span[name='file_link_" + id + "']").remove();
        $("input[name='remove_button_" + id + "']").remove();
        $("input[name='file_del_" + id + "']").val(1);
    }
    $(document).ready(function () {
        $('input[name="teacherids[]"]').each(function() {
            $("input[name=teacherimg_"+$(this).val()).change(function () {
                if ($(this).val() != "") {
                    var ext = $(this).val().split(".").pop().toLowerCase();
                    if ($.inArray(ext, ["gif", "jpg", "jpeg", "png"]) == -1) {
                        alert("gif, jpg, jpeg, png 파일만 업로드 해주세요.");
                        $(this).val("");
                        return;    
                    }
                }
            });
        });
    });


</script>
<?php
include_once ('../inc/footer.php');
