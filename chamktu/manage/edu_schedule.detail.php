<?php
/**
 * 교육일정관리 상세페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once dirname(dirname (__FILE__)).'/lib.php';
require_once $CFG->dirroot.'/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');

// Check for valid admin user - no guest autologin

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/manage/course_list_add.php');
    redirect(get_login_url());
}
$classtype     = optional_param('classtype', 1, PARAM_INT); //1:직무 2:위탁
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT);

if(!empty($id)) {
    $course_sql = " SELECT *
                        
                    FROM {lmsdata_schedule}
                    WHERE id = :id  order by id desc"  ;
    $params = array('id' => $id);
    $course = $DB->get_record_sql($course_sql, $params);
    $count_courses = $DB->count_records_sql("select count(*) FROM {lmsdata_schedule} WHERE id = :id ",$params);
    
    $classes_sql = "select lc.*, co.fullname from {lmsdata_class} lc join {course} co on lc.courseid = co.id where lc.classtype = $classtype and lc.scheduleid = :scheduleid and lc.classyear = :classyear";
    $classes_count_sql = "select count(*) from {lmsdata_class} lc join {course} co on lc.courseid = co.id where lc.classtype = $classtype and lc.scheduleid = :scheduleid and lc.classyear = :classyear";
    $classes = $DB->get_records_sql($classes_sql, array('scheduleid'=>$course->id,'classyear'=>$course->classyear));
    $count_classes = $DB->count_records_sql($classes_count_sql, array('scheduleid'=>$course->id,'classyear'=>$course->classyear));
    
}

$js = array(
        '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot.'/chamktu/manage/course_list.js'
);
$offline = array(0=>'없음',1=>'있음');

?>

<?php include_once (dirname(dirname (__FILE__)).'/inc/header.php'); ?>
<div id="contents">
    <?php include_once  (dirname(dirname (__FILE__)).'/inc/sidebar_manage.php');?>

    <div id="content">
        <h3 class="page_title"><?php echo ($classtype == 1) ? '교육 상세보기': '위탁교육 상세보기';?></h3>

        <form name="" id="course_search"  method="post" enctype="multipart/form-data">
            <?php
                if(!empty($course)) {
                    echo '<input type="hidden" name="courseid" value="'.$courseid.'" />';
                }
            ?>
            <table cellpadding="0" cellspacing="0" class="detail">
                <tbody>

                    <tr style="height: 45px">
                        <td class="field_title">년도</td>
                        <td class="field_value" >
                            <span class="text_field"><?php echo $course->classyear;?></span>
                        </td>
                        <td class="field_title">기수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $course->classnum.'기';?></span>
                        </td>
                    </tr>
                    
                    <tr style="height: 45px">
                        <td class="field_title">과정수</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $count_classes ;?></span>
                        </td>
                        <?php if($classtype == 1) { ?>
                        <td class="field_title">집합교육</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $offline[$course->offlineyn]; ?></span>
                        </td>
                        <?php }else { ?>
                        <td class="field_title">위탁처</td>
                        <td class="field_value">
                            <span class="text_field"><?php echo $course->consignmenttitle; ?></span>
                        </td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>
            <br><br><br><br><br>            
            <table style="margin-top: 40px; ">
            <thead>
            <tr>
                <th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>
                <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <?php if($classtype == 1) { ?>
                <th scope="row" width="15%">과정명</th>
                <th scope="row" width="10%">게시기간</th>
                <th scope="row" width="10%">수강신청기간</th>
                <th scope="row" width="10%">교육기간</th>
                <th scope="row" width="10%">성적조회기간</th>
                <th scope="row" width="10%">평가기간</th>
                <th scope="row" width="10%">이의신청기간</th>                
                <?php }else { ?>
                <th scope="row" width="40%">제목</th>
                <th scope="row" width="15%">게시기간</th>
                <th scope="row" width="10%">등록일</th>
                <?php } ?>
                <th scope="row" width="5%">입장</th>
                <!--<th scope="row" width="5%">관리</th>-->
            </tr>
            </thead>
            <?php
                if($count_class === 0) { ?>
                <tr>
                    <td colspan="10"><?php echo get_string('empty_course','local_lmsdata'); ?></td>
                </tr>
                <?php } else {
                    $startnum = $count_classes - (($currpage - 1) * $perpage);
                    foreach($classes as $class) {
                ?>
                <tr>
                    <td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $class->lcid; ?>"/></td>
                    <td><?php echo $startnum--; ?></td>
                    <?php if($classtype == 1) { ?>
                    <td><?php echo $class->fullname;?></td>
                    <td><?php echo date('Y-m-d',$class->postingstart).' ~ '.date('Y-m-d',$class->postingend) ?></td>
                    <td><?php echo date('Y-m-d',$class->enrolmentstart).' ~ '.date('Y-m-d',$class->enrolmentend) ?></td>
                    <td><?php echo date('Y-m-d',$class->learningstart).' ~ '.date('Y-m-d',$class->learningend) ?></td>
                    <td><?php echo date('Y-m-d',$class->gradeviewstart).' ~ '.date('Y-m-d',$class->gradeviewend) ?></td>
                    <td><?php echo date('Y-m-d',$class->evaluationstart).' ~ '.date('Y-m-d',$class->evaluationend) ?></td>
                    <td><?php echo date('Y-m-d',$class->appealstart).' ~ '.date('Y-m-d',$class->appealend) ?></td>
                    <?php }else { ?>
                    <td><?php echo $class->fullname?></td>                    
                    <td><?php echo date('Y-m-d',$class->postingstart).' ~ '.date('Y-m-d',$class->postingend) ?></td>
                    <td><?php echo date('Y-m-d',$class->timeupdated) ?></td>
                    
                <?php } ?>
                    <td>
                        <input type="button" class="normal_btn" value="입장" onclick="javascript:location.href = '<?php echo $CFG->wwwroot . '/course/view.php?id=' . $class->courseid; ?>';"/>
                    </td>
                </tr>
                <?php
                    }
                }
                ?>    
        </table><!--Table End-->
        <div id="btn_area">
            <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2','local_lmsdata'); ?>" onclick="javascript:location.href='edu_schedule.php?classtype=<?php echo $classtype;?>';"/>
        </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    $(document).ready(function() {
//        $('#table_consignment').hide();
//        $('#table_occasional').hide();
        $('#classlimit').hide();
        
        $( "#postingstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#postingend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#postingend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#postingstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $( "#enrolmentstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#enrolmentend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#enrolmentend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#enrolmentstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $( "#learningstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#learningend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#learningend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#learningstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $( "#gradeviewstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#gradeviewend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#gradeviewend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#gradeviewstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $( "#evaluationstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#evaluationend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#evaluationend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#evaluationstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $( "#appealstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#appealend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#appealend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#appealstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

    });
    
    /**
     * 숫자만입력
     * @param {type} event
     * @returns {undefined|Boolean}
     */
    function onlyNumber(event){
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ) 
            return;
        else
            return false;
    }
    /**
     * 문자열 제거
     * @param {type} event
     * @returns {undefined}
     */
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ( keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ) 
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
    /**
     * 밸리데이션 체크
     * @returns {Boolean}
     */
    function course_create_submit_check() {
        if($("select[name='course_select[]'] option").length == 0) {
            alert("과정을 선택하세요.");
            return false;
        }
        if($("select[name=classyear]").val() == '0') {
            alert("년도를 선택하세요.");
            return false;
        }        
        if( $.trim($("input[name='classnum']").val()) == '' ) {
            alert("기수를 입력하세요.");
            return false;
        }
        if( $.trim($("input[name='postingstart']").val()) == '' ||  $.trim($("input[name='postingend']").val()) == '' ) {
            alert("게시기간을 입력하세요.");
            return false;
        }
        if( $.trim($("input[name='enrolmentstart']").val()) == '' ||  $.trim($("input[name='enrolmentend']").val()) == '' ) {
            alert("수강신청기간을 입력하세요.");
            return false;
        }
        if( $.trim($("input[name='learningstart']").val()) == '' ||  $.trim($("input[name='learningend']").val()) == '' ) {
            alert("교육기간을 입력하세요.");
            return false;
        }
        if( $.trim($("input[name='gradeviewstart']").val()) == '' ||  $.trim($("input[name='gradeviewend']").val()) == '' ) {
            alert("성적조회기간을 입력하세요.");
            return false;
        }
        if( $.trim($("input[name='evaluationstart']").val()) == '' ||  $.trim($("input[name='evaluationend']").val()) == '' ) {
            alert("평가기간을 입력하세요.");
            return false;
        }
        $("select[name='course_select[]'] option").each(function () {
            $(this).prop("selected", true);
        });
        
        
    }

    /**
     * 일정 삭제
     * @param {type} courseid
     * @returns {undefined}
     */
    function course_delete(courseid){
        if(confirm("<?php echo get_string('deletecoursecheck');?>") == true) {
            $.ajax({
              url: '<?php echo $CFG->wwwroot."/chamktu/manage/course_delete.execute.php"?>',
              method: 'POST',
              data : {
                id : courseid,  
              },
              success: function(data) {
                document.location.href = "<?php echo $CFG->wwwroot."/chamktu/manage/course_list.php?classtype=".$classtype; ?>";
              }
            });
        }
    }
    /**
    * 신규 차수 체크

     * @returns {undefined}     */
    function classnum_check(){
        var classnum = $("input[name='classnum']").val();
        var year = $("select[name=classyear]").val();
        
        $.ajax({
            url: 'edu_classnumcheck.ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                classyear: year,
                classnum : classnum
            },
            success: function (data, textStatus, jqXHR) {
                if(data.status == 'false') {
                    alert('해당기수가 존재합니다.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }
    /**
    * 과정추가 멀티셀렉트

     * @returns {undefined}     */
    function course_multiple_select() {
        var tag = $("<div></div>");
        
        var optionValues = [];
            var teacher_type = '#course_select';
            var title = '과정선택';
            var redirect_url = 'edu_course_select.php';

        $(teacher_type+' option').each(function() {
            optionValues.push($(this).val());
        });
        
        
        $.ajax({
          url: redirect_url,
          method: 'POST',
          data: { 'id' : optionValues },
          success: function(data) {
            tag.html(data).dialog({
                title: title,
                modal: true,
                width: 800,
                maxHeight: getWindowSize().height - 20,
                buttons: [ {id:'save',
                            text:'추가',
                            click: function() {
                                var btnSave = $(this).parent().find("button[id='save']");
                                btnSave.button('disable');
                                
                                var status = course_multiple_select_submit();
                                if(status) {
                                    $('#frm_course_select').remove();
//                                    $('#frm_class_edit').submit();
                                    $( this ).dialog( "close" );
                                } else {
                                    btnSave.button('enable');
                                }
                            }},
                           {id:'close',
                            text:'닫기', 
                            disable: true,
                            click: function() {
                                $( this ).dialog( "close" );
                            }}],
                open : function() {
                    var t = $(this).parent();
                    var w = $(window);
                    var s = getWindowSize();
                    
                    var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                    if(x < 0) x = 0;
                    var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                    if(y < 0) y = 0;
                    t.offset({
                        top: y,
                        left: x
                    });
                },
                close: function () {
                    $('#frm_course_select').remove();
                    $( this ).dialog('destroy').remove()
                }
            }).dialog('open');
          }
        });
    }
    /**
    * 셀렉트된 옵션 추가

     * @returns {Boolean}     */
    function course_multiple_select_submit() {
        var teacher_type = '#course_select';
        var selCerts = $(teacher_type);
        $("#frm_course_select input:checkbox[name=id]:checked").each(function() {
            var id = $(this).val();
            if($(teacher_type+" option[value='" + id + "']").length == 0) {
                var name = $("#frm_course_select input[name=" + "id_" + id + "_name" + "]").val();
                selCerts.append('<option value="' + id + '">' + name + '</option>');
            }
        });
        return true;
    }
    /**
    * 선택된 과정 제거

     * @returns {undefined}     */
    function remove_multiple_select() {
        var teacher_type = '#course_select';
        $(teacher_type+" option:selected").each(
                function() {
                    $(this).remove();
                });
    }
    /**
    * 제한인원 체크박스 버튼

     * @returns {undefined}     */
    function checkbox_check(){
        if($(".classlimit_check").is(":checked")){
            $('#classlimit').show();
        }else{
            $('#classlimit').hide();
        }            
    }
</script>
 <?php
 include_once ('../inc/footer.php');
