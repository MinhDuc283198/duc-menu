<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');

global $DB, $PAGE;

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$courseid = required_param('courseid', PARAM_INT);
$filename = $_FILES['enrol_excel']['name'];
$filepath = $_FILES['enrol_excel']['tmp_name'];

$role_arr = $DB->get_records('role', null, '', 'shortname, id, name');
$role_type = array('t' => 'editingteacher', 's' => 'student');

$course = $DB->get_record('course', array('id' => $courseid));
$lmsdataClass = $DB->get_record('lmsdata_class', array('courseid' => $courseid));

// 강의의 usertypecode 확인(classobject)
$lmsdataCoursesql = 'SELECT lco.*, ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber 
                                    FROM {lmsdata_course} lco
                                    JOIN {course_categories} ca ON ca.id = lco.detailprocess 
                                    LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id
                                    WHERE lco.id = :id';
$lmsdataCourse = $DB->get_record_sql($lmsdataCoursesql, array('id' => $lmsdataClass->parentcourseid));

$enrol = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $course->id));
$manager = new course_enrolment_manager($PAGE, $course);

$instances = $manager->get_enrolment_instances();
$instance = $instances[$enrol->id];

$plugins = $manager->get_enrolment_plugins();
$plugin = $plugins[$instance->enrol];

$context = context_course::instance($course->id, MUST_EXIST);

//엑셀파일 read
$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
}

$maxRow = $objWorksheet->getHighestRow();


$returnvalue = new stdClass();
$returnvalue->success = 0;
$returnvalue->begin = 0;
$returnvalue->empty = 0;
$returnvalue->notobject = 0;
error_log('[START:]' . date('Y-m-d H:i:s') . "]\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');

for ($i = 2; $i <= $maxRow; $i++) {
//    $no = trim($objWorksheet->getCell('A' . $i)->getValue());      //no - 의미없는 값이어서 제외
    $fullname = $objWorksheet->getCell('B' . $i)->getValue();                     //이름
    $username = trim($objWorksheet->getCell('C' . $i)->getValue());               //위탁코드 - user테이블의 username
    $role_code = trim(strtolower($objWorksheet->getCell('D' . $i)->getValue()));   //역할(t:editingteacher, s:student)
    //엑셀값이 비어있나 체크
    if (!empty($username) && $username != '') {

        //username으로 사용자가 생성되어있나 체크
        $user = $DB->get_record('user', array('username' => $username));
        if (!empty($user)) {
            $role_shortname = $role_type[$role_code];
            $roleid = $role_arr[$role_shortname]->id;

            $lmsdatauser = $DB->get_record('lmsdata_user', array('userid' => $user -> id));
            $usertypecode = (($lmsdatauser->usertypecode) / 10);
            $userobject = $lmsdataCourse->classobject;
            $available = 1;
            if ($usertypecode == 3 && $lmsdataCourse->ca1idnumber != 'trust_course') {
                $available = 0;
            } else if ($userobject < 4 && $usertypecode != $userobject) {
                $available = 0;
            } else if ($userobject == 4 && $usertypecode == 3) {
                $available = 0;
            } else if ($userobject == 5 && $usertypecode == 1) {
                $available = 0;
            } else if ($userobject == 6 && $usertypecode == 2) {
                $available = 0;
            } else {
                $available = 1;
            }
            // 대상이 아닌 경우 등록되지 않도록 설정
            if ($available > 0) {
                //등록되는 roleid 체크
                if (!empty($roleid)) {
                    if (!$DB->record_exists('role_assignments', array('contextid' => $context->id, 'userid' => $user->id, 'roleid' => $roleid))) {
                        if ($role_shortname != 'student') {
                            $startdate = 0;
                            $enddate = 0;
                        } else {
                            ///학습시작일/종료일을 기수관리 교육기간에서 가져옴  
                            $startdate = $lmsdataClass->learningstart;
//                        $enddate   = $lmsdataClass->learningend + 1;     
                            $enddate = 0;
                        }
                        $plugin->enrol_user($instance, $user->id, $roleid, $startdate, $enddate, null);

                        $application = new stdClass();
                        $application->userid = $user->id;
                        $application->courseid = $course->id;
                        $application->status = 'apply';
                        $application->usertypecode = $DB->get_field('lmsdata_user', 'usertypecode', array('userid' => $user->id));
                        $application->birthday = ' ';
                        $application->phone = $user->phone1;
                        $application->email = $user->email;
                        $application->paymentid = ' ';
                        $application->ordernumber = ' ';
                        $application->price = 0;
                        $application->paymentmeans = 'unbank';
                        $application->paymentstatus = 'complete';
                        $application->completionstatus = 0;
                        $application->adminchange = 0;
                        $application->timecreated = time();
                        $application->timemodified = time();
                        $DB->insert_record('lmsdata_course_applications', $application);
                        //등록 로그 남김 - NEW:코스id:사용자이름:사용자ID:role이름
                        error_log("\t" . '-NEW:' . $course->id . ':' . $user->lastname . ':' . $user->username . ':' . $role_shortname . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
                        $returnvalue->success++;
                    } else {
                        //이미 등록된 사용자 로그 남김 - BEING:코스id:사용자이름:사용자ID:role이름
                        error_log("\t" . $available.'-BEING:' . $course->id . ':' . $user->lastname . ':' . $user->username . ':' . $role_shortname . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
                        $returnvalue->begin++;
                    }
                }
            } else {
                //수강대상이 아님 - NOTOBJECT:코스id:사용자이름:사용자ID:role이름
                error_log("\t" . '-NOTOBJECT:' . $course->id . ':' . $user->lastname . ':' . $user->username . ':' . $role_shortname . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
                $returnvalue->notobject++;
            }
        } else {
            //username이 맞는 사용자가 없음 - EMPTY:코스id:사용자ID
            error_log("\t" . '-EMPTY:' . $course->id . ':' . $username . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
            $returnvalue->empty++;
        }
    }
}
error_log('[END:]' . date('Y-m-d H:i:s') . "]\n\n\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
?>

