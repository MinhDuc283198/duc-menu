<?php 
require (dirname(dirname(dirname(__FILE__))) . '/config.php');

//병합될 강좌
$merger_id    = optional_param('merger_course_id', 0, PARAM_INT);
//기준강좌
$criterion_id = optional_param('criterion_course_id', 0, PARAM_INT);
//병합여부
$ismerger     = optional_param('ismerger', 1, PARAM_INT);
$context = context_system::instance();
$PAGE->set_context($context);

$merger = $DB->get_record('course', array('id'=>$merger_id));
$criterion = $DB->get_record('course', array('id'=>$criterion_id));

if($merger){
    $sql_select2 = "SELECT mu.*, lu.psosok, mr.name ";
    $sql_from2 = "FROM {context} ct
                JOIN {role_assignments} ra on ra.contextid = ct.id AND (ra.roleid = :roleid OR ra.roleid = :roleid2)
                JOIN {user} mu on mu.id = ra.userid
                JOIN {lmsdata_user} lu on lu.userid = ra.userid 
                JOIN {role} mr on mr.id = ra.roleid ";
    $sql_where2 = "WHERE ct.contextlevel = :contextlevel AND ct.instanceid = :instanceid";
    $users = $DB->get_records_sql($sql_select2.$sql_from2.$sql_where2, array('contextlevel'=>CONTEXT_COURSE, 'instanceid'=>$merger->id, 'roleid'=>5, 'roleid2'=>9));
    $count_users2 = $DB->count_records_sql("SELECT COUNT(*) ".$sql_from2.$sql_where2, array('contextlevel'=>CONTEXT_COURSE, 'instanceid'=>$merger->id, 'roleid'=>5, 'roleid2'=>9));
}

?>
<form name="merger_form" id="merger_form" class="search_area" method="post">
    <input type="checkbox" name="ismerger" value="1" <?php if($ismerger){ echo 'checked="checked"';}?>><?php echo get_string('ismerger', 'local_lmsdata');?> 
    <br>
    <span style="width:30%;"><?php echo get_string('merger_course', 'local_lmsdata');?> :</span> <input type="text" readonly title="merger_course" name="merger_course" value="<?php echo $merger->fullname; ?>" class="search-text"/>
    <input type="hidden" name="merger_course_id" value="<?php echo $merger_id;?>"  >
    <br>
    <span style="width:30%;"><?php echo get_string('criterion_course', 'local_lmsdata');?> :</span> <input type="text" title="criterion_course" name="criterion_course" value="<?php echo $criterion->fullname; ?>" class="search-text"/>
    <input type="hidden" name="criterion_course_id" value="<?php echo $criterion_id;?>" >
    <input type="button" class="search_btn" onclick="search_course_popup()" value="<?php echo get_string('search','local_lmsdata'); ?>"/>          
</form><!--Search Area2 End--> 
<?php
    if($merger){
?>
<form name="merger_submit" id="merger_submit" method="post" action="course_merger_submit.php">
    <input type="hidden" name="merger_course_id_submit" value="<?php echo $merger_id;?>">
    <input type="hidden" name="criterion_course_id_submit" value="<?php echo $criterion_id;?>">
    <input type="hidden" name="ismerger_submit" value="" >
    <table>
        <caption class="hidden-caption">학생목록</caption>
        <thead>
        <tr>
            <th scope="row" width="10%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
            <th scope="row" width="10%"><?php echo get_string('photo', 'local_lmsdata'); ?></th>
            <th scope="row"><?php echo get_string('major', 'local_lmsdata'); ?></th>
            <th scope="row" width="15%"><?php echo get_string('student_number', 'local_lmsdata'); ?></th>
            <th scope="row" width="10%"><?php echo get_string('user_role', 'local_lmsdata'); ?></th>
            <th scope="row" width="15%"><?php echo get_string('name', 'local_lmsdata'); ?></th>
        </tr>
        </thead>
        <?php
            if($count_users2 === 0) { ?>
            <tr>
                <td colspan="6"><?php echo get_string('empty_student','local_lmsdata'); ?></td>
            </tr>
            <?php } else {
                $startnum = $count_users2;
                foreach($users as $user) {
                    $userpic = new user_picture($user);
                    
                    echo "<tr>";
                    echo "<td>".$startnum--."</td>";
                    echo '<td><img src="'.$userpic->get_url($PAGE).'"/></td>';
                    echo '<td>'.$user->psosok.'</td>';
                    echo '<td>'.$user->username.'</td>';
                    echo "<td>".$user->name."</td>";
                    echo '<td>'.fullname($user).'</td>';
                    echo "</tr>";
                }
            }
            ?>    
    </table><!--Table End-->
    <div style="width:100%; float: left;">
        <center>
            <input type="submit" class="blue_btn" value="<?php echo get_string('delete_class', 'local_lmsdata');?>">
        </center>
    </div>
</form>
<?php
    }
?>

<script>    
    //기준강좌 검색
    function search_course_popup() {
        var tag = $("<div id='course_search_popup'></div>");
        $.ajax({
          url: '<?php echo $CFG->wwwroot.'/chamktu/manage/course_search.php?id='.$merger_id; ?>',
          method: 'POST',
          success: function(data) {
            tag.html(data).dialog({
                title: '<?php echo get_string('course_search','local_lmsdata'); ?>',
                modal: true,
                width: 800,
                resizable: false,
                height: 600,
                buttons: [ {id:'close',
                            text:'<?php echo get_string('cancle','local_lmsdata'); ?>',
                            disable: true,
                            click: function() {
                                $( this ).dialog( "close" );
                            }}],
                close: function () {
                    $('#frm_course_search').remove();
                    $( this ).dialog('destroy').remove()
                }
            }).dialog('open');
          }
        });
    }
    
    $('#merger_submit').submit(function(){
        var result = confirm('해당 수강생을 해당 강좌로 이동하고 강좌를 폐쇄하시겠습니까? *합강을 하면 해당 강좌는 삭제됩니다.');
        if(result === false){
            return false;
        } else {
            var ismerger = $( "#merger_form input[name=ismerger]:checked" ).val();
            if(!ismerger){
                ismerger = 0;
            }
            $( "#merger_submit input[name=ismerger_submit]" ).val(ismerger);
            return true;
        }
    });
</script>