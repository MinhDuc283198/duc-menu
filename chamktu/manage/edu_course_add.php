<?php
/**
 * 과정생성, 수정 페이지
 * 필수과정(신임과정, 연차별과정), 선택과정, 자격인증
 * 자격인증은 다른 과정들과 다른 입력창이 나옴
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/local/popup/lib.php';
require_once (dirname(dirname(dirname(__FILE__))) . '/lib/form/editor.php');
require_once $CFG->dirroot . '/local/jeipoint/lib.php';


require_login(0, false);
// Check for valid admin user - no guest autologin
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/course_list_add.php');
    redirect(get_login_url());
}
$context = context_system::instance();

require_capability('moodle/site:config', $context);

$courseid = optional_param('id', 0, PARAM_INT);

if (!empty($courseid)) {
    $course_sql = " SELECT co.id, co.fullname, lc.completestandard, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, lc.*                        
                    FROM {course} co
                    JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                    JOIN {course_categories} ca on ca.id = co.category 
                    left JOIN {course_categories} ca2 ON ca.parent = ca2.id 
                    WHERE co.id = :courseid ";
    $params = array('courseid' => $courseid);
    $course = $DB->get_record_sql($course_sql, $params);


    $context = context_course::instance($course->courseid);
    $fileobj = edu_courselist_get_file($context->id, 'thumbnailimg');

    $mainexposures = explode(',', $course->mainexposure);
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_manage.php'); ?>

    <div id="content">
        <h3 class="page_title"><?php echo empty($courseid) ? get_string('create_course', 'local_lmsdata') : '과정 수정' ?></h3>
        <div class="page_navbar"><a href="./edu_course.php"><?php echo get_string('course_management', 'local_lmsdata'); ?></a> > <strong><?php echo empty($courseid) ? get_string('create_course', 'local_lmsdata') : '과정 수정'; ?></strong></a></div>
        <form name="" id="course_search" action="edu_course_add.submit.php?id=<?php echo $courseid; ?>" method="post" enctype="multipart/form-data">
            <?php
            if (!empty($course)) {
                echo '<input type="hidden" name="courseid" value="' . $courseid . '" />';
            }
            ?>
            <table cellpadding="0" cellspacing="0" class="detail">
                <input type="hidden" readonly="" name="type" value="1"/>
                <input type="hidden" readonly="" name="trainingarea" value="0"/> <!-- 교육분야 주석처리로 인해 필수값 누락되어 추가 -->
                <input type="hidden" readonly="" name="coursegrade" value="22"/> <!-- 교육분야 주석처리로 인해 필수값 누락되어 추가 -->
                <tbody>
                    <tr>
                        <td class="field_title">과정명 <span class="red">*</span></td>
                        <td class="field_value">
                            <input type="text" title="과정명" name="coursename" placeholder="" size="50" value="<?php echo $course->fullname; ?>"/>
                        </td>
                        <td class="field_title">과정코드</td>
                        <td class="field_value">
                            <input type="text" title="과정코드" name="coursecd" placeholder="" size="10"   onkeyup="return checkcharlength(event)" value="<?php echo $course->coursecd; ?>"/>
                        </td>
                    </tr>
                    <tr >
                        <td class="field_title">수강 대상자 <span class="red">*</span></td> <!--수강대상으로 변경-->
                        <td class="field_value" colspan="3">
                            <input type="hidden" id="classtype" name="classtype" value=""/>
                            <?php
                            $type_arrs = array(1 => '임직원', 2 => '멘토', 3 => '교육생');
                            $radiobutton = '';
                            if ($course->classobject < 4) {
                                $classobject = explode(',', $course->classobject);

                                foreach ($type_arrs as $key => $val) {

                                    $checked = '';
                                    if (in_array($key, $classobject)) {
                                        $checked = 'checked = "checked"';
                                    }
//                                    $radiobutton .= '<input type="checkbox" id="classobject" name="classobject" ' . $checked . ' value="' . $key . '" onclick="radio_check(' . $key . ')">' . $val;
                                    $radiobutton .= '<input type="checkbox" id="classobject_' . $key . '" name="classobject" ' . $checked . ' value="' . $key . '">' . $val;
                                }
                            } else if ($course->classobject == 4) { // 멘토, 임직원
                                foreach ($type_arrs as $key => $val) {
                                    if ($key != 3) {
                                        $checked = 'checked = "checked"';
                                    }
//                                    $radiobutton .= '<input type="checkbox" id="classobject" name="classobject" value="' . $key . '" onclick="radio_check(' . $key . ') checked">' . $val;
                                    $radiobutton .= '<input type="checkbox" id="classobject_' . $key . '" name="classobject" value="' . $key . '" ' . $checked . '>' . $val;
                                    $checked = '';
                                }
                            } else if ($course->classobject == 5) { // 교육생, 임직원
                                if ($key != 2) {
                                    $checked = 'checked = "checked"';
                                }
                                foreach ($type_arrs as $key => $val) {
//                                    $radiobutton .= '<input type="checkbox" id="classobject" name="classobject" value="' . $key . '" onclick="radio_check(' . $key . ') checked">' . $val;
                                    $radiobutton .= '<input type="checkbox" id="classobject_' . $key . '" name="classobject" value="' . $key . '"' . $checked . '>' . $val;
                                    $checked = '';
                                }
                            } else if ($course->classobject == 6) { // 멘토, 교육생
                                if ($key != 1) {
                                    $checked = 'checked = "checked"';
                                }
                                foreach ($type_arrs as $key => $val) {
//                                    $radiobutton .= '<input type="checkbox" id="classobject" name="classobject" value="' . $key . '" onclick="radio_check(' . $key . ') checked">' . $val;
                                    $radiobutton .= '<input type="checkbox" id="classobject_' . $key . '" name="classobject" value="' . $key . '" ' . $checked . '>' . $val;
                                    $checked = '';
                                }
                            } else if ($course->classobject == 7) { // 전체 선택
                                foreach ($type_arrs as $key => $val) {
//                                    $radiobutton .= '<input type="checkbox" id="classobject" name="classobject" value="' . $key . '" onclick="radio_check(' . $key . ') checked">' . $val;
                                    $radiobutton .= '<input type="checkbox" id="classobject_' . $key . '" name="classobject" value="' . $key . '" checked>' . $val;
                                }
                            } else {
                                foreach ($type_arrs as $key => $val) {
//                                    $radiobutton .= '<input type="checkbox" id="classobject" name="classobject" value="' . $key . '" onclick="radio_check(' . $key . ') checked">' . $val;
                                    $radiobutton .= '<input type="checkbox" id="classobject_' . $key . '" name="classobject" value="' . $key . '">' . $val;
                                }
                            }
                            echo $radiobutton;
                            ?>  
                        </td> 
                    </tr>
                    <tr >
                        <td class="field_title">교육과정 <span class="red">*</span></td> <!--필수/선택과정으로 변경하고 필수과정을 클릭할 경우 바로 옆에 select가 나오게끔 한다.-->
                        <td class="field_value" colspan="3">
                            <select title="category01" name="cata1" id="course_search_cata1" onchange="cata_changed_c(this);"  class="w_160">
                                <option value="0"> - 선택하세요 -</option>
                                <?php
                                $catagories_pre = $DB->get_record('course_categories', array('visible' => 1, 'idnumber' => 'oklass_regular'));
                                $catagories = $DB->get_records('course_categories', array('visible' => 1, 'parent' => $catagories_pre->id), 'sortorder', 'id, idnumber, name');
                                if ($course) {
                                    if ($course->ca2idnumber == 'required_course') {
                                        foreach ($catagories as $catagory) {
                                            $selected = '';
                                            if ($catagory->idnumber == $course->ca2idnumber) {
                                                $selected = ' selected';
                                            }
                                            echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                                        }
                                    } else {
                                        foreach ($catagories as $catagory) {
                                            $selected = '';
                                            if ($catagory->idnumber == $course->ca1idnumber) {
                                                $selected = ' selected';
                                            }
                                            echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                                        }
                                    }
                                } else {
                                    foreach ($catagories as $catagory) {
                                        echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"> ' . $catagory->name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            <select title="category02" name="cata2" id="course_search_cata2" class="w_160" onchange="enable_yeartype(this)">
                                <option value="0"> - 선택하세요 -</option>
                                <?php
                                if ($course->ca2idnumber == 'required_course') {
                                    $catagories_pre = $DB->get_record('course_categories', array('visible' => 1, 'idnumber' => 'required_course'));
                                    $catagories = $DB->get_records('course_categories', array('visible' => 1, 'parent' => $catagories_pre->id), 'sortorder', 'id, idnumber, name');
                                    foreach ($catagories as $catagory) {
                                        $selected = '';
                                        if ($catagory->idnumber == $course->ca1idnumber) {
                                            $selected = ' selected';
                                        }
                                        echo '<option id="' . $course->ca1idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            <?php
                            if ($course->ca1idnumber == JEL_COURSE_CATEGORY_YEAR) {
                                $display = '';
                            } else {
                                $display = 'display: none';
                            }
                            ?>
                            <select title="yeartype" name="yeartype" id="course_yeartype" class="w_160" style="<?php echo $display; ?>;">
                                <?php
                                for ($i = 1; $i <= 10; $i++) {
                                    if ($course->yeartype == $i) {
                                        $selected = 'selected';
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option value="' . $i . '" ' . $selected . '>' . $i . '년차</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr id="t_7">
                        <td class="field_title">강의소개</td><!--진행일시/장소-->
                        <td class="field_value" colspan="3"> 
                            <textarea name="intro" title="강의소개" class="ckeditor" id="intro" rows="5"  ><?php echo!empty($course->intro) ? $course->intro : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr id="t_7">
                        <td class="field_title">학습목표</td><!--범위-->
                        <td class="field_value" colspan="3"> 
                            <textarea name="objectives" title="학습목표" class="ckeditor" id="objectives" rows="5" ><?php echo!empty($course->objectives) ? $course->objectives : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr id="t_7">
                        <td class="field_title">권장대상</td><!--없음-->
                        <td class="field_value" colspan="3"> 
                            <textarea name="target" title="권장대상" class="ckeditor" id="target" rows="5" ><?php echo!empty($course->subjects) ? $course->subjects : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr id="t_7">
                        <td class="field_title">교육내용</td> <!--시험출제 내용-->
                        <td class="field_value" colspan="3"> 
                            <textarea name="contents" title="교육내용" class="ckeditor" id="contents" rows="5" ><?php echo!empty($course->contents) ? $course->contents : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr id="t_12">
                        <td class="field_title">범위</td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="testrange" title="범위" class="ckeditor" id="testrange" rows="5" ><?php echo!empty($course->subjects) ? $course->subjects : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr id="t_12">
                        <td class="field_title">출제내용</td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="testcontent" title="출제내용" class="ckeditor" id="testcontent" rows="5" ><?php echo!empty($course->contents) ? $course->contents : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr id="t_12">
                        <td class="field_title">장소</td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="testarea" title="장소" class="ckeditor" id="testarea" rows="5" ><?php echo!empty($course->objectives) ? $course->objectives : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">이수기준</td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="completestandard" title="이수기준" class="ckeditor" id="completestandard" rows="5" ><?php echo!empty($course->completestandard) ? $course->completestandard : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title">썸네일이미지 <span class="red">*</span></td>
                        <td class="field_value" colspan="3">
                            <input type="file" title="썸네일" name="thumbnailimg" placeholder="" size="40" value=""/>
                            <?php echo $fileobj; ?>
                            <input type="hidden" name="file_del" value="0"/>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_lmsdata'); ?>" onclick="return course_create_submit_check()"/>
                <?php if (!empty($courseid)) { ?>
                    <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="course_delete('<?php echo $courseid; ?>')"/>
                <?php } ?>
                <input type="button" style="float:left; margin-right: 10px;" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_course.php';"/>
            </div>
        </form><!--Search Area2 End-->

    </div><!--Content End-->

</div> <!--Contents End-->
<script type="text/javascript">
    var thumbnailimg;

    /**
     * 썸네일 삭제 버튼 
     * @returns {undefined}
     */
    function remove_file() {
        $("span[name='file_link']").remove();
        $("input[name='remove_button']").remove();
        $("input[name='file_del']").val(1);
        thumbnailimg = '';
    }

    function checkcharlength(event) {
        var chcekchar = $("input[name='coursecd']").val();
        stringbytelength = chcekchar.replace(/[\0-\x7f]|([0-\u07ff]|(.))/g, "$&$1$2").length;
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        //  || (keyID >= 65 && keyID <= 90)
        if (!((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 107) || (keyID >= 65 && keyID <= 90) ||
                (keyID >= 109 && keyID <= 111) || (keyID >= 186 && keyID <= 192) || (keyID >= 219 && keyID <= 220))) {
            return;
        } else if (stringbytelength > 20) {
            alert('20byte 이하로 입력해주세요.');
            return false;
        } else {
            return;
        }
    }

    $(document).ready(function () {
        if ($("span[name='file_link']").attr('id')) {
            thumbnailimg = 'exists';
        }
        var cata11 = $("select[name=cata1] :selected").attr('id'); // certification_course

        if ($("select[name=cata1]").val() == 0 || $("select[name=cata1]").val() == '' || $("select[name=cata1]").val() == undefined) {
            $("tr[id^='t_']").hide();
        } else {
            if (cata11 == 'certification_course') {
                $("tr[id|='t_7']").hide();
                $("tr[id|='t_12']").show();
            } else if (cata11 == 'required_course' || cata11 == 'selection_course') {
                $("tr[id|='t_12']").hide();
                $("tr[id|='t_7']").show();
            }
        }

        if ($('#course_search_cata2').val() == 0 && cata11 != 'required_course') {
            $('#course_search_cata2').hide();
        }

        /**
         * 썸네일 등록시 이미지 파일 체크
         * @type type
         */
        $("input[name=thumbnailimg]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["gif", "jpg", "jpeg", "png"]) == -1) {
                    alert("gif, jpg, jpeg, png 파일만 업로드 해주세요.");
                    $(this).val("");
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });

        // cata2 보여주기 여부
        $("select[name=cata1]").change(function () {
            var cata11 = $("select[name=cata1] :selected").attr('id'); // certification_course
            var cataval = $("select[name=cata1]").val();
            if (cata11 == 'required_course') {
                $('#course_search_cata2').show();
            } else {
                $('#course_search_cata2').hide();
            }
            if (cata11 == 'certification_course') {
                $("tr[id|='t_7']").hide();
                $("tr[id|='t_12']").show();
            } else {
                $("tr[id|='t_12']").hide();
                $("tr[id|='t_7']").show();
            }
        });
    });

    /**
     * 숫자키만 입력가능하게
     * @param {type} event
     * @returns {undefined|Boolean}
     */
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39) {
            return;
        } else {
            return false;
        }
    }
    /**
     * 문자열은 삭제
     * @param {type} event
     * @returns {undefined}
     */
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;

        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39) {
            return;
        } else {
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
        }
    }
    /**
     * 벨리데이션 체크
     * 과정명, 교육과정 입력 체크
     * @returns {Boolean}
     */
    function course_create_submit_check() {

        //        var allcheckedlength = $("input:checkbox[name='classobject']").length;
        //        var checkedlength = $("input:checkbox[name='classobject']:checked").length;

        var chcekchar = $("input[name='coursecd']").val();
        stringbytelength = chcekchar.replace(/[\0-\x7f]|([0-\u07ff]|(.))/g, "$&$1$2").length;

        var classobject = $('input[name="classobject"]:checkbox:checked').length;

        if ($.trim($("input[name='coursename']").val()) == '') {
            alert("과정명을 입력하세요.");
            $("input[name='coursename']").focus();
            return false;
        }
        if (classobject == 0 || classobject == undefined || classobject == '') {
            alert("수강 대상자를 선택하세요.");
            return false;
        }

        if ($("select[name='cata1']").val() == '0') {
            alert("교육과정을 선택하세요.");
            $('select[name="cata1"]').focus();
            return false;
        }

        var cata1 = $('#course_search_cata1').val();
        var cata2 = $('#course_search_cata2').val();

        if (cata1 == 7 && cata2 == 0) {
            alert("세부 교육과정을 선택하세요.");
            $('#course_search_cata2').focus();
            return false;
        }

        if (stringbytelength > 20) {
            alert("과정코드는 20바이트 이하여야 합니다.");
            $("input[name='coursecd']").focus();
            return false;
        }

        if (thumbnailimg == '' || thumbnailimg == 0 || thumbnailimg == undefined) {
            alert("썸네일을 등록해주세요.")
            return false;
        }

        if (classobject == 1) {
            var checked = $("input[name='classobject']:checkbox:checked").val();
            $("#classtype").val(checked);
        } else if (classobject == 2) {
            var cb1 = $('#classobject_1').is(':checked');
            var cb2 = $('#classobject_2').is(':checked');
            var cb3 = $('#classobject_3').is(':checked');
            if (cb1 && cb2 && (!cb3)) {
                $("#classtype").val(4);
            } else if ((!cb1) && cb2 && cb3) {
                $("#classtype").val(5);
            } else if (cb1 && (!cb2) && cb3) {
                $("#classtype").val(6);
            }
        } else if (classobject == 3) {
            $("#classtype").val(7);
        }

        //        $("input[name='classobject']").attr('value', classobject);
        //        $('input:checkbox[name="classobject"]:checked').attr('value', classobject);
    }

    function course_delete(courseid) {
        if (confirm("강좌와 연결된 기수와 관련 정보들 모두 삭제하시겠습니까?") == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/chamktu/manage/edu_course_delete.php" ?>',
                method: 'POST',
                data: {
                    courseid: courseid
                },
                success: function (data) {
                    if (data.status == 'success') {
                        alert('삭제하였습니다.');
                        document.location.href = "<?php echo $CFG->wwwroot . "/chamktu/manage/edu_course.php"; ?>";
                    } else {
                        alert('삭제하지 못했습니다.');
                        document.location.href = "<?php echo $CFG->wwwroot . "/chamktu/manage/edu_course.php"; ?>";
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                    alert(jqXHR.responseText);
                }
            });
        }
    }

</script>
<?php
include_once ('../inc/footer.php');
