<?php
/**
 * 사용자 검색 팝업 화면 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
$con_name = optional_param('con_name', '', PARAM_RAW);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

if (!empty($con_name)) {
    $sql_where[] = $DB->sql_like('con_name', ':con_name');
    $params['con_name'] = '%' . $con_name . '%';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}
$contents = $DB->get_records_sql('select * from {lcms_contents} ' . $sql_where, $params, ($currpage - 1) * $perpage, $perpage);
$count_contents = $DB->count_records_sql("select count(*) from {lcms_contents}" . $sql_where . $orderby, $params);
?>

<div class="popup_content" id="course_contents">
    <h2>콘텐츠 명 검색</h2>
    <form id="frm_course_contents" name="frm_course_contents" onsubmit="return false;">
        <div style="float:left;">
            <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="콘텐츠 명을 입력하세요." class="search-text"/>
            <input type="button" class="blue_btn" value="검색" onclick="contents_search_name();"/>
        </div>
        <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <!--<th width="10%">선택</th>-->
                    <th scope="row" width="10%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th width="40%">콘텐츠명</th>
                    <th width="30%">등록일</th>
                    <th width="10%">선택</th>
                </tr>
                <?php
                $startnum = $count_contents - (($currpage - 1) * $perpage);
                foreach ($contents as $content) {
                    echo '<tr>';
                    echo '<td>' . $startnum-- . '</td>';
                    echo '<td>' . $content->con_name . '</td>';
                    echo '<td>' . date("Y.m.d",$content->reg_dt) . '</td>';
                    echo '<td><input type="button" value="선택" onclick="set_contents_info(' . $content->id . ', \'' . $content->con_name . '\')"></td>';

                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>        
    </form>
    <?php
                
    if (($count_contents / $perpage) > 1) {
        print_paging_navbar_script($count_contents, $currpage, $perpage, 'javascript:get_contents_paging(:page);',10);
    }
    ?>
</div>
<script>
    
    function set_contents_info(conid, con_name) {
        $('input[name=conid]').val(conid);
        $('input[name=conid]').prop('readonly', true);
        $('input[name=con_name]').val(con_name);
        $('input[name=con_name]').prop('readonly', true);
        $('#select_contents').dialog("close");
    }

    /**
     * 페이지이동
     * @param {type} page
     * @returns {undefined}
     */
    function get_contents_paging(page) {

        $.ajax({
            url: 'contents_report_records_getcontent.php',
            method: 'POST',
            data: {
                'page': page,
                'searchtext': $('#frm_course_contents').find('input[name=searchtext]').val(),
            },
            success: function (data) {
                $("#course_contents").parent().html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
    /**
     * 검색어 넘기는 함수
     * @returns {undefined}
     */
    function contents_search_name() {
        $.ajax({
            url: 'contents_report_records_getcontent.php',
            method: 'POST',
            data: {
                'page': <?php echo $currpage; ?>,
                'searchtext': $('#frm_course_contents').find('input[name=searchtext]').val()
            },
            success: function (data) {
                $("#course_contents").parent().html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
    /**
     * 엔터입력 submit
     
     * @type type     */
    $("input[name=searchtext]").keypress(function (e) {
        if (e.keyCode == 13) {
            contents_search_name();
        }
    });
</script>   