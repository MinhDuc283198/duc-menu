<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 2017, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터

//if ($classyear) {
//    $sql_where[] = " lc.classyear = :classyear ";
//    $params['classyear'] = $classyear;
//}
if (!empty($searchtext)) {
    $like = array();
    $like[] = $DB->sql_like('co.fullname', ':fullname');
    $params['fullname'] = '%' . $searchtext . '%';
    $sql_where[] = ' ' . implode(' OR ', $like);
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lco.*, co.fullname, ca.name ca1name, ca2.name ca2name, lc.parentcourseid"
        . ", (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid and (code1.codename like '%초등%' or code1.codename like '%유치원%') ) count1
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid and code1.codename like '%중등%' ) count2
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid and (code1.codename NOT like '%초등%' and code1.codename NOT like '%유치원%' and code1.codename NOT like '%중등%' ) ) count3
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid and (code1.codename like '%초등%' or code1.codename like '%유치원%') ) count4
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid and code1.codename like '%중등%' ) count5
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid and (code1.codename NOT like '%초등%' and code1.codename NOT like '%유치원%' and code1.codename NOT like '%중등%' ) ) count6
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid ) total
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid ) complet" ;
$sql_from = " FROM  {lmsdata_course} lco "
            . "join {course} co on co.id = lco.courseid "
            . "join {lmsdata_class} lc on lc.parentcourseid = lco.id and lc.classyear = $classyear "
            . "join {lmsdata_course_applications} lca on lc.id = lca.courseid and lca.paymentstatus = 'complete' "
            . "JOIN {course_categories} ca ON ca.id = co.category "
            . "left JOIN {course_categories} ca2 ON ca.parent = ca2.id";

$sql_orderby = " group by co.id ";

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT count(distinct co.id) " . $sql_from . $sql_where, $params);
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">운영현황보고</h3>
            <form name="course_search" id="course_search" class="search_area" action="edu_operation_status.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_160">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 10;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range($currentYear, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select>                     
                </div>
                <br><br><br>

                <div style="float:left;">
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="과정명을 입력하세요."  class="search-text"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
                </div>
            </form><!--Search Area2 End-->

            <table>
                <thead>
                    <tr>
                        <th scope="row" rowspan="2" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                        <th scope="row" rowspan="2" width="15%">과정명</th>
                        <th scope="row" rowspan="2" width="10%">영역</th>
                        <th scope="row" rowspan="2" width="10%">분류</th>
                        <th scope="row" rowspan="2" width="5%">교육구분</th>
                        <th scope="row" rowspan="2" width="5%">이수시간</th>
                        <th scope="row" rowspan="2" width="5%">교육대상</th>
                        <th scope="row" colspan="3" width="15%">신청인원</th>
                        <th scope="row" colspan="3" width="15%">이수인원</th>
                        <th scope="row" rowspan="2" width="5%">이수율</th>
                        <th scope="row" rowspan="2" width="5%">수강료</th>
                    </tr>
                    <tr>
                        <th scope="row" width="5%">초등</th>
                        <th scope="row" width="5%">중등</th>
                        <th scope="row" width="5%">기타</th>
                        <th scope="row" width="5%">초등</th>
                        <th scope="row" width="5%">중등</th>
                        <th scope="row" width="5%">기타</th>
                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="11">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    $startnum = $count_courses - (($currpage - 1) * $perpage);
                    foreach ($courses as $course) {
                        $completion_rate = sprintf("%2.2f" ,($course->complet/$course->total)*100);
                        ?>
                        <tr>
                            <!--<td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>-->
                            <td><?php echo $startnum--; ?></td>                    
                            <td><?php echo $course->fullname ?></td>
                            <td><?php echo $course->ca2name?></td>
                            <td><?php echo $course->ca1name ?></td>
                            <td><?php echo '' ?></td>
                            <td><?php echo $course->learningtime ?></td>
                            <td><?php echo '' ?></td>                            
                            <td><?php echo $course->count1?></td>
                            <td><?php echo $course->count2 ?></td>
                            <td><?php echo $course->count3 ?></td>
                            <td><?php echo $course->count4 ?></td>
                            <td><?php echo $course->count5 ?></td>
                            <td><?php echo $course->count6 ?></td>
                            <td><?php echo ($completion_rate>0)? $completion_rate.'%' :  '-';  ?></td>                            
                            <td><?php echo number_format($course->price) ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">            
                <div style="float:right;">
                    <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </div>
            <?php
            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params);
    $fields = array(
        '연번',
        '과정명',
        '영역',
        '분류',
        '연수구분',
        '이수시간',
        '연수대상',
        '신청인원',
        '이수인원',
        '이수율',
        '수강료'
    );
    $fields2 = array(
        '초등',
        '중등',
        '기타'
    );

    $date = date('Y-m-d', time());
    $filename = 'training_results' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        if($fieldname == '신청인원' || $fieldname == '이수인원'){
            $worksheet[0]->merge_cells(0, $col, 0, $col+2);
            foreach ($fields2 as $fieldname2) {
                $worksheet[0]->write(1, $col, $fieldname);
                $col++;
            }
        }else{         
            $worksheet[0]->merge_cells(0, $col, 1, $col);
            $col++;
        }
        
    }

    $row = 2;

    foreach ($courses as $course) {
        $completion_rate = sprintf("%2.2f" ,($course->complet/$course->total)*100);
        $col = 0;
        $worksheet[0]->write($row, $col++, $row-1);
        $worksheet[0]->write($row, $col++, $course->fullname);
        $worksheet[0]->write($row, $col++, $course->ca2name);
        $worksheet[0]->write($row, $col++, $course->ca1name);
        $worksheet[0]->write($row, $col++, '');
        $worksheet[0]->write($row, $col++, $course->learningtime);
        $worksheet[0]->write($row, $col++, '');
        $worksheet[0]->write($row, $col++, $course->count1);
        $worksheet[0]->write($row, $col++, $course->count2);
        $worksheet[0]->write($row, $col++, $course->count3);
        $worksheet[0]->write($row, $col++, $course->count4);
        $worksheet[0]->write($row, $col++, $course->count5);
        $worksheet[0]->write($row, $col++, $course->count6);
        $worksheet[0]->write($row, $col++, ($completion_rate>0)? $completion_rate.'%' :  '-');
        $worksheet[0]->write($row, $col++, number_format($course->price));
        
        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        var url = "edu_operation_status.php?excel=1";

        document.location.href = url;
    }

</script>    
