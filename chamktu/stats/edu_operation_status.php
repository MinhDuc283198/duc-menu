<?php /**
 * 교육운영현황 정기교육 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$classyear = optional_param('classyear', date('Y', time()), PARAM_INT);
$mon = optional_param('mon', date('m', time()), PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);


if ($classyear) { // 연도
    $sql_where[] = " lc.classyear = :classyear ";
    $params['classyear'] = $classyear;
}

if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$SQL_SELECT = "SELECT cc.id id,cc.name, lc.id icid,  lc.courseid, ca2.name ca2name , ca2.id ca2id
FROM {course_categories} cc 
join {lmsdata_course} lc ON lc.roughprocess = cc.id
join {lmsdata_class} lcl ON lcl.parentcourseid = lc.id 
left join {course_categories} ca2 ON cc.parent = ca2.id ";

$SQL_WHERE = " where lcl.classyear = :classyear " ;
$SQL_ORDERBY = " order by cc.sortorder asc ";

$class_datas = $DB->get_records_sql($SQL_SELECT . $SQL_WHERE . $SQL_ORDERBY, $params);

$datas = array();
$tables = array();

$fields = array(
    '구분',
    '',
    '과정명',
    '기수',
    '대상',
    '수강인원',
    '수료인원',
    '이수율',
    '과정이수율'    
);


$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');
$excelurl = "edu_operation_status.php?&classyear=$classyear&excel=1";
if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">교육과정 통계</h3>
            <div class="page_navbar"><a href="./contact_stats.php"><?php echo get_string('stats_management', 'local_lmsdata'); ?></a> > <strong>교육과정 통계</strong></div>


            <form name="course_search" id="course_search" class="search_area" action="edu_operation_status.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="$('form.search_area').submit();">
                        <option value="0">년도</option>
                        <?php

                    $max = $DB->get_field_sql("SELECT max(classyear) FROM {lmsdata_class}");
                    $min = $DB->get_field_sql("SELECT min(classyear) FROM {lmsdata_class}");

                    foreach (range($min, $max) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }                        
                        ?>
                    </select> 
                </div>
<!--                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                                       
                </div>-->
            </form><!--Search Area2 End-->
            <div style="float:right;">
            <input type="submit" onclick="location.href = '<?php echo $excelurl; ?>'" class="blue_btn" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?> " style="float:right;"/>    
            </div>
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="15%" colspan="2" rowspan="2">구분</th>
                        <th scope="row" width="15%" rowspan="2">과정명</th>
                        <th scope="row" width="3%" rowspan="2">기수</th>
                        <th scope="row" width="3%" rowspan="2">대상</th>
                        <th scope="row" width="3%" colspan="3">기수별 이수결과</th>
                        <th scope="row" width="3%" rowspan="2" colspan="2">과정 이수율</th>
                    </tr>
                    <tr>
                             <td scope="row" width="3%">수강인원</td>
                        <td scope="row" width="3%">수료인원</td>
                        <td scope="row" width="3%">이수율</td>  
                    </tr>
                </thead>                     
                <?php 
                foreach ($class_datas as $class_data) {
                       $sql_se1 = "select lcl.courseid, lc.roughprocess as rough FROM {lmsdata_class} lcl JOIN {lmsdata_course} lc on lc.roughprocess = $class_data->id WHERE lcl.classyear = $classyear and lcl.parentcourseid = lc.id"; 
                       $cc1 = $DB->get_records_sql($sql_se1);
                       $cco1 = count($cc1);
                       
                       $sql_class = "SELECT distinct cc.name, lc.id ,lcl.courseid, cc.id, lcl.classyear, lcl.parentcourseid , ca2.name ca2name , ca2.id ca2id
                        FROM {course_categories} cc 
                        join {lmsdata_course} lc ON lc.detailprocess = cc.id
                        join {lmsdata_class} lcl on lcl.parentcourseid = lc.id
                        left join {course_categories} ca2 ON cc.parent = ca2.id
                        where lcl.classyear = $classyear and  lc.coursearea = $class_data->id ORDER BY cc.sortorder asc";

                      
                       
                       $classnum_datas =$DB->get_records_sql($sql_class); 
                ?>
                <tr><!--구분--> 
                    
                  <td rowspan="<?php echo $cco1?>"><?php echo $class_data->name ?></td>                            
                <?php                
                    foreach ($classnum_datas as $classnum_data) {             

                        $sql_select = "SELECT distinct lc.coursename, lc.id ,lcl.courseid, cc.id,cc.NAME, lcl.classyear, lcl.parentcourseid 
                        FROM {course_categories} cc 
                        join {lmsdata_course} lc ON lc.detailprocess = cc.id
                        join {lmsdata_class} lcl on lcl.parentcourseid = lc.id
                        where lcl.classyear = $classyear and cc.id = $classnum_data->id";
                        $courses = $DB->get_records_sql($sql_select); 
                        
                       $sql_se = "select lcl.courseid, lc.detailprocess as detail FROM {lmsdata_class} lcl JOIN {lmsdata_course} lc on lc.detailprocess = $classnum_data->id WHERE lcl.classyear = $classyear and lcl.parentcourseid = lc.id"; 
                       $cc = $DB->get_records_sql($sql_se);
                       $cco = count($cc);
                      
                        ?>
                                    
                     <!--<td rowspan="<?php echo $cco?>"><?php echo ($classnum_data->ca2name == '교과과정') ? $classnum_data->name.'</td><td rowspan="'.$cco.'"></td>' : $classnum_data->ca2name . '</td><td rowspan="'.$cco.'">' . $classnum_data->name ?></td>-->                            
                          <td rowspan=" <?php echo ($classnum_data->ca2name == '필수과정') ? $cco.'">' .$classnum_data->name.'</td>' : $cco.'"></td>'?> 

        <?php
        foreach ($courses as $course) {
            $sql_class = "SELECT lcl.courseid,lcl.classnum ,lco.classobject lcoclassobject, lco.classtype lcoclasstype,               
                    (select count(*) FROM {lmsdata_class} lcl where lcl.classyear = $classyear and lcl.parentcourseid = $course->parentcourseid ) as cca
                from {lmsdata_course} lco 
                join {lmsdata_class} lcl
                join {course_categories} cc
                where lcl.classyear = $classyear and lcl.parentcourseid = $course->parentcourseid and lco.id = $course->parentcourseid  ";
            $classes = $DB->get_records_sql($sql_class);
            $ccc = count($classes);
            ?> <!--과정명-->    
                            <td rowspan = "<?php echo $ccc ?>">
                            <?php echo $course->coursename ?>
                            </td>   

            <?php
            foreach ($classes as $class) {
                $sql_result = "SELECT (select count(*) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                        . " AND lcl.parentcourseid = $course->parentcourseid  and lcl.classnum = $class->classnum where lcl.courseid = lc.courseid and lc.status = 'apply') as student,"
                        . "(select count(*) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                        . " AND lcl.parentcourseid = $course->parentcourseid  and lcl.classnum = $class->classnum where lcl.courseid = lc.courseid and lc.status = 'apply' and lc.completionstatus = 1) as complete,"
                        . "(select count(lc.userid) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                        . " AND lcl.parentcourseid = $course->parentcourseid  where lcl.courseid = lc.courseid and lc.status = 'apply') as allstu,"
                        . "(select count(lc.userid) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                        . " AND lcl.parentcourseid = $course->parentcourseid  where lcl.courseid = lc.courseid and lc.status = 'apply' and lc.completionstatus = 1) as allcom";
                $result = $DB->get_record_sql($sql_result);

                if ($class->cca == 1) {
                    ?>
                                    <td><?php echo $class->classnum ?>기</td>
                                    <td><!--기수-->    
                                    <?php
                                    $type_arrs = array(
                                        1 => '임직원',
                                        2 => '멘토',
                                        3 => '교육생',
                                        4 => '임직원, 멘토',
                                        5 => '멘토, 교육생',
                                        6 => '임직원, 교육생',
                                        7 => '전체'
                                    );
                                    $completion_rate = (int) sprintf("%2.2f", ( $result->complete / $result->student) * 100);
                                    echo (empty($class->lcoclassobject)) ? $type_arrs[$class->lcoclasstype] : $type_arrs[$class->lcoclassobject];
                                    ?>
                                    </td>                    
                                    <td><?php echo $result->student ?></td>
                                    <td><?php echo $result->complete ?></td>
                                    <td><?php echo $completion_rate ?>%</td>
                                    <td><?php echo $completion_rate ?>%</td></tr>                     
                    <?php
                } else if ($ccc > $class->cca) {
                    ?>
                                <td><?php echo $class->classnum ?>기</td>
                                <td><!--기수-->    
                                <?php
                                $type_arrs = array(
                                            1 => '임직원',
                                            2 => '멘토',
                                            3 => '교육생',
                                            4 => '임직원, 멘토',
                                            5 => '멘토, 교육생',
                                            6 => '임직원, 교육생',
                                            7 => '전체'
                                        );
                                $completion_rate = (int) sprintf("%2.2f", ( $result->complete / $result->student) * 100);

                                echo (empty($class->lcoclassobject)) ? $type_arrs[$class->lcoclasstype] : $type_arrs[$class->lcoclassobject];
                                ?>
                                </td>                    
                                <td><?php echo $result->student ?></td>
                                <td><?php echo $result->complete ?></td>
                                <td><?php echo $completion_rate ?>%</td>
                                </tr>

                    <?php
                    $ccc = $ccc + 1;
                } else {
                    ?>
                                <td><?php echo $class->classnum ?>기</td>
                                <td><!--기수-->    
                                <?php
                                $type_arrs = array(
                                            1 => '임직원',
                                            2 => '멘토',
                                            3 => '교육생',
                                            4 => '임직원, 멘토',
                                            5 => '멘토, 교육생',
                                            6 => '임직원, 교육생',
                                            7 => '전체'
                                        );
                                $completion_rate = (int) sprintf("%2.2f", ( $result->complete / $result->student) * 100);
                                $all = (int) sprintf("%2.2f", ( $result->allcom / $result->allstu) * 100);
                                echo (empty($class->lcoclassobject)) ? $type_arrs[$class->lcoclasstype] : $type_arrs[$class->lcoclassobject];
                                ?>
                                </td>                    
                                <td><?php echo $result->student ?></td>
                                <td><?php echo $result->complete ?></td>
                                <td><?php echo $completion_rate ?>%</td>
                                <td rowspan="<?php echo $ccc ?>"><?php echo $all ?>%</td>
                                </tr>
                                    <?php
                                    $ccc = $ccc + 1;
                                }
                            }
                        }
                    }
                }

                    if (empty($courses)) {
                        echo '<tr><td colspan="15">' . get_string('nodata', 'local_lmsdata') . '</td></tr>';
                    }
                    ?>    
            </table><!--Table End-->
            <div id="btn_area">            

            </div>
                <?php
//            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
                ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {

    $date = date('Y-m-d', time());
    $filename = 'edu_operation_status_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;

    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    foreach ($class_datas as $class_data) {
           $sql_se1 = "select lcl.courseid, lc.roughprocess as rough FROM {lmsdata_class} lcl JOIN {lmsdata_course} lc on lc.roughprocess = $class_data->id WHERE lcl.classyear = $classyear and lcl.parentcourseid = lc.id"; 
           $cc1 = $DB->get_records_sql($sql_se1);
           $cco1 = count($cc1);

           $sql_class1 = "SELECT distinct cc.name, lc.id ,lcl.courseid, cc.id, lcl.classyear, lcl.parentcourseid , ca2.name ca2name , ca2.id ca2id
            FROM {course_categories} cc 
            join {lmsdata_course} lc ON lc.detailprocess = cc.id
            join {lmsdata_class} lcl on lcl.parentcourseid = lc.id
            left join {course_categories} ca2 ON cc.parent = ca2.id
            where lcl.classyear = $classyear and  lc.coursearea = $class_data->id ORDER BY cc.sortorder asc";

           $classnum_datas =$DB->get_records_sql($sql_class1); 
           
           $col = 0;

           $worksheet[0]->set_column(0, 0, 10, array('v_align' => 'center'));
           $worksheet[0]->merge_cells(0, 0, 0, 1);
           $worksheet[0]->write($row, $col++, $class_data->name);
           $worksheet[0]->merge_cells($row, 0, $row + $cco1 - 1, 0);

            foreach ($classnum_datas as $classnum_data) {                
                $sql_select = "SELECT distinct lc.coursename, lc.id ,lcl.courseid, cc.id,cc.NAME, lcl.classyear, lcl.parentcourseid 
                FROM {course_categories} cc 
                join {lmsdata_course} lc ON lc.detailprocess = cc.id
                join {lmsdata_class} lcl on lcl.parentcourseid = lc.id
                where lcl.classyear = $classyear and cc.id = $classnum_data->id";

                $courses = $DB->get_records_sql($sql_select); 

                $sql_se = "select lcl.courseid, lc.detailprocess as detail FROM m_lmsdata_class lcl JOIN m_lmsdata_course lc on lc.detailprocess = $classnum_data->id "
                        . "WHERE lcl.classyear = $classyear and lcl.parentcourseid = lc.id";
                $cc = $DB->get_records_sql($sql_se);
                $cco = count($cc);
                $col = 1;

                if ($classnum_data->ca2name == '필수과정') {
                     $worksheet[0]->set_column(0, 1, 10, array('v_align' => 'center'));  
                    $worksheet[0]->write($row, $col++, $classnum_data->name);                                     
                    $worksheet[0]->merge_cells($row, 1, $row + $cco - 1, 1);
                } else {                    
                    $worksheet[0]->set_column(0, 1, 10, array('v_align' => 'center'));
                    $worksheet[0]->write($row, $col++, '');
                    $worksheet[0]->merge_cells($row, 1, $row + $cco - 1, 1);
                }
                
                foreach ($courses as $course) {
                    
                    $sql_class = "SELECT lcl.courseid,lcl.classnum ,lco.classobject lcoclassobject, lco.classtype lcoclasstype,               
                                      (select count(*) FROM {lmsdata_class} lcl where lcl.classyear = $classyear and lcl.parentcourseid = $course->parentcourseid ) as cca
                                      from m_lmsdata_course lco 
                                      join {lmsdata_class} lcl
                                      join {course_categories} cc
                                      where lcl.classyear = $classyear and lcl.parentcourseid = $course->parentcourseid and lco.id = $course->parentcourseid  ";
                    $classes = $DB->get_records_sql($sql_class);
                    $ccc = count($classes);
                    $col = 2;

                    $worksheet[0]->write($row, $col++, $course->coursename);
                    $worksheet[0]->set_column(0, 2, 25, array('v_align' => 'center'));
                    $worksheet[0]->merge_cells($row, 2, $row + $ccc - 1, 2);
                    foreach ($classes as $class) {
                        $sql_result = "SELECT (select count(*) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                                . " AND lcl.parentcourseid = $course->parentcourseid  and lcl.classnum = $class->classnum where lcl.courseid = lc.courseid and lc.status = 'apply') as student,"
                                . "(select count(*) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                                . " AND lcl.parentcourseid = $course->parentcourseid  and lcl.classnum = $class->classnum where lcl.courseid = lc.courseid and lc.status = 'apply' and lc.completionstatus = 1) as complete,"
                                . "(select count(lc.userid) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                                . " AND lcl.parentcourseid = $course->parentcourseid  where lcl.courseid = lc.courseid and lc.status = 'apply') as allstu,"
                                . "(select count(lc.userid) from {lmsdata_course_applications} lc JOIN  {lmsdata_class} lcl ON lcl.classyear = $classyear"
                                . " AND lcl.parentcourseid = $course->parentcourseid  where lcl.courseid = lc.courseid and lc.status = 'apply' and lc.completionstatus = 1) as allcom";
                        $result = $DB->get_record_sql($sql_result);
                        $col = 3;
                        $worksheet[0]->write($row, $col++, $class->classnum . '기');
                        if ($class->cca == 1) {
                            $type_arrs = array(
                                                    1 => '임직원',
                                                    2 => '멘토',
                                                    3 => '교육생',
                                                    4 => '임직원, 멘토',
                                                    5 => '멘토, 교육생',
                                                    6 => '임직원, 교육생',
                                                    7 => '전체'
                                                );
                            $completion_rate = (int) sprintf("%2.2f", ( $result->complete / $result->student) * 100);
                            $col = 4;
                            $worksheet[0]->write($row, $col++, (empty($class->lcoclassobject)) ? $type_arrs[$class->lcoclasstype] : $type_arrs[$class->lcoclassobject]);
                            $worksheet[0]->write($row, $col++, $result->student);
                            $worksheet[0]->write($row, $col++, $result->complete);
                            $worksheet[0]->write($row, $col++, $completion_rate . '%');
                            $worksheet[0]->write($row, $col++, $completion_rate . '%');
                        } else if ($ccc > $class->cca) {
                            $type_arrs = array(
                                                    1 => '임직원',
                                                    2 => '멘토',
                                                    3 => '교육생',
                                                    4 => '임직원, 멘토',
                                                    5 => '멘토, 교육생',
                                                    6 => '임직원, 교육생',
                                                    7 => '전체'
                                                );
                            $completion_rate = (int) sprintf("%2.2f", ( $result->complete / $result->student) * 100);
                            $col = 4;
                            $worksheet[0]->write($row, $col++, (empty($class->lcoclassobject)) ? $type_arrs[$class->lcoclasstype] : $type_arrs[$class->lcoclassobject]);
                            $worksheet[0]->write($row, $col++, $result->student);
                            $worksheet[0]->write($row, $col++, $result->complete);
                            $worksheet[0]->write($row, $col++, $completion_rate . '%');
                            $ccc = $ccc + 1;
                        } else {
                            $type_arrs = array(
                                                    1 => '임직원',
                                                    2 => '멘토',
                                                    3 => '교육생',
                                                    4 => '임직원, 멘토',
                                                    5 => '멘토, 교육생',
                                                    6 => '임직원, 교육생',
                                                    7 => '전체'
                                                );
                            $completion_rate = (int) sprintf("%2.2f", ( $result->complete / $result->student) * 100);
                            $all = (int) sprintf("%2.2f", ( $result->allcom / $result->allstu) * 100);
                            $col = 4;
                            $worksheet[0]->write($row, $col++, (empty($class->lcoclassobject)) ? $type_arrs[$class->lcoclasstype] : $type_arrs[$class->lcoclassobject]);
                            $worksheet[0]->write($row, $col++, $result->student);
                            $worksheet[0]->write($row, $col++, $result->complete);
                            $worksheet[0]->write($row, $col++, $completion_rate . '%');
                            $worksheet[0]->set_column($row, 8, 10, array('v_align' => 'center'));
                            $worksheet[0]->write($row, $col++, $all . '%');
                            $worksheet[0]->merge_cells($row, 8, $row + $ccc - 1, 8);
                            $ccc = $ccc + 1;
                        }
                        $row++;
                }
            }
        }        
    }    
    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }
</script>    
