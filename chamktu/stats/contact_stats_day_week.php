<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);

require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname (__FILE__)).'/lib/paging.php';
require_once dirname(dirname (__FILE__)).'/lib.php';



// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/stats/contact_stats_day_week.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage     = optional_param('page', 1, PARAM_INT);
$perpage      = optional_param('perpage', 31, PARAM_INT);
$year         = optional_param('year', date("Y"), PARAM_INT);
$mon          = optional_param('mon', 0, PARAM_INT);
$week         = optional_param('week', 0, PARAM_INT);
$target         = optional_param('target', 'all', PARAM_RAW);
$username     = optional_param('username', null, PARAM_RAW);


if(!$mon){
    $date = date("Y-m");
    $mon = date("m");
}else{
    $date = $year.'-'.$mon;
} 

$sql_select  = "SELECT CONCAT(FROM_UNIXTIME(lsl.log_date,'%w'),mobile) as dt,FROM_UNIXTIME(lsl.log_date,'%w') as day,mobile, COUNT(*) AS count , lsl.log_date ";

$sql_from    = " FROM {siteadmin_loginfo} lsl
                 JOIN {user} mu on mu.username = lsl.username 
                 JOIN {lmsdata_user} lu on lu.userid = mu.id ";

$page_params = array();
$sql_where = '';
$conditions = array('action = :isloggedin');
$param['isloggedin'] = 'logged';

$date_start =  strtotime($date.' 00:00:00');

$last_day = date('t',$date_start);    
$date_end =  strtotime($date.'-'.$last_day .' 23:59:59');

$week_array = array();

$start_day_week = date("w",$date_start);

$first_week = 6-$start_day_week+1;

$remainder_day  = $last_day - $first_week;

$second_week = $first_week+1;

$week_array[] = array('startday'=>$date_start, 'endday'=>strtotime($date.'-'.$first_week .' 23:59:59'));
for($i = $second_week; $i<=$last_day; $i = $i+7){
    $week_last_day = $i+6;
    if($week_last_day > $last_day){
        $week_last_day = $last_day;
    }
    $week_array[] = array('startday'=>strtotime($date.'-'.$i .' 00:00:00'), 'endday'=>strtotime($date.'-'.$week_last_day .' 23:59:59'));
}

foreach($week_array as $week_num=>$week_day){
    if($week==0 && $week_day['startday'] <= time() && $week_day['endday'] >= time()){
        $week = $week_num+1;
    }
}

$current_week = $week-1;

if($target != 'all'){
    if($target == 'target'){
        if(!empty($username)){
            $conditions[] = "mu.username = :username";
            $param['username'] = $username;
        }
    } else {
        $conditions[] = "lu.usergroup = :usergroup";
        $param['usergroup'] = $target;
    }
}

$conditions[] = "(lsl.log_date > :date_start AND lsl.log_date < :date_end)";
$param['date_start'] = $week_array[$current_week]['startday'];
$param['date_end'] = $week_array[$current_week]['endday'];
    
if($conditions) $sql_where = ' WHERE '.implode(' AND ',$conditions);
$sql_groupby = " GROUP BY FROM_UNIXTIME(lsl.log_date,'%w'),mobile";

$contact_stats = $DB->get_records_sql($sql_select.$sql_from.$sql_where.$sql_groupby, $param, ($currpage-1)*$perpage, $perpage);

$datas = array();
foreach ($contact_stats as $contact) {
    $datas[number_format($contact->day)][$contact->mobile] = $contact->count;
}

for ($i = 0; $i <= 6; $i++) {
        $day = get_string('day_week'.$i, 'local_lmsdata');
            $js_string .= '{
                        "day": "' . $day . '",';
            if(isset($datas[$i]['P'])){
                 $js_string .= '"PC" : '.$datas[$i]['P'].',';
            } else {
                $js_string .= '"PC" : 0,';
            }
            
            if(isset($datas[$i]['M'])){
                $js_string .= '"Mobile" : '.$datas[$i]['M'].',';
            } else {
                 $js_string .= '"Mobile" : 0,';
            }
            
            if(isset($datas[$i]['T'])){
                $js_string .= '"Tablet" : '.$datas[$i]['T'].',';
            } else {
                 $js_string .= '"Tablet" : 0,';
            }

            $js_string .= '},';
}

$js = array(
    $CFG->wwwroot.'/chamktu/manage/course_list.js'
);
?>

<?php include_once ('../inc/header.php');?>
<div id="contents">
    <?php include_once ('../inc/sidebar_stats.php');?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('stats_loginstats', 'local_lmsdata'); ?></h3>
        <p class="page_sub_title"> <?php echo get_string('stats_longtext1', 'local_lmsdata'); ?></p>
        <div class="siteadmin_tabs">
            <a href="contact_stats_day.php"><p class="black_btn"><?php echo get_string('stats_daily', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_day_week.php"><p class="black_btn black_btn_selected"><?php echo get_string('stats_day_week', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_week.php"><p class="black_btn"><?php echo get_string('stats_week', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_month.php"><p class="black_btn"><?php echo get_string('stats_monthly', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_year.php"><p class="black_btn"><?php echo get_string('stats_yearly', 'local_lmsdata'); ?></p></a>
            <a href="contact_stats_term.php"><p class="black_btn"><?php echo get_string('stats_periodsearch', 'local_lmsdata'); ?></p></a>
        </div>
        <div class="page_navbar"><a href="./contact_stats_day_week.php"><?php echo get_string('stats_management', 'local_lmsdata'); ?></a> > <strong><?php echo get_string('stats_loginstats', 'local_lmsdata'); ?></strong></div>
        
        <form name="" id="course_search" class="search_area" action="contact_stats_day_week.php" method="get">
            <input type="hidden" name="page" value="1" />
            
            <select name="year" class="w_80">
                <?php 
                $years = lmsdata_get_years();
                foreach($years as $v=>$y) {
                    $selected = '';
                    if($v == $year) {
                        $selected = ' selected';
                    }
                    echo '<option value="'.$v.'"'.$selected.'> '.$y.'</option>';
                }
                ?>
            </select>
            <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="mon" class="w_80" onchange="setweekselect('<?php echo $year;?>'+this.value)">
            <?php 
                $mons = lmsdata_get_mons($mon);
                echo $mons;
                ?>
            </select>
            <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="week" style="width:50px;">
            </select>
            <label><?php echo get_string('contents_week', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="target" onchange="setusernameinput();">
                <option value="all" <?php if($target == 'all') echo 'selected';?>><?php echo get_string('all', 'local_lmsdata'); ?></option>
                <option value="rs" <?php if($target == 'rs') echo 'selected';?>><?php echo get_string('role:rs', 'local_lmsdata'); ?></option>
                <option value="pr" <?php if($target == 'pr') echo 'selected';?>><?php echo get_string('role:pr', 'local_lmsdata'); ?></option>
                <option value="target" <?php if($target == 'target') echo 'selected';?>><?php echo get_string('specific_target', 'local_lmsdata'); ?></option>
            </select>
            <input value="<?php echo $username;?>" type="text" name="username" onclick="search_prof_popup()"> 
            <input type="submit" class="blue_btn" value="<?php echo get_string('stats_search', 'local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/>          
        </form><!--Search Area2 End-->
         <!-- Styles -->
        <style>
            #chartdiv {
                width	: 100%;
                height	: 500px;
            }					
                        .amcharts-main-div { 
                margin-top: 192px;
            }
        </style>

        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
        <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

        <!-- Chart code -->
        <script>
            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": [<?php echo $js_string; ?>],
                "valueAxes": [{
                        "integersOnly": true,
                        "minimum": 0,
                        "axisAlpha": 0,
                        "dashLength": 5,
                        "gridCount": 10,
                        "position": "left",
                        "title": ""
                    }],
                "startDuration": 0.5,
                "graphs": [
                    {
                        "balloonText": "[[value]]",
                        "bullet": "round",
                        "title": "Mobile",
                        "valueField": "Mobile",
                        "fillAlphas": 0
                    },{
                        "balloonText": "[[value]]",
                        "bullet": "round",
                        "title": "PC",
                        "valueField": "PC",
                        "fillAlphas": 0
                    },{
                        "balloonText": "[[value]]",
                        "bullet": "round",
                        "title": "Tablet",
                        "valueField": "Tablet",
                        "fillAlphas": 0
                    }
                ],
                "chartCursor": {
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "day",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "fillAlpha": 0.05,
                    "fillColor": "#000000",
                    "gridAlpha": 0,
                    "position": "top"
                },
  "export": {
    "enabled": true
  }

            });

        </script>
        <div id="chartdiv"></div>
    </div><!--Content End-->
    
</div> <!--Contents End-->
<script type="text/javascript">    
    $(window).load(function(){
        setweekselect();
        setusernameinput();
    });
    function course_edit_popup(id) {
        var tag = $("<div></div>");
        $.ajax({
          url: '<?php echo $SITECFG->wwwroot.'/chamktu/stats/course_form.php'; ?>',
          data: {
              parent: $('[name=parent]').val(),
              category: $('[name=category]').val(),
              id: id
          },
          success: function(data) {
            tag.html(data).dialog({
                title: '<?php echo get_string('stats_learningactivitystatus', 'local_lmsdata'); ?>',
                modal: true,
                width: 600,
                maxHeight: getWindowSize().height - 20,
                close: function () {
                    $( this ).dialog('destroy').remove()
                }
            }).dialog('open');
          }
        });
    }
        function course_all_excel() {
        <?php
        $query_string = '';
        if(!empty($param)) {
            $query_array = array();
            foreach($param as $key=>$value) {
                $query_array[] = urlencode( $key ) . '=' . urlencode( $value );
            }
            $query_string = '?'.implode('&', $query_array);
        }
        ?>
        var url = "course_all.excel.php<?php echo $query_string; ?>";
        
        document.location.href = url;
    }
    function contact_log(time) {
        var tag = $("<div id='contact_log_popup'></div>");
        $.ajax({
          url: '<?php echo $SITECFG->wwwroot.'/chamktu/stats/contact_view.ajax.php'; ?>',
          method: 'POST',
          data: {
              time: time,
              timestart : <?php echo $date_start?>,
              timeend : <?php echo $date_end?>,
              stat : 'month'
          },
          success: function(data) {
            tag.html(data).dialog({
                title: '접속로그',
                modal: true,
                width: 800,
                maxHeight: getWindowSize().height - 20,
                buttons: [ {id:'close',
                            text:'닫기', 
                            disable: true,
                            click: function() {
                                $( this ).dialog( "close" );
                            }}],
                open : function() {
                    var t = $(this).parent();
                    var w = $(window);
                    var s = getWindowSize();
                    
                    var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                    if(x < 0) x = 0;
                    var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                    if(y < 0) y = 0;
                    t.offset({
                        top: y,
                        left: x
                    });
                },
                close: function () {
                    $( this ).dialog('destroy').remove()
                }
            }).dialog('open');
          }
        });
    }
    
    function getWeekCountOfMonth(dateStr) {
        var year  = Number(dateStr.substring(0, 4));
        var month = Number(dateStr.substring(4, 6));

        var nowDate = new Date(year, month-1, 1);

        var lastDate = new Date(year, month, 0).getDate();
        var monthSWeek = nowDate.getDay();

        var weekSeq = parseInt((parseInt(lastDate) + monthSWeek - 1)/7) + 1;

        return weekSeq;
    }
    
    function setweekselect(){
        var cu_week = <?php echo $week;?>;
        var date = $('select[name=year]').val()+$('select[name=mon]').val();
        var max_week = getWeekCountOfMonth(date);
        var options = '';
        for(var i=1; i <= max_week; i++){
            var selected = '';
            if(cu_week == i){
                var selected = 'selected';
            }
            options += '<option value="'+i+'" '+selected+'>'+i+'</option>';
        }
        $('select[name=week]').html(options);
    }
    
    function setusernameinput(){
        if($('select[name=target]').val() == 'target'){
            $('input[name=username]').css('display', '');
        } else {
            $('input[name=username]').css('display', 'none');
        }
    }
    
    function search_prof_popup() {
        var tag = $("<div id='search_user_popup'></div>");
        $.ajax({
          url: '<?php echo $CFG->wwwroot.'/chamktu/stats/search_user.php'; ?>',
          method: 'POST',
          success: function(data) {
            tag.html(data).dialog({
                title: '<?php echo get_string('user_search','local_lmsdata'); ?>',
                modal: true,
                width: 800,
                resizable: false,
                height: 400,
                buttons: [ {id:'close',
                            text:'<?php echo get_string('cancle','local_lmsdata'); ?>',
                            disable: true,
                            click: function() {
                                $( this ).dialog( "close" );
                            }}],
                close: function () {
                    $('#frm_search_user').remove();
                    $( this ).dialog('destroy').remove()
                }
            }).dialog('open');
          }
        });
    }
</script>
 <?php include_once ('../inc/footer.php');?>

