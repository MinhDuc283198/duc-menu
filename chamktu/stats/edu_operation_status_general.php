<?php
/**
 * 교육운영현황 정기교육 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$classyear = optional_param('classyear', date('Y', time()), PARAM_INT);
$mon = optional_param('mon', date('m', time()), PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터
if ($classyear) {
    $sql_where .= " and YEAR(from_unixtime(lca.timecreated)) = $classyear";
}

//$sql_select = "select lco.id, lco.coursename ";
//$sql_from = " from m_lmsdata_course lco
//    where lco.isused = 0 ";
$sql_select = "select lco.id, lco.coursename ";
$sql_from = " from {lmsdata_course_applications} lca
join {lmsdata_class} lc on lc.id = lca.courseid
join {lmsdata_course} lco on lco.id = lc.parentcourseid
where lco.isused = 0 $sql_where and lc.classtype = 2
group by lc.classnum, lco.coursename ";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_orderby, $params);
$classnum_datas = $DB->get_records_sql("select * from {lmsdata_class} where classyear = $classyear and  classnum = 2 order by classnum asc ");


$datas = array();
$tables = array();

$fields = array(
    '과정명',
    '구분'
);
foreach ($classnum_datas as $classnum_data) {
    $tableth .= "<th scope='row' width='3%'>$classnum_data->classnum" . "기</th>";
    array_push($fields, $classnum_data->classnum.'기'); 
}
array_push($fields, '합계'); 
//print_object($datas);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">교육운영현황</h3>
            <br/><br/><br/>
            <input type="button"  class="" onclick="javascript:location.href = 'edu_operation_status.php'" value="전체"/>
            <input type="button"  class="orange_btn" onclick="javascript:location.href = 'edu_operation_status_general.php'" value="멘토"/>
            <input type="button"  class="" onclick="javascript:location.href = 'edu_operation_status_student.php'" value="임직원"/>
            <form name="course_search" id="course_search" class="search_area" action="edu_operation_status_general.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="$('form.search_area').submit();">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2018, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select> 
                </div>
                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="15%">과정명</th>
                        <th scope="row" width="3%">구분</th>
                        <?php
                        echo $tableth;
                        ?>
                        <th scope="row" width="3%">합계</th>

                    </tr>
                </thead>
                <?php
                foreach ($courses as $course) {
                    foreach ($classnum_datas as $classnum_data) {
                        $signup = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
                        where lca.status = 'apply' and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum ");
                        $complete = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
                        where lca.status = 'apply' and lca.completionstatus = 1 and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum");
                        $datas[$classnum_data->classnum]['signup'] = $signup;
                        $datas[$classnum_data->classnum]['complete'] = $complete;
                    }
                    ?>

                    <tr>
                        <td rowspan="3"><?php echo $course->coursename ?></td>
                        <td>수강인원</td>
                        <?php
                        $totalsignup = '';
                        foreach ($datas as $data => $val) {
                            $totalsignup += $val['signup'];
                            echo "<td>" . $val['signup'] . "</td>";
                        }
                        echo "<td>" . $totalsignup . "</td>";
                        ?>
                    </tr>
                    <tr>
                        <td>수료인원</td>
                        <?php
                        $totalcomplete = '';
                        foreach ($datas as $data => $val) {
                            $totalcomplete += $val['complete'];
                            echo "<td>" . $val['complete'] . "</td>";
                        }
                        echo "<td>" . $totalcomplete . "</td>";
                        ?>
                    </tr>
                    <tr>
                        <td>이수율</td>
                        <?php
                        $totalcountcompletion = (int) sprintf("%2.2f", ( $totalcomplete / $totalsignup) * 100);
                        foreach ($datas as $data => $val) {
                            $completion_rate = (int) sprintf("%2.2f", ( $val['complete'] / $val['signup']) * 100);
                            echo "<td>" . $completion_rate . "%</td>";
                        }
                        echo "<td>" . $totalcountcompletion . "%</td>";
                        ?>
                    </tr>                  

                    <?php
                }
                
                if(empty($courses)) {
                    echo '<tr><td colspan="15">'.get_string('nodata', 'local_lmsdata').'</td></tr>';
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">            

            </div>
            <?php
//            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {

    $date = date('Y-m-d', time());
    $filename = 'edu_operation_status_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    foreach ($courses as $course) {
        foreach ($classnum_datas as $classnum_data) {
            $signup = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
            where lca.status = 'apply' and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum ");
            $complete = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
            where lca.status = 'apply' and lca.completionstatus = 1 and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum");
            $datas[$classnum_data->classnum]['signup'] = $signup;
            $datas[$classnum_data->classnum]['complete'] = $complete;

        }
        $col = 0;
        $worksheet[0]->write($row, $col++, $course->coursename);
        $worksheet[0]->merge_cells($row, 0, $row+2, 0);
        $worksheet[0]->set_column(0, 0, 50);
        $worksheet[0]->write($row, $col++, '수강인원');
        $totalsignup = '';
        foreach ($datas as $data => $val) {
            $totalsignup += $val['signup'];
            $worksheet[0]->write($row, $col++, $val['signup']);
        }
        $worksheet[0]->write($row, $col++, $totalsignup);
        
        $row++;
        $col = 1;
        $worksheet[0]->write($row, $col++, '수료인원');
        $totalcomplete = '';
        foreach ($datas as $data => $val) {
            $totalcomplete += $val['complete'];
            $worksheet[0]->write($row, $col++, $val['complete']);
        }
        $worksheet[0]->write($row, $col++, $totalcomplete);
        
        $row++;
        $col = 1;
        $worksheet[0]->write($row, $col++, '이수율');
        $totalcountcompletion = (int) sprintf("%2.2f", ( $totalcomplete / $totalsignup) * 100);
        foreach ($datas as $data => $val) {
            $completion_rate = (int) sprintf("%2.2f", ( $val['complete'] / $val['signup']) * 100);
            $worksheet[0]->write($row, $col++, $completion_rate.'%');
        }
        $worksheet[0]->write($row, $col++, $totalcountcompletion.'%');
        
       
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }
</script>    
