<?php
/**
 * 케리스 운영실적 보고 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$classyear = optional_param('classyear', date('Y', time()), PARAM_INT);
$mon = optional_param('mon', date('m', time()), PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터
if ($classyear) {
    $sql_where1 .= " and YEAR(from_unixtime(lca1.timecreated)) = $classyear";
    $sql_where2 .= " where lc.classyear = $classyear";
}

$sql_select = "select lco.id, ca.id caid, ca2.name ca2name, ca.name ca1name, count(lc.id) count 
,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and co1.category = co.category and (code1.codename like '%초등%' or code1.codename like '%유치원%' ) $sql_where1 ) count1
 ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and co1.category = co.category and (code1.codename like '%중등%') $sql_where1  ) count2
 ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and co1.category = co.category and (code1.codename NOT like '%초등%' and code1.codename NOT like '%유치원%' and code1.codename NOT like '%중등%') $sql_where1   ) count3
  ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and co1.category = co.category $sql_where1 ) totalcount
 ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and lca1.completionstatus = 1 and co1.category = co.category and (code1.codename like '%초등%' or code1.codename like '%유치원%') $sql_where1  ) complete1
 ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and lca1.completionstatus = 1 and co1.category = co.category and (code1.codename like '%중등%') $sql_where1  ) complete2
 ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and lca1.completionstatus = 1 and co1.category = co.category and (code1.codename NOT like '%초등%' and code1.codename NOT like '%유치원%' and code1.codename NOT like '%중등%')  $sql_where1  ) complete3
  ,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and lca1.completionstatus = 1 and co1.category = co.category $sql_where1 ) totalcomplete
";
$sql_from = "
            from m_lmsdata_class lc
            join m_lmsdata_course lco on lc.parentcourseid = lco.id
            join m_course co on lco.courseid = co.id 
            JOIN m_course_categories ca ON ca.id = co.category 
            left JOIN m_course_categories ca2 ON ca.parent = ca2.id
            $sql_where2
            group by ca2name, ca1name ";

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_orderby, $params);

$datas = array();
$cnt = 0;
foreach ($courses as $course){
    $datas[$course->ca2name][$course->ca1name]['count'] = $course->count;
    $datas[$course->ca2name][$course->ca1name]['A'] = $course->count1;
    $datas[$course->ca2name][$course->ca1name]['B'] = $course->count2;
    $datas[$course->ca2name][$course->ca1name]['C'] = $course->count3;
    $datas[$course->ca2name][$course->ca1name]['D'] = $course->totalcount;
    $datas[$course->ca2name][$course->ca1name]['E'] = $course->complete1;
    $datas[$course->ca2name][$course->ca1name]['F'] = $course->complete2;
    $datas[$course->ca2name][$course->ca1name]['G'] = $course->complete3;
    $datas[$course->ca2name][$course->ca1name]['H'] = $course->totalcomplete;
    
}
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">KERIS보고(운영실적)</h3>
            <br/><br/><br/>
            <input type="button"  class="orange_btn" onclick="javascript:location.href = 'edu_keris_report.php'" value="운영실적"/>
            <input type="button"  class="" onclick="javascript:location.href = 'edu_keris_report_detail.php'" value="세부내용"/>
            <form name="course_search" id="course_search" class="search_area" action="edu_keris_report.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="$('form.search_area').submit();">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2017, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select>
                </div>
                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="15%" rowspan="2">영역</th>
                        <th scope="row" width="15%" rowspan="2">분류</th>
                        <th scope="row" width="10%" rowspan="2">개설과정수</th>
                        <th scope="row" width="20%" colspan="4">
                            신청자
                        </th>
                        <th scope="row" width="20%" colspan="4">
                            이수자
                        </th>
                        <th scope="row" width="10%" rowspan="2">
                            이수율
                        </th>
                    </tr>
                    <tr>
                        <th>초등</th>
                        <th>중등</th>
                        <th>기타(교육전문직 등)</th>
                        <th>계</th>
                        <th>초등</th>
                        <th>중등</th>
                        <th>기타(교육전문직 등)</th>
                        <th>계</th>
                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="11">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    foreach ($datas as $key => $val) {
                        echo "<tr><td rowspan='".count($val)."'>$key</td>";
                        foreach ($val as $a => $b){
                            $completion_rate = (int) sprintf("%2.2f", ( $b['H'] / $b['D']) * 100);
                            echo "<td>$a</td>";
                            echo "<td>".$b['count']."</td>";
                            echo "<td>".$b['A']."</td>";
                            echo "<td>".$b['B']."</td>";
                            echo "<td>".$b['C']."</td>";
                            echo "<td>".$b['D']."</td>";
                            echo "<td>".$b['E']."</td>";
                            echo "<td>".$b['F']."</td>";
                            echo "<td>".$b['G']."</td>";
                            echo "<td>".$b['H']."</td>";
                            echo "<td>".$completion_rate."%</td></tr>";
                        }
                    }
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">            
                
            </div>
            <?php
//            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $fields = array(
        '영역',
        '분류',
        '개설과정수',
        '신청자',
        '이수자',
        '이수율'
    );
    $fields2 = array(
        '초등',
        '중등',
        '기타(교육전문직 등)',
        '계'
    );

    $date = date('Y-m-d', time());
    $filename = 'edu_sales_status_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        if($fieldname == '신청자' || $fieldname == '이수자'){
            $worksheet[0]->merge_cells(0, $col, 0, $col+3);
            foreach ($fields2 as $fieldname2) {
                $worksheet[0]->write(1, $col, $fieldname2);
                $col++;
            }
        }else{
            $worksheet[0]->merge_cells(0, $col, 1, $col);
            $col++;
        }
        
    }
    $row = 2;
    
    foreach ($datas as $key => $val) {
        $col = 0;
        $worksheet[0]->merge_cells($row, $col, $row+count($val)-1, $col);
        $worksheet[0]->write($row, $col++, $key);
        foreach ($val as $a => $b){
            $col = 1;
            $completion_rate = (int) sprintf("%2.2f", ( $b['H'] / $b['D']) * 100);
            $worksheet[0]->write($row, $col++, $a);
            $worksheet[0]->write($row, $col++, $b['count']);
            $worksheet[0]->write($row, $col++, $b['A']);
            $worksheet[0]->write($row, $col++, $b['B']);
            $worksheet[0]->write($row, $col++, $b['C']);
            $worksheet[0]->write($row, $col++, $b['D']);
            $worksheet[0]->write($row, $col++, $b['E']);
            $worksheet[0]->write($row, $col++, $b['F']);
            $worksheet[0]->write($row, $col++, $b['G']);
            $worksheet[0]->write($row, $col++, $b['H']);
            $worksheet[0]->write($row, $col++, $completion_rate.'%');
            $row++;
        }
    }
    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }

</script>    
