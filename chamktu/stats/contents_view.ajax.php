<?php
require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

$userid = required_param('userid', PARAM_INT);
$timestart = required_param('timestart', PARAM_INT);
$timeend = required_param('timeend', PARAM_INT);
$username = optional_param('username', '', PARAM_RAW);

$select = " SELECT lp.id, lp.lcmsid, lp.positionfrom, lp.positionto, lp.timecreated, u.username, lu.usergroup, lc.con_name ";

//시간 제한 두기(정해진 기간 이내의 것만 나오도록. (where case when then else then)
$from = "FROM {lcms_playtime} lp
        JOIN {lcms_contents} lc on lc.id = lp.lcmsid
        JOIN {user} u on lp.userid = u.id
        JOIN {lmsdata_user} lu on lu.userid = u.id"; // 삭제된 사용자 기록은 제외
$conditions = array('lp.timecreated > :timestart1 AND lp.timecreated < :timeend1');
//$conditions = array('(case when lp.timecreated = "0" then (lt.timeview > :timestart AND lt.timeview < :timeend) ELSE (lp.timecreated > :timestart1 AND lp.timecreated < :timeend1) END)');
$param['timestart'] = $timestart;
$param['timeend'] = $timeend;
$param['timestart1'] = $timestart;
$param['timeend1'] = $timeend;

$conditions[] = "u.id = :uid";
$param['uid'] = $userid;
if (!empty($username)) {
    $conditions[] = "u.username = :username";
    $param['username'] = $username;
}

if ($conditions) {
    $where = ' WHERE ' . implode(' AND ', $conditions);
}
$sort = " order by lp.timecreated DESC";

$contacts = $DB->get_records_sql($select . $from . $where . $sort, $param);

//$totalcount = $DB->count_records_sql('SELECT count(*) ' . $from . $where, $param);
?>

<div class="popup_content" id="class_students">
    <h2>콘텐츠 시청 이력</h2>


    <form id="frm_class_students" name="frm_class_students" onsubmit="return false;">
        <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr>                
                    <th>콘텐츠명</th>
                    <th>아이디</th>
                    <th>구분</th>
                    <th>시작 시점</th>
                    <th>끝 시점</th>
                    <th>시청 시간</th>
                </tr>
                <?php
                foreach ($contacts as $contact) {

                    echo '<tr>';
                    echo '<td>' . $contact->con_name . '</td>';
                    echo '<td>' . $contact->username . '</td>';
                    if ($contact->usergroup == 'sa') {
                        echo '<td>관리자</td>';
                    } else if ($contact->usergroup == 'pr') {
                        echo '<td>임직원</td>';
                    } else if ($contact->usergroup == 'ad') {
                        echo '<td>멘토</td>';
                    } else if ($contact->usergroup == 'rs') {
                        echo '<td>교육생</td>';
                    } else {
                        echo '<td></td>';
                    }

                    if (gmdate('H', $contact->positionfrom) == 00) {
                        echo '<td>' . gmdate('i:s', $contact->positionfrom) . '</td>';
                    } else {
                        echo '<td>' . gmdate('H:i:s', $contact->positionfrom) . '</td>';
                    }
                    if (gmdate('H', $contact->positionto) == 00) {
                        echo '<td>' . gmdate('i:s', $contact->positionto) . '</td>';
                    } else {
                        echo '<td>' . gmdate('H:i:s', $contact->positionto) . '</td>';
                    }
                    if ($contact->timecreated == 0) {
                        echo '<td></td>';
                    } else {
                        echo '<td>' . date('Y.m.d H:i', $contact->timecreated) . '</td>';
                    }
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </form>

    <?php
//    if ($totalcount / $perpage > 1) {
//        print_paging_navbar_script($totalcount, $currpage, $perpage, 'javascript:class_students_search(:page);');
//    }
    ?>


