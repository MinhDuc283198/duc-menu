<?php
require_once (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");


// Check for valid admin user - no guest autologin
require_login(0, false);

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$page_num = optional_param('page_num', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$startyear = optional_param('startyear', 0, PARAM_INT);
$startmon = optional_param('startmon', 0, PARAM_INT);
$startday = optional_param('startday', 0, PARAM_INT);
$endyear = optional_param('endyear', 0, PARAM_INT);
$endmon = optional_param('endmon', 0, PARAM_INT);
$endday = optional_param('endday', 0, PARAM_INT);
$username = optional_param('username', null, PARAM_TEXT);
$excell = optional_param('excell', 0, PARAM_INT);
$viewtype = optional_param('viewtype', '', PARAM_TEXT);
$viewtypesort = optional_param('viewtypesort', 'desc', PARAM_TEXT);
$target = optional_param('target', 'all', PARAM_TEXT);

if (!$year) {
    $year = date("Y");
}
if (!$startmon || !$startday || !$endmon || !$endday) {
    $date = date("Y-m-d");
    $startyear = date("Y");
    $startmon = date("m");
    $startday = date("d");
    $endyear = date("Y");
    $endmon = date("m");
    $endday = date("d");
    $startdate = $startyear . '-' . $startmon . '-' . $startday;
    $enddate = $endyear . '-' . $endmon . '-' . $endday;
} else {
    $startdate = $startyear . '-' . $startmon . '-' . $startday;
    $enddate = $endyear . '-' . $endmon . '-' . $endday;
}

$conditions = array();
$conditions1 = array();
$conditions2 = array();
$params = array();

if ($target == 'target' && $username) {
    $getuserid = $DB->get_record('user', array('username' => $username));
//    $conditions[] = '(lt.userid = :ltuid or lce.userid = :lceuid)';
    $conditions1[] = 'lce.userid = :lceuid';
    $conditions2[] = 'lce.userid = :lceuid';
    $params['ltuid'] = $getuserid->id;
    $params['lceuid'] = $getuserid->id;
}


if ($target) {
    if ($target != 'all') {
        if ($target == 'target') {
            $addquery1 = ' AND lt.userid = :ltuid';
            $addquery = ' AND lce.userid = :lceuid';
        } else {
            $addquery = 'JOIN {lmsdata_user} lu ON (lu.userid = lce.userid OR lt.userid = lu.userid)  ';
            $addcondition = " AND lu.usergroup = :luusergroup";
            $params['luusergroup'] = $target;
        }
    }
}
$page_params = array();


$date_start = strtotime($startdate . ' 00:00:00');
$date_end = strtotime($enddate . ' 23:59:59');
$params['date_start'] = $date_start;
$params['date_end'] = $date_end;
$params['date_start1'] = $date_start;
$params['date_end1'] = $date_end;
$params['reg_end'] = $date_end;



$sql_select = "SELECT con.*,  count(lt.lcms) as count, sum(lt.attempts) as attempts, round(avg(lce.stars),1) AS averagestars, count(distinct lce.userid) AS countusers ";
$count_select = "SELECT count(distinct con.con_name) ";
$total_select = " SELECT count(lt.lcms) as usercount, sum(attempts) as viewcount ";

$sql_from = "  FROM {lcms_repository} rep
                            JOIN {lcms_contents} con ON con.id= rep.lcmsid
                            JOIN {lcms_track} lt ON lt.lcms = con.id AND lt.timeview > :date_start AND lt.timeview < :date_end ".$addquery1."
                            LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq
                            LEFT JOIN {lcms_contents_evaluation} lce ON con.id = lce.contentid AND lce.timecreated > :date_start1 AND lce.timecreated < :date_end1 " . $addquery . "
                            WHERE con.reg_dt < :reg_end" . $addcondition;


$conditions1[] = "(lce.timecreated > :date_start AND lce.timecreated < :date_end)";

$sql_where = '';
$sql_where1 = '';
$sql_where2 = '';

if ($conditions) {
    $sql_where = ' AND ' . implode(' AND ', $conditions);
}
if ($conditions1) {
    $sql_where1 = ' AND ' . implode(' AND ', $conditions1);
}
if ($conditions2) {
    $sql_where2 = ' AND ' . implode(' AND ', $conditions2);
}

$sql_groupby = " GROUP BY con.id";

if ($viewtype) {
    if ($viewtype == 'contentsname') {
        $sql_orderby = " ORDER BY con.con_name " . $viewtypesort;
    } else if ($viewtype == 'usernum') {
        $sql_orderby = " ORDER BY count " . $viewtypesort;
    } else if ($viewtype == 'readnum') {
        $sql_orderby = " ORDER BY attempts " . $viewtypesort;
    } else if ($viewtype == 'staraverage') {
        $sql_orderby = " ORDER BY averagestars " . $viewtypesort;
    } else if ($viewtype == 'staraverage') {
        $sql_orderby = " ORDER BY averagestars " . $viewtypesort;
    } else if ($viewtype == 'starusernum') {
        $sql_orderby = " ORDER BY countusers " . $viewtypesort;
    }
} else {
    $sql_orderby = " ORDER BY attempts DESC";
}


$contact_stats = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_groupby . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_stats = $DB->count_records_sql($count_select . $sql_from . $sql_where, $params);

$total = $DB->get_record_sql($total_select . $sql_from , $params);
$total_count = $DB->count_records_sql('select count(*) from {lcms_contents} ');
$total_view_count = $DB->get_record_sql('select sum(attempts) as count from {lcms_track} ');



$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$excelurl = "contents_report.php?page=$currpage&startyear=$startyear&startmon=$startmon&startday=$startday&endyear=$endyear&endmon=$endmon&endday=$endday&excell=1&username=$username&viewtype=$viewtype&viewsort=$viewtypesort&target=$target";
if (!$excell) {
    ?>

    <?php include_once ('../inc/header.php'); ?>
    <div id="contents">
        <?php include_once ('../inc/sidebar_stats.php'); ?>
        <div id="content">
            <h3 class="page_title">콘텐츠 통계</h3>
            <div class="page_navbar"><a href="./contact_stats_day.php"><?php echo get_string('stats_management', 'local_lmsdata'); ?></a> > <strong>콘텐츠 통계</strong></div>

            <div class="count_area" style="float:left; margin: 4px 4px 10px 10px;" >전체 동영상수 : <?php echo number_format($total_count); ?> / 총 누적조회수 : <?php echo number_format($total_view_count->count); ?></div>
            <div class="siteadmin_tabs">
                <a href="contents_report.php"><p class="black_btn black_btn_selected">콘텐츠 목록</p></a>
                <a href="contents_report_records.php"><p class="black_btn">시청 이력</p></a>
            </div>
            <form name="" id="course_search" class="search_area" action="contents_report.php" method="get">
                <!--<input type="submit" onclick="location.href = '<?php echo $excelurl; ?>'" class="blue_btn" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?> " style="float:right;"/>-->                
                <input type="hidden" name="page" value="1" />
                <input type="hidden" name="viewtype" value="<?php echo $viewtype; ?>" />
                <input type="hidden" name="viewtypesort" value="<?php echo $viewtypesort; ?>" />



                <?php
                if ($viewall) {
                    $checked = 'checked';
                    $disabled = 'disabled';
                    $style = 'style="background-color: #c8c8c8"';
                } else {
                    $checked = '';
                    $disabled = '';
                    $style = '';
                }
                ?>

                <select name="startyear" class="w_80" <?php echo $disabled ?> <?php echo $style ?>>
                    <?php
                    $years = lmsdata_get_years();
                    foreach ($years as $v => $y) {
                        $selected = '';
                        if ($v == $startyear) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                    }
                    ?>
                </select>
                <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="startmon" class="w_80" <?php echo $disabled ?> <?php echo $style ?>>
                    <?php
                    $mons = lmsdata_get_mons($startmon);
                    echo $mons;
                    ?>
                </select>
                <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="startday" class="w_80" <?php echo $disabled ?> <?php echo $style ?>>
                    <?php
                    $days = lmsdata_get_days($startday);
                    echo $days;
                    ?>             
                </select>
                <label><?php echo get_string('contents_day', 'local_lmsdata'); ?> &nbsp; ~ </label>
                <select name="endyear" class="w_80" <?php echo $disabled ?> <?php echo $style ?>>
                    <?php
                    $years = lmsdata_get_years();
                    foreach ($years as $v => $y) {
                        $selected = '';
                        if ($v == $endyear) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                    }
                    ?>
                </select>
                <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="endmon" class="w_80" <?php echo $disabled ?> <?php echo $style ?>>
                    <?php
                    $mons = lmsdata_get_mons($endmon);
                    echo $mons;
                    ?>
                </select>
                <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="endday" class="w_80" <?php echo $disabled ?> <?php echo $style ?>>
                    <?php
                    $days = lmsdata_get_days($endday);
                    echo $days;
                    ?>
                </select>
                <label><?php echo get_string('contents_day', 'local_lmsdata'); ?> &nbsp;</label>

                <select name="target" onchange="setusernameinput();">
                    <option value="all" <?php if ($target == 'all') echo 'selected'; ?>><?php echo get_string('all', 'local_lmsdata'); ?></option>
                    <option value="sa" <?php if ($target == 'sa') echo 'selected'; ?>>관리자</option>
                    <option value="pr" <?php if ($target == 'pr') echo 'selected'; ?>>임직원</option>
                    <option value="ad" <?php if ($target == 'ad') echo 'selected'; ?>>멘토</option>
                    <option value="rs" <?php if ($target == 'rs') echo 'selected'; ?>>교육생</option>
                    <option value="target" <?php if ($target == 'target') echo 'selected'; ?>><?php echo get_string('specific_target', 'local_lmsdata'); ?></option>
                </select>
                <input value="<?php echo $username; ?>" type="text" name="username" onclick="search_usergroup_popup()" readonly> 

                <input type="submit" class="blue_btn" value="<?php echo get_string('stats_search', 'local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/> 

                                    <!--                &nbsp;&nbsp;&nbsp;<input type="checkbox" name="viewall" id="view_all" value="1" onclick="search_all()" <?php echo $checked ?>>전체보기-->
            </form><!--Search Area2 End-->
            <div style="float:right;">             
                <input type="submit" onclick="location.href = '<?php echo $excelurl; ?>'" class="blue_btn" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?> " style="margin-right: 10px;"/>          
            </div> 

            <table>
                <tr title="항목을 클릭하여 항목별로 정렬을 할 수 있습니다.">
                    <th style="width:8%;"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th style='width:30%;' id="contentsname">콘텐츠 명</th>
                    <th style="width:8%;" id="usernum">사용자 수</th>
                    <th style="width:8%;" id="readnum"><?php echo get_string('readnum', 'local_media'); ?></th>
                    <th style="width:8%;" id="staraverage">평균 별점</th>    
                    <th style="width:8%;" id="starusernum">별점부여자 수</th>      
                </tr>
                <?php if (!$contact_stats) { ?>
                    <tr>
                        <td colspan="24">통계 내용이 없습니다. </td>
                    </tr>
                    <?php
                } else {

                    $startnum = 1 + (($currpage - 1) * $perpage);
                    foreach ($contact_stats as $contact_stat) {
                        ?>                    
                        <tr>
                            <td><?php echo $startnum++; ?></td>
                            <td><?php echo $contact_stat->con_name; ?></td>
                            <td><?php echo $contact_stat->count; ?></td>                            
                            <td><?php
                                if ($contact_stat->attempts) {
                                    echo ($contact_stat->attempts);
                                } else {
                                    echo '0';
                                }
                                ?></td>                            
                            <td><?php
                                if ($contact_stat->averagestars) {
                                    echo ($contact_stat->averagestars);
                                } else {
                                    echo '-';
                                }
                                ?></td>                          
                            <td><?php
                                if ($contact_stat->countusers) {
                                    echo ($contact_stat->countusers);
                                } else {
                                    echo '-';
                                }
                                ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <td></td>
                    <td>종합 통계</td>
                    <td><?php echo $total->usercount; ?></td>
                    <td><?php echo empty($total->viewcount) ? '0' : $total->viewcount; ?></td>
                    <td><?php
                        $sql_select1 = "select contentid, ROUND(avg(lce.stars),1) as score , count(lce.userid) as star ";
                        $cs_from = " from {lcms_contents_evaluation} lce join {lcms_contents} lc on lc.id = lce.contentid 
                                                           WHERE lce.timecreated > :date_start AND lce.timecreated < :date_end ";


                        $count_cs = $DB->get_record_sql($sql_select1 . $cs_from . $sql_where2, $params);
                        if ($count_cs->score) {
                            echo ($count_cs->score);
                        } else {
                            echo '-';
                        }
                        ?></td>
                    <td><?php echo empty($count_cs->star) ? '-' : $count_cs->star; ?></td>
                </tr>                        
            </table><!--Table End-->
            <?php
            if (($count_stats / $perpage) > 1) {
                print_paging_navbar_script($count_stats, $currpage, $perpage, 'javascript:cata_page(:page);');
            }
            ?>
        </div><!--Content End-->

    </div> <!--Contents End-->
    <script type="text/javascript">

        $(window).ready(function () {
            var viewtype = '#' + '<?php echo $viewtype; ?>';
            var sort = '<?php echo $viewtypesort; ?>';
            var viewtypesort;
            if (sort == 'asc') {
                viewtypesort = '오름차순';
            } else {
                viewtypesort = '내림차순';
            }
            $(viewtype).attr("title", viewtypesort)

            setusernameinput();
        });

        $('th').click(function () {
            var viewtype = $(this).attr("id");
            var viewtypesort = $(this).attr("title");
            $('input[name=page]').val("<?php echo $currpage; ?>");
            $('input[name=viewtype]').val(viewtype);
            var sort;
            if (viewtypesort == '오름차순') {
                sort = 'desc';
            } else {
                sort = 'asc';

            }
            $('input[name=viewtypesort]').val(sort);
            $('#course_search').submit();
        });

        function search_all() {
            $('#course_search').attr('action', 'contents_report.php');
            $('#course_search').attr('method', 'get');
            $('#course_search').submit();
        }

        function cata_page(page) {
            $('input[name=page]').val(page);
            $('#course_search').submit();
        }

        function setusernameinput() {
            if ($('select[name=target]').val() == 'target') {
                $('input[name=username]').css('display', '');
            } else {
                $('input[name=username]').css('display', 'none');
            }
        }

        function search_usergroup_popup() {
            var tag = $("<div id='search_user_popup'></div>");
            $.ajax({
                url: '<?php echo $CFG->wwwroot . '/chamktu/stats/search_user.php'; ?>',
                method: 'POST',
                success: function (data) {
                    tag.html(data).dialog({
                        title: '<?php echo get_string('user_search', 'local_lmsdata'); ?>',
                        modal: true,
                        width: 800,
                        resizable: false,
                        height: 400,
                        buttons: [{id: 'close',
                                text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                                disable: true,
                                click: function () {
                                    $(this).dialog("close");
                                }}],
                        close: function () {
                            $('#frm_search_user').remove();
                            $(this).dialog('destroy').remove()
                        }
                    }).dialog('open');
                }
            });
        }
    </script>
    <?php
    include_once ('../inc/footer.php');
} else {
    $contact_stats = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_groupby . $sql_orderby, $params);
    include_once (dirname(dirname(__FILE__)) . '/inc/lib.php');
//    if(!$auth = is_siteadmin($USER)){
//        redirect($CFG->wwwroot);
//    }

    $fields = array(
        get_string('number', 'local_lmsdata'),
        '콘텐츠 명',
        '사용자 수',
        '조회수',
        '평균별점',
        '별점부여자 수'
    );

    $date = date('Y-m-d', time());
    $filename = '콘텐츠통계' . $date . '.xls';
    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    $row = 1;
    $worksheet[0]->set_column(1, 1, 50, array('v_align' => 'top')); // 콘텐츠 명
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }
    foreach ($contact_stats as $contact_stat) {


        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $contact_stat->con_name);
        $worksheet[0]->write($row, $col++, $contact_stat->count);
        $worksheet[0]->write($row, $col++, $contact_stat->attempts ? $contact_stat->attempts : '0');
        $worksheet[0]->write($row, $col++, $contact_stat->averagestars ? $contact_stat->averagestars : '-');
        $worksheet[0]->write($row, $col++, $contact_stat->countusers ? $contact_stat->countusers : '-');

        $row++;
    }

    $workbook->close();
    die;
}
