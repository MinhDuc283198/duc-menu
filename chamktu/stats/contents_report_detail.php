<?php
/**
 * 교육과정 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', date('Y', time()), PARAM_INT);
$mon = optional_param('mon', date('m', time()), PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터
if ($classyear) {
    $sql_where1 .= " and YEAR(from_unixtime(lca1.timecreated)) = $classyear";
    $sql_where2 .= " where lc.classyear = $classyear";
}

$sql_select = "select lco.* , ca.id caid, ca2.name ca2name, ca.name ca1name
,(select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_course co1 on lc1.courseid = co1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
 where lca1.status = 'apply' and lc1.parentcourseid = lco.id and (code1.codename like '%임직원%') $sql_where1 ) count1
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid and code1.codename like '%멘토%' $sql_where1 ) count2
 , (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid and (code1.codename NOT like '%임직원%' and code1.codename NOT like '%멘토%' ) $sql_where1 ) count3
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lc1.parentcourseid = lc.parentcourseid $sql_where1 ) totalcount
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid and (code1.codename like '%임직원%') $sql_where1 ) complete1
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid and code1.codename like '%멘토%' $sql_where1 ) complete2
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid and (code1.codename NOT like '%임직원%' and code1.codename NOT like '%멘토%') $sql_where1 ) complete3
, (select count(*) from m_lmsdata_course_applications lca1 
join m_lmsdata_class lc1 on lca1.courseid = lc1.id 
join m_lmsdata_code code1 on lca1.qualification = code1.id
where lca1.status = 'apply' and lca1.completionstatus = 1 and lc1.parentcourseid = lc.parentcourseid $sql_where1 ) totalcomplete

";
$sql_from = "
            from m_lmsdata_class lc
            join m_lmsdata_course lco on lc.parentcourseid = lco.id
            join m_course co on lco.courseid = co.id 
            JOIN m_course_categories ca ON ca.id = co.category 
            left JOIN m_course_categories ca2 ON ca.parent = ca2.id
            $sql_where2 ";

//$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_orderby, $params);
$sql_orderby = " order by  ca2name, ca1name";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params);

//$sql_orderby = " group by co.id ";

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁', 3 => '자율', 4 => '영재원');

if (!$excel) {

    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">콘텐츠 통계(세부내용)</h3>
            <br/><br/><br/>
            <input type="button"  class="" onclick="javascript:location.href = 'contents_report.php'" value="콘텐츠 통계"/>
            <input type="button"  class="orange_btn" onclick="javascript:location.href = 'contents_report_detail.php'" value="세부내용"/>
            <form name="course_search" id="course_search" class="search_area" action="contents_report_detail.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="$('form.search_area').submit();">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2018, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select>
                </div>
                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->

            <table>
                <thead>
                    <tr>
                        <!--<th scope="row" rowspan="2" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>-->                
                        <th scope="row" rowspan="2" width="15%">과정명</th>
                        <th scope="row" rowspan="2" width="10%">영역</th>
                        <th scope="row" rowspan="2" width="10%">분류</th>
                        <th scope="row" rowspan="2" width="5%">교육구분</th>
                        <th scope="row" rowspan="2" width="5%">이수시간</th>
                        <th scope="row" rowspan="2" width="5%">교육대상</th>
                        <th scope="row" colspan="3" width="15%">신청인원</th>
                        <th scope="row" colspan="3" width="15%">이수인원</th>
                        <th scope="row" rowspan="2" width="5%">이수율</th>
                        <th scope="row" rowspan="2" width="5%">수강료</th>
                    </tr>
                    <tr>
                        <th scope="row" width="5%">임직원</th>
                        <th scope="row" width="5%">멘토</th>
                        <th scope="row" width="5%">기타</th>
                        <th scope="row" width="5%">임직원</th>
                        <th scope="row" width="5%">멘토</th>
                        <th scope="row" width="5%">기타</th>
                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="11">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    $startnum = $count_courses - (($currpage - 1) * $perpage);
                    foreach ($courses as $course) {
                        $completion_rate = sprintf("%2.2f" ,($course->totalcomplete/$course->totalcount)*100);
                        ?>
                        <tr>
                            <!--<td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>-->
                            <!--<td><?php echo $startnum--; ?></td>-->                    
                            <td><?php echo $course->coursename ?></td>
                            <td><?php echo $course->ca2name?></td>
                            <td><?php echo $course->ca1name ?></td>
                            <td><?php echo $arr[$course->classtype] ?></td>
                            <td><?php echo $course->learningtime ?></td>
                            <td><?php echo $course->subjects ?></td>                            
                            <td><?php echo $course->count1?></td>
                            <td><?php echo $course->count2 ?></td>
                            <td><?php echo $course->count3 ?></td>
                            <td><?php echo $course->complete1 ?></td>
                            <td><?php echo $course->complete2 ?></td>
                            <td><?php echo $course->complete3 ?></td>
                            <td><?php echo ($completion_rate>0)? $completion_rate.'%' :  '-';  ?></td>                            
                            <td><?php echo number_format($course->price) ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
     
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $fields = array(
        '연번',
        '과정명',
        '영역',
        '분류',
        '교육구분',
        '이수시간',
        '교육대상',
        '신청인원',
        '이수인원',
        '이수율',
        '수강료'
    );
    $fields2 = array(
        '임직원',
        '멘토',
        '기타'
    );

    $date = date('Y-m-d', time());
    $filename = 'training_results' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        if($fieldname == '신청인원' || $fieldname == '이수인원'){
            $worksheet[0]->merge_cells(0, $col, 0, $col+2);
            foreach ($fields2 as $fieldname2) {
                $worksheet[0]->write(1, $col, $fieldname);
                $col++;
            }
        }else{         
            $worksheet[0]->merge_cells(0, $col, 1, $col);
            $col++;
        }
        
    }

    $row = 2;

    foreach ($courses as $course) {
        $completion_rate = sprintf("%2.2f" ,($course->totalcomplete/$course->totalcount)*100);
        $col = 0;
        $worksheet[0]->write($row, $col++, $row-1);
        $worksheet[0]->write($row, $col++, $course->coursename);
        $worksheet[0]->write($row, $col++, $course->ca2name);
        $worksheet[0]->write($row, $col++, $course->ca1name);
        $worksheet[0]->write($row, $col++, $arr[$course->classtype]);
        $worksheet[0]->write($row, $col++, $course->learningtime);
        $worksheet[0]->write($row, $col++, $course->subjects);
        $worksheet[0]->write($row, $col++, $course->count1);
        $worksheet[0]->write($row, $col++, $course->count2);
        $worksheet[0]->write($row, $col++, $course->count3);
        $worksheet[0]->write($row, $col++, $course->complete1);
        $worksheet[0]->write($row, $col++, $course->complete2);
        $worksheet[0]->write($row, $col++, $course->complete3);
        $worksheet[0]->write($row, $col++, ($completion_rate>0)? $completion_rate.'%' :  '-');
        $worksheet[0]->write($row, $col++, number_format($course->price));
        
        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }

</script>    
