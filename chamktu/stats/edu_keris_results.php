<?php
/**
 * 이수자 결과 보고 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', date('Y',time()), PARAM_INT);
$classtype = optional_param('classtype', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$consignmenttitle = optional_param('consignmenttitle', '', PARAM_RAW);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터
if ($classyear) {
    $sql_where[] .= " lc.classyear = $classyear and lc.classnum < 100 ";
}
if ($classtype) {
    $sql_where[] .= " lc.classtype = $classtype ";
}
if ($consignmenttitle) {
    $sql_where[] = " ls.consignmenttitle = :consignmenttitle ";
    $params['consignmenttitle'] = $consignmenttitle;
}
if ($classnum) {
    $sql_where[] = " lc.classnum = :classnum ";
    $params['classnum'] = $classnum;
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lca.*, ca2.name ca2name,ls.consignmenttitle, lc.classtype, lc.classyear,lc.classnum,lc.enrolmentstart,lc.enrolmentend, lc.learningstart, lc.learningend,lc.certificatepre,ls.consignmentplace, co.fullname, u.lastname,u.username,lco.coursename, lco.learningtime, lco.trainingarea, lco.passnum  "
        . ",(select codename from {lmsdata_code} where lco.coursegrade = id ) as coursegrade ,(select codename from {lmsdata_code} where lca.qualification = id ) as qualification";
$sql_from = " FROM  {lmsdata_class} lc "
        . "join  {lmsdata_course_applications} lca on lc.id=lca.courseid and lca.completionstatus = 1 and lca.status = 'apply' "
        . "join {course} co on co.id = lc.courseid "
        . "JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id "        
        . "join {user} u on u.id = lca.userid "
        . "JOIN m_course_categories ca ON ca.id = co.category 
            left JOIN m_course_categories ca2 ON ca.parent = ca2.id
            left JOIN {lmsdata_schedule} ls ON ls.id = lc.scheduleid 
            ";

//$sql_orderby = " order by lg.id desc";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
//print_object($courses);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">이수자 결과 보고</h3>
            <br/><br/><br/>
            <form name="course_search" id="course_search" class="search_area" action="edu_keris_results.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                <select title="year" name="classyear" id="classyear" class="w_160" onchange="year_changed(this, 'lmsdata_class');">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2017, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                </select> 
                <select title="classtype" name="classtype" id="classtype" class="w_120">
                    <option value="0">교육타입</option>
                    <option value="1" <?php echo $classtype ==1 ? 'selected':'' ?> >기수</option>
                    <option value="2" <?php echo $classtype ==2 ? 'selected':'' ?> >위탁</option>
                </select>
                <select title="class" name="classnum" id="classnum" class="w_160">
                    <option value="0">기수</option>
                    <?php
                    if ($classyear) {
                        if ($classtype)
                            $classnum_where = " and classtype = $classtype";
                        if ($classyear)
                            $classnum_where .= " and classyear = $classyear";
                        $datas = $DB->get_records_sql('select classnum from {lmsdata_class} where classnum != 0 and classnum < 100 ' . $classnum_where);

                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->classnum == $classnum) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                        }
                    }
                    ?>
                </select>
                <select title="class" name="consignmenttitle" id="consignmenttitle" class="w_160">
                    <option value="0">위탁처</option>
                    <?php
                        $datas = $DB->get_records_sql("select consignmenttitle from {lmsdata_schedule} where  consignmenttitle != '' and classyear = $classyear ");
                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->consignmenttitle == $consignmenttitle) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->consignmenttitle . '"' . $selected . '> ' . $data->consignmenttitle . '</option>';
                        }
                    ?>
                </select>
                </div>
                <div style="float:left;">
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>
                </div>
                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>
                </div>

            </form><!--Search Area2 End-->

            <table>
                <thead>
                    <tr>
                        <!--<th scope="row" width="5%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>-->
                        <th scope="row" width="3%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                        <th scope="row" width="5%">수강형식</th>
                        <th scope="row" width="5%">년도</th>
                        <th scope="row" width="10%">기수/위탁처</th>
                        <th scope="row" width="20%">과정명</th>
                        <th scope="row" width="10%">교육기간</th>
                        <th scope="row" width="10%">이수번호</th>
                        <th scope="row" width="7%">이름(아이디)</th>
                        <th scope="row" width="10%">NEIS 개인정보</th>
                        <th scope="row" width="10%">지명번호</th>
                        <th scope="row" width="10%">생년월일</th>
                        <th scope="row" width="10%">합격증번호</th>
                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="11">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    $startnum = $count_courses - (($currpage - 1) * $perpage);
                    foreach ($courses as $course) {       
                        if ($course->certificatepre) {
                            if ($course->classtype  == 1) {
                                $certificatenum = $course->certificatepre . '-직무-' . $course->classyear . '-' . $course->classnum . '-' . sprintf("%08d", $course->id);
                            }else if ($course->classtype  == 2) {
                                $certificatenum = $course->certificatepre . '-위탁-' . $course->classyear . '-' . $course->consignmentplace . '-' . sprintf("%08d", $course->id);
                            }
                        } else {
                            if ($course->classtype  == 1) {
                                $certificatenum = '참교육-직무-' . $course->classyear . '-' . $course->classnum . '-' . sprintf("%08d", $course->id);
                            }else if ($course->classtype  == 2) {
                                $certificatenum = '참교육-위탁-' . $course->classyear . '-' . $course->consignmentplace . '-' . sprintf("%08d", $course->id);
                            }
                        }
                        ?>
                        <tr>
                            <!--<td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>-->
                            <td><?php echo $startnum--; ?></td>                    
                            <td><?php echo $arr[$course->classtype] ?></td>
                            <td><?php echo $course->classyear . '년' ?></td>
                            <?php if($course->classtype == 1) { ?>
                            <td><?php echo $course->classnum ? $course->classnum . '기' : '-' ?></td>
                            <?php }else { 
                                ?>
                            <td><?php echo $course->consignmenttitle ? $course->consignmenttitle : '-' ?></td>
                            <?php } ?>
                            <td><?php echo $course->coursename ?></td>
                            <td><?php echo date('Ymd', $course->learningstart) . ' ~ ' . date('Ymd', $course->learningend) ?></td>
                            <td><?php echo $certificatenum ?></td>
                            <td><?php echo $course->lastname . '(' . $course->username . ')' ?></td>
                            <td><?php echo $course->neiscode ?></td>
                            <td><?php echo $course->locationnumber ?></td>
                            <td><?php echo $course->birthday ?></td>
                            <?php if(!empty($course->passnum)){ ?>
                            <td><?php echo $course->passnum?></td>
                            <?php } else {?>
                            <td><?php echo '-'?></td>
                            <?php }?>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
            <?php
            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params);
    $fields = array(
        '연번',
        'NEIS 개인번호',
        '지명번호',
        '교육과정',
        '교육기관',
        '교육시작일',
        '교육종료일',
        '교육구분',
        '교육시간',
        '성적',
        '직무관련성',
        '평점학점',
        '이수번호',
        '성명',
        '생년월일',
        '학교명',
        '초/중등',
        '교육영역',
        '합격증번호'
    );

    $date = date('Y-m-d', time());
    $filename = 'training_results' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($courses as $course) {
        
        if ($course->certificatepre) {
            if ($course->classtype  == 1) {
                $certificatenum = $course->certificatepre . '-직무-' . $course->classyear . '-' . $course->classnum . '-' . sprintf("%08d", $course->id);
            }else if ($course->classtype  == 2) {
                $certificatenum = $course->certificatepre . '-위탁-' . $course->classyear . '-' . $course->consignmentplace . '-' . sprintf("%08d", $course->id);
            }
        } else {
            if ($course->classtype  == 1) {
                $certificatenum = '참교육-직무-' . $course->classyear . '-' . $course->classnum . '-' . sprintf("%08d", $course->id);
            }else if ($course->classtype  == 2) {
                $certificatenum = '참교육-위탁-' . $course->classyear . '-' . $course->consignmentplace . '-' . sprintf("%08d", $course->id);
            }
        }
        
        if(preg_match('/초등/',$course->qualification)){ 
            $courserating = '초등';
        }else if(preg_match('/중등/',$course->qualification)){ 
            $courserating = '중등';
        }else if(preg_match('/유치원/',$course->qualification)){ 
            $courserating = '초등';
        }else if(!$course->qualification){ 
            $courserating = '';
        }else{
            $courserating = '기타';
        }
        if(preg_match('/특수/',$course->qualification)) {
            $courserating ='특수';
        }
        
        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $course->neiscode);
        $worksheet[0]->write($row, $col++, $course->locationnumber);
        $worksheet[0]->write($row, $col++, $course->coursename);
        $worksheet[0]->write($row, $col++, '참교육 원격교육연수원');
        $worksheet[0]->write($row, $col++, date('Ymd', $course->learningstart));
        $worksheet[0]->write($row, $col++, date('Ymd', $course->learningend));
        $worksheet[0]->write($row, $col++, '2');
        $worksheet[0]->write($row, $col++, $course->learningtime);
        $worksheet[0]->write($row, $col++, '');
        $worksheet[0]->write($row, $col++, 'Y');
        $worksheet[0]->write($row, $col++, substr($course->coursegrade, 0, 1));
        $worksheet[0]->write($row, $col++, $certificatenum);
        $worksheet[0]->write($row, $col++, $course->lastname);
        $worksheet[0]->write($row, $col++, $course->birthday);
        $worksheet[0]->write($row, $col++, $course->school);
        $worksheet[0]->write($row, $col++, $courserating);
        $worksheet[0]->write($row, $col++, $course->trainingarea);
        $worksheet[0]->write($row, $col++, $course->passnum);
        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }

</script>    
