<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', date('Y',time()), PARAM_INT);
$mon = optional_param('mon', date('m',time()), PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);


//검색용 파라미터
$sql_where[] = "lca.paymentstatus = 'complete' ";
$sql_where[] = "lca.status = 'apply' ";
if ($classyear) {
    $sql_where[] = " lc.classyear = :classyear ";
    $params['classyear'] = $classyear;
    $list_text .= $classyear.'년도 ';
}

if ($mon) {
    $sql_where[] = " MONTH(from_unixtime(lca.timecreated)) = :mon ";
    $params['mon'] = $mon;
    $list_text .= $mon.'월 ';
    
}
if ($classnum) {
    $sql_where[] = " lc.classnum = :classnum ";
    $params['classnum'] = $classnum;
    $list_text .= $classnum.'기 ';
}
if (!empty($searchtext)) {
    $like = array();
    $like[] = $DB->sql_like('co.fullname', ':fullname');
    $like[] = $DB->sql_like('u.lastname', ':lastname');
    $params['fullname'] = '%' . $searchtext . '%';
    $params['lastname'] = '%' . $searchtext . '%';
    $sql_where[] = ' ' . implode(' OR ', $like);
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lca.*, co.fullname, lco.coursename, lc.classnum, u.lastname, lco.price lcoprice ";
$sql_from = " FROM  {lmsdata_course_applications} lca 
            JOIN {lmsdata_class} lc ON lc.id = lca.courseid 
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id 
            JOIN {lmsdata_schedule} ls ON lc.scheduleid = ls.id
            JOIN {course} co ON co.id = lc.courseid 
            JOIN {user} u ON u.id = lca.userid ";

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql("SELECT count(distinct co.id) " . $sql_from . $sql_where, $params);
$sql_orderby = " group by lca.paymentmeans ";
$list = $DB->get_records_sql(" select lca.paymentmeans, sum(lca.price) as price " . $sql_from . $sql_where . $sql_orderby, $params);
$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">매출정산</h3>
            <form name="course_search" id="course_search" class="search_area" action="edu_sales_status.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    
                <select title="year" name="classyear" id="classyear" class="w_120" onchange="year_changed(this, 'lmsdata_class');">
                    <option value="0">년도</option>
                    <?php
                    $yearRange = 1;
                    $currentYear = date('Y');
                    $maxYear = ($currentYear + $yearRange);

                    foreach (range(2017, $maxYear) as $year) {
                        $selected = '';
                        if ($year == $classyear) {
                            $selected = 'selected';
                        }
                        echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                    }
                    ?>
                </select> 

                    <select title="mon" name="mon" id="mon" class="w_160">
                        <?php
                        for ($x = 1; $x <= 12; $x++) {
                            $selected = '';
                            if ($x == $mon) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $x . '"' . $selected . '> ' . $x . '월</option>';
                        }
                        ?>
                    </select>
                    <select title="class" name="classnum" id="classnum" class="w_160">
                        <option value="0">기수</option>
                        <?php
                        if ($classyear) {
                            if ($classyear)
                                $classnum_where .= " where lc.classyear = $classyear order by lc.classnum asc";
                            $datas = $DB->get_records_sql('SELECT lc.classnum ' . $sql_from . $classnum_where);

                            foreach ($datas as $data) {
                                $selected = '';
                                if ($data->classnum == $classnum) {
                                    $selected = ' selected';
                                }
                                echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <br><br><br>

                <div style="float:left;">
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="과정명을 입력하세요."  class="search-text"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
                </div>
            </form><!--Search Area2 End-->
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="10%">기간</th>                
                        <th scope="row" width="10%">신청수</th>
                        <th scope="row" width="10%">신용카드</th>
                        <th scope="row" width="10%">가상계좌</th>
                        <th scope="row" width="10%">실시간이체</th>
                        <th scope="row" width="10%">무통장</th>
                        <th scope="row" width="10%">총결제금액</th>                        
                    </tr>
                </thead>
                <tr>                 
                    <td><?php echo $list_text ?></td>
                    <td><?php echo $count_courses.'명' ?></td>
                    <td><?php echo number_format($list[card]->price).'원' ?></td>
                    <td><?php echo number_format($list[vbank]->price).'원' ?></td>
                    <td><?php echo number_format($list[bank]->price).'원' ?></td>
                    <td><?php echo number_format($list[unbank]->price).'원' ?></td>
                    <td><?php echo number_format($list[card]->price+$list[vbank]->price+$list[bank]->price+$list[unbank]->price).'원' ?></td>
                </tr>
            </table>

            <table>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>                
                        <th scope="row" width="15%">과정명</th>
                        <th scope="row" width="10%">구분</th>
                        <th scope="row" width="10%">연수비</th>
                        <th scope="row" width="10%">기수</th>
                        <th scope="row" width="10%">신청자</th>
                        <th scope="row" width="10%">결제일</th>
                        <th scope="row" width="10%">결제방식</th>
                        <th scope="row" width="10%">결제금액</th>
                        <th scope="row" width="10%">할인유형</th>

                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="11">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    $startnum = $count_courses - (($currpage - 1) * $perpage);
                    foreach ($courses as $course) {
                        $completion_rate = sprintf("%2.2f", ($course->complet / $course->total) * 100);
                        ?>
                        <tr>
                            <!--<td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>-->
                            <td><?php echo $startnum--; ?></td>                    
                            <td><?php echo $course->coursename ?></td>
                            <td><?php echo $course->ca2name ?></td>
                            <td><?php echo $course->lcoprice ?></td>
                            <td><?php echo $course->classnum ?></td>
                            <td><?php echo $course->lastname ?></td>
                            <td><?php echo date('Y-m-d',$course->timecreated) ?></td>
                            <td><?php echo $course->paymentmeans ?></td>
                            <td><?php echo $course->price ?></td>
                            <td><?php echo $course->discounttype ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">            
                <div style="float:right;">
                    <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </div>
            <?php
            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $fields = array(
        '연번',
        '과정명',
        '영역',
        '분류',
        '연수구분',
        '이수시간',
        '연수대상',
        '신청인원',
        '이수인원',
        '이수율',
        '수강료'
    );
    $fields2 = array(
        '초등',
        '중등',
        '기타'
    );

    $date = date('Y-m-d', time());
    $filename = 'training_results' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        if ($fieldname == '신청인원' || $fieldname == '이수인원') {
            $worksheet[0]->merge_cells(0, $col, 0, $col + 2);
            foreach ($fields2 as $fieldname2) {
                $worksheet[0]->write(1, $col, $fieldname);
                $col++;
            }
        } else {
            $worksheet[0]->merge_cells(0, $col, 1, $col);
            $col++;
        }
    }

    $row = 2;

    foreach ($courses as $course) {
        $completion_rate = sprintf("%2.2f", ($course->complet / $course->total) * 100);
        $col = 0;
        $worksheet[0]->write($row, $col++, $row - 1);
        $worksheet[0]->write($row, $col++, $course->fullname);
        $worksheet[0]->write($row, $col++, $course->ca2name);
        $worksheet[0]->write($row, $col++, $course->ca1name);
        $worksheet[0]->write($row, $col++, '');
        $worksheet[0]->write($row, $col++, $course->learningtime);
        $worksheet[0]->write($row, $col++, '');
        $worksheet[0]->write($row, $col++, $course->count1);
        $worksheet[0]->write($row, $col++, $course->count2);
        $worksheet[0]->write($row, $col++, $course->count3);
        $worksheet[0]->write($row, $col++, $course->count4);
        $worksheet[0]->write($row, $col++, $course->count5);
        $worksheet[0]->write($row, $col++, $course->count6);
        $worksheet[0]->write($row, $col++, ($completion_rate > 0) ? $completion_rate . '%' : '-');
        $worksheet[0]->write($row, $col++, number_format($course->price));

        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        var url = "edu_sales_status.php?excel=1";

        document.location.href = url;
    }

</script>    
