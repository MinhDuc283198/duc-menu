<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");



// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/stats/menu_stats.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$startyear = optional_param('startyear', date("Y"), PARAM_INT);
$startmon = optional_param('startmon', 0, PARAM_INT);
$startday = optional_param('startday', 0, PARAM_INT);
$endyear = optional_param('endyear', date("Y"), PARAM_INT);
$endmon = optional_param('endmon', 0, PARAM_INT);
$endday = optional_param('endday', 0, PARAM_INT);
$target = optional_param('target', 'all', PARAM_RAW);
$username = optional_param('username', null, PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);

if (!$startmon || !$startday || !$endmon || !$endday) {
    $date = date("Y-m-d");
    $startyear = date("Y");
    $startmon = date("m");
    $startday = date("d");
    $endyear = date("Y");
    $endmon = date("m");
    $endday = date("d");
    $startdate = $startyear . '-' . $startmon . '-' . $startday;
    $enddate = $endyear . '-' . $endmon . '-' . $endday;
} else {
    $startdate = $startyear . '-' . $startmon . '-' . $startday;
    $enddate = $endyear . '-' . $endmon . '-' . $endday;
}

$sql_select = "select id, userid, usertype, timecreated, menu, menu_url,count(*) as count ";
$count_select = "select count(distinct menu)";
$sql_from = "from {main_menu_log} mml";
$sql_groupby = " GROUP BY mml.menu ";
$sql_orderby = " order by count desc";




$page_params = array();
$params = array();
$conditions = array();

$date_start = strtotime($startdate . ' 00:00:00');
$date_end = strtotime($enddate . ' 23:59:59');
$params['date_start'] = $date_start;
$params['date_end'] = $date_end;

$conditions[] = "(timecreated > $date_start AND timecreated < $date_end)";
$params['date'] = $date;

$sql_where = '';

if ($target != 'all') {
    if ($target == 'target') {
        if (!empty($username)) {
            $conditions[] = "mu.username = :username";
            $params['username'] = $username;
        }
    } else {
        $conditions[] = "usertype = :usertypecode";
        $params['usertypecode'] = $target;
    }
}
if ($conditions) {
    $sql_where = ' WHERE ' . implode(' AND ', $conditions);
}


//$stats = $DB->get_records_sql($sql_select.$sql_from.$sql_where, $param,($currpage-1)*$perpage, $perpage);
$stats = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_groupby . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);

$count_stats = $DB->count_records_sql($count_select . $sql_from . $sql_where, $params);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$excelurl = "menu_stats.php?page=$currpage&startyear=$startyear&startmon=$startmon&startday=$startday&endyear=$endyear&endmon=$endmon&endday=$endday&excel=1&target=$target ";
if (!$excel) {
    ?>

    <?php include_once ('../inc/header.php'); ?>
    <div id="contents">
        <?php include_once ('../inc/sidebar_stats.php'); ?>
        <div id="content">
            <h3 class="page_title">메뉴 이용 통계</h3>               
            <div class="page_navbar"><a href="./contact_stats.php"><?php echo get_string('stats_management', 'local_lmsdata'); ?></a> > <strong>메뉴 이용 통계</strong></div>


            <form name="" id="course_search" class="search_area" action="menu_stats.php" method="get">
                <input type="hidden" name="page" value="1" />

                <?php
                if ($viewall) {
                    $checked = 'checked';
                    $disabled = 'disabled';
                    $style = 'style="background-color: #c8c8c8"';
                } else {
                    $checked = '';
                    $disabled = '';
                    $style = '';
                }
                ?>

                <label><?php echo get_string('stats_periodsearch', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="startyear" class="w_80">
                    <?php
                    $years = lmsdata_get_years($startyear);
                    foreach ($years as $v => $y) {
                        $selected = '';
                        if ($v == $startyear) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                    }
                    ?>
                </select>
                <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="startmon" class="w_80">
                    <?php
                    $mons = lmsdata_get_mons($startmon);
                    echo $mons;
                    ?>
                </select>
                <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="startday" class="w_80">
                    <?php
                    $days = lmsdata_get_days($startday);
                    echo $days;
                    ?>
                </select>
                <label><?php echo get_string('contents_day', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp;&nbsp; ~ &nbsp;</label>
                <select name="endyear" class="w_80">
                    <?php
                    $years = lmsdata_get_years($endyear);
                    foreach ($years as $v => $y) {
                        $selected = '';
                        if ($v == $endyear) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                    }
                    ?>
                </select>
                <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="endmon" class="w_80">
                    <?php
                    $mons = lmsdata_get_mons($endmon);
                    echo $mons;
                    ?>
                </select>
                <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="endday" class="w_80">
                    <?php
                    $days = lmsdata_get_days($endday);
                    echo $days;
                    ?>
                </select>
                <label><?php echo get_string('contents_day', 'local_lmsdata'); ?> &nbsp;</label>
                <select name="target" onchange="setusernameinput();">
                    <option value="all" <?php if ($target == 'all') echo 'selected'; ?>><?php echo get_string('all', 'local_lmsdata'); ?></option>
                    <option value="10" <?php if ($target == '10') echo 'selected'; ?>>임직원</option>
                    <option value="20" <?php if ($target == '20') echo 'selected'; ?>>멘토</option>
                    <option value="30" <?php if ($target == '30') echo 'selected'; ?>>교육생</option>
    <!--                <option value="target" <?php if ($target == 'target') echo 'selected'; ?>><?php echo get_string('specific_target', 'local_lmsdata'); ?></option>-->
                </select>            
                <input type="submit" class="blue_btn" value="<?php echo get_string('stats_search', 'local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/>       

            </form><!--Search Area2 End-->
            <div style="float:right;">             
                <input type="submit" onclick="location.href = '<?php echo $excelurl; ?>'" class="blue_btn" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?> " style="margin-right: 10px;"/>          
            </div>         
            <table>
                <tr>
                    <th style='width:15%;'>순위</th>
                    <th style='width:10%;'>메뉴명</th>
                    <th style='width:30%;'>메뉴 주소</th>
                    <th style='width:20%;'>사용횟수</th>                
                </tr>
                <?php if (!$stats) { ?>
                    <tr>
                        <td colspan="24">통계내용이 없습니다.</td>
                    </tr>
                <?php
                } else {
                    $startnum = 1 + (($currpage - 1) * $perpage);


                    foreach ($stats as $stat) {

                        $sql = "SELECT name from {main_menu_name} where lang = 'ko' and menuid = $stat->menu";
                        $menu_name = $DB->get_field_sql($sql);
                        ?>     
                        <tr>
                            <td style='width:15%;'><?php echo $startnum++; ?></td>
                            <!--<td style='width:10%;'><?php echo ($stat->category == 0) ? '</td>' : $category ?></td>-->
                            <td style='width:15%;'><?php echo $menu_name; ?></td>
                            <td style='width:15%;'><?php echo $stat->menu_url; ?></td>
                            <td style='width:15%;'><?php echo $stat->count; ?></td>
                            <!--<td style='width:50%;'><div style="width: <?php echo ($contact_stat->count / $a1) * 100 ?>%; height: 10px;background-color: #a1a1a8;"></div></td>-->
                            <!--<td style='width:10%;'><input type="button" title="<?php echo get_string('stats_view', 'local_lmsdata'); ?>" alt="<?php echo get_string('stats_view', 'local_lmsdata'); ?>" id="view" class="blue_btn" value="<?php echo get_string('stats_view', 'local_lmsdata'); ?>" onclick="contact_log(<?php echo $contact_stat->log_date ?>)"></td>-->
                        </tr>
                        <?php
                    }
                }
                ?>
            </table><!--Table End-->
            <?php
            if (($count_stats / $perpage) > 1) {
                print_paging_navbar_script($count_stats, $currpage, $perpage, 'javascript:cata_page(:page);');
            }
            ?>
        </div><!--Content End-->
    </div> <!--Contents End-->
    <?php
    include_once ('../inc/footer.php');
} else {
    $stats = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_groupby . $sql_orderby, $params);
    include_once (dirname(dirname(__FILE__)) . '/inc/lib.php');

    $fields = array(
        '순위',
        '메뉴명',
        '메뉴 주소',
        '사용 횟수'
    );

    $date = date('Y-m-d', time());
    $filename = '메뉴 이용 통계' . $date . '.xls';
    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    $row = 1;
    $worksheet[0]->set_column(1, 1, 10, array('v_align' => 'top')); // 메뉴명
    $worksheet[0]->set_column(1, 2, 20, array('v_align' => 'top')); // 메뉴 주소
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    foreach ($stats as $stat) {

        $sql = "SELECT name from {main_menu_name} where lang = 'ko' and menuid = $stat->menu";
        $menu_name = $DB->get_field_sql($sql);

        $col = 0;
        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $menu_name);
        $worksheet[0]->write($row, $col++, $stat->menu_url);
        $worksheet[0]->write($row, $col++, $stat->count);


        $row++;
    }
    $workbook->close();
    die;
}
    
    
