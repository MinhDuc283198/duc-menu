<?php
/**
 * 교육운영현황 위탁교육 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$classyear = optional_param('classyear', date('Y', time()), PARAM_INT);
$mon = optional_param('mon', date('m', time()), PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터
if ($classyear) {
    $sql_where .= " and YEAR(from_unixtime(lca.timecreated)) = $classyear";
}

//$sql_select = "select lco.id, lco.coursename ";
//$sql_from = " from m_lmsdata_course lco
//    where lco.isused = 0 ";
$sql_select = "select lco.id, lco.coursename ";
$sql_from = " from m_lmsdata_course_applications lca
join m_lmsdata_class lc on lc.id = lca.courseid
left join m_lmsdata_schedule ls on ls.id = lc.scheduleid
join m_lmsdata_course lco on lco.id = lc.parentcourseid
where lco.isused = 0 $sql_where and ls.classtype = 2
group by ls.consignmentplace, lco.coursename ";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_orderby, $params);
$classnum_datas = $DB->get_records_sql("select consignmentplace from {lmsdata_schedule} where classyear = $classyear and classtype = 2 and consignmentplace !='' ");


$datas = array();
$tables = array();

$fields = array(
    '과정명',
    '구분'
);
foreach ($classnum_datas as $classnum_data) {
    $tableth .= "<th scope='row' width='3%'>$classnum_data->consignmentplace" . "</th>";
    array_push($fields, $classnum_data->consignmentplace); 
}
array_push($fields, '합계'); 
//print_object($datas);

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">교육운영현황</h3>
            <br/><br/><br/>
            <input type="button"  class="" onclick="javascript:location.href = 'edu_operation_status.php'" value="정기교육"/>
            <input type="button"  class="orange_btn" onclick="javascript:location.href = 'edu_operation_status_consignment.php'" value="위탁교육"/>
            <form name="course_search" id="course_search" class="search_area" action="edu_operation_status_consignment.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="$('form.search_area').submit();">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2017, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select> 
                </div>
                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="15%">과정명</th>
                        <th scope="row" width="3%">구분</th>
                        <?php
                        echo $tableth;
                        ?>
                        <th scope="row" width="3%">합계</th>

                    </tr>
                </thead>
                <?php
                foreach ($courses as $course) {
                    foreach ($classnum_datas as $classnum_data) {
                        $signup = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
                            left join m_lmsdata_schedule ls on ls.id = lc.scheduleid
                        where lca.status = 'apply' and lc.parentcourseid = $course->id and ls.consignmentplace = '$classnum_data->consignmentplace' ");
                        $complete = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
                            left join m_lmsdata_schedule ls on ls.id = lc.scheduleid
                        where lca.status = 'apply' and lca.completionstatus = 1 and lc.parentcourseid = $course->id and ls.consignmentplace = '$classnum_data->consignmentplace'");
                        $totalprice = $DB->get_record_sql("select sum(lca.price) price from m_lmsdata_course_applications lca 
                        join m_lmsdata_class lc on lca.courseid = lc.id 
                        left join m_lmsdata_schedule ls on ls.id = lc.scheduleid
                        where lca.status = 'apply' and lc.parentcourseid = $course->id and ls.consignmentplace = '$classnum_data->consignmentplace'");
                        $datas[$classnum_data->consignmentplace]['signup'] = $signup;
                        $datas[$classnum_data->consignmentplace]['complete'] = $complete;
                        $datas[$classnum_data->consignmentplace]['totalprice'] = $totalprice->price;
                    }
                    ?>

                    <tr>
                        <td rowspan="4"><?php echo $course->coursename ?></td>
                        <td>수강인원</td>
                        <?php
                        $totalcount = '';
                        foreach ($datas as $data => $val) {
                            $totalcount += $val['signup'];
                            echo "<td>" . $val['signup'] . "</td>";
                        }
                        echo "<td>" . $totalcount . "</td>";
                        ?>
                    </tr>
                    <tr>
                        <td>수료인원</td>
                        <?php
                        $totalcount = '';
                        foreach ($datas as $data => $val) {
                            $totalcount += $val['complete'];
                            echo "<td>" . $val['complete'] . "</td>";
                        }
                        echo "<td>" . $totalcount . "</td>";
                        ?>
                    </tr>
                    <tr>
                        <td>이수율</td>
                        <?php
                        $totalcount = '';
                        foreach ($datas as $data => $val) {
                            $completion_rate = (int) sprintf("%2.2f", ( $val['complete'] / $val['signup']) * 100);
                            $totalcount += $completion_rate;
                            echo "<td>" . $completion_rate . "%</td>";
                        }
//                        echo "<td>" . $totalcount . "</td>";
                        ?>
                    </tr>
                    <tr>
                        <td>수익</td>
                        <?php
                        $totalcount = '';
                        foreach ($datas as $data => $val) {
                            $totalcount += $val['totalprice'];
                            echo "<td>" . number_format($val['totalprice']) . "</td>";
                        }
                        echo "<td>" . number_format($totalcount) . "</td>";
                        ?>
                    </tr>
                    <?php
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">            

            </div>
            <?php
//            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {

    $date = date('Y-m-d', time());
    $filename = 'edu_operation_status_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    foreach ($courses as $course) {
        foreach ($classnum_datas as $classnum_data) {
            $signup = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
            where lca.status = 'apply' and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum ");
            $complete = $DB->count_records_sql("select count(*) from m_lmsdata_course_applications lca join m_lmsdata_class lc on lca.courseid = lc.id 
            where lca.status = 'apply' and lca.completionstatus = 1 and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum");
            $totalprice = $DB->get_record_sql("select sum(lca.price) price from m_lmsdata_course_applications lca 
            join m_lmsdata_class lc on lca.courseid = lc.id where lca.status = 'apply' and lc.parentcourseid = $course->id and lc.classnum = $classnum_data->classnum");
            $datas[$classnum_data->classnum]['signup'] = $signup;
            $datas[$classnum_data->classnum]['complete'] = $complete;
            $datas[$classnum_data->classnum]['totalprice'] = $totalprice->price;
        }
        print_object($datas);die;
        $col = 0;
        $worksheet[0]->write($row, $col++, $course->coursename);
        $worksheet[0]->merge_cells($row, 0, $row+3, 0);
        $worksheet[0]->set_column(0, 0, 50);
        $worksheet[0]->write($row, $col++, '수강인원');
        $totalcount = '';
        foreach ($datas as $data => $val) {
            $totalcount += $val['signup'];
            $worksheet[0]->write($row, $col++, $val['signup']);
        }
        $worksheet[0]->write($row, $col++, $totalcount);
        
        $row++;
        $col = 1;
        $worksheet[0]->write($row, $col++, '수료인원');
        $totalcount = '';
        foreach ($datas as $data => $val) {
            $totalcount += $val['complete'];
            $worksheet[0]->write($row, $col++, $val['complete']);
        }
        $worksheet[0]->write($row, $col++, $totalcount);
        
        $row++;
        $col = 1;
        $worksheet[0]->write($row, $col++, '이수율');
        $totalcount = '';
        foreach ($datas as $data => $val) {
            $completion_rate = (int) sprintf("%2.2f", ( $val['complete'] / $val['signup']) * 100);
            $totalcount += $completion_rate;
            $worksheet[0]->write($row, $col++, $completion_rate.'%');
        }
//        $worksheet[0]->write($row, $col++, $totalcount);
        
        $row++;
        $col = 1;
        $worksheet[0]->write($row, $col++, '수익');
        $totalcount = '';
        foreach ($datas as $data => $val) {
            $totalcount += $val['totalprice'];
            $worksheet[0]->write($row, $col++, number_format($val['totalprice']));
        }
        $worksheet[0]->write($row, $col++, number_format($totalcount));

        $row++;
    }

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }
</script>    
