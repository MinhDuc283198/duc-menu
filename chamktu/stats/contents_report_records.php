<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");


// Check for valid admin user - no guest autologin
require_login(0, false);

if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/stats/contents_report_records.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$startyear = optional_param('startyear', date("Y"), PARAM_INT);
$startmon = optional_param('startmon', 0, PARAM_INT);
$startday = optional_param('startday', 0, PARAM_INT);
$endyear = optional_param('endyear', date("Y"), PARAM_INT);
$endmon = optional_param('endmon', 0, PARAM_INT);
$endday = optional_param('endday', 0, PARAM_INT);
$username = optional_param('username', null, PARAM_TEXT);
$excell = optional_param('excell', 0, PARAM_INT);

$viewtypesort = optional_param('viewtypesort', 'desc', PARAM_TEXT);
$viewtype = optional_param('viewtype', '', PARAM_TEXT);

if (!$startmon || !$startday || !$endmon || !$endday) {
    $date = date("Y-m-d");
    $startyear = date("Y");
    $startmon = date("m");
    $startday = date("d");
    $endyear = date("Y");
    $endmon = date("m");
    $endday = date("d");
} else {
    $startdate = $startyear . '-' . $startmon . '-' . $startday;
    $enddate = $endyear . '-' . $endmon . '-' . $endday;
}

$sql_select = "SELECT u.id, lp.lcmsid, lu.usergroup, u.username, u.lastname, SUM((lp.positionto - lp.positionfrom)) AS contentsplaytime ";
$sql_count = "SELECT COUNT(distinct u.id) ";

$sql_from = " FROM {lcms_playtime} lp
                    JOIN {user} u on u.id = lp.userid
                    JOIN {lmsdata_user} lu on lu.userid = u.id";

$page_params = array();
$sql_where = '';

$date_start = strtotime($startdate . ' 00:00:00');
$date_end = strtotime($enddate . ' 23:59:59');

$conditions[] = "(lp.timecreated > :date_start AND lp.timecreated < :date_end)";
$param['date_start'] = $date_start;
$param['date_end'] = $date_end;

if (!empty($username)) {
    $conditions[] = "u.username = :username";
    $param['username'] = $username;
}

if ($conditions) {
    $sql_where = ' WHERE ' . implode(' AND ', $conditions);
}

if ($viewtype) {
    if ($viewtype == 'username') {
        $sql_orderby = " ORDER BY u.username " . $viewtypesort;
    } else if ($viewtype == 'userid') {
        $sql_orderby = " ORDER BY u.id " . $viewtypesort;
    } else if ($viewtype == 'role') {
        $sql_orderby = " ORDER BY lu.usergroup " . $viewtypesort;
    } else if ($viewtype == 'totwatch') {
        $sql_orderby = " ORDER BY contentsplaytime " . $viewtypesort;
    }
} else {
    $sql_orderby = " ORDER BY u.username DESC";
}

$sql_groupby = " GROUP BY u.id";


$contact_stats = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_groupby . $sql_orderby, $param, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql($sql_count . $sql_from . $sql_where, $param);

//$count_contents = $DB->get_record_sql('SELECT COUNT(distinct lp.lcmsid) AS contentsnum, SUM(lt.attempts) AS attempts FROM {lcms_playtime} lp JOIN {lcms_track} lt on lt.lcms = lp.lcmsid '. $sql_where, $param);
$total_count = $DB->count_records_sql('select count(*) from {lcms_contents} ');
$total_view_count = $DB->get_record_sql('select sum(attempts) as count from {lcms_track} ');

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>

<?php include_once ('../inc/header.php'); ?>
<div id="contents">
    <?php include_once ('../inc/sidebar_stats.php'); ?>
    <div id="content">
        <h3 class="page_title">시청 이력</h3>
        <div class="page_navbar"><a href="./contact_stats_day.php"><?php echo get_string('stats_management', 'local_lmsdata'); ?></a> > <strong>콘텐츠 상세 이력</strong></div>
        <div class="count_area" style="float:left; margin: 4px 4px 10px 10px;" >전체 동영상수 : <?php echo number_format($total_count); ?> / 총 누적조회수 : <?php echo number_format($total_view_count->count); ?></div>
        <div class="siteadmin_tabs">
            <a href="contents_report.php"><p class="black_btn">콘텐츠 목록</p></a>
            <a href="contents_report_records.php"><p class="black_btn black_btn_selected">시청 이력</p></a>
        </div>

        <form name="" id="course_search" class="search_area" action="contents_report_records.php" method="get">
            <input type="hidden" name="page" value="1" />
            <input type="hidden" name="conid" value="<?php echo $conid; ?>" />
            <input type="hidden" name="viewtype" value="<?php echo $viewtype; ?>" />
            <input type="hidden" name="viewtypesort" value="<?php echo $viewtypesort; ?>" />

            <input type="text" value="<?php echo $username; ?>" name="username" readonly /> 
            <input type="button" class="blue_btn" value="사용자 검색" onclick="search_usergroup_popup()" style="margin:0 5px 0 0;"/>          
            <input type="button" class="pink_btn" id="clearbutton" value="비우기" onclick="search_user_clear()" style="margin:0 20px 0 0;"/>          

            <select name="startyear" class="w_80">
                <?php
                $years = lmsdata_get_years($startyear);
                foreach ($years as $v => $y) {
                    $selected = '';
                    if ($v == $startyear) {
                        $selected = ' selected';
                    }
                    echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                }
                ?>
            </select>
            <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="startmon" class="w_80">
                <?php
                $mons = lmsdata_get_mons($startmon);
                echo $mons;
                ?>
            </select>
            <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="startday" class="w_80">
                <?php
                $days = lmsdata_get_days($startday);
                echo $days;
                ?>
            </select>
            <label><?php echo get_string('contents_day', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp;&nbsp; ~ &nbsp;</label>
            <select name="endyear" class="w_80">
                <?php
                $years = lmsdata_get_years($endyear);
                foreach ($years as $v => $y) {
                    $selected = '';
                    if ($v == $endyear) {
                        $selected = ' selected';
                    }
                    echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                }
                ?>
            </select>
            <label><?php echo get_string('contents_year', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="endmon" class="w_80">
                <?php
                $mons = lmsdata_get_mons($endmon);
                echo $mons;
                ?>
            </select>
            <label><?php echo get_string('contents_month', 'local_lmsdata'); ?> &nbsp;</label>
            <select name="endday" class="w_80">
                <?php
                $days = lmsdata_get_days($endday);
                echo $days;
                ?>
            </select>
            <label><?php echo get_string('contents_day', 'local_lmsdata'); ?> &nbsp;</label>

            <input type="submit" class="blue_btn" value="<?php echo get_string('stats_search', 'local_lmsdata'); ?>" onclick="#" style="margin:0 0 5px 5px;"/>          
        </form><!--Search Area2 End-->
        <table>
            <tr title="항목을 클릭하여 항목별로 정렬을 할 수 있습니다."/>
                <th style='width:10%;'><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th style='width:15%;' id="username">이름</th>
                <th style='width:15%;' id="userid">아이디</th>
                <th style='width:15%;' id="role">역할<br>(임직원 / 멘토 / 교육생 / 관리자)</th>
                <th style='width:10%;' id="totwatch">총 시청시간</th>
                <th style='width:10%;'>콘텐츠로그</th>
            </tr>
            <?php if (!$contact_stats) { ?>
                <tr>
                    <td colspan="24">통계내용이 없습니다.</td>
                </tr>
                <?php
            } else {

                $startnum = 1 + (($currpage - 1) * $perpage);

                foreach ($contact_stats as $contact_stat) {
                    switch ($contact_stat->usergroup) {
                        case 'sa':
                            if (current_language() == 'ko') {
                                $usergroup = '관리자';
                            } else {
                                $usergroup = 'Siteadmin';
                            }
                            break;
                        case 'pr':
                            if (current_language() == 'ko') {
                                $usergroup = '임직원';
                            } else {
                                $usergroup = 'Employee';
                            }
                            break;
                        case 'ad':
                            if (current_language() == 'ko') {
                                $usergroup = '멘토';
                            } else {
                                $usergroup = 'Mento';
                            }
                            break;
                        case 'rs':
                            if (current_language() == 'ko') {
                                $usergroup = '교육생';
                            } else {
                                $usergroup = 'Student';
                            }
                            break;
                    }
                    ?>
                    <tr>

                        <td style='width:10%;'><?php echo $startnum++; ?></td>
                        <td style='width:15%;'><?php echo $contact_stat->lastname; ?></td>
                        <td style='width:15%;'><?php echo $contact_stat->username; ?></td>
                        <td style='width:15%;'><?php echo $usergroup; ?></td>
                        <td style='width:10%;'><?php echo gmdate('H', $contact_stat->contentsplaytime) == 00 ? gmdate('i:s', $contact_stat->contentsplaytime) : gmdate('H:i:s', $contact_stat->contentsplaytime); ?></td>
                        <td style='width:10%;'><input type="button" title="<?php echo get_string('stats_view', 'local_lmsdata'); ?>" alt="<?php echo get_string('stats_view', 'local_lmsdata'); ?>" id="view" class="blue_btn" value="<?php echo get_string('stats_view', 'local_lmsdata'); ?>" onclick="contents_log(<?php echo $contact_stat->id ?>)"></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table><!--Table End-->


        <?php
        if (($count_courses / $perpage) > 1) {
            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        }
        ?>
    </div><!--Content End-->
</div> <!--Contents End-->
<script type="text/javascript">
    var sort;
    $(window).ready(function () {
        sort = '<?php echo $viewtypesort; ?>';
        setclearbutton();
    });
    function course_edit_popup(id) {
        var tag = $("<div></div>");
        $.ajax({
            url: '<?php echo $SITECFG->wwwroot . '/chamktu/stats/course_form.php'; ?>',
            data: {
                parent: $('[name=parent]').val(),
                category: $('[name=category]').val(),
                id: id
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: '<?php echo get_string('stats_learningactivitystatus', 'local_lmsdata'); ?>',
                    modal: true,
                    width: 600,
                    maxHeight: getWindowSize().height - 20,
                    close: function () {
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }
    function course_all_excel() {
<?php
$query_string = '';
if (!empty($param)) {
    $query_array = array();
    foreach ($param as $key => $value) {
        $query_array[] = urlencode($key) . '=' . urlencode($value);
    }
    $query_string = '?' . implode('&', $query_array);
}
?>
        var url = "course_all.excel.php<?php echo $query_string; ?>";

        document.location.href = url;
    }

    $('th').click(function () {
        var viewtype = $(this).attr("id");
        var viewtypesort;
        $('input[name=page]').val("<?php echo $currpage; ?>");
        $('input[name=viewtype]').val(viewtype);
        if (sort == 'asc') {
            viewtypesort = 'desc';
        } else {
            viewtypesort = 'asc';

        }
        $('input[name=viewtypesort]').val(viewtypesort);
        $('#course_search').submit();
    });



    function contents_log(userid) {
        var tag = $("<div id='contents_log_popup'></div>");
        $.ajax({
            url: '<?php echo $SITECFG->wwwroot . '/chamktu/stats/contents_view.ajax.php'; ?>',
            method: 'POST',
            data: {
                userid: userid,
                username: '<?php echo $username ?>',
                timestart: <?php echo $date_start ?>,
                timeend: <?php echo $date_end ?>
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: '콘텐츠 접속 로그',
                    modal: true,
                    width: 800,
                    maxHeight: getWindowSize().height - 20,
                    buttons: [{id: 'close',
                            text: '닫기',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    open: function () {
                        var t = $(this).parent();
                        var w = $(window);
                        var s = getWindowSize();

                        var x = (s.width / 2) - (t.width() / 2) + w.scrollLeft();
                        if (x < 0)
                            x = 0;
                        var y = (s.height / 2) - (t.height() / 2) + w.scrollTop();
                        if (y < 0)
                            y = 0;
                        t.offset({
                            top: y,
                            left: x
                        });
                    },
                    close: function () {
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }

    function search_usergroup_popup() {
        var tag = $("<div id='search_user_popup'></div>");
        $.ajax({
            url: '<?php echo $CFG->wwwroot . '/chamktu/stats/search_user.php'; ?>',
            method: 'POST',
            success: function (data) {
                tag.html(data).dialog({
                    title: '<?php echo get_string('user_search', 'local_lmsdata'); ?>',
                    modal: true,
                    width: 800,
                    resizable: false,
                    height: 400,
                    buttons: [{id: 'close',
                            text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    close: function () {
                        $('#frm_search_user').remove();
                        $(this).dialog('destroy').remove();
                        setclearbutton();
                    }
                }).dialog('open');
            }
        });
    }

    function search_user_clear() {
        $("input[name=username]").attr("value", '');
        setclearbutton();
    }

    function setclearbutton() {
        if ($("input[name=username]").val() != '') {
            $("#clearbutton").css('display', '');
        } else {
            $("#clearbutton").css('display', 'none');
        }
    }
</script>
<?php include_once ('../inc/footer.php'); ?>

