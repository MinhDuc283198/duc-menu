<?php
/**
 * 매출정산 월별정산 통계
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 100, PARAM_INT);
$classyear = optional_param('classyear', date('Y', time()), PARAM_INT);
$mon = optional_param('mon', 0, PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

//검색용 파라미터
if ($classyear) {
    $sql_where .= " and YEAR(from_unixtime(lca.timecreated)) = $classyear";
}
if ($mon) {
    $sql_where .= " and MONTH(from_unixtime(lca.timecreated)) = $mon";
}
$sql_where .= " and lc.classnum != '' and lc.classnum < 100 ";

$sql_select = "SELECT lco.* 
,(select count(*) from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lc.parentcourseid = lco.id  and lca.discounttype = 'member' $sql_where ) as  membercount
 ,(select count(*) from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lc.parentcourseid = lco.id and lca.discounttype = '' $sql_where ) as  normalcount
 ,(select count(*) from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lc.parentcourseid = lco.id and lca.discounttype = 'group' $sql_where ) as groupcount
 ,(select sum(lca.price) from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lc.parentcourseid = lco.id $sql_where ) as totalprice
    ";
$sql_from = " FROM  {lmsdata_course} lco
              join {lmsdata_class} lc on lc.parentcourseid = lco.id ";

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_orderby, $params);

$membercount = $DB->get_record_sql("select count(*) as count from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lca.discounttype = 'member' $sql_where ");
$normalcount = $DB->get_record_sql("select count(*) as count from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lca.discounttype = '' $sql_where ");
$groupcount = $DB->get_record_sql("select count(*) as count from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lca.discounttype = 'group' $sql_where ");
$totalcount = $membercount->count + $normalcount->count + $groupcount->count;
$totalprice = $DB->get_record_sql("select sum(price) as price from {lmsdata_course_applications} lca join {lmsdata_class} lc on lca.courseid = lc.id where lca.status = 'apply' and lca.price !='' $sql_where ");

$js = array(
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
$arr = array(1 => '직무', 2 => '위탁');

if (!$excel) {
    ?>

    <?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_stats.php'); ?>

        <div id="content">
            <h3 class="page_title">매출정산</h3>
            <br/><br/><br/>
            <input type="button"  class="orange_btn" onclick="javascript:location.href = 'edu_sales_status.php'" value="월별정산"/>
            <input type="button"  class="" onclick="javascript:location.href = 'edu_sales_status_classnum.php'" value="기수별정산"/>
            <form name="course_search" id="course_search" class="search_area" action="edu_sales_status.php" method="get">
                <input type="hidden" name="page" value="1" />
                <div style="float:left;">
                    <select title="year" name="classyear" id="classyear" class="w_120" onchange="year_changed(this, 'lmsdata_class');">
                        <option value="0">년도</option>
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);

                        foreach (range(2017, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                        ?>
                    </select> 
                    <select title="mon" name="mon" id="mon" class="w_160" onchange="$('form.search_area').submit();">
                        <option value="0">전체</option>
                        <?php
                        for ($x = 1; $x <= 12; $x++) {
                            $selected = '';
                            if ($x == $mon) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $x . '"' . $selected . '> ' . $x . '월</option>';
                        }
                        ?>
                    </select>
                </div>
                <div style="float:right;">
                    <input type="hidden" value="0" name="excel" >
                    <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
                </div>
            </form><!--Search Area2 End-->
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="50%">과정명</th>
                        <th scope="row" width="10%">영재원</th>
                        <th scope="row" width="10%">일반</th>
                        <th scope="row" width="10%">단체</th>
                        <th scope="row" width="10%">합계(명)</th>
                        <th scope="row" width="10%">수익(원)</th>

                    </tr>
                </thead>
                <?php if ($count_courses === 0) { ?>
                    <tr>
                        <td colspan="11">데이터가 없습니다.</td>
                    </tr>
                    <?php
                } else {
                    foreach ($courses as $course) {
                        ?>
                        <tr>
                            <td><?php echo $course->coursename ?></td>
                            <td><?php echo $course->membercount ?></td>
                            <td><?php echo $course->normalcount ?></td>
                            <td><?php echo $course->groupcount ?></td>
                            <td><?php echo $course->membercount + $course->normalcount + $course->groupcount ?></td>
                            <td><?php echo number_format($course->totalprice) ?></td>
                        </tr>
                        <?php
                    }

                    echo "<tr><td>합계</td>";
                    echo "<td>$membercount->count</td>";
                    echo "<td>$normalcount->count</td>";
                    echo "<td>$groupcount->count</td>";
                    echo "<td>$totalcount</td>";
                    echo "<td>" . number_format($totalprice->price) . "</td></tr>";
                }
                ?>    
            </table><!--Table End-->
            <div id="btn_area">            

            </div>
            <?php
//            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
            ?>            
        </div><!--Content End-->
    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $fields = array(
        '과정명',
        '영재원',
        '일반',
        '단체',
        '합계(명)',
        '수익(원)'
    );
    $fields2 = array(
        '초등',
        '중등',
        '기타'
    );

    $date = date('Y-m-d', time());
    $filename = 'edu_sales_status_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    foreach ($courses as $course) {
        $col = 0;
        $worksheet[0]->write($row, $col++, $course->coursename);
        $worksheet[0]->write($row, $col++, $course->membercount);
        $worksheet[0]->write($row, $col++, $course->normalcount);
        $worksheet[0]->write($row, $col++, $course->groupcount);
        $worksheet[0]->write($row, $col++, $course->membercount + $course->normalcount + $course->groupcount);
        $worksheet[0]->write($row, $col++, number_format($course->totalprice));

        $row++;
    }
    $worksheet[0]->write($row, 0, '합계');
    $worksheet[0]->write($row, 1, $membercount->count);
    $worksheet[0]->write($row, 2, $normalcount->count);
    $worksheet[0]->write($row, 3, $groupcount->count);
    $worksheet[0]->write($row, 4, $totalcount);
    $worksheet[0]->write($row, 5, number_format($totalprice->price));

    $workbook->close();
    die;
}
?>

<script type="text/javascript">
    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }

</script>    
