<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/lib/gdlib.php');
require_once($CFG->dirroot.'/lib/filelib.php');
require_once($CFG->libdir . '/filestorage/file_storage.php');
require_once($CFG->libdir . '/filestorage/stored_file.php');


$password = optional_param('password', '', PARAM_RAW);
$errmsg = ''; // Prevents eclipse warnings.

$imsilength = $CFG->minpasswordlength;
$CFG->minpasswordlength = 8;

if (!check_password_policy($password, $errmsg)) {
    $errors['password'] = $errmsg;
    //$errors['password2'] = $errmsg;
    $returnvalue->status = 'fail';
    $returnvalue->errors = $errmsg;
}else{
    $returnvalue->status = 'success';
}

$CFG->minpasswordlength = $imsilength;

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);