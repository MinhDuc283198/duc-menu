<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
  $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/notices_write.php');
  redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
require_once($CFG->libdir . '/filestorage/file_storage.php');
require_once($CFG->libdir . '/filestorage/stored_file.php');
require_once($CFG->libdir . '/filelib.php');

$type = optional_param('type', 1, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$perpage = optional_param('perpage', 10, PARAM_INT);
$create = optional_param('create', false, PARAM_BOOL);

$context = context_system::instance();
$nav = array('top' => 'site', 'left' => 'board', 'sub' => 'notice');

$id = optional_param("id", 0, PARAM_INT);
$mod = optional_param("mod", "write", PARAM_TEXT);

$admins = explode(',', $CFG->siteadmins);
$superadmin = $admins[0];
if ($superadmin != $USER->id) {
  if ($id != $USER->id) {
    echo '<script type="text/javascript">document.location.href="./infadmin.php"</script>';
  }
}

$temp = new stdclass();
$fullname = '';
if ($mod == 'edit') {
  $sql = "SELECT u.*, lu.usergroup, itemid "
          . "FROM {user} u "
          . "JOIN {lmsdata_user} lu ON lu.userid = u.id "
          . "LEFT JOIN (SELECT userid as fuserid, itemid FROM {files} WHERE component = 'user' AND filesize >0) f ON u.id = fuserid "
          . "WHERE u.id = " . $id;
  $temp = $DB->get_record_sql($sql);

  $file_obj = $DB->get_record('files', array('itemid' => $temp->itemid, 'license' => 'allrightsreserved'));
  if (!empty($file_obj)) {
    $file_stored = get_file_storage()->get_file_instance($file_obj);

    $file_url = file_encode_url("$CFG->wwwroot/pluginfile.php", '/' . $file_stored->get_contextid() . '/' . $file_stored->get_component() . '/' .
            $file_stored->get_filearea() . $file_stored->get_filepath() . $file_stored->get_itemid() . '/' . $file_stored->get_filename());
  }

  $fullname = fullname($temp);
}
?>
<?php include_once('../inc/header.php'); ?>
<div id="contents">
    <?php include_once('../inc/sidebar_support.php'); ?>
    <div id="content">
        <?php if (!empty($id) && $mod === 'edit') { ?>
          <h3 class="page_title">관리자 수정</h3>
          <div class="page_navbar"><a href="../support/evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
              <a href="./infadmin.php"><?php echo get_string('admin_management', 'local_lmsdata'); ?></a> > 
              <strong>관리자 수정</strong>
          </div>
          <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./infadmin_submit.php?mod=edit&id=<?php echo $id; ?>" method="POST">
            <?php } else { ?>
              <h3 class="page_title"><?php echo get_string('user_adminreg', 'local_lmsdata'); ?></h3>
              <div class="page_navbar"><a href="../support/evaluation/survey_form.php"><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
                  <a href="./infadmin.php"><?php echo get_string('admin_management', 'local_lmsdata'); ?></a> > 
                  <strong><?php echo get_string('user_adminreg', 'local_lmsdata'); ?></strong>
              </div>
              <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./infadmin_submit.php" method="POST">
                <?php } ?>
                <table cellpadding="0" cellspacing="0" class="detail">

                    <tbody>

                        <tr>
                            <td class="field_title"><span class="red">*</span> <?php echo get_string('user_id', 'local_lmsdata'); ?></td>
                            <td class="field_value">
                                <input type="text" title="id" class="w_300" name="userid" <?php if ($mod == 'edit') echo "disabled='true'"; ?> value="<?php echo (!empty($temp->username)) ? $temp->username : ""; ?>"/>
                                <?php if ($mod == 'write') { ?>
                                  <input type="button" name="id_chk" value="<?php echo get_string('user_duplicateinquiry', 'local_lmsdata'); ?>" class="gray_btn">
                                  <input type="hidden" name="usergroup"/>
                                  <input type="hidden" name="user_id"/>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><span class="red">*</span> <?php echo get_string('user_password', 'local_lmsdata'); ?></td>
                            <td class="field_value">
                                <input type="password" title="password" class="w_300" <?php if ($mod == 'edit') echo "disabled='true'"; ?>  name ="password" value=""/>
                                <?php if ($mod == 'edit') { ?><label for="pw_edit"><?php echo get_string('change_password', 'local_lmsdata'); ?></label><input type="checkbox" id="pw_edit" name="pw_edit"><?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><span class="red">*</span> <?php echo get_string('user_repassword', 'local_lmsdata'); ?></td>
                            <td class="field_value">
                                <input type="password" title="password" class="w_300" <?php if ($mod == 'edit') echo "disabled='true'"; ?> name ="repassword" value=""/>
                                <div id="password_fail2"></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><span class="red">*</span> <?php echo get_string('name', 'local_lmsdata'); ?></td>
                            <td class="field_value">
                                <input type="text" title="name" class="w_200" name ="username" value="<?php echo (!empty($fullname)) ? $fullname : ""; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><span class="red">*</span> <?php echo get_string('email', 'local_lmsdata'); ?></td>
                            <td class="field_value">
                                <input type="text" title="email" class="w_300" name ="email" value="<?php echo (!empty($temp->email)) ? $temp->email : ""; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><span class="red">*</span> <?php echo get_string('contact', 'local_lmsdata'); ?></td>
                            <td class="field_value">
                                <input type="text" title="phone" class="w_300" name ="phone" value="<?php echo (!empty($temp->phone1)) ? $temp->phone1 : ""; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title"><?php echo get_string('user_img', 'local_lmsdata'); ?></td>
                            <td class="field_value number">
                                <?php echo (!empty($temp->itemid) && $temp->itemid > 0 ? '<a name="file_link" href="' . $file_url . '">' . $file_stored->get_filename() . '<img src="../img/icon-attachment.png" alt="fileicon" class="icon-attachment"/></a>' : '') ?> 
                                <?php if (!empty($temp->itemid)) { ?>
                                  <input type="button" class="gray_btn_small" name="remove_button" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="remove_file();"/><br>
                                <?php } ?>
                                <input type="file" title="file" name="uploadfile" style="margin-top: 10px;"/> 
                                <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>
                                <input type="hidden" name="file_del" value="0"/>
                            </td>
                        </tr>


                    </tbody>

                </table>

                <div id="btn_area">
                    <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_lmsdata'); ?>" style="float: right;" />
                    <?php
                    if ($mod == 'edit') {
                      ?>
                      <input type="button" id="add_delete" class="red_btn" onclick="delete_admin_user('<?php echo $id; ?>');" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: right; margin-left: 15px;" />
                      <?php
                    }
                    ?>
                    <input type="button" id="admin_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float: left;" />
                </div> <!-- Bottom Button Area -->

            </form>
    </div>
</div>
<?php include_once('../inc/footer.php'); ?>


<script type="text/javascript">

  $(document).ready(function () {
      var check_true = 0;
      $('.field_value input[name=pw_edit]').click(function () {
          if ($('.field_value input[name=pw_edit]').prop('checked') == true) {
              $(".field_value input[name=password]").prop('disabled', false);
              $(".field_value input[name=repassword]").prop('disabled', false);
          } else {
              $(".field_value input[name=password]").prop('disabled', true);
              $(".field_value input[name=repassword]").prop('disabled', true);
          }
      });

      $('#admin_list').click(function () {
          location.href = "./infadmin.php";
      });


      $(".field_value input[name=userid]").keyup(function () {
          check_true = 0;
      });

      $(".field_value input[name=userid]").click(function () {
          check_true = 0;
      });

      $(".field_value input[name=id_chk]").click(function () {
          if ($(".field_value input[name=userid]").val().trim() == '') {
              alert("아이디를 입력해 주세요");
              return false;
          } else {
              $.ajax({
                  type: 'POST',
                  data: {
                      type: 'username',
                      sort: 'usertoadmin',
                      chkdata: $(".field_value input[name=userid]").val()
                  },
                  url: './user_data_chk.php',
                  success: function (data) {
                      console.log(data);
                      if (data && data.usergroup == 'sa') {
                          alert("현재 사용중인 아이디 입니다.");
                      } else {
                          alert("사용 가능한 아이디 입니다.");
                          check_true = 1;
                          if (data && data.usergroup != '') {
                              $(".field_value input[name=user_id]").attr('value', data.userid);
                              $(".field_value input[name=usergroup]").attr('value', data.usergroup);
                              $(".field_value input[name=password]").removeAttr('value');
                              $(".field_value input[name=repassword]").removeAttr('value');
                              $(".field_value input[name=password]").attr('disabled', true);
                              $(".field_value input[name=repassword]").attr('disabled', true);
                              $(".field_value input[name=username]").removeAttr('value');
                              $(".field_value input[name=username]").attr('value', data.username);
                              $(".field_value input[name=email]").attr('value', data.email);
                              $(".field_value input[name=phone]").attr('value', data.phone1);
                              $(".field_value input[name=username]").attr('disabled', true);
                              $(".field_value input[name=email]").attr('disabled', true);
                              $(".field_value input[name=phone]").attr('disabled', true);
                          } else {
                              $(".field_value input[name=user_id]").removeAttr('value');
                              $(".field_value input[name=usergroup]").removeAttr('value');
                              $(".field_value input[name=password]").removeAttr('value');
                              $(".field_value input[name=repassword]").removeAttr('value');
                              $(".field_value input[name=password]").removeAttr('disabled');
                              $(".field_value input[name=repassword]").removeAttr('disabled');
                              $(".field_value input[name=username]").removeAttr('value');
                              $(".field_value input[name=email]").removeAttr('value');
                              $(".field_value input[name=phone]").removeAttr('value');
                              $(".field_value input[name=username]").removeAttr('disabled');
                              $(".field_value input[name=email]").removeAttr('disabled');
                              $(".field_value input[name=phone]").removeAttr('disabled');
                          }
                      }
                  },
              });
          }
      });

      $('#frm_popup_submit').submit(function (event) {
          $("#password_fail2").html("");
          var userid = $(".field_value input[name=userid]").val();
          if (userid.trim() == '') {
              alert("아이디를 입력해 주세요");
              return false;
          }
          ;
<?php
if ($mod == 'write') {
  ?>
            if (check_true == 0) {
                alert("중복조회를 해주세요");
                return false;
            }
  <?php
}
?>
          var usergroup = $(".field_value input[name=usergroup]").val();
          var user_id = $(".field_value input[name=user_id]").val();
          if (usergroup == '' && user_id == '') {
                      if ($(".field_value input[name=password]").prop('disabled') == false) {
                  var password = $(".field_value input[name=password]").val();
                  if (password.trim() == '') {
                      alert("비밀번호를 입력해 주세요");
                      return false;
                  }
                  ;
                  var repassword = $(".field_value input[name=repassword]").val();
                  if (repassword.trim() == '') {
                      alert("비밀번호 확인을 입력해 주세요");
                      return false;
                  }
                  ;
                  if (password != repassword) {
                      alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
                      return false;
                  }
                  var flag = false;
                  $.ajax({
                      url: './password_change.ajax.php',
                      type: "post",
                      data: {
                          password: password
                      },
                      async: false,
                      success: function (data) {
                          if (data.status == "fail") {
                              flag = false;
                              //alert("비밀번호 재입력");
                              $("#password_fail2").html(data.errors);
                          } else {
                              flag = true;
                          }
                      },
                      error: function (e) {
                          //alert("에러");
                          //console.log(e.responseText);
                          //return false;
                      }
                  });
                  return flag;
              }
              var username = $(".field_value input[name=username]").val();
              if (username.trim() == '') {
                  alert("이름을 입력해 주세요");
                  return false;
              }
              ;
              var email = $(".field_value input[name=email]").val();
              if (email.trim() == '') {
                  alert("<?php echo get_string('user_emailalert', 'local_lmsdata'); ?>");
                  return false;
              }
              ;
              var phone = $(".field_value input[name=phone]").val();
              return false;
              if (phone.trim() == '') {
                  alert("연락처를 입력해 주세요");
                  return false;
              }
              ;
      }
      });
  });
  function remove_file() {

      $("a[name='file_link']").remove();
      $("input[name='remove_button']").remove();
      $("input[name='file_del']").val(1);

  }

  function delete_admin_user(did) {
      if (confirm("정말 삭제하시겠습니까??") == true) {
          location.href = '<?php echo 'infadmin_submit.php?id='; ?>' + did + '<?php echo '&mod=delete'; ?>';
      } else {
          return false;
      }
  }
</script>
