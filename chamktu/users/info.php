<?php
/**
 * 회원관리 페이지 리스트와 검색만 지원
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
  $SESSION->wantsurl = (string) new moodle_url('/chamktu/users/info.php');
  redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$page = optional_param('page', 1, PARAM_INT); //현재페이지
$searchtext = optional_param('searchtext', '', PARAM_RAW); //검색값
$phone2 = optional_param('phone2', '', PARAM_INT);
$username = optional_param('username', '', PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT); //한페이지에 보여줄 로우수
$excel = optional_param('excel', 0, PARAM_INT);
$psosok = optional_param('psosok', '', PARAM_RAW); //소속
$rank = optional_param('rank', '', PARAM_RAW); //직급
$position = optional_param('position', '', PARAM_RAW); //직책
$usertype = optional_param('usertype', '', PARAM_RAW); //회원구분
//데이터 가져오기

$sql = 'SELECT 
            ur.*, 
            lu.id AS lid, lu.usertypecode, lu.statuscode, lu.usergroup, lu.psosok, lu.rank, lu.position, lu.entrydate ';

$sql_from = 'FROM {user} ur
             JOIN {lmsdata_user} lu ON ur.id = lu.userid ';
if ($searchtext) {
  $where[] = ' (' . $DB->sql_like('ur.lastname', ':searchtext') . ' OR ' . $DB->sql_like('ur.phone2', ':phone2') . ' OR ' . $DB->sql_like('ur.username', ':username') . ')';
  $params = array(
	'searchtext' => '%' . $searchtext . '%',
	'phone2' => '%' . $searchtext . '%',
	'username' => '%' . $searchtext . '%'
  );
}

//회원구분 조건 추가
if (!empty($usertype)) {
  $where[] = ' lu.usertypecode = :usertype ';
  $params['usertype'] = $usertype;
}

if (!empty($psosok)) {
  $where[] = ' lu.psosok = :psosok ';
  $params['psosok'] = $psosok;
}

if (!empty($rank)) {
  $where[] = ' lu.rank = :rank ';
  $params['rank'] = $rank;
}

if (!empty($position)) {
  $where[] = ' lu.position = :position ';
  $params['position'] = $position;
}

$order = ' ORDER BY ur.timecreated DESC ';

if (!empty($where)) {
  $where = ' WHERE ' . implode(' AND ', $where);
}

$type_arr = array(
    10 => JEI_USER_TYPECODE_10,
    20 => JEI_USER_TYPECODE_20,
    30 => JEI_USER_TYPECODE_30,
);
if (!$excel) {

  $offset = ($page - 1) * $perpage;
  $users = $DB->get_records_sql($sql . $sql_from . $where . $order, $params, $offset, $perpage);
  $count_sql = "SELECT count(ur.id) ";
  $total_count = $DB->count_records_sql($count_sql . $sql_from . $where, $params);
  $num = $total_count - $offset;


  $js = array(
	$CFG->wwwroot . '/chamktu/manage/course_list.js'
  );

  include_once (dirname(dirname(__FILE__)) . '/inc/header.php');
  ?>
  <div id="contents">
	<?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_users.php'); ?>

      <div id="content">
  	  <h3 class="page_title">회원관리</h3>
  	  <div class="page_navbar"><a href="./info.php"><?php echo get_string('user_management', 'local_lmsdata'); ?></a> > <strong><?php echo get_string('user_management', 'local_lmsdata'); ?></strong></div>

  	  <form name="" id="course_search" class="search_area" action="info.php" method="get">
  		<input type="hidden" title="page" name="page" value="1" />
  		<input type="hidden" title="excel" name="excel" value="0" />
  		<select title="class" name="usertype" id="usertype" class="w_200">
  		    <option value="0">회원구분</option>
			<?php
			//회원구분에 따른 검색 추가
			foreach ($type_arr as $key => $val) {
			  $selected = '';
			  if ($key == $usertype) {
			    $selected = ' selected';
			  }
			  echo '<option value="' . $key . '"' . $selected . '> ' . $val . '</option>';
			}
			?>
  		</select>
  		<select title="class" name="psosok" id="psosok" class="w_200">
  		    <option value="0">소속</option>
			<?php
			$psosokSql = ' SELECT psosok FROM {lmsdata_user} WHERE psosok <> "" GROUP BY psosok ORDER BY psosok ASC ';
			$psosoks = $DB->get_records_sql($psosokSql);
			foreach ($psosoks as $ps) {
			  $selected = '';
			  if ($ps->psosok == $psosok) {
			    $selected = ' selected';
			  }
			  echo '<option value="' . $ps->psosok . '"' . $selected . '> ' . $ps->psosok . '</option>';
			}
			?>
  		</select>
  		<select title="class" name="rank" id="rank" class="w_200">
  		    <option value="0">직급</option>
			<?php
			$rankSql = ' SELECT rank FROM {lmsdata_user} WHERE rank <> "" GROUP BY rank ORDER BY rank ASC ';
			$ranks = $DB->get_records_sql($rankSql);
			foreach ($ranks as $ra) {
			  $selected = '';
			  if ($ra->rank == $rank) {
			    $selected = ' selected';
			  }
			  echo '<option value="' . $ra->rank . '"' . $selected . '> ' . $ra->rank . '</option>';
			}
			?>
  		</select>
  		<select title="class" name="position" id="position" class="w_200">
  		    <option value="0">직책</option>
			<?php
			$positionSql = ' SELECT position FROM {lmsdata_user} WHERE position <> "" GROUP BY position ORDER BY position ASC ';
			$positions = $DB->get_records_sql($positionSql);
			foreach ($positions as $pt) {
			  $selected = '';
			  if ($pt->position == $position) {
			    $selected = ' selected';
			  }
			  echo '<option value="' . $pt->position . '"' . $selected . '> ' . $pt->position . '</option>';
			}
			?>
  		</select>
  		<input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="이름/아이디/연락처를 입력하세요."  class="search-text"/>
  		<input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
  	  </form><!--Search Area2 End-->
  	  <div style="float:right;">
  		<input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/>                  
  	  </div>
  	  <table>
  		<caption class="hidden-caption">회원관리</caption>
  		<thead>
  		    <tr>
  			  <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
  			  <th scope="row">회원구분</th>
  			  <th scope="row" width="10%"><?php echo get_string('name', 'local_lmsdata'); ?></th>
  			  <th scope="row" width="10%">아이디/위탁코드</th>
  			  <th scope="row">소속</th>
  			  <th scope="row">직급</th>
  			  <th scope="row">직책</th>
  			  <th scope="row">입사일</th>
  			  <th scope="row"><?php echo get_string('contact', 'local_lmsdata'); ?></th>
  			  <th scope="row"><?php echo get_string('email', 'local_lmsdata'); ?></th>
  			  <th scope="row" width="10%">교육이력관리</th>
  			  <th scope="row">권한</th>
  			  <th scope="row">상태</th>
  		    </tr>
  		</thead>
  		<tbody>
			<?php
			$admins = explode(',', $CFG->siteadmins);
			$superadmin = $admins[0];
			if (!empty($users)) {
			  foreach ($users as $user) {
			    ?>
			    <tr>
				  <td width="5%"><?php echo $num; ?></td>
				  <td><?php echo!$user->usertypecode ? '-' : $type_arr[$user->usertypecode]; ?></td>
				  <td><?php echo fullname($user); ?></td>
				  <td><?php echo '<a href="./info_add.php?id=' . $user->id . '&mod=edit" style="color:#00769A;">' . $user->username . '</a>'; ?></td>
				  <td><?php echo empty($user->psosok) ? '-' : $user->psosok; ?></td>
				  <td><?php echo empty($user->rank) ? '-' : $user->rank; ?></td>
				  <td><?php echo empty($user->position) ? '-' : $user->position; ?></td>
				  <td><?php echo date('Y-m-d', $user->entrydate); ?></td>
				  <td><?php echo!$user->phone2 ? (!($user->phone1) ? '-' : $user->phone1) : $user->phone2; ?></td>
				  <td><?php echo!$user->email ? '-' : $user->email; ?></td>
				  <td><input type="button" name="" value="교육이력관리" onclick="popup_courses('<?php echo $user->id; ?>')" /></td>
				  <td><?php
					if (in_array($user->id, $admins) && $user->id == $superadmin) {
					  echo '관리자';
					} else if (in_array($user->id, $admins) && $user->id != $superadmin) {
					  echo '관리자(서브)';
					} else {
					  echo '사용자';
					}
					?>
				  </td>
				  <td><?php echo empty($user->suspended) ? '활성' : '비활성' ?></td>
			    </tr>
			    <?php
			    $num--;
			  }
			} else {
			  ?>
    		    <tr>
    			  <td colspan="13">등록된 회원이 없습니다.</td>
    		    </tr>
			<?php } ?>
  		</tbody>
  	  </table><!--Table End-->

	    <?php
	    if (($total_count / $perpage) > 1) {
		print_paging_navbar_script($total_count, $page, $perpage, 'javascript:cata_page(:page);');
	    }
	    ?>


      </div><!--Content End-->

  </div> <!--Contents End-->
  <script type="text/javascript">

    function cata_page(page) {
  	$('[name=page]').val(page);
  	$('#course_search').submit();
    }

    function popup_courses(userid) {
  	var tag = $("<div id='course_list'></div>");

  	$.ajax({
  	    url: "info_list.php?userid=" + userid,
  	    method: 'POST',
  	    success: function (data) {
  		  tag.html(data).dialog({
  			title: '교육 이력 관리',
  			modal: true,
  			width: 900,
  			resizable: false,
  			height: 700,
  			close: function () {
  			    $('#course_list').remove();
  			    $(this).dialog('destroy').remove()
  			}
  		  }).dialog('open');
  	    }
  	});
    }
    function user_excel_upload() {
  	var tag = $("<div id='course_list'></div>");
  	$.ajax({
  	    url: "sync_user.php",
  	    method: 'POST',
  	    success: function (data) {
  		  tag.html(data).dialog({
  			title: '사용자 엑셀업로드',
  			modal: true,
  			width: 500,
  			resizable: false,
  			height: 400,
  			close: function () {
  			    $('#course_list').remove();
  			    $(this).dialog('destroy').remove()
  			}
  		  }).dialog('open');
  	    }
  	});
    }
    function course_list_excel() {
  	$("input[name='excel']").val(1);
  	$("#course_search").submit();
    }
  </script>

  <?php
  include_once ('../inc/footer.php');
} else {
  $users = $DB->get_records_sql($sql . $sql_from . $where . $order, $params);
  $count_sql = "SELECT count(ur.id) ";
  $num = $DB->count_records_sql($count_sql . $sql_from . $where, $params);

  $fields = array(
	'번호',
	'회원구분',
	'이름',
	'아이디',
	'소속',
	'직급',
	'직책',
	'입사일',
	'연락처',
	'이메일',
	'권한',
	'상태'
  );

  $filename = '회원관리(' . date('Ymd') . ').xls';

  $workbook = new MoodleExcelWorkbook('-');
  $workbook->send($filename);

  $worksheet = array();

  $worksheet[0] = $workbook->add_worksheet('');
  $col = 0;
  foreach ($fields as $fieldname) {
    $worksheet[0]->write(0, $col, $fieldname);
    $col++;
  }

  $row = 1;
  $worksheet[0]->set_column(7, 7, 20, array('v_align' => 'center'));
  $worksheet[0]->set_column(8, 8, 20, array('v_align' => 'center'));
  foreach ($users as $user) {
    $admins = explode(',', $CFG->siteadmins);
    $superadmin = $admins[0];
    $col = 0;
    $worksheet[0]->write($row, $col++, $num--);
    $worksheet[0]->write($row, $col++, empty($user->usertypecode) ? '-' : $type_arr[$user->usertypecode]);
    $worksheet[0]->write($row, $col++, fullname($user));
    $worksheet[0]->write($row, $col++, $user->username);
    $worksheet[0]->write($row, $col++, empty($user->psosok) ? '-' : $user->psosok);
    $worksheet[0]->write($row, $col++, empty($user->rank) ? '-' : $user->rank);
    $worksheet[0]->write($row, $col++, empty($user->position) ? '-' : $user->position);
    $worksheet[0]->write($row, $col++, date('Y-m-d', $user->timecreated));
    $worksheet[0]->write($row, $col++, $user->phone1);
    $worksheet[0]->write($row, $col++, $user->email);
    if (in_array($user->id, $admins) && $user->id == $superadmin) {
	$worksheet[0]->write($row, $col++, '관리자');
    } else if (in_array($user->id, $admins) && $user->id != $superadmin) {
	$worksheet[0]->write($row, $col++, '관리자(서브)');
    } else {
	$worksheet[0]->write($row, $col++, '사용자');
    }
    $worksheet[0]->write($row, $col++, empty($user->suspended) ? '활성' : '비활성');
    $row++;
  }

  $workbook->close();
  die;
}
?>
