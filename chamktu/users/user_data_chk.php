<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$type = optional_param('type', 'username', PARAM_RAW);
$sortfor = optional_param('sort', '', PARAM_RAW);
$chkdata = optional_param('chkdata', '', PARAM_RAW);

if (trim($sortfor) != '' && $sortfor == 'usertoadmin') {

  $checksql = 'SELECT lu.userid, u.username, lu.usergroup, u.email, u.phone1 FROM {user} u JOIN {lmsdata_user} lu on lu.userid = u.id WHERE username = :username ';
  $check = $DB->get_record_sql($checksql, array('username' => $chkdata));
  @header('Content-type: application/json; charset=utf-8');
  echo json_encode($check);
  
} else {
  if (trim($chkdata) != '') {
    if ($type == 'email') {
      $username = optional_param('username', '', PARAM_RAW);

      if ($DB->get_records('user', array($type => $chkdata))) {
        if ($DB->get_records('user', array($type => $chkdata, 'username' => $username)) && $DB->count_records('user', array($type => $chkdata)) < 2) {
          $check = 0;
        } else {
          $check = 1;
        }
      }
    } else {
      $check = $DB->get_records('user', array($type => $chkdata));
    }
    if ($check) {
      echo false;
    } else {
      echo true;
    }
  }
}