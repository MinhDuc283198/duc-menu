<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/chamktu/lib.php');
require_once($CFG->dirroot . '/user/lib.php');


include_once (dirname(dirname(__FILE__)) . '/inc/header.php');
?>
<div id="contents">
    <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_users.php'); ?>
    <div id="content" class="line-txt">
        <?php
        if (!empty($_FILES['user']['name'])) {
            $insertCount = 0;
            $updateCount = 0;

            $insertCount2 = 0;
            $updateCount2 = 0;
            $filename = $_FILES['user']['name'];
            $filepath = $_FILES['user']['tmp_name'];

            $objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
            $objReader->setReadDataOnly(true);
            $objExcel = $objReader->load($filepath);

            $objExcel->setActiveSheetIndex(0);
            $objWorksheet = $objExcel->getActiveSheet();
            $rowIterator = $objWorksheet->getRowIterator();

            foreach ($rowIterator as $row) { // 모든 행에 대해서
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
            }

            $maxRow = $objWorksheet->getHighestRow();
            siteadmin_println('멘토/교육생 추가......');
            $mentoInsertCount = 0;
            $mentoUpdateCount = 0;
            $stuInsertCount = 0;
            $stuUpdateCount = 0;
            // 멘토/교육생 등록
            for ($i = 2; $i <= $maxRow; $i++) {
                $lastname = trim($objWorksheet->getCell('B' . $i)->getValue()); // 이름 user->lastname
                $username = strtolower(trim($objWorksheet->getCell('C' . $i)->getValue()));  //  위탁코드 user->username
                $psosok = trim($objWorksheet->getCell('D' . $i)->getValue());  //  소속 lmsdata_user->psosok 
                $startdate = trim($objWorksheet->getCell('E' . $i)->getValue()); // 위탁일
                $phone = trim($objWorksheet->getCell('F' . $i)->getValue()); //  휴대폰번호   user->phone 
                $email = strtolower(trim($objWorksheet->getCell('G' . $i)->getValue())); // 이메일주소  user->email
                $usertypecode = trim($objWorksheet->getCell('H' . $i)->getValue()); // 멘토(Y),교육생(Y가아닐때)
                $suspended = trim($objWorksheet->getCell('I' . $i)->getValue()); // 계정잠금(Y)

                if (!empty($lastname)) {
                    $user = new stdClass();
                    $user->auth = 'manual';
                    $user->confirmed = 1;
                    $user->mnethostid = 1;
                    $user->lang = 'ko';
                    $user->firstname = '&nbsp;';
                    $user->lastname = $lastname;
                    if (empty($email)) {
                        siteadmin_println('이메일 주소는 필수입니다.(이름 :' . $user->lastname . ')');
                        continue;
                    }
                    $user->username = $username;
                    $user->email = $email;
                    $user->phone1 = $phone;
                    $user->phone2 = $phone;
                    $user->timezone = 99;
                    $user->timecreated = time();
                    $user->timemodified = time();
                    $user->description = "Excell 유저 추가";

                    $lmsdata_user = new stdClass();
                    $lmsdata_user->eng_name = '&nbsp;';
                    $lmsdata_user->b_temp = 1;
                    $lmsdata_user->b_mobile = 0;
                    $lmsdata_user->b_email = 0;
                    $lmsdata_user->b_tel = 0;
                    $lmsdata_user->b_univ = 0;
                    $lmsdata_user->b_major = 0;
                    $lmsdata_user->ehks = null;
                    $lmsdata_user->edhs = null;
                    $lmsdata_user->domain = null;
                    $lmsdata_user->hyhg = null;
                    $lmsdata_user->persg = null;
                    $lmsdata_user->psosok = $psosok;
                    $lmsdata_user->sex = '';
                    $lmsdata_user->phone1 = $phone;
                    $lmsdata_user->entrydate = strtotime($startdate . ' 00:00:00');


                    if (!$DB->record_exists('user', array('username' => $username))) {
                        $user->password = hash_internal_user_password($email, TRUE);

                        //이메일 주소가 동일하면 사용자를 생성하지 않음
                        if ($DB->record_exists('user', array('email' => $email))) {
                            siteadmin_println('동일한 이메일 주소가 이미 등록 되어있습니다.(이름 :' . $user->lastname . ', 이메일 : ' . $user->email . ')');
                            continue;
                        }
                        //계정 잠금 기능
                        if (strtolower($suspended) == 'y') {
                            $user->suspended = 1;
                        }
                        $userid = $DB->insert_record('user', $user);
                        $user->id = $userid;

                        if (!get_user_preferences('auth_forcepasswordchange')) {
                            set_user_preference('auth_forcepasswordchange', 1, $user->id);
                        }
                        $lmsdata_user->userid = $userid;
                        $record = new stdClass();
                        $record->contextlevel = CONTEXT_USER;
                        $record->instanceid = $userid;
                        $record->depth = 0;
                        $record->path = null; //not known before insert

                        if (!$id = $DB->get_record('context', array('contextlevel' => CONTEXT_USER, 'instanceid' => $user->id))) {
                            $record->id = $DB->insert_record('context', $record);
                        } else {
                            $record->id = $id->id;
                        }
                        if (!is_null('/' . SYSCONTEXTID)) {
                            $record->path = '/' . $record->id;
                            $record->depth = substr_count($record->path, '/');
                            $DB->update_record('context', $record);
                        }
                        if (strtolower($usertypecode) == 'y') {
                            $lmsdata_user->usergroup = 'ad';
                            $lmsdata_user->usertypecode = 20;
                            $mentoInsertCount++;
                        } else {
                            $lmsdata_user->usergroup = 'rs';
                            $lmsdata_user->usertypecode = 30;
                            $stuInsertCount++;
                        }

                        $DB->insert_record('lmsdata_user', $lmsdata_user);

                        siteadmin_println('사용자 추가(이름 :' . $user->lastname . ', E-mail :' . $user->email . ')');

                        // 비밀번호 변경 +1년 체크를 위해 추가
                        set_user_preference('auth_manual_passwordupdatetime', time(), $user->id);
                    } else {
                        $user = $DB->get_record('user', array('username' => $username));

                        //이메일 주소가 동일한 사용자가 있으면 업데이트 하지 않음
                        if ($DB->record_exists_sql('SELECT * FROM {user} WHERE id <> :id AND email = :email', array('id' => $user->id, 'email' => $email))) {
//                            siteadmin_println('동일한 이메일 주소가 이미 등록 되어있습니다.(이름 :' . $user->lastname . ', 이메일 : ' . $user->email . ')');
//                            continue;
                        } else {
                            $user->email = $email;
                        }
                        $user->lastname = $lastname;
                        $user->phone1 = $phone;
                        $user->phone2 = $phone;
                        $user->timecreated = strtotime($startdate . ' 00:00:00');
                        $user->timemodified = time();
                        $userid = $DB->update_record('user', $user);

                        $lmsdata_user = $DB->get_record('lmsdata_user', array('userid' => $user->id));
                        if (strtolower($usertypecode) == 'y') {
                            $lmsdata_user->usergroup = 'ad';
                            $lmsdata_user->usertypecode = 20;
                            $mentoUpdateCount++;
                        } else {
                            $lmsdata_user->usergroup = 'rs';
                            $lmsdata_user->usertypecode = 30;
                            $stuUpdateCount++;
                        }
                        $lmsdata_user->psosok = $psosok;
                        $lmsdata_user->phone1 = $phone;
                        $lmsdata_user->entrydate = strtotime($startdate . ' 00:00:00');
                        $luser = $DB->update_record('lmsdata_user', $lmsdata_user);

                        //계정 잠금 기능
                        if (!is_siteadmin($user) && ($USER->id != $user->id) && strtolower($suspended) == 'y') {
                            $user->suspended = 1;
                            // Force logout.
                            \core\session\manager::kill_user_sessions($user->id);
                            user_update_user($user, false);
                        } else if ($user->suspended == 1) {
                            $user->suspended = 0;
                            user_update_user($user, false);
                        }

                        siteadmin_println('사용자 업데이트(이름 :' . $user->lastname . ', E-mail :' . $user->email . ')');
                        $updateCount++;
                    }
                }
            }
            //임원 등록
            $objExcel->setActiveSheetIndex(1);
            $objWorksheet = $objExcel->getActiveSheet();
            $rowIterator = $objWorksheet->getRowIterator();

            foreach ($rowIterator as $row) { // 모든 행에 대해서
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
            }

            siteadmin_println('임직원추가......');
            $maxRow = $objWorksheet->getHighestRow();
            for ($i = 2; $i <= $maxRow; $i++) {
                $lastname = trim($objWorksheet->getCell('B' . $i)->getValue()); // 이름 user->lastname
                $username = strtolower(trim($objWorksheet->getCell('C' . $i)->getValue()));  //  아이디 user->username
                $psosok = trim($objWorksheet->getCell('D' . $i)->getValue());  //  소속 lmsdata_user->psosok 
                $rank = trim($objWorksheet->getCell('E' . $i)->getValue()); // 직급 lmsdata_user->rank
                $position = trim($objWorksheet->getCell('F' . $i)->getValue()); //  직책 lmsdata_user->position
                $startdate = trim($objWorksheet->getCell('G' . $i)->getValue()); // 입사일 lmsdata_user->entrydate
                $phone = trim($objWorksheet->getCell('H' . $i)->getValue()); // 휴대폰 번호 user->phone2, lmsdata_user->phone2
                $email = strtolower(trim($objWorksheet->getCell('I' . $i)->getValue())); // 이메일 user->email
                $suspended = trim($objWorksheet->getCell('J' . $i)->getValue()); // 계정잠금(Y)

                if (!empty($lastname)) {
                    $user = new stdClass();
                    $user->auth = 'manual';
                    $user->confirmed = 1;
                    $user->mnethostid = 1;
                    $user->lang = 'ko';
                    $user->firstname = '&nbsp;';
                    $user->lastname = $lastname;
                    if (empty($email)) {
                        $email = ' ';
                    }
                    $user->username = $username;
                    $user->email = $email;
                    $user->phone1 = $phone;
                    $user->phone2 = $phone;
                    $user->timezone = 99;
                    $user->timecreated = time();
                    $user->timemodified = time();
                    $user->description = "Excell 유저 추가";
                    $lmsdata_user = new stdClass();
                    $lmsdata_user->eng_name = '&nbsp;';
                    $lmsdata_user->usergroup = 'pr';
                    $lmsdata_user->usertypecode = 10;
                    $lmsdata_user->b_temp = 1;
                    $lmsdata_user->b_mobile = 0;
                    $lmsdata_user->b_email = 0;
                    $lmsdata_user->b_tel = 0;
                    $lmsdata_user->b_univ = 0;
                    $lmsdata_user->b_major = 0;
                    $lmsdata_user->ehks = null;
                    $lmsdata_user->edhs = null;
                    $lmsdata_user->domain = null;
                    $lmsdata_user->hyhg = null;
                    $lmsdata_user->persg = null;
                    $lmsdata_user->psosok = $psosok;
                    $lmsdata_user->sex = '';
                    $lmsdata_user->phone1 = $phone;
                    $lmsdata_user->rank = $rank;
                    $lmsdata_user->position = $position;
                    $lmsdata_user->entrydate = strtotime($startdate . ' 00:00:00');

                    if (!$DB->record_exists('user', array('username' => $username))) {
                        $user->password = hash_internal_user_password($email, TRUE);
                        //이메일 주소가 동일하면 사용자를 생성하지 않음
                        if ($DB->record_exists('user', array('email' => $email))) {
                            siteadmin_println('동일한 이메일 주소가 이미 등록 되어있습니다.(이름 :' . $user->lastname . ', 이메일 : ' . $user->email . ')');
                            continue;
                        }
                        //계정 잠금 기능
                        if (strtolower($suspended) == 'y') {
                            $user->suspended = 1;
                        }
                        $userid = $DB->insert_record('user', $user);
                        $user->id = $userid;

                        if (!get_user_preferences('auth_forcepasswordchange')) {
                            set_user_preference('auth_forcepasswordchange', 1, $user->id);
                        }
                        $lmsdata_user->userid = $userid;
                        $record = new stdClass();
                        $record->contextlevel = CONTEXT_USER;
                        $record->instanceid = $userid;
                        $record->depth = 0;
                        $record->path = null; //not known before insert

                        if (!$id = $DB->get_record('context', array('contextlevel' => CONTEXT_USER, 'instanceid' => $user->id))) {
                            $record->id = $DB->insert_record('context', $record);
                        } else {
                            $record->id = $id->id;
                        }
                        if (!is_null('/' . SYSCONTEXTID)) {
                            $record->path = '/' . $record->id;
                            $record->depth = substr_count($record->path, '/');
                            $DB->update_record('context', $record);
                        }


                        $DB->insert_record('lmsdata_user', $lmsdata_user);

                        siteadmin_println('사용자 추가(이름 :' . $user->lastname . ', E-mail :' . $user->email . ')');
                        $insertCount2++;

                        // 비밀번호 변경 +1년 체크를 위해 추가
                        set_user_preference('auth_manual_passwordupdatetime', time(), $user->id);
                    } else {
                        $user = $DB->get_record('user', array('username' => $username));
                        //이메일 주소가 동일한 사용자가 있으면 업데이트 하지 않음
                        if ($DB->record_exists_sql('SELECT * FROM {user} WHERE id <> :id AND email = :email', array('id' => $user->id, 'email' => $email))) {
//                            siteadmin_println('동일한 이메일 주소가 이미 등록 되어있습니다.(이름 :' . $user->lastname . ', 이메일 : ' . $user->email . ')');
//                            continue;
                        } else {
                            $user->email = $email;
                        }
                        $user->lastname = $lastname;
                        $user->phone1 = $phone;
                        $user->phone2 = $phone;
                        $user->timecreated = time();
                        $user->timemodified = time();
                        $userid = $DB->update_record('user', $user);

                        $lmsdata_user = $DB->get_record('lmsdata_user', array('userid' => $user->id));

                        $lmsdata_user->usergroup = 'pr';
                        $lmsdata_user->usertypecode = 10;
                        $lmsdata_user->phone1 = $phone;
                        $lmsdata_user->entrydate = strtotime($startdate . ' 00:00:00');
                        $lmsdata_user->rank = $rank;
                        $lmsdata_user->position = $position;
                        $lmsdata_user->psosok = $psosok;
                        $luser = $DB->update_record('lmsdata_user', $lmsdata_user);

                        //계정 잠금 기능
                        if (!is_siteadmin($user) && ($USER->id != $user->id) && strtolower($suspended) == 'y') {
                            $user->suspended = 1;
                            // Force logout.
                            \core\session\manager::kill_user_sessions($user->id);
                            user_update_user($user, false);
                        } else if ($user->suspended == 1) {
                            $user->suspended = 0;
                            user_update_user($user, false);
                        }

                        siteadmin_println('사용자 업데이트(이름 :' . $user->lastname . ', E-mail :' . $user->email . ')');
                        $updateCount2++;
                    }
                }
            }
        }
        die;
        ?>

        <?php
        siteadmin_println(' ');
        siteadmin_println($stuInsertCount + $stuUpdateCount . '명의 교육생을 처리했습니다.(신규 : ' . $stuInsertCount . ', 업데이트 : ' . $stuUpdateCount . ')');
        siteadmin_println($mentoInsertCount + $mentoUpdateCount . '명의 멘토를 처리했습니다.(신규 : ' . $mentoInsertCount . ', 업데이트 : ' . $mentoUpdateCount . ')');
        siteadmin_println($insertCount2 + $updateCount2 . '명의 임직원을 처리했습니다.(신규 : ' . $insertCount2 . ', 업데이트 : ' . $updateCount2 . ')');
        ?>

        <div class="mg_tp10">
            <input type="button" class="blue_btn"  value="<?php echo get_string('okay', 'local_lmsdata'); ?>" onclick="location.href = 'info.php'"/>
        </div>
    </div>
</div>
<?php
siteadmin_scroll_down();
purge_all_caches();

include_once ('../inc/footer.php');
?>
