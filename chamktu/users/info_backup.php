<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once("$CFG->libdir/excellib.class.php");

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/users/info.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);
$hyear = optional_param('hyear', '', PARAM_RAW);
$perpage = optional_param('perpage', 20, PARAM_INT);
$excell = optional_param('excell', 0, PARAM_INT);

//데이터 가져오기



$luon = '';
$where = '';
if (!empty($hyear)) {
    if ($hyear == 'p1' || $hyear == 'p2') {
        $hyear = substr($hyear, 1);
        $luon = "and lu.hyear = :hyear and univ = '1'";
    } else {
        $luon = "and lu.hyear = :hyear ";
    }
}
if (!empty($searchtext)) {
    $where = ' and u.username like :searchtxt4  or  u.firstname like :searchtxt1 or u.lastname like :searchtxt2 or CONCAT(u. firstname,u.lastname) like :searchtxt3 ';
}

$sql = "select u.*, lu.*  from {user} u "
        . "join {lmsdata_user} lu on lu.userid = u.id and lu.usergroup = 'rs' " . $luon
        . "where u.suspended != 1 " . $where
        . "order by u.firstname asc";
$params = array('hyear' => $hyear, 'searchtxt1' => '%' . $searchtext . '%', 'searchtxt2' => '%' . $searchtext . '%', 'searchtxt3' => '%' . $searchtext . '%', 'searchtxt4' => '%' . $searchtext . '%');

if (!$excell) {

    $offset = ($currpage - 1) * $perpage;
    $users = $DB->get_records_sql($sql, $params, $offset, $perpage);
    $count_sql = "select count(u.id) from {user} u "
            . "join {lmsdata_user} lu on lu.userid = u.id and lu.usergroup = 'rs' $luon where u.suspended != 1" . $where;

    $total_count = $DB->count_records_sql($count_sql, $params);
    $num = $total_count - $offset;


    $js = array(
        $CFG->wwwroot . '/chamktu/manage/course_list.js'
    );

    include_once (dirname(dirname(__FILE__)) . '/inc/header.php');
    ?>
    <div id="contents">
        <?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_users.php'); ?>

        <div id="content">
            <h3 class="page_title"><?php echo get_string('stu_management', 'local_lmsdata'); ?></h3>
            <div class="page_navbar"><a href="./info.php"><?php echo get_string('user_management', 'local_lmsdata'); ?></a> > <a href="./info.php"><?php echo get_string('stu_management', 'local_lmsdata'); ?></a></div>

            <form name="" id="course_search" class="search_area" action="info.php" method="get">
                <input type="hidden" title="page" name="page" value="1" />
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('search_placeholder_u','local_lmsdata'); ?>"  class="search-text"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search','local_lmsdata'); ?>"/>          
                <input type="button" class="blue_btn" value="<?php echo get_string('excell_down','local_lmsdata'); ?>" onclick="location.href='<?php echo $CFG->wwwroot; ?>/chamktu/users/info.php?excell=1&search=<?php echo $search;?>&searchtext=<?php echo $searchtext; ?>&usergroup=<?php echo $hyear; ?>'">
            </form><!--Search Area2 End-->

            <table>
                <caption class="hidden-caption">학생관리</caption>
                <thead>
                <tr>
                    <th scope="row" width="5%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%"><?php echo get_string('user_studentid', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%"><?php echo get_string('name','local_lmsdata'); ?></th>
                    <th scope="row"><?php echo get_string('major', 'local_lmsdata'); ?></th>
                    <th scope="row"><?php echo get_string('email', 'local_lmsdata'); ?></th>
                    <th scope="row"><?php echo get_string('contact', 'local_lmsdata'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $lmsdata = $DB->get_record('lmsdata_user',array('userid'=>$user->id));
                foreach ($users as $user) {
                    
                   
                    ?>
                    <tr>
                        <td width="5%"><?php echo $num; ?></td>
                        <td width="10%"><?php echo $user->username; ?></td>
                        <td width="10%"><a target="_blank" href="/user/profile.php?id=<?php echo $user->id; ?>"><?php echo fullname($user); ?></a></td>
                        <td><?php echo $user->major; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo $user->phone2; ?></td>
                    </tr>
        <?php $num--;
    } if ($total_count <= 0) {
        ?>
                    <tr>
                        <td colspan="9">등록된 학생이 없습니다.</td>
                    </tr>
            <?php } ?>
                </tbody>
            </table><!--Table End-->

    <?php
    print_paging_navbar_script($total_count, $currpage, $perpage, 'javascript:cata_page(:page);');
    ?>


        </div><!--Content End-->

    </div> <!--Contents End-->

    <?php
    include_once ('../inc/footer.php');
} else {
    $users = $DB->get_records_sql($sql, $params);
    $num = count($users);

    $fields = array(
        get_string('number', 'local_lmsdata'),
        get_string('user_studentid', 'local_lmsdata'),
        get_string('name','local_lmsdata'),
        get_string('email', 'local_lmsdata'),
        get_string('contact', 'local_lmsdata'),
    );

    $filename = '사용자_학생('.date('Ymd').').xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($users as $user) {
        $col = 0;
        $worksheet[0]->write($row, $col++, $num--);
        $worksheet[0]->write($row, $col++, $user->username);
        $worksheet[0]->write($row, $col++, fullname($user));
        $worksheet[0]->write($row, $col++, $user->email);
        $worksheet[0]->write($row, $col++, $user->phone2);
        $row++;
    }
    
    $workbook->close();
    die;
}
?>
