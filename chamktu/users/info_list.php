<?php
require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';
$PAGE->set_context($context);

//강좌 아이디
$userid = optional_param('userid', 0, PARAM_INT);
$year = optional_param('year', date("Y"), PARAM_INT);

$sql_select = ' SELECT 
                    ra.id, 
                    lc.classyear, lc.classnum, lc.learningstart, lc.learningend, 
                    lco.coursename, lco.classtype, lco.classobject, lco.roughprocess, lco.detailprocess,lco.courseid coid  ';
$sql_from   = ' FROM {context} ctx 
                JOIN {lmsdata_class} lc ON lc.courseid = ctx.instanceid 
                JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid 
                JOIN {role_assignments} ra ON ra.contextid = ctx.id 
                LEFT JOIN {lmsdata_course_applications} lca ON lca.courseid = lc.courseid AND lca.userid = ra.userid 
                JOIN {role} ro ON ro.id = ra.roleid AND ro.archetype = :archetype 
                JOIN {user} ue ON ue.id = ra.userid AND ue.id = :userid ';

$sql_where = ' WHERE ctx.contextlevel = :contextlevel ';

$params = array('archetype' => 'student', 'userid' => $userid, 'contextlevel' => CONTEXT_COURSE);

$where = array();

if ($year != 0) {
    $where[] = ' lc.classyear = :year ';
    $params['year'] = $year;
}

if (!empty($where)) {
    $sql_where .= ' AND ' . implode(' AND ', $where);
}

$orderBy = ' ORDER BY lc.learningstart DESC, lc.learningend DESC ';

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderBy, $params);
$count = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);

$returnurl = $CFG->wwwroot . '/siteadmin/users/info_list.php?id=' . $userid . '&year=' . $year . '&term=' . $term;

$class_gubuns = array(
    1 => '임직원',
    2 => '멘토',
    3 => '교육생',
    4 => '임직원, 멘토',
    5 => '멘토, 교육생',
    6 => '임직원, 교육생',
    7 => '전체'
);
?>

<form id="course_search" class="search_area" style="margin-top: 10px;" onsubmit="info_list_popup_reload('<?php echo $userid; ?>'); return false;">
    <?php $year_arr = lmsdata_get_years(); ?>
    <select title="year" name="year" class="w_160">
        <option value="0"  <?php echo $year == 0 ? 'selected' : '' ?>><?php echo get_string('all', 'local_lmsdata'); ?></option>
        <?php
        foreach ($year_arr as $tg_year) {
            $selected = "";
            if ($tg_year == $year) {
                $selected = "selected";
            }
            echo '<option value="' . $tg_year . '"  ' . $selected . '>' . get_string('year', 'local_lmsdata', $tg_year) . '</option>';
        }
        ?>
    </select>
    <input type="submit" value="검색" class="search_btn" />
</form>

<table>
    <thead>
        <tr>
            <th scope="row" width="8%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
            <th scope="row">과정명</th>
            <th scope="row" width="8%">기수</th>
            <th scope="row" width="30%">학습기간</th>
            <th scope="row" width="8%">이수</th>
        </tr>
    </thead>
    <?php if (empty($courses)) { ?>
        <tr>
            <td colspan="5">수강중인 강좌가 없습니다.</td>
        </tr>
    <?php
    } else {
        $startnum = $count - $offset;
        foreach ($courses as $course) {
            $getcompletesql = " SELECT count(*)
                                from {course_completions}  
                                where  timecompleted is not null  and userid = :userid  and course = :courseid ;";
            $getcomplete = $DB->count_records_sql($getcompletesql, array('courseid' => $course->coid, 'userid' =>$userid));
            $completiontext = $getcomplete == 0 ? '미이수' : '이수';
            echo '<tr>';
            echo '<td>' . $startnum-- . '</td>';
            echo '<td>' . $course->coursename . '</td>';
            echo '<td>' . $course->classnum . '기</td>';
            echo '<td>' . date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend) . '</td>';
            echo '<td>'.  $completiontext.'</td>';
            echo '</tr>';
        }
    }
    ?>    
</table><!--Table End-->


<script type="text/javascript">
    function info_list_popup_reload(userid) {
        var tag = $("#course_list");
        var form = $("#course_search");

        var year = $("select[name=year]").val();

        $.ajax({
            url: '/chamktu/users/info_list.php',
            method: 'POST',
            data: {
                userid: userid,
                year: year
            },
            success: function (data) {
                tag.html(data);
            }
        });
    }
</script>