<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/manage/course_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

?>

<?php include_once (dirname(dirname(__FILE__)) . '/inc/header.php'); ?>
<div id="contents">
<?php include_once (dirname(dirname(__FILE__)) . '/inc/sidebar_users.php'); ?>
    <form name="sync_form" id="frm_user_excel" method="post" action="sync_user_submit.php" enctype="multipart/form-data">   
        <div id="content">
            <h3 class="page_title">엑셀 사용자 등록</h3>
            <div class="page_navbar">
                <a href="./info.php">회원관리</a> >
                <strong>엑셀 사용자 등록</strong>
            </div>
            <div style="clear:both;">
                <input type="file" name="user" /> 
                <br>
                <p><strong><a style="color:red;" href="user_sample_v4.xlsx" alt="Sample" alt="Sample">[Sample]</a></strong>양식에 맞춰서 사용자를 추가해주세요.</p>
                <br>
                <input type="button" class="gray_btn" value="업데이트" onclick="sync_user_excel()"/>
            </div>
        </div><!--Content End-->
    </form>
</div> <!--Contents End-->

<script type="text/javascript">
    function sync_user_excel() {
        if($.trim($("input[name='user']").val()) != '') {
             var filename = $.trim($("input[name='user']").val());
             var extension = filename.replace(/^.*\./, '');
             if(extension == filename) {
                 extension = "";
             } else {
                 extension = extension.toLowerCase();
             }
             
             if($.inArray( extension, [ "xls", "xlsx" ] ) == -1) {
                 alert("<?php echo get_string('onlyexcell','local_lmsdata'); ?>");
                 return false;
             } else {
                $("#frm_user_excel").submit();
             }
        }
    }
</script>   
<?php include_once ('../inc/footer.php'); ?>

