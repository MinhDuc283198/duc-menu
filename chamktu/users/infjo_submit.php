<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/chamktu/lib.php');
    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('/chamktu/user/infprof_submit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$id = optional_param("id", 0, PARAM_INT);

$usernew = new stdClass();
$usernew->auth = 'manual';
$usernew->timemodified = time();
$usernew->lang = "ko";
$usernew->mnethostid = 1; // Always local user.
$usernew->confirmed  = 1;
$usernew->mailedisplay = 0;

$usergroup = optional_param('usergroup', 'rs', PARAM_RAW);
$usernew->email = trim(optional_param('email', ' ', PARAM_RAW));
$usernew->firstname ='&nbsp;';
$usernew->lastname = trim(optional_param('lastname', ' ', PARAM_RAW));
$usernew->phone2 = optional_param('phone', ' ', PARAM_RAW);
$usernew->suspended = optional_param('suspended', ' ', PARAM_INT);

$usernew->timecreated = time();

if (!empty($id)) {
    $user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST);
    $PAGE->set_context(context_user::instance($user->id));
    
    $usercontext = context_user::instance($user->id);
    
    $authplugin = get_auth_plugin($usernew->auth);
    $usernew->id = $id; 
    
    $password_change = optional_param('pw_edit',false, PARAM_BOOL);
    if($password_change){
        $password = optional_param('password',false, PARAM_RAW);
        $usernew->password = hash_internal_user_password($password);
    
    } 
    user_update_user($usernew, false, false);
    
    $lmsuser = new stdClass();
    $lmsuser = $DB->get_record_sql('select * from {lmsdata_user} where userid = :userid ', array('userid'=>$id));    
        
    
    $lmsuser->userid = $usernew->id;
    $lmsuser->eng_name = $usernew->firstname;
    $lmsuser->usergroup = $usergroup;
    $lmsuser->usertypecode = 10;
    
    $DB->update_record('lmsdata_user', $lmsuser);
    
    if (!$authplugin->is_internal() and $authplugin->can_change_password() and !empty($usernew->password)) {
        if (!$authplugin->user_update_password($usernew, $usernew->password)) {
            // Do not stop here, we need to finish user creation.
            debugging(get_string('cannotupdatepasswordonextauth', '', '', $usernew->auth), DEBUG_NONE);
        }
    }
    
    // Load user preferences.
    useredit_load_preferences($user);

    // Load custom profile fields data.
    profile_load_data($user);

    // Update mail bounces.
    useredit_update_bounces($user, $usernew);

    // Update forum track preference.
    useredit_update_trackforums($user, $usernew);
   
} else {
 
    $username = optional_param('username', false, PARAM_RAW);
    $usernew->username = trim(core_text::strtolower($username));   
    
    $password = trim(optional_param('password', ' ', PARAM_RAW));
    if(!empty($password)) {
        $usernew->password = hash_internal_user_password($usernew->password);
    }
    $authplugin = get_auth_plugin($usernew->auth);
    $usernew->id = user_create_user($usernew, false, false);

    $lmsuser = new stdClass();
    $lmsuser->userid = $usernew->id;
    $lmsuser->eng_name = $usernew->firstname;
    $lmsuser->usergroup = $usergroup;
    $lmsuser->usertypecode = 10;     //10 : 영재원 , 20 : 영재센터, 30 : 일반인
    $lmsuser->phone1 = $usernew->phone2 ;    
    
    $DB->insert_record('lmsdata_user', $lmsuser);

    if (!$authplugin->is_internal() and $authplugin->can_change_password() and !empty($usernew->password)) {
        if (!$authplugin->user_update_password($usernew, $usernew->password)) {
            // Do not stop here, we need to finish user creation.
            debugging(get_string('cannotupdatepasswordonextauth', '', '', $usernew->auth), DEBUG_NONE);
        }
    }

    $usercontext = context_user::instance($usernew->id);

    // Update preferences.
    useredit_update_user_preference($usernew);

    // Save custom profile fields data.
    profile_save_data($usernew);
    
    // Reload from db.
    $usernew = $DB->get_record('user', array('id' => $usernew->id));

    // Trigger update/create event, after all fields are stored.
    \core\event\user_created::create_from_userid($usernew->id)->trigger();
    
}

echo '<script type="text/javascript">';
echo 'alert("등록이 완료되었습니다.");';
echo 'document.location.href="./info.php";';
echo '</script>';