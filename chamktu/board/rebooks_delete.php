<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>
        <?php
//추천도서 삭제 페이지

        require_once('../../config.php');
        require_once($CFG->libdir . '/filestorage/stored_file.php');
        require_once dirname(dirname(dirname(__FILE__))) . '/chamktu/lib.php';

        foreach ($_POST as $i => $value) {
            $getcontext_sql = 'select con.id conid, f.id fid, lr.id lrid '
                    . 'from {context} con '
                    . 'join {files} f on con.id = f.contextid '
                    . 'join {lmsdata_rebooks} lr on lr.id = f.itemid '
                    . 'where con.contextlevel = 10 and lr.id = :lrid and filename != "." ';
            $params_context = array('lrid' => $value);

            $getcontext = $DB->get_record_sql($getcontext_sql, $params_context);
            $fs = get_file_storage();
            $overlap_files = $fs->get_area_files($getcontext->conid, 'local_lmsdata', 'attachment', $getcontext->lrid, 'id');
            foreach ($overlap_files as $file) {
                $filename = $file->get_filename();
                $file->delete();
            }
            $DB->delete_records("lmsdata_rebooks", array("id" => $value));
        }

        ?>
        <script type="text/javascript">location.href = "<?php echo $CFG->wwwroot ?>/chamktu/board/rebooks_list.php?top_menu=1"</script>