<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
                    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/board/board_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
include_once('../inc/header.php');
?>
<div id="contents">
    <?php include_once('../inc/sidebar_board.php'); ?>
    <div id="content">
        <h3 class="page_title">추천도서관리</h3>
        <div class="page_navbar"> <a href="./notices.php">자료실/게시판</a> > 추천도서관리</div>
  
        <form action="rebooks_submit.php">
        <table>
            <tr>
                <td  style="background-color: #F2F2F2">도서명</td><td style="text-align:left;" colspan="3"><input type="text" maxlength="255"  style="width:70%;"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">저자명</td><td style="text-align:left;"><input type="text" maxlength="100" style="width:50%;" required="required" ></td> <td>ISBN</td><td style="text-align:left;"><input type="text"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">출판사</td><td style="text-align:left;"><input type="text" style="width:70%;" required="required"></td> <td>출판일</td><td style="text-align:left;"><input type="text"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">페이지</td><td style="text-align:left;"><input type="number" style="width:10%;"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">판매가</td><td style="text-align:left;"><input type="number" style="width:15%;" required="required"></td> <td>상태</td><td style="text-align:left;"><input type="radio" name="status" value="switch" checked="checked">출력 &nbsp; <input type="radio" name="status" value="switch">중지</td>
            </tr>
              <tr>
                    <td class="field_title" style="background-color: #F2F2F2">첨부파일</td>
                    <td class="field_value number" colspan="3">                                
                        <?php
                        if (count($files) > 0) {
                            $output = '';
                            $cnt = 0;
                            foreach ($files as $file) {
                                $filename = $file->get_filename();
                                $mimetype = $file->get_mimetype();
                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_jinotechboard/attachment/' . $id . '/' . $filename);
                                if ($file->get_filesize() > 0) {
                                    $fileobj[$cnt] = '<input type="hidden" name="file_del[' . $cnt . ']" value="0"/>';
                                    $fileobj[$cnt] .= "<div style='float:left;' id ='file" . $cnt . "'><a href=\"$path\">$iconimage</a>";
                                    $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '<a type="button" style="color:red;" href="#" onclick="remove_file(' . $cnt . ');">[삭제]</a></div>';
                                    $cnt++;
                                }
                            }
                        }
                            for ($n = 0; $n <= 0; $n++) {
                            ?>
                            <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile[<?php echo $n; ?>]" id="uploadfile<?php echo $n; ?>" style=" float:left;"/> 
                                <input type="button" value=삭제 onclick="fileReset('<?php echo $n; ?>');">
                                <?php
                                if (isset($fileobj[$n])) {
                                    echo $fileobj[$n];
                                }
                                echo "</div>";
                            }
                            ?>
                            <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                    </td>
                </tr>
            <tr>
                <td style="background-color: #F2F2F2">추천 이유 (20자 이내)</td><td colspan="3" style="text-align:left;"><input type="text" style="width:90%;" maxlength="20"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">인터파크</td><td colspan="3" style="text-align:left;"><input type="text" style="width:90%;"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">예스24</td><td colspan="3" style="text-align:left;"><input type="text" style="width:90%;"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">알라딘</td><td colspan="3" style="text-align:left;"><input type="text" style="width:90%;"></td>
            </tr>
            <tr>
                <td style="background-color: #F2F2F2">교보문고</td><td colspan="3" style="text-align:left;"><input type="text" style="width:90%;"></td>
            </tr>
        </table>
        <div clas="btn_area">
            <input type="button" id="notice_list" class="blue_btn" value="목록" style="float: left; border: 1px solid #999" />

            <input type="submit" id="add_user" class="blue_btn" value="저장" style="float: right; margin: 0 10px 0 0" />

        </div> <!-- Bottom Button Area -->
            
    </form>
        
        <script>
             function fileReset(n) {
            $("#uploadfile" + n).val("");

            //var attachFile = $('#uploadfile0');
            //$(attachFile).replaceWith($(attachFile).clone(true));

        }
            $(document).ready(function() {
        $( "#timestart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#timeend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#timeend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#timestart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $( "#timeregstart" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#timeregend" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#timeregend" ).datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function( selectedDate ) {
                $( "#timeregstart" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

    }
            
        </script>