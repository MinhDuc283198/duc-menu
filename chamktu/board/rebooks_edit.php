<?php
//추천도서관리 수정 페이지
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/board/board_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
include_once('../inc/header.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$rebook = lmsdata_rebooks_get_rebooks_content($id);

$temp->itemid = $rebook->id;

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'attachment', $id, 'timemodified', false);
?>

<script>
    $(document).ready(function () {
        $("#publication_date").datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function (selectedDate) {
                $("#timedue").datepicker("option", "minDate", selectedDate);
            }
        });
    });
</script>
<div id="contents">
    <?php include_once('../inc/sidebar_board.php'); ?>
    <div id="content">
        <h3 class="page_title">추천도서 수정</h3>
        <div class="page_navbar"> <a href="./board_list.php"><?php echo get_string('board_management', 'local_lmsdata');?></a> >
                                  <a href="./rebooks_list.php"><?php echo get_string('recommend_book', 'local_lmsdata');?></a> >
                                  <strong>추천도서 수정</strong></div>

        <form action="rebooks_submit.php" enctype="multipart/form-data" method="POST">
            <table class="detail">
                <colgroup>
                    <col width="200px" />
                    <col width="/" />
                </colgroup>
                <tr>
                    <th>도서명</th>
                    <td class="left" colspan="3">
                        <input type="text" name="name" maxlength="255" value="<?php echo $rebook->name ?>" class="w_100" required="required">
                    </td>
                </tr>
                <tr>
                    <th>저자명</th>
                    <td colspan="3" class="left">
                        <input type="text" maxlength="255" name="author" class="w_100" value="<?php echo $rebook->author ?>" required="required" >
                    </td>
                </tr>
                <tr class="none">
                    <td class>ISBN</td><td class="left"><input type="hidden" name="isbn" value="<?php echo $rebook->isbn ?>"></td>
                </tr>
                <tr>
                    <th>출판사</th>
                    <td class="left">
                        <input type="text" class="w_100" name="publisher" required="required" value="<?php echo $rebook->publisher ?>">
                    </td> 
                    <th>출판일</th>
                    <td class="field_value">
                        <input type="text" id="publication_date" title="time" name="publication_date" value="<?php echo $rebook->publication_date; ?>"/>
                    </td>
                </tr>
                <tr class="none">
                    <th>페이지</th>
                    <td class="left">
                        <input type="hidden" name="totalpage" value="<?php echo $rebook->totalpage ?>">
                    </td>
                </tr>
                <tr>
                    <th>판매가</td>
                    <td class="left">
                        <input type="number"  name="price" required="required" value="<?php echo $rebook->price ?>">(원)
                    </td> 
                    <th>게시 여부</th>
                    <td class="left">
                        <?php
                        if ($rebook->status == 1) {
                            echo '<input type="radio" name="status" value="1" checked="checked">게시 &nbsp; <input type="radio" name="status" value="0">중지</td>';
                        } else {
                            echo ' <input type="radio" name="status" value="1">게시 &nbsp; <input type="radio" name="status" value="0"  checked="checked">중지</td>';
                        }
                        ?>


                </tr>
                <tr>
                    <th class="field_title">첨부파일</th>
                    <td class="field_value number" colspan="3">                                
                        <?php
                        if ($temp->itemid) {
                            $output = '';
                            $cnt = 0;
                            foreach ($files as $file) {
                                $filename = $file->get_filename();
                                $mimetype = $file->get_mimetype();
                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $id . '/' . $filename);
                                if ($file->get_filesize() > 0) {
                                    $fileobj[$cnt] = '<input type="hidden" name="file_del" value="0"/>';
                                    $fileobj[$cnt] .= "<span id ='file'><a href=\"$path\">$iconimage</a>";
                                    $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</span> <br /> <img src="' . $path . '">';
                                    $cnt++;
                                }
                            }
                        }
                        for ($n = 0; $n <= 0; $n++) {
                            ?>
                            <input type="file" name="uploadfile" id="uploadfile"/> 

                            <?php
                            if (isset($fileobj[$n])) {
                                echo $fileobj[$n];
                            }
                            echo "</div>";
                        }
                        ?>
                        <input type="hidden" class="" name="file_id" value="<?php
                        if ($temp->itemid > 0) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                        ?>"/>

                    </td>
                </tr>
                <tr>
                    <th>추천 이유 (150자 이내)</th>
                    <td colspan="3" class="left">
                        <input type="text" name="recom" value="<?php echo $rebook->recom ?>" class="w_100" maxlength="150">
                    </td>
                </tr>
                <tr>
                    <th>인터파크</th>
                    <td colspan="3" class="left">
                        <input type="text" name="link1" value="<?php echo $rebook->link1 ?>" class="w_100">
                    </td>
                </tr>
                <tr>
                    <th>
                        예스24
                    </th>
                    <td colspan="3" class="left">
                        <input type="text" name="link2" value="<?php echo $rebook->link2 ?>" class="w_100">
                    </td>
                </tr>
                <tr>
                    <th>알라딘</th>
                    <td colspan="3" class="left">
                        <input type="text" name="link3" value="<?php echo $rebook->link3 ?>" class="w_100">
                    </td>
                </tr>
                <tr>
                    <th>교보문고</th>
                    <td colspan="3" class="left">
                        <input type="text" name="link4" value="<?php echo $rebook->link4 ?>" class="w_100">
                    </td>
                </tr>

                <input type="hidden" name="mod" value="<?php echo $mod ?>">
                <input type="hidden" name="id" value="<?php echo $id ?>">
    </tbody>
</table>
            <div class="btn_area">
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('board_save', 'local_lmsdata'); ?>" />
                <input type="button" id="notice_list" class="normal_btn" value="<?php echo get_string('board_list', 'local_lmsdata'); ?>" onclick="location.href='rebooks_list.php'"/>
            </div> <!-- Bottom Button Area -->
        </form>
    </div>
</div>
<?php include_once('../inc/footer.php');?>
