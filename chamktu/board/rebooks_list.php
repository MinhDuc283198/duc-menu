<?php

//추천도서 리스트 페이지
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
                    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/board/board_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
include_once('../inc/header.php');
//$rebooks = $DB->get_records('lmsdata_rebooks');



$currpage = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$id = optional_param('id', 0, PARAM_INT);
$searchvalue = optional_param('searchvalue',"",PARAM_TEXT);
$totalcount = lmsdata_rebooks_get_rebooks_count($searchvalue);
$rebooks = lmsdata_rebooks_get_rebooks_paging($currpage-1,$perpage,$searchvalue);

?>
<style>
.small { width: 80px; }
</style>
<div id="contents">
    <?php include_once('../inc/sidebar_board.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php echo get_string('recommend_book', 'local_lmsdata');?></h3>
        <div class="page_navbar"> <a href="./board_list.php"><?php echo get_string('board_management', 'local_lmsdata');?></a> > <strong><?php echo get_string('recommend_book', 'local_lmsdata');?></strong></div>
        <form id="frm_notices_search" class="search_area">
            <input type="text" title="serch" name="searchvalue" value="<?php echo $searchvalue; ?>" class="search-text" placeholder="도서명을 입력하세요">
            <input type="submit" class="search_btn" id="search" value="<?php echo get_string('search', 'local_jinoboard'); ?>">
        </form>
        <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/chamktu/board/rebooks_delete.php'>
            <table cellpadding="0" cellspacing="0" class="normal" width="100%">
                <caption class="hidden-caption">팝업관리</caption>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                        <th scope="row" width="5%">No.</th>
                        <th scope="row" width="10%">이미지</th>
                        <th scope="row" width="35%">도서명</th>
                        <th scope="row" width="10%">저자</th>
                        <th scope="row" width="15%">출판사</th>
                        <th scope="row" width="10%">판매가</th>
                        <th scope="row" width="10%"><?php echo get_string('post_visible','local_lmsdata'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $now = date("Y-m-d");
                    $offset = 0;
                    if ($currpage != 0) {
                        $offset = ($currpage - 1) * $perpage;
                    }
                    $num = $totalcount - $offset;
                    if($totalcount){
                    foreach ($rebooks as $book) {
                            $fs = get_file_storage();
                            $files = $fs->get_area_files($context->id, 'local_lmsdata', 'attachment', $book->itemid, 'timemodified', false);      
                        ?>
                        <tr>
                            <td class="chkbox"><input type="checkbox" title="check"  name="bookid<?php echo $book->id; ?>" value="<?php echo $book->id; ?>" /></td>
                            <td><?php echo $num; ?></td>                            
                                                    <?php
                        if ($book->itemid) {
                            $output = '';
                            foreach ($files as $file) {
                                $filename = $file->get_filename();
                                $mimetype = $file->get_mimetype();
                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';                                
                                
                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $book->itemid . '/' . $filename);
                                if ($file->get_filesize() > 0) {
                                    $fileobj = '<img class=\'small\' src="'.$path.'">';

                                }
                            }
                        }
                            ?>
                            
                            <td>
                                <?php 
                                if($book->itemid){
                                    echo $fileobj;
                                }
                                ?></td>
                            <td class="title"><a href='<?php echo $CFG->wwwroot ?>/chamktu/board/rebooks_edit.php?mod=edit&id=<?php echo $book->id ?>'><?php echo $book->name ?></a></td>
                            <td><?php echo $book-> author?></td> 
                            <td><?php echo $book -> publisher ?></td>
                            <td><?php echo number_format($book -> price); ?></td>
                            <td>
                                <?php 
                                if ($book->status==1){
                                    echo '게시';
                                } else{
                                    echo '중지';
                                }
                                 ?>
                            </td>
                        </tr>
                        <?php $num--;
                         } 
                    }else if (!$totalcount) { ?>
                        <tr><td colspan="8"><span>등록된 추천 도서가 없습니다.</span></td></tr>
                     <?php } ?>
                </tbody>
            </table>
        
        <div id="btn_area">
                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/chamktu/board/rebooks_write.php?mod=add"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" />
            </div>
            <?php 
             $page_params['searchvalue'] = $searchvalue;
            print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . '/chamktu/board/rebooks_list.php', $page_params);?>
        </form>
    </div>
</div>
<?php include_once('../inc/footer.php');?>

            
            
           
            
            
            <script>
                $(function() {
        $("#allcheck").click(function() {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function() {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function() {
                    this.checked = false;
                });
            }
        });
        //리스트 삭제기능
        $("#delete_button").click(function() {
            if(confirm("<?php echo get_string('delete_confirm', 'local_lmsdata');?>")){
                $('#form').submit();
            }
            
        });
    });
    
                </script>