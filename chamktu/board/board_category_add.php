<?php
    require(dirname(dirname(dirname(__FILE__))) . '/config.php');
    require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
    
    $maketime = time();
    $id = optional_param("id", 0, PARAM_INT);
    $usertype = implode( '/' ,$_POST['chkbox']);
    
    print_object($_REQUEST);
    
    $data = new stdClass();
    $data->userid = $USER->id;
    $data->board = $id;
    $data->name = $_POST['name'];
    $data->engname = $_POST['engname'];
    $data->isused = $_POST['isused'];
    $data->sortorder = $_POST['sortorder'];
    $data->timemodified = time();
    $data->usertype = $usertype;

    $DB->insert_record('jinoboard_category',$data);
    redirect('./board_category.php?id='.$id);