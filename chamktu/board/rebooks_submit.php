<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
//추천도서 등록 submit 페이지
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
require_once($CFG->libdir . '/filestorage/file_storage.php');
require_once($CFG->libdir . '/filestorage/stored_file.php');

if (!isloggedin()) {
    echo '<script type="text/javascript">
            alert("로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.");
            document.location.href="' . $SITECFG->wwwroot . '/main/user/login.php";
        </script>';
}

$id = optional_param('id', 0, PARAM_INT);
$name = required_param('name', PARAM_TEXT);
$mod = optional_param('mod', "", PARAM_TEXT);
$author = required_param('author', PARAM_TEXT);
$publisher = required_param('publisher', PARAM_TEXT);
$publication_date = optional_param('publication_date', 0, PARAM_TEXT);
$totalpage = optional_param('totalpage', 0, PARAM_TEXT);
$price = required_param('price', PARAM_TEXT);
$status = optional_param('status', 1, PARAM_INT);
$recom = optional_param('recom',"",PARAM_TEXT);
$link1 = optional_param('link1',"",PARAM_TEXT);
$link2 = optional_param('link2',"",PARAM_TEXT);
$link3 = optional_param('link3',"",PARAM_TEXT);
$link4 = optional_param('link4',"",PARAM_TEXT);
$isbn = optional_param('isbn',"",PARAM_TEXT);
$file_id = optional_param('file_id', 0, PARAM_INT);
$file_del = optional_param('file_del', 0, PARAM_INT);



$newdata = new object();

$newdata->id = $id;
$newdata->name = $name;
$newdata->author = $author;
$newdata->publisher = $publisher;
$newdata->publication_date = $publication_date;
$newdata->totalpage = $totalpage;
$newdata->price = $price;
$newdata->status = $status;
$newdata->recom =$recom;
$newdata->link1 =$link1;
$newdata->link2 =$link2;
$newdata->link3 =$link3;
$newdata->link4 =$link4;
$newdata->isbn = $isbn;


$context = context_system::instance();
    if ($mod == "edit") {

    $newdata->id = $id;
    $DB->update_record('lmsdata_rebooks', $newdata);

    $itemid = $id;
} else if ($mod == "add") {

   $temp = new stdClass();
   $temp= $DB->insert_record('lmsdata_rebooks', $newdata);


    $id = $temp;

}

if ($file_id != 0) {

    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($mod == 'edit' && !($_FILES['uploadfile']['name']==null || $_FILES['uploadfile']['name']=='')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'attachment', $id, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }

        if (!empty($_FILES['uploadfile']['tmp_name'])) {

            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_lmsdata',
                'filearea' => 'attachment',
                'itemid' => $id,
                'filepath' => '/',
                'filename' => $_FILES['uploadfile']['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
            );
            $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile']['tmp_name']);
        }
}

redirect($SITECFG->wwwroot . '/chamktu/board/rebooks_list.php');
?>