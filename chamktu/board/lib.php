<?php

//추천도서 메인 리스트 호출 함수
function lmsdata_rebooks_show_main_rebooks($page,$perpage) {
    global $DB;    
        
            $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    
    $sql="select * from {lmsdata_rebooks} lr
          left join (SELECT itemid FROM {files} WHERE component = 'local_lmsdata' AND filesize >0) f 
          ON lr.id = f.itemid
          where status = 1
          order by id desc";

    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}
//추천도서 카운트 함수
function lmsdata_rebooks_main_rebooks_count() {
    global $DB;    
    
    $where='status=1';
    $sql=$where;
    return $DB->count_records_select("lmsdata_rebooks", $sql, $params);
}

//추천도서 카운트 함수
function lmsdata_rebooks_get_rebooks_count( $searchvalue) {
    global $DB;    
    $params = array();

        
        if($searchvalue) {
      //      $where = " and ";
                    $where = $DB->sql_like('name', ':searchvalue');
                    $params['searchvalue'] = '%'.$searchvalue.'%';

            }
        $sql = $where;
    return $DB->count_records_select("lmsdata_rebooks", $sql, $params);
}


//추천도서 리스트 호출 함수
function lmsdata_rebooks_get_rebooks_content($id){
    global $DB;
    
    return $DB->get_record('lmsdata_rebooks',array('id'=>$id));
    
}
//추천도서 리스트 호출 함수
function lmsdata_rebooks_get_rebooks(){
    global $DB;
    
    $sql='select * from {lmsdata_rebooks} order by id desc';
    
    return $DB->get_records_sql($sql);
}
//추천도서 페이징 기능
function lmsdata_rebooks_get_rebooks_paging($page,$perpage,$searchvalue){
    global $DB;
    
    
            $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }


    $sql="select * from {lmsdata_rebooks} lr
          left join (SELECT itemid FROM {files} WHERE component = 'local_lmsdata' AND filesize >0) f 
          ON lr.id = f.itemid
          where name like '%".$searchvalue."%'
          order by id desc";

return $DB->get_records_sql($sql, $params, $offset, $perpage);

}

