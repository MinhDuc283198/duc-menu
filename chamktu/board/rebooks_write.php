<?php
//추천도서 등록 페이지
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
                    
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/board/board_list.php');
    redirect(get_login_url());
}

$js = array(
    '../js/ckeditor-4.3/ckeditor.js',
    '../js/ckfinder-2.4/ckfinder.js'
);

$context = context_system::instance();
require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
include_once('../inc/header.php');

$mod = optional_param('mod', "", PARAM_TEXT);

$temp->itemid = $rebook->id;

    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'attachment', $id, 'timemodified', false);
    
?>
<style>
    .none{display:none;}
</style>
<div id="contents">
    <?php include_once('../inc/sidebar_board.php'); ?>
    <div id="content">
        <h3 class="page_title">추천도서 작성</h3>        
        <div class="page_navbar"> <a href="./board_list.php"><?php echo get_string('board_management', 'local_lmsdata');?></a> >
                                  <a href="./rebooks_list.php"><?php echo get_string('recommend_book', 'local_lmsdata');?></a> >
                                  <strong>추천도서 작성</strong></div>
        
        <form action="rebooks_submit.php" id="rebooks_submit" enctype="multipart/form-data" method="POST">
            <table>
                <tr>
                    <td  style="background-color: #F2F2F2">도서명 <span class="red">*</span></td>
                    <td style="text-align:left;" colspan="3"><input type="text" name="name" maxlength="255"  style="width:70%;" required="required"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">저자명 <span class="red">*</span></td>
                    <td colspan="3" style="text-align:left;"><input type="text" maxlength="255" name="author" style="width:70%;" required="required" ></td>
                </tr>
                <tr class="none">
                     <td >ISBN</td><td style="text-align:left;"><input type="text" name="isbn"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">출판사 <span class="red">*</span></td>
                    <td style="text-align:left;"><input type="text" style="width:70%;" name="publisher" required="required"></td> 

                    <td>출판일</td><td class="field_value"><input type="text" id="publication_date" title="time" name="publication_date" style="margin-left: 6px; float:left" value="<?php
                                    if (!empty($temp->publication_date)) {
                                        echo date("Y-m-d", $temp->publication_date);
                                    } else {
                                        echo date("Y-m-d", time());
                                    }
                                    ?>"/>
                    </td>
                </tr>
                <tr class="none">
                    <td style="background-color: #F2F2F2">페이지</td><td style="text-align:left;"><input type="text" name="totalpage" style="width:10%;"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">판매가 <span class="red">*</span></td>
                    <td style="text-align:left;"><input type="number" name="price" style="width:15%;" required="required">(원)</td> 
                    <td>게시 여부</td>
                    <td style="text-align:left;"><input type="radio" name="status" value="1" checked="checked">게시 &nbsp; <input type="radio" name="status" value="0">중지</td>
                </tr>
                  <tr>
                        <td class="field_title" style="background-color: #F2F2F2">첨부파일</td>
                        <td class="field_value number" colspan="3">                                
                            <?php
                            if ($temp->itemid) {
                                $output = '';
                                $cnt = 0;
                                foreach ($files as $file) {
                                    $filename = $file->get_filename();
                                    $mimetype = $file->get_mimetype();
                                    $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';                                

                                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $id . '/' . $filename);
                                    if ($file->get_filesize() > 0) {
                                        $fileobj[$cnt] = '<input type="hidden" name="file_del" value="0"/><img src="'.$path.'">';
                                        $fileobj[$cnt] .= "<div style='float:left;' id ='file'><a href=\"$path\">$iconimage</a>";
                                        $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
                                        $cnt++;
                                    }
                                }
                            }
                                for ($n = 0; $n <= 0; $n++) {
                                ?>
                                <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile" id="uploadfile" style=" float:left;"/> 

                                    <?php
                                    if (isset($fileobj[$n])) {
                                        echo $fileobj[$n];
                                    }
                                    echo "</div>";
                                }
                                ?>
                                <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                        </td>
                    </tr>
                <tr>
                    <td style="background-color: #F2F2F2">추천 이유 (150자 이내) </td><td colspan="3" style="text-align:left;"><input type="text" name="recom" value="" style="width:90%;" maxlength="150"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">인터파크</td><td colspan="3" style="text-align:left;"><input type="text" name="link1" value="http://" style="width:90%;"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">예스24</td><td colspan="3" style="text-align:left;"><input type="text" name="link2" value="http://" style="width:90%;"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">알라딘</td><td colspan="3" style="text-align:left;"><input type="text" name="link3" value="http://" style="width:90%;"></td>
                </tr>
                <tr>
                    <td style="background-color: #F2F2F2">교보문고</td><td colspan="3" style="text-align:left;"><input type="text" name="link4" value="http://" style="width:90%;"></td>
                </tr>
                <input type="hidden" name="mod" value="<?php echo $mod?>">              
            </table>
            <div class="btn_area">
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('board_save', 'local_lmsdata'); ?>" />
                <input type="button" id="notice_list" class="normal_btn" value="<?php echo get_string('board_list', 'local_lmsdata'); ?>" onclick="location.href='rebooks_list.php'"/>
            </div> <!-- Bottom Button Area -->
        </form>
    </div>
</div>
<?php include_once('../inc/footer.php');?>

            
<script>
    $("#publication_date").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timedue").datepicker("option", "minDate", selectedDate);
        }
    });
    
    $('#rebooks_submit').submit(function(){
        var ext = $('input[name=uploadfile]').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1 && $('input[name=uploadfile]').val() != "" ) {
            alert('gif,png,jpg,jpeg 파일만 업로드 할수 있습니다.');
            return false;
        }
    });
   
</script>