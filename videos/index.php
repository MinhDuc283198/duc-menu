<?php
$user = $_GET['userid'];   // 접속 유저 아이디 (LMS Primary key)
$returnpath = $_GET['returnpath']; // 데이터를 전달받을 PAGE URL
$path = $_GET['path']; // 업로드될 Directory path
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>FFMPEG <?php echo $USER->id; ?>/</title>
            <!-- production -->
            <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
            <link rel="stylesheet" href="js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />
            <script type="text/javascript" src="js/plupload.full.min.js"></script>
            <script type="text/javascript" src="js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
            <script type="text/javascript" src="js/i18n/ko.js"></script>
            <script type="text/javascript" src="js/moxie.js"></script>
            <script type="text/javascript" src="js/plupload.dev.js"></script>
            <style type="text/css">
                .loading {
                    width:100%;
                    height: 306px;
                    background: white;
                    text-align: center;
                    display: none;
                }
                .loading img {
                    margin-top: 80px;
                    background: white;
                }
            </style>
    </head>
    <body style="width:100%; margin:0; padding:0; font: 13px Verdana; background: #eee; color: #333">
        <div class="loading"><img src="default.gif"></div>
        <div id="uploader">
            <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
        </div>
        <script type="text/javascript">
            $(function () {
                // Setup html5 version
                $("#uploader").plupload({
                    // General settings
                    runtimes: 'html5,flash,silverlight,html4',
                    url: "upload.php?path=<?php echo $path; ?>",
                    chunk_size: '1024mb',
                    rename: false,
                    dragdrop: true,
                    filters: {
                        // Maximum file size
                        max_file_size: '1024mb',
                        // Specify what files to browse for
                        mime_types: [
                            {title: "video files", extensions: "avi,mp4,wmv,mkv,gif"}
                        ]
                    },
                    // Flash settings
                    flash_swf_url: 'js/Moxie.swf',
                    // Silverlight settings
                    silverlight_xap_url: 'js/Moxie.xap',
                    preinit: {
                        Init: function (up, info) {
                        }
                    },
                    // Post init events, bound after the internal events
                    init: {
                        UploadComplete: function (up, files) {
                            $('#uploader').hide();
                            $('.loading').show();
                            // Called when all files are either uploaded or failed
                            $.each(files, function (f) {
                                var filename = files[f]['name'];
                                var path = '<?php echo $path; ?>';
                                var user = '<?php echo $user; ?>';
                                var returnpath = '<?php echo $returnpath; ?>';
                                $.ajax({
                                    method: "POST",
                                    url: "ffmpeg.php",
                                    data: {filename: filename, path: path, userid: user, returnpath: returnpath}
                                })
                                        .done(function (msg) {
                                            parm = {'path':path,'user_id':user,'o_file':filename,'t_file':filename,'duration':msg};
                                            var f = document.createElement('form');
                                            var objs, value;
                                            for (var key in parm) {
                                                 value = parm[key];
                                                 objs = document.createElement('input');
                                                 objs.setAttribute('type', 'hidden');
                                                 objs.setAttribute('name', key);
                                                 objs.setAttribute('value', value);
                                                 f.appendChild(objs);
                                                }
                                                f.setAttribute('method', 'post');
                                                f.setAttribute('action', returnpath);
                                                document.body.appendChild(f);
                                               f.submit();
                                        })
                            });
                        },
                        Error: function (up, args) {
                            // Called when error occurs
                            alert('[Error] ' + args);
                        }
                    }
                });
            });
        </script>
    </body>
</html>
