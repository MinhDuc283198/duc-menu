<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';

global $DB, $USER;

$amode = required_param('amode',PARAM_RAW);
//$lcmsid = required_param('lcmsid',PARAM_INT);
$stime = optional_param('stime','',PARAM_RAW);
$etime = optional_param('etime','',PARAM_RAW);
$lang = optional_param('lang','',PARAM_RAW);
$caption = optional_param('caption','',PARAM_RAW);
$num = optional_param('num',0,PARAM_INT);

$reg = new stdClass();

$reg->userid = $USER->id;
$reg->stime = seconds_from_time($stime);
$reg->etime = seconds_from_time($etime);
$reg->lang = $lang;
$reg->timereg = time();
//$reg->lcmsid = $lcmsid;    
$reg->caption = $caption;
$reg->num = $num;

if($amode == 'history'){
    
    $reg->type = optional_param('type','edit',PARAM_RAW);
    
    $DB->insert_record('lcms_caption_history',$reg);
    
}else if($amode=='police'){
    
    $reg->memo = optional_param('memo','',PARAM_RAW);
    
    $DB->insert_record('lcms_caption_police',$reg);
    
}

$returnvalue = new stdClass();
$returnvalue->status = 'success';

echo json_encode($returnvalue);
?>
