<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';

$id = required_param('id',PARAM_INT);
$qua = optional_param('qua',0,PARAM_INT);

$PAGE->set_url('/mod/lcms/video_player.php', array('id'=>$id));

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('course module is incorrect');
    }
   if($lcms->type=='video'){
        if (!$file = $DB->get_record('lcms_contents_file', array('con_seq' => $lcms->contents))) {
        print_error('lcms contents file is incorrect');
        }
   }else if($contents->type=='embed'){
        if (!$file = $DB->get_record('lcms_contents', array('id' => $lcms->contents))) {
        print_error('lcms contents is incorrect');
        }
   }

} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$PAGE->set_context($context);

$qua_txt = '';
if($qua==1) $qua_txt = '_h';
else if($qua==2) $qua_txt = '_l';

$cid = '123/456/20150331153139_h';
//$cid = $file->filename.$qua_txt; //동영상아이디: 20150210153101_h, 20150210153102_h, 20150210153103_h
$auto_play = 0; //자동재생여부: 1이면 자동재생
$lrn_time = 0; //이어보기여부
if($track = $DB->get_record('lcms_track',array('lcms'=>$lcms->id,'userid'=>$USER->id))){
    $lrn_time = $track->lasttime;
}

$interval_num = '5'; //재생정보를 저장하는 간격
$status_num = ($lcms->progress==1)? '01':'02'; //01:로그쌓기, 02:복습
$img_nm = ''; //이미지경로
$return_url = 'playtime_ajax.php'; //5초마다 되돌릴 url
$param1 = $id; //id값을 넘김
$param2 = '2';
$param3 = '3';

?>

<div id="vodplayer"></div>
<script src="player/flash.js"></script>
<script>
    function video_view(lrn,auto){
        var ff = mf("player/vod_player_lms.swf?cid=<?php echo $cid;?>&auto_play="+auto+"&lrn_time="+lrn+"&interval_num=<?php echo $interval_num;?>&status_num=<?php echo $status_num;?>&img_nm=<?php echo $img_nm;?>&param1=<?php echo $param1;?>&param2=<?php echo $param2;?>&param3=<?php echo $param3;?>&return_url=<?php echo $return_url;?>", "player", "100%", 480, "none");
        setcode(document.getElementById('vodplayer'), ff);
    }
</script>

<?php
if($lrn_time>0){
?>

<script>
    if(confirm('<?php echo get_string('continueplease','lcms');?>')){
        video_view('<?php echo $lrn_time;?>','1');
    }else{
        video_view('0','1');
    }
</script>

<?php
}else{
    echo '<script>video_view("0","0");</script>';
}
?>