<?php

require_once('../../config.php');
 
$id = required_param('id', PARAM_INT);           // Course ID
 
$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$strlcmses = get_string("modulenameplural", "lcms");
$strlcms = get_string("modulename", "lcms");
$strsectionname  = get_string('sectionname', 'format_'.$course->format);
$strname         = get_string('name', 'lcms');
$strintro        = get_string('lcmsintro', 'lcms');
$strlastmodified = get_string('lastmodified');


$PAGE->set_url('/mod/lcms/index.php', array('id' => $course->id));
$PAGE->set_title($course->shortname.': '.$strlcmses);
$PAGE->set_heading($course->fullname);
$PAGE->navbar->add($strlcmses);
echo $OUTPUT->header();

if (!$lcmses = get_all_instances_in_course('lcms', $course)) {
    notice(get_string('thereareno', 'moodle', $strlcmses), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit;
}

$usesections = course_format_uses_sections($course->format);
if ($usesections) {
    $sections = get_all_sections($course->id);
}

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

if ($usesections) {
    $table->head  = array ($strsectionname, $strname, $strintro);
    $table->align = array ('center', 'left', 'left');
} else {
    $table->head  = array ($strlastmodified, $strname, $strintro);
    $table->align = array ('left', 'left', 'left');
}

$modinfo = get_fast_modinfo($course);
$currentsection = '';
foreach ($lcmses as $lcms) {
    $cm = $modinfo->cms[$lcms->coursemodule];
    if ($usesections) {
        $printsection = '';
        if ($lcms->section !== $currentsection) {
            if ($lcms->section) {
                $printsection = get_section_name($course, $sections[$lcms->section]);
            }
            if ($currentsection !== '') {
                $table->data[] = 'hr';
            }
            $currentsection = $lcms->section;
        }
    } else {
        $printsection = '<span class="smallinfo">'.userdate($lcms->timemodified)."</span>";
    }

    $class = $lcms->visible ? '' : 'class="dimmed"'; // hidden modules are dimmed
    $table->data[] = array (
        $printsection,
        "<a $class href=\"view.php?id=$cm->id\">".format_string($lcms->name)."</a>",
        format_module_intro('folder', $lcms, $cm->id));
}

echo $OUTPUT->heading($strlcmses);
echo html_writer::table($table);

echo $OUTPUT->footer();

?>
