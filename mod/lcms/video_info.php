<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';

$vid = required_param('vid',PARAM_RAW);
$qua = optional_param('qua',0,PARAM_INT);

$curl = LCMS.'/viewer/index.php?vi='.$vid.'&qua='.$qua;

if(function_exists('curl_init')){
    
    //curl 리소스 초기화
    $ch = curl_init();
    
    // url을 설정
    curl_setopt($ch, CURLOPT_URL, $curl); 

    // 헤더는 제외하고 content 만 받음
    curl_setopt($ch, CURLOPT_HEADER, 0); 

    // 응답 값을 브라우저에 표시하지 말고 값을 리턴
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    // 브라우저처럼 보이기 위해 user agent 사용
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0'); 

    $content = curl_exec($ch); 

    // 리소스 해제를 위해 세션 연결 닫음
    curl_close($ch);
    
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Headers: X-Requested-With');
    header('Access-Control-Max-Age: 86400');
    header("Content-Type: text/xml; charset=UTF-8");
   
    echo $content;
   
}


?>


