<?php
require_once '../../config.php';
//require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once '../lcmsprogress/locallib.php';

global $DB, $USER;

$returnvalue = new stdClass();

$id = required_param('lcmsid', PARAM_INT);      // cmid
$cm = $DB->get_record('course_modules',array('id'=>$id));
    
if($USER->id && $cm){
    
    $cour = $DB->get_record('lcms',array('id'=>$cm->instance));
    
    if(!$totalpage = $DB->get_field('lcms_contents','con_total_time',array('id'=>$cour->contents))){
        $totalpage = 0;
    }
    
    $lcms = new stdClass();
    $lcms->userid = $USER->id;
    $lcms->lcmsid = $id;
    /*이벤트 종류 (0: 시작, 1:스톱, 2:건너뛰기, 3:재생종료, 4: 재생중[10초에 한번], 5: 페이지 이동 등) */
    $lcms->positionevent  = optional_param('positionevent', 0, PARAM_INT);
    $lcms->positionfrom = optional_param('positionfrom', 0, PARAM_INT);
    $lcms->positionto = optional_param('positionto', 0, PARAM_INT);
    $lcms->positionpage = optional_param('positionpage' , 1, PARAM_INT);
    $lcms->duration = optional_param('duration', 1, PARAM_INT);
    $lcms->device = optional_param('device', '0', PARAM_RAW);
    $lcms->timecreated = time();
    
    $lcmsduration = $DB->get_record('lcms_duration', array('contents'=>$cour->contents,'page'=>$lcms->positionpage));
    
    if($lcms->positionevent == 0 && $lcms->duration > 1){
        if(!$lcmsduration){
            $lcmsduration = new stdClass();
            $lcmsduration->contents = $cour->contents;
            $lcmsduration->page = $lcms->positionpage;
            $lcmsduration->duration = $lcms->duration;
            $DB->insert_record('lcms_duration',$lcmsduration);
        }
        
        $returnvalue->status = 'success';
        
    }else{
        
        if(!$track = $DB->get_record('lcms_track',array('lcms'=>$id,'userid'=>$USER->id))){
            $track = new stdClass();
            $track->lcms = $id;
            $track->userid = $USER->id;
            $track->timeview = time();
            $track->attempts = 1;
            $track->progress = 0;
            $track->playpage = 1;
            $track->lastpage = 1;
            $track->id = $DB->insert_record('lcms_track',$track);
        }

        if($totalpage > 0 && $lcms->duration > 1 && $track->progress < 100){

            $status = 0;
            
            if($lcmsduration->progress == 0){
                $lcms->positionto = $lcms->duration;
            }
            
            //이벤트에 따라 업데이트 처리
            if($lcms->positionevent == 5 && $lcmsduration->progress == 1){
                $query = 'select * from {lcms_playtime} '
                        . 'where userid=:userid and lcmsid=:lcmsid and positionpage=:positionpage and positionevent = :positionevent order by id desc limit 1';
                $params = array('userid'=>$USER->id,'lcmsid'=>$id,'positionpage'=>$lcms->positionpage,'positionevent'=>$lcms->positionevent);

                if($playtime = $DB->get_record_sql($query,$params)){
                    if($playtime->positionto == $lcms->positionfrom){
                        $playtime->positionto = $lcms->positionto;
                        $playtime->timemodified = time();
                        $DB->update_record('lcms_playtime',$playtime);
                        $status = 1;
                    }
                }
            } 

            //아직 전송되지 않았으면 인서트 처리
            if($status == 0) {
                $query = 'select count(*) from {lcms_playtime} 
                    where userid=:userid and lcmsid=:lcmsid and positionpage=:positionpage 
                    and positionfrom<=:positionfrom and positionto>=:positionto';
                $params = array('userid'=>$USER->id,'lcmsid'=>$id,'positionpage'=>$lcms->positionpage,
                    'positionto'=>$lcms->positionto,'positionfrom'=>$lcms->positionfrom);
                $playcount = $DB->count_records_sql($query,$params);

                //플레이시간을 저장한다. 
                if($playcount==0) $DB->insert_record('lcms_playtime',$lcms);    
            }

            $progress = lcms_get_progress_pageplaytime($id, $cour->contents, $USER->id);
            $track->playpage = $progress['playpage'];
            $track->playtime = $progress['playtime'];
            $track->progress = round($track->playtime/$progress['duration']*100);

        }

        $track->lasttime = $lcms->positionto;
        $track->lastpage = $lcms->positionpage;
        $track->device = $lcms->device;
        $track->timeview = time();
        $DB->update_record('lcms_track',$track);

        lcmsprogress_update_progress_score($cour->course, $USER->id);

        $returnvalue->last = date('Y-m-d H:i:s',$track->timeview);
        $ptm = time_from_seconds($track->playtime);
        $returnvalue->totaltime = $ptm->h.':'.$ptm->m.':'.$ptm->s;
        $returnvalue->totalpage = $track->playpage;
        $returnvalue->progress = $track->progress.' %';
        $returnvalue->status = 'success';

    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

