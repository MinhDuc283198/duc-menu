<?php
require_once '../../config.php';
require_once $CFG->libdir.'/completionlib.php';

require_once 'lib.php';
require_once '../lcmsprogress/locallib.php';

$id = required_param('id',PARAM_INT);

$PAGE->set_url('/mod/lcms/package.php', array('id'=>$id));

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('lcms contents is incorrect');
    }
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $lcms->contents))) {
        print_error('course module is incorrect');
    }
} else {
    print_error('missingparameter');
}

echo $OUTPUT->header();
?>
<link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/repository/viewer/flowplayer/skin/skin.css">
<script src="<?php echo $CFG->wwwroot; ?>/local/repository/viewer/flowplayer/flowplayer.min.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/local/repository/viewer/flowplayer/flowplayer.min.js"></script>
<div id="package">
</div>
    <script>
    $(window).load(function() {
        setInterval(function () {
                update_page_progress('<?php echo $id;?>');
        }, 5000);
                        var stream_url = '<?php echo $CFG->vodserver; ?>:1935/vod/uploads/mp4:<?php echo $contents->id; ?>/video.mp4/playlist.m3u8';
                        var api = $("#package").flowplayer({
                            splash: false,
                            autoplay: true,
                            embed: false, // setup would need iframe embedding
                            ratio: 5 / 12,
                            // manual HLS level selection for Drive videos
                            hlsQualities: "drive",
                            // manual VOD quality selection when hlsjs is not supported
                            //defaultQuality: "260p",
                            //qualities: ["160p", "260p", "530p", "800p"],

                            speeds: [0.75, 1, 1.25, 1.5],
                                // configuration common to all players in
                                // containers with class="player" goes here
                            clip: {
                                sources: [
                                    {type: "application/x-mpegurl",src: stream_url}
                                ]
                            }
                        });
    });
    function update_page_progress(id) {

    var status = true;
    
    $.ajax({
        url: 'package_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {id:id,curpage:1,totalpage:0},
        success: function (data, textStatus, jqXHR) {
            if (data.status == 'success') {
                status = true;
            } else {
                alert(data.message);
                status = false;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
            status = false;
        }
    });

    return true;

}
</script>

<?php
echo $OUTPUT->footer();