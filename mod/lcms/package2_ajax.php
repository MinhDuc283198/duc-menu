<?php

require_once '../../config.php';
require_once $CFG->libdir.'/completionlib.php';

require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once '../lcmsprogress/locallib.php';

$event  = required_param('event', PARAM_INT);   // 1=start, 2=finish
$id     = required_param('id', PARAM_INT);      // cmid
$page   = required_param('page', PARAM_INT);

$error = '';

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        $error = 'Course Module ID was incorrect';
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        $error = 'Course is misconfigured';
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        $error = 'Course module is incorrect';
    }
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $lcms->contents))) {
        $error = 'LCMS contents is incorrect';
    }
} else {
    $error = 'Missing parameter';
}

if(!empty($error)) {
    $returnvalue = new stdClass();
    $returnvalue->status = 'error';
    $returnvalue->message = $error;
    
    @header('Content-type: application/json; charset=utf-8');
    echo json_encode($returnvalue);
    
    die;
}

require_course_login($course, true, $cm);

// 콘텐츠 본 시간 측정
$timestart = 0;

$timeevent = time();

// playtime 테이블에 기록한다.
// 페이지당 하나의 레코다가 생성되도록 한다.
if($playtime = $DB->get_record('lcms_playtime',array('lcmsid'=>$lcms->id,'userid'=>$USER->id, 'positionpage'=>$page))) {
    if($event == 2) { // finish 인 경우 이전(시작) 시간 저장
        $timestart = $playtime->timecreated;
    }
    
    $playtime->positionevent = $event;
    $playtime->timecreated   = $timeevent;
    
    $DB->update_record('lcms_playtime', $playtime);
} else {
    $playtime = new stdClass();
    $playtime->lcmsid        = $lcms->id;
    $playtime->userid        = $USER->id;
    $playtime->positionto    = $page;
    $playtime->positionfrom  = $page;
    $playtime->positionpage  = $page;
    $playtime->positionevent = $event;
    $playtime->timecreated   = $timeevent;
    
    $DB->insert_record('lcms_playtime', $playtime);
}

// 레코드 갯수를 count 해서 진도를 계산한다.
$count_playtime = $DB->count_records('lcms_playtime', array('lcmsid'=>$lcms->id,'userid'=>$USER->id));
//$progress = intval (floor((floatval ($count_playtime) / floatval ($contents->con_total_time)) * 100));
$progress = floor( ($count_playtime / $contents->con_total_time) * 100 );

$timeplay = 0;
if($event == 2 && $timestart > 0) { // finish 인 경우
    $timeplay = ($timeevent - $timestart);
}

if($track = $DB->get_record('lcms_track',array('lcms'=>$lcms->id,'userid'=>$USER->id))){
    $track->timeview = $timeevent;
    $track->device = 0;
    $track->progress = $progress;
    $track->playpage = $count_playtime;
    $track->lastpage = $page;
    $track->playtime += $timeplay;
    
    $DB->update_record('lcms_track',$track);
} else {
    $track = new stdClass();
    $track->lcms = $lcms->id;
    $track->userid = $USER->id;
    $track->timeview = $timeevent;
    $track->device = 0;
    $track->attempts = 1;
    $track->progress = $progress;
    $track->playpage = $count_playtime;
    $track->lastpage = $page;
    $track->playtime = $timeplay;
    
    $DB->insert_record('lcms_track',$track);
}

lcmsprogress_update_progress_score($lcms->course, $USER->id);

$completion = new completion_info($course);
if($completion->is_enabled($cm)) {
    // Mark viewed if required
    $completion->set_module_viewed($cm);
    
    $complete = COMPLETION_INCOMPLETE;
    if($progress >= $lcms->completionprogress) {
        $complete = COMPLETION_COMPLETE;
    }
    $completion->update_state($cm, $complete);
}


$returnvalue = new stdClass();
$returnvalue->status = 'success';
$returnvalue->progress = $progress;
$returnvalue->view = $count_playtime;
$returnvalue->total = $contents->con_total_time;

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);