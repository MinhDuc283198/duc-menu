<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';

$key_code = required_param('key_code', PARAM_RAW);

//오늘 날짜, 유저아이디 조합해서 암호화
$current = date('m/d/Y');
$userid = $USER->id;
//$userid = 2;
$key = $userid.'/'.$current;
$keycode_hash = sha1($key);
echo ($keycode_hash == $key_code)? 0:1;



