<?php

$sql = 'select * from {lcms_caption} where lcms_id=? and lang=? order by stime, etime asc';
$captions_ko = $DB->get_records_sql($sql,array($lcms->id,'ko'));

$sql = 'select * from {lcms_caption} where lcms_id=? and lang=? order by stime, etime asc';
$captions_en = $DB->get_records_sql($sql,array($lcms->id,'en'));
$currenttab = 'tab_ko';
$rows = array (
    new tabobject('tab_ko', "javascript:lang_change('ko');", 'Korean', 'Korean subtitle', true),
    new tabobject('tab_en', "javascript:lang_change('en');", 'English', 'English subtitle', true)
    );
print_tabs(array($rows), $currenttab);

?>
<div class="subtitle_all_area">
<!-- Subtitle Table Wrap Start -->
<div class="subtitle_area" id="subtitle_ko" style="position:relative;width:100%;overflow:scroll;border:1px solid #ccc;">

<?php
$caption_table = '<ol class="subtitle_list">';

if ($captions_ko && is_array($captions_ko)) {
    $minute = 1;
    $time = '';
    foreach ($captions_ko as $caption) {
        $time_temp = $caption->stime / 60; 
        if($time_temp >= $minute) {
            $caption_table .= '<li class="print_caption" style="list-style:none;margin-bottom: 20px;">
                 <ul class="subtitle_time">'. $time .'</ul>
                 <ul class="subtitle_text">'. $content .'</ul>';
            $caption_table .= '</li>';
            $content = '';
            $time = '';
            $minute++;
        }
        
        $caption->caption = preg_replace('/\r\n|\r|\n/', '', $caption->caption);
        $caption->caption = stripslashes($caption->caption);
        $content .= $caption->caption.' ';
        $timeobj = time_from_seconds($caption->stime);
        if(empty($time)) {
            $time = $timeobj->h.' : '.$timeobj->m.' : '.$timeobj->s;
        }
    }
    $caption_table .= '<li class="print_caption" style="list-style:none;">
                 <ul class="subtitle_time">'. $time .'</ul>
                 <ul class="subtitle_text">'. $content .'</ul>'; 
    $caption_table .= '</li>';
} else {

    $caption_table = '<li>등록된 자막이 없습니다.</li>';
}

$caption_table .= '</ol>';

echo $caption_table;
?>

</div>

<!-- Subtitle Table Wrap Start -->
<div class="subtitle_area" id="subtitle_en" style="position:relative;width:100%;overflow:scroll;border:1px solid #ccc;display: none;">

<?php
$caption_table = '<ol class="subtitle_list">';

if ($captions_en && is_array($captions_en)) {
    $minute = 1;
    $time = '';
    foreach ($captions_en as $caption) {
        $time_temp = $caption->stime / 60; 
        if($time_temp >= $minute) {
            $caption_table .= '<li class="print_caption" style="list-style:none;margin-bottom: 20px;">
                 <ul class="subtitle_time">'. $time .'</ul>
                 <ul class="subtitle_text">'. $content .'</ul>';
            $caption_table .= '</li>';
            $content = '';
            $time = '';
            $minute++;
        }
        
        $caption->caption = preg_replace('/\r\n|\r|\n/', '', $caption->caption);
        $caption->caption = stripslashes($caption->caption);
        $content .= $caption->caption.' ';
        $timeobj = time_from_seconds($caption->stime);
        if(empty($time)) {
            $time = $timeobj->h.' : '.$timeobj->m.' : '.$timeobj->s;
        }
    }
    $caption_table .= '<li class="print_caption" style="list-style:none;">
                 <ul class="subtitle_time">'. $time .'</ul>
                 <ul class="subtitle_text">'. $content .'</ul>'; 
    $caption_table .= '</li>';
} else {

    $caption_table = '<li>등록된 자막이 없습니다.</li>';
}

$caption_table .= '</ol>';

echo $caption_table;
?>

</div>
</div>

<form name="caption_form" id="caption_form">
    <input type="hidden" name="amode"/>
    <input type="hidden" name="stime"/>
    <input type="hidden" name="etime"/>
    <input type="hidden" name="caption"/>
    <input type="hidden" name="lang" value="ko"/>
<!--    <input type="hidden" name="lcmsid" value="<?php echo $cid; ?>"/>-->
    <input type="hidden" name="num"/>
    <input type="hidden" name="type"/>
</form>
<!-- Subtitle Table Wrap End -->

<script type="text/javascript">
            
    function lang_change(lang,cur){
        $('.subtitle_area').hide();
        $('#subtitle_'+lang).show();
        $('input[name=lang]').val(lang);
        $('.nav-tabs li').removeClass('active');
        cur.addClass('active');
    }
</script>


