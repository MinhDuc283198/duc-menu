<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';

if(!isloggedin() || $USER->username == 'guest') {
     echo '<script type="text/javascript">
         alert("로그인이 필요한 서비스입니다.");
         document.location.href="'.$SITECFG->wwwroot.'/main/user/login.php";
         </script>';
     exit;
}

$id = required_param('id',PARAM_INT);
$vid = required_param('vid',PARAM_RAW);
$qua = optional_param('qua',0,PARAM_INT);

$contents = lcms_get_contents_info($vid,$qua);

if($contents->logoview=='Y'){
    $logourl = $CFG->wwwroot.'/mod/lcms/player/images/logo_snui.png';
}else{
    $logourl = '';
}

$caption_ko = LCMS.'/viewer/?cr='.$vid.'&lang=ko';
$caption_en = LCMS.'/viewer/?cr='.$vid.'&lang=en';
$capture = LCMS.'/viewer/?i='.$vid;

?>

<div id="mediaplayer"></div>

<form name="playtime_form" id="playtime_form">
    <input type="hidden" name="act"/>
    <input type="hidden" name="positionpage" value="1"/>
    <input type="hidden" name="positionfrom"/>
    <input type="hidden" name="positionto"/>
    <input type="hidden" name="duration"/>
    <input type="hidden" name="lcmsid" value="<?php echo $id;?>"/>
</form>

<script type="text/javascript">

function videoplayer(server,storage,vodname,author,ccmark){
        
    //디바이스별 판별
    var _ua = window.navigator.userAgent.toLowerCase();
        
    var browser = {
        ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
        ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
        iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
        android : /webkit/.test( _ua )&&/android/.test( _ua ),
        msie : /msie/.test( _ua )
    };   
        
        
    //디바이스별 플레이어 사용
    if(browser.ipod || browser.iphone) {
        html5player(server,storage,vodname,author,ccmark);
    } else if(browser.ipad) {
        html5player(server,storage,vodname,author,ccmark);
    } else if(browser.android) {
        var stream_url = 'rtsp://'+server+':1935/vod/_definst_/mp4:'+storage+'/'+vodname;
        self.location = stream_url;
    } else {
        flashplayer(server,storage,vodname,author,ccmark);
    }
        
}
            
function html5player(server,storage,vodname,author,ccmark){
            
    var stream_url = 'http://'+server+':1935/vod/_definst_/mp4:'+storage+'/'+vodname+'/playlist.m3u8';
                
    $('#mediaplayer').css('height','auto');
               
    $("#mediaplayer").html('<video preload controls autostart="true"'+
        'poster="<?php echo $capture;?>" src="'+stream_url+'">'+
        '<track kind=captions label="Korean subtitles" src="<?php echo $caption_ko;?>" lang="ko" default></track>'+
        '<track kind=captions label="English subtitles" src="<?php echo $caption_en;?>" lang="en"></track>'+
        '</video>').show();
            
}
            
    
//jwplayer호출
function flashplayer(server,storage,vodname,author,ccmark){
    
    jwplayer('mediaplayer').setup({
        'flashplayer': '<?php echo $CFG->wwwroot;?>/mod/lcms/player/swf/player.swf',
        'id': 'mediaplayer',
        'menu': 'false',
        'width': '100%',
        'height': '100%',
        'skin': '<?php echo $CFG->wwwroot;?>/mod/lcms/player/skins/darkrv5.zip',
        'logo': { 'file': '<?php echo $logourl;?>', 'hide': false, 'position': 'top-right' },
        'streamer': 'rtmp://'+server+'/vod/_definst_/',
        'provider': 'rtmp',
        'file': 'mp4:/'+storage+'/'+vodname,
        'image': '<?php echo $capture;?>',
        'plugins': {
            '<?php echo $CFG->wwwroot;?>/mod/lcms/player/js/textoutput.js': {'text': ccmark},
            'captions-2': {
                        files: '<?php echo $caption_ko;?>,<?php echo $caption_en;?>',
                        labels: 'Korean,English',
                        fontSize: '14',
                        fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                    }    
        },
                        
        'autostart': false
    });
                    
    jw_sync();
        
}
 
    
function youtubeplayer(emb,author,ccmark){

    //디바이스별 판별
    var _ua = window.navigator.userAgent.toLowerCase();
        
    var browser = {
        ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
        ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
        iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
        android : /webkit/.test( _ua )&&/android/.test( _ua ),
        msie : /msie/.test( _ua )
    };   
        
        
    //디바이스별 플레이어 사용
    if(browser.ipod || browser.iphone || browser.ipad || browser.android) {
        
        var videolist = '<iframe width="100%" height="300" src="http://www.youtube.com/embed/'+emb+'?html5=1" frameborder="0" allowfullscreen></iframe>';
        
        $('#mediaplayer').empty().append(videolist);
        
    } else {
        
        jwplayer('mediaplayer').setup({
            'flashplayer': '<?php echo $CFG->wwwroot;?>/mod/lcms/player/swf/player.swf',
            'id': 'mediaplayer',
            'menu': 'false',
            'width': '100%',
            'height': '100%',
            'skin': '<?php echo $CFG->wwwroot;?>/mod/lcms/player/skins/darkrv5.zip',
            'logo': { 'file': '<?php echo $logourl;?>', 'hide': false, 'position': 'top-right' },
            'file': 'http://www.youtube.com/watch?v='+emb,
            'plugins': {
                '<?php echo $CFG->wwwroot;?>/mod/lcms/player/js/textoutput.js': {'text': ccmark},
                'captions-2': {
                        files: '<?php echo $caption_ko;?>,<?php echo $caption_en;?>',
                        labels: 'Korean,English',
                        fontSize: '14',
                        fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                    }    
            },
            'autostart': false
        });
        
        jw_sync();
        
    }           
    
}
            
function vimeoplayer(emb,author,ccmark){
                
               
    var videolist = '<iframe src="//player.vimeo.com/video/'+emb+'" width="100%" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        
    $('#mediaplayer').empty().append(videolist);
  
                
    
}
            
            
function video_sync(){

    var positionfrom = $('#position').val(), positionto = 0, duration = 0, oldposition = 0, timestamps = [];
                
    var v = document.createElement('video');
    var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
    if ( supported == 'probably') { 
        
        positionto = playtime_get();
        positionfrom = positionto;
                    
        $('video').bind('play',function(event){
            positionfrom = positionto;
        });
                    
        $('video').bind('pause',function(event){
            if(positionfrom < positionto){
                playtime_update(positionfrom,positionto); 
            }
        });
                    
        $('video').bind('seeked',function(event){
            $('video').pause();
            $('video').play();
        });
                    
        $('video').bind('ended',function(event){
            playtime_update(positionfrom,positionto); 
            positionfrom = positionto;
        });
                    
   
        $('video').bind('timeupdate',function(event){
      
            positionto = parseInt(this.currentTime);
                        
            if(positionto > oldposition){
                caption_scroll();
            }
            
            oldposition = positionto;
      
        });
        
        $('.subtitle_area .subtitle_text').each(function(o){
                if($(this).attr('data-start')){
                    timestamps.push({
                        start : +$(this).attr('data-start'),
                        end : +$(this).attr('data-end'),
                        elm : $(this),
                        pos : $(this).position().top
                    });
                }
            });
        
        function caption_scroll(){
    
        $('.subtitle_area .subtitle_text').css({'background':'#fff'});
                
        //console.log(positionto);
      
        for(var i=0;i<timestamps.length;i++ ){
        if(positionto >= timestamps[i].start && positionto <= timestamps[i].end){ 
                        
            timestamps[i].elm.css({'background':'#eee'});
                        
            var position = timestamps[i].pos;
                        
            $('.subtitle_area').scrollTop(position);
                         
        }
        }
                         
    }
            
    }

}
    
function jw_sync(){
                
    var positionfrom = 0, positionto = 0, saveposition = 0, oldposition = 0, duration = 0, timestamps = [], timer;
    
    /* JWPlayer 상태 변화 */
    jwplayer('mediaplayer').onTime(function(event) {
        positionto = parseInt(event.position);
        duration = parseInt(event.duration);
        
        if(positionfrom < positionto && positionto > saveposition + 300){
           playtime_update(positionfrom,positionto,duration);
           saveposition = positionto;
           positionfrom = positionto;
        }
        
        if(positionto > oldposition){
            caption_scroll();
            oldposition = positionto;
        }
        
    });
    
    positionto = playtime_get();
    
    jwplayer('mediaplayer').onPlay(function() {
        positionfrom = positionto;
    });
    jwplayer('mediaplayer').onSeek(function(event) {
        if(positionfrom < positionto){
            playtime_update(positionfrom,positionto,duration);
        }
        positionfrom = event.offset;
        positionto = event.offset;
    });
    jwplayer('mediaplayer').onPause(function() {
        if(positionfrom < positionto){
           playtime_update(positionfrom,positionto,duration); 
        }
        
    }); 
	jwplayer('mediaplayer').onBuffer(function() {
		console.log(positionfrom+':'+positionto);
        if(positionfrom < positionto){
            playtime_update(positionfrom,positionto,duration); 
        }
    });
    jwplayer('mediaplayer').onIdle(function() {
        if(positionfrom < positionto){
            playtime_update(positionfrom,positionto,duration);
        }
    });
    
    $('.subtitle_area .subtitle_text').each(function(o){
        if($(this).attr('data-start')){
            timestamps.push({
                start : +$(this).attr('data-start'),
                end : +$(this).attr('data-end'),
                elm : $(this),
                pos : $(this).position().top
            });
        }
    });
    
    
    function caption_scroll(){
    
    $('.subtitle_area .subtitle_text').css({'background':'#fff'});
                
    //console.log(positionto);
      
    for(var i=0;i<timestamps.length;i++ ){
        if(positionto >= timestamps[i].start && positionto <= timestamps[i].end){ 
                        
            timestamps[i].elm.css({'background':'#eee'});
                        
            var position = timestamps[i].pos;
                        
            $('.subtitle_area').scrollTop(position);
                         
        }
    }
    
    }
    
}

function playtime_update(posfrom,posto,duration,second){
        
    var status = true;
    if(!second) second = 0;
        
    $.ajax({
        url: '<?php echo $CFG->wwwroot;?>/mod/lcms/playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: true,
        data: {
            act : 'update',
            positionpage : 1,
            positionto : posto,
            positionfrom : posfrom,
            duration : duration,
            lcmsid : '<?php echo $id;?>'  
        },
        success: function(data, textStatus, jqXHR ) {
            if(data.status == 'success') {
                status = true;
                $('#study_last_time').text(data.last);
                $('#study_total_time').text(data.totaltime);
                $('#study_rate').text(data.progress);
            } else {
                status = false;
            }
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            
            alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
            //playtime_update(posfrom,posto,duration,second);
            status = false;
        }
    });
        
    return status;
}

function playtime_get(){
    
    var frm = $(top.document).find('#playtime_form');
    frm.find('input[name=act]').val('get');
        
    var positionfrom = 0;
        
    $.ajax({
        url: '<?php echo $CFG->wwwroot;?>/mod/lcms/playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            act : 'get',
            lcmsid : '<?php echo $id;?>'
        },
        success: function(data, textStatus, jqXHR ) {
            if(data.status == 'success') {
                
                if(data.positionto>0&&data.progress<100){
                    var conf = confirm('이전 학습 부분부터 이어보시겠습니까?');
                    if(conf){
                        if($('video').length>0){
                            var v = document.createElement('video');
                            var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
                            if ( supported == 'probably') { 
                                $('#position').val(data.positionto);
                            }
                        }else{
                            jwplayer('mediaplayer').seek(data.positionto);
                            positionfrom = data.positionto;
                            return positionfrom;
                        }
                                    
                    }
                }
                
            } else {
                //alert(data.message);
            }
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
        }
    });
        
    return positionfrom;
    
}
</script>

<?php
if($contents->type=='Video' || $contents->type=='mid'){
    
    if ($contents->format == 'flv') {
        echo '<script type="text/javascript">
            flashplayer("'.$contents->server.'","'.$contents->storage.'","'.$contents->vodname.'","'.$contents->author.'","'.$contents->ccmark.'");
            </script>';
    } else {
        echo '<script type="text/javascript">
            videoplayer("'.$contents->server.'","'.$contents->storage.'","'.$contents->vodname.'","'.$contents->author.'","'.$contents->ccmark.'");
            </script>';
    }
    
}else if($contents->type=='Embed'){
   
    if ($contents->embed_type == 'youtube') {
        echo '<script type="text/javascript">
            youtubeplayer("'.$contents->embed_code.'","'.$contents->author.'","'.$contents->ccmark.'");
            </script>';
    } else {
        echo '<script type="text/javascript">
            vimeoplayer("'.$contents->embed_code.'","'.$contents->author.'","'.$contents->ccmark.'");
            </script>';
    }
    
}

?>

<?php
//require_once '../../config.php';
//require_once $CFG->dirroot . '/local/repository/config.php';
//require_once 'lib.php';
//
//if (!isloggedin() || $USER->username == 'guest') {
//    echo '<script type="text/javascript">
//         alert("로그인이 필요한 서비스입니다.");
//         document.location.href="' . $SITECFG->wwwroot . '/login/index.php";
//         </script>';
//    exit;
//}
//
//$userprogress = $track->progress;
////$comprogress = $okmedia->completionprogress;
//if (empty($userprogress)) {
//    $userprogress = 0;
//}
//if (empty($track)) {
//    $trackstatus = 0;
//} else {
//    $trackstatus = 1;
//}
//
//function lcms_courseinfo_urlf($url) {
//
//    global $CFG, $DB;
//
//    $obj = new StdClass();
//
//    $xmlfile = $url;
//    $entry = simplexml_load_file($xmlfile);
//
//    $obj->type = $entry->stream->type;
//    $obj->title = $entry->stream->title;
//    $obj->des = $entry->stream->des;
//    $obj->teacher = $entry->stream->teacher;
//    $obj->logoview = $entry->stream->logoview;
//    $obj->author = $entry->stream->author;
//    $obj->ccmark = $entry->stream->ccmark;
//    $obj->duration = $entry->stream->duration;
//
//    if ($obj->type == 'video') {
//
//        $obj->quality = $entry->stream->quality;
//        $obj->format = $entry->stream->format;
//        $obj->server = $entry->stream->server;
//        $obj->storage = $entry->stream->storage;
//        $obj->lcmsname = $entry->stream->vodname;
//    } else if ($obj->type == 'Flash') {
//
//        $obj->startfile = $entry->stream->startfile;
//        $obj->filepath = $entry->stream->filepath;
//        $obj->rtype = $entry->stream->rtype;
//        $obj->totalpage = $entry->stream->totalpage;
//    } else if ($obj->type == 'Embed') {
//
//        $obj->embed_type = $entry->stream->embed_type;
//        $obj->embed_code = $entry->stream->embed_code;
//    }
//
//    return $obj;
//}

// mod/lcms/lib.php 에 있는 함수이나 인식하지 못함.
//$contents;
//if (function_exists('lcms_courseinfo_urls')) {
//    $contents = lcms_courseinfo_urls($url);
//} else {
//    $contents = lcms_courseinfo_urlf($url);
//}
//if ($contents->logoview == 'Y') {
//    $logourl = $CFG->wwwroot . '/mod/lcms/player/images/logo_snui.png';
//} else {
//    $logourl = '';
//}
//
//$caption_ko = LCMS . '/viewer/?cr=' . $vid . '&lang=ko';
//$caption_en = LCMS . '/viewer/?cr=' . $vid . '&lang=en';
//$capture = LCMS . '/viewer/?i=' . $vid;
?>
<!--<script src="<?php // echo $CFG->wwwroot . '/mod/lcms/player/flowplayer/flowplayer.min.js' ?>"></script>
<script src="<?php // echo $CFG->wwwroot . '/lib/flowplayer/flowplayer-3.2.13.js' ?>"></script>
<script src="<?php echo $CFG->wwwroot . '/mod/lcms/player/flowplayer/hlsjs.min.js' ?>"></script>
<script src="<?php echo $CFG->wwwroot . '/mod/okmedia/viewer/player.js' ?>"></script>-->
<div id="mediaplayer"></div>
<!--playtime_update-->
<!--<form name="playtime_form" id="playtime_form">
    <input type="hidden" name="act"/>
    <input type="hidden" name="positionpage" value="1"/>
    <input type="hidden" name="positionfrom"/>
    <input type="hidden" name="positionto"/>
    <input type="hidden" name="duration"/>
    <input type="hidden" name="lcmsid" value="////<?php echo $id; ?>"/>
</form>-->

<!--<script type="text/javascript">

    var positionfrom = 0, positionto = 0, saveposition = 0, oldposition = 0, ispause = false, capposition = 0, saveposition = 0, evented = 0;
//    var satiscompleted = $('#satiscompleted').val();
    var trackstatus = '<?php echo $trackstatus ?>';
    var videourl = '<?php echo $play_url ?>';

    var duration = 0, timestamps = [], timer;

    window.onload = function () {
        
        positionto = lcms_playtime_get();

        if (trackstatus != 0 && positionto != 0) {
            if (confirm('이전 학습구간 시점부터 이어보시겠습니까?')) {
                positionfrom = positionto;
                flowplayer_load(videourl);
            } else {
                positionfrom = 0;
                flowplayer_load(videourl);
            }
        } else {
            positionfrom = 0;
            flowplayer_load(videourl);
        }
        return false;
    };

    function flowplayer_load(videourl) {

        flowplayer("#videoarea", {
            key: "$554493833420532",
            splash: false,
            autoplay: true,
            embed: false, // setup would need iframe embedding
            ratio: 5 / 12,
            // manual HLS level selection for Drive videos
            hlsQualities: "drive",
//            speeds: [0.75,1,1.25,1.5],
            hlsjs: {recoverNetworkError: true},
            clip: {
                sources: [
                    {type: 'video/mp4', src: videourl, suffix: 'mp4'}
                ]
            }

        }).on("ready", function (e, api, video) {
            api.seek(positionfrom);
//            lcms_pop_progress();
            duration = Math.floor(video.duration);
        });
    }

    /* global event listeners for demo purposes, omit in production */
    flowplayer(function (api, root) {
        console.log('flowplayer');
        var selfprogress = <?php echo $userprogress ?>;
        var comprogress = <?php echo $comprogress ?>;

        var instanceId = root.getAttribute("data-flowplayer-instance-id"),
                engineInfo = document.getElementById("engine" + instanceId),
                vtypeInfo = document.getElementById("vtype" + instanceId),
                detail = document.getElementById("detail" + instanceId);

        api.on("resume", function (e) {
            //플레이 버튼 클릭 시 호출
//            console.log('resume::'+positionfrom+'::'+positionto);
            if (positionto > 1) {
                positionfrom = positionto;
            }
            evented = 0;
        }).on("pause", function (e, api) {
            //Pause 버튼 클릭 시 호출
//            console.log('pause::'+positionfrom+'::'+positionto);
            if (positionfrom < positionto) {
                lcms_playtime_update(positionfrom, positionto, 1, duration, mode);
            }
            alert(positionfrom);
            alert(positionto);
        }).on("beforeseek", function (e, api) {
            //seek 버튼 클릭 시 호출
//            console.log('beforeseek::'+positionfrom+'::'+positionto);
            if (positionfrom < positionto) {
                lcms_playtime_update(positionfrom, positionto, 2, duration, mode);
            }
        }).on("seek", function (e, api) {
            //seek 버튼 클릭 후 seek되면 호출
//            console.log('seek::'+positionfrom+'::'+positionto);
            if (positionto > 1) {
                positionfrom = positionto;
            }
            evented = 0;
        }).on("finish", function (e, api) {
//            console.log('finish::'+positionfrom+'::'+positionto);
            if (evented != 3 && positionfrom < duration) {
                lcms_playtime_update(positionfrom, duration, 3, duration, mode);
            }
            positionfrom = positionto = 0;
            evented = 1;
        }).on("progress", function (e, api) {
            //타임라인
            positionto = Math.ceil(api.video.time);

            //진도율 저장 (3분마다 진도율을 저장함.)
            if (evented == 0 && positionfrom < positionto && positionto % (10) == 0) {
                lcms_playtime_update(positionfrom, positionto, 5, duration, mode);
                saveposition = positionto;
                positionfrom = positionto;
            }
        }).on("cuepoint", function (e, api, cuepoint) {
            if (cuepoint.subtitle) {
                oldposition = parseInt(cuepoint.time);
                caption_scroll(oldposition);
            }
        });
    });

    function videoplayer(server, storage, vodname, author, ccmark) {

        //디바이스별 판별
        var _ua = window.navigator.userAgent.toLowerCase();

        var browser = {
            ipod: /webkit/.test(_ua) && /\(ipod/.test(_ua),
            ipad: /webkit/.test(_ua) && /\(ipad/.test(_ua),
            iphone: /webkit/.test(_ua) && /\(iphone/.test(_ua),
            android: /webkit/.test(_ua) && /android/.test(_ua),
            msie: /msie/.test(_ua)
        };


        //디바이스별 플레이어 사용
        if (browser.ipod || browser.iphone) {
            html5player(server, storage, vodname, author, ccmark);
        } else if (browser.ipad) {
            html5player(server, storage, vodname, author, ccmark);
        } else if (browser.android) {
            var stream_url = 'rtsp://' + server + ':1935/vod/_definst_/mp4:' + storage + '/' + vodname;
            self.location = stream_url;
        } else {
            flashplayer(server, storage, vodname, author, ccmark);
        }

    }

    function html5player(server, storage, vodname, author, ccmark) {

        var stream_url = 'http://' + server + ':1935/vod/_definst_/mp4:' + storage + '/' + vodname + '/playlist.m3u8';

        $('#mediaplayer').css('height', 'auto');

        $("#mediaplayer").html('<video preload controls autostart="true"' +
                'poster="////<?php echo $capture; ?>" src="' + stream_url + '">' +
                '<track kind=captions label="Korean subtitles" src="////<?php echo $caption_ko; ?>" lang="ko" default></track>' +
                '<track kind=captions label="English subtitles" src="////<?php echo $caption_en; ?>" lang="en"></track>' +
                '</video>').show();

    }


//jwplayer호출
    function flashplayer(server, storage, vodname, author, ccmark) {

        jwplayer('mediaplayer').setup({
            'flashplayer': '////<?php echo $CFG->wwwroot; ?>/mod/lcms/player/swf/player.swf',
            'id': 'mediaplayer',
            'menu': 'false',
            'width': '100%',
            'height': '100%',
            'skin': '////<?php echo $CFG->wwwroot; ?>/mod/lcms/player/skins/darkrv5.zip',
            'logo': {'file': '////<?php echo $logourl; ?>', 'hide': false, 'position': 'top-right'},
            'streamer': 'rtmp://' + server + '/vod/_definst_/',
            'provider': 'rtmp',
            'file': 'mp4:/' + storage + '/' + vodname,
            'image': '////<?php echo $capture; ?>',
            'plugins': {
                '////<?php echo $CFG->wwwroot; ?>/mod/lcms/player/js/textoutput.js': {'text': ccmark},
                'captions-2': {
                    files: '////<?php echo $caption_ko; ?>,<?php echo $caption_en; ?>',
                                        labels: 'Korean,English',
                                        fontSize: '14',
                                        fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                                    }
                                },

                                'autostart': false
                            });

                            jw_sync();

                        }


                        function youtubeplayer(emb, author, ccmark) {

                            //디바이스별 판별
                            var _ua = window.navigator.userAgent.toLowerCase();

                            var browser = {
                                ipod: /webkit/.test(_ua) && /\(ipod/.test(_ua),
                                ipad: /webkit/.test(_ua) && /\(ipad/.test(_ua),
                                iphone: /webkit/.test(_ua) && /\(iphone/.test(_ua),
                                android: /webkit/.test(_ua) && /android/.test(_ua),
                                msie: /msie/.test(_ua)
                            };


                            //디바이스별 플레이어 사용
                            if (browser.ipod || browser.iphone || browser.ipad || browser.android) {

                                var videolist = '<iframe width="100%" height="300" src="http://www.youtube.com/embed/' + emb + '?html5=1" frameborder="0" allowfullscreen></iframe>';

                                $('#mediaplayer').empty().append(videolist);

                            } else {

                                jwplayer('mediaplayer').setup({
                                    'flashplayer': '////<?php echo $CFG->wwwroot; ?>/mod/lcms/player/swf/player.swf',
                                    'id': 'mediaplayer',
                                    'menu': 'false',
                                    'width': '100%',
                                    'height': '100%',
                                    'skin': '////<?php echo $CFG->wwwroot; ?>/mod/lcms/player/skins/darkrv5.zip',
                                    'logo': {'file': '////<?php echo $logourl; ?>', 'hide': false, 'position': 'top-right'},
                                    'file': 'http://www.youtube.com/watch?v=' + emb,
                                    'plugins': {
                                        '////<?php echo $CFG->wwwroot; ?>/mod/lcms/player/js/textoutput.js': {'text': ccmark},
                                        'captions-2': {
                                            files: '////<?php echo $caption_ko; ?>,<?php echo $caption_en; ?>',
                                                                    labels: 'Korean,English',
                                                                    fontSize: '14',
                                                                    fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                                                                }
                                                            },
                                                            'autostart': false
                                                        });

                                                        jw_sync();

                                                    }

                                                }


                                                /* end global event listeners setup */
                                                function popCloseFunction() {
                                                    console.log(positionfrom);
                                                    console.log(positionto);
                                                    console.log(duration);
                                                    lcms_playtime_update_close(positionfrom, positionto, duration, ////<?php echo $id; ?>);
                                                }

                                                function playtime_update(posfrom, posto, duration, second) {

                                                    var status = true;
                                                    if (!second)
                                                        second = 0;

                                                    $.ajax({
                                                        url: '////<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                                                        type: 'POST',
                                                        dataType: 'json',
                                                        async: true,
                                                        data: {
                                                            act: 'update',
                                                            positionpage: 1,
                                                            positionto: posto,
                                                            positionfrom: posfrom,
                                                            duration: duration,
                                                            lcmsid: '////<?php echo $id; ?>'
                                                        },
                                                        success: function (data, textStatus, jqXHR) {
                                                            if (data.status == 'success') {
                                                                status = true;
                                                                $('#study_last_time').text(data.last);
                                                                $('#study_total_time').text(data.totaltime);
                                                                $('#study_rate').text(data.progress);
                                                            } else {
                                                                status = false;
                                                            }
                                                        },
                                                        error: function (jqXHR, textStatus, errorThrown) {

                                                            alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                                                            //playtime_update(posfrom,posto,duration,second);
                                                            status = false;
                                                        }
                                                    });
                                                    return status;
                                                }

                                                function lcms_playtime_update_close(posfrom, posto, duration, id, second) {
                                                    var status = true;
                                                    console.log('heree');
                                                    console.log(posto);
                                                    console.log(posfrom);
                                                    console.log(duration);
                                                    console.log(id);
                                                    
                                                    if (!second)
                                                        second = 0;

                                                    $.ajax({
                                                        url: '////<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                                                        type: 'POST',
                                                        dataType: 'json',
                                                        async: true,
                                                        data: {
                                                            act: 'update',
                                                            positionpage: 1,
                                                            positionfrom: posfrom,
                                                            positionto: posto,
                                                            duration: duration,
                                                            lcmsid: id
                                                        },
                                                        success: function (data) {
                                                            console.log(data)
                                                            if (data.status == 'success') {
                                                                status = true;
                                                                $('#study_last_time').text(data.last);
                                                                $('#study_total_time').text(data.totaltime);
                                                                $('#study_rate').text(data.progress);
                                                            } else {
                                                                status = false;
                                                            }
                                                        },
                                                        error: function (e) {

                                                            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                                                            //playtime_update(id,posfrom,posto,duration,second);
                                                            status = false;
                                                        }
                                                    });
                                                    return status;
                                                }


                                                function lcms_playtime_get() {

                                                    var frm = $(top.document).find('#playtime_form');
                                                    frm.find('input[name=act]').val('get');

                                                    var positionfrom = 0;

                                                    $.ajax({
                                                        url: '////<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                                                        type: 'POST',
                                                        dataType: 'json',
                                                        async: false,
                                                        data: {
                                                            act: 'get',
                                                            lcmsid: '////<?php echo $id; ?>'
                                                        },
                                                        success: function (data, textStatus, jqXHR) {
                                                            if (data.status == 'success') {

                                                                if (data.positionto > 0 && data.progress < 100) {
                                                                    var conf = confirm('이전 학습 부분부터 이어보시겠습니까?');
                                                                    if (conf) {
                                                                        if ($('video').length > 0) {
                                                                            var v = document.createElement('video');
                                                                            var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
                                                                            if (supported == 'probably') {
                                                                                $('#position').val(data.positionto);
                                                                            }
                                                                        } else {
                                                                            jwplayer('mediaplayer').seek(data.positionto);
                                                                            positionfrom = data.positionto;
                                                                            return positionfrom;
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                //alert(data.message);
                                                            }
                                                        },
                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                            alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                                                        }
                                                    });
                                                    return positionfrom;
                                                }
                                                
                                                function lcms_pop_progress() {

                                                    /*진도율 팝업*/
                                                    var mediaid = $('#lcmsconid').val();
                                                    //console.log(mediaid+'/'+duration);

                                                    $("#popbg").height($("body").prop("scrollHeight"));
                                                    $.ajax({
                                                        url: "pop_progress.php",
                                                        data: {id: mediaid, duration: duration},
                                                        success: function (html) {
                                                            $("#durationbar").html($(html).find(".pop_wrap").html());
                                                            $("#durationbar").fadeIn();
                                                            $(".popclose").click(function () {
                                                                $("#durationbar").fadeOut();
                                                            });
                                                        }
                                                    });
                                                }
</script>-->
