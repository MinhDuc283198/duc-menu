<?php
require_once '../../config.php';
require_once $CFG->libdir.'/completionlib.php';

require_once $CFG->dirroot.'/mod/lcms/lib.php';
require_once $CFG->dirroot.'/mod/lcmsprogress/locallib.php';

$id = required_param('id',PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);

$PAGE->set_url($CFG->wwwroot.'/mod/lcms/package.php', array('id'=>$id));

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('lcms contents is incorrect');
    }
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $lcms->contents))) {
        print_error('course module is incorrect');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->set_url('/mod/lcms/package.php', array('id' => $id));

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
echo $OUTPUT->header();

$mode = 'player';
$learningend = $DB->get_field('lmsdata_class','learningend',array('courseid'=>$course->id));
if($learningend){
    if(date('Ymd',strtotime('+7 day',$learningend)) <= date('Ymd')){
        $mode = 'embed';
    }
}else{
    $mode = 'embed';
}

if($mode == 'player'){
    if($track = $DB->get_record('lcms_track',array('lcms'=>$id,'userid'=>$USER->id))){
        $track->attempts = $track->attempts+1;
        $track->timeview = time();
        $DB->update_record('lcms_track',$track);
    }else{        
        $track = new stdClass();
        $track->lcms = $id;
        $track->userid = $USER->id;
        $track->timeview = time();
        $track->attempts = 1;
        $track->progress = 0;
        $track->playpage = 1;
        $track->lastpage = 1;
        $track->id = $DB->insert_record('lcms_track',$track);
    }
}

if($contents->embed_type == 'url'){
    $con_dir = $contents->embed_code;
}else{
    $contents->data_dir = str_replace('storage/','',$contents->data_dir);
    $filename = $DB->get_field('lcms_contents_file','filename', array('con_seq' => $lcms->contents));
    $con_dir = $CFG->wwwroot.'/storage/'.$contents->data_dir.'/'.$filename;    
}

if($track->lastpage > 1 && $mode == 'player'){
    $explodedirs = explode('/',$con_dir);
    $lastpage = (strlen($track->lastpage)==1)? '0'.$track->lastpage : $track->lastpage;
    $explodedirs[sizeof($explodedirs)-1] = $lastpage.'.html';
    $con_dir = implode('/',$explodedirs);
}

if($page > 0){
    $explodedirs = explode('/',$con_dir);
    $lastpage = (strlen($page)==1)? '0'.$page : $page;
    $explodedirs[sizeof($explodedirs)-1] = $lastpage.'.html';
    $con_dir = implode('/',$explodedirs);
}


?>
<iframe id="package" style="width:100vw;height:98vh;border:0;overflow:hidden;" src="<?php echo $con_dir;?>"></iframe>

    <script>
    /*
    $('#package').load(function(){
        var fc = $('#package').contents();
        var fw = $('#package').get(0).contentWindow;
        var url1 = fc.get(0).contentWindow.location.href;
        setInterval(function () {
            var url2 = fc.fget(0).contentWindow.location.href;
            console.log(url2);
            if(url1 !== url2){
                update_page_progress('<?php echo $id;?>');
            }
        }, 5000);
    });
    */
       
    function update_playtime(page,starttime,endtime,posevent,duration) { 
       
        var status = true;
        var mode = '<?php echo $mode;?>';
        
        if(mode == 'player'){
            $.ajax({
                url: '<?php echo $CFG->wwwroot ?>/mod/lcms/package_ajax_api.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {positionpage:page, positionfrom:starttime, positionto:endtime, positionevent:posevent, duration:duration, lcmsid:'<?php echo $id;?>' },
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'success') {
                        status = true;
                    } else {
                        alert(data.message);
                        status = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    status = false;
                }
            });
            return status;
        }
    }
</script>

<?php
echo $OUTPUT->footer();
?>
