<?php

require_once $CFG->dirroot . '/local/repository/config.php';
require_once $CFG->dirroot . '/local/repository/lib.php';
require_once $CFG->dirroot . '/local/repository/pclzip.lib.php';

define('CAPION_FILE_PATTEN_KO', '/[_]{1}[kK]{1}[oO]{1}.srt\z$/');
define('CAPION_FILE_PATTEN_EN', '/[_]{1}[eE]{1}[nN]{1}.srt\z$/');
function lcms_supports($feature) {
    switch ($feature) {
        //case FEATURE_MOD_ARCHETYPE: return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_GROUPS: return true;
        case FEATURE_GROUPINGS: return true;
        case FEATURE_GROUPMEMBERSONLY: return true;
        case FEATURE_MOD_INTRO: return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES: return true;
        case FEATURE_GRADE_HAS_GRADE: return false;
        case FEATURE_GRADE_OUTCOMES: return false;
        case FEATURE_RATE: return false;
        case FEATURE_BACKUP_MOODLE2: return true;
        case FEATURE_SHOW_DESCRIPTION: return true;
        default: return null;
    }
}

function lcms_add_instance($lcms) {
    global $CFG, $DB, $USER;

    $lcms->timecreated = time();
    $lcms->timemodified = time();

    $modcontext = context_module::instance($lcms->coursemodule);
    if ($lcms->insert_file_yn == 1) {

        $lcms->con_name = $lcms->name;
        $lcms->cc_type = 1;
        $lcms->share_yn = 'N';
        $lcms->type = $lcms->con_type;
        
        $newconid = upload_lcms_contents($lcms);
        insert_lcms_history($newconid,'Mod Lcms Added',1);
        $lcms->contents = $newconid;
        
        if($lcms->type=='video'||$lcms->type=='embed'){
            lcms_set_mainfile($lcms);
        }
    }
    
    $lcms->progress = 1;
    
//    $cmid = $lcms->coursemodule;
    $lcms->timemodified = time();    

    $lcms->id = $DB->insert_record('lcms', $lcms);

    // we need to use context now, so we need to make sure all needed info is already in db
//    $DB->set_field('course_modules', 'instance', $lcms->id, array('id'=>$cmid));
    if($lcms->type=='video'||$lcms->type=='embed'){
        
        //자막 파일 DB로 생성
        lcms_update_subtitle($lcms);
        
        // 강의컨텐츠저장소 자막 파일 lcms activity로 복사
        lcms_capy_repository_caption($lcms->id, $lcms->contents, $modcontext->id);
 
    }
    return $lcms->id;
}

function lcms_update_instance($lcms) {
    global $CFG, $DB;

    if (!$lcms_old = $DB->get_record('lcms', array('id' => $lcms->instance))) {
        return false;
    }

    $lcms->timemodified = time();
  
    $lcms->id = $lcms->instance;
    if ($lcms->insert_file_yn == 1 && $lcms->stay_file > 0) {
        $lcms->con_name = $lcms->name;
        $lcms->cc_type = 1;
        $lcms->share_yn = 'N';
        if($lcms->con_type){ 
            $lcms->type = $lcms->con_type;
        } else {
            $lcms->con_type =  $lcms->type;
        }
        if($lcms->stay_file == 1){
            $contents = $DB->get_record('lcms_contents', array('id' => $lcms->contents));
            $contents->con_id = $contents->id;
            $contents->stay_file = 1;
            $contents->groupid = $lcms->groupid;
            $newconid = update_lcms_contents($contents);
        }else if($lcms->stay_file == 2){
            $newconid = upload_lcms_contents($lcms);
            $lcms->contents = $newconid;
        }

         lcms_set_mainfile($lcms);
    }
    if(isset($newconid)){
     insert_lcms_history($newconid,'Mod Lcms Updated',3);
    }
    $DB->update_record('lcms', $lcms);  

    return true;
}

function lcms_delete_instance($id) {
    global $DB;

    if (!$lcms = $DB->get_record('lcms', array('id' => $id))) {
        return false;
    }

    $DB->delete_records('lcms_track', array('lcms' => $lcms->id));

    $DB->delete_records('lcms', array('id' => $lcms->id));

    return true;
}
function lcms_get_coursemodule_info($coursemodule) {
    global $CFG, $DB;
    require_once("$CFG->libdir/filelib.php");
    require_once("$CFG->dirroot/mod/resource/locallib.php");
    require_once($CFG->libdir.'/completionlib.php');

    $context = context_module::instance($coursemodule->id);

    if (!$lcms = $DB->get_record('lcms', array('id'=>$coursemodule->instance))) {
        return NULL;
    }

    $info = new cached_cm_info();
    $info->name = $lcms->name;
    if ($coursemodule->showdescription) {
        // Convert intro to html. Do not filter cached version, filters run at display time.
        $info->content = format_module_intro('lcms', $lcms, $coursemodule->id, false);
    }

    if ($lcms->type == 'hgofohdfo') {
        $info->icon ='i/html';
        return $info;
    }

    if ($lcms->type == 'html') {
        $content = $DB->get_record('lcms_contents',array('id'=>$lcms->contents));
        $fullurl = "$CFG->wwwroot/mod/lcms/package.php?id=$coursemodule->id&amp;redirect=1&amp;inpopup=1";
        $popupwidth = !$content->popupwidth? '1280':$content->popupwidth;
        $popupheight = !$content->popupheight? '900':$content->popupheight;
        $wh = "width=".$popupwidth."px,height=".$popupheight."px,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no,fullscreen=yes";
        $info->onclick = "window.open('$fullurl', '', '$wh'); return false;";
    } 

    return $info;
}
/**
 * This function gets run whenever user is unenrolled from course
 *
 * @param stdClass $cp
 * @return void
 */
function lcms_user_unenrolled($cp) {
    global $DB;

    if ($cp->lastenrol) {
        $params = array('userid' => $cp->userid, 'courseid' => $cp->courseid);
        $lcmsselect = "IN (SELECT c.id FROM {lcms} c WHERE c.course = :courseid)";

        //$DB->delete_records_select('lcms_track', "userid = :userid AND lcms $lcmsselect", $params);
    }
}

function lcms_completion_state_updated($state) {
    /*
      stdClass::__set_state(array(
      'id' => 287,
      'coursemoduleid' => '1043',
      'userid' => '2',
      'completionstate' => 1,
      'viewed' => 1,
      'timemodified' => 1363683600,
      'course' => '67',
      'modname' => 'lcms',
      ))



      SELECT cm.id as cmid,
      cm.course as courseid,
      cm.module,
      cm.instance,
      m.name,
      cmc.userid,
      u.username,
      cmc.completionstate
      FROM m_course_modules cm
      JOIN m_modules m ON m.id = cm.module
      LEFT JOIN m_course_modules_completion cmc ON cmc.coursemoduleid = cm.id
      LEFT JOIN m_user u ON u.id = cmc.userid
      WHERE m.name IN ('quiz', 'lcms')
      AND cm.course = 67;
     */

    return true;
}

/* comment를 테이블에 저장한다.
 * @param stdClass $comment lcms_comment에 저장할 형태의 stdclass
 */

function lcms_comment_insert_instance($comment) {
    global $CFG, $DB;

    $comment->time = time();

    $comment->lcms_id = $DB->insert_record('lcms_comment', $comment);

    //$modcontext = get_context_instance(CONTEXT_MODULE, $lcms->coursemodule);

    return $lcms->id;
}

/*
 * 댓글의 submit 폼을 출력한다.
 * @param string $classname form class name 
 */

function lcms_comment_submit_form($classname = "comment_write") {
    $btname = get_string('submit_button', 'format_ocw');

    if (isloggedin() && $USER->username != 'guest') {
        $script = '<form class="' . $classname . '">
        <textarea name="textprint" id="reply" style="width:88%;height:88%;"> </textarea>
        <input type="button" class="search_btn" value="' . $btname . '" onclick="comment_submit()" flag="true" style="vertical-align:bottom;">
      </form>';
    } else {
        $script = '<p style="border:1px solid #ddd;padding:10px 0;text-align:center;">댓글은 로그인하셔야 등록하실 수 있습니다.</p>';
    }

    return $script;
}

/*
 * 댓글을 ajax로 보내는 스크립트
 * @param int $lcms_id
 * @param string $userid
 */

function lcms_comment_submit_script($course, $instance) {
    global $CFG;

    //$script = '<script src="http://wcetdesigns.com/assets/javascript/jquery.js"></script>';

    $script .= '<script>
                function comment_submit(){ 
                var flag = $(".comment_write .search_btn").attr("flag");
                var data = "reply="+$("#reply").val()+"&course=' . $course . '&instance=' . $instance . '";';
    $script .= ' if(flag=="true"){
                  $(".comment_write .search_btn").attr("flag", "false"); 
                $.ajax({
                type: "POST",
                url: "';
    $script .= $CFG->wwwroot . '/mod/lcms/comment_ajax.php", //POSTS FORM TO THIS FILE
                data: data,
                success: function(e){
                $("#reply").val("");
                   if($(".comment_table tr").length >=10){
                    $(".comment_table tr").last().remove();
                   }
                   $(".comment_table tr").first().before(e);
                   $(".comment_write .search_btn").attr("flag", "true");
                },
                 error:function(e){
                        alert(e.responseText);
                }';
    $script .= ' 
                });
                }
                }
                </script>';

    return $script;
}

/*
 * 댓글 목록을 검색해서 반환
 * @param int $lcms_id
 * @param int $page
 * @param int $limit
 * @param string $search
 * @param string $sort
 * @param string $fields
 * @param int $report_flag 
 */

function lcms_comment_select_instance($course, $page = 1, $limit = 10, $search = '', $sort = 'lc.time DESC', $fields = "lc.*, us.firstname, us.lastname") {
    global $DB;

    $sql = "SELECT " . $fields . " 
        FROM {lcms_comment} lc, {user}  us
        WHERE lc.userid = us.id and lc.course = :course ";

    if (!empty($search)) {
        $sql .= " AND name LIKE '%" . $search . "%'";
    }

    if (!empty($sort)) {
        $sql .= " ORDER BY " . $sort;
    }

    $offset = $limit * ($page - 1);

    if ($coments = $DB->get_records_sql($sql, array('course' => $course), $offset, $limit)) {
        return $coments;
    } else {
        return array();
    }

    return $comments;
}

/*
 * 댓글에 좋아요 갯수를 count
 * @param int $instance
 * @param string $rating  
 */

function lcms_coment_like_count($course, $instance, $rating = 'like', $mod = 'comment') {
    global $DB;

    $sql = "select count(*) from {like_content_info} ci
            join {like_ratings} lr on ci.id = lr.content_id
            where ci.mod_name = :mod_name and ci.course = :course and ci.instance = :instance and lr.rating = :rating";
    $like_count = $DB->count_records_sql($sql, array('course' => $course, 'instance' => $instance, 'mod_name' => $mod, 'rating' => $rating));

    return $like_count;
}

/*
 * 댓글 리스트를 뿌려주는 폼
 * @param array $comments
 */

function lcms_comment_list_form($comments) {

    $form = lcms_comment_submit_form();
    $form .= '<table cellpadding="0" cellspacing="0" class="comment_table"><tbody>';

    if (empty($comments)) {
        $form .="<tr></tr>";
    } else {
        foreach ($comments as $key => $comment) {

            $lc = lcms_coment_like_count($comment->course, $comment->id, "like", "comment");
            $dc = lcms_coment_like_count($comment->course, $comment->id, "dislike", "comment");


            $fullname = fullname($comment);
            $time = userdate($comment->time, '%Y-%m-%d %H:%M:%S');

            $form .= '<tr><td><p class="comment_author">' . $fullname . '<span class="comment_date">' . $time . '</span></p>';

            $form .= ' <ul class="comment_like">
                            <li class="good" id="ratings_comment_' . $comment->id . '" onclick="rate_comment($(this).attr(\'flag\'), ' . $comment->id . ')" flag="like" style="cursor: pointer;">&nbsp;&nbsp;' . $lc . '</li>
                            <li class="bad"  id="ratings_comment_' . $comment->id . '" onClick="rate_comment($(this).attr(\'flag\'), ' . $comment->id . ')" flag="dislike" style="cursor: pointer;">&nbsp;&nbsp;' . $dc . '</li>
                            <li class="alert">&nbsp;&nbsp;<a href="javascript:report_form(\'' . $comment->course . '\',\'0\', \'' . $comment->id . '\', \'comment\')">신고하기</a></li>
                       </ul>
                            <p class="comment">' . $comment->reply . '</p></td></tr>';
        }
    }

    $form .='</tbody></table>';

    return $form;
}

//스크립트 페이지 만들어 주는 폼
function lcms_print_paging_navbar_script($totalcount, $page = 1, $perpage = 10, $baseurl,$params = null, $maxdisplay = 18) {
    global $CFG;
    $pagelinks = array();
   
    $lastpage = 1;
    if($totalcount > 0) {
        $lastpage = ceil($totalcount / $perpage);
    }
   
    if($page > $lastpage) {
        $page = $lastpage;
    }
           
    if ($page > round(($maxdisplay/3)*2)) {
        $currpage = $page - round($maxdisplay/2);
        if($currpage > ($lastpage - $maxdisplay)) {
            if(($lastpage - $maxdisplay) > 0){
                $currpage = $lastpage - $maxdisplay;
            }
        }
    } else {
        $currpage = 1;
    }
   
   
   
    if($params == null) {
        $params = array();
    }
   
    $prevlink = '';
    if ($page > 1) {
        $params['page'] = 1;
        $prevlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/>', array('class'=>'next'));
//        $prevlink = '<span class="prev"><a href="'.$prevlink.'">&lt;</a></span>';
    } else {
        $prevlink = '<a href="#noclick" class="next"><img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/></a>';
        $prevlink = '<span class="prev"><a href="#noclick">&lt;</a></span>';
    }
   
    $nextlink = '';
     if ($page < $lastpage) {
        $params['page'] = $lastpage;
        $nextlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/>', array('class'=>'prev'));
 //       $nextlink = '<span class="next"><a href="#">&gt;</a></span>';
    } else {
        $nextlink = '<a href="#noclick" class="prev"><img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/></a>';
        $nextlink = '<span class="next"><a href="#noclick">&gt;</a></span>';
    }
   
   
    echo '<div class="pagination">';
   
    $pagelinks[] = $prevlink;
   
    if ($currpage > 1) {
        $params['page'] = 1;
        $firstlink = html_writer::link(new moodle_url($baseurl, $params), 1);
       
        $pagelinks[] = $firstlink;
        if($currpage > 2) {
            $pagelinks[] = '...';
        }
    }
   
    $displaycount = 0;
    while ($displaycount <= $maxdisplay and $currpage <= $lastpage) {
        if ($page == $currpage) {
            $pagelinks[] = '<strong>'.$currpage.'</strong>';
        } else {
            $params['page'] = $currpage;
            $pagelink = html_writer::link(new moodle_url($baseurl, $params), $currpage);
            $pagelinks[] = $pagelink;
        }
       
        $displaycount++;
        $currpage++;
    }
   
    if ($currpage - 1 < $lastpage) {
        $params['page'] = $lastpage;
        $lastlink = html_writer::link(new moodle_url($baseurl, $params), $lastpage);
       
        if($currpage != $lastpage) {
            $pagelinks[] = '...';
        }
        $pagelinks[] = $lastlink;
    }
   
    $pagelinks[] = $nextlink;
   
   
    echo implode('&nbsp;', $pagelinks);
   
    echo '</div>';
}

/*
 * page nav를 만들어 주는 폼
 */

function lcms_page_nav_form($url, $params, $total_pages, $current_page, $max_nav = 10) {
    $total_nav_pages = jino_get_total_pages($total_pages, $max_nav);
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }

    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = "page=";
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . "?page=";
    }

    echo '<div class="pagenum" >';
    if ($current_nav_page > 1) {
        echo '<a class="prev_" href="' . $url . (($current_nav_page - 2) * $max_nav + 1) . '"></a>';
    } else {
        echo '<a class="prev_" href="#"></a>';
    }
    if ($current_page > 1) {
        echo '<a class="prev" href="' . $url . ($current_page - 1) . '"></a>';
    } else {
        echo '<a class="prev" href="#"></a>';
    }
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<a href="#"><strong>' . $i . '<strong></a>';
        } else {
            echo '<a href="' . $url . '' . $i . '">' . $i . '</a>';
        }
    }
    if ($current_page < $total_pages) {
        echo '<a class="next" href="' . $url . ($current_page + 1) . '"></a>';
    } else {
        echo '<a class="next" href="#"></a>';
    }
    if ($current_nav_page < $total_nav_pages) {
        echo '<a class="next_" href="' . $url . ($current_nav_page * $max_nav + 1) . '"></a>';
    } else {
        echo '<a class="next_" href="#"></a>';
    }

    echo '</div>';
}

/*
 * 자막파일이 있을때는 자막파일을, 이미 들어가있는 자막이 있을때는 검색을 해서 lcms_id에 해당하는 자막을 array로 반환
 * @param int $lcms_id
 * @param string $path
 */

function lcms_caption_get_array($lcms_id = 1, $path) {
    global $DB, $USER;

    if (!$lcms = $DB->get_record('lcms_caption', array('lcms_id' => $lcms_id))) {

        $new_captions_ko = lcms_caption_file_array($path->kopath);
        $new_captions_en = lcms_caption_file_array($path->enpath);
        foreach ($new_captions_ko as $key => $caption) {

            $temp_caption = new stdClass();
            $temp_caption->lcms_id = $lcms_id;
            $temp_caption->stime = seconds_from_time($caption->startTime);
            $temp_caption->etime = seconds_from_time($caption->stopTime);
            $temp_caption->caption_ko = $caption->text;
            $temp_caption->caption_en = $new_captions_en[$key]->text;

            $temp_caption->userid = 0;

            $caption_id = $DB->insert_record('lcms_caption', $temp_caption);
        }
    }

    $sql = "SELECT * 
        FROM {lcms_caption} lc WHERE lc.lcms_id = " . $lcms_id . " ORDER BY lc.stime ASC ";

    $captions = $DB->get_records_sql($sql);

    return $captions;
}

/*
 * 00:00:00 형태의 시간을 초로 반환
 * @param string $time
 */

function seconds_from_time($time) {
    list($h, $m, $s) = explode(':', $time);
    return seconds_from_hms($h, $m, $s);
}

function seconds_from_hms($h, $m, $s) {
    return ($h * 3600) + ($m * 60) + $s;
}
/*
 * 초로 되어있는 것을 00:00:00 형티의 시간으로 반환
 * @param string $seconds
 */

function time_from_seconds($seconds) {
    $time = new stdclass();
    $time->h = sprintf('%02d', floor($seconds / 3600));
    $time->m = sprintf('%02d', floor(($seconds % 3600) / 60));
    $time->s = sprintf('%02d', $seconds - ($time->h * 3600) - ($time->m * 60));
    return $time;
}

function arrremove($index) {
    unset($this->arr[$index]);
    $f = array();
    $z = 0;
    for ($i=0; $i < $this->size()+1; $i++) {   
        if (isset($this->arr[$i]) ) {
        $f[$z++] = $this->arr[$i] ;
        } else {
    continue;
        }
    }
    $this->arr = $f;
}

/*
 * 자막파일을 읽어 array 형태로 바꿔서 반환
 * @param string $path 자막파일 경로
 */

function caption_get_array($file) {
    define('SRT_STATE_SUBNUMBER', 0);
    define('SRT_STATE_TIME', 1);
    define('SRT_STATE_TEXT', 2);
    define('SRT_STATE_BLANK', 3);
    
    $lines = file($file);
    unset($lines[0]);
    unset($lines[1]);
    $lines = array_values($lines);
    $total = count($lines);
    $numb = 0;    
    $subs = array();
    $state = SRT_STATE_SUBNUMBER;
    $subNum = 0;
    $subText = '';
    $subTime = '';

    foreach ($lines as $line) {

        $numb++;

        switch ($state) {
            case SRT_STATE_SUBNUMBER:
                $subNum = trim($line);
                $state = SRT_STATE_TIME;
                break;

            case SRT_STATE_TIME:
                $subTime = trim($line);
                $state = SRT_STATE_TEXT;
                break;

            case SRT_STATE_TEXT:
                if (trim($line) == '' || $numb == $total) {
                    if ($numb == $total)
                        $subText .= trim($line);
                    $sub = new stdClass;
                    $sub->number = $subNum;
                    list($sub->startTime, $sub->stopTime) = explode(' --> ', $subTime);
                    $sub->startTime = substr($sub->startTime, 0,8);
                    $sub->stopTime = substr($sub->stopTime, 0,8);
                    $sub->text = $subText;
                    $subText = '';
                    $state = SRT_STATE_SUBNUMBER;

                    $subs[] = $sub;
                } else {
                    $subText .= trim($line);
                }
                break;
        }
    }

    return $subs;
}

function lcms_caption_put_srt($contextid, $lcmsid, $itemid, $lang) {
    global $CFG;
    
    $fs = get_file_storage();
    
    $captions = $fs->get_area_files($contextid, 'mod_lcms', 'subtitle', $itemid);
   
    if(count($captions) > 0) {
        foreach($captions as $caption) {
            $captionname = $caption->get_filename();
            
            switch ($lang) {
                case 'ko':
                    $patten = CAPION_FILE_PATTEN_KO;
                    break;
                case 'en':
                    $patten = CAPION_FILE_PATTEN_EN;
                    break;
            }
            
            if(preg_match($patten, $captionname)){
                $fileinfo = array(
                    'contextid' => $contextid, // lcms context id
                    'component' => 'mod_lcms',     
                    'filearea' => 'subtitle',     
                    'itemid' => $itemid,             
                    'filepath' => '/',           
                    'filename' => $captionname); 
                
                $file = $fs->get_file($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'], 
                                      $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
                
                if ($file) {
                    $file->delete();
                }
                $srtstring = lcms_caption_to_srt($lcmsid, $lang);
                
                $test = $fs->create_file_from_string($fileinfo, $srtstring);
            }
        }
    }
}

function lcms_caption_to_srt($lcmsid, $lang) {
    global $DB;
    
    $captions = $DB->get_records('lcms_caption', array('lcms_id' => $lcmsid, 'lang' => $lang), ' stime, etime ASC');
    
    if(!empty($captions)) {
        $num = 0;
        $srt = '';
        foreach($captions as $caption) {
            $num++;
            $stime = time_from_seconds($caption->stime);
            $etime = time_from_seconds($caption->etime);
            $start = $stime->h.':'.$stime->m.':'.$stime->s.',000';
            $end = $etime->h.':'.$etime->m.':'.$etime->s.',000';
            $text = $caption->caption;

            $srt .= trim($num) . PHP_EOL;
            $srt .= trim($start) . ' --> ' . $end . PHP_EOL;
            $srt .= trim($text) . PHP_EOL. PHP_EOL ; 
        }
        
        return $srt;
    } else {
        return null; 
    }
    
}

function get_dir_size($path) {
    $result = explode("\t", exec("du -k -s " . $path), 2);
    return ($result[1] == $path ? $result[0] : "");
}

/*
 * 자막 겹치는 목록
 * @param string $lcms_id   activity id
 * @param string $caption_id    자막의 id
 * @param int $stime    새로 입력된 자막의 시작시간
 * @param string $lcms_id   새로 입력된 자막의 종료시간
 * @return stdclass 자신을 제외한 범위에 있는 자막 목록    
 */

function lcms_caption_overlap_list($lcms_id, $caption_id, $stime, $etime) {
    global $DB;
    $sql = "select * from {lcms_caption} where lcms_id = " . $lcms_id . " and  
            stime between " . $stime . " and " . $etime . " or etime between " . $stime . " and " . $etime . " ORDER BY id ASC ";

    $overlap_cap = $DB->get_records_sql($sql);

    //print_r($overlap_cap);
    $captions = array();
    foreach ($overlap_cap as $key => $caption) {
        if ($caption_id != $key)
            $captions[$key] = $caption;
    }
    return $captions;
}

function lcms_get_progress($lcmsid, $userid) {
    global $DB;

    $progress = 0;

    $track = $DB->get_record('lcms_track', array('lcms' => $lcmsid, 'userid' => $userid));

    $sql = "SELECT * FROM {lcms_playtime} 
        WHERE lcmsid = :lcmsid and userid = :userid  and positionfrom < positionto ";

    $params = array('lcmsid' => $lcmsid, 'userid' => $userid);
    $sql .= 'ORDER BY positionfrom ASC, positionto ASC';

    if ($details = $DB->get_records_sql($sql, $params)) {

        $start = -1;
        $end = -1;
        foreach ($details as $detail) {
            if ($start == -1) {
                $progress = ($detail->positionto - $detail->positionfrom);

                $start = $detail->positionfrom;
                $end = $detail->positionto;
            } else {
                if (($start <= $detail->positionfrom && $detail->positionfrom <= $end) && ($end <= $detail->positionto)) {
                    $progress += ($detail->positionto - $end);

                    $end = $detail->positionto;
                } else if ($end < $detail->positionfrom) {
                    $progress += ($detail->positionto - $detail->positionfrom);

                    $start = $detail->positionfrom;
                    $end = $detail->positionto;
                }
            }
        }
    }

    return $progress;
}

function lcms_get_progress_page($lcmsid, $userid) {
    global $DB;

    $sql = "SELECT positionpage FROM {lcms_playtime} 
        WHERE lcmsid = :lcmsid and userid = :userid group by positionpage";

    $params = array('lcmsid' => $lcmsid, 'userid' => $userid);
    $play = $DB->get_records_sql($sql, $params);
    $progress = sizeof($play);

    return $progress;
}

function lcms_get_progress_pageplaytime_culuration($details, $pageprogess = array()){
    
    foreach ($details as $detail) {
        //페이지별 진도율을 계산해서 페이지 총 재생시간과 동일하면 페이지 진도율이 100%인 것으로 치환
        if(!$pageprogress[$detail->positionpage]){
            $start = -1;
            $end = -1;
            $pageprogress[$detail->positionpage] = 0;
        }
        if ($start == -1) {
            $pageprogress[$detail->positionpage] = ($detail->positionto - $detail->positionfrom);

            $start = $detail->positionfrom;
            $end = $detail->positionto;
        } else {
            if (($start <= $detail->positionfrom && $detail->positionfrom <= $end) && ($end <= $detail->positionto)) {
                $pageprogress[$detail->positionpage] += ($detail->positionto - $end);

                $end = $detail->positionto;
            } else if ($end < $detail->positionfrom) {
                $pageprogress[$detail->positionpage] += ($detail->positionto - $detail->positionfrom);

                $start = $detail->positionfrom;
                $end = $detail->positionto;
            }
        }
    }
    
    return $pageprogress;
}

function lcms_get_progress_pageplaytime($lcmsid, $conid, $userid) {
    global $DB;
    
    $durations = $DB->get_records_menu('lcms_duration', array('contents'=>$conid, 'progress'=>1), '', 'page, duration');

    $sql = "SELECT * FROM {lcms_playtime} WHERE lcmsid = :lcmsid and userid = :userid  and positionfrom < positionto ";
    $params = array('lcmsid' => $lcmsid, 'userid' => $userid);
    $sql .= 'ORDER BY positionpage ASC, positionfrom ASC, positionto ASC';
    
    $progress = array('playpage'=>0,'playtime'=>0,'duration'=>0);
    $pageprogress = array();

    if ($details = $DB->get_records_sql($sql, $params)) {

        $pageprogress = lcms_get_progress_pageplaytime_culuration($details, $pageprogress);
        
        foreach($durations as $key=>$val){
            if($pageprogress[$key] == $val){
                $progress['playpage']++;
            }
            $progress['playtime'] += $pageprogress[$key];
            $progress['duration'] += $val;
        }
        
    }

    return $progress;

}

function lcms_get_contents_info($vid, $qua) {

    global $CFG, $DB;

    $obj = new StdClass();

    $xmlfile = $CFG->wwwroot . '/mod/lcms/video_info.php?vid=' . $vid . '&qua=' . $qua;
    $entry = simplexml_load_file($xmlfile);

    $obj->type = $entry->stream->type;
    $obj->title = $entry->stream->title;
    $obj->des = $entry->stream->des;
    $obj->teacher = $entry->stream->teacher;
    $obj->logoview = $entry->stream->logoview;
    $obj->author = $entry->stream->author;
    $obj->ccmark = $entry->stream->ccmark;
    $obj->duration = $entry->stream->duration;

    if ($obj->type == 'video') {

        $obj->quality = $entry->stream->quality;
        $obj->format = $entry->stream->format;
        $obj->server = $entry->stream->server;
        $obj->storage = $entry->stream->storage;
        $obj->lcmsname = $entry->stream->vodname;
    } else if ($obj->type == 'Flash') {

        $obj->startfile = $entry->stream->startfile;
        $obj->filepath = $entry->stream->filepath;
        $obj->rtype = $entry->stream->rtype;
        $obj->totalpage = $entry->stream->totalpage;
    } else if ($obj->type == 'Embed') {

        $obj->embed_type = $entry->stream->embed_type;
        $obj->embed_code = $entry->stream->embed_code;
    }

    return $obj;
}
function lcms_courseinfo_urls($url) {

    global $CFG, $DB;

    $obj = new StdClass();

    $xmlfile = $url;
    $entry = simplexml_load_file($xmlfile);

    $obj->type = $entry->stream->type;
    $obj->title = $entry->stream->title;
    $obj->des = $entry->stream->des;
    $obj->teacher = $entry->stream->teacher;
    $obj->logoview = $entry->stream->logoview;
    $obj->author = $entry->stream->author;
    $obj->ccmark = $entry->stream->ccmark;
    $obj->duration = $entry->stream->duration;

    if ($obj->type == 'video') {

        $obj->quality = $entry->stream->quality;
        $obj->format = $entry->stream->format;
        $obj->server = $entry->stream->server;
        $obj->storage = $entry->stream->storage;
        $obj->lcmsname = $entry->stream->vodname;
    } else if ($obj->type == 'Flash') {

        $obj->startfile = $entry->stream->startfile;
        $obj->filepath = $entry->stream->filepath;
        $obj->rtype = $entry->stream->rtype;
        $obj->totalpage = $entry->stream->totalpage;
    } else if ($obj->type == 'Embed') {

        $obj->embed_type = $entry->stream->embed_type;
        $obj->embed_code = $entry->stream->embed_code;
    }

    return $obj;
}

function lcms_user_complete($course, $user, $mod, $lcms) {
    global $DB;
    
    if($track = $DB->get_record('lcms_track', array('lcms'=>$lcms->id, 'userid'=>$user->id))) {
        //$contents = $DB->get_record('lcms_contents', array('id'=>$lcms->contents));
        echo "$track->progress/100";
    } else {
        print_string('neverseen', 'lcms');
    }
}

function lcms_get_completion_state ($course, $cm, $userid, $type) {
    global $DB;

    // Get LCMS details
    if (!($lcms=$DB->get_record('lcms',array('id'=>$cm->instance)))) {
        throw new Exception("Can't find LCMS {$cm->instance}");
    }

    $result = $type; // Default return value

    if ($lcms->completionprogress) {
        $value = false;
        if( $track = $DB->get_record('lcms_track', array('lcms'=>$lcms->id, 'userid'=>$userid)) ) {
            $value = ($track->progress >= $lcms->completionprogress);
        }
        
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }

    return $result;
}

function mod_lcms_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG, $DB;  
    
    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }
    
    if ($filearea !== 'subtitle') {
        // intro is handled automatically in pluginfile.php
        return false;
    }

    $itemid = array_shift($args); // ignore revision - designed to prevent caching problems only
    
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_lcms/$filearea/$itemid/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    send_stored_file($file, null, 0, $forcedownload, $options);
}

function lcms_capy_repository_caption($lcmsid, $conid, $contextid) {
    $fs = get_file_storage();
    
    $captions = $fs->get_area_files(1, 'local_repository', 'subtitle', $conid);
 
    if (count($captions) > 0) {
        foreach($captions as $caption) {
            $captionname = $caption->get_filename();
            if(preg_match(CAPION_FILE_PATTEN_KO, $captionname) || preg_match(CAPION_FILE_PATTEN_EN, $captionname)){
                $content = $caption->get_content();
                $fileinfo = array(
                    'contextid' => $contextid, // lcms context id
                    'component' => 'mod_lcms',     
                    'filearea' => 'subtitle',     
                    'itemid' => $conid,             
                    'filepath' => '/',           
                    'filename' => $captionname); 
                
                $fs->create_file_from_string($fileinfo, $content);
            }
        }
    }
}

/*자막파일 정보 가져오기
 * 
 * @param int $contextid    context id
 * @param int $dataid       itemid - lcms_contents의 id
 * 
 * @return string           srt 파일형식의 문자열 return
 * 
 */


function lcms_get_subtitle_list($contextid, $dataid) { 
    
    global $CFG, $OUTPUT;
    
    require_once($CFG->dirroot . "/lib/filelib.php");
    
    $fs = get_file_storage();
    $captions = $fs->get_area_files($contextid, 'mod_lcms', 'subtitle', $dataid, 'timemodified', false);
   
    $subtitles = new stdClass();
        
    if (count($captions) > 0) {
        $captionhtml = '<ul>';
        foreach ($captions as $caption) {
            $captionname = $caption->get_filename();
            $captiontype = $caption->get_mimetype();
            $captionicon = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($captiontype)) . '" class="icon" alt="' . $captiontype . '" />';
            $captionpath = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/mod_lcms/subtitle/' . $dataid . '/' . $captionname);          
            $captionhtml .= '<li>';
            $captionhtml .= '<a href="'.$captionpath.'">'.$captionicon.'</a> ';
            $captionhtml .= format_text('<a href="$captionpath">' . s($captionname) . '</a>', FORMAT_HTML, array('context' => $contextid));
            $captionhtml .= '<input type="button" value="'. get_string('captionpreview','lcms').'" id="captionpopup" onclick="caption_popup(\'' . $dataid . '\', \'' . $captionname . '\')">';
            $captionhtml .= '</li>';
        
        if(preg_match(CAPION_FILE_PATTEN_EN, $captionname)){
            $caption_en = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/'.$contextid.'/mod_lcms/subtitle/' .$dataid. '/' . $captionname);
        }
        if(preg_match(CAPION_FILE_PATTEN_KO, $captionname)){
            $caption_ko = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/'.$contextid.'/mod_lcms/subtitle/' .$dataid. '/' . $captionname);
        }
        }
        
        $captionhtml .= '</ul>';
        
    }
    
    $subtitles->list = $captionhtml;
    $subtitles->path_ko = $caption_ko;
    $subtitles->path_en = $caption_en;
    
    return $subtitles;
}

/*Youtube url을 입력받아 Youtube content id 값을 반환
 * 
 * @param string $url   Youtube Url
 * 
 * @return string       Youtube ID       
 * 
 */

function lcms_get_youtubeid($url) {
    $regExp = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
    preg_match($regExp, $url, $matches);
    return $matches[7];
}

function lcms_view($lcms, $course, $cm, $context) {
	// Trigger course_module_viewed event.
    $params = array(
        'context' => $context,
        'objectid' => $lcms->id
    );

    $event = \mod_lcms\event\course_module_viewed::create($params);
    $event->add_record_snapshot('course_modules', $cm);
    $event->add_record_snapshot('course', $course);
    $event->add_record_snapshot('lcms', $lcms);
    $event->trigger();

	// Completion.
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}