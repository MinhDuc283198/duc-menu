<?php
require_once '../../config.php';
require_once $CFG->libdir.'/completionlib.php';

require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once '../lcmsprogress/locallib.php';

$id = required_param('id', PARAM_INT);
$fileid = required_param('fileid', PARAM_INT);

$PAGE->set_url('/mod/lcms/download.php', array('id'=>$id,'fileid'=>$fileid));

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('course module is incorrect');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$PAGE->set_context($context);

$data = $DB->get_record('lcms_contents_file',array('id'=>$fileid));
$filepath = explode('/',$data->filepath);
if($filepath[0]=='lms'||$filepath[0]=='lcms'){
    $lcmsdata = '/lcmsdata/';
} else {
     $lcmsdata = '/';
}
$file = STORAGE2 . $lcmsdata . $data->filepath . '/' . $data->filename;
$filename = $data->fileoname;
$filesize = filesize($file);

if($track = $DB->get_record('lcms_track',array('lcms'=>$lcms->id,'userid'=>$USER->id))){
    $track->attempts = $track->attempts+1;
    $track->timeview = time();
    $DB->update_record('lcms_track',$track);
}else{        
    $track = new stdClass();
    $track->lcms = $lcms->id;
    $track->userid = $USER->id;
    $track->timeview = time();
    $track->attempts = 1;
    $track->progress = 0;
    $track->id = $DB->insert_record('lcms_track',$track);
}

//다운로드 받을경우 다운로드 받았으므로 기록 업데이트
//다운로드는 문서,기타 파일만 가능함
if($lcms->type=='word'&&$lcms->progress==1){
    
    $filecnt = $DB->count_records('lcms_contents_file',array('con_seq'=>$lcms->contents,'con_type'=>$lcms->type));
    
    //다운로드 받은 파일에 대한 진도율 업데이트
    if(!$play = $DB->get_record('lcms_playtime',array('lcmsid'=>$lcms->id,'userid'=>$USER->id,'positionpage'=>$fileid))){
        $play = new stdClass();
        $play->userid = $USER->id;
        $play->lcmsid = $lcms->id;
        $play->positionpage = $fileid;
        $play->positionevent = 6;
        $play->positionto = 0;
        $play->positionfrom = 0;
        $play->timecreated = time();
        $DB->insert_record('lcms_playtime',$play);
    }
       
    $track->playpage = lcms_get_progress_page($lcms->id,$USER->id);
    $track->progress = ceil($track->playpage/$filecnt*100);
    $DB->update_record('lcms_track',$track);
 
    lcmsprogress_update_progress_score($lcms->course, $USER->id);
    
    $completion = new completion_info($course);
    if($completion->is_enabled($cm)) {
        // Mark viewed if required
        $completion->set_module_viewed($cm);
        
        $complete = COMPLETION_INCOMPLETE;
        if($track->progress >= $lcms->completionprogress) {
            $complete = COMPLETION_COMPLETE;
        }
        $completion->update_state($cm, $complete);
    }
}

//IE인가 HTTP_USER_AGENT로 확인
$ie= isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false;
 
//IE인경우 한글파일명이 깨지는 경우를 방지하기 위한 코드
if( $ie ){
  $filename = iconv('utf-8', 'euc-kr', $filename);
}
 

// 접근경로 확인 
//if (!eregi($_SERVER['HTTP_HOST'], $_SERVER['HTTP_REFERER'])) Error("외부에서는 다운로드 받으실수 없습니다."); 

//기본 헤더 적용
$mimetype = "application/octet-stream";
header('Content-Type: '.$mimetype);
header('Content-Disposition: attachment; filename="'.$filename.'"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: '.sprintf('%d', $filesize));
header('Expires: 0');

// IE를 위한 헤더 적용
if( $ie ){
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
} else {
  header('Pragma: no-cache');
}

if (is_file($file)) { 
$fp = fopen($file, "r"); 
if (!fpassthru($fp)) 
    fclose($fp); 
}
?>
