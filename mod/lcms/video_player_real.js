$(function () {
    'use strict';
    

});

function videoplayer(id,server,storage,vodname,author,ccmark,logo,capture,caption_ko,caption_en,islock,xeno){
        
    //디바이스별 판별
    var _ua = window.navigator.userAgent.toLowerCase();
        
    var browser = {
        ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
        ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
        iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
        android : /webkit/.test( _ua )&&/android/.test( _ua ),
        msie : /msie/.test( _ua )
    };   
        
        
    //디바이스별 플레이어 사용
    if(browser.ipod || browser.iphone) {
        html5player(id,server,storage,vodname,author,ccmark,logo,capture,caption_ko,caption_en,islock,xeno);
    } else if(browser.ipad) {
        html5player(id,server,storage,vodname,author,ccmark,logo,capture,caption_ko,caption_en,islock,xeno);
    } else if(browser.android) {
        //var stream_url = 'rtsp://'+server+':1935/vod/_definst_/mp4:'+storage+'/'+vodname;
        var stream_url = server + '/' + storage + '/' + vodname;
        self.location = stream_url;
    } else {
        flashplayer(id,server,storage,vodname,author,ccmark,logo,capture,caption_ko,caption_en,islock,xeno);
    }
        
}
            
function html5player(id,server,storage,vodname,author,ccmark,logo,capture,caption_ko,caption_en,islock,xeno){
            
   // var stream_url = 'http://'+server+':1935/vod/_definst_/mp4:'+storage+'/'+vodname+'/playlist.m3u8';
   var stream_url = server + '/' + storage + '/' + vodname;
                
    $('#mediaplayer').css('height','auto');
    
    if(xeno != 1){
        capture = server + '/' + storage + '/' + capture;
    }
    $("#mediaplayer").html('<video preload controls autostart="true"'+
        'poster="'+capture+'" src="'+stream_url+'">'+
        '<track kind=captions label="Korean subtitles" src="'+caption_ko+'" lang="ko" default></track>'+
        '<track kind=captions label="English subtitles" src="'+caption_en+'" lang="en"></track>'+
        '</video>').show();

    if(islock==2) video_sync(id);
            
}
            
    
//jwplayer호출
function flashplayer(id,server,storage,vodname,author,ccmark,logo,capture,caption_ko,caption_en,islock,xeno){
    var caption = caption_ko+','+caption_en;
    if(xeno != 1){
        capture = server + '/' + storage + '/' + capture;
    }
    var server = '203.246.80.22';
    jwplayer('mediaplayer').setup({
        'flashplayer': 'player/swf/player.swf',
        'id': 'mediaplayer',
        'menu': 'false',
        'width': '100%',
        'height': '525px',
        'skin': 'player/skins/seven.css',
        'logo': { 'file': logo, 'hide': false, 'position': 'top-right' },
        'provider': 'rtmp',
        'streamer': 'rtmp://'+server+':1935/vod',
        'file': storage + '/' + vodname,
        //'image': capture,
        'plugins': { 
            'player/js/textoutput.js': {'text': ccmark},
            'captions-2': {
                        files: caption,
                        labels: 'Korean,English',
                        fontSize: '14',
                        fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                    }    
        },
                        
        'autostart': false
    });
                    
    if(islock==2) jw_sync(id);

}
 
    
function youtubeplayer(id,emb,author,ccmark,logo,capture,caption_ko,caption_en,islock){

    //디바이스별 판별
    var _ua = window.navigator.userAgent.toLowerCase();
        
    var browser = {
        ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
        ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
        iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
        android : /webkit/.test( _ua )&&/android/.test( _ua ),
        msie : /msie/.test( _ua )
    };   
        
        
    //디바이스별 플레이어 사용
    if(browser.ipod || browser.iphone || browser.ipad || browser.android) {
        var after_time = playtime_get(id);
        YouTube_Init("mediaplayer", "100%", "200", emb, after_time, id);
        
        //if(islock==2) video_sync(id);
        
    } else {
        
        var caption = caption_ko+','+caption_en;
        
        jwplayer('mediaplayer').setup({
            'flashplayer': 'player/swf/player.swf',
            'id': 'mediaplayer',
            'menu': 'false',
            'width': '100%',
            'height': '525px',
            'skin': 'player/skins/darkrv5.zip',
            'logo': { 'file': logo, 'hide': false, 'position': 'top-right' },
            'file': 'http://www.youtube.com/watch?v='+emb,
            'plugins': {
                'player/js/textoutput.js': {'text': ccmark},
                'captions-2': {
                        files: caption,
                        labels: 'Korean,English',
                        fontSize: '14',
                        fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                    }    
            },
            'autostart': false
        });
        
        if(islock==2) jw_sync(id);
        
    }           
    
}
            
function vimeoplayer(id,emb,author,ccmark,logo,capture,caption_ko,caption_en,islock){
                
               
    var videolist = '<iframe src="//player.vimeo.com/video/'+emb+'" width="100%" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        
    $('#mediaplayer').empty().append(videolist);
      
}
            
            
function video_sync(id){

    var positionfrom = $('#position').val(), positionto = 0, duration = 0, oldposition = 0, timestamps = [];
                
    var v = document.createElement('video');
    var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
    if ( supported == 'probably') { 
        
        positionto = playtime_get(id);
        positionfrom = positionto;
                    
        $('video').bind('play',function(event){
            positionfrom = positionto;
        });
                    
        $('video').bind('pause',function(event){
            if(positionfrom < positionto){
                playtime_update(id,positionfrom,positionto); 
            }
        });
                    
        $('video').bind('seeked',function(event){
            $('video').pause();
            $('video').play();
        });
                    
        $('video').bind('ended',function(event){
            playtime_update(id,positionfrom,positionto); 
            positionfrom = positionto;
        });
                    
   
        $('video').bind('timeupdate',function(event){
      
            positionto = parseInt(this.currentTime);
                        
            if(positionto > oldposition){
                caption_scroll();
            }
            
            oldposition = positionto;
      
        });
        
        $('.subtitle_area .subtitle_text').each(function(o){
                if($(this).attr('data-start')){
                    timestamps.push({
                        start : +$(this).attr('data-start'),
                        end : +$(this).attr('data-end'),
                        elm : $(this),
                        pos : $(this).position().top
                    });
                }
            });
        
        function caption_scroll(){
    
        $('.subtitle_area .subtitle_text').css({'background':'#fff'});
                
        //console.log(positionto);
      
        for(var i=0;i<timestamps.length;i++ ){
        if(positionto >= timestamps[i].start && positionto <= timestamps[i].end){ 
                        
            timestamps[i].elm.css({'background':'#eee'});
                        
            var position = timestamps[i].pos;
                        
            $('.subtitle_area').scrollTop(position);
                         
        }
        }
                         
    }
            
    }

}
    
function jw_sync(id){
                
    var positionfrom = 0, positionto = 0, saveposition = 0, oldposition = 0, duration = 0, timestamps = [], timer;
    
    /* JWPlayer 상태 변화 */
    jwplayer('mediaplayer').onTime(function(event) {
        positionto = parseInt(event.position);
        duration = parseInt(event.duration);
        
        if(positionfrom < positionto && positionto > saveposition + 300){
           playtime_update(id,positionfrom,positionto,duration);
           saveposition = positionto;
           positionfrom = positionto;
        }
        
        if(positionto > oldposition){
            caption_scroll();
            oldposition = positionto;
        }
        
    });
    
    positionto = playtime_get(id);
    
    jwplayer('mediaplayer').onPlay(function() {
        positionfrom = positionto;
    });
    jwplayer('mediaplayer').onSeek(function(event) {
        if(positionfrom < positionto){
            playtime_update(id,positionfrom,positionto,duration);
        }
        positionfrom = event.offset;
        positionto = event.offset;
    });
    jwplayer('mediaplayer').onPause(function() {
        if(positionfrom < positionto){
           playtime_update(id,positionfrom,positionto,duration); 
        }
        
    }); 
    jwplayer('mediaplayer').onBuffer(function() {
        console.log(positionfrom+':'+positionto);
        if(positionfrom < positionto){
            playtime_update(id,positionfrom,positionto,duration); 
        }
        playupdate = setInterval(function() {
            console.log(positionfrom+':'+positionto);
            if(positionfrom < positionto){
               playtime_update(id,positionfrom,positionto,duration); 
            }
        }, 5000);
    });
    jwplayer('mediaplayer').onIdle(function() {
        if(positionfrom < positionto){
            playtime_update(id,positionfrom,positionto,duration);
        }
    });
    
    $('.subtitle_area .subtitle_text').each(function(o){
        if($(this).attr('data-start')){
            timestamps.push({
                start : +$(this).attr('data-start'),
                end : +$(this).attr('data-end'),
                elm : $(this),
                pos : $(this).position().top
            });
        }
    });
    
    
    function caption_scroll(){
    
    $('.subtitle_area .subtitle_text').css({'background':'#fff'});
                
    //console.log(positionto);
      
    for(var i=0;i<timestamps.length;i++ ){
        if(positionto >= timestamps[i].start && positionto <= timestamps[i].end){ 
                        
            timestamps[i].elm.css({'background':'#eee'});
                        
            var position = timestamps[i].pos;
                        
            $('.subtitle_area').scrollTop(position);
                         
        }
    }
    
    }
    
}

function playtime_update(id,posfrom,posto,duration,second){
    var status = true;
    if(!second) second = 0;
        
    $.ajax({
        url: 'playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: true,
        data: {
            act : 'update',
            positionpage : 1,
            positionto : posto,
            positionfrom : posfrom,
            duration : duration,
            lcmsid : id  
        },
        success: function(data) {
            if(data.status == 'success') {
                status = true;
                $('#study_last_time').text(data.last);
                $('#study_total_time').text(data.totaltime);
                $('#study_rate').text(data.progress);
            } else {
                status = false;
            }
        },
        error: function(e) {
            
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
            //playtime_update(id,posfrom,posto,duration,second);
            status = false;
        }
    });
        
    return status;
}

function playtime_get(id){
    
    var frm = $(top.document).find('#playtime_form');
    frm.find('input[name=act]').val('get');
        
    var positionfrom = 0;
        
    $.ajax({
        url: 'playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            act : 'get',
            lcmsid : id
        },
        success: function(data) {
            if(data.status == 'success') {
                
                if(data.positionto>0&&data.progress<100){
                    //이전학습부터 이어보시겠습니까 메시지 제외하고 바로 이어보기함. 2016-12-13 정민정
                    //var conf = confirm('이전 학습 부분부터 이어보시겠습니까?');
                    //if(conf){
                        if($('video').length>0){
                            var v = document.createElement('video');
                            var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
                            if ( supported == 'probably') { 
                                $('#position').val(data.positionto);
                            }
                        }else{
                            jwplayer('mediaplayer').seek(data.positionto);
                            positionfrom = data.positionto;
                            return positionfrom;
                        }
                                    
                   // }
                }
                
            } else {
                //alert(data.message);
            }
        },
        error: function(e) {
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
        }
    });
        
    return positionfrom;
    
}

