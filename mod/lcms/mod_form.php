<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot . '/mod/lcms/lib.php');
require_once($CFG->dirroot . '/mod/lcms/locallib.php');
require_once($CFG->dirroot . '/local/repository/lib.php');

$LCFG = new stdClass();
$LCFG->allowexthtml = array('zip', 'html');
$LCFG->allowextword = array('hwp','mp4', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf');
$LCFG->allowextvideo = array('mp4');
$LCFG->allowextref = array('hwp', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf', 'mp4', 'mp3', 'wmv');
$LCFG->notallowfile = array('sh','exe','js','php','sql','jsp','asp','cgi','php3','php4','php5','unknown');
class mod_lcms_mod_form extends moodleform_mod {

    function definition() {
        global $CFG, $DB, $OUTPUT, $PAGE, $USER , $COURSE;

        $mform = & $this->_form;
        $PAGE->requires->jquery();
        $PAGE->requires->jquery_plugin('ui');
        $PAGE->requires->jquery_plugin('ui-css');
        $PAGE->requires->js_init_call('M.mod_lcms.init_edit_form', array($CFG->wwwroot, $mform->_attributes['id']), false, lcms_get_js_module());

        //-------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name', 'lcms'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');

        $this->add_intro_editor(true, get_string('lcmsintro', 'lcms'));


        //-------------------------------------------------------
        
        /* REPOSITORY  */

        $repositoryid = optional_param('repositoryid', 0, PARAM_INT);
        $update = optional_param('update', "", PARAM_RAW);
        $add = optional_param('add', "", PARAM_RAW);
         
        $context = context_system::instance();

        $mform->addElement('header', 'lcms_content_search', get_string('contentheader', 'lcms'));
        $mform->setExpanded('lcms_content_search', true);

        $mform->addElement('static', 'titlestatic', '<span color="#ed5565">'.get_string('content', 'lcms').'</span>'
                . '<img class="req" src="'.$CFG->wwwroot.'/theme/image.php/'.$PAGE->theme->name.'/core/1445922315/req" alt="필수 항목" title="필수 항목">',
                '<input size="58" readonly="readonly" name="titlestatic" type="text"/> '
                . '<input id="mod_lcms_btn_select" type="button" value="' . get_string('findcontent', 'lcms') . '" onclick="javascript:M.mod_lcms.show2('.$COURSE->id.');">'
                );
        //$mform->addRule('titlestatic',null, 'required', null);
        
        $mform->addElement('hidden','totalpage');
        $mform->setDefault('totalpage', 0);
        $mform->addElement('select','popup',get_string('popup', 'lcms'),array('1'=>get_string('popup','lcms'),'0'=>get_string('link','lcms')));
        
        if ($repositoryid != 0) {
            $mform->addElement('hidden', 'id');
            $mform->setType('id', PARAM_INT);
            $mform->setDefault('id', $repositoryid);
        }

        $mform->addElement('hidden', 'con_id');
        $mform->setType('con_id', PARAM_INT);

        $mform->addElement('hidden', 'ref');
        $mform->setType('ref', PARAM_INT);
        $mform->setDefault('ref', 0);

        $mform->addElement('hidden', 'mode');
        $mform->setType('mode', PARAM_CLEAN);
         if (!empty($update)) {
            $mform->setDefault('mode', 'edit');
        } else if(!empty($add)){
             $mform->setDefault('mode', 'write');
        }
        
        /* 기간설정 */
        /*기간설정 제외하고 모두 진도 체크가 가능하게 수정
        $mform->addElement('header', 'content', get_string('timeset', 'lcms'));
        $mform->setExpanded('content', true);

        $starttime = usertime(time());
        $mform->addElement('date_time_selector', 'timestart', get_string('starttime', 'lcms'));
        $mform->addElement('date_time_selector', 'timeend', get_string('endtime', 'lcms'));
        $mform->setDefault('timeend', strtotime('+7 day'));
        
        $options = array(1=>get_string('islock_y', 'lcms'),2=>get_string('islock_e', 'lcms'));
        $mform->addElement('select','islock',get_string('islock', 'lcms'),$options);
        $mform->setDefault('islock', 1);
        */
        // $mform->addElement('advcheckbox', 'progress', get_string('progress', 'lcms'), get_string('islcmsprogress','lcms'), array('group' => 1), array(0, 1));
        //$mform->setDefault('progress', 1);
        //$contents = $
        // static인 것은 전송이 안되서 hidden으로 추가함
        $mform->addElement('hidden', 'contents');
        $mform->addElement('hidden', 'title');
        $mform->addElement('hidden', 'type');
        $mform->addElement('hidden', 'progress');
        $mform->setDefault('progress', 1);

        //-------------------------------------------------------
        $this->standard_coursemodule_elements();
        //-------------------------------------------------------
        $this->add_action_buttons();
        
        $mform->addElement('html','<script src="/local/repository/contents_upload/js/vendor/jquery.ui.widget.js"></script>');
        $mform->addElement('html','<script src="/local/repository/contents_upload/js/jquery.fileupload.js"></script>');
        $mform->addElement('html','<script src="/local/repository/contents_upload/js/bootstrap.min.js"></script>');
        $mform->addElement('html','<script src="/mod/lcms/lcms.js"></script>');
    }

    public function data_preprocessing(&$data) {
        global $DB;
        $data = (array) $data;
        parent::data_preprocessing($data);
        // Set filterid from filter.
        $contents = (!empty($data['contents']))?$data['contents']:0;
        $con = $DB->get_record('lcms_contents', array('id' => $contents));
        $data['title'] = $con->con_name;
        $data['emb_code'] = $con->embed_code;
        
        $data['completionprogressenabled']=
            !empty($data['completionprogress']) ? 1 : 0;
        if (empty($data['completionprogress'])) {
            $data['completionprogress'] = 100;
        }
        if ($this->current->instance and !$this->current->tobemigrated) {
            $draftitemid = file_get_submitted_draft_itemid('subfiles');
            $context = context_system::instance();
            file_prepare_draft_area($draftitemid, $context->id, 'local_repository', 'subtitle', $contents, array('subdirs'=>true));
            $data['subfiles'] = $draftitemid;
        }
        
        $cfiles = $DB->get_records('lcms_contents_file', array('con_seq' => $con->id));
        $cfilelist = '<ul class="cfiles">';
        foreach ($cfiles as $cf) {
            $filename = $cf->fileoname;
            $filesize = filesize($filepath);
            $cfilelist .= '<li>';
           if ($con->con_type == 'word' || $con->con_type == 'ref')
               $cfilelist .= '<button type="button" class="btn btn-delete delete" data-type="DELETE" data-url="' . $cf->id . '">del</button>';
           $cfilelist .= $filename . '</li>';
        }
        $cfilelist .= '</ul>';
        $data['fileinfo'] = $cfilelist;
    }

    public function set_data($data) {
        global $DB;
        if (!empty($data->contents)) {
            $sql = 'select * from {lcms_contents} con where con.id = :contentid';
            $lcms_content = $DB->get_record_sql($sql, array('contentid' => $data->contents));
            unset($lcms_content->id);
            foreach ($lcms_content as $key => $value) {
                $data->$key = $value;
            }
            if ($groupid = $DB->get_field('lcms_repository', 'groupid', array('lcmsid' => $data->contents))) {
                $data->groupid = $groupid;
            }
        }
        $this->data_preprocessing($data);
        parent::set_data($data);
    }

    public static function editor_options($context) {
        global $PAGE, $CFG;
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'local_repository', 'files', 0)
        );
    }

    function add_completion_rules() {
        $mform =& $this->_form;
        
        $group=array();
        $group[] =& $mform->createElement('checkbox', 'completionprogressenabled', '', get_string('completionprogress','lcms'));
        $group[] =& $mform->createElement('text', 'completionprogress', '', array('size'=>3));
        $mform->setType('completionprogress',PARAM_INT);
        $mform->addGroup($group, 'completionprogressgroup', get_string('completionprogressgroup','lcms'), array(' '), false);
        $mform->disabledIf('completionprogress','completionprogressenabled','notchecked');
        
        return array('completionprogressgroup','completionrepliesgroup','completionpostsgroup');
    }
    
    function completion_rule_enabled($data) {
        return (!empty($data['completionprogressenabled']) && $data['completionprogress']!=0);
    }
    
    function get_data() {
        $data = parent::get_data();
        if (!$data) {
            return false;
        }
        // Turn off completion settings if the checkboxes aren't ticked
        if (!empty($data->completionunlocked)) {
            $autocompletion = !empty($data->completion) && $data->completion==COMPLETION_TRACKING_AUTOMATIC;
            if (empty($data->completionprogressenabled) || !$autocompletion) {
                $data->completionprogress = 0;
            }
        }
        return $data;
    }
    
    function validation($data, $files) {
        global $LCFG;
        $errors = parent::validation($data, $files);
        
         $mode = $data['mode'];

            if(!$data['title']){
                        $errors['titlestatic'] = get_string('error:notselectfile','lcms');
            }

        
        if ($data['completion']) {
            if ($data['completionprogress'] < 1 || $data['completionprogress'] > 100) {
                $errors['completionprogressgroup'] = get_string('error:completionprogress','lcms');
            }
        }
        
        return $errors;
    }
        public static function attachment_options() {

        return array(
            'subdirs' => 0,
            'maxfiles' => 2,
            'accepted_types' => array('.srt'),
            'return_types' => FILE_INTERNAL
        );
    }

}

?>
