M.mod_lcms = M.mod_lcms || {};

M.mod_lcms.init_edit_form = function(Y, wwwroot, formid) {
    this.wwwroot = wwwroot;
    this.formid = formid;
    var mform = document.getElementById(this.formid);
    mform.titlestatic.value = mform.title.value;
}

M.mod_lcms.show = function (id) {
    var divWidth = 1010;
    var divHeight = 730;
    
    var viewSize = M.mod_lcms.viewport();
    
    var divX = (viewSize.width - divWidth) / 2;
    if(divX < 0) divX = 0;
    
    var divY = (viewSize.height - divHeight) / 2;
    if(divY < 0) divY = 0;
    
    var vscroll = M.mod_lcms.scrollTop();
        
    var contentsDiv = document.createElement("div");
    contentsDiv.style.width = divWidth + "px";
    contentsDiv.style.height = divHeight + "px";
    contentsDiv.style.background = "white";
    contentsDiv.style.color = "black";
    contentsDiv.style.position = "absolute";
    contentsDiv.style.border = "1px solid #ccc";
    contentsDiv.style.padding = "0px";
    contentsDiv.style.zIndex = "99999";
    contentsDiv.innerHTML = "<div style='height: 30px; line-height: 30px;border-bottom: 1px solid #ccc;background:#182c42;'>\n\
<span style='cursor: pointer;float:right;margin-right: 10px;color:#fff;' onclick='M.mod_lcms.close()'>" + M.util.get_string('close', 'lcms') + "</span>\n\
</div>\n\
<div style='text-align: center; padding: 3px;'>\n\
<iframe src='" + this.wwwroot + "/mod/lcms/contents.php?id="+id+"' style='width: 998px; height: 680px; border: 0px; margin: 0px; padding: 0px;' />\n\
</div>";
    contentsDiv.setAttribute("id", "mod_lcms_contents");
    contentsDiv.style.visibility= "visible";
    contentsDiv.style.left = divX + "px";
    contentsDiv.style.top = (vscroll + divY) + "px";
    document.body.appendChild(contentsDiv);
}

M.mod_lcms.show2 = function (id) {    
    var url = this.wwwroot + "/mod/lcms/htmls.php?id="+id+"&formid="+this.formid; 
    var popOption = "width=1000, height=641, resizable=no, scrollbars=no, status=no;";
    window.open(url,'htmlSelector',popOption);
}

M.mod_lcms.close = function() {
    var contentsDiv = document.getElementById("mod_lcms_contents");
    if (contentsDiv != null) {
        document.body.removeChild(contentsDiv);
        contentsDiv = null;
    }
}

M.mod_lcms.position = function (obj) {
    var curleft = 0;
    var curtop = 0;
    
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return {X:curleft,Y:curtop};
    }
}

M.mod_lcms.viewport = function () {
    var myWidth = 0;
    var myHeight = 0;

    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if( document.documentElement &&
            ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    return {"width": myWidth, "height": myHeight}
}

M.mod_lcms.documentheight = function () {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

M.mod_lcms.select = function (content) {
    var mform = document.getElementById(this.formid);
    
    mform.titlestatic.value = content.title;
    mform.title.value = content.title;
    mform.contents.value = content.contents;
    mform.type.value = content.type;
}

M.mod_lcms.scrollTop = function () {
	return M.mod_lcms.filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
}

M.mod_lcms.filterResults = function (n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

M.mod_lcms.search_embed_contents = function () {
    var type = $('select[name=emb_type] option:selected').val(), type_txt = '';
    type_txt = (type == 'youtube') ? 'Youtube' : 'Vimeo';
    var title = type_txt;
    var dir = '/local/repository/ajax/search_embed_' + type + '.php';
    var search = $('input[name=emb_search]').val(), type_txt = '';
    var tag = $("<div id='load_form'></div>");
    $.ajax({
        url: dir,
        data: {
            search: search
        },
        success: function (data) {

                $('body').css({'overflow': 'hidden'});
                tag.html(data).dialog({
                    title: 'Youtube',
                    modal: true,
                    width: 670,
                    height: 650,
                    buttons: [{
                            id: 'close',
                            text: 'Close',
                            disable: true,
                            click: function () {
                                $('body').css({'overflow': 'auto'});
                                $('#load_form').remove();
                                $(this).dialog("close");
                            }
                        }]
                }).dialog('open');
                $(".ui-dialog-titlebar-close").click(function () {
                    $('body').css({'overflow': 'auto'});
                    $('#load_form').remove();
                });
        }

    });
}
