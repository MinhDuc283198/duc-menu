<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017112100;       // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2012061700;    // Requires this Moodle version
$plugin->component = 'mod_lcms';      // Full name of the plugin (used for diagnostics)
$plugin->cron      = 0;