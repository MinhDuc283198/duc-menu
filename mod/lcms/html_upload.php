<?php 
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$id = optional_param('id', 0, PARAM_INT);
$formid = optional_param('formid', '', PARAM_RAW);
$category = optional_param('category', '', PARAM_RAW);
$ccate = 0;
$pcate = 0;
//$corp = $DB->get_field('lmsdata_user','corporationcode',array('userid'=>$USER->id));
//$pcate = $DB->get_record('lmsdata_categories',array('codevalue'=>$ccate->highcode,'corporationcode'=>$corp));
?>  
<form id="form1" method="post" action="html_submit.php?id=<?php echo $id; ?>&formid=<?php echo $formid; ?>" enctype="multipart/form-data">
    <input type="hidden" value="html" name="con_type">
    <input type="hidden" value="0" name="groupid">
    <input type="hidden" value="<?php echo $pcate->id; ?>" name="area_cd">
    <input type="hidden" value="<?php echo $ccate->id; ?>" name="major_cd">
    <p><h2 style="padding-bottom: 10px;"><?php echo get_string('add_contents','lcms');?></h2></p> 
    <p class="files"><input id="fileupload" type="file" id="htmlfile" name="file_html"></p>
    <p><label for="contnm"><?php echo get_string('contentname','local_lmsdata'); ?><span class="red">*</span></label> <input type="text" id="contnm" name="con_name"></p>
    <p><label for="share_yn"><?php echo get_string('shareornot','local_lmsdata'); ?><span class="red">*</span></label> <select id="share_yn" name="share_yn"><option value="Y"><?php echo get_string('yes'); ?></option><option value="N"><?php echo get_string('no'); ?></option></select></p>
    <p><label for="strtfilenm"><?php echo get_string('startfilename','local_lmsdata'); ?><span class="red">*</span></label> <input type="text" name="baseurl" id="strtfilenm" value="index.html"></p>
    <p><label for="totalpage"><?php echo get_string('totalpagecount','local_lmsdata'); ?><span class="red">*</span></label> <input type="text" id="totalpage" name="totalpage" value="10"></p>
    <p><label for="popupwidth"><?php echo get_string('edupagesize','local_lmsdata'); ?><span class="red">*</span></label> <input type="text" id="popupwidth" name="popupwidth" value="1024">*<input type="text" id="popupheight" name="popupheight" value="768" /></p>
<input type="submit" class="edit button_style01 gray" value="<?php echo get_string('contentupload','local_lmsdata'); ?>">
</form>
<div id="loading" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1200; background-color:#757b8c; opacity:0.7; display:none;">
    <div style="position: absolute; top: 50%; left: 47%;"><img src="loading.gif"></div>
</div>
<script>
    $('#form1').submit(function (){ 
        if($('#htmlfile').val() == ''){
            alert('<?php echo get_string('selectfile','local_lmsdata'); ?>.');
            return false;
        }
        if($('#contnm').val() == ''){
            alert('<?php echo get_string('inputcontentname','local_lmsdata'); ?>.');
            $('#contnm').focus();
            return false;
        }
        if($('#strtfilenm').val() == ''){
            alert('<?php echo get_string('inputstartfilename','local_lmsdata'); ?>.');
            $('#strtfilenm').focus();
            return false;
        }
        if($('#totalpage').val() == ''){
            alert('<?php echo get_string('inputtotalpage','local_lmsdata'); ?>.');
            $('#totalpage').focus();
            return false;
        }
        if($('#popupwidth').val() == ''){
            alert('<?php echo get_string('popupwidth','local_lmsdata'); ?>.');
            $('#popupwidth').focus();
            return false;
        }
        if($('#popupheight').val() == ''){
            alert('<?php echo get_string('popupheight','local_lmsdata'); ?>.');
            $('#popupheight').focus();
            return false;
        }
        $('#loading').show();
        return true;
    });
</script>

