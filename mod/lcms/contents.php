<?php
require_once '../../config.php';
require_once($CFG->dirroot . "/lib/filelib.php");
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once '../../enrol/locallib.php';

$id = optional_param('id', 0, PARAM_INT); // courseid
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$groupid = optional_param('groupid', 0, PARAM_INT);

$PAGE->set_url('/mod/lcms/video_player.php', array('id' => $id));
$PAGE->set_pagelayout('embedded');

require_login();

echo $OUTPUT->header();


$course = get_course($id);
$teacher_sql = "select u.*
                                    from {course} c 
                                    join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
                                    join {role_assignments} ra on ra.contextid = ct.id 
                                    join {user} u on u.id = ra.userid  and u.id = :userid 
                                    join {role} ro on ro.id = ra.roleid and (ro.shortname = 'teacher')
                                    where c.id = :courseid";
$teacher = $DB->get_record_sql($teacher_sql, array('courseid' => $id, 'userid' => $USER->id));

$professors_sql = "select u.*
                                    from {course} c 
                                    join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
                                    join {role_assignments} ra on ra.contextid = ct.id 
                                    join {user} u on u.id = ra.userid  
                                    join {role} ro on ro.id = ra.roleid and (ro.shortname = 'editingteacher')
                                    where c.id = :courseid";
$professors = $DB->get_records_sql($professors_sql, array('courseid' => $id));
$where_in = "(";
foreach ($professors as $professor) {
    $where_in .= $professor->id . ",";
}
$where_in = rtrim($where_in, ",");
$where_in .= ")";

$allow_teacher = $DB->get_record('lcms_repository_open', array('course' => $id));

?>
<h3><?php echo get_string('findcontent', 'lcms');?></h3>
<form class="table-search-option" method="get">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
    <select name="groupid" onchange="this.form.submit();">
        <option><?php echo get_string('all', 'local_repository'); ?></option>
        <?php
        $groups = $DB->get_records("lcms_repository_groups", array('userid' => $USER->id));
        foreach ($groups as $group) {
            ?>
            <option <?php if ($groupid == $group->id) echo "selected"; ?> value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
        <?php } ?>
    </select>
    <input type="text" name="search" value="<?php echo $search; ?>" class="w-50" placeholder="<?php echo get_string('search', 'local_repository'); ?>">
    <input type="submit" class="board-search" value="<?php echo get_string('search', 'local_repository'); ?>" onclick='' />
</form> <!-- Search Area End -->
<table cellpadding="0" cellspacing="0" class="generaltable">
    <thead>
        <tr>
            <th width="6%"><?php echo get_string('list:no', 'local_repository'); ?></th>
            <th width="15%"><?php echo get_string('list:groupname', 'local_repository'); ?></th>
            <th><?php echo get_string('list:title', 'local_repository'); ?></th>
            <th width="10%"><?php echo get_string('list:type', 'local_repository'); ?></th>
            <th width="10%"><?php echo get_string('list:reference', 'local_repository'); ?></th>
            <th width="10%"><?php echo get_string('list:timecreated', 'local_repository'); ?></th>
            <th width="8%"><?php echo get_string('selectcontent', 'lcms'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        // 조교가 비어있고 오픈 노이면 
        if (empty($teacher->id)) {
            $sql = "select "
                    . "lr.id as lrid,lrg.name as gname,lc.id, lc.share_yn,lc.con_type,lc.con_name,lc.update_dt,lr.referencecnt "
                    . "from "
                    . "{lcms_repository} lr "
                    . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
                    . "join {lcms_contents} lc on lc.id=lr.lcmsid and lc.course_cd = :luid ";
            $count_sql = "select "
                    . "count(lr.id) "
                    . "from {lcms_repository} lr "
                    . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
                    . "join {lcms_contents} lc on lc.id=lr.lcmsid and lc.course_cd = :luid ";
        } else if ($allow_teacher->isopen == "Y") {
            $sql = "select "
                    . "lr.id as lrid,lrg.name as gname,lc.id, lc.share_yn,lc.con_type,lc.con_name,lc.update_dt,lr.referencecnt "
                    . "from {lcms_repository} lr "
                    . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
                    . "join {lcms_contents} lc on lc.id=lr.lcmsid and lc.course_cd in " . $where_in;
            $count_sql = "select "
                    . "count(lr.id) "
                    . "from {lcms_repository} lr "
                    . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
                    . "join {lcms_contents} lc on lc.id=lr.lcmsid and lc.course_cd in " . $where_in;
        } else {
            $sql = "select "
                    . "lr.id as lrid,lrg.name as gname,lc.id, lc.share_yn,lc.con_type,lc.con_name,lc.update_dt,lr.referencecnt "
                    . "from "
                    . "{lcms_repository} lr "
                    . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
                    . "join {lcms_contents} lc on lc.id=lr.lcmsid and lc.course_cd = :luid ";
            $count_sql = "select "
                    . "count(lr.id) "
                    . "from "
                    . "{lcms_repository} lr "
                    . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
                    . "join {lcms_contents} lc on lc.id=lr.lcmsid and lc.course_cd = :luid ";
        }
        // or lc.share_yn = :share_yn , 'share_yn' => 'L'
        $params = array('luid' => $USER->id);
        $sort = "order by lc.update_dt desc";
        $sql_where = "";
        if ($search) {

            $conditionname[] = $DB->sql_like('lc.con_name', ':search1', false);
            $conditionname[] = $DB->sql_like('lrg.name', ':search2', false);
            $conditionname[] = $DB->sql_like('lc.con_tag', ':search3', false);

            $params['search1'] = '%' . $search . '%';
            $params['search2'] = '%' . $search . '%';
            $params['search3'] = '%' . $search . '%';

            $sql_where .= ' AND (' . implode(' OR ', $conditionname) . ')';
        }
        $offset = 0;
        if ($page != 0) {
            $offset = ($page - 1) * $perpage;
        }
        if ($groupid) {
            $conditionname[] = $DB->sql_like('lrg.id', ':groupid', false);
            $params['groupid'] = $groupid;
            $sql_where .= ' AND (' . implode(' OR ', $conditionname) . ')';
        }

        $totalcount = $DB->count_records_sql($count_sql . $sql_where, $params);
        $total_pages = repository_get_total_pages($totalcount, $perpage);

        $files = $DB->get_records_sql($sql . $sql_where . $sort, $params, $offset, $perpage);

        $context = context_system::instance();

        $num = $totalcount - $offset;
        foreach ($files as $file) {
            if (!$file->gname)
                $file->gname = '-';
            if ($file->con_type == 'word')
                $type_txt = get_string('document', 'local_repository');
            elseif ($file->con_type == 'html')
                $type_txt = get_string('html', 'local_repository');
            elseif ($file->con_type == 'video')
                $type_txt = get_string('video', 'local_repository');
            elseif ($file->con_type == 'embed')
                $type_txt = get_string('embed', 'local_repository');
            ?>
            <tr>
                <td><?php echo $num; ?></td>
                <td><?php echo $file->gname; ?></td>
                <td class="title"><?php echo $file->con_name; ?></td>
                <td><?php echo $type_txt; ?></td>
                <td><?php echo $file->referencecnt; ?></td>
                <?php
                $public = (!$file->share_yn) ? get_string('n', 'local_repository') : get_string('y', 'local_repository');
                ?>
                <td class="number"><?php echo date('Y.m.d', $file->update_dt) ?></td>
                <td class="number"><input type="button" value="<?php echo get_string('selectcontent', 'lcms'); ?>" onclick="select_file('<?php echo $file->con_name; ?>', '<?php echo $file->id ?>', '<?php echo $file->con_type ?>')"></td>
            </tr>
            <?php
            $num--;
        }

        if (!$files) {
            echo '<tr><td colspan="8">' . get_string('nocontents', 'local_repository') . '</td></tr>';
        }
        ?>
    </tbody>
</table>
<div class="table-footer-area">
    <?php
    $page_params = array();
    $page_params['perpage'] = $perpage;
    $page_params['search'] = $search;
    $page_params['id'] = $id;
    $page_params['groupid'] = $groupid;
    repository_get_paging_bar($CFG->wwwroot . "/mod/lcms/contents.php", $page_params, $total_pages, $page);
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->

<script>
    function select_file(val, no, type) {
        $('input[name=titlestatic]', parent.document).val(val);
        $('input[name=title]', parent.document).val(val);
        $('input[name=contents]', parent.document).val(no);
        $('input[name=type]', parent.document).val(type);
        if(type=='video'||type=='embed'){
            $('#id_subtitlesection', parent.document).show();
            $('#fitem_id_subfiles', parent.document).hide();
        }else{
            $('#id_subtitlesection', parent.document).hide();
        }
        parent.M.mod_lcms.close();
    }
</script>

<?php
echo $OUTPUT->footer();
?>