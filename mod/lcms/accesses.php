<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


require('../../config.php');
require_once $CFG->libdir.'/accesslib.php';
require_once $CFG->dirroot.'/course/lib.php';

$contextid  = required_param('contextid', PARAM_INT);

$confirm    = optional_param('confirm', 0, PARAM_BOOL);

list($context, $course, $cm) = get_context_info_array($contextid);

$url = new moodle_url('/mod/lcms/accesses.php', array('contextid' => $contextid));

if ($course) {
    $isfrontpage = ($course->id == SITEID);
} else {
    $isfrontpage = false;
    if ($context->contextlevel == CONTEXT_USER) {
        $course = $DB->get_record('course', array('id'=>optional_param('courseid', SITEID, PARAM_INT)), '*', MUST_EXIST);
        $user = $DB->get_record('user', array('id'=>$context->instanceid), '*', MUST_EXIST);
        $url->param('courseid', $course->id);
        $url->param('userid', $user->id);
    } else {
        $course = $SITE;
    }
}

// Security first.
require_login($course, false, $cm);

$coursecontext = context_course::instance($course->id);
require_capability('moodle/course:enrolreview', $coursecontext);

$PAGE->set_url($url);

$PAGE->set_context($context);

$courseid = $course->id;

$ysclass = $DB->get_record('yscec_class', array('course'=>$courseid));


$contextname = $context->get_context_name();
$title = get_string('accesssettings', 'mod_lcms');

$PAGE->set_pagelayout('incourse');
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);



if ($confirm && confirm_sesskey()) {
    $allow = optional_param('allow', 0, PARAM_INT);
    
    $isopen = 'N';
    if($allow) {
        $isopen = 'Y';
    }
    
    $open = $DB->get_record('lcms_repository_open', 
        array(
            'year'  =>$ysclass->year,
            'term'  =>$ysclass->term,
            'course'=>$courseid,
            'userid'=>$USER->id));
    
    if($open !== false) {
        $open->isopen = $isopen;
        $DB->update_record('lcms_repository_open', $open);
    } else {
        $open = new stdClass();
        $open->year = $ysclass->year;
        $open->term = $ysclass->term;
        $open->course = $courseid;
        $open->userid = $USER->id;
        $open->isopen = $isopen;
        
        $DB->insert_record('lcms_repository_open', $open);
    }
    redirect($PAGE->url);
}



$open = $DB->get_record('lcms_repository_open', 
        array(
            'year'  =>$ysclass->year,
            'term'  =>$ysclass->term,
            'course'=>$courseid,
            'userid'=>$USER->id));

$allow = 0;
if($open !== false && $open->isopen == 'Y') {
    $allow = 1;
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);


// Make form
$formcontent  = html_writer::start_tag('form', array('method'=>'post', 'action'=>'accesses.php', 'id'=>'adminsettings'));
$formcontent .= html_writer::tag('div', '', array('class'=>'settingsform clearfix'));

$formcontent .= html_writer::tag('input', '', array('type'=>'hidden', 'name'=>'confirm', 'value'=>'1'));
$formcontent .= html_writer::tag('input', '', array('type'=>'hidden', 'name'=>'sesskey', 'value'=>sesskey()));
$formcontent .= html_writer::tag('input', '', array('type'=>'hidden', 'name'=>'contextid', 'value'=>$contextid));


$formcontent .= html_writer::start_tag('fieldset');

$formcontent .= html_writer::tag('div', '', array('class'=>'clearer'));

$formcontent .= html_writer::start_tag('div', array('class'=>'form-item clearfix', 'id'=>'allowtaaccess'));
$formcontent .= html_writer::start_tag('div', array('class'=>'form-label'));
$formcontent .= html_writer::tag('label', get_string('accesssettings', 'mod_lcms'), array('for'=>'id_s_allow'));
$formcontent .= html_writer::tag('span', '&nbsp;', array('class'=>'form-shortname'));
$formcontent .= html_writer::end_tag('div');

$formcontent .= html_writer::start_tag('div', array('class'=>'form-setting'));
$formcontent .= html_writer::start_tag('div', array('class'=>'form-text defaultsnext'));
$options = array('type'=>'checkbox', 'name'=>'allow', 'value'=>'1', 'id'=>'id_s_allow');
if($allow) {
    $options['checked']='checked';
}
$formcontent .= html_writer::tag('input', '', $options);
$defaultinfo = get_string('no');
$formcontent .= html_writer::tag('div', get_string('defaultsettinginfo', 'admin', $defaultinfo), array('class'=>'form-defaultinfo'));
$formcontent .= html_writer::end_tag('div');
$formcontent .= html_writer::end_tag('div');

$formcontent .= html_writer::tag('div', get_string('accesssettings_description', 'mod_lcms'), array('class'=>'form-form-description'));
$formcontent .= html_writer::end_tag('div');

$formcontent .= html_writer::end_tag('fieldset');



$cancel_url = $context->get_url();

$formcontent .= html_writer::start_tag('div', array('class'=>'form-buttons'));
$formcontent .= html_writer::tag('input', '', array('type'=>'submit', 'name'=>'submitbutton', 'id'=>'id_submitbutton', 'value'=>get_string('savechanges')));
$formcontent .= html_writer::tag('input', '', array('type'=>'button', 'name'=>'cancel', 'class'=>'btn-cancel', 'id'=>'id_cancel', 'value'=>get_string('cancel'), 'onclick'=>'location.href=\''.$cancel_url.'\''));
$formcontent .= html_writer::end_tag('div');

$formcontent .= html_writer::end_tag('div');
$formcontent .= html_writer::end_tag('form');

//echo $OUTPUT->box_start('generalbox capbox');
echo $formcontent;
//echo $OUTPUT->box_end();


echo $OUTPUT->footer($course);
