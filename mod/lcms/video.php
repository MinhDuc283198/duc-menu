<?php
global $CFG, $vid, $qua;

$contents = lcms_get_contents_info($vid, $qua);

if ($contents->logoview == 'Y') {
    $logourl = $CFG->wwwroot . '/mod/lcms/player/images/logo_snui.png';
} else {
    $logourl = '';
}

$caption_ko = LCMS . '/viewer/?cr=' . $vid . '&lang=ko';
$caption_en = LCMS . '/viewer/?cr=' . $vid . '&lang=en';
$capture = LCMS . '/viewer/?i=' . $vid;
?>

<!-- Video Player Area S -->
<div id="player_area">
    <div id="mediaplayer"><p>잠시만 기다려주세요. 로딩중입니다.</p></div>
</div>

<script type="text/javascript">

    function videoplayer(server,storage,vodname,author,ccmark){
        
        //디바이스별 판별
        var _ua = window.navigator.userAgent.toLowerCase();
        
        var browser = {
            ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
            ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
            iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
            android : /webkit/.test( _ua )&&/android/.test( _ua ),
            msie : /msie/.test( _ua )
        };   
        
        
        //디바이스별 플레이어 사용
        if(browser.ipod || browser.iphone) {
            html5player(server,storage,vodname,author,ccmark);
        } else if(browser.ipad) {
            html5player(server,storage,vodname,author,ccmark);
        } else if(browser.android) {
            androidplayer(server,storage,vodname,author,ccmark);
        } else {
            flashplayer(server,storage,vodname,author,ccmark);
        }
        
    }
            
    function html5player(server,storage,vodname,author,ccmark){
            
        var stream_url = 'http://'+server+':1935/vod/_definst_/mp4:'+storage+'/'+vodname+'/playlist.m3u8';
        
        $("#mediaplayer").html('<video preload controls autostart="true"'+
            'poster="<?php echo $capture; ?>" src="'+stream_url+'" width="540px">'+
            '<track kind=captions label="Korean subtitles" src="<?php echo $caption_ko; ?>" lang="ko" default></track>'+
            '<track kind=captions label="English subtitles" src="<?php echo $caption_en; ?>" lang="en"></track>'+
            '</video>').show();
                    
        video_sync();
            
    }

	function androidplayer(server,storage,vodname,author,ccmark){
            
        var stream_url = 'rtsp://'+server+':1935/vod/_definst_/mp4:'+storage+'/'+vodname;

		$("#mediaplayer").html('<a href="'+stream_url+'" alt="이미지를 클릭하시면 동영상을 보실 수 있습니다."><img src="<?php echo $capture; ?>" width="540px" height="300px"/></a>').show();
                    
        video_sync();
            
    }
            
    
    //jwplayer호출
    function flashplayer(server,storage,vodname,author,ccmark){
            
        jwplayer('mediaplayer').setup({
            'flashplayer': '<?php echo $CFG->wwwroot; ?>/mod/lcms/player/swf/player.swf',
            'id': 'mediaplayer',
            'menu': 'false',
            'width': '540px',
            'skin': '<?php echo $CFG->wwwroot; ?>/mod/lcms/player/skins/darkrv5.zip',
            'logo': { 'file': '<?php echo $logourl; ?>', 'hide': false, 'position': 'top-right' },
            'streamer': 'rtmp://'+server+'/vod/_definst_/',
            'provider': 'rtmp',
            'file': 'mp4:/'+storage+'/'+vodname,
            'image': '<?php echo $capture; ?>',
            'plugins': {
                '<?php echo $CFG->wwwroot; ?>/mod/lcms/player/js/textoutput.js': {'text': ccmark},
                "captions-2": {
                    files: '<?php echo $caption_ko; ?>,<?php echo $caption_en; ?>',
                    labels: 'Korean,English',
                    fontSize: '14',
                    fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                }
            },
                        
            'autostart': false
        });
                    
        jw_sync();
        
    }
 
    
    function youtubeplayer(emb,author,ccmark){

        //디바이스별 판별
        var _ua = window.navigator.userAgent.toLowerCase();
        
        var browser = {
            ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
            ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
            iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
            android : /webkit/.test( _ua )&&/android/.test( _ua ),
            msie : /msie/.test( _ua )
        };   
        
        
        //디바이스별 플레이어 사용
        if(browser.ipod || browser.iphone || browser.ipad || browser.android) {
        
            var videolist = '<iframe width="100%" height="300" src="http://www.youtube.com/embed/'+emb+'?html5=1" frameborder="0" allowfullscreen></iframe>';
            
           $('#mediaplayer').empty().append(videolist);
        
        } else {
            
            jwplayer('mediaplayer').setup({
                'flashplayer': '<?php echo $CFG->wwwroot; ?>/mod/lcms/player/swf/player.swf',
                'id': 'mediaplayer',
                'menu': 'false',
                'width': '540px',
                'skin': '<?php echo $CFG->wwwroot; ?>/mod/lcms/player/skins/darkrv5.zip',
                'logo': { 'file': '<?php echo $logourl; ?>', 'hide': false, 'position': 'top-right' },
                'file': 'http://www.youtube.com/watch?v='+emb,
                'plugins': {
                    '<?php echo $CFG->wwwroot; ?>/mod/lcms/player/js/textoutput.js': {'text': ccmark},
                    'captions-2': {
                        files: '<?php echo $caption_ko; ?>,<?php echo $caption_en; ?>',
                        labels: 'Korean,English',
                        fontSize: '14',
                        fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
                    }    
                },
                'autostart': false
            });
        
            jw_sync();
        
        }           
    
    }
            
    function vimeoplayer(emb,author,ccmark){
                
               
        var videolist = '<iframe src="//player.vimeo.com/video/'+emb+'" width="540" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        
        $('#mediaplayer').empty().append(videolist);
  
                
    
    }
            
            
    function video_sync(){

        var positionto = 0, oldposition = 0, subtitle, timestamps, timestamps_ko = [], timestamps_en = [], scroll = 0;
                
        var v = document.createElement('video');
        var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
        if ( supported == 'probably') { 
                            
            $('video').bind('timeupdate',function(event){
      
                positionto = parseInt(this.currentTime);
                
                if(positionto==1){
                    caption_array_get();
                }
                        
                if(positionto > oldposition){
					subtitle = $('#subtitle_ko');
				    timestamps = timestamps_ko;
                    caption_scroll();
                }
            
                oldposition = positionto;
      
            });
            
            function caption_array_get(){
                $('#subtitle_ko').find('.subtitle_text').each(function(o){
                    if($(this).attr('data-start')){
                        timestamps_ko.push({
                            start : +$(this).attr('data-start'),
                            end : +$(this).attr('data-end'),
                            elm : $(this),
                            pos : $(this).position().top
                        });
                    }
                });
            }
        
            function caption_scroll(){

				for(var i=0;i<timestamps.length;i++ ){
                    if(positionto >= timestamps[i].start && positionto < timestamps[i].end && scroll != i){ 
                        
                        subtitle.scrollTop(timestamps[i].pos);
						scroll = i;
						break;
                         
                    }
                }
                         
            }
            
        }

    }
    
    function jw_sync(){
    
        //window.onbeforeunload = function(){
        //playtime_update('pause',jwplayer().getDuration(),jwplayer().getPosition()); 
        //return '이 페이지에서 벗어납니다.';
        //}
                
        var positionto = 0, oldposition = 0, subtitle, timestamps, timestamps_ko = [], timestamps_en = [] ,scroll = 0;
                
        /* JWPlayer 상태 변화 */
        jwplayer('mediaplayer').onTime(function(event) {
            positionto = parseInt(event.position);
            
            if(positionto==1){
                caption_array_get();
				subtitle = $('#subtitle_ko');
				timestamps = timestamps_ko;
            }
        
            if(positionto > oldposition){
                caption_scroll();
            }
        
            oldposition = positionto;
        });
        
        
        function caption_array_get(){
            $('#subtitle_ko').find('.subtitle_text').each(function(o){
                if($(this).attr('data-start')){
                    timestamps_ko.push({
                        start : +$(this).attr('data-start'),
                        end : +$(this).attr('data-end'),
                        elm : $(this),
                        pos : $(this).position().top
                    });
                }
            });
        }
    
        function caption_scroll(){
      
                for(var i=0;i<timestamps.length;i++ ){
                    
					if(positionto >= timestamps[i].start && positionto < timestamps[i].end && scroll != i){ 
                        
                        subtitle.scrollTop(timestamps[i].pos);
						scroll = i;
						break;
                         
                    }
                }
                         
        }
    
    }

</script>

<?php
if ($contents->type == 'Video') {

    if ($contents->format == 'flv') {
        echo '<script type="text/javascript">
            flashplayer("' . $contents->server . '","' . $contents->storage . '","' . $contents->vodname . '","' . $contents->author . '","' . $contents->ccmark . '");
            </script>';
    } else {
        echo '<script type="text/javascript">
            videoplayer("' . $contents->server . '","' . $contents->storage . '","' . $contents->vodname . '","' . $contents->author . '","' . $contents->ccmark . '");
            </script>';
    }
} else if ($contents->type == 'Embed') {

    if ($contents->embed_type == 'youtube') {
        echo '<script type="text/javascript">
            youtubeplayer("' . $contents->embed_code . '","' . $contents->author . '","' . $contents->ccmark . '");
            </script>';
    } else {
        echo '<script type="text/javascript">
            vimeoplayer("' . $contents->embed_code . '","' . $contents->author . '","' . $contents->ccmark . '");
            </script>';
    }
}
?>

