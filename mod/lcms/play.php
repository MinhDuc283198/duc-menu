<?php

require_once('../../config.php');

$id = required_param('id', PARAM_INT);          // Commons ID


if (! $lcms = $DB->get_record("lcms", array("id" => $id))) {
    print_error('invalidlcmsid', 'lcms');
}
if (! $course = $DB->get_record("course", array("id" => $lcms->course))) {
    print_error('coursemisconf');
}
if (!$cm = get_coursemodule_from_instance("lcms", $lcms->id, $course->id)) {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);

$attempts = $DB->count_records("lcms_track", array('lcms' => $lcms->id, 'userid' => $USER->id));

$track = new stdClass();
$track->lcms = $lcms->id;
$track->userid = $USER->id;
$track->attempts = $attempts + 1;
$track->timeview = time();
$DB->insert_record('lcms_track', $track);

$completion = new completion_info($course);
$completion->set_module_viewed($cm);

?>

<html>
    <head>
        <title><?php echo $lcms->name; ?></title>
        <style>
            body {
                margin: 0;
                overflow: hidden;
            }
        </style>
    </head>
    <body>
        <iframe src="<?php echo $lcms->viewurl ?>" style="width: 100%; height: 100%; margin: 0; border: 0px;"/>
    </body>
</html>


