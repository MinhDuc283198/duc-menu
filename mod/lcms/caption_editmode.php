<?php

$sql = 'select * from {lcms_caption} where lcms_id=? and lang=? order by stime, etime asc';
$captions_ko = $DB->get_records_sql($sql,array($lcms->id,'ko'));

$sql = 'select * from {lcms_caption} where lcms_id=? and lang=? order by stime, etime asc';
$captions_en = $DB->get_records_sql($sql,array($lcms->id,'en'));
$currenttab = 'tab_ko';
$rows = array (
    new tabobject('tab_ko', "javascript:lang_change('ko');", 'Korean', 'Korean subtitle', true),
    new tabobject('tab_en', "javascript:lang_change('en');", 'English', 'English subtitle', true)
    );
print_tabs(array($rows), $currenttab);

?>
<div class="subtitle_all_area">
<div>
    <input type="button" class="add_btn search_btn" add="true" value="자막추가" onclick="caption_add($(this));">
</div>
<div class="subtitle_add_area" style="display:none;">
    <ul class="subtitle_list">
        <li class="subtitle">
            <p class="subtitle_time">
                <input type="text" maxlength="2" class="w_15" id="stime_hh" onkeyup="onlynumber(this);"> : 
                <input type="text" maxlength="2" class="w_15" id="stime_mm" onkeyup="onlynumber(this);"> : 
                <input type="text" maxlength="2" class="w_15" id="stime_ss" onkeyup="onlynumber(this);">
                ~ 
                <input type="text" maxlength="2" class="w_15" id="etime_hh" onkeyup="onlynumber(this);"> : 
                <input type="text" maxlength="2" class="w_15" id="etime_mm" onkeyup="onlynumber(this);"> : 
                <input type="text" maxlength="2" class="w_15" id="etime_ss" onkeyup="onlynumber(this);">
            </p>
            <p class="subtitle_text">
                <textarea id="textprint" rows="1"></textarea>
            </p>
            <p class="subtitle_edit">
                <input type="button" class="edit_btn blue_btn" value="등록" onclick="caption_add_ajax();">
            </p>
        </li>
    </ul>
</div>
<!-- Content Navigation Start -->
<div id="content_navigation">
<!--    <p class="black_btn black_selected ko_btn" onclick="lang_change('ko')" style="cursor:pointer;">한국어</p>
    <p class="black_btn en_btn" onclick="lang_change('en')" style="cursor:pointer;">영어</p>-->
    <p class="red_message">※ 자막 수정 후 “완료”를 눌러야 자막이 변경됩니다. </p>
</div>
<!-- Content Navigation End -->

<!-- Subtitle Table Wrap Start -->
<div class="subtitle_area" id="subtitle_ko" style="position:relative;width:100%;overflow:scroll;border:1px solid #ccc;">

<?php
$caption_table = '<ol class="subtitle_list">';

if ($captions_ko && is_array($captions_ko)) {
    $num = 1;
    foreach ($captions_ko as $caption) {
        $stimeobj = time_from_seconds($caption->stime);
        $etimeobj = time_from_seconds($caption->etime);

        $caption->caption = preg_replace('/\r\n|\r|\n/', '', $caption->caption);
        $caption->caption = stripslashes($caption->caption);

        $caption_table .= '<li class="subtitle">
             <a id="a' . $num . '" />
             <p class="subtitle_time">
             <input type="text" maxlength="2" class="w_15" id="stime_hh_' . $num . '" readonly value="' . $stimeobj->h . '"> : 
             <input type="text" maxlength="2" class="w_15" id="stime_mm_' . $num . '" readonly value="' . $stimeobj->m . '"> : 
             <input type="text" maxlength="2" class="w_15" id="stime_ss_' . $num . '" readonly value="' . $stimeobj->s . '">
              ~ 
             <input type="text" maxlength="2" class="w_15" id="etime_hh_' . $num . '" readonly value="' . $etimeobj->h . '"> : 
             <input type="text" maxlength="2" class="w_15" id="etime_mm_' . $num . '" readonly value="' . $etimeobj->m . '"> : 
             <input type="text" maxlength="2" class="w_15" id="etime_ss_' . $num . '" readonly value="' . $etimeobj->s . '">
             </p>
             <p class="subtitle_text" data-start="' . $caption->stime . '" data-end="' . $caption->etime . '">
             <textarea id="textprint_' . $num . '" rows="1"  readonly>' . $caption->caption . '</textarea>
             </p>';
        
        $caption_table .= '<p class="subtitle_edit">
             <input type="button" class="small_btn" value="편집" edit="true" onclick="caption_update($(this),\'ko\',' . $num . ')">
             <input type="button" class="small_btn" value="신고" edit="true" onclick="caption_police(\'' . $caption->stime . '\',\'' . $caption->etime . '\',\'' . $caption->caption . '\',\'ko\','.$num.');">
             </p>';
        
        $caption_table .= '</li>';
        $num++;
    }
} else {

    $caption_table = '<li>등록된 자막이 없습니다.</li>';
}

$caption_table .= '</ol>';

echo $caption_table;
?>

</div>

<!-- Subtitle Table Wrap Start -->
<div class="subtitle_area" id="subtitle_en" style="position:relative;width:100%;overflow:scroll;border:1px solid #ccc;display: none;">

<?php
$caption_table = '<ol class="subtitle_list">';

if ($captions_en && is_array($captions_en)) {
    $num = 1;
    foreach ($captions_en as $caption) {
        $stimeobj = time_from_seconds($caption->stime);
        $etimeobj = time_from_seconds($caption->etime);
        
        $caption->caption = preg_replace('/\r\n|\r|\n/', '', $caption->caption);
        $caption->caption = stripslashes($caption->caption);

        $caption_table .= '<li class="subtitle">
             <a id="a' . $num . '" />
             <p class="subtitle_time">
             <input type="text" maxlength="2" class="w_15" id="stime_hh_' . $num . '" readonly value="' . $stimeobj->h . '"> : 
             <input type="text" maxlength="2" class="w_15" id="stime_mm_' . $num . '" readonly value="' . $stimeobj->m . '"> : 
             <input type="text" maxlength="2" class="w_15" id="stime_ss_' . $num . '" readonly value="' . $stimeobj->s . '">
              ~ 
             <input type="text" maxlength="2" class="w_15" id="etime_hh_' . $num . '" readonly value="' . $etimeobj->h . '"> : 
             <input type="text" maxlength="2" class="w_15" id="etime_mm_' . $num . '" readonly value="' . $etimeobj->m . '"> : 
             <input type="text" maxlength="2" class="w_15" id="etime_ss_' . $num . '" readonly value="' . $etimeobj->s . '">
             </p>
             <p class="subtitle_text" data-start="' . $caption->stime . '" data-end="' . $caption->etime . '">
             <textarea id="textprint_' . $num . '" rows="1"  readonly>' . $caption->caption . '</textarea>
             </p>';
        
        $caption_table .= '<p class="subtitle_edit">
             <input type="button" class="small_btn" value="편집" edit="true" onclick="caption_update($(this),\'en\',' . $num . ')">
             <input type="button" class="small_btn" value="신고" edit="true" onclick="caption_police(\'' . $caption->stime . '\',\'' . $caption->etime . '\',\'' . $caption->caption . '\',\'en\','.$num.');">
             </p>';
        
        $caption_table .= '</li>';
        $num++;
    }
} else {

    $caption_table = '<li>등록된 자막이 없습니다.</li>';
}

$caption_table .= '</ol>';

echo $caption_table;
?>

</div>
</div>

<form name="caption_form" id="caption_form">
    <input type="hidden" name="amode"/>
    <input type="hidden" name="stime"/>
    <input type="hidden" name="etime"/>
    <input type="hidden" name="caption"/>
    <input type="hidden" name="lang" value="ko"/>
<!--    <input type="hidden" name="lcmsid" value="<?php echo $cid; ?>"/>-->
    <input type="hidden" name="num"/>
    <input type="hidden" name="type"/>
</form>
<!-- Subtitle Table Wrap End -->

<script type="text/javascript">
            
    function lang_change(lang,cur){
        $('.subtitle_area').hide();
        $('#subtitle_'+lang).show();
        $('input[name=lang]').val(lang);
        $('.nav-tabs li').removeClass('active');
        cur.addClass('active');
    }
            
    function caption_get(lcmsid, lang){
        $.ajax({
            url: 'caption_ajax_1.php',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                amode : 'get',
                lang : lang,
                lcmsid : lcmsid
            },
            success: function(data, textStatus, jqXHR ) {
                if(data.status == 'success') {
                    status = true;
                    $('#subtitle_'+lang).empty().append(data.captions);
                } else {
                    alert(data.message);
                    status = false;
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                alert(jqXHR.responseText);
                status = false;
            }
        });
                
    }
            
    function caption_add(id){
                
        var add = id.attr('add');
        if(add=='true'){
            $('.subtitle_add_area').show();
            id.val('등록취소');
            id.attr('add','false');
//	    jwplayer('mediaplayer').pause(true);
        }else{
            $('.subtitle_add_area').hide();
            id.val('자막추가');
            id.attr('add','true');
        }
                
    }
            
    function caption_update(id,lang,num){        
        if(id.attr('edit')=='true'){
            id.removeClass('orange_btn').addClass('blue_btn').val('완료');
            id.attr('edit','false');
            $('#subtitle_'+lang+' #stime_hh_'+num).add('#subtitle_'+lang+' #stime_mm_'+num).add('#subtitle_'+lang+' #stime_ss_'+num).attr('readonly',false).css('background','#fffbe8');
            $('#subtitle_'+lang+' #etime_hh_'+num).add('#subtitle_'+lang+' #etime_mm_'+num).add('#subtitle_'+lang+' #etime_ss_'+num).attr('readonly',false).css('background','#fffbe8');
            $('#subtitle_'+lang+' #textprint_'+num).attr('readonly',false).css('background','#fffbe8');
//            jwplayer('mediaplayer').pause(true);
        }else{
            var status = caption_edit_ajax(lang,num);
            if(status){
                id.removeClass('blue_btn').addClass('orange_btn').val('편집');
                id.attr('edit','true');
                $('#subtitle_'+lang+' #stime_hh_'+num).add('#stime_mm_'+num).add('#stime_ss_'+num).attr('readonly',true).css('background','#fff');
                $('#subtitle_'+lang+' #etime_hh_'+num).add('#etime_mm_'+num).add('#etime_ss_'+num).attr('readonly',true).css('background','#fff');
                $('#subtitle_'+lang+' #textprint_'+num).attr('readonly',true).css('background','#fff');
            }    
        }
    }
            
    function caption_edit_ajax(lang,num){        
        var status = true;
        var lcmsid = <?php echo $lcms->id; ?>;
        var contextid = <?php echo $context->id; ?>;
        var contentid = <?php echo $lcms->contents; ?>;
                
        if($('#subtitle_'+lang+' #stime_hh_'+num).val() == ''||$('#subtitle_'+lang+' #stime_mm_'+num).val() == ''||$('#subtitle_'+lang+' #stime_ss_'+num).val() == '') {
            alert('시작시간을 입력하세요.');
            return false;
        }
                
        if($('#subtitle_'+lang+' #etime_hh_'+num).val() == ''||$('#subtitle_'+lang+' #etime_mm_'+num).val() == ''||$('#subtitle_'+lang+' #etime_ss_'+num).val() == '') {
            alert('끝시간을 입력하세요.');
            return false;
        }
                
        if($('#subtitle_'+lang+' #textprint_'+num).val() == '') {
            alert('자막 내용을 입력하세요.');
            return false;
        }

        $.ajax({
            url: 'caption_ajax_1.php',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                amode: 'edit',
                num: num,
                lang: lang,
                lcmsid: lcmsid,
                contextid: contextid,
                contentid: contentid,
                stime_hh: $('#subtitle_'+lang+' #stime_hh_'+num).val(),
                stime_mm: $('#subtitle_'+lang+' #stime_mm_'+num).val(),
                stime_ss: $('#subtitle_'+lang+' #stime_ss_'+num).val(),
                etime_hh: $('#subtitle_'+lang+' #etime_hh_'+num).val(),
                etime_mm: $('#subtitle_'+lang+' #etime_mm_'+num).val(),
                etime_ss: $('#subtitle_'+lang+' #etime_ss_'+num).val(),
                textprint: $('#subtitle_'+lang+' #textprint_'+num).val()
            },
            success: function(data, textStatus, jqXHR ) {
                if(data.status == 'success') {
                    status = true;
                    caption_get(lcmsid, lang);
                } else {
                    alert(data.message);
                    status = false;
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                alert(jqXHR.responseText);
                status = false;
            }
        });
                
        return status;
    }
            
    function caption_add_ajax(){
                
        var status = true;
        var lang = $('input[name=lang]').val();
        var lcmsid = <?php echo $lcms->id; ?>;
        var contextid = <?php echo $context->id; ?>;
        var contentid = <?php echo $lcms->contents; ?>;
        
        if($('#stime_hh').val() == ''||$('#stime_mm').val() == ''||$('#stime_ss').val() == '') {
            alert('시작시간을 입력하세요.');
            return false;
        }
                
        if($('#etime_hh').val() == ''||$('#etime_mm').val() == ''||$('#etime_ss').val() == '') {
            alert('끝시간을 입력하세요.');
            return false;
        }
        if($('#textprint').val() == '') {
            alert('자막 내용을 입력하세요.');
            return false;
        }
       
        $.ajax({
            url: 'caption_ajax_1.php',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                amode: 'add',
                lang: lang,
                lcmsid: lcmsid,
                contextid: contextid,
                contentid: contentid,
                stime_hh: $('#stime_hh').val(),
                stime_mm: $('#stime_mm').val(),
                stime_ss: $('#stime_ss').val(),
                etime_hh: $('#etime_hh').val(),
                etime_mm: $('#etime_mm').val(),
                etime_ss: $('#etime_ss').val(),
                textprint: $('#textprint').val()
            },
            success: function(data, textStatus, jqXHR ) {
                if(data.status == 'success') {
                    status = true;
                    caption_get(lcmsid, lang);
                } else {
                    alert(data.message);
                    status = false;
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                alert(jqXHR.responseText);
                status = false;
            }
        });
                
        return status;
    }
        
    function onlynumber(t){
       t.value=t.value.replace(/[^0-9]/g,'');
    }
            
</script>


