<?php

$string['pluginadministration'] = 'Learning contents administration';
$string['pluginname'] = 'Learning contents';

$string['modulename'] = 'Learning contents';
$string['modulename_help'] = 'Learning Content is a resource for learning. This includes videos, documents, web pages, and even youtube videos. These resources can either be called from LCMS or directly uploaded from local device during the activity/resource generation. The student access status can be verified either in statistics or report as learning time, access time, or download occurrence.';
$string['modulename_link'] = 'mod/lcms/view';
$string['modulenameplural'] = 'Learning contents';

$string['close'] = 'close';
$string['name'] = 'name';
$string['progress'] = 'Progress Check';
$string['lcmsintro'] = 'Intro';
$string['contentheader'] = 'Content';
$string['content'] = 'Content';
$string['findcontent'] = 'Select';
$string['findcontent2'] = 'Pakage';
$string['selectcontent'] = 'Select in LCMS';
$string['pleasewait'] = 'Please wait...';
$string['invalidlcmsid'] = 'Invalid Xinics Commons Id';
$string['noviewpermission'] = 'You have no permission';
$string['viewcontent'] = 'Study';
$string['viewvideo'] = 'View Video';
$string['viewhighvideo'] = 'HD Video';
$string['viewlowvideo'] = 'SD Video';

$string['addcontent'] = 'Add a new content';
$string['starttime'] = 'Start Time';
$string['endtime'] = 'Deadline';
$string['islock'] = 'View after closing';
$string['islock_n'] = 'No';
$string['islock_y'] = 'Yes (No progress check)';
$string['islock_e'] = 'Yes (Progress can be checked)';

$string['islcmsprogress'] = 'LCMS Progress Check YN';
$string['firststudy'] = 'First Study';
$string['laststudy'] = 'Last: {$a}';
$string['studyrate'] = 'Rate: {$a} %';
$string['studyyn'] = 'Study: {$a}';
$string['totalstudytime'] = 'Total: {$a}';
$string['reference'] = 'Reference';

$string['continueplease'] = 'continue study?';
$string['beforeunload'] = 'Simyeon off the page without stopping the video playback , some jindoyul will not be saved.';
$string['gotocourselist'] = 'Go to my course';
$string['gotoprogresslist'] = 'Progress Check';
$string['nostudytime'] = 'No Study Time';
$string['waitloading'] = 'Please wait, loading..';

$string['insert_file'] = 'Select a File';
$string['insert_file_search'] = 'Search file in LMS Repository';
$string['insert_file_inrepositry'] = 'Insert new File';


$string['filechange'] = 'Change file';
$string['filechange_n'] = 'No change';
$string['filechange_y'] = 'Change file in repository';
$string['filechange_e'] = 'Create file in repository';

$string['subtitle'] = 'Subtitle';
$string['subtitleselect'] = 'Subtitle select';
$string['insert_subtitle'] = 'Subtitle add';
$string['insert_subtitle_n'] = 'No Subtitle';
$string['insert_subtitle_y'] = 'Select a SubtitleFile';
$string['subtitle_view_type'] = 'Subtitle View';
$string['subtitle_view_n'] = 'No';
$string['subtitle_view_y'] = 'YES (No edit)';
$string['subtitle_view_e'] = 'YES (editmode)';
$string['edit'] = 'Edit';
$string['notify'] = 'Notify';
$string['done'] = 'Done';
$string['register'] = 'Register done';
$string['register_cancel'] = 'Cancel';
$string['subtitle_edit_msg'] = '※ After editing subtitles, press "Done" to change subtitles.';
$string['subtitle_empty'] = 'There are no registered subtitles..';
$string['subtitle_starttime_input'] = 'Please enter the start time.';
$string['subtitle_endtime_input'] = 'Please enter the end time.';
$string['subtitle_text_input'] = 'Please enter captions.';
$string['needlogin'] = 'Please use after login.';
$string['subtitle_startendtime_error'] = 'Start time is greater than or equal to end time. Please reset.';

$string['timeset'] = 'Set Time';
$string['insert_file'] = 'Content';
$string['error:notselectfile'] = 'Please select a file';

$string['accesssettings'] = 'Learning contents access settings';
$string['accesssettings_description'] = 'Allow teaching assistant can access the contents of my learnings.';
$string['allowtaaccess'] = 'Allow a teaching assistant access';

$string['completionprogress'] = 'Student must complete a progress(%):';
$string['completionprogressgroup'] = 'Check progress';
$string['error:completionprogress'] = 'It must be between 1 and 100.';
$string['captionpreview'] = "Caption preview";

$string['search'] = 'Search';
$string['total_page'] = 'Total Page';
$string['view_type'] = 'Display';
$string['popup'] = 'Popup';
$string['link'] = 'Link';
$string['my_contents'] = 'My Contents';
$string['share_contents'] = 'Share Contents';
$string['refresh'] = 'Refresh';
$string['select'] = 'Select';
$string['add_contents'] = 'Add Contents';
$string['edit_contents'] = 'Edit Contents';