<?php

$string['pluginadministration'] = 'Learning contents administration';
$string['pluginname'] = 'Learning contents';

$string['modulename'] = 'Learning contents';
$string['modulename_help'] = 'Learning Content is a resource for learning. This includes videos, documents, web pages, and even youtube videos. These resources can either be called from LCMS or directly uploaded from local device during the activity/resource generation. The student access status can be verified either in statistics or report as learning time, access time, or download occurrence.';
$string['modulename_link'] = 'mod/lcms/view';
$string['modulenameplural'] = 'Learning contents';

$string['close'] = '閉じる';
$string['name'] = '名前';
$string['progress'] = '進歩チェック有無';
$string['lcmsintro'] = 'はじめに';
$string['contentheader'] = 'コンテンツ';
$string['content'] = 'コンテンツ';
$string['findcontent'] = '選択';
$string['findcontent2'] = 'Pakage';
$string['selectcontent'] = 'Select in LCMS';
$string['pleasewait'] = 'インポート中です･･･';
$string['invalidlcmsid'] = 'Invalid Xinics Commons Id';
$string['noviewpermission'] = '表示権限がありません。';
$string['viewcontent'] = '学習する';
$string['viewvideo'] = 'ビデオを見る';
$string['viewhighvideo'] = 'HD画質';
$string['viewlowvideo'] = '一般画質';

$string['addcontent'] = 'Add a new content';
$string['starttime'] = '開始時間';
$string['endtime'] = '締切';
$string['islock'] = '締め切り後に表示';
$string['islock_n'] = '隠す';
$string['islock_y'] = 'Yes (No progress check)';
$string['islock_e'] = 'Yes (Progress can be checked)';

$string['islcmsprogress'] = '進歩チェックに含める内容はチェックします。';
$string['firststudy'] = '最初の学習';
$string['laststudy'] = 'Last: {$a}';
$string['studyrate'] = '進度率: {$a} %';
$string['studyyn'] = '学習有無：{$a}';
$string['totalstudytime'] = '総学習時間: {$a}';
$string['reference'] = '参考';

$string['continueplease'] = '以前の学習から続けますか？';
$string['beforeunload'] = 'Simyeon off the page without stopping the video playback , some jindoyul will not be saved.';
$string['gotocourselist'] = '講義室に戻る';
$string['gotoprogresslist'] = '進度率確認';
$string['nostudytime'] = '学習期間ではありません。';
$string['waitloading'] = 'しばらくお待ちください。読み込んでいます。';

$string['insert_file'] = 'Select a File';
$string['insert_file_search'] = 'Search file in LMS Repository';
$string['insert_file_inrepositry'] = 'Insert new File';


$string['filechange'] = 'Change file';
$string['filechange_n'] = 'No change';
$string['filechange_y'] = 'Change file in repository';
$string['filechange_e'] = 'Create file in repository';

$string['subtitle'] = '字幕';
$string['subtitleselect'] = '字幕の選択';
$string['insert_subtitle'] = '字幕の追加';
$string['insert_subtitle_n'] = '字幕なし';
$string['insert_subtitle_y'] = '字幕登録';
$string['subtitle_view_type'] = '字幕表示';
$string['subtitle_view_n'] = '隠す';
$string['subtitle_view_y'] = '보기(편집 불가)';
$string['subtitle_view_e'] = '보기(편집 가능)';
$string['edit'] = '編集';
$string['notify'] = '申告';
$string['done'] = '完了';
$string['register'] = '登録完了';
$string['register_cancel'] = '登録解除';
$string['subtitle_edit_msg'] = '※字幕変更後、「完了」を押すと字幕が変更されます。';
$string['subtitle_empty'] = '登録された字幕はありません。';
$string['subtitle_starttime_input'] = '開始時刻を入力してください。';
$string['subtitle_endtime_input'] = '終了時刻を入力してください。';
$string['subtitle_text_input'] = '字幕の内容を入力してください。';
$string['needlogin'] = 'ログイン後に利用して下さい。';
$string['subtitle_startendtime_error'] = '開始時間が終了時間よりも大きいか同じです。再度設定してください。';

$string['timeset'] = '時間設定';

$string['insert_file'] = 'コンテンツ';
$string['error:notselectfile'] = 'ファイルを選択してください。';

$string['accesssettings'] = '강의 콘텐츠 접근 설정';
$string['accesssettings_description'] = '조교가 나의 강의 콘텐츠에 접근할 수 있도록 허용합니다.';
$string['allowtaaccess'] = '조교 접근 허용';

$string['completionprogress'] = '学生が完了すべき進度率(%)：';
$string['completionprogressgroup'] = '進度の確認';
$string['error:completionprogress'] = '1と100の間の値でなければなりません。';
$string['captionpreview'] = "字幕プレビュー";

$string['search'] = '検索';
$string['total_page'] = '総ページ数';
$string['view_type'] = '表示方式';
$string['popup'] = '팝업';
$string['link'] = '페이지 이동';
$string['my_contents'] = 'マイコンテンツ';
$string['share_contents'] = '共有コンテンツ';
$string['refresh'] = '更新';
$string['select'] = '選択';
$string['add_contents'] = 'コンテンツの登録';
$string['edit_contents'] = 'Edit Contents';