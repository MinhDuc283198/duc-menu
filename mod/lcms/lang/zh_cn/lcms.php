<?php

$string['pluginadministration'] = 'Learning contents administration';
$string['pluginname'] = 'Learning contents';

$string['modulename'] = 'Learning contents';
$string['modulename_help'] = 'Learning Content is a resource for learning. This includes videos, documents, web pages, and even youtube videos. These resources can either be called from LCMS or directly uploaded from local device during the activity/resource generation. The student access status can be verified either in statistics or report as learning time, access time, or download occurrence.';
$string['modulename_link'] = 'mod/lcms/view';
$string['modulenameplural'] = 'Learning contents';

$string['close'] = '关闭';
$string['name'] = '名称';
$string['progress'] = '是否校验进度';
$string['lcmsintro'] = '介绍';
$string['contentheader'] = '内容';
$string['content'] = '内容';
$string['findcontent'] = '选择';
$string['findcontent2'] = 'Pakage';
$string['selectcontent'] = 'Select in LCMS';
$string['pleasewait'] = '载入中……';
$string['invalidlcmsid'] = 'Invalid Xinics Commons Id';
$string['noviewpermission'] = '没有查看权限';
$string['viewcontent'] = '结尾时间';
$string['viewvideo'] = '查看视频';
$string['viewhighvideo'] = 'HD画质';
$string['viewlowvideo'] = '普通画质';

$string['addcontent'] = 'Add a new content';
$string['starttime'] = '开始时间';
$string['endtime'] = '结束时间';
$string['islock'] = '结束后查看';
$string['islock_n'] = '隐藏';
$string['islock_y'] = 'Yes (No progress check)';
$string['islock_e'] = 'Yes (Progress can be checked)';

$string['islcmsprogress'] = '校验进度校验需包含的内容。';
$string['firststudy'] = '最初学习';
$string['laststudy'] = 'Last: {$a}';
$string['studyrate'] = '进度率: {$a} %';
$string['studyyn'] = '是否学习：{$a}';
$string['totalstudytime'] = '总学习时间：{$a}';
$string['reference'] = '参考';

$string['continueplease'] = '是否继续上一个学习查看。';
$string['beforeunload'] = 'Simyeon off the page without stopping the video playback , some jindoyul will not be saved.';
$string['gotocourselist'] = '返回讲义室。';
$string['gotoprogresslist'] = '确认进度率。';
$string['nostudytime'] = '非学习期间。';
$string['waitloading'] = '请稍等。加载中。';

$string['insert_file'] = 'Select a File';
$string['insert_file_search'] = 'Search file in LMS Repository';
$string['insert_file_inrepositry'] = 'Insert new File';


$string['filechange'] = 'Change file';
$string['filechange_n'] = 'No change';
$string['filechange_y'] = 'Change file in repository';
$string['filechange_e'] = 'Create file in repository';

$string['subtitle'] = '字幕';
$string['subtitleselect'] = '选择字幕';
$string['insert_subtitle'] = '添加字幕';
$string['insert_subtitle_n'] = '无字幕';
$string['insert_subtitle_y'] = '登录字幕';
$string['subtitle_view_type'] = '查看字幕';
$string['subtitle_view_n'] = '隐藏';
$string['subtitle_view_y'] = '보기(편집 불가)';
$string['subtitle_view_e'] = '보기(편집 가능)';
$string['edit'] = '编辑';
$string['notify'] = '举报';
$string['done'] = '完成';
$string['register'] = '完成登录';
$string['register_cancel'] = '取消登录';
$string['subtitle_edit_msg'] = '※修改字幕后按“完成”才可变更字幕。';
$string['subtitle_empty'] = '没有上传的字幕。';
$string['subtitle_starttime_input'] = '请输入开始时间。';
$string['subtitle_endtime_input'] = '请输入结尾时间。';
$string['subtitle_text_input'] = '请输入字幕内容。';
$string['needlogin'] = '请登录后使用。';
$string['subtitle_startendtime_error'] = '开始时间与结尾时间大或相同。请重新设置。';

$string['timeset'] = '设置时间';

$string['insert_file'] = '内容';
$string['error:notselectfile'] = '请选择文件';

$string['accesssettings'] = '강의 콘텐츠 접근 설정';
$string['accesssettings_description'] = '조교가 나의 강의 콘텐츠에 접근할 수 있도록 허용합니다.';
$string['allowtaaccess'] = '조교 접근 허용';

$string['completionprogress'] = '学生应完成的进度率(%):';
$string['completionprogressgroup'] = '确认进度。';
$string['error:completionprogress'] = '应是1~100之间的值。';
$string['captionpreview'] = "预览字幕";

$string['search'] = '搜索';
$string['total_page'] = '总页数';
$string['view_type'] = '显示方式';
$string['popup'] = '팝업';
$string['link'] = '페이지 이동';
$string['my_contents'] = '我的内容';
$string['share_contents'] = '工学内容';
$string['refresh'] = '刷新';
$string['select'] = '选择';
$string['add_contents'] = '内容登录';
$string['edit_contents'] = 'Edit Contents';