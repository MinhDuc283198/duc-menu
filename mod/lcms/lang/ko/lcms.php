<?php

$string['pluginadministration'] = '강의 콘텐츠 관리';
$string['pluginname'] = '강의 콘텐츠';

$string['modulename'] = '강의 콘텐츠';
$string['modulename_help'] = '<p><b>강의 콘텐츠</b></p>
<div class="indent">
<p>강의파일 저장소와에서 업로드한 파일을 선택하여 강의실 콘텐츠로 제공하는 활동입니다. 선택된 콘텐츠가 동영상인 경우에는 자동으로 동영상 플레이어와 연결하여 동영상을 보여주고 동영상을 시청한 진도를 %로 계산하여 저장합니다. 강의실에 있는 모든 동영상 강의 콘텐츠은 "강의 콘텐츠 진도체크" 활동을 추가하면 해당 활동을 통해 각 학습자 별로 동영상 강의 진도 확인을 할 수 있으며 동일 페이지에서 교수자는 모든 학습자의 동영상 강의 진도 확인을 할 수 있습니다.</p>
</div>';
$string['modulename_link'] = 'mod/lcms/view';
$string['modulenameplural'] = '강의 콘텐츠';

$string['close'] = '닫기';
$string['name'] = '이름';
$string['progress'] = '진도체크여부';
$string['lcmsintro'] = '소개';
$string['contentheader'] = '콘텐츠';
$string['content'] = '콘텐츠';
$string['findcontent'] = '선택';
$string['findcontent2'] = '패키지';
$string['selectcontent'] = '선택';
$string['pleasewait'] = '불러오는 중입니다...';
$string['invalidlcmsid'] = '유효하지 못한 Xinics Commons 아이디';
$string['noviewpermission'] = '보기 권한이 없습니다.';
$string['viewcontent'] = '학습하기';
$string['viewvideo'] = '동영상보기';
$string['viewhighvideo'] = 'HD화질';
$string['viewlowvideo'] = '일반화질';

$string['addcontent'] = '강의 콘텐츠 등록하기';
$string['starttime'] = '시작 시간';
$string['endtime'] = '마감 시간';
$string['islock'] = '마감 후 보기';
$string['islock_n'] = '숨기기';
$string['islock_y'] = '보기 (진도체크 불가)';
$string['islock_e'] = '보기 (진도체크 가능)';

$string['islcmsprogress'] = '진도체크에 포함할 콘텐츠는 체크합니다.';
$string['firststudy'] = '최초학습';
$string['laststudy'] = '최종학습일: {$a}';
$string['studyrate'] = '진도율: {$a} %';
$string['studyyn'] = '학습여부: {$a}';
$string['totalstudytime'] = '총학습시간: {$a}';
$string['reference'] = '참고';

$string['continueplease'] = '이전 학습부터 이어보시겠습니까?';
$string['beforeunload'] = '동영상 재생을 중지하지 않고 페이지를 벗어나시면 일부 진도율이 저장되지 않습니다.';
$string['gotocourselist'] = '강의실로 돌아가기';
$string['gotoprogresslist'] = '진도율 확인';
$string['nostudytime'] = '학습기간이 아닙니다.';
$string['waitloading'] = '잠시만 기다려주세요. 로딩중입니다.';

$string['insert_file'] = '파일선택';
$string['insert_file_search'] = '강의파일저장소에서 가져오기';
$string['insert_file_inrepositry'] = '파일등록';

$string['filechange'] = '파일변경';
$string['filechange_n'] = '변경하지 않음';
$string['filechange_y'] = '저장소의 파일을 변경';
$string['filechange_e'] = '저장소의 파일을 새로 생성';

$string['subtitle'] = '자막';
$string['subtitleselect'] = '자막선택';
$string['insert_subtitle'] = '자막추가';
$string['insert_subtitle_n'] = '자막없음';
$string['insert_subtitle_y'] = '자막등록';
$string['subtitle_view_type'] = '자막보기';
$string['subtitle_view_n'] = '숨기기';
$string['subtitle_view_y'] = '보기(편집 불가)';
$string['subtitle_view_e'] = '보기(편집 가능)';
$string['edit'] = '편집';
$string['notify'] = '신고';
$string['done'] = '완료';
$string['register'] = '등록완료';
$string['register_cancel'] = '등록취소';
$string['subtitle_edit_msg'] = '※ 자막 수정 후 “완료”를 눌러야 자막이 변경됩니다.';
$string['subtitle_empty'] = '등록된 자막이 없습니다.';
$string['subtitle_starttime_input'] = '시작시간을 입력하세요.';
$string['subtitle_endtime_input'] = '끝시간을 입력하세요.';
$string['subtitle_text_input'] = '자막 내용을 입력하세요.';
$string['needlogin'] = '로그인 후에 이용하세요.';
$string['subtitle_startendtime_error'] = '시작시간이 끝 시간보다 크거나 같습니다. 다시 설정해주세요.';

$string['timeset'] = '시간설정';

$string['insert_file'] = '콘텐츠';
$string['error:notselectfile'] = '파일을 선택해주세요.';

$string['accesssettings'] = '강의 콘텐츠 접근 설정';
$string['accesssettings_description'] = '조교가 나의 강의 콘텐츠에 접근할 수 있도록 허용합니다.';
$string['allowtaaccess'] = '조교 접근 허용';

$string['completionprogress'] = '학생이 완료해야 할 진도율(%):';
$string['completionprogressgroup'] = '진도 확인';
$string['error:completionprogress'] = '1 과 100 사이의 값이어야 합니다.';
$string['captionpreview'] = "자막미리보기";

$string['search'] = '검색';
$string['total_page'] = '총 페이지 수';
$string['view_type'] = '표시방식';
$string['popup'] = '팝업';
$string['link'] = '페이지 이동';
$string['my_contents'] = '나의 콘텐츠';
$string['share_contents'] = '공유 콘텐츠';
$string['refresh'] = '새로고침';
$string['select'] = '선택';
$string['add_contents'] = '콘텐츠 등록';
$string['edit_contents'] = '콘텐츠 등록';