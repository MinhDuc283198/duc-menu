<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$ccate = optional_param('category', 0, PARAM_INT);     
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string
$share = optional_param('share', '', PARAM_RAW);
$currentlang = strtoupper(current_language());
if ($currentlang == 'ZH_CN') {
    $currentlang = 'ZH';
}

$query = "select lc.id,lcl.name from {lmsdata_user} lu 
                                  join {lmsdata_categories} lc on lc.corporationcode = lu.corporationcode and lc.isused = 'Y' and lc.id = :pcate  
                                  join {lmsdata_categories_lang} lcl on lcl.corporationcode = lc.corporationcode  and lcl.codevalue = lc.codevalue and langcode = :lang 
                                  where lc.code = 'CURRICULUM_CLASS_TAG_L2_CODE' ";
$params = array('lang'=>$currentlang,'pcate'=>$ccate); 
$cate = $DB->get_record_sql($query,$params);
if($share != 'Y'){
$cnt = $DB->count_records('lcms_contents',array('major_cd'=>$ccate));
} else {
    $cnt = $DB->count_records('lcms_contents',array('major_cd'=>$ccate,'share_yn'=>'Y'));
}
?>
<div class="header">
    <h3><?php echo $cate->name; ?>(<?php echo $cnt; ?>)</h3>
        <button class="reload"
            <?php if ($share != 'Y') { ?>
            onclick="select_category('<?php echo $ccate; ?>');" 
            <?php } else { ?>
            onclick="select_share_category('<?php echo $ccate; ?>');" 
            <?php } ?>
            title="reload"><?php echo get_string('refresh', 'lcms'); ?></button>
</div>
<div class="scrolly">
    <?php
    if($share == 'Y'){
    $sql = "select "
            . "lc.id,lc.con_name,con_total_time, lr.id as lrid,lrg.name as gname,lc.teacher ,lc.share_yn,lc.con_type,lc.con_name,lc.reg_dt,lr.referencecnt ,f.filename "
            . "from {lcms_repository} lr "
            . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
            . "join {lcms_contents} lc on lc.id=lr.lcmsid and share_yn = 'Y' and con_type = 'html' "
            . "join {lcms_contents_file} f on f.con_seq = lc.id "
            . "where lc.major_cd = :codeid ";
    $contents = $DB->get_records_sql($sql, array('userid' => $USER->id, 'codeid' => $ccate));
    } else {
        $sql = "select "
            . "lc.id,lc.con_name,con_total_time, lr.id as lrid,lrg.name as gname,lc.teacher ,lc.share_yn,lc.con_type,lc.con_name,lc.reg_dt,lr.referencecnt ,f.filename "
            . "from {lcms_repository} lr "
            . "left join {lcms_repository_groups} lrg on lrg.id = lr.groupid "
            . "join {lcms_contents} lc on lc.id=lr.lcmsid and course_cd = :userid and con_type = 'html' "
            . "join {lcms_contents_file} f on f.con_seq = lc.id "
            . "where lc.major_cd = :codeid ";
    $contents = $DB->get_records_sql($sql, array('userid' => $USER->id, 'codeid' => $ccate));
    }
    foreach ($contents as $content) {
        ?>
        <div class="col_list">
            <div class="info">
                <h3 class="title"><?php echo $content->con_name; ?></h3>
                <span class="date"><?php echo date('Y-m-d H:i', $content->reg_dt); ?></span>
                <sapn class="name"><?php echo $content->teacher; ?></span>
            </div>
            <div class="buttons"> 
                <p class="ok" onclick="select_content('<?php echo $content->id; ?>', '<?php echo $content->con_name; ?>', '<?php echo $content->con_total_time; ?>')"><?php echo get_string('select', 'lcms'); ?></p>
                <p class="edit" onclick="edit_html('<?php echo $content->id; ?>')"><?php echo get_string('edit','lcms'); ?></p>
            </div>
        </div>
    <?php 
    } 
    ?>
    <?php
    if (!$contents) {
        ?>
        <div class="nolist">
            등록된 파일이 없습니다.
        </div>
    <?php } ?>
</div>