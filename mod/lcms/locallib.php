<?php

function lcms_get_js_module() {
    global $PAGE;

    return array(
        'name' => 'mod_lcms',
        'fullpath' => '/mod/lcms/module.js',
        'requires' => array('base'),
        'strings' => array(
            array('close', 'lcms'),
        ),
    );
}

function lcms_get_js_module_package2() {
    global $PAGE;

    return array(
        'name' => 'mod_lcms_package2',
        'fullpath' => '/mod/lcms/package2.js',
        'requires' => array('base'),
        'strings' => array(
            array('close', 'lcms'),
        ),
    );
}
function lcms_set_mainfile($lcms) {
    global $DB;
    $fs = get_file_storage();
    $cmid = $lcms->coursemodule;
    $draftitemid = $lcms->subfiles;
    $draftitem = file_get_drafarea_files($draftitemid);   

    //$context = context_module::instance($cmid);
    $context = context_system::instance();
    if($draftitem){
        file_save_draft_area_files($draftitemid, $context->id, 'local_repository', 'subtitle', $lcms->contents, array('subdirs'=>true));
    }
    $files = $fs->get_area_files($context->id, 'local_repository', 'subtitle', $lcms->contents, 'sortorder', false);
    
    if (count($files) == 1) {
        // only one file attached, set it as main file automatically
        $file = reset($files);
        file_set_sortorder($context->id, 'local_repository', 'subtitle', $lcms->contents, $file->get_filepath(), $file->get_filename(), 1);
    }
    
}

function lcms_update_subtitle($lcms) {
    
    global $CFG, $USER;
    
    $fs = get_file_storage();
    $context = context_system::instance();
    $captions = $fs->get_area_files($context->id, 'local_repository', 'subtitle', $lcms->contents);
    $pattern1 = "/[_]{1}en[.]{0,1}srt\z$/";
    $pattern2 = "/[_]{1}ko[.]{0,1}srt\z$/";
    
    if (count($captions) > 0) {
        foreach($captions as $caption){            
            $captionname = $caption->get_filename();
            $captiontype = $caption->get_mimetype();
            $content = $caption->get_content();
            if(preg_match($pattern1, $captionname)){
                lcms_update_subtitle_insert($lcms->id,$content,'en');
            }
            if(preg_match($pattern2, $captionname)){
                lcms_update_subtitle_insert($lcms->id,$content,'ko');
            }
        }
    }
}

function lcms_update_subtitle_insert($id,$content,$lang){
    
    global $DB, $USER;
    
    if ($content) {

        $content_br = preg_replace('/\r\n|\r|\n/','<br/>',$content); 
        $lines = explode('<br/><br/>',$content_br);
        

        foreach ($lines as $line) {
            
            $numb = 0;
            
            $srts = explode('<br/>',$line);
            $sub = new stdClass();
            $sub->lang = $lang;
            $sub->lcms_id = $id;
            $sub->userid = $USER->id;
            $sub->timecreated = time();
            $sub->caption = '';
            
            foreach($srts as $key=>$val){
                $numb++;
                if($key==1){ //시간값
                    list($sub->startTime, $sub->stopTime) = explode(' --> ', $val);
                    $starttime = substr($sub->startTime, 0,8);
                    $stoptime = substr($sub->stopTime, 0,8);
                    if(preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',$starttime) && preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',$stoptime)){
                        $sub->stime = seconds_from_time($starttime);
                        $sub->etime = seconds_from_time($stoptime);
                    }else{
                        continue;
                    }            
                }
                if($key > 1 && trim($val)){
                    $sub->caption .= trim($val);
                    if($key < $numb) $sub->caption .= PHP_EOL;
                }
            }
            
            if($sub->stime >= 0 && $sub->etime >= 0 && $sub->caption){
                $subid = $DB->insert_record('lcms_caption',$sub);
            }
            
        }
    }
    
    return $subid;
}
