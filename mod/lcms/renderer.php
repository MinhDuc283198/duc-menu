<?php

defined('MOODLE_INTERNAL') || die();

class mod_lcms_renderer extends plugin_renderer_base {
    function view($lcms, $cm, $course) {
        global $CFG;
        
        $width = 600;
        $height = 400;
        
        //echo '<button id="lcms_viewcontent" onclick="window.open(\''.$lcms->viewurl.'\', \''.$lcms->name.'\', \'toolbar=0,scrollbars=0,location=0,menubar=0,status=0,width='.$width.',height='.$height.'\'); return false;">'.get_string('viewcontent', 'lcms').'</button>';
        echo '<button id="lcms_viewcontent" onclick="window.open(\''.$CFG->wwwroot.'/mod/lcms/play.php?id='.$lcms->id.'\', \'lcms\', \'toolbar=0,scrollbars=0,location=0,menubar=0,status=0,resizable=yes,width='.$width.',height='.$height.'\'); return false;">'.get_string('viewcontent', 'lcms').'</button>';
    }
}
?>
