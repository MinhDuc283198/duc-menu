<?php

require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
global $CFG;
@header('Content-type: application/json; charset=utf-8');
$returnvalue = new stdClass();

$amode = required_param('amode', PARAM_RAW);
$lang = required_param('lang', PARAM_RAW);
$lcmsid = required_param('lcmsid',PARAM_INT);

if ($amode == 'get') {
    
    $sql = 'select * from {lcms_caption} where lcms_id = :lcmsid and lang = :lang order by stime, etime asc';
    $captions = $DB->get_records_sql($sql,array('lcmsid' => $lcmsid, 'lang' => $lang));

    $num = 1;
    $caption_table = '<ol class="subtitle_list">';
    foreach ($captions as $key => $caption) {
        $stimeobj = time_from_seconds($caption->stime);
        $etimeobj = time_from_seconds($caption->etime);

        $caption->caption = preg_replace('/\r\n|\r|\n/', '', $caption->caption);
        $caption->caption = stripslashes($caption->caption);

        $caption_table .= '<li class="subtitle">
             <a id="a' . $num . '" />
             <p class="subtitle_time">
             <input type="text" maxlength="2" class="w_15" id="stime_hh_' . $num . '" readonly value="' . $stimeobj->h . '"> : 
             <input type="text" maxlength="2" class="w_15" id="stime_mm_' . $num . '" readonly value="' . $stimeobj->m . '"> : 
             <input type="text" maxlength="2" class="w_15" id="stime_ss_' . $num . '" readonly value="' . $stimeobj->s . '">
              ~ 
             <input type="text" maxlength="2" class="w_15" id="etime_hh_' . $num . '" readonly value="' . $etimeobj->h . '"> : 
             <input type="text" maxlength="2" class="w_15" id="etime_mm_' . $num . '" readonly value="' . $etimeobj->m . '"> : 
             <input type="text" maxlength="2" class="w_15" id="etime_ss_' . $num . '" readonly value="' . $etimeobj->s . '">
             </p>
             <p class="subtitle_text" data-start="' . $caption->stime . '" data-end="' . $caption->etime . '">
             <textarea id="textprint_' . $num . '" rows="1"  readonly>' . $caption->caption . '</textarea>
             </p>';
        $caption_table .= '<p class="subtitle_edit">
             <input type="button" class="small_btn" value="편집" edit="true" onclick="caption_update($(this),\'ko\',' . $num . ')">
             <input type="button" class="small_btn" value="신고" edit="true" onclick="caption_police(\'' . $caption->stime . '\',\'' . $caption->etime . '\',\'' . $caption->caption . '\',\'ko\','.$num.');">
             </p>';
        
        $caption_table .= '</li>';
        $num++;
    }
    
    $caption_table .= '</ol>';
    $returnvalue->captions = $caption_table;
        
} else {
    $contextid = required_param('contextid',PARAM_INT);
    $contentid = required_param('contentid',PARAM_INT);
    $num = optional_param('num', 0, PARAM_INT);
    $stime_hh = trim(optional_param('stime_hh', 0, PARAM_INT));
    $stime_mm = trim(optional_param('stime_mm', 0, PARAM_INT));
    $stime_ss = trim(optional_param('stime_ss', 0, PARAM_INT));
    $etime_hh = trim(optional_param('etime_hh', 0, PARAM_INT));
    $etime_mm = trim(optional_param('etime_mm', 0, PARAM_INT));
    $etime_ss = trim(optional_param('etime_ss', 0, PARAM_INT));
    $textprint = trim(optional_param('textprint','',PARAM_RAW));
    $userid = $USER->id;

    if(!$userid){
        $err_msg = '로그인 후에 이용하세요.';
    }

    if(!$textprint){
        $err_msg = '자막 내용을 입력하세요.';
    }

    $stime = seconds_from_hms($stime_hh, $stime_mm, $stime_ss);     //초단위로 환산
    $stimeobj = time_from_seconds($stime);
    
    $etime = seconds_from_hms($etime_hh, $etime_mm, $etime_ss);
    $stimeobj = time_from_seconds($etime);

    if ($stime > $etime) {
        $err_msg = '시작시간이 끝 시간보다 크거나 같습니다. 다시 설정해주세요.';
    }
    
    if (!empty($err_msg)) {
        $returnvalue->status = 'error';
        $returnvalue->message = $err_msg;
        echo json_encode($returnvalue);
        die;
    }
    
    $newcaption = new Stdclass();
    $newcaption->lcms_id = $lcmsid;
    $newcaption->stime = $stime;
    $newcaption->etime = $etime;
    $newcaption->caption = $textprint;
    $newcaption->lang = $lang;
    $newcaption->userid = $userid;
    $newcaption->timemodified = time();
  
    $history = new Stdclass();
    if($amode === 'add') {
        $newcaption->timecreated = time();
        $captionid = $DB->insert_record('lcms_caption', $newcaption);
        
        $history->captionid = $captionid;
        
    } else if($amode === 'edit'){
        
        $caption = $DB->get_records('lcms_caption', array('lcms_id' => $lcmsid, 'lang' => $lang), ' stime, etime ASC ', '*', $num-1, 1);
        $editcaption = array_pop($caption);
        $newcaption->id = $editcaption->id;
        $DB->update_record('lcms_caption', $newcaption);
        
        $history->captionid = $editcaption->id;
    }
    
    $history->userid = $userid;
    $history->stime = $stime;
    $history->etime = $etime;
    $history->caption = $textprint;
    $history->lang = $lang;
    $history->timereg = time();
    $history->lcmsid = $lcmsid;
    $history->num = $num;
    $history->type = $amode;
    $DB->insert_record('lcms_caption_history', $history);
    
    lcms_caption_put_srt($contextid, $lcmsid, $contentid, $lang);
}

$returnvalue->status = 'success';

echo json_encode($returnvalue);
?>

