<?php
require_once('../../config.php');

$id = required_param('id', PARAM_INT);
$type = required_param('type', PARAM_ALPHA);
$userid  = optional_param('u', 0, PARAM_INT);

if($userid<=0) $userid = $USER->id; 

@header('Content-type: text/html; charset=utf-8');

$lcms = $DB->get_record('lcms',array('id'=>$id));

$query = 'select * from {lcms_playtime} where userid=:userid and lcmsid=:lcmsid order by id asc';
$logs = $DB->get_records_sql($query,array('userid'=>$userid,'lcmsid'=>$id));

?>
<!DOCTYPE html>

<!--[if lt IE 7]> <html lang="ko" class="ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="ko" class="ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="ko" class="ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="ko" class="ie9"> <![endif]-->
<!--[if IE 10]>    <html lang="ko" class="ie10"> <![endif]-->
<!--[if gt IE 10]><!--> <html dir="ltr" lang="ko" xml:lang="ko"> <!--<![endif]-->
<head>
    <title><?php echo get_string('progresslog','lcmsprogress');?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<style>
    body{font-size:12px;}
    th{padding:5px 0; background:#eee; text-align:center;}
    td{padding:5px 0; border-bottom:1px dotted #eee; text-align:center;}
</style>    
</head>
<body>
    <div style="padding: 5px; font-weight: bold;"><?php echo $lcms->name;?></div>
<?php

if ($lcms->type == 'video'||$lcms->type == 'embed' || $lcms->type == 'html') {
    echo '<table width="100%" style="border:1px solid #ccc;"><tr>';
    echo '<th>'.get_string('starttime','lcmsprogress').'</th><th>'.get_string('enddtime','lcmsprogress').'</th><th>'.get_string('studytime','lcmsprogress').'</th>';
    echo '</tr>';
    foreach ($logs as $log) {
        $postionfrom = floor($log->positionfrom/60).':'.($log->positionfrom%60);
        $postionto = floor($log->positionto/60).':'.($log->positionto%60);
        echo '<tr><td>' . $postionfrom . '</td>
        <td>' . $postionto . '</td>
        <td>' . date('Y-m-d H:i:s', $log->timecreated) . '</td></tr>';
    }
    echo '</table>';
}
?>
</body>
</html>
