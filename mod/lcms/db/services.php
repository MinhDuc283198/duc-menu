<?php

defined('MOODLE_INTERNAL') || die;

$functions = array(
    'mod_lcms_view_lcms' => array(
        'classname'     => 'mod_lcms_external',
        'methodname'    => 'view_lcms',
        'description'   => '',
        'type'          => 'write',
        'capabilities'  => 'mod/lcms:view'
    ),
    
    'mod_lcms_get_lcmses_by_courses' => array(
        'classname'     => 'mod_lcms_external',
        'methodname'    => 'get_lcmses_by_courses',
        'description'   => '',
        'type'          => 'read',
        'capabilities'  => ''
    ),
);
