<?php

defined('MOODLE_INTERNAL') || die();

global $DB;

$logs = array(
    array('module'=>'lcms', 'action'=>'add', 'mtable'=>'lcms', 'field'=>'name'),
    array('module'=>'lcms', 'action'=>'update', 'mtable'=>'lcms', 'field'=>'name'),
    array('module'=>'lcms', 'action'=>'view', 'mtable'=>'lcms', 'field'=>'name')
);