<?php

function xmldb_lcms_upgrade($oldversion=0) {
    global $CFG, $DB, $OUTPUT;

    $dbman = $DB->get_manager(); 
      
    //여기에 변경이력을 기록한다.
    if($oldversion < 2015071000) {
        $table = new xmldb_table('lcms');
        
        $field = new xmldb_field('completionprogress', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '100');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    if($oldversion < 2016062100) {
        $table = new xmldb_table('lcms');
        
        $field = new xmldb_field('subtitleviewtype', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    if($oldversion < 2016062200) {
        
        $table = new xmldb_table('lcms_playtime'); 
        
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('device', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $table = new xmldb_table('lcms_caption'); 

        $field = new xmldb_field('caption_ko');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, null, null, '');
            $dbman->rename_field($table, $field, 'caption');
        } else {
            $field = new xmldb_field('caption');
            $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, null, null, '');
            if (!$dbman->field_exists($table, $field)) $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('caption_en');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '10', null, null, null, '');
            $dbman->rename_field($table, $field, 'lang');
        } else {
            $field = new xmldb_field('lang');
            $field->set_attributes(XMLDB_TYPE_CHAR, '10', null, null, null, '');
            if (!$dbman->field_exists($table, $field)) $dbman->add_field($table, $field);
        }        
        $field = new xmldb_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $index = new xmldb_index('lang', XMLDB_INDEX_NOTUNIQUE, array('lang'));
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
        
        $table = new xmldb_table('lcms_caption_history'); 
        
        $field = new xmldb_field('captionid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $index = new xmldb_index('lang', XMLDB_INDEX_NOTUNIQUE, array('lang'));
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
        $index = new xmldb_index('captionid', XMLDB_INDEX_NOTUNIQUE, array('captionid'));
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
    }
    
    if($oldversion < 2016062802) {
        $table = new xmldb_table('lcms_caption'); 
        $field = new xmldb_field('stime');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '20', null, null, null, '');
            $dbman->change_field_type($table, $field, 'stime');
        } else {
            $field = new xmldb_field('stime');
            $field->set_attributes(XMLDB_TYPE_CHAR, '20', null, null, null, '');
            if (!$dbman->field_exists($table, $field)) $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('etime');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '20', null, null, null, '');
            $dbman->change_field_type($table, $field, 'etime');
        } else {
            $field = new xmldb_field('etime');
            $field->set_attributes(XMLDB_TYPE_CHAR, '20', null, null, null, '');
            if (!$dbman->field_exists($table, $field)) $dbman->add_field($table, $field);
        }
    }
      if ($oldversion < 2016121600) {
         $table = new xmldb_table('lcms');
         $field = new xmldb_field('ispopup', XMLDB_TYPE_INTEGER, '1', null, null, null, '1');

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017110200) {
        // Define table lcms_duration to be created.
        $table = new xmldb_table('lcms_duration');

        // Adding fields to table lcms_duration.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('lcmsid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('page', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('duration', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table lcms_durationn.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for lcms_duration.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
    if($oldversion < 2017110300){
        $table = new xmldb_table('lcms_duration'); 
        $field = new xmldb_field('lcmsid');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '10', null, null, null, 0);
            $dbman->rename_field($table, $field, 'contents');
        } else {
            $field = new xmldb_field('contents');
            $field->set_attributes(XMLDB_TYPE_CHAR, '10', null, null, null, 0);
            if (!$dbman->field_exists($table, $field)) $dbman->add_field($table, $field);
        }
    }
    
    if ($oldversion < 2017112100) {
         $table = new xmldb_table('lcms_duration');
         $field = new xmldb_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
         if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
         }
         $field = new xmldb_field('progress', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
         if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
         }
    }
    
    
    return true;
}
