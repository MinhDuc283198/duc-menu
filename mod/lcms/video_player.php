<?php
require_once '../../config.php';
require_once($CFG->dirroot . '/local/repository/config.php');
require_once($CFG->dirroot . '/local/repository/lib.php');
require_once 'lib.php';

$id = required_param('id', PARAM_INT);
$qua = optional_param('qua', 0, PARAM_INT);
//$filehash = optional_param('filehash','', PARAM_RAW);
//if($filehash) $filedir = '/filedir/'. substr($filehash, 0,2) . '/' . substr($filehash, 2, 2) . '/' . $filehash;
$PAGE->set_url('/mod/lcms/video_player.php', array('id' => $id));
$PAGE->set_pagelayout('embedded');

require_login();

echo $OUTPUT->header();

if ($id) {
//    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
//        print_error('Course Module ID was incorrect');
//    }
//        if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
//        print_error('course module is incorrect');
//    }
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $id))) {
        print_error('lcms contents is incorrect');
    }
    if ($contents->con_type!='embed' && !$file = $DB->get_record('lcms_contents_file', array('con_seq' => $contents->id))) {
        print_error('lcms contents file is incorrect');
    } 
} else {
    print_error('missingparameter');
}

//자막파일영역
$context = context_module::instance($cm->id);
$subtitles = lcms_get_subtitle_list($context->id, $contents->id);

//파일 마감..(0: 숨기기, 1: 보기(진도체크불가), 2: 보기(진도체크가능)
$current = time();
$islock = 2;
if($current<$lcms->timestart||$current>$lcms->timeend){
    $islock = $lcms->islock;
}
//자막 보기 형식
$subtitleviewtype = $lcms->subtitleviewtype;

?>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/local/repository/viewer/jwplayer/js/jwplayer.js"></script>
<?php if ($contents->embed_type == 'youtube') { ?>
    <script type="text/javascript" src="player/youtube.js"></script>
<?php } else if ($contents->embed_type == 'vimeo') { ?>
    <script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>
<?php } ?>
<script type="text/javascript" src="video_player.js"></script>
    
<form name="playtime_form" id="playtime_form">
    <input type="hidden" name="act"/>
    <input type="hidden" name="positionpage" value="1"/>
    <input type="hidden" name="positionfrom"/>
    <input type="hidden" name="positionto"/>
    <input type="hidden" name="duration"/>
    <input type="hidden" name="lcmsid" value="<?php echo $lcms->id;?>"/>
</form>

<?php
if ($islock > 0) {
    echo '<div id="player_area">
    <div id="mediaplayer"><p>'.get_string('waitloading','lcms').'</p></div>
    </div>';
    if ($contents->con_type == 'video') {
        $namearray = explode('.',$file->filepath.'/'.$file->filename);
        $n = count($namearray);
        unset($namearray[$n-1]);
        $capture = ''; // 'http://open.jinotech.com:20080/uploads/'.implode('',$namearray).'.png';
        echo '<script type="text/javascript">
            
    videoplayer(' . $lcms->id . ',"' . MEDIA . '","uploads/' . $file->filepath . '","' . $file->filename . '","' . $contents->author . '","' . $contents->cc_mark . '","'.$capture.'","' . $subtitles->path_ko . '","' . $subtitles->path_en . '",' . $islock . ');
    </script>';
    } else if ($contents->con_type == 'embed') {

        if ($contents->embed_type == 'youtube') {
            $youtubeid = lcms_get_youtubeid($contents->embed_code);
            echo '<script type="text/javascript">
            youtubeplayer(' . $lcms->id . ',"' . $youtubeid . '","' . $contents->author . '","' . $contents->cc_mark . '","","","' . $subtitles->path_ko . '","' . $subtitles->path_en . '",' . $islock . ');
            </script>';
        } else {
            echo '<script type="text/javascript">
            vimeoplayer(' . $lcms->id . ',"' . $contents->embed_code . '","' . $contents->author . '","' . $contents->cc_mark . '","","","' . $subtitles->path_ko . '","' . $subtitles->path_en . '",' . $islock . ');
            </script>';
        }
    }
} else {
    echo '<div id="player_area">
    <div id="mediaplayer"><p>'.get_string('nostudytime','lcms').'</p></div>
    </div>';
}
?>

<?php
    echo $OUTPUT->footer();
?>