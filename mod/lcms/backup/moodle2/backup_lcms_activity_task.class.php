<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/lcms/backup/moodle2/backup_lcms_stepslib.php');
require_once($CFG->dirroot . '/mod/lcms/backup/moodle2/backup_lcms_settingslib.php');

class backup_lcms_activity_task extends backup_activity_task {
    protected function define_my_settings() {
        
    }

    protected function define_my_steps() {
        $this->add_step(new backup_lcms_activity_structure_step('lcms_structure', 'lcms.xml'));
    }
    
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of lcmss
        $search="/(".$base."\/mod\/lcms\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSINDEX*$2@$', $content);

        // Link to lcms view by cmm->id
        $search="/(".$base."\/mod\/lcms\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSVIEWBYID*$2@$', $content);
        
        // Link to lcms view by lcms->id
        $search="/(".$base."\/mod\/lcms\/view.php\?c\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSVIEWBYC*$2@$', $content);

        return $content;
    }
}