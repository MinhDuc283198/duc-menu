<?php

class backup_lcms_activity_structure_step extends backup_activity_structure_step {
    protected function define_structure() {
        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');
        
// Define each element separated
        $lcms = new backup_nested_element('lcms', array('id'), array(
            'name', 'intro', 'introformat',
            'contents', 'timecreated', 'timemodified', 'type', 'viewcount', 'progress', 'completionprogress', 'timestart', 'timeend', 'islock'));
        
        $tracks = new backup_nested_element('tracks');
        $track  = new backup_nested_element('track', array('id'), array(
            'userid', 'timeview', 'playtime', 'progress', 'attempts', 'playpage', 'lasttime', 'lastpage'));
        
        $playtimes = new backup_nested_element('playtimes');
        $playtime = new backup_nested_element('playtime', array('id'), array(
            'userid', 'positionfrom', 'positionto', 'positionevent', 'timecreated'));
        
// Build the tree
        $lcms->add_child($tracks);
        $tracks->add_child($track);
        
        $lcms->add_child($playtimes);
        $playtimes->add_child($playtime);
        
// Define sources
        $lcms->set_source_table('lcms', array('id' => backup::VAR_ACTIVITYID));
        
        if ($userinfo) {
            $track->set_source_table('lcms_track', array('lcms' => backup::VAR_PARENTID));
            $playtime->set_source_table('lcms_playtime', array('lcmsid' => backup::VAR_PARENTID));
        }
        
// Define id annotations
        $track->annotate_ids('user', 'userid');
        $playtime->annotate_ids('user', 'userid');
        
// Define file annotations
        $lcms->annotate_files('mod_lcms', 'intro', null); // This file area hasn't itemid
        
        // Return the root element (lcms), wrapped into standard activity structure
        return $this->prepare_activity_structure($lcms);
    }
}
