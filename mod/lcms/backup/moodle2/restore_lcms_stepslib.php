<?php

class restore_lcms_activity_structure_step extends restore_activity_structure_step {
    protected function define_structure() {
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');
        
        $paths[] = new restore_path_element('lcms', '/activity/lcms');
        if ($userinfo) {
            $paths[] = new restore_path_element('lcms_track', '/activity/lcms/tracks/track');
            $paths[] = new restore_path_element('lcms_playtime', '/activity/lcms/playtimes/playtime');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    protected function process_lcms($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the lcms record
        $newitemid = $DB->insert_record('lcms', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }
    
    protected function process_lcms_track($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->lcms = $this->get_new_parentid('lcms');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timeview = $this->apply_date_offset($data->timeview);

        $newitemid = $DB->insert_record('lcms_track', $data);
    }
    
    protected function process_lcms_playtime($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->lcmsid = $this->get_new_parentid('lcms');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timereg = $this->apply_date_offset($data->timereg);

        $newitemid = $DB->insert_record('lcms_playtime', $data);
    }
    
    protected function after_execute() {
        // Add lcms related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_lcms', 'intro', null);
    }
}