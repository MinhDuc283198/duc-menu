<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once 'locallib.php';
require_once $CFG->dirroot . '/local/repository/lib.php';

$id = required_param('id', PARAM_INT);          // Course Module ID
$c = optional_param('c', 0, PARAM_INT);        // LCMS ID
$inpopup = optional_param('inpopup', 0, PARAM_BOOL);

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['c'] = $c;
}

$PAGE->set_url('/mod/lcms/view.php', $params);

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('course module is incorrect');
    }
} else if ($c) {
    if (!$lcms = $DB->get_record("lcms", array("id" => $c))) {
        print_error('invalidlcmsid', 'lcms');
    }
    if (!$course = $DB->get_record("course", array("id" => $lcms->course))) {
        print_error('coursemisconf');
    }
    if (!$cm = get_coursemodule_from_instance("lcms", $lcms->id, $course->id)) {
        print_error('missingparameter');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);
$strlcmses = get_string("modulenameplural", "lcms");
$strlcms = get_string("modulename", "lcms");

$context = context_module::instance($cm->id);
$PAGE->set_context($context);

$eventparams = array(
    'context' => $context,
    'objectid' => $lcms->id
);
$event = \mod_lcms\event\course_module_viewed::create($eventparams);
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('lcms', $lcms);
$event->trigger();

//    $completion = new completion_info($course);
//    $completion->set_module_viewed($cm);

$output = $PAGE->get_renderer('mod_lcms');

/// Print header.

if ($inpopup and $lcms->type == 'html') {
    $PAGE->set_pagelayout('popup');
    $PAGE->set_title($course->shortname.': '.$lcms->name);
    $PAGE->set_heading($course->fullname);
} else {
    $PAGE->set_title($course->shortname.': '.$lcms->name);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($lcms);
}

echo $output->header();

if (empty($cm->visible) and !has_capability('moodle/course:viewhiddenactivities', $context)) {
    notice(get_string("activityiscurrentlyhidden"));
}
if (!has_capability('mod/lcms:view', $context)) {
    notice(get_string('noviewpermission', 'lcms'));
}

/// find out current groups mode
groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/lcms/view.php?id=' . $cm->id);
$currentgroup = groups_get_activity_group($cm);
$groupmode = groups_get_activity_groupmode($cm);

/// Okay, we can show the discussions. Log the forum view.
if ($cm->id) {
    add_to_log($course->id, "lcms", "view", "view.php?id=$cm->id", "$lcms->id", $cm->id);
} else {
    add_to_log($course->id, "lcms", "view", "view.php?c=$lcms->id", "$lcms->id");
}

echo $output->heading($strlcmses);

//나의 진도율 표시
$trackcount = $DB->count_records('lcms_track',array('lcms'=>$lcms->id,'userid'=>$USER->id));
$track = $DB->get_record('lcms_track',array('lcms'=>$lcms->id,'userid'=>$USER->id));

//최종학습일
$last = get_string('firststudy','lcms');    

//학습진도율
$rate = '0';

if($trackcount>0){
    
    //최종학습일
    $last = date('Y-m-d H:i:s',$track->timeview);
    $rate = $track->progress;

    if($lcms->type == 'video' || $lcms->type == 'embed' || $lcms->type == 'html'){
    
        //총학습시간
        $total = time_from_seconds($track->playtime);
        $totaltime = $total->h.':'.$total->m.':'.$total->s;
        
    }
    
}

//파일 마감..(0: 숨기기, 1: 보기(진도체크불가), 2: 보기(진도체크가능)
$current = time();
$islock = 2;
if($current<$lcms->timestart||$current>$lcms->timeend){
    $islock = $lcms->islock;
}
//자막 보기 형식
$subtitleviewtype = $lcms->subtitleviewtype;

//타이틀표시
$output_lcms = html_writer::start_tag('div', array('class' => 'board-detail-area'));
$output_lcms .= html_writer::start_tag('div', array('class' => 'detail-title-area'));
$output_lcms .= html_writer::tag('span', $lcms->name, array('class' => 'detail-title'));
$output_lcms .= html_writer::end_tag('div');

$output_lcms .= html_writer::start_tag('div', array('class' => 'detail-progress-area'));
$output_lcms .= html_writer::tag('span', get_string('laststudy','lcms',$last), array('class' => 'detail-laststudy'));
if($lcms->type == 'video' || $lcms->type == 'embed' || $lcms->type == 'html2'){
    $output_lcms .= html_writer::tag('span', get_string('studyrate','lcms',$rate), array('class' => 'detail-studyrate'));
}else{
    $studyyn = ($rate==100)? 'O':'X';
    $output_lcms .= html_writer::tag('span', get_string('studyyn','lcms',$studyyn), array('class' => 'detail-studyrate'));
}
if($totaltime) $output_lcms .= html_writer::tag('span', get_string('totalstudytime','lcms',$totaltime), array('class' => 'detail-totalstudytime'));
$output_lcms .= html_writer::end_tag('div');

$output_lcms .= html_writer::start_tag('div', array('class' => 'detail-contents-area'));
//$output_lcms .= html_writer::tag('span', get_string('modulenameplural', 'lcms'), array('class' => "detail-attachment-title"));

$attfile = '';
if($islock > 0){
//콘텐츠 정보
$contents = $DB->get_record('lcms_contents',array('id'=>$lcms->contents));
insert_lcms_history($contents->id,'Mod Lcms Viewed',2);
if($lcms->type=='word'){
    //파일정보(문서일경우 파일명 가져오기)
    $files = $DB->get_records('lcms_contents_file',array('con_seq'=>$lcms->contents,'con_type'=>$lcms->type));
    foreach ($files as $file) {

        $filename = $file->fileoname;
        $filepath = explode('/',$file->filepath);
        if($filepath[0]=='lms'||$filepath[0]=='lcms') $lcmsdata = '/lcmsdata/';
        else $lcmsdata = '/';
        $mimetype = mime_content_type(STORAGE2.$lcmsdata.$file->filepath.'/'.$file->filename); 
        $path = 'download.php?id='.$id.'&fileid='.$file->id;

        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        $attfile .= '<li>';
        $attfile .= "<a href=\"$path\">$iconimage</a> ";
        $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a></li>", FORMAT_HTML, array('context' => $context))."</li>";

    }
    $output_lcms .= html_writer::tag('ul', $attfile, array('class' => "detail-attachment"));
}else if($lcms->type=='html'){
    //html일경우 콘텐츠 보기
    $viewer_url = 'package.php';
    $width = '1040';
    $height = '720';
    $attfile .= '<li><a href="#viewer" class="blue_btn" onclick="load_viewer_popup('.$id.');">'.get_string('viewcontent','lcms').'</a></li>';
    $output_lcms .= html_writer::tag('ul', $attfile, array('class' => "detail-attachment"));
}else if($lcms->type=='video'){
    //동영상 콘텐츠 보기
    $viewer_url = 'video_player.php?id='.$id;
    $attfile .= '<iframe id="vod_viewer" title="vod" src="'.$viewer_url.'"></iframe>';
    $output_lcms .= html_writer::tag('div', $attfile, array('class' => ""));
 }else if($lcms->type=='embed'){
    //Embed 콘텐츠 보기
    $viewer_url = 'video_player.php?id='.$id;
    $attfile .= '<iframe id="vod_viewer" title="vod" src="'.$viewer_url.'"></iframe>';
    $output_lcms .= html_writer::tag('div', $attfile, array('class' => ""));
}

}else{
    $output_lcms .= html_writer::tag('span', get_string('nostudytime','lcms'), array());
}
$output_lcms .= html_writer::end_tag('div');

//강의설명표시
if (!empty($lcms->intro)) {
    $output_lcms .= html_writer::tag('div', format_module_intro('lcms', $lcms, $cm->id), array('class' => 'detail-contents'));
}

//참고자료
$attfile = '';
$query = "select cf.* from {lcms_contents_file} cf "
        . "join {lcms_contents} c on c.id=cf.con_seq "
        . "join {lcms_repository_reference} rr on rr.lcmsid=c.id "
        . "join {lcms_repository} r on r.id=rr.repository "
        . "where r.lcmsid=:lcmsid and cf.con_type=:type";
$refs = $DB->get_records_sql($query,array('lcmsid'=>$lcms->contents,'type'=>'ref'));
if($refs){
    $output_lcms .= html_writer::start_tag('div', array('class' => 'detail-attachment-area reference-area'));
    $output_lcms .= html_writer::tag('span', get_string('reference', 'lcms'), array('class' => "detail-attachment-title"));
    foreach ($refs as $file) {

        $filename = $file->fileoname;
        $filepath = explode('/',$file->filepath);
        if($filepath[0]=='lms'||$filepath[0]=='lcms') $lcmsdata = '/lcmsdata/';
        else $lcmsdata = '/';
        $filedir = STORAGE2 . $lcmsdata . $file->filepath . '/' . $file->filename;
        $mimetype = mime_content_type($filedir); 
        $path = 'download.php?id='.$id.'&fileid='.$file->id;

        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        $attfile .= '<li>';
        $attfile .= "<a href=\"$path\" target=\"_blank\">$iconimage</a> ";
        $attfile .= format_text("<a href=\"$path\" target=\"_blank\">" . s($filename) . "</a></li>", FORMAT_HTML, array('context' => $context))."</li>";

    }
    $output_lcms .= html_writer::tag('ul', $attfile, array('class' => "detail-attachment"));
    $output_lcms .= html_writer::end_tag('div');
}

$right_btns = html_writer::tag('button', get_string('gotocourselist','lcms'), array('class' => "blue-form", 'style'=>'cursor:pointer; text-align:right !important;', 'onclick' => "location.href='" . $CFG->wwwroot . "/course/view.php?id=" . $course->id . "'"));
$cols = html_writer::tag('div', $right_btns, array('class' => "btn-area btn-area-right",'style'=>'text-align:right !important;')); 
$output_lcms .= html_writer::tag('div', $cols, array('class' => "table-footer-area"));

$output_lcms .= html_writer::end_tag('div');

echo $output_lcms;
//자막 수정이 가능할 경우 편집모두가 뜨게 처리
if($subtitleviewtype == 1){
    include_once 'caption_viewmode.php';
}else if($subtitleviewtype == 2){
    include_once 'caption_editmode.php';
}

?>
<script type="text/javascript">
    function load_viewer_popup(id,qua){
       
        var width = '<?php echo $width;?>', height = '<?php echo $height;?>';
        if($(window).width()<width) {
            width = $(window).width();
        }
        
        if($(window).height()<height) {
            height = $(window).height();
        }
            
        var tag = $("<div id='viewer_popup' style='overflow:hidden;'></div>");
        
        $.ajax({
            url: '<?php echo $viewer_url; ?>',
            data: {
                id: id,
                qua: qua
            },
            success: function(data) {
                    
                $('body').css({'overflow':'hidden'});
                    
                tag.html(data).dialog({
                    title: '<?php echo $contents->con_name; ?>',
                    modal: true,
                    width: width,
                    height: height,
                    close: function () {
                        //if($('video').length==0) jwplayer('mediaplayer').pause(); 
                        $( this ).dialog('destroy').remove();
                        $('body').css({'overflow':'auto'});
                    }
                }).dialog('open');
                    
            }
                
        });
    }
    
    function close_viewer_popup(){
        $('#viewer_popup').dialog('destroy').remove();
        $('body').css({'overflow':'auto'});
    }
    function status_change(status, id) {
        if(status == 3){
             var msg = prompt('보류사유');
        } else {
             var msg = '';
        }
        $.ajax({
            method: "POST",
            url: "./cdms/status_change.ajax.php",
            data: {id: id, status: status , msg:msg}
        })
                .done(function (html) {
                    location.reload();
                });
    }
</script>
<?php
echo $OUTPUT->footer($course);
