<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once '../lcmsprogress/locallib.php';

$id = required_param('id',PARAM_INT);

$PAGE->set_url('/mod/lcms/package2.php', array('id'=>$id));

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcms', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('lcms contents is incorrect');
    }
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $lcms->contents))) {
        print_error('course module is incorrect');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$PAGE->set_context($context);

if($track = $DB->get_record('lcms_track',array('lcms'=>$lcms->id,'userid'=>$USER->id))){
    $track->attempts = $track->attempts+1;
    $track->timeview = time();
    $DB->update_record('lcms_track',$track);
}else{        
    $track = new stdClass();
    $track->lcms = $lcms->id;
    $track->userid = $USER->id;
    $track->timeview = time();
    $track->attempts = 1;
    $track->progress = 0;
    $track->playpage = 0;
    $track->id = $DB->insert_record('lcms_track',$track);
}

lcmsprogress_update_progress_score($lcms->course, $USER->id);

$contents->data_dir = str_replace('storage/','',$contents->data_dir);

?>

<iframe id="package" style="width:100%;height:100%;border:0;overflow:hidden;" src="<?php echo $CFG->wwwroot.'/storage/'.$contents->data_dir.'/index.html';?>"></iframe>
