<?php

class backup_okmedia_activity_structure_step extends backup_activity_structure_step {
    protected function define_structure() { 
        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');
        
// Define each element separated
        $okmedia = new backup_nested_element('okmedia', array('id'), array(
            'name', 'intro', 'introformat',
            'contents', 'timecreated', 'timemodified', 'type', 'viewcount', 'progress', 'completionprogress', 'timestart', 'timeend', 'islock', 'point'));
        
        $tracks = new backup_nested_element('tracks');
        $track  = new backup_nested_element('track', array('id'), array(
            'userid', 'timeview', 'playtime', 'progress', 'attempts', 'playpage', 'lasttime', 'lastpage'));
        
        $playtimes = new backup_nested_element('playtimes');
        $playtime = new backup_nested_element('playtime', array('id'), array(
            'userid', 'positionfrom', 'positionto', 'positionevent', 'timecreated'));
        
// Build the tree
        $okmedia->add_child($tracks);
        $tracks->add_child($track);
        
        $okmedia->add_child($playtimes);
        $playtimes->add_child($playtime);
        
// Define sources
        $okmedia->set_source_table('okmedia', array('id' => backup::VAR_ACTIVITYID));
        
        if ($userinfo) {
            $track->set_source_table('okmedia_track', array('okmediaid' => backup::VAR_PARENTID));
            $playtime->set_source_table('okmedia_playtime', array('okmediaid' => backup::VAR_PARENTID));
        }
        
// Define id annotations
        $track->annotate_ids('user', 'userid');
        $playtime->annotate_ids('user', 'userid');
        
// Define file annotations
        $okmedia->annotate_files('mod_okmedia', 'intro', null); // This file area hasn't itemid
        
        // Return the root element (okmedia), wrapped into standard activity structure
        return $this->prepare_activity_structure($okmedia);
    }
}
