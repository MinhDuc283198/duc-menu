<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/okmedia/backup/moodle2/backup_okmedia_stepslib.php');
require_once($CFG->dirroot . '/mod/okmedia/backup/moodle2/backup_okmedia_settingslib.php');

class backup_okmedia_activity_task extends backup_activity_task {
    protected function define_my_settings() {
        
    }

    protected function define_my_steps() {
        $this->add_step(new backup_okmedia_activity_structure_step('okmedia_structure', 'okmedia.xml'));
    }
    
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of okmedias
        $search="/(".$base."\/mod\/okmedia\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSINDEX*$2@$', $content);

        // Link to okmedia view by cmm->id
        $search="/(".$base."\/mod\/okmedia\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSVIEWBYID*$2@$', $content);
        
        return $content;
    }
}