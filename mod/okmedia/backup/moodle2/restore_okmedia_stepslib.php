<?php

class restore_okmedia_activity_structure_step extends restore_activity_structure_step {
    protected function define_structure() {
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');
        
        $paths[] = new restore_path_element('okmedia', '/activity/okmedia');
        if ($userinfo) {
            $paths[] = new restore_path_element('okmedia_track', '/activity/okmedia/tracks/track');
            $paths[] = new restore_path_element('okmedia_playtime', '/activity/okmedia/playtimes/playtime');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    protected function process_okmedia($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the okmedia record
        $newitemid = $DB->insert_record('okmedia', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }
    
    protected function process_okmedia_track($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->okmediaid = $this->get_new_parentid('okmedia');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timeview = $this->apply_date_offset($data->timeview);

        $newitemid = $DB->insert_record('okmedia_track', $data);
    }
    
    protected function process_okmedia_playtime($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->okmediaid = $this->get_new_parentid('okmedia');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timereg = $this->apply_date_offset($data->timereg);

        $newitemid = $DB->insert_record('okmedia_playtime', $data);
    }
    
    protected function after_execute() {
        // Add okmedia related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_okmedia', 'intro', null);
    }
}