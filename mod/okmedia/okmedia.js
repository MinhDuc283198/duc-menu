$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
            '//jquery-file-upload.appspot.com/' : '/local/repository/contents_upload/server/php/';
    
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                if (file.error) {
                    $('<li/>').html(file.name + '[Error: ' + file.error + ']').appendTo('#files');
                } else {
                    $('<li/>').html('<button type="button" class="btn btn-delete delete" data-type="DELETE" onclick="delete_uploadfile(this)" data-url="' + file.dirroot + '">del</button>' + file.name).appendTo('#files');
                }
            });
        },
        progress: function (e, data) {

        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                    );
        }
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

});


$(".files button.delete").click(function () {

    var filedir = $(this).attr('data-url');
    var papa = $(this).parent();

    $.ajax({
        url: '/local/repository/delete_contents.php',
        type: 'POST',
        async: true,
        data: {
            mode: 'filedel',
            filedir: filedir
        },
        success: function (data, textStatus, jqXHR) {
            papa.hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });

});

$(".cfiles button.delete").click(function () {

    var fileid = $(this).attr('data-url');
    var papa = $(this).parent();

    $.ajax({
        url: '/local/repository/delete_contents.php',
        type: 'POST',
        async: true,
        data: {
            mode: 'cfiledel',
            fileid: fileid
        },
        success: function (data, textStatus, jqXHR) { 
            papa.hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });

});

function delete_uploadfile(obj) {

    var filedir = $(obj).attr('data-url');
    var papa = $(obj).parent();

    $.ajax({
        url: '/local/repository/delete_contents.php',
        type: 'POST',
        async: true,
        data: {
            mode: 'filedel',
            filedir: filedir
        },
        success: function (data, textStatus, jqXHR) {
            papa.hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
};

$("input[name=con_type]").click(function () {
    var radio_val2 = $("input[name=con_type]:checked").val();
    if (radio_val2 == 'word' || radio_val2 == 'html') {
        $('#fgroup_id_attachmentsgroup').show();
        $('#fgroup_id_embedgroup').hide();
        $('#fgroup_id_videogroup').hide();
        $('#id_subtitlesection').hide();
        $('#fitem_id_subfiles').hide();
    } else if (radio_val2 == 'video') {
        $('#fgroup_id_attachmentsgroup').hide();
        $('#fgroup_id_embedgroup').hide();
        $('#fgroup_id_videogroup').show();
        $('#id_subtitlesection').show();
        $('#fitem_id_subfiles').show();
    } else if (radio_val2 == 'embed') {
        $('#fgroup_id_attachmentsgroup').hide();
        $('#fgroup_id_embedgroup').show();
        $('#fgroup_id_videogroup').hide();
        $('#id_subtitlesection').show();
        $('#fitem_id_subfiles').show();
    }
});

$('select[name=emb_type]').change(function(){
    if($(this).val() == 'youtube'){
        $('input[name=emb_search]').show();
        $('input[id=emb_search_btn]').show();
    } else {
        $('input[name=emb_search]').hide();
        $('input[id=emb_search_btn]').hide();
    }
});

