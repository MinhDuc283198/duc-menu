<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('MOODLE_INTERNAL') || die;
//주소 셋팅하는곳
if ($ADMIN->fulltree) {
    // config에 들어가는 값들 
    // admin_setting_configtext(name, title, description, default)
    $settings->add(new admin_setting_configtext('transcodingurl', '트랜스코딩 주소', '', ''));
    $settings->add(new admin_setting_configtext('wowzaurl', 'WOWZA 주소', '',''));
    $settings->add(new admin_setting_configtext('videopath', '동영상 위치', '',''));
}