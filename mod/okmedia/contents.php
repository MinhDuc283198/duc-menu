<?php
require_once '../../config.php';
require_once($CFG->dirroot . "/lib/filelib.php");
require_once $CFG->dirroot . '/local/repository/config.php';
require_once 'lib.php';
require_once '../../enrol/locallib.php';
require_once($CFG->dirroot . '/local/repository/lib.php');

$id = optional_param('id', 0, PARAM_INT); // courseid
$type = optional_param('type', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$categoryid = optional_param('categoryid', 0, PARAM_INT);

$PAGE->set_url('/mod/okmedia/video_player.php', array('id' => $id));
$PAGE->set_pagelayout('embedded');

require_login();

echo $OUTPUT->header();


$course = get_course($id);
$teacher_sql = "select u.*
                                    from {course} c 
                                    join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
                                    join {role_assignments} ra on ra.contextid = ct.id 
                                    join {user} u on u.id = ra.userid  and u.id = :userid 
                                    join {role} ro on ro.id = ra.roleid and (ro.shortname = 'teacher')
                                    where c.id = :courseid";
$teacher = $DB->get_record_sql($teacher_sql, array('courseid' => $id, 'userid' => $USER->id));

$professors_sql = "select u.*
                                    from {course} c 
                                    join {context} ct on ct.contextlevel = 50 and ct.instanceid = c.id 
                                    join {role_assignments} ra on ra.contextid = ct.id 
                                    join {user} u on u.id = ra.userid  
                                    join {role} ro on ro.id = ra.roleid and (ro.shortname = 'editingteacher')
                                    where c.id = :courseid";
$professors = $DB->get_records_sql($professors_sql, array('courseid' => $id));
$where_in = "(";
foreach ($professors as $professor) {
  $where_in .= $professor->id . ",";
}
$where_in = rtrim($where_in, ",");
$where_in .= ")";

$allow_teacher = $DB->get_record('lcms_repository_open', array('course' => $id));
?>
<h3><?php echo get_string('findcontent', 'okmedia'); ?></h3>
<form class="table-search-option" method="get">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <input type="hidden" name="type" value="<?php echo $type; ?>">
    <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
    <select name="categoryid" onchange="this.form.submit();">
        <option value="0">선택해주세요.</option>
	  <?php
	  $categorysql = 'select lcata.id lcataid, lcata.*, lcn.*   
                                from {lmsdata_category} lcata 
                                join {lmsdata_category_name} lcn on lcn.categoryid = lcata.id 
                                where lcn.lang = :lang and lcata.depth = :catadepth';
	  $categoryparam['lang'] = current_language();
	  $categoryparam['catadepth'] = 2;
	  $categories = $DB->get_records_sql($categorysql, $categoryparam);

	  foreach ($categories as $category) {
	    $category_parent = $DB->get_record('lmsdata_category_name', array('categoryid' => $category->parent, 'lang' => current_language()));
	    $selected = '';

	    if ($category->categoryid == $categoryid) {
		$selected = 'selected="selected"';
	    }
	    echo '<option value=" ' . $category->categoryid . ' " ' . $selected . ' >' . ' [ ' . $category_parent->name . ' ] ' . $category->name . '</option>';
	  }
	  ?>
        <!--<option><?php // echo get_string('all', 'local_repository');       ?></option>-->
    </select>
    <input type="text" name="search" value="<?php echo $search; ?>" class="w-300" placeholder="<?php echo get_string('search', 'local_repository'); ?>">
    <input type="submit" class="board-search" value="<?php echo get_string('search', 'local_repository'); ?>" onclick='' />
</form> <!-- Search Area End -->
<table cellpadding="0" cellspacing="0" class="generaltable">
    <thead>
        <tr>
            <th width="6%"><?php echo get_string('list:no', 'local_repository'); ?></th>
            <th width="15%"><?php echo get_string('category', 'local_lmsdata'); ?></th>
            <th><?php echo get_string('list:title', 'local_repository'); ?></th>
            <th><?php echo get_string('contents:code', 'local_management'); ?></th>
            <th width="10%"><?php echo get_string('list:type', 'local_repository'); ?></th>
<!--            <th width="10%"><?php echo get_string('list:reference', 'local_repository'); ?></th>-->
            <th width="10%"><?php echo get_string('list:timecreated', 'local_repository'); ?></th>
            <th width="8%"><?php echo get_string('selectcontent', 'okmedia'); ?></th>
        </tr>
    </thead>
    <tbody>
	  <?php
          if($type != 4 && $type != 3){
              $sql = 'SELECT
                        lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name,lc.con_name_vi, lc.con_name_en, lc.code, lc.update_dt, lr.referencecnt, lc.category, lcn.name 
                        FROM {lcms_repository} lr 
                        JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "video" OR lc.con_type = "embed" OR lc.con_type = "visangurl") AND lc.share_yn="Y" 
                        JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "video" OR lc.con_type = "embed" OR lc.con_type = "visangurl") AND lc.share_yn="Y"  
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
              /*
	  if (is_siteadmin()) {
	    $sql = 'SELECT
                        lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lc.category, lcn.name 
                        FROM {lcms_repository} lr 
                        JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "video" OR lc.con_type = "embed") AND lc.share_yn="Y" 
                        JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "video" OR lc.con_type = "embed") AND lc.share_yn="Y"  
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';

	    // 조교가 비어있고 오픈 노이면     
	  } else if (empty($teacher->id)) {
	    $sql = 'SELECT 
                    lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lcn.name 
                    FROM {lcms_repository} lr 
                    JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "video" OR lc.con_type = "embed") 
                    JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "video" OR lc.con_type = "embed") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	  } else if ($allow_teacher->isopen == "Y") {
	    $sql = 'SELECT 
                    lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lcn.name 
                    FROM {lcms_repository} lr 
                    JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.share_yn = "Y" OR lc.course_cd in ' . $where_in . ') AND (lc.con_type = "video" OR lc.con_type = "embed") 
                    JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.share_yn = "Y" OR lc.course_cd in ' . $where_in . ') AND (lc.con_type = "video" OR lc.con_type = "embed") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	  } else {
	    $sql = 'SELECT 
                    lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lcn.name 
                    FROM {lcms_repository} lr 
                    JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "video" OR lc.con_type = "embed") 
                    JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "video" OR lc.con_type = "embed") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	  }
               * 
               */
	  // or lc.share_yn = :share_yn , 'share_yn' => 'L'
	  
               
          }else{
              /*
              if (is_siteadmin()) {
	    $sql = 'SELECT
                        lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lc.category, lcn.name 
                        FROM {lcms_repository} lr 
                        JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "tplan") 
                        JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "tplan") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';

	    // 조교가 비어있고 오픈 노이면     
	  } else if (empty($teacher->id)) {
	    $sql = 'SELECT 
                    lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lcn.name 
                    FROM {lcms_repository} lr 
                    JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "tplan") 
                    JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "tplan") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	  } else if ($allow_teacher->isopen == "Y") {
	    $sql = 'SELECT 
                    lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lcn.name 
                    FROM {lcms_repository} lr 
                    JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.share_yn = "Y" OR lc.course_cd in ' . $where_in . ') AND (lc.con_type = "tplan") 
                    JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.share_yn = "Y" OR lc.course_cd in ' . $where_in . ') AND (lc.con_type = "tplan") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	  } else {
	    $sql = 'SELECT 
                    lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name, lc.update_dt, lr.referencecnt, lcn.name 
                    FROM {lcms_repository} lr 
                    JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "tplan") 
                    JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	    $count_sql = 'SELECT 
                                COUNT(lr.id) 
                                FROM {lcms_repository} lr 
                                JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.course_cd = :luid OR lc.share_yn = "Y") AND (lc.con_type = "tplan") 
                                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
	  }
               * 
               */
        $sql = 'SELECT
                  lr.id as lrid, lc.id, lc.share_yn, lc.con_type, lc.con_name,lc.con_name_vi, lc.con_name_en, lc.code, lc.update_dt, lr.referencecnt, lc.category, lcn.name 
                  FROM {lcms_repository} lr 
                  JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "tplan") AND lc.share_yn="Y" 
                JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
        $count_sql = 'SELECT 
                            COUNT(lr.id) 
                            FROM {lcms_repository} lr 
                            JOIN {lcms_contents} lc ON lc.id = lr.lcmsid AND (lc.con_type = "tplan") AND lc.share_yn="Y"  
                            JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.category';
        /*
	  // or lc.share_yn = :share_yn , 'share_yn' => 'L'
	  $params = array('luid' => $USER->id);
	  $sort = " order by lc.update_dt desc";
	  $sql_where = ' WHERE lcn.lang = :lcnlang';
	  $params['lcnlang'] = current_language();
	  if ($search) {

	    $conditionname[] = $DB->sql_like('lc.con_name', ':search1', false);
	    $params['search1'] = '%' . $search . '%';
	  }
	  if ($categoryid > 0) {
	    if ($search) {
		$conditionname[] = ' AND lc.category = :search2 ';
	    } else {
		$conditionname[] = ' lc.category = :search2 ';
	    }
	    $params['search2'] = $categoryid;
	  }
	  if ($search || $categoryid > 0) {
	    $sql_where .= ' AND (' . implode(' ', $conditionname) . ')';  // and 조건 where로 수정 
	  }
	  $offset = 0;
	  if ($page != 0) {
	    $offset = ($page - 1) * $perpage;
	  }

	  $totalcount = $DB->count_records_sql($count_sql . $sql_where, $params);
	  $total_pages = repository_get_total_pages($totalcount, $perpage);

	  $files = $DB->get_records_sql($sql . $sql_where . $sort, $params, $offset, $perpage);*/
        
          }
          $params = array('luid' => $USER->id);
	  $sort = " order by lc.update_dt desc";
	  $sql_where = ' WHERE lcn.lang = :lcnlang';
	  $params['lcnlang'] = current_language();
	  if ($search) {
	    $conditionname_search[] = $DB->sql_like('lc.con_name', ':search1');
	    $params['search1'] = '%' . $search . '%';
            $conditionname_search[] = $DB->sql_like('lc.con_name_vi', ':search2');
	    $params['search2'] = '%' . $search . '%';
            $conditionname_search[] = $DB->sql_like('lc.con_name_en', ':search3');
	    $params['search3'] = '%' . $search . '%';
            $conditionname_search[] = $DB->sql_like('lc.code', ':search4');
	    $params['search4'] = '%' . $search . '%';
            $conditionname[] = '(' . implode(' OR ', $conditionname_search) . ')';  // and 조건 where로 수정 
	  }
	  if ($categoryid > 0) {
	    if ($search) {
		$conditionname[] = ' AND lc.category = :search2 ';
	    } else {
		$conditionname[] = ' lc.category = :search2 ';
	    }
	    $params['search2'] = $categoryid;
	  }
	  if ($search || $categoryid > 0) {
	    $sql_where .= ' AND (' . implode(' ', $conditionname) . ')';  // and 조건 where로 수정 
	  }
	  $offset = 0;
	  if ($page != 0) {
	    $offset = ($page - 1) * $perpage;
	  }

	  $totalcount = $DB->count_records_sql($count_sql . $sql_where, $params);
	  $total_pages = repository_get_total_pages($totalcount, $perpage);

	  $files = $DB->get_records_sql($sql . $sql_where . $sort, $params, $offset, $perpage);
          
	  $context = context_system::instance();

	  $num = $totalcount - $offset;
	  foreach ($files as $file) {
	    if ($file->con_type == 'word') {
		$type_txt = get_string('document', 'local_repository');
	    } else if ($file->con_type == 'html') {
		$type_txt = get_string('html', 'local_repository');
	    } else if ($file->con_type == 'video') {
		$type_txt = get_string('video', 'local_repository');
	    } else if ($file->con_type == 'embed') {
		$type_txt = get_string('embed', 'local_repository');
	    }else if($file->con_type == "tplan"){
                $type_txt = get_string('contents:tplan', 'local_management');
            }else if($file->con_type == "visangurl"){
                $type_txt = get_string('contents:visang', 'local_management');
            }
            
            switch (current_language()){
                case 'ko' :
                    $contents_name = $file->con_name;
                    break;
                case 'en' :
                    $contents_name = $file->con_name_en;
                    break;
                case 'vi' :
                    $contents_name = $file->con_name_vi;
                    break;
            }
	    ?>
  	  <tr>
  		<td><?php echo $num; ?></td>
  		<td><?php echo $file->name; ?></td>
  		<td class="title"><?php echo $contents_name; ?></td>
                <td class="title"><?php echo $file->code; ?></td>
  		<td><?php echo $type_txt; ?></td>
          <!--                <td><?php echo $file->referencecnt; ?></td>-->
		  <?php
		  $public = (!$file->share_yn) ? get_string('n', 'local_repository') : get_string('y', 'local_repository');
		  ?>
  		<td class="number"><?php echo date('Y.m.d', $file->update_dt) ?></td>
  		<td class="number"><input type="button" value="<?php echo get_string('selectcontent', 'okmedia'); ?>" onclick='select_file("<?php echo $contents_name; ?>", "<?php echo $file->id ?>", "<?php echo $file->con_type ?>")'></td>
  	  </tr>
	    <?php
	    $num--;
	  }

	  if (!$files) {
	    echo '<tr><td colspan="8">' . get_string('nocontents', 'local_repository') . '</td></tr>';
	  }
	  ?>
    </tbody>
</table>
<div class="table-footer-area">
    <?php
    $page_params = array();
    $page_params['perpage'] = $perpage;
    $page_params['search'] = $search;
    $page_params['id'] = $id;
    $page_params['categoryid'] = $categoryid;
    $page_params['type'] = $type;
    repository_get_paging_bar($CFG->wwwroot . "/mod/okmedia/contents.php", $page_params, $total_pages, $page);
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->

<script>
function select_file(val, no, type) {
    var ptype = $('input[name=type]').val();
    if(ptype == 4){
        $('input[name=tplantitlestatic]', parent.document).val(val);
        $('input[name=contents_no]', parent.document).val(no);
    }else{
        $('input[name=titlestatic]', parent.document).val(val);
        $('input[name=title]', parent.document).val(val);
        $('input[name=contents]', parent.document).val(no);
        $('input[name=type]', parent.document).val(type);
    }
      
    if (type == 'video' || type == 'embed' || type == 'tplan') {
        $('#id_subtitlesection', parent.document).show();
        $('#fitem_id_subfiles', parent.document).hide();
    } else {
        $('#id_subtitlesection', parent.document).hide();
    }
    parent.M.mod_okmedia.close();
}
</script>

<?php
echo $OUTPUT->footer();
?>