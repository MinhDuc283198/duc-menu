<?php

function xmldb_okmedia_upgrade($oldversion=0) {
    global $CFG, $DB, $OUTPUT;

    $dbman = $DB->get_manager(); 
    if ($oldversion < 2017111500) {
        $table = new xmldb_table('okmedia_track');
        $field = new xmldb_field('realtime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    // 한양대 특화, 담당교수 필드 추가 - 2018.09.20 chs
    if ($oldversion < 2018092000) {
        $table = new xmldb_table('okmedia');
        $field = new xmldb_field('profid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    // okmedia index 추가 - 2018.10.17 chs
    if ($oldversion < 2018101700) {
        $table = new xmldb_table('okmedia_playtime');
        $index = new xmldb_index('okmediaid', XMLDB_INDEX_NOTUNIQUE, array('okmediaid'));

        // Conditionally launch add index userid-itemid.
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
    }
    
    // 재능 특화, okmedia에 포인트 설정값 - 2019.02.15 chs
    if ($oldversion < 2019021500) {
        $table = new xmldb_table('okmedia');
        $field = new xmldb_field('point', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
        
 
    return true;
}
