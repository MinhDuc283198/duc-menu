<?php

defined('MOODLE_INTERNAL') || die;

$functions = array(
    'mod_okmedia_view_okmedia' => array(
        'classname'     => 'mod_okmedia_external',
        'methodname'    => 'view_okmedia',
        'description'   => '',
        'type'          => 'write',
        'capabilities'  => 'mod/okmedia:view'
    ),
    
    'mod_okmedia_get_okmedias_by_courses' => array(
        'classname'     => 'mod_okmedia_external',
        'methodname'    => 'get_okmedias_by_courses',
        'description'   => '',
        'type'          => 'read',
        'capabilities'  => 'mod/okmedia:view'
    ),
);
