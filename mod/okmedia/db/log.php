<?php

defined('MOODLE_INTERNAL') || die();

global $DB;

$logs = array(
    array('module'=>'okmedia', 'action'=>'add', 'mtable'=>'okmedia', 'field'=>'name'),
    array('module'=>'okmedia', 'action'=>'update', 'mtable'=>'okmedia', 'field'=>'name'),
    array('module'=>'okmedia', 'action'=>'view', 'mtable'=>'okmedia', 'field'=>'name')
);