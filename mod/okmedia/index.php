<?php

require_once('../../config.php');
 
$id = required_param('id', PARAM_INT);           // Course ID
 
$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$strlcmses = get_string("modulenameplural", "okmedia");
$strlcms = get_string("modulename", "okmedia");
$strsectionname  = get_string('sectionname', 'format_'.$course->format);
$strname         = get_string('name', 'okmedia');
$strintro        = get_string('okmediaintro', 'okmedia');
$strlastmodified = get_string('lastmodified');


$PAGE->set_url('/mod/okmedia/index.php', array('id' => $course->id));
$PAGE->set_title($course->shortname.': '.$strlcmses);
$PAGE->set_heading($course->fullname);
$PAGE->navbar->add($strlcmses);
echo $OUTPUT->header();

if (!$lcmses = get_all_instances_in_course('okmedia', $course)) {
    notice(get_string('thereareno', 'moodle', $strlcmses), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit;
}

$usesections = course_format_uses_sections($course->format);
if ($usesections) {
    $modinfo = get_fast_modinfo($course->id);
    $sections = $modinfo->get_section_info_all();
}

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

$align = array ('center', 'left', 'left');
$headstrs = array ($strsectionname, $strname, $strintro);
if (!$usesections) {
    $headstrs  = array ($strlastmodified, $strname, $strintro);
    $align = array ('left', 'left', 'left');
}

$heads = array();
foreach($headstrs as $key=>$value) {
    $head = new html_table_cell($value);
    $head->style = 'text-align: '.$align[$key].';';
    $heads[] = $head;
}
$table->head = $heads;

$modinfo = get_fast_modinfo($course);
$data = array();
foreach ($lcmses as $lcms) {
    if(!$lcms->visible) {
        continue;
    }
    
    $section = $lcms->section;
    $cm = $modinfo->cms[$lcms->coursemodule];
    
    if(!isset($data[$section])) {
        $data[$section] = array();
    }
    
    if ($usesections) {
        $printsection = '';
        if ($lcms->section !== $currentsection) {
            if ($lcms->section) {
                $printsection = get_section_name($course, $sections[$lcms->section]);
            }
        }
    } else {
        $printsection = '<span class="smallinfo">'.userdate($lcms->timemodified)."</span>";
    }
    
    $encrypted = base64_encode($cm->id.'||player');
    $data[$section][] = array(
        $printsection,
        '<a onclick="window.open(\''.$CFG->wwwroot.'/mod/okmedia/link.php?v='.$encrypted.'\', \'\', \'width=1200,height=800,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=no\'); return false;" href="'.$CFG->wwwroot.'/mod/okmedia/view.php?id='.$cm->id.'">'.format_string($lcms->name).'</a>',
        format_module_intro('okmedia', $lcms, $cm->id)
    );
}

$rows = array();
foreach ($data as $week) {
    $rowspan = count($week);
    $count = 0;
    foreach($week as $lcms) {
        $row = new html_table_row();
        $cells = array();
        
        foreach($lcms as $key=>$value) {
            $style = 'text-align: '.$align[$key].';';
            if($rowspan > 1) {
                if($count == 0 && $key == 0) {
                    $cell = new html_table_cell($value);
                    $cell->rowspan = $rowspan;
                    $cell->style = $style;
                    $cells[] = $cell;
                } else if($key > 0) {
                    $cell = new html_table_cell($value);
                    $cell->style = $style;
                    $cells[] = $cell;
                }
            } else {
                $cell = new html_table_cell($value);
                $cell->style = $style;
                $cells[] = $cell;
            }
            //$cell = new html_table_cell($value);
        } 
        $row->cells = $cells;
        $rows[] = $row;
        
        $count++;
    }
}
$table->data = $rows;

//$modinfo = get_fast_modinfo($course);
//$currentsection = '';
//foreach ($lcmses as $lcms) {
//    $cm = $modinfo->cms[$lcms->coursemodule];
//    if ($usesections) {
//        $printsection = '';
//        if ($lcms->section !== $currentsection) {
//            if ($lcms->section) {
//                $printsection = get_section_name($course, $sections[$lcms->section]);
//            }
//            if ($currentsection !== '') {
//                $table->data[] = 'hr';
//            }
//            $currentsection = $lcms->section;
//        }
//    } else {
//        $printsection = '<span class="smallinfo">'.userdate($lcms->timemodified)."</span>";
//    }
//
//    $class = $lcms->visible ? 'class="dd"' : 'class="dimmed"'; // hidden modules are dimmed
//    
//    $table->data[] = array (
//        $printsection,
//        "<a $class href=\"view.php?id=$cm->id\">".format_string($lcms->name)."</a>",
//        format_module_intro('okmedia', $lcms, $cm->id));
//}

echo $OUTPUT->heading($strlcmses);

echo html_writer::table($table);

echo $OUTPUT->footer();