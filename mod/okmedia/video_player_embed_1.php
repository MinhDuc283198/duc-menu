<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
$id = required_param('id', PARAM_INT);

$PAGE->set_url('/mod/okmedia/video_player_embed.php', array('id' => $id));

if ($id) {
    if (!$cm = get_coursemodule_from_id('okmedia', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) { 
        print_error('course is misconfigured');
    }
    if (!$lcms = $DB->get_record('okmedia', array('id' => $cm->instance))) {
        print_error('course module is incorrect');
    }
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $lcms->contents))) {
        print_error('lcms contents is incorrect');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$PAGE->set_context($context);

if ($contents->embed_type == 'youtube') { 
    //유튜브 데이터
    $vids = explode('/', $contents->embed_code);
    $vid1 = trim(str_replace("watch?v=", "", $vids[sizeof($vids) - 1])); 
    $vid1s = explode('&', $vid1);
    $cid = trim($vid1s[0]);
    $img_nm = 'http://img.youtube.com/vi/'.$cid.'/0.jpg';
}

if ($contents->embed_type == 'vimeo') {

    $embs = explode('/', $contents->embed_code);
    $cid = trim($embs[sizeof($embs) - 1]);
    $embinfo = vimeoinfo($cid);
    $img_nm = $embinfo->thumbnail;
}

$auto_play = 1; //자동재생여부: 1이면 자동재생
$lrn_time = 0; //이어보기여부
if ($track = $DB->get_record('okmedia_track', array('okmediaid' => $lcms->id, 'userid' => $USER->id))) {
    $lrn_time = $track->lasttime;
    $progress = $track->progress;
}

$interval_num = '60'; //재생정보를 저장하는 간격
$status_num = ($lcms->progress == 1 && $progress < 100) ? '01' : '02'; //01:로그쌓기, 02:복습
$return_url = 'playtime_ajax.php'; //30초마다 되돌릴 url
$param1 = $id; //id값을 넘김
$param2 = '2';
$param3 = '3';

$url = $contents->embed_code;
$mode = 'player';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width,  target-densityDpi=device-dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" type="text/css" href="jquery-ui-1.10.3.custom.css" />
        <link rel="stylesheet" href="styles.css" />
        <link rel="stylesheet" href="//releases.flowplayer.org/7.0.4/skin/skin.css" />
        <script type="text/javascript" src="jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="jquery-ui-1.10.3.custom.min.js"></script>
        <script type="text/javascript" src="jquery.cookie.js"></script>
        <script src="./player/flowplayer7/flowplayer.min.js"></script>
        <script src="./viewer/player.js"></script>
        <?php if ($contents->embed_type == 'youtube') {?>
            <script type="text/javascript" src="player/player_youtube.js"></script>
        <?php } else if ($contents->embed_type == 'vimeo') { ?>
            <script src="https://player.vimeo.com/api/player.js"></script>
        <?php } ?>
<!--        <script type="text/javascript" src="video_player.js"></script>-->
        <!--<script type="text/javascript" src="player/flash.js"></script>-->
        <script type="text/javascript">
            var vodurl = '<?php echo $url;?>';
            var positionfrom = 0, positionto = 0, duration = 0, oldposition = 0, ispause = false, capposition = 0, saveposition = 0, evented = 0;
            var satiscompleted = $('#satiscompleted').val();
            var mode = '<?php echo $mode?>';
            
            $(document).ready(function(){
                videoplayer('<?php echo $contents->embed_type;?>','<?php echo $lrn_time;?>');
            });

            function videoplayer(type,lrn_time) {

                if(type == 'youtube'){
                     YouTube_Init("ytplayer","100%","400","<?php echo $cid;?>",0);
                } else {
                    flowplayer_load(vodurl);
                }
            }

            function flowplayer_load(vodurl){
        
                flowplayer("#hlsjsvod", {
                    key: "<?php echo get_config('local_repository','flowplayer_key')?>",
                    splash: false,
                    autoplay: true,
                    embed: false, // setup would need iframe embedding
                    ratio: 5 / 12,    
                    // manual HLS level selection for Drive videos

                    // manual VOD quality selection when hlsjs is not supported
                    //defaultQuality: "260p",
                    //qualities: ["160p", "260p", "530p", "800p"],

                    speeds: [0.75,1,1.25,1.5],

                    clip: {
                        sources: [
                           {
                               type: "application/x-mpegurl",
                               src: vodurl
                           }
                        ]
                    }

                });

            }

            flowplayer(function (api, root) {
                var instanceId = root.getAttribute("data-flowplayer-instance-id"),
                        engineInfo = document.getElementById("engine" + instanceId),
                        vtypeInfo = document.getElementById("vtype" + instanceId),
                        detail = document.getElementById("detail" + instanceId);

                        /*
                        var speedelement = root.querySelector(".buttons"),
                        buttonElements = speedelement.getElementsByTagName("span"),
                        buttons = [],
                        i;

                        for (i = 0; i < buttonElements.length; i += 1) {
                            buttons.push(buttonElements[i]);
                        };
                        */

                api.on("ready", function (e, api, video) {
                    var engineName = api.engine.engineName;

                    // remove speed buttons if playback rate changes are not available
        //            if (api.engine.engineName == "flash" || !flowplayer.support.inlineVideo) {
        //              speedelement.parentNode.removeChild(speedelement);
        //            } 

                    positionto = playtime_get(mode);
                    positionfrom = positionto;
                    duration = Math.ceil(video.duration);
                    if(positionfrom < duration){
                        api.seek(positionfrom);
                    }

                }).on("resume", function (e) {
                    //플레이 버튼 클릭 시 호출
                    //console.log('resume::'+positionfrom+'::'+positionto);
                    if(positionto > 1){
                        positionfrom = positionto;
                    }
                    evented = 0;
                }).on("pause", function (e, api) {
                    //Pause 버튼 클릭 시 호출
                    //console.log('pause::'+positionfrom+'::'+positionto);
                    if (positionfrom < positionto) {
                        playtime_update(positionfrom, positionto, 1, duration, mode);
                    }
                }).on("beforeseek", function (e, api) {
                    //seek 버튼 클릭 시 호출
                    //console.log('beforeseek::'+positionfrom+'::'+positionto);
                    if (positionfrom < positionto) {
                        playtime_update(positionfrom, positionto, 2, duration, mode);
                    }
                }).on("seek", function (e, api) {
                    //seek 버튼 클릭 후 seek되면 호출
                    //console.log('seek::'+positionfrom+'::'+positionto);
                    if(positionto > 1){
                        positionfrom = positionto;
                    }
                    evented = 0;
                }).on("finish", function (e, api) {
                    //console.log('finish::'+positionfrom+'::'+positionto);
                    if (evented !=3 && positionfrom < duration) {
                        playtime_update(positionfrom, duration, 3, duration, mode);
                    }
                    positionfrom = positionto = 0;
                    evented = 1;
                }).on("progress", function (e, api) {
                    //타임라인
                    positionto = Math.ceil(api.video.time);

                    //진도율 저장 (3분마다 진도율을 저장함.)
                    //console.log(positionfrom+'::'+saveposition+'::'+positionto);
                    if(evented == 0 && positionfrom < positionto && positionto % (60*3) == 0){
                       playtime_update(positionfrom, positionto, 5, duration, mode);
                       saveposition = positionto;
                       positionfrom = positionto;
                    }

                }).on("cuepoint", function (e, api, cuepoint) {

                    if (cuepoint.subtitle) {
                        oldposition = parseInt(cuepoint.time);
                        caption_scroll(oldposition);
                    }

                });

            });
            
            window.addEventListener("beforeunload", function (e) {
                var cook = $.cookie('video_event_<?php echo $id;?>_<?php echo $USER->id;?>');
                $.cookie('video_event_<?php echo $id;?>_<?php echo $USER->id;?>',null);
                if(cook=='1' || cook=='4' || cook=='5'){
                    var confirmationMessage = "<?php echo get_string('beforeunload','okmedia');?>";
                    (e || window.event).returnValue = confirmationMessage;     // Gecko and Trident
                    return confirmationMessage;
                }// Gecko and WebKit
            });

        </script>
    </head>
    <body style="margin:0;">
        <input type="hidden" name="lcmsid" value="<?php echo $lcms->id; ?>">
        <div id="hlsjsvod" class="is-closeable video_area"></div>
        <?php if ($contents->embed_type == 'youtube') {?>
        <div id="ytplayer"></div>
        <?php }else if($contents->embed_type == 'vimeo') {?>
        <iframe id="player1" src="https://player.vimeo.com/video/76979871?api=1&player_id=player1" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        <?php }?>
    </body>
</html>