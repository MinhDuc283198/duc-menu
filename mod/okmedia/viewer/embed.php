<?php
$mode = 'player';
$useprogress = $DB->get_field('lmsdata_class','useprogress',array('courseid'=>$course->id));

if($useprogress != 1){
    $mode = 'embed';
}

if($okmedia->progress != 1){
    $mode = 'embed';
}
$learningend = $DB->get_field('lmsdata_class','learningend',array('courseid'=>$course->id));

if($learningend){
    if(date('Ymd',strtotime('+7 day',$learningend)) <= date('Ymd')){
        $mode = 'embed';
    }
}else{
    $mode = 'embed';
}

//디튜브 동영상 데이터 가져오기
$conid = $okmedia->contents;

//디튜브 동영상 데이터 가져오기
if(!$content = $DB->get_record('lcms_contents', array('id'=>$conid))){
    print_error(get_string('nocontents','okmedia'));
    die();
}
$filename = $DB ->get_field('lcms_contents_file','filename',array('con_seq' => $content -> id));

$viewcount = $content->viewcount + 1;
$DB->set_field('okmedia','viewcount',$viewcount,array('id'=>$okmedia->id));

//진도율 가져오기
$lastview = '0000-00-00 00:00';
$playtime = '00:00';
$progress = '0%';
$studying = get_string('beforestudy','okmedia');
if($track = $DB->get_record('okmedia_track', array('okmediaid' => $okmedia->id, 'userid' => $USER->id))){
    $lastview = date('Y-m-d H:i',$track->timeview);
    $ptm = okmedia_time_from_seconds($track->playtime);
    $playtime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
    $progress = $track->progress.'%';
    $addclass = "";
    $studying = ($track->progress < 100)? get_string('studying','okmedia'):get_string('complete','okmedia');
    $addclass = ($track->progress > 100)? "":"inverse";
}

//전체 진도율
$sql = " SELECT l.id, cm.id as cmid, cm.section FROM {okmedia} l JOIN {course_modules} cm ON cm.instance = l.id JOIN {modules} mo ON mo.id = cm.module "
        . "where l.course = :courseid and mo.name = 'okmedia' and cm.visible = 1 and mo.visible = 1";
$lcmses = $DB->get_records_sql($sql, array('courseid'=>$course->id));
$myprogress = 0;
$mycount = 0;

if (!empty($lcmses)) {
    foreach ($lcmses as $lcms) {
        $mytrack = $DB->get_record_select('okmedia_track','okmediaid=:mediaid and userid=:userid',array('mediaid'=>$lcms->id,'userid'=>$USER->id));
        if($mytrack && $mytrack->progress && $mytrack->progress>=0){
            $myprogress += $mytrack->progress;
        }
        $mycount++;
    }
}

$myaverage = round($myprogress/$mycount).'%';

$file = $DB->get_record('lcms_contents_file', array('con_seq'=>$content->id));

//자막 데이터 가져오기
$fs = get_file_storage();

$modCtxSql = 'SELECT 
                        ctx.id 
              FROM {course_modules} cm 
              JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname
              JOIN {context} ctx ON ctx.instanceid = cm.id AND ctx.contextlevel = 70
              WHERE cm.instance = :instance';
$modCtxParam = array(
                    'modname' => 'okmedia',
                    'instance' => $okmedia->id
                );
$modContextId = $DB->get_field_sql($modCtxSql, $modCtxParam);
$okmediaConId = $okmedia->contents;

$captions = $fs->get_area_files(1, 'local_repository', 'subtitle', $okmediaConId, 'sortorder', false);
$currentlang =  current_language();
$subtitles = '';
$flowsubtitles = '';
$azursubtitles = '';

if ($captions) {
    $flowsubtitles .= ', subtitles: ';
    $azursubtitles .= ', ';
    $subtitles .= '[';
    $labels = array('ko' => 'Korean', 'en' => 'English');
    foreach ($captions as $caption) {
        $captionname = $caption->get_filename();

    }
   
    foreach ($captions as $caption) {
        $captionname = $caption->get_filename();
        if(preg_match(OKMEDIA_CAPION_FILE_PATTEN_EN, $captionname)){
            $captionlang = 'en';
        } else if(preg_match(OKMEDIA_CAPION_FILE_PATTEN_KO, $captionname)){
            $captionlang = 'ko';
        }
        
        $captionpath = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/1/local_repository/subtitle/' .$okmediaConId. '/' . $captionname);
        $subtitles .= '{';
        if ($currentlang == $captionlang) {
            $subtitles .= '"default": true, ';
            $defaultsubtitle = $captionpath;
            //플래시
            $captionurl = 'captionUrl:"'.$defaultsubtitle.'",';
        }
        $subtitles .= 'kind: "subtitles", srclang: "' . $captionlang . '", label: "' . $labels[$captionlang] . '",
              src:  "' . $captionpath . '"';
        $subtitles .= '},';

    }
    
    $subtitles .= ']';
    $flowsubtitles .= $subtitles;
    $azursubtitles .= $subtitles;
   
}


$html = '';
if($okmedia->progress == 1){
    /**video info**/
    $html .= html_writer::start_tag("div" ,array("class"=>"videoinfo"));

    $html .= html_writer::start_tag("div" ,array("class"=>"left_cont"));
    $html .= html_writer::tag("p" , html_writer::tag("span" , $studying ) ,array("class"=>"circletext " . $addclass));
    $html .= html_writer::start_tag("ul" ,array("class"=>"info"));
    $html .= html_writer::start_tag("li" ,array("class"=>"m-col3"));
    $html .= html_writer::tag("p" ,get_string('lastedu','local_media'));
    $html .= html_writer::tag("p" ,$lastview, array("id"=>"id_lastview"));
    $html .= html_writer::end_tag("li");

    $html .= html_writer::start_tag("li" ,array("class"=>"m-col3"));
    $html .= html_writer::tag("p" ,get_string('edutime','local_media'));
    $html .= html_writer::tag("p" ,$playtime, array("id"=>"id_playtime"));
    $html .= html_writer::end_tag("li");

    $html .= html_writer::start_tag("li" ,array("class"=>"m-col3"));
    $html .= html_writer::tag("p" ,get_string('studyrate','local_media'));
    $html .= html_writer::tag("p" ,$progress, array("id"=>"id_progress"));
    $html .= html_writer::end_tag("li");

    $html .= html_writer::start_tag('li' , array("id"=>"durationbar"));
    $html .= html_writer::end_tag("li");

    $html .= html_writer::end_tag("ul");
    $html .= html_writer::end_tag("div");
    
    $html .= '<div class="caption_btn_group">';    
    $html .= '<input class="button_style01 blue show" type="button" value="'.get_string('captionhide','okmedia').'">';
    $html .= '</div>';
    
    $html .= html_writer::end_tag("div");
    /**video info**/
    
    
    // professor information
    $prof = $DB->get_record('user', array('id'=>$okmedia->profid));
    
    //일단 임시 설정
    if(empty($prof)){
        $prof = $DB->get_record('user', array('id'=>2));
    }
    $userpicture = new user_picture($prof);
    $userpicture->size = 1; // Size f1.
    $profimageurl = $userpicture->get_url($PAGE)->out(false);
    
    $html .= html_writer::start_div("prof_info mobile_hide");
    $html .= html_writer::start_div("prof_inner");
    
    $html .= html_writer::start_span("prof_picture");
    $html .= html_writer::img($profimageurl, 'Admin User', array('class'=>'defaultuserpic'));
    $html .= html_writer::end_span();
    
    $prof_group = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $prof->id));
    $usergroups = array('pr'=>'교수', 'it'=>'강사', 'ad'=>'조교');
    
    $html .= html_writer::start_div("inner_cont");
    $html .= html_writer::span($prof->lastname. ' '.$usergroups[$prof_group], "prof_title");
    $html .= html_writer::span($prof->description);
    $html .= html_writer::end_div();
    
    $html .= html_writer::end_div();
    $html .= html_writer::end_div();

    echo $html;
}

//메뉴 타이틀바 - 강의영상, 자막
$subtitle_html = '';
$subtitle_html .= html_writer::start_tag("div", array("class" => "mooc-subtitle mobile_hide"));
$subtitle_html .= html_writer::tag("div",'강의영상', array("class" => "mooc-subtitle-video width68"));
$subtitle_html .= html_writer::tag("div",'자막', array("class" => "mooc-subtitle-caption width32", "style" => "width:32%"));
$subtitle_html .= html_writer::end_tag("div");
echo $subtitle_html;
?>  

<form name="playtime_form" id="playtime_form">
    <input type="hidden" name="id" id="okmediaid" value="<?php echo $okmedia->id;?>"/>
    <input type="hidden" name="subtitleslang" id="subtitleslang" value="<?php echo $currentlang;?>"/>
</form>
<!--플레이어 영역 시작-->
<?php 
    
echo '<div class="videoarea width70 height100">';
    
if($content->con_type == 'video'){
//    $brows = mod_okmedia_browser_check();                       
    if ($brows->device == 'P' && $brows->browser == 'IE') {
        include_once('player_flash.php');
    }else{
       include_once('player_video.php');
    }
}else if($content->con_type == 'embed'){
    if($content->embed_type == 'youtube'){
        include_once('player_youtube.php');
    } else if($content->embed_type == 'vimeo'){
        include_once('player_vimeo.php');
    } else {
//        $brows = mod_okmedia_browser_check();                       
        if ($brows->device == 'P' && $brows->browser == 'IE') {
            include_once('player_flash.php');
        }else{
           include_once('player_video.php');
        }
    }
}
?>
</div>
<?php
// 자막 영역 - 시작
if ($captions) {
    
    echo '<div class="caption_area mobile_hide">';
    foreach ($captions as $caption) {
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/1/local_repository/subtitle/' .$okmediaConId. '/' . $captionname);
        echo '<div class="captioninfo captioninfo' . $caption->get_id() . '">';
      
            $subs = okmedia_get_captions($path);
            echo '<div class="script_list ' . $caption->lang . '">';
            foreach ($subs as $sub) {
                $timestart = okmedia_seconds_from_time($sub->starttime);
                echo '<div class="subtext sub' . $timestart . '">' . $sub->text . '</div>';
            }
        echo '</div>';
        echo '</div>';
    }
    echo '</div>';
} else {
    echo '<div class="caption_area mobile_hide">';
    echo '<div class="empty-cpations">자막이 없습니다.</div>';
    echo '</div>';
}
// 자막 영역 - 끝
?>
<script>
function goto_seek(playtime){  
   flowplayer("#hlsjsvod").seek(playtime);
}
</script>



