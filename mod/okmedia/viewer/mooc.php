<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)) . '/lib.php');
require_once(dirname(dirname(__FILE__)) . '/locallib.php');
require_once($CFG->dirroot."/theme/oklassedu/layout/lib.php");
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/mod/okmedia/viewer/lib.php');

$v = required_param('v', PARAM_RAW); 
$decrypted = base64_decode($v);
list($id, $sectionid, $userid, $mode) = explode('||', $decrypted);
$course_modules = $DB->get_record('course_modules', array('id' => $id));
if (!$course = $DB->get_record('course', array('id' => $course_modules->course))) {
    print_error('course is misconfigured');
}

$context = context_course::instance($course->id, MUST_EXIST);
require_login($course);
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->css('/mod/okmedia/player/flowplayer7/skin/skin.css');
$PAGE->requires->css('/mod/okmedia/popup.css');
$PAGE->requires->css('/theme/oklassedu/style/font-awesome.min.css');
$titletext =get_course_title(0);
$classtitle = $DB->get_field("lmsdata_class","title",array("courseid"=>$COURSE->id));
echo $OUTPUT->header();
echo '<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<link href="/theme/oklassedu/style/media.css" rel="stylesheet" />
<script type="text/javascript" src="/lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js"></script>
<script src="/theme/oklassedu/javascript/theme.js" type="text/javascript"></script>';
//section 가져오기
$modinfo = get_fast_modinfo($course);

//section에 등록된 동영상 가져오기 (한 섹션에 동영상은 하나만 등록 가능)
//$sql = " SELECT l.*, cm.id as cmid FROM {okmedia} l JOIN {course_modules} cm ON cm.instance = l.id "
//        . "JOIN {modules} mo ON mo.id = cm.module  where mo.name = 'okmedia' and cm.visible = 1 and mo.visible = 1 and cm.section = :sectionid";
//$okmedia = $DB->get_record_sql($sql, array('sectionid' => $sectionid), 0, 1);

//현재 섹션명
$currentsection = $DB->get_record('course_sections', array('id' => $sectionid));
$currenttitle = get_section_name($course, $currentsection);

//현재 섹션의 학습활동 정보 가져오기
$sequence = explode(',', $currentsection->sequence);
$modules = array();
$modcount = 0;
//foreach ($sequence as $seq) {
    $mod_cm = $DB->get_record('course_modules', array('id' => $id));
    $module = $DB->get_record('modules', array('id' => $mod_cm->module));
    if ($mod_cm && $module && $module->name != 'okmedia') {
        $modData = $DB->get_record($module->name, array('id' => $mod_cm->instance));
        $modCompletion = $DB->get_field('course_modules_completion', 'completionstate', array('coursemoduleid'=>$mod_cm->id, 'userid'=>$USER->id));
        $modules[$id] = array();
        $modules[$id]['name'] = $modData->name;
        //vpl일 경우 소스 편집 url로 화면을 잡음
        if($module->name == 'vpl') {
            $modules[$id]['url'] = $CFG->wwwroot . '/mod/vpl/forms/edit.php?iframe=1&id='.$mod_cm->id.'&userid='.$USER->id;
        } else {
            $modules[$id]['url'] = $CFG->wwwroot . '/mod/' . $module->name . '/view.php?iframe=1&id=' . $mod_cm->id;
        }
        $modules[$id]['urlencode'] = base64_encode($modules[$id]['url']);
        $modules[$id]['module'] = $module->name;
        $modules[$id]['cmid'] = $mod_cm->id;
        $modules[$id]['instance'] = $mod_cm->instance;
        $modules[$id]['intro'] = $modData->intro;
        $modules[$id]['completion'] = $modCompletion;

        if ($modcount == 0) {
            $firstmodkey = $id;
            $firstmodname = $modules[$id]['name'];
            $firstmodurl = $modules[$id]['url'];
        }
        $modcount++;
    }
//}

//부모 section id 값 가져오기
$query = "select s.id from {course_sections} s 
join {course_format_options} fo 
on fo.value = s.section and fo.courseid = s.course  
where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and fo.sectionid = :sectionid";
$param = array('courseid' => $course->id, 'sectionid' => $sectionid);
$parentid = $DB->get_field_sql($query, $param);

//이전 section과 다음 section 가져오기
//$query = "SELECT s.section, s.id FROM {course_sections} s 
//join {course_format_options} fo 
//on fo.sectionid = s.id and fo.courseid = s.course 
//WHERE fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and section < :section 
//order by s.section desc limit 1";
//$param = array('courseid' => $course->id, 'section' => $currentsection->section);
//if ($prevsection = $DB->get_record_sql($query, $param)) {
//    if ($prevsection->id) {
//        $prevlink = format_oklass_mooc_shared_media_url_link($course->id, $prevsection->id, 'player');
//    }
//}
//
//$query = "SELECT s.section, s.id FROM {course_sections} s 
//join {course_format_options} fo 
//on fo.sectionid = s.id and fo.courseid = s.course 
//WHERE fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and section > :section 
//order by s.section asc limit 1";
//$param = array('courseid' => $course->id, 'section' => $currentsection->section);
//if ($nextsection = $DB->get_record_sql($query, $param)) {
//    if ($nextsection->id) {
//        $nextlink = format_oklass_mooc_shared_media_url_link($course->id, $nextsection->id, 'player');
//    }
//}


$html = "";
$html .= html_writer::start_tag("div", array("id" => "popwrap"));

//메뉴 타이틀바 - 진도현황, 교수소개
if($okmedia) {
    $html .= html_writer::start_tag("div", array("class" => "mooc-subtitle out mobile_hide"));
    $html .= html_writer::tag("div",'진도현황', array("class" => "mooc-subtitle-progress width68"));
    $html .= html_writer::tag("div",'교수소개', array("class" => "mooc-subtitle-prof width32", "style"=>"width: calc(32% - 16px);"));
    $html .= html_writer::end_tag("div");
}

/* * *title** */
//if($okmedia) {
//    $html .= html_writer::start_tag("h3", array("id" => "pop_title"));
//} else {
//    $html .= html_writer::start_tag("h3", array("id" => "pop_title", 'class'=>'width96'));
//}
//
//$html .= html_writer::tag("img", "", array("alt" => "menubtn", "title" => "menubtn", "src" => "/mod/okmedia/pix/bars_b.png", "class" => "menubtn"));
//$html .= html_writer::tag("span", $currenttitle, array("class" => "text","style" => "width:calc(100% - 300px)"));
//if ($okmedia && $okmedia->progress == 1) {
//    $html .= html_writer::start_tag("span", array("class" => "r_content", 'id'=>'popclose'));
//    $html .= get_string('close_text', 'okmedia');
//    $html .= html_writer::tag("img", "", array("alt" => "close", "class" => "close r_content", "src" => "/mod/okmedia/pix/close.png"));
//    $html .= html_writer::end_tag("span");
//} else {
//    $html .= html_writer::start_tag("span", array("class" => "r_content", 'id'=>'popclose2'));
//    $html .= html_writer::tag("img", "", array("alt" => "close", "class" => "close r_content", "src" => "/mod/okmedia/pix/close.png"));
//    $html .= html_writer::end_tag("span");
//}
//$html .= html_writer::end_tag("h3");
        
/* * *title** */


/* * menu* */
    $html .= html_writer::start_tag("div", array("class" => "media_menu"));
    $html .= html_writer::start_tag("ul", array("class" => "menu_ul"));
        $section_sql = "SELECT cs.* FROM {course_modules} cm 
                        JOIN {course} co ON co.id = cm.course
                        JOIN {course_sections} cs ON cs.section != 0 AND cs.course = co.id
                        WHERE cm.id = :coursemodule
                        ORDER BY cs.section ";
        $section_lists = $DB->get_records_sql($section_sql, array('coursemodule'=>$id));

        foreach($section_lists as $section_list){
            if(empty($section_list->name)){
                $tag_name = $section_list->section.'차';
            }else{
                $tag_name = $section_list->name;
            }
            $liparams = array('name'=>$tag_name);
            if($section_list->id == $course_modules->section) {
                $liparams['class'] = 'on';
            }
            $html .= html_writer::start_tag('li', $liparams);
                $html .= html_writer::tag('h3',$tag_name);

                $html .= html_writer::start_tag('ul');

                $module_sql = "SELECT cm.*, mo.name FROM {course_modules} cm
                            JOIN {modules} mo ON mo.id = cm.module
                            WHERE cm.course = :course AND cm.section = :section AND cm.visible = 1 ";
                $module_lists = $DB->get_records_sql($module_sql, array('course'=>$section_list->course, 'section'=>$section_list->id));
                
                $courseModInfo = get_fast_modinfo($course);
                
                foreach($module_lists as $module_list){
                    $module_info_sql = "SELECT * FROM {".$module_list->name."} WHERE id = :instance ";
                    $module_info = $DB->get_record_sql($module_info_sql, array('instance'=>$module_list->instance));
                    if($module_list->name == 'okmedia'){
                        $mode = "player";
                    }else{
                        $mode = "embed";
                    }
                    
                    $html .= html_writer::start_tag('li');
                    $html .= '<a class="completionCheck" data-cmid="'.$module_list->id.'" data-nevimode="'.$mode.'">' . $module_info->name . '</a>';;
                    $html .= html_writer::end_tag('li');
                }
                $html .= html_writer::end_tag('ul');
            $html .= html_writer::end_tag('li');
        }
    $html .= html_writer::end_tag("ul");
    $html .= html_writer::end_tag("div");
    $html .= html_writer::start_tag("div", array("class" => "pop_title"));
    $html .= '<p class="c-nm">'.$classtitle.'</p>';
    $html .= '<span class="pop_menu">메뉴</span><span>'.$modules[$id]['name'].'
        </span><img class="close r_content" src="/theme/oklassedu/pix/images/icon_close_w.png" onclick ="window.close();" />';
    $html .= html_writer::end_tag("div");

//$html .= html_writer::start_tag("div", array("class" => "media_menu"));
//$html .= html_writer::start_tag("ul");
//
//foreach ($modinfo->get_section_info_all() as $section => $thissection) {
//    if ($section == 0) {
//        continue;
//    }
//
//    $query = "select count(*) from {course_format_options} where format='oklass_mooc' and name = 'parent' and value != 0 and courseid = :courseid and sectionid = :sectionid";
//    $param = array('courseid' => $course->id, 'sectionid' => $thissection->id);
//    $optioncount = $DB->count_records_sql($query, $param);
//
//    if ($optioncount == 0) {
//        if ($childcount > 0) {
//            $html .= html_writer::end_tag("ul");
//            $html .= html_writer::end_tag("li");
//        }
//        $query = "select count(*) from {course_format_options} fo 
//            join {course_sections} s on s.id = fo.sectionid 
//            where fo.format='oklass_mooc' and fo.name = 'parent' and fo.value != 0 and fo.courseid = :courseid and fo.value = :section";
//        $param = array('courseid' => $course->id, 'section' => $section);
//        $sectioncount = $DB->count_records_sql($query, $param);
//        $sectionclass = ($sectioncount > 0) ? '' : ' no-content';
//        $parenton = ($parentid == $thissection->id) ? 'on' : '';
//        $html .= html_writer::start_tag("li", array("class" => $parenton));
//        $html .= html_writer::tag("h3", get_section_name($course, $section), array("class" => $sectionclass));
//        $childcount = 0;
//    } else {
//        if ($childcount == 0) {
//            $html .= html_writer::start_tag("ul");
//        }
//        $selectedon = ($sectionid == $thissection->id) ? 'on' : '';
//        $share_mediaurl = format_oklass_mooc_shared_media_url_link($course->id, $thissection->id, 'player');
//        $html .= html_writer::tag("li", html_writer::tag("a", get_section_name($course, $section), array("href" => $share_mediaurl)), array("class" => $selectedon));
//        $childcount++;
//    }
//}
//
//$html .= html_writer::end_tag("ul");
//$html .= html_writer::end_tag("div");
/* * menu* */


/* * *contents** */
$html .= html_writer::start_tag("div", array("id" => "contents"));

echo $html;

if ($okmedia) {
    echo html_writer::start_tag("div" ,array("class"=>"video_group mooc"));
    echo html_writer::start_tag("div" ,array());
    include_once 'embed.php';
    echo html_writer::end_tag("div");
    echo html_writer::end_tag("div");
}

$html = '';

$html .= html_writer::start_tag('div', array('class' => 'attachment'));  // 첨부파일 영역
$html .= html_writer::start_tag('ul');  

$sql = "SELECT ref.id, rcon.id as con_id, rcon.share_yn, rcon.con_type, rcon.con_name, rcon.update_dt, rcon.embed_code, rcon.embed_type, rep.id as repositoryid
        FROM {lcms_contents} con
        JOIN {lcms_repository} rep ON rep.lcmsid = con.id
        JOIN {lcms_repository_reference} ref ON ref.repository = rep.id
        JOIN {lcms_contents} rcon ON rcon.id = ref.lcmsid
        WHERE con.id = :conid";
$ref_files = $DB->get_records_sql($sql, array('conid' => $okmedia->contents));

foreach($ref_files as $ref_file){
    $files = $DB->get_records('lcms_contents_file', array('con_seq' => $ref_file->con_id, 'con_type' => $ref_file->con_type));
    foreach ($files as $file) {
        $filename = $file->fileoname;
        $filepath = explode('/', $file->filepath);
        if ($filepath[0] == 'lms' || $filepath[0] == 'lcms')
            $lcmsdata = '/lcmsdata/';
        else
            $lcmsdata = '/';

        $path = 'download.php?id=' . $ref_file->repositoryid . '&fileid=' . $file->id;

        $html .= html_writer::tag('li', html_writer::tag('a', format_text(s($filename), FORMAT_HTML, array('context' => $context)),  array('href'=>"$path")));  
    }
    if($ref_file->embed_type == 'embed'){
        $html .= html_writer::tag('li', html_writer::tag('a', $ref_file->con_name,  array('href'=>"$ref_file->embed_code", 'download'=>'true')));  
    }
}
$html .= html_writer::end_tag('ul');  
$html .= html_writer::end_tag("div");

if ($modules) {
    //메뉴 타이틀바 - 학습활동, 참고자료
//    $html .= html_writer::start_tag("div", array("class" => "mooc-subtitle mobile_hide"));
//    $html .= html_writer::tag("div",'학습활동', array("class" => "mooc-subtitle-activity width100"));
//    $html .= html_writer::tag("div",'참고자료', array("class" => "mooc-subtitle-data width32", "style"=>"float:right; width: calc(32% - 32px);"));
//    $html .= html_writer::end_tag("div");
    /*     * tab header* */
    echo $titletext;
    $html .= html_writer::start_tag("div", array("class" => "evaluation width100"));
    $html .= html_writer::start_tag("div", array("class" => "tab_header auto"));

    foreach ($modules as $key => $mod) {
        /*         * 모듈명 가져오기 * */
        $selectedon = ($firstmodkey == $key) ? 'on ' : '';
        $modIconUrl = $OUTPUT->pix_url('icon', $mod['module'])->out(false);
        
        $html .= html_writer::start_tag('a', array('href' => 'javascript:goto_modurl(' . $key . ',"' . $mod['url'] . '");'));
        $html .= html_writer::start_tag("span", array("class" => 'activity-tab '.$selectedon, "id" => "mod_tab_" . $key));
        $html .= html_writer::tag("img", "", array("alt" => $mod['name'], "title" => $mod['name'], "src" => $modIconUrl, "class"=>'mod-icon'));
        $html .= $mod['name'];
        if(!empty($mod['completion'])) {
            $html .= html_writer::tag("i",'', array("class" => 'fa fa-check-circle completion-icon', 'aria-hidden' => "true"));
//            $html .= $OUTPUT->pix_icon('i/completion-manual-y', '','',array('style'=>'float:right;'));
        }
        
        $html .= html_writer::end_tag("span");
        $html .= html_writer::end_tag("a");
    }
//
//    $html .= html_writer::end_tag("div");

    /*     * tab header* */

    /*     * tab contents* */
    $html .= html_writer::start_tag("div", array("class" => "e_content"));
    $html .= html_writer::start_tag("div", array("class" => "course_con"));

// 2018.10.01 CHS - 한양대는 퀴즈 관련 제약조건이 없어서 주석처리
//    $cm = get_coursemodule_from_id('quiz', $mod['cmid']);
//    $cm = $modinfo->get_cm($cm->id);
//    if(!$cm->uservisible) {
//        echo '<script>alert("이전 강의를 수강해 주세요 ");'
//        . 'self.close();</script>';
//    } else {
//        $attfile = '<iframe class="mod_viewer" id="mod_viewer_' . $firstmodkey . '" title="mod" src="' . $firstmodurl . '">';
//        $attfile .= '</iframe>';
//    }
    $attfile = '<iframe class="mod_viewer" id="mod_viewer_' . $firstmodkey . '" title="mod" src="' . $firstmodurl . '">';
    $attfile .= '</iframe>';
    
    $html .= $attfile;
    $html .= html_writer::start_tag("div",array("class" => "video_buttons"));
$section_btn_sql = "SELECT cs.* FROM {course_modules} cm 
                            JOIN {course} co ON co.id = cm.course
                            JOIN {course_sections} cs ON cs.section != 0 AND cs.course = co.id
                            WHERE cm.id = :coursemodule
                            ORDER BY cs.section ";
            $section_btn_lists = $DB->get_records_sql($section_sql, array('coursemodule' => $id));
            $aftertag = '';
            $nowtag = '';
            $beforetag = '';
            
            foreach ($section_btn_lists as $section_list) {
                $module_sql = "SELECT cm.*, mo.name 
                                   FROM {course_modules} cm
                                   JOIN {modules} mo ON mo.id = cm.module
                                   WHERE cm.course = :course AND cm.section = :section AND cm.visible = 1 
                                   ORDER BY cm.section asc";
                $module_lists = $DB->get_records_sql($module_sql, array('course' => $section_list->course, 'section' => $section_list->id));
                foreach ($module_lists as $module_list) {
                    if ($module_list->name == 'okmedia') {
                        $nevimode = "player";
                    } else {
                        $nevimode = "embed";
                    }
                    $encrypted = base64_encode($module_list->id . '||' . $nevimode);

                    if (!empty($nowtag) && empty($aftertag)) {
//                        $aftertag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    } else if ($id == $module_list->id && empty($nowtag)) {
                        $nowtag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    }
                }
            }
            foreach ($section_btn_lists as $section_list) {
                $module_sql = "SELECT cm.*, mo.name 
                                   FROM {course_modules} cm
                                   JOIN {modules} mo ON mo.id = cm.module
                                   WHERE cm.course = :course AND cm.section = :section AND cm.visible = 1 
                                   ORDER BY cm.section desc";
                $module_lists = $DB->get_records_sql($module_sql, array('course' => $section_list->course, 'section' => $section_list->id));

                foreach ($module_lists as $module_list) {
                    if ($module_list->name == 'okmedia') {
                        $nevimode = "player";
                    } else {
                        $nevimode = "embed";
                    }
                    $encrypted = base64_encode($module_list->id . '||' . $nevimode);

                    if (!empty($nowtag) && empty($beforetag)) {
//                        $beforetag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    } else if ($id == $module_list->id && empty($nowtag)) {
                        $nowtag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    }
                }
            }
            
            if ($beforeend = mod_okmedia_get_nextprev($course->id, $id, 'before')) {
                if ($beforeend->nextModule == 'okmedia') {
                    $nevimode = "player";
                } else {
                    $nevimode = "embed";
                }
                $html .= '<a data-cmid="'.$beforeend->nextCmid.'" data-nevimode="'.$nevimode.'" class="prev completionCheck">PREV</a>';
            }
            
            if ($nextenc = mod_okmedia_get_nextprev($course->id, $id, 'next')) {
                if ($nextenc->nextModule == 'okmedia') {
                    $nevimode = "player";
                } else {
                    $nevimode = "embed";
                }
                $html .= '<a data-cmid="'.$nextenc->nextCmid.'" data-nevimode="'.$nevimode.'" class="next completionCheck">NEXT</a>';
            }
       
            $html .= html_writer::end_tag("div");
    $html .= html_writer::end_tag("div");
    
    $html .= html_writer::end_tag("div");
    /*     * tab contents* */
    
    $html .= html_writer::end_tag('ul');  

    $html .= html_writer::end_tag("div");
    
    /* reference navigation */
//    $html .= html_writer::start_div("references");
//    if(!empty($modules)) {
//        $html .= html_writer::start_tag("ul", array());
//        foreach ($modules as $key => $mod) {
//            $html .= '<li class="on">
//                        <a href="#"><h3 class="">'.$mod['name'].'</h3></a>
//                        <ul>
//                            <li class="on">'.$mod['intro'].'</li>
//                        </ul>
//                     </li>';
//        }
//        $html .= html_writer::end_tag('ul');
//    }
//    $html .= html_writer::end_div();
}
    $html .= html_writer::end_tag('div');
    
    


$html .= html_writer::tag('div', html_writer::tag('div', '<img src="/mod/okmedia/player/img/loading.gif">', array('style' => 'position: absolute; top: 50%; left: 47%;')), array('id' => 'loading', 'style' => 'position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1200; background-color:#757b8c;opacity:0.7;display:none;'));

echo $html;
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    $(document).ready(function () {
        // 네비게이션 이벤트
        pop_menu_evnet();
        
        //접근제한 체크
        $(".completionCheck").click(function() {
            $.ajax({
                url: '/mod/okmedia/viewer/completion.ajax.php',
                type: 'POST',
                dataType: 'json',
                async: true,
                data: {
                    courseid: <?php echo $COURSE->id; ?>,
                    userid: <?php echo $USER->id; ?>,
                    cmid: $(this).data("cmid"),
                    nevimode: $(this).data("nevimode")
                },
                success: function (data) {
                    if (data.status == 'fail') {
                        alert(data.str);
                        return false;
                    } else {
                        document.location.href = data.url;
                    }
                }
            });
        });
    });

    function goto_modurl(key, url) {
        startloading();
        //var gotourl = 'loading.php?url='+url;
        $('.mod_viewer').attr({'src': url, 'id': 'mod_viewer_' + key});

    }

    function startloading() {
        $('#loading').show();
    }

    function endloading() {
        $('#loading').hide();
    }


</script>


