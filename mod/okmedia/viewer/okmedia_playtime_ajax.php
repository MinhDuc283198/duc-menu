<?php

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)) . '/lib.php');
require_once '../../lcmsprogress/locallib.php';
require_once $CFG->dirroot . '/mod/okmedia/locallib.php';

global $DB, $USER;

$returnvalue = new stdClass();

$act = required_param('act', PARAM_RAW);
$device = required_param('device', PARAM_RAW);
$event = optional_param('positionevent', 0, PARAM_INT);
$id = required_param('id', PARAM_INT);

//$cour = $DB->get_record('okmedia',array('id'=>$id));
$okmedia = $DB->get_record('okmedia', array('id' => $id));

$canupdate = okmedia_can_update_progress($okmedia);


$relearning = $DB->get_field('lmsdata_class', 'reviewperiod', array("courseid" => $okmedia->course));
$expday = $DB->get_field_sql("select timeend from {enrol} e 
        join {user_enrolments} ue on e.id = ue.enrolid 
        where ue.userid = ? and e.courseid =? ", array($USER->id, $okmedia->course));
$acceptlearn = strtotime("-$relearning days", $expday);


if ($acceptlearn > time()) {
    if ($USER) {
        $track = $DB->get_record('okmedia_track', array('okmediaid' => $okmedia->id, 'userid' => $USER->id));

        if ($act == 'get') {
            if ($canupdate) {
                if ($track) {
                    $track->attempts = $track->attempts + 1;
                    $track->device = $device;
                    $track->timeview = time();
                    $DB->update_record('okmedia_track', $track);
                } else {

                    $track = new stdClass();
                    $track->okmediaid = $okmedia->id;
                    $track->userid = $USER->id;
                    $track->timestart = time();
                    $track->timeview = time();
                    $track->attempts = 1;
                    $track->device = $device;
                    $DB->insert_record('okmedia_track', $track);
                }
            }

            $returnvalue->last = date('Y-m-d H:i:s', $track->timeview);
            $returnvalue->progress = $track->progress;
            $returnvalue->positionto = $track->lasttime;
        } else if ($act == 'update') {
            $duration = optional_param('duration', 0, PARAM_INT);
            if ($canupdate) {
                $playtime = new stdClass();
                $playtime->userid = $USER->id;
                $playtime->okmediaid = $okmedia->id;
                $playtime->positionto = optional_param('positionto', 0, PARAM_INT);
                $playtime->positionfrom = optional_param('positionfrom', 0, PARAM_INT);
                $playtime->positionevent = optional_param('positionevent', 0, PARAM_INT);
                $playtime->lastdevice = $device;
                $playtime->timecreated = time();

                if ($playtime->positionto > 0 && $duration > 1 && $track->progress < 100) {
                    if ($playtime->positionto > $duration) {
                        $playtime->positionto = $duration;
                    }

                    $status = 0;

                    //이벤트에 따라 업데이트 처리
                    if ($playtime->positionevent == 5 || $playtime->positionevent == 1 || $playtime->positionevent == 4) {
                        $query = 'select * from {okmedia_playtime} '
                                . 'where userid=:userid and okmediaid=:okmediaid and positionevent = :positionevent order by id desc limit 1';
                        $params = array('userid' => $USER->id, 'okmediaid' => $okmedia->id, 'positionevent' => $playtime->positionevent);

                        if ($usertime = $DB->get_record_sql($query, $params)) {
                            if ($playtime->positionto == $usertime->positionfrom) {
                                $playtime->id = $usertime->id;
                                $playtime->positionto = $usertime->positionto;
                                $playtime->timemodified = time();
                                $DB->update_record('okmedia_playtime', $playtime);
                                $status = 1;
                            }
                            // 유튜브에서 1, 4 이벤트 시 간혹 1초를 누락시켜서 추가
                            if ($playtime->positionfrom !== $usertime->positionto) {
//                            $playtime->positionfrom = $usertime->positionto;
                            }
                        }
                    }

                    if ($status == 0) {
                        $query = 'select count(*) from {okmedia_playtime} 
                        where userid=:userid and okmediaid=:okmediaid and positionfrom<=:positionfrom and positionto>=:positionto';
                        $params = array('userid' => $USER->id, 'okmediaid' => $okmedia->id, 'positionto' => $playtime->positionto, 'positionfrom' => $playtime->positionfrom);
                        $playcount = $DB->count_records_sql($query, $params);

                        //플레이시간을 저장한다. 
                        if ($playcount == 0) {
                            $DB->insert_record('okmedia_playtime', $playtime);
                        }
                    }

                    if (!$track) {
                        $track = new stdClass();
                        $track->okmediaid = $okmedia->id;
                        $track->userid = $USER->id;
                        $track->timestart = time();
                        $track->attempts = 1;
                    }

                    if ($duration == $playtime->positionto) {
                        $track->lasttime = 0;
                    } else {
                        $track->lasttime = $playtime->positionto;
                    }
                    $track->playtime = okmedia_get_progress($okmedia->id, $USER->id);
                    $track->progress = round($track->playtime / $duration * 100);
                    if ($track->progress > 97) {
                        $track->progress = 100;
                    }
                    $track->timeview = time();
                    $track->device = $device;

                    if ($track->id) {
                        $DB->update_record('okmedia_track', $track);
                    } else {
                        $DB->insert_record('okmedia_track', $track);
                    }
                } else {
                    if (!$track) {
                        $track = new stdClass();
                        $track->okmediaid = $okmedia->id;
                        $track->userid = $USER->id;
                        $track->timestart = time();
                        $track->attempts = 1;
                    } else {
                        $track->attempts = $track->attempts + 1;
                    }
                    if ($duration == $playtime->positionto) {
                        $track->lasttime = 0;
                    } else {
                        $track->lasttime = optional_param('positionto', 0, PARAM_INT);
                    }
                    $track->timeview = time();
                    $track->device = $device;
                    if ($track->id) {
                        $DB->update_record('okmedia_track', $track);
                    } else {
                        $DB->insert_record('okmedia_track', $track);
                    }
                }

                // course_module_completion 테이블에 이수 업데이트
                $course = $DB->get_record('course', array('id' => $okmedia->course));
                $cm = get_coursemodule_from_instance('okmedia', $okmedia->id, $course->id, false, MUST_EXIST);

                $msql = 'SELECT completionprogress FROM {okmedia} WHERE course = :course AND id = :id';
                $completionprogress = $DB->get_field_sql($msql, array('course' => $course->id, 'id' => $okmedia->id));
                $completion = new completion_info($course);
                if ($completion->is_enabled($cm)) {
                    // Mark viewed if required
                    $completion->set_module_viewed($cm);
                    $complete = COMPLETION_INCOMPLETE;
                    if ($track->progress >= $completionprogress) {
                        $complete = COMPLETION_COMPLETE;
                    }
                    //같지 않을 때만 status 업데이트
                    $completionData = $completion->get_data($cm, false, $track->userid);
                    if ($completionData->completionstate != $complete) {
                        $completion->update_state($cm, $complete, $track->userid);
                    }
                }
            } else {
                if (!$track) {
                    $track = new stdClass();
                    $track->okmediaid = $okmedia->id;
                    $track->userid = $USER->id;
                    $track->timestart = time();
                    $track->attempts = 1;
                } else {
                    $track->attempts = $track->attempts + 1;
                }
                if ($duration == $playtime->positionto) {
                    $track->lasttime = 0;
                } else {
                    $track->lasttime = optional_param('positionto', 0, PARAM_INT);
                }
                $track->timeview = time();
                $track->device = $device;
                if ($track->id) {
                    $DB->update_record('okmedia_track', $track);
                } else {
                    $DB->insert_record('okmedia_track', $track);
                }
            }

            $returnvalue->last = date('Y-m-d H:i:s', $track->timeview);
            $ptm = okmedia_time_from_seconds($track->playtime);
            $returnvalue->totaltime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
            $returnvalue->progress = $track->progress . ' %';
        }

        $returnvalue->status = 'success';
    $returnvalue->canupdate =$canupdate;

        lcmsprogress_update_progress_score($okmedia->course, $USER->id);


        //setcookie('video_event_'.$id.'_'.$USER->id, $event,time()+3600);
    }
}
@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

