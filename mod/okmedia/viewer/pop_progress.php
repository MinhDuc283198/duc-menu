<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)) . '/lib.php');

global $DB, $USER;

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();

$id = required_param('id', PARAM_INT);
$duration = required_param('duration', PARAM_INT);

$query = 'select positionfrom,positionto from {okmedia_playtime} where okmediaid = :okmediaid and userid = :userid and positionfrom < positionto order by positionfrom, positionto';
$plays = $DB->get_records_sql($query,array('okmediaid'=> $id,'userid'=>$USER->id));
$poses = array();
$start = -1;
$end = -1;
foreach($plays as $play){
    if ($start == -1) {       
        $start = $play->positionfrom;
        $end = $play->positionto;
    } else {
        if (($start <= $play->positionfrom && $play->positionfrom <= $end) && ($end <= $play->positionto)) {
            $end = $play->positionto;
        } else if ($end < $play->positionfrom) {
            $start = $play->positionfrom;
            $end = $play->positionto;
        }
    }
    $poses[$start] = $end;
}

$poshtml = '';
$start = $end = 0;
foreach($poses as $key=>$val){
    $start = ($key/$duration) * 100;
    $end = ($val/$duration) * 100 - $start;
    $sptm = okmedia_time_from_seconds($key);
    $eptm = okmedia_time_from_seconds($val);
    $splaytime = 'S: <a href="javascript:goto_seek('.$key.');">' . $sptm->h . ':' . $sptm->m . ':' . $sptm->s . '</a>';
    $eplaytime = 'E: <a href="javascript:goto_seek('.$val.');">' . $eptm->h . ':' . $eptm->m . ':' . $eptm->s . '</a>';
    $posgrade = html_writer::tag("div",$splaytime.'<br/>'.$eplaytime,array('class'=>'balloon'));
    $poshtml .= html_writer::tag("div" , $posgrade, array("class"=>"poshtmls", "data-start"=>$key, "data-end"=>$val, 'style'=>'left:'.$start.'%;width:'.$end.'%'));
}
//학습한 구간 표시
$html .= html_writer::start_div('pop_wrap');
$html .= html_writer::start_tag('div',array("id"=>"id_duration"));
$html .= $poshtml;
$html .= html_writer::end_tag("div");
$html .= html_writer::end_tag("div");

echo $html;

echo $OUTPUT->footer();
?>



