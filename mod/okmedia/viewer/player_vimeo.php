<?php
//비메오 데이터
$vids = explode('/', $content->embed_code);
$vid1 = trim(str_replace("watch?v=", "", $vids[sizeof($vids) - 1])); 
$vid1s = explode('&', $vid1);
$vid = trim($vid1s[0]);

?>

<div class="is-closeable video_area" style="height: 100%;">
    <div id="embed" style="height: 100%;"></div>
</div>

<script src="player_vimeo.js"></script>
<script src="player.js"></script>
<script>
    var options = {
        id: <?php echo $vid; ?>,
        width: 640,
        loop: false
    };

    var player = new Vimeo.Player('embed', options);
    
    var positionfrom = 0, positionto = 0, oldposition = 0, saveposition = 0, current = 0, duration = 0, evented = 0;
    var mode = "<?php echo $mode; ?>";
    var satiscompleted = $('#satiscompleted').val();
    
    player.on('loaded', function(data) {
        positionto = playtime_get(mode);
        positionfrom = positionto;

        if(positionfrom > 0) {
            player.setCurrentTime(positionfrom);
            player.pause();
        }
    });

    player.on('play', function(data) {
        positionfrom = Math.ceil(data.seconds);
        evented = 0;
    });
    
    player.on('pause', function(data) {
        //console.log('pause::'+positionfrom+'::'+positionto);
        if (positionfrom < Math.ceil(data.seconds)) {
            playtime_update(positionfrom, Math.ceil(data.seconds), 1, Math.ceil(data.duration), mode);
        }
    });
    
    player.on('ended', function(data) {
        //console.log('ended::'+positionfrom+'::'+positionto);
        if (evented == 0 && positionfrom < Math.ceil(data.seconds)) {
            playtime_update(positionfrom, Math.ceil(data.seconds), 3, Math.ceil(data.duration), mode);
        }
        evented = 1;
    });
    
    player.on('seeked', function(data) {
        //console.log('seeked::'+positionfrom+'::'+positionto);
        if (positionfrom < positionto && positionto < Math.ceil(data.duration)) {
            playtime_update(positionfrom, positionto, 2, Math.ceil(data.duration), mode);
        }
        
        positionfrom = Math.ceil(data.seconds);
        if(positionfrom == 1){ positionfrom = 0; }
        positionto = positionfrom;
        evented = 0;
        //console.log('seeked::'+positionfrom+'::'+positionto);
    });
    
    player.on('timeupdate', function(data) {
        current = Math.ceil(data.seconds);
        duration = Math.ceil(data.duration);
        // 1 초 이상 차이가 있으면 seek 한 걸로 판단 positionto 값을 변경하지 않는다
        if(Math.abs(positionto - current) == 1) {
            positionto = current;
        }
        
        //진도율 저장 (3분마다 진도율을 저장함.)
        //console.log(positionfrom+'::'+saveposition+'::'+current);
        if(evented == 0 && positionfrom < current && current % (60*3) == 0){
           playtime_update(positionfrom, current, 5, duration, mode);
           saveposition = current;
           positionfrom = current;
        }
        
        oldposition = parseInt(data.seconds);
        //caption_scroll(oldposition);
        caption_scroll2(oldposition);
    });
    
    function popClosePause(){
        playtime_update_close(positionfrom, current, 4, duration, mode);
    }
</script>