<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');

$courseid = required_param('courseid', PARAM_INT); 
$cmid = required_param('cmid', PARAM_INT); 
$userid = required_param('userid', PARAM_INT); 
$nevimode = required_param('nevimode', PARAM_RAW); 

$returnvalue = new stdClass();

$courseModInfo = get_fast_modinfo($courseid);

 // 선행조건 체크 
$coursemodule_data = $courseModInfo->get_cm($cmid);
$coursemodule_info = new \core_availability\info_module($coursemodule_data);
// 조건 확인
$uservisible = $coursemodule_info->is_user_visible($cmid, $userid);
// 조건 정보
if (!$uservisible) {
    $information = $coursemodule_info->get_full_information();
    preg_match('<AVAILABILITY_CMNAME_(\d*)\/>', $information, $getCmId);
    $availability_cmid = $getCmId[1];
     if(!empty($availability_cmid)) {
        $availability_name = $courseModInfo->cms[$availability_cmid]->name;
        $availability_str = $availability_name . '을(를) 먼저 완료해야 합니다.';
     }
}
if($availability_str) {
    $returnvalue->str = $availability_str; 
    $returnvalue->status = 'fail';
} else {
    $returnvalue->status = 'success';
    $encrypted = base64_encode($cmid . '||' . $nevimode);
    $returnvalue->url = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);