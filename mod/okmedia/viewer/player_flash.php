<?php

//스트리밍 주소 가져오기
$hlsurls = '';
$proxies = array();

$proxies['001']['idx'] = '001';
$proxies['001']['url'] = $content->embed_code;

$hlsurls = json_encode($proxies);

$hlsurls = json_encode($proxies);

if($content->islogo == 1){
    $playerlogo = '/theme/oklasscompany/pix/images/main_log o.png';
}

?>
<div class="is-closeable video_area"><a id="hlsjsvod" style="display: block; width: 660px; height: 400px; margin: auto;"></a></div>

<script type="text/javascript" src="flowplayer-3.2.12.min.js"></script>
<script type="text/javascript" src="flowplayer.ipad-3.2.12.min.js"></script>
<link href="//amp.azure.net/libs/amp/latest/skins/amp-default/azuremediaplayer.min.css" rel="stylesheet">
<script src="//amp.azure.net/libs/amp/latest/azuremediaplayer.min.js"></script>
<script src="player.js"></script>

<script type="text/javascript">
   
    var hlsurls = <?php echo $hlsurls;?>;  
    var intervalID;
    var positionfrom = 0, positionto = 0, duration = 0, oldposition = 0, saveposition = 0 ,ispause = false, current = 0, evented = 0;
    var satiscompleted = $('#satiscompleted').val();
    var mode = '<?php echo $mode?>';
    var captionurl = '';
    
    var urlExists = function(url) {

        var result = false;
        if(url != undefined && url != ''){
            $.ajax({
                type: 'HEAD',
                url: url,
                async: false,
                success: function() {result = true;}//$.proxy(callback, this, true),
            });
        }
        
        return result;
    };
    
    window.onload = function () {
        $.each(hlsurls, function(idx) {
            
            var result = urlExists(hlsurls[idx]['url']);
            
            if(result == true){     
                flowplayer_load(hlsurls[idx]['url']);
                 $f().load();
                 return false;
            }
        });
        
        if($('.caption_area').length > 0){
            $('.captiontab').change(function () {
                $f('hlsjsvod').getClip().update({
                    'captionUrl': $(this).find('option:selected').attr('subtitle')
                });
            });
        }
    };
    
    function flowplayer_load(hlsurl){
        
        $f("hlsjsvod", {src:"flowplayer.commercial-3.2.18.swf", wmode: 'opaque'}, {
        
        key: '<?php echo get_config('local_repository','flowplayer_key')?>',
        // now we can tweak the logo settings        
        logo: {
            url: '<?php echo $playerlogo;?>',
            fullscreenOnly: false,
            displayTime: 2000,
            opacity: 0.4,
            width: '200px'
        },  
        // now we can tweak the menu
        contextMenu: [
            // 1. load related videos with jQuery and AJAX into the HTML DIV
            {
                'Show related videos' : function() {
                    $("#related").load("/demos/configuration/related.html");
                }
            },

            // 2. display custom player information
            "acme.org video player 1.1"
        ],
        // configure the required plugins
        plugins: {
            httpstreaming: {
            url: 'flashlsFlowPlayer.swf',
            hls_debug : false,
            hls_debug2 : false,
            hls_lowbufferlength : 3,
            hls_minbufferlength : -1,
            hls_maxbufferlength : 60,
            hls_startfromlevel : -1,
            hls_seekfromlevel : -1,
            hls_live_flushurlcache : false,
            hls_seekmode : "ACCURATE",
            hls_fragmentloadmaxretry : -1,
            hls_manifestloadmaxretry : -1,
            hls_capleveltostage : false,
            hls_maxlevelcappingmode : "downscale"
            },
            // the captions plugin
            captions: {
                url: "flowplayer.captions-3.2.10.swf",

                // pointer to a content plugin (see below)
                captionTarget: 'content'
            },
            // configure a content plugin so that it
            // looks good for showing subtitles
            content: {
                url: "flowplayer.content-3.2.9.swf",
                bottom: 25,
                height:40,
                backgroundColor: 'transparent',
                backgroundGradient: 'none',
                border: 0,
                textDecoration: 'outline',
                style: {
                    body: {
                        fontSize: 16,
                        fontFamily: 'Arial',
                        textAlign: 'center',
                        color: '#ffffff'
                    }
                }
            }
        },
        clip: {
            accelerated: true,
            url: hlsurl,
            ipadUrl: hlsurl,
            <?php echo $captionurl;?>
            urlResolvers: "httpstreaming",
            lang: "ko",
            provider: "httpstreaming",
            autoPlay: true,
            autoBuffering: false,
            // attach event listener
            onBeforeBegin: function(clip){
                //console.log('onBeforeBegin::'+positionfrom+'::'+positionto);
                positionto = playtime_get(mode);
                positionfrom = positionto;
            },
            onStart: function(clip){
                //console.log('onStart::'+positionfrom+'::'+positionto);
                duration = Math.ceil(clip.duration);
                $f().seek(positionfrom);
                intervalID = setInterval(flashls_updateCurrentTime, 1000);
            },
            onBeforeResume: function(clip){
                //console.log('onBeforeResume::'+positionfrom+'::'+positionto);
                //플레이 버튼 클릭 시 호출
                intervalID = setInterval(flashls_updateCurrentTime, 1000);
            },
            onResume: function(clip){
                //console.log('onResume::'+positionfrom+'::'+positionto);
                positionfrom = positionto;
                evented = 0;
            },
            onPause: function(clip){
                //Pause 버튼 클릭 시 호출
                positionto = Math.ceil($f().getTime());
                //console.log('onPause::'+positionfrom+'::'+positionto);
                if (positionfrom < positionto) {
                    playtime_update(positionfrom, positionto, 1, duration, mode);
                }
                window.clearInterval(intervalID);
            },
            onSeek: function(clip){
                if (positionfrom < positionto && positionto < duration) {
                    //console.log('onSeek1::'+positionfrom+'::'+positionto);
                    playtime_update(positionfrom, positionto, 2, duration, mode);
                }
                
                positionfrom = Math.ceil($f().getTime());
                if(positionfrom == 1){ positionfrom = 0; }
                positionto = positionfrom;
                evented = 0;
                //console.log('onSeek2::'+positionfrom+'::'+positionto);
            },
            onFinish: function(clip){
                //console.log('onFinish::'+positionfrom+'::'+duration);
                if (evented == 0 && positionfrom < duration) {
                    playtime_update(positionfrom, duration, 3, duration, mode);
                }
                positionfrom = positionto = 0;
                evented = 1;
            },
            onCuepoint: function(clip, cuepoint){
                if(clip.captionUrl){
                    oldposition = parseInt(cuepoint.time/1000);
                    caption_scroll(oldposition);
                }
            }                   
        },
        onError: function(errorCode, errorMessage){ 
            alert(errorCode);
        }

        /*
        log: {
            level: 'debug',
            filter: 'org.flowplayer.controller.*'
        }
        */
        }).ipad();
        
        function flashls_updateCurrentTime() {
            current = Math.ceil($f().getTime());
            // 1 초 이상 차이가 있으면 seek 한 걸로 판단 positionto 값을 변경하지 않는다
            if(Math.abs(positionto - current) == 1) {
                positionto = current;
            }
            
            //진도율 저장 (3분마다 진도율을 저장함.)
            //console.log(positionfrom+'::'+saveposition+'::'+current);
            if(evented == 0 && positionfrom < current && current % (60*3) == 0){
               playtime_update(positionfrom, current, 5, duration, mode);
               saveposition = current;
               positionfrom = current;
            }
            
        }
        
    }
    
    function popClosePause(){
        playtime_update_close(positionfrom, current, 4, duration, mode);
    }
       
</script>

