<?php
header('Access-Control-Allow-Origin:*');
header("Content-type: application/json");
$userprogress = $track->progress;
$comprogress = $okmedia->completionprogress;
if (empty($userprogress)) {
    $userprogress = 0;
}
if (empty($track)) {
    $trackstatus = 0;
} else {
    $trackstatus = 1;
}
if($content->con_type == 'visangurl'){
    $playtyme = "application/x-mpegurl";
}else{
    $playtyme = "video/mp4";
}
//$viewer_url = $transcodingurl.'/jmedia/'.$data->data_dir.'/'.$file->filename;
?>

<style>
    #hlsjsvod .fp-logo{
        width: 150px;
        padding-left: 10px;
    }
    .flowplayer .fp-header .fp-share { display: none; }
</style>

<script src="<?php echo $CFG->wwwroot . '/chamktu/contents/viewer/flowplayer7.2.7/flowplayer.min.js' ?>"></script>
<script src="<?php echo $CFG->wwwroot . '/chamktu/contents/viewer/flowplayer7.2.7/flowplayer.hlsjs.min.js' ?>"></script>

<script src="<?php echo $CFG->wwwroot . '/mod/okmedia/viewer/player.js' ?>"></script>
<script src="player.js"></script>

<script src="https://releases.flowplayer.org/speed-menu/flowplayer.speed-menu.min.js"></script>
<div id="hlsjsvod" class="is-closeable">
    <!--    <div class="buttons">
            <span>0.75x</span>
            <span class="active">1x</span>
            <span>1.25x</span>
            <span>1.5x</span>
        </div>-->
</div>

<script>
    var positionfrom = 0, positionto = 0, duration = 0, oldposition = 0, ispause = false, capposition = 0, saveposition = 0, evented = 0;
    var satiscompleted = $('#satiscompleted').val();
    var mode = '<?php echo $mode ?>';
    var trackstatus = '<?php echo $trackstatus ?>';
    var videourl = '<?php echo $viewer_url ?>';
    var firstseek = 0;
    var play_confirm = false;
    window.onload = function () {
        
        if(navigator.userAgent.toLowerCase().indexOf('coc_coc') > 0){
           playAlert = setInterval(function() {
               if($("html").children('div').length > 0){
                   $("html").children('div').remove();
                   clearInterval(playAlert);
               }
            }, 500);
        }
        
        positionto = okmedia_playtime_get(mode);

        if (trackstatus != 0 && positionto != 0) {
            if (confirm("<?php echo get_string('okmedia:play_confirm', 'local_management'); ?>")) {
                play_confirm = true;
                positionfrom = positionto;
                flowplayer_load(videourl);
            } else {
                positionfrom = 0;
                flowplayer_load(videourl);
            }
        } else {
            positionfrom = 0;
            flowplayer_load(videourl);
        }
        return false;
    };

    function flowplayer_load() {

        flowplayer("#hlsjsvod", {
            key: "<?php echo get_config('local_repository','flowplayer_key')?>",
            splash: false,
            autoplay: true,
            fullscreen: true,
            unload:false,
            embed: false, // setup would need iframe embedding
            ratio: 9 / 16,
            // manual HLS level selection for Drive videos
            hlsQualities:  "driver",
            speeds: [0.5,0.75,1,1.25,1.5,2.0],
            hlsjs: {recoverNetworkError: true},
            clip: {
                sources: [
                    {type: '<?php echo $playtyme?>', src: videourl},
//                    {type: 'video/mp4', src: videourl, suffix: 'mp4'}
                ]
            }

        }).on("ready", function (e, api, video) {
            if(play_confirm) {
                setTimeout(function() {
                    api.pause();
                    api.seek(positionfrom);
                }, 200);
            }
            
            //pop_progress();
            duration = Math.floor(video.duration);
            firstseek = 1;
        });
    }

    /* global event listeners for demo purposes, omit in production */
    flowplayer(function (api, root) {

        seekback = '<?php echo $useseek ?>';
        if (seekback == 'unuse') {
            api.on("beforeseek", function (e) {
                if (firstseek != 0) {
                    e.preventDefault();
                }
            });

            $(".fp-buffer, .fp-progress", root).on("mousedown touchstart", function (e) {

                e.stopPropagation();
            });
            $(root).removeClass("is-touch");
        }

        var selfprogress = <?php echo $userprogress ?>;
        var comprogress = <?php echo $comprogress ?>;

        var instanceId = root.getAttribute("data-flowplayer-instance-id"),
                engineInfo = document.getElementById("engine" + instanceId),
                vtypeInfo = document.getElementById("vtype" + instanceId),
                detail = document.getElementById("detail" + instanceId);

        api.on("resume", function (e) {
            //플레이 버튼 클릭 시 호출
//            console.log('resume::'+positionfrom+'::'+positionto);
            if (positionto > 1) {
                positionfrom = positionto;
            }
            evented = 0;
        }).on("pause", function (e, api) {
            //Pause 버튼 클릭 시 호출
//            console.log('pause::'+positionfrom+'::'+positionto);
            if (positionfrom < positionto) {
                okmedia_playtime_update(positionfrom, positionto, 1, duration, mode);
            }
//            alert(positionfrom);
//            alert(positionto);
        }).on("beforeseek", function (e, api) {
            //seek 버튼 클릭 시 호출
//            console.log('beforeseek::'+positionfrom+'::'+positionto);
            if (seekback == 'use') {
                if (positionfrom < positionto) {
                    okmedia_playtime_update(positionfrom, positionto, 2, duration, mode);
                }
            }
        }).on("seek", function (e, api) {
            //seek 버튼 클릭 후 seek되면 호출
//            console.log('seek::'+positionfrom+'::'+positionto);
            if (positionto > 1) {
                positionfrom = positionto;
            }
            evented = 0;
        }).on("finish", function (e, api) {
//            console.log('finish::'+positionfrom+'::'+positionto);
            if (evented != 3 && positionfrom < duration) {
                okmedia_playtime_update(positionfrom, duration, 3, duration, mode);
            }
            positionfrom = positionto = 0;
            evented = 1;
        }).on("progress", function (e, api) {
            //타임라인
            positionto = Math.ceil(api.video.time);

            //진도율 저장 (3분마다 진도율을 저장함.)
            if (evented == 0 && positionfrom < positionto && positionto % (10) == 0) {
                okmedia_playtime_update(positionfrom, positionto, 5, duration, mode);
                saveposition = positionto;
                positionfrom = positionto;
            }
        }).on("cuepoint", function (e, api, cuepoint) {
            if (cuepoint.subtitle) {
                oldposition = parseInt(cuepoint.time);
                caption_scroll(oldposition);
            }
        });
    });

    function device_check() {

        //디바이스별 판별
        var _ua = window.navigator.userAgent.toLowerCase();
        var device = '';
        var browser = {
            ipod: /webkit/.test(_ua) && /\(ipod/.test(_ua),
            ipad: /webkit/.test(_ua) && /\(ipad/.test(_ua),
            iphone: /webkit/.test(_ua) && /\(iphone/.test(_ua),
            android: /webkit/.test(_ua) && /android/.test(_ua),
            msie: /msie/.test(_ua)
        };

        if (browser.ipod || browser.iphone) {
            device = 'iphone';
        } else if (browser.ipad) {
            device = 'ipad';
        } else if (browser.android) {
            device = 'android';
        } else if (browser.msie) {
            device = 'msie';
        } else {
            device = 'etc';
        }

        return _ua;

    }

    /* end global event listeners setup */
    function popClosePause() {
        okmedia_playtime_update_close(positionfrom, positionto, 4, duration, mode);
    }

    function popMovePause() {
        okmedia_playtime_update(positionfrom, positionto, 4, duration, mode);
    }

    function okmedia_playtime_update(posfrom, posto, posevent, duration, mode) {
        var status = true;
        if (mode == 'player') {
            var device = device_check();

            $.ajax({
                url: 'okmedia_playtime_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: true,
                data: {
                    act: 'update',
                    positionto: posto,
                    positionfrom: posfrom,
                    positionevent: posevent,
                    duration: duration,
                    device: device,
                    id: $('#okmediaid').val()
                },
                success: function (data) {
                    if (data.status == 'success') {
                        status = true;
                        if (data.progress != ' %') {
                            $('#id_lastview').text(data.last);
                            $('#id_playtime').text(data.totaltime);
                            $('#id_progress').text("진도율 " + data.progress);
                        }
                        //pop_progress();
                    } else {
                        status = false;
                    }
                },
                error: function (e) {

                    //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                    //playtime_update(id,posfrom,posto,duration,second);
                    status = false;
                }
            });
        }

        return status;
    }

    function okmedia_playtime_update_close(posfrom, posto, posevent, duration, mode) {
        var status = false;
        if (mode == 'player') {
            var device = device_check();
            $.ajax({
                url: 'okmedia_playtime_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: true,
                data: {
                    act: 'update',
                    positionto: posto,
                    positionfrom: posfrom,
                    positionevent: posevent,
                    duration: duration,
                    device: device,
                    id: $('#okmediaid').val()
                },
                success: function (data) {
                    if (data.status == 'success') {
                        status = true;
                        window.opener.location.reload();
                        self.close();
                    }
                },
                error: function (e) {
                    console.log(data);
                    //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                    //playtime_update(id,posfrom,posto,duration,second);
                    status = false;
                }
            });
        } else {
            status = true;
            window.parent.close();
        }
        return status;
    }

    function okmedia_playtime_get(mode) {

        var positionfrom = 0;

        if (mode == 'player') {

            var device = device_check();

            $.ajax({
                url: 'okmedia_playtime_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {
                    act: 'get',
                    device: device,
                    id: $('#okmediaid').val()
                },
                success: function (data) {
                    if (data.status == 'success') {
                        if (data.positionto > 0) {
                            //이어보기
                            positionfrom = data.positionto;
                        }

                    } else {
                        //alert(data.message);
                    }
                },
                error: function (e) {
                    //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                }
            });
        }

        return positionfrom;

    }
</script>
