<?php

require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->libdir . '/excellib.class.php');

function mod_okmedia_browser_check() { 

    $data = new stdClass();
    $data->device = '';

    $agent = get_browser(null, true);
    $data->browser = $agent['browser'];          // 브라우저 종류
    $data->version = $agent['version'];           // 브라우저 버전
    $data->platform = $agent['platform'];         // OS 종류 Win7, Win10, iOS, Android
    $data->device = $agent['device_type'];     // Desktop, Mobile Phone

    if ($data->device == 'Mobile Phone' || $data->device == 'Mobile Device') {
        $data->device = 'M';
    } else if ($data->device == 'Desktop') {
        $data->device = 'P';
    } else {
        $data->device = 'E';
    }

    return $data;
}

function mod_okmedia_mobile_check() {
    // 모바일 기종(배열 순서 중요, 대소문자 구분 안함)
    $ary_m = array("iPhone", "iPod", "IPad", "Android", "Blackberry", "SymbianOS|SCH-M\d+", "Opera Mini", "Windows CE", "Nokia", "Sony", "Samsung", "LGTelecom", "SKT", "Mobile", "Phone");
    for ($i = 0; $i < count($ary_m); $i++) {
        if (preg_match("/$ary_m[$i]/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return $ary_m[$i];
            break;
        }
    }
    return "PC";
}

function browser_check() {

    $userAgent = $_SERVER["HTTP_USER_AGENT"];
    if (preg_match("/MSIE*/", $userAgent)) {
        // 익스플로러

        if (preg_match("/MSIE 6.0[0-9]*/", $userAgent)) {
            $browser = 11;
        } else if (preg_match("/MSIE 7.0*/", $userAgent)) {
            $browser = 11;
        } else if (preg_match("/MSIE 8.0*/", $userAgent)) {
            $browser = 11;
        } else if (preg_match("/MSIE 9.0*/", $userAgent)) {
            $browser = 11;
        } else if (preg_match("/MSIE 10.0*/", $userAgent)) {
            $browser = 11;
        } else {
            // 익스플로러 기타
            $browser = 11;
        }
    } else if (preg_match("/Trident*/", $userAgent) && preg_match("/rv:11.0*/", $userAgent) && preg_match("/Gecko*/", $userAgent)) {
        $browser = 11;
    } else if (preg_match("/(Mozilla)*/", $userAgent)) {
        // 모질라 (파이어폭스)
        $browser = "mozilla";
    } else if (preg_match("/(Nav|Gold|X11|Mozilla|Nav|Netscape)*/", $userAgent)) {
        // 네스케이프, 모질라(파이어폭스)
        $browser = "Netscape/mozilla";
    } else if (preg_match("/Opera*/", $userAgent)) {
        // 오페라
        $browser = "Opera";
    } else {
        $browser = "Other";
    }
    return $browser;
}

function mod_okmedia_get_nextprev($courseid, $moduleid, $status) {
    global $DB;
    $nextprevsql = "  select @rownum:=@rownum+1 num ,datas.*  from ( 
                            select cm.id,cs.section,mo.name
                            from m_course_modules  cm
                            JOIN m_modules mo ON mo.id = cm.module
                            join m_course_sections cs on cm.section = cs.id and  cs.section != 0
                            where cm.course= :courseid and cm.visible != 0 and (@rownum:=0)=0 
                            order by cs.section asc,cm.id asc) datas ,(select @rownum:=0) TMP ";


    $npparams['courseid'] = $courseid;
    $npparams['nowid'] = $moduleid;

    $buttons = $DB->get_records_sql($nextprevsql, $npparams);
    foreach ($buttons as $key => $button) {
        if ($button->id == $moduleid) {
            $buttonkey = $key;
        }
    }
    if ($status == 'next') {
        $buttonkey = $buttonkey+1;
    } else if ($status == 'before') {
        $buttonkey = $buttonkey-1;
    }

    $encrypted = '';
    if (!empty($buttons[$buttonkey])) {
        $nextData = new stdclass();
        if ($buttons[$buttonkey]->name == 'okmedia') {
            $nevimode = "player";
        } else {
            $nevimode = "embed";
        }
        $nextData->encrypted = base64_encode($buttons[$buttonkey]->id . '||' . $nevimode);
        $nextData->nextCmid = $buttons[$buttonkey]->id;
        $nextData->nextModule = $buttons[$buttonkey]->name; 
    } 

    return $nextData;
}
