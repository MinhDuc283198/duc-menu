<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)) . '/lib.php');
require_once(dirname(dirname(__FILE__)) . '/locallib.php');
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/mod/okmedia/viewer/lib.php');
require_once $CFG->dirroot . '/lib/form/filemanager.php';

$v = required_param('v', PARAM_RAW); 
$decrypted = base64_decode($v);

list($id, $userid, $mode) = explode('||', $decrypted);

$sql = 'SELECT okm.id ,okm.name, lcf.filepath, lcf.filename, cm.section  
        FROM {course_modules} cm 
        JOIN {okmedia} okm ON cm.instance = okm.id 
        LEFT JOIN {lcms_contents_file} lcf ON okm.contents = lcf.con_seq 
        LEFT JOIN {okmedia_track} ot ON okm.id = ot.okmediaid 
        WHERE cm.id = :id ';
$param = array('id' => $id);

$media = $DB->get_record_sql($sql, $param);

list ($course, $cm) = get_course_and_cm_from_cmid($id, 'okmedia');
if (!$okmedia = $DB->get_record('okmedia', array('id' => $cm->instance))) {
    print_error('course module is incorrect');
}



//$useprogress = $DB->get_field('lmsdata_class', 'useprogress', array('courseid' => $course->id));
//if ($useprogress != 1) {
//    $mode = 'embed';
//}

require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
$PAGE->set_context($context);
okmedia_view($media, $course, $cm, $context);
$PAGE->set_pagelayout('popup');
$PAGE->set_url('/mod/okmedia/viewer/player.php', array('v' => $v));

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->css('/mod/okmedia/viewer/flowplayer7.2.7/skin/skin.css');
$PAGE->requires->css('/theme/oklassedu/style/popup.css');
echo $OUTPUT->header();

$conid = $okmedia->contents;
$classtitle = $DB->get_field("lmsdata_class","title",array("courseid"=>$COURSE->id));
//LCMS 동영상 데이터 가져오기
if (!$content = $DB->get_record('lcms_contents', array('id' => $conid))) {
    print_error(get_string('nocontents', 'okmedia'));
    die();
}
$file = $DB->get_record('lcms_contents_file', array('con_seq' => $content->id));
$filename = $file->filename;
$viewcount = $okmedia->viewcount + 1;
$DB->set_field('okmedia', 'viewcount', $viewcount, array('id' => $okmedia->id));
$okmediaprogress = $DB->get_field('okmedia', 'completionprogress', array('id' => $okmedia->id));
$okmediatimestart = $okmedia->timestart;

//진도율 가져오기
$lastview = '0000-00-00 00:00';
$playtime = '00:00';
$progress = '0%';
$studying = get_string('beforestudy', 'okmedia');
if ($track = $DB->get_record('okmedia_track', array('okmediaid' => $okmedia->id, 'userid' => $USER->id))) {
    $lastview = date('Y-m-d H:i', $track->timeview);
    $ptm = okmedia_time_from_seconds($track->playtime);
    $playtime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
    $progress = $track->progress . '%';
    $studying = ($track->progress <= 100) ? get_string('studying', 'okmedia') : get_string('complete', 'okmedia');
}
if ($okmedia->timestart < time() && time() < $okmedia->timeend && $track->progress < 100) {
    $useseek = 'unuse';
} else {
    $useseek = 'use';
}

// okmedia 마감시간 
$timeend = $okmedia->timeend;

// okmedia 마감후 보기 기능 0 숨기기, 1 보기(진도체크 불가), 2 보기 (진도체크가능
$islock = $okmedia->islock;

$fr = '로딩중..';

$broswer = browser_check();
$userrolecheck = $DB->get_field('lmsdata_user', usergroup, array('userid' => $USER->id));
$play_url = $CFG->vodurl . '/_definst_/' . $media->filepath . '/' . str_replace('.mp4', '_hd.mp4', $media->filename).'/playlist.m3u8';

$sql = "SELECT rep.id, lcf.filename, lcf.filepath, lcf.duration, "
        . "con.id as con_id, con.con_name, con.con_type, con.con_des, "
        . "con.update_dt, con.data_dir, con.embed_type, con.embed_code "
        . "FROM {lcms_repository} rep "
        . "JOIN {lcms_contents} con ON con.id= rep.lcmsid "
        . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
        . "LEFT JOIN {lcms_track} lt ON rep.lcmsid = con.id = lt.lcms "
        . "WHERE con.id= :id";
$data = $DB->get_record_sql($sql, array('id' => $conid));

$transcodingurl = get_config('moodle','vodurl');
$viewer_url = $transcodingurl.'/'.$data->data_dir.'/'.$file->filename;
if($content->con_type == 'visangurl'){
    $viewer_url = $file->filename;
}

//교안 가져오기
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_okmedia', 'tplan', $media->id, 'sortorder, id', false);
$file = reset($files);
unset($files);
$output = '';
if ($file) {
    $path = '/'.$context->id.'/mod_okmedia/tplan/'.'1'.$file->get_filepath().$file->get_filename();
    
    $fullurl = moodle_url::make_file_url('/pluginfile.php', $path);
   
    $type = '';
    $attfile = '';
    //foreach ($files as $file) {

        $filepath = $file->get_filepath();
        if (empty($filepath)) {
            $filepath = '/';
        }

        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_okmedia/tplan/' . $media->id . $filepath . $filename);
        
   
        if ($file->get_filesize() > 0) {
            //$attfile .= "<a href=\"$path\">$iconimage</a> ";
            $target = (strpos($path, '.pdf') !==false) ? ' target="_blank" ': '';
            $attfile .= format_text("<a href=\"$path\" class='btns br dwn' ".$target.">교안 다운로드</a>", FORMAT_HTML, array('context' => $context));
        } else {
            $alertstring = get_string('deletedfile', 'jinotechboard');
            //$attfile .= '<a href="#" class="btns br dwn"> onclick="javascript:alert(\'' . $alertstring . '\');">' . $iconimage . '</a>';
            $attfile .= '<a href="#" class="btns br dwn" onclick="javascript:alert(\'' . $alertstring . '\');">교안 다운로드</a>';
        }
    //}

    $attachments .= $attfile;

    $output .= $attfile;

}

$relearning = $DB->get_field('lmsdata_class','reviewperiod' ,array("courseid"=>$course->id));
$expday = $DB->get_field_sql("select timeend from {enrol} e 
        join {user_enrolments} ue on e.id = ue.enrolid 
        where ue.userid = ? and e.courseid =? ",array($USER->id,$course->id));
$acceptlearn = strtotime("-$relearning days", $expday);


?>

<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<link href="/theme/oklassedu/style/media.css" rel="stylesheet" />
<script type="text/javascript" src="/lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js"></script>
<script src="/theme/oklassedu/javascript/theme.js" type="text/javascript"></script>
<div class="popwrap footer_pop video_pop">
    <div class="media_menu" style="/* min-width: 440px !important; */">
        <ul class = "menu_ul">
            <?php
            $section_sql = "SELECT cs.* FROM {course_modules} cm 
                            JOIN {course} co ON co.id = cm.course
                            JOIN {course_sections} cs ON cs.section != 0 AND cs.course = co.id
                            WHERE cm.id = :coursemodule
                            ORDER BY cs.section ";
            $section_lists = $DB->get_records_sql($section_sql, array('coursemodule' => $id));
            
            $courseModInfo = get_fast_modinfo($course);
            
            foreach ($section_lists as $section_list) {
                $module_sql = "SELECT cm.*, mo.name 
                                   FROM {course_modules} cm
                                   JOIN {modules} mo ON mo.id = cm.module
                                   WHERE cm.course = :course AND cm.section = :section AND cm.visible = 1 ";
                $module_lists = $DB->get_records_sql($module_sql, array('course' => $section_list->course, 'section' => $section_list->id));

                $litag = '';
                foreach ($module_lists as $module_list) {
                    $module_info_sql = "SELECT * FROM {" . $module_list->name . "} WHERE id = :instance ";
                    $module_info = $DB->get_record_sql($module_info_sql, array('instance' => $module_list->instance));
                    if ($module_list->name == 'okmedia') {
                        $nevimode = "player";
                    } else {
                        $nevimode = "embed";
                    }
                    $encrypted = base64_encode($module_list->id . '||' . $nevimode);
                    
                    $litag .= html_writer::start_tag('li');
                    $litag .= '<a class="completionCheck" data-cmid="'.$module_list->id.'" data-nevimode="'.$nevimode.'">' . $module_info->name . '</a>';
                    $litag .= html_writer::end_tag('li');
                }

                if (empty($section_list->name)) {
                    $tag_name = $section_list->section . '차';
                } else {
                    $tag_name = $section_list->name;
                }
                $liparams = array('name' => $tag_name);
                if ($section_list->id == $media->section) {
                    $liparams['class'] = 'on';
                }
                echo html_writer::start_tag('li', $liparams);
                echo html_writer::tag('h3', $tag_name);
                echo html_writer::start_tag('ul');
                echo $litag;
                echo html_writer::end_tag('ul');
                echo html_writer::end_tag('li');
            }
            ?>
        </ul>
    </div>
    <div class="pop_title">
        <p class="c-nm"><?php  echo $classtitle ?> </p>
        <span class="pop_menu">메뉴</span>
        <span><?php echo $media->name; ?></span>
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_close_w.png" onclick ="window.close();" />
    </div>
    <div class="pop_content">
        <div class="video_area">
            <?php
            echo '<input type="hidden" value="' . $media->id . '" id = "okmediaid">';
            if ($content->con_type == 'video') {
                include_once('player_video.php');
            } else if ($content->con_type == 'embed') {
                if ($content->embed_type == 'youtube') {
                    include_once('player_youtube.php');
                } else if ($content->embed_type == 'vimeo') {
                    include_once('player_vimeo.php');
                } else {
                    include_once('player_other.php');
                }
            } else if ($content->con_type == 'visangurl') {
                require_once $CFG->dirroot . '/local/management/contentsI/lib.php';
                $viewer_url = visang_cdn_tokenmaker($content->embed_code);
                include_once('player_video.php');
            }
            ?>
            <!--<iframe src="<?php echo $play_url; ?>" frameborder="0" allowfullscreen></iframe>-->
        </div>
        <!-- 버튼 영역 -->
        <div class="video_buttons">
            <?php
            $section_btn_sql = "SELECT cs.* FROM {course_modules} cm 
                            JOIN {course} co ON co.id = cm.course
                            JOIN {course_sections} cs ON cs.section != 0 AND cs.course = co.id
                            WHERE cm.id = :coursemodule
                            ORDER BY cs.section ";
            $section_btn_lists = $DB->get_records_sql($section_sql, array('coursemodule' => $id));
            $aftertag = '';
            $nowtag = '';
            $beforetag = '';

            foreach ($section_btn_lists as $section_list) {
                $module_sql = "SELECT cm.*, mo.name 
                                   FROM {course_modules} cm
                                   JOIN {modules} mo ON mo.id = cm.module
                                   WHERE cm.course = :course AND cm.section = :section AND cm.visible = 1 
                                   ORDER BY cm.section asc";
                $module_lists = $DB->get_records_sql($module_sql, array('course' => $section_list->course, 'section' => $section_list->id));

                foreach ($module_lists as $module_list) {
                    if ($module_list->name == 'okmedia') {
                        $nevimode = "player";
                    } else {
                        $nevimode = "embed";
                    }
                    $encrypted = base64_encode($module_list->id . '||' . $nevimode);

                    if (!empty($nowtag) && empty($aftertag)) {
//                        $aftertag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    } else if ($id == $module_list->id && empty($nowtag)) {
                        $nowtag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    }
                }
            }
            foreach ($section_btn_lists as $section_list) {
                $module_sql = "SELECT cm.*, mo.name 
                                   FROM {course_modules} cm
                                   JOIN {modules} mo ON mo.id = cm.module
                                   WHERE cm.course = :course AND cm.section = :section AND cm.visible = 1 
                                   ORDER BY cm.section desc";
                $module_lists = $DB->get_records_sql($module_sql, array('course' => $section_list->course, 'section' => $section_list->id));

                foreach ($module_lists as $module_list) {
                    if ($module_list->name == 'okmedia') {
                        $nevimode = "player";
                    } else {
                        $nevimode = "embed";
                    }
                    $encrypted = base64_encode($module_list->id . '||' . $nevimode);

                    if (!empty($nowtag) && empty($beforetag)) {
//                        $beforetag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    } else if ($id == $module_list->id && empty($nowtag)) {
                        $nowtag = $CFG->wwwroot . '/mod/okmedia/link.php?v=' . $encrypted;
                    }
                }
            }
            if ($beforeend = mod_okmedia_get_nextprev($course->id, $id, 'before')) {
                if ($beforeend->nextModule == 'okmedia') {
                    $nevimode = "player";
                } else {
                    $nevimode = "embed";
                }
                echo '<a data-cmid="'.$beforeend->nextCmid.'" data-nevimode="'.$nevimode.'" class="prev completionCheck">PREV</a>';
            }
            
            if ($nextenc = mod_okmedia_get_nextprev($course->id, $id, 'next')) {
                if ($nextenc->nextModule == 'okmedia') {
                    $nevimode = "player";
                } else {
                    $nevimode = "embed";
                }
                echo '<a data-cmid="'.$nextenc->nextCmid.'" data-nevimode="'.$nevimode.'" class="next completionCheck">NEXT</a>';
            }
       
            ?>
        </div>
        <!-- 버튼 영역 -->
    </div>
    <div class="pop_footer">
        <ul>
            <li><?php echo '<li id="id_lastview"  title="최근에 콘텐츠를 본 시간입니다. ">' . $lastview . '</li>' . '<li id="id_progress">'.get_string('okmedia:play_progress', 'local_management').':' . $progress . '</li>'; ?></li> 
        </ul>
        <?php echo $output; ?>
        <a href="#"  class = "btn black" onclick ="popClosePause();"><?php echo get_string('okmedia:play_stop', 'local_management'); ?></a>
    </div>
</div>
<script type="text/javascript" src="Osinfo.js"></script>
<script>

            $(document).ready(function () {
                var acceptlearn = '<?php echo $acceptlearn?>';
                var nowdate = '<?php echo time(); ?>';
                var timeend = '<?php echo $timeend; ?>';
                var islock = '<?php echo $islock; ?>';
                var timestart = '<?php echo $okmediatimestart ?>';
                var usercheck = '<?php echo $userrolecheck ?>';
                if (nowdate > acceptlearn) {
                    alert("현재는 복습기간입니다 진도율에 반영되지 않습니다.");
                }
                if (nowdate > timeend && islock == 1) {
                    alert("본 교육과목은 수강기간 종료후에도 복습은 가능합니다.\n\n단, 수강기간 종료 후 복습 시 수강인정(진도율) 재반영은\n적용되지 않습니다.");
                }
                //접근제한 체크
                $(".completionCheck").click(function() {
                    $.ajax({
                        url: '/mod/okmedia/viewer/completion.ajax.php',
                        type: 'POST',
                        dataType: 'json',
                        async: true,
                        data: {
                            courseid: <?php echo $COURSE->id; ?>,
                            userid: <?php echo $USER->id; ?>,
                            cmid: $(this).data("cmid"),
                            nevimode: $(this).data("nevimode")
                        },
                        success: function (data) {
                            console.log(data);
                            if (data.status == 'fail') {
                                alert(data.str);
                                return false;
                            } else {
                                document.location.href = data.url;
                            }
                        }
                    });
                });
                
            });

            $(".popclose").click(function () {
                opener.location.reload();
            });

            // 네비게이션 이벤트
            pop_menu_evnet();
            
            $('.media_menu .menu_ul > li').click(function () {
                var arr = new Array();
                $('.media_menu .menu_ul > li').each(function () {
                    $(this).removeClass('on');
                });
                $(this).addClass('on');
            });

            function get_version_of_IE() {

                var word;

                var agent = navigator.userAgent.toLowerCase();

                // IE old version ( IE 10 or Lower ) 
                if (navigator.appName == "Microsoft Internet Explorer")
                    word = "msie ";

                // IE 11 
                else if (agent.search("trident") > -1)
                    word = "trident/.*rv:";

                // Microsoft Edge  
                else if (agent.search("edge/") > -1)
                    word = "edge/";

                // 그외, IE가 아니라면 ( If it's not IE or Edge )  
                else
                    return -1;

                var reg = new RegExp(word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})");

                if (reg.exec(agent) != null)
                    return parseFloat(RegExp.$1 + RegExp.$2);

                return -1;
            }

</script>

<?php
echo $OUTPUT->footer();

