<?php

function okmedia_get_captions($srtpath){
    
    if ($srtpath) {
        define('SRT_STATE_SUBNUMBER', 0);
        define('SRT_STATE_TIME', 1);
        define('SRT_STATE_TEXT', 2);
        define('SRT_STATE_BLANK', 3);

        $lines = file($srtpath);
        $total = count($lines);
        $numb = 0;

        $subs = array();
        $state = SRT_STATE_SUBNUMBER;
        $subNum = 0;
        $subText = '';
        $subTime = '';

        foreach ($lines as $line) {

            $numb++;

            switch ($state) {
                case SRT_STATE_SUBNUMBER:
                    preg_match('/[0-9]+/', $line, $numarr);
                    $subNum = trim($numarr[0]);
                    $state = SRT_STATE_TIME;
                    break;

                case SRT_STATE_TIME:
                    $subTime = trim($line);
                    $state = SRT_STATE_TEXT;
                    break;

                case SRT_STATE_TEXT:
                    if (trim($line) == '' || $numb == $total) {
                        if ($numb == $total) {
                            $subText .= $line;
                        }
                        $sub = new stdClass;
                        $sub->number = $subNum;

                        list($startTime, $stopTime) = explode(' --> ', $subTime);

                        $sub->text = $subText;
                        $sub->starttime = $startTime;
                        $sub->stoptime = $stopTime;
                        $subText = '';
                        $subs[] = $sub;
                        $state = SRT_STATE_SUBNUMBER;
                    } else {
                        $subText .= $line;
                    }
                    break;
            }
        }
        
    }
    
    return $subs;
    
}
