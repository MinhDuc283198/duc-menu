<?php

defined('MOODLE_INTERNAL') || die();

class mod_okmedia_renderer extends plugin_renderer_base {
    function view($okmedia, $cm, $course) {
        global $CFG;
        
        $width = 600;
        $height = 400;
        
        //echo '<button id="lcms_viewcontent" onclick="window.open(\''.$lcms->viewurl.'\', \''.$lcms->name.'\', \'toolbar=0,scrollbars=0,location=0,menubar=0,status=0,width='.$width.',height='.$height.'\'); return false;">'.get_string('viewcontent', 'lcms').'</button>';
        echo '<button id="okmedia_viewcontent" onclick="window.open(\''.$CFG->wwwroot.'/mod/okmedia/play.php?id='.$okmedia->id.'\', \'okmedia\', \'toolbar=0,scrollbars=0,location=0,menubar=0,status=0,resizable=yes,width='.$width.',height='.$height.'\'); return false;">'.get_string('viewcontent', 'okmedia').'</button>';
    }
}
?>
