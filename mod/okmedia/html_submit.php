<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/repository/lib.php');
require_once($CFG->dirroot . '/local/repository/config.php');
require_once $CFG->dirroot . '/local/repository/pclzip.lib.php';

$formid = optional_param('formid', '', PARAM_RAW);
$conid = optional_param('conid', 0, PARAM_INT);

if (!$conid) {
    
    $path_dir = $CFG->dirroot . '/local/repository/contents_upload/server/php/files/' . $USER->id;

    if (!is_dir($path_dir)) {
        if (!mkdir($path_dir, 0777, true)) {
            return 'create folder fail';
        }
    }

    $f = move_uploaded_file($_FILES['file_html']['tmp_name'], $path_dir . '/' . $_FILES['file_html']['name']);



    $contents = (object) $_REQUEST;
    $newconid = upload_lcms_contents($contents, 0);
    insert_lcms_history($newconid, 'Insert lcms content in local repository', 1);
} else {
    $contents = (object) $_REQUEST;
    $contents->con_id = $conid;
    if($_FILES['file_html']['name'] != ''){
        $contents->stay_file = 1;
        
        $path_dir = $CFG->dirroot . '/local/repository/contents_upload/server/php/files/' . $USER->id;

       if (!is_dir($path_dir)) {
            if (!mkdir($path_dir, 0777, true)) {
                return 'create folder fail';
            }
        }

        $f = move_uploaded_file($_FILES['file_html']['tmp_name'], $path_dir . '/' . $_FILES['file_html']['name']);
    } else {
        $contents->stay_file = 0;
    } 
        
    $newconid = update_lcms_contents($contents);
    insert_lcms_history($newconid, 'Update lcms content in multiple', 3);
}
redirect('htmls.php?&formid='.$formid);
?>