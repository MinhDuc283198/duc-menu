M.mod_okmedia = M.mod_okmedia || {};

M.mod_okmedia.init_edit_form = function(Y, wwwroot, formid) {
    this.wwwroot = wwwroot;
    this.formid = formid;
    var mform = document.getElementById(this.formid);
    //mform.titlestatic.value = mform.title.value;
}

M.mod_okmedia.iframePopup = function(url) {
    var divWidth = 1010;
    var divHeight = 730;
    
    var viewSize = M.mod_okmedia.viewport();
    
    var divX = (viewSize.width - divWidth) / 2;
    if(divX < 0) divX = 0;
    
    var divY = (viewSize.height - divHeight) / 2;
    if(divY < 0) divY = 0;
    
    var vscroll = M.mod_okmedia.scrollTop();
        
    var contentsDiv = document.createElement("div");
    contentsDiv.style.height = divHeight + "px";
    contentsDiv.style.background = "white";
    contentsDiv.style.color = "black";
    contentsDiv.style.position = "absolute";
    contentsDiv.style.border = "1px solid #ccc";
    contentsDiv.style.zIndex = "99999";
    contentsDiv.innerHTML = "<div class='pop_title'>콘텐츠 등록\n\
<img src='/theme/oklassedu/pix/images/icon_pop_close.png' class='r_content close' onclick='M.mod_okmedia.close()'>\n\
</div>\n\
<div class='pop_content'>\n\
<iframe src='" + url + "' />\n\
</div>";
    contentsDiv.setAttribute("id", "mod_okmedia_contents");
    contentsDiv.setAttribute("class","popwrap");
    contentsDiv.style.visibility= "visible";
    contentsDiv.style.left = divX + "px";
    contentsDiv.style.top = (vscroll + divY) + "px";
    document.body.appendChild(contentsDiv);
}

/**
 * 강의 컨텐츠 선택 팝업
 * @param {type} id
 */
M.mod_okmedia.show = function (id,type) {
    var url = this.wwwroot + "/mod/okmedia/contents.php?id="+id+"&type="+type;
    M.mod_okmedia.iframePopup(url);
}

/**
 * 강의컨텐츠 업로드 팝업 
 * @param {type} id
 */

M.mod_okmedia.upload = function (id) {
    var url = this.wwwroot + "/mod/okmedia/contents_upload.php";
    M.mod_okmedia.iframePopup(url);
    
    var iframe = $('iframe[src="'+url+'"]');
    iframe.addClass('contents_upload');
//    $(iframe).load(function(){
//        $(iframe).contents().find('.mform .fitem div.fitemtitle').css('width', '10%');
//        $(iframe).contents().find('.mform .fitem div.fitemtitle').siblings('.felement').css('margin-left', '10%')
//        $(iframe).contents().find('#sidebar').hide();
//        $(iframe).contents().find('#footer').hide();
//        $(iframe).contents().find('.page_navbar').hide();
//        $(iframe).contents().find('#contents').css('width', '97%');
//        $(iframe).contents().find('#contents').css('background-color', '');
//        $(iframe).contents().find('#content').css('border-left', '');
//    });
}

/**
 *  한양대학교 특화된 담당교수 선택 팝업 
 */
M.mod_okmedia.prof = function () {
    var url = this.wwwroot + "/mod/okmedia/profsearch.php";
    M.mod_okmedia.iframePopup(url);
}

M.mod_okmedia.close = function() {
    var contentsDiv = document.getElementById("mod_okmedia_contents");
    if (contentsDiv != null) {
        document.body.removeChild(contentsDiv);
        contentsDiv = null;
    }
}

M.mod_okmedia.position = function (obj) {
    var curleft = 0;
    var curtop = 0;
    
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return {X:curleft,Y:curtop};
    }
}

M.mod_okmedia.viewport = function () {
    var myWidth = 0;
    var myHeight = 0;

    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if( document.documentElement &&
            ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    return {"width": myWidth, "height": myHeight}
}

M.mod_okmedia.documentheight = function () {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

M.mod_okmedia.select = function (content) {
    var mform = document.getElementById(this.formid);
    
    mform.titlestatic.value = content.title;
    mform.title.value = content.title;
    mform.contents.value = content.contents;
    mform.type.value = content.type;
}

M.mod_okmedia.scrollTop = function () {
	return M.mod_okmedia.filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
}

M.mod_okmedia.filterResults = function (n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

M.mod_okmedia.search_embed_contents = function () {
    var type = $('select[name=emb_type] option:selected').val(), type_txt = '';
    type_txt = (type == 'youtube') ? 'Youtube' : 'Vimeo';
    var title = type_txt;
    var dir = '/local/repository/ajax/search_embed_' + type + '.php';
    var search = $('input[name=emb_search]').val(), type_txt = '';
    var tag = $("<div id='load_form'></div>");
    $.ajax({
        url: dir,
        data: {
            search: search
        },
        success: function (data) {

                $('body').css({'overflow': 'hidden'});
                tag.html(data).dialog({
                    title: 'Youtube',
                    modal: true,
                    width: 670,
                    height: 650,
                    buttons: [{
                            id: 'close',
                            text: 'Close',
                            disable: true,
                            click: function () {
                                $('body').css({'overflow': 'auto'});
                                $('#load_form').remove();
                                $(this).dialog("close");
                            }
                        }]
                }).dialog('open');
                $(".ui-dialog-titlebar-close").click(function () {
                    $('body').css({'overflow': 'auto'});
                    $('#load_form').remove();
                });
        }

    });
}

M.mod_okmedia.show2 = function (id) {    
    var url = this.wwwroot + "/mod/okmedia/htmls.php?id="+id+"&formid="+this.formid; 
    var popOption = "width=1000, height=641, resizable=no, scrollbars=no, status=no;";
    window.open(url,'htmlSelector',popOption);
}