<?php 
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$id = required_param('conid', PARAM_INT);
$formid = optional_param('formid', '', PARAM_RAW);

$sql = 'select lc.*,f.filename from {lcms_contents} lc '
        . 'join {lcms_contents_file} f on f.con_seq = lc.id '
        . 'where lc.id = :id';
$contents = $DB->get_record_sql($sql,array('id'=>$id));
?>  
<form method="post" id="form1" action="html_submit.php?conid=<?php echo $id; ?>&formid=<?php echo $formid; ?>" enctype="multipart/form-data">
    <input type="hidden" value="<?php echo $id; ?>" name="con_id">
    <input type="hidden" value="html" name="con_type">
    <input type="hidden" value="<?php echo $contents->data_dir; ?>" name="data_dir">
    <input type="hidden" value="<?php echo $contents->groupid; ?>" name="groupid">
    <!--<p class="files"><input id="fileupload" type="file" name="file_html" multiple><span>[<?php echo $contents->filename; ?>]</span></p>-->
    <p><h2 style="padding-bottom: 10px;"><?php echo get_string('edit_contents','lcms');?></h2></p> 
    <p><label for="contnm"><?php echo get_string('contentname','local_lmsdata'); ?><span class="red">*</span></label> <input type="text" id="contnm" value="<?php echo $contents->con_name; ?>" name="con_name"></p>
    <p><label for="share_yn"><?php echo get_string('shareornot','local_lmsdata'); ?><span class="red">*</span></label> 
        <select id="share_yn" name="share_yn">
            <option value="Y"><?php echo get_string('yes'); ?></option>
            <option value="N" <?php if($contents->share_yn == 'N'){ echo 'selected'; } ?>><?php echo get_string('no'); ?></option>
        </select>
    </p>
    <p><label for="strtfilenm"><?php echo get_string('startfilename','local_lmsdata'); ?><span class="red">*</span></label> <input id="strtfilenm" name="baseurl" type="text" value="<?php echo $contents->filename; ?>"></p>
    <p><label for="totalpage"><?php echo get_string('totalpagecount','local_lmsdata'); ?><span class="red">*</span></label> <input id="totalpage" type="text" name="totalpage" value="<?php echo $contents->con_total_time; ?>"></p>
    <p><label for="popupwidth"><?php echo get_string('edupagesize','local_lmsdata'); ?><span class="red">*</span> </label> <input id="popupwidth" type="text" name="popupwidth" value="<?php echo $contents->popupwidth; ?>">*<input type="text" name="popupheight" value="<?php echo $contents->popupheight; ?>" /></p>
    <input type="submit" class="edit button_style01 gray" value="<?php echo get_string('edit'); ?>">
</form>
<div id="loading" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1200; background-color:#757b8c; opacity:0.7; display:none;">
    <div style="position: absolute; top: 50%; left: 47%;"><img src="loading.gif"></div>
</div>
<script>
    $('#form1').submit(function (){ 
        if($('#contnm').val() == ''){
            alert('<?php echo get_string('inputcontentname','local_lmsdata'); ?>.');
            $('#contnm').focus();
            return false;
        }
        if($('#strtfilenm').val() == ''){
            alert('<?php echo get_string('inputstartfilename','local_lmsdata'); ?>.');
            $('#strtfilenm').focus();
            return false;
        }
        if($('#totalpage').val() == ''){
            alert('<?php echo get_string('inputtotalpage','local_lmsdata'); ?>.');
            $('#totalpage').focus();
            return false;
        }
        if($('#popupwidth').val() == ''){
            alert('<?php echo get_string('popupwidth','local_lmsdata'); ?>.');
            $('#popupwidth').focus();
            return false;
        }
        if($('#popupheight').val() == ''){
            alert('<?php echo get_string('popupheight','local_lmsdata'); ?>.');
            $('#popupheight').focus();
            return false;
        }
        $('#loading').show();
        return true;
    });
</script>

