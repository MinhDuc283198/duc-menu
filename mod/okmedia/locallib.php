<?php

function okmedia_get_js_module() {
    global $PAGE;

    return array(
        'name' => 'mod_okmedia',
        'fullpath' => '/mod/okmedia/module.js',
        'requires' => array('base'),
        'strings' => array(
            array('close', 'okmedia'),
        ),
    );
}

function okmedia_get_js_module_package2() {
    global $PAGE;

    return array(
        'name' => 'mod_okmedia_package2',
        'fullpath' => '/mod/okmedia/package2.js',
        'requires' => array('base'),
        'strings' => array(
            array('close', 'okmedia'),
        ),
    );
}
function okmedia_set_mainfile($lcms) {
    global $DB;
    $fs = get_file_storage();
    $cmid = $lcms->coursemodule;
    $draftitemid = $lcms->subfiles;
    $draftitem = file_get_drafarea_files($draftitemid);   

    //$context = context_module::instance($cmid);
    $context = context_system::instance();
    if($draftitem){
        file_save_draft_area_files($draftitemid, $context->id, 'local_repository', 'subtitle', $lcms->contents, array('subdirs'=>true));
    }
    $files = $fs->get_area_files($context->id, 'local_repository', 'subtitle', $lcms->contents, 'sortorder', false);
    
    if (count($files) == 1) {
        // only one file attached, set it as main file automatically
        $file = reset($files);
        file_set_sortorder($context->id, 'local_repository', 'subtitle', $lcms->contents, $file->get_filepath(), $file->get_filename(), 1);
    }
    
}

function okmedia_update_subtitle($lcms) {
    
    global $CFG, $USER;
    
    $fs = get_file_storage();
    $context = context_system::instance();
    $captions = $fs->get_area_files($context->id, 'local_repository', 'subtitle', $lcms->contents);
    $pattern1 = "/[_]{1}en[.]{0,1}srt\z$/";
    $pattern2 = "/[_]{1}ko[.]{0,1}srt\z$/";
    
    if (count($captions) > 0) {
        foreach($captions as $caption){            
            $captionname = $caption->get_filename();
            $captiontype = $caption->get_mimetype();
            $content = $caption->get_content();
            if(preg_match($pattern1, $captionname)){
                okmedia_update_subtitle_insert($lcms->id,$content,'en');
            }
            if(preg_match($pattern2, $captionname)){
                okmedia_update_subtitle_insert($lcms->id,$content,'ko');
            }
        }
    }
}

function okmedia_update_subtitle_insert($id,$content,$lang){
    
    global $DB, $USER;
    
    if ($content) {

        $content_br = preg_replace('/\r\n|\r|\n/','<br/>',$content); 
        $lines = explode('<br/><br/>',$content_br);
        

        foreach ($lines as $line) {
            
            $numb = 0;
            
            $srts = explode('<br/>',$line);
            $sub = new stdClass();
            $sub->lang = $lang;
            $sub->lcms_id = $id;
            $sub->userid = $USER->id;
            $sub->timecreated = time();
            $sub->caption = '';
            
            foreach($srts as $key=>$val){
                $numb++;
                if($key==1){ //시간값
                    list($sub->startTime, $sub->stopTime) = explode(' --> ', $val);
                    $starttime = substr($sub->startTime, 0,8);
                    $stoptime = substr($sub->stopTime, 0,8);
                    if(preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',$starttime) && preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/',$stoptime)){
                        $sub->stime = okmedia_seconds_from_time($starttime);
                        $sub->etime = okmedia_seconds_from_time($stoptime);
                    }else{
                        continue;
                    }            
                }
                if($key > 1 && trim($val)){
                    $sub->caption .= trim($val);
                    if($key < $numb) $sub->caption .= PHP_EOL;
                }
            }
            
            if($sub->stime >= 0 && $sub->etime >= 0 && $sub->caption){
                $subid = $DB->insert_record('lcms_caption',$sub);
            }
            
        }
    }
    
    return $subid;
}

function okmedia_can_update_progress($okmediaorid) {
    global $DB;
    
    if (is_object($okmediaorid)) {
        $okmedia = $okmediaorid;
    } else {
        $okmedia = $DB->get_record('okmedia', array('id' => $okmediaorid));
    }
    
    if($okmedia->progress != 1) {
        return false;
    }
    
    if($okmedia->islock == 2) {
        return true;
    }
    
    $timenow = time();
    if($okmedia->timestart <= $timenow
            && $okmedia->timeend >= $timenow) {
        return true;
    }
    
    return false;
}
function fcm_push($okmedia, $kinds) {
    global $USER, $DB, $CFG;
    require_once($CFG->dirroot.'/message/output/appnoti/message_output_appnoti.php');
    $pushclass = new message_output_appnoti();
    
    $courseid = $okmedia->course;
    $userfrom = $USER->id;
    $posthtml = '';

    $enrolestudent_select = ' select ra.id, ra.contextid, ra.userid, ctx.instanceid ';
    $enrolestudent_from = ' from {role_assignments} ra 
                            join {context} ctx on ctx.id = ra.contextid ';
    $enrolestudent_where = ' where ctx.instanceid=:courseid and ctx.contextlevel = 50 and ra.roleid = 5 ';
    $enrolestudents = $DB->get_records_sql($enrolestudent_select.$enrolestudent_from.$enrolestudent_where, array('courseid'=>$courseid));
    
    //보낼 내용 입력
    if($kinds == 'update') { 
        $posttext = $okmedia->name.' 동영상이 변경되었습니다.';
    } else { 
        $posttext = $okmedia->name.' 동영상이 삭제 되었습니다.';
    }
    $postsubject = '';
    foreach ($enrolestudents as $enrolestudent) {
        
        $user = $DB->get_record('user', array('id'=>$enrolestudent->userid));
        
        $eventdata = new object();
        $eventdata->component = 'moodle';    // the component sending the message. Along with name this must exist in the table message_providers
        $eventdata->name = 'instantmessage';        // type of message from that module (as module defines it). Along with component this must exist in the table message_providers
        $eventdata->userfrom = $userfrom;      // user object
        $eventdata->userto = $user;        // user object
        $eventdata->subject = '재능교육';   // very short one-line subject
        $eventdata->fullmessage = $posttext;      // raw text
        $eventdata->fullmessageformat = FORMAT_PLAIN;   // text format
        $eventdata->fullmessagehtml = $posthtml;      // html rendered version
        $eventdata->smallmessage = $postsubject;             // useful for plugins like sms or twitter
        $eventdata->notification = 1;
        $eventdata->link = $CFG->wwwroot.'/course/view.php?id='.$courseid;
        
        $usertokens = $DB->get_record('lmsdata_pushtoken', array('username'=>$user->username));
        if(!empty($usertokens)) {
            $pushclass->send_message($eventdata);
        }
    }
}