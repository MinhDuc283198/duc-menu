<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/mod/okmedia/lib.php';
$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->set_title(get_string('selectcontent','local_lmsdata'));
$PAGE->requires->jquery();
$PAGE->requires->css('/theme/jinui/style/popup.css');

$id = optional_param('id', 0, PARAM_INT);
$formid = optional_param('formid', '', PARAM_RAW);
$search = optional_param('search', '', PARAM_RAW);
$share = optional_param('share', '1', PARAM_RAW); //1: 나의 콘텐츠, 2: 공유콘텐츠
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$currentlang = strtoupper(current_language());
if ($currentlang == 'ZH_CN') {
    $currentlang = 'ZH';
}

//목록 가져오기
$sql_count = "select count(*) ";
$sql_query = "select lc.*, u.firstname ,u.lastname ";
$sql_from = "from {lcms_contents} lc "
        . "join {lmsdata_user} lu on lu.userid = lc.user_no "
        . "join {user} u on u.id = lu.userid ";
$conditions = array();
$query_param = array();
if ($share == 1) {
    //탭선택
    $shareon1 = 'on';
    //자신의 목록가져오기
    $conditions[] = 'lc.user_no = :userid';
    $query_param['userid'] = $USER->id;
} else {
    //탭선택
    $shareon2 = 'on';
    //과정에 공유된 콘텐츠 : 공유여부(isshared = 1)
    $conditions[] = 'lc.share_yn = :share';
    $conditions[] = 'lc.user_no != :userid';
    $query_param['userid'] = $USER->id;
    $query_param['share'] = 'Y';
}

if (!empty($search)) {
    $conditions2[] = $DB->sql_like('lc.con_name', ':search1', false);
    $query_param['search1'] = '%' . $search . '%';
    $conditions[] = '(' . implode(' or ', $conditions2) . ')';
}

$conditions[] = 'lc.con_type = :con_type';
$query_param['con_type'] = 'embed';

if ($conditions) {
    $where_clause = ' WHERE ' . implode(' and ', $conditions);
}

$sort_clause = ' ORDER BY lc.reg_dt DESC, lc.id DESC';
$totalcount = $DB->count_records_sql($sql_count . $sql_from . $where_clause, $query_param);
$contents = $DB->get_records_sql($sql_query . $sql_from . $where_clause . $sort_clause, $query_param, ($page - 1) * $perpage, $perpage);

$offset = 0;
if ($page != 0) {
    $offset = ($page - 1) * $perpage;
}


echo $OUTPUT->header();
?>  
<div id="pop_wrap" class="width1000">
    <div id="contents">
        <div class="tab_group full">
            <div class="tab"> 
                <div class="my <?php echo $shareon1; ?>" onclick="location.href = '?id=<?php echo $id; ?>&formid=<?php echo $formid; ?>&share=1&search=<?php echo $search; ?>';">
                    <h3><?php echo get_string('my_contents', 'okmedia'); ?></h3>
                </div>
                <div class="my <?php echo $shareon2; ?>" onclick="location.href = '?id=<?php echo $id; ?>&formid=<?php echo $formid; ?>&share=2&search=<?php echo $search; ?>';">
                    <h3><?php echo get_string('share_contents', 'okmedia'); ?></h3>
                </div>
            </div>
            <div class="right_cont">
                <form method="get">
                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                    <input type="hidden" value="<?php echo $formid ?>" name="formid">
                    <input type="hidden" value="<?php echo $share ?>" name="share">
                    <input type="text" class="search" value="<?php echo $search; ?>" name="search" placeholder="<?php echo get_string('search', 'okmedia'); ?>" title="<?php echo get_string('search', 'okmedia'); ?>" />
                    <input type="submit" class="ok" value="<?php echo get_string('search', 'okmedia'); ?>" title="<?php echo get_string('search', 'okmedia'); ?>" />
                </form>
            </div>
        </div>

         <div class="tab_con">
            <div class="sub_contents full tab course_con on">
                <div class="col2 listbox" style="overflow:auto; width:100% !important;">
                <?php
                if ($contents) {
                    foreach ($contents as $content) {
                        ?>
                        <div class="listbox normal">
                            <div class="col_list">
                                <div class="info">
                                    <h3 class="title"><a href="javascript:edit_html(<?php echo $content->id;?>);"><?php echo $content->con_name; ?></a></h3>
                                    <span class="date"><?php echo date('Y-m-d H:i', $content->reg_dt); ?></span>
                                    <sapn class="name"><?php echo fullname($content); ?></span>
                                </div>
                                <div class="buttons"> 
                                    <p class="ok" onclick="select_content('<?php echo $content->id; ?>', '<?php echo $content->con_name; ?>', '<?php echo $content->con_total_time; ?>')"><?php echo get_string('select', 'okmedia'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <div class="nolist">
                    <?php echo get_string('nofiles', 'okmedia'); ?>.
                    </div>
             <?php } ?>
                <!--paging-->
                <div class="paging">

                    <?php
                    if ($totalcount > 0) {
                        $page_params = array();
                        $page_params['formid'] = $formid;
                        $page_params['search'] = $search;
                        $page_params['share'] = $share;
                        okmedia_print_paging_navbar_script($totalcount, $page, $perpage, $CFG->wwwroot . '/mod/okmedia/htmls.php', $page_params);
                    }
                    ?>
                </div>
                <!--paging-->
                </div>             
                <div class="col3 mycontent" style="width:30%;border-left: 1px solid #ddd;"></div>
            </div>
        </div>
             
    </div>
</div>
<script>
    var category = 0;
    
//    add_html();
//   
//    function add_html() {
//        $.ajax({
//            url: "./html_upload.php?id=<?php echo $id; ?>&formid=<?php echo $formid; ?>&category="+category,
//            type: "post",
//            async: false,
//            success: function (data) {
//                $('.col3.mycontent').html(data);
//            },
//            error: function (e) {
//                console.log(e.responseText);
//            }
//        });
//
//    }
//    function edit_html(id) {
//        $.ajax({
//            url: "./html_edit.php?conid=" + id + "&formid=<?php echo $formid; ?>",
//            type: "post",
//            async: false,
//            success: function (data) {
//                $('.col3.mycontent').html(data);
//            },
//            error: function (e) {
//                console.log(e.responseText);
//            }
//        });
//
//    }
    function select_content(id, val, page) {

        var mform = opener.document.getElementById('<?php echo $formid; ?>');
        mform.titlestatic.value = val;
        mform.title.value = val;
        mform.contents.value = id;
        mform.type.value = 'embed';
        mform.totalpage.value = page;

        $(mform).find('#fitem_id_totalpage').show();
        $(mform).find('#fitem_id_popup').show();

        window.close();
    }
</script>
<?php echo $OUTPUT->footer(); ?>