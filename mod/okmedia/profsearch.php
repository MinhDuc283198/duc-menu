<?php
require_once '../../config.php';
require_once($CFG->dirroot . '/local/repository/lib.php');

$id = optional_param('id', 0, PARAM_INT); // courseid
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);

$PAGE->set_url('/mod/okmedia/profsearch.php');
$PAGE->set_pagelayout('embedded');

require_login();

echo $OUTPUT->header();

$profSql = 'SELECT 
                lu.userid, ur.username, ur.email, ur.lastname, ur.description ';
$profFrom = 'FROM {lmsdata_user} lu
             JOIN {user} ur ON ur.id = lu.userid ';


$profWhere = array();

$profWhere[] = ' lu.usergroup = :usergroup ';
$profParams['usergroup'] = 'pr';

if(!empty($search)) {
    $profWhere[] = '('.$DB->sql_like('username', ':search', false).' OR '.$DB->sql_like('lastname', ':search1', false).')';
    $profParams['search'] = '%' . $search . '%';
    $profParams['search1'] = '%' . $search . '%';
}

$profWhereSql = '';
$profWhereSql = ' WHERE '.implode(' AND ', $profWhere);

$profs = $DB->get_records_sql($profSql.$profFrom.$profWhereSql, $profParams, ($page - 1) * $perpage, $perpage);

$profsCntSql = 'SELECT COUNT(*) ';
$profsCnt = $DB->count_records_sql($profsCntSql.$profFrom.$profWhereSql, $profParams, ($page - 1) * $perpage, $perpage);

?>
<h3><?php echo get_string('findcontent', 'okmedia');?></h3>
<form class="table-search-option" method="get">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
    <input type="text" name="search" value="<?php echo $search; ?>" class="w-300" placeholder="<?php echo get_string('search', 'local_repository'); ?>">
    <input type="submit" class="board-search" value="<?php echo get_string('search', 'local_repository'); ?>" onclick='' />
</form> <!-- Search Area End -->
<table cellpadding="0" cellspacing="0" class="generaltable">
    <thead>
        <tr>
            <th width="6%"><?php echo get_string('list:no', 'okmedia'); ?></th>
            <th width="15%"><?php echo get_string('list:username', 'okmedia'); ?></th>
            <th><?php echo get_string('list:name', 'okmedia'); ?></th>
            <th width="10%"><?php echo get_string('list:email', 'okmedia'); ?></th>
            <th width="8%"><?php echo get_string('selectcontent', 'okmedia'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            $num = $profsCnt - ($page - 1) * $perpage;
            if(!empty($profs)) {
                foreach ($profs as $prof) {
        ?>
                <tr>
                    <td><?php echo $num--; ?></td>
                    <td><?php echo $prof->username; ?></td>
                    <td class="title"><?php echo $prof->lastname; ?></td>
                    <td><?php echo $prof->email; ?></td>
                    <td class="number">
                        <input type="button" style="background:#014099;color:#fff;" value="<?php echo get_string('selectcontent', 'okmedia'); ?>" onclick="select_prof('<?php echo $prof->lastname; ?>','<?php echo $prof->userid; ?>')">
                    </td>
                </tr>
            <?php
            }
        } else {
            echo '<tr><td colspan="5">' . get_string('list:empty', 'okmedia') . '</td></tr>';
        }
        ?>
    </tbody>
</table>
<div class="table-footer-area">
    <?php
    $page_params = array();
    $page_params['search'] = $search;
    $total_pages = repository_get_total_pages($profsCnt, $perpage);
    
    repository_get_paging_bar($CFG->wwwroot . "/mod/okmedia/contents.php", $page_params, $total_pages, $page);
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->

<script>
    function select_prof(profname, profid) {
        $('input[name=profid]', parent.document).val(profid);
        $('input[name=profnamestatic]', parent.document).val(profname);
        $('input[name=profname]', parent.document).val(profname);
        parent.M.mod_okmedia.close();
    }
</script>

<?php
echo $OUTPUT->footer();
?>