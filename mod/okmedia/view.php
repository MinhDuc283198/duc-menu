<?php
require_once '../../config.php';
require_once 'lib.php';
require_once 'locallib.php';

$id = required_param('id', PARAM_INT);          // Course Module ID
$c = optional_param('c', 0, PARAM_INT);        // okmedia ID
$inpopup = optional_param('inpopup', 0, PARAM_BOOL);
$mode = 'player';

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['c'] = $c;
}

$PAGE->set_url('/mod/okmedia/view.php', $params);

if (!$cm = get_coursemodule_from_id('okmedia', $id)) {
    print_error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
    print_error('course is misconfigured');
}
if (!$okmedia = $DB->get_record('okmedia', array('id' => $cm->instance))) {
    print_error('course module is incorrect');
}

require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
$PAGE->set_context($context);

// Initialize $PAGE.
$PAGE->set_url('/mod/okmedia/view.php', array('id' => $cm->id));
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);

// Print the page header.
echo $OUTPUT->header();

echo $OUTPUT->heading(format_string($okmedia->name), 2);

if ($okmedia->intro) {
    echo $OUTPUT->box(format_module_intro('okmedia', $okmedia, $cm->id), 'generalbox', 'intro');
}

$encrypted = base64_encode($id.'||'.$mode);
$fullurl = new moodle_url($CFG->lmsroot.'/mod/okmedia/link.php',array('v'=>$encrypted));

echo $OUTPUT->box_start('generalbox', 'okmediaviewer');
echo '<p>';
echo $OUTPUT->action_link($fullurl,
                          '동영상 학습하기',
                          new popup_action('click', $fullurl, '',
                                           array('height' => 1200, 'width' => 800)));
echo '</p>';
echo $OUTPUT->box_end();

echo $OUTPUT->footer();



