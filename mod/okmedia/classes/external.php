<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL external API
 *
 * @package    mod_okmedia
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php");

/**
 * URL external functions
 *
 * @package    mod_okmedia
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */
class mod_okmedia_external extends external_api {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function view_okmedia_parameters() {
        return new external_function_parameters(
            array(
                'lcmsid' => new external_value(PARAM_INT, 'okmedia instance id')
            )
        );
    }

    /**
     * Trigger the course module viewed event and update the module completion status.
     *
     * @param int $lcmsid the lcms instance id
     * @return array of warnings and status result
     * @since Moodle 3.0
     * @throws moodle_exception
     */
    public static function view_okmedia($lcmsid) {
        global $DB, $CFG;
        require_once($CFG->dirroot . "/mod/okmedia/lib.php");

        $params = self::validate_parameters(self::view_okmedia_parameters(),
                                            array(
                                                'lcmsid' => $lcmsid
                                            ));
        $warnings = array();

        // Request and permission validation.
        $lcms = $DB->get_record('okmedia', array('id' => $params['lcmsid']), '*', MUST_EXIST);
        list($course, $cm) = get_course_and_cm_from_instance($lcms, 'okmedia');

        $context = context_module::instance($cm->id);
        self::validate_context($context);

        require_capability('mod/okmedia:view', $context);

        // Call the okmedia/lib API.
        okmedia_view($lcms, $course, $cm, $context);

        $result = array();
        $result['status'] = true;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 3.0
     */
    public static function view_okmedia_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings()
            )
        );
    }



    /**
     * Describes the parameters for get_okmediaes_by_courses.
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function get_okmediaes_by_courses_parameters() {
        return new external_function_parameters (
            array(
                'courseids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'course id'), 'Array of course ids', VALUE_DEFAULT, array()
                ),
            )
        );
    }

    /**
     * Returns a list of okmediaes in a provided list of courses,
     * if no list is provided all okmediaes that the user can view will be returned.
     *
     * @param array $courseids the course ids
     * @return array the okmedia details
     * @since Moodle 3.0
     */
    public static function get_okmediaes_by_courses($courseids = array()) {
        global $CFG, $DB, $USER;

        $returnedlcmses = array();
        $warnings = array();

        $params = self::validate_parameters(self::get_okmediaes_by_courses_parameters(), array('courseids' => $courseids));

        if (empty($params['courseids'])) {
            $params['courseids'] = array_keys(enrol_get_my_courses());
        }

        // Ensure there are courseids to loop through.
        if (!empty($params['courseids'])) {

            list($courses, $warnings) = external_util::validate_courses($params['courseids']);

            // Get the okmediaes in this course, this function checks users visibility permissions.
            // We can avoid then additional validate_context calls.
            $lcmses = get_all_instances_in_courses("okmedia", $courses);

            foreach ($lcmses as $lcms) {
                $course = $courses[$lcms->course];
                $cm = get_coursemodule_from_instance('okmedia', $lcms->id, $course->id);
                $context = context_module::instance($cm->id);

                // Entry to return.
                $module = array();

                // First, we return information that any user can see in (or can deduce from) the web interface.
                $module['id'] = $lcms->id;
                $module['cmid'] = $cm->id;
                $module['course'] = $lcms->course;
                $module['name']  = external_format_string($lcms->name, $context->id);
                list($module['intro'], $module['introformat']) =
                    external_format_text($lcms->intro, $lcms->introformat, $context->id, 'mod_okmedia', 'intro', $lcms->id);
                $module['contents']       = $lcms->contents;
                $module['timecreated'] = $lcms->timecreated;
                $module['timemodified']    = $lcms->timemodified;
                $module['type']     = $lcms->type;
                $module['viewcount']   = $lcms->viewcount;
                $module['progress']    = $lcms->progress;
                $module['completionprogress']    = $lcms->completionprogress;
                $module['timestart']    = $lcms->timestart;
                $module['timeend']  = $lcms->timeend;
                $module['islock'] = $lcms->islock;
                $module['subtitleviewtype']     = $lcms->subtitleviewtype;
                
                $content = $DB->get_record('lcms_contents', array('id'=>$lcms->contents));
                $track = $DB->get_record('okmedia_track', array('okmediaid'=>$lcms->id, 'userid'=>$USER->id));
                
                $returnedlcmses[] = $module;
            }
        }

        $result = array();
        $result['lcmses'] = $returnedlcmses;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the get_lcmses_by_courses return value.
     *
     * @return external_single_structure
     * @since Moodle 3.0
     */
    public static function get_lcmses_by_courses_returns() {
        return new external_single_structure(
            array(
                'lcmses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'lcms id'),
                            'cmid' => new external_value(PARAM_INT, 'Course module id'),
                            'course' => new external_value(PARAM_INT, 'Course id'),
                            'name' => new external_value(PARAM_RAW, 'lcms name'),
                            'intro' => new external_value(PARAM_RAW, 'The lcms intro'),
                            'introformat' => new external_format_value('intro'),
                            'contents' => new external_value(PARAM_INT, 'Content id'),
                            'timecreated' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'timemodified' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'type' => new external_value(PARAM_ALPHA, '', VALUE_OPTIONAL),
                            'viewcount' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'progress' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'completionprogress' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'timestart' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'timeend' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'islock' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                            'subtitleviewtype' => new external_value(PARAM_INT, 0, VALUE_OPTIONAL),
                        ), 'lcms'
                    )
                ),
                'warnings' => new external_warnings(),
            )
        );
    }

}
