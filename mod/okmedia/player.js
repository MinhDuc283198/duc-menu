function device_check(){
    
    //디바이스별 판별
    var _ua = window.navigator.userAgent.toLowerCase();
    var device = '';
    var browser = {
        ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
        ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
        iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
        android : /webkit/.test( _ua )&&/android/.test( _ua ),
        msie : /msie/.test( _ua )
    };   
    
    if(browser.ipod || browser.iphone) {
        device = 'iphone';
    } else if(browser.ipad) {
        device = 'ipad';
    } else if(browser.android) {
        device = 'android';
    } else if(browser.msie){
        device = 'msie';
    } else {
        device = 'etc';
    }
    
    return _ua;
    
}


function playtime_update(posfrom,posto,posevent,duration,mode){
    var status = true;
    if(mode == 'player'){
    var device = device_check();
    
    $.ajax({
        url: 'playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: true,
        data: {
            act : 'update',
            positionto : posto,
            positionfrom : posfrom,
            positionevent : posevent,
            duration : duration,
            device : device,
            id : $('#okmediaid').val()
        },
        success: function(data) {
            if(data.status == 'success') {
                status = true;
                $('#id_lastview').text(data.last);
                $('#id_playtime').text(data.totaltime);
                $('#id_progress').text(data.progress);
            } else {
                status = false;
            }
        },
        error: function(e) {
            
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
            //playtime_update(id,posfrom,posto,duration,second);
            status = false;
        }
    });
    }
        
    return status;
}

function playtime_update_close(posfrom,posto,posevent,duration,mode){
    var status = false;
    if(mode == 'player'){
        var device = device_check();

        $.ajax({
            url: 'playtime_ajax.php',
            type: 'POST',
            dataType: 'json',
            async: true,
            data: {
                act : 'update',
                positionto : posto,
                positionfrom : posfrom,
                positionevent : posevent,
                duration : duration,
                device : device,
                id : $('#okmediaid').val()
            },
            success: function(data) {
                if(data.status == 'success') {
                    status = true;
                    window.parent.close();
                }
            },
            error: function(e) {

                //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                //playtime_update(id,posfrom,posto,duration,second);
                status = false;
            }
        });
    }else{
        status = true;
        window.parent.close();
    }   
    return status;
}

function playtime_get(mode){
     
    var positionfrom = 0;
    
    if(mode == 'player'){
        
    var device = device_check();
        
    $.ajax({
        url: 'playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            act : 'get',
            device : device, 
            id : $('#okmediaid').val()
        },
        success: function(data) {
            if(data.status == 'success') {
                
                $('#id_lastview').text(data.last);
                
                if(data.positionto>0){
                     //이어보기
                     positionfrom = data.positionto;
                }
                
            } else {
                //alert(data.message);
            }
        },
        error: function(e) {
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
        }
    });
    }
        
    return positionfrom;
    
}

function caption_scroll(position){
    
    var lang = $('#subtitleslang').val();
    if($('.script_list.'+lang).length > 0 && $('.script_list.'+lang+' div.subtext.sub'+position).length > 0){
        var scroll = $('.script_list.'+lang).scrollTop();
        var pos = $('.script_list.'+lang+' div.subtext.sub'+position).position().top - 200;
        var cur = scroll + pos;

        $('.script_list.'+lang+' div.subtext').css({'color':'#7d7d84','font-weight':'normal'});
        $('.script_list.'+lang+' div.subtext.sub'+position).css({'color':'#f34d78','font-weight':'600'});
        $('.script_list.'+lang).animate({scrollTop:cur}, 600);
    }
    
}

$(document).ready(function () {
    /**동영상 팝업 닫기**/
     $(parent.document).find("#popclose").click(function(){
         //창 닫기 전에 진도율 저장
         popClosePause();
     });
     /**동영상 팝업 닫기**/

});

function pop_url_player(url){
    window.open(url,'urlpop','width=700, height=600');
}

/*
window.addEventListener("beforeunload", function (e) {
    var cook = $.cookie('video_event_<?php echo $id;?>_<?php echo $USER->id;?>');
    $.cookie('video_event_<?php echo $id;?>_<?php echo $USER->id;?>',null);
    if(cook=='1' || cook=='4' || cook=='5'){
        var confirmationMessage = "<?php echo get_string('beforeunload','lcms');?>";
        (e || window.event).returnValue = confirmationMessage;     // Gecko and Trident
        return confirmationMessage;
    }// Gecko and WebKit
});
*/

function videoResize(target, ratio){ 
     
     if(target.length != 0){
         var width =  target.width() *ratio;
         target.height(width); 
         
         $(window).resize(function(){
            var width =  target.width() *ratio;
            target.height(width); 
        }); 
   }
 }