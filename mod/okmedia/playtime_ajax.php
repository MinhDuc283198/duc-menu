<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/local/repository/config.php';
require_once 'lib.php';
require_once $CFG->dirroot.'/mod/okmedia/locallib.php';

//require_once '../lcmsattend/locallib.php';
require_once '../lcmsprogress/locallib.php';

global $DB, $USER;

$returnvalue = new stdClass();

$act = required_param('act', PARAM_RAW);
$id = required_param('id', PARAM_INT);
$duration = optional_param('duration', 0, PARAM_INT);

$okmedia = $DB->get_record('okmedia',array('id'=>$id));
$track = $DB->get_record('okmedia_track',array('okmediaid'=>$id,'userid'=>$USER->id));

$canupdate = okmedia_can_update_progress($okmedia);

if ($act == 'get') {
    if($canupdate) {
        if($track->id){
            $track->attempts = $track->attempts+1;
            $track->timeview = time();
            $DB->update_record('okmedia_track',$track);
        }else{

            if($okmedia->type=='Flash'){
                $play = new stdClass();
                $play->userid = $USER->id;
                $play->okmediaid = $id;
                $play->positionto = 1;
                $play->positionfrom = 0;
                $play->timereg = time();

                $DB->insert_record('okmedia_playtime',$play);
            }

            $track = new stdClass();
            $track->okmediaid = $id;
            $track->userid = $USER->id;
            $track->timeview = time();
            $track->attempts = 1;
            $DB->insert_record('okmedia_track',$track);
        }
    }
    
    $returnvalue->progress = $track->progress;
    $returnvalue->positionto = $track->lasttime;
    $returnvalue->positionpage = $track->lastpage;
    
} else if($act == 'update'){
    
    if($canupdate && $USER->id){
        $playtime = new stdClass();
        $playtime->userid = $USER->id;
        $playtime->okmediaid = $id;
        $playtime->rtype = optional_param('rtype', '', PARAM_RAW);
        $playtime->positionto = ceil(optional_param('positionto', 0, PARAM_FLOAT));
        $playtime->positionfrom = optional_param('positionfrom', 0, PARAM_INT);
        $playtime->timereg = time();
        
        if($playtime->positionto>0&&$duration>1){
            
            if($okmedia->duration!=$duration){
                //$okmedia->duration = $duration;
                //$DB->update_record('lcms',$okmedia);
            }
            
            $query = 'select count(*) from {okmedia_playtime} 
                where userid=:userid and okmediaid=:okmediaid and positionpage=:positionpage 
                and positionfrom<=:positionfrom and positionto>=:positionto';
            $params = array('userid'=>$USER->id,'okmediaid'=>$id,
                'positionto'=>$playtime->positionto,'positionfrom'=>$playtime->positionfrom);
            $playcount = $DB->count_records_sql($query,$params);
            
            //플레이시간을 저장한다. 
            if($playcount==0) $DB->insert_record('okmedia_playtime',$playtime);
            
            if($okmedia->duration!=$duration){
                $okmedia->duration = $duration;
                $DB->update_record('okmedia',$okmedia);
            }
            
            if(!$track->id){
                $track = new stdClass();
                $track->okmediaid = $id;
                $track->userid = $USER->id;
                $track->attempts = 1;
            }
            $track->lasttime = $playtime->positionto;
            $track->lastpage = $playtime->positionpage;
            $track->playtime = okmedia_get_progress($id,$USER->id);
            $track->playpage = okmedia_get_progress_page($id,$USER->id);
            if($playtime->rtype==''){
                if($duration>1) $track->progress = round($track->playtime/$duration*100);
            }else{
                if($duration>1) $track->progress = round($track->playpage/$duration*100);
            }
            if($track->progress > 97){
                $track->progress =100;
            }
            $track->timeview = time();
            if($track->id){
                $DB->update_record('okmedia_track',$track);
            }else{
                $DB->insert_record('okmedia_track',$track);
            }
            
            //lcmsattend_update_attendance_score($okmedia->course, $USER->id);
            lcmsprogress_update_progress_score($okmedia->course, $USER->id);
            
            // course_module_completion 테이블에 이수 업데이트
            $course = $DB->get_record('course', array('id'=>$okmedia->course));
            $cm = get_coursemodule_from_instance('okmedia', $okmedia->id, $okmedia->course, false, MUST_EXIST);

            $completion = new completion_info($course);
            
            if($completion->is_enabled($cm)) {
                // Mark viewed if required
                $completion->set_module_viewed($cm);
        
                $msql = 'select completionprogress from {okmedia} where course = :course and id = :id';
                $newlcms = $DB->get_field_sql($msql, array('course'=>$okmedia->course, 'id'=>$okmedia->id));
                $complete = COMPLETION_INCOMPLETE;
                if($track->progress >= $newlcms->completionprogress) {
                    $complete = COMPLETION_COMPLETE;
                }
                $completion->update_state($cm, $complete, $track->userid);
            }
            
            $returnvalue->last = date('Y-m-d H:i:s',$track->timeview);
            $ptm = time_from_seconds($track->playtime);
            $returnvalue->totaltime = $ptm->h.':'.$ptm->m.':'.$ptm->s;
            $returnvalue->totalpage = $track->playpage;
            $returnvalue->progress = $track->progress.' %';
            
        }
            
    } else {
        $returnvalue->last = date('Y-m-d H:i:s',$track->timeview);
        $ptm = time_from_seconds($track->playtime);
        $returnvalue->totaltime = $ptm->h.':'.$ptm->m.':'.$ptm->s;
        $returnvalue->totalpage = $track->playpage;
        $returnvalue->progress = $track->progress.' %';
    }
    
}

$returnvalue->status = 'success';

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

