<?php

$string['pluginadministration'] = '演讲内容管理';
$string['pluginname'] = '讲座内容';

$string['modulename'] = '讲座内容';
$string['modulename_help'] = '<p><b>讲座内容</b></p>
<div class="indent">
<p>从文件存储库和教学活动提供内容的课堂上传的文件。如果视频的选择的内容，保存到显示视频自动连接到视频播放器计算你看过视频的百分比进度。 Kontencheueun加入了讲座在课堂上的所有视频“教学内容，进度检查”活动可以通过视频课程和活动，你可以检查所有的学习者的进度检查每个学生的进步演讲视频教授，谁在同一页面.</p>
</div>';
$string['modulename_link'] = 'mod/lcms/view';
$string['modulenameplural'] = '讲座内容';

$string['close'] = '关闭';
$string['name'] = '名';
$string['progress'] = '检查是否取得进展';
$string['lcmsintro'] = '介绍';
$string['contentheader'] = '内容';
$string['content'] = '内容';
$string['findcontent'] = '查找讲座文件存储';
$string['selectcontent'] = '选择';
$string['pleasewait'] = '它加载...';
$string['invalidlcmsid'] = '无效ID Xinics共享';
$string['noviewpermission'] = '您没有权限查看.';
$string['viewcontent'] = '学习';
$string['viewvideo'] = '观看视频';
$string['viewhighvideo'] = 'HD图片';
$string['viewlowvideo'] = '正常';

$string['addcontent'] = '注册制化学习内容';
$string['starttime'] = '开始时间';
$string['endtime'] = '期限';
$string['islock'] = '在截止日期之后更多';
$string['islock_n'] = '藏';
$string['islock_y'] = '查看（检查进度是不可能的）';
$string['islock_e'] = '查看（可能的进度检查）';

$string['islcmsprogress'] = '检验内容被列入进展确认.';
$string['firststudy'] = '首先学习';
$string['laststudy'] = '最后的研究日: {$a}';
$string['studyrate'] = '进度: {$a} %';
$string['studyyn'] = '无论是学习: {$a}';
$string['totalstudytime'] = '总学习时间: {$a}';
$string['reference'] = '参考';

$string['continueplease'] = '你想从后一视图学习?';
$string['beforeunload'] = '如果当您从该页面进行浏览，无需停止电影播放它不保存一些进展率.';
$string['gotocourselist'] = '返回课堂';
$string['gotoprogresslist'] = '检查进度';
$string['nostudytime'] = '没有一个学习周期.';
$string['waitloading'] = '请稍候。在加载.';

$string['insert_file'] = '文件选择';
$string['insert_file_search'] = '从文件库河汇入';
$string['insert_file_inrepositry'] = '注册文件';

$string['filechange'] = '更改文件';
$string['filechange_n'] = '无变化';
$string['filechange_y'] = '更改文件的存储';
$string['filechange_e'] = '在版本库创建一个新文件';

$string['subtitle'] = '标题';
$string['subtitleselect'] = '选择字幕';
$string['insert_subtitle'] = '添加字幕';
$string['insert_subtitle_n'] = '无标题';
$string['insert_subtitle_y'] = '字幕注册';
$string['subtitle_view_type'] = '查看拖车';
$string['subtitle_view_n'] = '藏';
$string['subtitle_view_y'] = '查看（不编辑）';
$string['subtitle_view_e'] = '查看（编辑）';
$string['edit'] = '汇编';
$string['notify'] = '声明';
$string['done'] = '完成';
$string['register'] = '完成注册';
$string['register_cancel'] = '取消登记';
$string['subtitle_edit_msg'] = '※ 修改字幕按“完成”后，它会更改字幕.';
$string['subtitle_empty'] = '没有登记的字幕.';
$string['subtitle_starttime_input'] = '请输入起始时间.';
$string['subtitle_endtime_input'] = '输入结束时间.';
$string['subtitle_text_input'] = '请输入您的字幕信息.';
$string['needlogin'] = '请登录后使用.';
$string['subtitle_startendtime_error'] = '开始时间等于或大于结束时间大。请重新设置.';

$string['timeset'] = '时间设置';

$string['insert_file'] = '内容';
$string['error:notselectfile'] = '请选择一个文件
.';

$string['accesssettings'] = '讲座内容访问设置';
$string['accesssettings_description'] = '助理被允许访问我讲课的内容.';
$string['allowtaaccess'] = '酷刑允许访问';

$string['completionprogress'] = '学生的进步将完成这率(%):';
$string['completionprogressgroup'] = '进度检查';
$string['error:completionprogress'] = '它必须是1和100之间.';
$string['captionpreview'] = "查看拖车预览";
$string['htmlprogress'] = '设置HTML内容';
$string['htmlprogress_help'] = '设置识别时间学习HTML内容.';

$string['isreview'] = '无论是评审活动';
$string['isreview_help'] = '通过检查的活动进行的审查不会出现在考勤.';

$string['studying'] = 'studying';
$string['complete'] = 'complete';

$string['close_text'] = 'Reflect progress rate and close window';
