M.mod_okmedia_pacakge2 = M.mod_okmedia_pacakge2 || {};

M.mod_okmedia_pacakge2.init = function(Y, cmid) {
    this.cmid = cmid;
};

M.mod_okmedia_pacakge2.start = function(chapter, clause, page, url) { //StudyStart
    var url = M.cfg.wwwroot + '/mod/okmedia/package2_ajax.php';
    var params = {
        'event': 1,
        'id': this.cmid,
        'page': page
    };
    
    Y.io(url, {
        method:'POST',
        data:  build_querystring(params),
        on : {
            success : function(id, response, arguments) {
                //console.log(response);
            },
            failure : function(id, response, arguments) {}
        },
        context:this
    });
};

M.mod_okmedia_pacakge2.finish = function(chapter, clause, page, url) { //StudyFinish
    var url = M.cfg.wwwroot + '/mod/okmedia/package2_ajax.php';
    var params = {
        'event': 2,
        'id': this.cmid,
        'page': page
    };
    
    Y.io(url, {
        method:'POST',
        data:  build_querystring(params),
        on : {
            success : function(id, response, arguments) {
                //var data = Y.JSON.parse(response.responseText);
                //console.log(data.status);
                //console.log(data.progress);
                //console.log(data.view);
                //console.log(data.total);
            },
            failure : function(id, response, arguments) {}
        },
        context:this
    });
};

M.mod_okmedia_pacakge2.move = function(dir,chapter, clause) { //StudyMove
};

M.mod_okmedia_pacakge2.goto = function(chapter, clause) { //StudyGoto
};

M.mod_okmedia_pacakge2.get_progress = function() { //GetProgress
    return "";
};

M.mod_okmedia_pacakge2.open_menu = function(menuid) { //OpenMenu
};

M.mod_okmedia_pacakge2.set_bookmark = function(no, name, info) { //BookmarkSet
};

M.mod_okmedia_pacakge2.get_bookmark = function(no) { //BookmarkGet
    return "";
};

M.mod_okmedia_pacakge2.delete_bookmark = function(no) { //BookmarkDel
};

M.mod_okmedia_pacakge2.set_value = function(key, value) { //ValueSet
};

M.mod_okmedia_pacakge2.get_bookmark = function(key) { //ValueGet
    return "";
};

M.mod_okmedia_pacakge2.set_memo = function(chapter, clause, topic, subtopic, open_yn, txt) { //MemoSet
};

M.mod_okmedia_pacakge2.get_memo = function(chapter, clause, topic, subtopic) { //MemoGet
    return "";
};

M.mod_okmedia_pacakge2.view_memo = function(chapter, clause) { //MemoView
};