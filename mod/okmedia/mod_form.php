<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot . '/mod/okmedia/lib.php');
require_once($CFG->dirroot . '/mod/okmedia/locallib.php');
require_once($CFG->dirroot . '/local/repository/lib.php');

$LCFG = new stdClass();
$LCFG->allowexthtml = array('zip', 'html');
$LCFG->allowextword = array('hwp','mp4', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf');
$LCFG->allowextvideo = array('mp4');
$LCFG->allowextref = array('hwp', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf', 'mp4', 'mp3', 'wmv');
$LCFG->notallowfile = array('sh','exe','js','php','sql','jsp','asp','cgi','php3','php4','php5','unknown');

class mod_okmedia_mod_form extends moodleform_mod {

    function definition() {
        global $CFG, $DB, $OUTPUT, $PAGE, $USER , $COURSE;
        
        $mform = & $this->_form;
        $PAGE->requires->jquery();
        $PAGE->requires->jquery_plugin('ui');
        $PAGE->requires->jquery_plugin('ui-css');
        $PAGE->requires->js_init_call('M.mod_okmedia.init_edit_form', array($CFG->wwwroot, $mform->_attributes['id']), false, okmedia_get_js_module());
        
        //-------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name', 'okmedia'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');

        $this->add_intro_editor(true, get_string('okmediaintro', 'okmedia'));

        $tplantitlestatic = $this->current->tplantitlestatic;
        //-------------------------------------------------------
        
        /* REPOSITORY  */

        $repositoryid = optional_param('repositoryid', 0, PARAM_INT);
        $update = optional_param('update', "", PARAM_RAW);
        $add = optional_param('add', "", PARAM_RAW);
         
        $context = context_system::instance();

         //값 없을때 처리 상황보고 지워야함 - 2018.10.12
//        $mform->addGroup(
//                    array(
//                        $mform->createElement('radio', 'insert_file_yn', '', get_string('insert_file_search', 'lcms'), 0)
//                    ), 
//                    'publicgroup', 
//                    get_string('insert_file', 'lcms'), 
//                    array(' '), 
//                    false
//                );
//        $mform->setDefault('insert_file_yn', 0);
        
//        // 직접 등록하는 로직 때문에 주석처리 2018.07.04 최현수
//        $mform->addGroup(array(
//            $mform->createElement('radio', 'insert_file_yn', '', get_string('insert_file_search', 'lcms'), 0),
//            $mform->createElement('radio', 'insert_file_yn', '', get_string('insert_file_inrepositry', 'lcms'), 1)
//                ), 'publicgroup', get_string('insert_file', 'lcms'), array(' '), false);
//        $mform->setDefault('insert_file_yn', 0);

        $mform->addElement('header', 'lcms_content_search', get_string('contentheader', 'okmedia'));
        $mform->setExpanded('lcms_content_search', true);

        $mform->addElement('static', 'titlestatic', '<font color="#ed5565">'
                .get_string('content', 'okmedia').'</font>',
                '<input size="58" readonly="readonly" name="titlestatic" type="text"/> '
                . '<input id="mod_lcms_btn_select" type="button" value="' . get_string('findcontent', 'okmedia') . '" onclick="javascript:M.mod_okmedia.show('.$COURSE->id.');">');
//                . '<input id="mod_lcms_btn_upload" style = "background:#014099;color:#fff;" type="button" value="' . get_string('insert_file_inrepositry', 'okmedia') . '" onclick="javascript:M.mod_okmedia.upload('.$COURSE->id.');">');
//        //M.mod_okmedia.show2 사용 필요성??? 모르겠음 주석처리 2018.07.04 최현수
//        $mform->addElement('static', 'titlestatic', '<font color="#ed5565">'
//                .get_string('content', 'lcms').'</font><img class="req" src="'.$CFG->wwwroot.'/theme/image.php/'.$PAGE->theme->name.'/core/1445922315/req" alt="필수 항목" title="필수 항목">',
//                '<input size="58" readonly="readonly" name="titlestatic" type="text"/> '
//                . '<input id="mod_lcms_btn_select" type="button" value="' . get_string('findcontent', 'lcms') . '" onclick="javascript:M.mod_okmedia.show('.$COURSE->id.');">'
//                . '<input id="mod_lcms_btn_select2" type="button" value="' . get_string('findcontent2', 'lcms') . '" onclick="javascript:M.mod_okmedia.show2('.$USER->id.');">');
//        $mform->addRule('titlestatic',null, 'required', null);

//        $mform->addElement('header', 'lcms_content_insert', get_string('contentheader', 'lcms'));
//        $mform->setExpanded('lcms_content_insert', true);
//        // 파일형식
//        if (!$update) {
//            $mform->addGroup(array(
//                $mform->createElement('radio', 'con_type', '', get_string('video', 'local_repository') . "&nbsp;&nbsp;", "video"),
//                $mform->createElement('radio', 'con_type', '', get_string('embed', 'local_repository') . "&nbsp;&nbsp;", "embed"),
//                    ), 'publicgroup', get_string('file_type', 'local_repository'), '', false);
//            $mform->setDefault('con_type', 'video');
//        } else {
//            $mform->addElement('hidden', 'con_type');
//            $mform->setType('con_type', PARAM_CLEAN);
//            $staygroup = array();
//            $options = array(0=>get_string('filechange_n', 'lcms'),1=>get_string('filechange_y', 'lcms'),2=>get_string('filechange_e', 'lcms'));
//            $staygroup[] = $mform->createElement('select','stay_file','',$options);
//            $staygroup[] = $mform->createElement('static', 'fileinfo', '');
//            $mform->addGroup($staygroup, 'staygroup', get_string('filechange', 'lcms'), '', false);
//        }
        
       // video
        $vodgroup = array();
        $path = 'lms/'.$USER->id.'u'.date('YmdAhis').'r'.  mt_rand(1, 99);
        $vodserver = $CFG->vodserver;
        $transcording_src = $CFG->vodserver
                . '?id=0'.'&path='.$path.''
                . '&userid='.$USER->id.''
                . '&returnpath='.$CFG->wwwroot.'/local/repository/return_file_data.php';
        $video_frame = '<iframe src="'.$transcording_src.'" width="100%" id="video_frame" height="306"></iframe>';
        $vodgroup[] = $mform->createElement('static', 'videobtn', '',$video_frame);
        $vodgroup[] = $mform->createElement('hidden', 'data_dir',$path);
        $vodgroup[] = $mform->createElement('hidden', 'video_file_id','',array('id'=>'video_file_id'));
        $vodgroup[] = $mform->createElement('text','filename','',array('id'=>'video_file_name','style'=>'display:none;','readonly'=>true));
        
        $mform->addGroup($vodgroup, 'videogroup', get_string('video', 'local_repository'), '', false);
        $mform->setType('filename', PARAM_RAW);
        $mform->setType('data_dir', PARAM_RAW);
        
        //embed
        $embed_seletc_options = array('youtube' => 'Youtube','vimeo'=> 'Vimeo');
        $mform->addGroup(array(
            $mform->createElement('select', 'emb_type', get_string('copy_none', 'local_repository'), $embed_seletc_options),
            $mform->createElement('text', 'emb_search', '', get_string('copy_none', 'local_repository')),
            $mform->createElement('static', 'search_btn', '', '<input type="button" id="emb_search_btn" value="' . get_string('search', 'local_repository') . '" class="red_form" onclick="M.mod_okmedia.search_embed_contents();">'),
            $mform->createElement('static', 'br', 'br', "<br><br>"),
            $mform->createElement('static', '', '', "URL : "),
            $mform->createElement('text', 'emb_code', '', get_string('author')),
                ), 'embedgroup', get_string('embed', 'local_repository'), ' ', false);
                
        if ($repositoryid != 0) {
            $mform->addElement('hidden', 'id');
            $mform->setType('id', PARAM_INT);
            $mform->setDefault('id', $repositoryid);
        }

        $mform->addElement('hidden', 'con_id');
        $mform->setType('con_id', PARAM_INT);

        $mform->addElement('hidden', 'ref');
        $mform->setType('ref', PARAM_INT);
        $mform->setDefault('ref', 0);

        $mform->addElement('hidden', 'mode');
        $mform->setType('mode', PARAM_CLEAN);
         if (!empty($update)) {
            $mform->setDefault('mode', 'edit');
        } else if(!empty($add)){
             $mform->setDefault('mode', 'write');
        }
      
        $group_options = array(0 => get_string('groupselect', 'local_repository'));

//        // 그룹 선택
//        $groups = $DB->get_records("lcms_repository_groups", array('userid' => $USER->id));
//
//        foreach ($groups as $group) {
//            $group_options[$group->id] = $group->name;
//        }
//
//        $mform->addGroup(array(
//            $mform->createElement('select', 'groupid', get_string('group', 'local_repository'), $group_options),
//            $mform->createElement('static', 'addnewGrooup', 'addnewGrooup', "<br><br>")
//                ), 'groupgroup', get_string('groupselect', 'local_repository'), ' ', false);
//        $mform->setDefault('groupid', 0);
        
//        //자막업로드
//        $mform->addElement('header', 'subtitlesection', get_string('subtitle', 'lcms'));
//        
//        //자막추가
//        $mform->setExpanded('subtitlesection');
//        $mform->addElement('filemanager', 'subfiles', get_string('subtitleselect', 'lcms'), null, self::attachment_options());
//        // 이 값을 이용하여 자막을 보여줄지 체크하는 코드가 없어서 주석처리 - 2018.10.10 최현
//        $options = array(0=>get_string('subtitle_view_n', 'lcms'),1=>get_string('subtitle_view_y', 'lcms'));
//        $mform->addElement('select','subtitleviewtype',get_string('subtitle_view_type', 'lcms'),$options);
//        $mform->setDefault('subtitleviewtype', 0);
        
        //포인트 선택 - 재능에 특화된 기능 - 시작
//        $options = array(0=>'0포인트',1=>'1포인트',2=>'2포인트',3=>'3포인트');
//        $mform->addElement('select','point',get_string('point:header', 'okmedia'),$options);
//        $mform->setDefault('point', 0);
        //포인트 선택 - 재능에 특화된 기능 - 종료
        
                
        
        $mform->addElement('header', 'lcms_content_search', '교안');
        $mform->setExpanded('lcms_content_search', true);

        $mform->addElement('static', 'titlestatic2', ''
                .'교안'.'',
                '<input size="58" readonly="readonly" name="tplantitlestatic" type="text" value="'.$tplantitlestatic.'"/> '
                . '<input id="mod_lcms_btn_select2" type="button" value="' . get_string('findcontent', 'okmedia') . '" onclick="javascript:M.mod_okmedia.show('.$COURSE->id.',4);">');
        $mform->addElement('hidden', 'contents_no');
        
        /* 기간설정 */
//        $mform->addElement('header', 'content', get_string('timeset', 'lcms'));
//        $mform->setExpanded('content', true);
//
//        $starttime = usertime(time());
//        $mform->addElement('date_time_selector', 'timestart', get_string('starttime', 'lcms'));
//        $mform->addElement('date_time_selector', 'timeend', get_string('endtime', 'lcms'));
//        $mform->setDefault('timeend', strtotime(date('Y-m-d 00:00:00',strtotime('+8 day'))));
//        $options = array(0=>get_string('islock_n', 'lcms'),1=>get_string('islock_y', 'lcms'),2=>get_string('islock_e', 'lcms'));
//        $mform->addElement('select','islock',get_string('islock', 'lcms'),$options);
//        $mform->setDefault('islock', 1);

        // $mform->addElement('advcheckbox', 'progress', get_string('progress', 'lcms'), get_string('islcmsprogress','lcms'), array('group' => 1), array(0, 1));
        //$mform->setDefault('progress', 1);
        //$contents = $
        // static인 것은 전송이 안되서 hidden으로 추가함
        $mform->addElement('hidden', 'contents');
        $mform->addElement('hidden', 'title');
        $mform->addElement('hidden', 'type');
        $mform->addElement('hidden', 'progress');
        $mform->addElement('hidden', 'islock',2);
        $mform->addElement('hidden', 'timestart');
        $mform->addElement('hidden', 'timeend');
        $mform->setDefault('progress', 1);
        $mform->addElement('hidden', 'groupid', 0);
        $mform->addElement('hidden', 'profid', 0);
        
        $mform->addElement('hidden', 'profname', '');
//        $mform->addElement('hidden', 'islock', 1);

        //-------------------------------------------------------
        $this->standard_coursemodule_elements();
        //-------------------------------------------------------
        $this->add_action_buttons();
        
        $mform->addElement('html','<script src="/local/repository/contents_upload/js/vendor/jquery.ui.widget.js"></script>');
        $mform->addElement('html','<script src="/local/repository/contents_upload/js/jquery.fileupload.js"></script>');
        $mform->addElement('html','<script src="/local/repository/contents_upload/js/bootstrap.min.js"></script>');
        $mform->addElement('html','<script src="/mod/okmedia/okmedia.js"></script>');
    }

    public function data_preprocessing(&$data) {
        global $DB;
        $data = (array) $data;
        parent::data_preprocessing($data);
        // Set filterid from filter.
        $contents = (!empty($data['contents']))?$data['contents']:0;
        $con = $DB->get_record('lcms_contents', array('id' => $contents));
        $data['title'] = $con->con_name;
        $data['emb_code'] = $con->embed_code;
        $courseinfo = $DB->get_record('lmsdata_class',array('courseid'=>$data['course']));
        $data['timestart'] = $courseinfo->learningstart;
        $data['timeend'] = $courseinfo->learningend;
        $data['completionprogressenabled']=
            !empty($data['completionprogress']) ? 1 : 0;
        if (empty($data['completionprogress'])) {
            $data['completionprogress'] = 100;
        }
        if ($this->current->instance and !$this->current->tobemigrated) {
            $draftitemid = file_get_submitted_draft_itemid('subfiles');
            $context = context_system::instance();
            file_prepare_draft_area($draftitemid, $context->id, 'local_repository', 'subtitle', $contents, array('subdirs'=>true));
            $data['subfiles'] = $draftitemid;
        }
        
        $cfiles = $DB->get_records('lcms_contents_file', array('con_seq' => $con->id));
        $cfilelist = '<ul class="cfiles">';
        foreach ($cfiles as $cf) {
            $filename = $cf->fileoname;
            $filesize = filesize($filepath);
            $cfilelist .= '<li>';
           if ($con->con_type == 'word' || $con->con_type == 'ref')
               $cfilelist .= '<button type="button" class="btn btn-delete delete" data-type="DELETE" data-url="' . $cf->id . '">del</button>';
           $cfilelist .= $filename . '</li>';
        }
        $cfilelist .= '</ul>';
        $data['fileinfo'] = $cfilelist;
    }

    public function set_data($data) {
        global $DB;
        if (!empty($data->contents)) {
            $sql = 'select * from {lcms_contents} con where con.id = :contentid';
            $lcms_content = $DB->get_record_sql($sql, array('contentid' => $data->contents));
            unset($lcms_content->id);
            foreach ($lcms_content as $key => $value) {
                $data->$key = $value;
            }
            if ($groupid = $DB->get_field('lcms_repository', 'groupid', array('lcmsid' => $data->contents))) {
                $data->groupid = $groupid;
            }
        }
        if (!empty($data->profid)) {
            $data->profname = $DB->get_field('user', 'lastname', array('id'=>$data->profid));
        }
        $this->data_preprocessing($data);
       
        parent::set_data($data);
    }

    public static function editor_options($context) { 
        global $PAGE, $CFG;
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'local_repository', 'files', 0)
        );
    }

    function add_completion_rules() {
        $mform =& $this->_form;
        
        $group=array();
        $group[] =& $mform->createElement('checkbox', 'completionprogressenabled', '', get_string('completionprogress','lcms'));
        $group[] =& $mform->createElement('text', 'completionprogress', '', array('size'=>3));
        $mform->setType('completionprogress',PARAM_INT);
        $mform->addGroup($group, 'completionprogressgroup', get_string('completionprogressgroup','lcms'), array(' '), false);
        $mform->disabledIf('completionprogress','completionprogressenabled','notchecked');
        
        return array('completionprogressgroup');
    }
    function completion_rule_enabled($data) {
        return (!empty($data['completionprogressenabled']) && $data['completionprogress']!=0);
    }
    
    function get_data() {
        $data = parent::get_data();
        if (!$data) {
            return false;
        }
        // Turn off completion settings if the checkboxes aren't ticked
        if (!empty($data->completionunlocked)) {
            $autocompletion = !empty($data->completion) && $data->completion==COMPLETION_TRACKING_AUTOMATIC;
            if (empty($data->completionprogressenabled) || !$autocompletion) {
                $data->completionprogress = 0;
            }
        }
        return $data;
    }
    
    function validation($data, $files) {
        global $LCFG;
        $errors = parent::validation($data, $files);
        $mode = $data['mode'];
        if ($data['insert_file_yn'] == 1) {
           if ($mode == 'write' || $mode == 'reply' || ($mode == 'edit' && $data['stay_file'] == 1) ) {
                if ( $data['con_type'] == 'ref' || $data['con_type'] =='video') {

                    switch ($data['con_type']) {
                        case 'video':
                            $extarr = $LCFG->allowextvideo;
                            $n = 0;
                            break;
                        case 'ref':
                            $extarr = $LCFG->allowextref;
                            $n = 0;
                            break;
                    }
                    $msg = lcms_temp_dir_allow_filecount($extarr, $n , $data['con_type']);

                    if ($msg != 1) {
                        $errors['attachmentsgroup'] = $msg;
                    }
                }

                if ($data['con_type'] == 'embed' && !$data['emb_code']) {
                    $errors['embedgroup'] = get_string('error:notembed', 'local_repository');
                }
                
            }
        } else {
            if(!$data['title']){
                $errors['titlestatic'] = get_string('error:notselectfile','lcms');
            }
        }
        
        if ($data['completion'] && $data['completionunlocked'] == 1) {
            if ($data['completionprogress'] < 1 || $data['completionprogress'] > 100) {
                $errors['completionprogressgroup'] = get_string('error:completionprogress','lcms');
            }
        }
        
        return $errors;
    }
        public static function attachment_options() {

        return array(
            'subdirs' => 0,
            'maxfiles' => 2,
            'accepted_types' => array('.srt'),
            'return_types' => FILE_INTERNAL
        );
    }

}

?>
