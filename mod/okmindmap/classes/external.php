<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL external API
 *
 * @package    mod_okmindmap
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php");

/**
 * URL external functions
 *
 * @package    mod_okmindmap
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */
class mod_okmindmap_external extends external_api {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function view_okmindmap_parameters() {
        return new external_function_parameters(
            array(
                'okmindmapid' => new external_value(PARAM_INT, 'okmindmap instance id')
            )
        );
    }

    /**
     * Trigger the course module viewed event and update the module completion status.
     *
     * @param int $okmindmapid the okmindmap instance id
     * @return array of warnings and status result
     * @since Moodle 3.0
     * @throws moodle_exception
     */
    public static function view_okmindmap($okmindmapid) {
        global $DB, $CFG;
        require_once($CFG->dirroot . "/mod/okmindmap/lib.php");

        $params = self::validate_parameters(self::view_okmindmap_parameters(),
                                            array(
                                                'okmindmapid' => $okmindmapid
                                            ));
        $warnings = array();

        // Request and permission validation.
        $okmindmap = $DB->get_record('okmindmap', array('id' => $params['okmindmapid']), '*', MUST_EXIST);
        list($course, $cm) = get_course_and_cm_from_instance($okmindmap, 'okmindmap');

        $context = context_module::instance($cm->id);
        self::validate_context($context);

        require_capability('mod/okmindmap:view', $context);

        // Call the okmindmap/lib API.
        okmindmap_view($okmindmap, $course, $cm, $context);

        $result = array();
        $result['status'] = true;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 3.0
     */
    public static function view_okmindmap_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings()
            )
        );
    }



    /**
     * Describes the parameters for get_okmindmaps_by_courses.
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function get_okmindmaps_by_courses_parameters() {
        return new external_function_parameters (
            array(
                'courseids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'course id'), 'Array of course ids', VALUE_DEFAULT, array()
                ),
            )
        );
    }

    /**
     * Returns a list of okmindmaps in a provided list of courses,
     * if no list is provided all okmindmaps that the user can view will be returned.
     *
     * @param array $courseids the course ids
     * @return array the okmindmap details
     * @since Moodle 3.0
     */
    public static function get_okmindmaps_by_courses($courseids = array()) {
        global $CFG;

        $returnedokmindmaps = array();
        $warnings = array();

        $params = self::validate_parameters(self::get_okmindmaps_by_courses_parameters(), array('courseids' => $courseids));

        if (empty($params['courseids'])) {
            $params['courseids'] = array_keys(enrol_get_my_courses());
        }

        // Ensure there are courseids to loop through.
        if (!empty($params['courseids'])) {

            list($courses, $warnings) = external_util::validate_courses($params['courseids']);

            // Get the okmindmaps in this course, this function checks users visibility permissions.
            // We can avoid then additional validate_context calls.
            $okmindmaps = get_all_instances_in_courses("okmindmap", $courses);

            foreach ($okmindmaps as $okmindmap) {
                $context = context_module::instance($okmindmap->coursemodule);

                // Entry to return.
                $module = array();

                // First, we return information that any user can see in (or can deduce from) the web interface.
                $module['id'] = $okmindmap->id;
                $module['cmid'] = $okmindmap->coursemodule;
                $module['course'] = $okmindmap->course;
                $module['name']  = external_format_string($okmindmap->name, $context->id);
                list($module['intro'], $module['introformat']) =
                    external_format_text($okmindmap->intro, $okmindmap->introformat, $context->id, 'mod_okmindmap', 'intro', $okmindmap->id);
				$module['mapkey'] = $okmindmap->mapkey;
				$module['password'] = $okmindmap->password;
				$module['width'] = $okmindmap->width;
				$module['height'] = $okmindmap->height;
				$module['timemodified'] = $okmindmap->timemodified;

                $returnedokmindmaps[] = $module;
            }
        }

        $result = array();
        $result['okmindmaps'] = $returnedokmindmaps;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the get_okmindmaps_by_courses return value.
     *
     * @return external_single_structure
     * @since Moodle 3.0
     */
    public static function get_okmindmaps_by_courses_returns() {
        return new external_single_structure(
            array(
                'okmindmaps' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'okmindmap id'),
                            'cmid' => new external_value(PARAM_INT, 'Course module id'),
                            'course' => new external_value(PARAM_INT, 'Course id'),
                            'name' => new external_value(PARAM_RAW, 'okmindmap name'),
                            'intro' => new external_value(PARAM_RAW, 'The okmindmap intro'),
                            'introformat' => new external_format_value('intro'),
                            'mapkey' => new external_value(PARAM_RAW, 'Map key'),
                            'password' => new external_value(PARAM_RAW, 'Password', VALUE_OPTIONAL),
                            'width' => new external_value(PARAM_INT, 'Width'),
							'height' => new external_value(PARAM_INT, 'Height'),
                            'timemodified' => new external_value(PARAM_INT, 'Time of last modification'),
                        ), 'okmindmap'
                    )
                ),
                'warnings' => new external_warnings(),
            )
        );
    }



	/**
     * Describes the parameters for get_okmindmap_by_id.
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function get_okmindmap_by_id_parameters() {
        return new external_function_parameters(
            array(
                'okmindmapid' => new external_value(PARAM_INT, 'okmindmap id')
            )
        );
    }

	public static function get_okmindmap_by_id($okmindmapid) {
		global $DB;

        $params = self::validate_parameters(self::get_okmindmap_by_id_parameters(),
                                            array(
                                                'okmindmapid' => $okmindmapid
                                            ));
        $warnings = array();

        $okmindmap = $DB->get_record('okmindmap', array('id' => $params['okmindmapid']), '*', MUST_EXIST);
        list($course, $cm) = get_course_and_cm_from_instance($okmindmap, 'okmindmap');

        $context = context_module::instance($cm->id);
        self::validate_context($context);

        require_capability('mod/okmindmap:view', $context);

        $result = array();
		$result['id']           = $okmindmap->id;
		$result['cmid']         = $cm->id;
		$result['course']       = $course->id;
		$result['name']         = $okmindmap->name;
		$result['intro']        = $okmindmap->intro;
		$result['introformat']  = $okmindmap->introformat;
		$result['mapkey']       = $okmindmap->mapkey;
		$result['password']     = $okmindmap->password;
		$result['width']        = $okmindmap->width;
		$result['height']       = $okmindmap->height;
		$result['timemodified'] = $okmindmap->timemodified;
        $result['warnings']     = $warnings;

        return $result;
    }

	public static function get_okmindmap_by_id_returns() {
        return new external_single_structure(
            array(
                    'id' => new external_value(PARAM_INT, 'okmindmap id'),
                    'cmid' => new external_value(PARAM_INT, 'Course module id'),
                    'course' => new external_value(PARAM_INT, 'Course id'),
                    'name' => new external_value(PARAM_RAW, 'okmindmap name'),
                    'intro' => new external_value(PARAM_RAW, 'The okmindmap intro'),
                    'introformat' => new external_format_value('intro'),
                    'mapkey' => new external_value(PARAM_RAW, 'Map key'),
                    'password' => new external_value(PARAM_RAW, 'Password', VALUE_OPTIONAL),
                    'width' => new external_value(PARAM_INT, 'Width'),
                    'height' => new external_value(PARAM_INT, 'Height'),
                    'timemodified' => new external_value(PARAM_INT, 'Time of last modification'),
                    'warnings' => new external_warnings()
            )
        );
    }
}
