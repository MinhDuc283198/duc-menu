<?php


defined('MOODLE_INTERNAL') || die();

function xmldb_okmindmap_upgrade($oldversion) {
    global $CFG, $DB;

	$dbman = $DB->get_manager(); // loads ddl manager and xmldb classes

	if ($oldversion < 2012040900) {
		$table = new xmldb_table('okmindmap');

		$field = new xmldb_field('time_start');
		$field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, false, '0', null);
        $dbman->drop_field($table, $field);

		$field = new xmldb_field('time_end');
		$field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, false, '0', null);
        $dbman->drop_field($table, $field);
	}

    return true;
}