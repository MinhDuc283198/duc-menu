<?php

defined('MOODLE_INTERNAL') || die();

global $DB;

$logs = array(
    array('module'=>'okmindmap', 'action'=>'view all', 'mtable'=>'okmindmap', 'field'=>'name'),
    array('module'=>'okmindmap', 'action'=>'view', 'mtable'=>'okmindmap', 'field'=>'name'),
    array('module'=>'okmindmap', 'action'=>'update', 'mtable'=>'okmindmap', 'field'=>'name'),
    array('module'=>'okmindmap', 'action'=>'add', 'mtable'=>'okmindmap', 'field'=>'name'),
);