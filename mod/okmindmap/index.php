<?php

require_once("../../config.php");
require_once("lib.php");

$id = required_param('id', PARAM_INT);   // course id
$url = new moodle_url('/mod/okmindmap/index.php', array('id'=>$id));

$PAGE->set_url($url);

if ($id) {
    if (! $course = $DB->get_record('course', array('id' => $id))) {
        print_error('invalidcourseid');
    }
} else {
    $course = get_site();
}

require_course_login($course);
$PAGE->set_pagelayout('incourse');
$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);

add_to_log($course->id, "okmindmap", "view all", "index.php?id=$course->id", "$course->fullname");


/// Get all required stringsokmindmap
$strokmindmaps = get_string("modulenameplural", "okmindmap");
$strokmindmap  = get_string("modulename", "okmindmap");


/// Print the header
$PAGE->navbar->add($strokmindmaps);
$PAGE->set_title("$course->shortname: $strokmindmaps");
$PAGE->set_heading($course->fullname);
$PAGE->set_button($searchform);
echo $OUTPUT->header();


/// Get all the appropriate data

if (! $okmindmaps = get_all_instances_in_course("okmindmap", $course)) {
    notice("There are no okmindmaps", "../../course/view.php?id=$course->id");
    die;
}

/// Print the list of instances (your module will probably extend this)
$table = new html_table();

$timenow = time();
$strname  = get_string("name");
$strweek  = get_string("week");
$strtopic  = get_string("topic");

if ($course->format == "weeks") {
    $table->head  = array ($strweek, $strname);
    $table->align = array ("center", "left");
} else if ($course->format == "topics") {
    $table->head  = array ($strtopic, $strname);
    $table->align = array ("center", "left");
} else {
    $table->head  = array ($strname);
    $table->align = array ("left");
}

foreach ($okmindmaps as $okmindmap) {
    if (!$okmindmap->visible) {
        //Show dimmed if the mod is hidden
        $link = "<a class=\"dimmed\" href=\"view.php?id=$okmindmap->coursemodule\">$okmindmap->name</a>";
    } else {
        //Show normal if the mod is visible
        $link = "<a href=\"view.php?id=$okmindmap->coursemodule\">$okmindmap->name</a>";
    }

    if ($course->format == "weeks" or $course->format == "topics") {
        $table->data[] = array ($okmindmap->section, $link);
    } else {
        $table->data[] = array ($link);
    }
}


echo html_writer::table($table);


/// Finish the page
echo $OUTPUT->footer();

?>
