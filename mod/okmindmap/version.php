<?php
defined('MOODLE_INTERNAL') || die();

$plugin->component = 'mod_okmindmap'; // Full name of the plugin (used for diagnostics).
$plugin->version  = 2016041902;    // The current module version (Date: YYYYMMDDXX).
$plugin->requires = 2015111000;    // Requires this Moodle version.
$plugin->cron     = 0;
