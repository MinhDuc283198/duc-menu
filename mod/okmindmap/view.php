<?php 	
	
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: ". gmdate("D, d M Y H:i:s",  time())." GMT");
    header("Pragma: no-cache"); 
    header("Last-Modified: ". gmdate("D, d M Y H:i:s",  time()) ." GMT"); 

    require_once("../../config.php");
    require_once("lib.php");


    $id = optional_param('id', 0, PARAM_INT); // Course Module ID, or
    $a  = optional_param('a', 0, PARAM_INT);  // okmindmap ID

    $params = array();
    if ($id) {
        $params['id'] = $id;
    } else {
        $params['a'] = $a;
    }
    $PAGE->set_url('/mod/okmindmap/view.php', $params);
    
    if ($id) {
        if (! $cm = get_coursemodule_from_id('okmindmap', $id)) {
            print_error('invalidcoursemodule');
        }
        if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
            error("Course is misconfigured");
        }
        if (! $okmindmap = $DB->get_record("okmindmap", array("id"=>$cm->instance))) {
            error("Course module is incorrect");
        }
    } else {
        if (! $okmindmap = $DB->get_record("okmindmap", array("id"=>$a))) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", array("id"=>$okmindmap->course))) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("okmindmap", $okmindmap->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }
    
    require_course_login($course, true, $cm);

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    $PAGE->set_context($context);
    
    // Mark viewed if required
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
            
/// Print header.

    $PAGE->set_title(format_string($okmindmap->name));
    $PAGE->set_heading(format_string($course->fullname));
    
    echo $OUTPUT->header();

/// Some capability checks.
    if (empty($cm->visible) and !has_capability('moodle/course:viewhiddenactivities', $context)) {
        notice(get_string("activityiscurrentlyhidden"));
    }

    if (!has_capability('mod/okmindmap:view', $context)) {
        notice(get_string('noviewpermission', 'okmindmap'));
    }

/// find out current groups mode
    groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/okmindmap/view.php?id=' . $cm->id);
    $currentgroup = groups_get_activity_group($cm);
    $groupmode = groups_get_activity_groupmode($cm);
    
    add_to_log($course->id, 'okmindmap', 'view', 'view.php?id='.$cm->id, "$okmindmap->name", $cm->id);


/// Print the main part of the page
	
	$timeformat = get_string('strftimedaydatetime');

        if (!empty($okmindmap->intro)) {
            echo $OUTPUT->box(format_module_intro('forum', $okmindmap, $cm->id), 'generalbox', 'intro');
        }

	echo '<center>
		<iframe style="border: solid 1px #ccc;" src="http://www.okmindmap.com/map/'.$okmindmap->mapkey.'?password='.$okmindmap->password.'&m=off" width="'.$okmindmap->width.'" height="'.$okmindmap->height.'" frameborder="no"></iframe>
		</center>';


/// Finish the page
    echo $OUTPUT->footer($course);
?>
