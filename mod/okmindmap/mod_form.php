<?php

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_okmindmap_mod_form extends moodleform_mod {

	function definition() {
		global $COURSE;
		$mform    =& $this->_form;

//-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

// name
        $mform->addElement('text', 'name', get_string('name'), array('size'=>'50'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');

// intro
        $this->add_intro_editor(true, get_string('okmindmapintro', 'okmindmap'));

// map key
        $mform->addElement('text', 'mapkey', get_string('mapkey','okmindmap'), array('size'=>'50'));
        $mform->setType('mapkey', PARAM_TEXT);
		$mform->addHelpButton('mapkey', 'mapkey', 'okmindmap');
        $mform->addRule('mapkey', get_string('required'), 'required');
        $mform->setDefault('mapkey', ' ');

// password
        $mform->addElement('password', 'password', get_string('password','okmindmap'), array('size'=>'20'));
        $mform->setType('password', PARAM_TEXT);
		$mform->addHelpButton('password', 'password', 'okmindmap');
        //$mform->addRule('password', get_string('required'), 'required');

// width
        $mform->addElement('text', 'width', get_string('width','okmindmap'), array('size'=>'10'));
        $mform->setType('width', PARAM_INT);
        $mform->addRule('width', get_string('required'), 'required');

// height
        $mform->addElement('text', 'height', get_string('height','okmindmap'), array('size'=>'10'));
        $mform->setType('height', PARAM_INT);
        $mform->addRule('height', get_string('required'), 'required');


//-------------------------------------------------------------------------------
        $this->standard_grading_coursemodule_elements();

        $this->standard_coursemodule_elements();
//-------------------------------------------------------------------------------
// buttons
        $this->add_action_buttons();
    }
}

?>
