<?php

class restore_okmindmap_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('okmindmap', '/activity/okmindmap');
        
        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    protected function process_okmindmap($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timemodified = $this->apply_date_offset($data->timemodified);

        $newitemid = $DB->insert_record('okmindmap', $data);
        $this->apply_activity_instance($newitemid);
    }
    
    protected function after_execute() {
        global $DB;

        // Add estream related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_okmindmap', 'intro', null);
    }
}

?>
