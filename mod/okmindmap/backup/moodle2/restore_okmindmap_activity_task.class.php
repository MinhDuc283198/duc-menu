<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/okmindmap/backup/moodle2/restore_okmindmap_stepslib.php');

class restore_okmindmap_activity_task extends restore_activity_task {
    
    protected function define_my_settings() {
        
    }
    
    protected function define_my_steps() {
        $this->add_step(new restore_okmindmap_activity_structure_step('okmindmap_structure', 'okmindmap.xml'));
    }
    
    
    static public function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('okmindmap', array('intro'), 'okmindmap');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();

        // List of okmindmaps in course
        $rules[] = new restore_decode_rule('OKMINDMAPINDEX', '/mod/okmindmap/index.php?id=$1', 'course');
        // okmindmap by cm->id and okmindmap->id
        $rules[] = new restore_decode_rule('OKMINDMAPVIEWID', '/mod/okmindmap/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('OKMINDMAPVIEWA',  '/mod/okmindmap/view.php?a=$1', 'okmindmap');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * okmindmap logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();

        $rules[] = new restore_log_rule('okmindmap', 'add', 'view.php?id={course_module}', '{okmindmap}');
        $rules[] = new restore_log_rule('okmindmap', 'update', 'view.php?id={course_module}', '{okmindmap}');
        $rules[] = new restore_log_rule('okmindmap', 'view', 'view.php?id={course_module}', '{okmindmap}');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();

        $rules[] = new restore_log_rule('okmindmap', 'view all', 'index.php?id={course}', null);

        return $rules;
    }
}

?>
