<?php

require_once($CFG->dirroot . '/mod/okmindmap/backup/moodle2/backup_okmindmap_stepslib.php'); // Because it exists (must)
require_once($CFG->dirroot . '/mod/okmindmap/backup/moodle2/backup_okmindmap_settingslib.php'); // Because it exists (optional)

class backup_okmindmap_activity_task extends backup_activity_task {
    
    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        //
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        $this->add_step(new backup_okmindmap_activity_structure_step('okmindmap structure', 'okmindmap.xml'));
    }
    
    /**
     * Code the transformations to perform in the activity in
     * order to get transportable (encoded) links
     */
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");
        
        $search="/(".$base."\/mod\/okmindmap\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@OKMINDMAPINDEX*$2@$', $content);
        
        $search="/(".$base."\/mod\/okmindmap\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@OKMINDMAPVIEWID*$2@$', $content);
        
        $search="/(".$base."\/mod\/okmindmap\/view.php\?a\=)([0-9]+)/";
        $content= preg_replace($search, '$@OKMINDMAPVIEWA*$2@$', $content);
        
        return $content;
    }
}
?>
