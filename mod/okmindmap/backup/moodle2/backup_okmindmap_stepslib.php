<?php

class backup_okmindmap_activity_structure_step extends backup_activity_structure_step {
    protected function define_structure() {
        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');
        
        // Define each element separated
        $okmindmap = new backup_nested_element('okmindmap', array('id'), array(
            'name', 'intro', 'introformat',
            'mapkey', 'password', 'width', 'height', 
            'timemodified'));
        
        // Build the tree
        
        
        // Define sources
        $okmindmap->set_source_table('okmindmap', array('id' => backup::VAR_ACTIVITYID));
        
        
        // Define id annotations
        
        
        // Define file annotations
        $okmindmap->annotate_files('mod_okmindmap', 'intro', null);
        
        // Return the root element (okmindmap), wrapped into standard activity structure
        return $this->prepare_activity_structure($okmindmap);
    }
}
?>
