<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/completionlib.php');


/**
 * List of features supported in IMS CP module
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, false if not, null if doesn't know
 */
function okmindmap_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:                  return true;
        case FEATURE_GROUPINGS:               return true;
        case FEATURE_GROUPMEMBERSONLY:        return true;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES:    return false;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_RATE:                    return false;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return true;

        default: return null;
    }
}

/**
 * List of view style log actions
 * @return array
 */
function okmindmap_get_view_actions() {
    return array('view', 'view all');
}

/**
 * List of update style log actions
 * @return array
 */
function okmindmap_get_post_actions() {
    return array('update', 'add');
}

/**
 * Given an object containing all the necessary data, 
 * (defined by the form in mod.html) this function 
 * will create a new instance and return the id number 
 * of the new instance.
 *
 * @param object $instance An object from the form in mod.html
 * @return int The id of the newly inserted okmindmap record
 **/
function okmindmap_add_instance($data, $mform) {
    global $CFG, $DB;
    
    $data->timemodified = time();
    $data->id = $DB->insert_record("okmindmap", $data);

    // we need to use context now, so we need to make sure all needed info is already in db
    //$okmindmap = $DB->get_record('okmindmap', array('id'=>$data->id), '*', MUST_EXIST);
    $modcontext = get_context_instance(CONTEXT_MODULE, $data->coursemodule);

    return $data->id;
}

/**
 * Given an object containing all the necessary data, 
 * (defined by the form in mod.html) this function 
 * will update an existing instance with new data.
 *
 * @param object $instance An object from the form in mod.html
 * @return boolean Success/Fail
 **/
function okmindmap_update_instance($okmindmap) {
    global $USER, $CFG, $DB;

    $okmindmap->timemodified = time();
    $okmindmap->id = $okmindmap->instance;

    $DB->update_record("okmindmap", $okmindmap);
    
    return true;
}

/**
 * Given an ID of an instance of this module, 
 * this function will permanently delete the instance 
 * and any data that depends on it. 
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 **/
function okmindmap_delete_instance($id) {
    global $DB;
    
    if (! $okmindmap = $DB->get_record("okmindmap", array("id"=>$id))) {
        return false;
    }
    if (!$cm = get_coursemodule_from_instance('okmindmap', $okmindmap->id)) {
        return false;
    }
    if (!$course = $DB->get_record('course', array('id'=>$cm->course))) {
        return false;
    }

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    // now get rid of all files
    $fs = get_file_storage();
    $fs->delete_area_files($context->id);

    $result = true;

    # Delete any dependent records here #
	//TODO

    if (! $DB->delete_records("okmindmap", array('id'=>$okmindmap->id))) {
        $result = false;
    }

    return $result;
}

/**
 * Return a small object with summary information about what a 
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return null
 * @todo Finish documenting this function
 **/
function okmindmap_user_outline($course, $user, $mod, $okmindmap) {
    return null;
}

/**
 * Print a detailed representation of what a user has done with 
 * a given particular instance of this module, for user activity reports.
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function okmindmap_user_complete($course, $user, $mod, $okmindmap) {
    return true;
}

/**
 * Given a course and a time, this module should find recent activity 
 * that has occurred in okmindmap activities and print it out. 
 * Return true if there was output, or false is there was none. 
 *
 * @uses $CFG
 * @return boolean
 * @todo Finish documenting this function
 **/
function okmindmap_print_recent_activity($course, $isteacher, $timestart) {
    global $CFG;

    return false;  //  True if anything was printed, otherwise false 
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such 
 * as sending out mail, toggling flags etc ... 
 *
 * @uses $CFG
 * @return boolean
 * @todo Finish documenting this function
 **/
function okmindmap_cron () {
    global $CFG;

    return true;
}

/**
 * Must return an array of grades for a given instance of this module, 
 * indexed by user.  It also returns a maximum allowed grade.
 * 
 * Example:
 *    $return->grades = array of grades;
 *    $return->maxgrade = maximum allowed grade;
 *
 *    return $return;
 *
 * @param int $okmindmapid ID of an instance of this module
 * @return mixed Null or object with an array of grades and with the maximum grade
 **/
function okmindmap_grades($okmindmapid) {
   return NULL;
}

/**
 * Must return an array of user records (all data) who are participants
 * for a given instance of okmindmap. Must include every user involved
 * in the instance, independient of his role (student, teacher, admin...)
 * See other modules as example.
 *
 * @param int $okmindmapid ID of an instance of this module
 * @return mixed boolean/array of students
 **/
function okmindmap_get_participants($okmindmapid) {
    return false;
}

/**
 * This function returns if a scale is being used by one okmindmap
 * it it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $okmindmapid ID of an instance of this module
 * @return mixed
 * @todo Finish documenting this function
 **/
function okmindmap_scale_used ($okmindmapid,$scaleid) {
    $return = false;

    //$rec = get_record("okmindmap","id","$okmindmapid","scale","-$scaleid");
    //
    //if (!empty($rec)  && !empty($scaleid)) {
    //    $return = true;
    //}
   
    return $return;
}


function okmindmap_view($okmindmap, $course, $cm, $context) {
	// Trigger course_module_viewed event.
    $params = array(
        'context' => $context,
        'objectid' => $okmindmap->id
    );

    $event = \mod_okmindmap\event\course_module_viewed::create($params);
    $event->add_record_snapshot('course_modules', $cm);
    $event->add_record_snapshot('course', $course);
    $event->add_record_snapshot('okmindmap', $okmindmap);
    $event->trigger();

	// Completion.
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}
