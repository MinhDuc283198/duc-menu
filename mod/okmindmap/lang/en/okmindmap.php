<?php

$string['modulename'] = 'OKMindmap';
$string['modulenameplural'] = 'OKMindmaps';
$string['pluginadministration'] = 'administration';
$string['pluginname'] = 'OKMindmap';

$string['okmindmapintro'] = 'OKMindmap introduction';
$string['mapkey'] = 'Map Key';
$string['mapkey_help'] = 'Map Key';
$string['password'] = 'Password';
$string['password_help'] = 'Password';
$string['width'] = 'Width';
$string['height'] = 'Height';

$string['start_time'] = 'Start Time';
$string['end_time'] = 'End Time';
$string['from_start_time'] = 'From $a';
$string['to_end_time'] = ' to $a';

$string['noviewpermission'] = 'You do not have the permission to view this map.';

?>
