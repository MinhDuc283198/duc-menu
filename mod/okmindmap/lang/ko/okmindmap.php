<?php

$string['modulename'] = 'OKMindmap';
$string['modulename_help'] = 'OKMindmap 활동은 okmindmap.com 에서 제공되는 협업 마인드맵 서비스를 활동으로 연계하는 활동입니다.
OKMindmap을 설정하는 과정에서 okmindmap.com 에서 이미 만들어 놓은 맵의 주로를 키 값으로 요구합니다.
또 맵에 비밀번호가 설정되어 있을 때는 이 비빌번호 값을 설정 단계에서 입력할 수 있습니다. 이렇게 생성된 OKMindap 활동은 클릭할 때 해당 OKMindmap을 열어서 연결해 줍니다.';
$string['modulenameplural'] = 'OKMindmaps';
$string['pluginadministration'] = 'administration';
$string['pluginname'] = 'OKMindmap';

$string['okmindmapintro'] = '소개';
$string['mapkey'] = '맵 키';
$string['mapkey_help'] = '맵 키는 맵 주소에서 /map/ 다음에 오는 값입니다.';
$string['password'] = '비밀번호';
$string['password_help'] = '맵을 비밀번호 공유를 한 경우 맵 비밀번호를 입력하세요.';
$string['width'] = '넓이';
$string['height'] = '높이';

$string['start_time'] = '시작 시간';
$string['end_time'] = '종료 시간';
$string['from_start_time'] = '$a 부터 ';
$string['to_end_time'] = '$a 까지';

$string['noviewpermission'] = '보기 권한이 없습니다.';

?>
