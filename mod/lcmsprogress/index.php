<?php
require_once('../../config.php');

require_once($CFG->dirroot.'/mod/lcmsprogress/locallib.php');
// For this type of page this is the course id.
$id = required_param('id', PARAM_INT);

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);
require_login($course);
$PAGE->set_url('/mod/lcmsprogress/index.php', array('id' => $id));
$PAGE->set_pagelayout('incourse');


// Print the header.
$strplural       = get_string("modulenameplural", "lcmsprogress");
$strprogressname = get_string('lcmsprogressname', 'lcmsprogress');
$strdescription  = get_string('description', 'lcmsprogress');

$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($strplural));

$context = context_course::instance($course->id);

require_capability('mod/lcmsprogress:view', $context);


$table = new html_table();
$table->head  = array ($strprogressname, $strdescription);
$table->align = array ('left', 'left');
$table->data = array();

$modinfo = get_fast_modinfo($course);

if (!isset($modinfo->instances['lcmsprogress'])) {
    $modinfo->instances['lcmsprogress'] = array();
}

foreach ($modinfo->instances['lcmsprogress'] as $jinotechboardid=>$cm) {
    if (!$cm->uservisible) {
        continue;
    }

    if (!$context = context_module::instance($cm->id)) {
        continue;
    }

	if($progress = $DB->get_record('lcmsprogress', array('id'=>$cm->instance))) {
		$name = '<a href="'.$CFG->wwwroot.'/mod/lcmsprogress/view.php?id='.$cm->id.'">'.$progress->name.'</a>';

		$row = array ($name, $progress->intro);
		$table->data[] = $row;
	}
}



echo html_writer::table($table);

echo $OUTPUT->footer();
