<?php
require_once('../../config.php');
require_once($CFG->libdir.'/accesslib.php');
require_once('locallib.php');

$id = required_param('id', PARAM_INT);          // Course Module ID
$c  = optional_param('c', 0, PARAM_INT);        // lcmsprogress ID
$userid  = optional_param('u', 0, PARAM_INT);        // User ID
$name = optional_param('name', '', PARAM_RAW);

$page = optional_param('page', -1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['c'] = $c;
}

$PAGE->set_url('/mod/lcmsprogress/view.php', $params);

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcmsprogress', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id'=> $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcmsprogress = $DB->get_record('lcmsprogress', array('id'=> $cm->instance))) {
        print_error('course module is incorrect');
    }
} else if ($c) {
    if (! $lcmsprogress = $DB->get_record("lcmsprogress", array("id" => $c))) {
        print_error('invalidlcmsid', 'lcmsprogress');
    }
    if (! $course = $DB->get_record("course", array("id" => $lcmsprogress->course))) {
        print_error('coursemisconf');
    }
    if (!$cm = get_coursemodule_from_instance("lcmsprogress", $lcmsprogress->id, $course->id)) {
        print_error('missingparameter');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);
$strlcmses = get_string("modulenameplural", "lcmsprogress");
$strlcms = get_string("modulename", "lcmsprogress");

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
if(has_capability('mod/lcmsprogress:viewall', $context)) {
    echo "<script>location.href='report.php?id=".$cm->id."'</script>";
}
$PAGE->set_context($context);

$output = $PAGE->get_renderer('mod_lcmsprogress');

$PAGE->set_title(format_string($lcmsprogress->name));
$PAGE->set_heading(format_string($lcmsprogress->name));
$PAGE->set_activity_record($lcmsprogress);

echo $output->header();

lcmsprogress_update_progress_score($course->id, $USER->id);

echo '<div class="tab-table-section">';
echo $output->view($lcmsprogress, $cm, $course, $context, $page, $perpage, $userid, $name);
echo '</div>';

echo $OUTPUT->footer($course);