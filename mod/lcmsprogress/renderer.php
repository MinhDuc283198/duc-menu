<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/lcmsprogress/locallib.php');
require_once($CFG->dirroot . '/mod/lcms/lib.php');

class mod_lcmsprogress_renderer extends plugin_renderer_base {

    function report($lcmsprogress, $cm, $course, $context, $page = -1, $perpage, $name = '', $prog = '') {
        global $CFG, $DB, $OUTPUT;

        $conditions = array();
        $params = array('lcmsprogress' => $lcmsprogress->id, 'courseid' => $course->id);

        if ($name) {

            $conditionname[] = $DB->sql_like('cu.firstname', ':firstname', false);
            $conditionname[] = $DB->sql_like('cu.lastname', ':lastname', false);
            $conditionname[] = $DB->sql_like('cu.username', ':username', false);
            $conditionname[] = $DB->sql_like($DB->sql_fullname('cu.firstname', 'cu.lastname'), ':fullname', false);
            $conditionname[] = $DB->sql_like($DB->sql_fullname('cu.lastname', 'cu.firstname'), ':fullname1', false);
            $conditionname[] = $DB->sql_like($DB->sql_concat('cu.firstname', 'cu.lastname'), ':fullname2', false);
            $conditionname[] = $DB->sql_like($DB->sql_concat('cu.lastname', 'cu.firstname'), ':fullname3', false);

            $conditions[] = '(' . implode(' OR ', $conditionname) . ')';

            $params['firstname'] = '%' . $name . '%';
            $params['lastname'] = '%' . $name . '%';
            $params['fullname'] = '%' . $name . '%';
            $params['fullname1'] = '%' . $name . '%';
            $params['fullname2'] = '%' . $name . '%';
            $params['fullname3'] = '%' . $name . '%';
            $params['username'] = '%' . $name . '%';
        }


        $sql_where = '';
        if (!empty($conditions)) {
            $sql_where = ' WHERE ' . implode(' and ', $conditions);
        }

        $sql_select = "SELECT cu.*, g.timemodified, g.progress, g.grade ";
        $sql_from = " FROM (SELECT DISTINCT u.id, u.username, u.email, u.lastname, u.firstname, e.courseid FROM {user} u 
                      JOIN {user_enrolments} ue ON ue.userid = u.id 
                      JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = :courseid)) cu
                JOIN {lcmsprogress} p ON (p.course = cu.courseid AND p.id = :lcmsprogress)
                LEFT JOIN {lcmsprogress_grades} g ON (g.lcmsprogress = p.id AND g.userid = cu.id) ";
        $sql_sort = "ORDER BY cu.lastname";

        echo '<div class="lcmsprogress_searchform"><form name="searchForm" method="GET" action="report.php">';
        echo '<input type="hidden" name="id" value="' . $cm->id . '"/>';
        echo '<input type="text" name="name" value="' . $name . '" style="margin-right:5px;width:200px;padding:5px;"/>';
        echo '<input type="submit" value="' . get_string('search') . '"/>';
        if(has_capability('mod/lcmsprogress:viewall', $context)) {
            echo '<input type="button" value="' . get_string('prevpage', 'lcmsprogress') . '" onclick="location.href=\'view.php?id='.$cm->id.'\';"/>';
        }
        echo '</form></div>';

        $table = new html_table();
        $table->attributes['class'] = 'generaltable';
        $table->attributes['cellspacing'] = '0';
        $table->head = array();
        $table->head[] = get_string('name');
        $lcmss = $DB->get_records('lcms', array('course' => $course->id), 'id asc', 'id,type');
//강의안들 모아서 배열에 들어갈 수 있게 처리
        $lcmscnt = 1;
        foreach ($lcmss as $lcms) {
            $table->head[] = get_string('lcmscontent', 'lcmsprogress') . $lcmscnt;
            $lcmscnt++;
        }
//$table->head[] = get_string('lastview', 'lcmsprogress');
        $table->head[] = get_string('progress', 'lcmsprogress');

        $grades = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_sort, $params, $page * $perpage, $perpage);
        foreach ($grades as $grade) {
            //        $lastview = '-';
            //        if ($grade->timemodified) {
            //            $lastview = userdate($grade->timemodified);
            //        }
            $progress = '0%';
            if ($grade->progress) {
                $progress = round($grade->progress) . '%';
            }
            $fullname = '<a href="view.php?id=' . $cm->id . '&u=' . $grade->id . '">' . fullname($grade) . '<br/>(' . $grade->username . ')</a>';
            $data_list = array();
            $data_list[] = $fullname;
            foreach ($lcmss as $lcms) {
                $progress2 = $DB->get_field('lcms_track', 'progress', array('userid' => $grade->id, 'lcms' => $lcms->id));
                $progress2 = ($progress2) ? $progress2 . '%' : '0%';
                if ($lcms->type == 'video' || $lcms->type == 'embed' || $lcms->type == 'html') {
                    $log_link_start = '<a href="#log" onclick="window.open(\'' . $CFG->wwwroot . '/mod/lcms/lcms_log.php?type=V&id=' . $lcms->id . '&u=' . $grade->id . '\',\'log\',\'width=500,height=600,scrollbars=yes\')" style="color:blue;">';
                    $log_link_end = '</a>';
                } else {
                    $log_link_start = '';
                    $log_link_end = '';
                }
                $data_list[] = $log_link_start . $progress2 . $log_link_end;
            }
//$data_list[] = $lastview;
            $data_list[] = $progress;
            $table->data[] = $data_list;
        }

        echo html_writer::table($table);
        $url = "report.php?id=$cm->id&name=$name&prog=$prog";
        if ($page != -1) {
            $numgrades = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
            echo $OUTPUT->paging_bar($numgrades, $page, $perpage, $url);
        }
        echo '<button onclick="location.href=\'' . $url . '&excell=1\'" class="red-form">다운로드</button>';
    }

    function view($lcmsprogress, $cm, $course, $context, $page = -1, $perpage, $userid = 0, $name = '') {
        global $CFG, $DB, $USER, $OUTPUT;

        if ($userid) {
            $user = $DB->get_record('user', array('id' => $userid));
            echo '<div class="lcmsprogress_userinfo">';
            echo get_string('name') . ': ' . fullname($user) . '<br/>';
            echo get_string('username') . ': ' . $user->username . '<br/>';
            echo get_string('email') . ': ' . $user->email;
            echo '</div>';
        } else {
            $userid = $USER->id;
        }

        $lcmss = lcmsprogress_get_lcms_modules($course->id, $page, $perpage, $name);

        $score = 0;
        if ($grade = $DB->get_record('lcmsprogress_grades', array('lcmsprogress' => $lcmsprogress->id, 'userid' => $userid))) {
            $score = number_format($grade->grade, 0);
        }
        echo '<div class="lcmsprogress_score">';
        echo get_string('progressscore', 'lcmsprogress', $score);
        echo '</div>';

        echo '<div class="lcmsprogress_searchform"><form name="searchForm" method="GET" action="view.php">';
        echo '<input type="hidden" name="id" value="' . $cm->id . '"/>';
        if ($userid != $USER->id)
            echo '<input type="hidden" name="u" value="' . $userid . '"/>';
        echo '<input type="text" name="name" value="' . $name . '" style="margin-right:5px;width:200px;padding:5px;"/>';
        echo '<input type="submit" value="' . get_string('search', 'lcmsprogress') . '"/>';
        if(has_capability('mod/lcmsprogress:viewall', $context)) {
            echo '<input type="button" value="' . get_string('viewreport', 'lcmsprogress') . '" onclick="location.href=\'report.php?id='.$cm->id.'\';"/>';
        }
        echo '</form></div>';


        $table = new html_table();
        $table->attributes['class'] = 'generaltable';
        $table->attributes['cellspacing'] = '0';

        $table->head = array(get_string('lcmscontentno', 'lcmsprogress'),
            get_string('lcmscontentname', 'lcmsprogress'),
            get_string('lcmscontent', 'lcmsprogress'),
            get_string('progressrate', 'lcmsprogress'),
            get_string('lastview', 'lcmsprogress'));
//$table->align = array('center', 'center', 'center', 'center');

        $yes = get_string('yesprogress', 'lcmsprogress');
        $no = get_string('noprogress', 'lcmsprogress');
        $numlcms = lcmsprogress_count_lcms_modules($course->id, $name);
        $totalcount = $numlcms;
        foreach ($lcmss as $lcms) {
            $progress = lcmsprogress_get_lcms_progress($lcms->id, $userid);
            $progressstr = "0%";
            if (!empty($progress->progress)) {
                $progressstr = number_format($progress->progress, 0) . "%";
            }

            $data_list = array();
            $data_list[] = $totalcount;
            $data_list[] = '<a href="' . $CFG->wwwroot . '/mod/lcms/view.php?id=' . $lcms->cmid . '">' . $lcms->name . '</a>';
            $data_list[] = '<a href="' . $CFG->wwwroot . '/mod/lcms/view.php?id=' . $lcms->cmid . '" class="gray_btn">' . get_string('lcmscontentview', 'lcmsprogress') . '</a>';
            if ($lcms->type == 'video' || $lcms->type == 'embed' || $lcms->type == 'html')
                $data_list[] = '<a href="#log" onclick="window.open(\'' . $CFG->wwwroot . '/mod/lcms/lcms_log.php?type=V&id=' . $lcms->id . '&u=' . $userid . '\',\'log\',\'width=500,height=600,scrollbars=yes\')"  style="color:blue;">' . $progressstr . '</a>';
            else
                $data_list[] = $progressstr;
            $data_list[] = $progress->timeview? date('Y-m-d H:i:s', $progress->timeview):'-';

            $table->data[] = $data_list;
            $totalcount--;
        }

        echo html_writer::table($table);

//weeks, topics, simple 형에 따라서 명칭 변경
        /*
          if(has_capability('moodle/course:manageactivities', $context)) {
          echo '<div class="btn-area btn-area-right">
          <select id="course_section"></select>
          <button class="red-form" onclick="location.href=;">'.get_string('lcmscontentreg','lcmsprogress').'</button></div>';
          }
         * */
        if ($page != -1) {
            echo $OUTPUT->paging_bar($numlcms, $page, $perpage, "view.php?id=$cm->id&u=$userid&name=$name");
        }
    }

    function excell_report($lcmsprogress, $cm, $course, $page = -1, $perpage, $name = '', $prog = '') {
        global $CFG, $DB, $OUTPUT , $COURSE;

        $conditions = array();
        $params = array('lcmsprogress' => $lcmsprogress->id, 'courseid' => $course->id);

        if ($name) {

            $conditionname[] = $DB->sql_like('cu.firstname', ':firstname', false);
            $conditionname[] = $DB->sql_like('cu.lastname', ':lastname', false);
            $conditionname[] = $DB->sql_like('cu.username', ':username', false);
            $conditionname[] = $DB->sql_like($DB->sql_fullname('cu.firstname', 'cu.lastname'), ':fullname', false);
            $conditionname[] = $DB->sql_like($DB->sql_fullname('cu.lastname', 'cu.firstname'), ':fullname1', false);
            $conditionname[] = $DB->sql_like($DB->sql_concat('cu.firstname', 'cu.lastname'), ':fullname2', false);
            $conditionname[] = $DB->sql_like($DB->sql_concat('cu.lastname', 'cu.firstname'), ':fullname3', false);

            $conditions[] = '(' . implode(' OR ', $conditionname) . ')';

            $params['firstname'] = '%' . $name . '%';
            $params['lastname'] = '%' . $name . '%';
            $params['fullname'] = '%' . $name . '%';
            $params['fullname1'] = '%' . $name . '%';
            $params['fullname2'] = '%' . $name . '%';
            $params['fullname3'] = '%' . $name . '%';
            $params['username'] = '%' . $name . '%';
        }


        $sql_where = '';
        if (!empty($conditions)) {
            $sql_where = ' WHERE ' . implode(' and ', $conditions);
        }

        $sql_select = "SELECT cu.*, g.timemodified, g.progress, g.grade ";
        $sql_from = " FROM (SELECT DISTINCT u.id, u.username, u.email, u.lastname, u.firstname, e.courseid FROM {user} u 
                      JOIN {user_enrolments} ue ON ue.userid = u.id 
                      JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = :courseid)) cu
                JOIN {lcmsprogress} p ON (p.course = cu.courseid AND p.id = :lcmsprogress)
                LEFT JOIN {lcmsprogress_grades} g ON (g.lcmsprogress = p.id AND g.userid = cu.id) ";
        $sql_sort = "ORDER BY cu.firstname";
        $fields = array(get_string('name'));

        $lcmss = $DB->get_records('lcms', array('course' => $course->id), 'id asc', 'id,type');
//강의안들 모아서 배열에 들어갈 수 있게 처리
        $lcmscnt = 1;
        foreach ($lcmss as $lcms) {
            $fields[] = get_string('lcmscontent', 'lcmsprogress') . $lcmscnt;
            $lcmscnt++;
        }
        $fields[] = get_string('progress', 'lcmsprogress');
        $filename = $COURSE->fullname.'_전자칠판.xls';
        $workbook = new MoodleExcelWorkbook('-');
        $workbook->send($filename);

        $worksheet = array();

        $worksheet[0] = $workbook->add_worksheet('');
        $col = 0;
        foreach ($fields as $fieldname) {
            $worksheet[0]->write(0, $col, $fieldname);
            $col++;
        }

        $row = 1;

        $grades = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_sort, $params);
        foreach ($grades as $grade) {
            $col = 0;
            $progress = ($grade->progress)?$progress = round($grade->progress) . '%':'0%';
            $fullname = fullname($grade).'(' . $grade->username . ')';
            $worksheet[0]->write($row, $col++, $fullname);
            foreach ($lcmss as $lcms) {
                $progress2 = $DB->get_field('lcms_track', 'progress', array('userid' => $grade->id, 'lcms' => $lcms->id));
                $progress2 = ($progress2) ? $progress2 . '%' : '0%';
                $worksheet[0]->write($row, $col++, $progress2);
            }
            $worksheet[0]->write($row, $col++, $progress);
            $row++;
        }
    $workbook->close();
    die;
    }

}

?>
