<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/lcmsprogress/locallib.php');


/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $lcmsprogress the data that came from the form.
 * @return mixed the id of the new instance on success,
 *          false or a string error message on failure.
 */
function lcmsprogress_add_instance($lcmsprogress) {
    global $DB;
    $cmid = $lcmsprogress->coursemodule;

    $lcmsprogress->timemodified = time();

    // Try to store it in the database.
    $lcmsprogress->id = $DB->insert_record('lcmsprogress', $lcmsprogress);
    
    // Update related grade item.
    lcmsprogress_grade_item_update($lcmsprogress);

    // Do the processing required after an add or an update.

    return $lcmsprogress->id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $lcmsprogress the data that came from the form.
 * @return mixed true on success, false or a string error message on failure.
 */
function lcmsprogress_update_instance($lcmsprogress, $mform) {
    global $CFG, $DB;

    // Get the current value, so we can see what changed.
//    $oldlcmsprogress = $DB->get_record('lcmsprogress', array('id' => $lcmsprogress->instance));

    // We need two values from the existing DB record that are not in the form,
    // in some of the function calls below.
//    $lcmsprogress->grade     = $oldlcmsprogress->grade;

    // Update the database.
    $lcmsprogress->id = $lcmsprogress->instance;
    $lcmsprogress->timemodified = time();
    
    $DB->update_record('lcmsprogress', $lcmsprogress);
    
    // Update related grade item.
    lcmsprogress_grade_item_update($lcmsprogress);


    return true;
}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id the id of the lcmsprogress to delete.
 * @return bool success or failure.
 */
function lcmsprogress_delete_instance($id) {
    global $DB;

    $lcmsprogress = $DB->get_record('lcmsprogress', array('id' => $id), '*', MUST_EXIST);

//    lcmsprogress_grade_item_delete($lcmsprogress);
    $DB->delete_records('lcmsprogress', array('id' => $lcmsprogress->id));
    
    lcmsprogress_grade_item_update($lcmsprogress);

    return true;
}

/**
 * Get the best current grade for a particular user in a lcmsprogress.
 *
 * @param object $lcmsprogress the lcmsprogress settings.
 * @param int $userid the id of the user.
 * @return float the user's current grade for this lcmsprogress, or null if this user does
 * not have a grade on this lcmsprogress.
 */
function lcmsprogress_get_best_grade($lcmsprogress, $userid) {
    global $DB;
    $grade = $DB->get_field('lcmsprogress_grades', 'grade',
            array('lcmsprogress' => $lcmsprogress->id, 'userid' => $userid));

    // Need to detect errors/no result, without catching 0 grades.
    if ($grade === false) {
        return null;
    }

    return $grade + 0; // Convert to number.
}

/**
 * Is this a graded lcmsprogress? If this method returns true, you can assume that
 * $lcmsprogress->grade and $lcmsprogress->sumgrades are non-zero (for example, if you want to
 * divide by them).
 *
 * @param object $lcmsprogress a row from the lcmsprogress table.
 * @return bool whether this is a graded lcmsprogress.
 */
function lcmsprogress_has_grades($lcmsprogress) {
    return $lcmsprogress->grade >= 0.000005;
}

/**
 * Return a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @param object $course
 * @param object $user
 * @param object $mod
 * @param object $lcmsprogress
 * @return object|null
 */
function lcmsprogress_user_outline($course, $user, $mod, $lcmsprogress) {
    global $DB, $CFG;
    
    require_once($CFG->libdir . '/gradelib.php');
    $grades = grade_get_grades($course->id, 'mod', 'lcmsprogress', $lcmsprogress->id, $user->id);

    if (empty($grades->items[0]->grades)) {
        return null;
    } else {
        $grade = reset($grades->items[0]->grades);
    }

    $result = new stdClass();
    $result->info = get_string('grade') . ': ' . $grade->str_long_grade;

    // Datesubmitted == time created. dategraded == time modified or time overridden
    // if grade was last modified by the user themselves use date graded. Otherwise use
    // date submitted.
    // TODO: move this copied & pasted code somewhere in the grades API. See MDL-26704.
    if ($grade->usermodified == $user->id || empty($grade->datesubmitted)) {
        $result->time = $grade->dategraded;
    } else {
        $result->time = $grade->datesubmitted;
    }

    return $result;
}


/**
 * Return grade for given user or all users.
 *
 * @param int $lcmsprogressid id of lcmsprogress
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none. These are raw grades. They should
 * be processed with lcmsprogress_format_grade for display.
 */
function lcmsprogress_get_user_grades($lcmsprogress, $userid = 0) {
    global $CFG, $DB;

    $params = array($lcmsprogress->id);
    $usertest = '';
    if ($userid) {
        $params[] = $userid;
        $usertest = 'AND u.id = ?';
    }
    return $DB->get_records_sql("
            SELECT
                u.id,
                u.id AS userid,
                qg.grade AS rawgrade,
                qg.timemodified AS dategraded
            FROM {user} u
            JOIN {lcmsprogress_grades} qg ON u.id = qg.userid

            WHERE qg.lcmsprogress = ?
            $usertest
            GROUP BY u.id, qg.grade, qg.timemodified", $params);
}

/**
 * Round a grade to to the correct number of decimal places, and format it for display.
 *
 * @param object $lcmsprogress The lcmsprogress table row, only $lcmsprogress->decimalpoints is used.
 * @param float $grade The grade to round.
 * @return float
 */
function lcmsprogress_format_grade($lcmsprogress, $grade) {
    
    if (is_null($grade)) {
        return get_string('notyetgraded', 'lcmsprogress');
    }
    return format_float($grade, 1);
}


/**
 * Update grades in central gradebook
 *
 * @category grade
 * @param object $lcmsprogress the lcmsprogress settings.
 * @param int $userid specific user only, 0 means all users.
 * @param bool $nullifnone If a single user is specified and $nullifnone is true a grade item with a null rawgrade will be inserted
 */
function lcmsprogress_update_grades($lcmsprogress, $userid = 0, $nullifnone = true) {
    global $CFG, $DB;
    
    require_once($CFG->libdir . '/gradelib.php');

    if ($lcmsprogress->grade == 0) {
        lcmsprogress_grade_item_update($lcmsprogress);

    } else if ($grades = lcmsprogress_get_user_grades($lcmsprogress, $userid)) {
        lcmsprogress_grade_item_update($lcmsprogress, $grades);
    } else if ($userid && $nullifnone) {
        $grade = new stdClass();
        $grade->userid = $userid;
        $grade->rawgrade = null;
        lcmsprogress_grade_item_update($lcmsprogress, $grade);

    } else {
        lcmsprogress_grade_item_update($lcmsprogress);
    }
}

/**
 * Update all grades in gradebook.
 */
function lcmsprogress_upgrade_grades() {
    global $DB;

    $sql = "SELECT COUNT('x')
              FROM {lcmsprogress} a, {course_modules} cm, {modules} m
             WHERE m.name='lcmsprogress' AND m.id=cm.module AND cm.instance=a.id";
    $count = $DB->count_records_sql($sql);

    $sql = "SELECT a.*, cm.idnumber AS cmidnumber, a.course AS courseid
              FROM {lcmsprogress} a, {course_modules} cm, {modules} m
             WHERE m.name='lcmsprogress' AND m.id=cm.module AND cm.instance=a.id";
    $rs = $DB->get_recordset_sql($sql);
    if ($rs->valid()) {
        $pbar = new progress_bar('lcmsprogressupgradegrades', 500, true);
        $i=0;
        foreach ($rs as $lcmsprogress) {
            $i++;
            upgrade_set_timeout(60*5); // Set up timeout, may also abort execution.
            lcmsprogress_update_grades($lcmsprogress, 0, false);
            $pbar->update($i, $count, "Updating LCMS Attendance grades ($i/$count).");
        }
    }
    $rs->close();
}

/**
 * Create or update the grade item for given lcmsprogress
 *
 * @category grade
 * @param object $lcmsprogress object with extra cmidnumber
 * @param mixed $grades optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok, error code otherwise
 */
function lcmsprogress_grade_item_update($lcmsprogress, $grades = null) {
    global $CFG, $OUTPUT;
    require_once($CFG->dirroot . '/mod/lcmsprogress/locallib.php');
    require_once($CFG->libdir . '/gradelib.php');

    if (array_key_exists('cmidnumber', $lcmsprogress)) { // May not be always present.
        $params = array('itemname' => $lcmsprogress->name, 'idnumber' => $lcmsprogress->cmidnumber);
    } else {
        $params = array('itemname' => $lcmsprogress->name);
    }

    if ($lcmsprogress->grade > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax']  = $lcmsprogress->grade;
        $params['grademin']  = 0;

    } else {
        $params['gradetype'] = GRADE_TYPE_NONE;
    }

    if ($grades  === 'reset') {
        $params['reset'] = true;
        $grades = null;
    }

    return grade_update('mod/lcmsprogress', $lcmsprogress->course, 'mod', 'lcmsprogress', $lcmsprogress->id, 0, $grades, $params);
}

/**
 * Delete grade item for given lcmsprogress
 *
 * @category grade
 * @param object $lcmsprogress object
 * @return object lcmsprogress
 */
function lcmsprogress_grade_item_delete($lcmsprogress) {
    global $CFG;
    
    require_once($CFG->libdir . '/gradelib.php');

    return grade_update('mod/lcmsprogress', $lcmsprogress->course, 'mod', 'lcmsprogress', $lcmsprogress->id, 0,
            null, array('deleted' => 1));
}



/**
 * Removes all grades from gradebook
 *
 * @param int $courseid
 * @param string optional type
 */
function lcmsprogress_reset_gradebook($courseid, $type='') {
    global $CFG, $DB;

    $lcmsprogresszes = $DB->get_records_sql("
            SELECT q.*, cm.idnumber as cmidnumber, q.course as courseid
            FROM {modules} m
            JOIN {course_modules} cm ON m.id = cm.module
            JOIN {lcmsprogress} q ON cm.instance = q.id
            WHERE m.name = 'lcmsprogress' AND cm.course = ?", array($courseid));

    foreach ($lcmsprogresszes as $lcmsprogress) {
        lcmsprogress_grade_item_update($lcmsprogress, 'reset');
    }
}

function lcmsprogress_user_complete($course, $user, $mod, $progress) {
    global $CFG, $DB;
    
    if($grade = $DB->get_record('lcmsprogress_grades', array('lcmsprogress'=>$progress->id, 'userid'=>$user->id))) {
        echo "$grade->progress";
    } else {
        print_string('neverseen', 'lcmsprogress');
    }
}

function lcmsprogress_get_completion_state ($course,$cm,$userid,$type) {
    global $CFG, $DB;

    if (!($progress=$DB->get_record('lcmsprogress',array('id'=>$cm->instance)))) {
        throw new Exception("Can't find LCMSProgress {$cm->instance}");
    }

    $result=$type; // Default return value

    if ($progress->completionprogress) {
        if( $grade = $DB->get_record('lcmsprogress_grades', array('lcmsprogress'=>$progress->id, 'userid'=>$userid)) ) {
            
            $value = $grade->progress >= $progress->completionprogress;
            if ($type == COMPLETION_AND) {
                $result = $result && $value;
            } else {
                $result = $result || $value;
            }
        }
    }

    return $result;
}

/**
 * @param string $feature FEATURE_xx constant for requested feature
 * @return bool True if lcmsprogress supports feature
 */
function lcmsprogress_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:                  return false;
        case FEATURE_GROUPINGS:               return false;
        case FEATURE_GROUPMEMBERSONLY:        return false;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES:    return true;
        case FEATURE_GRADE_HAS_GRADE:         return true;
        case FEATURE_GRADE_OUTCOMES:          return true;
        case FEATURE_ADVANCED_GRADING:        return true;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return false;
        case FEATURE_PLAGIARISM:              return false;

        default: return null;
    }
}



function lcmsprogress_mod_updated($eventdata) {
    global $CFG, $DB;
    
    if($eventdata->modulename == 'lcms' || $eventdata->modulename == 'okmedia' || $eventdata->modulename == 'lcmsprogress') {
        $sql = "SELECT DISTINCT u.id, u.username, u.email, u.lastname, u.firstname,
                                e.courseid
                FROM {user} u 
                JOIN {user_enrolments} ue ON ue.userid = u.id 
                JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = :courseid)";

        $courseid = $eventdata->courseid;

        $users = $DB->get_records_sql($sql, array('courseid'=>$courseid));
        foreach($users as $user) {
            lcmsprogress_update_progress_score($courseid, $user->id);
        }
    }
}