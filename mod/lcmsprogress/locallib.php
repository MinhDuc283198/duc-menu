<?php
require_once($CFG->libdir . '/datalib.php');
require_once($CFG->libdir . '/completionlib.php');

function lcmsprogress_update_progress_score($courseid, $userid) {
    global $CFG, $DB;
    
    $countlcms = lcmsprogress_count_lcms_modules($courseid);
    require_once $CFG->dirroot.'/mod/lcmsprogress/lib.php';
    
    if($countlcms > 0) {
        $progress = lcmsprogress_get_lcms_user_progress($courseid, $userid);
        $progress = $progress / $countlcms;
        $lcmsprogresss = $DB->get_records('lcmsprogress', array('course'=>$courseid));
        
        foreach ($lcmsprogresss as $lcmsprogress) {            
            if($grade = $DB->get_record('lcmsprogress_grades', array('lcmsprogress'=>$lcmsprogress->id, 'userid'=>$userid))) {
                $grade->grade = $progress / 100 * $lcmsprogress->grade;
                $grade->progress = $progress;
                $grade->timemodified = time();
                $DB->update_record('lcmsprogress_grades', $grade);
            } else {
                $grade = new stdClass();
                $grade->lcmsprogress = $lcmsprogress->id;
                $grade->userid = $userid;
                $grade->progress = $progress;
                $grade->grade = $progress / 100 * $lcmsprogress->grade;
                $grade->timecreated = time();
                $grade->timemodified = time();
                $DB->insert_record('lcmsprogress_grades', $grade);
            }
            
            lcmsprogress_update_grades($lcmsprogress, $userid);
            
            if ($lcmsprogress->completionprogress) {
                $course = $DB->get_record('course', array('id'=>$lcmsprogress->course));
                $cm = get_coursemodule_from_instance('lcmsprogress', $lcmsprogress->id, $lcmsprogress->course, false, MUST_EXIST);

                $completion = new completion_info($course);

                if ($progress >= $lcmsprogress->completionprogress) {
                    $completion->update_state($cm, COMPLETION_COMPLETE, $userid);
                } else {
                    $completion->update_state($cm, COMPLETION_INCOMPLETE, $userid);
                }
            }
        }
    }
        
    return true;
}

function lcmsprogress_get_lcms_user_progress($courseid, $userid) {
    global $CFG, $DB;
    
    $sql = "SELECT sum(t.progress) AS progress
                FROM {lcms} a 
                JOIN {course_modules} cm ON cm.instance = a.id 
                JOIN {lcms_track} t ON t.lcms = cm.id
                WHERE a.course = :courseid AND a.progress = 1 
                  AND t.userid = :userid";
    
    $lcmsprogress = $DB->get_record_sql($sql, array('courseid'=>$courseid, 'userid'=>$userid));
    
    if(empty($lcmsprogress)){
        $lcmsprogress = new stdClass();
        $lcmsprogress->progress = 0;
    }
    
    $sql = "SELECT sum(t.progress) AS progress
                FROM {okmedia} a 
                JOIN {course_modules} cm ON cm.instance = a.id
                JOIN {okmedia_track} t ON t.okmediaid = cm.id
                WHERE a.course = :courseid AND a.progress = 1 
                  AND t.userid = :userid"; 
    
    $okmediaprogress = $DB->get_record_sql($sql, array('courseid'=>$courseid, 'userid'=>$userid));
    
    if(empty($okmediaprogress)){
        $okmediaprogress = new stdClass();
        $okmediaprogress->progress = 0;
    }
    
    $progress = $lcmsprogress->progress + $okmediaprogress->progress;
    
    return $progress;
}

function lcmsprogress_count_lcms_modules($courseid,$name='') {
    global $CFG, $DB;
    
    $conditions = array('l.course = :courseid','l.progress = 1');
    $params = array('courseid'=>$courseid);
    
    if($name){ 
        $conditions[] = $DB->sql_like('l.name', ':name');
        $params['name'] = '%'.$name.'%';
    }
    
    $where = '';
    if(!empty($conditions)) {
        $where = ' WHERE '.implode(' and ', $conditions);
    }

    $sql = "SELECT count(*)
                FROM {lcms} l 
                ".$where;
    
    $lcmscount = $DB->count_records_sql($sql, $params);
    
    $sql = "SELECT count(*)
                FROM {okmedia} l 
                ".$where;
    
    $okmediacount = $DB->count_records_sql($sql, $params);
    
    $total = $lcmscount+$okmediacount;
    
    return $total;
}


function lcmsprogress_get_lcms_modules($courseid, $page=-1, $perpage, $name='') {
    global $CFG, $DB;

//    $lcmsmodules = array();
//    $modinfo = get_fast_modinfo($courseid);
//    foreach ($modinfo->instances['lcms'] as $lcmsid=>$cm) {
//        $lcms = $DB->get_record('lcms', array('id'=>$cm->instance));
//        if($lcms->type != 'Flash') {
//            $lcmsmodules[$lcms->id] = $lcms;
//        }
//    }

    $conditions = array('l.course = :courseid','l.progress = 1');
    $params = array('courseid'=>$courseid);
    
    if($name){ 
        $conditions[] = $DB->sql_like('l.name', ':name');
        $params['name'] = '%'.$name.'%';
    }
    
    $where = '';
    if(!empty($conditions)) {
        $where = ' WHERE '.implode(' and ', $conditions);
    }
    
    $sql = "SELECT l.*, cm.id AS cmid, l.course AS courseid
                FROM {lcms} l
                JOIN {course_modules} cm ON cm.instance = l.id
                JOIN {course_sections} cs ON cs.id = cm.section
                JOIN {modules} m ON m.name='lcms' AND m.id=cm.module 
                ".$where."
                ORDER BY cs.id,l.id ASC ";
    
    $lcmsmodules = false;
    if($page != -1) {
        $lcmsmodules = $DB->get_records_sql($sql, $params, $page*$perpage, $perpage);
    } else {
        $lcmsmodules = $DB->get_records_sql($sql, $params);
    }
        
    return $lcmsmodules;
}

function lcmsprogress_get_lcms_progress($lcmsid, $userid = 0) {
    global $CFG, $DB;

    $sql = "SELECT t.*, a.id AS lcmsid, a.course AS courseid
                FROM {lcms} a 
                JOIN {course_modules} cm ON cm.instance = a.id 
                LEFT JOIN {lcms_track} t ON t.lcms = cm.id
                WHERE a.id = :lcmsid and a.progress = 1 ";
    
    $params = array('lcmsid'=>$lcmsid);
    
    if($userid) {
        $sql .= ' AND t.userid = :userid';
        $params['userid'] = $userid;
    }
    
    $lcmsmodules = $DB->get_record_sql($sql, $params);
        
    return $lcmsmodules;
}

?>
