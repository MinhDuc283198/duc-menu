<?php

$handlers = array (
    'mod_created' => array (
        'handlerfile'      => '/mod/lcmsprogress/lib.php',
        'handlerfunction'  => 'lcmsprogress_mod_updated',
        'schedule'         => 'instant',
        'internal'         => 1,
    ),
 
    'mod_deleted' => array (
        'handlerfile'      => '/mod/lcmsprogress/lib.php',
        'handlerfunction'  => 'lcmsprogress_mod_updated',
        'schedule'         => 'instant',
        'internal'         => 1,
    ),
    
    'mod_updated' => array (
        'handlerfile'      => '/mod/lcmsprogress/lib.php',
        'handlerfunction'  => 'lcmsprogress_mod_updated',
        'schedule'         => 'instant',
        'internal'         => 1,
    ),
);