<?php

defined('MOODLE_INTERNAL') || die();

global $DB;

$logs = array(
    array('module'=>'lcmsprogress', 'action'=>'add', 'mtable'=>'lcmsprogress', 'field'=>'name'),
    array('module'=>'lcmsprogress', 'action'=>'update', 'mtable'=>'lcmsprogress', 'field'=>'name'),
    array('module'=>'lcmsprogress', 'action'=>'view', 'mtable'=>'lcmsprogress', 'field'=>'name')
);