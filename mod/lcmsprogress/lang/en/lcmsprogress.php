<?php
$string['pluginadministration'] = 'Progress Management';
$string['maxgrade'] = 'Maximum grade';
$string['modulename'] = 'Progress';
$string['pluginname'] = 'Progress';
$string['modulename_help'] = '';
$string['modulenameplural'] = 'Progress';

$string['lcmsprogressname'] = 'Progress Name';
$string['description'] = 'Description';
$string['lcmscontentno'] = 'NO.';
$string['lcmscontentname'] = 'Subject';
$string['lcmscontent'] = 'Contents';
$string['timetotal'] = 'Total';
$string['timeview'] = 'View';
$string['lastview'] = 'Last View';
$string['name'] = 'Student';
$string['progress'] = 'Progress';
$string['progressrate'] = 'Rate';
$string['progresslog'] = 'Log';
$string['progressscore'] = 'Score: <span class="lcmsprogress_score_text">{$a}%</span>';
$string['viewreport'] = 'Score Report';

$string['completionprogressgroup'] = 'Progress';
$string['completionprogress'] = 'Completion Progress';
$string['errorcompletionprogressvalue'] = '';
$string['neverseen'] = 'Never seen';

$string['emptylcmsdata'] = 'Empty Data';
$string['search'] = 'search';
$string['lcmscontentview'] = 'View';
$string['lcmscontentdown'] = 'Download';
$string['lcmscontentreg'] = 'Contents Register';
$string['prevpage'] = 'Prev';

$string['starttime'] = 'Start Time';
$string['enddtime'] = 'End Time';
$string['studytime'] = 'Study Time';
$string['progresslog'] = 'Video Progress Log';

$string['statistics'] = 'Lecture contents Statistics';
