<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017081801;       // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2012061700;    // Requires this Moodle version
$plugin->component = 'mod_lcmsprogress';      // Full name of the plugin (used for diagnostics)
$plugin->cron      = 0;