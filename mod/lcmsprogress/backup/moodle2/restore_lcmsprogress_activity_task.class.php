<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/lcmsprogress/backup/moodle2/restore_lcmsprogress_stepslib.php'); // Because it exists (must)

class restore_lcmsprogress_activity_task extends restore_activity_task {
    protected function define_my_settings() {
        // No particular settings for this activity
    }

    protected function define_my_steps() {
        // Commons only has one structure step
        $this->add_step(new restore_lcmsprogress_activity_structure_step('lcmsprogress_structure', 'lcmsprogress.xml'));
    }
    
    /**
     * Define the contents in the activity that must be
     * processed by the link decoder
     */
    static public function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('lcmsprogress', array('intro'), 'lcmsprogress');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();

        // List of lcmsprogress in course
        $rules[] = new restore_decode_rule('LCMSPROGRESSINDEX', '/mod/lcmsprogress/index.php?id=$1', 'course');
        // lcmsprogress by cm->id and forum->id
        $rules[] = new restore_decode_rule('LCMSPROGRESSVIEWBYID', '/mod/lcmsprogress/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('LCMSPROGRESSVIEWBYC', '/mod/lcmsprogress/view.php?c=$1', 'lcmsprogress');

        return $rules;

    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * lcmsprogress logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();

        $rules[] = new restore_log_rule('lcmsprogress', 'add', 'view.php?id={course_module}', '{lcmsprogress}');
        $rules[] = new restore_log_rule('lcmsprogress', 'update', 'view.php?id={course_module}', '{lcmsprogress}');
        $rules[] = new restore_log_rule('lcmsprogress', 'view', 'view.php?id={course_module}', '{lcmsprogress}');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();

        $rules[] = new restore_log_rule('lcmsprogress', 'view all', 'index.php?id={course}', null);

        return $rules;
    }
}