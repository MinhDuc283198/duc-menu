<?php

class restore_lcmsprogress_activity_structure_step extends restore_activity_structure_step {
    protected function define_structure() {
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');
        
        $paths[] = new restore_path_element('lcmsprogress', '/activity/lcmsprogress');
        if ($userinfo) {
            $paths[] = new restore_path_element('lcmsprogress_grades', '/activity/lcmsprogress/grades/grade');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    protected function process_lcmsprogress($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the lcmsprogress record
        $newitemid = $DB->insert_record('lcmsprogress', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }
    
    protected function process_lcmsprogress_grade($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->lcmsprogress = $this->get_new_parentid('lcmsprogress');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        $newitemid = $DB->insert_record('lcmsprogress_grades', $data);
    }
    
    
    protected function after_execute() {
        // Add lcms related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_lcmsprogress', 'intro', null);
    }
}