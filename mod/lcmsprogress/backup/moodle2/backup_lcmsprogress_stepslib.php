<?php

class backup_lcmsprogress_activity_structure_step extends backup_activity_structure_step {
    protected function define_structure() {
        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');
        
// Define each element separated
        $lcmsprogress = new backup_nested_element('lcmsprogress', array('id'), array(
            'name', 'intro', 'introformat',
            'grade', 'completionprogress', 'timemodified'));
        
        $grades = new backup_nested_element('grades');
        $grade  = new backup_nested_element('grade', array('id'), array(
            'lcmsprogress', 'userid', 'timecreated', 'timemodified', 'progress', 'grade'));
        
// Build the tree
        $lcmsprogress->add_child($grades);
        $grades->add_child($grade);
        
// Define sources
        $lcmsprogress->set_source_table('lcmsprogress', array('id' => backup::VAR_ACTIVITYID));
        
        if ($userinfo) {
            $grade->set_source_table('lcmsprogress_grades', array('lcmsprogress' => backup::VAR_PARENTID));
        }
        
// Define id annotations
        $grade->annotate_ids('user', 'userid');
        
// Define file annotations
        $lcmsprogress->annotate_files('mod_lcmsprogress', 'intro', null); // This file area hasn't itemid
        
        // Return the root element (lcmsprogress), wrapped into standard activity structure
        return $this->prepare_activity_structure($lcmsprogress);
    }
}
