<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/lcmsprogress/backup/moodle2/backup_lcmsprogress_stepslib.php');
require_once($CFG->dirroot . '/mod/lcmsprogress/backup/moodle2/backup_lcmsprogress_settingslib.php');

class backup_lcmsprogress_activity_task extends backup_activity_task {
    protected function define_my_settings() {
        
    }

    protected function define_my_steps() {
        $this->add_step(new backup_lcmsprogress_activity_structure_step('lcmsprogress_structure', 'lcmsprogress.xml'));
    }
    
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of lcmsprogress
        $search="/(".$base."\/mod\/lcmsprogress\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSPROGRESSINDEX*$2@$', $content);

        // Link to lcmsprogress view by cmm->id
        $search="/(".$base."\/mod\/lcmsprogress\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSPROGRESSVIEWBYID*$2@$', $content);
        
        // Link to lcmsprogress view by lcmsprogress->id
        $search="/(".$base."\/mod\/lcmsprogress\/view.php\?c\=)([0-9]+)/";
        $content= preg_replace($search, '$@LCMSPROGRESSVIEWBYC*$2@$', $content);

        return $content;
    }
}