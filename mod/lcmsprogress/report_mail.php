<?php

require_once('../../config.php');
require_once($CFG->libdir . '/accesslib.php');
require_once('locallib.php');
require_once(dirname(dirname(dirname(dirname (__FILE__)))).'/lib/mailform.php');

$id = required_param('id', PARAM_INT);          // Course Module ID
$c = optional_param('c', 0, PARAM_INT);        // lcmsprogress ID
$userid = optional_param('u', 0, PARAM_INT);        // User ID
$name = optional_param('name', '', PARAM_RAW);
$prog = optional_param('prog', '', PARAM_RAW);
$mail = optional_param('mail', 0, PARAM_INT);

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['c'] = $c;
}

$PAGE->set_url('/mod/lcmsprogress/view.php', $params);

if ($id) {
    if (!$cm = get_coursemodule_from_id('lcmsprogress', $id)) {
        print_error('Course Module ID was incorrect');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('course is misconfigured');
    }
    if (!$lcmsprogress = $DB->get_record('lcmsprogress', array('id' => $cm->instance))) {
        print_error('course module is incorrect');
    }
} else if ($c) {
    if (!$lcmsprogress = $DB->get_record("lcmsprogress", array("id" => $c))) {
        print_error('invalidlcmsid', 'lcmsprogress');
    }
    if (!$course = $DB->get_record("course", array("id" => $lcmsprogress->course))) {
        print_error('coursemisconf');
    }
    if (!$cm = get_coursemodule_from_instance("lcmsprogress", $lcmsprogress->id, $course->id)) {
        print_error('missingparameter');
    }
} else {
    print_error('missingparameter');
}

require_course_login($course, true, $cm);
$strlcmses = get_string("modulenameplural", "lcmsprogress");
$strlcms = get_string("modulename", "lcmsprogress");

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$PAGE->set_context($context);

$output = $PAGE->get_renderer('mod_lcmsprogress');

$PAGE->set_title(format_string($lcmsprogress->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_activity_record($lcmsprogress);

echo $output->header();



if (has_capability('mod/lcmsprogress:viewall', $context)) {

    $conditions = array();
    $params = array('lcmsprogress' => $lcmsprogress->id, 'courseid' => $course->id);

    if ($name) {

        $conditionname[] = $DB->sql_like('cu.firstname', ':firstname', false);
        $conditionname[] = $DB->sql_like('cu.lastname', ':lastname', false);
        $conditionname[] = $DB->sql_like($DB->sql_fullname('cu.firstname', 'cu.lastname'), ':fullname', false);
        $conditionname[] = $DB->sql_like($DB->sql_fullname('cu.lastname', 'cu.firstname'), ':fullname1', false);
        $conditionname[] = $DB->sql_like($DB->sql_concat('cu.firstname', 'cu.lastname'), ':fullname2', false);
        $conditionname[] = $DB->sql_like($DB->sql_concat('cu.lastname', 'cu.firstname'), ':fullname3', false);

        $conditions[] = '(' . implode(' OR ', $conditionname) . ')';

        $params['firstname'] = '%' . $name . '%';
        $params['lastname'] = '%' . $name . '%';
        $params['fullname'] = '%' . $name . '%';
        $params['fullname1'] = '%' . $name . '%';
        $params['fullname2'] = '%' . $name . '%';
        $params['fullname3'] = '%' . $name . '%';
    }

    if ($prog != '') {
        if ($prog == '0')
            $conditions[] = '(g.progress is null or (g.progress is not null and g.progress=:prog))';
        else
            $conditions[] = '(g.progress is null or (g.progress is not null and g.progress<:prog))';
	$prog2 = trim(str_replace('ow','',$prog));
        $params['prog'] = $prog2;
    }

    $sql_where = '';
    if (!empty($conditions)) {
        $sql_where = ' WHERE ' . implode(' and ', $conditions);
    }

    $sql_select = "SELECT cu.*, g.timemodified, g.progress, g.grade ";
    $sql_from = " FROM (SELECT DISTINCT u.id, u.username, u.email, u.lastname, u.firstname,
                                      e.courseid
                      FROM {user} u 
                      JOIN {user_enrolments} ue ON ue.userid = u.id 
                      JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = :courseid)) cu
                JOIN {lcmsprogress} p ON (p.course = cu.courseid AND p.id = :lcmsprogress)
                LEFT JOIN {lcmsprogress_grades} g ON (g.lcmsprogress = p.id AND g.userid = cu.id) ";
    $sql_sort = "ORDER BY cu.firstname";

    $grades = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_sort, $params);
    $users = '';
    $progparams = array();

    foreach ($grades as $grade) {

        $lastview = '-';
        if ($grade->timemodified) {
            $lastview = userdate($grade->timemodified);
        }
        $progress = '0%';
        if ($grade->progress) {
            $progress = round($grade->progress) . '%';
        }

        $users .= $grade->id . ',';
        $progparams[$grade->id] = $progress;
    }


    //평균구하기
    $average = $DB->get_field('lcmsprogress_grades', 'avg(progress)', array('lcmsprogress' => $lcmsprogress->id));
    //개강일
    $query = 'select c.timestart,c.timeend from {snule_class} c JOIN {snule_application} a ON a.class=c.id where a.moodlecourse=:course';
    $period = $DB->get_record_sql($query, array('course' => $course->id));
    $period1 = date('Y년 m월 d일',$period->timestart);
    //종료일
    $period2 = date('Y년 m월 d일',$period->timeend);
    //Email, SMS 전송
    $sendparams = array();
    $sendparams['coursename'] = $course->fullname;
    $sendparams['name'] = '{name}';
    $sendparams['progress'] = '{progress}';
    $sendparams['day'] = date('Y년 m월 d일');
    $sendparams['users'] = $users;
    $sendparams['prog'] = $progparams;
    $sendparams['period1'] = $period1;
    $sendparams['period2'] = $period2;
    $sendparams['average'] = round($average) . '%';
    $sendparams['issue'] = '진도율확인-' . $prog;
    $sendtype = 'progress' . $prog;

    $mail_cnt = system_mail_send($sendtype, $sendparams);
    $sms_cnt = system_sms_send($sendtype, $sendparams);

    echo '<p>메일:'.$mail_cnt.'건 발송'.'<br/>'.'SMS:'.$sms_cnt.'건 발송</p>';
    echo '<p>
    <input type="button" value="이메일발송확인" onclick="location.href=\''.$CFG->siteroot.'/admin/site/email.php\'"/>
    <input type="button" value="SMS발송확인" onclick="location.href=\''.$CFG->siteroot.'/admin/site/sms.php\'"/>        
    </p>';
    
} else {
    error("No permission");
}


echo $OUTPUT->footer($course);