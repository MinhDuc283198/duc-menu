<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the forms to create and edit an instance of this module
 *
 * @package   mod_lcmsprogress
 * @copyright 2012 NetSpot {@link http://www.netspot.com.au}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.');

require_once($CFG->dirroot.'/course/moodleform_mod.php');
//require_once($CFG->dirroot . '/mod/lcmsprogress/locallib.php');

/**
 * Assignment settings form.
 *
 * @package   mod_lcmsprogress
 * @copyright 2012 NetSpot {@link http://www.netspot.com.au}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_lcmsprogress_mod_form extends moodleform_mod {

    /**
     * Called to define this moodle form
     *
     * @return void
     */
    public function definition() {
        global $CFG, $DB, $PAGE;
        $mform = $this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('lcmsprogressname', 'lcmsprogress'), array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $this->add_intro_editor(true, get_string('description', 'lcmsprogress'));

//        $ctx = null;
//        if ($this->current && $this->current->coursemodule) {
//            $cm = get_coursemodule_from_instance('lcmsprogress', $this->current->id, 0, false, MUST_EXIST);
//            $ctx = context_module::instance($cm->id);
//        }
//        $lcmsprogressment = new lcmsprogress($ctx, null, null);
//        if ($this->current && $this->current->course) {
//            if (!$ctx) {
//                $ctx = context_course::instance($this->current->course);
//            }
//            $course = $DB->get_record('course', array('id'=>$this->current->course), '*', MUST_EXIST);
//            $lcmsprogressment->set_course($course);
//        }

//        $config = get_config('lcmsprogress');

//        $mform->addElement('header', 'availability', get_string('availability', 'lcmsprogress'));
//        $mform->setExpanded('availability', true);


        $this->standard_grading_coursemodule_elements();

        $this->standard_coursemodule_elements();

        $this->add_action_buttons();

        // Add warning popup/noscript tag, if grades are changed by user.
//        $hasgrade = false;
//        if (!empty($this->_instance)) {
//            $hasgrade = $DB->record_exists_select('lcmsprogress_grades',
//                                                  'lcmsprogress = ? AND grade <> -1',
//                                                  array($this->_instance));
//        }

//        if ($mform->elementExists('grade') && $hasgrade) {
//            $module = array(
//                'name' => 'mod_lcmsprogress',
//                'fullpath' => '/mod/lcmsprogress/module.js',
//                'requires' => array('node', 'event'),
//                'strings' => array(array('changegradewarning', 'mod_lcmsprogress'))
//                );
//            $PAGE->requires->js_init_call('M.mod_lcmsprogress.init_grade_change', null, false, $module);
//
//            // Add noscript tag in case.
//            $noscriptwarning = $mform->createElement('static',
//                                                     'warning',
//                                                     null,
//                                                     html_writer::tag('noscript',
//                                                     get_string('changegradewarning', 'mod_lcmsprogress')));
//            $mform->insertElementBefore($noscriptwarning, 'grade');
//        }
    }
    
    function data_preprocessing(&$default_values) {
        $default_values['completionprogressenabled'] =
            !empty($default_values['completionprogress']) ? 1 : 0;
        
        if (empty($default_values['completionprogress'])) {
            $default_values['completionprogress'] = 80;
        }
    }
    
    function add_completion_rules() {
        $mform =& $this->_form;

        $group=array();
        $group[] =& $mform->createElement('checkbox', 'completionprogressenabled', '', get_string('completionprogress','lcmsprogress'));
        $group[] =& $mform->createElement('text', 'completionprogress', '', array('size'=>5));
        $mform->setType('completionprogress', PARAM_INT);
        $mform->addGroup($group, 'completionprogressgroup', get_string('completionprogressgroup','lcmsprogress'), array(' '), false);
        $mform->disabledIf('completionprogress','completionprogressenabled','notchecked');

        return array('completionprogressgroup');
    }
    
    function completion_rule_enabled($data) {
        return (!empty($data['completionprogressenabled']) && $data['completionprogress']!=0);
    }
    
    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        if( $data['completionprogressenabled'] && ($data['completionprogress'] <= 0 || $data['completionprogress'] > 100)) {
            $errors['completionprogressgroup'] = get_string('errorcompletionprogressvalue', 'lcmsprogress');
        }

        return $errors;
    }

}
