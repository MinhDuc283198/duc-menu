<?php


require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/mod/teamboard/lib.php');

$id      = required_param('id', PARAM_INT);             
$mode    = optional_param('mode', null, PARAM_INT);     
$user    = optional_param('user', 0, PARAM_INT);        
$sesskey = optional_param('sesskey', null, PARAM_RAW);  

$url = new moodle_url('/mod/teamboard/subscribe.php', array('id'=>$id));
if (!is_null($mode)) {
    $url->param('mode', $mode);
}
if ($user !== 0) {
    $url->param('user', $user);
}
if (!is_null($sesskey)) {
    $url->param('sesskey', $sesskey);
}
$PAGE->set_url($url);

$teamboard   = $DB->get_record('teamboard', array('id' => $id), '*', MUST_EXIST);
$course  = $DB->get_record('course', array('id' => $teamboard->course), '*', MUST_EXIST);
$cm      = get_coursemodule_from_instance('teamboard', $teamboard->id, $course->id, false, MUST_EXIST);
$context = get_context_instance(CONTEXT_MODULE, $cm->id);

if ($user) {
    require_sesskey();
    if (!has_capability('mod/teamboard:managesubscriptions', $context)) {
        print_error('nopermissiontosubscribe', 'forum');
    }
    $user = $DB->get_record('user', array('id' => $user), MUST_EXIST);
} else {
    $user = $USER;
}

if (isset($cm->groupmode) && empty($course->groupmodeforce)) {
    $groupmode = $cm->groupmode;
} else {
    $groupmode = $course->groupmode;
}
if ($groupmode && !teamboard_is_subscribed($user->id, $teamboard) && !has_capability('moodle/site:accessallgroups', $context)) {
    if (!groups_get_all_groups($course->id, $USER->id)) {
        print_error('cannotsubscribe', 'forum');
    }
}

require_login($course->id, false, $cm);

if (is_null($mode) and !is_enrolled($context, $USER, '', true)) {  
    $PAGE->set_title($course->shortname);
    $PAGE->set_heading($course->fullname);
    if (isguestuser()) {
        echo $OUTPUT->header();
        echo $OUTPUT->confirm(get_string('subscribeenrolledonly', 'forum').'<br /><br />'.get_string('liketologin'),
                     get_login_url(), new moodle_url('/mod/teamboard/content_list.php', array('f'=>$id)));
        echo $OUTPUT->footer();
        exit;
    } else {
       
        redirect(new moodle_url('/mod/teamboard/content_list.php', array('b'=>$id)), get_string('subscribeenrolledonly', 'forum'));
    }
}

$returnto = optional_param('backtoindex',0,PARAM_INT)
    ? "index.php?id=".$course->id
    : "content_list.php?b=$id";

if (!is_null($mode) and has_capability('mod/teamboard:managesubscriptions', $context)) {
    require_sesskey();
    switch ($mode) {
        case TEAMBOARD_CHOOSESUBSCRIBE : 
            teamboard_forcesubscribe($teamboard->id, 0);
            redirect($returnto, get_string("everyonecannowchoose", "forum"), 1);
            break;
        case TEAMBOARD_FORCESUBSCRIBE : 
            teamboard_forcesubscribe($teamboard->id, 1);
            redirect($returnto, get_string("everyoneisnowsubscribed", "forum"), 1);
            break;
        case TEAMBOARD_INITIALSUBSCRIBE :
            teamboard_forcesubscribe($teamboard->id, 2);
            redirect($returnto, get_string("everyoneisnowsubscribed", "forum"), 1);
            break;
        case TEAMBOARD_DISALLOWSUBSCRIBE :
            teamboard_forcesubscribe($teamboard->id, 3);
            redirect($returnto, get_string("noonecansubscribenow", "forum"), 1);
            break;
        default:
            print_error(get_string('invalidforcesubscribe', 'forum'));
    }
}

if (teamboard_is_forcesubscribed($teamboard)) {
    redirect($returnto, get_string("everyoneisnowsubscribed", "forum"), 1);
}

$info->name  = fullname($user);
$info->teamboard = format_string($teamboard->name);

if (teamboard_is_subscribed($user->id, $teamboard->id)) {
    if (is_null($sesskey)) {    
        $PAGE->set_title($course->shortname);
        $PAGE->set_heading($course->fullname);
        echo $OUTPUT->header();
        echo $OUTPUT->confirm(get_string('confirmunsubscribe', 'forum', format_string($teamboard->name)),
                new moodle_url($PAGE->url, array('sesskey' => sesskey())), new moodle_url('/mod/teamboard/content_list.php', array('b' => $id)));
        echo $OUTPUT->footer();
        exit;
    }
    require_sesskey();
    if (teamboard_unsubscribe($user->id, $teamboard->id)) {
        add_to_log($course->id, "teamboard", "unsubscribe", "content_list.php?b=$teamboard->id", $teamboard->id, $cm->id);
        redirect($returnto, get_string("nownotsubscribed", "forum", $info), 1);
    } else {
        print_error('cannotunsubscribe', 'forum', $_SERVER["HTTP_REFERER"]);
    }

} else {  
    if ($teamboard->forcesubscribe == TEAMBOARD_DISALLOWSUBSCRIBE &&
                !has_capability('mod/teamboard:managesubscriptions', $context)) {
        print_error('disallowsubscribe', 'forum', $_SERVER["HTTP_REFERER"]);
    }
    if (!has_capability('mod/teamboard:viewcontent', $context)) {
        print_error('noviewdiscussionspermission', 'forum', $_SERVER["HTTP_REFERER"]);
    }
    if (is_null($sesskey)) {    
        $PAGE->set_title($course->shortname);
        $PAGE->set_heading($course->fullname);
        echo $OUTPUT->header();
        echo $OUTPUT->confirm(get_string('confirmsubscribe', 'forum', format_string($teamboard->name)),
                new moodle_url($PAGE->url, array('sesskey' => sesskey())), new moodle_url('/mod/teamboard/content_list.php', array('b' => $id)));
        echo $OUTPUT->footer();
        exit;
    }
    require_sesskey();
    teamboard_subscribe($user->id, $teamboard->id);
    add_to_log($course->id, "teamboard", "subscribe", "content_list.php?b=$teamboard->id", $teamboard->id, $cm->id);
    redirect($returnto, get_string("nowsubscribed", "forum", $info), 1);
}
