<?php

$string['pluginname'] = '게시판(조모임)';
$string['modulename'] = '게시판(조모임)';
$string['modulename_help'] = '<p><b>게시판(조모임)</b></p>
<div class="indent">
<p>조단위의 게시글 활용및 접근을 원활하게 사용할 수 있도록 특별히 만든 게시판입니다.</p>
<p>조단위로 게시판을 생성할 수 있으며 무들에서 조를 의마하는 “조모임” 별로 게시글을 접근하고 활용할 수 있습니다.</p>
<p>게시판의 글들은 북마킹이 가능하며 이때 북마킹된 내용은 강의 게시판의 나의 게시판에 모아 볼 수 있습니다.</p>
<p>게시판의 내용들은 게시글, 답글, 추천 등애 대한 통계 분석이 자동으로 제공됩니다.</p>
</div>';
$string['modulenameplural'] = '게시판(조모임)';
$string['boardname'] = '게시판 이름';
$string['boardtype'] = '게시판 종류';
$string['boardintro'] = '게시판 소개';
$string['maxattachments'] = '최대 첨부물 수';
$string['maxattachments_help'] = '<p>게시판(조모임)에서 각 게시글에 얼마나 많은 파일을 첨부할 수 있는지를 설정할 수 있다.</p>';
$string['maxattachmentsize'] = '최대 첨부 용량';
$string['massfile'] = '대용량 첨부 파일 사용';
$string['massfilemaxsize'] = '대용량 첨부 파일 최대 크기';
$string['normalfilemaxsize'] = '일반 첨부 파일 최대 크기';
$string['maxattachmentsize_help'] = '<p>이 설정은 게시판의 게시물에 첨부할 수 있는 파일의 최대 크기를 명시한다.</p>';

$string['boardtype:notice'] = '공지사항';
$string['boardtype:qna'] = '질문답변';
$string['boardtype:general'] = '게시판(일반)';
$string['boardtype:forum'] = '게시판(토론)';
$string['boardtype:team'] = '게시판(조모임)';

$string['invalidaccess'] = '이  페이지는 올바르게 접속되지 않았습니다.';
$string['invalidcontentsid'] = '게시물 ID가 틀렸거나 더 이상 존재하지 않습니다.';
$string['invalidboardid'] = '게시판 ID가 바르지 않음';
$string['invalidparentpostid'] = '상위 게시글 ID가 바르지 않음';
$string['invalidpostid'] = '잘못된 게시물 ID - {$a}';

$string['bynameondate'] = '{$a->date} 에 {$a->name} 씀 ';

$string['pluginadministration'] = '게시판(조모임) 관리';

$string['addanewdiscussion'] = '새로운 토론 주제 추가';
$string['addanewquestion'] = '새로운 질문 추가';
$string['addanewtopic'] = '새로운 주제 추가';
$string['addnewgroup'] = '조모임 생성';
$string['addnewgroupboard'] = '조모임 게시판 추가';
$string['cannotadddiscussion'] = '게시판(조모임)에 글을 제시하려면 프로젝트팀의 구성원이어야 합니다.';
$string['cannotadddiscussionall'] = '공동 토론 주제를 추가할 권한이 없습니다.';

$string['noquestions'] = '이 게시판에 작성된 글이 없습니다.';
$string['nonews'] = '작성된 공지사항이 없습니다.';
$string['nodiscussions'] = '이 게시판에 작성된 토론 주제가 없습니다.';
$string['noboard'] = '생성된 조모임 게시판이 없습니다.';
$string['addgroup'] = '선택하신 조모임을 이 게시판에 추가하시겠습니까?.';
$string['noselectgroup'] = '선택된 조모임이 없습니다.';

$string['timecreated'] = '작성일';
$string['options'] = '공개여부';
$string['discussion'] = '토론';
$string['startedby'] = '작성자';

$string['viewCount'] = '조회';
$string['view:cnt'] = '조회수';
$string['isIncludeFile'] = '파일';

$string['title']	= '제목';
$string['writer']	= '작성자';
$string['content']	= '내용';
$string['search'] = '검색';

$string['newpost'] = '글쓰기';
$string['attachment'] = '첨부';
$string['reply'] = '댓글';

$string['header_etc'] = '기타 설정';
$string['board_secret'] = '비밀글 기능사용';
$string['board_category'] = '카테고리';
$string['board_comment'] = '덧글';
$string['board_reply'] = '답글';
$string['board_notice'] = '공지사항';
$string['board_recommend'] = '추천';
$string['board_thread'] = '스레드 형식';
$string['board_general'] = '게시판 형식';
$string['no_delete_permission']	= '삭제 권한이 없습니다.';
$string['Deletionshavebeencompleted']	= '삭제되었습니다.';
$string['backtoboard']	= '돌아가기';
$string['isprivate_group'] = '비공개';
$string['ispublic_group'] = '다른 조원에게 공개';
$string['normar'] = '공개';
$string['next:content'] = '+더보기';
$string['nextpage'] = '다음';
$string['prevpage'] = '이전';
$string['cancel'] = '취소';

$string['edit'] = '수정';
$string['reply:cnt'] = '댓글수';
$string['delete'] = '삭제';
$string['enterstr_text'] = '내용을 입력하세요.';
$string['suredeleteselectedcontents'] = '선택한 글을 삭제하겠습니까?';
$string['surerecommendcontents'] = '선택한 글을 추천하겠습니까?';


$string['file'] = '첨부파일';
$string['save'] = '저장';
$string['name'] = '이름';
$string['nocontent'] = '게시글이 없습니다.';
$string['date'] = '날짜';
$string['no.'] = '번호';
$string['order'] = '순서';
$string['reCommondCnt'] = '추천';
$string['recommend'] = "추천";
$string['recommended'] = "추천됨";
$string['action'] = 'Action';
$string['count'] = 'Count';
$string['replies'] = '답글';

$string['cannotviewpostlist'] = '이 게시판을 볼 수 없습니다.';
$string['cannotviewpost'] = '이 게시글을 볼 수 없습니다.';
$string['cannoteditpost'] = '이 게시글을 수정할 수 없습니다.';
$string['cannotreplypost'] = '이 게시글에 답글을 남길 수 없습니다.';
$string['cannotaddpost'] = '글을 게시할 수 없습니다.';
$string['cannotdeletepost'] = '이 게시글을 삭제할 수 없습니다.';
$string['cannotmanagecategory'] = '카테고리를 관리할 수 없습니다.';
$string['content_recommend'] = '이 게시글을 추천하시겠습니까?';
$string['content_delete'] = '이 게시글을 삭제하시겠습니까?';

$string['cannotaddcomment'] = '댓글을 남길 수 없습니다.';

$string['isNotice'] = '상단노출';
$string['yesNotice'] = '상단공지노출';
$string['noNotice'] = '공지글이 아닙니다.';
$string['emptynotice'] = '과목 공지사항이 없습니다.';

$string['suredeleteselectedcategory'] = '선택한 카테고리를 삭제하시겠습니까?';


$string['teamboards'] = '게시판(조모임)';
$string['teamboard'] = '게시판(조모임)';
$string['discussions'] = '내용';
$string['subscribed'] = '구독됨';
$string['unreadposts'] = '읽지 않은 글';
$string['tracking'] = '추적';
$string['markallread'] = '이 게시판의 모든 게시물을 \'읽었음\'으로 표시합니다.';
$string['trackteamboard'] = '게시판(조모임) 추적';
$string['notrackteamboard'] = '읽지 않은 글 추적';
$string['subscribe'] = '이 게시판 구독';
$string['unsubscribe'] = '이 게시판 구독 해지';
$string['yes'] = '네';
$string['no'] = '아니오';

$string['allsubscribe'] = '모든 게시판 구독';
$string['allunsubscribe'] = '모든 게시판 구독 해지';

$string['learningteamboards'] = '게시판(조모임)';

$string['public_setup'] = '이 조모임 공개로 수정';
$string['ispublic_msg'] = '이 조모임을 공개로 설정하시겠습니까?';
$string['private_setup'] = '이 조모임 비공개로 수정';
$string['isprivate_msg'] = '이 조모임을 비공개로 설정하시겠습니까?';
$string['delete_msg'] = '조모임 삭제시 게시글도 함께 삭제 됩니다, 삭제하시겠습니까?';
$string['authorized'] = '권한이 없습니다.';

// ==========================================
// 게시판(조모임) 권한 설정
// ==========================================
$string['teamboard:categorymanager']			= '카테고리 관리';
$string['teamboard:noticemanager']			= '공지사항 관리';
$string['teamboard:secretmanager']			= '비밀글 관리';
$string['teamboard:viewsubscribers']				= '구독자 보기';

$string['teamboard:commentdelete']				= '덧글 삭제';
$string['teamboard:commentview']				= '덧글 보기';
$string['teamboard:commentwrite']				= '덧글 생성';

$string['teamboard:delete']				= '삭제';
$string['teamboard:edit']				= '수정';
$string['teamboard:reply']				= '댓글';
$string['teamboard:list']				= '목록';
$string['teamboard:viewcontent']				= '보기';
$string['teamboard:write']				= '쓰기';

$string['teamboard:managesubscriptions']			= '구독 관리';

$string['teamboard:modify']			= '게시물 수정';
$string['teamboard:view']				= '게시물 보기';
$string['teamboard:write']			= '게시물 추가';
$string['teamboard:reply']			= '게시물 답변';
$string['teamboard:manage']			= '게시물 관리';
$string['teamboard:comment']			= '덧글 기능';

$string['prohibitdistribution'] = '본 전자교재는 저작권법에 의해 연세대학교 학생들의 학습 용도로만 사용할 수 있도록 허가되었습니다.\n학습 용도 이외의 목적으로 외부에 전송, 변형한다면 저작권 관련 법적 문제가 발생할 수 있으니 각별히 주의바랍니다.\n위 내용에 동의하시면 다운로드가 시작됩니다.';
$string['list'] = "목록";
$string['next_content'] = '다음글';
$string['prev_content'] = '이전글';
$string['current_content'] = '현재글';

$string['deletedfile'] = '대용량 첨부파일로서, 등록된 학기 종료 후 삭제되었습니다.';
$string['isprivate'] = '비밀글 입니다.';
$string['maxlength'] = '댓글 최대글자 수는 300자 입니다.';
