<?php

$string['pluginname'] = 'Forum(Group)';
$string['modulename'] = 'Forum(Group)';
$string['modulename_help'] = 'The board activity module enables participants to have asynchronous discussions i.e. discussions that take place over an extended period of time. You can use comment and reply options along with uploading/downloading files. ';
$string['modulenameplural'] = 'Forum(Group)';
$string['boardname'] = 'Forum name';
$string['boardtype'] = 'Forum type';
$string['boardintro'] = 'Forum introduction';
$string['maxattachments'] = 'Maximum number of attachments';
$string['maxattachments_help'] = 'This setting specifies the maximum number of files that can be attached to a board post.';
$string['maxattachmentsize'] = 'Maximum attachment size';
$string['massfile'] = 'Mass file attachment';
$string['massfilemaxsize'] = 'Mass file size';
$string['normalfilemaxsize'] = 'Normal file size';
$string['maxattachmentsize_help'] = 'This setting specifies the largest size of file that can be attached to a board post.';

$string['boardtype:notice'] = 'Notice board';
$string['boardtype:qna'] = 'Q&A';
$string['boardtype:general'] = 'Forum(General)';
$string['boardtype:forum'] = 'Forum';
$string['boardtype:team'] = 'Forum(Group)';

$string['invalidaccess'] = 'This page was not accessed correctly';
$string['invalidcontentsid'] = 'ID was incorrect or no longer exists';
$string['invalidboardid'] = 'Forum ID was incorrect';
$string['invalidparentpostid'] = 'Parent post ID was incorrect';
$string['invalidpostid'] = 'Invalid post ID - {$a}';

$string['pluginadministration'] = 'Forum(Group) administration';

$string['addanewdiscussion'] = 'Add a new discussion topic';
$string['addanewquestion'] = 'Add a new question';
$string['addanewtopic'] = 'Add a new topic';
$string['addnewgroup'] = 'Add a new group';
$string['addnewgroupboard'] = 'Add a new group board';
$string['cannotadddiscussion'] = 'Adding discussions to this board requires group membership.';
$string['cannotadddiscussionall'] = 'You do not have permission to add a new discussion topic for all participants.';

$string['noquestions'] = 'There are no questions yet in this board';
$string['nonews'] = 'No news has been posted yet';
$string['nodiscussions'] = 'There are no discussion topics yet in this board';
$string['noboard'] = 'There is no board created.';

$string['timecreated'] = 'Timecreated';

$string['discussion'] = 'Discussion';
$string['startedby'] = 'Started by';

$string['viewCount'] = 'Views';
$string['view:cnt'] = 'Views';
$string['isIncludeFile'] = 'File(s)';

$string['title']	= 'Title';
$string['writer']	= 'Writer';
$string['content']	= 'Content';
$string['search'] = 'Search';

$string['newpost'] = 'Post';
$string['attachment'] = 'attachment';
$string['reply'] = 'reply';

$string['header_etc'] = 'Etc settings';
$string['board_secret'] = 'Secret';
$string['board_category'] = 'Category';
$string['board_comment'] = 'Comment';
$string['board_reply'] = 'Reply';
$string['board_notice'] = 'Notice';
$string['board_recommend'] = 'Recommend';
$string['board_thread'] = 'Thread type';
$string['board_general'] = 'Board type';

$string['bynameondate'] = 'by {$a->name} - {$a->date}';

$string['no_delete_permission']	= 'No delete_permission';
$string['Deletionshavebeencompleted']	= 'Post is Deleted';
$string['backtoboard']	= 'backtoboard';
$string['isprivate'] = 'Private';

$string['next:content'] = '+View more';
$string['nextpage'] = 'Next';
$string['prevpage'] = 'Previous';
$string['cancel'] = 'Cancel';

$string['edit'] = 'Edit';
$string['reply:cnt'] = 'reply';
$string['delete'] = 'Delete';
$string['enterstr_text'] = 'Enter contents.';
$string['suredeleteselectedcontents'] = 'Are you sure you want to delete selected contents?';
$string['surerecommendcontents'] = 'Are you sure you want to recommend selected contents?';
$string['content_recommend'] = 'Are you sure you want to recommend this contents?';
$string['content_delete'] = 'Are you sure you want to delete this contents?';

$string['file'] = 'File(s)';
$string['save'] = 'Save';
$string['name'] = 'Name';
$string['nocontent'] = 'There are no posts';
$string['date'] = 'Date';
$string['no.'] = 'No.';
$string['order'] = 'Order';
$string['replies'] = 'Replies';
$string['recommend'] = "Recommend";
$string['reCommondCnt'] = 'Recommend';
$string['recommended'] = "Recommended";
$string['action'] = 'Action';
$string['count'] = 'Count';

$string['cannotviewpostlist'] = 'You can not view this board';
$string['cannotviewpost'] = 'You can not view this post';
$string['cannoteditpost'] = 'You can not update this post';
$string['cannotreplypost'] = 'You can not reply this post';
$string['cannotaddpost'] = 'You can not post';
$string['cannotdeletepost'] = 'You can not delete this post';
$string['cannotmanagecategory'] = 'You can not manage category';


$string['cannotaddcomment'] = 'You can not add a comment';

$string['isNotice'] = 'Notice';
$string['yesNotice'] = 'Front Notice';
$string['noNotice'] = 'This post is not notice';
$string['emptynotice'] = 'There are no notices yet in this course';

$string['suredeleteselectedcategory'] = 'Are you sure you want to delete selected category?';
 

$string['teamboards'] = 'Jinotechboards';
$string['teamboard'] = 'Jinotechboard';
$string['discussions'] = 'Contents';
$string['subscribed'] = 'Subscribed';
$string['unreadposts'] = 'Unread posts';
$string['tracking'] = 'Track';
$string['markallread'] = 'Mark all posts in this board read.';
$string['trackteamboard'] = 'trackteamboard';
$string['notrackteamboard'] = 'Track unread contents';
$string['subscribe'] = 'Subscribe to this board';
$string['unsubscribe'] = 'Unsubscribe from this board';
$string['yes'] = 'yes';
$string['no'] = 'no';

$string['allsubscribe'] = 'Subscribe to all boards';
$string['allunsubscribe'] = 'Unsubscribe from all boards';

$string['learningteamboards'] = 'Forum(Group)';

$string['public_setup'] = 'Change the group to the public';
$string['ispublic_msg'] = 'Do you want to set up a public group?';
$string['private_setup'] = 'Change the group to the private';
$string['isprivate_msg'] = 'Do you want to set up a private group?';
$string['delete_msg'] = 'Group deleting posts will be deleted as well , you sure you want to delete ?';
$string['authorized'] = 'You are not authorized';
$string['addgroup'] = 'Do you want to add the group that was selected for this board?';
$string['noselectgroup'] = 'There is no selected group.';

// ==========================================
// 조별 게시판 권한 설정
// ==========================================
$string['teamboard:categorymanager']			= 'Category management';
$string['teamboard:noticemanager']			= 'Notice management';
$string['teamboard:secretmanager']			= 'Secret management';
$string['teamboard:viewsubscribers']				= 'View subscribers';

$string['teamboard:commentdelete']				= 'Comment delete';
$string['teamboard:commentview']				= 'Comment view';
$string['teamboard:commentwrite']				= 'Comment write';

$string['teamboard:delete']				= 'Contents delete';
$string['teamboard:edit']				= 'Contents edit';
$string['teamboard:reply']				= 'Contents reply';
$string['teamboard:list']				= 'Contents list';
$string['teamboard:viewcontent']				= 'Contents view';
$string['teamboard:write']				= 'Contents write';

$string['teamboard:managesubscriptions']			= 'Management Subscription';

$string['teamboard:modify']			= 'Edit post';
$string['teamboard:view']				= 'View post';
$string['teamboard:write']			= 'Add post';
$string['teamboard:reply']			= 'Reply post';
$string['teamboard:manage']			= 'Post management';
$string['teamboard:comment']			= 'Comment option';

$string['prohibitdistribution'] = 'This electronic teaching material is protected by copyright law and is only permitted for Yonsei University students\' learning purposes. Unauthorized distribution, modification is prohibited. If you accept the condition above, download will start immediately. ';
$string['options'] = 'Check Public';
$string['normar'] = 'Public';
$string['list'] = "List";
$string['next_content'] = 'Next Content';
$string['prev_content'] = 'Prev Content';
$string['current_content'] = 'Current Content';

$string['deletedfile'] = 'The attached file was deleted in server because of its big size after (end of) the semester when it was uploaded.';

$string['isprivate'] = 'This is a private';
$string['maxlength'] = 'Comments The maximum number of characters is 300 characters.';