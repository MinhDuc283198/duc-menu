<?php

require_once '../../config.php';
require_once './lib.php';
require_once './write_form.php';
require_once $CFG->libdir . '/completionlib.php';
require_once $CFG->dirroot . '/lib/formslib.php';
require_once $CFG->dirroot . '/lib/form/filemanager.php';

$reply = optional_param('reply', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);
$b = optional_param('b', 0, PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);
$delete = optional_param('delete', 0, PARAM_INT);
$groupid = optional_param('groupid', 0, PARAM_INT);
$name = optional_param('name', '', PARAM_CLEAN);
$confirm = optional_param('confirm', 0, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);
$isnotice = optional_param('isnotice', 0, PARAM_INT);
$isprivate = optional_param('isprivate', 0, PARAM_INT);
$ref = optional_param('ref', 0, PARAM_INT);
$step = optional_param('step', 0, PARAM_INT);
$lev = optional_param('lev', 0, PARAM_INT);
$contents = optional_param('contents', '', PARAM_CLEAN);
$title = optional_param('title', '', PARAM_CLEAN);
$confirmed = optional_param('confirmed', 0, PARAM_INT);
$mode = optional_param('mode', "", PARAM_CLEAN);
$boardoptions = optional_param('boardoptions', 0, PARAM_INT);
$contentId = optional_param('contentId', 0, PARAM_INT);
$parentId = optional_param('parentId', 0, PARAM_INT);
$num_notice = optional_param('num_notice', 0, PARAM_INT);
$itemid = optional_param('itemid', 0, PARAM_INT);
$isopen = optional_param('isopen', 0, PARAM_INT);
$moduleid = optional_param('moduleid', 0, PARAM_INT);
$boardform = optional_param('boardform', TEAMBOARD_BOARDFORM_THREAD, PARAM_INT);

$category_field = 0;

if ($b) {

    if (!$board = $DB->get_record("teamboard", array("id" => $b))) {
        print_error('invalidboardid', 'teamboard');
    }
    if (!$course = $DB->get_record("course", array("id" => $board->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("teamboard", $board->id, $course->id)) {
        print_error('missingparameter');
    }


    require_course_login($course, true, $cm);
    $strboards = get_string("modulenameplural", "teamboard");
    $strboard = get_string("modulename", "teamboard");
} else {
    print_error('missingparameter');
}

require_login();

$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

$PAGE->set_url('/mod/teamboard/write.php', array(
    'reply' => $reply,
    'b' => $b,
    'edit' => $edit,
    'groupid'=>$groupid,
    'delete' => $delete,
    'confirm' => $confirm,
));
$PAGE->set_context($context);
$PAGE->set_heading($course->fullname);
if ($board->course == SITEID) {
    $PAGE->set_pagelayout('general');
    $PAGE->navbar->ignore_active();
    $PAGE->navbar->add(get_string('study_reference_room', 'theme_dgist'), new moodle_url('mod/teamboard/content_list.php', array('b' => $CFG->NOTICEID)));
    $PAGE->navbar->add(get_string($board->name, "theme_dgist"));
    $PAGE->set_title(format_string(get_string($board->name, "theme_dgist")));
    $PAGE->set_heading(format_string(get_string($board->name, "theme_dgist")));

    $CFG->page_icon = '/theme/dgist/pix/images/support_icon.png';
    $CFG->page_description = get_string('support_' . $board->name . '_content', 'theme_dgist');
}
if($mode == 'isopen'){
    $content = new stdClass();
    $content->id = $contentId;
    $content->isopen = $isopen;
    $newid = $DB->update_record('teamboard_groups', $content);
    redirect("view.php?id=$moduleid");
}
echo $OUTPUT->header();
echo html_writer::tag('h2',$board->name ,array('class' => 'board-title'));

if ($confirmed == 0) {
    if ($mode == "edit" && $contentId > 0) {
        $data = $DB->get_record('teamboard_contents', array('id' => $contentId));

        $data = trusttext_pre_edit($data, 'contents', $context);

        if (!has_capability('mod/teamboard:edit', $context) && ($data->userid != $USER->id)) {
            notice('cannoteditpost', 'teamboard');
        }

        $title = $data->title;
        $contents = $data->contents;

        $category_field = $data->category;
    } else if ($mode == "reply") {
        if (!has_capability('mod/teamboard:reply', $context) || $board->allowreply == 0) {
            notice('cannotreplypost', 'teamboard');
        }

        $parentData = $DB->get_record('teamboard_contents', array('id' => $contentId));
        $title = "Re:" . $parentData->title;
    } else {
        if (!has_capability('mod/teamboard:write', $context)) {
            notice('cannotaddpost', 'teamboard');
        }
    }
} 

$postid = empty($data->id) ? null : $data->id;
$editor_option = teamboard_editor_options($context, $postid);

$draftid_editor = file_get_submitted_draft_itemid('contents');
$contents = file_prepare_draft_area($draftid_editor, $context->id, 'mod_teamboard', 'contents', $postid, $editor_option, $contents);

editors_head_setup();

$editor = editors_get_preferred_editor(FORMAT_HTML);
$editor->id = "editor_contents";
$args = new stdClass();
$args->accepted_types = array('web_image');
$args->return_types = (FILE_INTERNAL | FILE_EXTERNAL);
$args->context = $context;
$args->env = 'filepicker';

// advimage plugin
$image_options = initialise_filepicker($args);
$image_options->context = $context;
$image_options->client_id = uniqid();
$image_options->maxbytes = $editor_option['maxbytes'];
$image_options->env = 'editor';
$image_options->itemid = $draftitemid;

$fpoptions = array();
$fpoptions['image'] = $image_options;

$editor->use_editor($editor->id, $editor_option, $fpoptions);


// form data
$draftitemid = file_get_submitted_draft_itemid('attachments');
file_prepare_draft_area($draftitemid, $context->id, 'mod_teamboard', 'attachment', empty($contentId) ? null : $contentId, teamboard_write_form::attachment_options($board));


$url = new moodle_url($CFG->wwwroot . '/mod/teamboard/write.php?');

$editor_options = array('noclean' => true, 'subdirs' => true, 'maxfiles' => $board->maxattachments, 'maxbytes' => $board->maxbytes, 'context' => $context);

$mformdata = array('options' => $editor_options,
    'confirmed' => 1,
    'board' => $board,
    'itemid' => $draftid_editor,
    'boardform' => $boardform,
    'context' => $context,
    'type' => $board->type,
    'mode' => $mode);

if ($mode == "edit") {
    $mformdata['lev'] = $data->lev;
    $mformdata['isnotice'] = $data->isnotice;
    $mformdata['isprivate'] = $data->isprivate;
}
if ($mode == 'reply') {
    $mformdata['isprivate'] = $parentData->isprivate;
}
$mform = new teamboard_write_form($url, $mformdata);
if ($mform->is_cancelled()) {
    redirect("content_list.php?id=$cm->id&groupid=$groupid&b=$board->id&boardform=$boardform");
} else if ($fromform = $mform->get_data()) {
    if ($mode == "edit") {
        $data = $DB->get_record('teamboard_contents', array('id' => $contentId));

        if (!has_capability('mod/teamboard:edit', $context) && $USER->id != $data->userid) {
            notice('cannoteditpost', 'teamboard');
        }

        $newdata = new object();
        $newdata->course = $course->id;
        $newdata->board = $b;

        $newdata->title = $title;
        $newdata->contents = $contents['text'];
        $newdata->itemid = $itemid;

        $newdata->id = $contentId;
        $newdata->category = $category;

        $newdata->isnotice = $isnotice;
        $newdata->isprivate = $isprivate;

        $newdata->timemodified = time();


        $newdata->contents = file_save_draft_area_files($newdata->itemid, $context->id, 'mod_teamboard', 'contents', $newdata->id, teamboard_editor_options($context, null), $newdata->contents);
        $DB->set_field('teamboard_contents', 'contents', $newdata->contents, array('id' => $newdata->id));
        $DB->set_field('teamboard_contents', 'isprivate', $newdata->isprivate, array('ref' => $data->ref));
       
        $DB->update_record('teamboard_contents', $newdata);

        

        $draftitemid = file_get_submitted_draft_itemid('attachments');
        file_save_draft_area_files($draftitemid, $context->id, 'mod_teamboard', 'attachment', $newdata->id);
     

        echo '<script language="javascript">';
        echo 'document.location.href="' . $CFG->wwwroot . '/mod/teamboard/content.php?id='.$cm->id.'&contentId=' . $contentId . '&b=' . $b . '&groupid='.$groupid.'&boardform=' . $boardform . '";';
        echo '</script>';
        die();
    } else if ($mode == "reply") {
        if (!has_capability('mod/teamboard:reply', $context) || $board->allowreply == 0) {
            notice('cannotreplypost', 'teamboard');
        }

        $updatedata = new object();

        $DB->execute("update {teamboard_contents} set step = step + 1 where board = ? and ref = ? and step > ?", array($board->id, $ref, $step));

        $newdata = new object();

        $newdata->course = $course->id;
        $newdata->board = $b;
        $newdata->userid = $USER->id;

        $newdata->title = $title;
        $newdata->category = $category;
        $newdata->contents = $contents['text'];
        $newdata->groupid = $groupid;

        $parentData = $DB->get_record('teamboard_contents', array('id' => $ref), 'isprivate');
        $newdata->isprivate = $parentData->isprivate;
        $newdata->isnotice = 0;

        $newdata->ref = $ref;
        $newdata->step = $step + 1;
        $newdata->lev = $lev + 1;

        $newdata->viewcnt = 0;
        $newdata->timecreated = time();
        $newdata->timemodified = time();

        $newdata->itemid = $contents['itemid'];
        if ($newid = $DB->insert_record('teamboard_contents', $newdata)) {
            $newdata->contents = file_save_draft_area_files($newdata->itemid, $context->id, 'mod_teamboard', 'contents', $newid, teamboard_editor_options($context, null), $newdata->contents);
            $DB->set_field('teamboard_contents', 'contents', $newdata->contents, array('id' => $newid));

            $draftitemid = file_get_submitted_draft_itemid('attachments');
            file_save_draft_area_files($draftitemid, $context->id, 'mod_teamboard', 'attachment', $newid);
        }
        echo '<script language="javascript">';
        echo 'document.location.href="' . $CFG->wwwroot . '/mod/teamboard/content.php?id='.$cm->id.'&contentId=' . $newid . '&b=' . $b . '&groupid='.$groupid.'&boardform=' . $boardform . '";';
        echo '</script>';
        die();
    } else {
        if (!has_capability('mod/teamboard:write', $context)) {
            notice('cannotaddpost', 'teamboard');
        }

        $newdata = new object();

        if (!empty($title) && confirm_sesskey(sesskey())) {
            $newdata->course = $course->id;
            $newdata->board = $b;
            $newdata->userid = $USER->id;
            $newdata->title = $title;
            $newdata->category = $category;
            $newdata->contents = $contents['text'];

            $newdata->itemid = $contents['itemid'];

            $newdata->ref = 0;
            $newdata->step = 0;
            $newdata->lev = 0;
            $newdata->isnotice = $isnotice;
            $newdata->groupid = $groupid;
            $newdata->isprivate = $isprivate;

            $newdata->viewcnt = 0;

            $newdata->timecreated = time();
            $newdata->timemodified = time();

            if ($newid = $DB->insert_record('teamboard_contents', $newdata)) {
                $newdata->id = $newid;
                $DB->set_field_select('teamboard_contents', "ref", $newid, "id=$newid");

                $newdata->contents = file_save_draft_area_files($newdata->itemid, $context->id, 'mod_teamboard', 'contents', $newdata->id, teamboard_editor_options($context, null), $newdata->contents);
                $DB->set_field('teamboard_contents', 'contents', $newdata->contents, array('id' => $newdata->id));
            }
        }

        $draftitemid = file_get_submitted_draft_itemid('attachments');
        file_save_draft_area_files($draftitemid, $context->id, 'mod_teamboard', 'attachment', $newid);

        echo '<script language="javascript">';
        echo 'document.location.href="' . $CFG->wwwroot . '/mod/teamboard/content_list.php?b='.$b.'&groupid='.$groupid.'&boardform='.$boardform.'";';
        echo '</script>';
        die();
    }
    redirect("content_list.php?b=$board->id&groupid=$groupid");
}
$form_data = array(
    'title' => $title,
    'boardform' => $boardform,
    'groupid' => $groupid,
    'contents' => array(
        'text' => $contents,
        'format' => 1,
        'itemid' => $draftid_editor,
        ));

if ($board->allowsecret == 1 && $mode == "edit") {
    $form_data['isprivate'] = $data->isprivate;
}

if ($mode == "edit") {
    $form_data['attachments'] = $draftitemid;
    $content = $DB->get_record('teamboard_contents', array('id' => $contentId));
    $form_data['lev'] = $data->lev;
    $form_data['contentId'] = $contentId;
    $form_data['mode'] = 'edit';
}
if ($mode == "reply") {
    $form_data['contentId'] = $contentId;
    $form_data['ref'] = $parentData->ref;
    $form_data['step'] = $parentData->step;
    $form_data['lev'] = $parentData->lev;
    $form_data['mode'] = 'reply';
}

$mform->set_data($form_data);

$mform->display();

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_teamboard', 'attachment', $data->id, 'id', false);
if ($files) {
    ?>
    <script>
        $("input[name=view_filemanager]").attr("checked", true);
        $("#fgroup_id_filemanager").show();
    </script>
    <?php

}
?>
    <script>
    $(window).load(function () {
        if($("input[name=view_filemanager]").prop("checked")) {
            $("#fgroup_id_filemanager").show();
        } else {
            $("#fgroup_id_filemanager").hide();
        }
    });
    $("input[name=view_filemanager]").click(function () {
        if ($("input[name=view_filemanager]").prop("checked")) {
            $("#fgroup_id_filemanager").show();
        } else {
            $("#fgroup_id_filemanager").hide();
        }
    });
</script>
 <?php
echo $OUTPUT->footer();

