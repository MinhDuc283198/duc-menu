<?php


if (!defined('MOODLE_INTERNAL')) {
	die('Direct access to this script is forbidden.');   
}

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_teamboard_mod_form extends moodleform_mod {
	
	
	function definition() {

		global $CFG, $COURSE, $DB;
                $mform = &$this->_form;
		
		$mform->addElement('header', 'general', get_string('general', 'form'));
		

                $mform->addElement('text', 'name', get_string('boardname', 'teamboard'), array('size'=>'64'));
                if (!empty($CFG->formatstringstriptags)) {
                    $mform->setType('name', PARAM_TEXT);
                } else {
                     $mform->setType('name', PARAM_CLEANHTML);
                }
                $mform->addRule('name', null, 'required', null, 'client');
                $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
                
		
                $this->add_intro_editor(true, get_string('boardintro', 'mod_teamboard'));
              
                $mform->addElement('hidden', 'forcesubscribe', TEAMBOARD_CHOOSESUBSCRIBE);
                $mform->setType('forcesubscribe', PARAM_INT);

                $mform->addElement('hidden', 'trackingtype', TEAMBOARD_TRACKING_OFF);
                $mform->setType('trackingtype', PARAM_INT);
                
                $update = optional_param('update',0 , PARAM_INT);
                $checked = 0;
                
                if(!empty($update)) {
                    $sql = " select jb.maxbytes from {course_modules} cm
                             join {teamboard} jb on jb.id = cm.instance
                             where cm.id = :id ";
                    $param = array("id" => $update);
                    $max = $DB->get_record_sql($sql, $param);
                    
                }
                
                $mform->addElement('hidden','setmaxbyte', get_string('massfile','teamboard'));
                $mform->setDefault('setmaxbyte', 1);
                
                $mform->addElement('hidden', 'maxattachments', $CFG->board_maxattachments);
                $mform->setType('maxattachments', PARAM_INT);
		
		$this->standard_grading_coursemodule_elements();
                
		$this->standard_coursemodule_elements();
		
		$this->add_action_buttons();
	}
	
	
	function definition_after_data() {
		parent::definition_after_data();
	}

	function data_preprocessing(&$default_values) {
		parent::data_preprocessing($default_values);
	}
        
        function get_data(){
            $data = parent::get_data();
            if (!$data) {
                return false;
            }
            switch($data->type){
                case 1: //공지사항
                    $data->allownotice = 1;
                    $data->allowreply = 0;
                    $data->allowcomment = 0;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 0;
                    break;
                case 2: //질문답변
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 0;
                    $data->allowsecret = 1;
                    $data->allowrecommend = 0;
                    break;
                case 3: //일반게시판
                    $data->allownotice = 1;
                    $data->allowreply = 1;
                    $data->allowcomment = 1;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 1;
                    break;
                case 4: //토론게시판
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 1;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 1;
                    break;
                case 5: //조모임게시판
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 1;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 1;
                    break;
                default:
                    break;
            }
            
            return $data;
            
        }
        
}
?>
