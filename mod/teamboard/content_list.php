<?php
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT);       // Course Module ID
$b = optional_param('b', 0, PARAM_INT);        // Board ID
$groupid = optional_param('groupid', 0, PARAM_INT);        // Moodle Group ID
$boardform = optional_param('boardform', TEAMBOARD_BOARDFORM_THREAD, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);     // which page to show
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수


$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['b'] = $b;
}
if ($page) {
    $params['page'] = $page;
}
$params['groupid'] = $groupid;
$PAGE->set_url('/mod/teamboard/content_list.php', $params);

if ($id) {
    if (!$cm = get_coursemodule_from_id('teamboard', $id)) {
        print_error('invalidcoursemodule');
    }
    if (!$course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }
    if (!$board = $DB->get_record("teamboard", array("id" => $cm->instance))) {
        print_error('invalidboardid', 'teamboard');
    }

    // move require_course_login here to use forced language for course
    // fix for MDL-6926

    require_course_login($course, true, $cm);
} else if ($b) {

    if (!$board = $DB->get_record("teamboard", array("id" => $b))) {
        print_error('invalidboardid', 'teamboard');
    }
    if (!$course = $DB->get_record("course", array("id" => $board->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("teamboard", $board->id, $course->id)) {
        print_error('missingparameter');
    }
    // move require_course_login here to use forced language for course
    // fix for MDL-6926

    require_course_login($course, true, $cm);
} else {
    print_error('missingparameter');
}

$group = $DB->get_record('groups', array('id' => $groupid));

$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

$PAGE->set_context($context);

$PAGE->set_heading($course->fullname);

if (!has_capability('mod/teamboard:view', $context)) {
    require_login();
}

$sql = "select u.id, u.firstname,u.lastname from {groups_members} gu "
        . "join {user} u on u.id = gu.userid "
        . "where gu.groupid = :groupid";

$ingroup = 0;
$team_member = $DB->get_record('groups_members', array('groupid' => $group->id , 'userid'=>$USER->id), 'id');
if($team_member || has_capability('mod/teamboard:viewprivate', $context))$ingroup = 1;

echo $OUTPUT->header();

if ($cm->id) {
    add_to_log($course->id, "teamboard", "view board", "content_list.php?id=$cm->id", "$board->id", $cm->id);
} else {
    add_to_log($course->id, "teamboard", "view board", "content_list.php?id=$cm->id", "$board->id");
}
?>

<div class="tab-table-section">
    <h2 class="mylearning_title"><?php echo $board->name."[".$group->name."]"; ?></h2>
    <br>
    <div class="table-top-area">
        <?php
        if (!empty($id)) {
            $formparam = "id=" . $id;
        } else if (!empty($b)) {
            $formparam = "b=" . $b;
        }
        $formparam .= "&groupid=" . $groupid;
        $boardform_arr = array(TEAMBOARD_BOARDFORM_THREAD => get_string('board_thread', 'mod_teamboard'), TEAMBOARD_BOARDFORM_GENERAL => get_string('board_general', 'mod_teamboard'));
        $boardform_url = new moodle_url($CFG->wwwroot . '/mod/teamboard/content_list.php?' . $formparam);

        echo "<form name='searchForm' class='table-search-option' method='GET' action='" . $CFG->wwwroot . "/mod/teamboard/content_list.php'>";
        echo "<input type='hidden' name='id' value='" . $cm->id . "' />";



        $option_arr = array("1" => get_string('title', 'teamboard'),
            "2" => get_string('writer', 'teamboard'),
            "3" => get_string('content', 'teamboard'));
        echo html_writer::select($option_arr, 'searchfield', $searchfield, '');
        echo "<input type='text' name='searchvalue' class='board_search_input'  value='" . $searchvalue . "' />";
        echo "<input type='submit' class='board-search' value='" . get_string('search', 'teamboard') . "'/>";
        echo "<input type='hidden' name='boardform' value='" . $boardform . "' />";
        echo "<input type='hidden' name='groupid' value='" . $groupid . "' />";

        echo "</form>";

        //perpage
        $perpage_arr = array();

        for ($num = 10; $num <= 30;) {

            $perpage_arr[$num] = get_string("content:perpage", "local_board", $num);
            $num += 10;
        }
        $perpage_arr[50] = get_string("content:perpage", "local_board", 50);
        $perpage_url = new moodle_url($CFG->wwwroot . '/mod/teamboard/content_list.php?boardform=' . $boardform . '&id=' . $cm->id . '&groupid=' . $groupid);
        $select_perpage = new single_select($perpage_url, 'perpage', $perpage_arr, $perpage, null);
        $select_perpage->class = 'perpage fl';
        ?>
        <div class="table-top-left">
            <?php
            echo "<form method='get'>";
            echo "<input type='hidden' name='id' value='" . $cm->id . "' />";
            echo "<input type='hidden' name='boardform' value='" . $boardform . "' />";
            echo "<input type='hidden' name='groupid' value='" . $groupid . "' />";
            echo "<input type='hidden' name='searchvalue' value='" . $searchvalue . "' />";
            echo "<input type='hidden' name='searchfield' value='" . $searchfield . "' />";
            echo '<div class="board-style">';
            echo ($boardform == TEAMBOARD_BOARDFORM_THREAD) ? "<label class='board-style-thread'><input type='radio' name='boardform' value='" . TEAMBOARD_BOARDFORM_THREAD . "' checked>" . get_string('bstyle_thred', 'jinoforum') . "</label>" : "<label class='board-style-thread'><input type='radio' name='boardform' value='" . TEAMBOARD_BOARDFORM_THREAD . "' onclick='this.form.submit();'>" . get_string('bstyle_thred', 'jinoforum') . "</label>";
            echo ($boardform == TEAMBOARD_BOARDFORM_GENERAL) ? "<label class='board-style-list'><input type='radio' name='boardform' value='" . TEAMBOARD_BOARDFORM_GENERAL . "' checked>" . get_string('bstyle_list', 'jinoforum') . "</label>" : "<label class='board-style-list'><input type='radio' name='boardform' value='" . TEAMBOARD_BOARDFORM_GENERAL . "' onclick='this.form.submit();'>" . get_string('bstyle_list', 'jinoforum') . "</label>";
            echo '</div>';
            echo '</form>';
            if ($boardform === TEAMBOARD_BOARDFORM_GENERAL) {
                echo $OUTPUT->render($select_perpage);
            }
            ?>
        </div>
        <div class="table-top-right">
            <?php
                echo "<input type='button' style='cursor:pointer;' class='listmenu_list' value='" . get_string('list', 'teamboard') . "' onclick='location.href =\"$CFG->wwwroot/mod/teamboard/view.php?id=" . $cm->id ."\"' />";
            if (has_capability('mod/teamboard:write', $context) && $ingroup == 1) {
                echo "<input type='button' style='cursor:pointer;' class='listmenu_list' value='" . get_string('newpost', 'teamboard') . "' onclick='location.href =\"$CFG->wwwroot/mod/teamboard/write.php?id=".$cm->id."&b=" . $cm->instance . "&groupid=" . $groupid . "&boardform=" . $boardform . "\"' />";
            }
            ?>
        </div>
    </div>
</div>

<?php
if ($boardform === TEAMBOARD_BOARDFORM_THREAD) {
    $contents = teamboard_thread_contents($cm->instance, 0, TEAMBOARD_THREAD_DEFAULT, $searchfield, $searchvalue, "st.ref DESC, st.step asc", $groupid);
    $contentsnotice = teamboard_thread_contents_notice($cm->instance, 0, TEAMBOARD_THREAD_DEFAULT, "", "", "st.ref DESC, st.step asc", $groupid);
    $next = teamboard_thread_contents($cm->instance, 1, TEAMBOARD_THREAD_DEFAULT, $searchfield, $searchvalue, "st.ref DESC, st.step asc", $groupid);
    $next_count = count($next);
    ?>

    <div class="thread-style" id="thread">
        <ul class="thread-style-lists">
            <?php
            foreach ($contentsnotice as $content) {
                $contentid = $content->id;
                $content->id = $content->urid;
                $userdate = userdate($content->timecreated);
                $postcontent = format_text($content->contents, 1, null);

                if (!empty($content->itemid)) {
                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }
                
                if ($content->lev) {
                    if ($content->lev <= TEAMBOARD_INDENT_DEFAULT) {
                        $step = $content->lev;
                        $date_left_len = $step * 30;
                    } else {
                        $step = TEAMBOARD_INDENT_DEFAULT;
                        $date_left_len = $step * 30;
                    }
                     switch($content->lev){
                        case 1 : $calcwidth = 30; break; 
                        case 2 : $calcwidth = 60; break;
                        case 3 : $calcwidth = 90; break;
                        case 4 : $calcwidth = 120; break;
                        default : $calcwidth = 120; break;
                    }      
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - '.$calcwidth.'px) !important;"';
                    $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_teamboard') . '" alt="" /> ';
                } else {
                    $date_left = 'style="width:calc(100% - 0px) !important;"';
                    $step_icon = '';
                }

                echo "<li class='isnotice' ". $date_left .">";
                echo '<div class="thread-left">' . $OUTPUT->user_picture($content) . '</div>';
                echo "<div class='thread-content'><h1 class='thread-post-title'>" . $step_icon;


                $parentid = 0;
                if ($content->lev != 0) {
                    $parent = $DB->get_record('teamboard_contents', array('id' => $content->ref));
                    $parentid = $parent->id;
                }
                echo "<a href='" . $CFG->wwwroot . "/mod/teamboard/content.php?contentId=" . $contentid . "&id=".$id."&b=" . $cm->instance . "&groupid=" . $content->groupid . "&boardform=" . $boardform . "'>" . $content->title . "</a>";
                echo '&nbsp;'.$filecheck;
                $by = new stdClass();
                $by->name = fullname($content);
                $by->date = userdate($content->timemodified);

                echo '</h1><span class="thread-post-meta">' . get_string('bynameondate', 'jinoforum', $by) . '</span>';
                echo '</div>';
                echo "<div class='thread-right'>";
                if ($board->allowrecommend)
                    echo "<div class='count-block recomm-count'>" . $content->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_teamboard') . "</span></div>";
                if ($board->allowcomment)
                    echo "<div class='count-block reply_count'>" . $content->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_teamboard') . "</span></div>";
                echo "<div class='count-block view_count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_teamboard') . "</span></div>";
                echo "</div>";
                echo "</li>";
            }

            if (empty($contents)) {
                echo "<li style='width:100%; line-height:15px; text-align:center;'>" . get_string('nocontent', 'mod_teamboard') . "</li>";
            }
            foreach ($contents as $content) { 
                $contentid = $content->id;
                $content->id = $content->urid;
                $userdate = userdate($content->timecreated);
                $postcontent = format_text($content->contents, 1, null);

                if (!empty($content->itemid)) { 
                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }

                if ($content->lev) {
                    if ($content->lev <= TEAMBOARD_INDENT_DEFAULT) {
                        $step = $content->lev;
                        $date_left_len = $step * 30;
                    } else {
                        $step = TEAMBOARD_INDENT_DEFAULT;
                        $date_left_len = $step * 30;
                    }
                     switch($content->lev){
                        case 1 : $calcwidth = 30; break; 
                        case 2 : $calcwidth = 60; break;
                        case 3 : $calcwidth = 90; break;
                        case 4 : $calcwidth = 120; break;
                        default : $calcwidth = 120; break;
                    }      
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - '.$calcwidth.'px) !important;"';
                    $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_teamboard') . '" alt="" /> ';
                } else {
                    $date_left = 'style="width:calc(100% - 0px) !important;"';
                    $step_icon = '';
                }
                echo "<li ". $date_left .">";
                echo '<div class="thread-left">' . $OUTPUT->user_picture($content) . '</div>';
                echo "<div class='thread-content'><h1 class='thread-post-title'>" . $step_icon;
                
                $parentid = 0;
                if ($content->lev != 0) {
                    $parent = $DB->get_record('teamboard_contents', array('id' => $content->ref));
                    $parentid = $parent->userid;
                }

                echo "<a href='" . $CFG->wwwroot . "/mod/teamboard/content.php?contentId=" . $contentid . "&id=".$id."&b=" . $cm->instance . "&groupid=" . $groupid . "&boardform=" . $boardform . "'>" . $content->title . "</a>";
                echo '&nbsp;'.$filecheck;
                $by = new stdClass();
                $by->name = fullname($content);
                $by->date = userdate($content->timemodified);
                echo '</h1><span class="thread-post-meta">' . get_string('bynameondate', 'jinoforum', $by) . '</span>';
                echo "<p class='thread-post-content'>" . mb_strimwidth(strip_tags($postcontent), 0, 630,'...', 'utf-8').  "</p>";
                echo '</div>';
                echo '<div class="thread-right">';
                if ($board->allowrecommend)
                    echo "<div class='count-block recomm_count'>" . $content->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_teamboard') . "</span></div>";
                if ($board->allowcomment)
                    echo "<div class='count-block reply_count'>" . $content->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_teamboard') . "</span></div>";
                echo "<div class='count-block view_count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_teamboard') . "</span></div>";

                echo '</div>';
                echo "</li>";
            }
            ?>

        </ul>    
    </div>

    <?php
    if ($next_count > 0) {
        ?>
        <div id="next_btn">
            <input type="hidden" id="threadpage"  value="0">
            <input type="button" class="next" value="<?php echo get_string('next:content', 'mod_teamboard'); ?>" onclick="ThreadContent()">
        </div>
        <?php
    }
} else if ($boardform === TEAMBOARD_BOARDFORM_GENERAL) {
        $totalcount = teamboard_get_discussions_count($cm, $searchfield, $searchvalue, true, $groupid);
        teamboard_print_discussions($page - 1, $perpage, $totalcount, $cm, $board, 'st.ref DESC, st.step asc', $searchfield, $searchvalue, true, $boardform, $groupid);
    $page_params = array();
    $page_params['boardform'] = $boardform;
    $page_params['groupid'] = $groupid;
    $page_params['perpage'] = $perpage;
    if ($id) {
        $page_params['id'] = $id;
    } else {
        $page_params['b'] = $b;
    }
    if(!empty($searchfield)) {
        $page_params['searchfield'] = $searchfield;
    }

    if(!empty($searchvalue)) {
        $page_params['searchvalue'] = $searchvalue;
    }
    ?>
    <div class="table-footer-area">
        <?php
        $total_pages = teamboard_get_total_pages($totalcount, $perpage);

        teamboard_get_paging_bar($CFG->wwwroot . '/mod/teamboard/content_list.php', $page_params, $total_pages, $page);
        ?>
    </div>    

    <?php
}
echo $OUTPUT->footer();
?>

<script type="text/javascript">
    function ThreadContent() {
        var page = $('#threadpage').val();

        jQuery.ajax({
            url: "<?php echo $CFG->wwwroot . '/mod/teamboard/thread.ajax.php'; ?>",
            type: "POST",
            async: false,
            data: {
                "page": page,
<?php
if (!empty($searchfield)) {
    echo '"searchfield" : ' . $searchfield . ',';
    echo '"searchvalue" : ' . $searchvalue . ',';
}
?>
                "instance" : <?php echo $cm->instance; ?>,
                "groupid" : <?php echo $groupid; ?>,
           		"boardform" : <?php echo $boardform; ?>,        
            },
                    success: function (data) {
                        $('#next_btn').remove();
                        $('.thread-style-lists').append(data);
                    }
        });
    }
</script>    