<?php

require_once($CFG->dirroot . '/user/selector/lib.php');

define('TEAMBOARD_MODE_FLATOLDEST', 1);
define('TEAMBOARD_MODE_FLATNEWEST', -1);
define('TEAMBOARD_MODE_THREADED', 2);
define('TEAMBOARD_MODE_NESTED', 3);

define('TEAMBOARD_CHOOSESUBSCRIBE', 0);
define('TEAMBOARD_FORCESUBSCRIBE', 1);
define('TEAMBOARD_INITIALSUBSCRIBE', 2);
define('TEAMBOARD_DISALLOWSUBSCRIBE', 3);

define('TEAMBOARD_TRACKING_OFF', 0);
define('TEAMBOARD_TRACKING_OPTIONAL', 1);
define('TEAMBOARD_TRACKING_ON', 2);

define('TEAMBOARD_BOARDFORM_THREAD', 1);
define('TEAMBOARD_BOARDFORM_GENERAL', 2);
define('TEAMBOARD_THREAD_DEFAULT', 20);

define('TEAMBOARD_INDENT_DEFAULT', 4);

function teamboard_supports($feature) {
    switch ($feature) {
        case FEATURE_GROUPS: return true;
        case FEATURE_GROUPINGS: return true;
        case FEATURE_GROUPMEMBERSONLY: return true;
        case FEATURE_MOD_INTRO: return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES: return true; // compl
        case FEATURE_GRADE_HAS_GRADE: return true;
        case FEATURE_GRADE_OUTCOMES: return true;
        case FEATURE_RATE: return true;
        case FEATURE_BACKUP_MOODLE2: return true;
        case FEATURE_SHOW_DESCRIPTION: return true;

        default: return null;
    }
}

function teamboard_add_instance($board) {
    global $CFG, $DB;



    $board->timecreated = time();
    $board->timemodified = time();

    if (empty($board->setmaxbyte)) {
        $board->maxbytes = $CFG->board_normalmaxbytes;
    } else {
        $board->maxbytes = $CFG->board_massmaxbytes;
    }
    $board->allowcomment = 1;
    $board->allowrecommend = 1;
    $board->allownotice = 1;
    $board->allowreply = 1;

    $board->id = $DB->insert_record('teamboard', $board);


    return $board->id;
}

function teamboard_update_instance($board) {

    global $USER, $DB, $CFG;
    $board->timemodified = time();
    $board->id = $board->instance;

    if (empty($board->setmaxbyte)) {
        $board->maxbytes = $CFG->board_normalmaxbytes;
    } else {
        $board->maxbytes = $CFG->board_massmaxbytes;
    }

    $oldboard = $DB->get_record('teamboard', array('id' => $board->id));

    if (!$DB->update_record('teamboard', $board)) {
        print_error('Can not update board');
    }

    return true;
}

function teamboard_delete_instance($id) {

    global $cm, $DB;

    if (!$board = $DB->get_record('teamboard', array('id' => $id))) {
        return false;
    }


    $DB->delete_records('teamboard_comments', array('board' => $id));

    $DB->delete_records('teamboard_contents', array('board' => $id));



    if (!$DB->delete_records('teamboard', array('id' => $id))) {
        return false;
    }

    return true;
}

function teamboard_print_discussions($page, $perpage, $totalcount, $cm, $board = null, $boardsort = "st.ref DESC, st.step asc", $searchfield = "1", $searchvalue = "", $capability = true, $boardform = 1, $groupid) {
    global $CFG, $PAGE, $USER, $OUTPUT, $DB;
    $thid = "";

    $classname = context_helper::get_class_for_level(CONTEXT_MODULE);
    $contexts[$cm->id] = $classname::instance($cm->id);
    $context = $contexts[$cm->id];

    echo '
        <table class="generaltable">
            <thead>
            <tr>
		<th class="col-1"' . $thid . '>' . get_string("no.", 'teamboard') . '</th>
		<th class="col-2"' . $thid . '>' . get_string("title", 'teamboard') . '</th>
                <th class="col-3"' . $thid . '>' . get_string("writer", 'teamboard') . '</th>    
                <th class="col-4"' . $thid . '>' . get_string("date", 'teamboard') . '</th>      
                <th class="col-5"' . $thid . '>' . get_string("viewCount", 'teamboard') . '</th>';
    if ($board->allowcomment) {
        echo ' <th class="col-6"' . $thid . '>' . get_string("reply", 'teamboard') . '</th>';
    }
    if ($board->allowrecommend) {
        echo ' <th class="col-7"' . $thid . '>' . get_string("reCommondCnt", 'teamboard') . '</th>';
    }

    echo '  </tr>
            </thead>
        <tbody>';



    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }
    $list_num = $offset;
    $contentsRS = teamboard_get_discussions($cm, $boardsort, $searchfield, $searchvalue, $page, $perpage, false, $capability, $groupid);
    $num = $totalcount - $offset;
    if (!$contentsRS) {
        $colspan = 5;
        if ($board->allowcomment) {
            $colspan++;
        }
        if ($board->allowrecommend) {
            $colspan++;   
        }

        echo '<tr><td style="line-height:25px;" colspan="'.$colspan.'" align=center>' . get_string("nocontent", 'teamboard') . '</td></tr>';
    } else {
        $rowi = 0;
        $contentsNoticeRs = teamboard_get_discussions($cm, $boardsort, $searchfield, $searchvalue, 0, 0, true, $capability, $groupid);
        foreach ($contentsNoticeRs as $notice) {
            echo "<tr class='isnotice'>
		    <td class='col-1 $thid' align=center>&nbsp;</td>
                    <td class='title  col-2 $thid'>
		        <a href='" . $CFG->wwwroot . "/mod/teamboard/content.php?id=".$cm->id."&boardform=2&contentId=" . $notice->id . "&b=" . $cm->instance . "&groupid=" . $notice->groupid . "&boardform=" . $boardform . "'>" . $notice->title . "</a></td>";
            echo "<td class='col-3 $thid' align=center>" . fullname($notice) . "</td>";
            echo "<td class='col-4 $thid' align=center>" . date('Y-n-d', $notice->timemodified) . "</td>";
            echo "<td class='col-5 $thid' align=center>" . $notice->viewcnt . "</td>";
            if ($board->allowrecommend) {
                echo "<td class='col-6 $thid' align=center>" . $notice->recommendcnt . "</td>";
            }
            if ($board->allowcomment) {
                echo "<td class='col-7 $thid' align=center>" . $notice->commentscount . "</td>";
            }
            echo "</tr>";

            $rowi ++;
        }

        foreach ($contentsRS as $content) {
            $list_num++;
            echo "<tr>
		    <td class='col-1 $thid' align=center>" . $num . "</td>
                    <td class='title col-2 $thid'>
                  ";

            $step_left_len = "";
            $step_left_len = $content->lev * 15;
            $step_left = 'style="padding-left:' . $step_left_len . 'px;"';
            $parentid = "";
            $fs = get_file_storage();
            $files = $fs->get_area_files($context->id, 'mod_teamboard', 'attachment', $content->id, 'id', false);
            $attachments = "";
            if (count($files) > 0) {
                $attachments = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
            }
            if ($content->lev != 0) {
                $parent = $DB->get_record('teamboard_contents', array('id' => $content->ref));
                $parentid = $parent->userid;
            }
            if ($content->isprivate && !has_capability('mod/teamboard:secretmanager', $PAGE->cm->context) && $USER->id != $content->userid && $parentid != $USER->id) {
                echo "<a href='#' onclick='" . 'alert("비밀글 입니다.")' . "' $step_left>" . $content->title . $attachments . " </a>";
            } else {
                echo "<a href='" . $CFG->wwwroot . "/mod/teamboard/content.php?id=".$cm->id."&contentId=" . $content->id . "&b=" . $cm->instance . "&groupid=" . $content->groupid . "&boardform=" . $boardform ."&list_num=".$list_num."&perpage=".$perpage."&searchfield=".$searchfield."&searchvalue=".$searchvalue. "' $step_left>" . $content->title . $attachments . "</a>";
            }
            if ($content->isprivate) {
                echo "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'teamboard') . "' title='" . get_string('isprivate', 'teamboard') . "'>";
            }

            echo "  </td>
                    <td class='col-3 $thid' align=center>" . fullname($content) . "</td>";
            echo "<td class='col-4 $thid' align=center>" . date('Y-n-d', $content->timemodified) . "</td>";
            echo "<td class='col-5 $thid' align=center>" . $content->viewcnt . "</td>";
            if ($board->allowcomment) {
                echo "<td class='col-7 $thid' align=center>" . $content->commentscount . "</td>";
            }
            if ($board->allowrecommend) {
                echo "<td class='col-6 $thid' align=center>" . $content->recommendcnt . "</td>";
            }

            echo "</tr>";
            $num--;
            $rowi ++;
        }
    }
    echo "</tbody></table>";
}

function teamboard_get_discussions_count($cm, $searchfield = "", $searchvalue = "", $capability = true, $groupid) {
    global $CFG, $DB, $USER;

    $params = array();
    $where = "";
    $params[] = $cm->instance;
    if (teamboard_isNullOrEmptyStr($searchvalue)) {
        $sql = " board = ?";
    } else {

        switch ($searchfield) {
            case 1:
                $where = "and title LIKE ?"; //
                $params[] = '%' . $searchvalue . '%';
                break;
            case 2:

                break;
            case 3:
                $where = "and contents LIKE ?";
                $params[] = '%' . $searchvalue . '%';
                break;
        }
    }

    if (!$capability) {
        $where = " and userid = ? ";
        $params[] = $USER->id;
    }
    $sql = " board = ? and groupid= $groupid" . $where;
    return $DB->count_records_select("teamboard_contents", $sql, $params);
}

function teamboard_get_total_search_discussions($boardsort = "jcb.ref DESC, step asc", $searchvalue = "", $page, $perpage) {
    global $CFG, $DB, $USER;

    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $params = array();



    if ($searchvalue) {
        $where = "and title || contents LIKE ?"; //
        $params[] = '%' . $searchvalue . '%';
    }

    $sql = "SELECT jcb.*, u.firstname, u.lastname,c.title ctitle, f.itemid 
                from {teamboard_contents} jcb  
                left join {user} u on jcb.userid = u.id 
                left join (SELECT  itemid FROM {files} WHERE component = 'mod_teamboard' AND filesize != 0) f ON jcb.id = f.itemid 
                where jcb.board in ($CFG->NOTICEID, $CFG->DATAID) order by " . $boardsort;

    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function teamboard_get_discussions($cm, $boardsort = "st.ref DESC, st.step asc", $searchfield = "", $searchvalue = "", $page, $perpage, $isNotice = false, $capability = true, $groupid) {
    global $CFG, $DB, $USER;

    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $context = context_module::instance($cm->id);

    $params = array(
        'contextid' => $context->id,
        'boardid' => $cm->instance);

    $addNoticeSql = "";
    $where = "";
    if ($isNotice) {
        $addNoticeSql = "and isnotice = 1 ";
    }

    if (!$capability) {
        $where = " and jcb.userid = ? ";
        $params[] = $USER->id;
    }

    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.fullname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }

    $sql = "SELECT * FROM 
                (SELECT jcb.*, u.firstname || u.lastname as fullname, u.firstname, u.lastname, (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = jcb.id ) as itemid 
                    from {teamboard_contents} jcb  
                    left join {user} u on jcb.userid = u.id 
                    where jcb.groupid = " . $groupid . ") st
                where board = :boardid " . $addNoticeSql . $where . " order by " . $boardsort;

    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function teamboard_isNullOrEmptyStr($str) {
    if (!isset($str) || $str == NULL || $str == '')
        return true;
    else
        return false;
}

/**
 * 리스트 테이블 밑에 들어가는 페이지 네비게이션을 출력한다.
 * @param string $url
 * @param array $params
 * @param int $total_pages
 * @param int $current_page
 * @param int $max_nav 네비게이션에 표시한 최대 페이지 수
 */
function teamboard_get_paging_bar($url, $params, $total_pages, $current_page, $max_nav = 10) {
    $total_nav_pages = teamboard_get_total_pages($total_pages, $max_nav);
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }

    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = "page=";
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . "?page=";
    }

    echo '<div class="board-breadcrumbs" >';

    if ($current_page > 1) {
        echo '<span class="board-nav-prev"><a class="prev" href="' . $url . ($current_page - 1) . '"><</a></span>';
    } else {
        echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    echo '<ul>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="current"><a href="#">' . $i . '</a></li>';
        } else {
            echo '<li><a href="' . $url . '' . $i . '">' . $i . '</a></li>';
        }
    }
    echo '</ul>';
    if ($current_page < $total_pages) {
        echo '<span class="board-nav-next"><a class="next" href="' . $url . ($current_page + 1) . '">></a></span>';
    } else {
        echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
    }

    echo '</div>';
}

/**
 * 데이터를 표시하기 위한 페이지 수를 계산한다.
 * @param int $rows
 * @param int $limit
 * @return int
 */
function teamboard_get_total_pages($rows, $limit = 10) {
    if ($rows == 0) {
        return 1;
    }

    $total_pages = (int) ($rows / $limit);

    if (($rows % $limit) > 0) {
        $total_pages += 1;
    }

    return $total_pages;
}

function teamboard_get_categories($cm, $b, $boardsort = "orderseq asc, title asc") {
    global $DB;

    $sql = "SELECT * from {teamboard_category} where board = ? order by " . $boardsort;

    return $DB->get_records_sql($sql, array($b));
}

/**
 * Adds module specific settings to the settings block
 *
 * @param settings_navigation $settings The settings navigation object
 * @param navigation_node $teamboardnode The node to add module settings to
 */
function teamboard_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $teamboardnode) {
    global $USER, $PAGE, $CFG, $DB, $OUTPUT;

    $teamboardobject = $DB->get_record("teamboard", array("id" => $PAGE->cm->instance));
    if (empty($PAGE->cm->context)) {
        $PAGE->cm->context = get_context_instance(CONTEXT_MODULE, $PAGE->cm->instance);
    }

    // for some actions you need to be enrolled, beiing admin is not enough sometimes here
    $enrolled = is_enrolled($PAGE->cm->context, $USER, '', false);
    $activeenrolled = is_enrolled($PAGE->cm->context, $USER, '', true);

    $canmanage = has_capability('mod/teamboard:managesubscriptions', $PAGE->cm->context);
    $subscriptionmode = teamboard_get_forcesubscribed($teamboardobject);
    $cansubscribe = ($activeenrolled && $subscriptionmode != TEAMBOARD_FORCESUBSCRIBE && ($subscriptionmode != TEAMBOARD_DISALLOWSUBSCRIBE || $canmanage));

    if ($canmanage) {
        $mode = $teamboardnode->add(get_string('subscriptionmode', 'forum'), null, navigation_node::TYPE_CONTAINER);

        $allowchoice = $mode->add(get_string('subscriptionoptional', 'forum'), new moodle_url('/mod/teamboard/subscribe.php', array('id' => $teamboardobject->id, 'mode' => TEAMBOARD_CHOOSESUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
        $forceforever = $mode->add(get_string("subscriptionforced", "forum"), new moodle_url('/mod/teamboard/subscribe.php', array('id' => $teamboardobject->id, 'mode' => TEAMBOARD_FORCESUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
        $forceinitially = $mode->add(get_string("subscriptionauto", "forum"), new moodle_url('/mod/teamboard/subscribe.php', array('id' => $teamboardobject->id, 'mode' => TEAMBOARD_INITIALSUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
        $disallowchoice = $mode->add(get_string('subscriptiondisabled', 'forum'), new moodle_url('/mod/teamboard/subscribe.php', array('id' => $teamboardobject->id, 'mode' => TEAMBOARD_DISALLOWSUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);

        switch ($subscriptionmode) {
            case TEAMBOARD_CHOOSESUBSCRIBE : // 0
                $allowchoice->action = null;
                $allowchoice->add_class('activesetting');
                break;
            case TEAMBOARD_FORCESUBSCRIBE : // 1
                $forceforever->action = null;
                $forceforever->add_class('activesetting');
                break;
            case TEAMBOARD_INITIALSUBSCRIBE : // 2
                $forceinitially->action = null;
                $forceinitially->add_class('activesetting');
                break;
            case TEAMBOARD_DISALLOWSUBSCRIBE : // 3
                $disallowchoice->action = null;
                $disallowchoice->add_class('activesetting');
                break;
        }
    } else if ($activeenrolled) {

        switch ($subscriptionmode) {
            case TEAMBOARD_CHOOSESUBSCRIBE : // 0
                $notenode = $teamboardnode->add(get_string('subscriptionoptional', 'forum'));
                break;
            case TEAMBOARD_FORCESUBSCRIBE : // 1
                $notenode = $teamboardnode->add(get_string('subscriptionforced', 'forum'));
                break;
            case TEAMBOARD_INITIALSUBSCRIBE : // 2
                $notenode = $teamboardnode->add(get_string('subscriptionauto', 'forum'));
                break;
            case TEAMBOARD_DISALLOWSUBSCRIBE : // 3
                $notenode = $teamboardnode->add(get_string('subscriptiondisabled', 'forum'));
                break;
        }
    }

    if (has_capability('mod/teamboard:viewsubscribers', $PAGE->cm->context)) {
        $url = new moodle_url('/mod/teamboard/subscribers.php', array('id' => $teamboardobject->id));
        $teamboardnode->add(get_string('showsubscribers', 'forum'), $url, navigation_node::TYPE_SETTING);
    }

    /* if ($enrolled && forum_tp_can_track_forums($teamboardobject)) { // keep tracking info for users with suspended enrolments
      if ($teamboardobject->trackingtype != TEAMBOARD_TRACKING_OPTIONAL) {
      //tracking forced on or off in forum settings so dont provide a link here to change it
      //could add unclickable text like for forced subscription but not sure this justifies adding another menu item
      } else {
      if (forum_tp_is_tracked($teamboardobject)) {
      $linktext = get_string('notrackforum', 'forum');
      } else {
      $linktext = get_string('trackforum', 'forum');
      }
      $url = new moodle_url('/mod/forum/settracking.php', array('id'=>$teamboardobject->id));
      $teamboardnode->add($linktext, $url, navigation_node::TYPE_SETTING);
      }
      } */

    if (!isloggedin() && $PAGE->course->id == SITEID) {
        $userid = guest_user()->id;
    } else {
        $userid = $USER->id;
    }

    /* $hascourseaccess = ($PAGE->course->id == SITEID) || can_access_course($PAGE->course, $userid);
      $enablerssfeeds = !empty($CFG->enablerssfeeds) && !empty($CFG->forum_enablerssfeeds);

      if ($enablerssfeeds && $teamboardobject->rsstype && $teamboardobject->rssarticles && $hascourseaccess) {

      if (!function_exists('rss_get_url')) {
      require_once("$CFG->libdir/rsslib.php");
      }

      if ($teamboardobject->rsstype == 1) {
      $string = get_string('rsssubscriberssdiscussions','forum');
      } else {
      $string = get_string('rsssubscriberssposts','forum');
      }

      $url = new moodle_url(rss_get_url($PAGE->cm->context->id, $userid, "mod_forum", $teamboardobject->id));
      $teamboardnode->add($string, $url, settings_navigation::TYPE_SETTING, null, null, new pix_icon('i/rss', ''));
      } */
}

/**
 * @global object
 * @param object $forum
 * @return bool
 */
function teamboard_is_forcesubscribed($teamboard) {
    global $DB;
    if (isset($teamboard->forcesubscribe)) {    // then we use that
        return ($teamboard->forcesubscribe == TEAMBOARD_FORCESUBSCRIBE);
    } else {   // Check the database
        return ($DB->get_field('forum', 'forcesubscribe', array('id' => $teamboard)) == TEAMBOARD_FORCESUBSCRIBE);
    }
}

function teamboard_get_forcesubscribed($teamboard) {
    global $DB;
    if (isset($teamboard->forcesubscribe)) {    // then we use that
        return $teamboard->forcesubscribe;
    } else {   // Check the database
        return $DB->get_field('teamboard', 'forcesubscribe', array('id' => $teamboard));
    }
}

/**
 * @global object
 * @param int $forumid
 * @param mixed $value
 * @return bool
 */
function teamboard_forcesubscribe($teamboardid, $value = 1) {
    global $DB;
    return $DB->set_field("teamboard", "forcesubscribe", $value, array("id" => $teamboardid));
}

/**
 * @global object
 * @global object
 */
function teamboard_set_return() {
    global $CFG, $SESSION;

    if (!isset($SESSION->fromteamboard)) {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = "";
        }
        // If the referer is NOT a login screen then save it.
        if (!strncasecmp("$CFG->wwwroot/login", $referer, 300)) {
            $SESSION->fromteamboard = $_SERVER["HTTP_REFERER"];
        }
    }
}

/**
 * Abstract class used by teamboard subscriber selection controls
 * @package mod-teamboard
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class teamboard_subscriber_selector_base extends user_selector_base {

    /**
     * The id of the teamboard this selector is being used for
     * @var int
     */
    protected $teamboardid = null;

    /**
     * The context of the teamboard this selector is being used for
     * @var object
     */
    protected $context = null;

    /**
     * The id of the current group
     * @var int
     */
    protected $currentgroup = null;

    /**
     * Constructor method
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options) {
        $options['accesscontext'] = $options['context'];
        parent::__construct($name, $options);
        if (isset($options['context'])) {
            $this->context = $options['context'];
        }
        if (isset($options['currentgroup'])) {
            $this->currentgroup = $options['currentgroup'];
        }
        if (isset($options['teamboardid'])) {
            $this->teamboardid = $options['teamboardid'];
        }
    }

    /**
     * Returns an array of options to seralise and store for searches
     *
     * @return array
     */
    protected function get_options() {
        global $CFG;
        $options = parent::get_options();
        $options['file'] = substr(__FILE__, strlen($CFG->dirroot . '/'));
        $options['context'] = $this->context;
        $options['currentgroup'] = $this->currentgroup;
        $options['teamboardid'] = $this->teamboardid;
        return $options;
    }

}

/**
 * A user selector control for potential subscribers to the selected teamboard
 * @package mod-teamboard
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class teamboard_potential_subscriber_selector extends teamboard_subscriber_selector_base {

    /**
     * If set to true EVERYONE in this course is force subscribed to this teamboard
     * @var bool
     */
    protected $forcesubscribed = false;

    /**
     * Can be used to store existing subscribers so that they can be removed from
     * the potential subscribers list
     */
    protected $existingsubscribers = array();

    /**
     * Constructor method
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options) {
        parent::__construct($name, $options);
        if (isset($options['forcesubscribed'])) {
            $this->forcesubscribed = true;
        }
    }

    /**
     * Returns an arary of options for this control
     * @return array
     */
    protected function get_options() {
        $options = parent::get_options();
        if ($this->forcesubscribed === true) {
            $options['forcesubscribed'] = 1;
        }
        return $options;
    }

    /**
     * Finds all potential users
     *
     * Potential users are determined by checking for users with a capability
     * determined in {@see teamboard_get_potential_subscribers()}
     *
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;

        $availableusers = teamboard_get_potential_subscribers($this->context, $this->currentgroup, $this->required_fields_sql('u'), 'u.firstname ASC, u.lastname ASC');

        if (empty($availableusers)) {
            $availableusers = array();
        } else if ($search) {
            $search = strtolower($search);
            foreach ($availableusers as $key => $user) {
                if (stripos($user->firstname, $search) === false && stripos($user->lastname, $search) === false) {
                    unset($availableusers[$key]);
                }
            }
        }

        // Unset any existing subscribers
        if (count($this->existingsubscribers) > 0 && !$this->forcesubscribed) {
            foreach ($this->existingsubscribers as $group) {
                foreach ($group as $user) {
                    if (array_key_exists($user->id, $availableusers)) {
                        unset($availableusers[$user->id]);
                    }
                }
            }
        }

        if ($this->forcesubscribed) {
            return array(get_string("existingsubscribers", 'forum') => $availableusers);
        } else {
            return array(get_string("potentialsubscribers", 'forum') => $availableusers);
        }
    }

    /**
     * Sets the existing subscribers
     * @param array $users
     */
    public function set_existing_subscribers(array $users) {
        $this->existingsubscribers = $users;
    }

    /**
     * Sets this teamboard as force subscribed or not
     */
    public function set_force_subscribed($setting = true) {
        $this->forcesubscribed = true;
    }

}

/**
 * @global object
 * @param string $default
 * @return string
 */
function teamboard_go_back_to($default) {
    global $SESSION;

    if (!empty($SESSION->fromdiscussion)) {
        $returnto = $SESSION->fromdiscussion;
        unset($SESSION->fromdiscussion);
        return $returnto;
    } else {
        return $default;
    }
}

/**
 * @global object
 * @global object
 * @global object
 * @staticvar array $cache
 * @param object $forum
 * @param object $cm
 * @param object $course
 * @return mixed
 */
function teamboard_count_discussions($teamboard, $cm, $course) {
    global $CFG, $DB, $USER;

    static $cache = array();

    $now = round(time(), -2); // db cache friendliness

    $params = array($course->id);

    if (!isset($cache[$course->id])) {
        /* if (!empty($CFG->forum_enabletimedposts)) {
          $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
          $params[] = $now;
          $params[] = $now;
          } else {
          $timedsql = "";
          } */
        $timedsql = "";

        $sql = "SELECT f.id, COUNT(f.id) as dcount
                  FROM {teamboard} f
                       JOIN {teamboard_contents} d ON d.board = f.id
                 WHERE f.course = ?
                       $timedsql
              GROUP BY f.id";

        if ($counts = $DB->get_records_sql($sql, $params)) {
            foreach ($counts as $count) {
                $counts[$count->id] = $count->dcount;
            }
            $cache[$course->id] = $counts;
        } else {
            $cache[$course->id] = array();
        }
    }

    if (empty($cache[$course->id][$teamboard->id])) {
        return 0;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);

    if ($groupmode != SEPARATEGROUPS) {
        return $cache[$course->id][$teamboard->id];
    }

    if (has_capability('moodle/site:accessallgroups', get_context_instance(CONTEXT_MODULE, $cm->id))) {
        return $cache[$course->id][$teamboard->id];
    }

    require_once($CFG->dirroot . '/course/lib.php');

    $modinfo = & get_fast_modinfo($course);
    if (is_null($modinfo->groups)) {
        $modinfo->groups = groups_get_user_groups($course->id, $USER->id);
    }

    if (array_key_exists($cm->groupingid, $modinfo->groups)) {
        $mygroups = $modinfo->groups[$cm->groupingid];
    } else {
        $mygroups = false; // Will be set below
    }

    // add all groups posts
    if (empty($mygroups)) {
        $mygroups = array(-1 => -1);
    } else {
        $mygroups[-1] = -1;
    }

   // list($mygroups_sql, $params) = $DB->get_in_or_equal($mygroups);
    $params = array($teamboard->id);

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < $now AND (d.timeend = 0 OR d.timeend > $now)";
      $params[] = $now;
      $params[] = $now;
      } else {
      $timedsql = "";
      } */

    $sql = "SELECT COUNT(d.id)
              FROM {teamboard_contents} d
             WHERE d.board = ?
                   $timedsql";

    return $DB->get_field_sql($sql, $params);
}

/**
 * Returns the count of records for the provided user and forum and [optionally] group.
 *
 * @global object
 * @global object
 * @global object
 * @param object $cm
 * @param object $course
 * @return int
 */
function teamboard_tp_count_teamboard_unread_posts($cm, $course) {
    global $CFG, $USER, $DB;

    static $readcache = array();

    $teamboardid = $cm->instance;

    if (!isset($readcache[$course->id])) {
        $readcache[$course->id] = array();
        $counts = teamboard_tp_get_course_unread_posts($USER->id, $course->id);
        if ($counts) {
            foreach ($counts as $count) {
                $readcache[$course->id][$count->id] = $count->unread;
            }
        }
    }

    if (empty($readcache[$course->id][$teamboardid])) {
        // no need to check group mode ;-)
        return 0;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);

    if ($groupmode != SEPARATEGROUPS) {
        return $readcache[$course->id][$teamboardid];
    }

    if (has_capability('moodle/site:accessallgroups', get_context_instance(CONTEXT_MODULE, $cm->id))) {
        return $readcache[$course->id][$teamboardid];
    }

    require_once($CFG->dirroot . '/course/lib.php');

    $modinfo = & get_fast_modinfo($course);
    if (is_null($modinfo->groups)) {
        $modinfo->groups = groups_get_user_groups($course->id, $USER->id);
    }

    $mygroups = $modinfo->groups[$cm->groupingid];

    // add all groups posts
    if (empty($mygroups)) {
        $mygroups = array(-1 => -1);
    } else {
        $mygroups[-1] = -1;
    }

    list ($groups_sql, $groups_params) = $DB->get_in_or_equal($mygroups);

    $now = round(time(), -2); // db cache friendliness
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 60);
    $params = array($USER->id, $teamboardid, $cutoffdate);

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
      $params[] = $now;
      $params[] = $now;
      } else {
      $timedsql = "";
      } */
    $timedsql = "";

    $params = array_merge($params, $groups_params);

    $sql = "SELECT COUNT(p.id)
              FROM {teamboard_contents} jc
                   LEFT JOIN {forum_read} r   ON (r.jinotechcontentsid = jc.id AND r.userid = ?)
             WHERE jc.board = ?
                   AND jc.modified >= ? AND r.id is NULL
                   $timedsql
                   AND d.groupid $groups_sql";


    /*    $sql = "SELECT COUNT(p.id)
      FROM {forum_posts} p
      JOIN {forum_discussions} d ON p.discussion = d.id
      LEFT JOIN {forum_read} r   ON (r.postid = p.id AND r.userid = ?)
      WHERE d.forum = ?
      AND p.modified >= ? AND r.id is NULL
      $timedsql
      AND d.groupid $groups_sql"; */

    return $DB->get_field_sql($sql, $params);
}

/**
 * Returns the count of records for the provided user and course.
 * Please note that group access is ignored!
 *
 * @global object
 * @global object
 * @param int $userid
 * @param int $courseid
 * @return array
 */
function teamboard_tp_get_course_unread_posts($userid, $courseid) {
    global $CFG, $DB;
    $now = round(time(), -2); // db cache friendliness
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 60);
    $params = array($userid, $userid, $courseid, $cutoffdate);

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
      $params[] = $now;
      $params[] = $now;
      } else {
      $timedsql = "";
      } */
    $timedsql = "";

    $sql = "SELECT f.id, COUNT(p.id) AS unread
              FROM {teamboard_contents} p
                   JOIN {teamboard} f                   ON f.id = p.board
                   JOIN {course} c                  ON c.id = f.course
                   LEFT JOIN {teamboard_read} r         ON (r.contentsid = p.id AND r.userid = ?)
                   
             WHERE f.course = ?
                   AND p.timemodified >= ? AND r.id is NULL
                   AND (f.trackingtype = " . TEAMBOARD_TRACKING_ON . "
                        OR (f.trackingtype = " . TEAMBOARD_TRACKING_OPTIONAL . " ))
                   $timedsql
          GROUP BY f.id";

    if ($return = $DB->get_records_sql($sql, $params)) {
        return $return;
    }

    return array();
}

function teamboard_user_unenrolled($cp) {
    global $DB;

    // NOTE: this has to be as fast as possible!

    if ($cp->lastenrol) {
        $params = array('userid' => $cp->userid, 'courseid' => $cp->courseid);
        $teamboardselect = "IN (SELECT f.id FROM {teamboard} f WHERE f.course = :courseid)";

//        $DB->delete_records_select('teamboard_subscriptions', "userid = :userid AND forum $teamboardselect", $params);
//        $DB->delete_records_select('teamboard_track_prefs',   "userid = :userid AND forumid $teamboardselect", $params);
        $DB->delete_records_select('teamboard_read', "userid = :userid AND forumid $teamboardselect", $params);
    }
}

/**
 * Deletes read records for the specified index. At least one parameter must be specified.
 *
 * @global object
 * @param int $userid
 * @param int $postid
 * @param int $discussionid
 * @param int $teamboardid
 * @return bool
 */
function teamboard_tp_delete_read_records($userid = -1, $contentsid = -1, $teamboardid = -1) {
    global $DB;
    $params = array();

    $select = '';
    if ($userid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'userid = ?';
        $params[] = $userid;
    }
    if ($contentsid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'contentsid = ?';
        $params[] = $contentsid;
    }

    if ($teamboardid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'teamboardid = ?';
        $params[] = $teamboardid;
    }
    if ($select == '') {
        return false;
    } else {
        return $DB->delete_records_select('teamboard_read', $select, $params);
    }
}

/**
 * Adds information about unread messages, that is only required for the course view page (and
 * similar), to the course-module object.
 * @param cm_info $cm Course-module object
 */
function teamboard_cm_info_view(cm_info $cm) {
    global $CFG;

    // Get tracking status (once per request)
    static $initialised;
    static $usetracking, $strunreadpostsone;
    if (!isset($initialised)) {

        $initialised = true;
    }

    if ($usetracking) {
        $unread = teamboard_tp_count_teamboard_unread_posts($cm, $cm->get_course());

        if ($unread) {
            $out = '<span class="unread"> <a href="' . $cm->get_url() . '">';
            if ($unread == 1) {
                $out .= $strunreadpostsone;
            } else {
                $out .= get_string('unreadpostsnumber', 'forum', $unread);
            }
            $out .= '</a></span>';
            $cm->set_after_link($out);
        }
    }
}

/**
 *
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $cm
 * @return array
 */
function teamboard_get_discussions_unread($cm) {
    global $CFG, $DB, $USER;

    $now = round(time(), -2);
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 60);

    $params = array();
    $groupmode = groups_get_activity_groupmode($cm);
    $currentgroup = groups_get_activity_group($cm);

    if ($groupmode) {
        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);

        if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $modcontext)) {
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                $params['currentgroup'] = $currentgroup;
            } else {
                $groupselect = "";
            }
        } else {
            //separate groups without access all
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                $params['currentgroup'] = $currentgroup;
            } else {
                $groupselect = "AND d.groupid = -1";
            }
        }
    } else {
        $groupselect = "";
    }

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < :now1 AND (d.timeend = 0 OR d.timeend > :now2)";
      $params['now1'] = $now;
      $params['now2'] = $now;
      } else {
      $timedsql = "";
      } */
    $timedsql = "";
    $sql = "SELECT d.id, COUNT(d.id) AS unread
              FROM {teamboard_contents} d
                   LEFT JOIN {teamboard_read} r ON (r.contentsid = d.id AND r.userid = $USER->id)
             WHERE d.board = {$cm->instance}
                   AND d.timemodified >= :cutoffdate AND r.id is NULL
                   $groupselect
                   $timedsql
          GROUP BY d.id";
    $params['cutoffdate'] = $cutoffdate;

    if ($unreads = $DB->get_records_sql($sql, $params)) {
        foreach ($unreads as $unread) {
            $unreads[$unread->id] = $unread->unread;
        }
        return $unreads;
    } else {
        return array();
    }
}

/**
 * Mark post as read.
 * @global object
 * @global object
 * @param int $userid
 * @param int $contentsid
 */
function teamboard_tp_add_read_record($userid, $contentsId) {
    global $CFG, $DB;
    $now = time();
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 3600);
    $aa = $DB->record_exists('teamboard_read', array('userid' => $userid, 'contentsid' => $contentsId));

    if (!$DB->record_exists('teamboard_read', array('userid' => $userid, 'contentsid' => $contentsId))) {
        $sql = "INSERT INTO {teamboard_read} (userid, contentsid, teamboardid, firstread, lastread)

                SELECT ?,p.id,  p.board, ?, ?
                  FROM {teamboard_contents} p
                 WHERE p.id = ? AND p.timemodified >= ?";
        return $DB->execute($sql, array($userid, $now, $now, $contentsId, $cutoffdate));
    } else {
        $sql = "UPDATE {teamboard_read}
                   SET lastread = ?
                 WHERE userid = ? AND contentsid = ?";
        return $DB->execute($sql, array($now, $userid, $contentsId));
    }
}

/**
 * Return grade for given user or all users.
 *
 * @global object
 * @global object
 * @param object $teamboard
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function teamboard_get_user_grades($teamboard, $userid = 0) {
    global $CFG;

    require_once($CFG->dirroot . '/rating/lib.php');

    $ratingoptions = new stdClass;
    $ratingoptions->component = 'mod_teamboard';
    $ratingoptions->ratingarea = 'post';

    //need these to work backwards to get a context id. Is there a better way to get contextid from a module instance?
    $ratingoptions->modulename = 'teamboard';
    $ratingoptions->moduleid = $teamboard->id;
    $ratingoptions->userid = $userid;
    $ratingoptions->aggregationmethod = $teamboard->assessed;
    $ratingoptions->scaleid = $teamboard->scale;
    $ratingoptions->itemtable = 'teamboard_posts';
    $ratingoptions->itemtableusercolumn = 'userid';

    $rm = new rating_manager();
    return $rm->get_user_grades($ratingoptions);
}

/**
 * Update activity grades
 *
 * @global object
 * @global object
 * @param object $teamboard
 * @param int $userid specific user only, 0 means all
 * @param boolean $nullifnone return null if grade does not exist
 * @return void
 */
function teamboard_update_grades($teamboard, $userid = 0, $nullifnone = true) {
    global $CFG, $DB;
    require_once($CFG->libdir . '/gradelib.php');

    if (!$teamboard->assessed) {
        teamboard_grade_item_update($teamboard);
    } else if ($grades = teamboard_get_user_grades($teamboard, $userid)) {
        teamboard_grade_item_update($teamboard, $grades);
    } else if ($userid and $nullifnone) {
        $grade = new stdClass();
        $grade->userid = $userid;
        $grade->rawgrade = NULL;
        teamboard_grade_item_update($teamboard, $grade);
    } else {
        teamboard_grade_item_update($teamboard);
    }
}

/**
 * Update all grades in gradebook.
 * @global object
 */
function teamboard_upgrade_grades() {
    global $DB;

    $sql = "SELECT COUNT('x')
              FROM {teamboard} f, {course_modules} cm, {modules} m
             WHERE m.name='teamboard' AND m.id=cm.module AND cm.instance=f.id";
    $count = $DB->count_records_sql($sql);

    $sql = "SELECT f.*, cm.idnumber AS cmidnumber, f.course AS courseid
              FROM {teamboard} f, {course_modules} cm, {modules} m
             WHERE m.name='teamboard' AND m.id=cm.module AND cm.instance=f.id";
    $rs = $DB->get_recordset_sql($sql);
    if ($rs->valid()) {
        $pbar = new progress_bar('teamboardupgradegrades', 500, true);
        $i = 0;
        foreach ($rs as $teamboard) {
            $i++;
            upgrade_set_timeout(60 * 5); // set up timeout, may also abort execution
            teamboard_update_grades($teamboard, 0, false);
            $pbar->update($i, $count, "Updating teamboard grades ($i/$count).");
        }
    }
    $rs->close();
}

/**
 * Create/update grade item for given teamboard
 *
 * @global object
 * @uses GRADE_TYPE_NONE
 * @uses GRADE_TYPE_VALUE
 * @uses GRADE_TYPE_SCALE
 * @param object $teamboard object with extra cmidnumber
 * @param mixed $grades optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok
 */
function teamboard_grade_item_update($teamboard, $grades = NULL) {
    global $CFG;
    if (!function_exists('grade_update')) { //workaround for buggy PHP versions
        require_once($CFG->libdir . '/gradelib.php');
    }

    $params = array('itemname' => $teamboard->name, 'idnumber' => $teamboard->cmidnumber);

    if (!$teamboard->assessed or $teamboard->scale == 0) {
        $params['gradetype'] = GRADE_TYPE_NONE;
    } else if ($teamboard->scale > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax'] = $teamboard->scale;
        $params['grademin'] = 0;
    } else if ($teamboard->scale < 0) {
        $params['gradetype'] = GRADE_TYPE_SCALE;
        $params['scaleid'] = -$teamboard->scale;
    }

    if ($grades === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }

    return grade_update('mod/teamboard', $teamboard->course, 'mod', 'teamboard', $teamboard->id, 0, $grades, $params);
}

/**
 * Delete grade item for given teamboard
 *
 * @global object
 * @param object $teamboard object
 * @return object grade_item
 */
function teamboard_grade_item_delete($teamboard) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    return grade_update('mod/teamboard', $teamboard->course, 'mod', 'teamboard', $teamboard->id, 0, NULL, array('deleted' => 1));
}

/**
 * Serves the forum attachments. Implements needed access control ;-)
 *
 * @param object $course
 * @param object $cm
 * @param object $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return bool false if file not found, does not return if found - justsend the file
 */
function teamboard_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    require_course_login($course, true, $cm);

    $fileareas = array('attachment', 'contents');
    if (!in_array($filearea, $fileareas)) {
        return false;
    }

    $postid = (int) array_shift($args);

    if (!$contents = $DB->get_record('teamboard_contents', array('id' => $postid))) {
        return false;
    }


    if (!$teamboard = $DB->get_record('teamboard', array('id' => $cm->instance))) {
        return false;
    }

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_teamboard/$filearea/$postid/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // Make sure groups allow this user to see this file
    /*   if ($discussion->groupid > 0 and $groupmode = groups_get_activity_groupmode($cm, $course)) {   // Groups are being used
      if (!groups_group_exists($discussion->groupid)) { // Can't find group
      return false;                           // Be safe and don't send it to anyone
      }

      if (!groups_is_member($discussion->groupid) and !has_capability('moodle/site:accessallgroups', $context)) {
      // do not send posts from other groups when in SEPARATEGROUPS or VISIBLEGROUPS
      return false;
      }
      } */

    // Make sure we're allowed to see it...
    if (!teamboard_user_can_see_post($teamboard, $contents, NULL, $cm)) {
        return false;
    }


    // finally send the file
    send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}

/**
 * @global object
 * @global object
 * @param object $teamboard
 * @param object $post
 * @param object $user
 * @param object $cm
 * @return bool
 */
function teamboard_user_can_see_post($teamboard, $contents, $user = NULL, $cm = NULL) {
    global $CFG, $USER, $DB;

    // retrieve objects (yuk)
    if (is_numeric($teamboard)) {
        debugging('missing full teamboard', DEBUG_DEVELOPER);
        if (!$teamboard = $DB->get_record('teamboard', array('id' => $teamboard))) {
            return false;
        }
    }


    if (is_numeric($post)) {
        debugging('missing full post', DEBUG_DEVELOPER);
        if (!$contents = $DB->get_record('teamboard_contents', array('id' => $contents))) {
            return false;
        }
    }
    if (!isset($contents->id) && isset($contents->parent)) {
        $contents->id = $contents->parent;
    }

    if (!$cm) {
        debugging('missing cm', DEBUG_DEVELOPER);
        if (!$cm = get_coursemodule_from_instance('teamboard', $teamboard->id, $teamboard->course)) {
            print_error('invalidcoursemodule');
        }
    }

    if (empty($user) || empty($user->id)) {
        $user = $USER;
    }

    /*    $canviewcontents = !empty($cm->cache->caps['mod/teamboard:viewcontents']) || has_capability('mod/teamboard:viewcontents', get_context_instance(CONTEXT_MODULE, $cm->id), $user->id);
      if (!$canviewcontents && !has_all_capabilities(array('moodle/user:viewdetails', 'moodle/user:readuserposts'), get_context_instance(CONTEXT_USER, $post->userid))) {
      return false;
      } */

    if (isset($cm->uservisible)) {
        if (!$cm->uservisible) {
            return false;
        }
    } else {
        if (!coursemodule_visible_for_user($cm, $user->id)) {
            return false;
        }
    }


    return true;
}

/**
 * Return rating related permissions
 *
 * @param string $options the context id
 * @return array an associative array of the user's rating permissions
 */
function teamboard_rating_permissions($contextid, $component, $ratingarea) {
    $context = context::instance_by_id($contextid);
    if ($component != 'mod_teamboard' || $ratingarea != 'post') {
        // We don't know about this component/ratingarea so just return null to get the
        // default restrictive permissions.
        return null;
    }
    return array(
        'view' => has_capability('mod/teamboard:viewrating', $context),
        'viewany' => has_capability('mod/teamboard:viewanyrating', $context),
        'viewall' => has_capability('mod/teamboard:viewallratings', $context),
        'rate' => has_capability('mod/teamboard:rate', $context)
    );
}

function teamboard_get_ratings($context, $postid, $sort = "u.firstname ASC") {
    $options = new stdClass;
    $options->context = $context;
    $options->component = 'mod_teamboard';
    $options->ratingarea = 'post';
    $options->itemid = $postid;
    $options->sort = "ORDER BY $sort";

    $rm = new rating_manager();
    return $rm->get_all_ratings_for_item($options);
}

function teamboard_editor_options(context_module $context, $contentid) {
    global $COURSE, $PAGE, $CFG;
    // TODO: add max files and max size support
    $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
    $contentid = null;
    return array(
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'maxbytes' => $maxbytes,
        'trusttext' => true,
        'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
        'subdirs' => file_area_contains_subdirs($context, 'mod_teamboard', 'contents', $contentid)
    );
}

function teamboard_thread_contents($instance, $page, $perpage = TEAMBOARD_THREAD_DEFAULT, $searchfield = "1", $searchvalue = "", $boardsort = " st.ref DESC, st.step asc", $groupid = 0) {
    global $DB, $USER;

    $cm = get_coursemodule_from_instance('teamboard', $instance);
    $context = context_module::instance($cm->id);

    $params = array(
        'contextid' => $context->id,
        'boardid' => $instance);

    $boardsort = "st.ref DESC, st.step asc";
    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $where = "";

    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.fullname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }
    $sql = "SELECT * FROM 
            (SELECT jcb.*,u.picture,u.firstname,u.lastname,u.firstnamephonetic,u.lastnamephonetic,u.middlename,u.alternatename,u.imagealt,u.email,u.lastaccess ,u.id as urid , u.firstname || u.lastname as fullname, (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = jcb.id ) as itemid
                from {teamboard_contents} jcb  
                left join {user} u on jcb.userid = u.id 
                WHERE jcb.groupid = '" . $groupid . "' ) st
                where board = :boardid " . $where . " order by " . $boardsort;
    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function teamboard_thread_contents_notice($instance, $page, $perpage = TEAMBOARD_THREAD_DEFAULT, $searchfield = "1", $searchvalue = "", $boardsort = "st.isnotice DESC, st.ref DESC, st.step asc", $groupid = 0) {
    global $DB, $USER;

    $cm = get_coursemodule_from_instance('teamboard', $instance);
    $context = context_module::instance($cm->id);

    $params = array(
        'contextid' => $context->id,
        'boardid' => $instance);

    $boardsort = "st.isnotice DESC, st.ref DESC, st.step asc";
    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $where = "";

    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.fullname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }
    $sql = "SELECT * FROM 
            (SELECT jcb.*,u.picture,u.firstname,u.lastname,u.firstnamephonetic,u.lastnamephonetic,u.middlename,u.alternatename,u.imagealt,u.email,u.lastaccess ,u.id as urid , u.firstname || u.lastname as fullname, (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = jcb.id ) as itemid
                from {teamboard_contents} jcb  
                left join {user} u on jcb.userid = u.id 
                WHERE  jcb.groupid = '" . $groupid . "') st
                where isnotice = 1 and board = :boardid " . $where . " order by " . $boardsort;

    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function teamboard_thread_contents_count($cm, $searchfield, $searchvalue = "") {
    global $DB;

    $params = array();
    $where = "";
    $params[] = $cm;
    if (!empty($searchvalue)) {
        switch ($searchfield) {
            case 1:
                $where = "and title LIKE ?";
                $params[] = '%' . $searchvalue . '%';
                break;
            case 2:
                break;
            case 3:
                $where = "and contents LIKE ?";
                $params[] = '%' . $searchvalue . '%';
                break;
        }
    }
    $sql = " board = ? " . $where;

    return $DB->count_records_select("teamboard_contents", $sql, $params);
}

function teamboard_print_contents($ref, $text,$list_num,$searchvalue,$searchfield,$perpage,$id) {
    global $CFG, $DB, $USER ,$OUTPUT ,  $context;
    $boardform = required_param('boardform', PARAM_INT);
    $output = "";
    $sql = "select con.* , u.lastname , u.firstname , (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = con.id ) as itemid , pcon.userid as parentuserid "
            . "from {teamboard_contents} con "
            . "join {user} u on u.id = con.userid "
            . "join {teamboard_contents} pcon on pcon.id = con.ref "
            . "where con.ref = :ref "
            . "order by con.ref desc, con.step asc";
    $contents = $DB->get_records_sql($sql, array('ref' => $ref, 'contextid' => $context->id));
    foreach ($contents as $content) {
        $attachments = ($contents->itemid) ? '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">' : '';
        $output .= "<tr><td class='col-1 ' align=center> ";
        if ($content->lev == 0){ 
            $output .= $text;
        }
        if ($content->lev <= TEAMBOARD_INDENT_DEFAULT) {
            $step = $content->lev -1;
            $step_left_len = $step * 15;
        } else {
            $step = TEAMBOARD_INDENT_DEFAULT -1;
            $step_left_len = $step * 15;
        }
        $step_icon = '';
        $step_left = 'style="padding-left:' . $step_left_len . 'px;"';
        $output .= '</td><td class="title col-2">';
        if ($content->lev != 0){
            $step_icon = '<img src="'. $OUTPUT->pix_url('icon_reply', 'mod_teamboard') . '" alt="" ' .$step_left. ' /> ';
        }
        if ($content->isprivate && !has_capability('mod/teamboard:secretmanager', $context) && $content->parentuserid != $USER->id) {
            $output .= "<a href='#' onclick='" . 'alert("'.get_string('isprivate','teamboard').'")' . "' $step_left><span style='float:left;'>" . $content->title . $attachments . "</span>";
        } else {
            $output .= "<a href='" . $CFG->wwwroot . "/mod/teamboard/content.php?id=".$id."&contentId=" . $content->id . "&b=" . $content->board . "&groupid=" . $content->groupid . "&boardform=" . $boardform ."&list_num=".$list_num."&searchfield=".$searchfield."&searchvalue=".$searchvalue."&perpage=".$perpage. "'><span style='float:left;'>" . $step_icon . $content->title . "</span>";
        }
        if ($content->isprivate){
            $output .= "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'teamboard') . "' title='" . get_string('isprivate', 'teamboard') . "'>";
        }
        $output .= "</a></td>";
        $output .= "<td class='col-3 ' align=center>" . fullname($content) . "</td>";
        $output .= "<td class='col-4 ' align=center>" . date("Y-m-d", $content->timemodified) . "</td>";
        $output .= "<td class='col-5 ' align=center>" . $content->viewcnt . "</td>";
        $output .= "</tr>";
    }

    return $output;
}
