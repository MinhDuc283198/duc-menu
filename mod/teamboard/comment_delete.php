<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/teamboard/lib.php');
 
$id = optional_param('id', 0, PARAM_INT);
$cmid = optional_param('cmid', 0, PARAM_INT);
$contentid = optional_param('contentid', 0, PARAM_INT);
$groupid = optional_param('groupid', 0, PARAM_INT);

$content = $DB->get_record('teamboard_contents',array('id'=>$contentid));



$DB->delete_records('teamboard_comments',array('id'=>$id,'teamboard_contents'=>$contentid));
$DB->set_field('teamboard_contents', 'commentscount', ($content->commentscount-1), array('id'=>$content->id));

redirect($CFG->wwwroot."/mod/teamboard/content.php?id=".$cmid."&b=".$content->board."&contentId=".$content->id."&groupid=".$groupid."&boardform=1");
