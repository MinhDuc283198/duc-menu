<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_teamboard
 */

defined('MOODLE_INTERNAL') || die;
if ($ADMIN->fulltree) {
   // require_once($CFG->dirroot.'/mod/teamboard/lib.php');
    if (isset($CFG->maxbytes)) {
        $maxbytes = 0;
        if (isset($CFG->board_normalmaxbytes)) {
            $maxbytes = $CFG->board_normalmaxbytes;
        }
    $settings->add(new admin_setting_configselect('teamboard_normalmaxbytes', get_string('normalfilemaxsize', 'teamboard'),
                       '', 5242880, get_max_upload_sizes($CFG->maxbytes, 0, 0, $maxbytes)));
    }
    
    if (isset($CFG->maxbytes)) {
        $maxbytes = 0;
        if (isset($CFG->board_normalmaxbytes)) {
            $maxbytes = $CFG->board_normalmaxbytes;
        }
    $settings->add(new admin_setting_configselect('teamboard_massmaxbytes', get_string('massfilemaxsize', 'teamboard'),
                       '', 5368709120, get_max_upload_sizes($CFG->maxbytes, 0, 0, $maxbytes)));
    }

        // Default number of attachments allowed per post in all forums
    $settings->add(new admin_setting_configtext('teamboard_maxattachments', get_string('maxattachments', 'teamboard'),
                       '', 5, PARAM_INT));
  
}

