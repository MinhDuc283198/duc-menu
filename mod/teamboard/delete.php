<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>
        <?php
        require_once('../../config.php');

        $b = optional_param('b', 0, PARAM_INT);
        $contentId = optional_param('contentId', 0, PARAM_INT);
        $groupid = optional_param('groupid', 0, PARAM_INT);

        if ($b) {

            if (!$board = $DB->get_record("teamboard", array("id" => $b))) {
                print_error('invalidboardid', 'teamboard');
            }
            if (!$course = $DB->get_record("course", array("id" => $board->course))) {
                print_error('coursemisconf');
            }

            if (!$cm = get_coursemodule_from_instance("teamboard", $board->id, $course->id)) {
                print_error('missingparameter');
            }
        } else {
            print_error('missingparameter');
        }

        require_course_login($course, true, $cm);

        $context = get_context_instance(CONTEXT_MODULE, $cm->id);

        $PAGE->set_context($context);


        if (!has_capability('mod/teamboard:view', $context)) {
            notice('cannotdeletepost', 'teamboard');
        }

	

        $db_userid = $DB->get_field('teamboard_contents', 'userid', array('id' => $contentId, 'board' => $b));

        if ($db_userid == $USER->id || has_capability("mod/teamboard:delete", $context)) {

            $DB->delete_records_select("teamboard_contents", "id=? and board=?", array($contentId, $b));



            $msg = get_string('Deletionshavebeencompleted', 'teamboard');


            redirect("content_list.php?b=$b&groupid=$groupid");
        } else {
            $msg = get_string('no_delete_permission', 'teamboard');


            redirect("content_list.php?b=$b&groupid=$groupid&contentId=$contentId");
        }
        ?>

    </body>
</html>