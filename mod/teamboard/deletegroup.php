<?php

require_once('../../config.php');

$id = optional_param('id', 0, PARAM_INT);
$b =  optional_param('b', 0, PARAM_INT); // board instans

if($DB->delete_records('teamboard_groups',array('id'=>$id))){
    if($contents = $DB->get_records('teamboard_contents',array('groupid'=>$id), '','id')){
        $DB->delete_records('teamboard_comments',array('teamboard_contents'=>$contents->id));
        $DB->delete_records('teamboard_contents',array('groupid'=>$id));
    }
    
    redirect($CFG->wwwroot."/mod/teamboard/view.php?id=".$b);
}