<?php


require_once("../../config.php");
require_once("lib.php");

$id         = required_param('id',PARAM_INT);                           
$returnpage = optional_param('returnpage', 'index.php', PARAM_FILE);

$url = new moodle_url('/mod/teamboard/settracking.php', array('id'=>$id));
if ($returnpage !== 'index.php') {
    $url->param('returnpage', $returnpage);
}
$PAGE->set_url($url);

if (! $teamboard = $DB->get_record("teamboard", array("id" => $id))) {
    print_error('invalidforumid', 'forum');
}

if (! $course = $DB->get_record("course", array("id" => $teamboard->course))) {
    print_error('invalidcoursemodule');
}

if (! $cm = get_coursemodule_from_instance("teamboard", $teamboard->id, $course->id)) {
    print_error('invalidcoursemodule');
}

require_course_login($course, false, $cm);

$returnto = teamboard_go_back_to($returnpage.'?id='.$course->id.'&b='.$teamboard->id);

if (!teamboard_tp_can_track_teamboards($teamboard)) {
    redirect($returnto);
}

$info = new stdClass();
$info->name  = fullname($USER);
$info->teamboard = format_string($teamboard->name);
if (teamboard_tp_is_tracked($teamboard) ) {
    if (teamboard_tp_stop_tracking($teamboard->id)) {
        add_to_log($course->id, "teamboard", "stop tracking", "content_list.php?b=$teamboard->id", $teamboard->id, $cm->id);
        redirect($returnto, get_string("nownottracking", "forum", $info), 1);
    } else {
        print_error('cannottrack', '', $_SERVER["HTTP_REFERER"]);
    }

} else { 
    if (teamboard_tp_start_tracking($teamboard->id)) {
        add_to_log($course->id, "teamboard", "start tracking", "content_list.php?b=$teamboard->id", $teamboard->id, $cm->id);
        redirect($returnto, get_string("nowtracking", "forum", $info), 1);
    } else {
        print_error('cannottrack', '', $_SERVER["HTTP_REFERER"]);
    }
}


