<?php

require_once("../../../config.php");

$group_array = optional_param_array('groups', array(), PARAM_CLEAN); // Group Id
$board       = optional_param('board', 0, PARAM_INT);

if (!empty($group_array)) {
    foreach ($group_array as $id) {
        $val = explode('/',$id);
        $data = new stdClass();
        $data->groupid = $val[0];
        $data->isopen = $val[1];
        $data->board = $board;
        $data->usercnt = 0;
        $DB->insert_record('teamboard_groups', $data);
    }
}
