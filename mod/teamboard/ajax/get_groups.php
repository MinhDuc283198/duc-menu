<?php
require_once('../../../config.php');

$course = optional_param('course', 0, PARAM_INT);// Course ID
$b = optional_param('b', 0, PARAM_INT);        // Board ID
?>
 <ul class='board-list'>
                <?php
                $groups = $DB->get_records('groups', array('courseid' => $course));
                foreach ($groups as $group) {
                    if (!$DB->get_record('teamboard_groups', array('groupid' => $group->id , 'board'=>$b))) {
                        $sql = "select u.id, u.firstname,u.lastname from {groups_members} gu "
                                . "join {user} u on u.id = gu.userid "
                                . "where gu.groupid = :groupid";
                        $team_members = $DB->get_records_sql($sql, array('groupid' => $group->id));
                        $stdents = "";
                        foreach ($team_members as $team_member) {
                            $stdents.= fullname($team_member) . "&nbsp;";
                        }
                        ?>
                        <li>
                            <span class="left"><input type="checkbox" name="groups" class="group_checked" value="<?php echo $group->id; ?>" onclick="openboard(<?php echo $group->id; ?>)" /></span>
                            <span class="post-title"><?php echo $group->name; ?></span>
                            <span class="left"> 다른 조원에게 공개 <input type="radio" class="openboard" value="1" name="openboard<?php echo $group->id; ?>" checked disabled> 비공개 <input type="radio" name="openboard<?php echo $group->id; ?>" value="0"  class="openboard" disabled></span>
                            <br />
                            <span class="post-users"><?php echo $stdents; ?></span>
                            <span class="post-date"><?php echo date('Y-m-d', $group->timecreated); ?></span> 
                        </li>
                        
                    <?php }
                } ?>
            </ul>