<?php
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT);       // Course Module ID
$b = optional_param('b', 0, PARAM_INT);        // Board ID
$boardform = optional_param('boardform', TEAMBOARD_BOARDFORM_THREAD, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);     // which page to show
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['b'] = $b;
}
if ($page) {
    $params['page'] = $page;
}
if ($groupid) {
    $params['groupid'] = $groupid;
}
$PAGE->set_url('/mod/teamboard/view.php', $params);

if ($id) {
    if (!$cm = get_coursemodule_from_id('teamboard', $id)) {
        print_error('invalidcoursemodule');
    }
    if (!$course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }
    if (!$board = $DB->get_record("teamboard", array("id" => $cm->instance))) {
        print_error('invalidboardid', 'teamboard');
    }

    // move require_course_login here to use forced language for course
    // fix for MDL-6926

    require_course_login($course, true, $cm);
} else if ($b) {

    if (!$board = $DB->get_record("teamboard", array("id" => $b))) {
        print_error('invalidboardid', 'teamboard');
    }
    if (!$course = $DB->get_record("course", array("id" => $board->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("teamboard", $board->id, $course->id)) {
        print_error('missingparameter');
    }
    // move require_course_login here to use forced language for course
    // fix for MDL-6926

    require_course_login($course, true, $cm);
} else {
    print_error('missingparameter');
}


$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

$PAGE->set_context($context);

$PAGE->set_heading($course->fullname);

if (!has_capability('mod/teamboard:view', $context)) {
    if ($USER->id == 0) {
        require_login();
    } else {
        notice(get_string('cannotviewpostlist', 'teamboard'), $_SERVER["HTTP_REFERER"]);
    }
}

echo $OUTPUT->header();

$bookmark = $DB->get_record('bookmark',array('activity'=>'teamboard','course'=>$course->id,'instance'=>$board->id,'userid'=>$USER->id),'id');
      $bookmarking = 0;
    if($bookmark->id){
        $bookmarking = 1;
    } 
$title = $board->name;
//$title .= ($bookmarking)?'&nbsp;<span id="bookmark_ico" style="color:orange; cursor:pointer; text-shadow:1px 1px #999;">★</span>':'&nbsp;<span id="bookmark_ico" style="color:orange; cursor:pointer; text-shadow:1px 1px #999;">☆</span>';

echo $OUTPUT->heading(format_string($title), 2, array('class'=>'board-title'));

if (has_capability('mod/teamboard:addgroups', $context)) { ?>
<div>
    <input type="button" class="listmenu_list right" id="create_group" value="<?php echo get_string('addnewgroup','teamboard'); ?>" onclick="window.open('<?php echo $CFG->wwwroot; ?>/group/popup_index.php?id=<?php echo $course->id; ?>', '', 'width=1024,height=800')" />
    <input type="button" id="group_manage" class="listmenu_list right"   value="<?php echo get_string('addnewgroupboard', 'teamboard'); ?>" />
</div>
<?php
}

echo "<div class='board-list-area'><ul class='board-list'>";
$groups_sql = "select "
        . "boardgroup.id , boardgroup.isopen "
        . ",g.id as gid , g.name , g.timemodified "
        . "from {teamboard_groups} boardgroup "
        . "join {groups} g on g.id = boardgroup.groupid "
        . "where boardgroup.board = :board order by id asc";

$groups = $DB->get_records_sql($groups_sql, array('board' => $board->id));

foreach ($groups as $group) {
    $sql = "select u.id, u.firstname,u.lastname from {groups_members} gu "
            . "join {user} u on u.id = gu.userid "
            . "where gu.groupid = :groupid order by u.firstname asc";
    $view_capability = 0;
    $team_members = $DB->get_records_sql($sql, array('groupid' => $group->gid), 'id, lastname , firstname');
    $stdents = "";
    $participation = 0;
    foreach ($team_members as $team_member) {
        if ($USER->id == $team_member->id || has_capability('mod/teamboard:viewprivate', $context))
            $view_capability = 1;
        if ($USER->id == $team_member->id){
            $participation = 1;
        }
        $stdents.= fullname($team_member) . "&nbsp;";
    }
    if($participation == 1){
        echo "<li style='background-color:#E9F3FF;'>";
    } else {
        echo "<li>";
    }
    if (has_capability('mod/teamboard:addgroups', $context) || $view_capability || $group->isopen == 1) {
        $url = "location.href ='content_list.php?id=$id&groupid=$group->gid'";
        $gname = $group->name;
    } else {
        $url = "alert('".get_string('authorized','teamboard')."')";
        $gname = $group->name . "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
    }
    if (has_capability('mod/teamboard:addgroups', $context)) {
        ?>
        <button class='blue-form right' onclick="if (confirm('<?php echo get_string('delete_msg','teamboard');?>')) {
                    location.href = 'deletegroup.php?id=<?php echo $group->id; ?>&b=<?php echo $id; ?>'
                }"><?php echo get_string('delete','teamboard');?></button>
                <?php
        if($group->isopen == 1){
        ?>
        <button class='blue-form right' onclick="if (confirm('<?php echo get_string('isprivate_msg','teamboard');?>')) {
                        location.href = 'write.php?contentId=<?php echo $group->id; ?>&b=<?php echo $cm->instance; ?>&mode=isopen&isopen=0&moduleid=<?php echo $id;?>'
                    }"><?php echo get_string('private_setup','teamboard');?></button>
        <?php
        }else {
        ?>
        <button class='blue-form right' onclick="if (confirm('<?php echo get_string('ispublic_msg','teamboard');?>')) {
                        location.href = 'write.php?contentId=<?php echo $group->id; ?>&b=<?php echo $cm->instance; ?>&mode=isopen&isopen=1&moduleid=<?php echo $id;?>'
                    }"><?php echo get_string('public_setup','teamboard');?></button>
        <?php
        }
            }
            ?>
    <span class="post-title pointer" onclick="<?php echo $url; ?>"><?php echo $gname; ?></span>&nbsp;
    <span class="post-date"><?php echo date('Y-m-d', $group->timemodified); ?></span>
    <br>
    <span class="post-users"><?php echo $stdents; ?></span>
    <?php
    echo "</li>";
}
if (empty($groups)) {
    echo get_string('noboard','teamboard');
}

echo "</ul></div>";

echo $OUTPUT->footer();
?>
<div class="group_control_popup" id="group_control_popup" style="display:none;" title="<?php echo get_string('pluginadministration','teamboard');?>">
    <div style="height:100%; padding:5px;">
        <input type="checkbox" id="allcheck">
        <input type="button" class="board-search red-form" id="add_group" value="<?php echo get_string('addnewgroupboard','teamboard');?>" />
        <div class='board-list-area' id='dialog_groups'>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#bookmark_ico").click(function () {
        var url = "<?php echo $CFG->wwwroot; ?>/local/bookmark/ajax/add_new_bookmark.php?course=<?php echo $course->id; ?>&instance=<?php echo $board->id; ?>&activity=teamboard";

        $.ajax({url: url,
            success: function (result) {
                $("#bookmark_ico").html(result);
            }
        });
    });
    var dialog = $("#group_control_popup").dialog({
        autoOpen: false,
        modal: true,
        width: 800,
    });
    openboard = function (id) {
        if ($("input[name=openboard" + id + "]").prop("disabled")) {
            $("input[name=openboard" + id + "]").attr('disabled', false);
        } else {
            $("input[name=openboard" + id + "]").attr('disabled', true);
        }
    }
    $("#group_manage").button().on("click", function () {
        dialog.dialog("open");
        $.ajax({
            url: "./ajax/get_groups.php?b=<?php echo $board->id; ?>&course=<?php echo $course->id; ?>",
            success: function (result) {
                $("#dialog_groups").html(result);
            }
        });
    });
    $('#add_group').click(function () {
        var add_list = [];
        $(".group_checked").each(function (index, element) {
            if ($(this).is(":checked"))
                add_list.push($(this).val() + "/" + $("input[name=openboard" + $(this).val() + "]:checked").val());
        });
        if (add_list.length != 0) {
            if (confirm("<?php echo get_string('addgroup','teamboard');?>")) {
                $.ajax({
                    url: "./ajax/add_groups.php",
                    method: 'POST',
                    data: {
                        groups: add_list,
                        board: <?php echo $board->id; ?>
                    },
                    success: function (respon) {
                        location.href = "./view.php?id=<?php echo $id; ?>";
                    },
                });
            }
        } else {
            alert("<?php echo get_string('noselectgroup','teamboard');?>");
            return false;
        }
    });
    $('#allcheck').click(function () {
        $("input[class=openboard]").attr('disabled', true);
        if ($('#allcheck').is(":checked")) {
            $(".group_checked").each(function () {
                this.checked = true;
            });
            $("input[class=openboard]").attr('disabled', false);
        } else {
            $(".group_checked").each(function () {
                this.checked = false;
                $("input[class=openboard]").attr('disabled', true);
            });
        }
    });
</script>