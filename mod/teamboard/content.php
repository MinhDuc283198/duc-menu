<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/teamboard/lib.php');
require_once($CFG->libdir . '/portfoliolib.php');

$contentId = required_param('contentId', PARAM_INT);
$id = optional_param('id',0, PARAM_INT);
$boardform = required_param('boardform', PARAM_INT);
$groupid = required_param('groupid', PARAM_INT);
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$list_num = optional_param('list_num', 1, PARAM_INT); // 
$sql = "select c.*, u.lastname, u.firstname, u.username, c.userid userid 
                    from {teamboard_contents} c 
                    left join {user} u on c.userid = u.id
                    where c.id = :contentid";

$content = $DB->get_record_sql($sql, array('contentid' => $contentId));
$course = $DB->get_record('course', array('id' => $content->course), '*', MUST_EXIST);
$board = $DB->get_record('teamboard', array('id' => $content->board), '*', MUST_EXIST);
$cm = get_coursemodule_from_instance('teamboard', $board->id, $course->id, false, MUST_EXIST);

$postuser = $DB->get_record('user', array('id' => $content->userid));
$fullname = fullname($postuser);
$userdate = userdate($content->timecreated);

$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

require_course_login($course, true, $cm);

$group_sql = "select g.* , mem.id as memberid from {groups} g "
        . "left join {groups_members} mem on mem.groupid = g.id and mem.userid = :userid "
        . "where g.id = :groupid";
$group = $DB->get_record_sql($group_sql, array('groupid' => $groupid, 'userid' => $USER->id));
$ingroup = ($group->memberid || has_capability('mod/teamboard:viewprivate', $context)) ? 1 : 0;

$PAGE->set_url(new moodle_url('/mod/teamboard/content.php', array('id'=>$id,'contentId' => $contentId, 'groupid' => $groupid)));
$PAGE->set_title("$course->shortname: " . format_string($content->title));
$PAGE->set_context($context);
$PAGE->set_heading($course->fullname);

add_to_log($course->id, 'teamboard', 'view board content', $PAGE->url->out(false), $content->id, $cm->id);
unset($SESSION->fromdiscussion);
echo $OUTPUT->header();

if (isset($content->id)) {
    if($content->userid != $USER->id){
     $DB->insert_record('teamboard_read', array('userid' => $USER->id, 'courseid' => $content->course, 'teamboardid' => $content->board, 'contentsid' => $content->id, 'firstread' => time(), 'lastread' => time()));
     $DB->set_field_select('teamboard_contents', 'viewcnt', intval($content->viewcnt) + 1, " id='$contentId'");
    }
    $comments = $DB->get_records_select("teamboard_comments", "board=? and teamboard_contents=?", array($board->id, $contentId), "id DESC", "*");
    $by = new stdClass();
    $by->name = '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$postuser->id.'&amp;course='.$course->id.'">'.$fullname.'</a>';
    $by->date = $userdate;
}
?>
<h2 class='board-title'><?php echo $board->name ?></h2>
<div class="board-detail-area">
    <div class="detail-title-area">
        <span class="detail-title"><?php echo $content->title; ?></span>
        <span class="detail-date"><?php echo get_string('bynameondate','teamboard',$by);$userdate ."&nbsp;". $fullname; ?></span>
        <span class="detail-viewinfo area-right">
            <?php echo $content->viewcnt; ?> 
            <br/>
            <span> <?php echo get_string('view:cnt', 'mod_teamboard'); ?></span>
        </span>
        <?php if ($board->allowcomment) { ?>
            <span class="detail-commentinfo area-right"> 
                <?php echo $content->commentscount; ?> 
                <br/>
                <span><?php echo get_string('reply:cnt', 'mod_teamboard'); ?></span>
            </span>
        <?php } if ($board->allowrecommend) { ?>
            <span class="detail-recommendinfo area-right"> 
                <?php echo $content->recommendcnt; ?> 
                <br/>
                <span><?php echo get_string('reCommondCnt', 'mod_teamboard'); ?></span>
            </span>
        <?php } ?>
    </div>
    <?php
    $options = new stdClass;
    $options->para = false;
    $options->context = $modcontext;
    $content->contents = format_text(file_rewrite_pluginfile_urls($content->contents, 'pluginfile.php', $context->id, 'mod_teamboard', 'contents', $content->id), true, $options, $course->id);
    ?>
    <div class="detail-contents"><?php echo $content->contents; ?></div>
    <?php
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'mod_teamboard', 'attachment', $content->id, 'id', false);
    $attachments = "";
    if (count($files) > 0) {
        $type = '';
        $attfile = '';
        foreach ($files as $file) {
            $filepath = $file->get_filepath();
            if (empty($filepath)) {
                $filepath = '/';
            }
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            $iconimage = '<li><img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_teamboard/attachment/' . $content->id . $filepath . $filename);

            if ($file->get_filesize() > 0) {
                $attfile .= "<a href=\"$path\">$iconimage</a> ";
                $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a></li>", FORMAT_HTML, array('context' => $context)) . "</li>";
            } else {
                $alertstring = get_string('deletedfile', 'teamboard');
                $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . $iconimage . '</a>';
                $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . s($filename) . "</a></li>" . "</li>";
            }
        }
        $attachments .= $attfile;
        ?>
        <div class="detail-attachment-area">
            <span class="detail-attachment-title"><?php echo get_string('attachment', 'teamboard'); ?></span>
            <ul class="detail-attachment"><?php echo $attachments; ?></ul>
        </div>
    <?php 
    }
    $list_page = ceil($list_num/$perpage);
    ?>
    <div class="table-footer-area">
        <div class="btn-area btn-area-left">
            <button style="cursor:pointer;" onclick="location.href = '<?php echo $CFG->wwwroot; ?>/mod/teamboard/content_list.php?id=<?php echo $id; ?>&b=<?php echo $board->id; ?>&groupid=<?php echo $groupid; ?>&boardform=<?php echo $boardform; ?>&perpage=<?php echo $perpage;?>&page=<?php echo $list_page;?>&searchvalue=<?php echo $searchvalue;?>&searchfield=<?php echo $searchfield;?>'"><?php echo get_string('list', 'teamboard'); ?></button>
            <?php if (($board->allowreply) && $ingroup) { ?>
                <button style="cursor:pointer;" onclick="location.href = '<?php echo $CFG->wwwroot; ?>/mod/teamboard/write.php?id=<?php echo $id; ?>&b=<?php echo $board->id; ?>&contentId=<?php echo $content->id; ?>&groupid=<?php echo $groupid; ?>&mode=reply&boardform=<?php echo $boardform; ?>'"><?php echo get_string('replies', 'teamboard'); ?>
                <?php } ?>
        </div>
        <?php
            $recommended = $DB->get_record('teamboard_recommend', array('userid' => $USER->id, 'teamboardid' => $board->id, 'contentsid' => $content->id));
        ?>
        <div class="btn-area btn-area-right">
            
            
            <?php if ($board->allowrecommend == 1 && !$recommended && $ingroup) { ?>
                <button style="cursor:pointer;" onclick="if (confirm('<?php echo get_string('content_recommend','teamboard'); ?>')) { location.href = '<?php echo $CFG->wwwroot; ?>/mod/teamboard/recommend.php?b=<?php echo $board->id; ?>&groupid=<?php echo $groupid; ?>&boardform=<?php echo $boardform; ?>&contentId=<?php echo $content->id; ?>'}"><?php echo get_string('recommend', 'teamboard'); ?></button>
                <!-- 추천 -->
            <?php } else if ($board->allowrecommend == 1 && $recommended && $ingroup) { ?>
                <button><?php echo get_string('recommended', 'teamboard'); ?></button>
                <!-- 추천됨 -->
            <?php } if (has_capability('mod/teamboard:edit', $context) || $USER->id == $content->userid) { ?>
                <button onclick="location.href = '<?php echo $CFG->wwwroot; ?>/mod/teamboard/write.php?b=<?php echo $board->id; ?>&contentId=<?php echo $content->id; ?>&groupid=<?php echo $groupid; ?>&boardform=<?php echo $boardform; ?>&mode=edit'"><?php echo get_string('edit', 'teamboard'); ?></button>
                <!-- 수정 -->
            <?php } if (has_capability('mod/teamboard:delete', $context) || ($USER->id == $content->userid)) { ?>
                <button onclick="if (confirm('<?php echo get_string('content_delete','teamboard'); ?>')) {location.href = '<?php echo $CFG->wwwroot; ?>/mod/teamboard/delete.php?b=<?php echo $board->id; ?>&contentId=<?php echo $content->id; ?>&groupid=<?php echo $groupid; ?>&boardform=<?php echo $boardform; ?>&mode=delete'}"><?php echo get_string('delete', 'teamboard'); ?></button>
                <!-- 삭제 -->
            <?php } ?>
        </div>
    </div>
    <?php
//댓글영역

    if ($board->allowcomment == 1) {
        if ($ingroup) {
            $output = html_writer::start_tag('div', array('class' => 'table-reply-area'));
            $output .= html_writer::start_tag('form', array('method' => 'get', 'id' => 'comment_form', 'class' => 'reply', 'onsubmit' => 'return chkform();', 'action' => 'comment_write.php'));
            $input = html_writer::tag('textarea', '', array('id' => 'comment_textarea', 'name' => 'comment_value','maxlength'=>'300'));
            $cols = html_writer::tag('span', $input, array('class' => "option"));
            $btn .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'contentId', 'value' => $content->id));
            $btn .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'groupid', 'value' => $groupid));
            $btn .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'cmid', 'value' => $id));
            $btn .= html_writer::tag('input', '', array('id' => 'comment_submit', 'type' => 'submit', 'class' => 'reply-submit', 'value' => get_string('reply', 'teamboard')));
            $cols .= html_writer::tag('span', $btn, array('class' => "option"));
            $output .= html_writer::tag('tr', $cols, array('class' => "view_row"));
            $output .= html_writer::end_tag('form');
        }
        //댓글 목록 영역
        $comments_sql = "select com.* , u.firstname , u.lastname "
                . "from {teamboard_comments} com "
                . "join {user} u on u.id = com.userid "
                . "where com.teamboard_contents = :contentid "
                . "order by com.timecreated asc";
        $comments = $DB->get_records_sql($comments_sql, array('contentid' => $contentId));
            $output .= html_writer::start_tag('ul', array('class' => 'reply-list'));
            foreach ($comments as $comment) {
                $fullname = fullname($comment);
                $timecreated = date("Y-m-d H:i:s", $comment->timecreated);
                $linktext = "";
                if (has_capability('mod/teamboard:commentdelete', $context) || $USER->id == $comment->userid) {
                    $linktext = html_writer::tag('span', get_string('delete', 'teamboard'), array('class' => "bluetone", 'onclick' => "if(confirm('삭제하시겠습니까?')){ location.href='" . $CFG->wwwroot . "/mod/teamboard/comment_delete.php?cmid=".$id."&contentid=" . $content->id . "&groupid=" . $groupid . "&id=" . $comment->id . "' }"));
                }
                $p = html_writer::tag('p', text_to_html($comment->comments) . "&nbsp;" . $linktext, array('class' => "value", 'colspan' => '3'));
                $span = html_writer::tag('span', $fullname . " | " . $timecreated, array('class' => "comment_author"));
                $output .= html_writer::tag('li', $p . $span, array('class' => "comment_row"));
            }
            $output .= html_writer::end_tag('ul');

        $output .= html_writer::end_tag('div');
    }


    $param = array('ref' => $content->ref, 'board' => $board->id, 'groupid' => $groupid);
    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and title LIKE :title"; //
                $param['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and u.firstname || u.lastname LIKE :fullname"; //
                $param['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and contents LIKE :contents";
                $param['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }
    $next_sql = "select ref from {teamboard_contents} "
            . "left join {user} u on u.id = userid "
            . "where groupid = :groupid and lev = 0 and ref > :ref and board= :board ".$where." order by ref asc";
    $prev_sql = "select ref from {teamboard_contents} "
            . "left join {user} u on u.id = userid "
            . "where groupid = :groupid and lev = 0 and ref < :ref and board= :board ".$where." order by ref desc ";
    //$next_sql = "select min(ref)keep(dense_rank first order by ref asc)as ref from {teamboard_contents} where groupid = :groupid and lev = 0 and ref > :ref and board= :board";
    //$prev_sql = "select min(ref)keep(dense_rank first order by ref desc)as ref from {teamboard_contents} where groupid = :groupid and lev = 0 and ref < :ref and board= :board ";
    $next_contentref = $DB->get_field_sql($next_sql, $param);
    $prev_contentref = $DB->get_field_sql($prev_sql, $param);
    $output .= '<table class="generaltable"><thead><tr>';
    $output .= '<th class="col-1" style="width:10%;"> </th>';
    $output .= '<th class="col-2">' . get_string("title", 'teamboard') . ' </th>';
    $output .= '<th class="col-3" style="width:20%;">' . get_string("writer", 'teamboard') . '</th>';
    $output .= '<th class="col-4" style="width:10%">' . get_string("date", 'teamboard') . '</th>';
    $output .= '<th class="col-5" style="width:10%">' . get_string("view:cnt", 'teamboard') . '</th>';

    $next_num = $list_num-1;
    $prev_num = $list_num+1;
    
    $output .= teamboard_print_contents($next_contentref, '<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="next" />',$next_num,$searchvalue,$searchfield,$perpage,$id);
    $output .= teamboard_print_contents($content->ref, '<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="current" />',$list_num,$searchvalue,$searchfield,$perpage,$id);
    $output .= teamboard_print_contents($prev_contentref, '<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="prev" />',$prev_num,$searchvalue,$searchfield,$perpage,$id);

    $output .= "</table>";

    echo $output;
    ?>

    <form name="delForm" method="post">
        <input type="hidden" name="id" value="<?php echo($id); ?>">
        <input type="hidden" name="b" value="<?php echo($board->id); ?>">
        <input type="hidden" name="contentId" value="<?php echo($contentId); ?>">
    </form>

</div>
<script>
    function chkform() {
        if ($('#comment_textarea').val() == "") {
            alert("<?php echo get_string('enterstr_text', 'teamboard'); ?>");
            $('#comment_textarea').focus();
            return false;
        } else {
            return true;
        }
    }
    $("document").ready(function() {  
        var currentstr = 0;
        var mx = $("#comment_textarea").attr('maxlength');
        $('#comment_textarea').keyup(function(){
                var va = $(this).val().length;
                if(currentstr != va){
                    if(mx <= va){
                        alert('<?php echo get_string('maxlength','teamboard');?>');
                    }
                }
                    currentstr = va;    
        })
    }) 
</script>
<?php echo $OUTPUT->footer(); ?>