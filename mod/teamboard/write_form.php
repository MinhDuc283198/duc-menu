<?php

require_once($CFG->libdir . '/formslib.php');

class teamboard_write_form extends moodleform {

    function definition() {
        global $DB;

        $mform = $this->_form;
        $board = $this->_customdata['board'];
        $boardform = $this->_customdata['boardform'];
        $context = $this->_customdata['context'];
        $mode = $this->_customdata['mode'];
        $lev = $this->_customdata['lev'];
        $type = $this->_customdata['type'];

        $isnotice = $this->_customdata['isnotice'];
        $isprivate = $this->_customdata['isprivate'];

        $mform->addElement('header', 'nameforyourheaderelement', get_string('notice:write', 'local_board'));

        //제목영역
        $titlearray = array();
        $titlearray[] = & $mform->createElement('text', 'title', get_string('content:title', 'local_board'));
        if ($board->allownotice && $mode != 'reply' && $lev == 0 && has_capability('mod/teamboard:noticemanager', $context)) {
            $titlearray[] = & $mform->createElement('advcheckbox', 'isnotice', '', get_string('notice', 'local_jinoboard'), array('group' => 2), array(0, 1));
        }
        $mform->addGroup($titlearray, 'titlear', get_string('title', 'teamboard'), ' ', false);
        $mform->setType('title', PARAM_CLEAN);
        if ($board->allownotice && $mode != 'reply' && $lev == 0 && has_capability('mod/teamboard:noticemanager', $context))
            $mform->setDefault('isnotice', $isnotice);
        $mform->addRule('titlear', null, 'required');
        $titlegrprules = array();
        $titlegrprules['title'][] = array(null, 'required', null, 'client');
        $mform->addGroupRule('titlear', $titlegrprules);

        if ($board->allowsecret && $mode != 'reply') {
            $radioarray = array();
            $radioarray[] = & $mform->createElement('radio', 'isprivate', '', get_string('issecretno', 'local_jinoboard'), 0);
            $radioarray[] = & $mform->createElement('radio', 'isprivate', '', get_string('issecretyes', 'local_jinoboard'), 1);
            $mform->addGroup($radioarray, 'radioar', get_string('issecret', 'local_jinoboard'), array(' '), false);
            $mform->setDefault('isprivate', $isprivate);
            $mform->addRule('isprivate', null, 'required');
        } else {
            $mform->addElement('hidden', 'isprivate');
            $mform->setType('isprivate', PARAM_INT);
            $mform->setDefault('isprivate', $isprivate);
        }

        $mform->addElement('editor', 'contents', get_string('content', 'teamboard'), null, self::editor_options());

        $chk = "<input type='checkbox' name='view_filemanager' id='teamboardfilecheck'/> <label for='teamboardfilecheck'>" . get_string('attachmentcheck', 'local_jinoboard')."</label>";
        $mform->addElement('static', 'static', get_string('attachment', 'teamboard'), $chk);

        $staticmsg = "<br><span>" . get_string('attachmentmsg', 'local_jinoboard') . "</span>";
        $mform->addGroup(array(
            $mform->createElement('filemanager', 'attachments', get_string('attachment', 'teamboard'), null, self::attachment_options($board)),
            $mform->createElement('static', 'text', '', $staticmsg)
                ), 'filemanager', '', array(''), false);

        $this->add_action_buttons();

        $mform->addElement('hidden', 'b');
        $mform->setType('b', PARAM_INT);
        $mform->setDefault('b', $board->id);

        $mform->addElement('hidden', 'confirmed');
        $mform->setType('confirmed', PARAM_INT);
        $mform->setDefault('confirmed', $this->_customdata['confirmed']);

        $mform->addElement('hidden', 'boardform');
        $mform->setType('boardform', PARAM_INT);
        $mform->setDefault('boardform', $boardform);

        $mform->addElement('hidden', 'itemid');
        $mform->setType('itemid', PARAM_INT);
        $mform->setDefault('itemid', $this->_customdata['itemid']);

        $mform->addElement('hidden', 'boardform');
        $mform->setType('boardform', PARAM_INT);

        $mform->addElement('hidden', 'groupid');
        $mform->setType('groupid', PARAM_INT);

        if ($this->_customdata['mode'] == "edit") {
            $mform->addElement('hidden', 'mode');
            $mform->setType('mode', PARAM_TEXT);
            $mform->setDefault('mode', 'edit');

            $mform->addElement('hidden', 'contentId');
            $mform->setType('contentId', PARAM_INT);
        } else if ($this->_customdata['mode'] == "reply") {
            $mform->addElement('hidden', 'mode', "reply");
            $mform->setType('mode', PARAM_TEXT);

            $mform->addElement('hidden', 'contentId');
            $mform->setType('contentId', PARAM_INT);

            $mform->addElement('hidden', 'ref');
            $mform->setType('ref', PARAM_INT);

            $mform->addElement('hidden', 'step');
            $mform->setType('step', PARAM_INT);

            $mform->addElement('hidden', 'lev');
            $mform->setType('lev', PARAM_INT);
        }
    }

    public static function editor_options() {
        global $COURSE, $PAGE, $CFG;
        require_once($CFG->dirroot . "/repository/lib.php");

        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL
        );
    }

    public static function attachment_options($board) {

        return array(
            'subdirs' => 0,
            'maxbytes' => $board->maxbytes,
            'maxfiles' => $board->maxattachments,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL
        );
    }

}

?>
