<?php

require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once('lib.php');

$page = optional_param('page', 1, PARAM_INT);
$instance = optional_param('instance', 0, PARAM_INT);
$groupid = optional_param('groupid', 0, PARAM_INT);        // Moodle Group ID
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string

$context = context_system::instance();
$PAGE->set_context($context);

$board = $DB->get_record("teamboard", array("id" => $instance));
$contents = teamboard_thread_contents($instance, $page+1, TEAMBOARD_THREAD_DEFAULT, $searchfield, $searchvalue, "st.ref DESC, st.step asc", $groupid);
$next = teamboard_thread_contents($instance, $page+2, TEAMBOARD_THREAD_DEFAULT, $searchfield, $searchvalue, "st.ref DESC, st.step asc", $groupid);
$next_count = count($next);
$datetimeformat = "Y년 h월 d일, l , a g:i";
$by = new stdClass();

foreach($contents as $content){
    $contentid = $content->id;
                $content->id = $content->urid;
                $userdate = userdate($content->timecreated);
                $postcontent = format_text($content->contents, 1, null);

                if (!empty($content->itemid)) {
                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }

                if ($content->lev) {
                    if ($content->lev <= TEAMBOARD_INDENT_DEFAULT) {
                        $step = $content->lev;
                        $date_left_len = $step * 30;
                    } else {
                        $step = TEAMBOARD_INDENT_DEFAULT;
                        $date_left_len = $step * 30;
                    }
                     switch($content->lev){
                        case 1 : $calcwidth = 30; break; 
                        case 2 : $calcwidth = 60; break;
                        case 3 : $calcwidth = 90; break;
                        case 4 : $calcwidth = 120; break;
                        default : $calcwidth = 120; break;
                    }      
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - '.$calcwidth.'px) !important;"';
                    $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_teamboard') . '" alt="" /> ';
                } else {
                    $date_left = 'style="width:calc(100% - 0px) !important;"';
                    $step_icon = '';
                }
                echo "<li ". $date_left .">";
                echo '<div class="thread-left">' . $OUTPUT->user_picture($content) . '</div>';
                echo "<div class='thread-content'><h1 class='thread-post-title'>" . $step_icon;
                
                $parentid = 0;
                if ($content->lev != 0) {
                    $parent = $DB->get_record('teamboard_contents', array('id' => $content->ref));
                    $parentid = $parent->userid;
                }

                echo "<a href='" . $CFG->wwwroot . "/mod/teamboard/content.php?contentId=" . $contentid . "&b=" . $cm->instance . "&groupid=" . $groupid . "&boardform=" . $boardform . "'>" . $content->title . "</a>";
                echo '&nbsp;'.$filecheck;
                $by = new stdClass();
                $by->name = fullname($content);
                $by->date = userdate($content->timemodified);
                echo '</h1><span class="thread-post-meta">' . get_string('bynameondate', 'jinoforum', $by) . '</span>';
                echo "<p class='thread-post-content'>" . mb_strcut(strip_tags($postcontent), null, 630, 'utf-8') . '...' . "</p>";
                echo '</div>';
                echo '<div class="thread-right">';
                if ($board->allowrecommend)
                    echo "<div class='count-block recomm_count'>" . $content->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_teamboard') . "</span></div>";
                if ($board->allowcomment)
                    echo "<div class='count-block reply_count'>" . $content->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_teamboard') . "</span></div>";
                echo "<div class='count-block view_count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_teamboard') . "</span></div>";

                echo '</div>';
                echo "</li>";
        
}
    if ($next_count > 0) {
?>
    <div id="next_btn">
        <input type="hidden" id="threadpage"  value="<?php echo $page+1;?>">
        <input type="button" class="next" value="<?php echo get_string('next:content', 'mod_teamboard');?>" onclick="ThreadContent()">
    </div>
<?php
    }