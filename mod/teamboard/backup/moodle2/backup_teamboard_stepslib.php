<?php

class backup_teamboard_activity_structure_step extends backup_activity_structure_step {
    
    protected function define_structure() {
        
        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $teamboard = new backup_nested_element('teamboard', array('id'), array(
            'type',                     
            'name',
            'intro',
            'introformat',
            'maxbytes',
            'maxattachments',
            'allownotice',
            'allowreply',
            'allowcomment',
            'allowupload',
            'allowsecret',
            'allowpublic',
            'allowcategory',
            'allowrecommend',
            'timemodified'));
        $teamboard_groups = new backup_nested_element('teamboard_groups');
        $teamboard_group = new backup_nested_element('teamboard_group', array('id'), array(
            'groupid',
            'board',
            'usercnt',
            'isopen'));
        $teamboard_contents = new backup_nested_element('teamboard_contents');
        $teamboard_content = new backup_nested_element('teamboard_content', array('id'), array(
            'board',
            'groupid',
            'category',
            'userid',
            'ref',
            'step',
            'lev',
            'isnotice',
            'isprivate',
            'title',
            'contents',
            'viewcnt',
            'commentscount',
            'recommendcnt',
            'timecreated',
            'timemodified'));
        $teamboard_comments = new backup_nested_element('teamboard_comments');
        $teamboard_comment = new backup_nested_element('teamboard_comment', array('id'), array(
            'board',
            'teamboard_contents',
            'userid',
            'username',
            'ref',
            'step',
            'lev',
            'isprivate',
            'comments',
            'timecreated',
            'timemodified'));
        
        // Build the tree
	$teamboard->add_child($teamboard_contents);
        $teamboard_contents->add_child($teamboard_content);
        
        $teamboard->add_child($teamboard_groups); 
        $teamboard_groups->add_child($teamboard_group);
       
	$teamboard_content->add_child($teamboard_comments);
	$teamboard_comments->add_child($teamboard_comment);
        // Define sources
        $teamboard->set_source_table('teamboard', array('id' => backup::VAR_ACTIVITYID));
        
        if ($userinfo) {
            
            $teamboard_content->set_source_table('teamboard_contents', array('board' => backup::VAR_PARENTID));
            
            $teamboard_group->set_source_sql('
               SELECT *
                  FROM {teamboard_groups} 
                 WHERE board = ?',
               array(backup::VAR_PARENTID));
            
            $teamboard_comment->set_source_sql('
                SELECT *
                  FROM {teamboard_comments} 
                 WHERE teamboard_contents = ?',
                array(backup::VAR_PARENTID));

        }
        // Define file annotations
        $teamboard_content->annotate_files('mod_teamboard', 'attachment', 'id');
        
        // Define id annotations
        $teamboard_content->annotate_ids('user', 'userid');
        $teamboard_content->annotate_ids('groupid', 'groupid');
        $teamboard_comment->annotate_ids('user', 'userid');
        $teamboard_group->annotate_ids('groupid', 'groupid');

        // Return the root element (teamboard), wrapped into standard activity structure
        return $this->prepare_activity_structure($teamboard);
    }
}
?>
