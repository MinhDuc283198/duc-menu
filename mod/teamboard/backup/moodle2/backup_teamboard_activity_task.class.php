<?php

require_once($CFG->dirroot . '/mod/teamboard/backup/moodle2/backup_teamboard_stepslib.php'); // Because it exists (must)
//require_once($CFG->dirroot . '/mod/teamboard/backup/moodle2/backup_teamboard_settingslib.php'); // Because it exists (optional)



class backup_teamboard_activity_task extends backup_activity_task {
    
    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        //
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        $this->add_step(new backup_teamboard_activity_structure_step('teamboard structure', 'teamboard.xml'));
    }
    
    /**
     * Code the transformations to perform in the activity in
     * order to get transportable (encoded) links
     */
    static public function encode_content_links($content) {
        global $CFG;

         $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of teamboards
        $search="/(".$base."\/mod\/teamboard\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@TEAMBOARDINDEX*$2@$', $content);

        // Link to teamboard view by moduleid
        $search="/(".$base."\/mod\/teamboard\/content_list.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@TEAMBOARDVIEWBYID*$2@$', $content);

        // Link to teamboard view by teamboardid
        $search="/(".$base."\/mod\/teamboard\/content_list.php\?b\=)([0-9]+)/";
        $content= preg_replace($search, '$@TEAMBOARDVIEWBYB*$2@$', $content);

        // Link to teamboard discussion by discussionid
        $search="/(".$base."\/mod\/teamboard\/content.php\?contentId\=)([0-9]+)/";
        $content= preg_replace($search, '$@TEAMBOARDCONTENTVIEW*$2@$', $content);
        
        return $content;
    }
}
?>
