<?php

class restore_teamboard_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');
        
        $teamboard = new restore_path_element('teamboard', '/activity/teamboard');
        $paths[] = $teamboard;
        
        // Apply for 'assignment' subplugins optional paths at assignment level
        //$this->add_subplugin_structure('teamboard', $teamboard);
        
        
        if ($userinfo) {
            
            //$paths[] = new restore_path_element('teamboard_category', '/activity/teamboard/teamboard_categories/teamboard_category');
            $paths[] = new restore_path_element('teamboard_contents', '/activity/teamboard/teamboard_contents/teamboard_content');
            $paths[] = new restore_path_element('teamboard_comment', '/activity/teamboard/teamboard_contents/teamboard_content/teamboard_comments/teamboard_comment');
            $paths[] = new restore_path_element('teamboard_rating', '/activity/teamboard/teamboard_contents/teamboard_content/ratings/rating');
            $paths[] = new restore_path_element('teamboard_groups', '/activity/teamboard/teamboard_groups/teamboard_group');
            //$paths[] = new restore_path_element('teamboard_subscription', '/activity/teamboard/subscriptions/subscription');
            //$paths[] = new restore_path_element('teamboard_read', '/activity/teamboard/readposts/read');
            //$paths[] = new restore_path_element('teamboard_track', '/activity/teamboard/trackedprefs/track');
            
            //TODO $paths[] = new restore_path_element('forum_rating', '/activity/forum/discussions/discussion/posts/post/ratings/rating');
            
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    protected function process_teamboard($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timemodified = $this->apply_date_offset($data->timemodified);


        $newitemid = $DB->insert_record('teamboard', $data);
        $this->apply_activity_instance($newitemid);
    }

    protected function process_teamboard_category($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        
        $data->course = $this->get_courseid();
        $data->board = $this->get_new_parentid('teamboard');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        
        $newitemid = $DB->insert_record('teamboard_category', $data);
        $this->set_mapping('teamboard_category', $oldid, $newitemid);
    }
    
    protected function process_teamboard_contents($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        
        $data->course = $this->get_courseid();
        $data->board = $this->get_new_parentid('teamboard');
        $data->userid = $this->get_mappingid('user', $data->userid);
        //TODO category 어떻게 가져오는가?
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        
        $newitemid = $DB->insert_record('teamboard_contents', $data);
        $this->set_mapping('teamboard_contents', $oldid, $newitemid);
    }
    

    
    
    protected function process_teamboard_comment($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        
        $data->course = $this->get_courseid();
        $data->board = $this->get_new_parentid('teamboard');
        $data->teamboard_contents = $this->get_new_parentid('teamboard_contents');
        $data->userid = $this->get_mappingid('user', $data->userid);
        //TODO category 어떻게 가져오는가?
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        
        $newitemid = $DB->insert_record('teamboard_comments', $data);
        $this->set_mapping('teamboard_comment', $oldid, $newitemid);
    }
    

    protected function process_teamboard_rating($data) {
        global $DB;

        $data = (object)$data;

        // Cannot use ratings API, cause, it's missing the ability to specify times (modified/created)
        $data->contextid = $this->task->get_contextid();
        $data->itemid    = $this->get_new_parentid('teamboard_contents');
        if ($data->scaleid < 0) { // scale found, get mapping
            $data->scaleid = -($this->get_mappingid('scale', abs($data->scaleid)));
        }
        $data->rating = $data->value;
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // We need to check that component and ratingarea are both set here.
        if (empty($data->component)) {
            $data->component = 'mod_teamboard';
        }
        if (empty($data->ratingarea)) {
            $data->ratingarea = 'post';
        }

        $newitemid = $DB->insert_record('rating', $data);
    }
    protected function process_teamboard_groups($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        
        $data->course = $this->get_courseid();
        
        $newitemid = $DB->insert_record('teamboard_groups', $data);
        $this->set_mapping('teamboard_groups', $oldid, $newitemid);
    } 

    protected function process_teamboard_subscription($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->teamboard = $this->get_new_parentid('teamboard');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('teamboard_subscriptions', $data);
    }

    protected function process_teamboard_read($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->teamboardid = $this->get_new_parentid('teamboard');
        $data->contentsid = $this->get_mappingid('teamboard_contents', $data->contentsid);
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('teamboard_read', $data);
    }

    protected function process_teamboard_track($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->teamboardid = $this->get_new_parentid('teamboard');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('teamboard_track_prefs', $data);
    }

    protected function after_execute() {
        global $DB;

        
        $this->add_related_files('mod_teamboard', 'attachment', 'teamboard_contents');
    }
}

?>
