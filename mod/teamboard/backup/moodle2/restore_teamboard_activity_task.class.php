<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/teamboard/backup/moodle2/restore_teamboard_stepslib.php');

class restore_teamboard_activity_task extends restore_activity_task {
    
    protected function define_my_settings() {
        
    }
    
    protected function define_my_steps() {
        $this->add_step(new restore_teamboard_activity_structure_step('teamboard_structure', 'teamboard.xml'));
    }
    
    
    static public function define_decode_contents() {
        $contents = array();

        //$contents[] = new restore_decode_content('teamboard', array('intro'), 'teamboard');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();

        // List of teamboards in course
        $rules[] = new restore_decode_rule('TEAMBOARDINDEX', '/mod/teamboard/index.php?id=$1', 'course');
        // Forum by cm->id and teamboard->id
        $rules[] = new restore_decode_rule('TEAMBOARDVIEWBYID', '/mod/teamboard/content_list.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('TEAMBOARDVIEWBYB', '/mod/teamboard/content_list.php?b=$1', 'teamboard');
        // Link to teamboard discussion
        $rules[] = new restore_decode_rule('TEAMBOARDCONTENTVIEW', '/mod/teamboard/content.php?contentId=$1', 'teamboard_content');
        // Link to discussion with parent and with anchor posts
        
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * teamboard logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();

       /* $rules[] = new restore_log_rule('teamboard', 'add', 'content_list.php?id={course_module}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'update', 'content_list.php?id={course_module}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'view', 'content_list.php?id={course_module}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'view teamboard', 'content_list.php?id={course_module}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'mark read', 'content_list.php?f={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'start tracking', 'content_list.php?f={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'stop tracking', 'content_list.php?f={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'subscribe', 'content_list.php?f={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'unsubscribe', 'content_list.php?f={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'subscriber', 'subscribers.php?id={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'subscribers', 'subscribers.php?id={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'view subscribers', 'subscribers.php?id={teamboard}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'add discussion', 'discuss.php?d={teamboard_discussion}', '{teamboard_discussion}');
        $rules[] = new restore_log_rule('teamboard', 'view discussion', 'discuss.php?d={teamboard_discussion}', '{teamboard_discussion}');
        $rules[] = new restore_log_rule('teamboard', 'move discussion', 'discuss.php?d={teamboard_discussion}', '{teamboard_discussion}');
        $rules[] = new restore_log_rule('teamboard', 'delete discussi', 'content_list.php?id={course_module}', '{teamboard}',
                                        null, 'delete discussion');
        $rules[] = new restore_log_rule('teamboard', 'delete discussion', 'content_list.php?id={course_module}', '{teamboard}');
        $rules[] = new restore_log_rule('teamboard', 'add post', 'discuss.php?d={teamboard_discussion}&parent={teamboard_post}', '{teamboard_post}');
        $rules[] = new restore_log_rule('teamboard', 'update post', 'discuss.php?d={teamboard_discussion}&parent={teamboard_post}', '{teamboard_post}');
        $rules[] = new restore_log_rule('teamboard', 'prune post', 'discuss.php?d={teamboard_discussion}', '{teamboard_post}');
        $rules[] = new restore_log_rule('teamboard', 'delete post', 'discuss.php?d={teamboard_discussion}', '[post]');
*/
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();

    /*    $rules[] = new restore_log_rule('teamboard', 'view teamboards', 'index.php?id={course}', null);
        $rules[] = new restore_log_rule('teamboard', 'subscribeall', 'index.php?id={course}', '{course}');
        $rules[] = new restore_log_rule('teamboard', 'unsubscribeall', 'index.php?id={course}', '{course}');
        $rules[] = new restore_log_rule('teamboard', 'user report', 'user.php?course={course}&id={user}&mode=[mode]', '{user}');
        $rules[] = new restore_log_rule('teamboard', 'search', 'search.php?id={course}&search=[searchenc]', '[search]');*/

        return $rules;
    }
}

?>
