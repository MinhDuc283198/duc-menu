<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function xmldb_teamboard_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2014112101) {

        // Define field to be added to assign.
        $table = new xmldb_table('teamboard');
        $field = new xmldb_field('allowrecommend', XMLDB_TYPE_INTEGER, '1', null,
                                 XMLDB_NOTNULL, null, '0', null);

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Assign savepoint reached.
        upgrade_mod_savepoint(true, 2014112101, 'teamboard');
    }
    
    if ($oldversion < 2015040800) {

        // Define field to be added to assign.
        $table = new xmldb_table('teamboard_read');
        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null,
                                 XMLDB_NOTNULL, null, '0', null);

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Assign savepoint reached.
        upgrade_mod_savepoint(true, 2015040800, 'teamboard');
    }
    if ($oldversion < 2015040801) {

        // Define field to be added to assign.
        $table = new xmldb_table('teamboard_groups');
        $field = new xmldb_field('isopen', XMLDB_TYPE_INTEGER, '2', null,
                                 XMLDB_NOTNULL, null, '0', null);
        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Assign savepoint reached.
        upgrade_mod_savepoint(true, 2015040801, 'teamboard');
    }
	return true;
}