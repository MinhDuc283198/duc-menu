<style>
    .clearfix:before {
        content: "그룹 인원 추가";
        font-size:18px;
        font-weight: bold;
        color:#4d8ee0;
        border-bottom: 1px solid #4b8ee0;
        width:100%;
        float:left;
    }
    li {
        list-style: none;
    }
</style>
<?php
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT);       // Teamboard Group ID
$b = optional_param('b', 0, PARAM_INT);        // Teamboard ID

$param = array('id' => $id);
$PAGE->set_url('/mod/teamboard/newgroupusers.php', $params);

if (!$cm = get_coursemodule_from_instance('teamboard', $b)) {
    print_error('invalidcoursemodule');
}
if (!$board = $DB->get_record("teamboard", array("id" => $cm->instance))) {
    print_error('invalidboardid', 'teamboard');
}

$course = $DB->get_record('course', array('id' => $board->course), '*', MUST_EXIST);



$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

$PAGE->set_context($context);

require_course_login($course, true, $cm);

$course_context = get_course_context($context);

echo $OUTPUT->header();



$group = $DB->get_record('teamboard_groups', array('id' => $id));


$course_users = "";
$group_users = "";


$sql = "select u.id, u.firstname,u.lastname from {teamboard_group_users} gu "
        . "join {user} u on u.id = gu.userid "
        . "where gu.groupid = :groupid";

$group_members = $DB->get_records_sql($sql, array('groupid' => $group->id));



$userroles = $DB->get_records('role_assignments', array('contextid' => $course_context->id));
$rolenames = role_get_names($context, ROLENAME_ALIAS, false);

$cnt = 0;
foreach ($userroles as $userrole) {
    if ($rolenames[$userrole->roleid]->shortname == "student") {
        $cnt++;
        $user = $DB->get_record('user', array('id' => $userrole->userid), 'id ,firstname , lastname');
        if (!$added_users = $DB->get_record('teamboard_group_users', array('groupid' => $group->id, 'userid' => $user->id),'id')) {
            $course_users .= '<span class="courseuser' . $user->id . '" onclick="add_users(' . "'" . $user->id . "'" . ')">' . fullname($user) . '&nbsp;&nbsp;</span>';
        }
    }
}



foreach ($group_members as $group_member) {
    $group_users.= '<span class="groupuser' . $group_member->id . '" onclick="delete_users(' . "'" . $group_member->id . "'" . ')">' . fullname($group_member) . '&nbsp;&nbsp;</span>';
}



echo "<ul>";
echo "<li class='clearfix'></li>";
echo "<li>조이름 : " . $group->name . "</li>";
echo "<li class='group_user'>그룹에 등록된 유저 : " . $group_users . "</li>";
echo "<li class='course_user'>강좌에 등록된 유저 : " . $course_users . "<li>";
echo "<li>Tip : 클릭질로 등록 & 삭제 껄껄껄</li>";
echo "</ul>";
?>
<button class="red-form" onclick="location.href='view.php?id=<?php echo $cm->id; ?>'">백투더게시판</button>
<script>
    add_users = function (id) {
         $(".courseuser"+id).remove();
        $.ajax({
            url: "ajax/add_users.php",
            type: "POST",
            data: {id: id, group: <?php echo $group->id; ?>},
        })
                .done(function (html) {     
                    $(".group_user").append(html);
                });
    }
    delete_users = function (id) {
    $(".groupuser"+id).remove();
        $.ajax({
            url: "ajax/delete_users.php",
            type: "POST",
            data: {id: id, group: <?php echo $group->id; ?>},
        })
                .done(function (html) {       
                    $(".course_user").append(html);                    
                });
    }
</script>