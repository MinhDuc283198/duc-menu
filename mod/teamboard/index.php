<?php


require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/mod/teamboard/lib.php');


$id = optional_param('id', 0, PARAM_INT);                   
$subscribe = optional_param('subscribe', null, PARAM_INT);  

$url = new moodle_url('/mod/teamboard/index.php', array('id'=>$id));
if ($subscribe !== null) {
    require_sesskey();
    $url->param('subscribe', $subscribe);
}
$PAGE->set_url($url);

if ($id) {
    if (! $course = $DB->get_record('course', array('id' => $id))) {
        print_error('invalidcourseid');
    }
} else {
    $course = get_site();
}

require_course_login($course);
$PAGE->set_pagelayout('incourse');
$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);


unset($SESSION->fromdiscussion);

add_to_log($course->id, 'teamboard', 'view teamboards', "index.php?id=$course->id");





$strteamboards       = get_string('teamboards', 'teamboard');
$strteamboard        = get_string('teamboard', 'teamboard');
$strdescription  = get_string('description');
$strdiscussions  = get_string('discussions', 'teamboard');
$strsubscribed   = get_string('subscribed', 'teamboard');
$strunreadposts  = get_string('unreadposts', 'teamboard');
$strtracking     = get_string('tracking', 'teamboard');
$strmarkallread  = get_string('markallread', 'teamboard');
$strtrackteamboard   = get_string('trackteamboard', 'teamboard');
$strnotrackteamboard = get_string('notrackteamboard', 'teamboard');
$strsubscribe    = get_string('subscribe', 'teamboard');
$strunsubscribe  = get_string('unsubscribe', 'teamboard');
$stryes          = get_string('yes');
$strno           = get_string('no');







$generaltable = new html_table();
$generaltable->head  = array ($strteamboard, $strdescription, $strdiscussions);
$generaltable->align = array ('left', 'left', 'center');

//$usetracking = teamboard_tp_can_track_teamboards();
//
//if ($usetracking) {
//    $untracked = teamboard_tp_get_untracked_teamboards($USER->id, $course->id);
//
//    $generaltable->head[] = $strunreadposts;
//    $generaltable->align[] = 'center';
//
//    $generaltable->head[] = $strtracking;
//    $generaltable->align[] = 'center';
//}
//
//
//$subscribed_teamboards = teamboard_get_subscribed_teamboards($course);
//
//$can_subscribe = is_enrolled($coursecontext);
//if ($can_subscribe) {
//    $generaltable->head[] = $strsubscribed;
//    $generaltable->align[] = 'center';
//}

$usesections = course_format_uses_sections($course->format);
$sections = get_all_sections($course->id);

$table = new html_table();


$teamboards = $DB->get_records('teamboard', array('course' => $course->id));

$generalteamboards  = array();

$modinfo = get_fast_modinfo($course);

if (!isset($modinfo->instances['teamboard'])) {
    $modinfo->instances['teamboard'] = array();
}

foreach ($modinfo->instances['teamboard'] as $teamboardid=>$cm) {
    if (!$cm->uservisible or !isset($teamboards[$teamboardid])) {
        continue;
    }

    $teamboard = $teamboards[$teamboardid];

    if (!$context = context_module::instance($cm->id)) {
        continue;   
    }

    if (!has_capability('mod/teamboard:viewcontent', $context)) {
        continue;
    }


    $generalteamboards[$teamboard->id] = $teamboard;
}



if (!is_null($subscribe) and !isguestuser()) {
    foreach ($modinfo->instances['teamboard'] as $teamboardid=>$cm) {
        $teamboard = $teamboards[$teamboardid];
        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
        $cansub = false;

        if (has_capability('mod/teamboard:viewcontent', $modcontext)) {
            $cansub = true;
        }
        if ($cansub && $cm->visible == 0 &&
            !has_capability('mod/teamboard:managesubscriptions', $modcontext))
        {
            $cansub = false;
        }
        if (!teamboard_is_forcesubscribed($teamboard)) {
            $subscribed = teamboard_is_subscribed($USER->id, $teamboard);
            if ((has_capability('moodle/course:manageactivities', $coursecontext, $USER->id) || $teamboard->forcesubscribe != FORUM_DISALLOWSUBSCRIBE) && $subscribe && !$subscribed && $cansub) {
                teamboard_subscribe($USER->id, $teamboardid);
            } else if (!$subscribe && $subscribed) {
                teamboard_unsubscribe($USER->id, $teamboardid);
            }
        }
    }
    $returnto = teamboard_go_back_to("index.php?id=$course->id");
    $shortname = format_string($course->shortname, true, array('context' => get_context_instance(CONTEXT_COURSE, $course->id)));
    if ($subscribe) {
        add_to_log($course->id, 'teamboard', 'subscribeall', "index.php?id=$course->id", $course->id);
        redirect($returnto, get_string('nowallsubscribed', 'forum', $shortname), 1);
    } else {
        add_to_log($course->id, 'teamboard', 'unsubscribeall', "index.php?id=$course->id", $course->id);
        redirect($returnto, get_string('nowallunsubscribed', 'forum', $shortname), 1);
    }
}





if ($generalteamboards) {
    foreach ($generalteamboards as $teamboard) {
        $cm      = $modinfo->instances['teamboard'][$teamboard->id];
        $context = context_module::instance($cm->id);



        $count = teamboard_count_discussions($teamboard, $cm, $course);

//        if ($usetracking) {
            if ($teamboard->trackingtype == TEAMBOARD_TRACKING_OFF) {
                $unreadlink  = '-';
                $trackedlink = '-';

            } else {
                if (isset($untracked[$teamboard->id])) {
                        $unreadlink  = '-';
                } else if ($unread = teamboard_tp_count_teamboard_unread_posts($cm, $course)) {
                        $unreadlink = '<span class="unread"><a href="view.php?id='.$cm->id.'">'.$unread.'</a>';
                    $unreadlink .= '<a title="'.$strmarkallread.'" href="markposts.php?f='.
                                   $teamboard->id.'&amp;mark=read"><img src="'.$OUTPUT->pix_url('t/clear') . '" alt="'.$strmarkallread.'" /></a></span>';
                } else {
                    $unreadlink = '<span class="read">0</span>';
                }

                if ($teamboard->trackingtype == TEAMBOARD_TRACKING_ON) {
                    $trackedlink = $stryes;

                } else {
                    $aurl = new moodle_url('/mod/teamboard/settracking.php', array('id'=>$teamboard->id));
                    if (!isset($untracked[$teamboard->id])) {
                        $trackedlink = $OUTPUT->single_button($aurl, $stryes, 'post', array('title'=>$strnotrackteamboard));
                    } else {
                        $trackedlink = $OUTPUT->single_button($aurl, $strno, 'post', array('title'=>$strtrackteamboard));
                    }
                }
            }
//        }
        
        
        $teamboard->intro = shorten_text(format_module_intro('teamboard', $teamboard, $cm->id), $CFG->forum_shortpost);
        $teamboardname = format_string($teamboard->name, true);;

        
        
        
        if ($cm->visible) {
            $style = '';
        } else {
            $style = 'class="dimmed"';
        }
        $teamboardlink = "<a href=\"view.php?id=$cm->id\" $style>".format_string($teamboard->name,true)."</a>";
        $discussionlink = "<a href=\"view.php?id=$cm->id\" $style>".$count."</a>";

        $row = array ($teamboardlink, $teamboard->intro, $discussionlink);
//        if ($usetracking) {
//            $row[] = $unreadlink;
//            $row[] = $trackedlink; 
//        }

//        if ($can_subscribe) {
//            if ($teamboard->forcesubscribe != TEAMBOARD_DISALLOWSUBSCRIBE) {
//                $row[] = teamboard_get_subscribe_link($teamboard, $context, array('subscribed' => $stryes,
//                        'unsubscribed' => $strno, 'forcesubscribed' => $stryes,
//                        'cantsubscribe' => '-'), false, false, true, $subscribed_teamboards);
//            } else {
//                $row[] = '-';
//            }
//        }

        $generaltable->data[] = $row;
    }
}





$PAGE->navbar->add($strteamboards);
$PAGE->set_title("$course->shortname: $strteamboards");
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();


//if (!isguestuser() && isloggedin()) {
//    echo $OUTPUT->box_start('subscription');
//    echo html_writer::tag('div',
//        html_writer::link(new moodle_url('/mod/teamboard/index.php', array('id'=>$course->id, 'subscribe'=>1, 'sesskey'=>sesskey())),
//            get_string('allsubscribe', 'teamboard')),
//        array('class'=>'helplink'));
//    echo html_writer::tag('div',
//        html_writer::link(new moodle_url('/mod/teamboard/index.php', array('id'=>$course->id, 'subscribe'=>0, 'sesskey'=>sesskey())),
//            get_string('allunsubscribe', 'teamboard')),
//        array('class'=>'helplink'));
//    echo $OUTPUT->box_end();
//    echo $OUTPUT->box('&nbsp;', 'clearer');
//}

if ($generalteamboards) {
    echo $OUTPUT->heading(get_string('teamboards', 'teamboard'));
    echo html_writer::table($generaltable);
}


echo $OUTPUT->footer();

