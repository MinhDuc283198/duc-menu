<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/teamboard/lib.php');
 
$contentid = optional_param('contentId', 0, PARAM_INT);
$cmid = optional_param('cmid', 0, PARAM_INT);
$comment_value = optional_param('comment_value', "", PARAM_TEXT);
$groupid = optional_param('groupid', 0, PARAM_INT);

$content = $DB->get_record('teamboard_contents',array('id'=>$contentid));
if($content){
	
	$records = new stdClass();
	$records->course = $content->course;
	$records->board = $content->board;
	$records->teamboard_contents = $content->id;
	$records->userid = $USER->id;
	$records->username = fullname($USER);
	$records->comments = $comment_value;
	$records->timecreated = time();
	$records->timemodified = time();
	
	if($DB->insert_record('teamboard_comments',$records)){
		
		$DB->set_field('teamboard_contents', 'commentscount', ($content->commentscount+1), array('id'=>$content->id));
		redirect($CFG->wwwroot."/mod/teamboard/content.php?id=".$cmid."&b=".$content->board."&contentId=".$content->id."&groupid=".$groupid."&boardform=1");
	} 
} 

