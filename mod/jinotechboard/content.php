<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');

$PAGE->requires->jquery();

$contentId = required_param('contentId', PARAM_INT);
$boardform = required_param('boardform', PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);     // which page to show
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$list_num = optional_param('list_num', 1, PARAM_INT); // 

$sql = 'select c.*, u.lastname, u.firstname, u.username, c.userid userid 
                    from {jinotechboard_contents} c 
                    left join {user} u on c.userid = u.id
                    where c.id = :contentid';
$content = $DB->get_record_sql($sql, array('contentid' => $contentId));


$boardContents = array($content->id => $content);

$course = $DB->get_record('course', array('id' => $content->course), '*', MUST_EXIST);
$board = $DB->get_record('jinotechboard', array('id' => $content->board), '*', MUST_EXIST);
$cm = get_coursemodule_from_instance('jinotechboard', $board->id, $course->id, false, MUST_EXIST);

$postuser = $DB->get_record('user', array('id' => $content->userid));
$fullname = fullname($postuser);
$userdate = userdate($content->timecreated);

$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);
$title = $board->name;
$context = $contexts[$cm->id];

require_course_login($course, true, $cm);

$PAGE->set_url(new moodle_url('/mod/jinotechboard/content.php', array('contentId' => $contentId ,'boardform' => $boardform)));
$PAGE->set_title("$course->shortname: " . format_string($content->title));
$PAGE->set_context($context);
$PAGE->set_heading($board->name);

$eventparams = array(
    'objectid' => $content->id,
    'context' => $context,
    'other' => array(
        'jinotechboardid' => $board->id,
    )
);

$event = \mod_jinotechboard\event\content_viewed::create($eventparams);
$event->add_record_snapshot('jinotechboard_contents', $content);
$event->add_record_snapshot('jinotechboard', $board);
$event->trigger();

add_to_log($course->id, 'jinotechboard', 'view board content', $PAGE->url->out(false), $content->id, $cm->id);
unset($SESSION->fromdiscussion);

echo $OUTPUT->header();

if (isset($content->id)) {
    if($content->userid != $USER->id){
        $DB->insert_record('jinotechboard_read', array('userid' => $USER->id, 'courseid' => $content->course, 'jinotechboardid' => $content->board, 'contentsid' => $content->id, 'firstread' => time(), 'lastread' => time()));
        $DB->set_field_select('jinotechboard_contents', 'viewcnt', intval($content->viewcnt) + 1, " id='$contentId'");
    }
    $comments = $DB->get_records_select("jinotechboard_comments", "board=? and jinotechboard_contents=?", array($board->id, $contentId), "id DESC", "*");
}

require_once($CFG->dirroot . '/rating/lib.php');
$ratingoptions = new stdClass;
$ratingoptions->context = $context;
$ratingoptions->component = 'mod_jinotechboard';
$ratingoptions->ratingarea = 'post';
$ratingoptions->items = $boardContents;
$ratingoptions->aggregate = 1;
$ratingoptions->scaleid = 5;
$ratingoptions->userid = $USER->id;
if ($board->type == 'single' or ! $content->id) {
    $ratingoptions->returnurl = "$CFG->wwwroot/mod/jinotechboard/view.php?id=$cm->id";
} else {
    $ratingoptions->returnurl = "$CFG->wwwroot/mod/jinotechboard/content.php?d=$content->id";
}
$ratingoptions->assesstimestart = 0;
$ratingoptions->assesstimefinish = 0;

$rm = new rating_manager();
$boardContents = $rm->get_ratings($ratingoptions);

$output .= html_writer::start_tag('div', array('class' => 'board-detail-area'));

//여기가 글내용 디테일하게 들어가는 영역이다.
//제목영역
$output .= html_writer::tag('h2', get_string($title, 'theme_oklassedu'));
$output .= html_writer::start_tag('div', array('class' => 'detail-title-area'));
if($board->type == 1){
//콘텐츠 변수처리
$vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
$vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
$data['courseid'] = $course->id;
$data['userid'] = $USER->id;
$data['contents'] =$content->contents;
$data['title'] =$content->title;
//$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);

$output .= html_writer::tag('span', $data['title'], array('class' => 'detail-title'));
}else{
$data['title'] =$content->title;
$output .= html_writer::tag('span', $data['title'], array('class' => 'detail-title'));
}
//$output .= html_writer::tag('br');
$by = new stdClass();
$by->name = '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$postuser->id.'&amp;course='.$course->id.'">'.$fullname.'</a>';
$by->date = $userdate;
$output .= html_writer::tag('span',get_string('bynameondate','forum',$by) , array('class' => 'detail-date'));

if(has_capability('mod/jinotechboard:edit', $context)){ 
    $cnt =  '<br/><span style="cursor:pointer;" onclick="view_users('.$content->id.')">' . get_string('view:cnt', 'mod_jinotechboard') . '</span>';  
} else { 
    $cnt =  '<br/><span>' . get_string('view:cnt', 'mod_jinotechboard') . '</span>';  
}
$output .= html_writer::tag('span', $content->viewcnt.$cnt , array('class' => 'detail-viewinfo area-right'));

if ($board->allowcomment) {
    $output .= html_writer::tag('span', $content->commentscount . '<br/><span>' . get_string('reply:cnt', 'mod_jinotechboard') . '</span>', array('class' => 'detail-commentinfo area-right'));
}
if ($board->allowrecommend) {
    $output .= html_writer::tag('span', $content->recommendcnt . '<br/><span>' . get_string('reCommondCnt', 'mod_jinotechboard') . '</span>', array('class' => 'detail-recommendinfo area-right'));
}
$output .= html_writer::end_tag('div');

//내용영역


$options = new stdClass;
$options->para = false;
$options->context = $modcontext;

$content->contents = file_rewrite_pluginfile_urls($content->contents, 'pluginfile.php', $context->id, 'mod_jinotechboard', 'contents', $content->id);
$content->contents = format_text($content->contents, true, $options, $course->id);
if($board->type == 1){
//콘텐츠 변수처리
$vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
$vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
$data['courseid'] = $course->id;
$data['userid'] = $USER->id;
$data['contents'] =$content->contents;
$data['title'] =$content->title;
//$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);

$output .= html_writer::tag('div', $data['contents'], array('class' => 'detail-contents'));
}else{
$output .= html_writer::tag('div', $content->contents, array('class' => 'detail-contents'));    
}


//첨부파일영역
$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_jinotechboard', 'attachment', $content->id, 'id', false);
$attachments = "";
if (count($files) > 0) {
    $type = '';
    $attfile = '';

    if ($CFG->enableportfolios)
        $canexport = $USER->id == $content->userid;
    if ($canexport) {
        require_once($CFG->libdir . '/portfoliolib.php');
    }
    foreach ($files as $file) {

        $filepath = $file->get_filepath();
        if (empty($filepath)) {
            $filepath = '/';
        }

        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $attfile .= '<li>';
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_jinotechboard/attachment/' . $content->id . $filepath . $filename);
        if ($file->get_filesize() > 0) {
            //$attfile .= "<a href=\"$path\">$iconimage</a> ";
            $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
        } else {
            $alertstring = get_string('deletedfile', 'jinotechboard');
            $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . $iconimage . '</a>';
            $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . s($filename) . "</a>";
        }
        $attfile .= '</li>';
    }

    $attachments .= $attfile;

    $output .= html_writer::start_tag('div', array('class' => 'detail-attachment-area'));
    $output .= html_writer::tag('span', get_string('attachment', 'jinotechboard'), array('class' => "detail-attachment-title"));
    $output .= html_writer::tag('ul', $attachments, array('class' => "detail-attachment"));
    $output .= html_writer::end_tag('div');
}

//버튼영역
$list_page = ceil($list_num/$perpage);
$search_param = "";
if (!empty($searchfield)) {
    $search_param .= "&searchfield=" . $searchfield;
}

if (!empty($searchvalue)) {
    $search_param .= "&searchvalue=" . $searchvalue;
}

if (!empty($perpage)) {
    $search_param .= "&perpage=" . $perpage;
}
if($list_page == 0){ 
    $list_page = 1;
}
$list_url = $CFG->wwwroot . "/mod/jinotechboard/view.php?b=" . $board->id . "&boardform=" . $boardform . $search_param."&page=".$list_page;


$left_btns = html_writer::tag('button', get_string('jinotechboard:list', 'jinotechboard'), array('class' => "btns h40 br", 'style' => 'cursor:pointer;', 'onclick' => "location.href='" . $list_url . "'"));

$reply_url = $CFG->wwwroot . "/mod/jinotechboard/write.php?b=" . $board->id . "&contentId=" . $content->id . "&mode=reply&type=" . $board->type . "&boardform=" . $boardform . $search_param;
if ((($board->type == 2 && has_capability('mod/jinotechboard:reply', $context))) && !$content->isnotice && $content->userid != $USER->id) {
    $left_btns .= html_writer::tag('button', get_string('replies', 'jinotechboard'), array('class' => "btns h40 br", 'style' => 'cursor:pointer;', 'onclick' => "location.href='" . $reply_url . "'"));
}

$recommended = $DB->get_record('jinotechboard_recommend', array('userid' => $USER->id, 'jinotechboardid' => $board->id, 'contentsid' => $content->id));

if ($board->allowrecommend == 1 && !$recommended) {
    $left_btns .= html_writer::tag('button', get_string('recommend', 'jinotechboard'), array('class' => "btns h40 br", 'style' => 'cursor:pointer;', 'onclick' => "if(confirm('추천하시겠습니까?')){ location.href='" . $CFG->wwwroot . "/mod/jinotechboard/recommend.php?b=" . $board->id . "&boardform=" . $boardform . "&contentId=" . $content->id . "' }"));
} else if ($board->allowrecommend == 1 && $recommended) {
    $left_btns .= html_writer::tag('button', get_string('recommended', 'jinotechboard'), array('class' => "btns h40 br", 'style' => 'cursor:pointer;'));
}

$right_btns = "";

if (has_capability('mod/jinotechboard:edit', $context) || $USER->id == $content->userid) {
    $edit_url = $CFG->wwwroot . "/mod/jinotechboard/write.php?b=" . $board->id . "&contentId=" . $content->id . "&boardform=" . $boardform . '&mode=edit' . $search_param;
    $right_btns .= html_writer::tag('button', get_string('edit', 'jinotechboard'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $edit_url . '"', "class" => "btns h40 br"));
}
if (has_capability('mod/jinotechboard:delete', $context) || ($USER->id == $content->userid)) {
    $right_btns .= html_writer::tag('button', get_string('delete', 'jinotechboard'), array("class" => "btns h40 br", 'style' => 'cursor:pointer;', 'onclick' => 'if(confirm("삭제하시겠습니까?")){ location.href="' . $CFG->wwwroot . "/mod/jinotechboard/delete.php?b=" . $board->id . "&contentId=" . $content->id . "&boardform=" . $boardform . '&mode=delete" }'));
}
$cols = html_writer::tag('div', $left_btns, array('class' => "btn-area btn-area-left"));
$cols .= html_writer::tag('div', $right_btns, array('class' => "btn-area btn-area-right"));
$output .= html_writer::tag('div', $cols, array('class' => "table-footer-area"));


//댓글영역

if ($board->allowcomment == 1) {
    $output .= html_writer::start_tag('div', array('class' => 'table-reply-area'));
    $output .= html_writer::start_tag('form', array('method' => 'get', 'id' => 'comment_form', 'class' => 'reply', 'onsubmit' => 'return chkform();', 'action' => 'comment_write.php?b=' . $board->id));
    $input = html_writer::tag('textarea', '', array('id' => 'comment_textarea', 'name' => 'comment_value','maxlength'=>'300'));
    $cols = html_writer::tag('span', $input, array('class' => "option"));
    $btn .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'contentId', 'value' => $content->id));
    $btn .= html_writer::tag('input', '', array('id' => 'comment_submit', 'type' => 'submit', 'class' => 'reply-submit', 'value' => get_string('reply', 'jinotechboard')));
    $cols .= html_writer::tag('span', $btn, array('class' => "option"));
    $output .= html_writer::tag('div', $cols, array('class' => "view_row"));
    $output .= html_writer::end_tag('form');

    //댓글 목록 영역
    if ($comments = $DB->get_records('jinotechboard_comments', array('jinotechboard_contents' => $contentId), 'timecreated asc')) {
        $output .= html_writer::start_tag('ul', array('class' => 'reply-list'));
        foreach ($comments as $comment) {

            $commentuser = $DB->get_record('user', array('id' => $comment->userid));
            $fullname = fullname($commentuser);
            $timecreated = date("Y-m-d H:i:s", $comment->timecreated);
            $linktext = "";
            if (has_capability('mod/jinotechboard:commentdelete', $context) || $USER->id == $comment->userid) {
                $linktext = html_writer::tag('span', get_string('delete', 'jinotechboard'), array('class' => "bluetone", 'onclick' => "if(confirm('삭제하시겠습니까?')){ location.href='" . $CFG->wwwroot . "/mod/jinotechboard/comment_delete.php?contentid=" . $content->id . "&id=" . $comment->id . "' }"));
            }

            $p = html_writer::tag('p', text_to_html($comment->comments) . "&nbsp;" . $linktext, array('class' => "value", 'colspan' => '3'));
            $span = html_writer::tag('span', $fullname . " | " . $timecreated, array('class' => "comment_author"));
            $output .= html_writer::tag('li', $p . $span, array('class' => "comment_row"));
        }
        $output .= html_writer::end_tag('ul');
    }
    $output .= html_writer::end_tag('div');
}

//출력


$param = array('ref' => $content->ref, 'board' => $board->id);
if ($searchvalue) {
    switch ($searchfield) {
        case 1:
            $where = "and title LIKE :title"; //
            $param['title'] = '%' . $searchvalue . '%';
            break;
        case 2:
            $where = "and u.firstname || u.lastname LIKE :fullname"; //
            $param['fullname'] = '%' . $searchvalue . '%';
            break;
        case 3:
            $where = "and contents LIKE :contents";
            $param['contents'] = '%' . $searchvalue . '%';
            break;
    }
}

$next_sql = "select /*+ INDEX_ASC (M_JINOCONT_REF_IX)  */ min(ref) from {jinotechboard_contents} "
        . "left join {user} u on u.id = userid "
        . "where lev = 0 and ref > :ref and board= :board ".$where;
$prev_sql = "select /*+ INDEX_DESC (M_JINOCONT_REF_IX)  */ max(ref) from {jinotechboard_contents} "
        . "left join {user} u on u.id = userid "
        . "where lev = 0 and ref < :ref and board= :board ".$where;
// $next_sql = "select ref from {jinotechboard_contents} where lev = 0 and ref > :ref and board= :board ".$where." order by ref asc";
// $prev_sql = "select ref from {jinotechboard_contents} where lev = 0 and ref < :ref and board= :board ".$where." order by ref desc ";
//$next_sql = "select min(ref)keep(dense_rank first order by ref asc)as ref from {jinotechboard_contents} where lev = 0 and ref > :ref and board= :board"; 
//$prev_sql = "select min(ref)keep(dense_rank first order by ref desc)as ref from {jinotechboard_contents} where lev = 0 and ref < :ref and board= :board ";
$next_contentref = $DB->get_field_sql($next_sql, $param);
$prev_contentref = $DB->get_field_sql($prev_sql, $param);

//$output .= '<table class="generaltable"><thead><tr>';
//$output .= '<th class="col-1" style="width:5%"> </th>';
//$output .= '<th class="col-2">' . get_string("title", 'jinotechboard') . '</th>';
//$output .= '<th class="col-3" style="width:20%">' . get_string("writer", 'jinotechboard') . '</th>';
//$output .= '<th class="col-4" style="width:10%" class="mobile">' . get_string("date", 'jinotechboard') . '</th>';
//$output .= '<th class="col-5" style="width:10%" class="mobile">' . get_string("view:cnt", 'jinotechboard') . '</th>';
//
//$next_num = $list_num-1;
//$prev_num = $list_num+1;
//
//jinotechboard_print_contents($next_contentref,'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="next" />', $boardform,$search_param,$next_num);
//jinotechboard_print_contents($content->ref,'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="current" />', $boardform,$search_param,$list_num);
//jinotechboard_print_contents($prev_contentref,'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="prev" />', $boardform,$search_param,$prev_num);
//
//$output .= "</table>";

echo $output;
?>

<form name="delForm" method="post">
    <input type="hidden" name="b" value="<?php echo($board->id); ?>">
    <input type="hidden" name="contentId" value="<?php echo($contentId); ?>">
</form>

</div>
<script type="text/javascript">
    function view_users(id){
        window.open('view_users.php?id='+id,'view_users','width=500,height=500');
    }
    function chkform() {
        if ($('#comment_textarea').val() == "") {
            alert("<?php echo get_string('enterstr_text', 'jinotechboard'); ?>");
            $('#comment_textarea').focus();
            return false;
        } else {
            return true;
        }
    }
    $("document").ready(function() {  
        var currentstr = 0;
        var mx = $("#comment_textarea").attr('maxlength');
        $('#comment_textarea').keyup(function(){
                var va = $(this).val().length;
                if(currentstr != va){
                    if(mx <= va){
                        alert('<?php echo get_string('maxlength','jinotechboard');?>');
                    }
                }
                    currentstr = va;    
        })
    }) 
</script>
<?php echo $OUTPUT->footer(); ?>