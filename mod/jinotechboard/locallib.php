<?php



require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
require_once($CFG->libdir . '/portfolio/caller.php');
require_once($CFG->libdir . '/filebrowser/file_info.php');

class jinotechboard_portfolio_caller extends portfolio_module_caller_base {

    protected $contentId;
    protected $attachment;

    private $board;
    private $content;
    private $keyedfiles; 

    public static function expected_callbackargs() {
        return array(
            'contentId'       => false,
            'attachment'   => false,
        );
    }
    function __construct($callbackargs) {
        parent::__construct($callbackargs);
//        if (!$this->contentId) {
//            throw new portfolio_caller_exception('mustprovidediscussionorpost', 'jinotechboard');
//        }
    }
    public function load_data() {
        global $DB;

        if ($this->contentId) {
            if (!$this->content = $DB->get_record('jinotechboard_contents', array('id' => $this->contentId))) {
                throw new portfolio_caller_exception('invalidcontentId', 'jinotechboard');
            }
        }

        $dparams = array();
        if ($this->contentId) {
            $dbparams = array('id' => $this->contentId);
        } else {
            throw new portfolio_caller_exception('mustprovidediscussionorpost', 'jinotechboard');
        }

        if (!$this->board = $DB->get_record('jinotechboard', array('id' => $this->content->board))) {
            throw new portfolio_caller_exception('invalidboardid', 'board');
        }

        if (!$this->cm = get_coursemodule_from_instance('jinotechboard', $this->board->id)) {
            throw new portfolio_caller_exception('invalidcoursemodule');
        }

        $this->modcontext = get_context_instance(CONTEXT_MODULE, $this->cm->id);
        $fs = get_file_storage();
        if ($this->content) {
            if ($this->attachment) {
                $this->set_file_and_format_data($this->attachment);
            } else {
                $attach = $fs->get_area_files($this->modcontext->id, 'mod_jinotechboard', 'attachment', $this->content->id, 'timemodified', false);
                $embed  = $fs->get_area_files($this->modcontext->id, 'mod_jinotechboard', 'post', $this->content->id, 'timemodified', false);
                $files = array_merge($attach, $embed);
                $this->set_file_and_format_data($files);
            }
            if (!empty($this->multifiles)) {
                $this->keyedfiles[$this->content->id] = $this->multifiles;
            } else if (!empty($this->singlefile)) {
                $this->keyedfiles[$this->content->id] = array($this->singlefile);
            }
        }
        if (empty($this->multifiles) && !empty($this->singlefile)) {
            $this->multifiles = array($this->singlefile); 
        }
        
        if (empty($this->attachment)) {
            if (!empty($this->multifiles)) {
                $this->add_format(PORTFOLIO_FORMAT_RICHHTML);
            } else {
                $this->add_format(PORTFOLIO_FORMAT_PLAINHTML);
            }
        }
    }

    function get_return_url() {
        global $CFG;
        return $CFG->wwwroot . '/mod/jinotechboard/content.php?contentId=' . $this->content->id."&b=".$this->content->board;
    }
    function get_navigation() {
        global $CFG;

        $navlinks = array();
        $navlinks[] = array(
            'name' => format_string($this->content->name),
            'link' => $CFG->wwwroot . '/mod/jinotechboard/content.php?contentId=' . $this->content->id,
            'type' => 'title'
        );
        return array($navlinks, $this->cm);
    }
    function prepare_package() {
        global $CFG;

        $writingleap = false;
        if ($this->exporter->get('formatclass') == PORTFOLIO_FORMAT_LEAP2A) {
            $leapwriter = $this->exporter->get('format')->leap2a_writer();
            $writingleap = true;
        }
        if ($this->attachment) { 
            $this->copy_files(array($this->singlefile), $this->attachment);
            if ($writingleap) { 
                $entry = new portfolio_format_leap2a_file($this->singlefile->get_filename(), $this->singlefile);
                $leapwriter->add_entry($entry);
                return $this->exporter->write_new_file($leapwriter->to_xml(), $this->exporter->get('format')->manifest_name(), true);
            }

        } else { 
            $posthtml = $this->prepare_post($this->content);
            
            $contentTemp = $posthtml;
            $name = 'post.html';
            $manifest = ($this->exporter->get('format') instanceof PORTFOLIO_FORMAT_RICH);

            if ($writingleap) {
                $this->prepare_post_leap2a($leapwriter, $this->content, $posthtml);
                $contentTemp = $leapwriter->to_xml();
                $name = $this->exporter->get('format')->manifest_name();
            }
            $this->copy_files($this->multifiles);
            $this->get('exporter')->write_new_file($contentTemp, $name, $manifest);
        }
    }

    private function prepare_content_leap2a(portfolio_format_leap2a_writer $leapwriter, $content, $contenthtml) {
        $entry = new portfolio_format_leap2a_entry('jinotechcontent' . $content->id,  $content->title, 'resource', $contenthtml);
        $entry->published = $content->created;
        $entry->updated = $content->modified;
        $entry->author = $content->userid;
        if (is_array($this->keyedfiles) && array_key_exists($content->id, $this->keyedfiles) && is_array($this->keyedfiles[$content->id])) {
            $leapwriter->link_files($entry, $this->keyedfiles[$content->id], 'jinotechcontent' . $content->id . 'attachment');
        }
        $entry->add_category('web', 'resource_type');
        $leapwriter->add_entry($entry);
        return $entry->id;
    }

    private function copy_files($files, $justone=false) {
        if (empty($files)) {
            return;
        }
        foreach ($files as $f) {
            if ($justone && $f->get_id() != $justone) {
                continue;
            }
            $this->get('exporter')->copy_existing_file($f);
            if ($justone && $f->get_id() == $justone) {
                return true; 
            }
        }
    }
    private function prepare_post($content, $fileoutputextras=null) {
        global $DB;
        static $users;
        if (empty($users)) {
            $users = array($this->user->id => $this->user);
        }
        if (!array_key_exists($content->userid, $users)) {
            $users[$content->userid] = $DB->get_record('user', array('id' => $content->userid));
        }
    
        $content->author = $users[$content->userid];
        $viewfullnames = true;
    
        $options = portfolio_format_text_options();
        $format = $this->get('exporter')->get('format');
        $formattedtext = format_text($content->message, $content->messageformat, $options, $this->get('course')->id);
        $formattedtext = portfolio_rewrite_pluginfile_urls($formattedtext, $this->modcontext->id, 'mod_jinotechboard', 'content', $content->id, $format);

        $output = '<table border="0" cellpadding="3" cellspacing="0" class="forumpost">';

        $output .= '<tr class="header"><td>';
        $output .= '</td>';

            $output .= '<td class="topic starter">';
    
        $output .= '<div class="subject">'.format_string($content->title).'</div>';

        $fullname = fullname($users[$content->userid], $viewfullnames);
        $by = new stdClass();
        $by->name = $fullname;
        $by->date = userdate($content->modified, '', $this->user->timezone);
        $output .= '<div class="author">'.get_string('bynameondate', 'forum', $by).'</div>';

        $output .= '</td></tr>';

        $output .= '<tr><td class="left side" valign="top">';

        $output .= '</td><td class="content">';

        $output .= $formattedtext;

        if (is_array($this->keyedfiles) && array_key_exists($content->id, $this->keyedfiles) && is_array($this->keyedfiles[$content->id]) && count($this->keyedfiles[$content->id]) > 0) {
            $output .= '<div class="attachments">';
            $output .= '<br /><b>' .  get_string('attachments', 'jinotechboard') . '</b>:<br /><br />';
            foreach ($this->keyedfiles[$content->id] as $file) {
                $output .= $format->file_output($file)  . '<br/ >';
            }
            $output .= "</div>";
        }

        $output .= '</td></tr></table>'."\n\n";

        return $output;
    }
    function get_sha1() {
        $filesha = '';
        try {
            $filesha = $this->get_sha1_file();
        } catch (portfolio_caller_exception $e) { }

        if ($this->content) {
            return sha1($filesha . ',' . $this->content->title . ',' . $this->content->contnents);
        }
    }

    function expected_time() {
        $filetime = $this->expected_time_file();
        
        return $filetime;
    }
    function check_permissions() {
        $context = get_context_instance(CONTEXT_MODULE, $this->cm->id);
        if ($this->content) {
            return (has_capability('mod/jinotechboard:exportcontent', $context)
                || ($this->content->userid == $this->user->id
                    ));
        }
        return has_capability('mod/jinotechboard:exportcontent', $context);
    }
    public static function display_name() {
        return get_string('modulename', 'jinotechboard');
    }

    public static function base_supported_formats() {
        return array(PORTFOLIO_FORMAT_FILE, PORTFOLIO_FORMAT_RICHHTML, PORTFOLIO_FORMAT_PLAINHTML, PORTFOLIO_FORMAT_LEAP2A);
    }
}


class jinotechboard_file_info_container extends file_info {

    protected $browser;

    protected $course;

    protected $cm;

    protected $component;

    protected $context;

    protected $areas;

    protected $filearea;

    public function __construct($browser, $course, $cm, $context, $areas, $filearea) {
        parent::__construct($browser, $context);
        $this->browser = $browser;
        $this->course = $course;
        $this->cm = $cm;
        $this->component = 'mod_jinotechboard';
        $this->context = $context;
        $this->areas = $areas;
        $this->filearea = $filearea;
    }

    public function get_params() {
        return array(
            'contextid' => $this->context->id,
            'component' => $this->component,
            'filearea' => $this->filearea,
            'itemid' => null,
            'filepath' => null,
            'filename' => null,
        );
    }

    public function is_writable() {
        return false;
    }

    public function is_directory() {
        return true;
    }

    public function get_visible_name() {
        return $this->areas[$this->filearea];
    }

    public function get_children() {
        global $DB;

        $children = array();
        $itemids = $DB->get_records('files', array('contextid' => $this->context->id, 'component' => $this->component,
            'filearea' => $this->filearea), 'itemid DESC', "DISTINCT itemid");
        foreach ($itemids as $itemid => $unused) {
            if ($child = $this->browser->get_file_info($this->context, 'mod_jinotechboard', $this->filearea, $itemid)) {
                $children[] = $child;
            }
        }

        return $children;
    }

    public function get_parent() {
        return $this->browser->get_file_info($this->context);
    }
}
