<?php

require_once($CFG->dirroot . '/user/selector/lib.php');
require_once($CFG->libdir.'/eventslib.php');

define('JINOTECHBOARD_MODE_FLATOLDEST', 1);
define('JINOTECHBOARD_MODE_FLATNEWEST', -1);
define('JINOTECHBOARD_MODE_THREADED', 2);
define('JINOTECHBOARD_MODE_NESTED', 3);

define('JINOTECHBOARD_CHOOSESUBSCRIBE', 0);
define('JINOTECHBOARD_FORCESUBSCRIBE', 1);
define('JINOTECHBOARD_INITIALSUBSCRIBE', 2);
define('JINOTECHBOARD_DISALLOWSUBSCRIBE', 3);

define('JINOTECHBOARD_TRACKING_OFF', 0);
define('JINOTECHBOARD_TRACKING_OPTIONAL', 1);
define('JINOTECHBOARD_TRACKING_ON', 2);

define('JINOTECHBOARD_BOARDFORM_THREAD', 1);
define('JINOTECHBOARD_BOARDFORM_GENERAL', 2);
define('JINOTECHBOARD_THREAD_DEFAULT', 20);

define('JINOTECHBOARD_INDENT_DEFAULT', 4);

function jinotechboard_supports($feature) {
    switch ($feature) {
        case FEATURE_GROUPS: return true;
        case FEATURE_GROUPINGS: return true;
        case FEATURE_GROUPMEMBERSONLY: return true;
        case FEATURE_MOD_INTRO: return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES: return true; // compl
        case FEATURE_GRADE_HAS_GRADE: return true;
        case FEATURE_GRADE_OUTCOMES: return true;
        case FEATURE_RATE: return true;
        case FEATURE_BACKUP_MOODLE2: return true;
        case FEATURE_SHOW_DESCRIPTION: return true;
    }
}

function jinotechboard_add_instance($board) {
    global $CFG, $DB;



    $board->timecreated = time();
    $board->timemodified = time();

    if (empty($board->setmaxbyte)) {
        $board->maxbytes = $CFG->board_normalmaxbytes;
    } else {
        $board->maxbytes = $CFG->board_massmaxbytes;
    }

    $board->id = $DB->insert_record('jinotechboard', $board);


    return $board->id;
}

function jinotechboard_update_instance($board) {

    global $USER, $DB, $CFG;
    $board->timemodified = time();
    $board->id = $board->instance;

    if (empty($board->setmaxbyte)) {
        $board->maxbytes = $CFG->board_normalmaxbytes;
    } else {
        $board->maxbytes = $CFG->board_massmaxbytes;
    }

    $oldboard = $DB->get_record('jinotechboard', array('id' => $board->id));

    if (!$DB->update_record('jinotechboard', $board)) {
        print_error('Can not update board');
    }

    return true;
}

function jinotechboard_delete_instance($id) {

    global $cm, $DB;

    if (!$board = $DB->get_record('jinotechboard', array('id' => $id))) {
        return false;
    }


    $DB->delete_records('jinotechboard_comments', array('board' => $id));

    $DB->delete_records('jinotechboard_contents', array('board' => $id));



    if (!$DB->delete_records('jinotechboard', array('id' => $id))) {
        return false;
    }

    return true;
}

function jinotechboard_print_discussions($page, $perpage, $totalcount, $cm, $board = null, $boardsort = "st.ref DESC, st.step asc", $searchfield = "1", $searchvalue = "", $capability = true, $boardform = 1) {
    global $CFG, $PAGE, $USER, $OUTPUT, $DB, $COURSE;
    $thid = "";

    $classname = context_helper::get_class_for_level(CONTEXT_MODULE);
    $contexts[$cm->id] = $classname::instance($cm->id);
    $context = $contexts[$cm->id];

    if ($board->allowrecommend) {
        $recommend = true;
    } else {
        $recommend = false;
    }
 
    echo '<table class="table m-block">' .
    '<thead>' .
    '<tr>' .
    '<th width="30px" class="m-hide" nowrap>' . get_string("no.", 'jinotechboard') . '</th>' .
    '<th class="title">' . get_string("title", 'jinotechboard') . '</th>' .
    '<th width="120px" class="m-hide" nowrap>' . get_string("writer", 'jinotechboard') . '</th>' .
    '<th width="80px" nowrap>' . get_string("date", 'jinotechboard') . '</th>' .
    '<th width="30px" nowrap>' . get_string("viewCount", 'jinotechboard') . '</th>';
    if ($recommend) {
        echo ' <th width="30px" class="m-hide" nowrap>' . get_string("reCommondCnt", 'jinotechboard') . '</th>';
    }
    if ($board->allowcomment) {
        echo ' <th width="30px" class="m-hide" nowrap>' . get_string("reply", 'jinotechboard') . '</th>';
    }
    echo '  </tr>
            </thead>
        <tbody>';



    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }
    $list_num = $offset;

    $contentsRS = jinotechboard_get_discussions($cm, $boardsort, $searchfield, $searchvalue, $page, $perpage, false, $capability, true);
    $num = $totalcount - $offset;
    if (!$contentsRS) {
        $colspan = 5;
        if ($board->allowcomment) {
            $colspan++;
        }
        if ($recommend) {
            $colspan++;
        }

        echo '<tr><td style="line-height:25px;" colspan="' . $colspan . '" align=center>' . get_string("nocontent", 'jinotechboard') . '</td></tr>';
    } else {
        $rowi = 0;
        $contentsNoticeRs = jinotechboard_get_discussions($cm, $boardsort, $searchfield, $searchvalue, 0, 0, true, $capability, false);
        foreach ($contentsNoticeRs as $notice) {
            if($board->type == 1){
                require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                //콘텐츠 변수처리
                $vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
                $vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
                $data['courseid'] = $COURSE->id;
                $data['userid'] = $USER->id;
                $data['contents'] =$content->contents;
                $data['title'] =$content->title;
//                $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
                $content->contents = $data['contents'];
                $content->title = $data['title'];
            }
            if($notice->lev == 0){
                //$copybox = "<td><input type='checkbox' name='contests_copy' value='".$notice->id."'></td>";
            }
            echo "<tr class='isnotice'>".$copybox."
                    <td class='m-hide'></td>
                    <td class='title text-left'>
                        <a onclick='contentMove(\"" . $notice->id . "\")' style='cursor: pointer;'>" . $notice->title . "</a> 
                        " . ($notice->itemid > 0 ? "<img src=" . $CFG->wwwroot . "/mod/jinotechboard/pix/attached.png>" : "") . "
                    </td>";
            echo "<td class='m-hide'>" . fullname($notice) . "</td>";
            echo "<td>" . date('Y-n-d', $notice->timemodified) . "</td>";
            echo "<td class='m-f-r'><span class='m-show inline'>" . get_string("viewCount", 'jinotechboard') . " </span>" . $notice->viewcnt . "</td>";
            if ($recommend) {
                echo "<td class='m-hide'>" . $notice->recommendcnt . "</td>";
            }
            if ($board->allowcomment) {
                echo "<td class='m-hide'>" . $notice->commentscount . "</td>";
            }
            echo "</tr>";

            $rowi ++;
        }

        foreach ($contentsRS as $content) {
            if($board->type == 1){
                require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                //콘텐츠 변수처리
                $vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
                $vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
                $data['courseid'] = $COURSE->id;
                $data['userid'] = $USER->id;
                $data['contents'] =$content->contents;
                $data['title'] =$content->title;
//                $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
                $content->contents = $data['contents'];
                $content->title = $data['title'];
            }
            $list_num++;
            $userfrom = $DB->get_record('user', array('id' => $content->userid));
            $fs = get_file_storage();
            $files = $fs->get_area_files($context->id, 'mod_jinotechboard', 'attachment', $content->id, 'id', false);
            $attachments = "";
            if (count($files) > 0) {
                $attachments = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
            }
            
            if($content->lev == 0){
                //$copybox = "<td><input type='checkbox' name='contests_copy' value='".$content->id."'></td>";
            }

            echo "<tr>".$copybox."
                    <td class='m-hide item'>" . $num. "</td> 
                    <td class='title text-left'>
                  ";

            $step_left_len = "";
            if ($content->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
                $step = $content->lev - 1;
                $step_left_len = $step * 15;
                $date_left_len = ($step + 1) * 20;
            } else {
                $step = JINOTECHBOARD_INDENT_DEFAULT - 1;
                $step_left_len = $step * 15;
                $date_left_len = ($step + 1) * 20;
            }

            $step_left = 'style="padding-left:' . $step_left_len . 'px;"';
            $parentid = "";
            $step_icon = "";
            if ($content->lev != 0) {
                $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="" ' . $step_left . '/> ';
                $parent = $DB->get_record('jinotechboard_contents', array('id' => $content->ref));
                $parentid = $parent->userid;
            }
            if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $PAGE->cm->context) && $USER->id != $content->userid && $parentid != $USER->id) {
                echo "<a href='#' onclick='" . 'alert("' . get_string('issecret', 'jinotechboard') . '")' . "' $step_left><div style='float:left'>" . $content->title . "</div>";
            } else {
                echo '<a onclick="contentMove(\'' . $content->id . '\',\'' . $list_num . '\')" style="cursor: pointer;"><div style="float:left">' . $step_icon . $content->title . "</div>";
            }
            echo "&nbsp;" . $attachments;
            if ($content->isprivate) {
                echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
            }
            echo "</a>";


            echo "  </td>
                    <td class='m-hide item'>" . fullname($content) . "</td>";
            echo "<td class='item'>" . date('Y-n-d', $content->timemodified) . "</td>";
           echo "<td class='m-f-r'><span class='m-show inline'>" . get_string("viewCount", 'jinotechboard') . " </span>" . $content->viewcnt . "</td>";
            if ($recommend) {
                echo "<td class='m-hide item'>" . $content->recommendcnt . "</td>";
            }
            if ($board->allowcomment) {
                echo "<td class='m-hide item'>" . $content->commentscount . "</td>";
            }

            echo "</tr>";
            $num--;
            $rowi ++;
        }
    }
    echo "</tbody></table>
    ";
}

function jinotechboard_get_discussions_count($cm, $searchfield = "", $searchvalue = "", $capability = true) {
    global $CFG, $DB, $USER;

    $context = context_module::instance($cm->id);

    $where = "";
    $params = array(
        'contextid' => $context->id,
        'boardid' => $cm->instance);

    if (!empty($searchvalue)) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.fullname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }

    if (!$capability) {
        $where = " and st.userid = :userid ";
        $params['userid'] = $USER->id;
    }

    $sql = "select count(*) from 
                    (SELECT jcb.*, u.firstname || u.lastname as fullname, u.firstname, u.lastname, (SELECT count(*) FROM {files} f WHERE f.contextid = :contextid AND f.filearea = 'attachment' AND f.filesize != 0 and f.itemid = jcb.id ) as itemid 
                    from {jinotechboard_contents} jcb  
                    left join {user} u on jcb.userid = u.id) st
                where st.board = :boardid " . $where;


    return $DB->count_records_sql($sql, $params);
}

function jinotechboard_get_total_search_discussions($boardsort = "jcb.ref DESC, step asc", $searchvalue = "", $page, $perpage) {
    global $CFG, $DB, $USER;

    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $params = array();


    if (isNullOrEmptyStr($searchvalue)) {
        $sql = "SELECT jcb.*, u.firstname, u.lastname, c.title ctitle, f.itemid 
                from {jinotechboard_contents} jcb
                left join {user} u on jcb.userid = u.id 
                left join (SELECT  itemid FROM {files} 
                    WHERE component = 'mod_jinotechboard' AND filesize != 0) f ON jcb.id = f.itemid
        
            where jcb.board in ($CFG->NOTICEID, $CFG->DATAID) order by " . $boardsort;
    } else {
        if ($searchvalue) {
            $where = "and title || contents LIKE ?"; //
            $params[] = '%' . $searchvalue . '%';
        }

        $sql = "SELECT jcb.*, u.firstname, u.lastname,c.title ctitle, f.itemid 
                from {jinotechboard_contents} jcb  
                left join {user} u on jcb.userid = u.id 
                left join (SELECT  itemid FROM {files} WHERE component = 'mod_jinotechboard' AND filesize != 0) f ON jcb.id = f.itemid 
                where jcb.board in ($CFG->NOTICEID, $CFG->DATAID) order by " . $boardsort;
    }

    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function jinotechboard_get_discussions($cm, $boardsort = "st.ref DESC, st.step asc", $searchfield = "", $searchvalue = "", $page, $perpage, $isNotice = false, $capability = true) {
    global $CFG, $DB, $USER;

    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $context = context_module::instance($cm->id);

    $params = array(
        'contextid' => $context->id,
        'boardid' => $cm->instance);

    $addNoticeSql = "";
    $where = "";
    if ($isNotice) {
        $addNoticeSql = "and st.isnotice = 1 ";
    }

    if (!$capability) {
        $where = " and st.userid = :userid ";
        $params['userid'] = $USER->id;
    }

    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.fullname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }


    $sql = "select st.* from 
                    (SELECT jcb.*, u.firstname || u.lastname as fullname, u.firstname, u.lastname, (SELECT count(*) FROM {files} f WHERE f.contextid = :contextid AND f.filearea = 'attachment' AND f.filesize != 0 and f.itemid = jcb.id ) as itemid 
                    from {jinotechboard_contents} jcb  
                    left join {user} u on jcb.userid = u.id) st
                where st.board = :boardid " . $addNoticeSql . $where . " order by " . $boardsort;


    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function isNullOrEmptyStr($str) {
    if (!isset($str) || $str == NULL || $str == '')
        return true;
    else
        return false;
}

/**
 * 리스트 테이블 밑에 들어가는 페이지 네비게이션을 출력한다.
 * @param string $url
 * @param array $params
 * @param int $total_pages
 * @param int $current_page
 * @param int $max_nav 네비게이션에 표시한 최대 페이지 수
 */
function jinotechboard_get_paging_bar($url, $params, $total_pages, $current_page, $max_nav = 10) {
    $total_nav_pages = jinotechboard_get_total_pages($total_pages, $max_nav);
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }

    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = "page=";
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . "?page=";
    }

    echo '<div class="board-breadcrumbs" >';

    if ($current_page > 1) {
        echo '<span class="board-nav-prev"><a class="prev" href="' . $url . ($current_page - 1) . '"><</a></span>';
    } else {
        echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    echo '<ul>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="current"><a href="#">' . $i . '</a></li>';
        } else {
            echo '<li><a href="' . $url . '' . $i . '">' . $i . '</a></li>';
        }
    }
    echo '</ul>';
    if ($current_page < $total_pages) {
        echo '<span class="board-nav-next"><a class="next" href="' . $url . ($current_page + 1) . '">></a></span>';
    } else {
        echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
    }

    echo '</div>';
}

/**
 * 데이터를 표시하기 위한 페이지 수를 계산한다.
 * @param int $rows
 * @param int $limit
 * @return int
 */
function jinotechboard_get_total_pages($rows, $limit = 10) {
    if ($rows == 0) {
        return 1;
    }

    $total_pages = (int) ($rows / $limit);

    if (($rows % $limit) > 0) {
        $total_pages += 1;
    }

    return $total_pages;
}

function jinotechboard_get_categories($cm, $b, $boardsort = "orderseq asc, title asc") {
    global $DB;

    $sql = "SELECT * from {jinotechboard_category} where board = ? order by " . $boardsort;

    return $DB->get_records_sql($sql, array($b));
}

/**
 * Adds module specific settings to the settings block
 *
 * @param settings_navigation $settings The settings navigation object
 * @param navigation_node $jinotechboardnode The node to add module settings to
 */
//
//function jinotechboard_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $jinotechboardnode) {
//    global $USER, $PAGE, $CFG, $DB, $OUTPUT;
//
//    $jinotechboardobject = $DB->get_record("jinotechboard", array("id" => $PAGE->cm->instance));
//    if (empty($PAGE->cm->context)) {
//        $PAGE->cm->context = get_context_instance(CONTEXT_MODULE, $PAGE->cm->instance);
//    }
//
//    // for some actions you need to be enrolled, beiing admin is not enough sometimes here
//    $enrolled = is_enrolled($PAGE->cm->context, $USER, '', false);
//    $activeenrolled = is_enrolled($PAGE->cm->context, $USER, '', true);
//
//    $canmanage = has_capability('mod/jinotechboard:managesubscriptions', $PAGE->cm->context);
//    $subscriptionmode = jinotechboard_get_forcesubscribed($jinotechboardobject);
//    $cansubscribe = ($activeenrolled && $subscriptionmode != JINOTECHBOARD_FORCESUBSCRIBE && ($subscriptionmode != JINOTECHBOARD_DISALLOWSUBSCRIBE || $canmanage));
//
//    if ($canmanage) {
//        $mode = $jinotechboardnode->add(get_string('subscriptionmode', 'forum'), null, navigation_node::TYPE_CONTAINER);
//
//        $allowchoice = $mode->add(get_string('subscriptionoptional', 'forum'), new moodle_url('/mod/jinotechboard/subscribe.php', array('id' => $jinotechboardobject->id, 'mode' => JINOTECHBOARD_CHOOSESUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
//        $forceforever = $mode->add(get_string("subscriptionforced", "forum"), new moodle_url('/mod/jinotechboard/subscribe.php', array('id' => $jinotechboardobject->id, 'mode' => JINOTECHBOARD_FORCESUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
//        $forceinitially = $mode->add(get_string("subscriptionauto", "forum"), new moodle_url('/mod/jinotechboard/subscribe.php', array('id' => $jinotechboardobject->id, 'mode' => JINOTECHBOARD_INITIALSUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
//        $disallowchoice = $mode->add(get_string('subscriptiondisabled', 'forum'), new moodle_url('/mod/jinotechboard/subscribe.php', array('id' => $jinotechboardobject->id, 'mode' => JINOTECHBOARD_DISALLOWSUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
//
//        switch ($subscriptionmode) {
//            case JINOTECHBOARD_CHOOSESUBSCRIBE : // 0
//                $allowchoice->action = null;
//                $allowchoice->add_class('activesetting');
//                break;
//            case JINOTECHBOARD_FORCESUBSCRIBE : // 1
//                $forceforever->action = null;
//                $forceforever->add_class('activesetting');
//                break;
//            case JINOTECHBOARD_INITIALSUBSCRIBE : // 2
//                $forceinitially->action = null;
//                $forceinitially->add_class('activesetting');
//                break;
//            case JINOTECHBOARD_DISALLOWSUBSCRIBE : // 3
//                $disallowchoice->action = null;
//                $disallowchoice->add_class('activesetting');
//                break;
//        }
//    } else if ($activeenrolled) {
//
//        switch ($subscriptionmode) {
//            case JINOTECHBOARD_CHOOSESUBSCRIBE : // 0
//                $notenode = $jinotechboardnode->add(get_string('subscriptionoptional', 'forum'));
//                break;
//            case JINOTECHBOARD_FORCESUBSCRIBE : // 1
//                $notenode = $jinotechboardnode->add(get_string('subscriptionforced', 'forum'));
//                break;
//            case JINOTECHBOARD_INITIALSUBSCRIBE : // 2
//                $notenode = $jinotechboardnode->add(get_string('subscriptionauto', 'forum'));
//                break;
//            case JINOTECHBOARD_DISALLOWSUBSCRIBE : // 3
//                $notenode = $jinotechboardnode->add(get_string('subscriptiondisabled', 'forum'));
//                break;
//        }
//    }
//
//    if (has_capability('mod/jinotechboard:viewsubscribers', $PAGE->cm->context)) {
//        $url = new moodle_url('/mod/jinotechboard/subscribers.php', array('id' => $jinotechboardobject->id));
//        $jinotechboardnode->add(get_string('showsubscribers', 'forum'), $url, navigation_node::TYPE_SETTING);
//    }
//
//    /* if ($enrolled && forum_tp_can_track_forums($jinotechboardobject)) { // keep tracking info for users with suspended enrolments
//      if ($jinotechboardobject->trackingtype != JINOTECHBOARD_TRACKING_OPTIONAL) {
//      //tracking forced on or off in forum settings so dont provide a link here to change it
//      //could add unclickable text like for forced subscription but not sure this justifies adding another menu item
//      } else {
//      if (forum_tp_is_tracked($jinotechboardobject)) {
//      $linktext = get_string('notrackforum', 'forum');
//      } else {
//      $linktext = get_string('trackforum', 'forum');
//      }
//      $url = new moodle_url('/mod/forum/settracking.php', array('id'=>$jinotechboardobject->id));
//      $jinotechboardnode->add($linktext, $url, navigation_node::TYPE_SETTING);
//      }
//      } */
//
//    if (!isloggedin() && $PAGE->course->id == SITEID) {
//        $userid = guest_user()->id;
//    } else {
//        $userid = $USER->id;
//    }
//
//    /* $hascourseaccess = ($PAGE->course->id == SITEID) || can_access_course($PAGE->course, $userid);
//      $enablerssfeeds = !empty($CFG->enablerssfeeds) && !empty($CFG->forum_enablerssfeeds);
//
//      if ($enablerssfeeds && $jinotechboardobject->rsstype && $jinotechboardobject->rssarticles && $hascourseaccess) {
//
//      if (!function_exists('rss_get_url')) {
//      require_once("$CFG->libdir/rsslib.php");
//      }
//
//      if ($jinotechboardobject->rsstype == 1) {
//      $string = get_string('rsssubscriberssdiscussions','forum');
//      } else {
//      $string = get_string('rsssubscriberssposts','forum');
//      }
//
//      $url = new moodle_url(rss_get_url($PAGE->cm->context->id, $userid, "mod_forum", $jinotechboardobject->id));
//      $jinotechboardnode->add($string, $url, settings_navigation::TYPE_SETTING, null, null, new pix_icon('i/rss', ''));
//      } */
//}

/**
 * @global object
 * @param object $forum
 * @return bool
 */
function jinotechboard_is_forcesubscribed($jinotechboard) {
    global $DB;
    if (isset($jinotechboard->forcesubscribe)) {    // then we use that
        return ($jinotechboard->forcesubscribe == JINOTECHBOARD_FORCESUBSCRIBE);
    } else {   // Check the database
        return ($DB->get_field('forum', 'forcesubscribe', array('id' => $jinotechboard)) == JINOTECHBOARD_FORCESUBSCRIBE);
    }
}

function jinotechboard_get_forcesubscribed($jinotechboard) {
    global $DB;
    if (isset($jinotechboard->forcesubscribe)) {    // then we use that
        return $jinotechboard->forcesubscribe;
    } else {   // Check the database
        return $DB->get_field('jinotechboard', 'forcesubscribe', array('id' => $jinotechboard));
    }
}

/**
 * @global object
 * @param int $forumid
 * @param mixed $value
 * @return bool
 */
function jinotechboard_forcesubscribe($jinotechboardid, $value = 1) {
    global $DB;
    return $DB->set_field("jinotechboard", "forcesubscribe", $value, array("id" => $jinotechboardid));
}

/**
 * @global object
 * @global object
 */
function jinotechboard_set_return() {
    global $CFG, $SESSION;

    if (!isset($SESSION->fromjinotechboard)) {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = "";
        }
        // If the referer is NOT a login screen then save it.
        if (!strncasecmp("$CFG->wwwroot/login", $referer, 300)) {
            $SESSION->fromjinotechboard = $_SERVER["HTTP_REFERER"];
        }
    }
}

/**
 * Abstract class used by jinotechboard subscriber selection controls
 * @package mod-jinotechboard
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class jinotechboard_subscriber_selector_base extends user_selector_base {

    /**
     * The id of the jinotechboard this selector is being used for
     * @var int
     */
    protected $jinotechboardid = null;

    /**
     * The context of the jinotechboard this selector is being used for
     * @var object
     */
    protected $context = null;

    /**
     * The id of the current group
     * @var int
     */
    protected $currentgroup = null;

    /**
     * Constructor method
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options) {
        $options['accesscontext'] = $options['context'];
        parent::__construct($name, $options);
        if (isset($options['context'])) {
            $this->context = $options['context'];
        }
        if (isset($options['currentgroup'])) {
            $this->currentgroup = $options['currentgroup'];
        }
        if (isset($options['jinotechboardid'])) {
            $this->jinotechboardid = $options['jinotechboardid'];
        }
    }

    /**
     * Returns an array of options to seralise and store for searches
     *
     * @return array
     */
    protected function get_options() {
        global $CFG;
        $options = parent::get_options();
        $options['file'] = substr(__FILE__, strlen($CFG->dirroot . '/'));
        $options['context'] = $this->context;
        $options['currentgroup'] = $this->currentgroup;
        $options['jinotechboardid'] = $this->jinotechboardid;
        return $options;
    }

}

/**
 * A user selector control for potential subscribers to the selected jinotechboard
 * @package mod-jinotechboard
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class jinotechboard_potential_subscriber_selector extends jinotechboard_subscriber_selector_base {

    /**
     * If set to true EVERYONE in this course is force subscribed to this jinotechboard
     * @var bool
     */
    protected $forcesubscribed = false;

    /**
     * Can be used to store existing subscribers so that they can be removed from
     * the potential subscribers list
     */
    protected $existingsubscribers = array();

    /**
     * Constructor method
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options) {
        parent::__construct($name, $options);
        if (isset($options['forcesubscribed'])) {
            $this->forcesubscribed = true;
        }
    }

    /**
     * Returns an arary of options for this control
     * @return array
     */
    protected function get_options() {
        $options = parent::get_options();
        if ($this->forcesubscribed === true) {
            $options['forcesubscribed'] = 1;
        }
        return $options;
    }

    /**
     * Finds all potential users
     *
     * Potential users are determined by checking for users with a capability
     * determined in {@see jinotechboard_get_potential_subscribers()}
     *
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;

        $availableusers = jinotechboard_get_potential_subscribers($this->context, $this->currentgroup, $this->required_fields_sql('u'), 'u.firstname ASC, u.lastname ASC');

        if (empty($availableusers)) {
            $availableusers = array();
        } else if ($search) {
            $search = strtolower($search);
            foreach ($availableusers as $key => $user) {
                if (stripos($user->firstname, $search) === false && stripos($user->lastname, $search) === false) {
                    unset($availableusers[$key]);
                }
            }
        }

        // Unset any existing subscribers
        if (count($this->existingsubscribers) > 0 && !$this->forcesubscribed) {
            foreach ($this->existingsubscribers as $group) {
                foreach ($group as $user) {
                    if (array_key_exists($user->id, $availableusers)) {
                        unset($availableusers[$user->id]);
                    }
                }
            }
        }

        if ($this->forcesubscribed) {
            return array(get_string("existingsubscribers", 'forum') => $availableusers);
        } else {
            return array(get_string("potentialsubscribers", 'forum') => $availableusers);
        }
    }

    /**
     * Sets the existing subscribers
     * @param array $users
     */
    public function set_existing_subscribers(array $users) {
        $this->existingsubscribers = $users;
    }

    /**
     * Sets this jinotechboard as force subscribed or not
     */
    public function set_force_subscribed($setting = true) {
        $this->forcesubscribed = true;
    }

}

/**
 * @global object
 * @param string $default
 * @return string
 */
function jinotechboard_go_back_to($default) {
    global $SESSION;

    if (!empty($SESSION->fromdiscussion)) {
        $returnto = $SESSION->fromdiscussion;
        unset($SESSION->fromdiscussion);
        return $returnto;
    } else {
        return $default;
    }
}

/**
 * @global object
 * @global object
 * @global object
 * @staticvar array $cache
 * @param object $forum
 * @param object $cm
 * @param object $course
 * @return mixed
 */
function jinotechboard_count_discussions($jinotechboard, $cm, $course) {
    global $CFG, $DB, $USER;

    static $cache = array();

    $now = round(time(), -2); // db cache friendliness

    $params = array($course->id);

    if (!isset($cache[$course->id])) {
        /* if (!empty($CFG->forum_enabletimedposts)) {
          $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
          $params[] = $now;
          $params[] = $now;
          } else {
          $timedsql = "";
          } */
        $timedsql = "";

        $sql = "SELECT f.id, COUNT(f.id) as dcount
                  FROM {jinotechboard} f
                       JOIN {jinotechboard_contents} d ON d.board = f.id
                 WHERE f.course = ?
                       $timedsql
              GROUP BY f.id";

        if ($counts = $DB->get_records_sql($sql, $params)) {
            foreach ($counts as $count) {
                $counts[$count->id] = $count->dcount;
            }
            $cache[$course->id] = $counts;
        } else {
            $cache[$course->id] = array();
        }
    }

    if (empty($cache[$course->id][$jinotechboard->id])) {
        return 0;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);

    if ($groupmode != SEPARATEGROUPS) {
        return $cache[$course->id][$jinotechboard->id];
    }

    if (has_capability('moodle/site:accessallgroups', get_context_instance(CONTEXT_MODULE, $cm->id))) {
        return $cache[$course->id][$jinotechboard->id];
    }

    require_once($CFG->dirroot . '/course/lib.php');

    $modinfo = & get_fast_modinfo($course);
    if (is_null($modinfo->groups)) {
        $modinfo->groups = groups_get_user_groups($course->id, $USER->id);
    }

    if (array_key_exists($cm->groupingid, $modinfo->groups)) {
        $mygroups = $modinfo->groups[$cm->groupingid];
    } else {
        $mygroups = false; // Will be set below
    }

    // add all groups posts
    if (empty($mygroups)) {
        $mygroups = array(-1 => -1);
    } else {
        $mygroups[-1] = -1;
    }

    //   list($mygroups_sql, $params) = $DB->get_in_or_equal($mygroups);
    $params = array($jinotechboard->id);

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < $now AND (d.timeend = 0 OR d.timeend > $now)";
      $params[] = $now;
      $params[] = $now;
      } else {
      $timedsql = "";
      } */

    $sql = "SELECT COUNT(d.id)
              FROM {jinotechboard_contents} d
             WHERE d.board = ?
                   $timedsql";

    return $DB->get_field_sql($sql, $params);
}

/**
 * Returns the count of records for the provided user and forum and [optionally] group.
 *
 * @global object
 * @global object
 * @global object
 * @param object $cm
 * @param object $course
 * @return int
 */
function jinotechboard_tp_count_jinotechboard_unread_posts($cm, $course) {
    global $CFG, $USER, $DB;

    static $readcache = array();

    $jinotechboardid = $cm->instance;

    if (!isset($readcache[$course->id])) {
        $readcache[$course->id] = array();
        $counts = jinotechboard_tp_get_course_unread_posts($USER->id, $course->id);
        if ($counts) {
            foreach ($counts as $count) {
                $readcache[$course->id][$count->id] = $count->unread;
            }
        }
    }

    if (empty($readcache[$course->id][$jinotechboardid])) {
        // no need to check group mode ;-)
        return 0;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);

    if ($groupmode != SEPARATEGROUPS) {
        return $readcache[$course->id][$jinotechboardid];
    }

    if (has_capability('moodle/site:accessallgroups', get_context_instance(CONTEXT_MODULE, $cm->id))) {
        return $readcache[$course->id][$jinotechboardid];
    }

    require_once($CFG->dirroot . '/course/lib.php');

    $modinfo = & get_fast_modinfo($course);
    if (is_null($modinfo->groups)) {
        $modinfo->groups = groups_get_user_groups($course->id, $USER->id);
    }

    $mygroups = $modinfo->groups[$cm->groupingid];

    // add all groups posts
    if (empty($mygroups)) {
        $mygroups = array(-1 => -1);
    } else {
        $mygroups[-1] = -1;
    }

    list ($groups_sql, $groups_params) = $DB->get_in_or_equal($mygroups);

    $now = round(time(), -2); // db cache friendliness
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 60);
    $params = array($USER->id, $jinotechboardid, $cutoffdate);

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
      $params[] = $now;
      $params[] = $now;
      } else {
      $timedsql = "";
      } */
    $timedsql = "";

    $params = array_merge($params, $groups_params);

    $sql = "SELECT COUNT(p.id)
              FROM {jinotechboard_contents} jc
                   LEFT JOIN {forum_read} r   ON (r.jinotechcontentsid = jc.id AND r.userid = ?)
             WHERE jc.board = ?
                   AND jc.modified >= ? AND r.id is NULL
                   $timedsql
                   AND d.groupid $groups_sql";


    /*    $sql = "SELECT COUNT(p.id)
      FROM {forum_posts} p
      JOIN {forum_discussions} d ON p.discussion = d.id
      LEFT JOIN {forum_read} r   ON (r.postid = p.id AND r.userid = ?)
      WHERE d.forum = ?
      AND p.modified >= ? AND r.id is NULL
      $timedsql
      AND d.groupid $groups_sql"; */

    return $DB->get_field_sql($sql, $params);
}

/**
 * Returns the count of records for the provided user and course.
 * Please note that group access is ignored!
 *
 * @global object
 * @global object
 * @param int $userid
 * @param int $courseid
 * @return array
 */
function jinotechboard_tp_get_course_unread_posts($userid, $courseid) {
    global $CFG, $DB;
    $now = round(time(), -2); // db cache friendliness
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 60);
    $params = array($userid, $userid, $courseid, $cutoffdate);

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
      $params[] = $now;
      $params[] = $now;
      } else {
      $timedsql = "";
      } */
    $timedsql = "";

    $sql = "SELECT f.id, COUNT(p.id) AS unread
              FROM {jinotechboard_contents} p
                   JOIN {jinotechboard} f                   ON f.id = p.board
                   JOIN {course} c                  ON c.id = f.course
                   LEFT JOIN {jinotechboard_read} r         ON (r.contentsid = p.id AND r.userid = ?)
                   
             WHERE f.course = ?
                   AND p.timemodified >= ? AND r.id is NULL
                   AND (f.trackingtype = " . JINOTECHBOARD_TRACKING_ON . "
                        OR (f.trackingtype = " . JINOTECHBOARD_TRACKING_OPTIONAL . " ))
                   $timedsql
          GROUP BY f.id";

    if ($return = $DB->get_records_sql($sql, $params)) {
        return $return;
    }

    return array();
}

function jinotech_user_unenrolled($cp) {
    global $DB;

    // NOTE: this has to be as fast as possible!

    if ($cp->lastenrol) {
        $params = array('userid' => $cp->userid, 'courseid' => $cp->courseid);
        $jinotechboardselect = "IN (SELECT f.id FROM {jinotechboard} f WHERE f.course = :courseid)";

//        $DB->delete_records_select('jinotechboard_subscriptions', "userid = :userid AND forum $jinotechboardselect", $params);
//        $DB->delete_records_select('jinotechboard_track_prefs',   "userid = :userid AND forumid $jinotechboardselect", $params);
        $DB->delete_records_select('jinotechboard_read', "userid = :userid AND forumid $jinotechboardselect", $params);
    }
}

/**
 * Deletes read records for the specified index. At least one parameter must be specified.
 *
 * @global object
 * @param int $userid
 * @param int $postid
 * @param int $discussionid
 * @param int $jinotechboardid
 * @return bool
 */
function jinotechboard_tp_delete_read_records($userid = -1, $contentsid = -1, $jinotechboardid = -1) {
    global $DB;
    $params = array();

    $select = '';
    if ($userid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'userid = ?';
        $params[] = $userid;
    }
    if ($contentsid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'contentsid = ?';
        $params[] = $contentsid;
    }

    if ($jinotechboardid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'jinotechboardid = ?';
        $params[] = $jinotechboardid;
    }
    if ($select == '') {
        return false;
    } else {
        return $DB->delete_records_select('jinotechboard_read', $select, $params);
    }
}

/**
 * Adds information about unread messages, that is only required for the course view page (and
 * similar), to the course-module object.
 * @param cm_info $cm Course-module object
 */
function jinotechboard_cm_info_view(cm_info $cm) {
    global $CFG;

    // Get tracking status (once per request)
    static $initialised;
    static $usetracking, $strunreadpostsone;
    if (!isset($initialised)) {

        $initialised = true;
    }

    if ($usetracking) {
        $unread = jinotechboard_tp_count_jinotechboard_unread_posts($cm, $cm->get_course());

        if ($unread) {
            $out = '<span class="unread"> <a href="' . $cm->get_url() . '">';
            if ($unread == 1) {
                $out .= $strunreadpostsone;
            } else {
                $out .= get_string('unreadpostsnumber', 'forum', $unread);
            }
            $out .= '</a></span>';
            $cm->set_after_link($out);
        }
    }
}

/**
 *
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $cm
 * @return array
 */
function jinotechboard_get_discussions_unread($cm) {
    global $CFG, $DB, $USER;

    $now = round(time(), -2);
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 60 * 60);

    $params = array();
    $groupmode = groups_get_activity_groupmode($cm);
    $currentgroup = groups_get_activity_group($cm);

    if ($groupmode) {
        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);

        if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $modcontext)) {
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                $params['currentgroup'] = $currentgroup;
            } else {
                $groupselect = "";
            }
        } else {
            //separate groups without access all
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                $params['currentgroup'] = $currentgroup;
            } else {
                $groupselect = "AND d.groupid = -1";
            }
        }
    } else {
        $groupselect = "";
    }

    /* if (!empty($CFG->forum_enabletimedposts)) {
      $timedsql = "AND d.timestart < :now1 AND (d.timeend = 0 OR d.timeend > :now2)";
      $params['now1'] = $now;
      $params['now2'] = $now;
      } else {
      $timedsql = "";
      } */
    $timedsql = "";
    $sql = "SELECT d.id, COUNT(d.id) AS unread
              FROM {jinotechboard_contents} d
                   LEFT JOIN {jinotechboard_read} r ON (r.contentsid = d.id AND r.userid = $USER->id)
             WHERE d.board = {$cm->instance}
                   AND d.timemodified >= :cutoffdate AND r.id is NULL
                   $groupselect
                   $timedsql
          GROUP BY d.id";
    $params['cutoffdate'] = $cutoffdate;

    if ($unreads = $DB->get_records_sql($sql, $params)) {
        foreach ($unreads as $unread) {
            $unreads[$unread->id] = $unread->unread;
        }
        return $unreads;
    } else {
        return array();
    }
}

/**
 * Mark post as read.
 * @global object
 * @global object
 * @param int $userid
 * @param int $contentsid
 */
function jinotechboard_tp_add_read_record($userid, $contentsId) {
    global $CFG, $DB;
    $now = time();
    $cutoffdate = $now - ($CFG->forum_oldpostdays * 24 * 3600);
    $aa = $DB->record_exists('jinotechboard_read', array('userid' => $userid, 'contentsid' => $contentsId));

    if (!$DB->record_exists('jinotechboard_read', array('userid' => $userid, 'contentsid' => $contentsId))) {
        $sql = "INSERT INTO {jinotechboard_read} (userid, contentsid, jinotechboardid, firstread, lastread)

                SELECT ?,p.id,  p.board, ?, ?
                  FROM {jinotechboard_contents} p
                 WHERE p.id = ? AND p.timemodified >= ?";
        return $DB->execute($sql, array($userid, $now, $now, $contentsId, $cutoffdate));
    } else {
        $sql = "UPDATE {jinotechboard_read}
                   SET lastread = ?
                 WHERE userid = ? AND contentsid = ?";
        return $DB->execute($sql, array($now, $userid, $contentsId));
    }
}

/**
 * Return grade for given user or all users.
 *
 * @global object
 * @global object
 * @param object $jinotechboard
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function jinotechboard_get_user_grades($jinotechboard, $userid = 0) {
    global $CFG;

    require_once($CFG->dirroot . '/rating/lib.php');

    $ratingoptions = new stdClass;
    $ratingoptions->component = 'mod_jinotechboard';
    $ratingoptions->ratingarea = 'post';

    //need these to work backwards to get a context id. Is there a better way to get contextid from a module instance?
    $ratingoptions->modulename = 'jinotechboard';
    $ratingoptions->moduleid = $jinotechboard->id;
    $ratingoptions->userid = $userid;
    $ratingoptions->aggregationmethod = $jinotechboard->assessed;
    $ratingoptions->scaleid = $jinotechboard->scale;
    $ratingoptions->itemtable = 'jinotechboard_posts';
    $ratingoptions->itemtableusercolumn = 'userid';

    $rm = new rating_manager();
    return $rm->get_user_grades($ratingoptions);
}

/**
 * Update activity grades
 *
 * @global object
 * @global object
 * @param object $jinotechboard
 * @param int $userid specific user only, 0 means all
 * @param boolean $nullifnone return null if grade does not exist
 * @return void
 */
function jinotechboard_update_grades($jinotechboard, $userid = 0, $nullifnone = true) {
    global $CFG, $DB;
    require_once($CFG->libdir . '/gradelib.php');

    if (!$jinotechboard->assessed) {
        jinotechboard_grade_item_update($jinotechboard);
    } else if ($grades = jinotechboard_get_user_grades($jinotechboard, $userid)) {
        jinotechboard_grade_item_update($jinotechboard, $grades);
    } else if ($userid and $nullifnone) {
        $grade = new stdClass();
        $grade->userid = $userid;
        $grade->rawgrade = NULL;
        jinotechboard_grade_item_update($jinotechboard, $grade);
    } else {
        jinotechboard_grade_item_update($jinotechboard);
    }
}

/**
 * Update all grades in gradebook.
 * @global object
 */
function jinotechboard_upgrade_grades() {
    global $DB;

    $sql = "SELECT COUNT('x')
              FROM {jinotechboard} f, {course_modules} cm, {modules} m
             WHERE m.name='jinotechboard' AND m.id=cm.module AND cm.instance=f.id";
    $count = $DB->count_records_sql($sql);

    $sql = "SELECT f.*, cm.idnumber AS cmidnumber, f.course AS courseid
              FROM {jinotechboard} f, {course_modules} cm, {modules} m
             WHERE m.name='jinotechboard' AND m.id=cm.module AND cm.instance=f.id";
    $rs = $DB->get_recordset_sql($sql);
    if ($rs->valid()) {
        $pbar = new progress_bar('jinotechboardupgradegrades', 500, true);
        $i = 0;
        foreach ($rs as $jinotechboard) {
            $i++;
            upgrade_set_timeout(60 * 5); // set up timeout, may also abort execution
            jinotechboard_update_grades($jinotechboard, 0, false);
            $pbar->update($i, $count, "Updating jinotechboard grades ($i/$count).");
        }
    }
    $rs->close();
}

/**
 * Create/update grade item for given jinotechboard
 *
 * @global object
 * @uses GRADE_TYPE_NONE
 * @uses GRADE_TYPE_VALUE
 * @uses GRADE_TYPE_SCALE
 * @param object $jinotechboard object with extra cmidnumber
 * @param mixed $grades optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok
 */
function jinotechboard_grade_item_update($jinotechboard, $grades = NULL) {
    global $CFG;
    if (!function_exists('grade_update')) { //workaround for buggy PHP versions
        require_once($CFG->libdir . '/gradelib.php');
    }

    $params = array('itemname' => $jinotechboard->name, 'idnumber' => $jinotechboard->cmidnumber);

    if (!$jinotechboard->assessed or $jinotechboard->scale == 0) {
        $params['gradetype'] = GRADE_TYPE_NONE;
    } else if ($jinotechboard->scale > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax'] = $jinotechboard->scale;
        $params['grademin'] = 0;
    } else if ($jinotechboard->scale < 0) {
        $params['gradetype'] = GRADE_TYPE_SCALE;
        $params['scaleid'] = -$jinotechboard->scale;
    }

    if ($grades === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }

    return grade_update('mod/jinotechboard', $jinotechboard->course, 'mod', 'jinotechboard', $jinotechboard->id, 0, $grades, $params);
}

/**
 * Delete grade item for given jinotechboard
 *
 * @global object
 * @param object $jinotechboard object
 * @return object grade_item
 */
function jinotechboard_grade_item_delete($jinotechboard) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    return grade_update('mod/jinotechboard', $jinotechboard->course, 'mod', 'jinotechboard', $jinotechboard->id, 0, NULL, array('deleted' => 1));
}

/**
 * Serves the forum attachments. Implements needed access control ;-)
 *
 * @param object $course
 * @param object $cm
 * @param object $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return bool false if file not found, does not return if found - justsend the file
 */
function jinotechboard_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    require_course_login($course, true, $cm);

    $fileareas = array('attachment', 'contents');
    if (!in_array($filearea, $fileareas)) {
        return false;
    }

    $postid = (int) array_shift($args);

    if (!$contents = $DB->get_record('jinotechboard_contents', array('id' => $postid))) {
        return false;
    }


    if (!$jinotechboard = $DB->get_record('jinotechboard', array('id' => $cm->instance))) {
        return false;
    }

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_jinotechboard/$filearea/$postid/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // Make sure groups allow this user to see this file
    /*   if ($discussion->groupid > 0 and $groupmode = groups_get_activity_groupmode($cm, $course)) {   // Groups are being used
      if (!groups_group_exists($discussion->groupid)) { // Can't find group
      return false;                           // Be safe and don't send it to anyone
      }

      if (!groups_is_member($discussion->groupid) and !has_capability('moodle/site:accessallgroups', $context)) {
      // do not send posts from other groups when in SEPARATEGROUPS or VISIBLEGROUPS
      return false;
      }
      } */

    // Make sure we're allowed to see it...
    if (!jinotechboard_user_can_see_post($jinotechboard, $contents, NULL, $cm)) {
        return false;
    }


    // finally send the file
    send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}

/**
 * @global object
 * @global object
 * @param object $jinotechboard
 * @param object $post
 * @param object $user
 * @param object $cm
 * @return bool
 */
function jinotechboard_user_can_see_post($jinotechboard, $contents, $user = NULL, $cm = NULL) {
    global $CFG, $USER, $DB;

    // retrieve objects (yuk)
    if (is_numeric($jinotechboard)) {
        debugging('missing full jinotechboard', DEBUG_DEVELOPER);
        if (!$jinotechboard = $DB->get_record('jinotechboard', array('id' => $jinotechboard))) {
            return false;
        }
    }


    if (is_numeric($post)) {
        debugging('missing full post', DEBUG_DEVELOPER);
        if (!$contents = $DB->get_record('jinotechboard_contents', array('id' => $contents))) {
            return false;
        }
    }
    if (!isset($contents->id) && isset($contents->parent)) {
        $contents->id = $contents->parent;
    }

    if (!$cm) {
        debugging('missing cm', DEBUG_DEVELOPER);
        if (!$cm = get_coursemodule_from_instance('jinotechboard', $jinotechboard->id, $jinotechboard->course)) {
            print_error('invalidcoursemodule');
        }
    }

    if (empty($user) || empty($user->id)) {
        $user = $USER;
    }

    /*    $canviewcontents = !empty($cm->cache->caps['mod/jinotechboard:viewcontents']) || has_capability('mod/jinotechboard:viewcontents', get_context_instance(CONTEXT_MODULE, $cm->id), $user->id);
      if (!$canviewcontents && !has_all_capabilities(array('moodle/user:viewdetails', 'moodle/user:readuserposts'), get_context_instance(CONTEXT_USER, $post->userid))) {
      return false;
      } */

    if (isset($cm->uservisible)) {
        if (!$cm->uservisible) {
            return false;
        }
    } else {
        if (!\core_availability\info_module::is_user_visible($cm, $user->id)) {
            return false;
        }
    }


    return true;
}

/**
 * Return rating related permissions
 *
 * @param string $options the context id
 * @return array an associative array of the user's rating permissions
 */
function jinotechboard_rating_permissions($contextid, $component, $ratingarea) {
    $context = context::instance_by_id($contextid);
    if ($component != 'mod_jinotechboard' || $ratingarea != 'post') {
        // We don't know about this component/ratingarea so just return null to get the
        // default restrictive permissions.
        return null;
    }
    return array(
        'view' => has_capability('mod/jinotechboard:viewrating', $context),
        'viewany' => has_capability('mod/jinotechboard:viewanyrating', $context),
        'viewall' => has_capability('mod/jinotechboard:viewallratings', $context),
        'rate' => has_capability('mod/jinotechboard:rate', $context)
    );
}

function jinotechoard_get_ratings($context, $postid, $sort = "u.firstname ASC") {
    $options = new stdClass;
    $options->context = $context;
    $options->component = 'mod_jinotechboard';
    $options->ratingarea = 'post';
    $options->itemid = $postid;
    $options->sort = "ORDER BY $sort";

    $rm = new rating_manager();
    return $rm->get_all_ratings_for_item($options);
}

function jinotechboard_editor_options(context_module $context, $contentid) {
    global $COURSE, $PAGE, $CFG;
    // TODO: add max files and max size support
    $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
    $contentid = null;
    return array(
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'maxbytes' => $maxbytes,
        'trusttext' => true,
        'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
        'subdirs' => file_area_contains_subdirs($context, 'mod_jinotechboard', 'contents', $contentid)
    );
}

function jinotechboard_thread_contents($instance, $page, $perpage = JINOTECHBOARD_THREAD_DEFAULT, $searchfield = "1", $searchvalue = "", $boardsort = " st.ref DESC, st.step asc") {
    global $DB, $USER, $cm, $context;
    if (empty($cm))
        $cm = get_coursemodule_from_instance('jinotechboard', $instance);
    if (empty($context))
        $context = context_module::instance($cm->id);

    $params = array(
        'contextid' => $context->id,
        'boardid' => $instance);

    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $where = "";

    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.firstname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }


    $sql = "SELECT * FROM 
            (SELECT jcb.*,u.picture,u.firstname,u.lastname,u.firstnamephonetic,u.lastnamephonetic,u.middlename,u.alternatename,u.imagealt,u.email,u.lastaccess ,u.id as urid , u.firstname || u.lastname as fullname, (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = jcb.id ) as itemid 
                from {jinotechboard_contents} jcb  
                left join {user} u on jcb.userid = u.id 
               ) st
                where board = :boardid " . $where . " order by " . $boardsort;
    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function jinotechboard_thread_contents_notice($instance, $page, $perpage = JINOTECHBOARD_THREAD_DEFAULT, $searchfield = "1", $searchvalue = "", $boardsort = "st.isnotice DESC, st.ref DESC, st.step asc") {
    global $DB, $USER;

    $cm = get_coursemodule_from_instance('jinotechboard', $instance);
    $context = context_module::instance($cm->id);

    $params = array(
        'contextid' => $context->id,
        'boardid' => $instance);

    $offset = 0;
    if ($page != 0) {
        $offset = $page * $perpage;
    }

    $where = "";

    if ($searchvalue) {
        switch ($searchfield) {
            case 1:
                $where = "and st.title LIKE :title"; //
                $params['title'] = '%' . $searchvalue . '%';
                break;
            case 2:
                $where = "and st.fullname LIKE :fullname"; //
                $params['fullname'] = '%' . $searchvalue . '%';
                break;
            case 3:
                $where = "and st.contents LIKE :contents";
                $params['contents'] = '%' . $searchvalue . '%';
                break;
        }
    }
    $sql = "SELECT * FROM 
            (SELECT jcb.*,u.picture,u.firstname,u.lastname,u.firstnamephonetic,u.lastnamephonetic,u.middlename,u.alternatename,u.imagealt,u.email,u.lastaccess ,u.id as urid , u.firstname || u.lastname as fullname, (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = jcb.id ) as itemid 
                from {jinotechboard_contents} jcb 
                left join {user} u on jcb.userid = u.id ) st
                where isnotice = 1 and board = :boardid " . $where . " order by " . $boardsort;

    return $DB->get_records_sql($sql, $params, $offset, $perpage);
}

function jinotechboard_thread_contents_count($instance, $searchfield, $searchvalue = "") {
    global $DB;

    $params = array();
    $where = "";
    $params[] = $instance;
    if (!empty($searchvalue)) {
        switch ($searchfield) {
            case 1:
                $where = "and title LIKE ?";
                $params[] = '%' . $searchvalue . '%';
                break;
            case 2:
                break;
            case 3:
                $where = "and contents LIKE ?";
                $params[] = '%' . $searchvalue . '%';
                break;
        }
    }
    $sql = " board = ? " . $where;

    return $DB->count_records_select("jinotechboard_contents", $sql, $params);
}

/**
 * Print an overview of all jinotechboard
 * for the courses.
 *
 * @param mixed $courses The list of courses to print the overview for
 * @param array $htmlarray The array of html to return
 */
//OSE 대형 order by jc.id desc 추가
function jinotechboard_print_overview($courses, &$htmlarray) {
    global $USER, $CFG, $DB;

    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return array();
    }

    if (!$jinotechboards = get_all_instances_in_courses('jinotechboard', $courses)) {
        return;
    }

    $jinotechboardids = array();

    foreach ($jinotechboards as $jinotechboard) {
        if ($jinotechboard->type == 1) {
            $jinotechbardids[] = $jinotechboard->id;
        }
    }

    if (empty($jinotechbardids)) {
        return true;
    }

    list($sqljinotechboardids, $jinotechboardparams) = $DB->get_in_or_equal($jinotechbardids);
    $jinotechboardparams[] = time();
    $sql = ' SELECT jc.*, jb.name as boardname   
            FROM {jinotechboard} jb
            join {jinotechboard_contents} jc on jc.board = jb.id
            where jb.id ' . $sqljinotechboardids . ' and jc.timeend > ? order by jc.id desc';

    $contents = $DB->get_records_sql($sql, $jinotechboardparams);
    if (!empty($contents)) {
        foreach ($contents as $content) {
            $content_title = str_replace('<p>', "", $content->title);
            $content_title = str_replace('</p>', "", $content_title);

            $href = $CFG->wwwroot . '/mod/jinotechboard/content.php?contentId=' . $content->id . '&b=' . $content->board . '&boardform=1';
            $str = '<div class="jinotechboard overview">' .
                    '<div class="name">' . $content->boardname . ' : ' .
                    '<a ' .
                    'title="' . get_string('pluginname', 'mod_jinotechboard') . '" ' .
                    'href="' . $href . '">' . $content_title .
                    '</a></div></div>';
//            $str .= '<div class="detail jinotechboard_content">'.$content->contents.'</div>'; 
            $htmlarray[$content->course]['jinotechboard'] .= $str;
        }
    }
}

function jinotechboard_print_contents($ref, $text, $boardform = 1, $search_param, $list_num) {
    global $CFG, $DB, $OUTPUT, $context, $output;
    $sql = "select con.* , u.lastname , u.firstname , (SELECT count(*) FROM {files} f WHERE contextid = :contextid AND filearea = 'attachment' AND filesize != 0 and f.itemid = con.id ) as itemid , pcon.userid as parentuserid "
            . "from {jinotechboard_contents} con "
            . "join {user} u on u.id = con.userid "
            . "join {jinotechboard_contents} pcon on pcon.id = con.ref "
            . "where con.ref = :ref "
            . "order by con.ref desc, con.step asc";
    $contents = $DB->get_records_sql($sql, array('ref' => $ref, 'contextid' => $context->id));
    foreach ($contents as $content) {
        $attachments = "";
        if ($content->itemid) {
            $attachments = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
        }
        $output .= '<tr>
		    <td class="col-1 " align="center"> ';
        if ($content->lev == 0) {
            $output .= $text;
        }

        $output .= "</td>
                <td class='title col-2 '>
              ";

        if ($content->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
            $step = $content->lev - 1;
            $step_left_len = $step * 15;
        } else {
            $step = JINOTECHBOARD_INDENT_DEFAULT - 1;
            $step_left_len = $step * 15;
        }
        $step_icon = '';
        $step_left = 'style="padding-left:' . $step_left_len . 'px;"';
        $parentid = $content->parentuserid;
        if ($content->lev != 0) {
            $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="" ' . $step_left . '/> ';
        }
        if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $context) && $USER->id != $content->userid && $parentid != $USER->id) {
            $output .= "<a href='#' onclick='" . 'alert("비밀글 입니다.")' . "' $step_left><span style='float:left;'>" . $content->title . $attachments . "</span>";
        } else {
            $output .= "<a href='" . $CFG->wwwroot . "/mod/jinotechboard/content.php?contentId=" . $content->id . "&b=" . $cm->instance . "&boardform=" . $boardform . $search_param . "&list_num=" . $list_num . "'><span style='float:left;'>" . $step_icon . $content->title . "</span>";
        }
        if ($content->isprivate) {
            $output .= "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
        }
        $output .= "</a>  </td>
                <td class='col-3 ' align=center>" . fullname($content) . "</td>
                <td class='col-4 ' align=center>" . date("Y-m-d", $content->timemodified) . "</td>
                <td class='col-5 ' align=center>" . $content->viewcnt . "</td>";

        $output .= "</tr>";
    }
}

function jinotechboard_get_cookie($key, $d_val) {
    if (isset($_COOKIE[$key])) {
        return $_COOKIE[$key];
    } else {
        return $d_val;
    }
}

function jinotechboard_view($jinotechboard, $course, $cm, $context) {
	// Trigger course_module_viewed event.
    $params = array(
        'context' => $context,
        'objectid' => $jinotechboard->id
    );

    $event = \mod_jinotechboard\event\course_module_viewed::create($params);
    $event->add_record_snapshot('course_modules', $cm);
    $event->add_record_snapshot('course', $course);
    $event->add_record_snapshot('jinotechboard', $jinotechboard);
    $event->trigger();

	// Completion.
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}

/*
 *UST create course 시 기본으로 만들어야 하는 jinotechboard activity 
 * type 1   강의공지사항 
 * type 2   과정 qna

 *  * @param stdclass $newcourse
 */
function new_course_create_activity_jinotechboard($newcourse, $type){
    global $DB, $CFG;
    
    $modinfo = $DB->get_record('modules', array('name'=>'jinotechboard'));
    
    $board_list = array(
        1=> 'notice',
        2=> 'qna',
        3=> 'reference',
        4=> 'tplan',
        );
    
    $add_board = new stdClass();
    
    $add_board -> forcesubscribe = 0;
    $add_board -> maxbytes = $CFG->forum_maxbytes;
    $add_board -> maxattachments = 5;
    
    if($type == 1){
        $add_board -> allowreply = false;
        $add_board -> allownotice = true;
    } else {
        $add_board -> allowreply = 1;
        $add_board -> allownotice = 0;
    }
    if($type == 2) {
        $add_board -> allowsecret = 1;
        $add_board -> allowrecommend = 0;
        $add_board -> allowgigan = 0;
    }
    
    if($type == 3 || $type == 4){
        $add_board -> allowreply = 0;
    }
   
    $add_board -> allowcomment = 0;
    $add_board -> gradecat = 1;
    $add_board -> assessed = 1;
    $add_board -> assesstimestart = time();
    $add_board -> assesstimefinish = time();
    $add_board -> visible = 1;
    $add_board -> groupmode = 0;
    $add_board -> groupingid = 0;
    $add_board -> groupmembersonly = 0;
    $add_board -> course = $newcourse->id;
    $add_board -> section = 0; 
    $add_board -> module = $modinfo->id;
    $add_board -> modulename = 'jinotechboard';
    $add_board->completion = 0;
    $add_board->completionview = 0;
    $add_board->completiongradeitemnumber = 0;
    $add_board->availablefrom             = 0;
    $add_board->availableuntil            = 0; 
    $add_board->showavailability          = 0;
    $add_board->introformat = 1;
    $add_board->introeditor = '';
    
    $add_board -> type = $type;
    $add_board -> name = $board_list[$type];
    $add_board -> intro = $board_list[$type];
    create_module($add_board);
}



function set_smssend_mod_jinotechboard($sms_data) {
    global $CONN_ODBC;
    $select = "select NVL(MAX(TO_NUMBER(IF_MSG_NO)), 0) + 1 AS MSG_NO from EP_PNS.PNS_IF_MSG;";

    $exec = odbc_exec($CONN_ODBC, $select);

    
    $row = odbc_fetch_array($exec);

    $now = date('Ymdhis', time());
      
        $query = "INSERT INTO EP_PNS.PNS_IF_MSG (
                                    IF_MSG_NO
                                   ,IF_MSG_TIT
                                   ,IF_MSG_CTNT
                                   ,IF_MSG_SEND_DTTM
                                   ,IF_SYS_GBN
                                   ,IF_SYS_GBN_NM
                                   ,IF_MSG_PROC_YN
                                   ,IF_MSG_SEND_ID
                                   ) VALUES ( 
                                       '" . $row['MSG_NO'] . "',"
            . "'" . iconv('UTF-8','EUC-KR',$sms_data->subject) . "',"
            . "'" . iconv('UTF-8','EUC-KR',$sms_data->contents) . "',"
            . "'" . $now . "',"
            . "'227692',"
            . "'".iconv('UTF-8','EUC-KR','미래융합대학교')."',"
            . "'N',"
            . "'06931'"
            . ")";

    if ($result = odbc_exec($CONN_ODBC, $query) == TRUE) {
    } else {
        echo $query;
        print_object(odbc_error($CONN_ODBC));
    }
    
    return $row['MSG_NO'];
}

function send_sms_mod_jinotechboard($user, $sms_data,$msg_no) {
    global $CONN_ODBC;
    if ($user->username == 'admin') {
        $username = 'lms_admin';
    } else {
        $username = $user->username;
    }

    $now = date('Ymdhis', time());
    $send_date = date('Ymdhis', $send_date);

    $userinfo = $user->firstname . $user->lastname . '^' . $user->phone2;


    if(trim($user->phone2)){

    $query = "INSERT INTO EP_PNS.PNS_IF_MSG_USER (
         IF_MSG_NO
        ,IF_SEND_EMP_NO
        ,IF_SEND_EMP_TEL
        ,IF_RECV_USER_NM
        ,IF_RECV_USER_TEL
        ,IF_SEND_GBN
        ) VALUES ( 
            '" . iconv('UTF-8','EUC-KR',$msg_no) . "',"
            . "'".iconv('UTF-8','EUC-KR',$user->username)."',"
            . "'".iconv('UTF-8','EUC-KR',$sms_data->callback)."',"
            . "'".iconv('UTF-8','EUC-KR',fullname($user))."',"
            . "'".iconv('UTF-8','EUC-KR',$user->phone2)."',"
            . "'N'"
            . ")";

    if ($result = odbc_exec($CONN_ODBC, $query) == TRUE) {
    } else {
        echo $query;
        print_object(odbc_error($CONN_ODBC));
    }
    }
}