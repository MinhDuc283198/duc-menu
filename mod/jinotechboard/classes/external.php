<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL external API
 *
 * @package    mod_jinotechboard
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php");

/**
 * URL external functions
 *
 * @package    mod_jinotechboard
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */
class mod_jinotechboard_external extends external_api {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function view_jinotechboard_parameters() {
        return new external_function_parameters(
            array(
                'jinotechboardid' => new external_value(PARAM_INT, 'jinotechboard instance id')
            )
        );
    }

    /**
     * Trigger the course module viewed event and update the module completion status.
     *
     * @param int $jinotechboardid the jinotechboard instance id
     * @return array of warnings and status result
     * @since Moodle 3.0
     * @throws moodle_exception
     */
    public static function view_jinotechboard($jinotechboardid) {
        global $DB, $CFG;
        require_once($CFG->dirroot . "/mod/jinotechboard/lib.php");

        $params = self::validate_parameters(self::view_jinotechboard_parameters(),
                                            array(
                                                'jinotechboardid' => $jinotechboardid
                                            ));
        $warnings = array();

        // Request and permission validation.
        $jinotechboard = $DB->get_record('jinotechboard', array('id' => $params['jinotechboardid']), '*', MUST_EXIST);
        list($course, $cm) = get_course_and_cm_from_instance($jinotechboard, 'jinotechboard');

        $context = context_module::instance($cm->id);
        self::validate_context($context);

        require_capability('mod/jinotechboard:view', $context);

        // Call the jinotechboard/lib API.
        jinotechboard_view($jinotechboard, $course, $cm, $context);

        $result = array();
        $result['status'] = true;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 3.0
     */
    public static function view_jinotechboard_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings()
            )
        );
    }



    /**
     * Describes the parameters for get_jinotechboards_by_courses.
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function get_jinotechboards_by_courses_parameters() {
        return new external_function_parameters (
            array(
                'courseids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'course id'), 'Array of course ids', VALUE_DEFAULT, array()
                ),
            )
        );
    }

    /**
     * Returns a list of jinotechboards in a provided list of courses,
     * if no list is provided all jinotechboards that the user can view will be returned.
     *
     * @param array $courseids the course ids
     * @return array the jinotechboard details
     * @since Moodle 3.0
     */
    public static function get_jinotechboards_by_courses($courseids = array()) {
        global $CFG;

        $returnedjinotechboards = array();
        $warnings = array();

        $params = self::validate_parameters(self::get_jinotechboards_by_courses_parameters(), array('courseids' => $courseids));

        if (empty($params['courseids'])) {
            $params['courseids'] = array_keys(enrol_get_my_courses());
        }

        // Ensure there are courseids to loop through.
        if (!empty($params['courseids'])) {

            list($courses, $warnings) = external_util::validate_courses($params['courseids']);

            // Get the jinotechboards in this course, this function checks users visibility permissions.
            // We can avoid then additional validate_context calls.
            $jinotechboards = get_all_instances_in_courses("jinotechboard", $courses);

            foreach ($jinotechboards as $jinotechboard) {
                $course = $courses[$jinotechboard->course];
                $cm = get_coursemodule_from_instance('jinotechboard', $jinotechboard->id, $course->id);
                $context = context_module::instance($cm->id);

                // Entry to return.
                $module = array();

                // First, we return information that any user can see in (or can deduce from) the web interface.
                $module['id'] = $jinotechboard->id;
                $module['cmid'] = $cm->id;
                $module['course'] = $jinotechboard->course;
                $module['name']  = external_format_string($jinotechboard->name, $context->id);
                list($module['intro'], $module['introformat']) =
                    external_format_text($jinotechboard->intro, $jinotechboard->introformat, $context->id, 'mod_jinotechboard', 'intro', $jinotechboard->id);
                $module['maxbytes']       = $jinotechboard->maxbytes;
                $module['maxattachments'] = $jinotechboard->maxattachments;
                $module['allownotice']    = $jinotechboard->allownotice;
                $module['allowreply']     = $jinotechboard->allowreply;
                $module['allowcomment']   = $jinotechboard->allowcomment;
                $module['allowupload']    = $jinotechboard->allowupload;
                $module['allowsecret']    = $jinotechboard->allowsecret;
                $module['allowpublic']    = $jinotechboard->allowpublic;
                $module['allowcategory']  = $jinotechboard->allowcategory;
                $module['allowrecommend'] = $jinotechboard->allowrecommend;
                $module['allowgigan']     = $jinotechboard->allowgigan;
                $module['forcesubscribe'] = $jinotechboard->forcesubscribe;
                $module['trackingtype']   = $jinotechboard->trackingtype;
                $module['timemodified']   = $jinotechboard->timemodified;
                
                $module['numcontents']   = jinotechboard_get_discussions_count($cm);

                $returnedjinotechboards[] = $module;
            }
        }

        $result = array();
        $result['jinotechboards'] = $returnedjinotechboards;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the get_jinotechboards_by_courses return value.
     *
     * @return external_single_structure
     * @since Moodle 3.0
     */
    public static function get_jinotechboards_by_courses_returns() {
        return new external_single_structure(
            array(
                'jinotechboards' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'jinotechboard id'),
                            'cmid' => new external_value(PARAM_INT, 'Course module id'),
                            'course' => new external_value(PARAM_INT, 'Course id'),
                            'name' => new external_value(PARAM_RAW, 'jinotechboard name'),
                            'intro' => new external_value(PARAM_RAW, 'The jinotechboard intro'),
                            'introformat' => new external_format_value('intro'),
                            'maxbytes' => new external_value(PARAM_INT, 'Max bytes'),
                            'maxattachments' => new external_value(PARAM_INT, 'Max attachments', VALUE_OPTIONAL),
                            'allownotice' => new external_value(PARAM_INT, 'Allow notice', VALUE_OPTIONAL),
                            'allowreply' => new external_value(PARAM_INT, 'Allow reply', VALUE_OPTIONAL),
                            'allowcomment' => new external_value(PARAM_INT, 'Allow comment', VALUE_OPTIONAL),
                            'allowupload' => new external_value(PARAM_INT, 'Allow upload', VALUE_OPTIONAL),
                            'allowsecret' => new external_value(PARAM_INT, 'Allow secret', VALUE_OPTIONAL),
                            'allowpublic' => new external_value(PARAM_INT, 'Allow public', VALUE_OPTIONAL),
                            'allowcategory' => new external_value(PARAM_INT, 'Allow category', VALUE_OPTIONAL),
                            'allowrecommend' => new external_value(PARAM_INT, 'Allow recommend', VALUE_OPTIONAL),
                            'allowgigan' => new external_value(PARAM_INT, 'Allow gigan', VALUE_OPTIONAL),
                            'forcesubscribe' => new external_value(PARAM_INT, 'Force subscribe', VALUE_OPTIONAL),
                            'trackingtype' => new external_value(PARAM_INT, 'Tracking type', VALUE_OPTIONAL),
                            'timemodified' => new external_value(PARAM_INT, 'Time of last modification'),
                            'numcontents' => new external_value(PARAM_INT, 'Number of contents in the jinotechboard', VALUE_OPTIONAL),
                        ), 'jinotechboard'
                    )
                ),
                'warnings' => new external_warnings(),
            )
        );
    }


    
    public static function get_contents_parameters() {
        return new external_function_parameters(
            array(
                'jinotechboardid' => new external_value(PARAM_INT, 'jinotechboard id'),
                'page' => new external_value(PARAM_INT, 'current page', VALUE_DEFAULT, 1),
                'perpage' => new external_value(PARAM_INT, 'items per page', VALUE_DEFAULT, 10),
            )
        );
    }
    
    public static function get_contents($jinotechboardid, $page = 1, $perpage = 10) {
        global $PAGE, $DB;
        
        $params = self::validate_parameters(self::get_contents_parameters(),
                                            array(
                                                'jinotechboardid' => $jinotechboardid,
                                                'page' => $page,
                                                'perpage' => $perpage
                                            ));
        
        $board = $DB->get_record("jinotechboard", array("id" => $jinotechboardid));
        $course = $DB->get_record("course", array("id" => $board->course));
        $cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id);
        $modcontext = context_module::instance($cm->id);
        
        $boardsort = "st.ref DESC, st.step asc";
        $searchfield = "";
        $searchvalue = "";
        $capability = true;
        
        $notices = jinotechboard_get_discussions($cm, $boardsort, $searchfield, $searchvalue, 0, 0, true, $capability, false);
        $contents = jinotechboard_get_discussions($cm, $boardsort, $searchfield, $searchvalue, $page, $perpage, false, $capability, true);
        $allcontents = array_merge($notices, $contents);
        
        $returnedcontents = array();
        $warnings = array();
        
        $picturefields = explode(',', user_picture::fields());
        $canviewfullname = has_capability('moodle/site:viewfullnames', $modcontext);
        
        foreach($allcontents as $content) {
            // Load user objects from the results of the query.
            $user = new stdclass();
            $user->id = $content->userid;
            $user = username_load_fields_from_object($user, $content, null, $picturefields);
            $content->userfullname = fullname($user, $canviewfullname);
            
            $userpicture = new user_picture($user);
            $userpicture->size = 1; // Size f1.
            $content->userpictureurl = $userpicture->get_url($PAGE)->out(false);
            
            // Rewrite embedded images URLs.
            list($content->contents, $content->messageformat) =
                external_format_text($content->contents, 1,
                                        $modcontext->id, 'mod_jinotechboard', 'attachment', $content->id);

            // List attachments.

            $fs = get_file_storage();
            if ($files = $fs->get_area_files($modcontext->id, 'mod_jinotechboard', 'attachment',
                                                $content->id, "filename", false)) {
                $attachments = array();
                foreach ($files as $file) {
                    $filename = $file->get_filename();

                    $attachments[] = array(
                        'filename' => $filename,
                        'mimetype' => $file->get_mimetype(),
                        'fileurl'  => file_encode_url($CFG->wwwroot.'/webservice/pluginfile.php',
                                        '/'.$modcontext->id.'/mod_jinotechboard/attachment/'.$content->id.'/'.$filename)
                    );
                }
                
                $content->attachment = count($attachments);
                $content->attachments = $attachments;
            }
            
            // List comments
            if ($allcomments = $DB->get_records('jinotechboard_comments', array('jinotechboard_contents' => $content->id), 'timecreated asc')) {
                $comments = array();
                foreach($allcomments as $comment) {
                    $comments[] = $comment;
                    
                    $user = new stdclass();
                    $user->id = $content->userid;
                    $user = username_load_fields_from_object($user, $content, null, $picturefields);
                    $comment->userfullname = fullname($user, $canviewfullname);
                }
                $content->comments = $comments;
            }

            $returnedcontents[] = $content;
        }
        
        $result = array();
        $result['contents'] = $returnedcontents;
        $result['warnings'] = $warnings;
        return $result;
    }
    
    public static function get_contents_returns() {
        return new external_single_structure(
            array(
                'contents' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'id' => new external_value(PARAM_INT, 'id'),
                                'course' => new external_value(PARAM_INT, 'course id'),
                                'board' => new external_value(PARAM_INT, 'jinotechboard id'),
                                'category' => new external_value(PARAM_INT, ''),
                                'userid' => new external_value(PARAM_INT, ''),
                                'ref' => new external_value(PARAM_INT, ''),
                                'step' => new external_value(PARAM_INT, ''),
                                'lev' => new external_value(PARAM_INT, ''),
                                'isnotice' => new external_value(PARAM_INT, ''),
                                'title' => new external_value(PARAM_TEXT, ''),
                                'contents' => new external_value(PARAM_RAW, ''),
                                'viewcnt' => new external_value(PARAM_INT, ''),
                                'commentscount' => new external_value(PARAM_INT, ''),
                                'recommendcnt' => new external_value(PARAM_INT, ''),
                                'timeend' => new external_value(PARAM_INT, 'Time discussion ends'),
                                'timecreated' => new external_value(PARAM_INT, 'Creation time'),
                                'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                                'attachment' => new external_value(PARAM_RAW, 'Has attachments?'),
                                'attachments' => new external_multiple_structure(
                                    new external_single_structure(
                                        array (
                                            'filename' => new external_value(PARAM_FILE, 'file name'),
                                            'mimetype' => new external_value(PARAM_RAW, 'mime type'),
                                            'fileurl'  => new external_value(PARAM_URL, 'file download url')
                                        )
                                    ), 'attachments', VALUE_OPTIONAL
                                ),
                                'userfullname' => new external_value(PARAM_TEXT, 'Post author full name'),
                                'userpictureurl' => new external_value(PARAM_URL, 'Post author picture.'),
                                'comments' => new external_multiple_structure(
                                    new external_single_structure(
                                        array (
                                            'userid' => new external_value(PARAM_INT, ''),
                                            'userfullname' => new external_value(PARAM_TEXT, 'Post author full name'),
                                            'ref' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                                            'step' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                                            'lev' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                                            'isprivate' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                                            'comments' => new external_value(PARAM_RAW, ''),
                                            'timecreated' => new external_value(PARAM_INT, 'Creation time'),
                                            'timemodified' => new external_value(PARAM_INT, 'Time modified')
                                        )
                                    ), 'comments', VALUE_OPTIONAL
                                )
                            ), 'contents'
                        )
                    ),
                'warnings' => new external_warnings()
            )
        );
    }
    
    
    
    public static function get_content_parameters() {
        return new external_function_parameters(
            array(
                'contentid' => new external_value(PARAM_INT, 'content id')
            )
        );
    }
    
    public static function get_content($contentid) {
        global $PAGE, $DB;
        
        $params = self::validate_parameters(self::get_content_parameters(),
                                            array(
                                                'contentid' => $contentid
                                            ));
        
        $content = $DB->get_record("jiniotechboard_contents", array('id'=>$contentid));
        $board = $DB->get_record("jinotechboard", array("id" => $jinotechboardid));
        $course = $DB->get_record("course", array("id" => $board->course));
        $cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id);
        $modcontext = context_module::instance($cm->id);
        
        $warnings = array();
        
        $picturefields = explode(',', user_picture::fields());
        $canviewfullname = has_capability('moodle/site:viewfullnames', $modcontext);
        
        // Rewrite embedded images URLs.
        list($content->contents, $content->messageformat) =
            external_format_text($content->contents, 1,
                                    $modcontext->id, 'mod_jinotechboard', 'attachment', $content->id);
        
        $result = array();
        $result['id'] = $content->id;
        $result['course'] = $content->course;
        $result['board'] = $content->board;
        $result['category'] = $content->category;
        $result['userid'] = $content->userid;
        $result['ref'] = $content->ref;
        $result['step'] = $content->step;
        $result['lev'] = $content->lev;
        $result['isnotice'] = $content->isnotice;
        $result['title'] = $content->title;
        $result['contents'] = $content->contents;
        $result['viewcnt'] = $content->viewcnt;
        $result['commentscount'] = $content->commentscount;
        $result['recommendcnt'] = $content->recommendcnt;
        $result['timeend'] = $content->timeend;
        $result['timecreated'] = $content->timecreated;
        $result['timemodified'] = $content->timemodified;
        
        // Load user objects from the results of the query.
        $user = new stdclass();
        $user->id = $content->userid;
        $user = username_load_fields_from_object($user, $content, null, $picturefields);
        $result['userfullname'] = fullname($user, $canviewfullname);

        $userpicture = new user_picture($user);
        $userpicture->size = 1; // Size f1.
        $result['userpictureurl'] = $userpicture->get_url($PAGE)->out(false);

        // List attachments.
        $fs = get_file_storage();
        if ($files = $fs->get_area_files($modcontext->id, 'mod_jinotechboard', 'attachment',
                                            $content->id, "filename", false)) {
            $attachments = array();
            foreach ($files as $file) {
                $filename = $file->get_filename();

                $attachments[] = array(
                    'filename' => $filename,
                    'mimetype' => $file->get_mimetype(),
                    'fileurl'  => file_encode_url($CFG->wwwroot.'/webservice/pluginfile.php',
                                    '/'.$modcontext->id.'/mod_jinotechboard/attachment/'.$content->id.'/'.$filename)
                );
            }
            
            $result['attachment'] = count($attachments);
            $result['attachments'] = $attachments;
        }

        // List comments
        if ($allcomments = $DB->get_records('jinotechboard_comments', array('jinotechboard_contents' => $content->id), 'timecreated asc')) {
            $comments = array();
            foreach($allcomments as $comment) {
                $comments[] = $comment;

                $user = new stdclass();
                $user->id = $content->userid;
                $user = username_load_fields_from_object($user, $content, null, $picturefields);
                $comment->userfullname = fullname($user, $canviewfullname);
            }
            $result['comments'] = $comments;
        }

        $result['warnings'] = $warnings;
        
        return $result;
    }
    
    public static function get_content_returns() {
        return new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
                'course' => new external_value(PARAM_INT, 'course id'),
                'board' => new external_value(PARAM_INT, 'jinotechboard id'),
                'category' => new external_value(PARAM_INT, ''),
                'userid' => new external_value(PARAM_INT, ''),
                'ref' => new external_value(PARAM_INT, ''),
                'step' => new external_value(PARAM_INT, ''),
                'lev' => new external_value(PARAM_INT, ''),
                'isnotice' => new external_value(PARAM_INT, ''),
                'title' => new external_value(PARAM_TEXT, ''),
                'contents' => new external_value(PARAM_RAW, ''),
                'viewcnt' => new external_value(PARAM_INT, ''),
                'commentscount' => new external_value(PARAM_INT, ''),
                'recommendcnt' => new external_value(PARAM_INT, ''),
                'timeend' => new external_value(PARAM_INT, 'Time discussion ends'),
                'timecreated' => new external_value(PARAM_INT, 'Creation time'),
                'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                'attachment' => new external_value(PARAM_RAW, 'Has attachments?'),
                'attachments' => new external_multiple_structure(
                    new external_single_structure(
                        array (
                            'filename' => new external_value(PARAM_FILE, 'file name'),
                            'mimetype' => new external_value(PARAM_RAW, 'mime type'),
                            'fileurl'  => new external_value(PARAM_URL, 'file download url')
                        )
                    ), 'attachments', VALUE_OPTIONAL
                ),
                'userfullname' => new external_value(PARAM_TEXT, 'Post author full name'),
                'userpictureurl' => new external_value(PARAM_URL, 'Post author picture.'),
                'comments' => new external_multiple_structure(
                    new external_single_structure(
                        array (
                            'userid' => new external_value(PARAM_INT, ''),
                            'userfullname' => new external_value(PARAM_TEXT, 'Post author full name'),
                            'ref' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                            'step' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                            'lev' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                            'isprivate' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                            'comments' => new external_value(PARAM_RAW, ''),
                            'timecreated' => new external_value(PARAM_INT, 'Creation time'),
                            'timemodified' => new external_value(PARAM_INT, 'Time modified')
                        )
                    ), 'comments', VALUE_OPTIONAL
                ),
                'warnings' => new external_warnings()
            )
        );
    }
    
    public static function add_content_parameters() {
        return new external_function_parameters(
            array(
                'boardid' => new external_value(PARAM_INT, 'Board instance ID'),
                'title' => new external_value(PARAM_TEXT, 'New title'),
                'contents' => new external_value(PARAM_RAW, 'New content'),
                'options' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'name' => new external_value(PARAM_ALPHANUM, ''),
                            'value' => new external_value(PARAM_RAW, '')
                        )
                    ), 'Options', VALUE_DEFAULT, array())
            )
        );
    }
    
    public static function add_content($boardid, $title, $contents, $options = array()) {
        global $CFG, $DB, $USER;
        
        require_once($CFG->dirroot . "/mod/jinotechboard/lib.php");
        
        $params = self::validate_parameters(self::add_content_parameters(),
                                            array(
                                                'boardid' => $boardid,
                                                'title' => $title,
                                                'contents' => $contents,
                                                'options' => $options,
                                            ));
        
        // ref = 최상위 부모 id, 최상위 글인 경우 자신의 id
        // step = 답글의 순서, 최상위 글인 경우 0,
        //        최신 답글이 위에 오도록 처리: 새 답글에 대해 ref 가 부모의 id 와 같고 step 이 부모의 step 보다 큰 글의 step 을 1 증가 시킨고 자신의 step 값은 부모의 step 값 + 1
        // lev = 답글의 깊이(?), 최상위 글인 경우 0, 답글인 경우 부모 글의 lev + 1
        
        // Validate options.
        $options = array(
            'edit' => 0,        // 편집하는 글의 id
            'category' => 0,
            'ref' => 0,         // 최상위 부모 id
            'step' => 0,        // 부모의 step
            'lev' => 0,         // 부모의 lev
            'isnotice' => 0,
            'isprivate' => 0
        );
        
        foreach ($params['options'] as $option) {
            $name = trim($option['name']);
            $value = clean_param($option['value'], PARAM_INT);
            
            $options[$name] = $value;
        }
        
        $warnings = array();
        
        
        $board = $DB->get_record("jinotechboard", array("id" => $boardid));
        $course = $DB->get_record("course", array("id" => $board->course));
        $cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id);
        $context = context_module::instance($cm->id);
        
        // 수정?
        if($options['edit']) {
            $content = $DB->get_record('jinotechboard_contents', array('id'=>$options['edit']));
            $content->title = $title;
            $content->itemid = $options["inlineattachmentsid"];
            $content->contents = file_save_draft_area_files($content->itemid, $context->id, 'mod_jinotechboard', 'contents', $content->id, jinotechboard_editor_options($context, null), $contents);
                
            $DB->update_record('jinotechboard_contents', $content);
            
            $eventparams = array(
                'objectid' => $content->id,
                'context' => $context,
                'other' => array(
                    'jinotechboardid' => $board->id,
                )
            );

            $event = \mod_jinotechboard\event\content_updated::create($eventparams);
            $event->add_record_snapshot('jinotechboard_contents', $content);
            $event->trigger();
        } else {
            // 새글
            $content = new object();
            $content->course = $course->id;
            $content->board = $boardid;
            $content->category = $options["category"];
            $content->userid = $USER->id;
            $content->title = $title;
            $content->contents = $contents;
            $content->ref = $options['ref'];
            $content->step = 0;
            $content->lev = 0;
            $content->isnotice = $options["isnotice"];
            $content->isprivate = $options["isprivate"];
            $content->itemid = $options["inlineattachmentsid"];
            
            
            if ($newid = $DB->insert_record('jinotechboard_contents', $content)) {
                $content->id = $newid;
                
                // 답글?
                if($options['ref']) {
                    $parent = $DB->get_record('jinotechboard_contents', array('id'=>$options['ref']));

                    $DB->execute("UPDATE {jinotechboard_contents} SET step = step + 1 "
                            . "WHERE board = :board AND ref = :ref AND step > :step",
                            array('board'=>$board->id,
                                  'ref'=>$options['ref'],
                                  'step'=>$parent->step
                            )
                        );

                    $DB->set_field('jinotechboard_contents', "step", $options['step'] + 1, array('id'=>$newid));
                    $DB->set_field('jinotechboard_contents', "lev", $options['lev'] + 1, array('id'=>$newid));
                } else {
                    $DB->set_field('jinotechboard_contents', "ref", $newid, array('id'=>$newid));
                }

                $content->contents = file_save_draft_area_files($content->itemid, $context->id, 'mod_jinotechboard', 'contents', $content->id, jinotechboard_editor_options($context, null), $content->contents);
                $DB->set_field('jinotechboard_contents', 'contents', $content->contents, array('id' => $content->id));
                
                $eventparams = array(
                    'objectid' => $content->id,
                    'context' => $context,
                    'other' => array(
                        'jinotechboardid' => $board->id,
                    )
                );

                $event = \mod_jinotechboard\event\content_created::create($eventparams);
                $event->add_record_snapshot('jinotechboard_contents', $content);
                $event->trigger();
            }
        }
        
        //$draftitemid = file_get_submitted_draft_itemid('attachments');
        //file_save_draft_area_files($draftitemid, $context->id, 'mod_jinotechboard', 'attachment', $newid);
        
        $result = array();
        $result['contentid'] = $content->id;
        $result['warnings'] = $warnings;
        
        return $result;
    }
    
    public static function add_content_returns() {
        return new external_single_structure(
            array(
                'contentid' => new external_value(PARAM_INT, 'new content id'),
                'warnings' => new external_warnings()
            )
        );
    }
    
    public static function add_comment_parameters() {
        return new external_function_parameters(
            array(
                'contentid' => new external_value(PARAM_INT, ''),
                'comment' => new external_value(PARAM_RAW, '')
            )
        );
    }
    
    public static function add_comment($contentid, $comment) {
        global $DB, $USER;
        
        $params = self::validate_parameters(self::add_comment_parameters(),
                                            array(
                                                'contentid' => $contentid,
                                                'comment' => $comment
                                            ));
        
        $content = $DB->get_record('jinotechboard_contents',array('id'=>$contentid));
        $board = $DB->get_record('jinotechboard', array('id'=>$content->board));
        $course = $DB->get_record("course", array("id" => $board->course));
        $cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id);
        $context = context_module::instance($cm->id);
        
        $warnings = array();
        
        $records = new stdClass();
	$records->course = $content->course;
	$records->board = $content->board;
	$records->jinotechboard_contents = $content->id;
	$records->userid = $USER->id;
	$records->username = fullname($USER);
	$records->comments = $comment;
	$records->timecreated = time();
	$records->timemodified = time();
	
	if($commentid = $DB->insert_record('jinotechboard_comments',$records)){
            $DB->set_field('jinotechboard_contents', 'commentscount', ($content->commentscount+1), array('id'=>$content->id));

            $params = array(
                'objectid' => $records->id,
                'context' => $context,
                'other' => array(
                    'contentid' => $content->id,
                    'jinotechboardid' => $content->board,
                )
            );

            $event = \mod_jinotechboard\event\comment_created::create($params);
            $event->add_record_snapshot('jinotechboard_contents', $content);
            $event->add_record_snapshot('jinotechboard_comments', $records);
            $event->trigger();
	}
        
        $result = array();
        $result['commentid'] = $commentid;
        $result['warnings'] = $warnings;
        return $result;
    }
    
    public static function add_comment_returns() {
        return new external_single_structure(
            array(
                'commentid' => new external_value(PARAM_INT, 'new comment id'),
                'warnings' => new external_warnings()
            )
        );
    }
}
