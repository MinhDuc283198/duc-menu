<?php


require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir.'/completionlib.php');

$b  = optional_param('b', 0, PARAM_INT);
$mode   = optional_param('mode', "", PARAM_CLEAN);
$groupid = optional_param('groupid', null, PARAM_INT);

$confirmed   = optional_param('confirmed', 0, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);

$isnotice = optional_param('isnotice', 0, PARAM_INT);
$isprivate = optional_param('isprivate', 0, PARAM_INT);

$ref = optional_param('ref', 0, PARAM_INT);
$step = optional_param('step', 0, PARAM_INT);
$lev = optional_param('lev', 0, PARAM_INT);
        
$contents   = optional_param('contents', "", PARAM_CLEAN);
$title    = optional_param('title', '', PARAM_CLEAN);

$contentId   = optional_param('contentId', 0, PARAM_INT);
$category_field = 0;
        
$PAGE->set_url('/mod/jinotechboard/write_new.php', array(
    'b' => $b,
    'mode'  => $mode,
    'confirmed'=>$confirmed,
    'contentId'=>$contentId
));


$page_params = array('mode'=>$mode, 'b'=>$b, 'contentId'=>$contentId);

require_login();

if ($b) {

    if (! $board = $DB->get_record("jinotechboard", array("id" => $b))) {
        print_error('invalidboardid', 'jinotechboard');
    }
    if (! $course = $DB->get_record("course", array("id" => $board->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
        print_error('missingparameter');
    }else {
        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
    }
    
} else {
    print_error('missingparameter');
}



if($course->id != SITEID){
    require_course_login($course);
}

$PAGE->set_cm($cm, $course, $board);
$PAGE->set_context($modcontext);

require_once('write_form.php');

$mform = new write_form("write_new.php");
if ($mform->is_cancelled()) {
    redirect("$CFG->wwwroot/mod/jinotechboard/index.php?b=$b");
}

echo $OUTPUT->header();
if($mode == ""){
    $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
    if (!$cm->visible and !has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
        print_error("activityiscurrentlyhidden");
    }
    if (isset($_SERVER["HTTP_REFERER"])) {
        $SESSION->fromurl = $_SERVER["HTTP_REFERER"];
    } else {
        $SESSION->fromurl = '';
    }

    if (!has_capability('mod/jinotechboard:write', $coursecontext)) {
        notice('cannotaddpost', 'jinotechboard');
    }

    $post = new object();
    $post->course = $course->id;
    $post->board = $b;
    $post->title = "";
    $post->ref = 0;
    $post->step = 0;
    $post->lev = 0;
    $post->category = 0;
    $post->isnotice = 0;
    $post->isprivate = 0;
    $post->viewcnt = 0;
    $post->mode = "write";

     if (isset($groupid)) {
        $post->groupid = $groupid;
    } else {
        $post->groupid = groups_get_activity_group($cm);
    }


    jinotechboard_set_return();

}else if($mode=="edit" && $contentId>0){ 

    $data = $DB->get_record('jinotechboard_contents', array('id'=>$contentId));

    if (!has_capability('mod/jinotechboard:edit', $modcontext) || $data->userid != $USER->id) {
        print_error('cannoteditpost', 'jinotechboard');
    }
    $post = new stdClass();

    $post->title=$data->title;
    $post->$contents = $data->contents;
    $post->category = $data->category;
    $post->mode = $mode;
    $post->groupid = ($data->groupid == -1) ? 0 : $data->groupid;

    unset($SESSION->fromjinotechboard);

}else if($mode=="reply"){
    if (!has_capability('mod/jinotechboard:reply', $modcontext) || $board->allowreply==0) {
        notice('cannotreplypost', 'jinotechboard');
    }

    $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
    if (!$cm->visible and !has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
        print_error("activityiscurrentlyhidden");
    }

    $parentData = $DB->get_record('jinotechboard_contents', array('id'=>$contentId));

    $post = new stdClass();
    $post->title= "Re:".$parentData->title;
    $post->groupid = ($parentData->groupid == -1) ? 0 : $parentData->groupid;

    unset($SESSION->fromjinotechboard);


}else if($mode=="delete"){
   if (! $post = $DB->get_record('jinotechboard_contents', array('id'=>$contentId))) {
        print_error('invalidpostid', 'forum');
   } 

   if ( !(($post->userid == $USER->id && has_capability('mod/jinotechboard:delete', $modcontext)))) {
        print_error('cannotdeletepost', 'forum');
   }

   if (!empty($confirm) && confirm_sesskey()) {
        

        if ($post->userid == $USER->id  ) {	

        
            $DB->delete_records_select("jinotechboard_contents","id=? and board=?", array($contentId, $b) );
            $msg = get_string('Deletionshavebeencompleted','jinotechboard');
             add_to_log($post->course, "jinotechboard", "delete contents",
                   "view.php?id=$cm->id", "$post->board", $cm->id);
            redirect("view.php?b=$b");

        }




    } else { 

        jinotechboard_set_return();
        $PAGE->navbar->add(get_string('delete', 'jinotechboard'));
        $PAGE->set_title($course->shortname);
        $PAGE->set_heading($course->fullname);


            echo $OUTPUT->header();
            echo $OUTPUT->confirm(get_string("deletesure", "jinotechboard"),
                         "write_new.php?mode=delete&confirmed=1&contentId=$contentId",
                         $CFG->wwwroot.'/mod/jinotechboard/view.php?f='.$post->board);
    }
    echo $OUTPUT->footer();
    die;

}
 
if (!isset($forum->maxattachments)) {  
    $forum->maxattachments = 3;
}

require_once('write_form.php');


$draftid_editor = file_get_submitted_draft_itemid('contents');
$currenttext = file_prepare_draft_area($draftid_editor, $modcontext->id, 'mod_jinotechboard', 'post', empty($post->id) ? null : $post->id, write_form::editor_options(), $post->contents);


    

$mform->set_data(
    array(     
        'title'=>$post->title,
        'description'=>array(
            'text'=>$currenttext,
            'format'=>empty($post->messageformat) ? editors_get_preferred_format() : $post->messageformat,
            'itemid'=>$draftid_editor
        ),
        'useridfrom'=>$post->useridfrom,
        'mode'=>$post->mode,
        'phonefrom'=>$post->phonefrom,
        'course'=>$post->course) +
        $page_params

    );  

        
if ($fromform = $mform->get_data()) { 
    $fromform->itemid        = $fromform->description['itemid'];
    $fromform->contents       = $fromform->description['text'];
    $fromform->userid       = $USER->id;
    $fromform->timecreated = time();
    $fromform->timemodified = time();

    if($mode=="edit" && $contentId>0){

        $data = $DB->get_record('jinotechboard_contents', array('id'=>$contentId));

        if (!has_capability('mod/jinotechboard:edit', $modcontext)  || $data->userid != $USER->id ) {
            notice('cannoteditpost', 'jinotechboard');
        }



        $newdata = new object();
        $newdata->course = $course->id;
        $newdata->board = $b;
        $newdata->userid		= $USER->id;
        
        $newdata->title			= $title;
        $newdata->contents			= $contents;
        
        $newdata->id			= $contentId;
        $newdata->category              = $category;
        $newdata->isnotice              =$isnotice;
        $newdata->isprivate             = $isprivate;
        $newdata->timemodified = time();

        $DB->update_record('jinotechboard_contents', $newdata);
        $draftitemid = file_get_submitted_draft_itemid('attachments');                        
        file_save_draft_area_files($draftitemid, $modcontext->id, 'mod_jinotechboard', 'attachment', $newdata->id);

        
        redirect("$CFG->wwwroot.'/mod/jinotechboard/view.php?b='.$b");

    }else if($mode=="reply"){
        if (!has_capability('mod/jinotechboard:reply', $modcontext) || $board->allowreply==0 ) {
            notice('cannotreplypost', 'jinotechboard');
        }

        $updatedata = new object();

        $DB->execute("update {jinotechboard_contents} set step = step + 1 where board = ? and ref = ? and step > ?", array($board->id, $ref, $step ));

        $newdata = new object();

        $newdata->course = $course->id;
        $newdata->board = $b;
        $newdata->userid		= $USER->id;
        $newdata->title			= $title;
        $newdata->category              = $category;
        $newdata->contents			= $contents;
        $newdata->isnotice              =$isnotice;
        $newdata->isprivate             = $isprivate;
        $newdata->ref = $ref;
        $newdata->step = $step+1;
        $newdata->lev = $lev+1;

        $newdata->viewcnt = 0;
        $newdata->timecreated			= time();
        $newdata->timemodified = time();
        $newid = $DB->insert_record('jinotechboard_contents', $newdata);

        $draftitemid = file_get_submitted_draft_itemid('attachments');                        
        file_save_draft_area_files($draftitemid, $modcontext->id, 'mod_jinotechboard', 'attachment', $newid);

       redirect($CFG->wwwroot.'/mod/jinotechboard/view.php?b='.$b);

    }else{ 
        if (!has_capability('mod/jinotechboard:write', $modcontext)) {
            notice('cannotaddpost', 'jinotechboard');
        }

        $newdata = new object();

        if (!empty($title) && confirm_sesskey(sesskey())) {   


            $newdata->course = $course->id;
            $newdata->board = $b;
           
            $newdata->userid		= $USER->id;
           
            $newdata->title			= $title;
             $newdata->category              = $category;
            $newdata->contents			= $contents;
            $newdata->ref = 0;
            $newdata->step = 0;
            $newdata->lev = 0;
            $newdata->isnotice              =$isnotice;
            $newdata->isprivate             = $isprivate;
            $newdata->viewcnt = 0;
           
            $newdata->timecreated			= time();
            $newdata->timemodified = time();

            if ($newid = $DB->insert_record('jinotechboard_contents', $newdata)){
                    $newdata->id = $newid;
                    $DB->set_field_select('jinotechboard_contents', "ref",$newid,"id=$newid");
            }
        }
		
        $draftitemid = file_get_submitted_draft_itemid('attachments');
        file_save_draft_area_files($draftitemid, $modcontext->id, 'mod_jinotechboard', 'attachment', $newid);

        redirect ($CFG->wwwroot.'/mod/jinotechboard/view.php?b='.$b);

    }
}

        

echo $OUTPUT->box_start('generalbox');
$mform->display();
echo $OUTPUT->box_end();
echo $OUTPUT->footer();
  
?>


