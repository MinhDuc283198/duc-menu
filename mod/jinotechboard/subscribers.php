<?php


require_once("../../config.php");
require_once("lib.php");

$id    = required_param('id',PARAM_INT);          
$group = optional_param('group',0,PARAM_INT);     
$edit  = optional_param('edit',-1,PARAM_BOOL);    

$url = new moodle_url('/mod/jinotechboard/subscribers.php', array('id'=>$id));
if ($group !== 0) {
    $url->param('group', $group);
}
if ($edit !== 0) {
    $url->param('edit', $edit);
}
$PAGE->set_url($url);

$jinotechboard = $DB->get_record('jinotechboard', array('id'=>$id), '*', MUST_EXIST);
$course = $DB->get_record('course', array('id'=>$jinotechboard->course), '*', MUST_EXIST);
if (! $cm = get_coursemodule_from_instance('jinotechboard', $jinotechboard->id, $course->id)) {
    $cm->id = 0;
}

require_login($course->id, false, $cm);

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
if (!has_capability('mod/jinotechboard:viewsubscribers', $context)) {
    print_error('nopermissiontosubscribe', 'forum');
}

unset($SESSION->fromdiscussion);

add_to_log($course->id, "jinotechboard", "view subscribers", "subscribers.php?id=$jinotechboard->id", $jinotechboard->id, $cm->id);

$jinotechboardoutput = $PAGE->get_renderer('mod_jinotechboard');
$currentgroup = groups_get_activity_group($cm);
$options = array('jinotechboardid'=>$jinotechboard->id, 'currentgroup'=>$currentgroup, 'context'=>$context);
$existingselector = new forum_existing_subscriber_selector('existingsubscribers', $options);
$subscriberselector = new forum_potential_subscriber_selector('potentialsubscribers', $options);
$subscriberselector->set_existing_subscribers($existingselector->find_users(''));

if (data_submitted()) {
    require_sesskey();
    $subscribe = (bool)optional_param('subscribe', false, PARAM_RAW);
    $unsubscribe = (bool)optional_param('unsubscribe', false, PARAM_RAW);
    
    if (!($subscribe xor $unsubscribe)) {
        print_error('invalidaction');
    }
    if ($subscribe) {
        $users = $subscriberselector->get_selected_users();
        foreach ($users as $user) {
            if (!jinotechboard_subscribe($user->id, $id)) {
                print_error('cannotaddsubscriber', 'forum', '', $user->id);
            }
        }
    } else if ($unsubscribe) {
        $users = $existingselector->get_selected_users();
        foreach ($users as $user) {
            if (!jinotechboard_unsubscribe($user->id, $id)) {
                print_error('cannotremovesubscriber', 'forum', '', $user->id);
            }
        }
    }
    $subscriberselector->invalidate_selected_users();
    $existingselector->invalidate_selected_users();
    $subscriberselector->set_existing_subscribers($existingselector->find_users(''));
}

$strsubscribers = get_string("subscribers", "forum");
$PAGE->navbar->add($strsubscribers);
$PAGE->set_title($strsubscribers);
$PAGE->set_heading($COURSE->fullname);
if (has_capability('mod/jinotechboard:managesubscriptions', $context)) {
    $PAGE->set_button(jinotechboard_update_subscriptions_button($course->id, $id));
    if ($edit != -1) {
        $USER->subscriptionsediting = $edit;
    }
} else {
    unset($USER->subscriptionsediting);
}
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('forum', 'forum').' '.$strsubscribers);
if (empty($USER->subscriptionsediting)) {
    echo $jinotechboardoutput->subscriber_overview(jinotechboard_subscribed_users($course, $jinotechboard, $currentgroup, $context), $jinotechboard, $course);
} else if (jinotechboard_is_forcesubscribed($jinotechboard)) {
    $subscriberselector->set_force_subscribed(true);
    echo $jinotechboardoutput->subscribed_users($subscriberselector);
} else {
    echo $jinotechboardoutput->subscriber_selection_form($existingselector, $subscriberselector);
}
echo $OUTPUT->footer();