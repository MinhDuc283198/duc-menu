<?php

//모드
// 수정 : mode 가 edit
// DB 수정인 경우: mode가 edit 이고 confirm 이 1인경우

// 리플 : mode 가 비어있고 boardId 가 있는 경우
// DB 리플 : mode 가 비어있고 boardId 가 있는 경우 confirm 이 1인경우

// 새글쓰기: mode가 비어 있고 boardId도 비어있는 경우: 나머지 경우
// DB  입력인 경우: mode 가 비어있고 boardId 가 비어 있고 confirm 가 1인 경우

        require_once('../../../config.php');
        require_once('../lib.php');
        require_once($CFG->libdir.'/completionlib.php');
    //	require_once($CFG->dirroot .'/local/jinoboard/lib.php');

//        $reply   = optional_param('reply', 0, PARAM_INT);
        $b  = optional_param('b', 0, PARAM_INT);
        $confirmed   = optional_param('confirmed', 0, PARAM_INT);
        $mode   = optional_param('mode', "", PARAM_CLEAN);
        $categoryId   = optional_param('categoryId', 0, PARAM_INT);
//        $edit    = optional_param('edit', 0, PARAM_INT);
//        $delete  = optional_param('delete', 0, PARAM_INT);
        //$prune   = optional_param('prune', 0, PARAM_INT);
        //$name    = optional_param('name', '', PARAM_CLEAN);
//        $confirm = optional_param('confirm', 0, PARAM_INT);
        //$category = optional_param('category', 0, PARAM_INT);
        
        //$isnotice = optional_param('isnotice', 0, PARAM_INT);
        //$isprivate = optional_param('isprivate', 0, PARAM_INT);
        
        //$ref = optional_param('ref', 0, PARAM_INT);
        //$step = optional_param('step', 0, PARAM_INT);
        //$lev = optional_param('lev', 0, PARAM_INT);
        
        //$groupid = optional_param('groupid', null, PARAM_INT);
        //$contents   = optional_param('contents', "", PARAM_CLEAN);
        $title    = optional_param('title', '', PARAM_CLEAN);

        $PAGE->set_url('/mod/jinotechboard/cateogyr/write.php', array(
//                'reply' => $reply,
                'b' => $b,
                'mode'  => $mode,
//                'delete'=> $delete,
         //       'prune' => $prune,
         //       'name'  => $name,
                'confirmed'=>$confirmed,
                'categoryId'=>$categoryId,
                ));
        //these page_params will be passed as hidden variables later in the form.
        //$page_params = array('b'=>$b, 'edit'=>$edit);

        
        if ($b) {

            if (! $board = $DB->get_record("jinotechboard", array("id" => $b))) {
                print_error('invalidboardid', 'jinotechboard');
            }
            if (! $course = $DB->get_record("course", array("id" => $board->course))) {
                print_error('coursemisconf');
            }

            if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
                print_error('missingparameter');
            }
            // move require_course_login here to use forced language for course
            // fix for MDL-6926

//            $strboards = get_string("modulenameplural", "jinotechboard");
//            $strboard = get_string("modulename", "jinotechboard");
        } else {
            print_error('missingparameter');
        }
        require_course_login($course, true, $cm);
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        $sitecontext = get_context_instance(CONTEXT_SYSTEM);
        $PAGE->set_context($context);
        require_login();
        $PAGE->set_heading($board->name);

        echo $OUTPUT->header();
	


/*	TODO if (!has_capability('local/jinobard:write', $context)) {
		require_capability('local/jinobard:write', $context);
	}*/



        
//        $parentId   = optional_param('parentId', 0, PARAM_INT);//리플시만
//        $b   = optional_param('b', 0, PARAM_INT);
//        $num_notice   = optional_param('num_notice', 0, PARAM_INT);
//        $category_field = 0;
        if($confirmed==0){//데이터 불러오기
            if($mode=="edit" && $categoryId>0){ //수정
		$data = $DB->get_record('jinotechboard_category', array('id'=>$categoryId));
                
                if (!has_capability('mod/jinotechboard:categorymanager', $context) || $data->userid != $USER->id) {
                    notice('cannotmanagecategory', 'jinotechboard');
                }
                
		$title=$data->title;
		//$contents=$data->contents;
                
		//$category_field =$data->category_field;

		//$filedata = $DB->get_records("jino_board_files", array("boardId"=>$boardId));
	
            }else{ // 새글쓰기
                if (!has_capability('mod/jinotechboard:categorymanager', $context)) {
                    notice('cannotmanagecategory', 'jinotechboard');
                }
            }
        }else if($confirmed == 1){ //데이터 입력하기
            if($mode=="edit" && $categoryId>0){ //수정
                $data = $DB->get_record('jinotechboard_category', array('id'=>$categoryId));
                
                if (!has_capability('mod/jinotechboard:categorymanager', $context)  || $data->userid != $USER->id ) {
                    notice('cannotmanagecategory', 'jinotechboard');
                }
		
		$newdata = new object();
                //$newdata->course = $course->id;
                $newdata->board = $b;
                //$newdata->category;
                $newdata->userid		= $USER->id;
		//$newdata->name		= fullname($USER, true);
		$newdata->title			= $title;
		//$newdata->contents			= $contents;
		//$newdata->num_notice		= ($num_notice==1)?1:0;
		$newdata->id			= $categoryId;
                //$newdata->category              = $category;
                //$newdata->isnotice              =$isnotice;
                //$newdata->isprivate             = $isprivate;
		$newdata->timemodified = time();

                $DB->update_record('jinotechboard_category', $newdata);

		

		//리스트 이동
		echo '<script language="javascript">';
		echo 'document.location.href="'.$CFG->wwwroot.'/mod/jinotechboard/category/view.php?b='.$b.'";';
		echo '</script>';




            }else{ // 새글쓰기
                if (!has_capability('mod/jinotechboard:categorymanager', $context)) {
                    notice('cannotmanagecategory', 'jinotechboard');
                }
                
		$newdata = new object();
//                $newdata->course = $course->id;
                $newdata->board = $b;
                //$newdata->category;
                $newdata->userid		= $USER->id;
		//$newdata->name		= fullname($USER, true);
		$newdata->title			= $title;
                //$newdata->category              = $category;
		//$newdata->contents			= $contents;
                //$newdata->ref = 0;
                //$newdata->step = 0;
                //$newdata->lev = 0;
                //$newdata->isnotice              =$isnotice;
                //$newdata->isprivate             = $isprivate;
                //$newdata->viewcnt = 0;
		//$newdata->num_notice		= ($num_notice==1)?1:0;

		$newdata->timecreated = time();

		$DB->insert_record('jinotechboard_category', $newdata);
		
/*
		$file = $_FILES['contentfile'];

		foreach($file["name"] as $key => $filename) {
			if (!$filename) continue;

			if (!is_uploaded_file($file['tmp_name'][$key])) continue;
			$contentdir = local_jinoboard_get_content_dir();
    
			$tmpfile = $file['tmp_name'][$key];
			$filesize = $file['size'][$key];
			
			$filehash = sha1($tmpfile);
			
			$today = getdate();
			$savedir = '/'.$today['year'].'/'.$today['mon'].'/'.$today['mday'].'/'.$newdata->id;
			
			// Copy zip file to content directory
			local_jinoboard_check_dir_exists($contentdir.$savedir);
			echo ("tempfile:".$tmpfile);
			echo("<br>fff:".$contentdir.$savedir.'/'.$filehash );
			if (!move_uploaded_file($tmpfile, $contentdir.$savedir.'/'.$filehash )) {
		//        print_error('moveuploadedfile', 'local_lcms');
				echo("잘못되었음");
			}
			
			// Save file info to DB
			$fileinfo = new stdClass();


			$fileinfo->filepath = $savedir.'/'.$filehash;
			$fileinfo->filename = $filename;
			$fileinfo->filesize = $filesize;
			$fileinfo->boardId = $newdata->id;
			$DB->insert_record('jino_board_files', $fileinfo);
		}*/
			//리스트 이동
		echo '<script language="javascript">';
		echo 'document.location.href="'.$CFG->wwwroot.'/mod/jinotechboard/category/view.php?b='.$b.'";';
		echo '</script>';
		
            }
        }
//print_r($USER);

?>
<html>
<head>
<title>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script language='javascript' type='text/javascript'>

</script>


</head>
<body>

<div class="dialog_title">
</div>

<div style="padding-top:60px; padding-left:40px;">
<form id="frm_group" action="<?php echo($CFG->wwwroot."/mod/jinotechboard/category/write.php");?>" method='post' ENCTYPE='multipart/form-data'>
<input type="hidden" name="confirmed" value="1" />
<input type="hidden" name="b" value="<?php echo($b);?>" />
<?php if($mode=="edit"){?>
<input type="hidden" name="categoryId" value="<?php echo($categoryId);?>" />
<input type="hidden" name="mode" value="edit" />
<?php }?>
<table style="width:95%;" border=0 cellpadding="0" cellspacing="0">

	<tr>
		<th><?php echo get_string('title', 'jinotechboard'); ?></th>
		<td style="padding-left:2px;" class="nobody"><input type="text" name="title" value="<?php echo($title);?>" style="width:95%" /></td>
	</tr>
        
        
        
	
</table>
</form> 
</div>

<div style="margin-top:20px; text-align:center;">
	<input type="button" class="create_btn" value="<?php echo get_string('save', 'jinotechboard'); ?>"  onClick="javascript:document.getElementById('frm_group').submit()"/>
	<input type="button" class="create_btn" value="<?php echo get_string('cancel', 'jinotechboard'); ?>" onclick="document.location.href='<?php echo($CFG->wwwroot);?>/mod/jinotechboard/category/view.php?b=<?php echo($b);?>';"/>
</div>

</body>
</html>


<?php 

  echo $OUTPUT->footer();
  
  ?>


