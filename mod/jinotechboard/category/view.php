<?php


        require_once('../../../config.php');
        require_once('../lib.php');
        require_once($CFG->libdir.'/completionlib.php');


        $id          = optional_param('id', 0, PARAM_INT);       // Course Module ID
        $b           = optional_param('b', 0, PARAM_INT);        // Board ID
        $params = array();
        if ($id) {
            $params['id'] = $id;
        } else {
            $params['b'] = $b;

        }
        
        
        $PAGE->set_url('/mod/jinotechboard/category/view.php', $params);

        if ($id) {
            if (! $cm = get_coursemodule_from_id('jinotechboard', $id)) {
                print_error('invalidcoursemodule');
            }
            if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
                print_error('coursemisconf');
            }
            if (! $board = $DB->get_record("jinotechboard", array("id" => $cm->instance))) {
                print_error('invalidboardid', 'jinotechboard');
            }

        } else if ($b) {
            if (! $board = $DB->get_record("jinotechboard", array("id" => $b))) {
                print_error('invalidboardid', 'jinotechboard');
            }
            if (! $course = $DB->get_record("course", array("id" => $board->course))) {
                print_error('coursemisconf');
            }

            if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
                print_error('missingparameter');
            }

        } else {
            print_error('missingparameter');
        }
        
        require_course_login($course, true, $cm);

        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        $PAGE->set_context($context);
        $PAGE->set_title(format_string($board->name));
//        $PAGE->add_body_class('jinotechboard-'.$board->type);
        $PAGE->set_heading(format_string($course->fullname));

        //TODO 해더에 타이틀 추가하기, 액션추가
//        $PAGE->set_heading($board->name);

        echo $OUTPUT->header();
        

        /// Some capability checks.
        if (empty($cm->visible) and !has_capability('moodle/course:viewhiddenactivities', $context)) {
            notice(get_string("activityiscurrentlyhidden"));
        }


        if (!has_capability('mod/jinotechboard:categorymanager', $context)) {
            notice('cannotmanagecategory', 'jinotechboard');
        }


            /// Okay, we can show the discussions. Log the forum view.
        if ($cm->id) {
            add_to_log($course->id, "jinotechboard", "view category", "category/view.php?id=$cm->id", "$board->id", $cm->id);
        } else {
            add_to_log($course->id, "jinotechboard", "view category", "category/view.php?id=$cm->id", "$board->id");
        }


        
        
        $categories = jinotechboard_get_categories($cm, $b);
        
?>
        <script>
            function deleteCategory(b, categoryId) {
                if(confirm("<?php echo get_string('suredeleteselectedcategory', 'jinotechboard'); ?>")) {
			document.deleteForm.b.value = b;
			document.deleteForm.categoryId.value = categoryId;
			document.deleteForm.action = '<?php echo($CFG->wwwroot);?>/mod/jinotechboard/category/delete.php';
			document.deleteForm.submit();
			
		}
	}
        </script>
        
        <form name="deleteForm" method="post">
            <input type="hidden" name="categoryId">
            <input type="hidden" name="b" value="<?php echo($board->id);?>">
        </form>
<?php
        
        
        echo '<div class="jinotechboard">';
        echo '<table cellspacing="0" cellpadding="5" width=100% align=center class="board_table">
            <tr>
                <th>'.get_string("no", 'jinotechboard').'</th>
                <th>'.get_string("order", 'jinotechboard').'</th>
                <th>'.get_string("title", 'jinotechboard').'</th>
                <th>'.get_string("count", 'jinotechboard').'</th>
                <th>'.get_string("action", 'jinotechboard').'</th>
            </tr>';

        //$totalcount= jinotechboard_get_discussions_count($cm, $searchfield="", $searchvalue="");

        /*$offset = 0;
        if ($page != 0) {
                $offset = $page * $perpage;
        }*/


        $contentsRS = jinotechboard_get_categories($cm, $b, $boardsort="orderseq asc, title asc");
        $num = 1;
        if(!$contentsRS){
            echo "<tr><td colspan=5 align=center>".get_string("nocontent", 'jinotechboard')."</td></tr>";
             //return;
        }else {
            foreach ($categories as $category) {
                echo("<tr>
                    <td align=center>".$num."</td>
                        
                    <td>order</td>
                    <td align=center>".$category->title."</td>
                    <td align=center>count</td>
                    <td align=center>
                        <a href='#' onclick='document.location.href=\"".$CFG->wwwroot."/mod/jinotechboard/category/write.php?mode=edit&b=".$board->id."&categoryId=".$category->id."\"'>
			<input type=\"button\" value=\"".get_string('edit', 'jinotechboard')."\" class=\"create_btn\" /></a>
                        <input type=\"button\" value=\"".get_string('delete', 'jinotechboard')."\" class=\"create_btn\" onclick=\"deleteCategory('".$board->id."', '".$category->id."');\" /></td>
                        </tr>");
                $num++ ; 
            }

        }
        echo "</table>
        </div>";

        
        

        if (has_capability('mod/jinotechboard:categorymanager', $context)) {
                    echo "<div>";
            echo "<input type='button' onclick='document.location.href =\"".$CFG->wwwroot."/mod/jinotechboard/view.php?b=".$cm->instance."\"' class='create_btn' value='".get_string('backtoboard', 'jinotechboard')."' />";
            echo "<input type='button' onclick='document.location.href =\"".$CFG->wwwroot."/mod/jinotechboard/category/write.php?b=".$cm->instance."\"' class='create_btn' value='".get_string('newpost', 'jinotechboard')."' />";
            echo "</div>";
            
        }
         

        echo $OUTPUT->footer($course);

