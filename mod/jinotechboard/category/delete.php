<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<?php 

//	if($mode == 'board'){
		require_once('../../../config.php');
		//require_once($CFG->dirroot .'/mod/jinotechboard/lib.php');

		$categoryId   = optional_param('categoryId', 0, PARAM_INT);
		$b   = optional_param('b', 0, PARAM_INT);
                
                if ($b) {
                    if (! $board = $DB->get_record("jinotechboard", array("id" => $b))) {
                        print_error('invalidboardid', 'jinotechboard');
                    }
                    if (! $course = $DB->get_record("course", array("id" => $board->course))) {
                        print_error('coursemisconf');
                    }

                    if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
                        print_error('missingparameter');
                    }

                } else {
                    print_error('missingparameter');
                }
                
                require_course_login($course, true, $cm);
                
                $context = get_context_instance(CONTEXT_MODULE, $cm->id);
                
                if (!has_capability('mod/jinotechboard:categorymanager', $context)) {
                    notice('cannotmanagecategory', 'jinotechboard');
                }

                
        

		$db_userid = $DB->get_field('jinotechboard_category', 'userid', array('id'=>$categoryId));
		
		if ($db_userid == $USER->id ) //(|| isAdmin())
		{	
			
//			if($jino_board_category->id && $b_id)
//			{
				// 첨부파일 삭제
//				delete_jinoboard_content_files($jino_board_category->id,$b_id);

				// 첨부파일 정보 삭제
//				$DB->delete_records_select("jino_board_files","category='$jino_board_category->id' and num_main='$b_id'");

				// 코멘트삭제
				//$num_comment = $DB->count_records_select("jino_board_comment","category=? and num_main=?", array($boardType, $boardId));
				//$DB->set_field_select('jino_board', 'num_comment', intval($num_comment), " id=?", array($num_comment));		// 덧글 갯수 업데이트

				$DB->delete_records_select("jinotechboard_category", "id=?", array($categoryId));

				// 게시물 목록으로 이동
				$msg = get_string('Deletionshavebeencompleted','jinotechboard');
//				board_redirect_alert($this_url, $msg);

				echo '<script language="javascript">';
				echo 'alert("'.$msg.'");';
				echo 'document.location.href="'.$CFG->wwwroot.'/mod/jinotechboard/category/view.php?b='.$b.'"';
				echo '</script>';
//			}

		}
		else
		{
			// ========================
			// 권한이 없는 경우
			// ========================
			$msg =get_string('no_delete_permission','jinotechboard');
		//	board_redirect_alert($this_url."&proc_mode=view&b_id=".$b_id,$msg);

				echo '<script language="javascript">';
				echo 'alert("'.$msg.'");';
				echo 'document.location.href="'.$CFG->wwwroot.'/mod/jinotechboard/category/view.php?b='.$b.'"';
				echo '</script>';

		}

//	}
/*
	// ========================
	// 덧글 삭제
	// ========================
	elseif($mode == 'comment')
	{	

		$db_userid = $DB->get_field('jino_board_comment', 'str_userid',array('id'=>$comm_id,'num_main'=>$b_id,'category'=>$jino_board_category->id));

		if(($db_userid == $USER->username) || isadmin())
		{
			if($jino_board_category->id && $b_id && $comm_id)
			{
				$DB->delete_records_select("jino_board_comment","id='$comm_id' and num_main='$b_id' and category='$jino_board_category->id'");

				$num_comment = $DB->count_records_select("jino_board_comment","category='$jino_board_category->id' and num_main='$b_id'");
				$DB->set_field_select('jino_board', 'num_comment', intval($num_comment), " id='$b_id'");		// 덧글 갯수 업데이트
			}
		}
		else
		{
			$msg =get_string('no_delete_permission','local_jinoboard');
			board_redirect_alert($this_url."&proc_mode=view&b_id=".$b_id,$msg, $msg);
		}

		$msg =get_string('Deletionshavebeencompleted','local_jinoboard');
		board_redirect_alert($this_url."&proc_mode=view&b_id=".$b_id,$msg);
	}
*/

?>

</body>
</html>