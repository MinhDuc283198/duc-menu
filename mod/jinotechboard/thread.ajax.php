<?php

require_once dirname(dirname(dirname (__FILE__))).'/config.php';
require_once('lib.php');

$page = optional_param('page', 1, PARAM_INT);
$instance = optional_param('instance', 0, PARAM_INT);
$cmid = optional_param('cmid', 0, PARAM_INT);
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string

if (!$board = $DB->get_record("jinotechboard", array("id" => $instance))) {
    print_error('invalidboardid', 'jinotechboard');
}
if (!$course = $DB->get_record("course", array("id" => $board->course))) {
    print_error('coursemisconf');
}
if (!$cm = get_coursemodule_from_id("jinotechboard", $cmid)) {
    print_error('missingparameter');
}

$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

$PAGE->set_context($context);

$board = $DB->get_record("jinotechboard", array("id" => $instance));

$contents = jinotechboard_thread_contents($instance, $page, JINOTECHBOARD_THREAD_DEFAULT, $searchfield, $searchvalue);
$next = jinotechboard_thread_contents($instance, $page+1, JINOTECHBOARD_THREAD_DEFAULT, $searchfield, $searchvalue);

$next_count = count($next);
$datetimeformat = "Y년 h월 d일, l , a g:i";

foreach($contents as $content){
         $contentid = $content->id;
                $content->id = $content->urid;
                $userdate = userdate($content->timecreated);

                if (!empty($content->itemid)) {
                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }

                if ($content->lev) {
                    if ($content->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
                        $step = $content->lev;
                        $date_left_len = $step * 30;
                    } else {
                        $step = JINOTECHBOARD_INDENT_DEFAULT;
                        $date_left_len = $step * 30;
                    }
                    
                    switch($content->lev){
                        case 1 : $calcwidth = 30; break; 
                        case 2 : $calcwidth = 60; break;
                        case 3 : $calcwidth = 90; break;
                        case 4 : $calcwidth = 120; break;
                        default : $calcwidth = 120; break;
                    }      
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - '.$calcwidth.'px) !important;"';
                    $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="Reply" /> ';
                } else {
                    $date_left = 'style="width:calc(100% - 0px) !important;"';
                    $step_icon = '';
                }
                echo "<li ". $date_left .">";
                echo '<div class="thread-left">' . $OUTPUT->user_picture($content) . '</div>';
                echo "<div class='thread-content'><h1 class='thread-post-title'>" . $step_icon;
                
                $parentid = 0;
                if ($content->lev != 0) {
                    $parent = $DB->get_record('jinotechboard_contents', array('id' => $content->ref));
                    $parentid = $parent->userid;
                }
                if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $context) && $USER->id != $content->userid && $parentid != $USER->id) {
                    echo $content->title;
                } else {
                  echo '<a onclick="contentMove(\''.$contentid.'\')" style="cursor: pointer;">' . $content->title . '</a>';
                }
                if ($content->isprivate) {
                    echo "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
                }
                echo '&nbsp;'.$filecheck;
                echo "</h1>";
                $by = new stdClass();
                $by->name = fullname($content);
                $by->date = userdate($content->timemodified);
                echo '<span class="thread-post-meta">' . get_string('bynameondate', 'forum', $by) . '</span>';
				
				if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $context) && $USER->id != $content->userid && $parentid != $USER->id) {
                    echo '<p class="thread-post-content">' . get_string('content:secret', 'mod_jinotechboard') . '</p>';
                } else {
                    echo '<p class="thread-post-content">' . mb_strimwidth(strip_tags($content->contents), 0, 630,'...', 'utf-8') . '</p>';
                }
		echo "</div>";
		
                echo "<div class='thread-right'>";                
                if ($board->allowrecommend)
                    echo "<div class='thread-right'><div class='count-block recomm-count'>" . $content->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_jinotechboard') . "</span></div>";
                if ($board->allowcomment)
                    echo "<div class='count-block reply-count'>" . $content->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_jinotechboard') . "</span></div>";
                echo "<div class='count-block view-count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_jinotechboard') . "</span></div>";
                echo "</div>";
                echo "</li>";
}
    if ($next_count > 0) {
?>
    <div id="next_btn">
        <input type="button" class="next" value="<?php echo get_string('next:content', 'mod_jinotechboard');?>" onclick="ThreadContent()">
    </div>
<?php
    }