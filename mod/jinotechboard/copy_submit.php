<?php
require (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot . "/chamktu/lib.php");

$contents_id = optional_param('contents_id', '', PARAM_RAW);
$boards_id = optional_param('boards_id', '', PARAM_RAW);

$contents_id = json_decode($contents_id);
$boards_id = json_decode($boards_id);
foreach($boards_id as $board_id){
    $board = $DB->get_record('jinotechboard', array('id'=>$board_id));
    
    $cm = get_coursemodule_from_instance('jinotechboard', $board->id, $board->course, false, MUST_EXIST);
    
    $classname = context_helper::get_class_for_level(CONTEXT_MODULE);
    
    $context = $classname::instance($cm->id);
    
    foreach($contents_id as $content_id){
        $content = $DB->get_record('jinotechboard_contents', array('id'=>$content_id));
        $user = $DB->get_record('user', array('id'=>$content->userid));
        
        $cm2 = get_coursemodule_from_instance('jinotechboard', $content->board, $content->course, false, MUST_EXIST);
    
        $context2 = $classname::instance($cm2->id);
        
        if (!has_capability('mod/jinotechboard:write', $context)) {
            notice('cannotaddpost', 'jinotechboard');
        }

        $newdata = new object();

        $newdata->course = $board->course;
        $newdata->board = $board->id;
        $newdata->userid = $content->userid;
        $newdata->title = $content->title;
        $newdata->category = $content->category;
        $newdata->contents = $content->contents;
        
        $newdata->isnotice = 0;
        $newdata->isprivate = 0;

        $newdata->ref = 0;
        $newdata->step = 0;
        $newdata->lev = 0;
        if ($board->type == 2) {
        $newdata->isprivate = $content->isprivate;
        }

        $newdata->viewcnt = 0;

        $newdata->timecreated = time();
        $newdata->timemodified = time();
        if ($board->type == 1) {
            $newdata->isnotice = $content->isnotice;
            $newdata->timeend = $content->timeend;
        }


        if ($newid = $DB->insert_record('jinotechboard_contents', $newdata)) {
            $newdata->id = $newid;
            $DB->set_field_select('jinotechboard_contents', "ref", $newid, "id=$newid");

            $params = array(
                'objectid' => $newdata->id,
                'context' => $context,
                'other' => array(
                    'jinotechboardid' => $board->id,
                )
            );

            $event = \mod_jinotechboard\event\content_created::create($params);
            $event->add_record_snapshot('jinotechboard_contents', $newdata);
            $event->trigger();
        }

        
        $fs = get_file_storage();
    
        $captions = $fs->get_area_files($context2->id, 'mod_jinotechboard', 'attachment', $content_id, "itemid, filepath, filename", false);
        print_object($captions);
        if (count($captions) > 0) {
            foreach($captions as $caption) {
                $captionname = $caption->get_filename();
//                if(preg_match(CAPION_FILE_PATTEN_KO, $captionname) || preg_match(CAPION_FILE_PATTEN_EN, $captionname)){
                    $content = $caption->get_content();
                    $fileinfo = array(
                        'contextid' => $context->id, 
                        'component' => 'mod_jinotechboard',     
                        'filearea' => 'attachment',     
                        'itemid' => $newid,             
                        'filepath' => '/',           
                        'filename' => $captionname,
                        'license' => 'allrightsreserved',
                        'userid' => $user->id,
                        'author' => fullname($user)); 
                    print_object($fileinfo);
                    $fs->create_file_from_string($fileinfo, $content);
//                }
            }
        }
    }
}


//redirect($CFG->wwwroot . '/chamktu/manage/course_list.php?coursetype=1');

?>