<?php


require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');


$id = optional_param('id', 0, PARAM_INT);                   
$subscribe = optional_param('subscribe', null, PARAM_INT);  

$url = new moodle_url('/mod/jinotechboard/index.php', array('id'=>$id));
if ($subscribe !== null) {
    require_sesskey();
    $url->param('subscribe', $subscribe);
}
$PAGE->set_url($url);

if ($id) {
    if (! $course = $DB->get_record('course', array('id' => $id))) {
        print_error('invalidcourseid');
    }
} else {
    $course = get_site();
}

require_course_login($course);
$PAGE->set_pagelayout('incourse');
$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);


unset($SESSION->fromdiscussion);

add_to_log($course->id, 'jinotechboard', 'view jinotechboards', "index.php?id=$course->id");





$strjinotechboards       = get_string('jinotechboards', 'jinotechboard');
$strjinotechboard        = get_string('jinotechboard', 'jinotechboard');
$strdescription  = get_string('description');
$strdiscussions  = get_string('discussions', 'jinotechboard');
$strsubscribed   = get_string('subscribed', 'jinotechboard');
$strunreadposts  = get_string('unreadposts', 'jinotechboard');
$strtracking     = get_string('tracking', 'jinotechboard');
$strmarkallread  = get_string('markallread', 'jinotechboard');
$strtrackjinotechboard   = get_string('trackjinotechboard', 'jinotechboard');
$strnotrackjinotechboard = get_string('notrackjinotechboard', 'jinotechboard');
$strsubscribe    = get_string('subscribe', 'jinotechboard');
$strunsubscribe  = get_string('unsubscribe', 'jinotechboard');
$stryes          = get_string('yes');
$strno           = get_string('no');







$generaltable = new html_table();
$generaltable->head  = array ($strjinotechboard, $strdescription, $strdiscussions);
$generaltable->align = array ('left', 'left', 'center');

//$usetracking = jinotechboard_tp_can_track_jinotechboards();
//
//if ($usetracking) {
//    $untracked = jinotechboard_tp_get_untracked_jinotechboards($USER->id, $course->id);
//
//    $generaltable->head[] = $strunreadposts;
//    $generaltable->align[] = 'center';
//
//    $generaltable->head[] = $strtracking;
//    $generaltable->align[] = 'center';
//}
//
//
//$subscribed_jinotechboards = jinotechboard_get_subscribed_jinotechboards($course);
//
//$can_subscribe = is_enrolled($coursecontext);
//if ($can_subscribe) {
//    $generaltable->head[] = $strsubscribed;
//    $generaltable->align[] = 'center';
//}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();


$jinotechboards = $DB->get_records('jinotechboard', array('course' => $course->id));

$generaljinotechboards  = array();

$modinfo = get_fast_modinfo($course);

if (!isset($modinfo->instances['jinotechboard'])) {
    $modinfo->instances['jinotechboard'] = array();
}

foreach ($modinfo->instances['jinotechboard'] as $jinotechboardid=>$cm) {
    if (!$cm->uservisible or !isset($jinotechboards[$jinotechboardid])) {
        continue;
    }

    $jinotechboard = $jinotechboards[$jinotechboardid];

    if (!$context = context_module::instance($cm->id)) {
        continue;   
    }

    if (!has_capability('mod/jinotechboard:viewcontent', $context)) {
        continue;
    }


    $generaljinotechboards[$jinotechboard->id] = $jinotechboard;
}



if (!is_null($subscribe) and !isguestuser()) {
    foreach ($modinfo->instances['jinotechboard'] as $jinotechboardid=>$cm) {
        $jinotechboard = $jinotechboards[$jinotechboardid];
        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
        $cansub = false;

        if (has_capability('mod/jinotechboard:viewcontent', $modcontext)) {
            $cansub = true;
        }
        if ($cansub && $cm->visible == 0 &&
            !has_capability('mod/jinotechboard:managesubscriptions', $modcontext))
        {
            $cansub = false;
        }
        if (!jinotechboard_is_forcesubscribed($jinotechboard)) {
            $subscribed = jinotechboard_is_subscribed($USER->id, $jinotechboard);
            if ((has_capability('moodle/course:manageactivities', $coursecontext, $USER->id) || $jinotechboard->forcesubscribe != FORUM_DISALLOWSUBSCRIBE) && $subscribe && !$subscribed && $cansub) {
                jinotechboard_subscribe($USER->id, $jinotechboardid);
            } else if (!$subscribe && $subscribed) {
                jinotechboard_unsubscribe($USER->id, $jinotechboardid);
            }
        }
    }
    $returnto = jinotechboard_go_back_to("index.php?id=$course->id");
    $shortname = format_string($course->shortname, true, array('context' => get_context_instance(CONTEXT_COURSE, $course->id)));
    if ($subscribe) {
        add_to_log($course->id, 'jinotechboard', 'subscribeall', "index.php?id=$course->id", $course->id);
        redirect($returnto, get_string('nowallsubscribed', 'forum', $shortname), 1);
    } else {
        add_to_log($course->id, 'jinotechboard', 'unsubscribeall', "index.php?id=$course->id", $course->id);
        redirect($returnto, get_string('nowallunsubscribed', 'forum', $shortname), 1);
    }
}





if ($generaljinotechboards) {
    foreach ($generaljinotechboards as $jinotechboard) {
        $cm      = $modinfo->instances['jinotechboard'][$jinotechboard->id];
        $context = context_module::instance($cm->id);



        $count = jinotechboard_count_discussions($jinotechboard, $cm, $course);

//        if ($usetracking) {
            if ($jinotechboard->trackingtype == JINOTECHBOARD_TRACKING_OFF) {
                $unreadlink  = '-';
                $trackedlink = '-';

            } else {
                if (isset($untracked[$jinotechboard->id])) {
                        $unreadlink  = '-';
                } else if ($unread = jinotechboard_tp_count_jinotechboard_unread_posts($cm, $course)) {
                        $unreadlink = '<span class="unread"><a href="view.php?b='.$jinotechboard->id.'">'.$unread.'</a>';
                    $unreadlink .= '<a title="'.$strmarkallread.'" href="markposts.php?f='.
                                   $jinotechboard->id.'&amp;mark=read"><img src="'.$OUTPUT->pix_url('t/clear') . '" alt="'.$strmarkallread.'" /></a></span>';
                } else {
                    $unreadlink = '<span class="read">0</span>';
                }

                if ($jinotechboard->trackingtype == JINOTECHBOARD_TRACKING_ON) {
                    $trackedlink = $stryes;

                } else {
                    $aurl = new moodle_url('/mod/jinotechboard/settracking.php', array('id'=>$jinotechboard->id));
                    if (!isset($untracked[$jinotechboard->id])) {
                        $trackedlink = $OUTPUT->single_button($aurl, $stryes, 'post', array('title'=>$strnotrackjinotechboard));
                    } else {
                        $trackedlink = $OUTPUT->single_button($aurl, $strno, 'post', array('title'=>$strtrackjinotechboard));
                    }
                }
            }
//        }
        
        
        $jinotechboard->intro = shorten_text(format_module_intro('jinotechboard', $jinotechboard, $cm->id), $CFG->forum_shortpost);
        $jinotechboardname = format_string($jinotechboard->name, true);;

        
        
        
        if ($cm->visible) {
            $style = '';
        } else {
            $style = 'class="dimmed"';
        }
        $jinotechboardlink = "<a href=\"view.php?b=$jinotechboard->id\" $style>".format_string($jinotechboard->name,true)."</a>";
        $discussionlink = "<a href=\"view.php?b=$jinotechboard->id\" $style>".$count."</a>";

        $row = array ($jinotechboardlink, $jinotechboard->intro, $discussionlink);
//        if ($usetracking) {
//            $row[] = $unreadlink;
//            $row[] = $trackedlink; 
//        }

//        if ($can_subscribe) {
//            if ($jinotechboard->forcesubscribe != JINOTECHBOARD_DISALLOWSUBSCRIBE) {
//                $row[] = jinotechboard_get_subscribe_link($jinotechboard, $context, array('subscribed' => $stryes,
//                        'unsubscribed' => $strno, 'forcesubscribed' => $stryes,
//                        'cantsubscribe' => '-'), false, false, true, $subscribed_jinotechboards);
//            } else {
//                $row[] = '-';
//            }
//        }

        $generaltable->data[] = $row;
    }
}





$PAGE->navbar->add($strjinotechboards);
$PAGE->set_title("$course->shortname: $strjinotechboards");
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();


//if (!isguestuser() && isloggedin()) {
//    echo $OUTPUT->box_start('subscription');
//    echo html_writer::tag('div',
//        html_writer::link(new moodle_url('/mod/jinotechboard/index.php', array('id'=>$course->id, 'subscribe'=>1, 'sesskey'=>sesskey())),
//            get_string('allsubscribe', 'jinotechboard')),
//        array('class'=>'helplink'));
//    echo html_writer::tag('div',
//        html_writer::link(new moodle_url('/mod/jinotechboard/index.php', array('id'=>$course->id, 'subscribe'=>0, 'sesskey'=>sesskey())),
//            get_string('allunsubscribe', 'jinotechboard')),
//        array('class'=>'helplink'));
//    echo $OUTPUT->box_end();
//    echo $OUTPUT->box('&nbsp;', 'clearer');
//}

if ($generaljinotechboards) {
    echo $OUTPUT->heading(get_string('jinotechboards', 'jinotechboard'));
    echo html_writer::table($generaltable);
}


echo $OUTPUT->footer();

