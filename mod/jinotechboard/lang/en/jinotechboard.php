<?php

$string['pluginname'] = 'Forum(General)';
$string['modulename'] = 'Forum(General)';
$string['modulename_help'] = 'The board activity module enables participants to have asynchronous discussions i.e. discussions that take place over an extended period of time.';
$string['modulenameplural'] = 'Forum(General)';
$string['boardname'] = 'Forum name';
$string['boardtype'] = 'Forum type';
$string['boardintro'] = 'Forum introduction';
$string['maxattachments'] = 'Maximum number of attachments';
$string['maxattachments_help'] = 'This setting specifies the maximum number of files that can be attached to a board post.';
$string['maxattachmentsize'] = 'Maximum attachment size';
$string['massfile'] = 'Mass file attachment';
$string['massfilemaxsize'] = 'Mass file size';
$string['normalfilemaxsize'] = 'Normal file size';
$string['maxattachmentsize_help'] = 'This setting specifies the largest size of file that can be attached to a board post. Administrator can control the attachable file size. ';

$string['jinotechboard:notice'] = 'Course Notice';
$string['jinotechboard:qna'] = 'Questions and answers';
$string['jinotechboard:courseqna'] = 'Course Q&A';
$string['jinotechboard:reference'] = 'Reference Room';

$string['boardtype:notice'] = 'Notice board';
$string['boardtype:qna'] = 'Q&A';
$string['boardtype:general'] = 'Free board';
$string['boardtype:forum'] = 'Forum';
$string['boardtype:team'] = 'Team board';

$string['invalidaccess'] = 'This page was not accessed correctly';
$string['invalidcontentsid'] = 'ID was incorrect or no longer exists';
$string['invalidboardid'] = 'Forum ID was incorrect';
$string['invalidparentpostid'] = 'Parent post ID was incorrect';
$string['invalidpostid'] = 'Invalid post ID - {$a}';

$string['pluginadministration'] = 'Forum(General) administration';

$string['addanewdiscussion'] = 'Add a new discussion topic';
$string['addanewquestion'] = 'Add a new question';
$string['addanewtopic'] = 'Add a new topic';

$string['cannotadddiscussion'] = 'Adding discussions to this board requires group membership.';
$string['cannotadddiscussionall'] = 'You do not have permission to add a new discussion topic for all participants.';

$string['noquestions'] = 'There are no questions yet in this board';
$string['nonews'] = 'No news has been posted yet';
$string['nodiscussions'] = 'There are no discussion topics yet in this board';

$string['timecreated'] = 'Timecreated';

$string['discussion'] = 'Discussion';
$string['startedby'] = 'Started by';

$string['viewCount'] = 'Views';
$string['view:cnt'] = 'Views';
$string['isIncludeFile'] = 'File(s)';

$string['title'] = 'Title';
$string['writer'] = 'Writer';
$string['content'] = 'Content';
$string['search'] = 'Search';

$string['newpost'] = 'Post';
$string['attachment'] = 'attachment';
$string['reply'] = 'reply';

$string['header_etc'] = 'Etc settings';
$string['board_secret'] = 'Secret';
$string['content:secret'] = '**** Content created by the private. ****';
$string['board_category'] = 'Category';
$string['board_comment'] = 'Comment';
$string['board_reply'] = 'Reply';
$string['board_notice'] = 'Notice';
$string['board_recommend'] = 'Recommend';
$string['board_thread'] = 'Thread type';
$string['board_general'] = 'Forum type';

$string['bynameondate'] = 'by {$a->name} - {$a->date}';

$string['no_delete_permission'] = 'No delete_permission';
$string['Deletionshavebeencompleted'] = 'Post is Deleted';
$string['backtoboard'] = 'backtoboard';
$string['isprivate'] = 'Private';

$string['next:content'] = '+View more';
$string['nextpage'] = 'Next';
$string['prevpage'] = 'Previous';
$string['cancel'] = 'Cancel';

$string['edit'] = 'Edit';
$string['reply:cnt'] = 'reply';
$string['delete'] = 'Delete';
$string['enterstr_text'] = 'Enter contents.';
$string['suredeleteselectedcontents'] = 'Are you sure you want to delete selected contents?';
$string['surerecommendcontents'] = 'Are you sure you want to recommend selected contents?';

$string['file'] = 'File(s)';
$string['save'] = 'Save';
$string['name'] = 'Name';
$string['nocontent'] = 'There are no posts';
$string['date'] = 'Date';
$string['no.'] = 'No.';
$string['order'] = 'Order';
$string['replies'] = 'Replies';
$string['recommend'] = "Recommend";
$string['reCommondCnt'] = 'Recommend';
$string['recommended'] = "Recommended";
$string['action'] = 'Action';
$string['count'] = 'Count';

$string['cannotviewpostlist'] = 'You can not view this board';
$string['cannotviewpost'] = 'You can not view this post';
$string['cannoteditpost'] = 'You can not update this post';
$string['cannotreplypost'] = 'You can not reply this post';
$string['cannotaddpost'] = 'You can not post';
$string['cannotdeletepost'] = 'You can not delete this post';
$string['cannotmanagecategory'] = 'You can not manage category';


$string['cannotaddcomment'] = 'You can not add a comment';

$string['isNotice'] = 'Notice';
$string['yesNotice'] = 'Front Notice';
$string['noNotice'] = 'This post is not notice';
$string['emptynotice'] = 'There are no notices yet in this course';

$string['suredeleteselectedcategory'] = 'Are you sure you want to delete selected category?';


$string['jinotechboards'] = 'Jinotechboards';
$string['jinotechboard'] = 'Jinotechboard';
$string['discussions'] = 'Contents';
$string['subscribed'] = 'Subscribed';
$string['unreadposts'] = 'Unread posts';
$string['tracking'] = 'Track';
$string['markallread'] = 'Mark all posts in this board read.';
$string['trackjinotechboard'] = 'trackjinotechboard';
$string['notrackjinotechboard'] = 'Track unread contents';
$string['subscribe'] = 'Subscribe to this board';
$string['unsubscribe'] = 'Unsubscribe from this board';
$string['yes'] = 'yes';
$string['no'] = 'no';

$string['allsubscribe'] = 'Subscribe to all boards';
$string['allunsubscribe'] = 'Unsubscribe from all boards';

$string['learningjinotechboards'] = 'Forum(General)';


// ==========================================
// 게시판 권한 설정
// ==========================================
$string['jinotechboard:categorymanager'] = 'Category management';
$string['jinotechboard:noticemanager'] = 'Notice management';
$string['jinotechboard:secretmanager'] = 'Secret management';
$string['jinotechboard:viewsubscribers'] = 'View subscribers';

$string['jinotechboard:commentdelete'] = 'Comment delete';
$string['jinotechboard:commentview'] = 'Comment view';
$string['jinotechboard:commentwrite'] = 'Comment write';

$string['jinotechboard:delete'] = 'Contents delete';
$string['jinotechboard:edit'] = 'Contents edit';
$string['jinotechboard:reply'] = 'Reply post';
$string['jinotechboard:list'] = 'Contents list';
$string['jinotechboard:viewcontent'] = 'Contents view';
$string['jinotechboard:write'] = 'Add post';

$string['jinotechboard:managesubscriptions'] = 'Subscription Management';

$string['jinotechboard:modify'] = 'Edit post';
$string['jinotechboard:view'] = 'View post';
$string['jinotechboard:manage'] = 'Post Management';
$string['jinotechboard:comment'] = 'Use Comments';

$string['prohibitdistribution'] = 'This electronic teaching material is protected by copyright law and is only permitted for Gtec University students\' learning purposes. Unauthorized distribution, modification is prohibited. If you accept the condition above, download will start immediately. ';
$string['options'] = 'Check Public';
$string['normar'] = 'Public';
$string['next_content'] = 'Next Content';
$string['prev_content'] = 'Prev Content';
$string['current_content'] = 'Current Content';

$string['deletedfile'] = 'The attached file was deleted in server because of its big size after (end of) the semester when it was uploaded.';
$string['issecret'] = 'Private writings';
$string['maxlength'] = 'Comments The maximum number of characters is 300 characters.';

$string['reader'] = 'Reader';
$string['readdate'] = 'Read Time';
$string['noreadstudent'] = 'Empty Read Users';

$string['content_copy'] = 'Copy the post';

$string['send_type'] = 'Send Type';
$string['sms_sender'] = 'SMS Sender';
$string['sms_content'] = 'SMS Contents';
