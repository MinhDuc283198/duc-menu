<?php

$string['pluginname'] = 'Diễn đàn';
$string['modulename'] = 'Diễn đàn';
$string['modulename_help'] = '
Mô-đun hoạt động của hội đồng quản trị cho phép người tham gia có các cuộc thảo luận không đồng bộ, tức là các cuộc thảo luận diễn ra trong một khoảng thời gian dài.';
$string['modulenameplural'] = 'Diễn đàn';
$string['boardname'] = 'Tên diễn đàn';
$string['boardtype'] = 'Loại diễn đàn';
$string['boardintro'] = 'Giới thiệu diễn đàn';
$string['maxattachments'] = 'Số tệp đính kèm tối đa';
$string['maxattachments_help'] = 'Những thiết lặp về dung lượng tệp này có thể được áp dụng khi đính kèm tệp trong bản tin.';
$string['maxattachmentsize'] = 'Dung lượng tệp đính kèm tối đa';
$string['massfile'] = 'Đính kèm tệp';
$string['massfilemaxsize'] = 'Kich thước tệp đinh kèm';
$string['normalfilemaxsize'] = 'Tệp đính kèm bình thường';
$string['maxattachmentsize_help'] = 'Những thiết lặp về dung lượng tệp này có thể được áp dụng khi đính kèm tệp trong bản tin. Quản trị viên có thể quản lý kích thước tệp đính kèm.';

$string['jinotechboard:notice'] = 'Thông báo khóa học';
$string['jinotechboard:qna'] = 'Hỏi và đáp';
$string['jinotechboard:courseqna'] = 'Q&A Khóa học';
$string['jinotechboard:reference'] = 'Phòng tham khảo';

$string['boardtype:notice'] = 'Thông báo';
$string['boardtype:qna'] = 'Q&A';
$string['boardtype:general'] = 'Bản bình thường';
$string['boardtype:forum'] = 'Diễn đàn';
$string['boardtype:team'] = 'Bản nhóm';

$string['invalidaccess'] = 'Trang bị hạn chế quyền truy cập';
$string['invalidcontentsid'] = 'ID không đúng hoặc không tồn tại';
$string['invalidboardid'] = 'ID diễn đàn không đúng';
$string['invalidparentpostid'] = 'ID bản tin nguồn không đúng';
$string['invalidpostid'] = 'ID bản tin không đúng - {$a}_vi';

$string['pluginadministration'] = 'Quản trị diễn đàn';

$string['addanewdiscussion'] = 'Thêm chủ đề thảo luận';
$string['addanewquestion'] = 'Thêm câu hỏi';
$string['addanewtopic'] = 'Thêm chủ đề';

$string['cannotadddiscussion'] = 'Chỉ thành viên mới được thêm chủ đề thảo luận';
$string['cannotadddiscussionall'] = 'Bạn không đủ quyền thêm chủ đề thảo luận cho tất cả thành viên';

$string['noquestions'] = 'Không có câu hỏi nào';
$string['nonews'] = 'Không có bản tin nào được đăng';
$string['nodiscussions'] = 'Không có chủ đề thảo luận nào';

$string['timecreated'] = 'Thời gian tạo';

$string['discussion'] = 'Thảo luận';
$string['startedby'] = 'Bắt đầu bởi';

$string['viewCount'] = 'Lượt xem';
$string['view:cnt'] = 'Lượt xem';
$string['isIncludeFile'] = 'Tệp';

$string['title'] = 'Tiêu đề';
$string['writer'] = 'Tác giả';
$string['content'] = 'Nội dung';
$string['search'] = 'Tìm kiếm';

$string['newpost'] = 'Bản tin';
$string['attachment'] = 'Đính kèm';
$string['reply'] = 'Trả lời';

$string['header_etc'] = 'Cài đặt';
$string['board_secret'] = 'Bảo mật';
$string['content:secret'] = '**** Nội dung riêng tư ****';
$string['board_category'] = 'Danh mục';
$string['board_comment'] = 'Bình luận';
$string['board_reply'] = 'Trả lời';
$string['board_notice'] = 'Thông báo';
$string['board_recommend'] = 'Đề xuất';
$string['board_thread'] = 'Loại luồng';
$string['board_general'] = 'Loại diễn đàn';

$string['bynameondate'] = 'bởi {$a->name} - {$a->date}_vi';

$string['no_delete_permission'] = 'Không xóa quyền';
$string['Deletionshavebeencompleted'] = 'Bản tin đã xóa';
$string['backtoboard'] = 'Trở lại bản tin';
$string['isprivate'] = 'Riêng tư';

$string['next:content'] = '+ Xem thêm';
$string['nextpage'] = 'Tới';
$string['prevpage'] = 'Lùi';
$string['cancel'] = 'Hủy';

$string['edit'] = 'Sửa';
$string['reply:cnt'] = 'Trả lời';
$string['delete'] = 'Xóa';
$string['enterstr_text'] = 'Nhập khóa học';
$string['suredeleteselectedcontents'] = 'Bạn chắc chắn xóa nội dung được chọn?';
$string['surerecommendcontents'] = 'Bạn có chắc muốn đề xuất nội dung được chọn?';

$string['file'] = 'Tệp';
$string['save'] = 'Lưu';
$string['name'] = 'Tên';
$string['nocontent'] = 'Không có bản tin';
$string['date'] = 'Ngày';
$string['no.'] = 'No.';
$string['order'] = 'Sắp xếp';
$string['replies'] = 'Trả lời nhiều';
$string['recommend'] = "Đề xuất";
$string['reCommondCnt'] = 'Đề xuất';
$string['recommended'] = "Đề xuất";
$string['action'] = 'Hoạt động';
$string['count'] = 'Tổng';

$string['cannotviewpostlist'] = 'Bạn không thể xem bản tin này';
$string['cannotviewpost'] = 'Bạn không thể xem bản tin này';
$string['cannoteditpost'] = 'Bạn không thể cập nhật bản tin này';
$string['cannotreplypost'] = 'Bạn không thể trả lời bản tin này';
$string['cannotaddpost'] = 'Bạn không thể đăng bản tin';
$string['cannotdeletepost'] = 'Bạn không thể xóa bản tin này';
$string['cannotmanagecategory'] = 'Bạn không thể quản lý danh mục';


$string['cannotaddcomment'] = 'Bạn không thể thêm bình luận';

$string['isNotice'] = 'Thông báo';
$string['yesNotice'] = 'Đây là thông báo';
$string['noNotice'] = 'Đây không phải là thông báo';
$string['emptynotice'] = 'Không có thông báo nào trong khóa học này';

$string['suredeleteselectedcategory'] = 'Bạn có chắc chắn xóa danh mục đã chọn';


$string['jinotechboards'] = 'Jinotechboards';
$string['jinotechboard'] = 'Jinotechboard';
$string['discussions'] = 'Nội dung';
$string['subscribed'] = 'Đăng ký';
$string['unreadposts'] = 'Bản tin chưa đọc';
$string['tracking'] = 'Theo dõi';
$string['markallread'] = 'Đánh dấu tất cả các bản tin trong khóa học';
$string['trackjinotechboard'] = 'Theo dõi Jinotechboard';
$string['notrackjinotechboard'] = 'Theo dõi nội dung chưa đọc';
$string['subscribe'] = 'Đăng ký khóa học này';
$string['unsubscribe'] = 'Bỏ theo dõi bản tin này';
$string['yes'] = 'Có';
$string['no'] = 'Không';

$string['allsubscribe'] = 'Theo dõi tất cả bản tin';
$string['allunsubscribe'] = 'Bảo theo dỗi tất cả';

$string['learningjinotechboards'] = 'Diễn đàn';


// ==========================================
// 게시판 권한 설정
// ==========================================
$string['jinotechboard:categorymanager'] = 'Quản lý danh mục';
$string['jinotechboard:noticemanager'] = 'Quản lý thông báo';
$string['jinotechboard:secretmanager'] = 'Quản lý bảo mật';
$string['jinotechboard:viewsubscribers'] = 'Xem thành viên đăng ký';

$string['jinotechboard:commentdelete'] = 'Xóa bình luận';
$string['jinotechboard:commentview'] = 'Xem bình luận';
$string['jinotechboard:commentwrite'] = 'Viết bình luận';

$string['jinotechboard:delete'] = 'Xóa nội dung';
$string['jinotechboard:edit'] = 'Sửa nội dung';
$string['jinotechboard:reply'] = 'Trả lời bản tin';
$string['jinotechboard:list'] = 'Danh sách nội dung';
$string['jinotechboard:viewcontent'] = 'Xem nội dung';
$string['jinotechboard:write'] = 'Thêm bản tin';

$string['jinotechboard:managesubscriptions'] = 'Quản lý đăng ký';

$string['jinotechboard:modify'] = 'Sửa bản tin';
$string['jinotechboard:view'] = 'Xem bản tin';
$string['jinotechboard:manage'] = 'Quản lý bản tin';
$string['jinotechboard:comment'] = 'Bình luận';

$string['prohibitdistribution'] = 'Việc giảng dạy điện tử được bảo vệ bởi luật bản quyền, và chỉ áp dụng cho sinh viên Gtec University. Nếu bạn đồng ý điều khoản sử dụng, có thể tải về và sử dụng ngay';
$string['options'] = 'Kiểm tra công khai';
$string['normar'] = 'Công khai';
$string['next_content'] = 'Nội dung sau';
$string['prev_content'] = 'Nội dung trước';
$string['current_content'] = 'Nội dung hiện tại';

$string['deletedfile'] = 'Tệp đính kèm đã bị xóa vì dung lượng lớn.';
$string['issecret'] = 'Bài viết riêng tư';
$string['maxlength'] = 'Nội dung không quá 300 ký tự';

$string['reader'] = 'Người xem';
$string['readdate'] = 'Thời gian xem';
$string['noreadstudent'] = 'Không có người xem';

$string['content_copy'] = 'Sao chép bản tin';

$string['send_type'] = 'Phương thức gửi';
$string['sms_sender'] = 'SMS Sender';
$string['sms_content'] = 'Nội dung SMS';
