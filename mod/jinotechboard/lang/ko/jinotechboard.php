<?php

$string['pluginname'] = '게시판(일반)';
$string['modulename'] = '게시판(일반)';
$string['modulename_help'] = '<p><b>게시판(일반)</b></p>
<div class="indent">
<p>공지형, 질문 답변형, 일반형과 같이 다양한 용도로 사용하는 활동입니다.</p>
<p>공지형의 경우에는 게시글을 공지 형식으로 상단에 노출하며 공지 종료일을 설정할 수 있습니다.</p>
<p>질문 답변형의 경우에는 공지형과는 달리 답글 달기가 가능하며 비밀글도 설정할 수있습니다.</p>
<p>일반형의 경우에는 답글과 댓글이 모두 가능하며 게시글에 대한 추천을 할 수 있습니다.</p>
<p>게시판의 글들은 북마킹이 가능하며 이때 북마킹된 내용은 강의 게시판의 나의 게시판에 모아 볼 수 있습니다.</p> 
<p>게시판의 내용들은 게시글, 답글, 추천 등애 대한 통계 분석이 자동으로 제공됩니다.</p>
</div>';
$string['modulenameplural'] = '게시판(일반)';
$string['boardname'] = '게시판 이름';
$string['boardtype'] = '게시판 종류';
$string['boardintro'] = '게시판 소개';
$string['action'] = '활동';
$string['count'] = '카운트';
$string['bynameondate'] = '{$a->date} 에 {$a->name} 씀 ';

$string['maxattachments'] = '최대 첨부물 수';
$string['maxattachments_help'] = '<p>게시판에서 각 게시글에 얼마나 많는 첨부물을 부가할 수 있는지를 설정할 수 있게 한다.</p>';
$string['maxattachmentsize'] = '최대 첨부 용량';
$string['massfile'] = '대용량 첨부 파일 사용';
$string['massfilemaxsize'] = '대용량 첨부 파일 최대 크기';
$string['normalfilemaxsize'] = '일반 첨부 파일 최대 크기';
$string['maxattachmentsize_help'] = '<p>이 세팅은 게시판 글에 첨부할 수 있는 파일의 최대 용량을 설정합니다. 첨부할 수 있는 파일의 크기는 게시판을 개설한 관리자가 제한할 수 있습니다.</p>';

$string['jinotechboard:notice'] = '강의 공지사항';
$string['jinotechboard:qna'] = '질문과 답변';
$string['jinotechboard:courseqna'] = '과정 Q&A';
$string['jinotechboard:reference'] = '자료실';

$string['boardtype:notice'] = '공지사항';
$string['boardtype:qna'] = '질문답변';
$string['boardtype:general'] = '게시판(일반)';
$string['boardtype:forum'] = '게시판(토론)';
$string['boardtype:team'] = '게시판(모둠)';

$string['invalidaccess'] = '이  페이지는 올바르게 접속되지 않았습니다.';
$string['invalidcontentsid'] = '게시물 ID가 틀렸거나 더 이상 존재하지 않습니다.';
$string['invalidboardid'] = '게시물이 삭제되었거나, 잘못된 ID 입니다.';
$string['invalidparentpostid'] = '상위 게시판 ID가 바르지 않음';
$string['invalidpostid'] = '잘못된 게시물 ID - {$a}';

$string['pluginadministration'] = '게시판(일반) 관리';

$string['addanewdiscussion'] = '글쓰기';
$string['addanewquestion'] = '글쓰기';
$string['addanewtopic'] = '글쓰기';

$string['cannotadddiscussion'] = '게시판(일반)에 글을 제시하려면 프로젝트팀의 구성원이어야 합니다.';
$string['cannotadddiscussionall'] = '공동 게시 주제를 추가할 권한이 없습니다.';

$string['noquestions'] = '이 게시판에 아직 질문이 없습니다.';
$string['nonews'] = '이 게시판에 아직 작성된 공지사항이 없습니다.';
$string['nodiscussions'] = '이 게시판에 아직 토론 주제가 없습니다.';

$string['timecreated'] = '작성일';
$string['options'] = '공개여부';
$string['discussion'] = '제목';
$string['startedby'] = '작성자';

$string['viewCount'] = '조회';
$string['view:cnt'] = '조회수';
$string['isIncludeFile'] = '파일';

$string['title'] = '제목';
$string['writer'] = '작성자';
$string['content'] = '내용';
$string['search'] = '검색';

$string['newpost'] = '글쓰기';
$string['attachment'] = '첨부';
$string['reply'] = '댓글';

$string['header_etc'] = '기타 설정';
$string['board_secret'] = '비밀글 기능사용';
$string['content:secret'] = '**** 비공개로 작성된 글입니다. ****';
$string['board_category'] = '카테고리';
$string['board_comment'] = '덧글';
$string['board_reply'] = '답글';
$string['board_notice'] = '공지 기능사용';
$string['board_recommend'] = '추천 기능사용';
$string['board_thread'] = '스레드 형식';
$string['board_general'] = '게시판 형식';
$string['no_delete_permission'] = '삭제 권한이 없습니다.';
$string['Deletionshavebeencompleted'] = '삭제되었습니다.';
$string['backtoboard'] = '돌아가기';
$string['isprivate'] = '비공개';
$string['normar'] = '공개';
$string['next:content'] = '+더보기';
$string['nextpage'] = '다음';
$string['prevpage'] = '이전';
$string['cancel'] = '취소';

$string['edit'] = '수정';
$string['reply:cnt'] = '댓글수';
$string['delete'] = '삭제';
$string['enterstr_text'] = '내용을 입력하세요.';
$string['suredeleteselectedcontents'] = '선택한 글을 삭제하겠습니까?';
$string['surerecommendcontents'] = '선택한 글을 추천하겠습니까?';


$string['file'] = '첨부파일';
$string['save'] = '저장';
$string['name'] = '이름';
$string['nocontent'] = '게시글이 없습니다.';
$string['date'] = '날짜';
$string['no.'] = '번호';
$string['order'] = '순서';
$string['reCommondCnt'] = '추천';
$string['recommend'] = "추천";
$string['recommended'] = "추천됨";
$string['replies'] = '답글';

$string['cannotviewpostlist'] = '게시판 보기 권한이 없습니다. ';
$string['cannotviewpost'] = '게시글 보기 권한이 없습니다.  ';
$string['cannoteditpost'] = '게시글 수정 권한이 없습니다. ';
$string['cannotreplypost'] = '게시글 답글 달기 권한이 없습니다. ';
$string['cannotaddpost'] = '게시판 글쓰기 권한이 없습니다. ';
$string['cannotdeletepost'] = '게시글 삭제 권한이 없습니다. ';
$string['cannotmanagecategory'] = '카테고리 관리 권한이 없습니다.';


$string['cannotaddcomment'] = '덧글 쓰기 권한이 없습니다.';

$string['isNotice'] = '상단노출';
$string['yesNotice'] = '상단공지노출';
$string['noNotice'] = '공지글이 아닙니다.';
$string['emptynotice'] = '과목 공지사항이 없습니다.';

$string['suredeleteselectedcategory'] = '선택한 카테고리를 삭제하시겠습니까?';


$string['jinotechboards'] = '게시판(일반)';
$string['jinotechboard'] = '게시판(일반)';
$string['discussions'] = '내용';
$string['subscribed'] = '이 토론을 이메일로 받기';
$string['unreadposts'] = '읽지 않은 글';
$string['tracking'] = '추적';
$string['markallread'] = '토론의 모든 게시물을 \'읽었음\'으로 표시합니다.';
$string['trackjinotechboard'] = '게시판 추적하기';
$string['notrackjinotechboard'] = '읽지 않은 글 추적하기';
$string['subscribe'] = '이 게시판 구독하기';
$string['unsubscribe'] = '이 게시판 구독해지';
$string['yes'] = '네';
$string['no'] = '아니오';

$string['allsubscribe'] = '모든 게시판 구독';
$string['allunsubscribe'] = '모든 게시판 구독 해지';

$string['learningjinotechboards'] = '게시판(일반)';


// ==========================================
// 게시판(일반) 권한 설정
// ==========================================
$string['jinotechboard:categorymanager'] = '카테고리 관리';
$string['jinotechboard:noticemanager'] = '공지사항 관리';
$string['jinotechboard:secretmanager'] = '비밀글 관리';
$string['jinotechboard:viewsubscribers'] = '구독자 보기';

$string['jinotechboard:commentdelete'] = '덧글 삭제';
$string['jinotechboard:commentview'] = '덧글 보기';
$string['jinotechboard:commentwrite'] = '덧글 생성';

$string['jinotechboard:delete'] = '삭제';
$string['jinotechboard:edit'] = '수정';
$string['jinotechboard:reply'] = '댓글';
$string['jinotechboard:list'] = '목록';
$string['jinotechboard:viewcontent'] = '보기';
$string['jinotechboard:write'] = '쓰기';

$string['jinotechboard:managesubscriptions'] = '구독 관리';

$string['jinotechboard:modify'] = '게시물 수정';
$string['jinotechboard:view'] = '게시물 보기';
$string['jinotechboard:write'] = '게시물 추가';
$string['jinotechboard:reply'] = '게시물 답변';
$string['jinotechboard:manage'] = '게시물 관리';
$string['jinotechboard:comment'] = '덧글 기능';

$string['prohibitdistribution'] = '본 전자교재는 저작권법에 의해 DGIST 학생들의 학습 용도로만 사용할 수 있도록 허가되었습니다.\n학습 용도 이외의 목적으로 외부에 전송, 변형한다면 저작권 관련 법적 문제가 발생할 수 있으니 각별히 주의바랍니다.\n위 내용에 동의하시면 다운로드가 시작됩니다.';
$string['next_content'] = '다음글';
$string['prev_content'] = '이전글';
$string['current_content'] = '현재글';

$string['deletedfile'] = '대용량 첨부파일로서, 등록된 학기 종료 후 삭제되었습니다.';
$string['issecret'] = '비공개 게시물 입니다.';
$string['maxlength'] = '댓글 최대글자 수는 300자 입니다.';


$string['reader'] = '읽은이';
$string['readdate'] = '읽은시간';
$string['noreadstudent'] = '읽은이가 없습니다.';

$string['content_copy'] = '게시글 복사';

$string['send_type'] = '발송형식';
$string['sms_sender'] = 'SMS 발송자';
$string['sms_content'] = 'SMS 내용';
