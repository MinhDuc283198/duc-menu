<?php

class backup_jinotechboard_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $jinotechboard = new backup_nested_element('jinotechboard', array('id'), array(
            'type', 'name', 'intro', 'introformat', 'maxbytes',
            'maxattachments', 'allownotice', 'allowreply', 'allowcomment', 'allowupload',
            'allowsecret', 'allowpublic', 'allowcategory', 'allowrecommend', 'timemodified'));


        $jinotechboard_contents = new backup_nested_element('jinotechboard_contents');
        $jinotechboard_content = new backup_nested_element('jinotechboard_content', array('id'), array(
            'board', 'category', 'userid', 'ref', 'step', 'lev', 'isnotice', 'isprivate', 'title', 'contents', 'viewcnt', 'commentscount', 'recommendcnt', 'timecreated', 'timemodified'));

        $jinotechboard_comments = new backup_nested_element('jinotechboard_comments');
        $jinotechboard_comment = new backup_nested_element('jinotechboard_comment', array('id'), array(
            'board', 'jinotechboard_contents', 'userid', 'username', 'ref', 'step', 'lev', 'isprivate', 'comments', 'timecreated', 'timemodified'));

        // Build the tree
        $jinotechboard->add_child($jinotechboard_contents);
        $jinotechboard_contents->add_child($jinotechboard_content);
        $jinotechboard_content->add_child($jinotechboard_comments);
        $jinotechboard_comments->add_child($jinotechboard_comment);

        // Define sources
        $jinotechboard->set_source_table('jinotechboard', array('id' => backup::VAR_ACTIVITYID));

        if ($userinfo) {


            $jinotechboard_content->set_source_sql('
                SELECT *
                  FROM {jinotechboard_contents}
                 WHERE board = ? order by ref DESC, step asc', array(backup::VAR_PARENTID));



            $jinotechboard_comment->set_source_sql('
                SELECT *
                  FROM {jinotechboard_comments}
                 WHERE jinotechboard_contents = ?', array(backup::VAR_PARENTID));
        }

        // Define file annotations
        $jinotechboard_content->annotate_files('mod_jinotechboard', 'attachment', 'id');

        // Define id annotations
        $jinotechboard_content->annotate_ids('user', 'userid');
        $jinotechboard_comment->annotate_ids('user', 'userid');


        // Return the root element (jinotechboard), wrapped into standard activity structure
        return $this->prepare_activity_structure($jinotechboard);
    }

}

?>
