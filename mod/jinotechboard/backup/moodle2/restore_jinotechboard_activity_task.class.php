<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/jinotechboard/backup/moodle2/restore_jinotechboard_stepslib.php');

class restore_jinotechboard_activity_task extends restore_activity_task {
    
    protected function define_my_settings() {
        
    }
    
    protected function define_my_steps() {
        $this->add_step(new restore_jinotechboard_activity_structure_step('jinotechboard_structure', 'jinotechboard.xml'));
    }
    
    
    static public function define_decode_contents() {
        $contents = array();

        //$contents[] = new restore_decode_content('jinotechboard', array('intro'), 'jinotechboard');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();

        // List of jinotechboards in course
        $rules[] = new restore_decode_rule('JINOTECHBOARDINDEX', '/mod/jinotechboard/index.php?id=$1', 'course');
        // Forum by cm->id and jinotechboard->id
        $rules[] = new restore_decode_rule('JINOTECHBOARDVIEWBYID', '/mod/jinotechboard/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('JINOTECHBOARDVIEWBYB', '/mod/jinotechboard/view.php?b=$1', 'jinotechboard');
        // Link to jinotechboard discussion
        $rules[] = new restore_decode_rule('JINOTECHBOARDCONTENTVIEW', '/mod/jinotechboard/content.php?contentId=$1', 'jinotechboard_content');
        // Link to discussion with parent and with anchor posts
        
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * jinotechboard logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();

       /* $rules[] = new restore_log_rule('jinotechboard', 'add', 'view.php?id={course_module}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'update', 'view.php?id={course_module}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'view', 'view.php?id={course_module}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'view jinotechboard', 'view.php?id={course_module}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'mark read', 'view.php?f={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'start tracking', 'view.php?f={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'stop tracking', 'view.php?f={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'subscribe', 'view.php?f={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'unsubscribe', 'view.php?f={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'subscriber', 'subscribers.php?id={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'subscribers', 'subscribers.php?id={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'view subscribers', 'subscribers.php?id={jinotechboard}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'add discussion', 'discuss.php?d={jinotechboard_discussion}', '{jinotechboard_discussion}');
        $rules[] = new restore_log_rule('jinotechboard', 'view discussion', 'discuss.php?d={jinotechboard_discussion}', '{jinotechboard_discussion}');
        $rules[] = new restore_log_rule('jinotechboard', 'move discussion', 'discuss.php?d={jinotechboard_discussion}', '{jinotechboard_discussion}');
        $rules[] = new restore_log_rule('jinotechboard', 'delete discussi', 'view.php?id={course_module}', '{jinotechboard}',
                                        null, 'delete discussion');
        $rules[] = new restore_log_rule('jinotechboard', 'delete discussion', 'view.php?id={course_module}', '{jinotechboard}');
        $rules[] = new restore_log_rule('jinotechboard', 'add post', 'discuss.php?d={jinotechboard_discussion}&parent={jinotechboard_post}', '{jinotechboard_post}');
        $rules[] = new restore_log_rule('jinotechboard', 'update post', 'discuss.php?d={jinotechboard_discussion}&parent={jinotechboard_post}', '{jinotechboard_post}');
        $rules[] = new restore_log_rule('jinotechboard', 'prune post', 'discuss.php?d={jinotechboard_discussion}', '{jinotechboard_post}');
        $rules[] = new restore_log_rule('jinotechboard', 'delete post', 'discuss.php?d={jinotechboard_discussion}', '[post]');
*/
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();

    /*    $rules[] = new restore_log_rule('jinotechboard', 'view jinotechboards', 'index.php?id={course}', null);
        $rules[] = new restore_log_rule('jinotechboard', 'subscribeall', 'index.php?id={course}', '{course}');
        $rules[] = new restore_log_rule('jinotechboard', 'unsubscribeall', 'index.php?id={course}', '{course}');
        $rules[] = new restore_log_rule('jinotechboard', 'user report', 'user.php?course={course}&id={user}&mode=[mode]', '{user}');
        $rules[] = new restore_log_rule('jinotechboard', 'search', 'search.php?id={course}&search=[searchenc]', '[search]');*/

        return $rules;
    }
}

?>
