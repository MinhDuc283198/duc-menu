<?php

class restore_jinotechboard_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');
        
        $jinotechboard = new restore_path_element('jinotechboard', '/activity/jinotechboard');
        $paths[] = $jinotechboard;
        
        // Apply for 'assignment' subplugins optional paths at assignment level
        //$this->add_subplugin_structure('jinotechboard', $jinotechboard);
        
        
        if ($userinfo) {
            
          //   $paths[] = new restore_path_element('jinotechboard_category', '/activity/jinotechboard/jinotechboard_categories/jinotechboard_category');
            $paths[] = new restore_path_element('jinotechboard_contents', '/activity/jinotechboard/jinotechboard_contents/jinotechboard_content');
            $paths[] = new restore_path_element('jinotechboard_comment', '/activity/jinotechboard/jinotechboard_contents/jinotechboard_content/jinotechboard_comments/jinotechboard_comment');
            $paths[] = new restore_path_element('jinotechboard_rating', '/activity/jinotechboard/jinotechboard_contents/jinotechboard_content/ratings/rating');
         //   $paths[] = new restore_path_element('jinotechboard_subscription', '/activity/jinotechboard/subscriptions/subscription');
          //  $paths[] = new restore_path_element('jinotechboard_read', '/activity/jinotechboard/readposts/read');
          //  $paths[] = new restore_path_element('jinotechboard_track', '/activity/jinotechboard/trackedprefs/track');
            
            //TODO $paths[] = new restore_path_element('forum_rating', '/activity/forum/discussions/discussion/posts/post/ratings/rating');
            
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    protected function process_jinotechboard($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timemodified = $this->apply_date_offset($data->timemodified);


        $newitemid = $DB->insert_record('jinotechboard', $data);
        $this->apply_activity_instance($newitemid);
    }

    
    protected function process_jinotechboard_contents($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        
        $data->course = $this->get_courseid();
        $data->board = $this->get_new_parentid('jinotechboard');
        
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        
        $newitemid = $DB->insert_record('jinotechboard_contents', $data);
        $this->set_mapping('jinotechboard_contents', $oldid, $newitemid, true);
        
        if (!empty($data->ref)) {
            $newrefid = $this->get_mappingid('jinotechboard_contents', $data->ref);
            $DB->set_field('jinotechboard_contents', 'ref', $newrefid, array('id' => $newitemid));
        }
    }
    

    
    
    protected function process_jinotechboard_comment($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        
        $data->course = $this->get_courseid();
        $data->board = $this->get_new_parentid('jinotechboard');
        $data->jinotechboard_contents = $this->get_new_parentid('jinotechboard_contents');
        $data->userid = $this->get_mappingid('user', $data->userid);
        //TODO category 어떻게 가져오는가?
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        
        $newitemid = $DB->insert_record('jinotechboard_comments', $data);
        $this->set_mapping('jinotechboard_comment', $oldid, $newitemid);
    }
    

    protected function process_jinotechboard_rating($data) {
        global $DB;

        $data = (object)$data;

        // Cannot use ratings API, cause, it's missing the ability to specify times (modified/created)
        $data->contextid = $this->task->get_contextid();
        $data->itemid    = $this->get_new_parentid('jinotechboard_contents');
        if ($data->scaleid < 0) { // scale found, get mapping
            $data->scaleid = -($this->get_mappingid('scale', abs($data->scaleid)));
        }
        $data->rating = $data->value;
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // We need to check that component and ratingarea are both set here.
        if (empty($data->component)) {
            $data->component = 'mod_jinotechboard';
        }
        if (empty($data->ratingarea)) {
            $data->ratingarea = 'post';
        }

        $newitemid = $DB->insert_record('rating', $data);
    }


    protected function after_execute() {
        global $DB;

        
        $this->add_related_files('mod_jinotechboard', 'attachment', 'jinotechboard_contents');
    }
}

?>
