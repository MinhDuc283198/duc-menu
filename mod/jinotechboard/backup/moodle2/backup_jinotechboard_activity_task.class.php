<?php

require_once($CFG->dirroot . '/mod/jinotechboard/backup/moodle2/backup_jinotechboard_stepslib.php'); // Because it exists (must)
//require_once($CFG->dirroot . '/mod/jinotechboard/backup/moodle2/backup_jinotechboard_settingslib.php'); // Because it exists (optional)



class backup_jinotechboard_activity_task extends backup_activity_task {
    
    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        //
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        $this->add_step(new backup_jinotechboard_activity_structure_step('jinotechboard structure', 'jinotechboard.xml'));
    }
    
    /**
     * Code the transformations to perform in the activity in
     * order to get transportable (encoded) links
     */
    static public function encode_content_links($content) {
        global $CFG;

         $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of jinotechboards
        $search="/(".$base."\/mod\/jinotechboard\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@JINOTECHBOARDINDEX*$2@$', $content);

        // Link to jinotechboard view by moduleid
        $search="/(".$base."\/mod\/jinotechboard\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@JINOTECHBOARDVIEWBYID*$2@$', $content);

        // Link to jinotechboard view by jinotechboardid
        $search="/(".$base."\/mod\/jinotechboard\/view.php\?b\=)([0-9]+)/";
        $content= preg_replace($search, '$@JINOTECHBOARDVIEWBYB*$2@$', $content);

        // Link to jinotechboard discussion by discussionid
        $search="/(".$base."\/mod\/jinotechboard\/content.php\?contentId\=)([0-9]+)/";
        $content= preg_replace($search, '$@JINOTECHBOARDCONTENTVIEW*$2@$', $content);
        
        return $content;
    }
}
?>
