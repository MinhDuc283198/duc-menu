<?php


if (!defined('MOODLE_INTERNAL')) {
	die('Direct access to this script is forbidden.');   
}

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_jinotechboard_mod_form extends moodleform_mod {
	
	
	function definition() {

		global $CFG, $COURSE, $DB;
                $mform = &$this->_form;
		
		$mform->addElement('header', 'general', get_string('general', 'form'));
		

                $mform->addElement('text', 'name', get_string('boardname', 'jinotechboard'), array('size'=>'64'));
                if (!empty($CFG->formatstringstriptags)) {
                    $mform->setType('name', PARAM_TEXT);
                } else {
                     $mform->setType('name', PARAM_CLEANHTML);
                }
                $mform->addRule('name', null, 'required', null, 'client');
                $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
                
                //게시판 종류 선택
                $radioarray=array();
                $radioarray[] =& $mform->createElement('radio', 'type', '', get_string('boardtype:notice','mod_jinotechboard'), 1);
                $radioarray[] =& $mform->createElement('radio', 'type', '', get_string('boardtype:qna','mod_jinotechboard'), 2);
                $radioarray[] =& $mform->createElement('radio', 'type', '', get_string('boardtype:general','mod_jinotechboard'), 3);
                $radioarray[] =& $mform->createElement('radio', 'type', '', get_string('jinotechboard:reference','mod_jinotechboard'), 6);
                $mform->addGroup($radioarray, 'radioar', get_string('boardtype', 'mod_jinotechboard'), array(' '), false);
                $mform->setDefault('type', 3);
                $mform->addRule('radioar', null, 'required');
		
                $this->add_intro_editor(true, get_string('boardintro', 'mod_jinotechboard'));
              
                $mform->addElement('hidden', 'forcesubscribe', JINOTECHBOARD_CHOOSESUBSCRIBE);
                $mform->setType('forcesubscribe', PARAM_INT);

                $mform->addElement('hidden', 'trackingtype', JINOTECHBOARD_TRACKING_OFF);
                $mform->setType('trackingtype', PARAM_INT);
                
                $update = optional_param('update',0 , PARAM_INT);
                $checked = 0;
                
                if(!empty($update)) {
                    $sql = " select jb.maxbytes from {course_modules} cm
                             join {jinotechboard} jb on jb.id = cm.instance
                             where cm.id = :id ";
                    $param = array("id" => $update);
                    $max = $DB->get_record_sql($sql, $param);
                    
                }
                
                $mform->addElement('hidden','setmaxbyte', get_string('massfile','jinotechboard'));
                $mform->setDefault('setmaxbyte', 1);
                
                $mform->addElement('hidden', 'maxattachments', $CFG->board_maxattachments);
                $mform->setType('maxattachments', PARAM_INT);
		
		$this->standard_grading_coursemodule_elements();
                
		$this->standard_coursemodule_elements();
		
		$this->add_action_buttons();
	}
	
	
	function definition_after_data() {
		parent::definition_after_data();
	}

	function data_preprocessing(&$default_values) {
		parent::data_preprocessing($default_values);
	}
        
        function get_data(){
            $data = parent::get_data();
            if (!$data) {
                return false;
            }
            switch($data->type){
                case 1: //공지사항
                    $data->allownotice = 1;
                    $data->allowreply = 0;
                    $data->allowcomment = 0;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 0;
                    $data->allowgigan = 1;
                    break;
                case 2: //질문답변
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 0;
                    $data->allowsecret = 1;
                    $data->allowrecommend = 0;
                    $data->allowgigan = 0;
                    break;
                case 3: //일반게시판
                    $data->allownotice = 1;
                    $data->allowreply = 1;
                    $data->allowcomment = 1;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 1;
                    $data->allowgigan = 0;
                    break;
                case 4: //토론게시판
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 1;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 1;
                    $data->allowgigan = 0;
                    break;
                case 5: //조모임게시판
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 1;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 1;
                    $data->allowgigan = 0;
                    break;
                case 6: //자료실
                    $data->allownotice = 0;
                    $data->allowreply = 1;
                    $data->allowcomment = 0;
                    $data->allowsecret = 0;
                    $data->allowrecommend = 0;
                    $data->allowgigan = 0;
                    break;
                default:
                    break;
            }
            
            return $data;
            
        }
        
}
?>
