<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
 
$id = optional_param('id', 0, PARAM_INT);
$contentid = optional_param('contentid', 0, PARAM_INT);

$content = $DB->get_record('jinotechboard_contents',array('id'=>$contentid));
$board = $DB->get_record('jinotechboard', array('id'=>$content->board));
$course = $DB->get_record("course", array("id" => $board->course));
$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id);
$context = context_module::instance($cm->id);


$DB->delete_records('jinotechboard_comments',array('id'=>$id,'jinotechboard_contents'=>$contentid));
$DB->set_field('jinotechboard_contents', 'commentscount', ($content->commentscount-1), array('id'=>$content->id));


$params = array(
    'objectid' => $id,
    'context' => $context,
    'other' => array(
        'contentid' => $content->id,
        'jinotechboardid' => $content->board,
    )
);

$event = \mod_jinotechboard\event\comment_deleted::create($params);
$event->add_record_snapshot('jinotechboard_contents', $content);
$event->add_record_snapshot('jinotechboard_comments', $records);
$event->trigger();

redirect($CFG->wwwroot."/mod/jinotechboard/content.php?b=".$content->board."&contentId=".$content->id."&boardform=1");
