<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');


require_course_login($course, true, $cm);

$PAGE->set_pagelayout('popup');

$PAGE->requires->jquery();

$contentId = required_param('id', PARAM_INT);

$query = 'select u.*,jr.firstread from {jinotechboard_read} jr join {user} u on u.id = jr.userid '
        . 'where jr.contentsid = :id';
$params = array('id' => $contentId);
$read_users = $DB->get_records_sql($query, $params);
$num = 1;
echo $OUTPUT->header();
?>
<table class="generaltable">
    <thead>
        <tr>
            <th><?php echo get_string("no.", 'jinotechboard'); ?></th>
            <th><?php echo get_string("reader", 'jinotechboard'); ?></th>
            <th><?php echo get_string("readdate", 'jinotechboard'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($read_users as $user) { ?>
            <tr>
                <td><?php echo $num++; ?></td>
                <td><?php echo fullname($user); ?></td>
                <td><?php echo date('Y-m-d h:i:s', $user->firstread); ?></td>
            </tr>
        <?php
            } 
            if(!$read_users){
                echo '<tr><td colspan=3>'.get_string('noreadstudent','jinotechboard').'</td></tr>';
            }
        ?>
    </tbody>
</table>
<?php echo $OUTPUT->footer(); ?>