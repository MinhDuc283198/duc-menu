<?php
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT);       // Course Module ID
$b = optional_param('b', 0, PARAM_INT);        // Board ID
$boardform = optional_param('boardform', jinotechboard_get_cookie("boardform", JINOTECHBOARD_BOARDFORM_THREAD), PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);     // which page to show
$searchfield = optional_param('searchfield', '', PARAM_CLEAN); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_CLEAN); // search string
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수

if ($id) {
    if (!$cm = get_coursemodule_from_id('jinotechboard', $id)) {
        print_error('invalidcoursemodule');
    }
    if (!$course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }
    if (!$board = $DB->get_record("jinotechboard", array("id" => $cm->instance))) {
        print_error('invalidboardid', 'jinotechboard');
    }

    // move require_course_login here to use forced language for course
    // fix for MDL-6926

    require_course_login($course, true, $cm);
} else if ($b) {

    if (!$board = $DB->get_record("jinotechboard", array("id" => $b))) {
        print_error('invalidboardid', 'jinotechboard');
    }
    if (!$course = $DB->get_record("course", array("id" => $board->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
        print_error('missingparameter');
    }
    // move require_course_login here to use forced language for course
    // fix for MDL-6926

    require_course_login($course, true, $cm);
} else {
    print_error('missingparameter');
}

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['b'] = $b;
}
if ($page) {
    $params['page'] = $page;
}
$title = $board->name;

$context = context_module::instance($cm->id);

//$classname = context_helper::get_class_for_level(CONTEXT_MODULE);
//
//$contexts[$cm->id] = $classname::instance($cm->id);
//
//$context = $contexts[$cm->id];
//
$PAGE->set_url('/mod/jinotechboard/view.php', $params);

$PAGE->set_context($context);
$PAGE->set_title(get_string('pluginname', 'jinotechboard'));
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->set_pagelayout('course');

if (!has_capability('mod/jinotechboard:view', $context)) {
    if ($USER->id == 0) {
        require_login();
    } else {
        notice(get_string('cannotviewpostlist', 'jinotechboard'), $_SERVER["HTTP_REFERER"]);
    }
}

//$eventparams = array(
//    'context' => $context,
//    'objectid' => $board->id
//);
//$event = \mod_jinotechboard\event\course_module_viewed::create($eventparams);
//$event->add_record_snapshot('course_modules', $cm);
//$event->add_record_snapshot('course', $course);
//$event->add_record_snapshot('jinotechboard', $board);
//$event->trigger();


echo $OUTPUT->header();

if ($cm->id) {
    add_to_log($course->id, "jinotechboard", "view board", "view.php?id=$cm->id", "$board->id", $cm->id);
} else {
    add_to_log($course->id, "jinotechboard", "view board", "view.php?id=$cm->id", "$board->id");
}
?>
<h2><?php echo get_string($title, 'theme_oklassedu'); ?></h2>
<div class="tab-table-section">
    <div class="table-top-area">
        <?php
        if (!empty($id)) {
            $formparam = "id=" . $id;
        } else if (!empty($b)) {
            $formparam = "b=" . $b;
        }
        $boardform_arr = array(JINOTECHBOARD_BOARDFORM_THREAD => get_string('board_thread', 'mod_jinotechboard'), JINOTECHBOARD_BOARDFORM_GENERAL => get_string('board_general', 'mod_jinotechboard'));
        $boardform_url = new moodle_url($CFG->wwwroot . '/mod/jinotechboard/view.php?' . $formparam);

        echo "<form name='searchForm' class='table-search-option' method='POST' action='" . $CFG->wwwroot . "/mod/jinotechboard/view.php?id=$cm->id'>";

        $option_arr = array("1" => get_string('title', 'jinotechboard'),
            "2" => get_string('writer', 'jinotechboard'),
            "3" => get_string('content', 'jinotechboard'));
        echo html_writer::select($option_arr, 'searchfield', $searchfield, '', array("title" => "title"));
        echo "<input type='text' title='search' name='searchvalue' class='board_search_input'  value='" . $searchvalue . "' />";
        echo "<input type='submit' class='board-search' value='" . get_string('search', 'jinotechboard') . "'/>";
        echo "<input type='hidden' name='boardform' value='" . $boardform . "' />";
        echo "<input type='hidden' name='perpage' value='" . $perpage . "' />";

        echo "</form>";

        //perpage
        $perpage_arr = array();
        $perpage_arr[10] = get_string("content:perpage", "local_board", 10);
        $perpage_arr[20] = get_string("content:perpage", "local_board", 20);
        $perpage_arr[30] = get_string("content:perpage", "local_board", 30);
        $perpage_arr[50] = get_string("content:perpage", "local_board", 50);

        $perpage_param = '';
        if (!empty($searchfield)) {
            $perpage_param .= "&searchfield=" . $searchfield;
        }

        if (!empty($searchvalue)) {
            $perpage_param .= "&searchvalue=" . $searchvalue;
        }

        $perpage_url = new moodle_url($CFG->wwwroot . '/mod/jinotechboard/view.php?boardform=' . $boardform . '&id=' . $cm->id . $perpage_param);
        $select_perpage = new single_select($perpage_url, 'perpage', $perpage_arr, $perpage, null);
        $select_perpage->class = 'perpage fl';
        $select_perpage->attributes[title] = "page";


        //$select_boardform = new single_select($boardform_url, 'boardform', $boardform_arr, $boardform, null);
        //$select_boardform->class = 'boardform fl';
        ?>
        <div class="table-top-left">
            <?php
            echo "<form method='get' id='f_form' >";
            echo "<input type='hidden' name='id' value='" . $cm->id . "' />";
            echo "<input type='hidden' name='groupid' value='" . $groupid . "' />";
            echo "<input type='hidden' name='searchvalue' value='" . $searchvalue . "' />";
            echo "<input type='hidden' name='searchfield' value='" . $searchfield . "' />";
            echo "<input type='hidden' name='perpage' value='" . $perpage . "' />";
            echo '<div class="board-style">';
            echo ($boardform == JINOTECHBOARD_BOARDFORM_THREAD) ? "<label class='board-style-thread'><input type='radio' id='f_boardform' name='boardform' value='" . JINOTECHBOARD_BOARDFORM_THREAD . "' checked>" . get_string('bstyle_thred', 'jinoforum') . "<span></span></label>" : "<label class='board-style-thread'><input type='radio' name='boardform' value='" . JINOTECHBOARD_BOARDFORM_THREAD . "'>" . get_string('bstyle_thred', 'jinoforum') . "<span></span></label>";
            echo ($boardform == JINOTECHBOARD_BOARDFORM_GENERAL) ? "<label class='board-style-list'><input type='radio' id='f_boardform' name='boardform' value='" . JINOTECHBOARD_BOARDFORM_GENERAL . "' checked>" . get_string('bstyle_list', 'jinoforum') . "<span></span></label>" : "<label class='board-style-list'><input type='radio' name='boardform' value='" . JINOTECHBOARD_BOARDFORM_GENERAL . "'>" . get_string('bstyle_list', 'jinoforum') . "<span></span></label>";
            echo '</div>';
            echo '</form>';
            if ($boardform == JINOTECHBOARD_BOARDFORM_GENERAL) {
                echo $OUTPUT->render($select_perpage);
            }
            ?>
        </div> <!-- table top left end -->
        <div class="table-top-right">
            <?php
            if ($boardform == JINOTECHBOARD_BOARDFORM_THREAD) {
                if ((($board->type != 1 && $board->type != 6 && $board->type != 4 && $board->type != 3) || has_capability('mod/jinotechboard:edit', $context)) && has_capability('mod/jinotechboard:write', $context)) {
                    echo "<input type='button' style='cursor:pointer;' class='edit' value='" . get_string('newpost', 'jinotechboard') . "' onclick='document.location.href =\"$CFG->wwwroot/mod/jinotechboard/write.php?b=" . $cm->instance . "\"' />";
//                    if(is_siteadmin() || has_capability('mod/jinotechboard:edit', $context)){
//                        echo "<input type='button' value='".get_string('content_copy','jinotechboard')."' onclick='search_course_popup()'>";
//                    }
                }
            } else {
                if ((($board->type != 1 && $board->type != 6 && $board->type != 4 && $board->type != 3) || has_capability('mod/jinotechboard:edit', $context)) && has_capability('mod/jinotechboard:write', $context)) {
                    echo "<input type='button' style='cursor:pointer;' class='listmenu_list' value='" . get_string('newpost', 'jinotechboard') . "' onclick='document.location.href =\"$CFG->wwwroot/mod/jinotechboard/write.php?b=" . $cm->instance . "&boardform=" . $boardform . "\"' />";
//                    if(is_siteadmin() || has_capability('mod/jinotechboard:edit', $context)){
//                        echo "<input type='button' class='listmenu_list' value='".get_string('content_copy','jinotechboard')."' onclick='search_course_popup()'>";
//                    }
                }
            }
            ?>
        </div> <!-- table top right end -->
    </div> <!-- table top area end -->
</div> <!-- tab table section end -->

<form class="a_link_param" method="POST"action="<?php echo $CFG->wwwroot . '/mod/jinotechboard/content.php?b=' . $cm->instance . '&id='. $id; ?>">
    <input type="hidden" name="b" value="<?php echo $cm->instance; ?>" /> 
    <input type="hidden" name="page" value="<?php echo $page; ?>" /> 
    <input type="hidden" name="perpage" value="<?php echo $perpage; ?>" /> 
    <input type="hidden" name="boardform" value="<?php echo $boardform; ?>" /> 
    <input type="hidden" name="searchfield" value="<?php echo $searchfield; ?>" /> 
    <input type="hidden" name="searchvalue" value="<?php echo $searchvalue; ?>" /> 
    <input type="hidden" name="contentId" /> 
    <input type="hidden" name="list_num" value="" /> 

</form>

<?php
if ($boardform == JINOTECHBOARD_BOARDFORM_THREAD) {
    $contents = jinotechboard_thread_contents($cm->instance, 0, JINOTECHBOARD_THREAD_DEFAULT * $page, $searchfield, $searchvalue);
    $contentsnotice = jinotechboard_thread_contents_notice($cm->instance, 0, JINOTECHBOARD_THREAD_DEFAULT, "", "");
    $next = jinotechboard_thread_contents($cm->instance, 1, JINOTECHBOARD_THREAD_DEFAULT * $page, $searchfield, $searchvalue);
    $next_count = count($next);
    ?>

    <div class="thread-style" id="thread">
        <ul class="thread-style-lists">
            <?php
            foreach ($contentsnotice as $content) {
                $contentid = $content->id;
                $content->id = $content->urid;
                $userdate = userdate($content->timecreated);

                if (!empty($content->itemid)) {
                    $filecheck = '&nbsp;&nbsp;<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }

                if ($content->lev) {
                    if ($content->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
                        $step = $content->lev;
                        $date_left_len = $step * 30;
                    } else {
                        $step = JINOTECHBOARD_INDENT_DEFAULT;
                        $date_left_len = $step * 30;
                    }
                    switch ($content->lev) {
                        case 1 : $calcwidth = 30;
                            break;
                        case 2 : $calcwidth = 60;
                            break;
                        case 3 : $calcwidth = 90;
                            break;
                        case 4 : $calcwidth = 120;
                            break;
                        default : $calcwidth = 120;
                            break;
                    }
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                    $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="" /> ';
                } else {
                    $date_left = 'style="width:calc(100% - 0px) !important;"';
                    $step_icon = '';
                }

                echo "<li class='isnotice' " . $date_left . ">";
                if ($content->lev == 0) {
                    //echo '<div class="thread-left"><input type="checkbox" name="contests_copy" value="'.$contentid.'"></div>';
                }
                //echo '<div class="thread-left">' . $OUTPUT->user_picture($content) . '</div>';
                echo "<div class='thread-content'><h1 class='thread-post-title'>" . $step_icon;

                $parentid = 0;
                if ($content->lev != 0) {
                    $parent = $DB->get_record('jinotechboard_contents', array('id' => $content->ref));
                    $parentid = $parent->userid;
                }
                if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $PAGE->cm->context) && $USER->id != $content->userid && $parentid != $USER->id) {
                    echo $content->title;
                } else {
                    echo '<a onclick="contentMove(\'' . $contentid . '\')" style="cursor: pointer;">' . $content->title . '</a>';
                }
                if ($content->isprivate) {
                    echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
                }

                $by = new stdClass();
                $by->name = fullname($content);
                $by->date = userdate($content->timemodified);
                echo '&nbsp;' . $filecheck;
                echo '</h1><span class="thread-post-meta">' . get_string('bynameondate', 'forum', $by) . '</span>';




                echo "</div>";
                echo "<div class='thread-right'>";
                if ($board->allowrecommend)
                    echo "<div class='count-block recomm-count'>" . $content->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_jinotechboard') . "</span></div>";
                if ($board->allowcomment)
                    echo "<div class='count-block reply-count'>" . $content->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_jinotechboard') . "</span></div>";
                echo "<div class='count-block view-count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_jinotechboard') . "</span></div>";
                echo "</div>";
                echo "</li>";
            }

            if (empty($contents)) {
                echo "<div class='thread-right' style='height:30px; line-height:30px;'>" . get_string('nocontent', 'mod_jinotechboard') . "</div>";
            }
            foreach ($contents as $content) {
                if ($board->type == 1) {
                    require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                    //콘텐츠 변수처리
                    $vartypes = $DB->get_records_menu('lmsdata_sendvariable', array('isused' => 1), '', 'fieldname, fieldtype');
                    $vars = $DB->get_records_menu('lmsdata_sendvariable', array('isused' => 1), '', 'fieldname, varname');
                    $data['courseid'] = $course->id;
                    $data['userid'] = $USER->id;
                    $data['contents'] = $content->contents;
                    $data['title'] = $content->title;
                    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
                    $content->contents = $data['contents'];
                    $content->title = $data['title'];
                }
                $contentid = $content->id;
                $content->id = $content->urid;
                $userdate = userdate($content->timecreated);

                if (!empty($content->itemid)) {
                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }

                if ($content->lev) {
                    if ($content->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
                        $step = $content->lev;
                        $date_left_len = $step * 30;
                    } else {
                        $step = JINOTECHBOARD_INDENT_DEFAULT;
                        $date_left_len = $step * 30;
                    }

                    switch ($content->lev) {
                        case 1 : $calcwidth = 30;
                            break;
                        case 2 : $calcwidth = 60;
                            break;
                        case 3 : $calcwidth = 90;
                            break;
                        case 4 : $calcwidth = 120;
                            break;
                        default : $calcwidth = 120;
                            break;
                    }
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                    $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="Reply" /> ';
                } else {
                    $date_left = 'style="width:calc(100% - 0px) !important;"';
                    $step_icon = '';
                }
                echo "<li " . $date_left . ">";
                if ($content->lev == 0) {
                    //echo '<div class="thread-left"><input type="checkbox" name="contests_copy" value="'.$contentid.'"></div>';
                }
                //echo '<div class="thread-left">' . $OUTPUT->user_picture($content) . '</div>';
                echo "<div class='thread-content'><h1 class='thread-post-title'>" . $step_icon;

                $parentid = 0;
                if ($content->lev != 0) {
                    $parent = $DB->get_record('jinotechboard_contents', array('id' => $content->ref));
                    $parentid = $parent->userid;
                }
                if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $PAGE->cm->context) && $USER->id != $content->userid && $parentid != $USER->id) {
                    echo $content->title;
                } else {
                    echo '<a onclick="contentMove(\'' . $contentid . '\')" style="cursor: pointer;">' . $content->title . '</a>';
                }
                if ($content->isprivate) {
                    echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
                }
                echo '&nbsp;' . $filecheck;
                echo "</h1>";
                $by = new stdClass();
                $by->name = fullname($content);
                $by->date = userdate($content->timemodified);
                echo '<span class="thread-post-meta">' . get_string('bynameondate', 'forum', $by) . '</span>';

                if ($content->isprivate && !has_capability('mod/jinotechboard:secretmanager', $PAGE->cm->context) && $USER->id != $content->userid && $parentid != $USER->id) {
                    echo '<p class="thread-post-content">' . get_string('content:secret', 'mod_jinotechboard') . '</p>';
                } else {
                    echo '<p class="thread-post-content">' . mb_strimwidth(strip_tags($content->contents), 0, 630, '...', 'utf-8') . '</p>';
                }
                echo "</div>";

                echo "<div class='thread-right'>";
                if ($board->allowrecommend)
                    echo "<div class='thread-right'><div class='count-block recomm-count'>" . $content->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_jinotechboard') . "</span></div>";
                if ($board->allowcomment)
                    echo "<div class='count-block reply-count'>" . $content->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_jinotechboard') . "</span></div>";

                if (has_capability('mod/jinotechboard:edit', $context)) {
                    echo "<div style='cursor:pointer;' onclick='view_users(" . $contentid . ")' class='count-block view-count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_jinotechboard') . "</span></div>";
                } else {
                    echo "<div class='count-block view-count'>" . $content->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_jinotechboard') . "</span></div>";
                }
                echo "</div>";
                echo "</li>";
            }
            ?>

        </ul>    
    </div>

    <?php
    if ($next_count > 0) {
        ?>
        <div id="next_btn">
            <input type="button" class="next" value="<?php echo get_string('next:content', 'mod_jinotechboard'); ?>" onclick="ThreadContent()">
        </div>
        <?php
    }
} else if ($boardform == JINOTECHBOARD_BOARDFORM_GENERAL) {
    $totalcount = jinotechboard_get_discussions_count($cm, $searchfield, $searchvalue, true);
    jinotechboard_print_discussions($page - 1, $perpage, $totalcount, $cm, $board, 'st.ref DESC, st.step asc', $searchfield, $searchvalue, true, $boardform);

    $page_params = array();
    $page_params['boardform'] = $boardform;
    $page_params['perpage'] = $perpage;

    if (!empty($searchfield)) {
        $page_params['searchfield'] = $searchfield;
    }

    if (!empty($searchvalue)) {
        $page_params['searchvalue'] = $searchvalue;
    }

    if ($id) {
        $page_params['id'] = $id;
    } else {
        $page_params['b'] = $b;
    }
    ?>
    <div class="table-footer-area">
    <?php
    $total_pages = jinotechboard_get_total_pages($totalcount, $perpage);

    jinotechboard_get_paging_bar($CFG->wwwroot . '/mod/jinotechboard/view.php', $page_params, $total_pages, $page);
    ?>
    </div>    

    <?php
}
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    $("input[name=boardform]:radio").change(function () {
        var val = $("input[name=boardform]:checked").val();
        setCookie("boardform", val);
        $("#f_form").submit();
    });
//    $("#bookmark_ico").click(function () {
//        var url = "<?php echo $CFG->wwwroot; ?>/local/bookmark/ajax/add_new_bookmark.php?course=<?php echo $course->id; ?>&instance=<?php echo $board->id; ?>&activity=jinotechboard";
//
//        $.ajax({url: url,
//            success: function (result) {
//                $("#bookmark_ico").html(result);
//            }
//        });
//    });

    function setCookie(cKey, cValue)  // name,pwd
    {
        var date = new Date();
        var validity = 90;
        date.setDate(date.getDate() + validity);
        // 쿠키 저장
        document.cookie = cKey + '=' + escape(cValue) + ';path=/;expires=' + date.toGMTString();

    }

    function ThreadContent() {
        var page = $('input:hidden[name=page]').val();
        jQuery.ajax({
        url: "<?php echo $CFG->wwwroot . '/mod/jinotechboard/thread.ajax.php'; ?>",
                data: {
                "page": page,
<?php
if (!empty($searchfield)) {
    echo '"searchfield" : ' . $searchfield . ',';
}
if (!empty($searchvalue)) {
    echo '"searchvalue" : ' . $searchvalue . ',';
}
?>
                "instance" : <?php echo $cm->instance; ?>,
                        "cmid" : <?php echo $cm->id; ?>
                },
                type: "POST",
                async: false,
                success: function (data) {
                $('#next_btn').remove();
                        $('.thread-style-lists').append(data);
                }
        });
        page = Number(page);
        $('input:hidden[name=page]').val(page + 1);
    }

    function contentMove(contentId, list_num, searchfield, searchvalue, page, perpage) {
        $('input:hidden[name=contentId]').val(contentId);
        $('input:hidden[name=list_num]').val(list_num);
        $('.a_link_param').submit();
    }

    function search_course_popup() {
        var tag = $("<div id='course_search_popup'></div>");

        var contents_id = [];
        $("input[name='contests_copy']:checked").each(function () {
            contents_id.push($(this).val());
        });
        jQuery.ajaxSettings.traditional = true;

        $.ajax({
            url: '<?php echo $CFG->wwwroot . '/mod/jinotechboard/course_search.php'; ?>',
            method: 'POST',
            data: {
                'contents_id': JSON.stringify(contents_id)
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: '<?php echo get_string('course_search', 'local_lmsdata'); ?>',
                    modal: true,
                    width: 800,
                    resizable: false,
                    height: 600,
                    buttons: [{id: 'close',
                            text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }},
                        {id: 'copy',
                            text: '<?php echo get_string('content_copy', 'jinotechboard'); ?>',
                            disable: true,
                            click: function () {
                                $('#frm_course_board').submit();
                            }}],
                    close: function () {
                        $('#frm_course_search').remove();
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }

    function view_users(id) {
        window.open('view_users.php?id=' + id, 'view_users', 'width=500,height=500');
    }
</script>