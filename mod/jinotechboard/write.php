<?php

require_once '../../config.php';
require_once './lib.php';
require_once './write_form.php';
require_once $CFG->libdir . '/completionlib.php';
require_once $CFG->dirroot . '/lib/formslib.php';
require_once $CFG->dirroot . '/lib/form/filemanager.php';
require_once($CFG->dirroot . '/chamktu/support/smsconfig.php');

$reply = optional_param('reply', 0, PARAM_INT);
$b = optional_param('b', 0, PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);
$delete = optional_param('delete', 0, PARAM_INT);
$name = optional_param('name', '', PARAM_CLEAN);
$confirm = optional_param('confirm', 0, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);
$isnotice = optional_param('isnotice', 0, PARAM_INT);
$isprivate = optional_param('isprivate', 0, PARAM_INT);
$ref = optional_param('ref', 0, PARAM_INT);
$step = optional_param('step', 0, PARAM_INT);
$lev = optional_param('lev', 0, PARAM_INT);
$contents = optional_param('contents', '', PARAM_RAW);
$title = optional_param('title', '', PARAM_CLEAN);
$confirmed = optional_param('confirmed', 0, PARAM_INT);
$mode = optional_param('mode', "", PARAM_CLEAN);
$boardoptions = optional_param('boardoptions', 0, PARAM_INT);
$contentId = optional_param('contentId', 0, PARAM_INT);
$parentId = optional_param('parentId', 0, PARAM_INT);
$num_notice = optional_param('num_notice', 0, PARAM_INT);
$itemid = optional_param('itemid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$searchfield = optional_param('searchfield', '', PARAM_RAW); // search string
$searchvalue = optional_param('searchvalue', '', PARAM_RAW); // search string
$boardform = optional_param('boardform', JINOTECHBOARD_BOARDFORM_THREAD, PARAM_INT);
$availablefromenabled = optional_param('availablefromenabled', 0, PARAM_INT);
$send_type = optional_param_array('send_type', array(), PARAM_RAW);
$sms_content = optional_param('sms_content', '', PARAM_RAW);
$smssender = optional_param('smssender', '010-0000-0000', PARAM_RAW);
$contents_no = optional_param('contents_no', 0, PARAM_INT);

$category_field = 0;
if ($b) {

    if (!$board = $DB->get_record("jinotechboard", array("id" => $b))) {
        print_error('invalidboardid', 'jinotechboard');
    }
    if (!$course = $DB->get_record("course", array("id" => $board->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
        print_error('missingparameter');
    }


    require_course_login($course, true, $cm);
    $strboards = get_string("modulenameplural", "jinotechboard");
    $strboard = get_string("modulename", "jinotechboard");
} else {
    print_error('missingparameter');
}

require_login();

$classname = context_helper::get_class_for_level(CONTEXT_MODULE);

$contexts[$cm->id] = $classname::instance($cm->id);

$context = $contexts[$cm->id];

$PAGE->set_url('/mod/jinotechboard/write.php', array(
    'reply' => $reply,
    'b' => $b,
    'edit' => $edit,
    'delete' => $delete,
    'confirm' => $confirm,
));
if ($board->type == 1 && !has_capability('mod/jinotechboard:edit', $context)) {
    redirect('view.php?id=' . $cm->id, 'No capability', 2);
}

$PAGE->set_context($context);
$PAGE->set_heading($course->fullname);
if ($board->course == SITEID) {
    $PAGE->set_pagelayout('general');
    $PAGE->navbar->ignore_active();
    $PAGE->navbar->add(get_string('study_reference_room', 'theme_dgist'), new moodle_url('mod/jinotechboard/view.php', array('b' => $CFG->NOTICEID)));
    $PAGE->navbar->add(get_string($board->name, "theme_dgist"));
    $PAGE->set_title(format_string(get_string($board->name, "theme_dgist")));
    $PAGE->set_heading(format_string(get_string($board->name, "theme_dgist")));

    $CFG->page_icon = '/theme/dgist/pix/images/support_icon.png';
    $CFG->page_description = get_string('support_' . $board->name . '_content', 'theme_dgist');
}

echo $OUTPUT->header();
echo html_writer::tag('h2', get_string($board->name, 'theme_oklassedu'), array('class' => 'board-title'));

if ($confirmed == 0) {
    if ($mode == "edit" && $contentId > 0) {
        $data = $DB->get_record('jinotechboard_contents', array('id' => $contentId));
        $data = trusttext_pre_edit($data, 'contents', $context);

//        if (!has_capability('mod/jinotechboard:edit', $context) || ($data->userid != $USER->id && (jino_get_usercase($USER->id) != 'manager'))) {
        if (!has_capability('mod/jinotechboard:edit', $context) && ($data->userid != $USER->id)) {
            notice('cannoteditpost', 'jinotechboard');
        }

        $title = $data->title;
        $contents = $data->contents;

        $category_field = $data->category;
        //교안
        $tplanlist = $DB->get_records('files',array('contextid'=>$context->id, 'component'=>'mod_jinotechboard', 'filearea'=>'attachment','itemid'=>$contentId) );
        foreach($tplanlist as $tplan){
            if($tplan->filesize > 0){
                $contents_no = $tplan->referencefileid;
                $tplan_contents = $DB->get_record('lcms_contents', array('id'=>$contents_no));
                $tplantitlestatic = $tplan_contents->con_name;
            }
        }

    } else if ($mode == "reply") {
        if (!has_capability('mod/jinotechboard:reply', $context) || $board->allowreply == 0) {
            notice('cannotreplypost', 'jinotechboard');
        }

        $parentData = $DB->get_record('jinotechboard_contents', array('id' => $contentId));
        $title = "Re:" . $parentData->title;
    } else {
        if (!has_capability('mod/jinotechboard:write', $context)) {
            notice('cannotaddpost', 'jinotechboard');
        }
    }
}

$postid = empty($data->id) ? null : $data->id;
$editor_option = jinotechboard_editor_options($context, $postid);

$draftid_editor = file_get_submitted_draft_itemid('contents');
$contents = file_prepare_draft_area($draftid_editor, $context->id, 'mod_jinotechboard', 'contents', $postid, $editor_option, $contents);

editors_head_setup();

$editor = editors_get_preferred_editor(FORMAT_HTML);
$editor->id = "editor_contents";
$args = new stdClass();
$args->accepted_types = array('web_image');
$args->return_types = (FILE_INTERNAL | FILE_EXTERNAL);
$args->context = $context;
$args->env = 'filepicker';

$draftitemid = file_get_submitted_draft_itemid('attachments');
// advimage plugin
$image_options = initialise_filepicker($args);
$image_options->context = $context;
$image_options->client_id = uniqid();
$image_options->maxbytes = $editor_option['maxbytes'];
$image_options->env = 'editor';
$image_options->itemid = $draftitemid;

$fpoptions = array();
$fpoptions['image'] = $image_options;

$editor->use_editor($editor->id, $editor_option, $fpoptions);


// form data
file_prepare_draft_area($draftitemid, $context->id, 'mod_jinotechboard', 'attachment', empty($contentId) ? null : $contentId, jinotechboard_write_form::attachment_options($board));


$url = new moodle_url($CFG->wwwroot . '/mod/jinotechboard/write.php?');

$editor_options = array('noclean' => true, 'subdirs' => true, 'maxfiles' => $board->maxattachments, 'maxbytes' => $board->maxbytes, 'context' => $context);

$mformdata = array('options' => $editor_options,
    'confirmed' => 1,
    'board' => $board,
    'itemid' => $draftid_editor,
    'boardform' => $boardform,
    'context' => $context,
    'type' => $board->type,
    'mode' => $mode);

if ($mode == "edit") {
    $mformdata['lev'] = $data->lev;
    $mformdata['isnotice'] = $data->isnotice;
    $mformdata['isprivate'] = $data->isprivate;
    $form_data['timeend'] = $data->timeend;
    if($board->type == 4){
        $mformdata['contents_no'] = $contents_no;
        $mformdata['tplantitlestatic'] = $tplantitlestatic;
    }
}
if ($mode == 'reply') {
    $mformdata['isprivate'] = $parentData->isprivate;
}

$search_param = "";
if (!empty($searchfield)) {
    $mformdata['searchfield'] = $searchfield;
    $search_param .= "&searchfield=" . $searchfield;
}

if (!empty($searchvalue)) {
    $mformdata['searchvalue'] = $searchvalue;
    $search_param .= "&searchvalue=" . $searchvalue;
}

if (!empty($page)) {
    $mformdata['page'] = $page;
    $search_param .= "&page=" . $page;
}

if (!empty($perpage)) {
    $mformdata['perpage'] = $perpage;
    $search_param .= "&perpage=" . $perpage;
}

$mform = new jinotechboard_write_form($url, $mformdata);


if ($mform->is_cancelled()) {
    redirect("view.php?b=$board->id&boardform=$boardform$search_param");
} else if ($fromform = $mform->get_data()) {
    if ($mode == "edit") {
        $data = $DB->get_record('jinotechboard_contents', array('id' => $contentId));

        if (!has_capability('mod/jinotechboard:edit', $context) && $USER->id != $data->userid) {
            notice('cannoteditpost', 'jinotechboard');
        }



        $newdata = new object();
        $newdata->course = $course->id;
        $newdata->board = $b;

        $newdata->title = $title;
        $newdata->contents = $contents['text'];
        $newdata->itemid = $itemid;

        $newdata->id = $contentId;
        $newdata->category = $category;

        $newdata->isnotice = $isnotice;
        $newdata->isprivate = $isprivate;

        $newdata->timemodified = time();
        if ($availablefromenabled != 0) {
            $newdata->timeend = $fromform->timeend;
        }
        $newdata->contents = file_save_draft_area_files($newdata->itemid, $context->id, 'mod_jinotechboard', 'contents', $newdata->id, jinotechboard_editor_options($context, null), $newdata->contents);
        $DB->set_field('jinotechboard_contents', 'contents', $newdata->contents, array('id' => $newdata->id));
        $DB->set_field('jinotechboard_contents', 'isprivate', $newdata->isprivate, array('ref' => $data->ref));

        $DB->update_record('jinotechboard_contents', $newdata);

        $params = array(
            'objectid' => $newdata->id,
            'context' => $context,
            'other' => array(
                'jinotechboardid' => $board->id,
            )
        );

        if ($board->type == 1 && $send_type['sms'] == true) {
            $smsdata = new stdClass();
            $smsdata->subject = $title;
            $smsdata->contents = $sms_content;
            $code = set_smssend_mod_jinotechboard($smsdata);
            if(!$code){
                echo 'Set Sms Error';
                die();
            }
        }

        if ($board->type == 1) {
            $course_context = context_course::instance($course->id, IGNORE_MISSING);
            $sql = "select u.* from {role_assignments} ra "
                    . "join {user} u on u.id = ra.userid "
                    . "join {role} r on r.id = ra.roleid and r.archetype ='student'"
                    . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
                    . "where ra.contextid = :contextid ";
            $param = array('contextid' => $course_context->id, 'contextlevel' => CONTEXT_COURSE);
            $users = $DB->get_records_sql($sql, $param);
            foreach ($users as $user) {
                if ($send_type['mail'] == true) {
                    $mailresult = email_to_user($user, $USER, $title, html_to_text($newdata->contents), $newdata->contents);
                }
                if ($send_type['message'] == true) {
                    $eventdata = new stdClass();
                    $eventdata->name = 'message';
                    $eventdata->component = 'mod_jinotechboard';
                    $eventdata->userfrom = $USER;
                    $eventdata->userto = $user;
                    $eventdata->subject = $title;
                    $eventdata->fullmessage = html_to_text($newdata->contents);
                    $eventdata->fullmessageformat = FORMAT_PLAIN;
                    $eventdata->fullmessagehtml = $newdata->contents;
                    $eventdata->smallmessage = '';
                    $good = $good && message_send($eventdata);
                }
                if ($send_type['sms'] == true) {
                    $smsdata->callback = $smssender;
                    send_sms_mod_jinotechboard($user, $smsdata, $code);
                }
            }
        }


        $event = \mod_jinotechboard\event\content_updated::create($params);
        $event->add_record_snapshot('jinotechboard_contents', $newdata);
        $event->trigger();
        
        if($board->type == 4){
            if($contents_no != 0){
                $fs = get_file_storage();
                $overlap_files = $fs->get_area_files($context->id, 'mod_jinotechboard', "attachment", $contentId, 'id');
                foreach ($overlap_files as $file) {
                    if ($file->get_filesize() > 0) {
                        $filename = $file->get_filename();
                        $file->delete();
                    }
                }
            
                // Copy the files from the source area.
                if ($files = $fs->get_area_files(1, 'local_lmsdata', 'tplan', $contents_no, 'sortorder, id', false)) {
            //    if ($files = $fs->get_area_files(1, 'local_lmsdata', 'banner', 25, 'sortorder, id', false)) {
                    foreach ($files as $file) {
                        $newrecord = new \stdClass();
                        $newrecord->contextid = $context->id;
                        $newrecord->component = "mod_jinotechboard";
                        $newrecord->filearea = "attachment";
                        $newrecord->itemid = $contentId;
                        $newrecord->referencefileid = $contents_no;
                        $aa = $fs->create_file_from_storedfile($newrecord, $file);
                    }
                }
            }
        }else{
            $draftitemid = file_get_submitted_draft_itemid('attachments');
            file_save_draft_area_files($draftitemid, $context->id, 'mod_jinotechboard', 'attachment', $newdata->id);
        }
        echo '<script language="javascript">';
        echo 'document.location.href="' . $CFG->wwwroot . '/mod/jinotechboard/content.php?contentId=' . $contentId . '&b=' . $b . '&boardform=' . $boardform . $search_param . '";';
        echo '</script>';
        die();
    } else if ($mode == "reply") {
        if (!has_capability('mod/jinotechboard:reply', $context) || $board->allowreply == 0) {
            notice('cannotreplypost', 'jinotechboard');
        }

        $updatedata = new object();

        $DB->execute("update {jinotechboard_contents} set step = step + 1 where board = ? and ref = ? and step > ?", array($board->id, $ref, $step));

        $newdata = new object();

        $newdata->course = $course->id;
        $newdata->board = $b;
        $newdata->userid = $USER->id;

        $newdata->title = $title;
        $newdata->category = $category;
        $newdata->contents = $contents['text'];

        $parentData = $DB->get_record('jinotechboard_contents', array('id' => $ref), 'isprivate');
        $newdata->isprivate = $parentData->isprivate;
        $newdata->isnotice = 0;

        $newdata->ref = $ref;
        $newdata->step = $step + 1;
        $newdata->lev = $lev + 1;

        $newdata->viewcnt = 0;
        $newdata->timecreated = time();
        $newdata->timemodified = time();

        $newdata->itemid = $contents['itemid'];
        if ($newid = $DB->insert_record('jinotechboard_contents', $newdata)) {
            $newdata->contents = file_save_draft_area_files($newdata->itemid, $context->id, 'mod_jinotechboard', 'contents', $newid, jinotechboard_editor_options($context, null), $newdata->contents);
            $DB->set_field('jinotechboard_contents', 'contents', $newdata->contents, array('id' => $newid));

            $draftitemid = file_get_submitted_draft_itemid('attachments');
            file_save_draft_area_files($draftitemid, $context->id, 'mod_jinotechboard', 'attachment', $newid);

            $params = array(
                'objectid' => $newdata->id,
                'context' => $context,
                'other' => array(
                    'jinotechboardid' => $board->id,
                )
            );

            $event = \mod_jinotechboard\event\content_created::create($params);
            $event->add_record_snapshot('jinotechboard_contents', $newdata);
            $event->trigger();
        }
        echo '<script language="javascript">';
        echo 'document.location.href="' . $CFG->wwwroot . '/mod/jinotechboard/content.php?contentId=' . $newid . '&b=' . $b . '&boardform=' . $boardform . $search_param . '";';
        echo '</script>';
        die();
    } else {
        if (!has_capability('mod/jinotechboard:write', $context)) {
            notice('cannotaddpost', 'jinotechboard');
        }

        $newdata = new object();

        if (!empty($title) && confirm_sesskey(sesskey())) {
            $newdata->course = $course->id;
            $newdata->board = $b;
            $newdata->userid = $USER->id;
            $newdata->title = $title;
            $newdata->category = $category;
            $newdata->contents = $contents['text'];

            $newdata->itemid = $contents['itemid'];

            $newdata->ref = 0;
            $newdata->step = 0;
            $newdata->lev = 0;
            $newdata->isnotice = $isnotice;
            $newdata->isprivate = $isprivate;

            $newdata->viewcnt = 0;

            $newdata->timecreated = time();
            $newdata->timemodified = time();
            if ($availablefromenabled != 0) {
                $newdata->timeend = $fromform->timeend;
            }

            if ($board->type == 1 && $send_type['sms'] == true) {
                $smsdata = new stdClass();
                $smsdata->subject = $title;
                $smsdata->contents = $sms_content;
                $code = set_smssend_mod_jinotechboard($smsdata);
            }



            if ($newid = $DB->insert_record('jinotechboard_contents', $newdata)) {
                $newdata->id = $newid;
                $DB->set_field_select('jinotechboard_contents', "ref", $newid, "id=$newid");

                $newdata->contents = file_save_draft_area_files($newdata->itemid, $context->id, 'mod_jinotechboard', 'contents', $newdata->id, jinotechboard_editor_options($context, null), $newdata->contents);
                $DB->set_field('jinotechboard_contents', 'contents', $newdata->contents, array('id' => $newdata->id));

                $params = array(
                    'objectid' => $newdata->id,
                    'context' => $context,
                    'other' => array(
                        'jinotechboardid' => $board->id,
                    )
                );
                if ($board->type == 1) {
                    $course_context = context_course::instance($course->id, IGNORE_MISSING);
                    $sql = "select u.* from {role_assignments} ra "
                            . "join {user} u on u.id = ra.userid "
                            . "join {role} r on r.id = ra.roleid and r.archetype ='student'"
                            . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
                            . "where ra.contextid = :contextid ";
                    $param = array('contextid' => $course_context->id, 'contextlevel' => CONTEXT_COURSE);
                    $users = $DB->get_records_sql($sql, $param);
                    foreach ($users as $user) {
                        if ($send_type['mail'] == true) {
                            $mailresult = email_to_user($user, $USER, $title, html_to_text($newdata->contents), $newdata->contents);
                        }
                        if ($send_type['message'] == true) {
                            $eventdata = new stdClass();
                            $eventdata->name = 'message';
                            $eventdata->component = 'mod_questionnaire';
                            $eventdata->userfrom = $USER;
                            $eventdata->userto = $user;
                            $eventdata->subject = $title;
                            $eventdata->fullmessage = html_to_text($newdata->contents);
                            $eventdata->fullmessageformat = FORMAT_PLAIN;
                            $eventdata->fullmessagehtml = $newdata->contents;
                            $eventdata->smallmessage = '';
                            $good = $good && message_send($eventdata);
                        }
                        if ($send_type['sms'] == true) {
                            $smsdata->callback = $smssender;
                            send_sms_mod_jinotechboard($user, $smsdata, $code);
                        }
                    }
                }
                $event = \mod_jinotechboard\event\content_created::create($params);
                $event->add_record_snapshot('jinotechboard_contents', $newdata);
                $event->trigger();
            }
        }

        if($board->type == 4){
            if($contents_no != 0){
                $fs = get_file_storage();
                // Copy the files from the source area.
                if ($files = $fs->get_area_files(1, 'local_lmsdata', 'tplan', $contents_no, 'sortorder, id', false)) {
            //    if ($files = $fs->get_area_files(1, 'local_lmsdata', 'banner', 25, 'sortorder, id', false)) {
                    foreach ($files as $file) {
                        $newrecord = new \stdClass();
                        $newrecord->contextid = $context->id;
                        $newrecord->component = "mod_jinotechboard";
                        $newrecord->filearea = "attachment";
                        $newrecord->itemid = $newid;
                        $newrecord->referencefileid = $contents_no;
                        $aa = $fs->create_file_from_storedfile($newrecord, $file);
                    }
                }
            }
        }else{
            $draftitemid = file_get_submitted_draft_itemid('attachments');
            file_save_draft_area_files($draftitemid, $context->id, 'mod_jinotechboard', 'attachment', $newid);
        }
        
        echo '<script language="javascript">';
        echo 'document.location.href="' . $CFG->wwwroot . '/mod/jinotechboard/view.php?b=' . $b . '&boardform=' . $boardform . $search_param . '";';
        echo '</script>';
        die();
    }
    redirect("index.php?b=$board->id");
}

$form_data = array(
    'title' => $title,
    'boardform' => $boardform,
    'contents' => array(
        'text' => $contents,
        'format' => 1,
        'itemid' => $draftid_editor,
        'timeend' => null,
        ));

if ($board->allowsecret == 1 && $mode == "edit") {
    $form_data['isprivate'] = $data->isprivate;
}

if ($mode == "edit") {
    $form_data['attachments'] = $draftitemid;
    $content = $DB->get_record('jinotechboard_contents', array('id' => $contentId));
    $form_data['lev'] = $data->lev;
    $form_data['contentId'] = $contentId;
    $form_data['mode'] = 'edit';
    $form_data['timeend'] = $data->timeend;
    $form_data['contents_no'] = $contents_no;
    $form_data['tplantitlestatic'] = $tplantitlestatic;
}
if ($mode == "reply") {
    $form_data['contentId'] = $contentId;
    $form_data['ref'] = $parentData->ref;
    $form_data['step'] = $parentData->step;
    $form_data['lev'] = $parentData->lev;
    $form_data['mode'] = 'reply';
}

$mform->set_data($form_data);

$mform->display();

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_jinotechboard', 'attachment', $data->id, 'id', false);
if ($files) {
    ?>
    <script>
        $("input[name=view_filemanager]").attr("checked", true);
        $("#fitem_id_attachments").show();
    </script>
    <?php

}
?>
<script>
    $(window).load(function () {
        if ($("input[name=view_filemanager]").prop("checked")) {
            $("#fgroup_id_filemanager").show();
        } else {
            $("#fgroup_id_filemanager").hide();
        }
    });
    $("input[name=view_filemanager]").click(function () {
        if ($("input[name=view_filemanager]").prop("checked")) {
            $("#fgroup_id_filemanager").show();
        } else {
            $("#fgroup_id_filemanager").hide();
        }
    });
</script>
<?php

echo $OUTPUT->footer();


