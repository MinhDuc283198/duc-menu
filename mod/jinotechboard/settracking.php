<?php


require_once("../../config.php");
require_once("lib.php");

$id         = required_param('id',PARAM_INT);                           
$returnpage = optional_param('returnpage', 'index.php', PARAM_FILE);

$url = new moodle_url('/mod/jinotechboard/settracking.php', array('id'=>$id));
if ($returnpage !== 'index.php') {
    $url->param('returnpage', $returnpage);
}
$PAGE->set_url($url);

if (! $jinotechboard = $DB->get_record("jinotechboard", array("id" => $id))) {
    print_error('invalidforumid', 'forum');
}

if (! $course = $DB->get_record("course", array("id" => $jinotechboard->course))) {
    print_error('invalidcoursemodule');
}

if (! $cm = get_coursemodule_from_instance("jinotechboard", $jinotechboard->id, $course->id)) {
    print_error('invalidcoursemodule');
}

require_course_login($course, false, $cm);

$returnto = jinotechboard_go_back_to($returnpage.'?id='.$course->id.'&b='.$jinotechboard->id);

if (!jinotechboard_tp_can_track_jinotechboards($jinotechboard)) {
    redirect($returnto);
}

$info = new stdClass();
$info->name  = fullname($USER);
$info->jinotechboard = format_string($jinotechboard->name);
if (jinotechboard_tp_is_tracked($jinotechboard) ) {
    if (jinotechboard_tp_stop_tracking($jinotechboard->id)) {
        add_to_log($course->id, "jinotechboard", "stop tracking", "view.php?b=$jinotechboard->id", $jinotechboard->id, $cm->id);
        redirect($returnto, get_string("nownottracking", "forum", $info), 1);
    } else {
        print_error('cannottrack', '', $_SERVER["HTTP_REFERER"]);
    }

} else { 
    if (jinotechboard_tp_start_tracking($jinotechboard->id)) {
        add_to_log($course->id, "jinotechboard", "start tracking", "view.php?b=$jinotechboard->id", $jinotechboard->id, $cm->id);
        redirect($returnto, get_string("nowtracking", "forum", $info), 1);
    } else {
        print_error('cannottrack', '', $_SERVER["HTTP_REFERER"]);
    }
}


