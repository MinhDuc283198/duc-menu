<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function xmldb_jinotechboard_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2014112101) {

        // Define field to be added to assign.
        $table = new xmldb_table('jinotechboard');
        $field = new xmldb_field('allowrecommend', XMLDB_TYPE_INTEGER, '1', null,
                                 XMLDB_NOTNULL, null, '0', null);

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Assign savepoint reached.
        upgrade_mod_savepoint(true, 2014112101, 'jinotechboard');
    }
    
    if ($oldversion < 2015030505) {

        // Define field to be added to assign.
        $table = new xmldb_table('jinotechboard');
        $field = new xmldb_field('allowgigan', XMLDB_TYPE_INTEGER, '1', null,
                                 XMLDB_NOTNULL, null, '0', null);

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

    }
    
    if ($oldversion < 2015030505) {

        // Define field to be added to assign.
        $table = new xmldb_table('jinotechboard_contents');
        $field = new xmldb_field('timeend', XMLDB_TYPE_INTEGER, '10', null,
                                 XMLDB_NOTNULL, null, '0', null);

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

    }
    if ($oldversion < 2015040800) {

        // Define field to be added to assign.
        $table = new xmldb_table('jinotechboard_read');
        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null,
                                 XMLDB_NOTNULL, null, '0', null);

        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Assign savepoint reached.
        upgrade_mod_savepoint(true, 2015040800, 'jinotechboard');
    }
	
	if($oldversion < 2017012400) {     
        $table = new xmldb_table('jinotechboard_recommend');  
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', NULL, XMLDB_NOTNULL, null, 0);
        $table->add_field('jinotechboardid', XMLDB_TYPE_INTEGER, '10', NULL, XMLDB_NOTNULL, null, 0);
        $table->add_field('contentsid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timerecommend', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
	
	return true;
}