<?php

defined('MOODLE_INTERNAL') || die;

$functions = array(
    'mod_jinotechboard_view_jinotechboard' => array(
        'classname'     => 'mod_jinotechboard_external',
        'methodname'    => 'view_jinotechboard',
        'description'   => '',
        'type'          => 'write',
        'capabilities'  => 'mod/jinotechboard:view'
    ),
    
    'mod_jinotechboard_get_jinotechboards_by_courses' => array(
        'classname'     => 'mod_jinotechboard_external',
        'methodname'    => 'get_jinotechboards_by_courses',
        'description'   => '',
        'type'          => 'read',
        'capabilities'  => ''
    ),
    
    'mod_jinotechboard_get_contents' => array(
        'classname'     => 'mod_jinotechboard_external',
        'methodname'    => 'get_contents',
        'description'   => '',
        'type'          => 'read',
        'capabilities'  => ''
    ),
    
    'mod_jinotechboard_get_content' => array(
        'classname'     => 'mod_jinotechboard_external',
        'methodname'    => 'get_content',
        'description'   => '',
        'type'          => 'read',
        'capabilities'  => ''
    ),
    
    'mod_jinotechboard_add_content' => array(
        'classname'     => 'mod_jinotechboard_external',
        'methodname'    => 'add_content',
        'description'   => '',
        'type'          => 'write',
        'capabilities'  => ''
    ),
    
    'mod_jinotechboard_add_comment' => array(
        'classname'     => 'mod_jinotechboard_external',
        'methodname'    => 'add_comment',
        'description'   => '',
        'type'          => 'write',
        'capabilities'  => ''
    ),
);
