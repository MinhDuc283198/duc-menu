<?php 
require (dirname(dirname(dirname(__FILE__))) . '/config.php');

$contents_id = optional_param('contents_id', '', PARAM_RAW);
$searchstring = optional_param('value', '', PARAM_RAW);

$context = context_system::instance();
$PAGE->set_context($context);

if(is_siteadmin()){
$sql_select  = "SELECT mc.id, mc.fullname, mc.shortname, 
                lc.timestart, lc.timeend, lc.timeregstart, lc.timeregend, 
                lc.subject_id, lc.year, lc.term, lc.isreged, lc.prof_userid,
                ur.firstname, ur.lastname";

$sql_from    = " FROM {course} mc
                 JOIN {lmsdata_class} lc ON lc.course = mc.id
                 JOIN {course_categories} ca ON ca.id = mc.category 
                 LEFT JOIN {user} ur ON ur.id = lc.prof_userid ";
} else {
$sql_select  = "SELECT mc.id, mc.fullname, mc.shortname, 
            lc.timestart, lc.timeend, lc.timeregstart, lc.timeregend, 
            lc.subject_id, lc.year, lc.term, lc.isreged, lc.prof_userid,
            ur.firstname, ur.lastname";

$sql_from    = " FROM {course} mc
                 JOIN {lmsdata_class} lc ON lc.course = mc.id
                 JOIN {course_categories} ca ON ca.id = mc.category 
                 LEFT JOIN {user} ur ON ur.id = lc.prof_userid 
                 JOIN {context} ct on ct.instanceid = mc.id and ct.contextlevel = 50
                 JOIN {role_assignments} ra on ra.contextid = ct.id and userid = :userid and roleid = :roleid";
}
$sql_where   =  array();
$params = array();

if(!empty($searchstring)) {
    $sql_where[]= '( '.$DB->sql_like('lc.subject_id', ':subject_id').' or '.$DB->sql_like('lc.kor_lec_name', ':kor_lec_name') . ')';
    $params['subject_id'] = '%'.$searchstring.'%';
    $params['kor_lec_name'] = '%'.$searchstring.'%';
}

$params['userid'] = $USER->id;
$params['roleid'] = 3;

$sql_orderby = " ORDER BY lc.subject_id ASC, mc.timecreated DESC ";

if(!empty($sql_where)) {
    $sql_where = ' WHERE '.implode(' and ', $sql_where);
}else {
    $sql_where = '';
}

$courses = $DB->get_records_sql($sql_select.$sql_from.$sql_where.$sql_orderby, $params);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) ".$sql_from.$sql_where, $params);
?>

<div class="popup_content" id="course_search_popup">
    <form id="frm_course_search" class="search_area" onsubmit="re_course_search(); return false;" method="POST">
        <input type="text" name="value" value="<?php echo $searchstring; ?>" class="w_300" placeholder="<?php echo get_string('placeholder7','local_lmsdata'); ?>"/>   
        <input type="submit" class="blue_btn" id="search" value="<?php echo get_string('search','local_lmsdata'); ?>"/>
    </form>
    <form id="frm_course_board" onsubmit="copy_contents(); return false;" class="generaltable" name="frm_course_board" onsubmit="return false;">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th></th>
                <th><?php echo get_string('number', 'local_lmsdata'); ?></th>
                <th><?php echo get_string('stats_coursename','local_lmsdata'); ?></th>
                <th><?php echo get_string('prof_name','local_lmsdata'); ?></th>
                <th><?php echo get_string('select','local_lmsdata'); ?></th>
            </tr>
            <?php
            if($count_courses > 0) {
                $count = 0;
                foreach($courses as $course) {
                    $boards = $DB->get_records('jinotechboard', array('course'=>$course->id));
                    if(empty($boards)){
                        continue;
                    }
                    echo '<tr>';
                    echo '<td><input type="checkbox" value="'.$course->id.'" name="ispost"></td>';
                    echo '<td>'.($count_courses - $count).'</td>';
                    echo '<td>'.$course->fullname.'</td>';
                    echo '<td>'.$course->firstname.$course->lastname.'</td>';
                    echo '<td>';
                    echo '<select name="board_id_'.$course->id.'">';
                    foreach($boards as $board){
                        echo '<option value="'.$board->id.'">'.$board->name.'</option>';
                    }
                    echo '</select>';
                    echo '</td>';
                    echo '</tr>';
                   
                    $count++;
                }
            } else {
                echo '<tr><td colspan="4">'.get_string('empty_course','local_lmsdata').'</td></tr>';
            }
            ?>
            </tbody>
        </table>
    </form>
</div>

<script type="text/javascript">
    function re_course_search() {
        var searchstring = $( "#frm_course_search input[name=value]" ).val();
        $.ajax({
            url: '<?php echo $CFG->wwwroot.'/chamktu/manage/course_search.php'; ?>',
            method: 'POST',
            data: {
                'contents_id': contents_id,
                'value': searchstring
            },
            traditional: true,
            success: function(data) {
                $("#irregular_course_search_popup").parent().html(data);
            },
            error: function(jqXHR, textStatus, errorThrown ) {
            }
        });
    }
    function copy_contents(){
        var board_id_array = [];
        $("input[name='ispost']:checked").each(function() {
            var board_id = $("select[name=board_id_"+$(this).val()+"]").val();
            board_id_array.push(board_id);
        });
        board_id_array = JSON.stringify(board_id_array);
        
        $.ajax({
          url: '<?php echo $CFG->wwwroot.'/mod/jinotechboard/copy_submit.php'; ?>',
          method: 'POST',
          data: {
                'contents_id': '<?php echo $contents_id;?>',
                'boards_id': board_id_array
          },
          success: function(data) {
              alert('복사되었습니다.')
          }
        });
        
        return false;
    }
    function irregular_course_select(id, name) {
        $( "input[name=merger_course_id]" ).attr("value",id);
        $( "input[name=merger_course]" ).attr("value",name);
        $("#course_search_popup").dialog( "close" );
        get_user_list();
    }
</script> 
