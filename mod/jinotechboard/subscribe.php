<?php


require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/mod/jinotechboard/lib.php');

$id      = required_param('id', PARAM_INT);             
$mode    = optional_param('mode', null, PARAM_INT);     
$user    = optional_param('user', 0, PARAM_INT);        
$sesskey = optional_param('sesskey', null, PARAM_RAW);  

$url = new moodle_url('/mod/jinotechboard/subscribe.php', array('id'=>$id));
if (!is_null($mode)) {
    $url->param('mode', $mode);
}
if ($user !== 0) {
    $url->param('user', $user);
}
if (!is_null($sesskey)) {
    $url->param('sesskey', $sesskey);
}
$PAGE->set_url($url);

$jinotechboard   = $DB->get_record('jinotechboard', array('id' => $id), '*', MUST_EXIST);
$course  = $DB->get_record('course', array('id' => $jinotechboard->course), '*', MUST_EXIST);
$cm      = get_coursemodule_from_instance('jinotechboard', $jinotechboard->id, $course->id, false, MUST_EXIST);
$context = get_context_instance(CONTEXT_MODULE, $cm->id);

if ($user) {
    require_sesskey();
    if (!has_capability('mod/jinotechboard:managesubscriptions', $context)) {
        print_error('nopermissiontosubscribe', 'forum');
    }
    $user = $DB->get_record('user', array('id' => $user), MUST_EXIST);
} else {
    $user = $USER;
}

if (isset($cm->groupmode) && empty($course->groupmodeforce)) {
    $groupmode = $cm->groupmode;
} else {
    $groupmode = $course->groupmode;
}
if ($groupmode && !jinotechboard_is_subscribed($user->id, $jinotechboard) && !has_capability('moodle/site:accessallgroups', $context)) {
    if (!groups_get_all_groups($course->id, $USER->id)) {
        print_error('cannotsubscribe', 'forum');
    }
}

require_login($course->id, false, $cm);

if (is_null($mode) and !is_enrolled($context, $USER, '', true)) {  
    $PAGE->set_title($course->shortname);
    $PAGE->set_heading($course->fullname);
    if (isguestuser()) {
        echo $OUTPUT->header();
        echo $OUTPUT->confirm(get_string('subscribeenrolledonly', 'forum').'<br /><br />'.get_string('liketologin'),
                     get_login_url(), new moodle_url('/mod/jinotechboard/view.php', array('f'=>$id)));
        echo $OUTPUT->footer();
        exit;
    } else {
       
        redirect(new moodle_url('/mod/jinotechboard/view.php', array('b'=>$id)), get_string('subscribeenrolledonly', 'forum'));
    }
}

$returnto = optional_param('backtoindex',0,PARAM_INT)
    ? "index.php?id=".$course->id
    : "view.php?b=$id";

if (!is_null($mode) and has_capability('mod/jinotechboard:managesubscriptions', $context)) {
    require_sesskey();
    switch ($mode) {
        case JINOTECHBOARD_CHOOSESUBSCRIBE : 
            jinotechboard_forcesubscribe($jinotechboard->id, 0);
            redirect($returnto, get_string("everyonecannowchoose", "forum"), 1);
            break;
        case JINOTECHBOARD_FORCESUBSCRIBE : 
            jinotechboard_forcesubscribe($jinotechboard->id, 1);
            redirect($returnto, get_string("everyoneisnowsubscribed", "forum"), 1);
            break;
        case JINOTECHBOARD_INITIALSUBSCRIBE :
            jinotechboard_forcesubscribe($jinotechboard->id, 2);
            redirect($returnto, get_string("everyoneisnowsubscribed", "forum"), 1);
            break;
        case JINOTECHBOARD_DISALLOWSUBSCRIBE :
            jinotechboard_forcesubscribe($jinotechboard->id, 3);
            redirect($returnto, get_string("noonecansubscribenow", "forum"), 1);
            break;
        default:
            print_error(get_string('invalidforcesubscribe', 'forum'));
    }
}

if (jinotechboard_is_forcesubscribed($jinotechboard)) {
    redirect($returnto, get_string("everyoneisnowsubscribed", "forum"), 1);
}

$info->name  = fullname($user);
$info->jinotechboard = format_string($jinotechboard->name);

if (jinotechboard_is_subscribed($user->id, $jinotechboard->id)) {
    if (is_null($sesskey)) {    
        $PAGE->set_title($course->shortname);
        $PAGE->set_heading($course->fullname);
        echo $OUTPUT->header();
        echo $OUTPUT->confirm(get_string('confirmunsubscribe', 'forum', format_string($jinotechboard->name)),
                new moodle_url($PAGE->url, array('sesskey' => sesskey())), new moodle_url('/mod/jinotechboard/view.php', array('b' => $id)));
        echo $OUTPUT->footer();
        exit;
    }
    require_sesskey();
    if (jinotechboard_unsubscribe($user->id, $jinotechboard->id)) {
        add_to_log($course->id, "jinotechboard", "unsubscribe", "view.php?b=$jinotechboard->id", $jinotechboard->id, $cm->id);
        redirect($returnto, get_string("nownotsubscribed", "forum", $info), 1);
    } else {
        print_error('cannotunsubscribe', 'forum', $_SERVER["HTTP_REFERER"]);
    }

} else {  
    if ($jinotechboard->forcesubscribe == JINOTECHBOARD_DISALLOWSUBSCRIBE &&
                !has_capability('mod/jinotechboard:managesubscriptions', $context)) {
        print_error('disallowsubscribe', 'forum', $_SERVER["HTTP_REFERER"]);
    }
    if (!has_capability('mod/jinotechboard:viewcontent', $context)) {
        print_error('noviewdiscussionspermission', 'forum', $_SERVER["HTTP_REFERER"]);
    }
    if (is_null($sesskey)) {    
        $PAGE->set_title($course->shortname);
        $PAGE->set_heading($course->fullname);
        echo $OUTPUT->header();
        echo $OUTPUT->confirm(get_string('confirmsubscribe', 'forum', format_string($jinotechboard->name)),
                new moodle_url($PAGE->url, array('sesskey' => sesskey())), new moodle_url('/mod/jinotechboard/view.php', array('b' => $id)));
        echo $OUTPUT->footer();
        exit;
    }
    require_sesskey();
    jinotechboard_subscribe($user->id, $jinotechboard->id);
    add_to_log($course->id, "jinotechboard", "subscribe", "view.php?b=$jinotechboard->id", $jinotechboard->id, $cm->id);
    redirect($returnto, get_string("nowsubscribed", "forum", $info), 1);
}
