<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
 
$contentid = optional_param('contentId', 0, PARAM_INT);
$comment_value = optional_param('comment_value', "", PARAM_TEXT);

$content = $DB->get_record('jinotechboard_contents',array('id'=>$contentid));


if($content){
	$board = $DB->get_record('jinotechboard', array('id'=>$content->board));
        $course = $DB->get_record("course", array("id" => $board->course));
        $cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id);
        $context = context_module::instance($cm->id);
        
	$records = new stdClass();
	$records->course = $content->course;
	$records->board = $content->board;
	$records->jinotechboard_contents = $content->id;
	$records->userid = $USER->id;
	$records->username = fullname($USER);
	$records->comments = $comment_value;
	$records->timecreated = time();
	$records->timemodified = time();
	
	if($DB->insert_record('jinotechboard_comments',$records)){
            $DB->set_field('jinotechboard_contents', 'commentscount', ($content->commentscount+1), array('id'=>$content->id));

            $params = array(
                'objectid' => $records->id,
                'context' => $context,
                'other' => array(
                    'contentid' => $content->id,
                    'jinotechboardid' => $content->board,
                )
            );

            $event = \mod_jinotechboard\event\comment_created::create($params);
            $event->add_record_snapshot('jinotechboard_contents', $content);
            $event->add_record_snapshot('jinotechboard_comments', $records);
            $event->trigger();

            redirect($CFG->wwwroot."/mod/jinotechboard/content.php?b=".$content->board."&contentId=".$content->id."&boardform=1");
	} 
} 

