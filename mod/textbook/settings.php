<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * textbook module admin settings and defaults
 *
 * @package    mod_textbook
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once("$CFG->libdir/resourcelib.php");

    $displayoptions = resourcelib_get_displayoptions(array(textbookLIB_DISPLAY_AUTO,
                                                           textbookLIB_DISPLAY_EMBED,
                                                           textbookLIB_DISPLAY_FRAME,
                                                           textbookLIB_DISPLAY_DOWNLOAD,
                                                           textbookLIB_DISPLAY_OPEN,
                                                           textbookLIB_DISPLAY_NEW,
                                                           textbookLIB_DISPLAY_POPUP,
                                                          ));
    $defaultdisplayoptions = array(textbookLIB_DISPLAY_AUTO,
                                   textbookLIB_DISPLAY_EMBED,
                                   textbookLIB_DISPLAY_DOWNLOAD,
                                   textbookLIB_DISPLAY_OPEN,
                                   textbookLIB_DISPLAY_POPUP,
                                  );

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_configtext('textbook/framesize',
        get_string('framesize', 'textbook'), get_string('configframesize', 'textbook'), 130, PARAM_INT));
    $settings->add(new admin_setting_configmultiselect('textbook/displayoptions',
        get_string('displayoptions', 'textbook'), get_string('configdisplayoptions', 'textbook'),
        $defaultdisplayoptions, $displayoptions));

    //--- modedit defaults -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('textbookmodeditdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));

    $settings->add(new admin_setting_configcheckbox('textbook/printintro',
        get_string('printintro', 'textbook'), get_string('printintroexplain', 'textbook'), 1));
    $settings->add(new admin_setting_configselect('textbook/display',
        get_string('displayselect', 'textbook'), get_string('displayselectexplain', 'textbook'), textbookLIB_DISPLAY_AUTO,
        $displayoptions));
    $settings->add(new admin_setting_configcheckbox('textbook/showsize',
        get_string('showsize', 'textbook'), get_string('showsize_desc', 'textbook'), 0));
    $settings->add(new admin_setting_configcheckbox('textbook/showtype',
        get_string('showtype', 'textbook'), get_string('showtype_desc', 'textbook'), 0));
    $settings->add(new admin_setting_configcheckbox('textbook/showdate',
        get_string('showdate', 'textbook'), get_string('showdate_desc', 'textbook'), 0));
    $settings->add(new admin_setting_configtext('textbook/popupwidth',
        get_string('popupwidth', 'textbook'), get_string('popupwidthexplain', 'textbook'), 620, PARAM_INT, 7));
    $settings->add(new admin_setting_configtext('textbook/popupheight',
        get_string('popupheight', 'textbook'), get_string('popupheightexplain', 'textbook'), 450, PARAM_INT, 7));
    $options = array('0' => get_string('none'), '1' => get_string('allfiles'), '2' => get_string('htmlfilesonly'));
    $settings->add(new admin_setting_configselect('textbook/filterfiles',
        get_string('filterfiles', 'textbook'), get_string('filterfilesexplain', 'textbook'), 0, $options));
}
