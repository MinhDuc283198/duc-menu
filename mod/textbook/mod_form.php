<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * textbook configuration form
 *
 * @package    mod_textbook
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/textbook/locallib.php');
require_once($CFG->dirroot.'/mod/okmedia/locallib.php');
require_once($CFG->libdir.'/filelib.php');

class mod_textbook_mod_form extends moodleform_mod {
    function definition() {
        global $CFG, $DB, $OUTPUT, $PAGE, $COURSE;
        $mform =& $this->_form;
        $PAGE->requires->js_init_call('M.mod_okmedia.init_edit_form', array($CFG->wwwroot, $mform->_attributes['id']), false, okmedia_get_js_module());

        $config = get_config('textbook');
        $titlestatic = $this->current->titlestatic;
        if ($this->current->instance and $this->current->tobemigrated) {
            // textbook not migrated yet
            $textbook_old = $DB->get_record('textbook_old', array('oldid'=>$this->current->instance));
            $mform->addElement('static', 'warning', '', get_string('notmigrated', 'textbook', $textbook_old->type));
            $mform->addElement('cancel');
            $this->standard_hidden_coursemodule_elements();
            return;
        }
        
        //-------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));
        $mform->addElement('text', 'name', get_string('name'), array('size'=>'48'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $this->standard_intro_elements();
        $element = $mform->getElement('introeditor');
        $attributes = $element->getAttributes();
        $attributes['rows'] = 5;
        $element->setAttributes($attributes);
//        
//        $filemanager_options = array();
//        $filemanager_options['accepted_types'] = '*';
//        $filemanager_options['maxbytes'] = 0;
//        $filemanager_options['maxfiles'] = -1;
//        $filemanager_options['mainfile'] = true;
//
//        $mform->addElement('filemanager', 'files', get_string('selectfiles'), null, $filemanager_options);
//        
//        
        
        $mform->addElement('header', 'lcms_content_search', get_string('contentheader', 'okmedia'));
        $mform->setExpanded('lcms_content_search', true);

        $mform->addElement('static', 'titlestatic1', '<font color="#ed5565">'
                .get_string('content', 'okmedia').'</font>',
                '<input size="58" readonly="readonly" name="titlestatic"  type="text" value="'.$titlestatic.'"/> '
                . '<input id="mod_lcms_btn_select" type="button" value="' . get_string('findcontent', 'okmedia') . '" onclick="javascript:M.mod_okmedia.show('.$COURSE->id.',3);">');
        $mform->addElement('hidden', 'contents');
        $mform->addElement('hidden', 'titlestatic');
        
        
        
        // add legacy files flag only if used
        if (isset($this->current->legacyfiles) and $this->current->legacyfiles != textbookLIB_LEGACYFILES_NO) {
            $options = array(textbookLIB_LEGACYFILES_DONE   => get_string('legacyfilesdone', 'textbook'),
                             textbookLIB_LEGACYFILES_ACTIVE => get_string('legacyfilesactive', 'textbook'));
            $mform->addElement('select', 'legacyfiles', get_string('legacyfiles', 'textbook'), $options);
        }

        //-------------------------------------------------------
//        $mform->addElement('header', 'optionssection', get_string('appearance'));

        if ($this->current->instance) {
            $options = resourcelib_get_displayoptions(explode(',', $config->displayoptions), $this->current->display);
        } else {
            $options = resourcelib_get_displayoptions(explode(',', $config->displayoptions));
        }

        if (count($options) == 1) {
            $mform->addElement('hidden', 'display');
            $mform->setType('display', PARAM_INT);
            reset($options);
            $mform->setDefault('display', key($options));
        } else {
            $mform->addElement('select', 'display', get_string('displayselect', 'textbook'), $options);
            $mform->setDefault('display', $config->display);
            $mform->addHelpButton('display', 'displayselect', 'textbook');
        }

//        $mform->addElement('checkbox', 'showsize', get_string('showsize', 'textbook'));
//        $mform->setDefault('showsize', $config->showsize);
//        $mform->addHelpButton('showsize', 'showsize', 'textbook');
//        $mform->addElement('checkbox', 'showtype', get_string('showtype', 'textbook'));
//        $mform->setDefault('showtype', $config->showtype);
//        $mform->addHelpButton('showtype', 'showtype', 'textbook');
//        $mform->addElement('checkbox', 'showdate', get_string('showdate', 'textbook'));
//        $mform->setDefault('showdate', $config->showdate);
//        $mform->addHelpButton('showdate', 'showdate', 'textbook');

        if (array_key_exists(textbookLIB_DISPLAY_POPUP, $options)) {
            $mform->addElement('text', 'popupwidth', get_string('popupwidth', 'textbook'), array('size'=>3));
            if (count($options) > 1) {
                $mform->disabledIf('popupwidth', 'display', 'noteq', textbookLIB_DISPLAY_POPUP);
            }
            $mform->setType('popupwidth', PARAM_INT);
            $mform->setDefault('popupwidth', $config->popupwidth);
            $mform->setAdvanced('popupwidth', true);

            $mform->addElement('text', 'popupheight', get_string('popupheight', 'textbook'), array('size'=>3));
            if (count($options) > 1) {
                $mform->disabledIf('popupheight', 'display', 'noteq', textbookLIB_DISPLAY_POPUP);
            }
            $mform->setType('popupheight', PARAM_INT);
            $mform->setDefault('popupheight', $config->popupheight);
            $mform->setAdvanced('popupheight', true);
        }

        if (array_key_exists(textbookLIB_DISPLAY_AUTO, $options) or
          array_key_exists(textbookLIB_DISPLAY_EMBED, $options) or
          array_key_exists(textbookLIB_DISPLAY_FRAME, $options)) {
            $mform->addElement('checkbox', 'printintro', get_string('printintro', 'textbook'));
            $mform->disabledIf('printintro', 'display', 'eq', textbookLIB_DISPLAY_POPUP);
            $mform->disabledIf('printintro', 'display', 'eq', textbookLIB_DISPLAY_DOWNLOAD);
            $mform->disabledIf('printintro', 'display', 'eq', textbookLIB_DISPLAY_OPEN);
            $mform->disabledIf('printintro', 'display', 'eq', textbookLIB_DISPLAY_NEW);
            $mform->setDefault('printintro', $config->printintro);
        }

        $options = array('0' => get_string('none'), '1' => get_string('allfiles'), '2' => get_string('htmlfilesonly'));
//        $mform->addElement('select', 'filterfiles', get_string('filterfiles', 'textbook'), $options);
//        $mform->setDefault('filterfiles', $config->filterfiles);
//        $mform->setAdvanced('filterfiles', true);

        //-------------------------------------------------------
        $this->standard_coursemodule_elements();

        //-------------------------------------------------------
        $this->add_action_buttons();

        //-------------------------------------------------------
        $mform->addElement('hidden', 'revision');
        $mform->setType('revision', PARAM_INT);
        $mform->setDefault('revision', 1);
    }

    function data_preprocessing(&$default_values) {
        if ($this->current->instance and !$this->current->tobemigrated) {
            $draftitemid = file_get_submitted_draft_itemid('files');
            file_prepare_draft_area($draftitemid, $this->context->id, 'mod_textbook', 'content', 0, array('subdirs'=>true));
            $default_values['files'] = $draftitemid;
        }
        if (!empty($default_values['displayoptions'])) {
            $displayoptions = unserialize($default_values['displayoptions']);
            if (isset($displayoptions['printintro'])) {
                $default_values['printintro'] = $displayoptions['printintro'];
            }
            if (!empty($displayoptions['popupwidth'])) {
                $default_values['popupwidth'] = $displayoptions['popupwidth'];
            }
            if (!empty($displayoptions['popupheight'])) {
                $default_values['popupheight'] = $displayoptions['popupheight'];
            }
            if (!empty($displayoptions['showsize'])) {
                $default_values['showsize'] = $displayoptions['showsize'];
            } else {
                // Must set explicitly to 0 here otherwise it will use system
                // default which may be 1.
                $default_values['showsize'] = 0;
            }
            if (!empty($displayoptions['showtype'])) {
                $default_values['showtype'] = $displayoptions['showtype'];
            } else {
                $default_values['showtype'] = 0;
            }
            if (!empty($displayoptions['showdate'])) {
                $default_values['showdate'] = $displayoptions['showdate'];
            } else {
                $default_values['showdate'] = 0;
            }
        }
    }

    function definition_after_data() {
        if ($this->current->instance and $this->current->tobemigrated) {
            // textbook not migrated yet
            return;
        }

        parent::definition_after_data();
    }

    function validation($data, $files) {
        global $USER;

        $errors = parent::validation($data, $files);
        
        if(!$data['contents']){
            $errors['titlestatic'] = get_string('error:notselectfile','lcms');
        }
        $usercontext = context_user::instance($USER->id);
        $fs = get_file_storage();
        
        if (count($files) == 1) {
            // no need to select main file if only one picked
            return $errors;
        } else if(count($files) > 1) {
            $mainfile = false;
            foreach($files as $file) {
                if ($file->get_sortorder() == 1) {
                    $mainfile = true;
                    break;
                }
            }
            // set a default main file
            if (!$mainfile) {
                $file = reset($files);
                file_set_sortorder($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(),
                                   $file->get_filepath(), $file->get_filename(), 1);
            }
        }
        return $errors;
    }
}
