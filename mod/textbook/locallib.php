<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private textbook module utility functions
 *
 * @package    mod_textbook
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/filelib.php");
require_once("$CFG->libdir/resourcelib.php");
require_once("$CFG->dirroot/mod/textbook/lib.php");

/**
 * Redirected to migrated textbook if needed,
 * return if incorrect parameters specified
 * @param int $oldid
 * @param int $cmid
 * @return void
 */
function textbook_redirect_if_migrated($oldid, $cmid) {
    global $DB, $CFG;

    if ($oldid) {
        $old = $DB->get_record('textbook_old', array('oldid'=>$oldid));
    } else {
        $old = $DB->get_record('textbook_old', array('cmid'=>$cmid));
    }

    if (!$old) {
        return;
    }

    redirect("$CFG->wwwroot/mod/$old->newmodule/view.php?id=".$old->cmid);
}

/**
 * Display embedded textbook file.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @param stored_file $file main file
 * @return does not return
 */
function textbook_display_embed($textbook, $cm, $course, $file) {
    global $CFG, $PAGE, $OUTPUT;

    $clicktoopen = textbook_get_clicktoopen($file, $textbook->revision);

    $context = context_module::instance($cm->id);
    $path = '/'.$context->id.'/mod_textbook/content/'.$textbook->revision.$file->get_filepath().$file->get_filename();
    $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
    $moodleurl = new moodle_url('/pluginfile.php' . $path);

    $mimetype = $file->get_mimetype();
    $title    = $textbook->name;

    $extension = textbooklib_get_extension($file->get_filename());

    $mediarenderer = $PAGE->get_renderer('core', 'media');
    $embedoptions = array(
        core_media::OPTION_TRUSTED => true,
        core_media::OPTION_BLOCK => true,
    );

    if (file_mimetype_in_typegroup($mimetype, 'web_image')) {  // It's an image
        $code = textbooklib_embed_image($fullurl, $title);

    } else if ($mimetype === 'application/pdf') {
        // PDF document
        $code = textbooklib_embed_pdf($fullurl, $title, $clicktoopen);

    } else if ($mediarenderer->can_embed_url($moodleurl, $embedoptions)) {
        // Media (audio/video) file.
        $code = $mediarenderer->embed_url($moodleurl, $title, 0, 0, $embedoptions);

    } else {
        // anything else - just try object tag enlarged as much as possible
        $code = textbooklib_embed_general($fullurl, $title, $clicktoopen, $mimetype);
    }

    textbook_print_header($textbook, $cm, $course);
    textbook_print_heading($textbook, $cm, $course);

    echo $code;

    textbook_print_intro($textbook, $cm, $course);

    echo $OUTPUT->footer();
    die;
}

/**
 * Display textbook frames.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @param stored_file $file main file
 * @return does not return
 */
function textbook_display_frame($textbook, $cm, $course, $file) {
    global $PAGE, $OUTPUT, $CFG;

    $frame = optional_param('frameset', 'main', PARAM_ALPHA);

    if ($frame === 'top') {
        $PAGE->set_pagelayout('frametop');
        textbook_print_header($textbook, $cm, $course);
        textbook_print_heading($textbook, $cm, $course);
        textbook_print_intro($textbook, $cm, $course);
        echo $OUTPUT->footer();
        die;

    } else {
        $config = get_config('textbook');
        $context = context_module::instance($cm->id);
        $path = '/'.$context->id.'/mod_textbook/content/'.$textbook->revision.$file->get_filepath().$file->get_filename();
        $fileurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
        $navurl = "$CFG->wwwroot/mod/textbook/view.php?id=$cm->id&amp;frameset=top";
        $title = strip_tags(format_string($course->shortname.': '.$textbook->name));
        $framesize = $config->framesize;
        $contentframetitle = s(format_string($textbook->name));
        $modulename = s(get_string('modulename','textbook'));
        $dir = get_string('thisdirection', 'langconfig');

        $file = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html dir="$dir">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>$title</title>
  </head>
  <frameset rows="$framesize,*">
    <frame src="$navurl" title="$modulename" />
    <frame src="$fileurl" title="$contentframetitle" />
  </frameset>
</html>
EOF;

        @header('Content-Type: text/html; charset=utf-8');
        echo $file;
        die;
    }
}

/**
 * Internal function - create click to open text with link.
 */
function textbook_get_clicktoopen($file, $revision, $extra='') {
    global $CFG;

    $filename = $file->get_filename();
    $path = '/'.$file->get_contextid().'/mod_textbook/content/'.$revision.$file->get_filepath().$file->get_filename();
    $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);

    $string = get_string('clicktoopen2', 'textbook', "<a href=\"$fullurl\" $extra>$filename</a>");

    return $string;
}

/**
 * Internal function - create click to open text with link.
 */
function textbook_get_clicktodownload($file, $revision) {
    global $CFG;

    $filename = $file->get_filename();
    $path = '/'.$file->get_contextid().'/mod_textbook/content/'.$revision.$file->get_filepath().$file->get_filename();
    $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, true);

    $string = get_string('clicktodownload', 'textbook', "<a href=\"$fullurl\">$filename</a>");

    return $string;
}

/**
 * Print textbook info and workaround link when JS not available.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @param stored_file $file main file
 * @return does not return
 */
function textbook_print_workaround($textbook, $cm, $course, $file) {
    global $CFG, $OUTPUT;

    textbook_print_header($textbook, $cm, $course);
    textbook_print_heading($textbook, $cm, $course, true);
    textbook_print_intro($textbook, $cm, $course, true);

    $textbook->mainfile = $file->get_filename();
    echo '<div class="textbookworkaround">';
    switch (textbook_get_final_display_type($textbook)) {
        case textbookLIB_DISPLAY_POPUP:
            $path = '/'.$file->get_contextid().'/mod_textbook/content/'.$textbook->revision.$file->get_filepath().$file->get_filename();
            $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
            $options = empty($textbook->displayoptions) ? array() : unserialize($textbook->displayoptions);
            $width  = empty($options['popupwidth'])  ? 620 : $options['popupwidth'];
            $height = empty($options['popupheight']) ? 450 : $options['popupheight'];
            $wh = "width=$width,height=$height,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes";
            $extra = "onclick=\"window.open('$fullurl', '', '$wh'); return false;\"";
            echo textbook_get_clicktoopen($file, $textbook->revision, $extra);
            break;

        case textbookLIB_DISPLAY_NEW:
            $extra = 'onclick="this.target=\'_blank\'"';
            echo textbook_get_clicktoopen($file, $textbook->revision, $extra);
            break;

        case textbookLIB_DISPLAY_DOWNLOAD:
            echo textbook_get_clicktodownload($file, $textbook->revision);
            break;

        case textbookLIB_DISPLAY_OPEN:
        default:
            echo textbook_get_clicktoopen($file, $textbook->revision);
            break;
    }
    echo '</div>';

    echo $OUTPUT->footer();
    die;
}

/**
 * Print textbook header.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @return void
 */
function textbook_print_header($textbook, $cm, $course) {
    global $PAGE, $OUTPUT;

    $PAGE->set_title($course->shortname.': '.$textbook->name);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($textbook);
    $PAGE->set_button(update_module_button($cm->id, '', get_string('modulename', 'textbook')));
    echo $OUTPUT->header();
}

/**
 * Print textbook heading.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @param bool $notused This variable is no longer used
 * @return void
 */
function textbook_print_heading($textbook, $cm, $course, $notused = false) {
    global $OUTPUT;
    echo $OUTPUT->heading(format_string($textbook->name), 2);
}


/**
 * Gets details of the file to cache in course cache to be displayed using {@link textbook_get_optional_details()}
 *
 * @param object $textbook textbook table row (only property 'displayoptions' is used here)
 * @param object $cm Course-module table row
 * @return string Size and type or empty string if show options are not enabled
 */
function textbook_get_file_details($textbook, $cm) {
    $options = empty($textbook->displayoptions) ? array() : @unserialize($textbook->displayoptions);
    $filedetails = array();
    if (!empty($options['showsize']) || !empty($options['showtype']) || !empty($options['showdate'])) {
        $context = context_module::instance($cm->id);
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'mod_textbook', 'content', 0, 'sortorder DESC, id ASC', false);
        // For a typical file textbook, the sortorder is 1 for the main file
        // and 0 for all other files. This sort approach is used just in case
        // there are situations where the file has a different sort order.
        $mainfile = $files ? reset($files) : null;
        if (!empty($options['showsize'])) {
            $filedetails['size'] = 0;
            foreach ($files as $file) {
                // This will also synchronize the file size for external files if needed.
                $filedetails['size'] += $file->get_filesize();
                if ($file->get_repository_id()) {
                    // If file is a reference the 'size' attribute can not be cached.
                    $filedetails['isref'] = true;
                }
            }
        }
        if (!empty($options['showtype'])) {
            if ($mainfile) {
                $filedetails['type'] = get_mimetype_description($mainfile);
                // Only show type if it is not unknown.
                if ($filedetails['type'] === get_mimetype_description('document/unknown')) {
                    $filedetails['type'] = '';
                }
            } else {
                $filedetails['type'] = '';
            }
        }
        if (!empty($options['showdate'])) {
            if ($mainfile) {
                // Modified date may be up to several minutes later than uploaded date just because
                // teacher did not submit the form promptly. Give teacher up to 5 minutes to do it.
                if ($mainfile->get_timemodified() > $mainfile->get_timecreated() + 5 * MINSECS) {
                    $filedetails['modifieddate'] = $mainfile->get_timemodified();
                } else {
                    $filedetails['uploadeddate'] = $mainfile->get_timecreated();
                }
                if ($mainfile->get_repository_id()) {
                    // If main file is a reference the 'date' attribute can not be cached.
                    $filedetails['isref'] = true;
                }
            } else {
                $filedetails['uploadeddate'] = '';
            }
        }
    }
    return $filedetails;
}

/**
 * Gets optional details for a textbook, depending on textbook settings.
 *
 * Result may include the file size and type if those settings are chosen,
 * or blank if none.
 *
 * @param object $textbook textbook table row (only property 'displayoptions' is used here)
 * @param object $cm Course-module table row
 * @return string Size and type or empty string if show options are not enabled
 */
function textbook_get_optional_details($textbook, $cm) {
    global $DB;

    $details = '';

    $options = empty($textbook->displayoptions) ? array() : @unserialize($textbook->displayoptions);
    if (!empty($options['showsize']) || !empty($options['showtype']) || !empty($options['showdate'])) {
        if (!array_key_exists('filedetails', $options)) {
            $filedetails = textbook_get_file_details($textbook, $cm);
        } else {
            $filedetails = $options['filedetails'];
        }
        $size = '';
        $type = '';
        $date = '';
        $langstring = '';
        $infodisplayed = 0;
        if (!empty($options['showsize'])) {
            if (!empty($filedetails['size'])) {
                $size = display_size($filedetails['size']);
                $langstring .= 'size';
                $infodisplayed += 1;
            }
        }
        if (!empty($options['showtype'])) {
            if (!empty($filedetails['type'])) {
                $type = $filedetails['type'];
                $langstring .= 'type';
                $infodisplayed += 1;
            }
        }
        if (!empty($options['showdate']) && (!empty($filedetails['modifieddate']) || !empty($filedetails['uploadeddate']))) {
            if (!empty($filedetails['modifieddate'])) {
                $date = get_string('modifieddate', 'mod_textbook', userdate($filedetails['modifieddate'],
                    get_string('strftimedatetimeshort', 'langconfig')));
            } else if (!empty($filedetails['uploadeddate'])) {
                $date = get_string('uploadeddate', 'mod_textbook', userdate($filedetails['uploadeddate'],
                    get_string('strftimedatetimeshort', 'langconfig')));
            }
            $langstring .= 'date';
            $infodisplayed += 1;
        }

        if ($infodisplayed > 1) {
            $details = get_string("textbookdetails_{$langstring}", 'textbook',
                    (object)array('size' => $size, 'type' => $type, 'date' => $date));
        } else {
            // Only one of size, type and date is set, so just append.
            $details = $size . $type . $date;
        }
    }

    return $details;
}

/**
 * Print textbook introduction.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @param bool $ignoresettings print even if not specified in modedit
 * @return void
 */
function textbook_print_intro($textbook, $cm, $course, $ignoresettings=false) {
    global $OUTPUT;

    $options = empty($textbook->displayoptions) ? array() : unserialize($textbook->displayoptions);

    $extraintro = textbook_get_optional_details($textbook, $cm);
    if ($extraintro) {
        // Put a paragaph tag around the details
        $extraintro = html_writer::tag('p', $extraintro, array('class' => 'textbookdetails'));
    }

    if ($ignoresettings || !empty($options['printintro']) || $extraintro) {
        $gotintro = trim(strip_tags($textbook->intro));
        if ($gotintro || $extraintro) {
            echo $OUTPUT->box_start('mod_introbox', 'textbookintro');
            if ($gotintro) {
                echo format_module_intro('textbook', $textbook, $cm->id);
            }
            echo $extraintro;
            echo $OUTPUT->box_end();
        }
    }
}

/**
 * Print warning that instance not migrated yet.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @return void, does not return
 */
function textbook_print_tobemigrated($textbook, $cm, $course) {
    global $DB, $OUTPUT;

    $textbook_old = $DB->get_record('textbook_old', array('oldid'=>$textbook->id));
    textbook_print_header($textbook, $cm, $course);
    textbook_print_heading($textbook, $cm, $course);
    textbook_print_intro($textbook, $cm, $course);
    echo $OUTPUT->notification(get_string('notmigrated', 'textbook', $textbook_old->type));
    echo $OUTPUT->footer();
    die;
}

/**
 * Print warning that file can not be found.
 * @param object $textbook
 * @param object $cm
 * @param object $course
 * @return void, does not return
 */
function textbook_print_filenotfound($textbook, $cm, $course) {
    global $DB, $OUTPUT;

    $textbook_old = $DB->get_record('textbook_old', array('oldid'=>$textbook->id));
    textbook_print_header($textbook, $cm, $course);
    textbook_print_heading($textbook, $cm, $course);
    textbook_print_intro($textbook, $cm, $course);
    if ($textbook_old) {
        echo $OUTPUT->notification(get_string('notmigrated', 'textbook', $textbook_old->type));
    } else {
        echo $OUTPUT->notification(get_string('filenotfound', 'textbook'));
    }
    echo $OUTPUT->footer();
    die;
}

/**
 * Decide the best display format.
 * @param object $textbook
 * @return int display type constant
 */
function textbook_get_final_display_type($textbook) {
    global $CFG, $PAGE;

    if ($textbook->display != textbookLIB_DISPLAY_AUTO) {
        return $textbook->display;
    }

    if (empty($textbook->mainfile)) {
        return textbookLIB_DISPLAY_DOWNLOAD;
    } else {
        $mimetype = mimeinfo('type', $textbook->mainfile);
    }

    if (file_mimetype_in_typegroup($mimetype, 'archive')) {
        return textbookLIB_DISPLAY_DOWNLOAD;
    }
    if (file_mimetype_in_typegroup($mimetype, array('web_image', '.htm', 'web_video', 'web_audio'))) {
        return textbookLIB_DISPLAY_EMBED;
    }

    // let the browser deal with it somehow
    return textbookLIB_DISPLAY_OPEN;
}

/**
 * File browsing support class
 */
class textbook_content_file_info extends file_info_stored {
    public function get_parent() {
        if ($this->lf->get_filepath() === '/' and $this->lf->get_filename() === '.') {
            return $this->browser->get_file_info($this->context);
        }
        return parent::get_parent();
    }
    public function get_visible_name() {
        if ($this->lf->get_filepath() === '/' and $this->lf->get_filename() === '.') {
            return $this->topvisiblename;
        }
        return parent::get_visible_name();
    }
}

function textbook_set_mainfile($data) {
    global $DB;
    $fs = get_file_storage();
    $cmid = $data->coursemodule;
    $draftitemid = $data->files;
    
    $context = context_module::instance($cmid);

    // Get file
    $files = $fs->get_area_files($context->id, 'mod_textbook','content',0);
    // Delete it if it exists
    if ($files) {
        foreach($files as $file){
            $file->delete();
        }
    }
    // Copy the files from the source area.
    if ($files = $fs->get_area_files(1, 'local_lmsdata', 'tplan', $data->contents, 'sortorder, id', false)) {
//    if ($files = $fs->get_area_files(1, 'local_lmsdata', 'banner', 25, 'sortorder, id', false)) {
        foreach ($files as $file) {
            $newrecord = new \stdClass();
            $newrecord->contextid = $context->id;
            $newrecord->component = "mod_textbook";
            $newrecord->filearea = "content";
            $newrecord->itemid = 0;
            $newrecord->referencefileid = $data->contents;
            $aa = $fs->create_file_from_storedfile($newrecord, $file);
        }
    }
    
//    if ($draftitemid) {
//        file_save_draft_area_files($draftitemid, $context->id, 'mod_textbook', 'content', 0, array('subdirs'=>true));
//    }
//    $files = $fs->get_area_files($context->id, 'mod_textbook', 'content', 0, 'sortorder', false);
//    if (count($files) == 1) {
//        // only one file attached, set it as main file automatically
//        $file = reset($files);
//        file_set_sortorder($context->id, 'mod_textbook', 'content', 0, $file->get_filepath(), $file->get_filename(), 1);
//    }
}
