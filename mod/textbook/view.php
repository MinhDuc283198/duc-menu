<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * textbook module version information
 *
 * @package    mod_textbook
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/textbook/lib.php');
require_once($CFG->dirroot.'/mod/textbook/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id       = optional_param('id', 0, PARAM_INT); // Course Module ID
$r        = optional_param('r', 0, PARAM_INT);  // textbook instance ID
$redirect = optional_param('redirect', 0, PARAM_BOOL);

if ($r) {
    if (!$textbook = $DB->get_record('textbook', array('id'=>$r))) {
        textbook_redirect_if_migrated($r, 0);
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('textbook', $textbook->id, $textbook->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('textbook', $id)) {
        textbook_redirect_if_migrated(0, $id);
        print_error('invalidcoursemodule');
    }
    $textbook = $DB->get_record('textbook', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/textbook:view', $context);

// Completion and trigger events.
textbook_view($textbook, $course, $cm, $context);

$PAGE->set_url('/mod/textbook/view.php', array('id' => $cm->id));

if ($textbook->tobemigrated) {
    textbook_print_tobemigrated($textbook, $cm, $course);
    die;
}

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_textbook', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
if (count($files) < 1) {
    textbook_print_filenotfound($textbook, $cm, $course);
    die;
} else {
    $file = reset($files);
    unset($files);
}

$textbook->mainfile = $file->get_filename();
$displaytype = textbook_get_final_display_type($textbook);
if ($displaytype == textbookLIB_DISPLAY_OPEN || $displaytype == textbookLIB_DISPLAY_DOWNLOAD) {
    // For 'open' and 'download' links, we always redirect to the content - except
    // if the user just chose 'save and display' from the form then that would be
    // confusing
    if (strpos(get_local_referer(false), 'modedit.php') === false) {
        $redirect = true;
    }
}

// Don't redirect teachers, otherwise they can not access course or module settings.
if ($redirect && !course_get_format($course)->has_view_page() &&
        (has_capability('moodle/course:manageactivities', $context) ||
        has_capability('moodle/course:update', context_course::instance($course->id)))) {
    $redirect = false;
}

if ($redirect) {
    // coming from course page or url index page
    // this redirect trick solves caching problems when tracking views ;-)
    $path = '/'.$context->id.'/mod_textbook/content/'.$textbook->revision.$file->get_filepath().$file->get_filename();
    $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == textbookLIB_DISPLAY_DOWNLOAD);
    redirect($fullurl);
}

switch ($displaytype) {
    case textbookLIB_DISPLAY_EMBED:
        textbook_display_embed($textbook, $cm, $course, $file);
        break;
    case textbookLIB_DISPLAY_FRAME:
        textbook_display_frame($textbook, $cm, $course, $file);
        break;
    default:
        textbook_print_workaround($textbook, $cm, $course, $file);
        break;
}

