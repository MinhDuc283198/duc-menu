<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit and save a new post to a discussion
 *
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$reply = optional_param('reply', 0, PARAM_INT);
$jinoforum = optional_param('jinoforum', 0, PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);
$delete = optional_param('delete', 0, PARAM_INT);
$prune = optional_param('prune', 0, PARAM_INT);
$name = optional_param('name', '', PARAM_CLEAN);
$confirm = optional_param('confirm', 0, PARAM_INT);
$groupid = optional_param('groupid', null, PARAM_INT);

$PAGE->set_url('/mod/jinoforum/post.php', array(
    'reply' => $reply,
    'jinoforum' => $jinoforum,
    'edit' => $edit,
    'delete' => $delete,
    'prune' => $prune,
    'name' => $name,
    'confirm' => $confirm,
    'groupid' => $groupid,
));
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
//these page_params will be passed as hidden variables later in the form.
$page_params = array('reply' => $reply, 'jinoforum' => $jinoforum, 'edit' => $edit);

$sitecontext = context_system::instance();

if (!isloggedin() or isguestuser()) {

    if (!isloggedin() and ! get_referer()) {
        // No referer+not logged in - probably coming in via email  See MDL-9052
        require_login();
    }

    if (!empty($jinoforum)) {   // User is starting a new discussion in a jinoforum
        if (!$jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum))) {
            print_error('invalidjinoforumid', 'jinoforum');
        }
    } else if (!empty($reply)) {   // User is writing a new reply
        if (!$parent = jinoforum_get_post_full($reply)) {
            print_error('invalidparentpostid', 'jinoforum');
        }
        if (!$discussion = $DB->get_record('jinoforum_discussions', array('id' => $parent->discussion))) {
            print_error('notpartofdiscussion', 'jinoforum');
        }
        if (!$jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum))) {
            print_error('invalidjinoforumid');
        }
    }
    if (!$course = $DB->get_record('course', array('id' => $jinoforum->course))) {
        print_error('invalidcourseid');
    }

    if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id)) { // For the logs
        print_error('invalidcoursemodule');
    } else {
        $modcontext = context_module::instance($cm->id);
    }
    
    $PAGE->set_cm($cm, $course, $jinoforum);
    $PAGE->set_context($modcontext);
    $PAGE->set_title($course->shortname);
    $PAGE->set_heading($course->fullname);

    echo $OUTPUT->header();
    echo $OUTPUT->confirm(get_string('noguestpost', 'jinoforum') . '<br /><br />' . get_string('liketologin'), get_login_url(), get_referer(false));
    echo $OUTPUT->footer();
    exit;
}

require_login(0, false);   // Script is useless unless they're logged in

if (!empty($jinoforum)) {   // User is starting a new discussion in a jinoforum
    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $jinoforum))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if (!$course = $DB->get_record("course", array("id" => $jinoforum->course))) {
        print_error('invalidcourseid');
    }
    if (!$cm = get_coursemodule_from_instance("jinoforum", $jinoforum->id, $course->id)) {
        print_error("invalidcoursemodule");
    }

    $coursecontext = context_course::instance($course->id);

    if (!jinoforum_user_can_post_discussion($jinoforum, $groupid, -1, $cm)) {
        if (!isguestuser()) {
            if (!is_enrolled($coursecontext)) {
                if (enrol_selfenrol_available($course->id)) {
                    $SESSION->wantsurl = qualified_me();
                    $SESSION->enrolcancel = $_SERVER['HTTP_REFERER'];
                    redirect($CFG->wwwroot . '/enrol/index.php?id=' . $course->id, get_string('youneedtoenrol'));
                }
            }
        }
        print_error('nopostjinoforum', 'jinoforum');
    }

    if (!$cm->visible and ! has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
        print_error("activityiscurrentlyhidden");
    }

    if (isset($_SERVER["HTTP_REFERER"])) {
        $SESSION->fromurl = $_SERVER["HTTP_REFERER"];
    } else {
        $SESSION->fromurl = '';
    }


    // Load up the $post variable.

    $post = new stdClass();
    $post->course = $course->id;
    $post->jinoforum = $jinoforum->id;
    $post->discussion = 0;  // ie discussion # not defined yet
    $post->parent = 0;
    $post->subject = '';
    $post->userid = $USER->id;
    $post->message = '';
    $post->messageformat = editors_get_preferred_format();
    $post->messagetrust = 0;

    if (isset($groupid)) {
        $post->groupid = $groupid;
    } else {
        $post->groupid = groups_get_activity_group($cm);
    }

    // Unsetting this will allow the correct return URL to be calculated later.
    unset($SESSION->fromdiscussion);
} else if (!empty($reply)) {   // User is writing a new reply
    if (!$parent = jinoforum_get_post_full($reply)) {
        print_error('invalidparentpostid', 'jinoforum');
    }
    if (!$discussion = $DB->get_record("jinoforum_discussions", array("id" => $parent->discussion))) {
        print_error('notpartofdiscussion', 'jinoforum');
    }
    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $discussion->jinoforum))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if (!$course = $DB->get_record("course", array("id" => $discussion->course))) {
        print_error('invalidcourseid');
    }
    if (!$cm = get_coursemodule_from_instance("jinoforum", $jinoforum->id, $course->id)) {
        print_error('invalidcoursemodule');
    }

    // Ensure lang, theme, etc. is set up properly. MDL-6926
    $PAGE->set_cm($cm, $course, $jinoforum);

    $coursecontext = context_course::instance($course->id);
    $modcontext = context_module::instance($cm->id);

    if (!jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $modcontext)) {
        if (!isguestuser()) {
            if (!is_enrolled($coursecontext)) {  // User is a guest here!
                $SESSION->wantsurl = qualified_me();
                $SESSION->enrolcancel = $_SERVER['HTTP_REFERER'];
                redirect($CFG->wwwroot . '/enrol/index.php?id=' . $course->id, get_string('youneedtoenrol'));
            }
        }
        print_error('nopostjinoforum', 'jinoforum');
    }

    // Make sure user can post here
    if (isset($cm->groupmode) && empty($course->groupmodeforce)) {
        $groupmode = $cm->groupmode;
    } else {
        $groupmode = $course->groupmode;
    }
    if ($groupmode == SEPARATEGROUPS and ! has_capability('moodle/site:accessallgroups', $modcontext)) {
        if ($discussion->groupid == -1) {
            print_error('nopostjinoforum', 'jinoforum');
        } else {
            if (!groups_is_member($discussion->groupid)) {
                print_error('nopostjinoforum', 'jinoforum');
            }
        }
    }

    if (!$cm->visible and ! has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
        print_error("activityiscurrentlyhidden");
    }

    // Load up the $post variable.

    $post = new stdClass();
    $post->course = $course->id;
    $post->jinoforum = $jinoforum->id;
    $post->discussion = $parent->discussion;
    $post->parent = $parent->id;
    $post->subject = $parent->subject;
    $post->userid = $USER->id;
    $post->message = '';

    $post->groupid = ($discussion->groupid == -1) ? 0 : $discussion->groupid;

    $strre = get_string('re', 'jinoforum');
    if (!(substr($post->subject, 0, strlen($strre)) == $strre)) {
        $post->subject = $strre . ' ' . $post->subject;
    }

    // Unsetting this will allow the correct return URL to be calculated later.
    unset($SESSION->fromdiscussion);
} else if (!empty($edit)) {  // User is editing their own post
    if (!$post = jinoforum_get_post_full($edit)) {
        print_error('invalidpostid', 'jinoforum');
    }
    if ($post->parent) {
        if (!$parent = jinoforum_get_post_full($post->parent)) {
            print_error('invalidparentpostid', 'jinoforum');
        }
    }

    if (!$discussion = $DB->get_record("jinoforum_discussions", array("id" => $post->discussion))) {
        print_error('notpartofdiscussion', 'jinoforum');
    }
    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $discussion->jinoforum))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if (!$course = $DB->get_record("course", array("id" => $discussion->course))) {
        print_error('invalidcourseid');
    }
    if (!$cm = get_coursemodule_from_instance("jinoforum", $jinoforum->id, $course->id)) {
        print_error('invalidcoursemodule');
    } else {
        $modcontext = context_module::instance($cm->id);
    }

    $PAGE->set_cm($cm, $course, $jinoforum);
    /*
      if (!($jinoforum->type == 'news' && !$post->parent && $discussion->timestart > time())) {
      if (((time() - $post->created) > $CFG->maxeditingtime) and
      !has_capability('mod/jinoforum:editanypost', $modcontext)) {
      print_error('maxtimehaspassed', 'jinoforum', '', format_time($CFG->maxeditingtime));
      }
      }
     */
    if (($post->userid <> $USER->id) and ! has_capability('mod/jinoforum:editanypost', $modcontext)) {
        print_error('cannoteditposts', 'jinoforum');
    }


    // Load up the $post variable.
    $post->edit = $edit;
    $post->course = $course->id;
    $post->jinoforum = $jinoforum->id;
    $post->groupid = ($discussion->groupid == -1) ? 0 : $discussion->groupid;

    $post = trusttext_pre_edit($post, 'message', $modcontext);

    // Unsetting this will allow the correct return URL to be calculated later.
    unset($SESSION->fromdiscussion);
} else if (!empty($delete)) {  // User is deleting a post
    if (!$post = jinoforum_get_post_full($delete)) {
        print_error('invalidpostid', 'jinoforum');
    }
    if (!$discussion = $DB->get_record("jinoforum_discussions", array("id" => $post->discussion))) {
        print_error('notpartofdiscussion', 'jinoforum');
    }
    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $discussion->jinoforum))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if (!$cm = get_coursemodule_from_instance("jinoforum", $jinoforum->id, $jinoforum->course)) {
        print_error('invalidcoursemodule');
    }
    if (!$course = $DB->get_record('course', array('id' => $jinoforum->course))) {
        print_error('invalidcourseid');
    }

    require_login($course, false, $cm);
    $modcontext = context_module::instance($cm->id);

    if (!(($post->userid == $USER->id && has_capability('mod/jinoforum:deleteownpost', $modcontext)) || has_capability('mod/jinoforum:deleteanypost', $modcontext))) {
        print_error('cannotdeletepost', 'jinoforum');
    }


    $replycount = jinoforum_count_replies($post);

    if (!empty($confirm) && confirm_sesskey()) { // User has confirmed the delete
        //check user capability to delete post.
        $timepassed = time() - $post->created;
        if (!has_capability('mod/jinoforum:deleteanypost', $modcontext)) {
            print_error("cannotdeletepost", "jinoforum", jinoforum_go_back_to("discuss.php?d=$post->discussion"));
        }

        if ($post->totalscore) {
            notice(get_string('couldnotdeleteratings', 'rating'), jinoforum_go_back_to("discuss.php?d=$post->discussion"));
        } else if ($replycount && !has_capability('mod/jinoforum:deleteanypost', $modcontext)) {
            print_error("couldnotdeletereplies", "jinoforum", jinoforum_go_back_to("discuss.php?d=$post->discussion"));
        } else {
            if (!$post->parent) {  // post is a discussion topic as well, so delete discussion
                if ($jinoforum->type == 'single') {
                    notice("Sorry, but you are not allowed to delete that discussion!", jinoforum_go_back_to("discuss.php?d=$post->discussion"));
                }
                jinoforum_delete_discussion($discussion, false, $course, $cm, $jinoforum);

                $params = array(
                    'objectid' => $discussion->id,
                    'context' => $modcontext,
                    'other' => array(
                        'jinoforumid' => $jinoforum->id,
                    )
                );

                $event = \mod_jinoforum\event\discussion_deleted::create($params);
                $event->add_record_snapshot('jinoforum_discussions', $discussion);
                $event->trigger();

                redirect("view.php?f=$discussion->jinoforum");
            } else if (jinoforum_delete_post($post, has_capability('mod/jinoforum:deleteanypost', $modcontext), $course, $cm, $jinoforum)) {

                if ($jinoforum->type == 'single') {
                    // Single discussion jinoforums are an exception. We show
                    // the jinoforum itself since it only has one discussion
                    // thread.
                    $discussionurl = "view.php?f=$jinoforum->id";
                } else {
                    $discussionurl = "discuss.php?d=$post->discussion";
                }

                $params = array(
                    'context' => $modcontext,
                    'objectid' => $post->id,
                    'other' => array(
                        'discussionid' => $discussion->id,
                        'jinoforumid' => $jinoforum->id,
                        'jinoforumtype' => $jinoforum->type,
                    )
                );

                if ($post->userid !== $USER->id) {
                    $params['relateduserid'] = $post->userid;
                }
                $event = \mod_jinoforum\event\post_deleted::create($params);
                $event->add_record_snapshot('jinoforum_posts', $post);
                $event->add_record_snapshot('jinoforum_discussions', $discussion);
                $event->trigger();

                redirect(jinoforum_go_back_to($discussionurl));
            } else {
                print_error('errorwhiledelete', 'jinoforum');
            }
        }
    } else { // User just asked to delete something
        jinoforum_set_return();
        $PAGE->navbar->add(get_string('delete', 'jinoforum'));
        $PAGE->set_title($course->shortname);
        $PAGE->set_heading($course->fullname);

        if ($replycount) {
            if (!has_capability('mod/jinoforum:deleteanypost', $modcontext)) {
                print_error("couldnotdeletereplies", "jinoforum", jinoforum_go_back_to("discuss.php?d=$post->discussion"));
            }
            echo $OUTPUT->header();
            echo $OUTPUT->heading(format_string($jinoforum->name), 2);
            echo $OUTPUT->confirm(get_string("deletesureplural", "jinoforum", $replycount + 1), "post.php?delete=$delete&confirm=$delete", $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '#p' . $post->id);

            jinoforum_print_post($post, $discussion, $jinoforum, $cm, $course, false, false, false);

            if (empty($post->edit)) {
                $jinoforumtracked = jinoforum_tp_is_tracked($jinoforum);
                $posts = jinoforum_get_all_discussion_posts($discussion->id, "created ASC", $jinoforumtracked);
                jinoforum_print_posts_nested($course, $cm, $jinoforum, $discussion, $post, false, false, $jinoforumtracked, $posts);
            }
        } else {
            echo $OUTPUT->header();
            echo $OUTPUT->heading(format_string($jinoforum->name), 2);
            echo $OUTPUT->confirm(get_string("deletesure", "jinoforum", $replycount), "post.php?delete=$delete&confirm=$delete", $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '#p' . $post->id);
            jinoforum_print_post($post, $discussion, $jinoforum, $cm, $course, false, false, false);
        }
    }
    echo $OUTPUT->footer();
    die;
} else if (!empty($prune)) {  // Pruning
    if (!$post = jinoforum_get_post_full($prune)) {
        print_error('invalidpostid', 'jinoforum');
    }
    if (!$discussion = $DB->get_record("jinoforum_discussions", array("id" => $post->discussion))) {
        print_error('notpartofdiscussion', 'jinoforum');
    }
    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $discussion->jinoforum))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if ($jinoforum->type == 'single') {
        print_error('cannotsplit', 'jinoforum');
    }
    if (!$post->parent) {
        print_error('alreadyfirstpost', 'jinoforum');
    }
    if (!$cm = get_coursemodule_from_instance("jinoforum", $jinoforum->id, $jinoforum->course)) { // For the logs
        print_error('invalidcoursemodule');
    } else {
        $modcontext = context_module::instance($cm->id);
    }
    if (!has_capability('mod/jinoforum:splitdiscussions', $modcontext)) {
        print_error('cannotsplit', 'jinoforum');
    }

    if (!empty($name) && confirm_sesskey()) { // User has confirmed the prune
        $newdiscussion = new stdClass();
        $newdiscussion->course = $discussion->course;
        $newdiscussion->jinoforum = $discussion->jinoforum;
        $newdiscussion->name = $name;
        $newdiscussion->firstpost = $post->id;
        $newdiscussion->userid = $discussion->userid;
        $newdiscussion->groupid = $discussion->groupid;
        $newdiscussion->assessed = $discussion->assessed;
        $newdiscussion->usermodified = $post->userid;
        $newdiscussion->timestart = $discussion->timestart;
        $newdiscussion->timeend = $discussion->timeend;

        $newid = $DB->insert_record('jinoforum_discussions', $newdiscussion);

        $newpost = new stdClass();
        $newpost->id = $post->id;
        $newpost->parent = 0;
        $newpost->subject = $name;

        $DB->update_record("jinoforum_posts", $newpost);

        jinoforum_change_discussionid($post->id, $newid);

        // update last post in each discussion
        jinoforum_discussion_update_last_post($discussion->id);
        jinoforum_discussion_update_last_post($newid);

        // Fire events to reflect the split..
        $params = array(
            'context' => $modcontext,
            'objectid' => $discussion->id,
            'other' => array(
                'jinoforumid' => $jinoforum->id,
            )
        );
        $event = \mod_jinoforum\event\discussion_updated::create($params);
        $event->trigger();

        $params = array(
            'context' => $modcontext,
            'objectid' => $newid,
            'other' => array(
                'jinoforumid' => $jinoforum->id,
            )
        );
        $event = \mod_jinoforum\event\discussion_created::create($params);
        $event->trigger();

        $params = array(
            'context' => $modcontext,
            'objectid' => $post->id,
            'other' => array(
                'discussionid' => $newid,
                'jinoforumid' => $jinoforum->id,
                'jinoforumtype' => $jinoforum->type,
            )
        );
        $event = \mod_jinoforum\event\post_updated::create($params);
        $event->add_record_snapshot('jinoforum_discussions', $discussion);
        $event->trigger();

        redirect(jinoforum_go_back_to("discuss.php?d=$newid"));
    } else { // User just asked to prune something
        $course = $DB->get_record('course', array('id' => $jinoforum->course));

        $PAGE->set_cm($cm);
        $PAGE->set_context($modcontext);
        $PAGE->navbar->add(format_string($post->subject, true), new moodle_url('/mod/jinoforum/discuss.php', array('d' => $discussion->id)));
        $PAGE->navbar->add(get_string("prune", "jinoforum"));
        $PAGE->set_title(format_string($discussion->name) . ": " . format_string($post->subject));
        $PAGE->set_heading($course->fullname);
        echo $OUTPUT->header();
        echo $OUTPUT->heading(format_string($jinoforum->name), 2);
        echo $OUTPUT->heading(get_string('pruneheading', 'jinoforum'), 3);
        echo '<center>';

        include('prune.html');

        jinoforum_print_post($post, $discussion, $jinoforum, $cm, $course, false, false, false);
        echo '</center>';
    }
    echo $OUTPUT->footer();
    die;
} else {
    print_error('unknowaction');
}

if (!isset($coursecontext)) {
    // Has not yet been set by post.php.
    $coursecontext = context_course::instance($jinoforum->course);
}


// from now on user must be logged on properly

if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id)) { // For the logs
    print_error('invalidcoursemodule');
}
$modcontext = context_module::instance($cm->id);
require_login($course, false, $cm);

if (isguestuser()) {
    // just in case
    print_error('noguest');
}

if (!isset($jinoforum->maxattachments)) {  // TODO - delete this once we add a field to the jinoforum table
    $jinoforum->maxattachments = 3;
}

$thresholdwarning = jinoforum_check_throttling($jinoforum, $cm);
$mform_post = new mod_jinoforum_post_form('post.php', array('course' => $course,
    'cm' => $cm,
    'coursecontext' => $coursecontext,
    'modcontext' => $modcontext,
    'jinoforum' => $jinoforum,
    'post' => $post,
    'thresholdwarning' => $thresholdwarning,
    'edit' => $edit), 'post', '', array('id' => 'mformjinoforum'));
if($mform_post->is_cancelled()){
    redirect($CFG->wwwroot."/mod/jinoforum/view.php?id=".$cm->id);
}
$draftitemid = file_get_submitted_draft_itemid('attachments');
file_prepare_draft_area($draftitemid, $modcontext->id, 'mod_jinoforum', 'attachment', empty($post->id) ? null : $post->id, mod_jinoforum_post_form::attachment_options($jinoforum));

//load data into form NOW!

if ($USER->id != $post->userid) {   // Not the original author, so add a message to the end
    $data = new stdClass();
    $data->date = userdate($post->modified);
    if ($post->messageformat == FORMAT_HTML) {
        $data->name = '<a href="' . $CFG->wwwroot . '/user/view.php?id=' . $USER->id . '&course=' . $post->course . '">' .
                fullname($USER) . '</a>';
        $post->message .= '<p><span class="edited">(' . get_string('editedby', 'jinoforum', $data) . ')</span></p>';
    } else {
        $data->name = fullname($USER);
        $post->message .= "\n\n(" . get_string('editedby', 'jinoforum', $data) . ')';
    }
    unset($data);
}

$formheading = '';
if (!empty($parent)) {
    $heading = get_string("yourreply", "jinoforum");
    $formheading = get_string('reply', 'jinoforum');
} else {
    if ($jinoforum->type == 'qanda') {
        $heading = get_string('yournewquestion', 'jinoforum');
    } else {
        $heading = get_string('yournewtopic', 'jinoforum');
    }
}

if (jinoforum_is_subscribed($USER->id, $jinoforum->id)) {
    $subscribe = true;
} else if (jinoforum_user_has_posted($jinoforum->id, 0, $USER->id)) {
    $subscribe = false;
} else {
    // user not posted yet - use subscription default specified in profile
    $subscribe = !empty($USER->autosubscribe);
}

$postid = empty($post->id) ? null : $post->id;
$draftid_editor = file_get_submitted_draft_itemid('message');
$currenttext = file_prepare_draft_area($draftid_editor, $modcontext->id, 'mod_jinoforum', 'post', $postid, mod_jinoforum_post_form::editor_options($modcontext, $postid), $post->message);

$mform_post->set_data(array('attachments' => $draftitemid,
    'general' => $heading,
    'subject' => $post->subject,
    'message' => array(
        'text' => $currenttext,
        'format' => empty($post->messageformat) ? editors_get_preferred_format() : $post->messageformat,
        'itemid' => $draftid_editor,
    ),
    'subscribe' => $subscribe ? 1 : 0,
    'mailnow' => !empty($post->mailnow),
    'userid' => $post->userid,
    'parent' => $post->parent,
    'discussion' => $post->discussion,
    'course' => $course->id) +
        $page_params +
        (isset($post->format) ? array(
            'format' => $post->format) :
                array()) +
        (isset($discussion->timestart) ? array(
            'timestart' => $discussion->timestart) :
                array()) +
        (isset($discussion->timeend) ? array(
            'timeend' => $discussion->timeend) :
                array()) +
        (isset($post->groupid) ? array(
            'groupid' => $post->groupid) :
                array()) +
        (isset($discussion->id) ?
                array('discussion' => $discussion->id) :
                array()));

if ($fromform = $mform_post->get_data()) {

    if (empty($SESSION->fromurl)) {
        $errordestination = "$CFG->wwwroot/mod/jinoforum/view.php?f=$jinoforum->id";
    } else {
        $errordestination = $SESSION->fromurl;
    }

    $fromform->itemid = $fromform->message['itemid'];
    $fromform->messageformat = $fromform->message['format'];
    $fromform->message = $fromform->message['text'];
    // WARNING: the $fromform->message array has been overwritten, do not use it anymore!
    $fromform->messagetrust = trusttext_trusted($modcontext);

    $contextcheck = isset($fromform->groupinfo) && has_capability('mod/jinoforum:movediscussions', $modcontext);

    if ($fromform->edit) {  // Updating a post
        unset($fromform->groupid);
        $fromform->id = $fromform->edit;
        $message = '';

        //fix for bug #4314
        if (!$realpost = $DB->get_record('jinoforum_posts', array('id' => $fromform->id))) {
            $realpost = new stdClass();
            $realpost->userid = -1;
        }

        $DB->delete_records('jinoforum_users', array('discussionid' => $post->discussion));

        if ($fromform->allow_user && !$mform->ispublic) {
            $fromform->allow_user = str_replace("[", "", $fromform->allow_user);
            $allowusers = explode("]", $fromform->allow_user);

            $allowusers = array_filter($allowusers);
            foreach ($allowusers as $allowuser => $uid) {
                $udate = new stdClass();
                $udata->userid = $uid;
                $udata->discussionid = $post->discussion;
                $timenow = time();
                $udata->timecreated = $timenow;
                if (!$DB->get_records("jinoforum_users", array('discussionid' => $post->discussion, 'userid' => $uid))) {
                    $DB->insert_record("jinoforum_users", $udata);
                }
            }
        }



        // if user has edit any post capability
        // or has either startnewdiscussion or reply capability and is editting own post
        // then he can proceed
        // MDL-7066
        if (!(($realpost->userid == $USER->id && (has_capability('mod/jinoforum:replypost', $modcontext) || has_capability('mod/jinoforum:startdiscussion', $modcontext))) ||
                has_capability('mod/jinoforum:editanypost', $modcontext))) {
            print_error('cannotupdatepost', 'jinoforum');
        }

        // If the user has access to all groups and they are changing the group, then update the post.
        if ($contextcheck) {
            if (empty($fromform->groupinfo)) {
                $fromform->groupinfo = -1;
            }
            $DB->set_field('jinoforum_discussions', 'groupid', $fromform->groupinfo, array('firstpost' => $fromform->id));
        }

        $updatepost = $fromform; //realpost
        $updatepost->jinoforum = $jinoforum->id;
        if (!jinoforum_update_post($updatepost, $mform_post, $message)) {
            print_error("couldnotupdate", "jinoforum", $errordestination);
        }

        // MDL-11818
        if (($jinoforum->type == 'single') && ($updatepost->parent == '0')) { // updating first post of single discussion type -> updating jinoforum intro
            $jinoforum->intro = $updatepost->message;
            $jinoforum->timemodified = time();
            $DB->update_record("jinoforum", $jinoforum);
        }

        $timemessage = 2;
        if (!empty($message)) { // if we're printing stuff about the file upload
            $timemessage = 4;
        }

        if ($realpost->userid == $USER->id) {
            $message .= '<br />' . get_string("postupdated", "jinoforum");
        } else {
            $realuser = $DB->get_record('user', array('id' => $realpost->userid));
            $message .= '<br />' . get_string("editedpostupdated", "jinoforum", fullname($realuser));
        }

        if ($subscribemessage = jinoforum_post_subscription($fromform, $jinoforum)) {
            $timemessage = 4;
        }
        if ($jinoforum->type == 'single') {
            // Single discussion jinoforums are an exception. We show
            // the jinoforum itself since it only has one discussion
            // thread.
            $discussionurl = "view.php?f=$jinoforum->id";
        } else {
            $discussionurl = "discuss.php?d=$discussion->id#p$fromform->id";
        }

        $params = array(
            'context' => $modcontext,
            'objectid' => $fromform->id,
            'other' => array(
                'discussionid' => $discussion->id,
                'jinoforumid' => $jinoforum->id,
                'jinoforumtype' => $jinoforum->type,
            )
        );

        if ($realpost->userid !== $USER->id) {
            $params['relateduserid'] = $realpost->userid;
        }

        $event = \mod_jinoforum\event\post_updated::create($params);
        $event->add_record_snapshot('jinoforum_discussions', $discussion);
        $event->trigger();

        redirect(jinoforum_go_back_to("$discussionurl"));

        exit;
    } else if ($fromform->discussion) { // Adding a new post to an existing discussion
        // Before we add this we must check that the user will not exceed the blocking threshold.
        jinoforum_check_blocking_threshold($thresholdwarning);

        unset($fromform->groupid);
        $message = '';
        $addpost = $fromform;
        $addpost->jinoforum = $jinoforum->id;

        if ($fromform->id = jinoforum_add_new_post($addpost, $mform_post, $message)) {

            $timemessage = 2;
            if (!empty($message)) { // if we're printing stuff about the file upload
                $timemessage = 4;
            }

            if ($subscribemessage = jinoforum_post_subscription($fromform, $jinoforum)) {
                $timemessage = 4;
            }

            if (!empty($fromform->mailnow)) {
                $message .= get_string("postmailnow", "jinoforum");
                $timemessage = 4;
            } else {
                $message .= '<p>' . get_string("postaddedsuccess", "jinoforum") . '</p>';
                // $message .= '<p>'.get_string("postaddedtimeleft", "jinoforum", format_time($CFG->maxeditingtime)) . '</p>';
            }

            if ($jinoforum->type == 'single') {
                // Single discussion jinoforums are an exception. We show
                // the jinoforum itself since it only has one discussion
                // thread.
                $discussionurl = "view.php?f=$jinoforum->id";
            } else {
                $discussionurl = "discuss.php?d=$discussion->id";
            }

            $params = array(
                'context' => $modcontext,
                'objectid' => $fromform->id,
                'other' => array(
                    'discussionid' => $discussion->id,
                    'jinoforumid' => $jinoforum->id,
                    'jinoforumtype' => $jinoforum->type,
                )
            );
            $event = \mod_jinoforum\event\post_created::create($params);
            $event->add_record_snapshot('jinoforum_posts', $fromform);
            $event->add_record_snapshot('jinoforum_discussions', $discussion);
            $event->trigger();

            // Update completion state
            $completion = new completion_info($course);
            if ($completion->is_enabled($cm) &&
                    ($jinoforum->completionreplies || $jinoforum->completionposts)) {
                $completion->update_state($cm, COMPLETION_COMPLETE);
            }

            redirect(jinoforum_go_back_to("$discussionurl#p$fromform->id"));
        } else {
            print_error("couldnotadd", "jinoforum", $errordestination);
        }
        exit;
    } else { // Adding a new discussion.
        // Before we add this we must check that the user will not exceed the blocking threshold.
        jinoforum_check_blocking_threshold($thresholdwarning);

        if (!jinoforum_user_can_post_discussion($jinoforum, $fromform->groupid, -1, $cm, $modcontext)) {
            print_error('cannotcreatediscussion', 'jinoforum');
        }
        // If the user has access all groups capability let them choose the group.
        if ($contextcheck) {
            $fromform->groupid = $fromform->groupinfo;
        }
        if (empty($fromform->groupid)) {
            $fromform->groupid = -1;
        }

        $fromform->mailnow = empty($fromform->mailnow) ? 0 : 1;

        $discussion = $fromform;
        $discussion->name = $fromform->subject;

        $newstopic = false;
        if ($jinoforum->type == 'news' && !$fromform->parent) {
            $newstopic = true;
        }
        $discussion->timestart = $fromform->timestart;
        $discussion->timeend = $fromform->timeend;

        $message = '';
        if ($discussion->id = jinoforum_add_discussion($discussion, $mform_post, $message)) {

            $params = array(
                'context' => $modcontext,
                'objectid' => $discussion->id,
                'other' => array(
                    'jinoforumid' => $jinoforum->id,
                )
            );
            $event = \mod_jinoforum\event\discussion_created::create($params);
            $event->add_record_snapshot('jinoforum_discussions', $discussion);
            $event->trigger();

            $timemessage = 2;
            if (!empty($message)) { // if we're printing stuff about the file upload
                $timemessage = 4;
            }

            if ($fromform->mailnow) {
                $message .= get_string("postmailnow", "jinoforum");
                $timemessage = 4;
            } else {
                $message .= '<p>' . get_string("postaddedsuccess", "jinoforum") . '</p>';
                //  $message .= '<p>'.get_string("postaddedtimeleft", "jinoforum", format_time($CFG->maxeditingtime)) . '</p>';
            }

            if ($subscribemessage = jinoforum_post_subscription($discussion, $jinoforum)) {
                $timemessage = 6;
            }

            // Update completion status
            $completion = new completion_info($course);
            if ($completion->is_enabled($cm) &&
                    ($jinoforum->completiondiscussions || $jinoforum->completionposts)) {
                $completion->update_state($cm, COMPLETION_COMPLETE);
            }

            redirect(jinoforum_go_back_to("view.php?f=$fromform->jinoforum"));
        } else {
            print_error("couldnotadd", "jinoforum", $errordestination);
        }

        exit;
    }
}



// To get here they need to edit a post, and the $post
// variable will be loaded with all the particulars,
// so bring up the form.
// $course, $jinoforum are defined.  $discussion is for edit and reply only.

if ($post->discussion) {
    if (!$toppost = $DB->get_record("jinoforum_posts", array("discussion" => $post->discussion, "parent" => 0))) {
        print_error('cannotfindparentpost', 'jinoforum', '', $post->id);
    }
} else {
    $toppost = new stdClass();
    $toppost->subject = ($jinoforum->type == "news") ? get_string("addanewtopic", "jinoforum") :
            get_string("addanewdiscussion", "jinoforum");
}

if (empty($post->edit)) {
    $post->edit = '';
}

if (empty($discussion->name)) {
    if (empty($discussion)) {
        $discussion = new stdClass();
    }
    $discussion->name = $jinoforum->name;
}
if ($jinoforum->type == 'single') {
    // There is only one discussion thread for this jinoforum type. We should
    // not show the discussion name (same as jinoforum name in this case) in
    // the breadcrumbs.
    $strdiscussionname = '';
} else {
    // Show the discussion name in the breadcrumbs.
    $strdiscussionname = format_string($discussion->name) . ':';
}

$forcefocus = empty($reply) ? NULL : 'message';

if (!empty($discussion->id)) {
    $PAGE->navbar->add(format_string($toppost->subject, true), "discuss.php?d=$discussion->id");
}

if ($post->parent) {
    $PAGE->navbar->add(get_string('reply', 'jinoforum'));
}

if ($edit) {
    $PAGE->navbar->add(get_string('edit', 'jinoforum'));
}

$PAGE->set_title("$course->shortname: $strdiscussionname " . format_string($toppost->subject));
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($jinoforum->name), 2);

// checkup
if (!empty($parent) && !jinoforum_user_can_see_post($jinoforum, $discussion, $post, null, $cm)) {
    print_error('cannotreply', 'jinoforum');
}
if (empty($parent) && empty($edit) && !jinoforum_user_can_post_discussion($jinoforum, $groupid, -1, $cm, $modcontext)) {
    print_error('cannotcreatediscussion', 'jinoforum');
}

if ($jinoforum->type == 'qanda' && !has_capability('mod/jinoforum:viewqandawithoutposting', $modcontext) && !empty($discussion->id) && !jinoforum_user_has_posted($jinoforum->id, $discussion->id, $USER->id)) {
    echo $OUTPUT->notification(get_string('qandanotify', 'jinoforum'));
}

// If there is a warning message and we are not editing a post we need to handle the warning.
if (!empty($thresholdwarning) && !$edit) {
    // Here we want to throw an exception if they are no longer allowed to post.
    jinoforum_check_blocking_threshold($thresholdwarning);
}

if (!empty($parent)) {
    if (!$discussion = $DB->get_record('jinoforum_discussions', array('id' => $parent->discussion))) {
        print_error('notpartofdiscussion', 'jinoforum');
    }

    jinoforum_print_post($parent, $discussion, $jinoforum, $cm, $course, false, false, false);
    if (empty($post->edit)) {
        if ($jinoforum->type != 'qanda' || jinoforum_user_can_see_discussion($jinoforum, $discussion, $modcontext)) {
            $jinoforumtracked = jinoforum_tp_is_tracked($jinoforum);
            $posts = jinoforum_get_all_discussion_posts($discussion->id, "created ASC", $jinoforumtracked);
            //jinoforum_print_posts_threaded($course, $cm, $jinoforum, $discussion, $parent, 0, false, $jinoforumtracked, $posts);
        }
    }
} else {
    if (!empty($jinoforum->intro)) {
        echo $OUTPUT->box(format_module_intro('jinoforum', $jinoforum, $cm->id), 'generalbox', 'intro');

        if (!empty($CFG->enableplagiarism)) {
            require_once($CFG->libdir . '/plagiarismlib.php');
            echo plagiarism_print_disclosure($cm->id);
        }
    }
}

if (!empty($formheading)) {
    echo $OUTPUT->heading($formheading, 2, array('class' => 'accesshide'));
}
$mform_post->display();
?>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
    $(window).load(function () {
        if ($("input[name=view_filemanager]").prop("checked")) {
            $("#fgroup_id_filemanager").show();
        } else {
            $("#fgroup_id_filemanager").hide();
        }
    });

    $("input[name=view_filemanager]").click(function () {
        if ($("input[name=view_filemanager]").prop("checked")) {
            $("#fgroup_id_filemanager").show();
        } else {
            $("#fgroup_id_filemanager").hide();
        }
    });
    $(function () {
        $("#jinoforum_user_dialog").css('zIndex', 9999);
        $("#jinoforum_user_dialog").dialog({
             maxWidth:600,
             maxHeight: 500,
             width: 400,
             height: 500,
            autoOpen: false,
            hide: {
                effect: "explode",
                duration: 1000,
            },
            buttons: {
                Ok: function () {
                    var name = "";
                    var userid = "";
                    if ($("input[class=user_add_btn]").size() > 0) {
                        for (i = 1; i <= $("input[class=user_add_btn]").size(); i++) {
                            if ($("input[name=allow_user]").val().indexOf('[' + $("input[name=user" + i + "]").val() + ']') < 0 && $("input[name=user" + i + "]").prop("checked")) {
                                userid += '[' + $("input[name=user" + i + "]").val() + ']';
                                name += "<span class='belong_user' userid='"+$("input[name=user" + i + "]").val()+"' id='username" + $("input[name=user" + i + "]").val() + "'>" + $("input[name=username" + i + "]").val() + "<span class='deletex' onclick='" + 'delete_user(' + $("input[name=user" + i + "]").val() + ')' + "'>x</span></span>";
                            }
                        }
                    }
                    $("#allow_user").html($("#allow_user").html() + name);
                    $("input[name=allow_user]").val($("input[name=allow_user]").val() + userid);
                    name = "";
                    userid = "";
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
        // 다이얼로그 오픈
        $("#opener").click(function () {
            $("#jinoforum_user_dialog").dialog("open");
            var usedid = '';
            jQuery.each($('.belong_user'),function(i, obj){
                usedid += obj.getAttribute("userid")+',';
            });
            $.ajax({
                url: "post_usersearch_ajax.php?courseid=<?php echo $course->id; ?>"+'&cmid=<?php echo $cm->id; ?>&usedid=' + usedid,
                success: function (result) {
                    $("#studentlist").html(result);
                }
            });
        });
        // 공개 비공개 전환 
        $("input[name=is_public]").click(function () {
            if ($("input[name=is_public]").prop("checked")) {
                $("#opener").hide();
                $("#allow_user").hide();
                $("input[name=ispublic]").val(1);
            } else {
                $("#opener").show();
                $("#allow_user").show();
                $("input[name=ispublic]").val(0);
            }
        });
        // 다이얼로그 검색버튼 클릭
        $("#searchbtn").click(function () {
            var usedid = '';
            jQuery.each($('.belong_user'),function(i, obj){
                usedid += obj.getAttribute("userid")+',';
            });
            $.ajax({url: "post_usersearch_ajax.php?courseid=<?php echo $course->id; ?>&cmid=<?php echo $cm->id; ?>&search=" + $("#user_search_ajax").val() + '&usedid=' + usedid,
                success: function (result) {
                    $("#studentlist").html(result);
                }
            });
        });
    });
    function delete_user(id) {
        $("#username" + id).remove();
        $("input[name=allow_user]").val($("input[name=allow_user]").val().replace("[" + id + "]", ""));
    }
</script>  
<div id="jinoforum_user_dialog" title="<?php echo get_string('studentindiscussion','jinoforum') ?>">
    <input type="text" id="user_search_ajax" name="user_search">
    <button id="searchbtn" class="blue-form"><?php echo get_string('search'); ?></button>
    <span id="studentlist"></span>
</div>
<?php 
echo $OUTPUT->footer();
?>
