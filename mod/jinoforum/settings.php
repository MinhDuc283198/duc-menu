<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_jinoforum
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once($CFG->dirroot.'/mod/jinoforum/lib.php');

    $settings->add(new admin_setting_configselect('jinoforum_displaymode', get_string('displaymode', 'jinoforum'),
                       get_string('configdisplaymode', 'jinoforum'), JINOFORUM_MODE_NESTED, jinoforum_get_layout_modes()));

    $settings->add(new admin_setting_configcheckbox('jinoforum_replytouser', get_string('replytouser', 'jinoforum'),
                       get_string('configreplytouser', 'jinoforum'), 1));

    // Less non-HTML characters than this is short
    $settings->add(new admin_setting_configtext('jinoforum_shortpost', get_string('shortpost', 'jinoforum'),
                       get_string('configshortpost', 'jinoforum'), 300, PARAM_INT));

    // More non-HTML characters than this is long
    $settings->add(new admin_setting_configtext('jinoforum_longpost', get_string('longpost', 'jinoforum'),
                       get_string('configlongpost', 'jinoforum'), 600, PARAM_INT));
	/*
    // Number of discussions on a page
    $settings->add(new admin_setting_configtext('jinoforum_manydiscussions', get_string('manydiscussions', 'jinoforum'),
                       get_string('configmanydiscussions', 'jinoforum'), 100, PARAM_INT));
	*/
	$settings->add(new admin_setting_configcheckbox('jinoforum_maxbytes', get_string('largefileattachments', 'jinoforum'),
                       get_string('configmaxbytes', 'jinoforum'), 0));

    // Default number of attachments allowed per post in all jinoforums
    $settings->add(new admin_setting_configtext('jinoforum_maxattachments', get_string('maxattachments', 'jinoforum'),
                       get_string('configmaxattachments', 'jinoforum'), 9, PARAM_INT));

    // Default Read Tracking setting.
    $options = array();
    $options[JINOFORUM_TRACKING_OPTIONAL] = get_string('trackingoptional', 'jinoforum');
    $options[JINOFORUM_TRACKING_OFF] = get_string('trackingoff', 'jinoforum');
    $options[JINOFORUM_TRACKING_FORCED] = get_string('trackingon', 'jinoforum');
    $settings->add(new admin_setting_configselect('jinoforum_trackingtype', get_string('trackingtype', 'jinoforum'),
                       get_string('configtrackingtype', 'jinoforum'), JINOFORUM_TRACKING_OPTIONAL, $options));
	
	 $options = array();
     $options[JINOFORUM_CHOOSESUBSCRIBE] = get_string('subscriptionoptional', 'jinoforum');
     $options[JINOFORUM_FORCESUBSCRIBE] = get_string('subscriptionforced', 'jinoforum');
     $options[JINOFORUM_INITIALSUBSCRIBE] = get_string('subscriptionauto', 'jinoforum');
     $options[JINOFORUM_DISALLOWSUBSCRIBE] = get_string('subscriptiondisabled','jinoforum');
	 
    $settings->add(new admin_setting_configselect('jinoforum_forcesubscribe', get_string('subscriptionmode', 'jinoforum'),
                       get_string('subscriptionmode', 'jinoforum'), JINOFORUM_FORCESUBSCRIBE, $options));

    // Default whether user needs to mark a post as read
    $settings->add(new admin_setting_configcheckbox('jinoforum_trackreadposts', get_string('trackjinoforum', 'jinoforum'),
                       get_string('configtrackreadposts', 'jinoforum'), 1));

    // Default whether user needs to mark a post as read.
    $settings->add(new admin_setting_configcheckbox('jinoforum_allowforcedreadtracking', get_string('forcedreadtracking', 'jinoforum'),
                       get_string('forcedreadtracking_desc', 'jinoforum'), 0));

    // Default number of days that a post is considered old
    $settings->add(new admin_setting_configtext('jinoforum_oldpostdays', get_string('oldpostdays', 'jinoforum'),
                       get_string('configoldpostdays', 'jinoforum'), 14, PARAM_INT));

    // Default whether user needs to mark a post as read
    $settings->add(new admin_setting_configcheckbox('jinoforum_usermarksread', get_string('usermarksread', 'jinoforum'),
                       get_string('configusermarksread', 'jinoforum'), 0));

    $options = array();
    for ($i = 0; $i < 24; $i++) {
        $options[$i] = sprintf("%02d",$i);
    }
    // Default time (hour) to execute 'clean_read_records' cron
    $settings->add(new admin_setting_configselect('jinoforum_cleanreadtime', get_string('cleanreadtime', 'jinoforum'),
                       get_string('configcleanreadtime', 'jinoforum'), 2, $options));

    // Default time (hour) to send digest email
    $settings->add(new admin_setting_configselect('digestmailtime', get_string('digestmailtime', 'jinoforum'),
                       get_string('configdigestmailtime', 'jinoforum'), 17, $options));

    if (empty($CFG->enablerssfeeds)) {
        $options = array(0 => get_string('rssglobaldisabled', 'admin'));
        $str = get_string('configenablerssfeeds', 'jinoforum').'<br />'.get_string('configenablerssfeedsdisabled2', 'admin');

    } else {
        $options = array(0=>get_string('no'), 1=>get_string('yes'));
        $str = get_string('configenablerssfeeds', 'jinoforum');
    }
    $settings->add(new admin_setting_configselect('jinoforum_enablerssfeeds', get_string('enablerssfeeds', 'admin'),
                       $str, 0, $options));

    $settings->add(new admin_setting_configcheckbox('jinoforum_enabletimedposts', get_string('timedposts', 'jinoforum'),
                       get_string('configenabletimedposts', 'jinoforum'), 1));
}

