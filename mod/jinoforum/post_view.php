<?php
/**
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$p = required_param('post', PARAM_INT); // post ID

$post = $DB->get_record('jinoforum_posts', array('id' => $p), '*', MUST_EXIST);
$parent_post = $DB->get_record('jinoforum_posts', array('id' => $post->parent), '*', MUST_EXIST);
$discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion), '*', MUST_EXIST);
$course = $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
$jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum), '*', MUST_EXIST);

if($post->userid != $USER->id){
    $DB->insert_record('jinoforum_read', array('userid' => $USER->id, 'jinoforumid' => $discussion->jinoforum, 'discussionid' => $discussion->id, 'postid' => $post->id, 'firstread' => time(), 'lastread' => time()));
    $DB->set_field_select('jinoforum_posts', 'viewcnt', intval($post->viewcnt) + 1, " id='$post->id'");
}

$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id, false, MUST_EXIST);
$modcontext = context_module::instance($cm->id);

require_course_login($course, true, $cm);
$PAGE->requires->jquery();
$url = new moodle_url('/mod/jinoforum/post_view.php', array('p' => $p));
$PAGE->set_title("$course->shortname: " . format_string($post->name));
$PAGE->set_heading($course->fullname);


$postuser = $DB->get_record('user', array('id' => $post->userid));
echo $OUTPUT->header();
$userdate = userdate($post->modified);


$output .= html_writer::start_tag('div', array('class' => 'board-detail-area'));
echo $OUTPUT->heading(format_string($jinoforum->name), 2, array('class'=>'board-title'));
//제목영역
$output .= html_writer::start_tag('div', array('class' => 'detail-title-area'));
$output .= html_writer::tag('span', $post->subject, array('class' => 'detail-title'));
//$output .= html_writer::tag('br');
$by = new stdClass();
$by->date = $userdate;
$by->name = fullname($postuser);
$output .= html_writer::tag('span', get_string('bynameondate','jinoforum',$by), array('class' => 'detail-date'));

$output .= html_writer::tag('span', $post->recommendcnt . '<br/><span>' . get_string('recommend:cnt', 'jinoforum') . '</span>', array('class' => 'detail-recommendinfo area-right'));
$output .= html_writer::tag('span', $post->commentscount . '<br/><span>' . get_string('comment:cnt', 'jinoforum') . '</span>', array('class' => 'detail-commentinfo area-right'));
$output .= html_writer::tag('span', $post->viewcnt . '<br/><span>' . get_string('hit', 'jinoforum') . '</span>', array('class' => 'detail-viewinfo area-right'));
 
$output .= html_writer::end_tag('div');

$options = new stdClass;
$options->para = false;
$options->trusted = $post->messagetrust;
$options->context = $modcontext;

$post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_jinoforum', 'post', $post->id);
$post->message = format_text($post->message, $post->messageformat, $options, $course->id);


$output .= html_writer::tag('div', $post->message, array('class' => 'detail-contents'));

$fs = get_file_storage();

$files = $fs->get_area_files($modcontext->id, 'mod_jinoforum', 'attachment', $post->id, 'timemodified', false);
if (count($files) > 0) {
    $type = '';
    $attfile = '<ul>';

    if ($CFG->enableportfolios)
        $canexport = $USER->id == $boardContent->userid;
    if ($canexport) {
        require_once($CFG->libdir . '/portfoliolib.php');
    }
    foreach ($files as $file) {
        $filepath = $file->get_filepath();
        if (empty($filepath)) {
            $filepath = '/';
        }

        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<li><img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $modcontext->id . '/mod_jinoforum/attachment/' . $post->id . $filepath . $filename);

        $attfile .= "<a href=\"$path\">$iconimage</a> ";
        $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));

        $attfile .= '</li>';
    }
    $attfile .= '</ul>';
    $attachments .= $attfile;

    $output .= html_writer::start_tag('div', array('class' => 'detail-attachment-area'));
    $output .= html_writer::tag('span', get_string('attachment', 'jinoforum'), array('class' => "detail-attachment-title"));
    $output .= html_writer::tag('span', $attachments, array('class' => "detail-attachment"));
    $output .= html_writer::end_tag('div');
}

$left_btns = html_writer::tag('button', get_string('commentlist', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $CFG->wwwroot . "/mod/jinoforum/discuss.php?d=" . $discussion->id . '"'));
$left_btns .= html_writer::tag('button', get_string('replies', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $CFG->wwwroot . "/mod/jinoforum/post.php?reply=" . $post->id . '#mformjinoforum"'));
if (!$DB->get_records("jinoforum_recommend", array('postid' => $p, 'userid' => $USER->id))) {
    $left_btns .= html_writer::tag('button', get_string('recommend', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => "if(confirm('".  get_string('real_recommend','jinoforum')."')){ location.href='" . $CFG->wwwroot . "/mod/jinoforum/post_recommend.php?post=" . $post->id . "'}"));
} else {
    $left_btns .= html_writer::tag('button', get_string('recommend', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => "alert('".  get_string('already_recommended','jinoforum')."');"));
}

$right_btns = "";
if (has_capability('mod/jinoforum:editanypost', $modcontext) || $USER->id == $post->userid) {
    $right_btns .= html_writer::tag('button', get_string('edit', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $CFG->wwwroot . "/mod/jinoforum/post.php?edit=" . $post->id . '"'));
}
if (has_capability('mod/jinoforum:deleteanypost', $modcontext) || ($USER->id == $post->userid && has_capability('mod/jinoforum:deleteownpost', $modcontext))) {
    $right_btns .= html_writer::tag('button', get_string('delete', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $CFG->wwwroot . "/mod/jinoforum/post.php?delete=" . $post->id . '"'));
}
$cols = html_writer::tag('div', $left_btns, array('class' => "btn-area btn-area-left"));
$cols .= html_writer::tag('div', $right_btns, array('class' => "btn-area btn-area-right"));
$output .= html_writer::tag('div', $cols, array('class' => "table-footer-area"));

$output .= html_writer::start_tag('div', array('class' => 'table-reply-area'));
$output .= html_writer::start_tag('form', array('method' => 'post', 'id' => 'comment_form', 'class' => 'reply', 'action' => 'post_comment.php?post=' . $p));
$input = html_writer::tag('textarea', '', array('id' => 'comment_textarea', 'name' => 'comment_value'));
$cols = html_writer::tag('span', $input, array('class' => "option"));
$btn = html_writer::tag('input', '', array('id' => 'comment_submit', 'type' => 'submit', 'class' => 'reply-submit', 'value' => get_string('comment', 'jinoforum')));
$cols .= html_writer::tag('span', $btn, array('class' => "option"));
$output .= html_writer::tag('div', $cols, array('class' => "view_row"));
$output .= html_writer::end_tag('form');



if ($comments = $DB->get_records('jinoforum_comments', array('postid' => $p), ' id desc')) {
    $output .= html_writer::start_tag('ul', array('class' => 'reply-list'));
    foreach ($comments as $comment) {

        $commentuser = $DB->get_record('user', array('id' => $comment->userid));
        $fullname = fullname($commentuser);
        $timecreated = date("Y-m-d H:i:s", $comment->timecreated);
        $linktext = "";
        if (has_capability('mod/jinoforum:deletecomment', $modcontext) || $USER->id == $comment->userid) {
            $linktext = html_writer::link("post_comment.php?post=" . $p . "&type=delete&id=" . $comment->id, 'x');
        }

        $delete = html_writer::tag('span', $linktext, array('class' => "comment_delete"));
        $p = html_writer::tag('p', $comment->comments . $delete, array('class' => "value", 'colspan' => '3'));
        $span = html_writer::tag('span', $fullname . " | " . $timecreated, array('class' => "comment_author"));
        $output .= html_writer::tag('li', $p . $span, array('class' => "comment_row"));
    }
    $output .= html_writer::end_tag('ul');
}
 $output .= html_writer::end_tag('div');

$param = array('ref' => $post->ref, 'discussion' => $discussion->id, 'id' => $post->id);

$prev_sql = "select id from {jinoforum_posts} where lev = 1 and id < :id and ref = :ref and discussion= :discussion  order by id desc ";
$sql = "select id from {jinoforum_posts} where lev = 1 and id = :id and ref = :ref and discussion= :discussion";    
$next_sql = "select id from {jinoforum_posts} where lev = 1 and id > :id and ref = :ref and discussion= :discussion order by id asc";    


$output .= "<table class='generaltable'><thead><tr>";
$output .= '<th class="col-1"' . $thid . ' style="padding: 0; border-bottom: 0 !important; border-top: 1px solid #676767 !important; height: 0px;"></th></thead><tbody>';

if($post->lev == 1){
jinoforum_print_content($prev_sql, $param,'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="prev" />');
jinoforum_print_content($sql, $param,'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="current" />');
} else {
$parentid = $DB->get_record_sql($prev_sql, $param);
jinoforum_print_content($prev_sql, array('ref' => $post->ref, 'discussion' => $discussion->id, 'id' => $parentid->id),'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="prev" />'); 
jinoforum_print_content($sql, array('ref' => $post->ref, 'discussion' => $discussion->id, 'id' => $parentid->id),'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="current" />');    
}
jinoforum_print_content($next_sql, $param,'<img src="' . $OUTPUT->pix_url('t/collapsed') . '" class="icon" alt="next" />');
$output .= '</tbody></table>';

$output .= html_writer::end_tag('div');

echo $output;

echo $OUTPUT->footer();
?> 
<script>
    $(document).ready(function () {

        $('#comment_submit').click(function () {
            if (!$('#comment_textarea').val()) {
                alert('<?php echo get_string('emptycomment', 'jinoforum'); ?>');
                $('#comment_textarea').focus();
                return false;
            } else {
                $('comment_form').submit();
            }
        });

    });
</script>
