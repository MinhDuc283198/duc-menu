<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays a post, and all the posts below it.
 * If no post is given, displays all posts in a discussion
 *
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');

$d = required_param('d', PARAM_INT); // Discussion ID
$parent = optional_param('parent', 0, PARAM_INT);  // If set, then display this post and all children.
$mode = optional_param('mode', 3, PARAM_INT); // If set, changes the layout of the thread
$move = optional_param('move', 0, PARAM_INT); // If set, moves this discussion to another jinoforum
$mark = optional_param('mark', '', PARAM_ALPHA); // Used for tracking read posts if user initiated.
$page = optional_param('page', 1, PARAM_INT);  // which page to show
$postid = optional_param('postid', 0, PARAM_INT);  // Used for tracking read posts if user initiated.
$perpage = optional_param('perpage', 5, PARAM_INT); //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$searchval = optional_param('searchval', "", PARAM_CLEAN);
$searchtype = optional_param('searchtype', 1, PARAM_INT);

$url = new moodle_url('/mod/jinoforum/discuss.php', array('d' => $d));
if ($parent !== 0) {
	$url->param('parent', $parent);
}
$PAGE->set_url($url);

$discussion = $DB->get_record('jinoforum_discussions', array('id' => $d), '*', MUST_EXIST);
$course = $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
$jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum), '*', MUST_EXIST);
$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id, false, MUST_EXIST);

require_course_login($course, true, $cm);

// move this down fix for MDL-6926
require_once($CFG->dirroot . '/mod/jinoforum/lib.php');

$modcontext = context_module::instance($cm->id);
require_capability('mod/jinoforum:viewdiscussion', $modcontext, NULL, true, 'noviewdiscussionspermission', 'jinoforum');
if( !has_capability('mod/jinoforum:viewcloseddiscussion', $modcontext)  && (!$discussion->ispublic && (! $DB->get_records('jinoforum_users',array('discussionid'=>$discussion->id,'userid'=>$USER->id) )   &&   $USER->id != $discussion->userid))){
	print_error('cannotmovenotvisible', 'jinoforum', $return);
}
if (!empty($CFG->enablerssfeeds) && !empty($CFG->jinoforum_enablerssfeeds) && $jinoforum->rsstype && $jinoforum->rssarticles) {
	require_once("$CFG->libdir/rsslib.php");

	$rsstitle = format_string($course->shortname, true, array('context' => context_course::instance($course->id))) . ': ' . format_string($jinoforum->name);
	rss_add_http_header($modcontext, 'mod_jinoforum', $jinoforum, $rsstitle);
}

/// move discussion if requested
if ($move > 0 and confirm_sesskey()) {
	$return = $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $discussion->id;

	require_capability('mod/jinoforum:movediscussions', $modcontext);

	if ($jinoforum->type == 'single') {
		print_error('cannotmovefromsinglejinoforum', 'jinoforum', $return);
	}

	if (!$jinoforumto = $DB->get_record('jinoforum', array('id' => $move))) {
		print_error('cannotmovetonotexist', 'jinoforum', $return);
	}

	if ($jinoforumto->type == 'single') {
		print_error('cannotmovetosinglejinoforum', 'jinoforum', $return);
	}

	// Get target jinoforum cm and check it is visible to current user.
	$modinfo = get_fast_modinfo($course);
	$jinoforums = $modinfo->get_instances_of('jinoforum');
	if (!array_key_exists($jinoforumto->id, $jinoforums)) {
		print_error('cannotmovetonotfound', 'jinoforum', $return);
	}
	$cmto = $jinoforums[$jinoforumto->id];
	if (!$cmto->uservisible) {
		print_error('cannotmovenotvisible', 'jinoforum', $return);
	}

	$destinationctx = context_module::instance($cmto->id);
	require_capability('mod/jinoforum:startdiscussion', $destinationctx);

	if (!jinoforum_move_attachments($discussion, $jinoforum->id, $jinoforumto->id)) {
		echo $OUTPUT->notification("Errors occurred while moving attachment directories - check your file permissions");
	}
	$DB->set_field('jinoforum_discussions', 'jinoforum', $jinoforumto->id, array('id' => $discussion->id));
	$DB->set_field('jinoforum_read', 'jinoforumid', $jinoforumto->id, array('discussionid' => $discussion->id));

	$params = array(
		'context' => $destinationctx,
		'objectid' => $discussion->id,
		'other' => array(
			'fromjinoforumid' => $jinoforum->id,
			'tojinoforumid' => $jinoforumto->id,
		)
	);
	$event = \mod_jinoforum\event\discussion_moved::create($params);
	$event->add_record_snapshot('jinoforum_discussions', $discussion);
	$event->add_record_snapshot('jinoforum', $jinoforum);
	$event->add_record_snapshot('jinoforum', $jinoforumto);
	$event->trigger();

	// Delete the RSS files for the 2 jinoforums to force regeneration of the feeds
	require_once($CFG->dirroot . '/mod/jinoforum/rsslib.php');
	jinoforum_rss_delete_file($jinoforum);
	jinoforum_rss_delete_file($jinoforumto);

	redirect($return . '&moved=-1&sesskey=' . sesskey());
}

$params = array(
	'context' => $modcontext,
	'objectid' => $discussion->id,
);
$event = \mod_jinoforum\event\discussion_viewed::create($params);
$event->add_record_snapshot('jinoforum_discussions', $discussion);
$event->add_record_snapshot('jinoforum', $jinoforum);
$event->trigger();

unset($SESSION->fromdiscussion);

if ($mode) {
	set_user_preference('jinoforum_displaymode', $mode);
}

$displaymode = get_user_preferences('jinoforum_displaymode', $CFG->jinoforum_displaymode);

if ($parent) {
	// If flat AND parent, then force nested display this time
	if ($displaymode == JINOFORUM_MODE_FLATOLDEST or $displaymode == JINOFORUM_MODE_FLATNEWEST) {
		$displaymode = JINOFORUM_MODE_NESTED;
	}
} else {
	$parent = $discussion->firstpost;
}

if (!$post = jinoforum_get_post_full($parent)) {
	print_error("notexists", 'jinoforum', "$CFG->wwwroot/mod/jinoforum/view.php?f=$jinoforum->id");
}
$parentpostid = $post->id;
if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, null, $cm)) {
	print_error('noviewdiscussionspermission', 'jinoforum', "$CFG->wwwroot/mod/jinoforum/view.php?id=$jinoforum->id");
}

if ($mark == 'read' or $mark == 'unread') {
//	if ($CFG->jinoforum_usermarksread && jinoforum_tp_can_track_jinoforums($jinoforum) && jinoforum_tp_is_tracked($jinoforum)) {
		if ($mark == 'read') {
			jinoforum_tp_add_read_record($USER->id, $postid);
		} else {
			// unread
			jinoforum_tp_delete_read_records($USER->id, $postid);
		}
//	}
}

// $searchform = jinoforum_search_form($course);

$jinoforumnode = $PAGE->navigation->find($cm->id, navigation_node::TYPE_ACTIVITY);
if (empty($jinoforumnode)) {
	$jinoforumnode = $PAGE->navbar;
} else {
	$jinoforumnode->make_active();
}
$node = $jinoforumnode->add(format_string($discussion->name), new moodle_url('/mod/jinoforum/discuss.php', array('d' => $discussion->id)));
$node->display = false;
if ($node && $post->id != $discussion->firstpost) {
	$node->add(format_string($post->subject), $PAGE->url);
}

$PAGE->set_title("$course->shortname: " . format_string($discussion->name));
$PAGE->set_heading($course->fullname);
 // $PAGE->set_button($searchform);
echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($jinoforum->name), 2, array('class'=>'board-title'));

/// Check to see if groups are being used in this jinoforum
/// If so, make sure the current person is allowed to see this discussion
/// Also, if we know they should be able to reply, then explicitly set $canreply for performance reasons

$canreply = jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $modcontext);
if (!$canreply and $jinoforum->type !== 'news') {
	if (isguestuser() or ! isloggedin()) {
		$canreply = true;
	}
	if (!is_enrolled($modcontext) and ! is_viewing($modcontext)) {
		// allow guests and not-logged-in to see the link - they are prompted to log in after clicking the link
		// normal users with temporary guest access see this link too, they are asked to enrol instead
		$canreply = enrol_selfenrol_available($course->id);
	}
}

/// Print the controls across the top
echo '<div class="discussioncontrols clearfix">';

if (!empty($CFG->enableportfolios) && has_capability('mod/jinoforum:exportdiscussion', $modcontext)) {
	require_once($CFG->libdir . '/portfoliolib.php');
	$button = new portfolio_add_button();
	$button->set_callback_options('jinoforum_portfolio_caller', array('discussionid' => $discussion->id, 'style'=>'cursor:pointer;'), 'mod_jinoforum');
	$button = $button->to_html(PORTFOLIO_ADD_FULL_FORM, get_string('exportdiscussion', 'mod_jinoforum'));
	$buttonextraclass = '';
	if (empty($button)) {
		// no portfolio plugin available.
		$button = '&nbsp;';
		$buttonextraclass = ' noavailable';
	}
	echo html_writer::tag('div', $button, array('class' => 'discussioncontrol exporttoportfolio' . $buttonextraclass, 'style'=>'cursor:pointer;'));
} else {
	echo html_writer::tag('div', '&nbsp;', array('class' => 'discussioncontrol nullcontrol', 'style'=>'cursor:pointer;'));
}
// groups selector not needed here
/*
  echo '<div class="discussioncontrol displaymode">';
  jinoforum_print_mode_form($discussion->id, $displaymode);
  echo "</div>";
 */
if ($jinoforum->type != 'single' && has_capability('mod/jinoforum:movediscussions', $modcontext)) {

	echo '<div class="discussioncontrol movediscussion">';
	// Popup menu to move discussions to other jinoforums. The discussion in a
	// single discussion jinoforum can't be moved.
	$modinfo = get_fast_modinfo($course);
	echo "</div>";
}
echo '<div class="clearfloat">&nbsp;</div>';
echo "</div>";

if (!empty($jinoforum->blockafter) && !empty($jinoforum->blockperiod)) {
	$a = new stdClass();
	$a->blockafter = $jinoforum->blockafter;
	$a->blockperiod = get_string('secondstotime' . $jinoforum->blockperiod);
	echo $OUTPUT->notification(get_string('thisjinoforumisthrottled', 'jinoforum', $a));
}

if ($jinoforum->type == 'qanda' && !has_capability('mod/jinoforum:viewqandawithoutposting', $modcontext) &&
	!jinoforum_user_has_posted($jinoforum->id, $discussion->id, $USER->id)) {
	echo $OUTPUT->notification(get_string('qandanotify', 'jinoforum'));
}

if ($move == -1 and confirm_sesskey()) {
	echo $OUTPUT->notification(get_string('discussionmoved', 'jinoforum', format_string($jinoforum->name, true)));
}

$canrate = has_capability('mod/jinoforum:rate', $modcontext);

 jinoforum_print_discussion($course, $cm, $jinoforum, $discussion, $post, $displaymode, $canreply, $canrate, $perpage ,$page , $searchtype , $searchval);

echo $OUTPUT->footer();



