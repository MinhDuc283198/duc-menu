<?php

require_once('../../config.php');
require_once('lib.php');


$p = optional_param('post', 0, PARAM_INT);				// post ID

	$post = $DB->get_record('jinoforum_posts', array('id' => $p), 'id, discussion, recommendcnt', MUST_EXIST);
	$discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion), 'id, course, jinoforum', MUST_EXIST);
	$course = $DB->get_record('course', array('id' => $discussion->course), 'id', MUST_EXIST);
	$jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum), 'id', MUST_EXIST);

$recommend = new stdClass();
$recommend->userid = $USER->id;
$recommend->jinoforumid = $jinoforum->id;
$recommend->discussionid = $discussion->id;
$recommend->postid = $p;
$recommend->timerecommend = time();
	
if (!$DB->get_records("jinoforum_recommend", array('postid' => $p , 'userid'=>$USER->id))) {
	$DB->insert_record("jinoforum_recommend", $recommend);
	$DB->set_field_select('jinoforum_posts', 'recommendcnt', intval($post->recommendcnt) + 1, " id='$post->id'");
}

redirect(new moodle_url('/mod/jinoforum/post_view.php', array('post' => $p)));
