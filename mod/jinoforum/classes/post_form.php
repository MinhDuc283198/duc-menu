<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the form definition to post in the jinoforum.
 *
 * @package   mod_jinoforum
 * @copyright Jamie Pratt <me@jamiep.org>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/repository/lib.php');

/**
 * Class to post in a jinoforum.
 *
 * @package   mod_jinoforum
 * @copyright Jamie Pratt <me@jamiep.org>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_jinoforum_post_form extends moodleform {

    /**
     * Returns the options array to use in filemanager for jinoforum attachments
     *
     * @param stdClass $jinoforum
     * @return array
     */
    public static function attachment_options($jinoforum) {
        global $COURSE, $PAGE, $CFG;
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes, $jinoforum->maxbytes);
        return array(
            'subdirs' => 0,
            'maxbytes' => $maxbytes,
            'maxfiles' => $jinoforum->maxattachments,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL
        );
    }

    /**
     * Returns the options array to use in jinoforum text editor
     *
     * @param context_module $context
     * @param int $postid post id, use null when adding new post
     * @return array
     */
    public static function editor_options(context_module $context, $postid) {
        global $COURSE, $PAGE, $CFG;
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'mod_jinoforum', 'post', $postid)
        );
    }

    /**
     * Form definition
     *
     * @return void
     */
    function definition() {
        global $CFG, $OUTPUT, $DB;

        $mform = & $this->_form;

        $course = $this->_customdata['course'];
        $cm = $this->_customdata['cm'];
        $coursecontext = $this->_customdata['coursecontext'];
        $modcontext = $this->_customdata['modcontext'];
        $jinoforum = $this->_customdata['jinoforum'];
        $post = $this->_customdata['post'];
        $edit = $this->_customdata['edit'];
        $thresholdwarning = $this->_customdata['thresholdwarning'];
        $discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion));

        $mform->addElement('header', 'general', ''); //fill in the data depending on page params later using set_data
        // If there is a warning message and we are not editing a post we need to handle the warning.
        if (!empty($thresholdwarning) && !$edit) {
            // Here we want to display a warning if they can still post but have reached the warning threshold.
            if ($thresholdwarning->canpost) {
                $message = get_string($thresholdwarning->errorcode, $thresholdwarning->module, $thresholdwarning->additional);
                $mform->addElement('html', $OUTPUT->notification($message));
            }
        }

        $mform->addElement('text', 'subject', get_string('subject', 'jinoforum'), 'size="48"');
        $mform->setType('subject', PARAM_TEXT);
        $mform->addRule('subject', get_string('required'), 'required', null, 'client');
        $mform->addRule('subject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('editor', 'message', get_string('message', 'jinoforum'), null, self::editor_options($modcontext, (empty($post->id) ? null : $post->id)));
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', get_string('required'), 'required', null, 'client');

        if (isset($jinoforum->id) && jinoforum_is_forcesubscribed($jinoforum)) {

            $mform->addElement('hidden', 'subscribemessage', get_string('subscription', 'jinoforum'), get_string('everyoneissubscribed', 'jinoforum'));
            $mform->addElement('hidden', 'subscribe');
            $mform->setType('subscribe', PARAM_INT);
            $mform->addHelpButton('subscribemessage', 'subscription', 'jinoforum');
        } else if (isset($jinoforum->forcesubscribe) && $jinoforum->forcesubscribe != JINOFORUM_DISALLOWSUBSCRIBE ||
                has_capability('moodle/course:manageactivities', $coursecontext)) {

            $options = array();
            $options[0] = get_string('subscribestop', 'jinoforum');
            $options[1] = get_string('subscribestart', 'jinoforum');

            $mform->addElement('select', 'subscribe', get_string('subscription', 'jinoforum'), $options);
            $mform->addHelpButton('subscribe', 'subscription', 'jinoforum');
        } else if ($jinoforum->forcesubscribe == JINOFORUM_DISALLOWSUBSCRIBE) {
            $mform->addElement('static', 'subscribemessage', get_string('subscription', 'jinoforum'), get_string('disallowsubscribe', 'jinoforum'));
            $mform->addElement('hidden', 'subscribe');
            $mform->setType('subscribe', PARAM_INT);
            $mform->addHelpButton('subscribemessage', 'subscription', 'jinoforum');
        }
        /*         * JB* */
        $reply = optional_param('reply', 0, PARAM_INT);
        if (!$reply) {
            $chked1 = "";
            $chked2 = "";
            $scr = "";
            if ($discussion->ispublic == 1 || !$discussion) {
                $chked1 = "checked";
            } else {
                $chked2 = "checked";
                $scr = '<script> $("#opener").show(); </script>';
            }
            $allowusers = $DB->get_records('jinoforum_users', array('discussionid' => $discussion->id));
            $userary = "";
            $username = "";
            foreach ($allowusers as $allowuser) {
                $userary .= "[" . $allowuser->userid . "]";
                $usernames .= "<span id='username" . $allowuser->userid . "'>" . fullname($DB->get_record('user', array('id' => $allowuser->userid))) . "<span class='deletex' onclick='" . 'delete_user(' . $allowuser->userid . ')' . "'>x</span></span>";
            }
            $mform->addElement('static', 'is_public', get_string('public_distinction', 'jinoforum'), '<input type="radio" class="fl" name="is_public" ' . $chked1 . ' value="1" /><span class="publicchk">' . get_string('public', 'jinoforum') . '</span><input type="radio" ' . $chked2 . ' class="fl" name="is_public" value="0" /><span class="bt">' . get_string('closed', 'jinoforum') . '</span>&nbsp;&nbsp;&nbsp;<input type="button" id="opener" style="display : none;" value="' . get_string('studentindiscussion', 'jinoforum') . '" />' . $scr);
            $mform->addElement('static', 'userallow', '', '<span id="allow_user" style="color:#333;display:none">참여자 : ' . $usernames . '</span>');

            $mform->addElement('hidden', 'allow_user');
            $mform->setDefault('allow_user', $userary);

            $mform->addElement('hidden', 'ispublic');
            if ($discussion) {
                $mform->setDefault('ispublic', $discussion->ispublic);
            } else {
                $mform->setDefault('ispublic', 1);
            }
        }
        /*         * JB* */

        if (!empty($jinoforum->maxattachments) && $jinoforum->maxbytes != 1 && has_capability('mod/jinoforum:createattachment', $modcontext)) {  //  1 = No attachments at all
          
            $chk = "<input type='checkbox' name='view_filemanager' id='jinoforumfilecheck'/> <label for='jinoforumfilecheck'>" . get_string('attachmentcheck', 'local_jinoboard')."</label>";

            $mform->addElement('static', 'static', get_string('attachment', 'jinotechboard'), $chk);

            $staticmsg = "<br><span>" . get_string('attachmentmsg', 'local_jinoboard') . "</span>";
            $mform->addGroup(array(
                $mform->createElement('filemanager', 'attachments', get_string('attachment', 'jinoforum'), null, self::attachment_options($jinoforum)),
                $mform->createElement('static', 'text', '', $staticmsg)
                    ), 'filemanager', '', array(''), false);
        }

        if (empty($post->id) && has_capability('moodle/course:manageactivities', $coursecontext)) { // hack alert
            //	$mform->addElement('checkbox', 'mailnow', get_string('mailnow', 'jinoforum'));
        }

        if (!empty($CFG->jinoforum_enabletimedposts) && !$post->parent && has_capability('mod/jinoforum:viewhiddentimedposts', $coursecontext)) { // hack alert
            $mform->addElement('header', 'displayperiod', get_string('displayperiod', 'jinoforum'));

            $mform->addElement('date_selector', 'timestart', get_string('displaystart', 'jinoforum'), array('optional' => true));
            $mform->addHelpButton('timestart', 'displaystart', 'jinoforum');

            $mform->addElement('date_selector', 'timeend', get_string('displayend', 'jinoforum'), array('optional' => true));
            $mform->addHelpButton('timeend', 'displayend', 'jinoforum');
        } else {
            $mform->addElement('hidden', 'timestart');
            $mform->setType('timestart', PARAM_INT);
            $mform->addElement('hidden', 'timeend');
            $mform->setType('timeend', PARAM_INT);
            $mform->setConstants(array('timestart' => 0, 'timeend' => 0));
        }

        if ($groupmode = groups_get_activity_groupmode($cm, $course)) { // hack alert
            $groupdata = groups_get_activity_allowed_groups($cm);
            $groupcount = count($groupdata);
            $groupinfo = array();
            $modulecontext = context_module::instance($cm->id);

            // Check whether the user has access to all groups in this jinoforum from the accessallgroups cap.
            if ($groupmode == VISIBLEGROUPS || has_capability('moodle/site:accessallgroups', $modulecontext)) {
                // Only allow posting to all groups if the user has access to all groups.
                $groupinfo = array('0' => get_string('allparticipants'));
                $groupcount++;
            }

            $contextcheck = has_capability('mod/jinoforum:movediscussions', $modulecontext) && empty($post->parent) && $groupcount > 1;
            if ($contextcheck) {
                foreach ($groupdata as $grouptemp) {
                    $groupinfo[$grouptemp->id] = $grouptemp->name;
                }
                $mform->addElement('select', 'groupinfo', get_string('group'), $groupinfo);
                $mform->setDefault('groupinfo', $post->groupid);
                $mform->setType('groupinfo', PARAM_INT);
            } else {
                if (empty($post->groupid)) {
                    $groupname = get_string('allparticipants');
                } else {
                    $groupname = format_string($groupdata[$post->groupid]->name);
                }
                $mform->addElement('static', 'groupinfo', get_string('group'), $groupname);
            }
        }
        //-------------------------------------------------------------------------------
        // buttons
        if (isset($post->edit)) { // hack alert
            $submit_string = get_string('savechanges');
        } else {
            $submit_string = get_string('posttojinoforum', 'jinoforum');
        }
        $this->add_action_buttons(true, $submit_string);
  
        $mform->addElement('hidden', 'course');
        $mform->setType('course', PARAM_INT);

        $mform->addElement('hidden', 'jinoforum');
        $mform->setType('jinoforum', PARAM_INT);

        $mform->addElement('hidden', 'discussion');
        $mform->setType('discussion', PARAM_INT);

        $mform->addElement('hidden', 'parent');
        $mform->setType('parent', PARAM_INT);

        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);

        $mform->addElement('hidden', 'groupid');
        $mform->setType('groupid', PARAM_INT);

        $mform->addElement('hidden', 'edit');
        $mform->setType('edit', PARAM_INT);

        $mform->addElement('hidden', 'reply');
        $mform->setType('reply', PARAM_INT);
    }

    /**
     * Form validation
     *
     * @param array $data data from the form.
     * @param array $files files uploaded.
     * @return array of errors.
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        if (($data['timeend'] != 0) && ($data['timestart'] != 0) && $data['timeend'] <= $data['timestart']) {
            $errors['timeend'] = get_string('timestartenderror', 'jinoforum');
        }
        if (empty($data['message']['text'])) {
            $errors['message'] = get_string('erroremptymessage', 'jinoforum');
        }
        if (empty($data['subject'])) {
            $errors['subject'] = get_string('erroremptysubject', 'jinoforum');
        }
        return $errors;
    }

}
