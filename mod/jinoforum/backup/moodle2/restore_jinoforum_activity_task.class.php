<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_jinoforum
 * @subpackage backup-moodle2
 * @copyright  2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/jinoforum/backup/moodle2/restore_jinoforum_stepslib.php'); // Because it exists (must)

/**
 * jinoforum restore task that provides all the settings and steps to perform one
 * complete restore of the activity
 */
class restore_jinoforum_activity_task extends restore_activity_task {

    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        // No particular settings for this activity
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        // Choice only has one structure step
        $this->add_step(new restore_jinoforum_activity_structure_step('jinoforum_structure', 'jinoforum.xml'));
    }

    /**
     * Define the contents in the activity that must be
     * processed by the link decoder
     */
    static public function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('jinoforum', array('intro'), 'jinoforum');
        $contents[] = new restore_decode_content('jinoforum_posts', array('message'), 'jinoforum_post');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();

        // List of jinoforums in course
        $rules[] = new restore_decode_rule('JINOFORUMINDEX', '/mod/jinoforum/index.php?id=$1', 'course');
        // Jinoforum by cm->id and jinoforum->id
        $rules[] = new restore_decode_rule('JINOFORUMVIEWBYID', '/mod/jinoforum/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('JINOFORUMVIEWBYF', '/mod/jinoforum/view.php?f=$1', 'jinoforum');
        // Link to jinoforum discussion
        $rules[] = new restore_decode_rule('JINOFORUMDISCUSSIONVIEW', '/mod/jinoforum/discuss.php?d=$1', 'jinoforum_discussion');
        // Link to discussion with parent and with anchor posts
        $rules[] = new restore_decode_rule('JINOFORUMDISCUSSIONVIEWPARENT', '/mod/jinoforum/discuss.php?d=$1&parent=$2',
                                           array('jinoforum_discussion', 'jinoforum_post'));
        $rules[] = new restore_decode_rule('JINOFORUMDISCUSSIONVIEWINSIDE', '/mod/jinoforum/discuss.php?d=$1#$2',
                                           array('jinoforum_discussion', 'jinoforum_post'));

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * jinoforum logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();

        $rules[] = new restore_log_rule('jinoforum', 'add', 'view.php?id={course_module}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'update', 'view.php?id={course_module}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'view', 'view.php?id={course_module}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'view jinoforum', 'view.php?id={course_module}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'mark read', 'view.php?f={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'start tracking', 'view.php?f={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'stop tracking', 'view.php?f={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'subscribe', 'view.php?f={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'unsubscribe', 'view.php?f={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'subscriber', 'subscribers.php?id={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'subscribers', 'subscribers.php?id={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'view subscribers', 'subscribers.php?id={jinoforum}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'add discussion', 'discuss.php?d={jinoforum_discussion}', '{jinoforum_discussion}');
        $rules[] = new restore_log_rule('jinoforum', 'view discussion', 'discuss.php?d={jinoforum_discussion}', '{jinoforum_discussion}');
        $rules[] = new restore_log_rule('jinoforum', 'move discussion', 'discuss.php?d={jinoforum_discussion}', '{jinoforum_discussion}');
        $rules[] = new restore_log_rule('jinoforum', 'delete discussi', 'view.php?id={course_module}', '{jinoforum}',
                                        null, 'delete discussion');
        $rules[] = new restore_log_rule('jinoforum', 'delete discussion', 'view.php?id={course_module}', '{jinoforum}');
        $rules[] = new restore_log_rule('jinoforum', 'add post', 'discuss.php?d={jinoforum_discussion}&parent={jinoforum_post}', '{jinoforum_post}');
        $rules[] = new restore_log_rule('jinoforum', 'update post', 'discuss.php?d={jinoforum_discussion}#p{jinoforum_post}&parent={jinoforum_post}', '{jinoforum_post}');
        $rules[] = new restore_log_rule('jinoforum', 'update post', 'discuss.php?d={jinoforum_discussion}&parent={jinoforum_post}', '{jinoforum_post}');
        $rules[] = new restore_log_rule('jinoforum', 'prune post', 'discuss.php?d={jinoforum_discussion}', '{jinoforum_post}');
        $rules[] = new restore_log_rule('jinoforum', 'delete post', 'discuss.php?d={jinoforum_discussion}', '[post]');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();

        $rules[] = new restore_log_rule('jinoforum', 'view jinoforums', 'index.php?id={course}', null);
        $rules[] = new restore_log_rule('jinoforum', 'subscribeall', 'index.php?id={course}', '{course}');
        $rules[] = new restore_log_rule('jinoforum', 'unsubscribeall', 'index.php?id={course}', '{course}');
        $rules[] = new restore_log_rule('jinoforum', 'user report', 'user.php?course={course}&id={user}&mode=[mode]', '{user}');
        $rules[] = new restore_log_rule('jinoforum', 'search', 'search.php?id={course}&search=[searchenc]', '[search]');

        return $rules;
    }
}
