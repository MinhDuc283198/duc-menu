<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_jinoforum
 * @subpackage backup-moodle2
 * @copyright  2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_jinoforum_activity_task
 */

/**
 * Structure step to restore one jinoforum activity
 */
class restore_jinoforum_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('jinoforum', '/activity/jinoforum');
        if ($userinfo) {
            $paths[] = new restore_path_element('jinoforum_discussion', '/activity/jinoforum/discussions/discussion');
            $paths[] = new restore_path_element('jinoforum_post', '/activity/jinoforum/discussions/discussion/posts/post');
			$paths[] = new restore_path_element('jinoforum_comment', '/activity/jinoforum/discussions/discussion/posts/post/comments/comment');
			$paths[] = new restore_path_element('jinoforum_recommend', '/activity/jinoforum/discussions/discussion/posts/post/recommends/recommend');
            $paths[] = new restore_path_element('jinoforum_rating', '/activity/jinoforum/discussions/discussion/posts/post/ratings/rating');
            $paths[] = new restore_path_element('jinoforum_subscription', '/activity/jinoforum/subscriptions/subscription');
            $paths[] = new restore_path_element('jinoforum_digest', '/activity/jinoforum/digests/digest');
            $paths[] = new restore_path_element('jinoforum_read', '/activity/jinoforum/readposts/read');
            $paths[] = new restore_path_element('jinoforum_track', '/activity/jinoforum/trackedprefs/track');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_jinoforum($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->assesstimestart = $this->apply_date_offset($data->assesstimestart);
        $data->assesstimefinish = $this->apply_date_offset($data->assesstimefinish);
        if ($data->scale < 0) { // scale found, get mapping
            $data->scale = -($this->get_mappingid('scale', abs($data->scale)));
        }

        $newitemid = $DB->insert_record('jinoforum', $data);
        $this->apply_activity_instance($newitemid);
    }

    protected function process_jinoforum_discussion($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->jinoforum = $this->get_new_parentid('jinoforum');
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        $data->timestart = $this->apply_date_offset($data->timestart);
        $data->timeend = $this->apply_date_offset($data->timeend);
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->groupid = $this->get_mappingid('group', $data->groupid);
        $data->usermodified = $this->get_mappingid('user', $data->usermodified);

        $newitemid = $DB->insert_record('jinoforum_discussions', $data);
        $this->set_mapping('jinoforum_discussion', $oldid, $newitemid);
    }

    protected function process_jinoforum_post($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->discussion = $this->get_new_parentid('jinoforum_discussion');
        $data->created = $this->apply_date_offset($data->created);
        $data->modified = $this->apply_date_offset($data->modified);
        $data->userid = $this->get_mappingid('user', $data->userid);
        // If post has parent, map it (it has been already restored)
        if (!empty($data->parent)) {
            $data->parent = $this->get_mappingid('jinoforum_post', $data->parent);
        }

        $newitemid = $DB->insert_record('jinoforum_posts', $data);
        $this->set_mapping('jinoforum_post', $oldid, $newitemid, true);

        // If !post->parent, it's the 1st post. Set it in discussion
        if (empty($data->parent)) {
            $DB->set_field('jinoforum_discussions', 'firstpost', $newitemid, array('id' => $data->discussion));
        }
    }

    protected function process_jinoforum_rating($data) {
        global $DB;

        $data = (object)$data;

        // Cannot use ratings API, cause, it's missing the ability to specify times (modified/created)
        $data->contextid = $this->task->get_contextid();
        $data->itemid    = $this->get_new_parentid('jinoforum_post');
        if ($data->scaleid < 0) { // scale found, get mapping
            $data->scaleid = -($this->get_mappingid('scale', abs($data->scaleid)));
        }
        $data->rating = $data->value;
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // We need to check that component and ratingarea are both set here.
        if (empty($data->component)) {
            $data->component = 'mod_jinoforum';
        }
        if (empty($data->ratingarea)) {
            $data->ratingarea = 'post';
        }

        $newitemid = $DB->insert_record('rating', $data);
    }

    protected function process_jinoforum_subscription($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->jinoforum = $this->get_new_parentid('jinoforum');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('jinoforum_subscriptions', $data);
    }

    protected function process_jinoforum_digest($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->jinoforum = $this->get_new_parentid('jinoforum');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('jinoforum_digests', $data);
    }
	
	protected function process_jinoforum_comment($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

		$data->postid = $this->get_mappingid('jinoforum_post', $data->postid);
        $data->jinoforumid = $this->get_new_parentid('jinoforum');
        $data->discussionid = $this->get_mappingid('jinoforum_discussion', $data->discussionid);
        $data->userid = $this->get_mappingid('user', $data->userid);
		
        $newitemid = $DB->insert_record('jinoforum_comments', $data);
    }
	
	protected function process_jinoforum_recommend($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
		
		$data->userid = $this->get_mappingid('user', $data->userid);
        $data->jinoforumid = $this->get_new_parentid('jinoforum');
        $data->discussionid = $this->get_mappingid('jinoforum_discussion', $data->discussionid); 
        $data->postid = $this->get_mappingid('jinoforum_post', $data->postid);
        

        $newitemid = $DB->insert_record('jinoforum_recommend', $data);
    }

    protected function process_jinoforum_read($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->jinoforumid = $this->get_new_parentid('jinoforum');
        $data->discussionid = $this->get_mappingid('jinoforum_discussion', $data->discussionid);
        $data->postid = $this->get_mappingid('jinoforum_post', $data->postid);
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('jinoforum_read', $data);
    }

    protected function process_jinoforum_track($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->jinoforumid = $this->get_new_parentid('jinoforum');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('jinoforum_track_prefs', $data);
    }

    protected function after_execute() {
        global $DB;

        // Add jinoforum related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_jinoforum', 'intro', null);

        // If the jinoforum is of type 'single' and no discussion has been ignited
        // (non-userinfo backup/restore) create the discussion here, using jinoforum
        // information as base for the initial post.
        $jinoforumid = $this->task->get_activityid();
        $jinoforumrec = $DB->get_record('jinoforum', array('id' => $jinoforumid));
        if ($jinoforumrec->type == 'single' && !$DB->record_exists('jinoforum_discussions', array('jinoforum' => $jinoforumid))) {
            // Create single discussion/lead post from jinoforum data
            $sd = new stdclass();
            $sd->course   = $jinoforumrec->course;
            $sd->jinoforum    = $jinoforumrec->id;
            $sd->name     = $jinoforumrec->name;
            $sd->assessed = $jinoforumrec->assessed;
            $sd->message  = $jinoforumrec->intro;
            $sd->messageformat = $jinoforumrec->introformat;
            $sd->messagetrust  = true;
            $sd->mailnow  = false;
            $sdid = jinoforum_add_discussion($sd, null, null, $this->task->get_userid());
            // Mark the post as mailed
            $DB->set_field ('jinoforum_posts','mailed', '1', array('discussion' => $sdid));
            // Copy all the files from mod_foum/intro to mod_jinoforum/post
            $fs = get_file_storage();
            $files = $fs->get_area_files($this->task->get_contextid(), 'mod_jinoforum', 'intro');
            foreach ($files as $file) {
                $newfilerecord = new stdclass();
                $newfilerecord->filearea = 'post';
                $newfilerecord->itemid   = $DB->get_field('jinoforum_discussions', 'firstpost', array('id' => $sdid));
                $fs->create_file_from_storedfile($newfilerecord, $file);
            }
        }

        // Add post related files, matching by itemname = 'jinoforum_post'
        $this->add_related_files('mod_jinoforum', 'post', 'jinoforum_post');
        $this->add_related_files('mod_jinoforum', 'attachment', 'jinoforum_post');
    }
}
