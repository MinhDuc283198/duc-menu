<?php
require_once('../../config.php');
require_once('lib.php');

$search = optional_param('search', '', PARAM_CLEAN);
$courseid = optional_param('courseid', 0, PARAM_INT);
$usedid = rtrim(optional_param('usedid', "", PARAM_CLEAN), ',');
$cmid =  optional_param('cmid', 0, PARAM_INT);
$modcontext = context_module::instance($cmid);

$usedid = explode(',', $usedid);
if ($users = jino_coursein_user($courseid, trim($search))) {
    $i = 1;
    echo "<br />";
    foreach ($users as $user) {
        $continue = "";
        foreach ($usedid as $id => $val)
            if ($user->id == $val)
                $continue = true;
        if ($continue)
            continue;
        ?>
        <label><div style="
                    padding:5px 0px; line-height: 24px; border-bottom: 1px solid #ccc; 
                    background-color: #fafafa;
                    color: #676767;
                    ">
                <input type="checkbox" class="user_add_btn" name="user<?php echo $i; ?>" value="<?php echo $user->id; ?>" /><?php echo fullname($user); ?>
        <?php if (has_capability('mod/jinoforum:addinstance', $modcontext)) {
            echo "(" . $user->username . ")";
        }
        ?><br />
                <input type="hidden" name="username<?php echo $i; ?>" value="<?php echo fullname($user); ?>">
            </div></label>
        <?php
        $i++;
    }
}
    