<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once('lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID
$f = optional_param('f', 0, PARAM_INT);  // Jinoforum ID
$mode = optional_param('mode', 0, PARAM_INT);  // Display mode (for single jinoforum)
$changegroup = optional_param('group', -1, PARAM_INT);   // choose the current group
$page = optional_param('page', 1, PARAM_INT);  // which page to show
$search = optional_param('search', '', PARAM_CLEAN); // search string
$perpage = optional_param('perpage', 10, PARAM_INT); //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수

$params = array();
if ($id) {
    $params['id'] = $id;
} else {
    $params['f'] = $f;
}
if ($page) {
    $params['page'] = $page;
}
if ($search) {
    $params['search'] = $search;
}
$PAGE->set_url('/mod/jinoforum/view.php', $params);

if ($id) {
    if (!$cm = get_coursemodule_from_id('jinoforum', $id)) {
        print_error('invalidcoursemodule');
    }
    if (!$course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }
    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $cm->instance))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if ($jinoforum->type == 'single') {
        $PAGE->set_pagetype('mod-jinoforum-discuss');
    }
    // move require_course_login here to use forced language for course
    // fix for MDL-6926
    require_course_login($course, true, $cm);
    $strjinoforums = get_string("modulenameplural", "jinoforum");
    $strjinoforum = get_string("modulename", "jinoforum");
} else if ($f) {

    if (!$jinoforum = $DB->get_record("jinoforum", array("id" => $f))) {
        print_error('invalidjinoforumid', 'jinoforum');
    }
    if (!$course = $DB->get_record("course", array("id" => $jinoforum->course))) {
        print_error('coursemisconf');
    }

    if (!$cm = get_coursemodule_from_instance("jinoforum", $jinoforum->id, $course->id)) {
        print_error('missingparameter');
    }
    // move require_course_login here to use forced language for course
    // fix for MDL-6926
    require_course_login($course, true, $cm);
    $strjinoforums = get_string("modulenameplural", "jinoforum");
    $strjinoforum = get_string("modulename", "jinoforum");
} else {
    print_error('missingparameter');
}


$context = context_module::instance($cm->id);
$PAGE->set_context($context);

if (!empty($CFG->enablerssfeeds) && !empty($CFG->jinoforum_enablerssfeeds) && $jinoforum->rsstype && $jinoforum->rssarticles) {
    require_once("$CFG->libdir/rsslib.php");

    $rsstitle = format_string($course->shortname, true, array('context' => context_course::instance($course->id))) . ': ' . format_string($jinoforum->name);
    rss_add_http_header($context, 'mod_jinoforum', $jinoforum, $rsstitle);
}

// Mark viewed if required
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

/// Print header.

$PAGE->set_title($jinoforum->name);
$PAGE->add_body_class('jinoforumtype-' . $jinoforum->type);
$PAGE->set_heading($course->fullname);

$totalcount = $DB->count_records('jinoforum_discussions', array('jinoforum' => $jinoforum->id));
$total_pages = jinoforum_get_total_pages($totalcount, $perpage);

echo $OUTPUT->header();

/// Some capability checks.
if (empty($cm->visible) and ! has_capability('moodle/course:viewhiddenactivities', $context)) {
    notice(get_string("activityiscurrentlyhidden"));
}

if (!has_capability('mod/jinoforum:viewdiscussion', $context)) {
    notice(get_string('noviewdiscussionspermission', 'jinoforum'));
}
//$bookmark = $DB->get_record('bookmark', array('activity' => 'jinoforum', 'course' => $course->id, 'instance' => $jinoforum->id, 'userid' => $USER->id), 'id');
//$bookmarking = 0;
//if ($bookmark->id) {
//    $bookmarking = 1;
//}
$title = $jinoforum->name;
//$title .= ($bookmarking) ? '&nbsp;<span id="bookmark_ico" style="color:orange; cursor:pointer; text-shadow:1px 1px #999;">★</span>' : '&nbsp;<span id="bookmark_ico" style="color:orange; cursor:pointer; text-shadow:1px 1px #999;">☆</span>';

echo $OUTPUT->heading($title, 2,array('class'=>'board-title'));
if (!empty($jinoforum->intro) && $jinoforum->type != 'single' && $jinoforum->type != 'teacher') {
    echo $OUTPUT->box(format_module_intro('jinoforum', $jinoforum, $cm->id), 'generalbox', 'intro');
}

/// find out current groups mode
groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/jinoforum/view.php?id=' . $cm->id);

$params = array(
    'context' => $context,
    'objectid' => $jinoforum->id
);
$event = \mod_jinoforum\event\course_module_viewed::create($params);
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('jinoforum', $jinoforum);
$event->trigger();

$SESSION->fromdiscussion = qualified_me();   // Return here if we post or set subscription etc
/// Print settings and things across the top
// If it's a simple single discussion jinoforum, we need to print the display
// mode control.
if ($jinoforum->type == 'single') {
    $discussion = NULL;
    $discussions = $DB->get_records('jinoforum_discussions', array('jinoforum' => $jinoforum->id), 'timemodified ASC');
    if (!empty($discussions)) {
        $discussion = array_pop($discussions);
    }
    if ($discussion) {
        if ($mode) {
            set_user_preference("jinoforum_displaymode", $mode);
        }
        $displaymode = get_user_preferences("jinoforum_displaymode", $CFG->jinoforum_displaymode);
        jinoforum_print_mode_form($jinoforum->id, $displaymode, $jinoforum->type);
    }
}

if (!empty($jinoforum->blockafter) && !empty($jinoforum->blockperiod)) {
    $a = new stdClass();
    $a->blockafter = $jinoforum->blockafter;
    $a->blockperiod = get_string('secondstotime' . $jinoforum->blockperiod);
    echo $OUTPUT->notification(get_string('thisjinoforumisthrottled', 'jinoforum', $a));
}

if ($jinoforum->type == 'qanda' && !has_capability('moodle/course:manageactivities', $context)) {
    echo $OUTPUT->notification(get_string('qandanotify', 'jinoforum'));
}

switch ($jinoforum->type) {
    case 'single':
        if (!empty($discussions) && count($discussions) > 1) {
            echo $OUTPUT->notification(get_string('warnformorepost', 'jinoforum'));
        }
        if (!$post = jinoforum_get_post_full($discussion->firstpost)) {
            print_error('cannotfindfirstpost', 'jinoforum');
        }
        if ($mode) {
            set_user_preference("jinoforum_displaymode", $mode);
        }

        $canreply = jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $context);
        $canrate = has_capability('mod/jinoforum:rate', $context);
        $displaymode = get_user_preferences("jinoforum_displaymode", $CFG->jinoforum_displaymode);

        echo '&nbsp;'; // this should fix the floating in FF
        jinoforum_print_discussion($course, $cm, $jinoforum, $discussion, $post, $displaymode, $canreply, $canrate);
        break;



    case 'eachuser':
        echo '<p class="mdl-align">';
        if (jinoforum_user_can_post_discussion($jinoforum, null, -1, $cm)) {
            print_string("allowsdiscussions", "jinoforum");
        } else {
            echo '&nbsp;';
        }
        echo '</p>';
        jinoforum_print_latest_discussions($course, $jinoforum, -1, 'header', '', -1, -1, $page - 1, $perpage, $cm, $totalcount);
        break;

    case 'teacher':
        jinoforum_print_latest_discussions($course, $jinoforum, -1, 'header', '', -1, -1, $page - 1, $perpage, $cm, $totalcount);
        break;

    case 'blog':
        echo '<br />';
        jinoforum_print_latest_discussions($course, $jinoforum, -1, 'plain', '', -1, -1, $page - 1, $perpage, $cm, $totalcount);
        break;

    default:
        echo '<br />';
        jinoforum_print_latest_discussions($course, $jinoforum, -1, 'header', '', -1, -1, $page - 1, $perpage, $cm, $totalcount);


        break;
}

$page_params = array();
$page_params['id'] = $id;
$page_params['perpage'] = $perpage;

echo html_writer::start_tag('div', array('class' => 'table-footer-area'));
jinoforum_get_paging_bar($totalcount, $page, $perpage, 'javascript:goto_page(:page);', $pagerange);
echo html_writer::end_tag('div');
echo $OUTPUT->footer($course);
?>
<script type="text/javascript">
    $("#bookmark_ico").click(function () {
        var url = "<?php echo $CFG->wwwroot; ?>/local/bookmark/ajax/add_new_bookmark.php?course=<?php echo $course->id; ?>&instance=<?php echo $jinoforum->id; ?>&activity=jinoforum";

        $.ajax({url: url,
            success: function (result) {
                $("#bookmark_ico").html(result);
            }
        });
    });
    function goto_page(page) {
        $('input[name=page]').val(page);
        $('#frm_search').submit();
    }
</script>    

