<?php

require_once('../../config.php');
require_once('lib.php');


$p = optional_param('post', 0, PARAM_INT);				// post ID
$comment_value = optional_param('comment_value', "", PARAM_TEXT);
$type = optional_param('type', 'write', PARAM_TEXT);
$commentid = optional_param('id', 0, PARAM_INT);

if ($type == "write") {

	$post = $DB->get_record('jinoforum_posts', array('id' => $p), 'id, discussion, recommendcnt', MUST_EXIST);
	$discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion), 'id, course, jinoforum', MUST_EXIST);
	$course = $DB->get_record('course', array('id' => $discussion->course), 'id', MUST_EXIST);
	$jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum), 'id', MUST_EXIST);

	$comment = new stdClass();
	$comment->jinoforumid = $jinoforum->id;
	$comment->discussionid = $discussion->id;
	$comment->postid = $p;
	$comment->userid = $USER->id;

	$comment->comments = $comment_value;
	$comment->timecreated = time();
} else if ($type == 'modify') {
	$comment->id = $commentid;
	$comment->comments = $comment_value;
} else {
	$DB->delete_records("jinoforum_comments", array('id' => $commentid));
	$DB->set_field_select('jinoforum_posts', 'commentscount', intval($post->commentscount) - 1, " id='$post->id'");
	redirect(new moodle_url('/mod/jinoforum/post_view.php', array('post' => $p)));
}
$comment->timemodified = time();

if (!$DB->get_records("jinoforum_comments", array('id' => $commentid))) {
	$DB->insert_record('jinoforum_comments', $comment);
	$DB->set_field_select('jinoforum_posts', 'commentscount', intval($post->commentscount) + 1, " id='$post->id'");
} else {
	$DB->update_record('jinoforum_comments', $comment);
}

redirect(new moodle_url('/mod/jinoforum/post_view.php', array('post' => $p)));
