<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'jinoforum', language 'ko', branch 'MOODLE_27_STABLE'
 *
 * @package   jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activityoverview'] = '새 게시판(토론) 게시글이 있습니다.';
$string['addanewdiscussion'] = '새 토론 주제 추가';
$string['addanewquestion'] = '새 질문 추가';
$string['addanewtopic'] = '새로운 주제 추가';
$string['addopinion'] = '의견 등록';
$string['advancedsearch'] = '상세 검색';
$string['alljinoforums'] = '모든 게시판(토론)';
$string['allowdiscussions'] = '{$a} 가 이 게시판(토론)에 게시할 수 있도록 할까요?';
$string['allowsallsubscribe'] = '모든 사람에게 구독여부의 선택권을 부여함';
$string['allowsdiscussions'] = '개인당 하나의 주제로 토론을 시작할 수 있도록 허용함';
$string['allsubscribe'] = '모든 게시판(토론) 구독';
$string['allunsubscribe'] = '모든 게시판(토론) 구독 해지';
$string['alreadyfirstpost'] = '이미 토론의 발제가 되어있음';
$string['anyfile'] = '모든 파일';
$string['areaattachment'] = '첨부물';
$string['areapost'] = '내용';
$string['attachment'] = '첨부';
$string['attachment_help'] = '여러분은 선택적으로 게시판(토론) 게시글에 하나 이상의 파일을 첨부할 수 있습니다. 만일 이미지를 첨부하면 메세지 다음에 표시될 것입니다.

';
$string['bstyle_thred'] = 'Thread 형식';
$string['bstyle_list'] = '게시판 형식';
$string['attachmentnopost'] = '게시 ID 없이 첨부물을 내보낼 수 없음';
$string['attachments'] = '첨부물';
$string['attachmentswordcount'] = '첨부 및 단어 수';
$string['blockafter'] = '차단하기 전 게시 한도';
$string['blockafter_help'] = '이 설정은 사용자가 주어진 기간안에 게시할 수 있는 최대 글의 개수를 명시합니다. mod/jinoforum:postwithoutthrottling 권한이 있는 사용자는 게시한도에서 제외됩니다.';
$string['blockperiod'] = '차단 시간';
$string['blockperioddisabled'] = '차단하지 않음';
$string['blockperiod_help'] = '학생은 주어진 기간안에 게시할 수 있는 최대 글의 개수보다 많은 글을 게시하지 못합니다. mod/jinoforum:postwithoutthrottling 능력이 있는 사용자는 게시한계에서 제외됩니다.';
$string['blogjinoforum'] = '블로그 형식의 표준 게시판(토론)';
$string['bynameondate'] = '{$a->date} 에 {$a->name} 씀';
$string['cannotadd'] = '게시판(토론)에 토론을 추가할 수 없음';
$string['cannotadddiscussion'] = '게시판(토론)에 의견을 제시하려면 모둠의 구성원이어야 합니다.';
$string['cannotadddiscussionall'] = '공동의 토론 주제 추가 권한이 없습니다.';
$string['cannotaddsubscriber'] = '이 게시판(토론)에 id {$a} 인 구독자를 추가할 수 없음!';
$string['cannotaddteacherjinoforumto'] = '강좌의 섹션 0에 변환된 선생님의 게시판(토론) 인스턴스를 추가할 수 없었습니다.';
$string['cannotcreatejinoforum'] = '새 토론을 생성할 수 없음';
$string['cannotcreateinstanceforteacher'] = '선생님 게시판(토론)을 위한 새 강좌모듈을 생성할 수 없음';
$string['cannotdeletepost'] = '게시물을 삭제할 수 없음!';
$string['cannoteditposts'] = '다른 사람의 게시물을 수정할 수 없음!';
$string['cannotfinddiscussion'] = '이 게시판(토론)에서 토론내용을 찾을 수 없음';
$string['cannotfindfirstpost'] = '이 게시판(토론)의 발제문을 찾을 수 없음';
$string['cannotfindorcreatejinoforum'] = '사이트의 주 뉴스 게시판(토론)을 만들거나 찾을 수 없음';
$string['cannotfindparentpost'] = '{$a} 게시물의 최상위 게시물이 없음';
$string['cannotmovefromsinglejinoforum'] = '단순 단일 게시판(토론)에서는 토론을 이동할 수 없음';
$string['cannotmovenotvisible'] = '비 공개 게시판(토론)';
$string['cannotmovetonotexist'] = '그 게시판(토론)으로는 옮길 수 없음 - 존재하지 않는 게시판(토론)!';
$string['cannotmovetonotfound'] = '강좌에서 대상 게시판(토론)을 찾을 수 없음';
$string['cannotmovetosinglejinoforum'] = '토론을 단순 단일 게시판(토론)으로 옮길 수 없습니다.';
$string['cannotpurgecachedrss'] = '정보원/발송지 게시판(토론)에 저장된 RSS 피드를 삭제할 수 없음 - 게시판(토론) 권한 파일을 점검할 것';
$string['cannotremovesubscriber'] = '이 게시판(토론)에서 id {$a} 인 구독자를 제거할 수 없음!';
$string['cannotreply'] = '본 게시물에 답할 수 없음';
$string['cannotsplit'] = '본 게시판(토론)에서 토론을 분리할 수 없음';
$string['cannotsubscribe'] = '안타깝게도 구독하려면 모둠의 구성원이어야 합니다.';
$string['cannottrack'] = '게시판(토론)의 추적기능을 끌 수 없음';
$string['cannotunsubscribe'] = '게시판(토론) 구독을 해지할 수 없음';
$string['cannotupdatepost'] = '본 게시물을 업데이트할 수 없음';
$string['cannotviewpostyet'] = '이 토론에 게시물을 올리지 않았기 때문에 다른 학생들의 질문을 읽을 수 없음';
$string['cannotviewusersposts'] = '이 사용자가 게시한 글 중 볼 수 있는 글이 없습니다.';
$string['cleanreadtime'] = '게시물 읽은 시각으로 이전 글 표시';
$string['content'] = '내용';
$string['comment'] = '댓글';
$string['commentlist'] = '의견목록';
$string['completiondiscussions'] = '학생들은 토론을 시작하여야 합니다:';
$string['completiondiscussionsgroup'] = '토론 필수';
$string['completiondiscussionshelp'] = '토론 이수 필수';
$string['completionposts'] = '학생들은 토론이나 응답을 게시해야 합니다.';
$string['completionpostsgroup'] = '게시 필수';
$string['completionpostshelp'] = '이수하기 위해 토론이나 응답이 필수 사항임';
$string['completionreplies'] = '학생들은 답변을 게시해야 합니다 :';
$string['completionrepliesgroup'] = '응답 필수';
$string['completionreplieshelp'] = '완료하려면 필히 응답해야 함';
$string['configcleanreadtime'] = '\'읽었음\'표에서 지난 게시물을 정리하는 시각';
$string['configdigestmailtime'] = '요약문 형태로 메일을 전송받기를 선택한 사람들은 매일 요약문을 이메일로 받게 됩니다. 이 설정에서는 메일 발송 시간를 정할 수 있습니다. (다음 크론이 이 시간 이후에 작동하하여 이메일을 발송합니다)';
$string['configdisplaymode'] = '아무 것도 설정되지 않았을 경우 기본적으로 보여지는 제시 형태';
$string['configenablerssfeeds'] = '이 스위치는 모든 게시판(토론)에서 RSS 피드를 이용할 수 있게 합니다. 설정창에서 각 게시판(토론)에서도 피드를 지원할 수 있도록 수동으로 각 설정을 켜야합니다.';
$string['configenabletimedposts'] = '게시판(토론)에서 새 토론을 게시할 때 표시 기간을 설정할 수 있게 허용하려면 "예"로 설정하십시오.(아직 완전히 검증되지 않은 시험적 기능임)';
$string['configlongpost'] = '제시된 길이를 초과한 게시물(HTML코드 제외)은 긴 것으로 여겨집니다. 사이트의 시작 페이지, 사회적 형식의 강좌 페이지 또는 사용자 개인정보에 게시된 글은 jinoforum_shortpost와 jinoforum_longpost에서 지정된 길이로 조절됩니다.';
$string['configmanydiscussions'] = '페이지당 한 게시판(토론)에서 보여지는 최대 토론의 수';
$string['configmaxattachments'] = '각 게시글에 추가할 수 있는 최대 첨부물 수';
$string['configmaxbytes'] = '이 사이트 모든 게시판(토론) 첨부물의 기본 최대 크기(강좌 및 기타 사이트 설정에 따라 다름)';
$string['configoldpostdays'] = '지정한 날보다 오래된 게시물은 읽은 것으로 간주됩니다.';
$string['configreplytouser'] = '게시판(토론) 게시글을 메일로 발송할 때, 받는 사람들이 게시판(토론)을 통하는 대신 개인적으로 응답할 수 있도록 게시자의 메일 주소를 포함시킬까요? 만약 \'예\'로 하더라도, 사용자는 자신의 메일 주소를 안전하게 지키기 위해 개인정보에서 이를 비밀로 유지할 수 있습니다.';
$string['configshortpost'] = '여기에 제시된 길이보다 짧은 게시물(HTML 코드 제외)은 짧은 것으로 여겨집니다(아래 참조).';
$string['configtrackreadposts'] = '만약 각 사용자가 게시물을 읽었는지의 여부를 추적하고 싶다면 \'예\'로 설정하시오.';
$string['configusermarksread'] = '만약 \'예\'라면, 사용자는 손수 게시글 읽었음을 기표해야 합니다. \'아니오\'를 선택하면 게시글이 제시되면 자동으로 읽었음으로 표시됩니다.';
$string['confirmsubscribe'] = '게시판(토론) \'{$a}\'를 구독하시기를 원하십니까?';
$string['confirmunsubscribe'] = '게시판(토론)\'{$a}\'를 구독해지하시기를 원하십니까?';
$string['couldnotadd'] = '알 수 없는 오류로 인해 게시할 수 없음';
$string['couldnotdeletereplies'] = '죄송합니다, 사람들이 이미 답변을 게시하여 지울 수 없습니다';
$string['couldnotupdate'] = '알 수 없는 오류로 인해 업데이트 할 수 없음';
$string['crontask'] = '게시판(토론) 이메일 및 관리 작업';
$string['delete'] = '삭제';
$string['deleteddiscussion'] = '토론 주제가 삭제되었음';
$string['deletedpost'] = '그 게시물은 삭제되었음';
$string['deletedposts'] = '그 게시물들은 삭제되었음';
$string['deletesure'] = '정말 이 게시물을 삭제하겠습니까?';
$string['deletesureplural'] = '({$a} 게시글)이 게시물과 모든 답글을 삭제하는 것이 확실합니까?';
$string['digestmailheader'] = '{$a->sitename} 게시판(토론)에 새로 게시된 글들의 요약문입니다. 게시판(토론) 요약문의 구독여부를 변경하려면 {$a->userprefs} 로 가십시오.';
$string['digestmailpost'] = '게시판(토론) 요약문 구독 여부 변경';
$string['digestmailprefs'] = '사용자 개인정보';
$string['digestmailsubject'] = '{$a}: 게시판(토론) 요약';
$string['digestmailtime'] = '요약 이메일 보내는 시각';
$string['digestsentusers'] = '메일 요약문이 {$a} 사용자에게 성공적으로 보내졌습니다.';
$string['disallowsubscribe'] = '구독이 허용되지 않음';
$string['disallowsubscribeteacher'] = '(선생님을 제외하고는) 구독이 허용되지 않음';
$string['discussion'] = '토론 주제 명';
$string['discussionmoved'] = '이 토론은 \'{$a}\'로 옮겨졌습니다.';
$string['discussionmovedpost'] = '이 토론은 <a href="{$a->discusshref}">이 곳</a> 게시판(토론)<a href="{$a->forumhref}">{$a->forumname}</a>으로 이동되었습니다.';
$string['discussionname'] = '토론명';
$string['discussions'] = '토론들';
$string['discussionsstartedby'] = '{$a} 가 발의한 토론';
$string['discussionsstartedbyrecent'] = '{$a} 가 최근 발의한 토론';
$string['discussionsstartedbyuserincourse'] = '{$a->coursename}에서  {$a->fullname} 가 시작한 토론';
$string['discussthistopic'] = '이 주제에 대해 토론하기';
$string['displayend'] = '게시 종료일';
$string['displayend_help'] = '<p>게시판(토론)의 게시물을 언제부터 언제까지 혹은 어느 일정 기간동안 게시할 것인지를 선택할 수 있다.</p>

<p>시작/종료일을 표시하기 위해서는 불가능 옵션을 선택하지 않으면 됩니다.</p>

<p>관리자로 접속하게 되면 언제 게시되었는 지 또 언제 종료되었는 지에 대한 메시지를 볼 수 있음을 유의하십시오.</p>';
$string['displaymode'] = '표시 모드';
$string['displayperiod'] = '게시 기간';
$string['displaystart'] = '게시 시작일';
$string['displaystart_help'] = '<p>게시판(토론)의 게시물을 언제부터 언제까지 혹은 어느 일정 기간동안 게시할 것인지를 선택할 수 있다.</p>

<p>시작/종료일을 표시하기 위해서는 불가능 옵션을 선택하지 않으면 됩니다.</p>

<p>관리자로 접속하게 되면 언제 게시되었는 지 또 언제 종료되었는 지에 대한 메시지를 볼 수 있음을 유의하십시오.</p>';
$string['displaywordcount'] = '단어 수 표시';
$string['eachuserjinoforum'] = '개인별 단독 게시판(토론)';
$string['edit'] = '수정';
$string['editedby'] = '{$a->name}에 의해 편집됨 -  원문 제출일 {$a->date} ';
$string['editedpostupdated'] = '{$a}의 게시글이 업데이트 되었습니다.';
$string['editing'] = '수정하기';
$string['emaildigestupdated'] = '게시판(토론) \'{$a->forum}\'에 대한 {$a->maildigestdescription}이메일 요약 옵션이 \'{$a->maildigesttitle}\' 변경되었습니다.  ';
$string['emptymessage'] = '올린 글에서 오류가 발견되었습니다. 빈칸이거나 첨부 파일이 너무 큰 것일 수 있습니다. 변경사항이 저장되지 않았습니다.';
$string['erroremptymessage'] = '게시 메세지는 비어 있을 수 없습니다.';
$string['erroremptysubject'] = '제목이 없으면 안됩니다.';
$string['errorenrolmentrequired'] = '이 내용에 접근하기 위해서는 이 강좌에 등록해야 합니다.';
$string['errorwhiledelete'] = '기록 삭제 중 오류 발생';
$string['eventcoursesearched'] = '강좌 검색됨';
$string['eventdiscussioncreated'] = '토론 생성됨';
$string['eventdiscussiondeleted'] = '토론 삭제됨';
$string['eventdiscussionmoved'] = '토론 이동됨';
$string['eventdiscussionupdated'] = '토론 업데이트됨';
$string['eventdiscussionviewed'] = '토론 봄';
$string['eventpostcreated'] = '게시글 생성됨';
$string['eventpostdeleted'] = '게시글 삭제됨';
$string['eventpostupdated'] = '게시글 업데이트됨';
$string['eventreadtrackingdisabled'] = '읽기 추적 비활성화됨';
$string['eventreadtrackingenabled'] = '읽기 추적 활성화됨';
$string['eventsubscribersviewed'] = '구독자 봄';
$string['eventsubscriptioncreated'] = '구독 생성됨';
$string['eventsubscriptiondeleted'] = '구독 삭제됨';
$string['eventuserreportviewed'] = '사용자 보고서 봄';
$string['everyonecanchoose'] = '모든 사람이 구독여부를 선택할 수 있음';
$string['everyonecannowchoose'] = '모든 사람이 구독여부를 선택할 수 있게 됨';
$string['everyoneisnowsubscribed'] = '이제 모든 사람이 이 게시판(토론)을 구독하게 됨';
$string['everyoneissubscribed'] = '모든 사람이 이 게시판(토론)을 구독함';
$string['existingsubscribers'] = '존재하는 구독자들';
$string['exportdiscussion'] = '전체 토론을 내보내기';
$string['forcedreadtracking'] = '강제 읽기 추적 허용';
$string['forcessubscribe'] = '모든 사람이 게시판(토론)을 구독해야 함';
$string['jinoforum'] = '게시판(토론)';
$string['jinoforum:addinstance'] = '새 게시판(토론) 추가';
$string['jinoforum:addnews'] = '새소식 추가';
$string['jinoforum:addquestion'] = '질문 추가';
$string['jinoforum:allowforcesubscribe'] = '강제 구독 허용';
$string['jinoforumauthorhidden'] = '작성자(비공개)';
$string['jinoforumblockingalmosttoomanyposts'] = '게시 한계에 도달하려 합니다. 지난 {$a->blockperiod} 동안 {$a->numposts} 번 게시물을 게시하였으며 게시 한계는 {$a->blockafter} 번 입니다.';
$string['jinoforumbodyhidden'] = '이 게시물은 볼 수 없습니다. 한 번도 토론에 참여하지 않았거나, 최대 편집 시간이 지나지 않았거나, 토론이 시작되지 않았거나, 또는 토론이 만료되었을 수 있습니다.';
$string['jinoforum:createattachment'] = '첨부물 생성';
$string['jinoforum:deleteanypost'] = '(언제든) 어떤 게시물이든 삭제';
$string['jinoforum:deleteownpost'] = '자신의 게시물 삭제(시한 내)';
$string['jinoforum:editanypost'] = '게시물 편집';
$string['jinoforum:exportjinoforum'] = '전체 토론 내보내기';
$string['jinoforum:exportownpost'] = '내 게시물 내보내기';
$string['jinoforum:exportpost'] = '게시내용 내보내기';
$string['jinoforumintro'] = '설명';
$string['jinoforum:managesubscriptions'] = '구독 관리';
$string['jinoforum:movejinoforums'] = '토론내용 이동';
$string['jinoforumname'] = '게시판(토론) 이름';
$string['jinoforumposts'] = '게시판(토론) 게시글';
$string['jinoforum:postwithoutthrottling'] = '게시 한계에서 제외';
$string['jinoforum:rate'] = '게시물 평가';
$string['jinoforum:replynews'] = '새소식에 답장';
$string['jinoforum:replypost'] = '게시물에 답장';
$string['jinoforums'] = '게시판(토론)들';
$string['jinoforum:splitjinoforums'] = '토론주제 분리';
$string['jinoforum:startjinoforum'] = '새로운 토론 시작';
$string['jinoforumsubjecthidden'] = '제목(비공개)';
$string['jinoforumtracked'] = '읽지않은 게시물을 추적함';
$string['jinoforumtrackednot'] = '읽지않은 게시물 추적하지 않음';
$string['jinoforumtype'] = '게시판(토론) 유형';
$string['jinoforumtype_help'] = '5가지 게시판(토론) 유형이 있습니다

* 하나의 간단한 토론 - 모든 사람이 응답할 수 있는 한 토론 주제.

* 일인 한개 토론 - 학생마다 모든 사람들이 응답할 수 있는  한개의 토론 주제만 게시할 수 있습니다

* 질의 응답식 게시판(토론) - 다른 학생들의 게시물을 보기 전에 자신들의 견해를 미리 게시해야 합니다.

* 블로그 형식으로 표시된 표준 게시판(토론) - 누구던지 아무때나 새로운 토론 주제를 게시할 수 있는 열린 게시판(토론). 토론 주제들은 한 페이지에  "이 주제 토론" 링크와 함께 표시됩니다.

* 일반적 용도의 표준 게시판(토론)  - 언제, 누구라도 새로운 주제를 게시할 수 있는 열린 게시판(토론)';
$string['jinoforum:viewallratings'] = '원점수 보기';
$string['jinoforum:viewanyrating'] = '모두가 받은 총점수 보기';
$string['jinoforum:viewjinoforum'] = '토론 보기';
$string['jinoforum:viewhiddentimedposts'] = '한시적 비공개 게시물 보기';
$string['jinoforum:viewqandawithoutposting'] = '항상 질의응답 게시물 보기';
$string['jinoforum:viewrating'] = '내가 받은 총 점수 보기';
$string['jinoforum:viewsubscribers'] = '구독자 보기';
$string['generaljinoforum'] = '일반 용도의 표준 게시판(토론)';
$string['generaljinoforums'] = '일반 게시판(토론)들';
$string['hiddenjinoforumpost'] = '감춰진 게시판(토론) 게시글';
$string['hour'] = "시";
$string['injinoforum'] = '{$a} 에 있는';
$string['list'] = "토론주제목록";
$string['introblog'] = '이 게시판(토론)에 있는 게시물들은, 개인 블로그를 더이상 이용할 수 없기 때문에 기존의 개인 블로그에서 자동으로 복사하여 가져온 것입니다. ';
$string['intronews'] = '강좌관련 공지사항';
$string['introsocial'] = '모든 것에 대해 논의할 수 있는 열린 게시판(토론)';
$string['introteacher'] = '선생님 게시판(토론)';
$string['invalidaccess'] = '이  페이지는 올바르게 접속되지 않았습니다.';
$string['largefileattachments'] = "대용량 첨부파일 사용";
$string['invalidjinoforumid'] = '토론 ID가 틀렸거나 더 이상 존재하지 않습니다.';
$string['invalidforcesubscribe'] = '쓸 수 없는 강제 구독 모드';
$string['invalidjinoforumid'] = '게시판(토론)의 ID가 바르지 않음';
$string['invalidparentpostid'] = '상위 게시판 ID가 바르지 않음';
$string['invalidpostid'] = '잘못된 게시물 ID - {$a}';
$string['lastpost'] = '최근 게시';
$string['learningjinoforums'] = '학습 게시판(토론)';
$string['longpost'] = '긴 게시물';
$string['mailnow'] = '지금 메일보내기';
$string['manydiscussions'] = '페이지당 토론';
$string['markalldread'] = '토론의 모든 게시물을 \'읽었음\'으로 표시합니다.';
$string['markallread'] = '게시판(토론)의 모든 게시물을 \'읽었음\'으로 표시합니다.';
$string['markread'] = '\'읽었음\'으로 표시';
$string['markreadbutton'] = '표시<br />\' 읽었음\'';
$string['markunread'] = '\'읽지 않음\' 표시';
$string['markunreadbutton'] = '표시<br />\'읽지 않음\'';
$string['maxattachments'] = '최대 첨부물 수';
$string['maxattachments_help'] = '<p>본 게시판(토론)에서 각 게시글에 얼마나 많는 첨부물을 부가할 수 있는지를 설정할 수 있게 한다.</p>';
$string['maxattachmentsize'] = '최대 첨부 용량';
$string['maxattachmentsize_help'] = '<p>첨부할 수 있는 파일의 크기는 게시판(토론)을 개설한 관리자가 제한할 수 있다.</p>

<p>가끔 이 크기보다 큰 용량의 파일을 올릴 수 있는 듯 보이지만
결국 서버에 저장되지 못하고 오류메시지를 보게 될 것이다.</p>';
$string['maxtimehaspassed'] = '죄송합니다. 이글의 수정 횟수({$a})는 제한되어 있습니다!';
$string['message'] = '메세지';
$string['messageprovider:digests'] = '게시판(토론) 요약을 구독했음';
$string['messageprovider:posts'] = '게시판(토론) 게시글을 구독했음';
$string['missingsearchterms'] = '다음 검색 용어는 이 메시지의 HTML 단계에서만 나타납니다:';
$string['modeflatnewestfirst'] = '새 답글부터 내용 보기';
$string['modeflatoldestfirst'] = '옛 답글부터 내용 보기';
$string['modenested'] = '주제 중심으로 답글 보기';
$string['modethreaded'] = '글타래 형태로 목록 보기';
$string['modulename'] = '게시판(토론)';
$string['modulename_help'] = '<p><b>게시판(토론)</b></p>
<div class="indent">
<p>포럼 형식의 게시판으로 토론자를 선택하여 공개/비밀 토론을 진행할 수 있는 특별한 기능을 갖추었습니다.</p> 
<p>답글, 댓글, 추천 등이 모두 가능합니다.</p>
<p>게시판의 글들은 북마킹이 가능하며 이때 북마킹된 내용은 강의 게시판의 나의 게시판에 모아 볼 수 있습니다.</p>
<p>게시판의 내용들은 게시글, 답글, 추천 등애 대한 통계 분석이 자동으로 제공됩니다.</p>
</div>';
$string['modulenameplural'] = '게시판(토론)모음';
$string['more'] = '자세히';
$string['movedmarker'] = '(옮겨짐)';
$string['movethisdiscussionto'] = '이 토론 옮기기 ...';
$string['mustprovidediscussionorpost'] = '내보내기 위해서는 게시 ID나 토론 ID를 제공해야만 함';
$string['namenews'] = '강좌공지';
$string['namenews_help'] = '공지사항 게시판(토론)은 강좌가 생성될때 자동적으로 생성되는 공지를 위한 특별한 게시판(토론)입니다. 강좌에는 하나의 공지사항 게시판(토론)만 있을 수 있습니다. 선생님과 관리자만 이 공지사항 게시판(토론)에 글을 올릴 수 있습니다. "새 공지사항" 블록은 이 공지사항 게시판의  최근 게시 내용을 표시합니다.  
';
$string['namesocial'] = '사회적 게시판(토론)';
$string['nameteacher'] = '선생님 게시판(토론)';
$string['newjinoforumposts'] = '새로운 게시물';
$string['noattachments'] = '이 게시물에는 첨부물이 없음';
$string['nodiscussions'] = '아직 토론 주제가 제시되지 않았음';
$string['nodiscussionsstartedby'] = '{$a}는 아직 시작한 토론이 없음';
$string['nodiscussionsstartedbyyou'] = '아직 토론을 시작하지 않았습니다.';
$string['noguestpost'] = '죄송하지만, 손님 계정으로는 글을 올릴 수 없습니다.';
$string['noguesttracking'] = '죄송합니다만, 손님은 경로 추적이 허용되지 않습니다.';
$string['nomorepostscontaining'] = '더이상 \'{$a}\'를 포함한 글이 없음';
$string['nonews'] = '아직 뉴스가 게시되지 않았음';
$string['noonecansubscribenow'] = '구독이 허용되지 않음';
$string['nopermissiontosubscribe'] = '게시판(토론) 구독자를 볼 수 있는 권한이 없음';
$string['nopermissiontoview'] = '이 게시물을 볼 수 있는 권한이 없음';
$string['nopostjinoforum'] = '미안합니다만, 이 게시판(토론)에 게시할 수 없습니다.';
$string['noposts'] = '게시물 없음';
$string['nopostsmadebyuser'] = '{$a}는 올린 글이 없습니다.';
$string['nopostsmadebyyou'] = '아무런 글도 올리지 않았습니다.';
$string['noquestions'] = '게시판(토론)에 아직 질문이 없음';
$string['nosubscribers'] = '이 토론게시판에는 아직 구독자 없음';
$string['notexists'] = '더 이상 존재하지 않는 토론임';
$string['nothingnew'] = '{$a} 에 새 게시물 없음';
$string['notingroup'] = '죄송합니다. 이 게시판(토론)을 보려면 모둠에 속해있어야 합니다.';
$string['notinstalled'] = '게시판(토론)모듈이 탑재되지 않았음';
$string['notpartofdiscussion'] = '본 게시물은 토론의 일부분이 아님!';
$string['notrackjinoforum'] = '읽지 않은 게시물 추적 안함';
$string['noviewdiscussionspermission'] = '이 게시판(토론)의 토론을 볼 수 있는 권한이 없음';
$string['nowallsubscribed'] = '{$a}에 있는 모든 게시판(토론)을 구독합니다.';
$string['nowallunsubscribed'] = '{$a}에 있는 모든 게시판(토론)을 구독해지합니다.';
$string['nownotsubscribed'] = '{$a->name} 는 이메일로 \'{$a->forum}\'의 새 게시물에 대한 알림을 받을 수 없게 됩니다.';
$string['nownottracking'] = '{$a->name} 은 더이상 \'{$a->jinoforum}\'의 경로를 추적을 하지 않습니다.';
$string['nowsubscribed'] = '{$a->name} 는 \'{$a->forum}\'에 새 글에 대해 알림을 받아 볼 수 있습니다.';
$string['nowtracking'] = '{$a->name} 이 \'{$a->jinoforum}\'경로를 추적 중입니다.';
$string['number'] = '번호';
$string['numposts'] = '{$a} 게시물';
$string['olderdiscussions'] = '오래된 토론들';
$string['oldertopics'] = '오래된 주제들';
$string['oldpostdays'] = '지난 게시물 읽음 처리';
$string['openmode0'] = '토론 및 답글 불가';
$string['openmode1'] = '토론 없음, 답글 허용됨';
$string['openmode2'] = '토론과 답글 허용됨';
$string['overviewnumpostssince'] = '마지막 접속 후 게시물 수 : {$a}';
$string['overviewnumunread'] = '읽지 않은 수 : {$a}';
$string['page-mod-jinoforum-discuss'] = '게시판(토론) 모듈 토론 쓰레드 페이지';
$string['page-mod-jinoforum-view'] = '게시판(토론) 모듈 메인 페이지';
$string['page-mod-jinoforum-x'] = '모든 게시판(토론) 모듈 페이지';
$string['parent'] = '상위 글 보기';
$string['parentofthispost'] = '이 게시물의 상위 글';
$string['pluginadministration'] = '게시판(토론) 관리';
$string['pluginname'] = '게시판(토론)';
$string['postadded'] = '<p> 성공적으로 글이 추가되었습니다.</p> <p> {$a} 분 내에는 글을 수정할 수 있습니다.</p>';
$string['postaddedsuccess'] = '성공적으로 글이 추가되었습니다.';
$string['postaddedtimeleft'] = '{$a} 분 내에는 글을 수정할 수 있습니다.';
$string['postincontext'] = '문맥에서 이 글 보기';
$string['postmailinfo'] = '{$a} 웹사이트에 게시된 글의 사본입니다. 응답하기 위해서는 이 링크를 클릭하세요. ';
$string['postmailnow'] = '<p>모든 게시판(토론) 사용자들에게 이 게시물이 즉시 발송될 것입니다.</p>';
$string['postrating1'] = '대부분 분리된 배움';
$string['postrating2'] = '분리와 연결';
$string['postrating3'] = '연대형 학습';
$string['posts'] = '게시물';
$string['postsmadebyuser'] = '{$a}가 올린 글';
$string['postsmadebyuserincourse'] = '{$a->coursename}에서 {$a->fullname} 가 작성한 글';
$string['posttojinoforum'] = '게시판(토론)에 올리기';
$string['postupdated'] = '게시글이 업데이트됨';
$string['potentialsubscribers'] = '잠재 구독자들';
$string['processingdigest'] = '{$a} 사용자를 위한 메일 요약 처리중';
$string['processingpost'] = '게시물 {$a} 처리중';
$string['prune'] = '분리';
$string['prunedpost'] = '게시물과 관련한 새 토론이 게시판에 올려졌음';
$string['pruneheading'] = '이 게시물을 분리시켜 새 토론으로 올림';
$string['qandajinoforum'] = '질의 응답 게시판(토론)';
$string['qandanotify'] = '여기는 질의 응답 게시판(토론)입니다. 이 질문에 대한 다른 응답을 보려면 먼저 당신의 답을 게시하여야만 합니다.';
$string['re'] = '회신:';
$string['readtherest'] = '이 주제의 나머지 글 읽기';
$string['recommend'] = '추천';
$string['replies'] = '답글';
$string['replies_num'] = '답글수';
$string['repliesmany'] = '올라온 답글들 {$a}';
$string['repliesone'] = '올라온 답글 {$a}';
$string['reply'] = '답글';
$string['replyjinoforum'] = '게시판(토론)에 답글 쓰기';
$string['replytouser'] = '답신에 이메일 주소 사용';
$string['resetjinoforums'] = '로부터 게시물 삭제';
$string['resetjinoforumsall'] = '모든 게시물 삭제';
$string['resetsubscriptions'] = '모든 포럼 구독 삭제';
$string['resettrackprefs'] = '모든 게시판(토론) 추적 선택사항 삭제';
$string['rssarticles'] = '최신 RSS 기사 수';
$string['rssarticles_help'] = '<p>이 기능은 RSS 피드에 포함시킬 문서(토론이나 게시물)의 수를 정하는 것입니다.</p>

<p>대개의 게시판(토론)에서는 5-20 정도가 적당합니다. 포럼이 자주 이용될 때는 이 값을 높이세요.</p>';
$string['rsssubscriberssdiscussions'] = '토론에 대한 RSS 피드';
$string['rsssubscriberssposts'] = '게시물에 대한 RSS 피드';
$string['rsstype'] = '이 활동에 대한 RSS 피드';
$string['rsstype_help'] = '<p>이 기능은 게시판(토론)을 RSS로 내보낼 수 있게 지정하는 것이다.</p>

<p>다음 두 유형 중에서 한 유형을 선택할 수 있다:</p>

<ul>
<li><b>토론글타래:</b> 이 옵션은, 원래 게시된 글과 더불어 새로 논의된 사항을 RSS로 내보낸다.</li>

<li><b>게시물:</b> 이 옵션은, 매번 게시판(토론) 내에 새로 게시된 글만 RSS로 내보낸다.</li>
</ul>';
$string['sort'] =  '{$a}개씩 보기';
$string['search'] = '검색';
$string['searchdatefrom'] = '게시물들은 이 날짜 이후에 작성된 것이어야 합니다';
$string['searchdateto'] = '게시물들은 이 날짜보다 더 오래된 것이어야 합니다';
$string['searchjinoforumintro'] = '하나 또는 그 이상의 영역에 검색어를 입력하세요. ';
$string['searchjinoforums'] = '게시판(토론) 검색';
$string['searchfullwords'] = '다음 단어 포함';
$string['searchnotwords'] = '다음 단어 제외';
$string['searcholderposts'] = '이전의 게시물 검색...';
$string['searchphrase'] = '다음 문장 포함';
$string['searchresults'] = '검색 결과들';
$string['searchsubject'] = '이 단어들이 제목에 있어야 합니다.';
$string['searchuser'] = '발제자';
$string['searchuserid'] = '글쓴이 아이디';
$string['searchwhichjinoforums'] = '검색할 게시판(토론)';
$string['searchwords'] = '다음 단어들을 포함한 게시물';
$string['seeallposts'] = '다음 사용자가 쓴 모든 글 보기';
$string['shortpost'] = '간단한 게시';
$string['showsubscribers'] = '구독자 보기/고치기';
$string['singlejinoforum'] = '하나의 간단한 토론';
$string['smallmessage'] = '{$a->user} 가 {$a->jinoforumname}에 글을 남겼습니다. ';
$string['startedby'] = '시작됨';
$string['subject'] = '제목';
$string['subscribe'] = '이 게시판(토론)을 이메일로 받기';
$string['subscribeall'] = '이 게시판(토론)을 모두에게 이메일로 보내기';
$string['subscribed'] = '구독함';
$string['subscribeenrolledonly'] = '죄송합니다. 등록된 사용자만이 게시판(토론) 게시물을 이메일로 구독할 수 있습니다.';
$string['subscribenone'] = '아무에게도 이메일 보내지 않기';
$string['subscribers'] = '구독자';
$string['subscribersto'] = '{$a}에게 보내기';
$string['subscribestart'] = '이 게시판(토론) 게시물의 사본을 내 이메일로 보내기';
$string['subscribestop'] = '이 게시판(토론) 게시물의 사본을 내 이메일로 받지 않음';
$string['subscription'] = '이메일로 구독';
$string['subscriptionandtracking'] = '구독 및 추적';
$string['subscriptionauto'] = '자동 구독';
$string['subscriptiondisabled'] = '구독 불가능';
$string['subscriptionforced'] = '강제 구독';
$string['subscription_help'] = '게시판(토론)을 구독하면 게시판(토론) 게시글에 대한 이메일 사본을 받게 됩니다. 보통 구독을 선택할 수 있지만 게시판(토론) 게시글에 대해 모든 사람들이 이메일 사본을 받도록 강제적으로 구독하게 할 수 있습니다.';
$string['subscriptionmode'] = '구독 옵션';
$string['subscriptionmode_help'] = '참여자가 게시판(토론)을 구독하면 게시판(토론)에 게시된 글의 사본을 이메일로 받겠다는 것을 의미합니다.

4가지 구독 모드 옵션이 있습니다.

* 선택적 구독 - 참여자들은 구독할 것인지를 선택할 수 있습니다.

* 강제 구독 - 참여자 모두 구독하게 되며, 구독을 해지 할 수 없습니다.

* 자동 구독 - 참여자들은 처음에만 구독하게 되며 언제든지 구독을 그만 둘 수 있습니다.

* 구독 비활성화 - 구독이 허용되지 않습니다.';
$string['subscriptionoptional'] = '선택적 구독';
$string['subscriptions'] = '이메일로 구독';
$string['thisjinoforumisthrottled'] = '이 게시판(토론)은 주어진 기간안에 게시할 수 있는 게시판(토론) 게시물의 수에 한계가 있습니다. 현재 설정은 {$a->blockperiod} 기간동안 {$a->blockafter} 게시물 입니다.';
$string['timedposts'] = '게시 시한 제한';
$string['timestartenderror'] = '게시 종료일이 시작일보다 먼저일 수는 없습니다.';
$string['trackjinoforum'] = '읽지 않은 글 추적';
$string['tracking'] = '추적';
$string['trackingoff'] = '추적않기';
$string['trackingon'] = '강제';
$string['trackingoptional'] = '선택적';
$string['trackingtype'] = '읽기 추적';
$string['trackingtype_help'] = '활성화하면 참여자들은 게시판(토론) 혹은 토론 내에서 이미 읽었거나 읽지 않은 메세지를 추적할 수 있습니다.

세 가지 옵션이 있습니다:

* 선택: 참여자가 추적 기능을 켜거나 끌 수 있다.
* 켬: 추적 기능을 항상 켜둠. 운영자 설정에 따라 가능함. 
* 끔: 추적 기능을 항상 끔.';
$string['unread'] = '읽지 않음';
$string['unreadposts'] = '읽지 않은 글';
$string['unreadpostsnumber'] = '{$a} 개의 읽지 않은 글';
$string['unreadpostsone'] = '읽지 않은 글 하나';
$string['unsubscribe'] = '이 게시판(토론) 구독해지';
$string['unsubscribeall'] = '모든 게시판(토론) 구독 해지';
$string['unsubscribeallconfirm'] = '지금 {$a} 게시판(토론)을 구독하고 있습니다. 정말 모든 포럼의 구독을 해지하고 게시판(토론) 자동 구독을 비활성화하겠습니까?';
$string['unsubscribealldone'] = '게시판(토론)에 대한 모든 구독사항을 삭제하였지만, 강제 구독해야 하는 게시판(토론)의 공지사항은 받아볼 수 있습니다. 만일 이 서버에서 보내는 어떤 메일도 받고 싶지 않다면, 개인 정보수정의 이메일 활성난에서 이메일 전송 금지를 선택하기 바랍니다.';
$string['unsubscribeallempty'] = '죄송합니다만, 어떤 게시판(토론)도 구독하고 있지 않습니다. 만일 이 서버에서 보내는 어떤 메일도 받고 싶지 않다면, 개인정보 수정의 이메일 활성란에서 이메일 전송 금지를 선택하기 바랍니다.';
$string['unsubscribed'] = '구독해지 됨';
$string['unsubscribeshort'] = '구독해지';
$string['usermarksread'] = '메세지 읽음 수동 표시';
$string['viewalldiscussions'] = '모든 토론 보기';
$string['warnafter'] = '경고를 위한 게시물 한계';
$string['warnafter_help'] = '학생들은 주어진 기간에 허용된 최대 게시물 수에 가까워지면 경고를 받을 수 있습니다. 이 설정은 몇개 게시물 후에 그들이 경고 받을지를 명시합니다. mod/jinoforum:postwithoutthrottling 능력이 있는 사용자는 게시 한계에서 제외됩니다.';
$string['warnformorepost'] = '경고! 이 게시판(토론)에는 한개 이상의 토론이 있습니다. - 가장 최근의 것을 이용합니다.';
$string['wdate'] = '작성일';
$string['yournewquestion'] = '내 새로운 질문';
$string['yournewtopic'] = '내 새로운 토론 주제';
$string['yourreply'] = '내 답글';
$string['hit'] = '조회수';
$string['emptycomment'] = '댓글을 입력해주세요.';
$string['writer'] = '작성자';
$string['allowpublic'] = '공개';
$string['studentindiscussion'] = '토론 참여 학생';
$string['public'] = '공개';
$string['closed'] = '비공개';
$string['recommend:cnt'] = '추천수';
$string['comment:cnt'] = '댓글수';
$string['content:file'] = '첨부파일';
$string['next_content'] = '다음글';
$string['prev_content'] = '이전글';
$string['current_content'] = '현재글';
$string['view_detail'] = '상세보기';

$string['deletedfile'] = '대용량 첨부파일로서, 등록된 학기 종료 후 삭제되었습니다.';
$string['public_distinction'] = '공개여부';
$string['real_recommend'] = '정말로 추천하시겠습니까?';
$string['already_recommended'] = '이미 추천하셨습니다.';