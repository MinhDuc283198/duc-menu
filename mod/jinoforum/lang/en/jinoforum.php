<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'jinoforum', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['activityoverview'] = 'There are new Forum(Debate) posts';
$string['addanewdiscussion'] = 'Add a new discussion topic';
$string['addanewquestion'] = 'Add a new question';
$string['addanewtopic'] = 'Add a new topic';
$string['addopinion'] = 'Add a new opinion';
$string['advancedsearch'] = 'Advanced search';
$string['alljinoforums'] = 'All Forum(Debate)s';
$string['allowdiscussions'] = 'Can a {$a} post to this Forum(Debate)?';
$string['allowsallsubscribe'] = 'This Forum(Debate) allows everyone to choose whether to subscribe or not';
$string['allowsdiscussions'] = 'This Forum(Debate) allows each person to start one discussion topic.';
$string['allsubscribe'] = 'Subscribe to all Forum(Debate)s';
$string['allunsubscribe'] = 'Unsubscribe from all Forum(Debate)s';
$string['alreadyfirstpost'] = 'This is already the first post in the discussion';
$string['anyfile'] = 'Any file';
$string['areaattachment'] = 'Attachments';
$string['areapost'] = 'Contents';
$string['attachment'] = 'Attachment';
$string['attachment_help'] = 'You can optionally attach one or more files to a Forum(Debate) post. If you attach an image, it will be displayed after the message.';
$string['attachmentnopost'] = 'You cannot export attachments without a post id';
$string['attachments'] = 'Attachments';
$string['attachmentswordcount'] = 'Attachments and word count';
$string['blockafter'] = 'Post threshold for blocking';
$string['blockafter_help'] = 'This setting specifies the maximum number of posts which a user can post in the given time period. Users with the capability mod/jinoforum:postwithoutthrottling are exempt from post limits.';
$string['blockperiod'] = 'Time period for blocking';
$string['blockperiod_help'] = 'Students can be blocked from posting more than a given number of posts in a given time period. Users with the capability mod/jinoforum:postwithoutthrottling are exempt from post limits.';
$string['blockperioddisabled'] = 'Don\'t block';
$string['blogjinoforum'] = 'Standard Forum(Debate) displayed in a blog-like format';
$string['bynameondate'] = 'by {$a->name} - {$a->date}';
$string['cannotadd'] = 'Could not add the discussion for this Forum(Debate)';
$string['cannotadddiscussion'] = 'Adding discussions to this Forum(Debate) requires group membership.';
$string['cannotadddiscussionall'] = 'You do not have permission to add a new discussion topic for all participants.';
$string['cannotaddsubscriber'] = 'Could not add subscriber with id {$a} to this Forum(Debate)!';
$string['cannotaddteacherjinoforumto'] = 'Could not add converted teacher Forum(Debate) instance to section 0 in the course';
$string['cannotcreatediscussion'] = 'Could not create new discussion';
$string['cannotcreateinstanceforteacher'] = 'Could not create new course module instance for the teacher Forum(Debate)';
$string['cannotdeletepost'] = 'You can\'t delete this post!';
$string['cannoteditposts'] = 'You can\'t edit other people\'s posts!';
$string['cannotfinddiscussion'] = 'Could not find the discussion in this Forum(Debate)';
$string['cannotfindfirstpost'] = 'Could not find the first post in this Forum(Debate)';
$string['cannotfindorcreatejinoforum'] = 'Could not find or create a main news Forum(Debate) for the site';
$string['cannotfindparentpost'] = 'Could not find top parent of post {$a}';
$string['cannotmovefromsinglejinoforum'] = 'Cannot move discussion from a simple single discussion Forum(Debate)';
$string['cannotmovenotvisible'] = 'Jinoforum not visible';
$string['cannotmovetonotexist'] = 'You can\'t move to that Forum(Debate) - it doesn\'t exist!';
$string['cannotmovetonotfound'] = 'Target Forum(Debate) not found in this course.';
$string['cannotmovetosinglejinoforum'] = 'Cannot move discussion to a simple single discussion Forum(Debate)';
$string['cannotpurgecachedrss'] = 'Could not purge the cached RSS feeds for the source and/or destination Forum(Debate)(s) - check your file permissionsForum(Debate)s';
$string['cannotremovesubscriber'] = 'Could not remove subscriber with id {$a} from this Forum(Debate)!';
$string['cannotreply'] = 'You cannot reply to this post';
$string['cannotsplit'] = 'Discussions from this Forum(Debate) cannot be split';
$string['cannotsubscribe'] = 'Sorry, but you must be a group member to subscribe.';
$string['cannottrack'] = 'Could not stop tracking that Forum(Debate)';
$string['cannotunsubscribe'] = 'Could not unsubscribe you from that Forum(Debate)';
$string['cannotupdatepost'] = 'You can not update this post';
$string['cannotviewpostyet'] = 'You cannot read other students questions in this discussion yet because you haven\'t posted';
$string['cannotviewusersposts'] = 'There are no posts made by this user that you are able to view.';
$string['cleanreadtime'] = 'Mark old posts as read hour';
$string['completiondiscussions'] = 'Student must create discussions:';
$string['content'] = 'content';
$string['completiondiscussionsgroup'] = 'Require discussions';
$string['completiondiscussionshelp'] = 'requiring discussions to complete';
$string['completionposts'] = 'Student must post discussions or replies:';
$string['completionpostsgroup'] = 'Require posts';
$string['completionpostshelp'] = 'requiring discussions or replies to complete';
$string['completionreplies'] = 'Student must post replies:';
$string['completionrepliesgroup'] = 'Require replies';
$string['completionreplieshelp'] = 'requiring replies to complete';
$string['commentlist'] = 'comment list';
$string['configcleanreadtime'] = 'The hour of the day to clean old posts from the \'read\' table.';
$string['configdigestmailtime'] = 'People who choose to have emails sent to them in digest form will be emailed the digest daily. This setting controls which time of day the daily mail will be sent (the next cron that runs after this hour will send it).';
$string['configdisplaymode'] = 'The default display mode for discussions if one isn\'t set.';
$string['configenablerssfeeds'] = 'This switch will enable the possibility of RSS feeds for all Forum(Debate)s.  You will still need to turn feeds on manually in the settings for each Forum(Debate).';
$string['configenabletimedposts'] = 'Set to \'yes\' if you want to allow setting of display periods when posting a new Forum(Debate) discussion (Experimental as not yet fully tested)';
$string['configlongpost'] = 'Any post over this length (in characters not including HTML) is considered long. Posts displayed on the site front page, social format course pages, or user profiles are shortened to a natural break somewhere between the jinoforum_shortpost and jinoforum_longpost values.';
$string['configmanydiscussions'] = 'Maximum number of discussions shown in a Forum(Debate) per page';
$string['configmaxattachments'] = 'Default maximum number of attachments allowed per post.';
$string['configmaxbytes'] = 'Default maximum size for all Forum(Debate) attachments on the site (subject to course limits and other local settings)';
$string['configoldpostdays'] = 'Number of days old any post is considered read.';
$string['configreplytouser'] = 'When a Forum(Debate) post is mailed out, should it contain the user\'s email address so that recipients can reply personally rather than via the Forum(Debate)? Even if set to \'Yes\' users can choose in their profile to keep their email address secret.';
$string['configshortpost'] = 'Any post under this length (in characters not including HTML) is considered short (see below).';
$string['configtrackingtype'] = 'Default setting for read tracking.';
$string['configtrackreadposts'] = 'Set to \'yes\' if you want to track read/unread for each user.';
$string['configusermarksread'] = 'If \'yes\', the user must manually mark a post as read. If \'no\', when the post is viewed it is marked as read.';
$string['confirmsubscribe'] = 'Do you really want to subscribe to Forum(Debate) \'{$a}\'?';
$string['confirmunsubscribe'] = 'Do you really want to unsubscribe from Forum(Debate) \'{$a}\'?';
$string['couldnotadd'] = 'Could not add your post due to an unknown error';
$string['couldnotdeletereplies'] = 'Sorry, that cannot be deleted as people have already responded to it';
$string['couldnotupdate'] = 'Could not update your post due to an unknown error';
$string['crontask'] = 'Jinoforum mailings and maintenance jobs';
$string['delete'] = 'Delete';
$string['deleteddiscussion'] = 'The discussion topic has been deleted';
$string['deletedpost'] = 'The post has been deleted';
$string['deletedposts'] = 'Those posts have been deleted';
$string['deletesure'] = 'Are you sure you want to delete this post?';
$string['deletesureplural'] = 'Are you sure you want to delete this post and all replies? ({$a} posts)';
$string['digestmailheader'] = 'This is your daily digest of new posts from the {$a->sitename} Forum(Debate)s. To change your default Forum(Debate) email preferences, go to {$a->userprefs}.';
$string['digestmailpost'] = 'Change your Forum(Debate) digest preferences';
$string['digestmailprefs'] = 'your user profile';
$string['digestmailsubject'] = '{$a}: Forum(Debate) digest';
$string['digestmailtime'] = 'Hour to send digest emails';
$string['digestsentusers'] = 'Email digests successfully sent to {$a} users.';
$string['disallowsubscribe'] = 'Subscriptions not allowed';
$string['disallowsubscribeteacher'] = 'Subscriptions not allowed (except for teachers)';
$string['discussion'] = 'Discussion';
$string['discussionmoved'] = 'This discussion has been moved to \'{$a}\'.';
$string['discussionmovedpost'] = 'This discussion has been moved to <a href="{$a->discusshref}">here</a> in the Forum(Debate) <a href="{$a->jinoforumhref}">{$a->jinoforumname}</a>';
$string['discussionname'] = 'Discussion name';
$string['discussions'] = 'Discussions';
$string['discussionsstartedby'] = 'Discussions started by {$a}';
$string['discussionsstartedbyrecent'] = 'Discussions recently started by {$a}';
$string['discussionsstartedbyuserincourse'] = 'Discussions started by {$a->fullname} in {$a->coursename}';
$string['discussthistopic'] = 'Discuss this topic';
$string['displayend'] = 'Display end';
$string['displayend_help'] = 'This setting specifies whether a forum post should be hidden after a certain date. If you want to display start/end date, do not select the \'disable\' option. Note that administrators can always view forum posts.';
$string['displaymode'] = 'Display mode';
$string['displayperiod'] = 'Display period';
$string['displaystart'] = 'Display start';
$string['displaystart_help'] = 'This setting specifies whether a forum post should be displayed from a certain date. If you want to display start/end date, do not select the \'disable\' option. Note that administrators can always view forum posts.';
$string['displaywordcount'] = 'Display word count';
$string['displaywordcount_help'] = 'This setting specifies whether the word count of each post should be displayed or not.';
$string['eachuserjinoforum'] = 'Each person posts one discussion';
$string['edit'] = 'Edit';
$string['editedby'] = 'Edited by {$a->name} - original submission {$a->date}';
$string['editedpostupdated'] = '{$a}\'s post was updated';
$string['editing'] = 'Editing';
$string['eventcoursesearched'] = 'Course searched';
$string['eventdiscussioncreated'] = 'Discussion created';
$string['eventdiscussionupdated'] = 'Discussion updated';
$string['eventdiscussiondeleted'] = 'Discussion deleted';
$string['eventdiscussionmoved'] = 'Discussion moved';
$string['eventdiscussionviewed'] = 'Discussion viewed';
$string['eventuserreportviewed'] = 'User report viewed';
$string['eventpostcreated'] = 'Post created';
$string['eventpostdeleted'] = 'Post deleted';
$string['eventpostupdated'] = 'Post updated';
$string['eventreadtrackingdisabled'] = 'Read tracking disabled';
$string['eventreadtrackingenabled'] = 'Read tracking enabled';
$string['eventsubscribersviewed'] = 'Subscribers viewed';
$string['eventsubscriptioncreated'] = 'Subscription created';
$string['eventsubscriptiondeleted'] = 'Subscription deleted';
$string['emaildigestcompleteshort'] = 'Complete posts';
$string['emaildigestdefault'] = 'Default ({$a})';
$string['emaildigestoffshort'] = 'No digest';
$string['emaildigestsubjectsshort'] = 'Subjects only';
$string['emaildigesttype'] = 'Email digest options';
$string['emaildigesttype_help'] = 'The type of notification that you will receive for each Forum(Debate).

* Default - follow the digest setting found in your user profile. If you update your profile, then that change will be reflected here too;
* No digest - you will receive one e-mail per Forum(Debate) post;
* Digest - complete posts - you will receive one digest e-mail per day containing the complete contents of each Forum(Debate) post;
* Digest - subjects only - you will receive one digest e-mail per day containing just the subject of each Forum(Debate) post.
';
$string['emaildigestupdated'] = 'The e-mail digest option was changed to \'{$a->maildigesttitle}\' for the Forum(Debate) \'{$a->jinoforum}\'. {$a->maildigestdescription}';
$string['emaildigestupdated_default'] = 'Your default profile setting of \'{$a->maildigesttitle}\' was used for the Forum(Debate) \'{$a->jinoforum}\'. {$a->maildigestdescription}.';
$string['emaildigest_0'] = 'You will receive one e-mail per Forum(Debate) post.';
$string['emaildigest_1'] = 'You will receive one digest e-mail per day containing the complete contents of each Forum(Debate) post.';
$string['emaildigest_2'] = 'You will receive one digest e-mail per day containing the subject of each Forum(Debate) post.';
$string['emptymessage'] = 'Something was wrong with your post. Perhaps you left it blank, or the attachment was too big. Your changes have NOT been saved.';
$string['erroremptymessage'] = 'Post message cannot be empty';
$string['erroremptysubject'] = 'Post subject cannot be empty.';
$string['errorenrolmentrequired'] = 'You must be enrolled in this course to access this content';
$string['errorwhiledelete'] = 'An error occurred while deleting record.';
$string['eventassessableuploaded'] = 'Some content has been posted.';
$string['everyonecanchoose'] = 'Everyone can choose to be subscribed';
$string['everyonecannowchoose'] = 'Everyone can now choose to be subscribed';
$string['everyoneisnowsubscribed'] = 'Everyone is now subscribed to this Forum(Debate)';
$string['everyoneissubscribed'] = 'Everyone is subscribed to this Forum(Debate)';
$string['existingsubscribers'] = 'Existing subscribers';
$string['exportdiscussion'] = 'Export whole discussion';
$string['forcedreadtracking'] = 'Allow forced read tracking';
$string['forcedreadtracking_desc'] = 'Allows Forum(Debate)s to be set to forced read tracking. Will result in decreased performance for some users, particularly on courses with many Forum(Debate)s and posts. When off, any Forum(Debate)s previously set to Forced are treated as optional.';
$string['forcessubscribe'] = 'This Forum(Debate) forces everyone to be subscribed';
$string['jinoforum'] = 'Jinoforum';
$string['jinoforum:addinstance'] = 'Add a new Forum(Debate)';
$string['jinoforum:addnews'] = 'Add news';
$string['jinoforum:addquestion'] = 'Add question';
$string['jinoforum:allowforcesubscribe'] = 'Allow force subscribe';
$string['jinoforumauthorhidden'] = 'Author (hidden)';
$string['jinoforumblockingalmosttoomanyposts'] = 'You are approaching the posting threshold. You have posted {$a->numposts} times in the last {$a->blockperiod} and the limit is {$a->blockafter} posts.';
$string['jinoforumbodyhidden'] = 'This post cannot be viewed by you, probably because you have not posted in the discussion, the maximum editing time hasn\'t passed yet, the discussion has not started or the discussion has expired.';
$string['jinoforum:createattachment'] = 'Create attachments';
$string['jinoforum:deleteanypost'] = 'Delete any posts (anytime)';
$string['jinoforum:deleteownpost'] = 'Delete own posts (within deadline)';
$string['jinoforum:editanypost'] = 'Edit any post';
$string['jinoforum:exportdiscussion'] = 'Export whole discussion';
$string['jinoforum:exportownpost'] = 'Export own post';
$string['jinoforum:exportpost'] = 'Export post';
$string['jinoforumintro'] = 'Description';
$string['jinoforum:managesubscriptions'] = 'Manage subscriptions';
$string['jinoforum:movediscussions'] = 'Move discussions';
$string['jinoforum:postwithoutthrottling'] = 'Exempt from post threshold';
$string['jinoforumname'] = 'Jinoforum name';
$string['jinoforumposts'] = 'Jinoforum posts';
$string['jinoforum:rate'] = 'Rate posts';
$string['jinoforum:replynews'] = 'Reply to news';
$string['jinoforum:replypost'] = 'Reply to posts';
$string['jinoforums'] = 'Jinoforums';
$string['jinoforum:splitdiscussions'] = 'Split discussions';
$string['jinoforum:startdiscussion'] = 'Start new discussions';
$string['jinoforumsubjecthidden'] = 'Subject (hidden)';
$string['jinoforumtracked'] = 'Unread posts are being tracked';
$string['jinoforumtrackednot'] = 'Unread posts are not being tracked';
$string['jinoforumtype'] = 'Jinoforum type';
$string['jinoforumtype_help'] = 'There are 5 Forum(Debate) types:

* A single simple discussion - A single discussion topic which everyone can reply to (cannot be used with separate groups)
* Each person posts one discussion - Each student can post exactly one new discussion topic, which everyone can then reply to
* Q and A Forum(Debate) - Students must first post their perspectives before viewing other students\' posts
* Standard Forum(Debate) displayed in a blog-like format - An open Forum(Debate) where anyone can start a new discussion at any time, and in which discussion topics are displayed on one page with "Discuss this topic" links
* Standard Forum(Debate) for general use - An open Forum(Debate) where anyone can start a new discussion at any time';
$string['bstyle_thred'] = 'Thread';
$string['bstyle_list'] = 'List';
$string['jinoforum:viewallratings'] = 'View all raw ratings given by individuals';
$string['jinoforum:viewanyrating'] = 'View total ratings that anyone received';
$string['jinoforum:viewdiscussion'] = 'View discussions';
$string['jinoforum:viewhiddentimedposts'] = 'View hidden timed posts';
$string['jinoforum:viewqandawithoutposting'] = 'Always see Q and A posts';
$string['jinoforum:viewrating'] = 'View the total rating you received';
$string['jinoforum:viewsubscribers'] = 'View subscribers';
$string['generaljinoforum'] = 'Standard Forum(Debate) for general use';
$string['generaljinoforums'] = 'General Forum(Debate)s';
$string['hiddenjinoforumpost'] = 'Hidden Forum(Debate) post';
$string['hour'] = "hour";
$string['injinoforum'] = 'in {$a}';
$string['introblog'] = 'The posts in this Forum(Debate) were copied here automatically from blogs of users in this course because those blog entries are no longer available';
$string['intronews'] = 'General news and announcements';
$string['introsocial'] = 'An open Forum(Debate) for chatting about anything you want to';
$string['introteacher'] = 'A Forum(Debate) for teacher-only notes and discussion';
$string['invalidaccess'] = 'This page was not accessed correctly';
$string['invaliddiscussionid'] = 'Discussion ID was incorrect or no longer exists';
$string['invaliddigestsetting'] = 'An invalid mail digest setting was provided';
$string['invalidforcesubscribe'] = 'Invalid force subscription mode';
$string['invalidjinoforumid'] = 'Jinoforum ID was incorrect';
$string['invalidparentpostid'] = 'Parent post ID was incorrect';
$string['invalidpostid'] = 'Invalid post ID - {$a}';
$string['largefileattachments'] = "Large file attachments";
$string['lastpost'] = 'Last post';
$string['learningjinoforums'] = 'Learning Forum(Debate)s';
$string['longpost'] = 'Long post';
$string['list'] = 'list';
$string['mailnow'] = 'Mail now';
$string['manydiscussions'] = 'Discussions per page';
$string['markalldread'] = 'Mark all posts in this discussion read.';
$string['markallread'] = 'Mark all posts in this Forum(Debate) read.';
$string['markread'] = 'Mark read';
$string['markreadbutton'] = 'Mark<br />read';
$string['markunread'] = 'Mark unread';
$string['markunreadbutton'] = 'Mark<br />unread';
$string['maxattachments'] = 'Maximum number of attachments';
$string['maxattachments_help'] = 'This setting specifies the maximum number of files that can be attached to a Forum(Debate) post.';
$string['maxattachmentsize'] = 'Maximum attachment size';
$string['maxattachmentsize_help'] = 'This setting specifies the largest size of file that can be attached to a forum post. Sometimes it might seem as if it is possible to upload files with bigger size, but it can not be saved in the server and an error message will appear.';
$string['maxtimehaspassed'] = 'Sorry, but the maximum time for editing this post ({$a}) has passed!';
$string['message'] = 'Message';
$string['messageprovider:digests'] = 'Subscribed Forum(Debate) digests';
$string['messageprovider:posts'] = 'Subscribed Forum(Debate) posts';
$string['missingsearchterms'] = 'The following search terms occur only in the HTML markup of this message:';
$string['modeflatnewestfirst'] = 'Display replies flat, with newest first';
$string['modeflatoldestfirst'] = 'Display replies flat, with oldest first';
$string['modenested'] = 'Display replies in nested form';
$string['modethreaded'] = 'Display replies in threaded form';
$string['modulename'] = 'Forum(Debate)';
$string['modulename_help'] = 'The Forum(Debate) activity module enables participants to have asynchronous discussions i.e. discussions that take place over an extended period of time.

There are several Forum(Debate) types to choose from, such as a standard Forum(Debate) where anyone can start a new discussion at any time; a Forum(Debate) where each student can post exactly one discussion; or a question and answer Forum(Debate) where students must first post before being able to view other students\' posts. A teacher can allow files to be attached to Forum(Debate) posts. Attached images are displayed in the Forum(Debate) post.

Participants can subscribe to a Forum(Debate) to receive notifications of new Forum(Debate) posts. A teacher can set the subscription mode to optional, forced or auto, or prevent subscription completely. If required, students can be blocked from posting more than a given number of posts in a given time period; this can prevent individuals from dominating discussions.

Jinoforum posts can be rated by teachers or students (peer evaluation). Ratings can be aggregated to form a final grade which is recorded in the gradebook.

Jinoforums have many uses, such as

* A social space for students to get to know each other
* For course announcements (using a news Forum(Debate) with forced subscription)
* For discussing course content or reading materials
* For continuing online an issue raised previously in a face-to-face session
* For teacher-only discussions (using a hidden Forum(Debate))
* A help centre where tutors and students can give advice
* A one-on-one support area for private student-teacher communications (using a Forum(Debate) with separate groups and with one student per group)
* For extension activities, for example ‘brain teasers’ for students to ponder and suggest solutions to';
$string['modulename_link'] = 'mod/jinoforum/view';
$string['modulenameplural'] = 'Jinoforums';
$string['more'] = 'more';
$string['movedmarker'] = '(Moved)';
$string['movethisdiscussionto'] = 'Move this discussion to ...';
$string['mustprovidediscussionorpost'] = 'You must provide either a discussion id or post id to export';
$string['namenews'] = 'News Forum(Debate)';
$string['namenews_help'] = 'The news Forum(Debate) is a special Forum(Debate) for announcements that is automatically created when a course is created. A course can have only one news Forum(Debate). Only teachers and administrators can post in the news Forum(Debate). The "Latest news" block will display recent discussions from the news Forum(Debate).';
$string['namesocial'] = 'Social Forum(Debate)';
$string['nameteacher'] = 'Teacher Forum(Debate)';
$string['newjinoforumposts'] = 'New Forum(Debate) posts';
$string['noattachments'] = 'There are no attachments to this post';
$string['nodiscussions'] = 'There are no discussion topics yet in this Forum(Debate)';
$string['nodiscussionsstartedby'] = '{$a} has not started any discussions';
$string['nodiscussionsstartedbyyou'] = 'You haven\'t started any discussions yet';
$string['noguestpost'] = 'Sorry, guests are not allowed to post.';
$string['noguesttracking'] = 'Sorry, guests are not allowed to set tracking options.';
$string['nomorepostscontaining'] = 'No more posts containing \'{$a}\' were found';
$string['nonews'] = 'No news has been posted yet';
$string['noonecansubscribenow'] = 'Subscriptions are now disallowed';
$string['nopermissiontosubscribe'] = 'You do not have the permission to view Forum(Debate) subscribers';
$string['nopermissiontoview'] = 'You do not have permissions to view this post';
$string['nopostjinoforum'] = 'Sorry, you are not allowed to post to this Forum(Debate)';
$string['noposts'] = 'No posts';
$string['nopostsmadebyuser'] = '{$a} has made no posts';
$string['nopostsmadebyyou'] = 'You haven\'t made any posts';
$string['noquestions'] = 'There are no questions yet in this Forum(Debate)';
$string['nosubscribers'] = 'There are no subscribers yet for this Forum(Debate)';
$string['notexists'] = 'Discussion no longer exists';
$string['nothingnew'] = 'Nothing new for {$a}';
$string['notingroup'] = 'Sorry, but you need to be part of a group to see this Forum(Debate).';
$string['notinstalled'] = 'The Forum(Debate) module is not installed';
$string['notpartofdiscussion'] = 'This post is not part of a discussion!';
$string['notrackjinoforum'] = 'Don\'t track unread posts';
$string['noviewdiscussionspermission'] = 'You do not have the permission to view discussions in this Forum(Debate)';
$string['nowallsubscribed'] = 'All Forum(Debate)s in {$a} are subscribed.';
$string['nowallunsubscribed'] = 'All Forum(Debate)s in {$a} are not subscribed.';
$string['nownotsubscribed'] = '{$a->name} will NOT be notified of new posts in \'{$a->jinoforum}\'';
$string['nownottracking'] = '{$a->name} is no longer tracking \'{$a->jinoforum}\'.';
$string['nowsubscribed'] = '{$a->name} will be notified of new posts in \'{$a->jinoforum}\'';
$string['nowtracking'] = '{$a->name} is now tracking \'{$a->jinoforum}\'.';
$string['number'] = 'number';
$string['numposts'] = '{$a} posts';
$string['olderdiscussions'] = 'Older discussions';
$string['oldertopics'] = 'Older topics';
$string['oldpostdays'] = 'Read after days';
$string['openmode0'] = 'No discussions, no replies';
$string['openmode1'] = 'No discussions, but replies are allowed';
$string['openmode2'] = 'Discussions and replies are allowed';
$string['overviewnumpostssince'] = '{$a} posts since last login';
$string['overviewnumunread'] = '{$a} total unread';
$string['page-mod-jinoforum-x'] = 'Any Forum(Debate) module page';
$string['page-mod-jinoforum-view'] = 'Jinoforum module main page';
$string['page-mod-jinoforum-discuss'] = 'Jinoforum module discussion thread page';
$string['parent'] = 'Show parent';
$string['parentofthispost'] = 'Parent of this post';
$string['pluginadministration'] = 'Jinoforum administration';
$string['pluginname'] = 'Jinoforum';
$string['postadded'] = '<p>Your post was successfully added.</p> <p>You have {$a} to edit it if you want to make any changes.</p>';
$string['postaddedsuccess'] = 'Your post was successfully added.';
$string['postaddedtimeleft'] = 'You have {$a} to edit it if you want to make any changes.';
$string['postbyuser'] = '{$a->post} by {$a->user}';
$string['postincontext'] = 'See this post in context';
$string['postmailinfo'] = 'This is a copy of a message posted on the {$a} website.

To reply click on this link:';
$string['postmailnow'] = '<p>This post will be mailed out immediately to all Forum(Debate) subscribers.</p>';
$string['postmailsubject'] = '{$a->courseshortname}: {$a->subject}';
$string['postrating1'] = 'Mostly separate knowing';
$string['postrating2'] = 'Separate and connected';
$string['postrating3'] = 'Mostly connected knowing';
$string['posts'] = 'Posts';
$string['postsmadebyuser'] = 'Posts made by {$a}';
$string['postsmadebyuserincourse'] = 'Posts made by {$a->fullname} in {$a->coursename}';
$string['posttojinoforum'] = 'Post to Forum(Debate)';
$string['postupdated'] = 'Your post was updated';
$string['potentialsubscribers'] = 'Potential subscribers';
$string['processingdigest'] = 'Processing email digest for user {$a}';
$string['processingpost'] = 'Processing post {$a}';
$string['prune'] = 'Split';
$string['prunedpost'] = 'A new discussion has been created from that post';
$string['pruneheading'] = 'Split the discussion and move this post to a new discussion';
$string['qandajinoforum'] = 'Q and A Forum(Debate)';
$string['qandanotify'] = 'This is a question and answer Forum(Debate). In order to see other responses to these questions, you must first post your answer';
$string['re'] = 'Re:';
$string['readtherest'] = 'Read the rest of this topic';
$string['replies'] = 'Replies';
$string['replies_num'] = 'Number of Replies';
$string['repliesmany'] = '{$a} replies so far';
$string['repliesone'] = '{$a} reply so far';
$string['reply'] = 'Reply';
$string['replyjinoforum'] = 'Reply to Forum(Debate)';
$string['replytouser'] = 'Use email address in reply';
$string['resetjinoforums'] = 'Delete posts from';
$string['resetjinoforumsall'] = 'Delete all posts';
$string['resetdigests'] = 'Delete all per-user Forum(Debate) digest preferences';
$string['resetsubscriptions'] = 'Delete all Forum(Debate) subscriptions';
$string['resettrackprefs'] = 'Delete all Forum(Debate) tracking preferences';
$string['rsssubscriberssdiscussions'] = 'RSS feed of discussions';
$string['rsssubscriberssposts'] = 'RSS feed of posts';
$string['rssarticles'] = 'Number of RSS recent articles';
$string['rssarticles_help'] = 'This setting specifies the number of articles (either discussions or posts) to include in the RSS feed. Between 5 and 20 generally acceptable. If Forum(Debate) is frequently used, use a larger value.';
$string['rsstype'] = 'RSS feed for this activity';
$string['rsstype_help'] = 'This setting allows the Forum(Debate)s to be exported to RSS. To enable the RSS feed for this activity, select either discussions or posts to be included in the feed. You can choose between two options. Thread: This option exports the original post and new discussions to RSS. Posts: This option exports only the recently published posts to RSS.';
$string['search'] = 'Search';
$string['searchdatefrom'] = 'Posts must be newer than this';
$string['searchdateto'] = 'Posts must be older than this';
$string['searchjinoforumintro'] = 'Please enter search terms into one or more of the following fields:';
$string['searchjinoforums'] = 'Search Forum(Debate)s';
$string['searchfullwords'] = 'These words should appear as whole words';
$string['searchnotwords'] = 'These words should NOT be included';
$string['searcholderposts'] = 'Search older posts...';
$string['searchphrase'] = 'This exact phrase must appear in the post';
$string['searchresults'] = 'Search results';
$string['searchsubject'] = 'These words should be in the subject';
$string['searchuser'] = 'This name should match the author';
$string['searchuserid'] = 'The Moodle ID of the author';
$string['searchwhichjinoforums'] = 'Choose which Forum(Debate)s to search';
$string['searchwords'] = 'These words can appear anywhere in the post';
$string['seeallposts'] = 'See all posts made by this user';
$string['shortpost'] = 'Short post';
$string['showsubscribers'] = 'Show/edit current subscribers';
$string['singlejinoforum'] = 'A single simple discussion';
$string['smallmessage'] = '{$a->user} posted in {$a->jinoforumname}';
$string['startedby'] = 'Started by';
$string['subject'] = 'Subject';
$string['subscribe'] = 'Subscribe to this Forum(Debate)';
$string['subscribeall'] = 'Subscribe everyone to this Forum(Debate)';
$string['subscribeenrolledonly'] = 'Sorry, only enrolled users are allowed to subscribe to Forum(Debate) post notifications.';
$string['subscribed'] = 'Subscribed';
$string['subscribenone'] = 'Unsubscribe everyone from this Forum(Debate)';
$string['subscribers'] = 'Subscribers';
$string['subscribersto'] = 'Subscribers to \'{$a}\'';
$string['subscribestart'] = 'Send me email copies of posts to this Forum(Debate)';
$string['subscribestop'] = 'I don\'t want email copies of posts to this Forum(Debate)';
$string['subscription'] = 'Subscription';
$string['subscription_help'] = 'If you are subscribed to a Forum(Debate) it means you will receive email copies of Forum(Debate) posts. Usually you can choose whether you wish to be subscribed, though sometimes subscription is forced so that everyone receives email copies of Forum(Debate) posts.';
$string['subscriptionandtracking'] = 'Subscription and tracking';
$string['subscriptionmode'] = 'Subscription mode';
$string['subscriptionmode_help'] = 'When a participant is subscribed to a Forum(Debate) it means they will receive Forum(Debate) post notifications. There are 4 subscription mode options:

* Optional subscription - Participants can choose whether to be subscribed
* Forced subscription - Everyone is subscribed and cannot unsubscribe
* Auto subscription - Everyone is subscribed initially but can choose to unsubscribe at any time
* Subscription disabled - Subscriptions are not allowed

Note: Any subscription mode changes will only affect users who enrol in the course in the future, and not existing users.';
$string['subscriptionoptional'] = 'Optional subscription';
$string['subscriptionforced'] = 'Forced subscription';
$string['subscriptionauto'] = 'Auto subscription';
$string['subscriptiondisabled'] = 'Subscription disabled';
$string['subscriptions'] = 'Subscriptions';
$string['sort'] = 'Display by {$a}';
$string['thisjinoforumisthrottled'] = 'This Forum(Debate) has a limit to the number of Forum(Debate) postings you can make in a given time period - this is currently set at {$a->blockafter} posting(s) in {$a->blockperiod}';
$string['timedposts'] = 'Timed posts';
$string['timestartenderror'] = 'Display end date cannot be earlier than the start date';
$string['trackjinoforum'] = 'Track unread posts';
$string['tracking'] = 'Track';
$string['trackingoff'] = 'Off';
$string['trackingon'] = 'Forced';
$string['trackingoptional'] = 'Optional';
$string['trackingtype'] = 'Read tracking';
$string['trackingtype_help'] = 'If enabled, participants can track read and unread posts in the Forum(Debate) and in discussions. There are three options:

* Optional - Participants can choose whether to turn tracking on or off via a link in the administration block. Jinoforum tracking must also be enabled in the user\'s profile settings.
* Forced - Tracking is always on, regardless of user setting. Available depending on administrative setting.
* Off - Read and unread posts are not tracked.';
$string['unread'] = 'Unread';
$string['unreadposts'] = 'Unread posts';
$string['unreadpostsnumber'] = '{$a} unread posts';
$string['unreadpostsone'] = '1 unread post';
$string['unsubscribe'] = 'Unsubscribe from this Forum(Debate)';
$string['unsubscribeall'] = 'Unsubscribe from all Forum(Debate)s';
$string['unsubscribeallconfirm'] = 'You are subscribed to {$a} Forum(Debate)s now. Do you really want to unsubscribe from all Forum(Debate)s and disable Forum(Debate) auto-subscribe?';
$string['unsubscribealldone'] = 'All optional Forum(Debate) subscriptions were removed. You will still receive notifications from Forum(Debate)s with forced subscription. To manage Forum(Debate) notifications go to Messaging in My Profile Settings.';
$string['unsubscribeallempty'] = 'You are not subscribed to any Forum(Debate)s. To disable all notifications from this server go to Messaging in My Profile Settings.';
$string['unsubscribed'] = 'Unsubscribed';
$string['unsubscribeshort'] = 'Unsubscribe';
$string['usermarksread'] = 'Manual message read marking';
$string['viewalldiscussions'] = 'View all discussions';
$string['warnafter'] = 'Post threshold for warning';
$string['warnafter_help'] = 'Students can be warned as they approach the maximum number of posts allowed in a given period. This setting specifies after how many posts they are warned. Users with the capability mod/jinoforum:postwithoutthrottling are exempt from post limits.';
$string['warnformorepost'] = 'Warning! There is more than one discussion in this Forum(Debate) - using the most recent';
$string['wdate'] = 'The day of the writer';
$string['yournewquestion'] = 'Your new question';
$string['yournewtopic'] = 'Your new discussion topic';
$string['yourreply'] = 'Your reply';

$string['comment'] = 'comment';
$string['recommend'] = 'recommend';
$string['hit'] = 'hit';
$string['emptycomment'] = 'Please enter a comment';
$string['writer'] = 'writer';
$string['allowpublic'] = 'public';
$string['studentindiscussion'] = 'Students Involved In A Discussion';
$string['public'] = 'Public';
$string['closed'] = 'Closed';
$string['recommend:cnt'] = 'Recommend';
$string['reply:cnt'] = 'reply';
$string['comment:cnt'] = 'Comment';
$string['content:file'] = 'Attachments';
$string['next_content'] = 'Next Content';
$string['current_content'] = 'Current Content';
$string['prev_content'] = 'Prev Content';
$string['view_detail'] = 'Details';

$string['deletedfile'] = 'The attached file was deleted in server because of its big size after (end of) the semester when it was uploaded.';
$string['public_distinction'] = 'Public Distinction';
$string['real_recommend'] = 'Really would you recommend?';
$string['already_recommended'] = 'Have already recommended.';