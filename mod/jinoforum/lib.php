<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/** Include required files */
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/eventslib.php');
require_once($CFG->dirroot . '/user/selector/lib.php');

/// CONSTANTS ///////////////////////////////////////////////////////////

define('JINOFORUM_MODE_FLATOLDEST', 1);
define('JINOFORUM_MODE_FLATNEWEST', -1);
define('JINOFORUM_MODE_THREADED', 2);
define('JINOFORUM_MODE_NESTED', 3);
define('JINOFORUM_MODE_LIST', 5);

define('JINOFORUM_CHOOSESUBSCRIBE', 0);
define('JINOFORUM_FORCESUBSCRIBE', 1);
define('JINOFORUM_INITIALSUBSCRIBE', 2);
define('JINOFORUM_DISALLOWSUBSCRIBE', 3);

/**
 * JINOFORUM_TRACKING_OFF - Tracking is not available for this jinoforum.
 */
define('JINOFORUM_TRACKING_OFF', 0);

/**
 * JINOFORUM_TRACKING_OPTIONAL - Tracking is based on user preference.
 */
define('JINOFORUM_TRACKING_OPTIONAL', 1);

/**
 * JINOFORUM_TRACKING_FORCED - Tracking is on, regardless of user setting.
 * Treated as JINOFORUM_TRACKING_OPTIONAL if $CFG->jinoforum_allowforcedreadtracking is off.
 */
define('JINOFORUM_TRACKING_FORCED', 2);

/**
 * JINOFORUM_TRACKING_ON - deprecated alias for JINOFORUM_TRACKING_FORCED.
 * @deprecated since 2.6
 */
define('JINOFORUM_TRACKING_ON', 2);

define('SMALL_FILE', 5242880);
define('LARGE_FILE', 5368709120);

define('JINOFORUM_MAILED_PENDING', 0);
define('JINOFORUM_MAILED_SUCCESS', 1);
define('JINOFORUM_MAILED_ERROR', 2);

if (!defined('JINOFORUM_CRON_USER_CACHE')) {
    /** Defines how many full user records are cached in jinoforum cron. */
    define('JINOFORUM_CRON_USER_CACHE', 5000);
}

/// STANDARD FUNCTIONS ///////////////////////////////////////////////////////////

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $jinoforum add jinoforum instance
 * @param mod_jinoforum_mod_form $mform
 * @return int intance id
 */
function jinoforum_add_instance($jinoforum, $mform = null) {
    global $CFG, $DB;

    $jinoforum->timemodified = time();

    if (empty($jinoforum->assessed)) {
        $jinoforum->assessed = 0;
    }

    $jinoforum->maxbytes = ($jinoforum->largefileattachments) ? LARGE_FILE : SMALL_FILE;
    if (empty($jinoforum->ratingtime) or empty($jinoforum->assessed)) {
        $jinoforum->assesstimestart = 0;
        $jinoforum->assesstimefinish = 0;
    }

    $jinoforum->id = $DB->insert_record('jinoforum', $jinoforum);
    $modcontext = context_module::instance($jinoforum->coursemodule);

    if ($jinoforum->type == 'single') {  // Create related discussion.
        $discussion = new stdClass();
        $discussion->course = $jinoforum->course;
        $discussion->jinoforum = $jinoforum->id;
        $discussion->name = $jinoforum->name;
        $discussion->assessed = $jinoforum->assessed;
        $discussion->message = $jinoforum->intro;
        $discussion->messageformat = $jinoforum->introformat;
        $discussion->messagetrust = trusttext_trusted(context_course::instance($jinoforum->course));
        $discussion->mailnow = false;
        $discussion->groupid = -1;

        $message = '';

        $discussion->id = jinoforum_add_discussion($discussion, null, $message);

        if ($mform and $draftid = file_get_submitted_draft_itemid('introeditor')) {
            // Ugly hack - we need to copy the files somehow.
            $discussion = $DB->get_record('jinoforum_discussions', array('id' => $discussion->id), '*', MUST_EXIST);
            $post = $DB->get_record('jinoforum_posts', array('id' => $discussion->firstpost), '*', MUST_EXIST);

            $options = array('subdirs' => true); // Use the same options as intro field!
            $post->message = file_save_draft_area_files($draftid, $modcontext->id, 'mod_jinoforum', 'post', $post->id, $options, $post->message);
            $DB->set_field('jinoforum_posts', 'message', $post->message, array('id' => $post->id));
        }
    }

    jinoforum_grade_item_update($jinoforum);

    return $jinoforum->id;
}

/**
 * Handle changes following the creation of a jinoforum instance.
 * This function is typically called by the course_module_created observer.
 *
 * @param object $context the jinoforum context
 * @param stdClass $jinoforum The jinoforum object
 * @return void
 */
function jinoforum_instance_created($context, $jinoforum) {
    if ($jinoforum->forcesubscribe == JINOFORUM_INITIALSUBSCRIBE) {
        $users = jinoforum_get_potential_subscribers($context, 0, 'u.id, u.email');
        foreach ($users as $user) {
            jinoforum_subscribe($user->id, $jinoforum->id, $context);
        }
    }
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @global object
 * @param object $jinoforum jinoforum instance (with magic quotes)
 * @return bool success
 */
function jinoforum_update_instance($jinoforum, $mform) {
    global $DB, $OUTPUT, $USER;

    $jinoforum->timemodified = time();
    $jinoforum->id = $jinoforum->instance;

    if (empty($jinoforum->assessed)) {
        $jinoforum->assessed = 0;
    }
    $jinoforum->maxbytes = ($jinoforum->largefileattachments) ? LARGE_FILE : SMALL_FILE;
    if (empty($jinoforum->ratingtime) or empty($jinoforum->assessed)) {
        $jinoforum->assesstimestart = 0;
        $jinoforum->assesstimefinish = 0;
    }

    $oldjinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum->id));

    // MDL-3942 - if the aggregation type or scale (i.e. max grade) changes then recalculate the grades for the entire jinoforum
    // if  scale changes - do we need to recheck the ratings, if ratings higher than scale how do we want to respond?
    // for count and sum aggregation types the grade we check to make sure they do not exceed the scale (i.e. max score) when calculating the grade
    if (($oldjinoforum->assessed <> $jinoforum->assessed) or ( $oldjinoforum->scale <> $jinoforum->scale)) {
        jinoforum_update_grades($jinoforum); // recalculate grades for the jinoforum
    }

    if ($jinoforum->type == 'single') {  // Update related discussion and post.
        $discussions = $DB->get_records('jinoforum_discussions', array('jinoforum' => $jinoforum->id), 'timemodified ASC');
        if (!empty($discussions)) {
            if (count($discussions) > 1) {
                echo $OUTPUT->notification(get_string('warnformorepost', 'jinoforum'));
            }
            $discussion = array_pop($discussions);
        } else {
            // try to recover by creating initial discussion - MDL-16262
            $discussion = new stdClass();
            $discussion->course = $jinoforum->course;
            $discussion->jinoforum = $jinoforum->id;
            $discussion->name = $jinoforum->name;
            $discussion->assessed = $jinoforum->assessed;
            $discussion->message = $jinoforum->intro;
            $discussion->messageformat = $jinoforum->introformat;
            $discussion->messagetrust = true;
            $discussion->mailnow = false;
            $discussion->groupid = -1;

            $message = '';

            jinoforum_add_discussion($discussion, null, $message);

            if (!$discussion = $DB->get_record('jinoforum_discussions', array('jinoforum' => $jinoforum->id))) {
                print_error('cannotadd', 'jinoforum');
            }
        }
        if (!$post = $DB->get_record('jinoforum_posts', array('id' => $discussion->firstpost))) {
            print_error('cannotfindfirstpost', 'jinoforum');
        }

        $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id);
        $modcontext = context_module::instance($cm->id, MUST_EXIST);

        $post = $DB->get_record('jinoforum_posts', array('id' => $discussion->firstpost), '*', MUST_EXIST);
        $post->subject = $jinoforum->name;
        $post->message = $jinoforum->intro;
        $post->messageformat = $jinoforum->introformat;
        $post->messagetrust = trusttext_trusted($modcontext);
        $post->modified = $jinoforum->timemodified;
        $post->userid = $USER->id; // MDL-18599, so that current teacher can take ownership of activities.

        if ($mform and $draftid = file_get_submitted_draft_itemid('introeditor')) {
            // Ugly hack - we need to copy the files somehow.
            $options = array('subdirs' => true); // Use the same options as intro field!
            $post->message = file_save_draft_area_files($draftid, $modcontext->id, 'mod_jinoforum', 'post', $post->id, $options, $post->message);
        }

        $DB->update_record('jinoforum_posts', $post);
        $discussion->name = $jinoforum->name;
        $DB->update_record('jinoforum_discussions', $discussion);
    }

    $DB->update_record('jinoforum', $jinoforum);

    $modcontext = context_module::instance($jinoforum->coursemodule);
    if (($jinoforum->forcesubscribe == JINOFORUM_INITIALSUBSCRIBE) && ($oldjinoforum->forcesubscribe <> $jinoforum->forcesubscribe)) {
        $users = jinoforum_get_potential_subscribers($modcontext, 0, 'u.id, u.email', '');
        foreach ($users as $user) {
            jinoforum_subscribe($user->id, $jinoforum->id, $modcontext);
        }
    }

    jinoforum_grade_item_update($jinoforum);

    return true;
}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @global object
 * @param int $id jinoforum instance id
 * @return bool success
 */
function jinoforum_delete_instance($id) {
    global $DB;

    if (!$jinoforum = $DB->get_record('jinoforum', array('id' => $id))) {
        return false;
    }
    if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id)) {
        return false;
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        return false;
    }

    $context = context_module::instance($cm->id);

    // now get rid of all files
    $fs = get_file_storage();
    $fs->delete_area_files($context->id);

    $result = true;

    if ($discussions = $DB->get_records('jinoforum_discussions', array('jinoforum' => $jinoforum->id))) {
        foreach ($discussions as $discussion) {
            if (!jinoforum_delete_discussion($discussion, true, $course, $cm, $jinoforum)) {
                $result = false;
            }
        }
    }

    if (!$DB->delete_records('jinoforum_digests', array('jinoforum' => $jinoforum->id))) {
        $result = false;
    }

    if (!$DB->delete_records('jinoforum_subscriptions', array('jinoforum' => $jinoforum->id))) {
        $result = false;
    }

    if (!$DB->delete_records('jinoforum_comments', array('jinoforumid' => $jinoforum->id))) {
        $result = false;
    }

    if (!$DB->delete_records('jinoforum_recommend', array('jinoforumid' => $jinoforum->id))) {
        $result = false;
    }

    jinoforum_tp_delete_read_records(-1, -1, -1, $jinoforum->id);

    if (!$DB->delete_records('jinoforum', array('id' => $jinoforum->id))) {
        $result = false;
    }

    jinoforum_grade_item_delete($jinoforum);

    return $result;
}

/**
 * Indicates API features that the jinoforum supports.
 *
 * @uses FEATURE_GROUPS
 * @uses FEATURE_GROUPINGS
 * @uses FEATURE_GROUPMEMBERSONLY
 * @uses FEATURE_MOD_INTRO
 * @uses FEATURE_COMPLETION_TRACKS_VIEWS
 * @uses FEATURE_COMPLETION_HAS_RULES
 * @uses FEATURE_GRADE_HAS_GRADE
 * @uses FEATURE_GRADE_OUTCOMES
 * @param string $feature
 * @return mixed True if yes (some features may use other values)
 */
function jinoforum_supports($feature) {
    switch ($feature) {
        case FEATURE_GROUPS: return true;
        case FEATURE_GROUPINGS: return true;
        case FEATURE_GROUPMEMBERSONLY: return true;
        case FEATURE_MOD_INTRO: return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES: return true;
        case FEATURE_GRADE_HAS_GRADE: return true;
        case FEATURE_GRADE_OUTCOMES: return true;
        case FEATURE_RATE: return true;
        case FEATURE_BACKUP_MOODLE2: return true;
        case FEATURE_SHOW_DESCRIPTION: return true;
        case FEATURE_PLAGIARISM: return true;

        default: return null;
    }
}

/**
 * Obtains the automatic completion state for this jinoforum based on any conditions
 * in jinoforum settings.
 *
 * @global object
 * @global object
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function jinoforum_get_completion_state($course, $cm, $userid, $type) {
    global $CFG, $DB;

    // Get jinoforum details
    if (!($jinoforum = $DB->get_record('jinoforum', array('id' => $cm->instance)))) {
        throw new Exception("Can't find jinoforum {$cm->instance}");
    }

    $result = $type; // Default return value

    $postcountparams = array('userid' => $userid, 'jinoforumid' => $jinoforum->id);
    $postcountsql = "
SELECT
    COUNT(1)
FROM
    {jinoforum_posts} fp
    INNER JOIN {jinoforum_discussions} fd ON fp.discussion=fd.id
WHERE
    fp.userid=:userid AND fd.jinoforum=:jinoforumid";

    if ($jinoforum->completiondiscussions) {
        $value = $jinoforum->completiondiscussions <=
                $DB->count_records('jinoforum_discussions', array('jinoforum' => $jinoforum->id, 'userid' => $userid));
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }
    if ($jinoforum->completionreplies) {
        $value = $jinoforum->completionreplies <=
                $DB->get_field_sql($postcountsql . ' AND fp.parent<>0', $postcountparams);
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }
    if ($jinoforum->completionposts) {
        $value = $jinoforum->completionposts <= $DB->get_field_sql($postcountsql, $postcountparams);
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }

    return $result;
}

/**
 * Create a message-id string to use in the custom headers of jinoforum notification emails
 *
 * message-id is used by email clients to identify emails and to nest conversations
 *
 * @param int $postid The ID of the jinoforum post we are notifying the user about
 * @param int $usertoid The ID of the user being notified
 * @param string $hostname The server's hostname
 * @return string A unique message-id
 */
function jinoforum_get_email_message_id($postid, $usertoid, $hostname) {
    return '<' . hash('sha256', $postid . 'to' . $usertoid) . '@' . $hostname . '>';
}

/**
 * Removes properties from user record that are not necessary
 * for sending post notifications.
 * @param stdClass $user
 * @return void, $user parameter is modified
 */
function jinoforum_cron_minimise_user_record(stdClass $user) {

    // We store large amount of users in one huge array,
    // make sure we do not store info there we do not actually need
    // in mail generation code or messaging.

    unset($user->institution);
    unset($user->department);
    unset($user->address);
    unset($user->city);
    unset($user->url);
    unset($user->currentlogin);
    unset($user->description);
    unset($user->descriptionformat);
}

/**
 * Function to be run periodically according to the scheduled task.
 *
 * Finds all posts that have yet to be mailed out, and mails them
 * out to all subscribers as well as other maintance tasks.
 *
 * NOTE: Since 2.7.2 this function is run by scheduled task rather
 * than standard cron.
 *
 * @todo MDL-44734 The function will be split up into seperate tasks.
 */
function jinoforum_cron() {
    global $CFG, $USER, $DB;

    $site = get_site();

    // All users that are subscribed to any post that needs sending,
    // please increase $CFG->extramemorylimit on large sites that
    // send notifications to a large number of users.
    $users = array();
    $userscount = 0; // Cached user counter - count($users) in PHP is horribly slow!!!
    // status arrays
    $mailcount = array();
    $errorcount = array();

    // caches
    $discussions = array();
    $jinoforums = array();
    $courses = array();
    $coursemodules = array();
    $subscribedusers = array();


    // Posts older than 2 days will not be mailed.  This is to avoid the problem where
    // cron has not been running for a long time, and then suddenly people are flooded
    // with mail from the past few weeks or months
    $timenow = time();
    //$endtime = $timenow - $CFG->maxeditingtime;
    $endtime = $timenow - 3600;
    $starttime = $endtime - 48 * 3600;   // Two days earlier
    // Get the list of jinoforum subscriptions for per-user per-jinoforum maildigest settings.
    $digestsset = $DB->get_recordset('jinoforum_digests', null, '', 'id, userid, jinoforum, maildigest');
    $digests = array();
    foreach ($digestsset as $thisrow) {
        if (!isset($digests[$thisrow->jinoforum])) {
            $digests[$thisrow->jinoforum] = array();
        }
        $digests[$thisrow->jinoforum][$thisrow->userid] = $thisrow->maildigest;
    }
    $digestsset->close();

    if ($posts = jinoforum_get_unmailed_posts($starttime, $endtime, $timenow)) {
        // Mark them all now as being mailed.  It's unlikely but possible there
        // might be an error later so that a post is NOT actually mailed out,
        // but since mail isn't crucial, we can accept this risk.  Doing it now
        // prevents the risk of duplicated mails, which is a worse problem.

        if (!jinoforum_mark_old_posts_as_mailed($endtime)) {
            mtrace('Errors occurred while trying to mark some posts as being mailed.');
            return false;  // Don't continue trying to mail them, in case we are in a cron loop
        }

        // checking post validity, and adding users to loop through later
        foreach ($posts as $pid => $post) {

            $discussionid = $post->discussion;
            if (!isset($discussions[$discussionid])) {
                if ($discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion))) {
                    $discussions[$discussionid] = $discussion;
                } else {
                    mtrace('Could not find discussion ' . $discussionid);
                    unset($posts[$pid]);
                    continue;
                }
            }
            $jinoforumid = $discussions[$discussionid]->jinoforum;
            if (!isset($jinoforums[$jinoforumid])) {
                if ($jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforumid))) {
                    $jinoforums[$jinoforumid] = $jinoforum;
                } else {
                    mtrace('Could not find jinoforum ' . $jinoforumid);
                    unset($posts[$pid]);
                    continue;
                }
            }
            $courseid = $jinoforums[$jinoforumid]->course;
            if (!isset($courses[$courseid])) {
                if ($course = $DB->get_record('course', array('id' => $courseid))) {
                    $courses[$courseid] = $course;
                } else {
                    mtrace('Could not find course ' . $courseid);
                    unset($posts[$pid]);
                    continue;
                }
            }
            if (!isset($coursemodules[$jinoforumid])) {
                if ($cm = get_coursemodule_from_instance('jinoforum', $jinoforumid, $courseid)) {
                    $coursemodules[$jinoforumid] = $cm;
                } else {
                    mtrace('Could not find course module for jinoforum ' . $jinoforumid);
                    unset($posts[$pid]);
                    continue;
                }
            }


            // caching subscribed users of each jinoforum
            if (!isset($subscribedusers[$jinoforumid])) {
                $modcontext = context_module::instance($coursemodules[$jinoforumid]->id);
                if ($subusers = jinoforum_subscribed_users($courses[$courseid], $jinoforums[$jinoforumid], 0, $modcontext, "u.*")) {
                    foreach ($subusers as $postuser) {
                        // this user is subscribed to this jinoforum
                        $subscribedusers[$jinoforumid][$postuser->id] = $postuser->id;
                        $userscount++;
                        if ($userscount > JINOFORUM_CRON_USER_CACHE) {
                            // Store minimal user info.
                            $minuser = new stdClass();
                            $minuser->id = $postuser->id;
                            $users[$postuser->id] = $minuser;
                        } else {
                            // Cache full user record.
                            jinoforum_cron_minimise_user_record($postuser);
                            $users[$postuser->id] = $postuser;
                        }
                    }
                    // Release memory.
                    unset($subusers);
                    unset($postuser);
                }
            }

            $mailcount[$pid] = 0;
            $errorcount[$pid] = 0;
        }
    }

    if ($users && $posts) {

        $urlinfo = parse_url($CFG->wwwroot);
        $hostname = $urlinfo['host'];

        foreach ($users as $userto) {

            core_php_time_limit::raise(120); // terminate if processing of any account takes longer than 2 minutes

            mtrace('Processing user ' . $userto->id);

            // Init user caches - we keep the cache for one cycle only,
            // otherwise it could consume too much memory.
            if (isset($userto->username)) {
                $userto = clone($userto);
            } else {
                $userto = $DB->get_record('user', array('id' => $userto->id));
                jinoforum_cron_minimise_user_record($userto);
            }
            $userto->viewfullnames = array();
            $userto->canpost = array();
            $userto->markposts = array();

            // set this so that the capabilities are cached, and environment matches receiving user
            cron_setup_user($userto);

            // reset the caches
            foreach ($coursemodules as $jinoforumid => $unused) {
                $coursemodules[$jinoforumid]->cache = new stdClass();
                $coursemodules[$jinoforumid]->cache->caps = array();
                unset($coursemodules[$jinoforumid]->uservisible);
            }

            foreach ($posts as $pid => $post) {

                // Set up the environment for the post, discussion, jinoforum, course
                $discussion = $discussions[$post->discussion];
                $jinoforum = $jinoforums[$discussion->jinoforum];
                $course = $courses[$jinoforum->course];
                $cm = & $coursemodules[$jinoforum->id];

                // Do some checks  to see if we can bail out now
                // Only active enrolled users are in the list of subscribers
                if (!isset($subscribedusers[$jinoforum->id][$userto->id])) {
                    continue; // user does not subscribe to this jinoforum
                }

                // Don't send email if the jinoforum is Q&A and the user has not posted
                // Initial topics are still mailed
                if ($jinoforum->type == 'qanda' && !jinoforum_get_user_posted_time($discussion->id, $userto->id) && $pid != $discussion->firstpost) {
                    mtrace('Did not email ' . $userto->id . ' because user has not posted in discussion');
                    continue;
                }

                // Get info about the sending user
                if (array_key_exists($post->userid, $users)) { // we might know him/her already
                    $userfrom = $users[$post->userid];
                    if (!isset($userfrom->idnumber)) {
                        // Minimalised user info, fetch full record.
                        $userfrom = $DB->get_record('user', array('id' => $userfrom->id));
                        jinoforum_cron_minimise_user_record($userfrom);
                    }
                } else if ($userfrom = $DB->get_record('user', array('id' => $post->userid))) {
                    jinoforum_cron_minimise_user_record($userfrom);
                    // Fetch only once if possible, we can add it to user list, it will be skipped anyway.
                    if ($userscount <= JINOFORUM_CRON_USER_CACHE) {
                        $userscount++;
                        $users[$userfrom->id] = $userfrom;
                    }
                } else {
                    mtrace('Could not find user ' . $post->userid);
                    continue;
                }

                //if we want to check that userto and userfrom are not the same person this is probably the spot to do it
                // setup global $COURSE properly - needed for roles and languages
                cron_setup_user($userto, $course);

                // Fill caches
                if (!isset($userto->viewfullnames[$jinoforum->id])) {
                    $modcontext = context_module::instance($cm->id);
                    $userto->viewfullnames[$jinoforum->id] = has_capability('moodle/site:viewfullnames', $modcontext);
                }
                if (!isset($userto->canpost[$discussion->id])) {
                    $modcontext = context_module::instance($cm->id);
                    $userto->canpost[$discussion->id] = jinoforum_user_can_post($jinoforum, $discussion, $userto, $cm, $course, $modcontext);
                }
                if (!isset($userfrom->groups[$jinoforum->id])) {
                    if (!isset($userfrom->groups)) {
                        $userfrom->groups = array();
                        if (isset($users[$userfrom->id])) {
                            $users[$userfrom->id]->groups = array();
                        }
                    }
                    $userfrom->groups[$jinoforum->id] = groups_get_all_groups($course->id, $userfrom->id, $cm->groupingid);
                    if (isset($users[$userfrom->id])) {
                        $users[$userfrom->id]->groups[$jinoforum->id] = $userfrom->groups[$jinoforum->id];
                    }
                }

                // Make sure groups allow this user to see this email
                if ($discussion->groupid > 0 and $groupmode = groups_get_activity_groupmode($cm, $course)) {   // Groups are being used
                    if (!groups_group_exists($discussion->groupid)) { // Can't find group
                        continue;   // Be safe and don't send it to anyone
                    }

                    if (!groups_is_member($discussion->groupid) and ! has_capability('moodle/site:accessallgroups', $modcontext)) {
                        // do not send posts from other groups when in SEPARATEGROUPS or VISIBLEGROUPS
                        continue;
                    }
                }

                // Make sure we're allowed to see it...
                if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, NULL, $cm)) {
                    mtrace('user ' . $userto->id . ' can not see ' . $post->id);
                    continue;
                }

                // OK so we need to send the email.
                // Does the user want this post in a digest?  If so postpone it for now.
                $maildigest = jinoforum_get_user_maildigest_bulk($digests, $userto, $jinoforum->id);

                if ($maildigest > 0) {
                    // This user wants the mails to be in digest form
                    $queue = new stdClass();
                    $queue->userid = $userto->id;
                    $queue->discussionid = $discussion->id;
                    $queue->postid = $post->id;
                    $queue->timemodified = $post->created;
                    $DB->insert_record('jinoforum_queue', $queue);
                    continue;
                }


                // Prepare to actually send the post now, and build up the content

                $cleanjinoforumname = str_replace('"', "'", strip_tags(format_string($jinoforum->name)));

                $userfrom->customheaders = array(// Headers to make emails easier to track
                    'Precedence: Bulk',
                    'List-Id: "' . $cleanjinoforumname . '" <moodlejinoforum' . $jinoforum->id . '@' . $hostname . '>',
                    'List-Help: ' . $CFG->wwwroot . '/mod/jinoforum/view.php?f=' . $jinoforum->id,
                    'Message-ID: ' . jinoforum_get_email_message_id($post->id, $userto->id, $hostname),
                    'X-Course-Id: ' . $course->id,
                    'X-Course-Name: ' . format_string($course->fullname, true)
                );

                if ($post->parent) {  // This post is a reply, so add headers for threading (see MDL-22551)
                    $userfrom->customheaders[] = 'In-Reply-To: ' . jinoforum_get_email_message_id($post->parent, $userto->id, $hostname);
                    $userfrom->customheaders[] = 'References: ' . jinoforum_get_email_message_id($post->parent, $userto->id, $hostname);
                }

                $shortname = format_string($course->shortname, true, array('context' => context_course::instance($course->id)));

                $a = new stdClass();
                $a->courseshortname = $shortname;
                $a->jinoforumname = $cleanjinoforumname;
                $a->subject = format_string($post->subject, true);
                $postsubject = html_to_text(get_string('postmailsubject', 'jinoforum', $a));
                $posttext = jinoforum_make_mail_text($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto);
                $posthtml = jinoforum_make_mail_html($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto);

                // Send the post now!

                mtrace('Sending ', '');

                $eventdata = new stdClass();
                $eventdata->component = 'mod_jinoforum';
                $eventdata->name = 'posts';
                $eventdata->userfrom = $userfrom;
                $eventdata->userto = $userto;
                $eventdata->subject = $postsubject;
                $eventdata->fullmessage = $posttext;
                $eventdata->fullmessageformat = FORMAT_PLAIN;
                $eventdata->fullmessagehtml = $posthtml;
                $eventdata->notification = 1;

                // If jinoforum_replytouser is not set then send mail using the noreplyaddress.
                if (empty($CFG->jinoforum_replytouser)) {
                    // Clone userfrom as it is referenced by $users.
                    $cloneduserfrom = clone($userfrom);
                    $cloneduserfrom->email = $CFG->noreplyaddress;
                    $eventdata->userfrom = $cloneduserfrom;
                }

                $smallmessagestrings = new stdClass();
                $smallmessagestrings->user = fullname($userfrom);
                $smallmessagestrings->jinoforumname = "$shortname: " . format_string($jinoforum->name, true) . ": " . $discussion->name;
                $smallmessagestrings->message = $post->message;
                //make sure strings are in message recipients language
                $eventdata->smallmessage = get_string_manager()->get_string('smallmessage', 'jinoforum', $smallmessagestrings, $userto->lang);

                $eventdata->contexturl = "{$CFG->wwwroot}/mod/jinoforum/discuss.php?d={$discussion->id}#p{$post->id}";
                $eventdata->contexturlname = $discussion->name;

                $mailresult = message_send($eventdata);
                if (!$mailresult) {
                    mtrace("Error: mod/jinoforum/lib.php jinoforum_cron(): Could not send out mail for id $post->id to user $userto->id" .
                            " ($userto->email) .. not trying again.");
                    $errorcount[$post->id] ++;
                } else {
                    $mailcount[$post->id] ++;

                    // Mark post as read if jinoforum_usermarksread is set off
                    if (!$CFG->jinoforum_usermarksread) {
                        $userto->markposts[$post->id] = $post->id;
                    }
                }

                mtrace('post ' . $post->id . ': ' . $post->subject);
            }

            // mark processed posts as read
            jinoforum_tp_mark_posts_read($userto, $userto->markposts);
            unset($userto);
        }
    }

    if ($posts) {
        foreach ($posts as $post) {
            mtrace($mailcount[$post->id] . " users were sent post $post->id, '$post->subject'");
            if ($errorcount[$post->id]) {
                $DB->set_field('jinoforum_posts', 'mailed', JINOFORUM_MAILED_ERROR, array('id' => $post->id));
            }
        }
    }

    // release some memory
    unset($subscribedusers);
    unset($mailcount);
    unset($errorcount);

    cron_setup_user();

    $sitetimezone = $CFG->timezone;

    // Now see if there are any digest mails waiting to be sent, and if we should send them

    mtrace('Starting digest processing...');

    core_php_time_limit::raise(300); // terminate if not able to fetch all digests in 5 minutes

    if (!isset($CFG->digestmailtimelast)) { // To catch the first time
        set_config('digestmailtimelast', 0);
    }

    $timenow = time();
    $digesttime = usergetmidnight($timenow, $sitetimezone) + ($CFG->digestmailtime * 3600);

    // Delete any really old ones (normally there shouldn't be any)
    $weekago = $timenow - (7 * 24 * 3600);
    $DB->delete_records_select('jinoforum_queue', "timemodified < ?", array($weekago));
    mtrace('Cleaned old digest records');

    if ($CFG->digestmailtimelast < $digesttime and $timenow > $digesttime) {

        mtrace('Sending jinoforum digests: ' . userdate($timenow, '', $sitetimezone));

        $digestposts_rs = $DB->get_recordset_select('jinoforum_queue', "timemodified < ?", array($digesttime));

        if ($digestposts_rs->valid()) {

            // We have work to do
            $usermailcount = 0;

            //caches - reuse the those filled before too
            $discussionposts = array();
            $userdiscussions = array();

            foreach ($digestposts_rs as $digestpost) {
                if (!isset($posts[$digestpost->postid])) {
                    if ($post = $DB->get_record('jinoforum_posts', array('id' => $digestpost->postid))) {
                        $posts[$digestpost->postid] = $post;
                    } else {
                        continue;
                    }
                }
                $discussionid = $digestpost->discussionid;
                if (!isset($discussions[$discussionid])) {
                    if ($discussion = $DB->get_record('jinoforum_discussions', array('id' => $discussionid))) {
                        $discussions[$discussionid] = $discussion;
                    } else {
                        continue;
                    }
                }
                $jinoforumid = $discussions[$discussionid]->jinoforum;
                if (!isset($jinoforums[$jinoforumid])) {
                    if ($jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforumid))) {
                        $jinoforums[$jinoforumid] = $jinoforum;
                    } else {
                        continue;
                    }
                }

                $courseid = $jinoforums[$jinoforumid]->course;
                if (!isset($courses[$courseid])) {
                    if ($course = $DB->get_record('course', array('id' => $courseid))) {
                        $courses[$courseid] = $course;
                    } else {
                        continue;
                    }
                }

                if (!isset($coursemodules[$jinoforumid])) {
                    if ($cm = get_coursemodule_from_instance('jinoforum', $jinoforumid, $courseid)) {
                        $coursemodules[$jinoforumid] = $cm;
                    } else {
                        continue;
                    }
                }
                $userdiscussions[$digestpost->userid][$digestpost->discussionid] = $digestpost->discussionid;
                $discussionposts[$digestpost->discussionid][$digestpost->postid] = $digestpost->postid;
            }
            $digestposts_rs->close(); /// Finished iteration, let's close the resultset
            // Data collected, start sending out emails to each user
            foreach ($userdiscussions as $userid => $thesediscussions) {

                core_php_time_limit::raise(120); // terminate if processing of any account takes longer than 2 minutes

                cron_setup_user();

                mtrace(get_string('processingdigest', 'jinoforum', $userid), '... ');

                // First of all delete all the queue entries for this user
                $DB->delete_records_select('jinoforum_queue', "userid = ? AND timemodified < ?", array($userid, $digesttime));

                // Init user caches - we keep the cache for one cycle only,
                // otherwise it would unnecessarily consume memory.
                if (array_key_exists($userid, $users) and isset($users[$userid]->username)) {
                    $userto = clone($users[$userid]);
                } else {
                    $userto = $DB->get_record('user', array('id' => $userid));
                    jinoforum_cron_minimise_user_record($userto);
                }
                $userto->viewfullnames = array();
                $userto->canpost = array();
                $userto->markposts = array();

                // Override the language and timezone of the "current" user, so that
                // mail is customised for the receiver.
                cron_setup_user($userto);

                $postsubject = get_string('digestmailsubject', 'jinoforum', format_string($site->shortname, true));

                $headerdata = new stdClass();
                $headerdata->sitename = format_string($site->fullname, true);
                $headerdata->userprefs = $CFG->wwwroot . '/user/edit.php?id=' . $userid . '&amp;course=' . $site->id;

                $posttext = get_string('digestmailheader', 'jinoforum', $headerdata) . "\n\n";
                $headerdata->userprefs = '<a target="_blank" href="' . $headerdata->userprefs . '">' . get_string('digestmailprefs', 'jinoforum') . '</a>';

                $posthtml = "<head>";
                /*                foreach ($CFG->stylesheets as $stylesheet) {
                  //TODO: MDL-21120
                  $posthtml .= '<link rel="stylesheet" type="text/css" href="'.$stylesheet.'" />'."\n";
                  } */
                $posthtml .= "</head>\n<body id=\"email\">\n";
                $posthtml .= '<p>' . get_string('digestmailheader', 'jinoforum', $headerdata) . '</p><br /><hr size="1" noshade="noshade" />';

                foreach ($thesediscussions as $discussionid) {

                    core_php_time_limit::raise(120);   // to be reset for each post

                    $discussion = $discussions[$discussionid];
                    $jinoforum = $jinoforums[$discussion->jinoforum];
                    $course = $courses[$jinoforum->course];
                    $cm = $coursemodules[$jinoforum->id];

                    //override language
                    cron_setup_user($userto, $course);

                    // Fill caches
                    if (!isset($userto->viewfullnames[$jinoforum->id])) {
                        $modcontext = context_module::instance($cm->id);
                        $userto->viewfullnames[$jinoforum->id] = has_capability('moodle/site:viewfullnames', $modcontext);
                    }
                    if (!isset($userto->canpost[$discussion->id])) {
                        $modcontext = context_module::instance($cm->id);
                        $userto->canpost[$discussion->id] = jinoforum_user_can_post($jinoforum, $discussion, $userto, $cm, $course, $modcontext);
                    }

                    $strjinoforums = get_string('jinoforums', 'jinoforum');
                    $canunsubscribe = !jinoforum_is_forcesubscribed($jinoforum);
                    $canreply = $userto->canpost[$discussion->id];
                    $shortname = format_string($course->shortname, true, array('context' => context_course::instance($course->id)));

                    $posttext .= "\n \n";
                    $posttext .= '=====================================================================';
                    $posttext .= "\n \n";
                    $posttext .= "$shortname -> $strjinoforums -> " . format_string($jinoforum->name, true);
                    if ($discussion->name != $jinoforum->name) {
                        $posttext .= " -> " . format_string($discussion->name, true);
                    }
                    $posttext .= "\n";
                    $posttext .= $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $discussion->id;
                    $posttext .= "\n";

                    $posthtml .= "<p><font face=\"sans-serif\">" .
                            "<a target=\"_blank\" href=\"$CFG->wwwroot/course/view.php?id=$course->id\">$shortname</a> -> " .
                            "<a target=\"_blank\" href=\"$CFG->wwwroot/mod/jinoforum/index.php?id=$course->id\">$strjinoforums</a> -> " .
                            "<a target=\"_blank\" href=\"$CFG->wwwroot/mod/jinoforum/view.php?f=$jinoforum->id\">" . format_string($jinoforum->name, true) . "</a>";
                    if ($discussion->name == $jinoforum->name) {
                        $posthtml .= "</font></p>";
                    } else {
                        $posthtml .= " -> <a target=\"_blank\" href=\"$CFG->wwwroot/mod/jinoforum/discuss.php?d=$discussion->id\">" . format_string($discussion->name, true) . "</a></font></p>";
                    }
                    $posthtml .= '<p>';

                    $postsarray = $discussionposts[$discussionid];
                    sort($postsarray);

                    foreach ($postsarray as $postid) {
                        $post = $posts[$postid];

                        if (array_key_exists($post->userid, $users)) { // we might know him/her already
                            $userfrom = $users[$post->userid];
                            if (!isset($userfrom->idnumber)) {
                                $userfrom = $DB->get_record('user', array('id' => $userfrom->id));
                                jinoforum_cron_minimise_user_record($userfrom);
                            }
                        } else if ($userfrom = $DB->get_record('user', array('id' => $post->userid))) {
                            jinoforum_cron_minimise_user_record($userfrom);
                            if ($userscount <= JINOFORUM_CRON_USER_CACHE) {
                                $userscount++;
                                $users[$userfrom->id] = $userfrom;
                            }
                        } else {
                            mtrace('Could not find user ' . $post->userid);
                            continue;
                        }

                        if (!isset($userfrom->groups[$jinoforum->id])) {
                            if (!isset($userfrom->groups)) {
                                $userfrom->groups = array();
                                if (isset($users[$userfrom->id])) {
                                    $users[$userfrom->id]->groups = array();
                                }
                            }
                            $userfrom->groups[$jinoforum->id] = groups_get_all_groups($course->id, $userfrom->id, $cm->groupingid);
                            if (isset($users[$userfrom->id])) {
                                $users[$userfrom->id]->groups[$jinoforum->id] = $userfrom->groups[$jinoforum->id];
                            }
                        }

                        $userfrom->customheaders = array("Precedence: Bulk");

                        $maildigest = jinoforum_get_user_maildigest_bulk($digests, $userto, $jinoforum->id);
                        if ($maildigest == 2) {
                            // Subjects and link only
                            $posttext .= "\n";
                            $posttext .= $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $discussion->id;
                            $by = new stdClass();
                            $by->name = fullname($userfrom);
                            $by->date = userdate($post->modified);
                            $posttext .= "\n" . format_string($post->subject, true) . ' ' . get_string("bynameondate", "jinoforum", $by);
                            $posttext .= "\n---------------------------------------------------------------------";

                            $by->name = "<a target=\"_blank\" href=\"$CFG->wwwroot/user/view.php?id=$userfrom->id&amp;course=$course->id\">$by->name</a>";
                            $posthtml .= '<div><a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $discussion->id . '#p' . $post->id . '">' . format_string($post->subject, true) . '</a> ' . get_string("bynameondate", "jinoforum", $by) . '</div>';
                        } else {
                            // The full treatment
                            $posttext .= jinoforum_make_mail_text($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto, true);
                            $posthtml .= jinoforum_make_mail_post($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto, false, $canreply, true, false);

                            // Create an array of postid's for this user to mark as read.
                            if (!$CFG->jinoforum_usermarksread) {
                                $userto->markposts[$post->id] = $post->id;
                            }
                        }
                    }
                    $footerlinks = array();
                    if ($canunsubscribe) {
                        $footerlinks[] = "<a href=\"$CFG->wwwroot/mod/jinoforum/subscribe.php?id=$jinoforum->id\">" . get_string("unsubscribe", "jinoforum") . "</a>";
                    } else {
                        $footerlinks[] = get_string("everyoneissubscribed", "jinoforum");
                    }
                    $footerlinks[] = "<a href='{$CFG->wwwroot}/mod/jinoforum/index.php?id={$jinoforum->course}'>" . get_string("digestmailpost", "jinoforum") . '</a>';
                    $posthtml .= "\n<div class='mdl-right'><font size=\"1\">" . implode('&nbsp;', $footerlinks) . '</font></div>';
                    $posthtml .= '<hr size="1" noshade="noshade" /></p>';
                }
                $posthtml .= '</body>';

                if (empty($userto->mailformat) || $userto->mailformat != 1) {
                    // This user DOESN'T want to receive HTML
                    $posthtml = '';
                }

                $attachment = $attachname = '';
                // Directly email jinoforum digests rather than sending them via messaging, use the
                // site shortname as 'from name', the noreply address will be used by email_to_user.
                $mailresult = email_to_user($userto, $site->shortname, $postsubject, $posttext, $posthtml, $attachment, $attachname);

                if (!$mailresult) {
                    mtrace("ERROR: mod/jinoforum/cron.php: Could not send out digest mail to user $userto->id " .
                            "($userto->email)... not trying again.");
                } else {
                    mtrace("success.");
                    $usermailcount++;

                    // Mark post as read if jinoforum_usermarksread is set off
                    jinoforum_tp_mark_posts_read($userto, $userto->markposts);
                }
            }
        }
        /// We have finishied all digest emails, update $CFG->digestmailtimelast
        set_config('digestmailtimelast', $timenow);
    }

    cron_setup_user();

    if (!empty($usermailcount)) {
        mtrace(get_string('digestsentusers', 'jinoforum', $usermailcount));
    }

    if (!empty($CFG->jinoforum_lastreadclean)) {
        $timenow = time();
        if ($CFG->jinoforum_lastreadclean + (24 * 3600) < $timenow) {
            set_config('jinoforum_lastreadclean', $timenow);
            mtrace('Removing old jinoforum read tracking info...');
            jinoforum_tp_clean_read_records();
        }
    } else {
        set_config('jinoforum_lastreadclean', time());
    }


    return true;
}

/**
 * Builds and returns the body of the email notification in plain text.
 *
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @param object $course
 * @param object $cm
 * @param object $jinoforum
 * @param object $discussion
 * @param object $post
 * @param object $userfrom
 * @param object $userto
 * @param boolean $bare
 * @return string The email body in plain text format.
 */
function jinoforum_make_mail_text($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto, $bare = false) {
    global $CFG, $USER;

    $modcontext = context_module::instance($cm->id);

    if (!isset($userto->viewfullnames[$jinoforum->id])) {
        $viewfullnames = has_capability('moodle/site:viewfullnames', $modcontext, $userto->id);
    } else {
        $viewfullnames = $userto->viewfullnames[$jinoforum->id];
    }

    if (!isset($userto->canpost[$discussion->id])) {
        $canreply = jinoforum_user_can_post($jinoforum, $discussion, $userto, $cm, $course, $modcontext);
    } else {
        $canreply = $userto->canpost[$discussion->id];
    }

    $by = New stdClass;
    $by->name = fullname($userfrom, $viewfullnames);
    $by->date = userdate($post->modified, "", $userto->timezone);

    $strbynameondate = get_string('bynameondate', 'jinoforum', $by);

    $strjinoforums = get_string('jinoforums', 'jinoforum');

    $canunsubscribe = !jinoforum_is_forcesubscribed($jinoforum);

    $posttext = '';

    if (!$bare) {
        $shortname = format_string($course->shortname, true, array('context' => context_course::instance($course->id)));
        $posttext = "$shortname -> $strjinoforums -> " . format_string($jinoforum->name, true);

        if ($discussion->name != $jinoforum->name) {
            $posttext .= " -> " . format_string($discussion->name, true);
        }
    }

    // add absolute file links
    $post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_jinoforum', 'post', $post->id);

    $posttext .= "\n";
    $posttext .= $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $discussion->id;
    $posttext .= "\n---------------------------------------------------------------------\n";
    $posttext .= format_string($post->subject, true);
    if ($bare) {
        $posttext .= " ($CFG->wwwroot/mod/jinoforum/discuss.php?d=$discussion->id#p$post->id)";
    }
    $posttext .= "\n" . $strbynameondate . "\n";
    $posttext .= "---------------------------------------------------------------------\n";
    $posttext .= format_text_email($post->message, $post->messageformat);
    $posttext .= "\n\n";
    $posttext .= jinoforum_print_attachments($post, $cm, "text");

    if (!$bare && $canreply) {
        $posttext .= "---------------------------------------------------------------------\n";
        $posttext .= get_string("postmailinfo", "jinoforum", $shortname) . "\n";
        $posttext .= "$CFG->wwwroot/mod/jinoforum/post.php?reply=$post->id\n";
    }
    if (!$bare && $canunsubscribe) {
        $posttext .= "\n---------------------------------------------------------------------\n";
        $posttext .= get_string("unsubscribe", "jinoforum");
        $posttext .= ": $CFG->wwwroot/mod/jinoforum/subscribe.php?id=$jinoforum->id\n";
    }

    $posttext .= "\n---------------------------------------------------------------------\n";
    $posttext .= get_string("digestmailpost", "jinoforum");
    $posttext .= ": {$CFG->wwwroot}/mod/jinoforum/index.php?id={$jinoforum->course}\n";

    return $posttext;
}

/**
 * Builds and returns the body of the email notification in html format.
 *
 * @global object
 * @param object $course
 * @param object $cm
 * @param object $jinoforum
 * @param object $discussion
 * @param object $post
 * @param object $userfrom
 * @param object $userto
 * @return string The email text in HTML format
 */
function jinoforum_make_mail_html($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto) {
    global $CFG;

    if ($userto->mailformat != 1) {  // Needs to be HTML
        return '';
    }

    if (!isset($userto->canpost[$discussion->id])) {
        $canreply = jinoforum_user_can_post($jinoforum, $discussion, $userto, $cm, $course);
    } else {
        $canreply = $userto->canpost[$discussion->id];
    }

    $strjinoforums = get_string('jinoforums', 'jinoforum');
    $canunsubscribe = !jinoforum_is_forcesubscribed($jinoforum);
    $shortname = format_string($course->shortname, true, array('context' => context_course::instance($course->id)));

    $posthtml = '<head>';
    /*    foreach ($CFG->stylesheets as $stylesheet) {
      //TODO: MDL-21120
      $posthtml .= '<link rel="stylesheet" type="text/css" href="'.$stylesheet.'" />'."\n";
      } */
    $posthtml .= '</head>';
    $posthtml .= "\n<body id=\"email\">\n\n";

    $posthtml .= '<div class="navbar">' .
            '<a target="_blank" href="' . $CFG->wwwroot . '/course/view.php?id=' . $course->id . '">' . $shortname . '</a> &raquo; ' .
            '<a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/index.php?id=' . $course->id . '">' . $strjinoforums . '</a> &raquo; ' .
            '<a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/view.php?f=' . $jinoforum->id . '">' . format_string($jinoforum->name, true) . '</a>';
    if ($discussion->name == $jinoforum->name) {
        $posthtml .= '</div>';
    } else {
        $posthtml .= ' &raquo; <a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $discussion->id . '">' .
                format_string($discussion->name, true) . '</a></div>';
    }
    $posthtml .= jinoforum_make_mail_post($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto, false, $canreply, true, false);

    $footerlinks = array();
    if ($canunsubscribe) {
        $footerlinks[] = '<a href="' . $CFG->wwwroot . '/mod/jinoforum/subscribe.php?id=' . $jinoforum->id . '">' . get_string('unsubscribe', 'jinoforum') . '</a>';
        $footerlinks[] = '<a href="' . $CFG->wwwroot . '/mod/jinoforum/unsubscribeall.php">' . get_string('unsubscribeall', 'jinoforum') . '</a>';
    }
    $footerlinks[] = "<a href='{$CFG->wwwroot}/mod/jinoforum/index.php?id={$jinoforum->course}'>" . get_string('digestmailpost', 'jinoforum') . '</a>';
    $posthtml .= '<hr /><div class="mdl-align unsubscribelink">' . implode('&nbsp;', $footerlinks) . '</div>';

    $posthtml .= '</body>';

    return $posthtml;
}

/**
 *
 * @param object $course
 * @param object $user
 * @param object $mod TODO this is not used in this function, refactor
 * @param object $jinoforum
 * @return object A standard object with 2 variables: info (number of posts for this user) and time (last modified)
 */
function jinoforum_user_outline($course, $user, $mod, $jinoforum) {
    global $CFG;
    require_once("$CFG->libdir/gradelib.php");
    $grades = grade_get_grades($course->id, 'mod', 'jinoforum', $jinoforum->id, $user->id);
    if (empty($grades->items[0]->grades)) {
        $grade = false;
    } else {
        $grade = reset($grades->items[0]->grades);
    }

    $count = jinoforum_count_user_posts($jinoforum->id, $user->id);

    if ($count && $count->postcount > 0) {
        $result = new stdClass();
        $result->info = get_string("numposts", "jinoforum", $count->postcount);
        $result->time = $count->lastpost;
        if ($grade) {
            $result->info .= ', ' . get_string('grade') . ': ' . $grade->str_long_grade;
        }
        return $result;
    } else if ($grade) {
        $result = new stdClass();
        $result->info = get_string('grade') . ': ' . $grade->str_long_grade;

        //datesubmitted == time created. dategraded == time modified or time overridden
        //if grade was last modified by the user themselves use date graded. Otherwise use date submitted
        //TODO: move this copied & pasted code somewhere in the grades API. See MDL-26704
        if ($grade->usermodified == $user->id || empty($grade->datesubmitted)) {
            $result->time = $grade->dategraded;
        } else {
            $result->time = $grade->datesubmitted;
        }

        return $result;
    }
    return NULL;
}

/**
 * @global object
 * @global object
 * @param object $coure
 * @param object $user
 * @param object $mod
 * @param object $jinoforum
 */
function jinoforum_user_complete($course, $user, $mod, $jinoforum) {
    global $CFG, $USER, $OUTPUT;
    require_once("$CFG->libdir/gradelib.php");

    $grades = grade_get_grades($course->id, 'mod', 'jinoforum', $jinoforum->id, $user->id);
    if (!empty($grades->items[0]->grades)) {
        $grade = reset($grades->items[0]->grades);
        echo $OUTPUT->container(get_string('grade') . ': ' . $grade->str_long_grade);
        if ($grade->str_feedback) {
            echo $OUTPUT->container(get_string('feedback') . ': ' . $grade->str_feedback);
        }
    }

    if ($posts = jinoforum_get_user_posts($jinoforum->id, $user->id)) {

        if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id)) {
            print_error('invalidcoursemodule');
        }
        $discussions = jinoforum_get_user_involved_discussions($jinoforum->id, $user->id);

        foreach ($posts as $post) {
            if (!isset($discussions[$post->discussion])) {
                continue;
            }
            $discussion = $discussions[$post->discussion];

            jinoforum_print_post($post, $discussion, $jinoforum, $cm, $course, false, false, false);
        }
    } else {
        echo "<p>" . get_string("noposts", "jinoforum") . "</p>";
    }
}

/**
 * Filters the jinoforum discussions according to groups membership and config.
 *
 * @since  Moodle 2.8, 2.7.1, 2.6.4
 * @param  array $discussions Discussions with new posts array
 * @return array Jinoforums with the number of new posts
 */
function jinoforum_filter_user_groups_discussions($discussions) {

    // Group the remaining discussions posts by their jinoforumid.
    $filteredjinoforums = array();

    // Discard not visible groups.
    foreach ($discussions as $discussion) {

        // Course data is already cached.
        $instances = get_fast_modinfo($discussion->course)->get_instances();
        $jinoforum = $instances['jinoforum'][$discussion->jinoforum];

        // Continue if the user should not see this discussion.
        if (!jinoforum_is_user_group_discussion($jinoforum, $discussion->groupid)) {
            continue;
        }

        // Grouping results by jinoforum.
        if (empty($filteredjinoforums[$jinoforum->instance])) {
            $filteredjinoforums[$jinoforum->instance] = new stdClass();
            $filteredjinoforums[$jinoforum->instance]->id = $jinoforum->id;
            $filteredjinoforums[$jinoforum->instance]->count = 0;
        }
        $filteredjinoforums[$jinoforum->instance]->count += $discussion->count;
    }

    return $filteredjinoforums;
}

/**
 * Returns whether the discussion group is visible by the current user or not.
 *
 * @since Moodle 2.8, 2.7.1, 2.6.4
 * @param cm_info $cm The discussion course module
 * @param int $discussiongroupid The discussion groupid
 * @return bool
 */
function jinoforum_is_user_group_discussion(cm_info $cm, $discussiongroupid) {

    if ($discussiongroupid == -1 || $cm->effectivegroupmode != SEPARATEGROUPS) {
        return true;
    }

    if (isguestuser()) {
        return false;
    }

    if (has_capability('moodle/site:accessallgroups', context_module::instance($cm->id)) ||
            in_array($discussiongroupid, $cm->get_modinfo()->get_groups($cm->groupingid))) {
        return true;
    }

    return false;
}

/**
 * @global object
 * @global object
 * @global object
 * @param array $courses
 * @param array $htmlarray
 */
function jinoforum_print_overview($courses, &$htmlarray) {
    global $USER, $CFG, $DB, $SESSION;

    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return array();
    }

    if (!$jinoforums = get_all_instances_in_courses('jinoforum', $courses)) {
        return;
    }

    // Courses to search for new posts
    $coursessqls = array();
    $params = array();
    foreach ($courses as $course) {

        // If the user has never entered into the course all posts are pending
        if ($course->lastaccess == 0) {
            $coursessqls[] = '(f.course = ?)';
            $params[] = $course->id;

            // Only posts created after the course last access
        } else {
            $coursessqls[] = '(f.course = ? AND p.created > ?)';
            $params[] = $course->id;
            $params[] = $course->lastaccess;
        }
    }
    $params[] = $USER->id;
    $coursessql = implode(' OR ', $coursessqls);

    $sql = "SELECT d.id, d.jinoforum, f.course, d.groupid, COUNT(*) as count "
            . 'FROM {jinoforum} f '
            . 'JOIN {jinoforum_discussions} d ON d.jinoforum = f.id '
            . 'JOIN {jinoforum_posts} p ON p.discussion = d.id '
            . "WHERE ($coursessql) "
            . 'AND p.userid != ? '
            . 'GROUP BY d.id, d.jinoforum, f.course, d.groupid';

    // Avoid warnings.
    if (!$discussions = $DB->get_records_sql($sql, $params)) {
        $discussions = array();
    }

    $jinoforumsnewposts = jinoforum_filter_user_groups_discussions($discussions);

    // also get all jinoforum tracking stuff ONCE.
    $trackingjinoforums = array();
    foreach ($jinoforums as $jinoforum) {
//        if (jinoforum_tp_can_track_jinoforums($jinoforum)) {
        $trackingjinoforums[$jinoforum->id] = $jinoforum;
//        }
    }

    if (count($trackingjinoforums) > 0) {
        $cutoffdate = isset($CFG->jinoforum_oldpostdays) ? (time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60)) : 0;
        $sql = 'SELECT d.jinoforum,d.course,COUNT(p.id) AS count ' .
                ' FROM {jinoforum_posts} p ' .
                ' JOIN {jinoforum_discussions} d ON p.discussion = d.id ' .
                ' LEFT JOIN {jinoforum_read} r ON r.postid = p.id AND r.userid = ? WHERE (';
        $params = array($USER->id);

        foreach ($trackingjinoforums as $track) {
            $sql .= '(d.jinoforum = ? AND (d.groupid = -1 OR d.groupid = 0 OR d.groupid = ?)) OR ';
            $params[] = $track->id;
            if (isset($SESSION->currentgroup[$track->course])) {
                $groupid = $SESSION->currentgroup[$track->course];
            } else {
                // get first groupid
                $groupids = groups_get_all_groups($track->course, $USER->id);
                if ($groupids) {
                    reset($groupids);
                    $groupid = key($groupids);
                    $SESSION->currentgroup[$track->course] = $groupid;
                } else {
                    $groupid = 0;
                }
                unset($groupids);
            }
            $params[] = $groupid;
        }
        $sql = substr($sql, 0, -3); // take off the last OR
        $sql .= ') AND p.modified >= ? AND r.id is NULL GROUP BY d.jinoforum,d.course';
        $params[] = $cutoffdate;

        if (!$unread = $DB->get_records_sql($sql, $params)) {
            $unread = array();
        }
    } else {
        $unread = array();
    }

    if (empty($unread) and empty($jinoforumsnewposts)) {
        return;
    }

    $strjinoforum = get_string('modulename', 'jinoforum');

    foreach ($jinoforums as $jinoforum) {
        $str = '';
        $count = 0;
        $thisunread = 0;
        $showunread = false;
        // either we have something from logs, or trackposts, or nothing.
        if (array_key_exists($jinoforum->id, $jinoforumsnewposts) && !empty($jinoforumsnewposts[$jinoforum->id])) {
            $count = $jinoforumsnewposts[$jinoforum->id]->count;
        }
        if (array_key_exists($jinoforum->id, $unread)) {
            $thisunread = $unread[$jinoforum->id]->count;
            $showunread = true;
        }
        if ($count > 0 || $thisunread > 0) {
            $str .= '<div class="overview jinoforum"><div class="name">' . $strjinoforum . ': <a title="' . $strjinoforum . '" href="' . $CFG->wwwroot . '/mod/jinoforum/view.php?f=' . $jinoforum->id . '">' .
                    $jinoforum->name . '</a></div>';
            $str .= '<div class="info"><span class="postsincelogin">';
            $str .= get_string('overviewnumpostssince', 'jinoforum', $count) . "</span>";
            if (!empty($showunread)) {
                $str .= '<div class="unreadposts">' . get_string('overviewnumunread', 'jinoforum', $thisunread) . '</div>';
            }
            $str .= '</div></div>';
        }
        if (!empty($str)) {
            if (!array_key_exists($jinoforum->course, $htmlarray)) {
                $htmlarray[$jinoforum->course] = array();
            }
            if (!array_key_exists('jinoforum', $htmlarray[$jinoforum->course])) {
                $htmlarray[$jinoforum->course]['jinoforum'] = ''; // initialize, avoid warnings
            }
            $htmlarray[$jinoforum->course]['jinoforum'] .= $str;
        }
    }
}

/**
 * Given a course and a date, prints a summary of all the new
 * messages posted in the course since that date
 *
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $course
 * @param bool $viewfullnames capability
 * @param int $timestart
 * @return bool success
 */
function jinoforum_print_recent_activity($course, $viewfullnames, $timestart) {
    global $CFG, $USER, $DB, $OUTPUT;

    // do not use log table if possible, it may be huge and is expensive to join with other tables

    $allnamefields = user_picture::fields('u', null, 'duserid');
    if (!$posts = $DB->get_records_sql("SELECT p.*, f.type AS jinoforumtype, d.jinoforum, d.groupid,
                                              d.timestart, d.timeend, $allnamefields
                                         FROM {jinoforum_posts} p
                                              JOIN {jinoforum_discussions} d ON d.id = p.discussion
                                              JOIN {jinoforum} f             ON f.id = d.jinoforum
                                              JOIN {user} u              ON u.id = p.userid
                                        WHERE p.created > ? AND f.course = ?
                                     ORDER BY p.id ASC", array($timestart, $course->id))) { // order by initial posting date
        return false;
    }

    $modinfo = get_fast_modinfo($course);

    $groupmodes = array();
    $cms = array();

    $strftimerecent = get_string('strftimerecent');

    $printposts = array();
    foreach ($posts as $post) {
        if (!isset($modinfo->instances['jinoforum'][$post->jinoforum])) {
            // not visible
            continue;
        }
        $cm = $modinfo->instances['jinoforum'][$post->jinoforum];
        if (!$cm->uservisible) {
            continue;
        }
        $context = context_module::instance($cm->id);

        if (!has_capability('mod/jinoforum:viewdiscussion', $context)) {
            continue;
        }

        if (!empty($CFG->jinoforum_enabletimedposts) and $USER->id != $post->duserid
                and ( ($post->timestart > 0 and $post->timestart > time()) or ( $post->timeend > 0 and $post->timeend < time()))) {
            if (!has_capability('mod/jinoforum:viewhiddentimedposts', $context)) {
                continue;
            }
        }

        // Check that the user can see the discussion.
        if (jinoforum_is_user_group_discussion($cm, $post->groupid)) {
            $printposts[] = $post;
        }
    }
    unset($posts);

    if (!$printposts) {
        return false;
    }

    echo $OUTPUT->heading(get_string('newjinoforumposts', 'jinoforum') . ':', 3);
    echo "\n<ul class='unlist'>\n";

    foreach ($printposts as $post) {
        $subjectclass = empty($post->parent) ? ' bold' : '';

        echo '<li><div class="head">' .
        '<div class="date">' . userdate($post->modified, $strftimerecent) . '</div>' .
        '<div class="name">' . fullname($post, $viewfullnames) . '</div>' .
        '</div>';
        echo '<div class="info' . $subjectclass . '">';
        if (empty($post->parent)) {
            echo '"<a href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '">';
        } else {
            echo '"<a href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '&amp;parent=' . $post->parent . '#p' . $post->id . '">';
        }
        $post->subject = break_up_long_words(format_string($post->subject, true));
        echo $post->subject;
        echo "</a>\"</div></li>\n";
    }

    echo "</ul>\n";

    return true;
}

/**
 * Return grade for given user or all users.
 *
 * @global object
 * @global object
 * @param object $jinoforum
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function jinoforum_get_user_grades($jinoforum, $userid = 0) {
    global $CFG;

    require_once($CFG->dirroot . '/rating/lib.php');

    $ratingoptions = new stdClass;
    $ratingoptions->component = 'mod_jinoforum';
    $ratingoptions->ratingarea = 'post';

    //need these to work backwards to get a context id. Is there a better way to get contextid from a module instance?
    $ratingoptions->modulename = 'jinoforum';
    $ratingoptions->moduleid = $jinoforum->id;
    $ratingoptions->userid = $userid;
    $ratingoptions->aggregationmethod = $jinoforum->assessed;
    $ratingoptions->scaleid = $jinoforum->scale;
    $ratingoptions->itemtable = 'jinoforum_posts';
    $ratingoptions->itemtableusercolumn = 'userid';

    $rm = new rating_manager();
    return $rm->get_user_grades($ratingoptions);
}

/**
 * Update activity grades
 *
 * @category grade
 * @param object $jinoforum
 * @param int $userid specific user only, 0 means all
 * @param boolean $nullifnone return null if grade does not exist
 * @return void
 */
function jinoforum_update_grades($jinoforum, $userid = 0, $nullifnone = true) {
    global $CFG, $DB;
    require_once($CFG->libdir . '/gradelib.php');

    if (!$jinoforum->assessed) {
        jinoforum_grade_item_update($jinoforum);
    } else if ($grades = jinoforum_get_user_grades($jinoforum, $userid)) {
        jinoforum_grade_item_update($jinoforum, $grades);
    } else if ($userid and $nullifnone) {
        $grade = new stdClass();
        $grade->userid = $userid;
        $grade->rawgrade = NULL;
        jinoforum_grade_item_update($jinoforum, $grade);
    } else {
        jinoforum_grade_item_update($jinoforum);
    }
}

/**
 * Update all grades in gradebook.
 * @global object
 */
function jinoforum_upgrade_grades() {
    global $DB;

    $sql = "SELECT COUNT('x')
              FROM {jinoforum} f, {course_modules} cm, {modules} m
             WHERE m.name='jinoforum' AND m.id=cm.module AND cm.instance=f.id";
    $count = $DB->count_records_sql($sql);

    $sql = "SELECT f.*, cm.idnumber AS cmidnumber, f.course AS courseid
              FROM {jinoforum} f, {course_modules} cm, {modules} m
             WHERE m.name='jinoforum' AND m.id=cm.module AND cm.instance=f.id";
    $rs = $DB->get_recordset_sql($sql);
    if ($rs->valid()) {
        $pbar = new progress_bar('jinoforumupgradegrades', 500, true);
        $i = 0;
        foreach ($rs as $jinoforum) {
            $i++;
            upgrade_set_timeout(60 * 5); // set up timeout, may also abort execution
            jinoforum_update_grades($jinoforum, 0, false);
            $pbar->update($i, $count, "Updating Jinoforum grades ($i/$count).");
        }
    }
    $rs->close();
}

/**
 * Create/update grade item for given jinoforum
 *
 * @category grade
 * @uses GRADE_TYPE_NONE
 * @uses GRADE_TYPE_VALUE
 * @uses GRADE_TYPE_SCALE
 * @param stdClass $jinoforum Jinoforum object with extra cmidnumber
 * @param mixed $grades Optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok
 */
function jinoforum_grade_item_update($jinoforum, $grades = NULL) {
    global $CFG;
    if (!function_exists('grade_update')) { //workaround for buggy PHP versions
        require_once($CFG->libdir . '/gradelib.php');
    }

    $params = array('itemname' => $jinoforum->name, 'idnumber' => $jinoforum->cmidnumber);

    if (!$jinoforum->assessed or $jinoforum->scale == 0) {
        $params['gradetype'] = GRADE_TYPE_NONE;
    } else if ($jinoforum->scale > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax'] = $jinoforum->scale;
        $params['grademin'] = 0;
    } else if ($jinoforum->scale < 0) {
        $params['gradetype'] = GRADE_TYPE_SCALE;
        $params['scaleid'] = -$jinoforum->scale;
    }

    if ($grades === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }

    return grade_update('mod/jinoforum', $jinoforum->course, 'mod', 'jinoforum', $jinoforum->id, 0, $grades, $params);
}

/**
 * Delete grade item for given jinoforum
 *
 * @category grade
 * @param stdClass $jinoforum Jinoforum object
 * @return grade_item
 */
function jinoforum_grade_item_delete($jinoforum) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    return grade_update('mod/jinoforum', $jinoforum->course, 'mod', 'jinoforum', $jinoforum->id, 0, NULL, array('deleted' => 1));
}

/**
 * This function returns if a scale is being used by one jinoforum
 *
 * @global object
 * @param int $jinoforumid
 * @param int $scaleid negative number
 * @return bool
 */
function jinoforum_scale_used($jinoforumid, $scaleid) {
    global $DB;
    $return = false;

    $rec = $DB->get_record("jinoforum", array("id" => "$jinoforumid", "scale" => "-$scaleid"));

    if (!empty($rec) && !empty($scaleid)) {
        $return = true;
    }

    return $return;
}

/**
 * Checks if scale is being used by any instance of jinoforum
 *
 * This is used to find out if scale used anywhere
 *
 * @global object
 * @param $scaleid int
 * @return boolean True if the scale is used by any jinoforum
 */
function jinoforum_scale_used_anywhere($scaleid) {
    global $DB;
    if ($scaleid and $DB->record_exists('jinoforum', array('scale' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

// SQL FUNCTIONS ///////////////////////////////////////////////////////////

/**
 * Gets a post with all info ready for jinoforum_print_post
 * Most of these joins are just to get the jinoforum id
 *
 * @global object
 * @global object
 * @param int $postid
 * @return mixed array of posts or false
 */
function jinoforum_get_post_full($postid) {
    global $CFG, $DB;

    $allnames = get_all_user_name_fields(true, 'u');
    return $DB->get_record_sql("SELECT p.*, d.jinoforum, $allnames, u.email, u.picture, u.imagealt
                             FROM {jinoforum_posts} p
                                  JOIN {jinoforum_discussions} d ON p.discussion = d.id
                                  LEFT JOIN {user} u ON p.userid = u.id
                            WHERE p.id = ?", array($postid));
}

/**
 * Gets posts with all info ready for jinoforum_print_post
 * We pass jinoforumid in because we always know it so no need to make a
 * complicated join to find it out.
 *
 * @global object
 * @global object
 * @return mixed array of posts or false
 */
function jinoforum_get_discussion_posts($discussion, $sort, $jinoforumid) {
    global $CFG, $DB;

    $allnames = get_all_user_name_fields(true, 'u');
    return $DB->get_records_sql("SELECT p.*, $jinoforumid AS jinoforum, $allnames, u.email, u.picture, u.imagealt
                              FROM {jinoforum_posts} p
                         LEFT JOIN {user} u ON p.userid = u.id
                             WHERE p.discussion = ?
                               AND p.parent > 0 $sort", array($discussion));
}

/**
 * Gets all posts in discussion including top parent.
 *
 * @global object
 * @global object
 * @global object
 * @param int $discussionid
 * @param string $sort
 * @param bool $tracking does user track the jinoforum?
 * @return array of posts
 */
function jinoforum_get_all_discussion_posts($discussionid, $sort, $tracking = false, $searchtype = 1, $searchval = "") {
    global $CFG, $DB, $USER;

    $tr_sel = "";
    $tr_join = "";
    $params = array();

    if ($tracking) {
        $now = time();
        $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 3600);
        $tr_sel = ", fr.id AS postread";
        $tr_join = "LEFT JOIN {jinoforum_read} fr ON (fr.postid = p.id AND fr.userid = :userid)";
        $params['userid'] = $USER->id;
    }
    $join = "";
    if ($searchtype == 1) {
        $search = 'lower(p.subject) like lower(:searchval)';
    } else if ($searchtype == 2) {
        $join = "join {user} us on us.id = p.userid ";
        $search = "(lower(us.firstname) like lower(:searchval) or lower(us.lastname) like lower(:searchval2)  or concat(lower(us.firstname),lower(us.lastname)) like lower(:searchval3))";
    } else if ($searchtype == 3) {
        $search = 'lower(p.message) like lower(:searchval)';
    }
    $params['searchval'] = '%' . $searchval . '%';
    $params['searchval2'] = '%' . $searchval . '%';
    $params['searchval3'] = '%' . $searchval . '%';

    $allnames = get_all_user_name_fields(true, 'u');
    $params['discussionid'] = $discussionid;
    $sql = "SELECT p.*, $allnames, u.email, u.picture, u.imagealt $tr_sel 
                                     FROM {jinoforum_posts} p 
                                          $join  
                                          LEFT JOIN {user} u ON p.userid = u.id 
                                          $tr_join 
                                    WHERE ((p.lev = 0) or ($search)) and p.discussion = :discussionid 
                                 ORDER BY $sort"; 
    if (!$posts = $DB->get_records_sql($sql, $params)) {
        return array();
    }
    foreach ($posts as $pid => $p) {
        if ($tracking) {
            if (jinoforum_tp_is_post_old($p)) {
                $posts[$pid]->postread = true;
            }
        }
        if (!$p->parent) {
            continue;
        }
        if (!isset($posts[$p->parent])) {
            continue; // parent does not exist??
        }
        if (!isset($posts[$p->parent]->children)) {
            $posts[$p->parent]->children = array();
        }
        $posts[$p->parent]->children[$pid] = & $posts[$pid];
    }

    return $posts;
}

/**
 * Gets posts with all info ready for jinoforum_print_post
 * We pass jinoforumid in because we always know it so no need to make a
 * complicated join to find it out.
 *
 * @global object
 * @global object
 * @param int $parent
 * @param int $jinoforumid
 * @return array
 */
function jinoforum_get_child_posts($parent, $jinoforumid) {
    global $CFG, $DB;

    $allnames = get_all_user_name_fields(true, 'u');
    return $DB->get_records_sql("SELECT p.*, $jinoforumid AS jinoforum, $allnames, u.email, u.picture, u.imagealt
                              FROM {jinoforum_posts} p
                         LEFT JOIN {user} u ON p.userid = u.id
                             WHERE p.parent = ?
                          ORDER BY p.created ASC", array($parent));
}

/**
 * An array of jinoforum objects that the user is allowed to read/search through.
 *
 * @global object
 * @global object
 * @global object
 * @param int $userid
 * @param int $courseid if 0, we look for jinoforums throughout the whole site.
 * @return array of jinoforum objects, or false if no matches
 *         Jinoforum objects have the following attributes:
 *         id, type, course, cmid, cmvisible, cmgroupmode, accessallgroups,
 *         viewhiddentimedposts
 */
function jinoforum_get_readable_jinoforums($userid, $courseid = 0) {

    global $CFG, $DB, $USER;
    require_once($CFG->dirroot . '/course/lib.php');

    if (!$jinoforummod = $DB->get_record('modules', array('name' => 'jinoforum'))) {
        print_error('notinstalled', 'jinoforum');
    }

    if ($courseid) {
        $courses = $DB->get_records('course', array('id' => $courseid));
    } else {
        // If no course is specified, then the user can see SITE + his courses.
        $courses1 = $DB->get_records('course', array('id' => SITEID));
        $courses2 = enrol_get_users_courses($userid, true, array('modinfo'));
        $courses = array_merge($courses1, $courses2);
    }
    if (!$courses) {
        return array();
    }

    $readablejinoforums = array();

    foreach ($courses as $course) {

        $modinfo = get_fast_modinfo($course);

        if (empty($modinfo->instances['jinoforum'])) {
            // hmm, no jinoforums?
            continue;
        }

        $coursejinoforums = $DB->get_records('jinoforum', array('course' => $course->id));

        foreach ($modinfo->instances['jinoforum'] as $jinoforumid => $cm) {
            if (!$cm->uservisible or ! isset($coursejinoforums[$jinoforumid])) {
                continue;
            }
            $context = context_module::instance($cm->id);
            $jinoforum = $coursejinoforums[$jinoforumid];
            $jinoforum->context = $context;
            $jinoforum->cm = $cm;

            if (!has_capability('mod/jinoforum:viewdiscussion', $context)) {
                continue;
            }

            /// group access
            if (groups_get_activity_groupmode($cm, $course) == SEPARATEGROUPS and ! has_capability('moodle/site:accessallgroups', $context)) {

                $jinoforum->onlygroups = $modinfo->get_groups($cm->groupingid);
                $jinoforum->onlygroups[] = -1;
            }

            /// hidden timed discussions
            $jinoforum->viewhiddentimedposts = true;
            if (!empty($CFG->jinoforum_enabletimedposts)) {
                if (!has_capability('mod/jinoforum:viewhiddentimedposts', $context)) {
                    $jinoforum->viewhiddentimedposts = false;
                }
            }

            /// qanda access
            if ($jinoforum->type == 'qanda' && !has_capability('mod/jinoforum:viewqandawithoutposting', $context)) {

                // We need to check whether the user has posted in the qanda jinoforum.
                $jinoforum->onlydiscussions = array();  // Holds discussion ids for the discussions
                // the user is allowed to see in this jinoforum.
                if ($discussionspostedin = jinoforum_discussions_user_has_posted_in($jinoforum->id, $USER->id)) {
                    foreach ($discussionspostedin as $d) {
                        $jinoforum->onlydiscussions[] = $d->id;
                    }
                }
            }

            $readablejinoforums[$jinoforum->id] = $jinoforum;
        }

        unset($modinfo);
    } // End foreach $courses

    return $readablejinoforums;
}

/**
 * Returns a list of posts found using an array of search terms.
 *
 * @global object
 * @global object
 * @global object
 * @param array $searchterms array of search terms, e.g. word +word -word
 * @param int $courseid if 0, we search through the whole site
 * @param int $limitfrom
 * @param int $limitnum
 * @param int &$totalcount
 * @param string $extrasql
 * @return array|bool Array of posts found or false
 */
function jinoforum_search_posts($searchterms, $courseid = 0, $limitfrom = 0, $limitnum = 50, &$totalcount, $extrasql = '') {
    global $CFG, $DB, $USER;
    require_once($CFG->libdir . '/searchlib.php');

    $jinoforums = jinoforum_get_readable_jinoforums($USER->id, $courseid);

    if (count($jinoforums) == 0) {
        $totalcount = 0;
        return false;
    }

    $now = round(time(), -2); // db friendly

    $fullaccess = array();
    $where = array();
    $params = array();

    foreach ($jinoforums as $jinoforumid => $jinoforum) {
        $select = array();

        if (!$jinoforum->viewhiddentimedposts) {
            $select[] = "(d.userid = :userid{$jinoforumid} OR (d.timestart < :timestart{$jinoforumid} AND (d.timeend = 0 OR d.timeend > :timeend{$jinoforumid})))";
            $params = array_merge($params, array('userid' . $jinoforumid => $USER->id, 'timestart' . $jinoforumid => $now, 'timeend' . $jinoforumid => $now));
        }

        $cm = $jinoforum->cm;
        $context = $jinoforum->context;

        if ($jinoforum->type == 'qanda' && !has_capability('mod/jinoforum:viewqandawithoutposting', $context)) {
            if (!empty($jinoforum->onlydiscussions)) {
                list($discussionid_sql, $discussionid_params) = $DB->get_in_or_equal($jinoforum->onlydiscussions, SQL_PARAMS_NAMED, 'qanda' . $jinoforumid . '_');
                $params = array_merge($params, $discussionid_params);
                $select[] = "(d.id $discussionid_sql OR p.parent = 0)";
            } else {
                $select[] = "p.parent = 0";
            }
        }

        if (!empty($jinoforum->onlygroups)) {
            list($groupid_sql, $groupid_params) = $DB->get_in_or_equal($jinoforum->onlygroups, SQL_PARAMS_NAMED, 'grps' . $jinoforumid . '_');
            $params = array_merge($params, $groupid_params);
            $select[] = "d.groupid $groupid_sql";
        }

        if ($select) {
            $selects = implode(" AND ", $select);
            $where[] = "(d.jinoforum = :jinoforum{$jinoforumid} AND $selects)";
            $params['jinoforum' . $jinoforumid] = $jinoforumid;
        } else {
            $fullaccess[] = $jinoforumid;
        }
    }

    if ($fullaccess) {
        list($fullid_sql, $fullid_params) = $DB->get_in_or_equal($fullaccess, SQL_PARAMS_NAMED, 'fula');
        $params = array_merge($params, $fullid_params);
        $where[] = "(d.jinoforum $fullid_sql)";
    }

    $selectdiscussion = "(" . implode(" OR ", $where) . ")";

    $messagesearch = '';
    $searchstring = '';

    // Need to concat these back together for parser to work.
    foreach ($searchterms as $searchterm) {
        if ($searchstring != '') {
            $searchstring .= ' ';
        }
        $searchstring .= $searchterm;
    }

    // We need to allow quoted strings for the search. The quotes *should* be stripped
    // by the parser, but this should be examined carefully for security implications.
    $searchstring = str_replace("\\\"", "\"", $searchstring);
    $parser = new search_parser();
    $lexer = new search_lexer($parser);

    if ($lexer->parse($searchstring)) {
        $parsearray = $parser->get_parsed_array();
        // Experimental feature under 1.8! MDL-8830
        // Use alternative text searches if defined
        // This feature only works under mysql until properly implemented for other DBs
        // Requires manual creation of text index for jinoforum_posts before enabling it:
        // CREATE FULLTEXT INDEX foru_post_tix ON [prefix]jinoforum_posts (subject, message)
        // Experimental feature under 1.8! MDL-8830
        if (!empty($CFG->jinoforum_usetextsearches)) {
            list($messagesearch, $msparams) = search_generate_text_SQL($parsearray, 'p.message', 'p.subject', 'p.userid', 'u.id', 'u.firstname', 'u.lastname', 'p.modified', 'd.jinoforum');
        } else {
            list($messagesearch, $msparams) = search_generate_SQL($parsearray, 'p.message', 'p.subject', 'p.userid', 'u.id', 'u.firstname', 'u.lastname', 'p.modified', 'd.jinoforum');
        }
        $params = array_merge($params, $msparams);
    }

    $fromsql = "{jinoforum_posts} p,
                  {jinoforum_discussions} d,
                  {user} u";

    $selectsql = " $messagesearch
               AND p.discussion = d.id
               AND p.userid = u.id
               AND $selectdiscussion
                   $extrasql";

    $countsql = "SELECT COUNT(*)
                   FROM $fromsql
                  WHERE $selectsql";

    $allnames = get_all_user_name_fields(true, 'u');
    $searchsql = "SELECT p.*,
                         d.jinoforum,
                         $allnames,
                         u.email,
                         u.picture,
                         u.imagealt
                    FROM $fromsql
                   WHERE $selectsql
                ORDER BY p.modified DESC";

    $totalcount = $DB->count_records_sql($countsql, $params);

    return $DB->get_records_sql($searchsql, $params, $limitfrom, $limitnum);
}

/**
 * Returns a list of ratings for a particular post - sorted.
 *
 * TODO: Check if this function is actually used anywhere.
 * Up until the fix for MDL-27471 this function wasn't even returning.
 *
 * @param stdClass $context
 * @param int $postid
 * @param string $sort
 * @return array Array of ratings or false
 */
function jinoforum_get_ratings($context, $postid, $sort = "u.firstname ASC") {
    $options = new stdClass;
    $options->context = $context;
    $options->component = 'mod_jinoforum';
    $options->ratingarea = 'post';
    $options->itemid = $postid;
    $options->sort = "ORDER BY $sort";

    $rm = new rating_manager();
    return $rm->get_all_ratings_for_item($options);
}

/**
 * Returns a list of all new posts that have not been mailed yet
 *
 * @param int $starttime posts created after this time
 * @param int $endtime posts created before this
 * @param int $now used for timed discussions only
 * @return array
 */
function jinoforum_get_unmailed_posts($starttime, $endtime, $now = null) {
    global $CFG, $DB;

    $params = array();
    $params['mailed'] = JINOFORUM_MAILED_PENDING;
    $params['ptimestart'] = $starttime;
    $params['ptimeend'] = $endtime;
    $params['mailnow'] = 1;

    if (!empty($CFG->jinoforum_enabletimedposts)) {
        if (empty($now)) {
            $now = time();
        }
        $timedsql = "AND (d.timestart < :dtimestart AND (d.timeend = 0 OR d.timeend > :dtimeend))";
        $params['dtimestart'] = $now;
        $params['dtimeend'] = $now;
    } else {
        $timedsql = "";
    }

    return $DB->get_records_sql("SELECT p.*, d.course, d.jinoforum
                                 FROM {jinoforum_posts} p
                                 JOIN {jinoforum_discussions} d ON d.id = p.discussion
                                 WHERE p.mailed = :mailed
                                 AND p.created >= :ptimestart
                                 AND (p.created < :ptimeend OR p.mailnow = :mailnow)
                                 $timedsql
                                 ORDER BY p.modified ASC", $params);
}

/**
 * Marks posts before a certain time as being mailed already
 *
 * @global object
 * @global object
 * @param int $endtime
 * @param int $now Defaults to time()
 * @return bool
 */
function jinoforum_mark_old_posts_as_mailed($endtime, $now = null) {
    global $CFG, $DB;

    if (empty($now)) {
        $now = time();
    }

    $params = array();
    $params['mailedsuccess'] = JINOFORUM_MAILED_SUCCESS;
    $params['now'] = $now;
    $params['endtime'] = $endtime;
    $params['mailnow'] = 1;
    $params['mailedpending'] = JINOFORUM_MAILED_PENDING;

    if (empty($CFG->jinoforum_enabletimedposts)) {
        return $DB->execute("UPDATE {jinoforum_posts}
                             SET mailed = :mailedsuccess
                             WHERE (created < :endtime OR mailnow = :mailnow)
                             AND mailed = :mailedpending", $params);
    } else {
        return $DB->execute("UPDATE {jinoforum_posts}
                             SET mailed = :mailedsuccess
                             WHERE discussion NOT IN (SELECT d.id
                                                      FROM {jinoforum_discussions} d
                                                      WHERE d.timestart > :now)
                             AND (created < :endtime OR mailnow = :mailnow)
                             AND mailed = :mailedpending", $params);
    }
}

/**
 * Get all the posts for a user in a jinoforum suitable for jinoforum_print_post
 *
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @return array
 */
function jinoforum_get_user_posts($jinoforumid, $userid) {
    global $CFG, $DB;

    $timedsql = "";
    $params = array($jinoforumid, $userid);

    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $cm = get_coursemodule_from_instance('jinoforum', $jinoforumid);
        if (!has_capability('mod/jinoforum:viewhiddentimedposts', context_module::instance($cm->id))) {
            $now = time();
            $timedsql = "AND (d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?))";
            $params[] = $now;
            $params[] = $now;
        }
    }

    $allnames = get_all_user_name_fields(true, 'u');
    return $DB->get_records_sql("SELECT p.*, d.jinoforum, $allnames, u.email, u.picture, u.imagealt
                              FROM {jinoforum} f
                                   JOIN {jinoforum_discussions} d ON d.jinoforum = f.id
                                   JOIN {jinoforum_posts} p       ON p.discussion = d.id
                                   JOIN {user} u              ON u.id = p.userid
                             WHERE f.id = ?
                                   AND p.userid = ?
                                   $timedsql
                          ORDER BY p.modified ASC", $params);
}

/**
 * Get all the discussions user participated in
 *
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @param int $jinoforumid
 * @param int $userid
 * @return array Array or false
 */
function jinoforum_get_user_involved_discussions($jinoforumid, $userid) {
    global $CFG, $DB;

    $timedsql = "";
    $params = array($jinoforumid, $userid);
    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $cm = get_coursemodule_from_instance('jinoforum', $jinoforumid);
        if (!has_capability('mod/jinoforum:viewhiddentimedposts', context_module::instance($cm->id))) {
            $now = time();
            $timedsql = "AND (d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?))";
            $params[] = $now;
            $params[] = $now;
        }
    }

    return $DB->get_records_sql("SELECT DISTINCT d.*
                              FROM {jinoforum} f
                                   JOIN {jinoforum_discussions} d ON d.jinoforum = f.id
                                   JOIN {jinoforum_posts} p       ON p.discussion = d.id
                             WHERE f.id = ?
                                   AND p.userid = ?
                                   $timedsql", $params);
}

/**
 * Get all the posts for a user in a jinoforum suitable for jinoforum_print_post
 *
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int $userid
 * @return array of counts or false
 */
function jinoforum_count_user_posts($jinoforumid, $userid) {
    global $CFG, $DB;

    $timedsql = "";
    $params = array($jinoforumid, $userid);
    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $cm = get_coursemodule_from_instance('jinoforum', $jinoforumid);
        if (!has_capability('mod/jinoforum:viewhiddentimedposts', context_module::instance($cm->id))) {
            $now = time();
            $timedsql = "AND (d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?))";
            $params[] = $now;
            $params[] = $now;
        }
    }

    return $DB->get_record_sql("SELECT COUNT(p.id) AS postcount, MAX(p.modified) AS lastpost
                             FROM {jinoforum} f
                                  JOIN {jinoforum_discussions} d ON d.jinoforum = f.id
                                  JOIN {jinoforum_posts} p       ON p.discussion = d.id
                                  JOIN {user} u              ON u.id = p.userid
                            WHERE f.id = ?
                                  AND p.userid = ?
                                  $timedsql", $params);
}

/**
 * Given a log entry, return the jinoforum post details for it.
 *
 * @global object
 * @global object
 * @param object $log
 * @return array|null
 */
function jinoforum_get_post_from_log($log) {
    global $CFG, $DB;

    $allnames = get_all_user_name_fields(true, 'u');
    if ($log->action == "add post") {

        return $DB->get_record_sql("SELECT p.*, f.type AS jinoforumtype, d.jinoforum, d.groupid, $allnames, u.email, u.picture
                                 FROM {jinoforum_discussions} d,
                                      {jinoforum_posts} p,
                                      {jinoforum} f,
                                      {user} u
                                WHERE p.id = ?
                                  AND d.id = p.discussion
                                  AND p.userid = u.id
                                  AND u.deleted <> '1'
                                  AND f.id = d.jinoforum", array($log->info));
    } else if ($log->action == "add discussion") {

        return $DB->get_record_sql("SELECT p.*, f.type AS jinoforumtype, d.jinoforum, d.groupid, $allnames, u.email, u.picture
                                 FROM {jinoforum_discussions} d,
                                      {jinoforum_posts} p,
                                      {jinoforum} f,
                                      {user} u
                                WHERE d.id = ?
                                  AND d.firstpost = p.id
                                  AND p.userid = u.id
                                  AND u.deleted <> '1'
                                  AND f.id = d.jinoforum", array($log->info));
    }
    return NULL;
}

/**
 * Given a discussion id, return the first post from the discussion
 *
 * @global object
 * @global object
 * @param int $dicsussionid
 * @return array
 */
function jinoforum_get_firstpost_from_discussion($discussionid) {
    global $CFG, $DB;

    return $DB->get_record_sql("SELECT p.*
                             FROM {jinoforum_discussions} d,
                                  {jinoforum_posts} p
                            WHERE d.id = ?
                              AND d.firstpost = p.id ", array($discussionid));
}

/**
 * Returns an array of counts of replies to each discussion
 *
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param string $jinoforumsort
 * @param int $limit
 * @param int $page
 * @param int $perpage
 * @return array
 */
function jinoforum_count_discussion_replies($jinoforumid, $jinoforumsort = "", $limit = -1, $page = -1, $perpage = 0) {
    global $CFG, $DB;

    if ($limit > 0) {
        $limitfrom = 0;
        $limitnum = $limit;
    } else if ($page != -1) {
        $limitfrom = $page * $perpage;
        $limitnum = $perpage;
    } else {
        $limitfrom = 0;
        $limitnum = 0;
    }

    if ($jinoforumsort == "") {
        $orderby = "";
        $groupby = "";
    } else {
        $orderby = "ORDER BY $jinoforumsort";
        $groupby = ", " . strtolower($jinoforumsort);
        $groupby = str_replace('desc', '', $groupby);
        $groupby = str_replace('asc', '', $groupby);
    }

    if (($limitfrom == 0 and $limitnum == 0) or $jinoforumsort == "") {
        $sql = "SELECT p.discussion, COUNT(p.id) AS replies, MAX(p.id) AS lastpostid
                  FROM {jinoforum_posts} p
                       JOIN {jinoforum_discussions} d ON p.discussion = d.id
                 WHERE p.parent > 0 AND d.jinoforum = ?
              GROUP BY p.discussion";
        return $DB->get_records_sql($sql, array($jinoforumid));
    } else {
        $sql = "SELECT p.discussion, (COUNT(p.id) - 1) AS replies, MAX(p.id) AS lastpostid
                  FROM {jinoforum_posts} p
                       JOIN {jinoforum_discussions} d ON p.discussion = d.id
                 WHERE d.jinoforum = ?
              GROUP BY p.discussion $groupby
              $orderby";
        return $DB->get_records_sql("SELECT * FROM ($sql) sq", array($jinoforumid), $limitfrom, $limitnum);
    }
}

/**
 * @global object
 * @global object
 * @global object
 * @staticvar array $cache
 * @param object $jinoforum
 * @param object $cm
 * @param object $course
 * @return mixed
 */
function jinoforum_count_discussions($jinoforum, $cm, $course) {
    global $CFG, $DB, $USER;

    static $cache = array();

    $now = round(time(), -2); // db cache friendliness

    $params = array($course->id);

    if (!isset($cache[$course->id])) {
        if (!empty($CFG->jinoforum_enabletimedposts)) {
            $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
            $params[] = $now;
            $params[] = $now;
        } else {
            $timedsql = "";
        }

        $sql = "SELECT f.id, COUNT(d.id) as dcount
                  FROM {jinoforum} f
                       JOIN {jinoforum_discussions} d ON d.jinoforum = f.id
                 WHERE f.course = ?
                       $timedsql
              GROUP BY f.id";

        if ($counts = $DB->get_records_sql($sql, $params)) {
            foreach ($counts as $count) {
                $counts[$count->id] = $count->dcount;
            }
            $cache[$course->id] = $counts;
        } else {
            $cache[$course->id] = array();
        }
    }

    if (empty($cache[$course->id][$jinoforum->id])) {
        return 0;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);

    if ($groupmode != SEPARATEGROUPS) {
        return $cache[$course->id][$jinoforum->id];
    }

    if (has_capability('moodle/site:accessallgroups', context_module::instance($cm->id))) {
        return $cache[$course->id][$jinoforum->id];
    }

    require_once($CFG->dirroot . '/course/lib.php');

    $modinfo = get_fast_modinfo($course);

    $mygroups = $modinfo->get_groups($cm->groupingid);

    // add all groups posts
    $mygroups[-1] = -1;

    list($mygroups_sql, $params) = $DB->get_in_or_equal($mygroups);
    $params[] = $jinoforum->id;

    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $timedsql = "AND d.timestart < $now AND (d.timeend = 0 OR d.timeend > $now)";
        $params[] = $now;
        $params[] = $now;
    } else {
        $timedsql = "";
    }

    $sql = "SELECT COUNT(d.id)
              FROM {jinoforum_discussions} d
             WHERE d.groupid $mygroups_sql AND d.jinoforum = ?
                   $timedsql";

    return $DB->get_field_sql($sql, $params);
}

/**
 * How many posts by other users are unrated by a given user in the given discussion?
 *
 * TODO: Is this function still used anywhere?
 *
 * @param int $discussionid
 * @param int $userid
 * @return mixed
 */
function jinoforum_count_unrated_posts($discussionid, $userid) {
    global $CFG, $DB;

    $sql = "SELECT COUNT(*) as num
              FROM {jinoforum_posts}
             WHERE parent > 0
               AND discussion = :discussionid
               AND userid <> :userid";
    $params = array('discussionid' => $discussionid, 'userid' => $userid);
    $posts = $DB->get_record_sql($sql, $params);
    if ($posts) {
        $sql = "SELECT count(*) as num
                  FROM {jinoforum_posts} p,
                       {rating} r
                 WHERE p.discussion = :discussionid AND
                       p.id = r.itemid AND
                       r.userid = userid AND
                       r.component = 'mod_jinoforum' AND
                       r.ratingarea = 'post'";
        $rated = $DB->get_record_sql($sql, $params);
        if ($rated) {
            if ($posts->num > $rated->num) {
                return $posts->num - $rated->num;
            } else {
                return 0; // Just in case there was a counting error
            }
        } else {
            return $posts->num;
        }
    } else {
        return 0;
    }
}

/**
 * Get all discussions in a jinoforum
 *
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $cm
 * @param string $jinoforumsort
 * @param bool $fullpost
 * @param int $unused
 * @param int $limit
 * @param bool $userlastmodified
 * @param int $page
 * @param int $perpage
 * @return array
 */
function jinoforum_get_discussions($cm, $jinoforumsort = "d.timemodified DESC", $fullpost = true, $unused = -1, $limit = -1, $userlastmodified = false, $page = -1, $perpage = 0) {
    global $CFG, $DB, $USER;

    $timelimit = '';

    $now = round(time(), -2);
    $params = array($cm->instance);

    $modcontext = context_module::instance($cm->id);

    if (!has_capability('mod/jinoforum:viewdiscussion', $modcontext)) { /// User must have perms to view discussions
        return array();
    }

    if (!empty($CFG->jinoforum_enabletimedposts)) { /// Users must fulfill timed posts
        if (!has_capability('mod/jinoforum:viewhiddentimedposts', $modcontext)) {
            $timelimit = " AND ((d.timestart <= ? AND (d.timeend = 0 OR d.timeend > ?))";
            $params[] = $now;
            $params[] = $now;
            if (isloggedin()) {
                $timelimit .= " OR d.userid = ?";
                $params[] = $USER->id;
            }
            $timelimit .= ")";
        }
    }

    if ($limit > 0) {
        $limitfrom = 0;
        $limitnum = $limit;
    } else if ($page != -1) {
        $limitfrom = $page * $perpage;
        $limitnum = $perpage;
    } else {
        $limitfrom = 0;
        $limitnum = 0;
    }

    $groupmode = groups_get_activity_groupmode($cm);
    $currentgroup = groups_get_activity_group($cm);

    if ($groupmode) {
        if (empty($modcontext)) {
            $modcontext = context_module::instance($cm->id);
        }

        if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $modcontext)) {
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = ? OR d.groupid = -1)";
                $params[] = $currentgroup;
            } else {
                $groupselect = "";
            }
        } else {
            //seprate groups without access all
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = ? OR d.groupid = -1)";
                $params[] = $currentgroup;
            } else {
                $groupselect = "AND d.groupid = -1";
            }
        }
    } else {
        $groupselect = "";
    }


    if (empty($jinoforumsort)) {
        $jinoforumsort = "d.timemodified DESC";
    }
    if (empty($fullpost)) {
        $postdata = "p.id,d.id as did,p.subject,p.modified,p.discussion,p.userid";
    } else {
        $postdata = "p.*";
    }

    if (empty($userlastmodified)) {  // We don't need to know this
        $umfields = "";
        $umtable = "";
    } else {
        $umfields = ', ' . get_all_user_name_fields(true, 'um', null, 'um');
        $umtable = " LEFT JOIN {user} um ON (d.usermodified = um.id)";
    }
    $allnames = get_all_user_name_fields(true, 'u');
    $sql = "SELECT $postdata, d.name, d.timemodified, d.ispublic , d.usermodified, d.groupid, d.timestart, d.timeend, $allnames,
                   u.email, u.picture, u.imagealt $umfields
              FROM {jinoforum_discussions} d
                   JOIN {jinoforum_posts} p ON p.discussion = d.id
                   JOIN {user} u ON p.userid = u.id
                   $umtable
             WHERE d.jinoforum = ? AND p.parent = 0
                   $timelimit $groupselect
          ORDER BY $jinoforumsort";
    return $DB->get_records_sql($sql, $params, $limitfrom, $limitnum);
}

/**
 *
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $cm
 * @return array
 */
function jinoforum_get_discussions_unread($cm) {
    global $CFG, $DB, $USER;

    $now = round(time(), -2);
    $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);

    $params = array();
    $groupmode = groups_get_activity_groupmode($cm);
    $currentgroup = groups_get_activity_group($cm);

    if ($groupmode) {
        $modcontext = context_module::instance($cm->id);

        if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $modcontext)) {
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                $params['currentgroup'] = $currentgroup;
            } else {
                $groupselect = "";
            }
        } else {
            //separate groups without access all
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = :currentgroup OR d.groupid = -1)";
                $params['currentgroup'] = $currentgroup;
            } else {
                $groupselect = "AND d.groupid = -1";
            }
        }
    } else {
        $groupselect = "";
    }

    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $timedsql = "AND d.timestart < :now1 AND (d.timeend = 0 OR d.timeend > :now2)";
        $params['now1'] = $now;
        $params['now2'] = $now;
    } else {
        $timedsql = "";
    }

    $sql = "SELECT d.id, COUNT(p.id) AS unread
              FROM {jinoforum_discussions} d
                   JOIN {jinoforum_posts} p     ON p.discussion = d.id
                   LEFT JOIN {jinoforum_read} r ON (r.postid = p.id AND r.userid = $USER->id)
             WHERE d.jinoforum = {$cm->instance}
                   AND p.modified >= :cutoffdate AND r.id is NULL
                   $groupselect
                   $timedsql
          GROUP BY d.id";
    $params['cutoffdate'] = $cutoffdate;

    if ($unreads = $DB->get_records_sql($sql, $params)) {
        foreach ($unreads as $unread) {
            $unreads[$unread->id] = $unread->unread;
        }
        return $unreads;
    } else {
        return array();
    }
}

/**
 * @global object
 * @global object
 * @global object
 * @uses CONEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $cm
 * @return array
 */
function jinoforum_get_discussions_count($cm) {
    global $CFG, $DB, $USER;

    $now = round(time(), -2);
    $params = array($cm->instance);
    $groupmode = groups_get_activity_groupmode($cm);
    $currentgroup = groups_get_activity_group($cm);

    if ($groupmode) {
        $modcontext = context_module::instance($cm->id);

        if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $modcontext)) {
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = ? OR d.groupid = -1)";
                $params[] = $currentgroup;
            } else {
                $groupselect = "";
            }
        } else {
            //seprate groups without access all
            if ($currentgroup) {
                $groupselect = "AND (d.groupid = ? OR d.groupid = -1)";
                $params[] = $currentgroup;
            } else {
                $groupselect = "AND d.groupid = -1";
            }
        }
    } else {
        $groupselect = "";
    }

    $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);

    $timelimit = "";

    if (!empty($CFG->jinoforum_enabletimedposts)) {

        $modcontext = context_module::instance($cm->id);

        if (!has_capability('mod/jinoforum:viewhiddentimedposts', $modcontext)) {
            $timelimit = " AND ((d.timestart <= ? AND (d.timeend = 0 OR d.timeend > ?))";
            $params[] = $now;
            $params[] = $now;
            if (isloggedin()) {
                $timelimit .= " OR d.userid = ?";
                $params[] = $USER->id;
            }
            $timelimit .= ")";
        }
    }

    $sql = "SELECT COUNT(d.id)
              FROM {jinoforum_discussions} d
                   JOIN {jinoforum_posts} p ON p.discussion = d.id
             WHERE d.jinoforum = ? AND p.parent = 0
                   $groupselect $timelimit";

    return $DB->get_field_sql($sql, $params);
}

/**
 * Get all discussions started by a particular user in a course (or group)
 * This function no longer used ...
 *
 * @todo Remove this function if no longer used
 * @global object
 * @global object
 * @param int $courseid
 * @param int $userid
 * @param int $groupid
 * @return array
 */
function jinoforum_get_user_discussions($courseid, $userid, $groupid = 0) {
    global $CFG, $DB;
    $params = array($courseid, $userid);
    if ($groupid) {
        $groupselect = " AND d.groupid = ? ";
        $params[] = $groupid;
    } else {
        $groupselect = "";
    }

    $allnames = get_all_user_name_fields(true, 'u');
    return $DB->get_records_sql("SELECT p.*, d.groupid, $allnames, u.email, u.picture, u.imagealt,
                                   f.type as jinoforumtype, f.name as jinoforumname, f.id as jinoforumid
                              FROM {jinoforum_discussions} d,
                                   {jinoforum_posts} p,
                                   {user} u,
                                   {jinoforum} f
                             WHERE d.course = ?
                               AND p.discussion = d.id
                               AND p.parent = 0
                               AND p.userid = u.id
                               AND u.id = ?
                               AND d.jinoforum = f.id $groupselect
                          ORDER BY p.created DESC", $params);
}

/**
 * Get the list of potential subscribers to a jinoforum.
 *
 * @param object $jinoforumcontext the jinoforum context.
 * @param integer $groupid the id of a group, or 0 for all groups.
 * @param string $fields the list of fields to return for each user. As for get_users_by_capability.
 * @param string $sort sort order. As for get_users_by_capability.
 * @return array list of users.
 */
function jinoforum_get_potential_subscribers($jinoforumcontext, $groupid, $fields, $sort = '') {
    global $DB;

    // only active enrolled users or everybody on the frontpage
    list($esql, $params) = get_enrolled_sql($jinoforumcontext, 'mod/jinoforum:allowforcesubscribe', $groupid, true);
    if (!$sort) {
        list($sort, $sortparams) = users_order_by_sql('u');
        $params = array_merge($params, $sortparams);
    }

    $sql = "SELECT $fields
              FROM {user} u
              JOIN ($esql) je ON je.id = u.id
          ORDER BY $sort";

    return $DB->get_records_sql($sql, $params);
}

/**
 * Returns list of user objects that are subscribed to this jinoforum
 *
 * @global object
 * @global object
 * @param object $course the course
 * @param jinoforum $jinoforum the jinoforum
 * @param integer $groupid group id, or 0 for all.
 * @param object $context the jinoforum context, to save re-fetching it where possible.
 * @param string $fields requested user fields (with "u." table prefix)
 * @return array list of users.
 */
function jinoforum_subscribed_users($course, $jinoforum, $groupid = 0, $context = null, $fields = null) {
    global $CFG, $DB;

    $allnames = get_all_user_name_fields(true, 'u');
    if (empty($fields)) {
        $fields = "u.id,
                  u.username,
                  $allnames,
                  u.maildisplay,
                  u.mailformat,
                  u.maildigest,
                  u.imagealt,
                  u.email,
                  u.emailstop,
                  u.city,
                  u.country,
                  u.lastaccess,
                  u.lastlogin,
                  u.picture,
                  u.timezone,
                  u.theme,
                  u.lang,
                  u.trackforums,
                  u.mnethostid";
    }

    if (empty($context)) {
        $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id);
        $context = context_module::instance($cm->id);
    }

    if (jinoforum_is_forcesubscribed($jinoforum)) {
        $results = jinoforum_get_potential_subscribers($context, $groupid, $fields, "u.email ASC");
    } else {
        // only active enrolled users or everybody on the frontpage
        list($esql, $params) = get_enrolled_sql($context, '', $groupid, true);
        $params['jinoforumid'] = $jinoforum->id;
        $results = $DB->get_records_sql("SELECT $fields
                                           FROM {user} u
                                           JOIN ($esql) je ON je.id = u.id
                                           JOIN {jinoforum_subscriptions} s ON s.userid = u.id
                                          WHERE s.jinoforum = :jinoforumid
                                       ORDER BY u.email ASC", $params);
    }

    // Guest user should never be subscribed to a jinoforum.
    unset($results[$CFG->siteguest]);

    return $results;
}

// OTHER FUNCTIONS ///////////////////////////////////////////////////////////

/**
 * @global object
 * @global object
 * @param int $courseid
 * @param string $type
 */
function jinoforum_get_course_jinoforum($courseid, $type) {
// How to set up special 1-per-course jinoforums
    global $CFG, $DB, $OUTPUT, $USER;

    if ($jinoforums = $DB->get_records_select("jinoforum", "course = ? AND type = ?", array($courseid, $type), "id ASC")) {
        // There should always only be ONE, but with the right combination of
        // errors there might be more.  In this case, just return the oldest one (lowest ID).
        foreach ($jinoforums as $jinoforum) {
            return $jinoforum;   // ie the first one
        }
    }

    // Doesn't exist, so create one now.
    $jinoforum = new stdClass();
    $jinoforum->course = $courseid;
    $jinoforum->type = "$type";
    if (!empty($USER->htmleditor)) {
        $jinoforum->introformat = $USER->htmleditor;
    }
    switch ($jinoforum->type) {
        case "news":
            $jinoforum->name = get_string("namenews", "jinoforum");
            $jinoforum->intro = get_string("intronews", "jinoforum");
            $jinoforum->forcesubscribe = JINOFORUM_FORCESUBSCRIBE;
            $jinoforum->assessed = 0;
            if ($courseid == SITEID) {
                $jinoforum->name = get_string("sitenews");
                $jinoforum->forcesubscribe = 0;
            }
            break;
        case "social":
            $jinoforum->name = get_string("namesocial", "jinoforum");
            $jinoforum->intro = get_string("introsocial", "jinoforum");
            $jinoforum->assessed = 0;
            $jinoforum->forcesubscribe = 0;
            break;
        case "blog":
            $jinoforum->name = get_string('blogjinoforum', 'jinoforum');
            $jinoforum->intro = get_string('introblog', 'jinoforum');
            $jinoforum->assessed = 0;
            $jinoforum->forcesubscribe = 0;
            break;
        default:
            echo $OUTPUT->notification("That jinoforum type doesn't exist!");
            return false;
            break;
    }

    $jinoforum->timemodified = time();
    $jinoforum->id = $DB->insert_record("jinoforum", $jinoforum);

    if (!$module = $DB->get_record("modules", array("name" => "jinoforum"))) {
        echo $OUTPUT->notification("Could not find jinoforum module!!");
        return false;
    }
    $mod = new stdClass();
    $mod->course = $courseid;
    $mod->module = $module->id;
    $mod->instance = $jinoforum->id;
    $mod->section = 0;
    include_once("$CFG->dirroot/course/lib.php");
    if (!$mod->coursemodule = add_course_module($mod)) {
        echo $OUTPUT->notification("Could not add a new course module to the course '" . $courseid . "'");
        return false;
    }
    $sectionid = course_add_cm_to_section($courseid, $mod->coursemodule, 0);
    return $DB->get_record("jinoforum", array("id" => "$jinoforum->id"));
}

/**
 * Given the data about a posting, builds up the HTML to display it and
 * returns the HTML in a string.  This is designed for sending via HTML email.
 *
 * @global object
 * @param object $course
 * @param object $cm
 * @param object $jinoforum
 * @param object $discussion
 * @param object $post
 * @param object $userform
 * @param object $userto
 * @param bool $ownpost
 * @param bool $reply
 * @param bool $link
 * @param bool $rate
 * @param string $footer
 * @return string
 */
function jinoforum_make_mail_post($course, $cm, $jinoforum, $discussion, $post, $userfrom, $userto, $ownpost = false, $reply = false, $link = false, $rate = false, $footer = "") {

    global $CFG, $OUTPUT;

    $modcontext = context_module::instance($cm->id);

    if (!isset($userto->viewfullnames[$jinoforum->id])) {
        $viewfullnames = has_capability('moodle/site:viewfullnames', $modcontext, $userto->id);
    } else {
        $viewfullnames = $userto->viewfullnames[$jinoforum->id];
    }

    // add absolute file links
    $post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_jinoforum', 'post', $post->id);

    // format the post body
    $options = new stdClass();
    $options->para = true;
    $formattedtext = format_text($post->message, $post->messageformat, $options, $course->id);

    $output = '<table border="0" cellpadding="3" cellspacing="0" class="jinoforumpost">';

    $output .= '<tr class="header"><td width="35" valign="top" class="picture left">';
    $output .= $OUTPUT->user_picture($userfrom, array('courseid' => $course->id));
    $output .= '</td>';

    if ($post->parent) {
        $output .= '<td class="topic">';
    } else {
        $output .= '<td class="topic starter">';
    }
    $output .= '<div class="subject">' . format_string($post->subject) . '</div>';

    $fullname = fullname($userfrom, $viewfullnames);
    $by = new stdClass();
    $by->name = '<a href="' . $CFG->wwwroot . '/user/view.php?id=' . $userfrom->id . '&amp;course=' . $course->id . '">' . $fullname . '</a>';
    $by->date = userdate($post->modified, '', $userto->timezone);
    $output .= '<div class="author">' . get_string('bynameondate', 'jinoforum', $by) . '</div>';

    $output .= '</td></tr>';

    $output .= '<tr><td class="left side" valign="top">';

    if (isset($userfrom->groups)) {
        $groups = $userfrom->groups[$jinoforum->id];
    } else {
        $groups = groups_get_all_groups($course->id, $userfrom->id, $cm->groupingid);
    }

    if ($groups) {
        $output .= print_group_picture($groups, $course->id, false, true, true);
    } else {
        $output .= '&nbsp;';
    }

    $output .= '</td><td class="content">';

    $attachments = jinoforum_print_attachments($post, $cm, 'html');
    if ($attachments !== '') {
        $output .= '<div class="attachments">';
        $output .= $attachments;
        $output .= '</div>';
    }

    $output .= $formattedtext;

// Commands
    $commands = array();

    if ($post->parent) {
        // $commands[] = '<a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' .$post->discussion . '&amp;parent=' . $post->parent . '">' . get_string('parent', 'jinoforum') . '</a>';
    }

    if ($reply) {
        $commands[] = '<a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/post.php?reply=' . $post->id . '">' .
                get_string('reply', 'jinoforum') . '</a>';
    }

    $output .= '<div class="commands">';
    $output .= implode(' | ', $commands);
    $output .= '</div>';

// Context link to post if required
    if ($link) {
        $output .= '<div class="link">';
        $output .= '<a target="_blank" href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '#p' . $post->id . '">' .
                get_string('postincontext', 'jinoforum') . '</a>';
        $output .= '</div>';
    }

    if ($footer) {
        $output .= '<div class="footer">' . $footer . '</div>';
    }
    $output .= '</td></tr></table>' . "\n\n";

    return $output;
}

/**
 * Print a jinoforum post
 *
 * @global object
 * @global object
 * @uses JINOFORUM_MODE_THREADED
 * @uses PORTFOLIO_FORMAT_PLAINHTML
 * @uses PORTFOLIO_FORMAT_FILE
 * @uses PORTFOLIO_FORMAT_RICHHTML
 * @uses PORTFOLIO_ADD_TEXT_LINK
 * @uses CONTEXT_MODULE
 * @param object $post The post to print.
 * @param object $discussion
 * @param object $jinoforum
 * @param object $cm
 * @param object $course
 * @param boolean $ownpost Whether this post belongs to the current user.
 * @param boolean $reply Whether to print a 'reply' link at the bottom of the message.
 * @param boolean $link Just print a shortened version of the post as a link to the full post.
 * @param string $footer Extra stuff to print after the message.
 * @param string $highlight Space-separated list of terms to highlight.
 * @param int $post_read true, false or -99. If we already know whether this user
 *          has read this post, pass that in, otherwise, pass in -99, and this
 *          function will work it out.
 * @param boolean $dummyifcantsee When jinoforum_user_can_see_post says that
 *          the current user can't see this post, if this argument is true
 *          (the default) then print a dummy 'you can't see this post' post.
 *          If false, don't output anything at all.
 * @param bool|null $istracked
 * @return void
 */
function jinoforum_print_post_first($post, $discussion, $jinoforum, &$cm, $course, $ownpost = false, $reply = false, $link = false, $footer = "", $highlight = "", $postisread = null, $dummyifcantsee = true, $istracked = null, $return = false) {
    global $USER, $CFG, $OUTPUT;

    require_once($CFG->libdir . '/filelib.php');

    // String cache
    static $str;

    $modcontext = context_module::instance($cm->id);

    $post->course = $course->id;
    $post->jinoforum = $jinoforum->id;
    $post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_jinoforum', 'post', $post->id);
    if (!empty($CFG->enableplagiarism)) {
        require_once($CFG->libdir . '/plagiarismlib.php');
        $post->message .= plagiarism_get_links(array('userid' => $post->userid,
            'content' => $post->message,
            'cmid' => $cm->id,
            'course' => $post->course,
            'jinoforum' => $post->jinoforum));
    }

    // caching
    if (!isset($cm->cache)) {
        $cm->cache = new stdClass;
    }

    if (!isset($cm->cache->caps)) {
        $cm->cache->caps = array();
        $cm->cache->caps['mod/jinoforum:viewdiscussion'] = has_capability('mod/jinoforum:viewdiscussion', $modcontext);
        $cm->cache->caps['moodle/site:viewfullnames'] = has_capability('moodle/site:viewfullnames', $modcontext);
        $cm->cache->caps['mod/jinoforum:editanypost'] = has_capability('mod/jinoforum:editanypost', $modcontext);
        $cm->cache->caps['mod/jinoforum:splitdiscussions'] = false;    //has_capability('mod/jinoforum:splitdiscussions', $modcontext); 
        $cm->cache->caps['mod/jinoforum:deleteownpost'] = has_capability('mod/jinoforum:deleteownpost', $modcontext);
        $cm->cache->caps['mod/jinoforum:deleteanypost'] = has_capability('mod/jinoforum:deleteanypost', $modcontext);
        $cm->cache->caps['mod/jinoforum:viewanyrating'] = has_capability('mod/jinoforum:viewanyrating', $modcontext);
        $cm->cache->caps['mod/jinoforum:exportpost'] = has_capability('mod/jinoforum:exportpost', $modcontext);
        $cm->cache->caps['mod/jinoforum:exportownpost'] = has_capability('mod/jinoforum:exportownpost', $modcontext);
    }

    if (!isset($cm->uservisible)) {
        $cm->uservisible = \core_availability\info_module::is_user_visible($cm, 0, false);
    }

    if ($istracked && is_null($postisread)) {
        $postisread = jinoforum_tp_is_post_read($USER->id, $post);
    }

    if (empty($str)) {
        $str = new stdClass;
        $str->edit = get_string('edit', 'jinoforum');
        $str->delete = get_string('delete', 'jinoforum');
        $str->reply = get_string('reply', 'jinoforum');
        $str->parent = get_string('parent', 'jinoforum');
        $str->pruneheading = get_string('pruneheading', 'jinoforum');
        $str->prune = get_string('prune', 'jinoforum');
        $str->displaymode = get_user_preferences('jinoforum_displaymode', $CFG->jinoforum_displaymode);
        $str->markread = get_string('markread', 'jinoforum');
        $str->markunread = get_string('markunread', 'jinoforum');
    }

    $discussionlink = new moodle_url('/mod/jinoforum/discuss.php', array('d' => $post->discussion));

    // Build an object that represents the posting user
    $postuser = new stdClass;
    $postuserfields = explode(',', user_picture::fields());
    $postuser = username_load_fields_from_object($postuser, $post, null, $postuserfields);
    $postuser->id = $post->userid;
    $postuser->fullname = fullname($postuser, $cm->cache->caps['moodle/site:viewfullnames']);
    $postuser->profilelink = new moodle_url('/user/view.php', array('id' => $post->userid, 'course' => $course->id));

    // Prepare the groups the posting user belongs to
    if (isset($cm->cache->usersgroups)) {
        $groups = array();
        if (isset($cm->cache->usersgroups[$post->userid])) {
            foreach ($cm->cache->usersgroups[$post->userid] as $gid) {
                $groups[$gid] = $cm->cache->groups[$gid];
            }
        }
    } else {
        $groups = groups_get_all_groups($course->id, $post->userid, $cm->groupingid);
    }

    // Prepare the attachements for the post, files then images
    list($attachments, $attachedimages) = jinoforum_print_attachments($post, $cm, 'separateimages');

    // Determine if we need to shorten this post
    $shortenpost = ($link && (strlen(strip_tags($post->message)) > $CFG->jinoforum_longpost));


    // Prepare an array of commands
    $commands = array();

    // SPECIAL CASE: The front page can display a news item post to non-logged in users.
    // Don't display the mark read / unread controls in this case.
    if ($istracked && $CFG->jinoforum_usermarksread && isloggedin()) {
        $url = new moodle_url($discussionlink, array('postid' => $post->id, 'mark' => 'unread'));
        $text = $str->markunread;
        if (!$postisread) {
            $url->param('mark', 'read');
            $text = $str->markread;
        }
        if ($str->displaymode == JINOFORUM_MODE_THREADED) {
            $url->param('parent', $post->parent);
        } else {
            $url->set_anchor('p' . $post->id);
        }
        $commands[] = array('url' => $url, 'text' => $text);
    }

    // Zoom in to the parent specifically
    if ($post->parent) {
        $url = new moodle_url($discussionlink);
        if ($str->displaymode == JINOFORUM_MODE_THREADED) {
            $url->param('parent', $post->parent);
        } else {
            $url->set_anchor('p' . $post->parent);
        }
        //$commands[] = array('url' => $url, 'text' => $str->parent);
    }

    // Hack for allow to edit news posts those are not displayed yet until they are displayed
    $age = time() - $post->created;
    if (!$post->parent && $jinoforum->type == 'news' && $discussion->timestart > time()) {
        $age = 0;
    }

    if ($jinoforum->type == 'single' and $discussion->firstpost == $post->id) {
        if (has_capability('moodle/course:manageactivities', $modcontext)) {
            // The first post in single simple is the jinoforum description.
            $commands[] = array('url' => new moodle_url('/course/modedit.php', array('update' => $cm->id, 'sesskey' => sesskey(), 'return' => 1)), 'text' => $str->edit);
        }
    } else if (($ownpost && $age < $CFG->maxeditingtime) || $cm->cache->caps['mod/jinoforum:editanypost']) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('edit' => $post->id)), 'text' => $str->edit);
    }

    if ($cm->cache->caps['mod/jinoforum:splitdiscussions'] && $post->parent && $jinoforum->type != 'single') {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('prune' => $post->id)), 'text' => $str->prune, 'title' => $str->pruneheading);
    }

    if ($jinoforum->type == 'single' and $discussion->firstpost == $post->id) {
        // Do not allow deleting of first post in single simple type.
    } else if (($ownpost && $age < $CFG->maxeditingtime && $cm->cache->caps['mod/jinoforum:deleteownpost']) || $cm->cache->caps['mod/jinoforum:deleteanypost']) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('delete' => $post->id)), 'text' => $str->delete);
    }


    if ($CFG->enableportfolios && ($cm->cache->caps['mod/jinoforum:exportpost'] || ($ownpost && $cm->cache->caps['mod/jinoforum:exportownpost']))) {
        $p = array('postid' => $post->id);
        require_once($CFG->libdir . '/portfoliolib.php');
        $button = new portfolio_add_button();
        $button->set_callback_options('jinoforum_portfolio_caller', array('postid' => $post->id), 'mod_jinoforum');
        if (empty($attachments)) {
            $button->set_formats(PORTFOLIO_FORMAT_PLAINHTML);
        } else {
            $button->set_formats(PORTFOLIO_FORMAT_RICHHTML);
        }

        $porfoliohtml = $button->to_html(PORTFOLIO_ADD_TEXT_LINK);
        if (!empty($porfoliohtml)) {
            $commands[] = $porfoliohtml;
        }
    }
    // Finished building commands
    // Begin output

    $output = '';

    if ($istracked) {
        if ($postisread) {
            $jinoforumpostclass = ' read';
        } else {
            $jinoforumpostclass = ' unread';
            $output .= html_writer::tag('a', '', array('name' => 'unread'));
        }
    } else {
        // ignore trackign status if not tracked or tracked param missing
        $jinoforumpostclass = '';
    }

    $topicclass = '';
    if (empty($post->parent)) {
        $topicclass = ' firstpost starter';
    }

    $postbyuser = new stdClass;
    $postbyuser->post = $post->subject;
    $postbyuser->user = $postuser->fullname;
    $discussionbyuser = get_string('postbyuser', 'jinoforum', $postbyuser);
    $output .= html_writer::tag('a', '', array('id' => 'p' . $post->id));
    $output .= html_writer::start_tag('div', array('class' => 'jinoforumpost clearfix ' . $jinoforumpostclass . $topicclass,
                'role' => 'region',
                'aria-label' => $discussionbyuser));
    $output .= html_writer::start_tag('div', array('class' => 'row header clearfix'));
    $output .= html_writer::start_tag('div', array('class' => 'left picture'));
    $output .= $OUTPUT->user_picture($postuser, array('courseid' => $course->id));
    $output .= html_writer::end_tag('div');


    $output .= html_writer::start_tag('div', array('class' => 'topic' . $topicclass));

    $postsubject = $post->subject;
    if (empty($post->subjectnoformat)) {
        $postsubject = format_string($postsubject);
    }
    $output .= html_writer::tag('div', $postsubject, array('class' => 'subject',
                'role' => 'heading',
                'aria-level' => '2'));

    $by = new stdClass();
    $by->name = html_writer::link($postuser->profilelink, $postuser->fullname);
    $by->date = userdate($post->modified);
    $output .= html_writer::tag('div', get_string('bynameondate', 'jinoforum', $by), array('class' => 'author',
                'role' => 'heading',
                'aria-level' => '2'));

    $output .= html_writer::end_tag('div'); //topic
    $output .= html_writer::end_tag('div'); //row

    $output .= html_writer::start_tag('div', array('class' => 'row maincontent clearfix'));
    $output .= html_writer::start_tag('div', array('class' => 'left'));

    $groupoutput = '';
    if ($groups) {
        $groupoutput = print_group_picture($groups, $course->id, false, true, true);
    }
    if (empty($groupoutput)) {
        $groupoutput = '&nbsp;';
    }
    $output .= html_writer::tag('div', $groupoutput, array('class' => 'grouppictures'));

    $output .= html_writer::end_tag('div'); //left side
    $output .= html_writer::start_tag('div', array('class' => 'no-overflow'));
    $output .= html_writer::start_tag('div', array('class' => 'content'));
    if (!empty($attachments)) {
        $output .= html_writer::tag('div', $attachments, array('class' => 'attachments'));
    }
    $options = new stdClass;
    $options->para = false;
    $options->trusted = $post->messagetrust;
    $options->context = $modcontext;
    if ($shortenpost) {
        // Prepare shortened version by filtering the text then shortening it.
        $postclass = 'shortenedpost';
        $postcontent = format_text($post->message, $post->messageformat, $options);
        $postcontent = shorten_text($postcontent, $CFG->jinoforum_shortpost);
        $postcontent .= html_writer::link($discussionlink, get_string('readtherest', 'jinoforum'));
        $postcontent .= html_writer::tag('div', '(' . get_string('numwords', 'moodle', count_words($post->message)) . ')', array('class' => 'post-word-count'));
    } else {
        // Prepare whole post
        $postclass = 'fullpost';
        $postcontent = format_text($post->message, $post->messageformat, $options, $course->id);
        if (!empty($highlight)) {
            $postcontent = highlight($highlight, $postcontent);
        }
        if (!empty($jinoforum->displaywordcount)) {
            $postcontent .= html_writer::tag('div', get_string('numwords', 'moodle', count_words($post->message)), array('class' => 'post-word-count'));
        }
        $postcontent .= html_writer::tag('div', $attachedimages, array('class' => 'attachedimages'));
    }

    // Output the post content


    $output .= html_writer::tag('div', $postcontent, array('class' => 'posting ' . $postclass));

    $output .= html_writer::end_tag('div'); // Content
    $output .= html_writer::end_tag('div'); // Content mask
    $output .= html_writer::end_tag('div'); // Row

    $output .= html_writer::start_tag('div', array('class' => 'row side'));
    $output .= html_writer::tag('div', '&nbsp;', array('class' => 'left'));
    $output .= html_writer::start_tag('div', array('class' => 'options clearfix'));

    // Output ratings
    if (!empty($post->rating)) {
        $output .= html_writer::tag('div', $OUTPUT->render($post->rating), array('class' => 'jinoforum-post-rating'));
    }

    // Output the commands
    $commandhtml = array();
    foreach ($commands as $command) {
        if (is_array($command)) {
            $commandhtml[] = html_writer::link($command['url'], $command['text']);
        } else {
            $commandhtml[] = $command;
        }
    }
    $output .= html_writer::tag('div', implode(' | ', $commandhtml), array('class' => 'commands'));

    // Output link to post if required
    if ($link && jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $modcontext)) {
        if ($post->replies == 1) {
            $replystring = get_string('repliesone', 'jinoforum', $post->replies);
        } else {
            $replystring = get_string('repliesmany', 'jinoforum', $post->replies);
        }

        $output .= html_writer::start_tag('div', array('class' => 'link'));
        $output .= html_writer::link($discussionlink, get_string('discussthistopic', 'jinoforum'));
        $output .= '&nbsp;(' . $replystring . ')';
        $output .= html_writer::end_tag('div'); // link
    }

    // Output footer if required
    if ($footer) {

        $output .= html_writer::tag('div', $footer, array('class' => 'footer'));
    }

    // Close remaining open divs
    $output .= html_writer::end_tag('div'); // content
    $output .= html_writer::end_tag('div'); // row
    $output .= html_writer::end_tag('div'); // jinoforumpost
    // Mark the jinoforum post as read if required
    if ($istracked && !$CFG->jinoforum_usermarksread && !$postisread) {
        jinoforum_tp_mark_post_read($USER->id, $post, $jinoforum->id);
    }

    if ($return) {
        return $output;
    }
    echo $output;
    return;
}

function jinoforum_print_post_discussion($post, $discussion, $jinoforum, &$cm, $course, $ownpost = false, $reply = false, $link = false, $footer = "", $highlight = "", $postisread = null, $dummyifcantsee = true, $istracked = null, $return = false) {
    global $USER, $CFG, $OUTPUT;

    require_once($CFG->libdir . '/filelib.php');

    // String cache
    static $str;

    $modcontext = context_module::instance($cm->id);

    $post->course = $course->id;
    $post->jinoforum = $jinoforum->id;
    $post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_jinoforum', 'post', $post->id);
    if (!empty($CFG->enableplagiarism)) {
        require_once($CFG->libdir . '/plagiarismlib.php');
        $post->message .= plagiarism_get_links(array('userid' => $post->userid,
            'content' => $post->message,
            'cmid' => $cm->id,
            'course' => $post->course,
            'jinoforum' => $post->jinoforum));
    }

    // caching
    if (!isset($cm->cache)) {
        $cm->cache = new stdClass;
    }

    if (!isset($cm->cache->caps)) {
        $cm->cache->caps = array();
        $cm->cache->caps['mod/jinoforum:viewdiscussion'] = has_capability('mod/jinoforum:viewdiscussion', $modcontext);
        $cm->cache->caps['moodle/site:viewfullnames'] = has_capability('moodle/site:viewfullnames', $modcontext);
        $cm->cache->caps['mod/jinoforum:editanypost'] = has_capability('mod/jinoforum:editanypost', $modcontext);
        $cm->cache->caps['mod/jinoforum:splitdiscussions'] = has_capability('mod/jinoforum:splitdiscussions', $modcontext);
        $cm->cache->caps['mod/jinoforum:deleteownpost'] = has_capability('mod/jinoforum:deleteownpost', $modcontext);
        $cm->cache->caps['mod/jinoforum:deleteanypost'] = has_capability('mod/jinoforum:deleteanypost', $modcontext);
        $cm->cache->caps['mod/jinoforum:viewanyrating'] = has_capability('mod/jinoforum:viewanyrating', $modcontext);
        $cm->cache->caps['mod/jinoforum:exportpost'] = has_capability('mod/jinoforum:exportpost', $modcontext);
        $cm->cache->caps['mod/jinoforum:exportownpost'] = has_capability('mod/jinoforum:exportownpost', $modcontext);
    }

    if (!isset($cm->uservisible)) {
        $cm->uservisible = \core_availability\info_module::is_user_visible($cm, 0, false);
    }

    if ($istracked && is_null($postisread)) {
        $postisread = jinoforum_tp_is_post_read($USER->id, $post);
    }

    if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, NULL, $cm)) {
        $output = '';
        if (!$dummyifcantsee) {
            if ($return) {
                return $output;
            }
            echo $output;
            return;
        }
        $output .= html_writer::tag('a', '', array('id' => 'p' . $post->id));
        $output .= html_writer::start_tag('div', array('class' => 'jinoforumpost clearfix',
                    'role' => 'region',
                    'aria-label' => get_string('hiddenjinoforumpost', 'jinoforum')));
        $output .= html_writer::start_tag('div', array('class' => 'row header'));
        $output .= html_writer::tag('div', '', array('class' => 'left picture')); // Picture
        if ($post->parent) {
            $output .= html_writer::start_tag('div', array('class' => 'topic'));
        } else {
            $output .= html_writer::start_tag('div', array('class' => 'topic starter'));
        }
        $output .= html_writer::tag('div', get_string('jinoforumsubjecthidden', 'jinoforum'), array('class' => 'subject',
                    'role' => 'header')); // Subject.
        $output .= html_writer::tag('div', get_string('jinoforumauthorhidden', 'jinoforum'), array('class' => 'author',
                    'role' => 'header')); // Author.
        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div'); // row
        $output .= html_writer::start_tag('div', array('class' => 'row'));
        $output .= html_writer::tag('div', '&nbsp;', array('class' => 'left side')); // Groups
        $output .= html_writer::tag('div', get_string('jinoforumbodyhidden', 'jinoforum'), array('class' => 'content')); // Content
        $output .= html_writer::end_tag('div'); // row
        $output .= html_writer::end_tag('div'); // jinoforumpost

        if ($return) {
            return $output;
        }
        echo $output;
        return;
    }

    if (empty($str)) {
        $str = new stdClass;
        $str->edit = get_string('edit', 'jinoforum');
        $str->delete = get_string('delete', 'jinoforum');
        $str->reply = get_string('reply', 'jinoforum');
        $str->parent = get_string('parent', 'jinoforum');
        $str->pruneheading = get_string('pruneheading', 'jinoforum');
        $str->prune = get_string('prune', 'jinoforum');
        $str->displaymode = get_user_preferences('jinoforum_displaymode', $CFG->jinoforum_displaymode);
        $str->markread = get_string('markread', 'jinoforum');
        $str->markunread = get_string('markunread', 'jinoforum');
    }

    $discussionlink = new moodle_url('/mod/jinoforum/discuss.php', array('d' => $post->discussion));

    // Build an object that represents the posting user
    $postuser = new stdClass;
    $postuserfields = explode(',', user_picture::fields());
    $postuser = username_load_fields_from_object($postuser, $post, null, $postuserfields);
    $postuser->id = $post->userid;
    $postuser->fullname = fullname($postuser, $cm->cache->caps['moodle/site:viewfullnames']);
    $postuser->profilelink = new moodle_url('/user/view.php', array('id' => $post->userid, 'course' => $course->id));

    // Prepare the groups the posting user belongs to
    if (isset($cm->cache->usersgroups)) {
        $groups = array();
        if (isset($cm->cache->usersgroups[$post->userid])) {
            foreach ($cm->cache->usersgroups[$post->userid] as $gid) {
                $groups[$gid] = $cm->cache->groups[$gid];
            }
        }
    } else {
        $groups = groups_get_all_groups($course->id, $post->userid, $cm->groupingid);
    }

    // Prepare the attachements for the post, files then images
    list($attachments, $attachedimages) = jinoforum_print_attachments($post, $cm, 'separateimages');

    // Determine if we need to shorten this post
    $shortenpost = ($link && (strlen(strip_tags($post->message)) > $CFG->jinoforum_longpost));


    // Prepare an array of commands
    $commands = array();

    // SPECIAL CASE: The front page can display a news item post to non-logged in users.
    // Don't display the mark read / unread controls in this case.
    if ($istracked && $CFG->jinoforum_usermarksread && isloggedin()) {
        $url = new moodle_url($discussionlink, array('postid' => $post->id, 'mark' => 'unread'));
        $text = $str->markunread;
        if (!$postisread) {
            $url->param('mark', 'read');
            $text = $str->markread;
        }
        if ($str->displaymode == JINOFORUM_MODE_THREADED) {
            $url->param('parent', $post->parent);
        } else {
            $url->set_anchor('p' . $post->id);
        }
        $commands[] = array('url' => $url, 'text' => $text);
    }

    // Zoom in to the parent specifically
    if ($post->parent) {
        $url = new moodle_url($discussionlink);
        if ($str->displaymode == JINOFORUM_MODE_THREADED) {
            $url->param('parent', $post->parent);
        } else {
            $url->set_anchor('p' . $post->parent);
        }
        //	$commands[] = array('url' => $url, 'text' => $str->parent);
    }

    // Hack for allow to edit news posts those are not displayed yet until they are displayed
    $age = time() - $post->created;
    if (!$post->parent && $jinoforum->type == 'news' && $discussion->timestart > time()) {
        $age = 0;
    }

    if ($jinoforum->type == 'single' and $discussion->firstpost == $post->id) {
        if (has_capability('moodle/course:manageactivities', $modcontext)) {
            // The first post in single simple is the jinoforum description.
            $commands[] = array('url' => new moodle_url('/course/modedit.php', array('update' => $cm->id, 'sesskey' => sesskey(), 'return' => 1)), 'text' => $str->edit);
        }
    } else if (($ownpost && $age < $CFG->maxeditingtime) || $cm->cache->caps['mod/jinoforum:editanypost']) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('edit' => $post->id)), 'text' => $str->edit);
    }

    if ($cm->cache->caps['mod/jinoforum:splitdiscussions'] && $post->parent && $jinoforum->type != 'single') {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('prune' => $post->id)), 'text' => $str->prune, 'title' => $str->pruneheading);
    }

    if ($jinoforum->type == 'single' and $discussion->firstpost == $post->id) {
        // Do not allow deleting of first post in single simple type.
    } else if (($ownpost && $age < $CFG->maxeditingtime && $cm->cache->caps['mod/jinoforum:deleteownpost']) || $cm->cache->caps['mod/jinoforum:deleteanypost']) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('delete' => $post->id)), 'text' => $str->delete);
    }

    if ($reply) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php#mformforum', array('reply' => $post->id)), 'text' => $str->reply);
    }

    if ($CFG->enableportfolios && ($cm->cache->caps['mod/jinoforum:exportpost'] || ($ownpost && $cm->cache->caps['mod/jinoforum:exportownpost']))) {
        $p = array('postid' => $post->id);
        require_once($CFG->libdir . '/portfoliolib.php');
        $button = new portfolio_add_button();
        $button->set_callback_options('jinoforum_portfolio_caller', array('postid' => $post->id), 'mod_jinoforum');
        if (empty($attachments)) {
            $button->set_formats(PORTFOLIO_FORMAT_PLAINHTML);
        } else {
            $button->set_formats(PORTFOLIO_FORMAT_RICHHTML);
        }

        $porfoliohtml = $button->to_html(PORTFOLIO_ADD_TEXT_LINK);
        if (!empty($porfoliohtml)) {
            $commands[] = $porfoliohtml;
        }
    }
    // Finished building commands
    // Begin output

    $output = '';

    if ($istracked) {
        if ($postisread) {
            $jinoforumpostclass = ' read';
        } else {
            $jinoforumpostclass = ' unread';
            $output .= html_writer::tag('a', '', array('name' => 'unread'));
        }
    } else {
        // ignore trackign status if not tracked or tracked param missing
        $jinoforumpostclass = '';
    }

    $topicclass = '';
    if (empty($post->parent)) {
        $topicclass = ' firstpost starter';
    }

    $postbyuser = new stdClass;
    $postbyuser->post = $post->subject;
    $postbyuser->user = $postuser->fullname;
    $discussionbyuser = get_string('postbyuser', 'jinoforum', $postbyuser);
    $output .= html_writer::tag('a', '', array('id' => 'p' . $post->id));
    $output .= html_writer::start_tag('div', array('class' => 'jinoforumpost clearfix' . $jinoforumpostclass . $topicclass,
                'role' => 'region',
                'aria-label' => $discussionbyuser));

    $output .= html_writer::start_tag('div', array('class' => 'row header clearfix'));
    $output .= html_writer::start_tag('div', array('class' => 'left picture'));
    $output .= $OUTPUT->user_picture($postuser, array('courseid' => $course->id));
    $output .= html_writer::end_tag('div');


    $output .= html_writer::start_tag('div', array('class' => 'topic' . $topicclass));

    $postsubject = $post->subject;
    if (empty($post->subjectnoformat)) {
        $postsubject = format_string($postsubject);
    }
    if ($post->attachment == '') {
        $fileimg = "";
    } else {
        $fileimg = "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/icon-attachment.png' alt='" . get_string('content:file', 'jinoforum') . "'>&nbsp;";
    }

    $output .= html_writer::tag('div', $fileimg . "<a href='" . $CFG->wwwroot . "/mod/jinoforum/post_view.php?post=" . $post->id . "'>" . $postsubject . "</a>", array('class' => 'subject',
                'role' => 'heading',
                'aria-level' => '2'));
    $postcontent .= "<span class='post-viewinfo area-right'>" . $post->viewcnt . "<br/><span>" . get_string('viewCount', 'mod_jinotechboard') . "</span></span>";
    $postcontent .= "<span class='post-viewinfo area-right'>" . $post->commentscount . "<br/><span>" . get_string('reply:cnt', 'mod_jinotechboard') . "</span></span>";
    $postcontent .= "<span class='post-viewinfo area-right'>" . $post->recommendcnt . "<br/><span>" . get_string('reCommondCnt', 'mod_jinotechboard') . "</span></span>";
    $output .= html_writer::tag('div', $postcontent, array('class' => 'posting ' . $postclass));
    $by = new stdClass();
    $by->name = html_writer::link($postuser->profilelink, $postuser->fullname);
    $by->date = userdate($post->modified);
    $output .= html_writer::tag('div', get_string('bynameondate', 'jinoforum', $by), array('class' => 'author',
                'role' => 'heading',
                'aria-level' => '2'));

    $output .= html_writer::end_tag('div'); //topic
    $output .= html_writer::end_tag('div'); //row

    $output .= html_writer::start_tag('div', array('class' => 'row maincontent clearfix'));
    $output .= html_writer::start_tag('div', array('class' => 'left'));

    $groupoutput = '';
    if ($groups) {
        $groupoutput = print_group_picture($groups, $course->id, false, true, true);
    }
    if (empty($groupoutput)) {
        $groupoutput = '&nbsp;';
    }
    $output .= html_writer::tag('div', $groupoutput, array('class' => 'grouppictures'));

    $output .= html_writer::end_tag('div'); //left side
    $output .= html_writer::start_tag('div', array('class' => 'no-overflow'));
    $output .= html_writer::start_tag('div', array('class' => 'content'));
    if (!empty($attachments)) {
        $output .= html_writer::tag('div', $attachments, array('class' => 'attachments'));
    }

    $options = new stdClass;
    $options->para = false;
    $options->trusted = $post->messagetrust;
    $options->context = $modcontext;
    if ($shortenpost) {
        // Prepare shortened version by filtering the text then shortening it.
        $postclass = 'shortenedpost';
        $postcontent = format_text($post->message, $post->messageformat, $options);
        $postcontent = shorten_text($postcontent, $CFG->jinoforum_shortpost);
        $postcontent .= html_writer::link($discussionlink, get_string('readtherest', 'jinoforum'));
        $postcontent .= html_writer::tag('div', '(' . get_string('numwords', 'moodle', count_words($post->message)) . ')', array('class' => 'post-word-count'));
    } else {
        // Prepare whole post
        $postclass = 'fullpost';
        $postcontent = format_text($post->message, $post->messageformat, $options, $course->id);
        if (!empty($highlight)) {
            $postcontent = highlight($highlight, $postcontent);
        }
        if (!empty($jinoforum->displaywordcount)) {
            $postcontent .= html_writer::tag('div', get_string('numwords', 'moodle', count_words($post->message)), array('class' => 'post-word-count'));
        }
        $postcontent .= html_writer::tag('div', $attachedimages, array('class' => 'attachedimages'));
    }

    // Output the post content

    $output .= html_writer::tag('div', mb_strcut(strip_tags($postcontent), null, 630, 'utf-8') . '...', array('class' => 'posting ' . $postclass));
    $output .= html_writer::end_tag('div'); // Content
    $output .= html_writer::end_tag('div'); // Content mask
    $output .= html_writer::end_tag('div'); // Row

    $output .= html_writer::start_tag('div', array('class' => 'row side'));
    $output .= html_writer::tag('div', '&nbsp;', array('class' => 'left'));
    $output .= html_writer::start_tag('div', array('class' => 'options clearfix'));

    // Output ratings
    if (!empty($post->rating)) {
        $output .= html_writer::tag('div', $OUTPUT->render($post->rating), array('class' => 'jinoforum-post-rating'));
    }


    // Output link to post if required
    if ($link && jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $modcontext)) {
        if ($post->replies == 1) {
            $replystring = get_string('repliesone', 'jinoforum', $post->replies);
        } else {
            $replystring = get_string('repliesmany', 'jinoforum', $post->replies);
        }

        $output .= html_writer::start_tag('div', array('class' => 'link'));
        $output .= html_writer::link($discussionlink, get_string('discussthistopic', 'jinoforum'));
        $output .= '&nbsp;(' . $replystring . ')';
        $output .= html_writer::end_tag('div'); // link
    }

    // Output footer if required
    if ($footer) {
        $output .= html_writer::tag('div', $footer, array('class' => 'footer'));
    }

    // Close remaining open divs
    $output .= html_writer::end_tag('div'); // content
    $output .= html_writer::end_tag('div'); // row
    $output .= html_writer::end_tag('div'); // jinoforumpost
    // Mark the jinoforum post as read if required
    if ($istracked && !$CFG->jinoforum_usermarksread && !$postisread) {
        jinoforum_tp_mark_post_read($USER->id, $post, $jinoforum->id);
    }

    if ($return) {
        return $output;
    }
    echo $output;
    return;
}

function jinoforum_print_post($post, $discussion, $jinoforum, &$cm, $course, $ownpost = false, $reply = false, $link = false, $footer = "", $highlight = "", $postisread = null, $dummyifcantsee = true, $istracked = null, $return = false) {
    global $USER, $CFG, $OUTPUT;

    require_once($CFG->libdir . '/filelib.php');

    // String cache
    static $str;

    $modcontext = context_module::instance($cm->id);

    $post->course = $course->id;
    $post->jinoforum = $jinoforum->id;
    $post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_jinoforum', 'post', $post->id);
    if (!empty($CFG->enableplagiarism)) {
        require_once($CFG->libdir . '/plagiarismlib.php');
        $post->message .= plagiarism_get_links(array('userid' => $post->userid,
            'content' => $post->message,
            'cmid' => $cm->id,
            'course' => $post->course,
            'jinoforum' => $post->jinoforum));
    }

    // caching
    if (!isset($cm->cache)) {
        $cm->cache = new stdClass;
    }

    if (!isset($cm->cache->caps)) {
        $cm->cache->caps = array();
        $cm->cache->caps['mod/jinoforum:viewdiscussion'] = has_capability('mod/jinoforum:viewdiscussion', $modcontext);
        $cm->cache->caps['moodle/site:viewfullnames'] = has_capability('moodle/site:viewfullnames', $modcontext);
        $cm->cache->caps['mod/jinoforum:editanypost'] = has_capability('mod/jinoforum:editanypost', $modcontext);
        $cm->cache->caps['mod/jinoforum:splitdiscussions'] = has_capability('mod/jinoforum:splitdiscussions', $modcontext);
        $cm->cache->caps['mod/jinoforum:deleteownpost'] = has_capability('mod/jinoforum:deleteownpost', $modcontext);
        $cm->cache->caps['mod/jinoforum:deleteanypost'] = has_capability('mod/jinoforum:deleteanypost', $modcontext);
        $cm->cache->caps['mod/jinoforum:viewanyrating'] = has_capability('mod/jinoforum:viewanyrating', $modcontext);
        $cm->cache->caps['mod/jinoforum:exportpost'] = has_capability('mod/jinoforum:exportpost', $modcontext);
        $cm->cache->caps['mod/jinoforum:exportownpost'] = has_capability('mod/jinoforum:exportownpost', $modcontext);
    }

    if (!isset($cm->uservisible)) {
        $cm->uservisible = \core_availability\info_module::is_user_visible($cm, 0, false);
    }

    if ($istracked && is_null($postisread)) {
        $postisread = jinoforum_tp_is_post_read($USER->id, $post);
    }

    if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, NULL, $cm)) {
        $output = '';
        if (!$dummyifcantsee) {
            if ($return) {
                return $output;
            }
            echo $output;
            return;
        }
        $output .= html_writer::tag('a', '', array('id' => 'p' . $post->id));
        $output .= html_writer::start_tag('div', array('class' => 'jinoforumpost clearfix',
                    'role' => 'region',
                    'aria-label' => get_string('hiddenjinoforumpost', 'jinoforum')));
        $output .= html_writer::start_tag('div', array('class' => 'row header'));
        $output .= html_writer::tag('div', '', array('class' => 'left picture')); // Picture
        if ($post->parent) {
            $output .= html_writer::start_tag('div', array('class' => 'topic'));
        } else {
            $output .= html_writer::start_tag('div', array('class' => 'topic starter'));
        }
        $output .= html_writer::tag('div', get_string('jinoforumsubjecthidden', 'jinoforum'), array('class' => 'subject',
                    'role' => 'header')); // Subject.
        $output .= html_writer::tag('div', get_string('jinoforumauthorhidden', 'jinoforum'), array('class' => 'author',
                    'role' => 'header')); // Author.
        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div'); // row
        $output .= html_writer::start_tag('div', array('class' => 'row'));
        $output .= html_writer::tag('div', '&nbsp;', array('class' => 'left side')); // Groups
        $output .= html_writer::tag('div', get_string('jinoforumbodyhidden', 'jinoforum'), array('class' => 'content')); // Content
        $output .= html_writer::end_tag('div'); // row
        $output .= html_writer::end_tag('div'); // jinoforumpost

        if ($return) {
            return $output;
        }
        echo $output;
        return;
    }

    if (empty($str)) {
        $str = new stdClass;
        $str->edit = get_string('edit', 'jinoforum');
        $str->delete = get_string('delete', 'jinoforum');
        $str->reply = get_string('reply', 'jinoforum');
        $str->parent = get_string('parent', 'jinoforum');
        $str->pruneheading = get_string('pruneheading', 'jinoforum');
        $str->prune = get_string('prune', 'jinoforum');
        $str->displaymode = get_user_preferences('jinoforum_displaymode', $CFG->jinoforum_displaymode);
        $str->markread = get_string('markread', 'jinoforum');
        $str->markunread = get_string('markunread', 'jinoforum');
    }

    $discussionlink = new moodle_url('/mod/jinoforum/discuss.php', array('d' => $post->discussion));

    // Build an object that represents the posting user
    $postuser = new stdClass;
    $postuserfields = explode(',', user_picture::fields());
    $postuser = username_load_fields_from_object($postuser, $post, null, $postuserfields);
    $postuser->id = $post->userid;
    $postuser->fullname = fullname($postuser, $cm->cache->caps['moodle/site:viewfullnames']);
    $postuser->profilelink = new moodle_url('/user/view.php', array('id' => $post->userid, 'course' => $course->id));

    // Prepare the groups the posting user belongs to
    if (isset($cm->cache->usersgroups)) {
        $groups = array();
        if (isset($cm->cache->usersgroups[$post->userid])) {
            foreach ($cm->cache->usersgroups[$post->userid] as $gid) {
                $groups[$gid] = $cm->cache->groups[$gid];
            }
        }
    } else {
        $groups = groups_get_all_groups($course->id, $post->userid, $cm->groupingid);
    }

    // Prepare the attachements for the post, files then images
    list($attachments, $attachedimages) = jinoforum_print_attachments($post, $cm, 'separateimages');

    // Determine if we need to shorten this post
    $shortenpost = ($link && (strlen(strip_tags($post->message)) > $CFG->jinoforum_longpost));


    // Prepare an array of commands
    $commands = array();

    // SPECIAL CASE: The front page can display a news item post to non-logged in users.
    // Don't display the mark read / unread controls in this case.
    if ($istracked && $CFG->jinoforum_usermarksread && isloggedin()) {
        $url = new moodle_url($discussionlink, array('postid' => $post->id, 'mark' => 'unread'));
        $text = $str->markunread;
        if (!$postisread) {
            $url->param('mark', 'read');
            $text = $str->markread;
        }
        if ($str->displaymode == JINOFORUM_MODE_THREADED) {
            $url->param('parent', $post->parent);
        } else {
            $url->set_anchor('p' . $post->id);
        }
        $commands[] = array('url' => $url, 'text' => $text);
    }

    // Zoom in to the parent specifically
    if ($post->parent) {
        $url = new moodle_url($discussionlink);
        if ($str->displaymode == JINOFORUM_MODE_THREADED) {
            $url->param('parent', $post->parent);
        } else {
            $url->set_anchor('p' . $post->parent);
        }
        //	$commands[] = array('url' => $url, 'text' => $str->parent);
    }

    // Hack for allow to edit news posts those are not displayed yet until they are displayed
    $age = time() - $post->created;
    if (!$post->parent && $jinoforum->type == 'news' && $discussion->timestart > time()) {
        $age = 0;
    }

    if ($jinoforum->type == 'single' and $discussion->firstpost == $post->id) {
        if (has_capability('moodle/course:manageactivities', $modcontext)) {
            // The first post in single simple is the jinoforum description.
            $commands[] = array('url' => new moodle_url('/course/modedit.php', array('update' => $cm->id, 'sesskey' => sesskey(), 'return' => 1)), 'text' => $str->edit);
        }
    } else if (($ownpost && $age < $CFG->maxeditingtime) || $cm->cache->caps['mod/jinoforum:editanypost']) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('edit' => $post->id)), 'text' => $str->edit);
    }

    if ($cm->cache->caps['mod/jinoforum:splitdiscussions'] && $post->parent && $jinoforum->type != 'single') {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('prune' => $post->id)), 'text' => $str->prune, 'title' => $str->pruneheading);
    }

    if ($jinoforum->type == 'single' and $discussion->firstpost == $post->id) {
        // Do not allow deleting of first post in single simple type.
    } else if (($ownpost && $age < $CFG->maxeditingtime && $cm->cache->caps['mod/jinoforum:deleteownpost']) || $cm->cache->caps['mod/jinoforum:deleteanypost']) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php', array('delete' => $post->id)), 'text' => $str->delete);
    }

    if ($reply) {
        $commands[] = array('url' => new moodle_url('/mod/jinoforum/post.php#mformjinoforum', array('reply' => $post->id)), 'text' => $str->reply);
    }

    if ($CFG->enableportfolios && ($cm->cache->caps['mod/jinoforum:exportpost'] || ($ownpost && $cm->cache->caps['mod/jinoforum:exportownpost']))) {
        $p = array('postid' => $post->id);
        require_once($CFG->libdir . '/portfoliolib.php');
        $button = new portfolio_add_button();
        $button->set_callback_options('jinoforum_portfolio_caller', array('postid' => $post->id), 'mod_jinoforum');
        if (empty($attachments)) {
            $button->set_formats(PORTFOLIO_FORMAT_PLAINHTML);
        } else {
            $button->set_formats(PORTFOLIO_FORMAT_RICHHTML);
        }

        $porfoliohtml = $button->to_html(PORTFOLIO_ADD_TEXT_LINK);
        if (!empty($porfoliohtml)) {
            $commands[] = $porfoliohtml;
        }
    }
    // Finished building commands
    // Begin output

    $output = '';

    if ($istracked) {
        if ($postisread) {
            $jinoforumpostclass = ' read';
        } else {
            $jinoforumpostclass = ' unread';
            $output .= html_writer::tag('a', '', array('name' => 'unread'));
        }
    } else {
        // ignore trackign status if not tracked or tracked param missing
        $jinoforumpostclass = '';
    }

    $topicclass = '';
    if (empty($post->parent)) {
        $topicclass = ' firstpost starter';
    }

    $postbyuser = new stdClass;
    $postbyuser->post = $post->subject;
    $postbyuser->user = $postuser->fullname;
    $discussionbyuser = get_string('postbyuser', 'jinoforum', $postbyuser);
    $output .= html_writer::tag('a', '', array('id' => 'p' . $post->id));
    $output .= html_writer::start_tag('div', array('class' => 'jinoforumpost clearfix ' . $jinoforumpostclass . $topicclass,
                'role' => 'region',
                'aria-label' => $discussionbyuser));
    $output .= html_writer::start_tag('div', array('class' => 'row header clearfix'));
    $output .= html_writer::start_tag('div', array('class' => 'left picture'));
    $output .= $OUTPUT->user_picture($postuser, array('courseid' => $course->id));
    $output .= html_writer::end_tag('div');


    $output .= html_writer::start_tag('div', array('class' => 'topic' . $topicclass));

    $postsubject = $post->subject;
    if (empty($post->subjectnoformat)) {
        $postsubject = format_string($postsubject);
    }
    $output .= html_writer::tag('div', $postsubject, array('class' => 'subject',
                'role' => 'heading',
                'aria-level' => '2'));

    $by = new stdClass();
    $by->name = html_writer::link($postuser->profilelink, $postuser->fullname);
    $by->date = userdate($post->modified);
    $output .= html_writer::tag('div', get_string('bynameondate', 'jinoforum', $by), array('class' => 'author',
                'role' => 'heading',
                'aria-level' => '2'));

    $output .= html_writer::end_tag('div'); //topic
    $output .= html_writer::end_tag('div'); //row

    $output .= html_writer::start_tag('div', array('class' => 'row maincontent clearfix'));
    $output .= html_writer::start_tag('div', array('class' => 'left'));

    $groupoutput = '';
    if ($groups) {
        $groupoutput = print_group_picture($groups, $course->id, false, true, true);
    }
    if (empty($groupoutput)) {
        $groupoutput = '&nbsp;';
    }
    $output .= html_writer::tag('div', $groupoutput, array('class' => 'grouppictures'));

    $output .= html_writer::end_tag('div'); //left side
    $output .= html_writer::start_tag('div', array('class' => 'no-overflow'));
    $output .= html_writer::start_tag('div', array('class' => 'content'));
    if (!empty($attachments)) {
        $output .= html_writer::tag('div', $attachments, array('class' => 'attachments'));
    }

    $options = new stdClass;
    $options->para = false;
    $options->trusted = $post->messagetrust;
    $options->context = $modcontext;
    if ($shortenpost) {
        // Prepare shortened version by filtering the text then shortening it.
        $postclass = 'shortenedpost';
        $postcontent = format_text($post->message, $post->messageformat, $options);
        $postcontent = shorten_text($postcontent, $CFG->jinoforum_shortpost);
        $postcontent .= html_writer::link($discussionlink, get_string('readtherest', 'jinoforum'));
        $postcontent .= html_writer::tag('div', '(' . get_string('numwords', 'moodle', count_words($post->message)) . ')', array('class' => 'post-word-count'));
    } else {
        // Prepare whole post
        $postclass = 'fullpost';
        $postcontent = format_text($post->message, $post->messageformat, $options, $course->id);
        if (!empty($highlight)) {
            $postcontent = highlight($highlight, $postcontent);
        }
        if (!empty($jinoforum->displaywordcount)) {
            $postcontent .= html_writer::tag('div', get_string('numwords', 'moodle', count_words($post->message)), array('class' => 'post-word-count'));
        }
        $postcontent .= html_writer::tag('div', $attachedimages, array('class' => 'attachedimages'));
    }

    // Output the post content
    $output .= html_writer::tag('div', $postcontent, array('class' => 'posting ' . $postclass));
    $output .= html_writer::end_tag('div'); // Content
    $output .= html_writer::end_tag('div'); // Content mask
    $output .= html_writer::end_tag('div'); // Row

    $output .= html_writer::start_tag('div', array('class' => 'row side'));
    $output .= html_writer::tag('div', '&nbsp;', array('class' => 'left'));
    $output .= html_writer::start_tag('div', array('class' => 'options clearfix'));

    // Output ratings
    if (!empty($post->rating)) {
        $output .= html_writer::tag('div', $OUTPUT->render($post->rating), array('class' => 'jinoforum-post-rating'));
    }

    // Output the commands
    $commandhtml = array();
    foreach ($commands as $command) {
        if (is_array($command)) {
            $commandhtml[] = html_writer::link($command['url'], $command['text']);
        } else {
            $commandhtml[] = $command;
        }
    }
    $output .= html_writer::tag('div', implode(' | ', $commandhtml), array('class' => 'commands'));

    // Output link to post if required
    if ($link && jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $modcontext)) {
        if ($post->replies == 1) {
            $replystring = get_string('repliesone', 'jinoforum', $post->replies);
        } else {
            $replystring = get_string('repliesmany', 'jinoforum', $post->replies);
        }

        $output .= html_writer::start_tag('div', array('class' => 'link'));
        $output .= html_writer::link($discussionlink, get_string('discussthistopic', 'jinoforum'));
        $output .= '&nbsp;(' . $replystring . ')';
        $output .= html_writer::end_tag('div'); // link
    }

    // Output footer if required
    if ($footer) {
        $output .= html_writer::tag('div', $footer, array('class' => 'footer'));
    }

    // Close remaining open divs
    $output .= html_writer::end_tag('div'); // content
    $output .= html_writer::end_tag('div'); // row
    $output .= html_writer::end_tag('div'); // jinoforumpost
    // Mark the jinoforum post as read if required
    if ($istracked && !$CFG->jinoforum_usermarksread && !$postisread) {
        jinoforum_tp_mark_post_read($USER->id, $post, $jinoforum->id);
    }

    if ($return) {
        return $output;
    }
    echo $output;
    return;
}

/**
 * Return rating related permissions
 *
 * @param string $options the context id
 * @return array an associative array of the user's rating permissions
 */
function jinoforum_rating_permissions($contextid, $component, $ratingarea) {
    $context = context::instance_by_id($contextid, MUST_EXIST);
    if ($component != 'mod_jinoforum' || $ratingarea != 'post') {
        // We don't know about this component/ratingarea so just return null to get the
        // default restrictive permissions.
        return null;
    }
    return array(
        'view' => has_capability('mod/jinoforum:viewrating', $context),
        'viewany' => has_capability('mod/jinoforum:viewanyrating', $context),
        'viewall' => has_capability('mod/jinoforum:viewallratings', $context),
        'rate' => has_capability('mod/jinoforum:rate', $context)
    );
}

/**
 * Validates a submitted rating
 * @param array $params submitted data
 *            context => object the context in which the rated items exists [required]
 *            component => The component for this module - should always be mod_jinoforum [required]
 *            ratingarea => object the context in which the rated items exists [required]
 *            itemid => int the ID of the object being rated [required]
 *            scaleid => int the scale from which the user can select a rating. Used for bounds checking. [required]
 *            rating => int the submitted rating [required]
 *            rateduserid => int the id of the user whose items have been rated. NOT the user who submitted the ratings. 0 to update all. [required]
 *            aggregation => int the aggregation method to apply when calculating grades ie RATING_AGGREGATE_AVERAGE [required]
 * @return boolean true if the rating is valid. Will throw rating_exception if not
 */
function jinoforum_rating_validate($params) {
    global $DB, $USER;

    // Check the component is mod_jinoforum
    if ($params['component'] != 'mod_jinoforum') {
        throw new rating_exception('invalidcomponent');
    }

    // Check the ratingarea is post (the only rating area in jinoforum)
    if ($params['ratingarea'] != 'post') {
        throw new rating_exception('invalidratingarea');
    }

    // Check the rateduserid is not the current user .. you can't rate your own posts
    if ($params['rateduserid'] == $USER->id) {
        throw new rating_exception('nopermissiontorate');
    }

    // Fetch all the related records ... we need to do this anyway to call jinoforum_user_can_see_post
    $post = $DB->get_record('jinoforum_posts', array('id' => $params['itemid'], 'userid' => $params['rateduserid']), '*', MUST_EXIST);
    $discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion), '*', MUST_EXIST);
    $jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $jinoforum->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id, false, MUST_EXIST);
    $context = context_module::instance($cm->id);

    // Make sure the context provided is the context of the jinoforum
    if ($context->id != $params['context']->id) {
        throw new rating_exception('invalidcontext');
    }

    if ($jinoforum->scale != $params['scaleid']) {
        //the scale being submitted doesnt match the one in the database
        throw new rating_exception('invalidscaleid');
    }

    // check the item we're rating was created in the assessable time window
    if (!empty($jinoforum->assesstimestart) && !empty($jinoforum->assesstimefinish)) {
        if ($post->created < $jinoforum->assesstimestart || $post->created > $jinoforum->assesstimefinish) {
            throw new rating_exception('notavailable');
        }
    }

    //check that the submitted rating is valid for the scale
    // lower limit
    if ($params['rating'] < 0 && $params['rating'] != RATING_UNSET_RATING) {
        throw new rating_exception('invalidnum');
    }

    // upper limit
    if ($jinoforum->scale < 0) {
        //its a custom scale
        $scalerecord = $DB->get_record('scale', array('id' => -$jinoforum->scale));
        if ($scalerecord) {
            $scalearray = explode(',', $scalerecord->scale);
            if ($params['rating'] > count($scalearray)) {
                throw new rating_exception('invalidnum');
            }
        } else {
            throw new rating_exception('invalidscaleid');
        }
    } else if ($params['rating'] > $jinoforum->scale) {
        //if its numeric and submitted rating is above maximum
        throw new rating_exception('invalidnum');
    }

    // Make sure groups allow this user to see the item they're rating
    if ($discussion->groupid > 0 and $groupmode = groups_get_activity_groupmode($cm, $course)) {   // Groups are being used
        if (!groups_group_exists($discussion->groupid)) { // Can't find group
            throw new rating_exception('cannotfindgroup'); //something is wrong
        }

        if (!groups_is_member($discussion->groupid) and ! has_capability('moodle/site:accessallgroups', $context)) {
            // do not allow rating of posts from other groups when in SEPARATEGROUPS or VISIBLEGROUPS
            throw new rating_exception('notmemberofgroup');
        }
    }

    // perform some final capability checks
    if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, $USER, $cm)) {
        throw new rating_exception('nopermissiontorate');
    }

    return true;
}

/**
 * This function prints the overview of a discussion in the jinoforum listing.
 * It needs some discussion information and some post information, these
 * happen to be combined for efficiency in the $post parameter by the function
 * that calls this one: jinoforum_print_latest_discussions()
 *
 * @global object
 * @global object
 * @param object $post The post object (passed by reference for speed).
 * @param object $jinoforum The jinoforum object.
 * @param int $group Current group.
 * @param string $datestring Format to use for the dates.
 * @param boolean $cantrack Is tracking enabled for this jinoforum.
 * @param boolean $jinoforumtracked Is the user tracking this jinoforum.
 * @param boolean $canviewparticipants True if user has the viewparticipants permission for this course
 */
function jinoforum_print_discussion_header(&$post, $jinoforum, $group = -1, $datestring = "", $cantrack = true, $jinoforumtracked = true, $canviewparticipants = true, $modcontext = NULL, $num, $discussion) {

    global $USER, $CFG, $OUTPUT, $DB;

    static $rowcount;
    static $strmarkalldread;

    if (empty($modcontext)) {
        if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course)) {
            print_error('invalidcoursemodule');
        }
        $modcontext = context_module::instance($cm->id);
    }

    if (!isset($rowcount)) {
        $rowcount = 0;
        $strmarkalldread = get_string('markalldread', 'jinoforum');
    } else {
        $rowcount = ($rowcount + 1) % 2;
    }

    $post->subject = format_string($post->subject, true);

    echo "\n\n";
    echo '<li>';

    //number 
    // Topic
    // Picture
    $postuser = new stdClass();
    $postuserfields = explode(',', user_picture::fields());
    $postuser = username_load_fields_from_object($postuser, $post, null, $postuserfields);
    $postuser->id = $post->userid;

    echo '<span class="post-picture">';
    echo $OUTPUT->user_picture($postuser, array('courseid' => $jinoforum->course));
    echo "</span>\n";
    echo '<span class="post-title">';
    $users = $DB->get_record('jinoforum_users', array('discussionid' => $discussion->id, 'userid' => $USER->id));
    if (!has_capability('mod/jinoforum:viewcloseddiscussion', $modcontext) && !$discussion->ispublic && !$DB->get_record('jinoforum_users', array('discussionid' => $discussion->did, 'userid' => $USER->id)) && $USER->id != $discussion->userid) {
        echo '<a href="#" onclick="alert(' . "'비공개 글입니다.'" . ')">' . $post->subject . '</a>';
    } else {
        echo '<a href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '&postid=' . $post->id . '&mark=read">' . $post->subject . '</a>';
    }
    if (!$discussion->ispublic) {
        echo "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/lock.png' width='15' height='15' alt='" . get_string('isprivate', 'jinotechboard') . "' title='" . get_string('isprivate', 'jinotechboard') . "'>";
    }
    echo "<br>";

    //attachment
    /*
      echo '<span class="attachment">';
      // 첨부파일 가져오기
      $fs = get_file_storage();
      $files = $fs->get_area_files($context->id, 'mod_jinoforum', 'attachment', $post->id, 'id', false);

      if (count($files) > 0) {
      echo('        <div class="attachments" style="text-align:right; padding-bottom:15px">   ');
      $type = '';
      $output = '';
      foreach ($files as $file) {
      $filename = $file->get_filename();
      $mimetype = $file->get_mimetype();
      $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
      $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_jinotechboard/attachment/' . $post->id . '/' . $filename);
      $output .= "<a href=\"$path\">$iconimage</a> ";
      // $output .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
      //$output .= '<br />';
      }
      echo($output);
      echo ("</div>");
      }

      echo "</span>\n";
     */

    // Group picture
    if ($group !== -1) {  // Groups are active - group is a group data object or NULL
        echo '<span class="picture group">';
        if (!empty($group->picture) and empty($group->hidepicture)) {
            print_group_picture($group, $jinoforum->course, false, false, true);
        } else if (isset($group->id)) {
            if ($canviewparticipants) {
                echo '<a href="' . $CFG->wwwroot . '/user/index.php?id=' . $jinoforum->course . '&amp;group=' . $group->id . '">' . $group->name . '</a>';
            } else {
                echo $group->name;
            }
        }
        echo "</span>\n";
    }

    // User name

    echo '<span class="post-date">';
    $by = new stdClass();
    $by->name = fullname($postuser);
    $by->date = userdate($post->timemodified);
    echo get_string('bynameondate', 'jinoforum', $by);
    echo "</span>\n";
    echo "</span>\n";
    // Show the column with replies
    if (has_capability('mod/jinoforum:viewdiscussion', $modcontext)) {
        echo '<span class="post-viewinfo area-right">';
        echo $post->replies . '<br>' . get_string('replies_num', 'jinoforum');
        echo "</span>\n";

        if ($cantrack) {
            echo '<td class="replies">';
            if ($jinoforumtracked) {
                if ($post->unread > 0) {
                    echo '<span class="unread">';
                    echo '<a href="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php?d=' . $post->discussion . '#unread">';
                    echo $post->unread;
                    echo '</a>';
                    echo '<a title="' . $strmarkalldread . '" href="' . $CFG->wwwroot . '/mod/jinoforum/markposts.php?f=' .
                    $jinoforum->id . '&amp;d=' . $post->discussion . '&amp;mark=read&amp;returnpage=view.php">' .
                    '<img src="' . $OUTPUT->pix_url('t/markasread') . '" class="iconsmall" alt="' . $strmarkalldread . '" /></a>';
                    echo '</span>';
                } else {
                    echo '<span class="read">';
                    echo $post->unread;
                    echo '</span>';
                }
            } else {
                echo '<span class="read">';
                echo '-';
                echo '</span>';
            }
            echo "</td>\n";
        }
    }

    echo "</tr>\n\n";
}

/**
 * This function is now deprecated. Use shorten_text($message, $CFG->jinoforum_shortpost) instead.
 *
 * Given a post object that we already know has a long message
 * this function truncates the message nicely to the first
 * sane place between $CFG->jinoforum_longpost and $CFG->jinoforum_shortpost
 *
 * @deprecated since Moodle 2.6
 * @see shorten_text()
 * @todo finalise deprecation in 2.8 in MDL-40851
 * @global object
 * @param string $message
 * @return string
 */
function jinoforum_shorten_post($message) {
    global $CFG;
    debugging('jinoforum_shorten_post() is deprecated since Moodle 2.6. Please use shorten_text($message, $CFG->jinoforum_shortpost) instead.', DEBUG_DEVELOPER);
    return shorten_text($message, $CFG->jinoforum_shortpost);
}

/**
 * Print the drop down that allows the user to select how they want to have
 * the discussion displayed.
 *
 * @param int $id jinoforum id if $jinoforumtype is 'single',
 *              discussion id for any other jinoforum type
 * @param mixed $mode jinoforum layout mode
 * @param string $jinoforumtype optional
 */
function jinoforum_print_mode_form($id, $mode, $jinoforumtype = '') {
    global $OUTPUT;
    if ($jinoforumtype == 'single') {
        $select = new single_select(new moodle_url("/mod/jinoforum/view.php", array('f' => $id)), 'mode', jinoforum_get_layout_modes(), $mode, null, "mode");
        $select->set_label(get_string('displaymode', 'jinoforum'), array('class' => 'accesshide'));
        $select->class = "jinoforummode";
    } else {
        $select = new single_select(new moodle_url("/mod/jinoforum/discuss.php", array('d' => $id)), 'mode', jinoforum_get_layout_modes(), $mode, null, "mode");
        $select->set_label(get_string('displaymode', 'jinoforum'), array('class' => 'accesshide'));
    }
    echo $OUTPUT->render($select);
}

/**
 * @global object
 * @param object $course
 * @param string $search
 * @return string
 */
function jinoforum_search_form($search = '', $d, $mode, $type) {
    global $CFG, $OUTPUT;

    $output = '<div>';
    $output .= '<form name="searchForm" class="table-search-option" action="' . $CFG->wwwroot . '/mod/jinoforum/discuss.php" style="display:inline">';
    $output .= '<select id="menusearchfield" class="select menusearchfield" name="searchtype">';
    $checked = "";
    $checked2 = "";
    $checked3 = "";
    switch ($type) {
        case 1: $checked = 'selected';
            break;
        case 2: $checked2 = 'selected';
            break;
        case 3: $checked3 = 'selected';
            break;
    }
    $output .= '<option value="1"  ' . $checked . '>' . get_string('subject', 'jinoforum') . '</option>';
    $output .= '<option value="2"  ' . $checked2 . '>' . get_string('writer', 'jinoforum') . '</option>';
    $output .= '<option value="3"  ' . $checked3 . '>' . get_string('areapost', 'jinoforum') . '</option>';
    $output .= '</select>';
    $output .= '<label class="accesshide" for="search" >' . get_string('search', 'jinoforum') . '</label>';
    $output .= '<input id="search" name="searchval" type="text" size="18" value="' . s($search, true) . '" alt="search" />';
    $output .= '<label class="accesshide" for="searchjinoforums" >' . get_string('searchjinoforums', 'jinoforum') . '</label>';
    $output .= '<input id="searchjinoforums" class="board-search" value="' . get_string('searchjinoforums', 'jinoforum') . '" type="submit" />';
    $output .= '<input name="d" type="hidden" value="' . $d . '" />';
    $output .= '<input name="mode" type="hidden" value="' . $mode . '" />';
    $output .= '</form>';
    $output .= '</div>';

    echo $output;
}

/**
 * @global object
 * @global object
 */
function jinoforum_set_return() {
    global $CFG, $SESSION;

    if (!isset($SESSION->fromdiscussion)) {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        } else {
            $referer = "";
        }
        // If the referer is NOT a login screen then save it.
        if (!strncasecmp("$CFG->wwwroot/login", $referer, 300)) {
            $SESSION->fromdiscussion = $_SERVER["HTTP_REFERER"];
        }
    }
}

/**
 * @global object
 * @param string $default
 * @return string
 */
function jinoforum_go_back_to($default) {
    global $SESSION;

    if (!empty($SESSION->fromdiscussion)) {
        $returnto = $SESSION->fromdiscussion;
        unset($SESSION->fromdiscussion);
        return $returnto;
    } else {
        return $default;
    }
}

/**
 * Given a discussion object that is being moved to $jinoforumto,
 * this function checks all posts in that discussion
 * for attachments, and if any are found, these are
 * moved to the new jinoforum directory.
 *
 * @global object
 * @param object $discussion
 * @param int $jinoforumfrom source jinoforum id
 * @param int $jinoforumto target jinoforum id
 * @return bool success
 */
function jinoforum_move_attachments($discussion, $jinoforumfrom, $jinoforumto) {
    global $DB;

    $fs = get_file_storage();

    $newcm = get_coursemodule_from_instance('jinoforum', $jinoforumto);
    $oldcm = get_coursemodule_from_instance('jinoforum', $jinoforumfrom);

    $newcontext = context_module::instance($newcm->id);
    $oldcontext = context_module::instance($oldcm->id);

    // loop through all posts, better not use attachment flag ;-)
    if ($posts = $DB->get_records('jinoforum_posts', array('discussion' => $discussion->id), '', 'id, attachment')) {
        foreach ($posts as $post) {
            $fs->move_area_files_to_new_context($oldcontext->id, $newcontext->id, 'mod_jinoforum', 'post', $post->id);
            $attachmentsmoved = $fs->move_area_files_to_new_context($oldcontext->id, $newcontext->id, 'mod_jinoforum', 'attachment', $post->id);
            if ($attachmentsmoved > 0 && $post->attachment != '1') {
                // Weird - let's fix it
                $post->attachment = '1';
                $DB->update_record('jinoforum_posts', $post);
            } else if ($attachmentsmoved == 0 && $post->attachment != '') {
                // Weird - let's fix it
                $post->attachment = '';
                $DB->update_record('jinoforum_posts', $post);
            }
        }
    }

    return true;
}

/**
 * Returns attachments as formated text/html optionally with separate images
 *
 * @global object
 * @global object
 * @global object
 * @param object $post
 * @param object $cm
 * @param string $type html/text/separateimages
 * @return mixed string or array of (html text withouth images and image HTML)
 */
function jinoforum_print_attachments($post, $cm, $type) {
    global $CFG, $DB, $USER, $OUTPUT;

    if (empty($post->attachment)) {
        return $type !== 'separateimages' ? '' : array('', '');
    }

    if (!in_array($type, array('separateimages', 'html', 'text'))) {
        return $type !== 'separateimages' ? '' : array('', '');
    }

    if (!$context = context_module::instance($cm->id)) {
        return $type !== 'separateimages' ? '' : array('', '');
    }
    $strattachment = get_string('attachment', 'jinoforum');

    $fs = get_file_storage();

    $imagereturn = '';
    $output = '';

    $canexport = !empty($CFG->enableportfolios) && (has_capability('mod/jinoforum:exportpost', $context) || ($post->userid == $USER->id && has_capability('mod/jinoforum:exportownpost', $context)));

    if ($canexport) {
        require_once($CFG->libdir . '/portfoliolib.php');
    }

    $files = $fs->get_area_files($context->id, 'mod_jinoforum', 'attachment', $post->id, "timemodified", false);
    if ($files) {
        if ($canexport) {
            $button = new portfolio_add_button();
        }
        foreach ($files as $file) {
            $filepath = $file->get_filepath();
            if (empty($filepath)) {
                $filepath = '/';
            }

            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            $iconimage = $OUTPUT->pix_icon(file_file_icon($file), get_mimetype_description($file), 'moodle', array('class' => 'icon'));
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_jinoforum/attachment/' . $post->id . $filepath . $filename);

            if ($file->get_filesize() == -1) {
                $alertstring = get_string('deletedfile', 'jinoforum');
                $output .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . $iconimage . '</a>';
                $output .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . s($filename) . "</a>";
                $output .= "<br />";
            } else {
                if ($type == 'html') {
                    $output .= "<a href=\"$path\">$iconimage</a> ";
                    $output .= "<a href=\"$path\">" . s($filename) . "</a>";
                    if ($canexport) {
                        $button->set_callback_options('jinoforum_portfolio_caller', array('postid' => $post->id, 'attachment' => $file->get_id()), 'mod_jinoforum');
                        $button->set_format_by_file($file);
                        $output .= $button->to_html(PORTFOLIO_ADD_ICON_LINK);
                    }
                    $output .= "<br />";
                } else if ($type == 'text') {
                    $output .= "$strattachment " . s($filename) . ":\n$path\n";
                } else { //'returnimages'
                    if (in_array($mimetype, array('image/gif', 'image/jpeg', 'image/png'))) {
                        // Image attachments don't get printed as links
                        $imagereturn .= "<br /><img src=\"$path\" alt=\"\" />";
                        if ($canexport) {
                            $button->set_callback_options('jinoforum_portfolio_caller', array('postid' => $post->id, 'attachment' => $file->get_id()), 'mod_jinoforum');
                            $button->set_format_by_file($file);
                            $imagereturn .= $button->to_html(PORTFOLIO_ADD_ICON_LINK);
                        }
                    } else {
                        $output .= "<a href=\"$path\">$iconimage</a> ";
                        $output .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                        if ($canexport) {
                            $button->set_callback_options('jinoforum_portfolio_caller', array('postid' => $post->id, 'attachment' => $file->get_id()), 'mod_jinoforum');
                            $button->set_format_by_file($file);
                            $output .= $button->to_html(PORTFOLIO_ADD_ICON_LINK);
                        }
                        $output .= '<br />';
                    }
                }

                if (!empty($CFG->enableplagiarism)) {
                    require_once($CFG->libdir . '/plagiarismlib.php');
                    $output .= plagiarism_get_links(array('userid' => $post->userid,
                        'file' => $file,
                        'cmid' => $cm->id,
                        'course' => $post->course,
                        'jinoforum' => $post->jinoforum));
                    $output .= '<br />';
                }
            }
        }
    }

    if ($type !== 'separateimages') {
        return $output;
    } else {
        return array($output, $imagereturn);
    }
}

////////////////////////////////////////////////////////////////////////////////
// File API                                                                   //
////////////////////////////////////////////////////////////////////////////////

/**
 * Lists all browsable file areas
 *
 * @package  mod_jinoforum
 * @category files
 * @param stdClass $course course object
 * @param stdClass $cm course module object
 * @param stdClass $context context object
 * @return array
 */
function jinoforum_get_file_areas($course, $cm, $context) {
    return array(
        'attachment' => get_string('areaattachment', 'mod_jinoforum'),
        'post' => get_string('areapost', 'mod_jinoforum'),
    );
}

/**
 * File browsing support for jinoforum module.
 *
 * @package  mod_jinoforum
 * @category files
 * @param stdClass $browser file browser object
 * @param stdClass $areas file areas
 * @param stdClass $course course object
 * @param stdClass $cm course module
 * @param stdClass $context context module
 * @param string $filearea file area
 * @param int $itemid item ID
 * @param string $filepath file path
 * @param string $filename file name
 * @return file_info instance or null if not found
 */
function jinoforum_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    global $CFG, $DB, $USER;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return null;
    }

    // filearea must contain a real area
    if (!isset($areas[$filearea])) {
        return null;
    }

    // Note that jinoforum_user_can_see_post() additionally allows access for parent roles
    // and it explicitly checks qanda jinoforum type, too. One day, when we stop requiring
    // course:managefiles, we will need to extend this.
    if (!has_capability('mod/jinoforum:viewdiscussion', $context)) {
        return null;
    }

    if (is_null($itemid)) {
        require_once($CFG->dirroot . '/mod/jinoforum/locallib.php');
        return new jinoforum_file_info_container($browser, $course, $cm, $context, $areas, $filearea);
    }

    static $cached = array();
    // $cached will store last retrieved post, discussion and jinoforum. To make sure that the cache
    // is cleared between unit tests we check if this is the same session
    if (!isset($cached['sesskey']) || $cached['sesskey'] != sesskey()) {
        $cached = array('sesskey' => sesskey());
    }

    if (isset($cached['post']) && $cached['post']->id == $itemid) {
        $post = $cached['post'];
    } else if ($post = $DB->get_record('jinoforum_posts', array('id' => $itemid))) {
        $cached['post'] = $post;
    } else {
        return null;
    }

    if (isset($cached['discussion']) && $cached['discussion']->id == $post->discussion) {
        $discussion = $cached['discussion'];
    } else if ($discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion))) {
        $cached['discussion'] = $discussion;
    } else {
        return null;
    }

    if (isset($cached['jinoforum']) && $cached['jinoforum']->id == $cm->instance) {
        $jinoforum = $cached['jinoforum'];
    } else if ($jinoforum = $DB->get_record('jinoforum', array('id' => $cm->instance))) {
        $cached['jinoforum'] = $jinoforum;
    } else {
        return null;
    }

    $fs = get_file_storage();
    $filepath = is_null($filepath) ? '/' : $filepath;
    $filename = is_null($filename) ? '.' : $filename;
    if (!($storedfile = $fs->get_file($context->id, 'mod_jinoforum', $filearea, $itemid, $filepath, $filename))) {
        return null;
    }

    // Checks to see if the user can manage files or is the owner.
    // TODO MDL-33805 - Do not use userid here and move the capability check above.
    if (!has_capability('moodle/course:managefiles', $context) && $storedfile->get_userid() != $USER->id) {
        return null;
    }
    // Make sure groups allow this user to see this file
    if ($discussion->groupid > 0 && !has_capability('moodle/site:accessallgroups', $context)) {
        $groupmode = groups_get_activity_groupmode($cm, $course);
        if ($groupmode == SEPARATEGROUPS && !groups_is_member($discussion->groupid)) {
            return null;
        }
    }

    // Make sure we're allowed to see it...
    if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, NULL, $cm)) {
        return null;
    }

    $urlbase = $CFG->wwwroot . '/pluginfile.php';
    return new file_info_stored($browser, $context, $storedfile, $urlbase, $itemid, true, true, false, false);
}

/**
 * Serves the jinoforum attachments. Implements needed access control ;-)
 *
 * @package  mod_jinoforum
 * @category files
 * @param stdClass $course course object
 * @param stdClass $cm course module object
 * @param stdClass $context context object
 * @param string $filearea file area
 * @param array $args extra arguments
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 * @return bool false if file not found, does not return if found - justsend the file
 */
function jinoforum_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    global $CFG, $DB;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    require_course_login($course, true, $cm);

    $areas = jinoforum_get_file_areas($course, $cm, $context);

    // filearea must contain a real area
    if (!isset($areas[$filearea])) {
        return false;
    }

    $postid = (int) array_shift($args);

    if (!$post = $DB->get_record('jinoforum_posts', array('id' => $postid))) {
        return false;
    }

    if (!$discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion))) {
        return false;
    }

    if (!$jinoforum = $DB->get_record('jinoforum', array('id' => $cm->instance))) {
        return false;
    }

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_jinoforum/$filearea/$postid/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // Make sure groups allow this user to see this file
    if ($discussion->groupid > 0) {
        $groupmode = groups_get_activity_groupmode($cm, $course);
        if ($groupmode == SEPARATEGROUPS) {
            if (!groups_is_member($discussion->groupid) and ! has_capability('moodle/site:accessallgroups', $context)) {
                return false;
            }
        }
    }

    // Make sure we're allowed to see it...
    if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, NULL, $cm)) {
        return false;
    }

    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!
}

/**
 * If successful, this function returns the name of the file
 *
 * @global object
 * @param object $post is a full post record, including course and jinoforum
 * @param object $jinoforum
 * @param object $cm
 * @param mixed $mform
 * @param string $unused
 * @return bool
 */
function jinoforum_add_attachment($post, $jinoforum, $cm, $mform = null, $unused = null) {
    global $DB;

    if (empty($mform)) {
        return false;
    }

    if (empty($post->attachments)) {
        return true;   // Nothing to do
    }

    $context = context_module::instance($cm->id);

    $info = file_get_draft_area_info($post->attachments);
    $present = ($info['filecount'] > 0) ? '1' : '';
    file_save_draft_area_files($post->attachments, $context->id, 'mod_jinoforum', 'attachment', $post->id, mod_jinoforum_post_form::attachment_options($jinoforum));

    $DB->set_field('jinoforum_posts', 'attachment', $present, array('id' => $post->id));

    return true;
}

/**
 * Add a new post in an existing discussion.
 *
 * @global object
 * @global object
 * @global object
 * @param object $post
 * @param mixed $mform
 * @param string $message
 * @return int
 */
function jinoforum_add_new_post($post, $mform, &$message) {
    global $USER, $CFG, $DB;

    $discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion));
    $jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum));
    $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id);
    $context = context_module::instance($cm->id);


    if ($parent = $DB->get_record('jinoforum_posts', array('id' => $post->parent))) {
        $DB->execute("update {jinoforum_posts} set step = step + 1 where discussion = ? and ref = ? and step > ?", array($discussion->id, $parent->ref, $parent->step));



        $post->ref = $parent->ref;
        $post->step = $parent->step + 1;
        $post->lev = $parent->lev + 1;
    }

    $post->created = $post->modified = time();
    $post->mailed = JINOFORUM_MAILED_PENDING;
    $post->userid = $USER->id;
    $post->attachment = "";
    if (!isset($post->totalscore)) {
        $post->totalscore = 0;
    }
    if (!isset($post->mailnow)) {
        $post->mailnow = 0;
    }

    if ($post->id = $DB->insert_record("jinoforum_posts", $post)) {
        $post->message = file_save_draft_area_files($post->itemid, $context->id, 'mod_jinoforum', 'post', $post->id, mod_jinoforum_post_form::editor_options($context, null), $post->message);
        $DB->set_field('jinoforum_posts', 'message', $post->message, array('id' => $post->id));
        jinoforum_add_attachment($post, $jinoforum, $cm, $mform, $message);
    }

    // Update discussion modified date
    $DB->set_field("jinoforum_discussions", "timemodified", $post->modified, array("id" => $post->discussion));
    $DB->set_field("jinoforum_discussions", "usermodified", $post->userid, array("id" => $post->discussion));

    if (jinoforum_tp_can_track_jinoforums($jinoforum) && jinoforum_tp_is_tracked($jinoforum)) {
        jinoforum_tp_mark_post_read($post->userid, $post, $post->jinoforum);
    }

    // Let Moodle know that assessable content is uploaded (eg for plagiarism detection)
    jinoforum_trigger_content_uploaded_event($post, $cm, 'jinoforum_add_new_post');

    return $post->id;
}

/**
 * Update a post
 *
 * @global object
 * @global object
 * @global object
 * @param object $post
 * @param mixed $mform
 * @param string $message
 * @return bool
 */
function jinoforum_update_post($post, $mform, &$message) {
    global $USER, $CFG, $DB;

    $discussion = $DB->get_record('jinoforum_discussions', array('id' => $post->discussion));
    $jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum));
    $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id);
    $context = context_module::instance($cm->id);

    $post->modified = time();

    $DB->update_record('jinoforum_posts', $post);

    $discussion->timemodified = $post->modified; // last modified tracking
    $discussion->usermodified = $post->userid;   // last modified tracking

    if (!$post->parent) {   // Post is a discussion starter - update discussion title and times too
        $discussion->name = $post->subject;
        $discussion->timestart = $post->timestart;
        $discussion->timeend = $post->timeend;
    }
    $mform_data = $mform->get_data();
    $discussion->ispublic = $mform_data->ispublic;
    $post->message = file_save_draft_area_files($post->itemid, $context->id, 'mod_jinoforum', 'post', $post->id, mod_jinoforum_post_form::editor_options($context, $post->id), $post->message);
    $DB->set_field('jinoforum_posts', 'message', $post->message, array('id' => $post->id));

    $DB->update_record('jinoforum_discussions', $discussion);

    jinoforum_add_attachment($post, $jinoforum, $cm, $mform, $message);

    if (jinoforum_tp_can_track_jinoforums($jinoforum) && jinoforum_tp_is_tracked($jinoforum)) {
        jinoforum_tp_mark_post_read($post->userid, $post, $post->jinoforum);
    }

    // Let Moodle know that assessable content is uploaded (eg for plagiarism detection)
    jinoforum_trigger_content_uploaded_event($post, $cm, 'jinoforum_update_post');

    return true;
}

/**
 * Given an object containing all the necessary data,
 * create a new discussion and return the id
 *
 * @param object $post
 * @param mixed $mform
 * @param string $unused
 * @param int $userid
 * @return object
 */
function jinoforum_add_discussion($discussion, $mform = null, $unused = null, $userid = null) {
    global $USER, $CFG, $DB;

    $timenow = time();

    if (is_null($userid)) {
        $userid = $USER->id;
    }

    // The first post is stored as a real post, and linked
    // to from the discuss entry.

    $jinoforum = $DB->get_record('jinoforum', array('id' => $discussion->jinoforum));
    $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id);

    $post = new stdClass();
    $post->discussion = 0;
    $post->parent = 0;
    $post->userid = $userid;
    $post->created = $timenow;
    $post->modified = $timenow;
    $post->mailed = JINOFORUM_MAILED_PENDING;
    $post->subject = $discussion->name;
    $post->message = $discussion->message;
    $post->messageformat = $discussion->messageformat;
    $post->messagetrust = $discussion->messagetrust;
    $post->attachments = isset($discussion->attachments) ? $discussion->attachments : null;
    $post->jinoforum = $jinoforum->id;  // speedup
    $post->course = $jinoforum->course; // speedup
    $post->mailnow = $discussion->mailnow;
    $post->ref = 0;
    $post->step = 0;
    $post->lev = 0;

    if ($post->id = $DB->insert_record("jinoforum_posts", $post)) {

        $DB->set_field_select('jinoforum_posts', "ref", $post->id, "id=" . $post->id);
    }

    // TODO: Fix the calling code so that there always is a $cm when this function is called
    if (!empty($cm->id) && !empty($discussion->itemid)) {   // In "single simple discussions" this may not exist yet
        $context = context_module::instance($cm->id);
        $text = file_save_draft_area_files($discussion->itemid, $context->id, 'mod_jinoforum', 'post', $post->id, mod_jinoforum_post_form::editor_options($context, null), $post->message);
        $DB->set_field('jinoforum_posts', 'message', $text, array('id' => $post->id));
    }

    // Now do the main entry for the discussion, linking to this first post

    $discussion->firstpost = $post->id;
    $discussion->timemodified = $timenow;
    $discussion->usermodified = $post->userid;
    $discussion->userid = $userid;
    $discussion->assessed = 0;

    $post->discussion = $DB->insert_record("jinoforum_discussions", $discussion);
    $DB->delete_records('jinoforum_users', array('discussionid' => $post->discussion));
    if ($discussion->allow_user && !$discussion->ispublic) {
        $discussion->allow_user = str_replace("[", "", $discussion->allow_user);
        $allowusers = explode("]", $discussion->allow_user);
        $allowusers = array_filter($allowusers);
        foreach ($allowusers as $allowuser => $uid) {
            $udate = new stdClass();
            $udata->userid = $uid;
            $udata->discussionid = $post->discussion;
            $udata->timecreated = $timenow;
            if (!$DB->get_records("jinoforum_users", array('discussionid' => $post->discussion, 'userid' => $uid))) {
                $DB->insert_record("jinoforum_users", $udata);
            }
        }
    }

    // Finally, set the pointer on the post.
    $DB->set_field("jinoforum_posts", "discussion", $post->discussion, array("id" => $post->id));

    if (!empty($cm->id)) {
        jinoforum_add_attachment($post, $jinoforum, $cm, $mform, $unused);
    }

    if (jinoforum_tp_can_track_jinoforums($jinoforum) && jinoforum_tp_is_tracked($jinoforum)) {
        jinoforum_tp_mark_post_read($post->userid, $post, $post->jinoforum);
    }

    // Let Moodle know that assessable content is uploaded (eg for plagiarism detection)
    if (!empty($cm->id)) {
        jinoforum_trigger_content_uploaded_event($post, $cm, 'jinoforum_add_discussion');
    }

    return $post->discussion;
}

/**
 * Deletes a discussion and handles all associated cleanup.
 *
 * @global object
 * @param object $discussion Discussion to delete
 * @param bool $fulldelete True when deleting entire jinoforum
 * @param object $course Course
 * @param object $cm Course-module
 * @param object $jinoforum Jinoforum
 * @return bool
 */
function jinoforum_delete_discussion($discussion, $fulldelete, $course, $cm, $jinoforum) {
    global $DB, $CFG;
    require_once($CFG->libdir . '/completionlib.php');

    $result = true;

    if ($posts = $DB->get_records("jinoforum_posts", array("discussion" => $discussion->id))) {
        foreach ($posts as $post) {
            $post->course = $discussion->course;
            $post->jinoforum = $discussion->jinoforum;
            if (!jinoforum_delete_post($post, 'ignore', $course, $cm, $jinoforum, $fulldelete)) {
                $result = false;
            }
        }
    }

    jinoforum_tp_delete_read_records(-1, -1, $discussion->id);

    if (!$DB->delete_records("jinoforum_discussions", array("id" => $discussion->id))) {
        $result = false;
    }

    // Update completion state if we are tracking completion based on number of posts
    // But don't bother when deleting whole thing
    if (!$fulldelete) {
        $completion = new completion_info($course);
        if ($completion->is_enabled($cm) == COMPLETION_TRACKING_AUTOMATIC &&
                ($jinoforum->completiondiscussions || $jinoforum->completionreplies || $jinoforum->completionposts)) {
            $completion->update_state($cm, COMPLETION_INCOMPLETE, $discussion->userid);
        }
    }

    return $result;
}

/**
 * Deletes a single jinoforum post.
 *
 * @global object
 * @param object $post Jinoforum post object
 * @param mixed $children Whether to delete children. If false, returns false
 *   if there are any children (without deleting the post). If true,
 *   recursively deletes all children. If set to special value 'ignore', deletes
 *   post regardless of children (this is for use only when deleting all posts
 *   in a disussion).
 * @param object $course Course
 * @param object $cm Course-module
 * @param object $jinoforum Jinoforum
 * @param bool $skipcompletion True to skip updating completion state if it
 *   would otherwise be updated, i.e. when deleting entire jinoforum anyway.
 * @return bool
 */
function jinoforum_delete_post($post, $children, $course, $cm, $jinoforum, $skipcompletion = false) {
    global $DB, $CFG;
    require_once($CFG->libdir . '/completionlib.php');

    $context = context_module::instance($cm->id);

    if ($children !== 'ignore' && ($childposts = $DB->get_records('jinoforum_posts', array('parent' => $post->id)))) {
        if ($children) {
            foreach ($childposts as $childpost) {
                jinoforum_delete_post($childpost, true, $course, $cm, $jinoforum, $skipcompletion);
            }
        } else {
            return false;
        }
    }

    // Delete ratings.
    require_once($CFG->dirroot . '/rating/lib.php');
    $delopt = new stdClass;
    $delopt->contextid = $context->id;
    $delopt->component = 'mod_jinoforum';
    $delopt->ratingarea = 'post';
    $delopt->itemid = $post->id;
    $rm = new rating_manager();
    $rm->delete_ratings($delopt);

    // Delete attachments.
    $fs = get_file_storage();
    $fs->delete_area_files($context->id, 'mod_jinoforum', 'attachment', $post->id);
    $fs->delete_area_files($context->id, 'mod_jinoforum', 'post', $post->id);

    // Delete cached RSS feeds.
    if (!empty($CFG->enablerssfeeds)) {
        require_once($CFG->dirroot . '/mod/jinoforum/rsslib.php');
        jinoforum_rss_delete_file($jinoforum);
    }

    if ($DB->delete_records("jinoforum_posts", array("id" => $post->id))) {

        jinoforum_tp_delete_read_records(-1, $post->id);

        // Just in case we are deleting the last post
        jinoforum_discussion_update_last_post($post->discussion);

        // Update completion state if we are tracking completion based on number of posts
        // But don't bother when deleting whole thing

        if (!$skipcompletion) {
            $completion = new completion_info($course);
            if ($completion->is_enabled($cm) == COMPLETION_TRACKING_AUTOMATIC &&
                    ($jinoforum->completiondiscussions || $jinoforum->completionreplies || $jinoforum->completionposts)) {
                $completion->update_state($cm, COMPLETION_INCOMPLETE, $post->userid);
            }
        }

        return true;
    }
    return false;
}

/**
 * Sends post content to plagiarism plugin
 * @param object $post Jinoforum post object
 * @param object $cm Course-module
 * @param string $name
 * @return bool
 */
function jinoforum_trigger_content_uploaded_event($post, $cm, $name) {
    $context = context_module::instance($cm->id);
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'mod_jinoforum', 'attachment', $post->id, "timemodified", false);
    $params = array(
        'context' => $context,
        'objectid' => $post->id,
        'other' => array(
            'content' => $post->message,
            'pathnamehashes' => array_keys($files),
            'discussionid' => $post->discussion,
            'triggeredfrom' => $name,
        )
    );
    $event = \mod_jinoforum\event\assessable_uploaded::create($params);
    $event->trigger();
    return true;
}

/**
 * @global object
 * @param object $post
 * @param bool $children
 * @return int
 */
function jinoforum_count_replies($post, $children = true) {
    global $DB;
    $count = 0;

    if ($children) {
        if ($childposts = $DB->get_records('jinoforum_posts', array('parent' => $post->id))) {
            foreach ($childposts as $childpost) {
                $count ++; // For this child
                $count += jinoforum_count_replies($childpost, true);
            }
        }
    } else {
        $count += $DB->count_records('jinoforum_posts', array('parent' => $post->id));
    }

    return $count;
}

/**
 * @global object
 * @param int $jinoforumid
 * @param mixed $value
 * @return bool
 */
function jinoforum_forcesubscribe($jinoforumid, $value = 1) {
    global $DB;
    return $DB->set_field("jinoforum", "forcesubscribe", $value, array("id" => $jinoforumid));
}

/**
 * @global object
 * @param object $jinoforum
 * @return bool
 */
function jinoforum_is_forcesubscribed($jinoforum) {
    global $DB;
    if (isset($jinoforum->forcesubscribe)) { // then we use that
        return ($jinoforum->forcesubscribe == JINOFORUM_FORCESUBSCRIBE);
    } else {   // Check the database
        return ($DB->get_field('jinoforum', 'forcesubscribe', array('id' => $jinoforum)) == JINOFORUM_FORCESUBSCRIBE);
    }
}

function jinoforum_get_forcesubscribed($jinoforum) {
    global $DB;
    if (isset($jinoforum->forcesubscribe)) { // then we use that
        return $jinoforum->forcesubscribe;
    } else {   // Check the database
        return $DB->get_field('jinoforum', 'forcesubscribe', array('id' => $jinoforum));
    }
}

/**
 * @global object
 * @param int $userid
 * @param object $jinoforum
 * @return bool
 */
function jinoforum_is_subscribed($userid, $jinoforum) {
    global $DB;
    if (is_numeric($jinoforum)) {
        $jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum));
    }
    // If jinoforum is force subscribed and has allowforcesubscribe, then user is subscribed.
    $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id);
    if (jinoforum_is_forcesubscribed($jinoforum) && $cm &&
            has_capability('mod/jinoforum:allowforcesubscribe', context_module::instance($cm->id), $userid)) {
        return true;
    }
    return $DB->record_exists("jinoforum_subscriptions", array("userid" => $userid, "jinoforum" => $jinoforum->id));
}

function jinoforum_get_subscribed_jinoforums($course) {
    global $USER, $CFG, $DB;
    $sql = "SELECT f.id
              FROM {jinoforum} f
                   LEFT JOIN {jinoforum_subscriptions} fs ON (fs.jinoforum = f.id AND fs.userid = ?)
             WHERE f.course = ?
                   AND f.forcesubscribe <> " . JINOFORUM_DISALLOWSUBSCRIBE . "
                   AND (f.forcesubscribe = " . JINOFORUM_FORCESUBSCRIBE . " OR fs.id IS NOT NULL)";
    if ($subscribed = $DB->get_records_sql($sql, array($USER->id, $course->id))) {
        foreach ($subscribed as $s) {
            $subscribed[$s->id] = $s->id;
        }
        return $subscribed;
    } else {
        return array();
    }
}

/**
 * Returns an array of jinoforums that the current user is subscribed to and is allowed to unsubscribe from
 *
 * @return array An array of unsubscribable jinoforums
 */
function jinoforum_get_optional_subscribed_jinoforums() {
    global $USER, $DB;

    // Get courses that $USER is enrolled in and can see
    $courses = enrol_get_my_courses();
    if (empty($courses)) {
        return array();
    }

    $courseids = array();
    foreach ($courses as $course) {
        $courseids[] = $course->id;
    }
    list($coursesql, $courseparams) = $DB->get_in_or_equal($courseids, SQL_PARAMS_NAMED, 'c');

    // get all jinoforums from the user's courses that they are subscribed to and which are not set to forced
    $sql = "SELECT f.id, cm.id as cm, cm.visible
              FROM {jinoforum} f
                   JOIN {course_modules} cm ON cm.instance = f.id
                   JOIN {modules} m ON m.name = :modulename AND m.id = cm.module
                   LEFT JOIN {jinoforum_subscriptions} fs ON (fs.jinoforum = f.id AND fs.userid = :userid)
             WHERE f.forcesubscribe <> :forcesubscribe AND fs.id IS NOT NULL
                   AND cm.course $coursesql";
    $params = array_merge($courseparams, array('modulename' => 'jinoforum', 'userid' => $USER->id, 'forcesubscribe' => JINOFORUM_FORCESUBSCRIBE));
    if (!$jinoforums = $DB->get_records_sql($sql, $params)) {
        return array();
    }

    $unsubscribablejinoforums = array(); // Array to return

    foreach ($jinoforums as $jinoforum) {

        if (empty($jinoforum->visible)) {
            // the jinoforum is hidden
            $context = context_module::instance($jinoforum->cm);
            if (!has_capability('moodle/course:viewhiddenactivities', $context)) {
                // the user can't see the hidden jinoforum
                continue;
            }
        }

        // subscribe.php only requires 'mod/jinoforum:managesubscriptions' when
        // unsubscribing a user other than yourself so we don't require it here either
        // A check for whether the jinoforum has subscription set to forced is built into the SQL above

        $unsubscribablejinoforums[] = $jinoforum;
    }

    return $unsubscribablejinoforums;
}

/**
 * Adds user to the subscriber list
 *
 * @param int $userid
 * @param int $jinoforumid
 * @param context_module|null $context Module context, may be omitted if not known or if called for the current module set in page.
 */
function jinoforum_subscribe($userid, $jinoforumid, $context = null) {
    global $DB, $PAGE;

    if ($DB->record_exists("jinoforum_subscriptions", array("userid" => $userid, "jinoforum" => $jinoforumid))) {
        return true;
    }

    $sub = new stdClass();
    $sub->userid = $userid;
    $sub->jinoforum = $jinoforumid;

    $result = $DB->insert_record("jinoforum_subscriptions", $sub);

    if (!$context) {
        // Find out jinoforum context. First try to take current page context to save on DB query.
        if ($PAGE->cm && $PAGE->cm->modname === 'jinoforum' && $PAGE->cm->instance == $jinoforumid && $PAGE->context->contextlevel == CONTEXT_MODULE && $PAGE->context->instanceid == $PAGE->cm->id) {
            $context = $PAGE->context;
        } else {
            $cm = get_coursemodule_from_instance('jinoforum', $jinoforumid);
            $context = context_module::instance($cm->id);
        }
    }
    $params = array(
        'context' => $context,
        'objectid' => $result,
        'relateduserid' => $userid,
        'other' => array('jinoforumid' => $jinoforumid),
    );
    $event = \mod_jinoforum\event\subscription_created::create($params);
    $event->trigger();

    return $result;
}

/**
 * Removes user from the subscriber list
 *
 * @param int $userid
 * @param int $jinoforumid
 * @param context_module|null $context Module context, may be omitted if not known or if called for the current module set in page.
 */
function jinoforum_unsubscribe($userid, $jinoforumid, $context = null) {
    global $DB, $PAGE;

    $DB->delete_records('jinoforum_digests', array('userid' => $userid, 'jinoforum' => $jinoforumid));

    if ($jinoforumsubscription = $DB->get_record('jinoforum_subscriptions', array('userid' => $userid, 'jinoforum' => $jinoforumid))) {
        $DB->delete_records('jinoforum_subscriptions', array('id' => $jinoforumsubscription->id));

        if (!$context) {
            // Find out jinoforum context. First try to take current page context to save on DB query.
            if ($PAGE->cm && $PAGE->cm->modname === 'jinoforum' && $PAGE->cm->instance == $jinoforumid && $PAGE->context->contextlevel == CONTEXT_MODULE && $PAGE->context->instanceid == $PAGE->cm->id) {
                $context = $PAGE->context;
            } else {
                $cm = get_coursemodule_from_instance('jinoforum', $jinoforumid);
                $context = context_module::instance($cm->id);
            }
        }
        $params = array(
            'context' => $context,
            'objectid' => $jinoforumsubscription->id,
            'relateduserid' => $userid,
            'other' => array('jinoforumid' => $jinoforumid),
        );
        $event = \mod_jinoforum\event\subscription_deleted::create($params);
        $event->add_record_snapshot('jinoforum_subscriptions', $jinoforumsubscription);
        $event->trigger();
    }

    return true;
}

/**
 * Given a new post, subscribes or unsubscribes as appropriate.
 * Returns some text which describes what happened.
 *
 * @global objec
 * @param object $post
 * @param object $jinoforum
 */
function jinoforum_post_subscription($post, $jinoforum) {

    global $USER;

    $action = '';
    $subscribed = jinoforum_is_subscribed($USER->id, $jinoforum);

    if ($jinoforum->forcesubscribe == JINOFORUM_FORCESUBSCRIBE) { // database ignored
        return "";
    } elseif (($jinoforum->forcesubscribe == JINOFORUM_DISALLOWSUBSCRIBE) && !has_capability('moodle/course:manageactivities', context_course::instance($jinoforum->course), $USER->id)) {
        if ($subscribed) {
            $action = 'unsubscribe'; // sanity check, following MDL-14558
        } else {
            return "";
        }
    } else { // go with the user's choice
        if (isset($post->subscribe)) {
            // no change
            if ((!empty($post->subscribe) && $subscribed) || (empty($post->subscribe) && !$subscribed)) {
                return "";
            } elseif (!empty($post->subscribe) && !$subscribed) {
                $action = 'subscribe';
            } elseif (empty($post->subscribe) && $subscribed) {
                $action = 'unsubscribe';
            }
        }
    }

    $info = new stdClass();
    $info->name = fullname($USER);
    $info->jinoforum = format_string($jinoforum->name);

    switch ($action) {
        case 'subscribe':
            jinoforum_subscribe($USER->id, $post->jinoforum);
        //return "<p>" . get_string("nowsubscribed", "jinoforum", $info) . "</p>";
        case 'unsubscribe':
            jinoforum_unsubscribe($USER->id, $post->jinoforum);
        //return "<p>" . get_string("nownotsubscribed", "jinoforum", $info) . "</p>";
    }
}

/**
 * Generate and return the subscribe or unsubscribe link for a jinoforum.
 *
 * @param object $jinoforum the jinoforum. Fields used are $jinoforum->id and $jinoforum->forcesubscribe.
 * @param object $context the context object for this jinoforum.
 * @param array $messages text used for the link in its various states
 *      (subscribed, unsubscribed, forcesubscribed or cantsubscribe).
 *      Any strings not passed in are taken from the $defaultmessages array
 *      at the top of the function.
 * @param bool $cantaccessagroup
 * @param bool $fakelink
 * @param bool $backtoindex
 * @param array $subscribed_jinoforums
 * @return string
 */
function jinoforum_get_subscribe_link($jinoforum, $context, $messages = array(), $cantaccessagroup = false, $fakelink = true, $backtoindex = false, $subscribed_jinoforums = null) {
    global $CFG, $USER, $PAGE, $OUTPUT;
    $defaultmessages = array(
        'subscribed' => get_string('unsubscribe', 'jinoforum'),
        'unsubscribed' => get_string('subscribe', 'jinoforum'),
        'cantaccessgroup' => get_string('no'),
        'forcesubscribed' => get_string('everyoneissubscribed', 'jinoforum'),
        'cantsubscribe' => get_string('disallowsubscribe', 'jinoforum')
    );
    $messages = $messages + $defaultmessages;

    if (jinoforum_is_forcesubscribed($jinoforum)) {
        return $messages['forcesubscribed'];
    } else if ($jinoforum->forcesubscribe == JINOFORUM_DISALLOWSUBSCRIBE && !has_capability('mod/jinoforum:managesubscriptions', $context)) {
        return $messages['cantsubscribe'];
    } else if ($cantaccessagroup) {
        return $messages['cantaccessgroup'];
    } else {
        if (!is_enrolled($context, $USER, '', true)) {
            return '';
        }
        if (is_null($subscribed_jinoforums)) {
            $subscribed = jinoforum_is_subscribed($USER->id, $jinoforum);
        } else {
            $subscribed = !empty($subscribed_jinoforums[$jinoforum->id]);
        }
        if ($subscribed) {
            $linktext = $messages['subscribed'];
            $linktitle = get_string('subscribestop', 'jinoforum');
        } else {
            $linktext = $messages['unsubscribed'];
            $linktitle = get_string('subscribestart', 'jinoforum');
        }

        $options = array();
        if ($backtoindex) {
            $backtoindexlink = '&amp;backtoindex=1';
            $options['backtoindex'] = 1;
        } else {
            $backtoindexlink = '';
        }
        $link = '';

        if ($fakelink) {
            $PAGE->requires->js('/mod/jinoforum/jinoforum.js');
            $PAGE->requires->js_function_call('jinoforum_produce_subscribe_link', array($jinoforum->id, $backtoindexlink, $linktext, $linktitle));
            $link = "<noscript>";
        }
        $options['id'] = $jinoforum->id;
        $options['sesskey'] = sesskey();
        $url = new moodle_url('/mod/jinoforum/subscribe.php', $options);
        $link .= $OUTPUT->single_button($url, $linktext, 'get', array('title' => $linktitle));
        if ($fakelink) {
            $link .= '</noscript>';
        }

        return $link;
    }
}

/**
 * Generate and return the track or no track link for a jinoforum.
 *
 * @global object
 * @global object
 * @global object
 * @param object $jinoforum the jinoforum. Fields used are $jinoforum->id and $jinoforum->forcesubscribe.
 * @param array $messages
 * @param bool $fakelink
 * @return string
 */
function jinoforum_get_tracking_link($jinoforum, $messages = array(), $fakelink = true) {
    global $CFG, $USER, $PAGE, $OUTPUT;

    static $strnotrackjinoforum, $strtrackjinoforum;

    if (isset($messages['trackjinoforum'])) {
        $strtrackjinoforum = $messages['trackjinoforum'];
    }
    if (isset($messages['notrackjinoforum'])) {
        $strnotrackjinoforum = $messages['notrackjinoforum'];
    }
    if (empty($strtrackjinoforum)) {
        $strtrackjinoforum = get_string('trackjinoforum', 'jinoforum');
    }
    if (empty($strnotrackjinoforum)) {
        $strnotrackjinoforum = get_string('notrackjinoforum', 'jinoforum');
    }

    if (jinoforum_tp_is_tracked($jinoforum)) {
        $linktitle = $strnotrackjinoforum;
        $linktext = $strnotrackjinoforum;
    } else {
        $linktitle = $strtrackjinoforum;
        $linktext = $strtrackjinoforum;
    }

    $link = '';
    if ($fakelink) {
        $PAGE->requires->js('/mod/jinoforum/jinoforum.js');
        $PAGE->requires->js_function_call('jinoforum_produce_tracking_link', Array($jinoforum->id, $linktext, $linktitle));
        // use <noscript> to print button in case javascript is not enabled
        $link .= '<noscript>';
    }
    $url = new moodle_url('/mod/jinoforum/settracking.php', array('id' => $jinoforum->id));
    $link .= $OUTPUT->single_button($url, $linktext, 'get', array('title' => $linktitle));

    if ($fakelink) {
        $link .= '</noscript>';
    }

    return $link;
}

/**
 * Returns true if user created new discussion already
 *
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int $userid
 * @return bool
 */
function jinoforum_user_has_posted_discussion($jinoforumid, $userid) {
    global $CFG, $DB;

    $sql = "SELECT 'x'
              FROM {jinoforum_discussions} d, {jinoforum_posts} p
             WHERE d.jinoforum = ? AND p.discussion = d.id AND p.parent = 0 and p.userid = ?";

    return $DB->record_exists_sql($sql, array($jinoforumid, $userid));
}

/**
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int $userid
 * @return array
 */
function jinoforum_discussions_user_has_posted_in($jinoforumid, $userid) {
    global $CFG, $DB;

    $haspostedsql = "SELECT d.id AS id,
                            d.*
                       FROM {jinoforum_posts} p,
                            {jinoforum_discussions} d
                      WHERE p.discussion = d.id
                        AND d.jinoforum = ?
                        AND p.userid = ?";

    return $DB->get_records_sql($haspostedsql, array($jinoforumid, $userid));
}

/**
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int $did
 * @param int $userid
 * @return bool
 */
function jinoforum_user_has_posted($jinoforumid, $did, $userid) {
    global $DB;

    if (empty($did)) {
        // posted in any jinoforum discussion?
        $sql = "SELECT 'x'
                  FROM {jinoforum_posts} p
                  JOIN {jinoforum_discussions} d ON d.id = p.discussion
                 WHERE p.userid = :userid AND d.jinoforum = :jinoforumid";
        return $DB->record_exists_sql($sql, array('jinoforumid' => $jinoforumid, 'userid' => $userid));
    } else {
        return $DB->record_exists('jinoforum_posts', array('discussion' => $did, 'userid' => $userid));
    }
}

/**
 * Returns creation time of the first user's post in given discussion
 * @global object $DB
 * @param int $did Discussion id
 * @param int $userid User id
 * @return int|bool post creation time stamp or return false
 */
function jinoforum_get_user_posted_time($did, $userid) {
    global $DB;

    $posttime = $DB->get_field('jinoforum_posts', 'MIN(created)', array('userid' => $userid, 'discussion' => $did));
    if (empty($posttime)) {
        return false;
    }
    return $posttime;
}

/**
 * @global object
 * @param object $jinoforum
 * @param object $currentgroup
 * @param int $unused
 * @param object $cm
 * @param object $context
 * @return bool
 */
function jinoforum_user_can_post_discussion($jinoforum, $currentgroup = null, $unused = -1, $cm = NULL, $context = NULL) {
// $jinoforum is an object
    global $USER;

    // shortcut - guest and not-logged-in users can not post
    if (isguestuser() or ! isloggedin()) {
        return false;
    }

    if (!$cm) {
        debugging('missing cm', DEBUG_DEVELOPER);
        if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course)) {
            print_error('invalidcoursemodule');
        }
    }

    if (!$context) {
        $context = context_module::instance($cm->id);
    }

    if ($currentgroup === null) {
        $currentgroup = groups_get_activity_group($cm);
    }

    $groupmode = groups_get_activity_groupmode($cm);

    if ($jinoforum->type == 'news') {
        $capname = 'mod/jinoforum:addnews';
    } else if ($jinoforum->type == 'qanda') {
        $capname = 'mod/jinoforum:addquestion';
    } else {
        $capname = 'mod/jinoforum:startdiscussion';
    }

    if (!has_capability($capname, $context)) {
        return false;
    }

    if ($jinoforum->type == 'single') {
        return false;
    }

    if ($jinoforum->type == 'eachuser') {
        if (jinoforum_user_has_posted_discussion($jinoforum->id, $USER->id)) {
            return false;
        }
    }

    if (!$groupmode or has_capability('moodle/site:accessallgroups', $context)) {
        return true;
    }

    if ($currentgroup) {
        return groups_is_member($currentgroup);
    } else {
        // no group membership and no accessallgroups means no new discussions
        // reverted to 1.7 behaviour in 1.9+,  buggy in 1.8.0-1.9.0
        return false;
    }
}

/**
 * This function checks whether the user can reply to posts in a jinoforum
 * discussion. Use jinoforum_user_can_post_discussion() to check whether the user
 * can start discussions.
 *
 * @global object
 * @global object
 * @uses DEBUG_DEVELOPER
 * @uses CONTEXT_MODULE
 * @uses VISIBLEGROUPS
 * @param object $jinoforum jinoforum object
 * @param object $discussion
 * @param object $user
 * @param object $cm
 * @param object $course
 * @param object $context
 * @return bool
 */
function jinoforum_user_can_post($jinoforum, $discussion, $user = NULL, $cm = NULL, $course = NULL, $context = NULL) {
    global $USER, $DB;
    if (empty($user)) {
        $user = $USER;
    }

    // shortcut - guest and not-logged-in users can not post
    if (isguestuser($user) or empty($user->id)) {
        return false;
    }

    if (!isset($discussion->groupid)) {
        debugging('incorrect discussion parameter', DEBUG_DEVELOPER);
        return false;
    }

    if (!$cm) {
        debugging('missing cm', DEBUG_DEVELOPER);
        if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course)) {
            print_error('invalidcoursemodule');
        }
    }

    if (!$course) {
        debugging('missing course', DEBUG_DEVELOPER);
        if (!$course = $DB->get_record('course', array('id' => $jinoforum->course))) {
            print_error('invalidcourseid');
        }
    }

    if (!$context) {
        $context = context_module::instance($cm->id);
    }

    // normal users with temporary guest access can not post, suspended users can not post either
    if (!is_viewing($context, $user->id) and ! is_enrolled($context, $user->id, '', true)) {
        return false;
    }

    if ($jinoforum->type == 'news') {
        $capname = 'mod/jinoforum:replynews';
    } else {
        $capname = 'mod/jinoforum:replypost';
    }

    if (!has_capability($capname, $context, $user->id)) {
        return false;
    }

    if (!$groupmode = groups_get_activity_groupmode($cm, $course)) {
        return true;
    }

    if (has_capability('moodle/site:accessallgroups', $context)) {
        return true;
    }

    if ($groupmode == VISIBLEGROUPS) {
        if ($discussion->groupid == -1) {
            // allow students to reply to all participants discussions - this was not possible in Moodle <1.8
            return true;
        }
        return groups_is_member($discussion->groupid);
    } else {
        //separate groups
        if ($discussion->groupid == -1) {
            return false;
        }
        return groups_is_member($discussion->groupid);
    }
}

/**
 * Checks to see if a user can view a particular post.
 *
 * @deprecated since Moodle 2.4 use jinoforum_user_can_see_post() instead
 *
 * @param object $post
 * @param object $course
 * @param object $cm
 * @param object $jinoforum
 * @param object $discussion
 * @param object $user
 * @return boolean
 */
function jinoforum_user_can_view_post($post, $course, $cm, $jinoforum, $discussion, $user = null) {
    debugging('jinoforum_user_can_view_post() is deprecated. Please use jinoforum_user_can_see_post() instead.', DEBUG_DEVELOPER);
    return jinoforum_user_can_see_post($jinoforum, $discussion, $post, $user, $cm);
}

/**
 * Check to ensure a user can view a timed discussion.
 *
 * @param object $discussion
 * @param object $user
 * @param object $context
 * @return boolean returns true if they can view post, false otherwise
 */
function jinoforum_user_can_see_timed_discussion($discussion, $user, $context) {
    global $CFG;

    // Check that the user can view a discussion that is normally hidden due to access times.
    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $time = time();
        if (($discussion->timestart != 0 && $discussion->timestart > $time) || ($discussion->timeend != 0 && $discussion->timeend < $time)) {
            if (!has_capability('mod/jinoforum:viewhiddentimedposts', $context, $user->id)) {
                return false;
            }
        }
    }

    return true;
}

/**
 * Check to ensure a user can view a group discussion.
 *
 * @param object $discussion
 * @param object $cm
 * @param object $context
 * @return boolean returns true if they can view post, false otherwise
 */
function jinoforum_user_can_see_group_discussion($discussion, $cm, $context) {

    // If it's a grouped discussion, make sure the user is a member.
    if ($discussion->groupid > 0) {
        $groupmode = groups_get_activity_groupmode($cm);
        if ($groupmode == SEPARATEGROUPS) {
            return groups_is_member($discussion->groupid) || has_capability('moodle/site:accessallgroups', $context);
        }
    }

    return true;
}

/**
 * @global object
 * @global object
 * @uses DEBUG_DEVELOPER
 * @param object $jinoforum
 * @param object $discussion
 * @param object $context
 * @param object $user
 * @return bool
 */
function jinoforum_user_can_see_discussion($jinoforum, $discussion, $context, $user = NULL) {
    global $USER, $DB;

    if (empty($user) || empty($user->id)) {
        $user = $USER;
    }

    // retrieve objects (yuk)
    if (is_numeric($jinoforum)) {
        debugging('missing full jinoforum', DEBUG_DEVELOPER);
        if (!$jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum))) {
            return false;
        }
    }
    if (is_numeric($discussion)) {
        debugging('missing full discussion', DEBUG_DEVELOPER);
        if (!$discussion = $DB->get_record('jinoforum_discussions', array('id' => $discussion))) {
            return false;
        }
    }
    if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course)) {
        print_error('invalidcoursemodule');
    }

    if (!has_capability('mod/jinoforum:viewdiscussion', $context)) {
        return false;
    }

    if (!jinoforum_user_can_see_timed_discussion($discussion, $user, $context)) {
        return false;
    }

    if (!jinoforum_user_can_see_group_discussion($discussion, $cm, $context)) {
        return false;
    }

    if ($jinoforum->type == 'qanda' &&
            !jinoforum_user_has_posted($jinoforum->id, $discussion->id, $user->id) &&
            !has_capability('mod/jinoforum:viewqandawithoutposting', $context)) {
        return false;
    }
    return true;
}

/**
 * @global object
 * @global object
 * @param object $jinoforum
 * @param object $discussion
 * @param object $post
 * @param object $user
 * @param object $cm
 * @return bool
 */
function jinoforum_user_can_see_post($jinoforum, $discussion, $post, $user = NULL, $cm = NULL) {
    global $CFG, $USER, $DB;

    // Context used throughout function.
    $modcontext = context_module::instance($cm->id);

    // retrieve objects (yuk)
    if (is_numeric($jinoforum)) {
        debugging('missing full jinoforum', DEBUG_DEVELOPER);
        if (!$jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum))) {
            return false;
        }
    }

    if (is_numeric($discussion)) {
        debugging('missing full discussion', DEBUG_DEVELOPER);
        if (!$discussion = $DB->get_record('jinoforum_discussions', array('id' => $discussion))) {
            return false;
        }
    }
    if (is_numeric($post)) {
        debugging('missing full post', DEBUG_DEVELOPER);
        if (!$post = $DB->get_record('jinoforum_posts', array('id' => $post))) {
            return false;
        }
    }

    if (!isset($post->id) && isset($post->parent)) {
        $post->id = $post->parent;
    }

    if (!$cm) {
        debugging('missing cm', DEBUG_DEVELOPER);
        if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course)) {
            print_error('invalidcoursemodule');
        }
    }

    if (empty($user) || empty($user->id)) {
        $user = $USER;
    }

    $canviewdiscussion = !empty($cm->cache->caps['mod/jinoforum:viewdiscussion']) || has_capability('mod/jinoforum:viewdiscussion', $modcontext, $user->id);
    if (!$canviewdiscussion && !has_all_capabilities(array('moodle/user:viewdetails', 'moodle/user:readuserposts'), context_user::instance($post->userid))) {
        return false;
    }

    if (isset($cm->uservisible)) {
        if (!$cm->uservisible) {
            return false;
        }
    } else {
        if (!\core_availability\info_module::is_user_visible($cm, $user->id, false)) {
            return false;
        }
    }

    if (!jinoforum_user_can_see_timed_discussion($discussion, $user, $modcontext)) {
        return false;
    }

    if (!jinoforum_user_can_see_group_discussion($discussion, $cm, $modcontext)) {
        return false;
    }

    if ($jinoforum->type == 'qanda') {
        $firstpost = jinoforum_get_firstpost_from_discussion($discussion->id);
        $userfirstpost = jinoforum_get_user_posted_time($discussion->id, $user->id);

        return (($userfirstpost !== false && (time() - $userfirstpost >= $CFG->maxeditingtime)) ||
                $firstpost->id == $post->id || $post->userid == $user->id || $firstpost->userid == $user->id ||
                has_capability('mod/jinoforum:viewqandawithoutposting', $modcontext, $user->id));
    }
    return true;
}

/**
 * Prints the discussion view screen for a jinoforum.
 *
 * @global object
 * @global object
 * @param object $course The current course object.
 * @param object $jinoforum Jinoforum to be printed.
 * @param int $maxdiscussions .
 * @param string $displayformat The display format to use (optional).
 * @param string $sort Sort arguments for database query (optional).
 * @param int $groupmode Group mode of the jinoforum (optional).
 * @param void $unused (originally current group)
 * @param int $page Page mode, page to display (optional).
 * @param int $perpage The maximum number of discussions per page(optional)
 *
 */
function jinoforum_print_latest_discussions($course, $jinoforum, $maxdiscussions = -1, $displayformat = 'plain', $sort = '', $currentgroup = -1, $groupmode = -1, $page = -1, $perpage = 100, $cm = NULL, $totalcount = 0) {
    global $CFG, $USER, $OUTPUT;

    if (!$cm) {
        if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course)) {
            print_error('invalidcoursemodule');
        }
    }
    $context = context_module::instance($cm->id);

    if (empty($sort)) {
        $sort = "d.timemodified DESC";
    }

    $olddiscussionlink = false;

    // Sort out some defaults
    if ($perpage <= 0) {
        $perpage = 0;
        $page = -1;
    }

    if ($maxdiscussions == 0) {
        // all discussions - backwards compatibility
        $page = -1;
        $perpage = 0;
        if ($displayformat == 'plain') {
            $displayformat = 'header';  // Abbreviate display by default
        }
    } else if ($maxdiscussions > 0) {
        $page = -1;
        $perpage = $maxdiscussions;
    }

    $fullpost = false;
    if ($displayformat == 'plain') {
        $fullpost = true;
    }


// Decide if current user is allowed to see ALL the current discussions or not
// First check the group stuff
    if ($currentgroup == -1 or $groupmode == -1) {
        $groupmode = groups_get_activity_groupmode($cm, $course);
        $currentgroup = groups_get_activity_group($cm);
    }

    $groups = array(); //cache
// If the user can post discussions, then this is a good place to put the
// button for it. We do not show the button if we are showing site news
// and the current user is a guest.

    $canstart = jinoforum_user_can_post_discussion($jinoforum, $currentgroup, $groupmode, $cm, $context);
    if (!$canstart and $jinoforum->type !== 'news') {
        if (isguestuser() or ! isloggedin()) {
            $canstart = true;
        }
        if (!is_enrolled($context) and ! is_viewing($context)) {
            // allow guests and not-logged-in to see the button - they are prompted to log in after clicking the link
            // normal users with temporary guest access see this button too, they are asked to enrol instead
            // do not show the button to users with suspended enrolments here
            $canstart = enrol_selfenrol_available($course->id);
        }
    }

    if ($canstart) {
        echo '<div class="singlebutton jinoforumaddnew">';
        echo "<form id=\"newdiscussionform\" method=\"get\" action=\"$CFG->wwwroot/mod/jinoforum/post.php\">";
        echo '<div>';
        echo "<input type=\"hidden\" name=\"jinoforum\" value=\"$jinoforum->id\" />";
        switch ($jinoforum->type) {
            case 'news':
            case 'blog':
                $buttonadd = get_string('addanewtopic', 'jinoforum');
                break;
            case 'qanda':
                $buttonadd = get_string('addanewquestion', 'jinoforum');
                break;
            default:
                $buttonadd = get_string('addanewdiscussion', 'jinoforum');
                break;
        }
        echo '<input type="submit" value="' . $buttonadd . '" />';
        echo '</div>';
        echo '</form>';
        echo "</div>\n";
        $sort_select = '<form id="frm_search">';
        $sort_select .= "<input type='hidden' name='page' value='" . ($page + 1) . "'>";
        $sort_select .= "<input type='hidden' name='id' value='" . $cm->id . "'>";
        $sort_select .= '<select name ="perpage" onchange="goto_page(1);">';
        for ($i = 10; $i <= 30; $i+=10) {
            $sort_select .= "<option value='" . $i . "'";
            if ($perpage == $i) {
                $sort_select .= "selected";
            }
            $sort_select .= " >" . get_string('sort', 'jinoforum', $i) . "</option>";
        }
        $sort_select .= "</select>";
        $sort_select .= "</form>";
        echo html_writer::tag('div', $sort_select, array('class' => 'sortselect'));
    } else if (isguestuser() or ! isloggedin() or $jinoforum->type == 'news' or
            $jinoforum->type == 'qanda' and ! has_capability('mod/jinoforum:addquestion', $context) or
            $jinoforum->type != 'qanda' and ! has_capability('mod/jinoforum:startdiscussion', $context)) {
        // no button and no info
    } else if ($groupmode and ! has_capability('moodle/site:accessallgroups', $context)) {
        // inform users why they can not post new discussion
        if (!$currentgroup) {
            echo $OUTPUT->notification(get_string('cannotadddiscussionall', 'jinoforum'));
        } else if (!groups_is_member($currentgroup)) {
            echo $OUTPUT->notification(get_string('cannotadddiscussion', 'jinoforum'));
        }
    }

// Get all the recent discussions we're allowed to see

    $getuserlastmodified = ($displayformat == 'header');
    if (!$discussions = jinoforum_get_discussions($cm, $sort, $fullpost, null, $maxdiscussions, $getuserlastmodified, $page, $perpage)) {
        echo '<div class="jinoforumnodiscuss">';
        if ($jinoforum->type == 'news') {
            echo '(' . get_string('nonews', 'jinoforum') . ')';
        } else if ($jinoforum->type == 'qanda') {
            echo '(' . get_string('noquestions', 'jinoforum') . ')';
        } else {
            echo '(' . get_string('nodiscussions', 'jinoforum') . ')';
        }
        echo "</div>\n";
        return;
    }

// If we want paging
    if ($page != -1) {
        ///Get the number of discussions found
        $numdiscussions = jinoforum_get_discussions_count($cm);

        ///Show the paging bar
        if ($numdiscussions > 1000) {
            // saves some memory on sites with very large jinoforums
            $replies = jinoforum_count_discussion_replies($jinoforum->id, $sort, $maxdiscussions, $page, $perpage);
        } else {
            $replies = jinoforum_count_discussion_replies($jinoforum->id);
        }
    } else {
        $replies = jinoforum_count_discussion_replies($jinoforum->id);

        if ($maxdiscussions > 0 and $maxdiscussions <= count($discussions)) {
            $olddiscussionlink = true;
        }
    }

    $canviewparticipants = has_capability('moodle/course:viewparticipants', $context);

    $strdatestring = get_string('strftimerecentfull');

    // Check if the jinoforum is tracked.
    if ($cantrack = jinoforum_tp_can_track_jinoforums($jinoforum)) {
        $jinoforumtracked = jinoforum_tp_is_tracked($jinoforum);
    } else {
        $jinoforumtracked = false;
    }

    if ($jinoforumtracked) {
        $unreads = jinoforum_get_discussions_unread($cm);
    } else {
        $unreads = array();
    }

    $num = $totalcount - ($page * $perpage);
    echo "<div class='thread-style'><ul class='thread-style-lists'>";
    foreach ($discussions as $discussion) {
        if ($jinoforum->type == 'qanda' && !has_capability('mod/jinoforum:viewqandawithoutposting', $context) &&
                !jinoforum_user_has_posted($jinoforum->id, $discussion->discussion, $USER->id)) {
            $canviewparticipants = false;
        }

        if (!empty($replies[$discussion->discussion])) {
            $discussion->replies = $replies[$discussion->discussion]->replies;
            $discussion->lastpostid = $replies[$discussion->discussion]->lastpostid;
        } else {
            $discussion->replies = 0;
        }

        // SPECIAL CASE: The front page can display a news item post to non-logged in users.
        // All posts are read in this case.
        if (!$jinoforumtracked) {
            $discussion->unread = '-';
        } else if (empty($USER)) {
            $discussion->unread = 0;
        } else {
            if (empty($unreads[$discussion->discussion])) {
                $discussion->unread = 0;
            } else {
                $discussion->unread = $unreads[$discussion->discussion];
            }
        }

        if (isloggedin()) {
            $ownpost = ($discussion->userid == $USER->id);
        } else {
            $ownpost = false;
        }
        // Use discussion name instead of subject of first post
        $discussion->subject = $discussion->name;

        switch ($displayformat) {
            case 'header':
                if ($groupmode > 0) {
                    if (isset($groups[$discussion->groupid])) {
                        $group = $groups[$discussion->groupid];
                    } else {
                        $group = $groups[$discussion->groupid] = groups_get_group($discussion->groupid);
                    }
                } else {
                    $group = -1;
                }

                jinoforum_print_discussion_header($discussion, $jinoforum, $group, $strdatestring, $cantrack, $jinoforumtracked, $canviewparticipants, $context, $num, $discussion);
                $num--;
                break;
            default:
                $link = false;

                if ($discussion->replies) {
                    $link = true;
                } else {
                    $modcontext = context_module::instance($cm->id);
                    $link = jinoforum_user_can_see_discussion($jinoforum, $discussion, $modcontext, $USER);
                }

                $discussion->jinoforum = $jinoforum->id;
                jinoforum_print_post($discussion, $discussion, $jinoforum, $cm, $course, $ownpost, 0, $link, false, '', null, true, $jinoforumtracked);
                break;
        }
    }
    echo "</ul></div>";
    if ($olddiscussionlink) {
        if ($jinoforum->type == 'news') {
            $strolder = get_string('oldertopics', 'jinoforum');
        } else {
            $strolder = get_string('olderdiscussions', 'jinoforum');
        }
        echo '<div class="jinoforumolddiscuss">';
        echo '<a href="' . $CFG->wwwroot . '/mod/jinoforum/view.php?f=' . $jinoforum->id . '&amp;showall=1">';
        echo $strolder . '</a> ...</div>';
    }
}

/**
 * Prints a jinoforum discussion
 *
 * @uses CONTEXT_MODULE
 * @uses JINOFORUM_MODE_FLATNEWEST
 * @uses JINOFORUM_MODE_FLATOLDEST
 * @uses JINOFORUM_MODE_THREADED
 * @uses JINOFORUM_MODE_NESTED
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $jinoforum
 * @param stdClass $discussion
 * @param stdClass $post
 * @param int $mode
 * @param mixed $canreply
 * @param bool $canrate
 */
function jinoforum_print_discussion($course, $cm, $jinoforum, $discussion, $post, $mode, $canreply = NULL, $canrate = false, $perpage = 0, $page = 0, $searchtype = 1, $searchval = "") {
    global $USER, $CFG;

    require_once($CFG->dirroot . '/rating/lib.php');

    $ownpost = (isloggedin() && $USER->id == $post->userid);

    $modcontext = context_module::instance($cm->id);
    if ($canreply === NULL) {
        $reply = jinoforum_user_can_post($jinoforum, $discussion, $USER, $cm, $course, $modcontext);
    } else {
        $reply = $canreply;
    }

    // $cm holds general cache for jinoforum functions
    $cm->cache = new stdClass;
    $cm->cache->groups = groups_get_all_groups($course->id, 0, $cm->groupingid);
    $cm->cache->usersgroups = array();

    $posters = array();

    // preload all posts - TODO: improve...
    if ($mode == JINOFORUM_MODE_FLATNEWEST) {
        $sort = "p.created DESC";
    } else {
        $sort = "p.created ASC";
    }

    $jinoforumtracked = jinoforum_tp_is_tracked($jinoforum);
    $posts = jinoforum_get_all_discussion_posts($discussion->id, $sort, $jinoforumtracked, $searchtype, $searchval);
    $post = $posts[$post->id];

    foreach ($posts as $pid => $p) {
        $posters[$p->userid] = $p->userid;
    }

    // preload all groups of ppl that posted in this discussion
    if ($postersgroups = groups_get_all_groups($course->id, $posters, $cm->groupingid, 'gm.id, gm.groupid, gm.userid')) {
        foreach ($postersgroups as $pg) {
            if (!isset($cm->cache->usersgroups[$pg->userid])) {
                $cm->cache->usersgroups[$pg->userid] = array();
            }
            $cm->cache->usersgroups[$pg->userid][$pg->groupid] = $pg->groupid;
        }
        unset($postersgroups);
    }

    //load ratings
    if ($jinoforum->assessed != RATING_AGGREGATE_NONE) {
        $ratingoptions = new stdClass;
        $ratingoptions->context = $modcontext;
        $ratingoptions->component = 'mod_jinoforum';
        $ratingoptions->ratingarea = 'post';
        $ratingoptions->items = $posts;
        $ratingoptions->aggregate = $jinoforum->assessed; //the aggregation method
        $ratingoptions->scaleid = $jinoforum->scale;
        $ratingoptions->userid = $USER->id;
        if ($jinoforum->type == 'single' or ! $discussion->id) {
            $ratingoptions->returnurl = "$CFG->wwwroot/mod/jinoforum/view.php?id=$cm->id";
        } else {
            $ratingoptions->returnurl = "$CFG->wwwroot/mod/jinoforum/discuss.php?d=$discussion->id";
        }
        $ratingoptions->assesstimestart = $jinoforum->assesstimestart;
        $ratingoptions->assesstimefinish = $jinoforum->assesstimefinish;

        $rm = new rating_manager();
        $posts = $rm->get_ratings($ratingoptions);
    }


    $post->jinoforum = $jinoforum->id;   // Add the jinoforum id to the post object, later used by jinoforum_print_post
    $post->jinoforumtype = $jinoforum->type;

    $post->subject = format_string($post->subject);

    $postread = !empty($post->postread);

    jinoforum_search_form($searchval, $discussion->id, $mode, $searchtype);
    jinoforum_print_post_first($post, $discussion, $jinoforum, $cm, $course, $ownpost, $reply, false, '', '', $postread, true, $jinoforumtracked);
    jinoforum_print_sort($discussion, $mode, $perpage, $cm, $post->id, $searchval, $searchtype);

    switch ($mode) {
        default :
            jinoforum_print_listtype($post->id, $perpage, $discussion->id, $page, $searchtype, $searchval);
            break;
        case JINOFORUM_MODE_NESTED :
            jinoforum_print_posts_nested($course, $cm, $jinoforum, $discussion, $post, $reply, $jinoforumtracked, $posts);
            break;
        case JINOFORUM_MODE_LIST :
            jinoforum_print_listtype($post->id, $perpage, $discussion->id, $page, $searchtype, $searchval);
            break;
    }
}

/**
 * @global object
 * @global object
 * @uses JINOFORUM_MODE_FLATNEWEST
 * @param object $course
 * @param object $cm
 * @param object $jinoforum
 * @param object $discussion
 * @param object $post
 * @param object $mode
 * @param bool $reply
 * @param bool $jinoforumtracked
 * @param array $posts
 * @return void
 */
function jinoforum_print_posts_flat($course, &$cm, $jinoforum, $discussion, $post, $mode, $reply, $jinoforumtracked, $posts) {
    global $USER, $CFG;

    $link = false;

    if ($mode == JINOFORUM_MODE_FLATNEWEST) {
        $sort = "ORDER BY created DESC";
    } else {
        $sort = "ORDER BY created ASC";
    }

    foreach ($posts as $post) {
        if (!$post->parent) {
            continue;
        }
        $post->subject = format_string($post->subject);
        $ownpost = ($USER->id == $post->userid);

        $postread = !empty($post->postread);

        jinoforum_print_post($post, $discussion, $jinoforum, $cm, $course, $ownpost, $reply, $link, '', '', $postread, true, $jinoforumtracked);
    }
}

/**
 * @todo Document this function
 *
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @return void
 */
function jinoforum_print_posts_threaded($course, &$cm, $jinoforum, $discussion, $parent, $depth, $reply, $jinoforumtracked, $posts) {
    global $USER, $CFG;

    $link = false;

    if (!empty($posts[$parent->id]->children)) {
        $posts = $posts[$parent->id]->children;

        $modcontext = context_module::instance($cm->id);
        $canviewfullnames = has_capability('moodle/site:viewfullnames', $modcontext);

        foreach ($posts as $post) {

            echo '<div class="indent">';
            if ($depth > 0) {
                $ownpost = ($USER->id == $post->userid);
                $post->subject = format_string($post->subject);

                $postread = !empty($post->postread);

                jinoforum_print_post($post, $discussion, $jinoforum, $cm, $course, $ownpost, $reply, $link, '', '', $postread, true, $jinoforumtracked);
            } else {
                if (!jinoforum_user_can_see_post($jinoforum, $discussion, $post, NULL, $cm)) {
                    echo "</div>\n";
                    continue;
                }
                $by = new stdClass();
                $by->name = fullname($post, $canviewfullnames);
                $by->date = userdate($post->modified);

                if ($jinoforumtracked) {
                    if (!empty($post->postread)) {
                        $style = '<span class="jinoforumthread read">';
                    } else {
                        $style = '<span class="jinoforumthread unread">';
                    }
                } else {
                    $style = '<span class="jinoforumthread">';
                }
                echo $style . "<a name=\"$post->id\"></a>" .
                "<a href=\"discuss.php?d=$post->discussion&amp;parent=$post->id\">" . format_string($post->subject, true) . "</a> ";
                print_string("bynameondate", "jinoforum", $by);
                echo "</span>";
            }

            jinoforum_print_posts_threaded($course, $cm, $jinoforum, $discussion, $post, $depth - 1, $reply, $jinoforumtracked, $posts);
            echo "</div>\n";
        }
    }
}

/**
 * @todo Document this function
 * @global object
 * @global object
 * @return void
 */
function jinoforum_print_posts_nested($course, &$cm, $jinoforum, $discussion, $parent, $reply, $jinoforumtracked, $posts) {
    global $USER, $CFG;

    $link = false;
    if (!empty($posts[$parent->id]->children)) {
        $posts = $posts[$parent->id]->children;

        foreach ($posts as $post) {
            echo '<div class="indent">';
            if (!isloggedin()) {
                $ownpost = false;
            } else {
                $ownpost = ($USER->id == $post->userid);
            }

            $post->subject = format_string($post->subject);
            $postread = !empty($post->postread);

            jinoforum_print_post_discussion($post, $discussion, $jinoforum, $cm, $course, $ownpost, $reply, $link, '', '', $postread, true, $jinoforumtracked);
            jinoforum_print_posts_nested($course, $cm, $jinoforum, $discussion, $post, $reply, $jinoforumtracked, $posts);
            echo "</div>\n";
        }
    }
}

/**
 * Returns all jinoforum posts since a given time in specified jinoforum.
 *
 * @todo Document this functions args
 * @global object
 * @global object
 * @global object
 * @global object
 */
function jinoforum_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid = 0, $groupid = 0) {
    global $CFG, $COURSE, $USER, $DB;

    if ($COURSE->id == $courseid) {
        $course = $COURSE;
    } else {
        $course = $DB->get_record('course', array('id' => $courseid));
    }

    $modinfo = get_fast_modinfo($course);

    $cm = $modinfo->cms[$cmid];
    $params = array($timestart, $cm->instance);

    if ($userid) {
        $userselect = "AND u.id = ?";
        $params[] = $userid;
    } else {
        $userselect = "";
    }

    if ($groupid) {
        $groupselect = "AND d.groupid = ?";
        $params[] = $groupid;
    } else {
        $groupselect = "";
    }

    $allnames = get_all_user_name_fields(true, 'u');
    if (!$posts = $DB->get_records_sql("SELECT p.*, f.type AS jinoforumtype, d.jinoforum, d.groupid,
                                              d.timestart, d.timeend, d.userid AS duserid,
                                              $allnames, u.email, u.picture, u.imagealt, u.email
                                         FROM {jinoforum_posts} p
                                              JOIN {jinoforum_discussions} d ON d.id = p.discussion
                                              JOIN {jinoforum} f             ON f.id = d.jinoforum
                                              JOIN {user} u              ON u.id = p.userid
                                        WHERE p.created > ? AND f.id = ?
                                              $userselect $groupselect
                                     ORDER BY p.id ASC", $params)) { // order by initial posting date
        return;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);
    $cm_context = context_module::instance($cm->id);
    $viewhiddentimed = has_capability('mod/jinoforum:viewhiddentimedposts', $cm_context);
    $accessallgroups = has_capability('moodle/site:accessallgroups', $cm_context);

    $printposts = array();
    foreach ($posts as $post) {

        if (!empty($CFG->jinoforum_enabletimedposts) and $USER->id != $post->duserid
                and ( ($post->timestart > 0 and $post->timestart > time()) or ( $post->timeend > 0 and $post->timeend < time()))) {
            if (!$viewhiddentimed) {
                continue;
            }
        }

        if ($groupmode) {
            if ($post->groupid == -1 or $groupmode == VISIBLEGROUPS or $accessallgroups) {
                // oki (Open discussions have groupid -1)
            } else {
                // separate mode
                if (isguestuser()) {
                    // shortcut
                    continue;
                }

                if (!in_array($post->groupid, $modinfo->get_groups($cm->groupingid))) {
                    continue;
                }
            }
        }

        $printposts[] = $post;
    }

    if (!$printposts) {
        return;
    }

    $aname = format_string($cm->name, true);

    foreach ($printposts as $post) {
        $tmpactivity = new stdClass();

        $tmpactivity->type = 'jinoforum';
        $tmpactivity->cmid = $cm->id;
        $tmpactivity->name = $aname;
        $tmpactivity->sectionnum = $cm->sectionnum;
        $tmpactivity->timestamp = $post->modified;

        $tmpactivity->content = new stdClass();
        $tmpactivity->content->id = $post->id;
        $tmpactivity->content->discussion = $post->discussion;
        $tmpactivity->content->subject = format_string($post->subject);
        $tmpactivity->content->parent = $post->parent;

        $tmpactivity->user = new stdClass();
        $additionalfields = array('id' => 'userid', 'picture', 'imagealt', 'email');
        $additionalfields = explode(',', user_picture::fields());
        $tmpactivity->user = username_load_fields_from_object($tmpactivity->user, $post, null, $additionalfields);
        $tmpactivity->user->id = $post->userid;

        $activities[$index++] = $tmpactivity;
    }

    return;
}

/**
 * @todo Document this function
 * @global object
 */
function jinoforum_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
    global $CFG, $OUTPUT;

    if ($activity->content->parent) {
        $class = 'reply';
    } else {
        $class = 'discussion';
    }

    echo '<table border="0" cellpadding="3" cellspacing="0" class="jinoforum-recent">';

    echo "<tr><td class=\"userpicture\" valign=\"top\">";
    echo $OUTPUT->user_picture($activity->user, array('courseid' => $courseid));
    echo "</td><td class=\"$class\">";

    echo '<div class="title">';
    if ($detail) {
        $aname = s($activity->name);
        echo "<img src=\"" . $OUTPUT->pix_url('icon', $activity->type) . "\" " .
        "class=\"icon\" alt=\"{$aname}\" />";
    }
    echo "<a href=\"$CFG->wwwroot/mod/jinoforum/discuss.php?d={$activity->content->discussion}"
    . "#p{$activity->content->id}\">{$activity->content->subject}</a>";
    echo '</div>';

    echo '<div class="user">';
    $fullname = fullname($activity->user, $viewfullnames);
    echo "<a href=\"$CFG->wwwroot/user/view.php?id={$activity->user->id}&amp;course=$courseid\">"
    . "{$fullname}</a> - " . userdate($activity->timestamp);
    echo '</div>';
    echo "</td></tr></table>";

    return;
}

/**
 * recursively sets the discussion field to $discussionid on $postid and all its children
 * used when pruning a post
 *
 * @global object
 * @param int $postid
 * @param int $discussionid
 * @return bool
 */
function jinoforum_change_discussionid($postid, $discussionid) {
    global $DB;
    $DB->set_field('jinoforum_posts', 'discussion', $discussionid, array('id' => $postid));
    if ($posts = $DB->get_records('jinoforum_posts', array('parent' => $postid))) {
        foreach ($posts as $post) {
            jinoforum_change_discussionid($post->id, $discussionid);
        }
    }
    return true;
}

/**
 * Prints the editing button on subscribers page
 *
 * @global object
 * @global object
 * @param int $courseid
 * @param int $jinoforumid
 * @return string
 */
function jinoforum_update_subscriptions_button($courseid, $jinoforumid) {
    global $CFG, $USER;

    if (!empty($USER->subscriptionsediting)) {
        $string = get_string('turneditingoff');
        $edit = "off";
    } else {
        $string = get_string('turneditingon');
        $edit = "on";
    }

    return "<form method=\"get\" action=\"$CFG->wwwroot/mod/jinoforum/subscribers.php\">" .
            "<input type=\"hidden\" name=\"id\" value=\"$jinoforumid\" />" .
            "<input type=\"hidden\" name=\"edit\" value=\"$edit\" />" .
            "<input type=\"submit\" value=\"$string\" /></form>";
}

/**
 * This function gets run whenever user is enrolled into course
 *
 * @deprecated deprecating this function as we will be using \mod_jinoforum\observer::role_assigned()
 * @param stdClass $cp
 * @return void
 */
function jinoforum_user_enrolled($cp) {
    global $DB;

    // NOTE: this has to be as fast as possible - we do not want to slow down enrolments!
    //       Originally there used to be 'mod/jinoforum:initialsubscriptions' which was
    //       introduced because we did not have enrolment information in earlier versions...

    $sql = "SELECT f.id
              FROM {jinoforum} f
         LEFT JOIN {jinoforum_subscriptions} fs ON (fs.jinoforum = f.id AND fs.userid = :userid)
             WHERE f.course = :courseid AND f.forcesubscribe = :initial AND fs.id IS NULL";
    $params = array('courseid' => $cp->courseid, 'userid' => $cp->userid, 'initial' => JINOFORUM_INITIALSUBSCRIBE);

    $jinoforums = $DB->get_records_sql($sql, $params);
    foreach ($jinoforums as $jinoforum) {
        jinoforum_subscribe($cp->userid, $jinoforum->id);
    }
}

// Functions to do with read tracking.

/**
 * Mark posts as read.
 *
 * @global object
 * @global object
 * @param object $user object
 * @param array $postids array of post ids
 * @return boolean success
 */
function jinoforum_tp_mark_posts_read($user, $postids) {
    global $CFG, $DB;

    if (!jinoforum_tp_can_track_jinoforums(false, $user)) {
        return true;
    }

    $status = true;

    $now = time();
    $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 3600);

    if (empty($postids)) {
        return true;
    } else if (count($postids) > 200) {
        while ($part = array_splice($postids, 0, 200)) {
            $status = jinoforum_tp_mark_posts_read($user, $part) && $status;
        }
        return $status;
    }

    list($usql, $params) = $DB->get_in_or_equal($postids);
    $params[] = $user->id;

    $sql = "SELECT id
              FROM {jinoforum_read}
             WHERE postid $usql AND userid = ?";
    if ($existing = $DB->get_records_sql($sql, $params)) {
        $existing = array_keys($existing);
    } else {
        $existing = array();
    }

    $new = array_diff($postids, $existing);

    if ($new) {
        list($usql, $new_params) = $DB->get_in_or_equal($new);
        $params = array($user->id, $now, $now, $user->id);
        $params = array_merge($params, $new_params);
        $params[] = $cutoffdate;

        if ($CFG->jinoforum_allowforcedreadtracking) {
            $trackingsql = "AND (f.trackingtype = " . JINOFORUM_TRACKING_FORCED . "
                            OR (f.trackingtype = " . JINOFORUM_TRACKING_OPTIONAL . " AND tf.id IS NULL))";
        } else {
            $trackingsql = "AND ((f.trackingtype = " . JINOFORUM_TRACKING_OPTIONAL . "  OR f.trackingtype = " . JINOFORUM_TRACKING_FORCED . ")
                                AND tf.id IS NULL)";
        }

        $sql = "INSERT INTO {jinoforum_read} (userid, postid, discussionid, jinoforumid, firstread, lastread)

                SELECT ?, p.id, p.discussion, d.jinoforum, ?, ?
                  FROM {jinoforum_posts} p
                       JOIN {jinoforum_discussions} d       ON d.id = p.discussion
                       JOIN {jinoforum} f                   ON f.id = d.jinoforum
                       LEFT JOIN {jinoforum_track_prefs} tf ON (tf.userid = ? AND tf.jinoforumid = f.id)
                 WHERE p.id $usql
                       AND p.modified >= ?
                       $trackingsql";
        $status = $DB->execute($sql, $params) && $status;
    }

    if ($existing) {
        list($usql, $new_params) = $DB->get_in_or_equal($existing);
        $params = array($now, $user->id);
        $params = array_merge($params, $new_params);

        $sql = "UPDATE {jinoforum_read}
                   SET lastread = ?
                 WHERE userid = ? AND postid $usql";
        $status = $DB->execute($sql, $params) && $status;
    }

    return $status;
}

/**
 * Mark post as read.
 * @global object
 * @global object
 * @param int $userid
 * @param int $postid
 */
function jinoforum_tp_add_read_record($userid, $postid) {
    global $CFG, $DB;

    $now = time();
    $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 3600);

    if (!$DB->record_exists('jinoforum_read', array('userid' => $userid, 'postid' => $postid))) {
        $sql = "INSERT INTO {jinoforum_read} (userid, postid, discussionid, jinoforumid, firstread, lastread)

                SELECT ?, p.id, p.discussion, d.jinoforum, ?, ?
                  FROM {jinoforum_posts} p
                       JOIN {jinoforum_discussions} d ON d.id = p.discussion
                 WHERE p.id = ? AND p.modified >= ?";
        return $DB->execute($sql, array($userid, $now, $now, $postid, $cutoffdate));
    } else {
        $sql = "UPDATE {jinoforum_read}
                   SET lastread = ?
                 WHERE userid = ? AND postid = ?";
        return $DB->execute($sql, array($now, $userid, $userid));
    }
}

/**
 * Returns all records in the 'jinoforum_read' table matching the passed keys, indexed
 * by userid.
 *
 * @global object
 * @param int $userid
 * @param int $postid
 * @param int $discussionid
 * @param int $jinoforumid
 * @return array
 */
function jinoforum_tp_get_read_records($userid = -1, $postid = -1, $discussionid = -1, $jinoforumid = -1) {
    global $DB;
    $select = '';
    $params = array();

    if ($userid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'userid = ?';
        $params[] = $userid;
    }
    if ($postid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'postid = ?';
        $params[] = $postid;
    }
    if ($discussionid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'discussionid = ?';
        $params[] = $discussionid;
    }
    if ($jinoforumid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'jinoforumid = ?';
        $params[] = $jinoforumid;
    }

    return $DB->get_records_select('jinoforum_read', $select, $params);
}

/**
 * Returns all read records for the provided user and discussion, indexed by postid.
 *
 * @global object
 * @param inti $userid
 * @param int $discussionid
 */
function jinoforum_tp_get_discussion_read_records($userid, $discussionid) {
    global $DB;
    $select = 'userid = ? AND discussionid = ?';
    $fields = 'postid, firstread, lastread';
    return $DB->get_records_select('jinoforum_read', $select, array($userid, $discussionid), '', $fields);
}

/**
 * If its an old post, do nothing. If the record exists, the maintenance will clear it up later.
 *
 * @return bool
 */
function jinoforum_tp_mark_post_read($userid, $post, $jinoforumid) {
    if (!jinoforum_tp_is_post_old($post)) {
        return jinoforum_tp_add_read_record($userid, $post->id);
    } else {
        return true;
    }
}

/**
 * Marks a whole jinoforum as read, for a given user
 *
 * @global object
 * @global object
 * @param object $user
 * @param int $jinoforumid
 * @param int|bool $groupid
 * @return bool
 */
function jinoforum_tp_mark_jinoforum_read($user, $jinoforumid, $groupid = false) {
    global $CFG, $DB;

    $cutoffdate = time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);

    $groupsel = "";
    $params = array($user->id, $jinoforumid, $cutoffdate);

    if ($groupid !== false) {
        $groupsel = " AND (d.groupid = ? OR d.groupid = -1)";
        $params[] = $groupid;
    }

    $sql = "SELECT p.id
              FROM {jinoforum_posts} p
                   LEFT JOIN {jinoforum_discussions} d ON d.id = p.discussion
                   LEFT JOIN {jinoforum_read} r        ON (r.postid = p.id AND r.userid = ?)
             WHERE d.jinoforum = ?
                   AND p.modified >= ? AND r.id is NULL
                   $groupsel";

    if ($posts = $DB->get_records_sql($sql, $params)) {
        $postids = array_keys($posts);
        return jinoforum_tp_mark_posts_read($user, $postids);
    }

    return true;
}

/**
 * Marks a whole discussion as read, for a given user
 *
 * @global object
 * @global object
 * @param object $user
 * @param int $discussionid
 * @return bool
 */
function jinoforum_tp_mark_discussion_read($user, $discussionid) {
    global $CFG, $DB;

    $cutoffdate = time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);

    $sql = "SELECT p.id
              FROM {jinoforum_posts} p
                   LEFT JOIN {jinoforum_read} r ON (r.postid = p.id AND r.userid = ?)
             WHERE p.discussion = ?
                   AND p.modified >= ? AND r.id is NULL";

    if ($posts = $DB->get_records_sql($sql, array($user->id, $discussionid, $cutoffdate))) {
        $postids = array_keys($posts);
        return jinoforum_tp_mark_posts_read($user, $postids);
    }

    return true;
}

/**
 * @global object
 * @param int $userid
 * @param object $post
 */
function jinoforum_tp_is_post_read($userid, $post) {
    global $DB;
    return (jinoforum_tp_is_post_old($post) ||
            $DB->record_exists('jinoforum_read', array('userid' => $userid, 'postid' => $post->id)));
}

/**
 * @global object
 * @param object $post
 * @param int $time Defautls to time()
 */
function jinoforum_tp_is_post_old($post, $time = null) {
    global $CFG;

    if (is_null($time)) {
        $time = time();
    }
    return ($post->modified < ($time - ($CFG->jinoforum_oldpostdays * 24 * 3600)));
}

/**
 * Returns the count of records for the provided user and discussion.
 *
 * @global object
 * @global object
 * @param int $userid
 * @param int $discussionid
 * @return bool
 */
function jinoforum_tp_count_discussion_read_records($userid, $discussionid) {
    global $CFG, $DB;

    $cutoffdate = isset($CFG->jinoforum_oldpostdays) ? (time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60)) : 0;

    $sql = 'SELECT COUNT(DISTINCT p.id) ' .
            'FROM {jinoforum_discussions} d ' .
            'LEFT JOIN {jinoforum_read} r ON d.id = r.discussionid AND r.userid = ? ' .
            'LEFT JOIN {jinoforum_posts} p ON p.discussion = d.id ' .
            'AND (p.modified < ? OR p.id = r.postid) ' .
            'WHERE d.id = ? ';

    return ($DB->count_records_sql($sql, array($userid, $cutoffdate, $discussionid)));
}

/**
 * Returns the count of records for the provided user and discussion.
 *
 * @global object
 * @global object
 * @param int $userid
 * @param int $discussionid
 * @return int
 */
function jinoforum_tp_count_discussion_unread_posts($userid, $discussionid) {
    global $CFG, $DB;

    $cutoffdate = isset($CFG->jinoforum_oldpostdays) ? (time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60)) : 0;

    $sql = 'SELECT COUNT(p.id) ' .
            'FROM {jinoforum_posts} p ' .
            'LEFT JOIN {jinoforum_read} r ON r.postid = p.id AND r.userid = ? ' .
            'WHERE p.discussion = ? ' .
            'AND p.modified >= ? AND r.id is NULL';

    return $DB->count_records_sql($sql, array($userid, $discussionid, $cutoffdate));
}

/**
 * Returns the count of posts for the provided jinoforum and [optionally] group.
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int|bool $groupid
 * @return int
 */
function jinoforum_tp_count_jinoforum_posts($jinoforumid, $groupid = false) {
    global $CFG, $DB;
    $params = array($jinoforumid);
    $sql = 'SELECT COUNT(*) ' .
            'FROM {jinoforum_posts} fp,{jinoforum_discussions} fd ' .
            'WHERE fd.jinoforum = ? AND fp.discussion = fd.id';
    if ($groupid !== false) {
        $sql .= ' AND (fd.groupid = ? OR fd.groupid = -1)';
        $params[] = $groupid;
    }
    $count = $DB->count_records_sql($sql, $params);


    return $count;
}

/**
 * Returns the count of records for the provided user and jinoforum and [optionally] group.
 * @global object
 * @global object
 * @param int $userid
 * @param int $jinoforumid
 * @param int|bool $groupid
 * @return int
 */
function jinoforum_tp_count_jinoforum_read_records($userid, $jinoforumid, $groupid = false) {
    global $CFG, $DB;

    $cutoffdate = time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);

    $groupsel = '';
    $params = array($userid, $jinoforumid, $cutoffdate);
    if ($groupid !== false) {
        $groupsel = "AND (d.groupid = ? OR d.groupid = -1)";
        $params[] = $groupid;
    }

    $sql = "SELECT COUNT(p.id)
              FROM  {jinoforum_posts} p
                    JOIN {jinoforum_discussions} d ON d.id = p.discussion
                    LEFT JOIN {jinoforum_read} r   ON (r.postid = p.id AND r.userid= ?)
              WHERE d.jinoforum = ?
                    AND (p.modified < $cutoffdate OR (p.modified >= ? AND r.id IS NOT NULL))
                    $groupsel";

    return $DB->get_field_sql($sql, $params);
}

/**
 * Returns the count of records for the provided user and course.
 * Please note that group access is ignored!
 *
 * @global object
 * @global object
 * @param int $userid
 * @param int $courseid
 * @return array
 */
function jinoforum_tp_get_course_unread_posts($userid, $courseid) {
    global $CFG, $DB;

    $now = round(time(), -2); // DB cache friendliness.
    $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);
    $params = array($userid, $userid, $courseid, $cutoffdate, $userid);

    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
        $params[] = $now;
        $params[] = $now;
    } else {
        $timedsql = "";
    }

    if ($CFG->jinoforum_allowforcedreadtracking) {
        $trackingsql = "AND (f.trackingtype = " . JINOFORUM_TRACKING_FORCED . "
                            OR (f.trackingtype = " . JINOFORUM_TRACKING_OPTIONAL . " AND tf.id IS NULL
                                AND (SELECT trackforums FROM {user} WHERE id = ?) = 1))";
    } else {
        $trackingsql = "AND ((f.trackingtype = " . JINOFORUM_TRACKING_OPTIONAL . " OR f.trackingtype = " . JINOFORUM_TRACKING_FORCED . ")
                            AND tf.id IS NULL
                            AND (SELECT trackforums FROM {user} WHERE id = ?) = 1)";
    }

    $sql = "SELECT f.id, COUNT(p.id) AS unread
              FROM {jinoforum_posts} p
                   JOIN {jinoforum_discussions} d       ON d.id = p.discussion
                   JOIN {jinoforum} f                   ON f.id = d.jinoforum
                   JOIN {course} c                  ON c.id = f.course
                   LEFT JOIN {jinoforum_read} r         ON (r.postid = p.id AND r.userid = ?)
                   LEFT JOIN {jinoforum_track_prefs} tf ON (tf.userid = ? AND tf.jinoforumid = f.id)
             WHERE f.course = ?
                   AND p.modified >= ? AND r.id is NULL
                   $trackingsql
                   $timedsql
          GROUP BY f.id";

    if ($return = $DB->get_records_sql($sql, $params)) {
        return $return;
    }

    return array();
}

/**
 * Returns the count of records for the provided user and jinoforum and [optionally] group.
 *
 * @global object
 * @global object
 * @global object
 * @param object $cm
 * @param object $course
 * @return int
 */
function jinoforum_tp_count_jinoforum_unread_posts($cm, $course) {
    global $CFG, $USER, $DB;

    static $readcache = array();

    $jinoforumid = $cm->instance;

    if (!isset($readcache[$course->id])) {
        $readcache[$course->id] = array();
        if ($counts = jinoforum_tp_get_course_unread_posts($USER->id, $course->id)) {
            foreach ($counts as $count) {
                $readcache[$course->id][$count->id] = $count->unread;
            }
        }
    }

    if (empty($readcache[$course->id][$jinoforumid])) {
        // no need to check group mode ;-)
        return 0;
    }

    $groupmode = groups_get_activity_groupmode($cm, $course);

    if ($groupmode != SEPARATEGROUPS) {
        return $readcache[$course->id][$jinoforumid];
    }

    if (has_capability('moodle/site:accessallgroups', context_module::instance($cm->id))) {
        return $readcache[$course->id][$jinoforumid];
    }

    require_once($CFG->dirroot . '/course/lib.php');

    $modinfo = get_fast_modinfo($course);

    $mygroups = $modinfo->get_groups($cm->groupingid);

    // add all groups posts
    $mygroups[-1] = -1;

    list ($groups_sql, $groups_params) = $DB->get_in_or_equal($mygroups);

    $now = round(time(), -2); // db cache friendliness
    $cutoffdate = $now - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);
    $params = array($USER->id, $jinoforumid, $cutoffdate);

    if (!empty($CFG->jinoforum_enabletimedposts)) {
        $timedsql = "AND d.timestart < ? AND (d.timeend = 0 OR d.timeend > ?)";
        $params[] = $now;
        $params[] = $now;
    } else {
        $timedsql = "";
    }

    $params = array_merge($params, $groups_params);

    $sql = "SELECT COUNT(p.id)
              FROM {jinoforum_posts} p
                   JOIN {jinoforum_discussions} d ON p.discussion = d.id
                   LEFT JOIN {jinoforum_read} r   ON (r.postid = p.id AND r.userid = ?)
             WHERE d.jinoforum = ?
                   AND p.modified >= ? AND r.id is NULL
                   $timedsql
                   AND d.groupid $groups_sql";

    return $DB->get_field_sql($sql, $params);
}

/**
 * Deletes read records for the specified index. At least one parameter must be specified.
 *
 * @global object
 * @param int $userid
 * @param int $postid
 * @param int $discussionid
 * @param int $jinoforumid
 * @return bool
 */
function jinoforum_tp_delete_read_records($userid = -1, $postid = -1, $discussionid = -1, $jinoforumid = -1) {
    global $DB;
    $params = array();

    $select = '';
    if ($userid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'userid = ?';
        $params[] = $userid;
    }
    if ($postid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'postid = ?';
        $params[] = $postid;
    }
    if ($discussionid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'discussionid = ?';
        $params[] = $discussionid;
    }
    if ($jinoforumid > -1) {
        if ($select != '')
            $select .= ' AND ';
        $select .= 'jinoforumid = ?';
        $params[] = $jinoforumid;
    }
    if ($select == '') {
        return false;
    } else {
        return $DB->delete_records_select('jinoforum_read', $select, $params);
    }
}

/**
 * Get a list of jinoforums not tracked by the user.
 *
 * @global object
 * @global object
 * @param int $userid The id of the user to use.
 * @param int $courseid The id of the course being checked.
 * @return mixed An array indexed by jinoforum id, or false.
 */
function jinoforum_tp_get_untracked_jinoforums($userid, $courseid) {
    global $CFG, $DB;

    if ($CFG->jinoforum_allowforcedreadtracking) {
        $trackingsql = "AND (f.trackingtype = " . JINOFORUM_TRACKING_OFF . "
                            OR (f.trackingtype = " . JINOFORUM_TRACKING_OPTIONAL . " AND (ft.id IS NOT NULL
                                OR (SELECT trackforums FROM {user} WHERE id = ?) = 0)))";
    } else {
        $trackingsql = "AND (f.trackingtype = " . JINOFORUM_TRACKING_OFF . "
                            OR ((f.trackingtype = " . JINOFORUM_TRACKING_OPTIONAL . " OR f.trackingtype = " . JINOFORUM_TRACKING_FORCED . ")
                                AND (ft.id IS NOT NULL
                                    OR (SELECT trackforums FROM {user} WHERE id = ?) = 0)))";
    }

    $sql = "SELECT f.id
              FROM {jinoforum} f
                   LEFT JOIN {jinoforum_track_prefs} ft ON (ft.jinoforumid = f.id AND ft.userid = ?)
             WHERE f.course = ?
                   $trackingsql";

    if ($jinoforums = $DB->get_records_sql($sql, array($userid, $courseid, $userid))) {
        foreach ($jinoforums as $jinoforum) {
            $jinoforums[$jinoforum->id] = $jinoforum;
        }
        return $jinoforums;
    } else {
        return array();
    }
}

/**
 * Determine if a user can track jinoforums and optionally a particular jinoforum.
 * Checks the site settings, the user settings and the jinoforum settings (if
 * requested).
 *
 * @global object
 * @global object
 * @global object
 * @param mixed $jinoforum The jinoforum object to test, or the int id (optional).
 * @param mixed $userid The user object to check for (optional).
 * @return boolean
 */
function jinoforum_tp_can_track_jinoforums($jinoforum = false, $user = false) {
    global $USER, $CFG, $DB;

    // if possible, avoid expensive
    // queries
    if (empty($CFG->jinoforum_trackreadposts)) {
        return false;
    }

    if ($user === false) {
        $user = $USER;
    }

    if (isguestuser($user) or empty($user->id)) {
        return false;
    }

    if ($jinoforum === false) {
        if ($CFG->jinoforum_allowforcedreadtracking) {
            // Since we can force tracking, assume yes without a specific jinoforum.
            return true;
        } else {
            return (bool) (isset($user->trackforums)) ? $user->trackforums : "";
        }
    }

    // Work toward always passing an object...
    if (is_numeric($jinoforum)) {
        debugging('Better use proper jinoforum object.', DEBUG_DEVELOPER);
        $jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum), '', 'id,trackingtype');
    }

    $jinoforumallows = ($jinoforum->trackingtype == JINOFORUM_TRACKING_OPTIONAL);
    $jinoforumforced = ($jinoforum->trackingtype == JINOFORUM_TRACKING_FORCED);

    if ($CFG->jinoforum_allowforcedreadtracking) {
        // If we allow forcing, then forced jinoforums takes procidence over user setting.
        return ($jinoforumforced || ($jinoforumallows && (!empty($user->trackforums) && (bool) $user->trackforums)));
    } else {
        // If we don't allow forcing, user setting trumps.
        return ($jinoforumforced || $jinoforumallows) && !empty($user->trackforums);
    }
}

/**
 * Tells whether a specific jinoforum is tracked by the user. A user can optionally
 * be specified. If not specified, the current user is assumed.
 *
 * @global object
 * @global object
 * @global object
 * @param mixed $jinoforum If int, the id of the jinoforum being checked; if object, the jinoforum object
 * @param int $userid The id of the user being checked (optional).
 * @return boolean
 */
function jinoforum_tp_is_tracked($jinoforum, $user = false) {
    global $USER, $CFG, $DB;

    if ($user === false) {
        $user = $USER;
    }

    if (isguestuser($user) or empty($user->id)) {
        return false;
    }

    // Work toward always passing an object...
    if (is_numeric($jinoforum)) {
        debugging('Better use proper jinoforum object.', DEBUG_DEVELOPER);
        $jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum));
    }

    if (!jinoforum_tp_can_track_jinoforums($jinoforum, $user)) {
        return false;
    }

    $jinoforumallows = ($jinoforum->trackingtype == JINOFORUM_TRACKING_OPTIONAL);
    $jinoforumforced = ($jinoforum->trackingtype == JINOFORUM_TRACKING_FORCED);
    $userpref = $DB->get_record('jinoforum_track_prefs', array('userid' => $user->id, 'jinoforumid' => $jinoforum->id));

    if ($CFG->jinoforum_allowforcedreadtracking) {
        return $jinoforumforced || ($jinoforumallows && $userpref === false);
    } else {
        return ($jinoforumallows || $jinoforumforced) && $userpref === false;
    }
}

/**
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int $userid
 */
function jinoforum_tp_start_tracking($jinoforumid, $userid = false) {
    global $USER, $DB;

    if ($userid === false) {
        $userid = $USER->id;
    }

    return $DB->delete_records('jinoforum_track_prefs', array('userid' => $userid, 'jinoforumid' => $jinoforumid));
}

/**
 * @global object
 * @global object
 * @param int $jinoforumid
 * @param int $userid
 */
function jinoforum_tp_stop_tracking($jinoforumid, $userid = false) {
    global $USER, $DB;

    if ($userid === false) {
        $userid = $USER->id;
    }

    if (!$DB->record_exists('jinoforum_track_prefs', array('userid' => $userid, 'jinoforumid' => $jinoforumid))) {
        $track_prefs = new stdClass();
        $track_prefs->userid = $userid;
        $track_prefs->jinoforumid = $jinoforumid;
        $DB->insert_record('jinoforum_track_prefs', $track_prefs);
    }

    return jinoforum_tp_delete_read_records($userid, -1, -1, $jinoforumid);
}

/**
 * Clean old records from the jinoforum_read table.
 * @global object
 * @global object
 * @return void
 */
function jinoforum_tp_clean_read_records() {
    global $CFG, $DB;

    if (!isset($CFG->jinoforum_oldpostdays)) {
        return;
    }
// Look for records older than the cutoffdate that are still in the jinoforum_read table.
    $cutoffdate = time() - ($CFG->jinoforum_oldpostdays * 24 * 60 * 60);

    //first get the oldest tracking present - we need tis to speedup the next delete query
    $sql = "SELECT MIN(fp.modified) AS first
              FROM {jinoforum_posts} fp
                   JOIN {jinoforum_read} fr ON fr.postid=fp.id";
    if (!$first = $DB->get_field_sql($sql)) {
        // nothing to delete;
        return;
    }

    // now delete old tracking info
    $sql = "DELETE
              FROM {jinoforum_read}
             WHERE postid IN (SELECT fp.id
                                FROM {jinoforum_posts} fp
                               WHERE fp.modified >= ? AND fp.modified < ?)";
    $DB->execute($sql, array($first, $cutoffdate));
}

/**
 * Sets the last post for a given discussion
 *
 * @global object
 * @global object
 * @param into $discussionid
 * @return bool|int
 * */
function jinoforum_discussion_update_last_post($discussionid) {
    global $CFG, $DB;

// Check the given discussion exists
    if (!$DB->record_exists('jinoforum_discussions', array('id' => $discussionid))) {
        return false;
    }

// Use SQL to find the last post for this discussion
    $sql = "SELECT id, userid, modified
              FROM {jinoforum_posts}
             WHERE discussion=?
             ORDER BY modified DESC";

// Lets go find the last post
    if (($lastposts = $DB->get_records_sql($sql, array($discussionid), 0, 1))) {
        $lastpost = reset($lastposts);
        $discussionobject = new stdClass();
        $discussionobject->id = $discussionid;
        $discussionobject->usermodified = $lastpost->userid;
        $discussionobject->timemodified = $lastpost->modified;
        $DB->update_record('jinoforum_discussions', $discussionobject);
        return $lastpost->id;
    }

// To get here either we couldn't find a post for the discussion (weird)
// or we couldn't update the discussion record (weird x2)
    return false;
}

/**
 * List the actions that correspond to a view of this module.
 * This is used by the participation report.
 *
 * Note: This is not used by new logging system. Event with
 *       crud = 'r' and edulevel = LEVEL_PARTICIPATING will
 *       be considered as view action.
 *
 * @return array
 */
function jinoforum_get_view_actions() {
    return array('view discussion', 'search', 'jinoforum', 'jinoforums', 'subscribers', 'view jinoforum');
}

/**
 * List the actions that correspond to a post of this module.
 * This is used by the participation report.
 *
 * Note: This is not used by new logging system. Event with
 *       crud = ('c' || 'u' || 'd') and edulevel = LEVEL_PARTICIPATING
 *       will be considered as post action.
 *
 * @return array
 */
function jinoforum_get_post_actions() {
    return array('add discussion', 'add post', 'delete discussion', 'delete post', 'move discussion', 'prune post', 'update post');
}

/**
 * Returns a warning object if a user has reached the number of posts equal to
 * the warning/blocking setting, or false if there is no warning to show.
 *
 * @param int|stdClass $jinoforum the jinoforum id or the jinoforum object
 * @param stdClass $cm the course module
 * @return stdClass|bool returns an object with the warning information, else
 *         returns false if no warning is required.
 */
function jinoforum_check_throttling($jinoforum, $cm = null) {
    global $CFG, $DB, $USER;

    if (is_numeric($jinoforum)) {
        $jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum), '*', MUST_EXIST);
    }

    if (!is_object($jinoforum)) {
        return false; // This is broken.
    }

    if (!$cm) {
        $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $jinoforum->course, false, MUST_EXIST);
    }

    if (empty($jinoforum->blockafter)) {
        return false;
    }

    if (empty($jinoforum->blockperiod)) {
        return false;
    }

    $modcontext = context_module::instance($cm->id);
    if (has_capability('mod/jinoforum:postwithoutthrottling', $modcontext)) {
        return false;
    }

    // Get the number of posts in the last period we care about.
    $timenow = time();
    $timeafter = $timenow - $jinoforum->blockperiod;
    $numposts = $DB->count_records_sql('SELECT COUNT(p.id) FROM {jinoforum_posts} p
                                        JOIN {jinoforum_discussions} d
                                        ON p.discussion = d.id WHERE d.jinoforum = ?
                                        AND p.userid = ? AND p.created > ?', array($jinoforum->id, $USER->id, $timeafter));

    $a = new stdClass();
    $a->blockafter = $jinoforum->blockafter;
    $a->numposts = $numposts;
    $a->blockperiod = get_string('secondstotime' . $jinoforum->blockperiod);

    if ($jinoforum->blockafter <= $numposts) {
        $warning = new stdClass();
        $warning->canpost = false;
        $warning->errorcode = 'jinoforumblockingtoomanyposts';
        $warning->module = 'error';
        $warning->additional = $a;
        $warning->link = $CFG->wwwroot . '/mod/jinoforum/view.php?f=' . $jinoforum->id;

        return $warning;
    }

    if ($jinoforum->warnafter <= $numposts) {
        $warning = new stdClass();
        $warning->canpost = true;
        $warning->errorcode = 'jinoforumblockingalmosttoomanyposts';
        $warning->module = 'jinoforum';
        $warning->additional = $a;
        $warning->link = null;

        return $warning;
    }
}

/**
 * Throws an error if the user is no longer allowed to post due to having reached
 * or exceeded the number of posts specified in 'Post threshold for blocking'
 * setting.
 *
 * @since Moodle 2.5
 * @param stdClass $thresholdwarning the warning information returned
 *        from the function jinoforum_check_throttling.
 */
function jinoforum_check_blocking_threshold($thresholdwarning) {
    if (!empty($thresholdwarning) && !$thresholdwarning->canpost) {
        print_error($thresholdwarning->errorcode, $thresholdwarning->module, $thresholdwarning->link, $thresholdwarning->additional);
    }
}

/**
 * Removes all grades from gradebook
 *
 * @global object
 * @global object
 * @param int $courseid
 * @param string $type optional
 */
function jinoforum_reset_gradebook($courseid, $type = '') {
    global $CFG, $DB;

    $wheresql = '';
    $params = array($courseid);
    if ($type) {
        $wheresql = "AND f.type=?";
        $params[] = $type;
    }

    $sql = "SELECT f.*, cm.idnumber as cmidnumber, f.course as courseid
              FROM {jinoforum} f, {course_modules} cm, {modules} m
             WHERE m.name='jinoforum' AND m.id=cm.module AND cm.instance=f.id AND f.course=? $wheresql";

    if ($jinoforums = $DB->get_records_sql($sql, $params)) {
        foreach ($jinoforums as $jinoforum) {
            jinoforum_grade_item_update($jinoforum, 'reset');
        }
    }
}

/**
 * This function is used by the reset_course_userdata function in moodlelib.
 * This function will remove all posts from the specified jinoforum
 * and clean up any related data.
 *
 * @global object
 * @global object
 * @param $data the data submitted from the reset course.
 * @return array status array
 */
function jinoforum_reset_userdata($data) {
    global $CFG, $DB;
    require_once($CFG->dirroot . '/rating/lib.php');

    $componentstr = get_string('modulenameplural', 'jinoforum');
    $status = array();

    $params = array($data->courseid);

    $removeposts = false;
    $typesql = "";
    if (!empty($data->reset_jinoforum_all)) {
        $removeposts = true;
        $typesstr = get_string('resetjinoforumsall', 'jinoforum');
        $types = array();
    } else if (!empty($data->reset_jinoforum_types)) {
        $removeposts = true;
        $typesql = "";
        $types = array();
        $jinoforum_types_all = jinoforum_get_jinoforum_types_all();
        foreach ($data->reset_jinoforum_types as $type) {
            if (!array_key_exists($type, $jinoforum_types_all)) {
                continue;
            }
            $typesql .= " AND f.type=?";
            $types[] = $jinoforum_types_all[$type];
            $params[] = $type;
        }
        $typesstr = get_string('resetjinoforums', 'jinoforum') . ': ' . implode(', ', $types);
    }
    $alldiscussionssql = "SELECT fd.id
                            FROM {jinoforum_discussions} fd, {jinoforum} f
                           WHERE f.course=? AND f.id=fd.jinoforum";

    $alljinoforumssql = "SELECT f.id
                            FROM {jinoforum} f
                           WHERE f.course=?";

    $allpostssql = "SELECT fp.id
                            FROM {jinoforum_posts} fp, {jinoforum_discussions} fd, {jinoforum} f
                           WHERE f.course=? AND f.id=fd.jinoforum AND fd.id=fp.discussion";

    $jinoforumssql = $jinoforums = $rm = null;

    if ($removeposts || !empty($data->reset_jinoforum_ratings)) {
        $jinoforumssql = "$alljinoforumssql $typesql";
        $jinoforums = $jinoforums = $DB->get_records_sql($jinoforumssql, $params);
        $rm = new rating_manager();
        $ratingdeloptions = new stdClass;
        $ratingdeloptions->component = 'mod_jinoforum';
        $ratingdeloptions->ratingarea = 'post';
    }

    if ($removeposts) {
        $discussionssql = "$alldiscussionssql $typesql";
        $postssql = "$allpostssql $typesql";

        // now get rid of all attachments
        $fs = get_file_storage();
        if ($jinoforums) {
            foreach ($jinoforums as $jinoforumid => $unused) {
                if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforumid)) {
                    continue;
                }
                $context = context_module::instance($cm->id);
                $fs->delete_area_files($context->id, 'mod_jinoforum', 'attachment');
                $fs->delete_area_files($context->id, 'mod_jinoforum', 'post');

                //remove ratings
                $ratingdeloptions->contextid = $context->id;
                $rm->delete_ratings($ratingdeloptions);
            }
        }

        // first delete all read flags
        $DB->delete_records_select('jinoforum_read', "jinoforumid IN ($jinoforumssql)", $params);

        // remove tracking prefs
        $DB->delete_records_select('jinoforum_track_prefs', "jinoforumid IN ($jinoforumssql)", $params);

        // remove posts from queue
        $DB->delete_records_select('jinoforum_queue', "discussionid IN ($discussionssql)", $params);

        // all posts - initial posts must be kept in single simple discussion jinoforums
        $DB->delete_records_select('jinoforum_posts', "discussion IN ($discussionssql) AND parent <> 0", $params); // first all children
        $DB->delete_records_select('jinoforum_posts', "discussion IN ($discussionssql AND f.type <> 'single') AND parent = 0", $params); // now the initial posts for non single simple
        // finally all discussions except single simple jinoforums
        $DB->delete_records_select('jinoforum_discussions', "jinoforum IN ($jinoforumssql AND f.type <> 'single')", $params);

        // remove all grades from gradebook
        if (empty($data->reset_gradebook_grades)) {
            if (empty($types)) {
                jinoforum_reset_gradebook($data->courseid);
            } else {
                foreach ($types as $type) {
                    jinoforum_reset_gradebook($data->courseid, $type);
                }
            }
        }

        $status[] = array('component' => $componentstr, 'item' => $typesstr, 'error' => false);
    }

    // remove all ratings in this course's jinoforums
    if (!empty($data->reset_jinoforum_ratings)) {
        if ($jinoforums) {
            foreach ($jinoforums as $jinoforumid => $unused) {
                if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforumid)) {
                    continue;
                }
                $context = context_module::instance($cm->id);

                //remove ratings
                $ratingdeloptions->contextid = $context->id;
                $rm->delete_ratings($ratingdeloptions);
            }
        }

        // remove all grades from gradebook
        if (empty($data->reset_gradebook_grades)) {
            jinoforum_reset_gradebook($data->courseid);
        }
    }

    // remove all digest settings unconditionally - even for users still enrolled in course.
    if (!empty($data->reset_jinoforum_digests)) {
        $DB->delete_records_select('jinoforum_digests', "jinoforum IN ($alljinoforumssql)", $params);
        $status[] = array('component' => $componentstr, 'item' => get_string('resetdigests', 'jinoforum'), 'error' => false);
    }

    // remove all subscriptions unconditionally - even for users still enrolled in course
    if (!empty($data->reset_jinoforum_subscriptions)) {
        $DB->delete_records_select('jinoforum_subscriptions', "jinoforum IN ($alljinoforumssql)", $params);
        $status[] = array('component' => $componentstr, 'item' => get_string('resetsubscriptions', 'jinoforum'), 'error' => false);
    }

    // remove all tracking prefs unconditionally - even for users still enrolled in course
    if (!empty($data->reset_jinoforum_track_prefs)) {
        $DB->delete_records_select('jinoforum_track_prefs', "jinoforumid IN ($alljinoforumssql)", $params);
        $status[] = array('component' => $componentstr, 'item' => get_string('resettrackprefs', 'jinoforum'), 'error' => false);
    }

    /// updating dates - shift may be negative too
    if ($data->timeshift) {
        shift_course_mod_dates('jinoforum', array('assesstimestart', 'assesstimefinish'), $data->timeshift, $data->courseid);
        $status[] = array('component' => $componentstr, 'item' => get_string('datechanged'), 'error' => false);
    }

    return $status;
}

/**
 * Called by course/reset.php
 *
 * @param $mform form passed by reference
 */
function jinoforum_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'jinoforumheader', get_string('modulenameplural', 'jinoforum'));

    $mform->addElement('checkbox', 'reset_jinoforum_all', get_string('resetjinoforumsall', 'jinoforum'));

    $mform->addElement('select', 'reset_jinoforum_types', get_string('resetjinoforums', 'jinoforum'), jinoforum_get_jinoforum_types_all(), array('multiple' => 'multiple'));
    $mform->setAdvanced('reset_jinoforum_types');
    $mform->disabledIf('reset_jinoforum_types', 'reset_jinoforum_all', 'checked');

    $mform->addElement('checkbox', 'reset_jinoforum_digests', get_string('resetdigests', 'jinoforum'));
    $mform->setAdvanced('reset_jinoforum_digests');

    $mform->addElement('checkbox', 'reset_jinoforum_subscriptions', get_string('resetsubscriptions', 'jinoforum'));
    $mform->setAdvanced('reset_jinoforum_subscriptions');

    $mform->addElement('checkbox', 'reset_jinoforum_track_prefs', get_string('resettrackprefs', 'jinoforum'));
    $mform->setAdvanced('reset_jinoforum_track_prefs');
    $mform->disabledIf('reset_jinoforum_track_prefs', 'reset_jinoforum_all', 'checked');

    $mform->addElement('checkbox', 'reset_jinoforum_ratings', get_string('deleteallratings'));
    $mform->disabledIf('reset_jinoforum_ratings', 'reset_jinoforum_all', 'checked');
}

/**
 * Course reset form defaults.
 * @return array
 */
function jinoforum_reset_course_form_defaults($course) {
    return array('reset_jinoforum_all' => 1, 'reset_jinoforum_digests' => 0, 'reset_jinoforum_subscriptions' => 0, 'reset_jinoforum_track_prefs' => 0, 'reset_jinoforum_ratings' => 1);
}

/**
 * Converts a jinoforum to use the Roles System
 *
 * @global object
 * @global object
 * @param object $jinoforum        a jinoforum object with the same attributes as a record
 *                        from the jinoforum database table
 * @param int $jinoforummodid   the id of the jinoforum module, from the modules table
 * @param array $teacherroles array of roles that have archetype teacher
 * @param array $studentroles array of roles that have archetype student
 * @param array $guestroles   array of roles that have archetype guest
 * @param int $cmid         the course_module id for this jinoforum instance
 * @return boolean      jinoforum was converted or not
 */
function jinoforum_convert_to_roles($jinoforum, $jinoforummodid, $teacherroles = array(), $studentroles = array(), $guestroles = array(), $cmid = NULL) {

    global $CFG, $DB, $OUTPUT;

    if (!isset($jinoforum->open) && !isset($jinoforum->assesspublic)) {
        // We assume that this jinoforum has already been converted to use the
        // Roles System. Columns jinoforum.open and jinoforum.assesspublic get dropped
        // once the jinoforum module has been upgraded to use Roles.
        return false;
    }

    if ($jinoforum->type == 'teacher') {

        // Teacher jinoforums should be converted to normal jinoforums that
        // use the Roles System to implement the old behavior.
        // Note:
        //   Seems that teacher jinoforums were never backed up in 1.6 since they
        //   didn't have an entry in the course_modules table.
        require_once($CFG->dirroot . '/course/lib.php');

        if ($DB->count_records('jinoforum_discussions', array('jinoforum' => $jinoforum->id)) == 0) {
            // Delete empty teacher jinoforums.
            $DB->delete_records('jinoforum', array('id' => $jinoforum->id));
        } else {
            // Create a course module for the jinoforum and assign it to
            // section 0 in the course.
            $mod = new stdClass();
            $mod->course = $jinoforum->course;
            $mod->module = $jinoforummodid;
            $mod->instance = $jinoforum->id;
            $mod->section = 0;
            $mod->visible = 0;  // Hide the jinoforum
            $mod->visibleold = 0;  // Hide the jinoforum
            $mod->groupmode = 0;

            if (!$cmid = add_course_module($mod)) {
                print_error('cannotcreateinstanceforteacher', 'jinoforum');
            } else {
                $sectionid = course_add_cm_to_section($jinoforum->course, $mod->coursemodule, 0);
            }

            // Change the jinoforum type to general.
            $jinoforum->type = 'general';
            $DB->update_record('jinoforum', $jinoforum);

            $context = context_module::instance($cmid);

            // Create overrides for default student and guest roles (prevent).
            foreach ($studentroles as $studentrole) {
                assign_capability('mod/jinoforum:viewdiscussion', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:viewhiddentimedposts', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:startdiscussion', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:replypost', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:viewrating', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:viewanyrating', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:rate', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:createattachment', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:deleteownpost', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:deleteanypost', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:splitdiscussions', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:movediscussions', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:editanypost', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:viewqandawithoutposting', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:viewsubscribers', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:managesubscriptions', CAP_PREVENT, $studentrole->id, $context->id);
                assign_capability('mod/jinoforum:postwithoutthrottling', CAP_PREVENT, $studentrole->id, $context->id);
            }
            foreach ($guestroles as $guestrole) {
                assign_capability('mod/jinoforum:viewdiscussion', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:viewhiddentimedposts', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:startdiscussion', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:replypost', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:viewrating', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:viewanyrating', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:rate', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:createattachment', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:deleteownpost', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:deleteanypost', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:splitdiscussions', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:movediscussions', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:editanypost', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:viewqandawithoutposting', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:viewsubscribers', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:managesubscriptions', CAP_PREVENT, $guestrole->id, $context->id);
                assign_capability('mod/jinoforum:postwithoutthrottling', CAP_PREVENT, $guestrole->id, $context->id);
            }
        }
    } else {
        // Non-teacher jinoforum.

        if (empty($cmid)) {
            // We were not given the course_module id. Try to find it.
            if (!$cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id)) {
                echo $OUTPUT->notification('Could not get the course module for the jinoforum');
                return false;
            } else {
                $cmid = $cm->id;
            }
        }
        $context = context_module::instance($cmid);

        // $jinoforum->open defines what students can do:
        //   0 = No discussions, no replies
        //   1 = No discussions, but replies are allowed
        //   2 = Discussions and replies are allowed
        switch ($jinoforum->open) {
            case 0:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:startdiscussion', CAP_PREVENT, $studentrole->id, $context->id);
                    assign_capability('mod/jinoforum:replypost', CAP_PREVENT, $studentrole->id, $context->id);
                }
                break;
            case 1:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:startdiscussion', CAP_PREVENT, $studentrole->id, $context->id);
                    assign_capability('mod/jinoforum:replypost', CAP_ALLOW, $studentrole->id, $context->id);
                }
                break;
            case 2:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:startdiscussion', CAP_ALLOW, $studentrole->id, $context->id);
                    assign_capability('mod/jinoforum:replypost', CAP_ALLOW, $studentrole->id, $context->id);
                }
                break;
        }

        // $jinoforum->assessed defines whether jinoforum rating is turned
        // on (1 or 2) and who can rate posts:
        //   1 = Everyone can rate posts
        //   2 = Only teachers can rate posts
        switch ($jinoforum->assessed) {
            case 1:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:rate', CAP_ALLOW, $studentrole->id, $context->id);
                }
                foreach ($teacherroles as $teacherrole) {
                    assign_capability('mod/jinoforum:rate', CAP_ALLOW, $teacherrole->id, $context->id);
                }
                break;
            case 2:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:rate', CAP_PREVENT, $studentrole->id, $context->id);
                }
                foreach ($teacherroles as $teacherrole) {
                    assign_capability('mod/jinoforum:rate', CAP_ALLOW, $teacherrole->id, $context->id);
                }
                break;
        }

        // $jinoforum->assesspublic defines whether students can see
        // everybody's ratings:
        //   0 = Students can only see their own ratings
        //   1 = Students can see everyone's ratings
        switch ($jinoforum->assesspublic) {
            case 0:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:viewanyrating', CAP_PREVENT, $studentrole->id, $context->id);
                }
                foreach ($teacherroles as $teacherrole) {
                    assign_capability('mod/jinoforum:viewanyrating', CAP_ALLOW, $teacherrole->id, $context->id);
                }
                break;
            case 1:
                foreach ($studentroles as $studentrole) {
                    assign_capability('mod/jinoforum:viewanyrating', CAP_ALLOW, $studentrole->id, $context->id);
                }
                foreach ($teacherroles as $teacherrole) {
                    assign_capability('mod/jinoforum:viewanyrating', CAP_ALLOW, $teacherrole->id, $context->id);
                }
                break;
        }

        if (empty($cm)) {
            $cm = $DB->get_record('course_modules', array('id' => $cmid));
        }

        // $cm->groupmode:
        // 0 - No groups
        // 1 - Separate groups
        // 2 - Visible groups
        switch ($cm->groupmode) {
            case 0:
                break;
            case 1:
                foreach ($studentroles as $studentrole) {
                    assign_capability('moodle/site:accessallgroups', CAP_PREVENT, $studentrole->id, $context->id);
                }
                foreach ($teacherroles as $teacherrole) {
                    assign_capability('moodle/site:accessallgroups', CAP_ALLOW, $teacherrole->id, $context->id);
                }
                break;
            case 2:
                foreach ($studentroles as $studentrole) {
                    assign_capability('moodle/site:accessallgroups', CAP_ALLOW, $studentrole->id, $context->id);
                }
                foreach ($teacherroles as $teacherrole) {
                    assign_capability('moodle/site:accessallgroups', CAP_ALLOW, $teacherrole->id, $context->id);
                }
                break;
        }
    }
    return true;
}

/**
 * Returns array of jinoforum layout modes
 *
 * @return array
 */
function jinoforum_get_layout_modes() {
    return array(JINOFORUM_MODE_FLATOLDEST => get_string('modeflatoldestfirst', 'jinoforum'),
        JINOFORUM_MODE_FLATNEWEST => get_string('modeflatnewestfirst', 'jinoforum'),
        JINOFORUM_MODE_THREADED => get_string('modethreaded', 'jinoforum'),
        JINOFORUM_MODE_NESTED => get_string('modenested', 'jinoforum'));
}

/**
 * Returns array of jinoforum types chooseable on the jinoforum editing form
 *
 * @return array
 */
function jinoforum_get_jinoforum_types() {
    return array('general' => get_string('generaljinoforum', 'jinoforum'),
        'eachuser' => get_string('eachuserjinoforum', 'jinoforum'),
        'single' => get_string('singlejinoforum', 'jinoforum'),
        'qanda' => get_string('qandajinoforum', 'jinoforum'),
        'blog' => get_string('blogjinoforum', 'jinoforum'));
}

/**
 * Returns array of all jinoforum layout modes
 *
 * @return array
 */
function jinoforum_get_jinoforum_types_all() {
    return array('news' => get_string('namenews', 'jinoforum'),
        'social' => get_string('namesocial', 'jinoforum'),
        'general' => get_string('generaljinoforum', 'jinoforum'),
        'eachuser' => get_string('eachuserjinoforum', 'jinoforum'),
        'single' => get_string('singlejinoforum', 'jinoforum'),
        'qanda' => get_string('qandajinoforum', 'jinoforum'),
        'blog' => get_string('blogjinoforum', 'jinoforum'));
}

/**
 * Returns array of jinoforum open modes
 *
 * @return array
 */
function jinoforum_get_open_modes() {
    return array('2' => get_string('openmode2', 'jinoforum'),
        '1' => get_string('openmode1', 'jinoforum'),
        '0' => get_string('openmode0', 'jinoforum'));
}

/**
 * Returns all other caps used in module
 *
 * @return array
 */
function jinoforum_get_extra_capabilities() {
    return array('moodle/site:accessallgroups', 'moodle/site:viewfullnames', 'moodle/site:trustcontent', 'moodle/rating:view', 'moodle/rating:viewany', 'moodle/rating:viewall', 'moodle/rating:rate');
}

/**
 * Adds module specific settings to the settings block
 *
 * @param settings_navigation $settings The settings navigation object
 * @param navigation_node $jinoforumnode The node to add module settings to
 */
function jinoforum_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $jinoforumnode) {
    global $USER, $PAGE, $CFG, $DB, $OUTPUT;

    $jinoforumobject = $DB->get_record("jinoforum", array("id" => $PAGE->cm->instance));
    if (empty($PAGE->cm->context)) {
        $PAGE->cm->context = context_module::instance($PAGE->cm->instance);
    }

    // for some actions you need to be enrolled, beiing admin is not enough sometimes here
    $enrolled = is_enrolled($PAGE->cm->context, $USER, '', false);
    $activeenrolled = is_enrolled($PAGE->cm->context, $USER, '', true);

    $canmanage = has_capability('mod/jinoforum:managesubscriptions', $PAGE->cm->context);
    $subscriptionmode = jinoforum_get_forcesubscribed($jinoforumobject);
    $cansubscribe = ($activeenrolled && $subscriptionmode != JINOFORUM_FORCESUBSCRIBE && ($subscriptionmode != JINOFORUM_DISALLOWSUBSCRIBE || $canmanage));

    if ($canmanage) {
        $mode = $jinoforumnode->add(get_string('subscriptionmode', 'jinoforum'), null, navigation_node::TYPE_CONTAINER);

        $allowchoice = $mode->add(get_string('subscriptionoptional', 'jinoforum'), new moodle_url('/mod/jinoforum/subscribe.php', array('id' => $jinoforumobject->id, 'mode' => JINOFORUM_CHOOSESUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
        $forceforever = $mode->add(get_string("subscriptionforced", "jinoforum"), new moodle_url('/mod/jinoforum/subscribe.php', array('id' => $jinoforumobject->id, 'mode' => JINOFORUM_FORCESUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
        $forceinitially = $mode->add(get_string("subscriptionauto", "jinoforum"), new moodle_url('/mod/jinoforum/subscribe.php', array('id' => $jinoforumobject->id, 'mode' => JINOFORUM_INITIALSUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);
        $disallowchoice = $mode->add(get_string('subscriptiondisabled', 'jinoforum'), new moodle_url('/mod/jinoforum/subscribe.php', array('id' => $jinoforumobject->id, 'mode' => JINOFORUM_DISALLOWSUBSCRIBE, 'sesskey' => sesskey())), navigation_node::TYPE_SETTING);

        switch ($subscriptionmode) {
            case JINOFORUM_CHOOSESUBSCRIBE : // 0
                $allowchoice->action = null;
                $allowchoice->add_class('activesetting');
                break;
            case JINOFORUM_FORCESUBSCRIBE : // 1
                $forceforever->action = null;
                $forceforever->add_class('activesetting');
                break;
            case JINOFORUM_INITIALSUBSCRIBE : // 2
                $forceinitially->action = null;
                $forceinitially->add_class('activesetting');
                break;
            case JINOFORUM_DISALLOWSUBSCRIBE : // 3
                $disallowchoice->action = null;
                $disallowchoice->add_class('activesetting');
                break;
        }
    } else if ($activeenrolled) {

        switch ($subscriptionmode) {
            case JINOFORUM_CHOOSESUBSCRIBE : // 0
                $notenode = $jinoforumnode->add(get_string('subscriptionoptional', 'jinoforum'));
                break;
            case JINOFORUM_FORCESUBSCRIBE : // 1
                $notenode = $jinoforumnode->add(get_string('subscriptionforced', 'jinoforum'));
                break;
            case JINOFORUM_INITIALSUBSCRIBE : // 2
                $notenode = $jinoforumnode->add(get_string('subscriptionauto', 'jinoforum'));
                break;
            case JINOFORUM_DISALLOWSUBSCRIBE : // 3
                $notenode = $jinoforumnode->add(get_string('subscriptiondisabled', 'jinoforum'));
                break;
        }
    }

    if ($cansubscribe) {
        if (jinoforum_is_subscribed($USER->id, $jinoforumobject)) {
            $linktext = get_string('unsubscribe', 'jinoforum');
        } else {
            $linktext = get_string('subscribe', 'jinoforum');
        }
        $url = new moodle_url('/mod/jinoforum/subscribe.php', array('id' => $jinoforumobject->id, 'sesskey' => sesskey()));
        $jinoforumnode->add($linktext, $url, navigation_node::TYPE_SETTING);
    }

    if (has_capability('mod/jinoforum:viewsubscribers', $PAGE->cm->context)) {
        $url = new moodle_url('/mod/jinoforum/subscribers.php', array('id' => $jinoforumobject->id));
        $jinoforumnode->add(get_string('showsubscribers', 'jinoforum'), $url, navigation_node::TYPE_SETTING);
    }

    if ($enrolled && jinoforum_tp_can_track_jinoforums($jinoforumobject)) { // keep tracking info for users with suspended enrolments
        if ($jinoforumobject->trackingtype == JINOFORUM_TRACKING_OPTIONAL || ((!$CFG->jinoforum_allowforcedreadtracking) && $jinoforumobject->trackingtype == JINOFORUM_TRACKING_FORCED)) {
            if (jinoforum_tp_is_tracked($jinoforumobject)) {
                $linktext = get_string('notrackjinoforum', 'jinoforum');
            } else {
                $linktext = get_string('trackjinoforum', 'jinoforum');
            }
            $url = new moodle_url('/mod/jinoforum/settracking.php', array('id' => $jinoforumobject->id));
            $jinoforumnode->add($linktext, $url, navigation_node::TYPE_SETTING);
        }
    }

    if (!isloggedin() && $PAGE->course->id == SITEID) {
        $userid = guest_user()->id;
    } else {
        $userid = $USER->id;
    }

    $hascourseaccess = ($PAGE->course->id == SITEID) || can_access_course($PAGE->course, $userid);
    $enablerssfeeds = !empty($CFG->enablerssfeeds) && !empty($CFG->jinoforum_enablerssfeeds);

    if ($enablerssfeeds && $jinoforumobject->rsstype && $jinoforumobject->rssarticles && $hascourseaccess) {

        if (!function_exists('rss_get_url')) {
            require_once("$CFG->libdir/rsslib.php");
        }

        if ($jinoforumobject->rsstype == 1) {
            $string = get_string('rsssubscriberssdiscussions', 'jinoforum');
        } else {
            $string = get_string('rsssubscriberssposts', 'jinoforum');
        }

        $url = new moodle_url(rss_get_url($PAGE->cm->context->id, $userid, "mod_jinoforum", $jinoforumobject->id));
        $jinoforumnode->add($string, $url, settings_navigation::TYPE_SETTING, null, null, new pix_icon('i/rss', ''));
    }
}

/**
 * Abstract class used by jinoforum subscriber selection controls
 * @package   mod_jinoforum
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class jinoforum_subscriber_selector_base extends user_selector_base {

    /**
     * The id of the jinoforum this selector is being used for
     * @var int
     */
    protected $jinoforumid = null;

    /**
     * The context of the jinoforum this selector is being used for
     * @var object
     */
    protected $context = null;

    /**
     * The id of the current group
     * @var int
     */
    protected $currentgroup = null;

    /**
     * Constructor method
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options) {
        $options['accesscontext'] = $options['context'];
        parent::__construct($name, $options);
        if (isset($options['context'])) {
            $this->context = $options['context'];
        }
        if (isset($options['currentgroup'])) {
            $this->currentgroup = $options['currentgroup'];
        }
        if (isset($options['jinoforumid'])) {
            $this->jinoforumid = $options['jinoforumid'];
        }
    }

    /**
     * Returns an array of options to seralise and store for searches
     *
     * @return array
     */
    protected function get_options() {
        global $CFG;
        $options = parent::get_options();
        $options['file'] = substr(__FILE__, strlen($CFG->dirroot . '/'));
        $options['context'] = $this->context;
        $options['currentgroup'] = $this->currentgroup;
        $options['jinoforumid'] = $this->jinoforumid;
        return $options;
    }

}

/**
 * A user selector control for potential subscribers to the selected jinoforum
 * @package   mod_jinoforum
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class jinoforum_potential_subscriber_selector extends jinoforum_subscriber_selector_base {

    /**
     * If set to true EVERYONE in this course is force subscribed to this jinoforum
     * @var bool
     */
    protected $forcesubscribed = false;

    /**
     * Can be used to store existing subscribers so that they can be removed from
     * the potential subscribers list
     */
    protected $existingsubscribers = array();

    /**
     * Constructor method
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options) {
        parent::__construct($name, $options);
        if (isset($options['forcesubscribed'])) {
            $this->forcesubscribed = true;
        }
    }

    /**
     * Returns an arary of options for this control
     * @return array
     */
    protected function get_options() {
        $options = parent::get_options();
        if ($this->forcesubscribed === true) {
            $options['forcesubscribed'] = 1;
        }
        return $options;
    }

    /**
     * Finds all potential users
     *
     * Potential subscribers are all enroled users who are not already subscribed.
     *
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;

        $whereconditions = array();
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        if ($wherecondition) {
            $whereconditions[] = $wherecondition;
        }

        if (!$this->forcesubscribed) {
            $existingids = array();
            foreach ($this->existingsubscribers as $group) {
                foreach ($group as $user) {
                    $existingids[$user->id] = 1;
                }
            }
            if ($existingids) {
                list($usertest, $userparams) = $DB->get_in_or_equal(
                        array_keys($existingids), SQL_PARAMS_NAMED, 'existing', false);
                $whereconditions[] = 'u.id ' . $usertest;
                $params = array_merge($params, $userparams);
            }
        }

        if ($whereconditions) {
            $wherecondition = 'WHERE ' . implode(' AND ', $whereconditions);
        }

        list($esql, $eparams) = get_enrolled_sql($this->context, '', $this->currentgroup, true);
        $params = array_merge($params, $eparams);

        $fields = 'SELECT ' . $this->required_fields_sql('u');
        $countfields = 'SELECT COUNT(u.id)';

        $sql = " FROM {user} u
                 JOIN ($esql) je ON je.id = u.id
                      $wherecondition";

        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);
        $order = ' ORDER BY ' . $sort;

        // Check to see if there are too many to show sensibly.
        if (!$this->is_validating()) {
            $potentialmemberscount = $DB->count_records_sql($countfields . $sql, $params);
            if ($potentialmemberscount > $this->maxusersperpage) {
                return $this->too_many_results($search, $potentialmemberscount);
            }
        }

        // If not, show them.
        $availableusers = $DB->get_records_sql($fields . $sql . $order, array_merge($params, $sortparams));

        if (empty($availableusers)) {
            return array();
        }

        if ($this->forcesubscribed) {
            return array(get_string("existingsubscribers", 'jinoforum') => $availableusers);
        } else {
            return array(get_string("potentialsubscribers", 'jinoforum') => $availableusers);
        }
    }

    /**
     * Sets the existing subscribers
     * @param array $users
     */
    public function set_existing_subscribers(array $users) {
        $this->existingsubscribers = $users;
    }

    /**
     * Sets this jinoforum as force subscribed or not
     */
    public function set_force_subscribed($setting = true) {
        $this->forcesubscribed = true;
    }

}

/**
 * User selector control for removing subscribed users
 * @package   mod_jinoforum
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class jinoforum_existing_subscriber_selector extends jinoforum_subscriber_selector_base {

    /**
     * Finds all subscribed users
     *
     * @param string $search
     * @return array
     */
    public function find_users($search) {
        global $DB;
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        $params['jinoforumid'] = $this->jinoforumid;

        // only active enrolled or everybody on the frontpage
        list($esql, $eparams) = get_enrolled_sql($this->context, '', $this->currentgroup, true);
        $fields = $this->required_fields_sql('u');
        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);
        $params = array_merge($params, $eparams, $sortparams);

        $subscribers = $DB->get_records_sql("SELECT $fields
                                               FROM {user} u
                                               JOIN ($esql) je ON je.id = u.id
                                               JOIN {jinoforum_subscriptions} s ON s.userid = u.id
                                              WHERE $wherecondition AND s.jinoforum = :jinoforumid
                                           ORDER BY $sort", $params);

        return array(get_string("existingsubscribers", 'jinoforum') => $subscribers);
    }

}

/**
 * Adds information about unread messages, that is only required for the course view page (and
 * similar), to the course-module object.
 * @param cm_info $cm Course-module object
 */
function jinoforum_cm_info_view(cm_info $cm) {
    global $CFG;

    if (jinoforum_tp_can_track_jinoforums()) {
        if ($unread = jinoforum_tp_count_jinoforum_unread_posts($cm, $cm->get_course())) {
            $out = '<span class="unread"> <a href="' . $cm->url . '">';
            if ($unread == 1) {
                $out .= get_string('unreadpostsone', 'jinoforum');
            } else {
                $out .= get_string('unreadpostsnumber', 'jinoforum', $unread);
            }
            $out .= '</a></span>';
            $cm->set_after_link($out);
        }
    }
}

/**
 * Return a list of page types
 * @param string $pagetype current page type
 * @param stdClass $parentcontext Block's parent context
 * @param stdClass $currentcontext Current context of block
 */
function jinoforum_page_type_list($pagetype, $parentcontext, $currentcontext) {
    $jinoforum_pagetype = array(
        'mod-jinoforum-*' => get_string('page-mod-jinoforum-x', 'jinoforum'),
        'mod-jinoforum-view' => get_string('page-mod-jinoforum-view', 'jinoforum'),
        'mod-jinoforum-discuss' => get_string('page-mod-jinoforum-discuss', 'jinoforum')
    );
    return $jinoforum_pagetype;
}

/**
 * Gets all of the courses where the provided user has posted in a jinoforum.
 *
 * @global moodle_database $DB The database connection
 * @param stdClass $user The user who's posts we are looking for
 * @param bool $discussionsonly If true only look for discussions started by the user
 * @param bool $includecontexts If set to trye contexts for the courses will be preloaded
 * @param int $limitfrom The offset of records to return
 * @param int $limitnum The number of records to return
 * @return array An array of courses
 */
function jinoforum_get_courses_user_posted_in($user, $discussionsonly = false, $includecontexts = true, $limitfrom = null, $limitnum = null) {
    global $DB;

    // If we are only after discussions we need only look at the jinoforum_discussions
    // table and join to the userid there. If we are looking for posts then we need
    // to join to the jinoforum_posts table.
    if (!$discussionsonly) {
        $subquery = "(SELECT DISTINCT fd.course
                         FROM {jinoforum_discussions} fd
                         JOIN {jinoforum_posts} fp ON fp.discussion = fd.id
                        WHERE fp.userid = :userid )";
    } else {
        $subquery = "(SELECT DISTINCT fd.course
                         FROM {jinoforum_discussions} fd
                        WHERE fd.userid = :userid )";
    }

    $params = array('userid' => $user->id);

    // Join to the context table so that we can preload contexts if required.
    if ($includecontexts) {
        $ctxselect = ', ' . context_helper::get_preload_record_columns_sql('ctx');
        $ctxjoin = "LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = :contextlevel)";
        $params['contextlevel'] = CONTEXT_COURSE;
    } else {
        $ctxselect = '';
        $ctxjoin = '';
    }

    // Now we need to get all of the courses to search.
    // All courses where the user has posted within a jinoforum will be returned.
    $sql = "SELECT c.* $ctxselect
            FROM {course} c
            $ctxjoin
            WHERE c.id IN ($subquery)";
    $courses = $DB->get_records_sql($sql, $params, $limitfrom, $limitnum);
    if ($includecontexts) {
        array_map('context_helper::preload_from_record', $courses);
    }
    return $courses;
}

/**
 * Gets all of the jinoforums a user has posted in for one or more courses.
 *
 * @global moodle_database $DB
 * @param stdClass $user
 * @param array $courseids An array of courseids to search or if not provided
 *                       all courses the user has posted within
 * @param bool $discussionsonly If true then only jinoforums where the user has started
 *                       a discussion will be returned.
 * @param int $limitfrom The offset of records to return
 * @param int $limitnum The number of records to return
 * @return array An array of jinoforums the user has posted within in the provided courses
 */
function jinoforum_get_jinoforums_user_posted_in($user, array $courseids = null, $discussionsonly = false, $limitfrom = null, $limitnum = null) {
    global $DB;

    if (!is_null($courseids)) {
        list($coursewhere, $params) = $DB->get_in_or_equal($courseids, SQL_PARAMS_NAMED, 'courseid');
        $coursewhere = ' AND f.course ' . $coursewhere;
    } else {
        $coursewhere = '';
        $params = array();
    }
    $params['userid'] = $user->id;
    $params['jinoforum'] = 'jinoforum';

    if ($discussionsonly) {
        $join = 'JOIN {jinoforum_discussions} ff ON ff.jinoforum = f.id';
    } else {
        $join = 'JOIN {jinoforum_discussions} fd ON fd.jinoforum = f.id
                 JOIN {jinoforum_posts} ff ON ff.discussion = fd.id';
    }

    $sql = "SELECT f.*, cm.id AS cmid
              FROM {jinoforum} f
              JOIN {course_modules} cm ON cm.instance = f.id
              JOIN {modules} m ON m.id = cm.module
              JOIN (
                  SELECT f.id
                    FROM {jinoforum} f
                    {$join}
                   WHERE ff.userid = :userid
                GROUP BY f.id
                   ) j ON j.id = f.id
             WHERE m.name = :jinoforum
                 {$coursewhere}";

    $coursejinoforums = $DB->get_records_sql($sql, $params, $limitfrom, $limitnum);
    return $coursejinoforums;
}

/**
 * Returns posts made by the selected user in the requested courses.
 *
 * This method can be used to return all of the posts made by the requested user
 * within the given courses.
 * For each course the access of the current user and requested user is checked
 * and then for each post access to the post and jinoforum is checked as well.
 *
 * This function is safe to use with usercapabilities.
 *
 * @global moodle_database $DB
 * @param stdClass $user The user whose posts we want to get
 * @param array $courses The courses to search
 * @param bool $musthaveaccess If set to true errors will be thrown if the user
 *                             cannot access one or more of the courses to search
 * @param bool $discussionsonly If set to true only discussion starting posts
 *                              will be returned.
 * @param int $limitfrom The offset of records to return
 * @param int $limitnum The number of records to return
 * @return stdClass An object the following properties
 *               ->totalcount: the total number of posts made by the requested user
 *                             that the current user can see.
 *               ->courses: An array of courses the current user can see that the
 *                          requested user has posted in.
 *               ->jinoforums: An array of jinoforums relating to the posts returned in the
 *                         property below.
 *               ->posts: An array containing the posts to show for this request.
 */
function jinoforum_get_posts_by_user($user, array $courses, $musthaveaccess = false, $discussionsonly = false, $limitfrom = 0, $limitnum = 50) {
    global $DB, $USER, $CFG;

    $return = new stdClass;
    $return->totalcount = 0; // The total number of posts that the current user is able to view
    $return->courses = array(); // The courses the current user can access
    $return->jinoforums = array();  // The jinoforums that the current user can access that contain posts
    $return->posts = array();   // The posts to display
    // First up a small sanity check. If there are no courses to check we can
    // return immediately, there is obviously nothing to search.
    if (empty($courses)) {
        return $return;
    }

    // A couple of quick setups
    $isloggedin = isloggedin();
    $isguestuser = $isloggedin && isguestuser();
    $iscurrentuser = $isloggedin && $USER->id == $user->id;

    // Checkout whether or not the current user has capabilities over the requested
    // user and if so they have the capabilities required to view the requested
    // users content.
    $usercontext = context_user::instance($user->id, MUST_EXIST);
    $hascapsonuser = !$iscurrentuser && $DB->record_exists('role_assignments', array('userid' => $USER->id, 'contextid' => $usercontext->id));
    $hascapsonuser = $hascapsonuser && has_all_capabilities(array('moodle/user:viewdetails', 'moodle/user:readuserposts'), $usercontext);

    // Before we actually search each course we need to check the user's access to the
    // course. If the user doesn't have the appropraite access then we either throw an
    // error if a particular course was requested or we just skip over the course.
    foreach ($courses as $course) {
        $coursecontext = context_course::instance($course->id, MUST_EXIST);
        if ($iscurrentuser || $hascapsonuser) {
            // If it is the current user, or the current user has capabilities to the
            // requested user then all we need to do is check the requested users
            // current access to the course.
            // Note: There is no need to check group access or anything of the like
            // as either the current user is the requested user, or has granted
            // capabilities on the requested user. Either way they can see what the
            // requested user posted, although its VERY unlikely in the `parent` situation
            // that the current user will be able to view the posts in context.
            if (!is_viewing($coursecontext, $user) && !is_enrolled($coursecontext, $user)) {
                // Need to have full access to a course to see the rest of own info
                if ($musthaveaccess) {
                    print_error('errorenrolmentrequired', 'jinoforum');
                }
                continue;
            }
        } else {
            // Check whether the current user is enrolled or has access to view the course
            // if they don't we immediately have a problem.
            if (!can_access_course($course)) {
                if ($musthaveaccess) {
                    print_error('errorenrolmentrequired', 'jinoforum');
                }
                continue;
            }

            // Check whether the requested user is enrolled or has access to view the course
            // if they don't we immediately have a problem.
            if (!can_access_course($course, $user)) {
                if ($musthaveaccess) {
                    print_error('notenrolled', 'jinoforum');
                }
                continue;
            }

            // If groups are in use and enforced throughout the course then make sure
            // we can meet in at least one course level group.
            // Note that we check if either the current user or the requested user have
            // the capability to access all groups. This is because with that capability
            // a user in group A could post in the group B jinoforum. Grrrr.
            if (groups_get_course_groupmode($course) == SEPARATEGROUPS && $course->groupmodeforce && !has_capability('moodle/site:accessallgroups', $coursecontext) && !has_capability('moodle/site:accessallgroups', $coursecontext, $user->id)) {
                // If its the guest user to bad... the guest user cannot access groups
                if (!$isloggedin or $isguestuser) {
                    // do not use require_login() here because we might have already used require_login($course)
                    if ($musthaveaccess) {
                        redirect(get_login_url());
                    }
                    continue;
                }
                // Get the groups of the current user
                $mygroups = array_keys(groups_get_all_groups($course->id, $USER->id, $course->defaultgroupingid, 'g.id, g.name'));
                // Get the groups the requested user is a member of
                $usergroups = array_keys(groups_get_all_groups($course->id, $user->id, $course->defaultgroupingid, 'g.id, g.name'));
                // Check whether they are members of the same group. If they are great.
                $intersect = array_intersect($mygroups, $usergroups);
                if (empty($intersect)) {
                    // But they're not... if it was a specific course throw an error otherwise
                    // just skip this course so that it is not searched.
                    if ($musthaveaccess) {
                        print_error("groupnotamember", '', $CFG->wwwroot . "/course/view.php?id=$course->id");
                    }
                    continue;
                }
            }
        }
        // Woo hoo we got this far which means the current user can search this
        // this course for the requested user. Although this is only the course accessibility
        // handling that is complete, the jinoforum accessibility tests are yet to come.
        $return->courses[$course->id] = $course;
    }
    // No longer beed $courses array - lose it not it may be big
    unset($courses);

    // Make sure that we have some courses to search
    if (empty($return->courses)) {
        // If we don't have any courses to search then the reality is that the current
        // user doesn't have access to any courses is which the requested user has posted.
        // Although we do know at this point that the requested user has posts.
        if ($musthaveaccess) {
            print_error('permissiondenied');
        } else {
            return $return;
        }
    }

    // Next step: Collect all of the jinoforums that we will want to search.
    // It is important to note that this step isn't actually about searching, it is
    // about determining which jinoforums we can search by testing accessibility.
    $jinoforums = jinoforum_get_jinoforums_user_posted_in($user, array_keys($return->courses), $discussionsonly);

    // Will be used to build the where conditions for the search
    $jinoforumsearchwhere = array();
    // Will be used to store the where condition params for the search
    $jinoforumsearchparams = array();
    // Will record jinoforums where the user can freely access everything
    $jinoforumsearchfullaccess = array();
    // DB caching friendly
    $now = round(time(), -2);
    // For each course to search we want to find the jinoforums the user has posted in
    // and providing the current user can access the jinoforum create a search condition
    // for the jinoforum to get the requested users posts.
    foreach ($return->courses as $course) {
        // Now we need to get the jinoforums
        $modinfo = get_fast_modinfo($course);
        if (empty($modinfo->instances['jinoforum'])) {
            // hmmm, no jinoforums? well at least its easy... skip!
            continue;
        }
        // Iterate
        foreach ($modinfo->get_instances_of('jinoforum') as $jinoforumid => $cm) {
            if (!$cm->uservisible or ! isset($jinoforums[$jinoforumid])) {
                continue;
            }
            // Get the jinoforum in question
            $jinoforum = $jinoforums[$jinoforumid];

            // This is needed for functionality later on in the jinoforum code. It is converted to an object
            // because the cm_info is readonly from 2.6. This is a dirty hack because some other parts of the
            // code were expecting an writeable object. See {@link jinoforum_print_post()}.
            $jinoforum->cm = new stdClass();
            foreach ($cm as $key => $value) {
                $jinoforum->cm->$key = $value;
            }

            // Check that either the current user can view the jinoforum, or that the
            // current user has capabilities over the requested user and the requested
            // user can view the discussion
            if (!has_capability('mod/jinoforum:viewdiscussion', $cm->context) && !($hascapsonuser && has_capability('mod/jinoforum:viewdiscussion', $cm->context, $user->id))) {
                continue;
            }

            // This will contain jinoforum specific where clauses
            $jinoforumsearchselect = array();
            if (!$iscurrentuser && !$hascapsonuser) {
                // Make sure we check group access
                if (groups_get_activity_groupmode($cm, $course) == SEPARATEGROUPS and ! has_capability('moodle/site:accessallgroups', $cm->context)) {
                    $groups = $modinfo->get_groups($cm->groupingid);
                    $groups[] = -1;
                    list($groupid_sql, $groupid_params) = $DB->get_in_or_equal($groups, SQL_PARAMS_NAMED, 'grps' . $jinoforumid . '_');
                    $jinoforumsearchparams = array_merge($jinoforumsearchparams, $groupid_params);
                    $jinoforumsearchselect[] = "d.groupid $groupid_sql";
                }

                // hidden timed discussions
                if (!empty($CFG->jinoforum_enabletimedposts) && !has_capability('mod/jinoforum:viewhiddentimedposts', $cm->context)) {
                    $jinoforumsearchselect[] = "(d.userid = :userid{$jinoforumid} OR (d.timestart < :timestart{$jinoforumid} AND (d.timeend = 0 OR d.timeend > :timeend{$jinoforumid})))";
                    $jinoforumsearchparams['userid' . $jinoforumid] = $user->id;
                    $jinoforumsearchparams['timestart' . $jinoforumid] = $now;
                    $jinoforumsearchparams['timeend' . $jinoforumid] = $now;
                }

                // qanda access
                if ($jinoforum->type == 'qanda' && !has_capability('mod/jinoforum:viewqandawithoutposting', $cm->context)) {
                    // We need to check whether the user has posted in the qanda jinoforum.
                    $discussionspostedin = jinoforum_discussions_user_has_posted_in($jinoforum->id, $user->id);
                    if (!empty($discussionspostedin)) {
                        $jinoforumonlydiscussions = array();  // Holds discussion ids for the discussions the user is allowed to see in this jinoforum.
                        foreach ($discussionspostedin as $d) {
                            $jinoforumonlydiscussions[] = $d->id;
                        }
                        list($discussionid_sql, $discussionid_params) = $DB->get_in_or_equal($jinoforumonlydiscussions, SQL_PARAMS_NAMED, 'qanda' . $jinoforumid . '_');
                        $jinoforumsearchparams = array_merge($jinoforumsearchparams, $discussionid_params);
                        $jinoforumsearchselect[] = "(d.id $discussionid_sql OR p.parent = 0)";
                    } else {
                        $jinoforumsearchselect[] = "p.parent = 0";
                    }
                }

                if (count($jinoforumsearchselect) > 0) {
                    $jinoforumsearchwhere[] = "(d.jinoforum = :jinoforum{$jinoforumid} AND " . implode(" AND ", $jinoforumsearchselect) . ")";
                    $jinoforumsearchparams['jinoforum' . $jinoforumid] = $jinoforumid;
                } else {
                    $jinoforumsearchfullaccess[] = $jinoforumid;
                }
            } else {
                // The current user/parent can see all of their own posts
                $jinoforumsearchfullaccess[] = $jinoforumid;
            }
        }
    }

    // If we dont have any search conditions, and we don't have any jinoforums where
    // the user has full access then we just return the default.
    if (empty($jinoforumsearchwhere) && empty($jinoforumsearchfullaccess)) {
        return $return;
    }

    // Prepare a where condition for the full access jinoforums.
    if (count($jinoforumsearchfullaccess) > 0) {
        list($fullidsql, $fullidparams) = $DB->get_in_or_equal($jinoforumsearchfullaccess, SQL_PARAMS_NAMED, 'fula');
        $jinoforumsearchparams = array_merge($jinoforumsearchparams, $fullidparams);
        $jinoforumsearchwhere[] = "(d.jinoforum $fullidsql)";
    }

    // Prepare SQL to both count and search.
    // We alias user.id to useridx because we jinoforum_posts already has a userid field and not aliasing this would break
    // oracle and mssql.
    $userfields = user_picture::fields('u', null, 'useridx');
    $countsql = 'SELECT COUNT(*) ';
    $selectsql = 'SELECT p.*, d.jinoforum, d.name AS discussionname, ' . $userfields . ' ';
    $wheresql = implode(" OR ", $jinoforumsearchwhere);

    if ($discussionsonly) {
        if ($wheresql == '') {
            $wheresql = 'p.parent = 0';
        } else {
            $wheresql = 'p.parent = 0 AND (' . $wheresql . ')';
        }
    }

    $sql = "FROM {jinoforum_posts} p
            JOIN {jinoforum_discussions} d ON d.id = p.discussion
            JOIN {user} u ON u.id = p.userid
           WHERE ($wheresql)
             AND p.userid = :userid ";
    $orderby = "ORDER BY p.modified DESC";
    $jinoforumsearchparams['userid'] = $user->id;

    // Set the total number posts made by the requested user that the current user can see
    $return->totalcount = $DB->count_records_sql($countsql . $sql, $jinoforumsearchparams);
    // Set the collection of posts that has been requested
    $return->posts = $DB->get_records_sql($selectsql . $sql . $orderby, $jinoforumsearchparams, $limitfrom, $limitnum);

    // We need to build an array of jinoforums for which posts will be displayed.
    // We do this here to save the caller needing to retrieve them themselves before
    // printing these jinoforums posts. Given we have the jinoforums already there is
    // practically no overhead here.
    foreach ($return->posts as $post) {
        if (!array_key_exists($post->jinoforum, $return->jinoforums)) {
            $return->jinoforums[$post->jinoforum] = $jinoforums[$post->jinoforum];
        }
    }

    return $return;
}

/**
 * Set the per-jinoforum maildigest option for the specified user.
 *
 * @param stdClass $jinoforum The jinoforum to set the option for.
 * @param int $maildigest The maildigest option.
 * @param stdClass $user The user object. This defaults to the global $USER object.
 * @throws invalid_digest_setting thrown if an invalid maildigest option is provided.
 */
function jinoforum_set_user_maildigest($jinoforum, $maildigest, $user = null) {
    global $DB, $USER;

    if (is_number($jinoforum)) {
        $jinoforum = $DB->get_record('jinoforum', array('id' => $jinoforum));
    }

    if ($user === null) {
        $user = $USER;
    }

    $course = $DB->get_record('course', array('id' => $jinoforum->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id, $course->id, false, MUST_EXIST);
    $context = context_module::instance($cm->id);

    // User must be allowed to see this jinoforum.
    require_capability('mod/jinoforum:viewdiscussion', $context, $user->id);

    // Validate the maildigest setting.
    $digestoptions = jinoforum_get_user_digest_options($user);

    if (!isset($digestoptions[$maildigest])) {
        throw new moodle_exception('invaliddigestsetting', 'mod_jinoforum');
    }

    // Attempt to retrieve any existing jinoforum digest record.
    $subscription = $DB->get_record('jinoforum_digests', array(
        'userid' => $user->id,
        'jinoforum' => $jinoforum->id,
    ));

    // Create or Update the existing maildigest setting.
    if ($subscription) {
        if ($maildigest == -1) {
            $DB->delete_records('jinoforum_digests', array('jinoforum' => $jinoforum->id, 'userid' => $user->id));
        } else if ($maildigest !== $subscription->maildigest) {
            // Only update the maildigest setting if it's changed.

            $subscription->maildigest = $maildigest;
            $DB->update_record('jinoforum_digests', $subscription);
        }
    } else {
        if ($maildigest != -1) {
            // Only insert the maildigest setting if it's non-default.

            $subscription = new stdClass();
            $subscription->jinoforum = $jinoforum->id;
            $subscription->userid = $user->id;
            $subscription->maildigest = $maildigest;
            $subscription->id = $DB->insert_record('jinoforum_digests', $subscription);
        }
    }
}

/**
 * Determine the maildigest setting for the specified user against the
 * specified jinoforum.
 *
 * @param Array $digests An array of jinoforums and user digest settings.
 * @param stdClass $user The user object containing the id and maildigest default.
 * @param int $jinoforumid The ID of the jinoforum to check.
 * @return int The calculated maildigest setting for this user and jinoforum.
 */
function jinoforum_get_user_maildigest_bulk($digests, $user, $jinoforumid) {
    if (isset($digests[$jinoforumid]) && isset($digests[$jinoforumid][$user->id])) {
        $maildigest = $digests[$jinoforumid][$user->id];
        if ($maildigest === -1) {
            $maildigest = $user->maildigest;
        }
    } else {
        $maildigest = $user->maildigest;
    }
    return $maildigest;
}

/**
 * Retrieve the list of available user digest options.
 *
 * @param stdClass $user The user object. This defaults to the global $USER object.
 * @return array The mapping of values to digest options.
 */
function jinoforum_get_user_digest_options($user = null) {
    global $USER;

    // Revert to the global user object.
    if ($user === null) {
        $user = $USER;
    }

    $digestoptions = array();
    $digestoptions['0'] = get_string('emaildigestoffshort', 'mod_jinoforum');
    $digestoptions['1'] = get_string('emaildigestcompleteshort', 'mod_jinoforum');
    $digestoptions['2'] = get_string('emaildigestsubjectsshort', 'mod_jinoforum');

    // We need to add the default digest option at the end - it relies on
    // the contents of the existing values.
    $digestoptions['-1'] = get_string('emaildigestdefault', 'mod_jinoforum', $digestoptions[$user->maildigest]);

    // Resort the options to be in a sensible order.
    ksort($digestoptions);

    return $digestoptions;
}

function jinoforum_get_paging_bar($totalcount, $page, $perpage, $baseurl, $maxdisplay = 10) {
    $pagelinks = array();

    $lastpage = 1;
    if ($totalcount > 0) {
        $lastpage = ceil($totalcount / $perpage);
    }

    if ($page > $lastpage) {
        $page = $lastpage;
    }

    if ($page > round(($maxdisplay / 3) * 2)) {
        $currpage = $page - round($maxdisplay / 2);
        if ($currpage > ($lastpage - $maxdisplay)) {
            $currpage = $lastpage - $maxdisplay;
        }
    } else {
        $currpage = 1;
    }

    $prevlink = '';
    if ($page > 1) {
        $prevlink = '<span class="board-nav-prev">' . html_writer::link(str_replace(':page', $page - 1, $baseurl), "<") . "</span>";
    } else {
        $prevlink = '<span class="board-nav-prev"><a href="#"><</a></span>';
    }

    $nextlink = '';
    if ($page < $lastpage) {
        $nextlink = '<span class="board-nav-next">' . html_writer::link(str_replace(':page', $page + 1, $baseurl), '>', array('class' => 'board-nav-next')) . "</span>";
        ;
    } else {
        $nextlink = '<span class="board-nav-next"><a href="#">></a></span>';
    }


    echo '<div class="board-breadcrumbs">';

    $pagelinks[] = $prevlink;

    $pagelinks[] = "<ul>";
    if ($currpage > 1) {
        $firstlink = '<li>' . html_writer::link(str_replace(':page', 1, $baseurl), 1) . '</li>';

        $pagelinks[] = $firstlink;
        if ($currpage > 2) {
            $pagelinks[] = '<li>...</li>';
        }
    }

    $displaycount = 0;
    while ($displaycount <= $maxdisplay and $currpage <= $lastpage) {
        if ($page == $currpage) {
            $pagelinks[] = '<li class="current"><a href="#">' . $currpage . '</a></li>';
        } else {
            $pagelink = '<li>' . html_writer::link(str_replace(':page', $currpage, $baseurl), $currpage) . '</li>';
            $pagelinks[] = $pagelink;
        }

        $displaycount++;
        $currpage++;
    }

    if ($currpage - 1 < $lastpage) {
        $lastlink = '<li>' . html_writer::link(str_replace(':page', $lastpage, $baseurl), $lastpage) . '</li>';

        if ($currpage != $lastpage) {
            $pagelinks[] = '<li>...</li>';
        }
        $pagelinks[] = $lastlink;
    }
    $pagelinks[] = "</ul>";

    $pagelinks[] = $nextlink;


    echo implode('', $pagelinks);

    echo '</div>';
}

function jinoforum_get_total_pages($rows, $limit = 10) {
    if ($rows == 0) {
        return 1;
    }

    $total_pages = (int) ($rows / $limit);

    if (($rows % $limit) > 0) {
        $total_pages += 1;
    }

    return $total_pages;
}

function jinoforum_print_sort($discussion, $mode, $perpage, $cm, $parentpostid, $searchval, $searchtype) {
    global $DB, $CFG;
    echo html_writer::start_tag('div', array('class' => 'table-footer-area'));
    $sort_select = '<form>';
    $sort_select .= "<input type='hidden' name='d' value='" . $discussion->id . "'>";
    if ($mode == 5) {

        $sort_select .= '<select name ="perpage" onchange="this.form.submit();">';
        for ($i = 10; $i <= 30; $i+=10) {
            $sort_select .= "<option value='" . $i . "'";
            if ($perpage == $i) {
                $sort_select .= "selected";
            }
            $sort_select .= " >" . get_string('sort', 'jinoforum', $i) . "</option>";
        }
        $sort_select .= "<option value='50'>50개씩 보기</option>";
        $sort_select .= "</select>";
    }
    //$course = $DB->get_record("course",array('id'=>$discussion->course));
    //echo jinoforum_search_form($course, $searchval , $discussion->id , $mode , $searchtype);
    $sort_select .= '<div class="board-style">';
    //  $sort_select .= ($mode == JINOFORUM_MODE_NESTED) ? "<label class='board-style-thread'><input type='radio' name='mode' value='" . JINOFORUM_MODE_NESTED . "' checked>" . get_string('bstyle_thred', 'jinoforum') . "</label>" : "<label class='board-style-thread'><input type='radio' name='mode' value='" . JINOFORUM_MODE_NESTED . "' onclick='this.form.submit();'>" . get_string('bstyle_thred', 'jinoforum') . "</label>";
    //  $sort_select .= ($mode == JINOFORUM_MODE_LIST) ? "<label class='board-style-list'><input type='radio' name='mode' value='" . JINOFORUM_MODE_LIST . "' checked>" . get_string('bstyle_list', 'jinoforum') . "</label>" : "<label class='board-style-list'><input type='radio' name='mode' value='" . JINOFORUM_MODE_LIST . "' onclick='this.form.submit();'>" . get_string('bstyle_list', 'jinoforum') . "</label>";
    $sort_select .= '</div>';
    $sort_select .= "</form>";

    echo html_writer::tag('div', $sort_select, array('class' => 'btn-area btn-area-left'));
    echo html_writer::start_tag('div', array('class' => 'btn-area btn-area-right'));
    echo html_writer::tag('button', get_string('addopinion', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $CFG->wwwroot . "/mod/jinoforum/post.php?reply=" . $parentpostid . '#mformjinoforum"'));
    echo html_writer::tag('button', get_string('list', 'jinoforum'), array('style' => 'cursor:pointer;', 'onclick' => 'location.href="' . $CFG->wwwroot . "/mod/jinoforum/view.php?id=" . $cm->id . '"'));
    echo html_writer::end_tag('div');

    echo html_writer::end_tag('div');
}

function jinoforum_print_listtype($parentpostid, $perpage, $d, $page, $searchtype, $searchval) {
    global $CFG, $DB;
    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }

    $search = "";
    $usersearch = "";
    $join = "";
    $param = array();
    $param['ref'] = $parentpostid;
    if ($searchtype == 1) {
        $search = $DB->sql_like('jp.subject', ':searchval', false) . " and";
    } else if ($searchtype == 2) {
        $search = "";
        $join = "join {user} u on u.id = jp.userid ";
        $usersearch = "and (" . $DB->sql_like('u.firstname', ':searchval', false) . " or  " . $DB->sql_like('u.lastname', ':searchval2', false) . " or   " . $DB->sql_like('concat(u.firstname,u.lastname)', ':searchval3', false) . ")";
    } else if ($searchtype == 3) {
        $search = $DB->sql_like('jp.subject', ':searchval', false) . " and";
    }
    $param['searchval'] = '%' . $searchval . '%';
    $param['searchval2'] = '%' . $searchval . '%';
    $param['searchval3'] = '%' . $searchval . '%';
    $cntsql = "select count(jp.id) from {jinoforum_posts} jp $join where $search (jp.ref = :ref and jp.lev != 0) " . $usersearch;
    $totalcount = $DB->count_records_sql($cntsql, $param);
    $total_pages = jinoforum_get_total_pages($totalcount, $perpage);

    $num = $totalcount - $offset;

    $sql = "select jp.* from {jinoforum_posts} jp $join where $search (jp.ref = :ref and jp.lev != 0) " . $usersearch . " order by jp.ref DESC, jp.step ASC";
    $posts = $DB->get_records_sql($sql, $param, $offset, $perpage);
    $output = html_writer::start_tag('table', array('class' => 'boardlist'));
    $output .= html_writer::start_tag('thead');
    $output .= html_writer::start_tag('tr', array('class' => 'titles'));

    $output .= html_writer::tag('th', get_string("number", "jinoforum"), array('class' => 'number title left', 'width' => '8%'));
    $output .= html_writer::tag('th', get_string("subject", "jinoforum"), array('class' => 'subject title left'));
    $output .= html_writer::tag('th', get_string("comment", "jinoforum"), array('class' => 'comment title left', 'width' => '8%'));
    $output .= html_writer::tag('th', get_string("recommend", "jinoforum"), array('class' => 'recommend title left', 'width' => '8%'));
    $output .= html_writer::tag('th', get_string("hit", "jinoforum"), array('class' => 'hit', 'width' => '8%'));

    $output .= html_writer::end_tag('tr'); // titles
    $output .= html_writer::end_tag('thead');
    $output .= html_writer::start_tag('tbody');
    $by = new stdClass();
    foreach ($posts as $post) {
        $output .= html_writer::start_tag('tr', array('class' => 'titles'));
        $user = $DB->get_record("user", array('id' => $post->userid));
        $username = fullname($user);
        $output .= html_writer::tag('td', $num, array('class' => 'number cell textcenter', 'width' => '10%'));
        $subjectlink = html_writer::link($CFG->wwwroot . "/mod/jinoforum/post_view.php?post=" . $post->id, $post->subject);
        $userdate = userdate($post->created);
        if ($post->attachment == '') {
            $fileimg = "";
        } else {
            $fileimg = "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/icon-attachment.png' alt='" . get_string('content:file', 'local_jinoboard') . "'>&nbsp;";
        }
        $by->date = $userdate;
        $by->name = $username;
        $output .= html_writer::tag('td', $fileimg . "<b>" . $subjectlink . "</b></br><span class='post-date'>" . get_string('bynameondate', 'jinoforum', $by) . "</span>", array('class' => 'subject title left'));
        $output .= html_writer::tag('td', $post->commentscount, array('class' => 'comment cell textcenter'));
        $output .= html_writer::tag('td', $post->recommendcnt, array('class' => 'recommend cell textcenter'));
        $output .= html_writer::tag('td', $post->viewcnt, array('class' => 'hit textcenter'));

        $output .= html_writer::end_tag('tr'); // titles

        $num--;
    }


    $output .= html_writer::end_tag('tbody');
    $output .= html_writer::end_tag('table');

    echo $output;

    $page_params = array();
    $page_params['d'] = $d;
    $page_params['perpage'] = $perpage;


    jinoforum_get_paging_bar($CFG->wwwroot . "/mod/jinoforum/discuss.php", $page_params, $total_pages, $page);
}

function jino_coursein_user($courseid, $search) {
    global $DB;
    $context = context_course::instance($courseid);

    $userroles = $DB->get_records('role_assignments', array('contextid' => $context->id));
    $rolenames = role_get_names($context, ROLENAME_ALIAS, false);

    $userids = "(";
    foreach ($userroles as $userrole) {
        if ($rolenames[$userrole->roleid]->shortname == "student") {
            $userids .= $userrole->userid . ",";
        }
    }
    $userids = rtrim($userids, ",");
    $userids .= ")";
    if ($userids != "()") {
        $sql = "select * from {user} where id IN " . $userids;
        if ($search) {
            $sql .= " and (" . $DB->sql_like('lastname', ':search', false) . " or " . $DB->sql_like('firstname', ':search1', false) . " or " . $DB->sql_like($DB->sql_concat('firstname', "' '", 'lastname'), ':search2', false) . ")";
            return $DB->get_records_sql($sql, array('search' => '%' . $search . '%', 'search1' => '%' . $search . '%', 'search2' => '%' . $search . '%'));
        } else {
            return $DB->get_records_sql($sql);
        }
    }
    return "";
}

function jinoforum_print_content($sql, $param, $text) {

    global $CFG, $DB, $output;
    $contentsid = $DB->get_record_sql($sql, $param);
    if ($contentsid) {
        $post = $DB->get_record('jinoforum_posts', array('id' => $contentsid->id), '*', 'ref desc, step asc');
        $output .= html_writer::start_tag('tr', array('class' => 'titles'));
        $user = $DB->get_record("user", array('id' => $post->userid));
        $username = fullname($user);
        $subjectlink = $text;
        $subjectlink .= html_writer::link($CFG->wwwroot . "/mod/jinoforum/post_view.php?post=" . $post->id, $post->subject);
        $userdate = userdate($post->created);
        $fileimg = ($post->attachment == '') ? "" : "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/icon-attachment.png' alt='" . get_string('content:file', 'local_jinoboard') . "'>&nbsp;";
        $by = new stdClass();
        $by->date = $userdate;
        $by->name = $username;
        $output .= html_writer::tag('td', $fileimg . "<b>" . $subjectlink . "</b></br><span class='post-date'>" . get_string('bynameondate', 'jinoforum', $by) . "</span>", array('class' => 'subject title left'));
        $output .= html_writer::end_tag('tr');
        jinoforum_get_children($post->id);
    }
}

function jinoforum_get_children($parantid) {
    global $DB, $output;
    $posts = $DB->get_records('jinoforum_posts', array('parent' => $parantid), 'id asc, step desc');
    if ($posts) {
        $by = new stdClass();
        foreach ($posts as $post) {
            $output .= html_writer::start_tag('tr', array('class' => 'titles'));
            $user = $DB->get_record("user", array('id' => $post->userid));
            $username = fullname($user);
            $subjectlink = html_writer::link($CFG->wwwroot . "/mod/jinoforum/post_view.php?post=" . $post->id, $post->subject);
            $userdate = userdate($post->created);
            $fileimg = ($post->attachment == '') ? "" : "<img src='" . $CFG->wwwroot . "/theme/oklasscampus/pix/icon-attachment.png' alt='" . get_string('content:file', 'local_jinoboard') . "'>&nbsp;";
            $by->date = $userdate;
            $by->name = $username;
            $output .= html_writer::tag('td', $fileimg . "<b>" . $subjectlink . "</b></br><span class='post-date'>" . get_string('bynameondate', 'jinoforum', $by) . "</span>", array('class' => 'subject title left'));
            $output .= html_writer::end_tag('tr');
        }
    }
}

//function mod_jinoforum_cm_info_dynamic(cm_info $cm) {
//    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
//    if (!has_capability('mod/jinoforum:view', $context)) {
//        $cm->set_user_visible(false);
//    }
//}