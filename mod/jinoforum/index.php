<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_jinoforum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/mod/jinoforum/lib.php');
require_once($CFG->libdir . '/rsslib.php');

$id = optional_param('id', 0, PARAM_INT);                   // Course id
$subscribe = optional_param('subscribe', null, PARAM_INT);  // Subscribe/Unsubscribe all jinoforums

$url = new moodle_url('/mod/jinoforum/index.php', array('id'=>$id));
if ($subscribe !== null) {
    require_sesskey();
    $url->param('subscribe', $subscribe);
}
$PAGE->set_url($url);

if ($id) {
    if (! $course = $DB->get_record('course', array('id' => $id))) {
        print_error('invalidcourseid');
    }
} else {
    $course = get_site();
}

require_course_login($course);
$PAGE->set_pagelayout('incourse');
$coursecontext = context_course::instance($course->id);


unset($SESSION->fromdiscussion);

$params = array(
    'context' => context_course::instance($course->id)
);
$event = \mod_jinoforum\event\course_module_instance_list_viewed::create($params);
$event->add_record_snapshot('course', $course);
$event->trigger();

$strjinoforums       = get_string('jinoforums', 'jinoforum');
$strjinoforum        = get_string('jinoforum', 'jinoforum');
$strdescription  = get_string('description');
$strdiscussions  = get_string('discussions', 'jinoforum');
$strsubscribed   = get_string('subscribed', 'jinoforum');
$strunreadposts  = get_string('unreadposts', 'jinoforum');
$strtracking     = get_string('tracking', 'jinoforum');
$strmarkallread  = get_string('markallread', 'jinoforum');
$strtrackjinoforum   = get_string('trackjinoforum', 'jinoforum');
$strnotrackjinoforum = get_string('notrackjinoforum', 'jinoforum');
$strsubscribe    = get_string('subscribe', 'jinoforum');
$strunsubscribe  = get_string('unsubscribe', 'jinoforum');
$stryes          = get_string('yes');
$strno           = get_string('no');
$strrss          = get_string('rss');
$stremaildigest  = get_string('emaildigest');


// Retrieve the list of jinoforum digest options for later.
$digestoptions = jinoforum_get_user_digest_options();
$digestoptions_selector = new single_select(new moodle_url('/mod/jinoforum/maildigest.php',
    array(
        'backtoindex' => 1,
    )),
    'maildigest',
    $digestoptions,
    null,
    '');
$digestoptions_selector->method = 'post';

// Start of the table for General Jinoforums

$generaltable = new html_table();
$generaltable->head  = array ($strjinoforum, $strdescription, $strdiscussions);
$generaltable->align = array ('left', 'left', 'center');

if ($usetracking = jinoforum_tp_can_track_jinoforums()) {
    $untracked = jinoforum_tp_get_untracked_jinoforums($USER->id, $course->id);

    $generaltable->head[] = $strunreadposts;
    $generaltable->align[] = 'center';

    $generaltable->head[] = $strtracking;
    $generaltable->align[] = 'center';
}

$subscribed_jinoforums = jinoforum_get_subscribed_jinoforums($course);

$can_subscribe = is_enrolled($coursecontext);
if ($can_subscribe) {
    $generaltable->head[] = $strsubscribed;
    $generaltable->align[] = 'center';

    $generaltable->head[] = $stremaildigest . ' ' . $OUTPUT->help_icon('emaildigesttype', 'mod_jinoforum');
    $generaltable->align[] = 'center';
}

if ($show_rss = (($can_subscribe || $course->id == SITEID) &&
                 isset($CFG->enablerssfeeds) && isset($CFG->jinoforum_enablerssfeeds) &&
                 $CFG->enablerssfeeds && $CFG->jinoforum_enablerssfeeds)) {
    $generaltable->head[] = $strrss;
    $generaltable->align[] = 'center';
}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();

// Parse and organise all the jinoforums.  Most jinoforums are course modules but
// some special ones are not.  These get placed in the general jinoforums
// category with the jinoforums in section 0.

$jinoforums = $DB->get_records_sql("
    SELECT f.*,
           d.maildigest
      FROM {jinoforum} f
 LEFT JOIN {jinoforum_digests} d ON d.jinoforum = f.id AND d.userid = ?
     WHERE f.course = ?
    ", array($USER->id, $course->id));

$generaljinoforums  = array();
$learningjinoforums = array();
$modinfo = get_fast_modinfo($course);

foreach ($modinfo->get_instances_of('jinoforum') as $jinoforumid=>$cm) {
    if (!$cm->uservisible or !isset($jinoforums[$jinoforumid])) {
        continue;
    }

    $jinoforum = $jinoforums[$jinoforumid];

    if (!$context = context_module::instance($cm->id, IGNORE_MISSING)) {
        continue;   // Shouldn't happen
    }

    if (!has_capability('mod/jinoforum:viewdiscussion', $context)) {
        continue;
    }

    // fill two type array - order in modinfo is the same as in course
    if ($jinoforum->type == 'news' or $jinoforum->type == 'social') {
        $generaljinoforums[$jinoforum->id] = $jinoforum;

    } else if ($course->id == SITEID or empty($cm->sectionnum)) {
        $generaljinoforums[$jinoforum->id] = $jinoforum;

    } else {
        $learningjinoforums[$jinoforum->id] = $jinoforum;
    }
}

// Do course wide subscribe/unsubscribe if requested
if (!is_null($subscribe)) {
    if (isguestuser() or !$can_subscribe) {
        // there should not be any links leading to this place, just redirect
        redirect(new moodle_url('/mod/jinoforum/index.php', array('id' => $id)), get_string('subscribeenrolledonly', 'jinoforum'));
    }
    // Can proceed now, the user is not guest and is enrolled
    foreach ($modinfo->get_instances_of('jinoforum') as $jinoforumid=>$cm) {
        $jinoforum = $jinoforums[$jinoforumid];
        $modcontext = context_module::instance($cm->id);
        $cansub = false;

        if (has_capability('mod/jinoforum:viewdiscussion', $modcontext)) {
            $cansub = true;
        }
        if ($cansub && $cm->visible == 0 &&
            !has_capability('mod/jinoforum:managesubscriptions', $modcontext))
        {
            $cansub = false;
        }
        if (!jinoforum_is_forcesubscribed($jinoforum)) {
            $subscribed = jinoforum_is_subscribed($USER->id, $jinoforum);
            if ((has_capability('moodle/course:manageactivities', $coursecontext, $USER->id) || $jinoforum->forcesubscribe != JINOFORUM_DISALLOWSUBSCRIBE) && $subscribe && !$subscribed && $cansub) {
                jinoforum_subscribe($USER->id, $jinoforumid, $modcontext);
            } else if (!$subscribe && $subscribed) {
                jinoforum_unsubscribe($USER->id, $jinoforumid, $modcontext);
            }
        }
    }
    $returnto = jinoforum_go_back_to("index.php?id=$course->id");
    $shortname = format_string($course->shortname, true, array('context' => context_course::instance($course->id)));
    if ($subscribe) {
        redirect($returnto, get_string('nowallsubscribed', 'jinoforum', $shortname), 1);
    } else {
        redirect($returnto, get_string('nowallunsubscribed', 'jinoforum', $shortname), 1);
    }
}

/// First, let's process the general jinoforums and build up a display

if ($generaljinoforums) {
    foreach ($generaljinoforums as $jinoforum) {
        $cm      = $modinfo->instances['jinoforum'][$jinoforum->id];
        $context = context_module::instance($cm->id);

        $count = jinoforum_count_discussions($jinoforum, $cm, $course);

        if ($usetracking) {
            if ($jinoforum->trackingtype == JINOFORUM_TRACKING_OFF) {
                $unreadlink  = '-';
                $trackedlink = '-';

            } else {
                if (isset($untracked[$jinoforum->id])) {
                        $unreadlink  = '-';
                } else if ($unread = jinoforum_tp_count_jinoforum_unread_posts($cm, $course)) {
                        $unreadlink = '<span class="unread"><a href="view.php?f='.$jinoforum->id.'">'.$unread.'</a>';
                    $unreadlink .= '<a title="'.$strmarkallread.'" href="markposts.php?f='.
                                   $jinoforum->id.'&amp;mark=read"><img src="'.$OUTPUT->pix_url('t/markasread') . '" alt="'.$strmarkallread.'" class="iconsmall" /></a></span>';
                } else {
                    $unreadlink = '<span class="read">0</span>';
                }

                if (($jinoforum->trackingtype == JINOFORUM_TRACKING_FORCED) && ($CFG->jinoforum_allowforcedreadtracking)) {
                    $trackedlink = $stryes;
                } else if ($jinoforum->trackingtype === JINOFORUM_TRACKING_OFF || ($USER->trackforums == 0)) {
                    $trackedlink = '-';
                } else {
                    $aurl = new moodle_url('/mod/jinoforum/settracking.php', array('id'=>$jinoforum->id));
                    if (!isset($untracked[$jinoforum->id])) {
                        $trackedlink = $OUTPUT->single_button($aurl, $stryes, 'post', array('title'=>$strnotrackjinoforum));
                    } else {
                        $trackedlink = $OUTPUT->single_button($aurl, $strno, 'post', array('title'=>$strtrackjinoforum));
                    }
                }
            }
        }

        $jinoforum->intro = shorten_text(format_module_intro('jinoforum', $jinoforum, $cm->id), $CFG->jinoforum_shortpost);
        $jinoforumname = format_string($jinoforum->name, true);

        if ($cm->visible) {
            $style = '';
        } else {
            $style = 'class="dimmed"';
        }
        $jinoforumlink = "<a href=\"view.php?f=$jinoforum->id\" $style>".format_string($jinoforum->name,true)."</a>";
        $discussionlink = "<a href=\"view.php?f=$jinoforum->id\" $style>".$count."</a>";

        $row = array ($jinoforumlink, $jinoforum->intro, $discussionlink);
        if ($usetracking) {
            $row[] = $unreadlink;
            $row[] = $trackedlink;    // Tracking.
        }

        if ($can_subscribe) {
            if ($jinoforum->forcesubscribe != JINOFORUM_DISALLOWSUBSCRIBE) {
                $row[] = jinoforum_get_subscribe_link($jinoforum, $context, array('subscribed' => $stryes,
                        'unsubscribed' => $strno, 'forcesubscribed' => $stryes,
                        'cantsubscribe' => '-'), false, false, true, $subscribed_jinoforums);
            } else {
                $row[] = '-';
            }

            $digestoptions_selector->url->param('id', $jinoforum->id);
            if ($jinoforum->maildigest === null) {
                $digestoptions_selector->selected = -1;
            } else {
                $digestoptions_selector->selected = $jinoforum->maildigest;
            }
            $row[] = $OUTPUT->render($digestoptions_selector);
        }

        //If this jinoforum has RSS activated, calculate it
        if ($show_rss) {
            if ($jinoforum->rsstype and $jinoforum->rssarticles) {
                //Calculate the tooltip text
                if ($jinoforum->rsstype == 1) {
                    $tooltiptext = get_string('rsssubscriberssdiscussions', 'jinoforum');
                } else {
                    $tooltiptext = get_string('rsssubscriberssposts', 'jinoforum');
                }

                if (!isloggedin() && $course->id == SITEID) {
                    $userid = guest_user()->id;
                } else {
                    $userid = $USER->id;
                }
                //Get html code for RSS link
                $row[] = rss_get_link($context->id, $userid, 'mod_jinoforum', $jinoforum->id, $tooltiptext);
            } else {
                $row[] = '&nbsp;';
            }
        }

        $generaltable->data[] = $row;
    }
}


// Start of the table for Learning Jinoforums
$learningtable = new html_table();
$learningtable->head  = array ($strjinoforum, $strdescription, $strdiscussions);
$learningtable->align = array ('left', 'left', 'center');

if ($usetracking) {
    $learningtable->head[] = $strunreadposts;
    $learningtable->align[] = 'center';

    $learningtable->head[] = $strtracking;
    $learningtable->align[] = 'center';
}

if ($can_subscribe) {
    $learningtable->head[] = $strsubscribed;
    $learningtable->align[] = 'center';

    $learningtable->head[] = $stremaildigest . ' ' . $OUTPUT->help_icon('emaildigesttype', 'mod_jinoforum');
    $learningtable->align[] = 'center';
}

if ($show_rss = (($can_subscribe || $course->id == SITEID) &&
                 isset($CFG->enablerssfeeds) && isset($CFG->jinoforum_enablerssfeeds) &&
                 $CFG->enablerssfeeds && $CFG->jinoforum_enablerssfeeds)) {
    $learningtable->head[] = $strrss;
    $learningtable->align[] = 'center';
}

/// Now let's process the learning jinoforums

if ($course->id != SITEID) {    // Only real courses have learning jinoforums
    // 'format_.'$course->format only applicable when not SITEID (format_site is not a format)
    $strsectionname  = get_string('sectionname', 'format_'.$course->format);
    // Add extra field for section number, at the front
    array_unshift($learningtable->head, $strsectionname);
    array_unshift($learningtable->align, 'center');


    if ($learningjinoforums) {
        $currentsection = '';
            foreach ($learningjinoforums as $jinoforum) {
            $cm      = $modinfo->instances['jinoforum'][$jinoforum->id];
            $context = context_module::instance($cm->id);

            $count = jinoforum_count_discussions($jinoforum, $cm, $course);

            if ($usetracking) {
                if ($jinoforum->trackingtype == JINOFORUM_TRACKING_OFF) {
                    $unreadlink  = '-';
                    $trackedlink = '-';

                } else {
                    if (isset($untracked[$jinoforum->id])) {
                        $unreadlink  = '-';
                    } else if ($unread = jinoforum_tp_count_jinoforum_unread_posts($cm, $course)) {
                        $unreadlink = '<span class="unread"><a href="view.php?f='.$jinoforum->id.'">'.$unread.'</a>';
                        $unreadlink .= '<a title="'.$strmarkallread.'" href="markposts.php?f='.
                                       $jinoforum->id.'&amp;mark=read"><img src="'.$OUTPUT->pix_url('t/markasread') . '" alt="'.$strmarkallread.'" class="iconsmall" /></a></span>';
                    } else {
                        $unreadlink = '<span class="read">0</span>';
                    }

                    if (($jinoforum->trackingtype == JINOFORUM_TRACKING_FORCED) && ($CFG->jinoforum_allowforcedreadtracking)) {
                        $trackedlink = $stryes;
                    } else if ($jinoforum->trackingtype === JINOFORUM_TRACKING_OFF || ($USER->trackforums == 0)) {
                        $trackedlink = '-';
                    } else {
                        $aurl = new moodle_url('/mod/jinoforum/settracking.php', array('id'=>$jinoforum->id));
                        if (!isset($untracked[$jinoforum->id])) {
                            $trackedlink = $OUTPUT->single_button($aurl, $stryes, 'post', array('title'=>$strnotrackjinoforum));
                        } else {
                            $trackedlink = $OUTPUT->single_button($aurl, $strno, 'post', array('title'=>$strtrackjinoforum));
                        }
                    }
                }
            }

            $jinoforum->intro = shorten_text(format_module_intro('jinoforum', $jinoforum, $cm->id), $CFG->jinoforum_shortpost);

            if ($cm->sectionnum != $currentsection) {
                $printsection = get_section_name($course, $cm->sectionnum);
                if ($currentsection) {
                    $learningtable->data[] = 'hr';
                }
                $currentsection = $cm->sectionnum;
            } else {
                $printsection = '';
            }

            $jinoforumname = format_string($jinoforum->name,true);

            if ($cm->visible) {
                $style = '';
            } else {
                $style = 'class="dimmed"';
            }
            $jinoforumlink = "<a href=\"view.php?f=$jinoforum->id\" $style>".format_string($jinoforum->name,true)."</a>";
            $discussionlink = "<a href=\"view.php?f=$jinoforum->id\" $style>".$count."</a>";

            $row = array ($printsection, $jinoforumlink, $jinoforum->intro, $discussionlink);
            if ($usetracking) {
                $row[] = $unreadlink;
                $row[] = $trackedlink;    // Tracking.
            }

            if ($can_subscribe) {
                if ($jinoforum->forcesubscribe != JINOFORUM_DISALLOWSUBSCRIBE) {
                    $row[] = jinoforum_get_subscribe_link($jinoforum, $context, array('subscribed' => $stryes,
                        'unsubscribed' => $strno, 'forcesubscribed' => $stryes,
                        'cantsubscribe' => '-'), false, false, true, $subscribed_jinoforums);
                } else {
                    $row[] = '-';
                }

                $digestoptions_selector->url->param('id', $jinoforum->id);
                if ($jinoforum->maildigest === null) {
                    $digestoptions_selector->selected = -1;
                } else {
                    $digestoptions_selector->selected = $jinoforum->maildigest;
                }
                $row[] = $OUTPUT->render($digestoptions_selector);
            }

            //If this jinoforum has RSS activated, calculate it
            if ($show_rss) {
                if ($jinoforum->rsstype and $jinoforum->rssarticles) {
                    //Calculate the tolltip text
                    if ($jinoforum->rsstype == 1) {
                        $tooltiptext = get_string('rsssubscriberssdiscussions', 'jinoforum');
                    } else {
                        $tooltiptext = get_string('rsssubscriberssposts', 'jinoforum');
                    }
                    //Get html code for RSS link
                    $row[] = rss_get_link($context->id, $USER->id, 'mod_jinoforum', $jinoforum->id, $tooltiptext);
                } else {
                    $row[] = '&nbsp;';
                }
            }

            $learningtable->data[] = $row;
        }
    }
}


/// Output the page
$PAGE->navbar->add($strjinoforums);
$PAGE->set_title("$course->shortname: $strjinoforums");
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
jinoforum_search_form($course);
// Show the subscribe all options only to non-guest, enrolled users
if (!isguestuser() && isloggedin() && $can_subscribe) {
    echo $OUTPUT->box_start('subscription');
    echo html_writer::tag('div',
        html_writer::link(new moodle_url('/mod/jinoforum/index.php', array('id'=>$course->id, 'subscribe'=>1, 'sesskey'=>sesskey())),
            get_string('allsubscribe', 'jinoforum')),
        array('class'=>'helplink'));
    echo html_writer::tag('div',
        html_writer::link(new moodle_url('/mod/jinoforum/index.php', array('id'=>$course->id, 'subscribe'=>0, 'sesskey'=>sesskey())),
            get_string('allunsubscribe', 'jinoforum')),
        array('class'=>'helplink'));
    echo $OUTPUT->box_end();
    echo $OUTPUT->box('&nbsp;', 'clearer');
}

if ($generaljinoforums) {
    echo $OUTPUT->heading(get_string('generaljinoforums', 'jinoforum'), 2);
    echo html_writer::table($generaltable);
}

if ($learningjinoforums) {
    echo $OUTPUT->heading(get_string('learningjinoforums', 'jinoforum'), 2);
    echo html_writer::table($learningtable);
}

echo $OUTPUT->footer();

