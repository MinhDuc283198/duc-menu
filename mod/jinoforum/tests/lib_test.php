<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The module jinoforums tests
 *
 * @package    mod_jinoforum
 * @copyright  2013 Frédéric Massart
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class mod_jinoforum_lib_testcase extends advanced_testcase {

    public function test_jinoforum_trigger_content_uploaded_event() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();
        $course = $this->getDataGenerator()->create_course();
        $jinoforum = $this->getDataGenerator()->create_module('jinoforum', array('course' => $course->id));
        $context = context_module::instance($jinoforum->cmid);

        $this->setUser($user->id);
        $fakepost = (object) array('id' => 123, 'message' => 'Yay!', 'discussion' => 100);
        $cm = get_coursemodule_from_instance('jinoforum', $jinoforum->id);

        $fs = get_file_storage();
        $dummy = (object) array(
            'contextid' => $context->id,
            'component' => 'mod_jinoforum',
            'filearea' => 'attachment',
            'itemid' => $fakepost->id,
            'filepath' => '/',
            'filename' => 'myassignmnent.pdf'
        );
        $fi = $fs->create_file_from_string($dummy, 'Content of ' . $dummy->filename);

        $data = new stdClass();
        $sink = $this->redirectEvents();
        jinoforum_trigger_content_uploaded_event($fakepost, $cm, 'some triggered from value');
        $events = $sink->get_events();

        $this->assertCount(1, $events);
        $event = reset($events);
        $this->assertInstanceOf('\mod_jinoforum\event\assessable_uploaded', $event);
        $this->assertEquals($context->id, $event->contextid);
        $this->assertEquals($fakepost->id, $event->objectid);
        $this->assertEquals($fakepost->message, $event->other['content']);
        $this->assertEquals($fakepost->discussion, $event->other['discussionid']);
        $this->assertCount(1, $event->other['pathnamehashes']);
        $this->assertEquals($fi->get_pathnamehash(), $event->other['pathnamehashes'][0]);
        $expected = new stdClass();
        $expected->modulename = 'jinoforum';
        $expected->name = 'some triggered from value';
        $expected->cmid = $jinoforum->cmid;
        $expected->itemid = $fakepost->id;
        $expected->courseid = $course->id;
        $expected->userid = $user->id;
        $expected->content = $fakepost->message;
        $expected->pathnamehashes = array($fi->get_pathnamehash());
        $this->assertEventLegacyData($expected, $event);
        $this->assertEventContextNotUsed($event);
    }

    public function test_jinoforum_get_courses_user_posted_in() {
        $this->resetAfterTest();

        $user1 = $this->getDataGenerator()->create_user();
        $user2 = $this->getDataGenerator()->create_user();
        $user3 = $this->getDataGenerator()->create_user();

        $course1 = $this->getDataGenerator()->create_course();
        $course2 = $this->getDataGenerator()->create_course();
        $course3 = $this->getDataGenerator()->create_course();

        // Create 3 jinoforums, one in each course.
        $record = new stdClass();
        $record->course = $course1->id;
        $jinoforum1 = $this->getDataGenerator()->create_module('jinoforum', $record);

        $record = new stdClass();
        $record->course = $course2->id;
        $jinoforum2 = $this->getDataGenerator()->create_module('jinoforum', $record);

        $record = new stdClass();
        $record->course = $course3->id;
        $jinoforum3 = $this->getDataGenerator()->create_module('jinoforum', $record);

        // Add a second jinoforum in course 1.
        $record = new stdClass();
        $record->course = $course1->id;
        $jinoforum4 = $this->getDataGenerator()->create_module('jinoforum', $record);

        // Add discussions to course 1 started by user1.
        $record = new stdClass();
        $record->course = $course1->id;
        $record->userid = $user1->id;
        $record->jinoforum = $jinoforum1->id;
        $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        $record = new stdClass();
        $record->course = $course1->id;
        $record->userid = $user1->id;
        $record->jinoforum = $jinoforum4->id;
        $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        // Add discussions to course2 started by user1.
        $record = new stdClass();
        $record->course = $course2->id;
        $record->userid = $user1->id;
        $record->jinoforum = $jinoforum2->id;
        $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        // Add discussions to course 3 started by user2.
        $record = new stdClass();
        $record->course = $course3->id;
        $record->userid = $user2->id;
        $record->jinoforum = $jinoforum3->id;
        $discussion3 = $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        // Add post to course 3 by user1.
        $record = new stdClass();
        $record->course = $course3->id;
        $record->userid = $user1->id;
        $record->jinoforum = $jinoforum3->id;
        $record->discussion = $discussion3->id;
        $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_post($record);

        // User 3 hasn't posted anything, so shouldn't get any results.
        $user3courses = jinoforum_get_courses_user_posted_in($user3);
        $this->assertEmpty($user3courses);

        // User 2 has only posted in course3.
        $user2courses = jinoforum_get_courses_user_posted_in($user2);
        $this->assertCount(1, $user2courses);
        $user2course = array_shift($user2courses);
        $this->assertEquals($course3->id, $user2course->id);
        $this->assertEquals($course3->shortname, $user2course->shortname);

        // User 1 has posted in all 3 courses.
        $user1courses = jinoforum_get_courses_user_posted_in($user1);
        $this->assertCount(3, $user1courses);
        foreach ($user1courses as $course) {
            $this->assertContains($course->id, array($course1->id, $course2->id, $course3->id));
            $this->assertContains($course->shortname, array($course1->shortname, $course2->shortname,
                $course3->shortname));

        }

        // User 1 has only started a discussion in course 1 and 2 though.
        $user1courses = jinoforum_get_courses_user_posted_in($user1, true);
        $this->assertCount(2, $user1courses);
        foreach ($user1courses as $course) {
            $this->assertContains($course->id, array($course1->id, $course2->id));
            $this->assertContains($course->shortname, array($course1->shortname, $course2->shortname));
        }
    }

    /**
     * Test the logic in the jinoforum_tp_can_track_jinoforums() function.
     */
    public function test_jinoforum_tp_can_track_jinoforums() {
        global $CFG;

        $this->resetAfterTest();

        $useron = $this->getDataGenerator()->create_user(array('trackjinoforums' => 1));
        $useroff = $this->getDataGenerator()->create_user(array('trackjinoforums' => 0));
        $course = $this->getDataGenerator()->create_course();
        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OFF); // Off.
        $jinoforumoff = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_FORCED); // On.
        $jinoforumforce = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OPTIONAL); // Optional.
        $jinoforumoptional = $this->getDataGenerator()->create_module('jinoforum', $options);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        // User on, jinoforum off, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoff, $useron);
        $this->assertEquals(false, $result);

        // User on, jinoforum on, should be on.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumforce, $useron);
        $this->assertEquals(true, $result);

        // User on, jinoforum optional, should be on.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoptional, $useron);
        $this->assertEquals(true, $result);

        // User off, jinoforum off, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoff, $useroff);
        $this->assertEquals(false, $result);

        // User off, jinoforum force, should be on.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumforce, $useroff);
        $this->assertEquals(true, $result);

        // User off, jinoforum optional, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoptional, $useroff);
        $this->assertEquals(false, $result);

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        // User on, jinoforum off, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoff, $useron);
        $this->assertEquals(false, $result);

        // User on, jinoforum on, should be on.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumforce, $useron);
        $this->assertEquals(true, $result);

        // User on, jinoforum optional, should be on.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoptional, $useron);
        $this->assertEquals(true, $result);

        // User off, jinoforum off, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoff, $useroff);
        $this->assertEquals(false, $result);

        // User off, jinoforum force, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumforce, $useroff);
        $this->assertEquals(false, $result);

        // User off, jinoforum optional, should be off.
        $result = jinoforum_tp_can_track_jinoforums($jinoforumoptional, $useroff);
        $this->assertEquals(false, $result);

    }

    /**
     * Test the logic in the test_jinoforum_tp_is_tracked() function.
     */
    public function test_jinoforum_tp_is_tracked() {
        global $CFG;

        $this->resetAfterTest();

        $useron = $this->getDataGenerator()->create_user(array('trackjinoforums' => 1));
        $useroff = $this->getDataGenerator()->create_user(array('trackjinoforums' => 0));
        $course = $this->getDataGenerator()->create_course();
        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OFF); // Off.
        $jinoforumoff = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_FORCED); // On.
        $jinoforumforce = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OPTIONAL); // Optional.
        $jinoforumoptional = $this->getDataGenerator()->create_module('jinoforum', $options);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        // User on, jinoforum off, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoff, $useron);
        $this->assertEquals(false, $result);

        // User on, jinoforum force, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useron);
        $this->assertEquals(true, $result);

        // User on, jinoforum optional, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useron);
        $this->assertEquals(true, $result);

        // User off, jinoforum off, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoff, $useroff);
        $this->assertEquals(false, $result);

        // User off, jinoforum force, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useroff);
        $this->assertEquals(true, $result);

        // User off, jinoforum optional, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useroff);
        $this->assertEquals(false, $result);

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        // User on, jinoforum off, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoff, $useron);
        $this->assertEquals(false, $result);

        // User on, jinoforum force, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useron);
        $this->assertEquals(true, $result);

        // User on, jinoforum optional, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useron);
        $this->assertEquals(true, $result);

        // User off, jinoforum off, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoff, $useroff);
        $this->assertEquals(false, $result);

        // User off, jinoforum force, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useroff);
        $this->assertEquals(false, $result);

        // User off, jinoforum optional, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useroff);
        $this->assertEquals(false, $result);

        // Stop tracking so we can test again.
        jinoforum_tp_stop_tracking($jinoforumforce->id, $useron->id);
        jinoforum_tp_stop_tracking($jinoforumoptional->id, $useron->id);
        jinoforum_tp_stop_tracking($jinoforumforce->id, $useroff->id);
        jinoforum_tp_stop_tracking($jinoforumoptional->id, $useroff->id);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        // User on, preference off, jinoforum force, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useron);
        $this->assertEquals(true, $result);

        // User on, preference off, jinoforum optional, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useron);
        $this->assertEquals(false, $result);

        // User off, preference off, jinoforum force, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useroff);
        $this->assertEquals(true, $result);

        // User off, preference off, jinoforum optional, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useroff);
        $this->assertEquals(false, $result);

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        // User on, preference off, jinoforum force, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useron);
        $this->assertEquals(false, $result);

        // User on, preference off, jinoforum optional, should be on.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useron);
        $this->assertEquals(false, $result);

        // User off, preference off, jinoforum force, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumforce, $useroff);
        $this->assertEquals(false, $result);

        // User off, preference off, jinoforum optional, should be off.
        $result = jinoforum_tp_is_tracked($jinoforumoptional, $useroff);
        $this->assertEquals(false, $result);
    }

    /**
     * Test the logic in the jinoforum_tp_get_course_unread_posts() function.
     */
    public function test_jinoforum_tp_get_course_unread_posts() {
        global $CFG;

        $this->resetAfterTest();

        $useron = $this->getDataGenerator()->create_user(array('trackjinoforums' => 1));
        $useroff = $this->getDataGenerator()->create_user(array('trackjinoforums' => 0));
        $course = $this->getDataGenerator()->create_course();
        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OFF); // Off.
        $jinoforumoff = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_FORCED); // On.
        $jinoforumforce = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OPTIONAL); // Optional.
        $jinoforumoptional = $this->getDataGenerator()->create_module('jinoforum', $options);

        // Add discussions to the tracking off jinoforum.
        $record = new stdClass();
        $record->course = $course->id;
        $record->userid = $useron->id;
        $record->jinoforum = $jinoforumoff->id;
        $discussionoff = $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        // Add discussions to the tracking forced jinoforum.
        $record = new stdClass();
        $record->course = $course->id;
        $record->userid = $useron->id;
        $record->jinoforum = $jinoforumforce->id;
        $discussionforce = $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        // Add post to the tracking forced discussion.
        $record = new stdClass();
        $record->course = $course->id;
        $record->userid = $useroff->id;
        $record->jinoforum = $jinoforumforce->id;
        $record->discussion = $discussionforce->id;
        $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_post($record);

        // Add discussions to the tracking optional jinoforum.
        $record = new stdClass();
        $record->course = $course->id;
        $record->userid = $useron->id;
        $record->jinoforum = $jinoforumoptional->id;
        $discussionoptional = $this->getDataGenerator()->get_plugin_generator('mod_jinoforum')->create_discussion($record);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        $result = jinoforum_tp_get_course_unread_posts($useron->id, $course->id);
        $this->assertEquals(2, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));
        $this->assertEquals(2, $result[$jinoforumforce->id]->unread);
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));
        $this->assertEquals(1, $result[$jinoforumoptional->id]->unread);

        $result = jinoforum_tp_get_course_unread_posts($useroff->id, $course->id);
        $this->assertEquals(1, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));
        $this->assertEquals(2, $result[$jinoforumforce->id]->unread);
        $this->assertEquals(false, isset($result[$jinoforumoptional->id]));

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        $result = jinoforum_tp_get_course_unread_posts($useron->id, $course->id);
        $this->assertEquals(2, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));
        $this->assertEquals(2, $result[$jinoforumforce->id]->unread);
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));
        $this->assertEquals(1, $result[$jinoforumoptional->id]->unread);

        $result = jinoforum_tp_get_course_unread_posts($useroff->id, $course->id);
        $this->assertEquals(0, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(false, isset($result[$jinoforumforce->id]));
        $this->assertEquals(false, isset($result[$jinoforumoptional->id]));

        // Stop tracking so we can test again.
        jinoforum_tp_stop_tracking($jinoforumforce->id, $useron->id);
        jinoforum_tp_stop_tracking($jinoforumoptional->id, $useron->id);
        jinoforum_tp_stop_tracking($jinoforumforce->id, $useroff->id);
        jinoforum_tp_stop_tracking($jinoforumoptional->id, $useroff->id);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        $result = jinoforum_tp_get_course_unread_posts($useron->id, $course->id);
        $this->assertEquals(1, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));
        $this->assertEquals(2, $result[$jinoforumforce->id]->unread);
        $this->assertEquals(false, isset($result[$jinoforumoptional->id]));

        $result = jinoforum_tp_get_course_unread_posts($useroff->id, $course->id);
        $this->assertEquals(1, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));
        $this->assertEquals(2, $result[$jinoforumforce->id]->unread);
        $this->assertEquals(false, isset($result[$jinoforumoptional->id]));

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        $result = jinoforum_tp_get_course_unread_posts($useron->id, $course->id);
        $this->assertEquals(0, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(false, isset($result[$jinoforumforce->id]));
        $this->assertEquals(false, isset($result[$jinoforumoptional->id]));

        $result = jinoforum_tp_get_course_unread_posts($useroff->id, $course->id);
        $this->assertEquals(0, count($result));
        $this->assertEquals(false, isset($result[$jinoforumoff->id]));
        $this->assertEquals(false, isset($result[$jinoforumforce->id]));
        $this->assertEquals(false, isset($result[$jinoforumoptional->id]));
    }

    /**
     * Test the logic in the test_jinoforum_tp_get_untracked_jinoforums() function.
     */
    public function test_jinoforum_tp_get_untracked_jinoforums() {
        global $CFG;

        $this->resetAfterTest();

        $useron = $this->getDataGenerator()->create_user(array('trackjinoforums' => 1));
        $useroff = $this->getDataGenerator()->create_user(array('trackjinoforums' => 0));
        $course = $this->getDataGenerator()->create_course();
        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OFF); // Off.
        $jinoforumoff = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_FORCED); // On.
        $jinoforumforce = $this->getDataGenerator()->create_module('jinoforum', $options);

        $options = array('course' => $course->id, 'trackingtype' => JINOFORUM_TRACKING_OPTIONAL); // Optional.
        $jinoforumoptional = $this->getDataGenerator()->create_module('jinoforum', $options);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        // On user with force on.
        $result = jinoforum_tp_get_untracked_jinoforums($useron->id, $course->id);
        $this->assertEquals(1, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));

        // Off user with force on.
        $result = jinoforum_tp_get_untracked_jinoforums($useroff->id, $course->id);
        $this->assertEquals(2, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        // On user with force off.
        $result = jinoforum_tp_get_untracked_jinoforums($useron->id, $course->id);
        $this->assertEquals(1, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));

        // Off user with force off.
        $result = jinoforum_tp_get_untracked_jinoforums($useroff->id, $course->id);
        $this->assertEquals(3, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));

        // Stop tracking so we can test again.
        jinoforum_tp_stop_tracking($jinoforumforce->id, $useron->id);
        jinoforum_tp_stop_tracking($jinoforumoptional->id, $useron->id);
        jinoforum_tp_stop_tracking($jinoforumforce->id, $useroff->id);
        jinoforum_tp_stop_tracking($jinoforumoptional->id, $useroff->id);

        // Allow force.
        $CFG->jinoforum_allowforcedreadtracking = 1;

        // On user with force on.
        $result = jinoforum_tp_get_untracked_jinoforums($useron->id, $course->id);
        $this->assertEquals(2, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));

        // Off user with force on.
        $result = jinoforum_tp_get_untracked_jinoforums($useroff->id, $course->id);
        $this->assertEquals(2, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));

        // Don't allow force.
        $CFG->jinoforum_allowforcedreadtracking = 0;

        // On user with force off.
        $result = jinoforum_tp_get_untracked_jinoforums($useron->id, $course->id);
        $this->assertEquals(3, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));

        // Off user with force off.
        $result = jinoforum_tp_get_untracked_jinoforums($useroff->id, $course->id);
        $this->assertEquals(3, count($result));
        $this->assertEquals(true, isset($result[$jinoforumoff->id]));
        $this->assertEquals(true, isset($result[$jinoforumoptional->id]));
        $this->assertEquals(true, isset($result[$jinoforumforce->id]));
    }

    /**
     * Test subscription using automatic subscription on create.
     */
    public function test_jinoforum_auto_subscribe_on_create() {
        global $CFG;

        $this->resetAfterTest();

        $usercount = 5;
        $course = $this->getDataGenerator()->create_course();
        $users = array();

        for ($i = 0; $i < $usercount; $i++) {
            $user = $this->getDataGenerator()->create_user();
            $users[] = $user;
            $this->getDataGenerator()->enrol_user($user->id, $course->id);
        }

        $options = array('course' => $course->id, 'forcesubscribe' => JINOFORUM_INITIALSUBSCRIBE); // Automatic Subscription.
        $jinoforum = $this->getDataGenerator()->create_module('jinoforum', $options);

        $result = jinoforum_subscribed_users($course, $jinoforum);
        $this->assertEquals($usercount, count($result));
        foreach ($users as $user) {
            $this->assertTrue(jinoforum_is_subscribed($user->id, $jinoforum));
        }
    }

    /**
     * Test subscription using forced subscription on create.
     */
    public function test_jinoforum_forced_subscribe_on_create() {
        global $CFG;

        $this->resetAfterTest();

        $usercount = 5;
        $course = $this->getDataGenerator()->create_course();
        $users = array();

        for ($i = 0; $i < $usercount; $i++) {
            $user = $this->getDataGenerator()->create_user();
            $users[] = $user;
            $this->getDataGenerator()->enrol_user($user->id, $course->id);
        }

        $options = array('course' => $course->id, 'forcesubscribe' => JINOFORUM_FORCESUBSCRIBE); // Forced subscription.
        $jinoforum = $this->getDataGenerator()->create_module('jinoforum', $options);

        $result = jinoforum_subscribed_users($course, $jinoforum);
        $this->assertEquals($usercount, count($result));
        foreach ($users as $user) {
            $this->assertTrue(jinoforum_is_subscribed($user->id, $jinoforum));
        }
    }

    /**
     * Test subscription using optional subscription on create.
     */
    public function test_jinoforum_optional_subscribe_on_create() {
        global $CFG;

        $this->resetAfterTest();

        $usercount = 5;
        $course = $this->getDataGenerator()->create_course();
        $users = array();

        for ($i = 0; $i < $usercount; $i++) {
            $user = $this->getDataGenerator()->create_user();
            $users[] = $user;
            $this->getDataGenerator()->enrol_user($user->id, $course->id);
        }

        $options = array('course' => $course->id, 'forcesubscribe' => JINOFORUM_CHOOSESUBSCRIBE); // Subscription optional.
        $jinoforum = $this->getDataGenerator()->create_module('jinoforum', $options);

        $result = jinoforum_subscribed_users($course, $jinoforum);
        // No subscriptions by default.
        $this->assertEquals(0, count($result));
        foreach ($users as $user) {
            $this->assertFalse(jinoforum_is_subscribed($user->id, $jinoforum));
        }
    }

    /**
     * Test subscription using disallow subscription on create.
     */
    public function test_jinoforum_disallow_subscribe_on_create() {
        global $CFG;

        $this->resetAfterTest();

        $usercount = 5;
        $course = $this->getDataGenerator()->create_course();
        $users = array();

        for ($i = 0; $i < $usercount; $i++) {
            $user = $this->getDataGenerator()->create_user();
            $users[] = $user;
            $this->getDataGenerator()->enrol_user($user->id, $course->id);
        }

        $options = array('course' => $course->id, 'forcesubscribe' => JINOFORUM_DISALLOWSUBSCRIBE); // Subscription prevented.
        $jinoforum = $this->getDataGenerator()->create_module('jinoforum', $options);

        $result = jinoforum_subscribed_users($course, $jinoforum);
        // No subscriptions by default.
        $this->assertEquals(0, count($result));
        foreach ($users as $user) {
            $this->assertFalse(jinoforum_is_subscribed($user->id, $jinoforum));
        }
    }
}
