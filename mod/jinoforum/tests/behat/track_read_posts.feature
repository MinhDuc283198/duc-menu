@mod @mod_jinoforum
Feature: A teacher can set one of 3 possible options for tracking read jinoforum posts
  In order to ease the jinoforum posts follow up
  As a user
  I need to distinct the unread posts from the read ones

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email | trackjinoforums |
      | student1 | Student | 1 | student1@asd.com | 1 |
      | student2 | Student | 2 | student2@asd.com | 0 |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1 | 0 |
    And the following "course enrolments" exist:
      | user | course | role |
      | student1 | C1 | student |
      | student2 | C1 | student |
    And I log in as "admin"
    And I follow "Course 1"
    And I turn editing mode on

  @javascript
  Scenario: Tracking jinoforum posts off
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Off |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I log out
    When I log in as "student1"
    And I follow "Course 1"
    Then I should not see "1 unread post"
    And I follow "Test jinoforum name"
    And I should not see "Track unread posts"

  @javascript
  Scenario: Tracking jinoforum posts optional with user tracking on
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Optional |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I log out
    When I log in as "student1"
    And I follow "Course 1"
    Then I should see "1 unread post"
    And I follow "Test jinoforum name"
    And I follow "Don't track unread posts"
    And I wait to be redirected
    And I follow "Course 1"
    And I should not see "1 unread post"
    And I follow "Test jinoforum name"
    And I follow "Track unread posts"
    And I wait to be redirected
    And I click on "1" "link" in the "Admin User" "table_row"
    And I follow "Course 1"
    And I should not see "1 unread post"

  @javascript
  Scenario: Tracking jinoforum posts optional with user tracking off
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Optional |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I log out
    When I log in as "student2"
    And I follow "Course 1"
    Then I should not see "1 unread post"
    And I follow "Test jinoforum name"
    And I should not see "Track unread posts"

  @javascript
  Scenario: Tracking jinoforum posts forced with user tracking on
    Given I set the following administration settings values:
      | Allow forced read tracking | 1 |
    And I follow "Home"
    And I follow "Course 1"
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Force |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I log out
    When I log in as "student1"
    And I follow "Course 1"
    Then I should see "1 unread post"
    And I follow "1 unread post"
    And I should not see "Don't track unread posts"
    And I follow "Test post subject"
    And I follow "Course 1"
    And I should not see "1 unread post"

  @javascript
  Scenario: Tracking jinoforum posts forced with user tracking off
    Given I set the following administration settings values:
      | Allow forced read tracking | 1 |
    And I follow "Home"
    And I follow "Course 1"
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Force |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I log out
    When I log in as "student2"
    And I follow "Course 1"
    Then I should see "1 unread post"
    And I follow "1 unread post"
    And I should not see "Don't track unread posts"
    And I follow "Test post subject"
    And I follow "Course 1"
    And I should not see "1 unread post"

  @javascript
  Scenario: Tracking jinoforum posts forced (with force disabled) with user tracking on
    Given I set the following administration settings values:
      | Allow forced read tracking | 1 |
    And I follow "Home"
    And I follow "Course 1"
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Force |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I set the following administration settings values:
      | Allow forced read tracking | 0 |
    And I log out
    When I log in as "student1"
    And I follow "Course 1"
    Then I should see "1 unread post"
    And I follow "Test jinoforum name"
    And I follow "Don't track unread posts"
    And I wait to be redirected
    And I follow "Course 1"
    And I should not see "1 unread post"
    And I follow "Test jinoforum name"
    And I follow "Track unread posts"
    And I wait to be redirected
    And I click on "1" "link" in the "Admin User" "table_row"
    And I follow "Course 1"
    And I should not see "1 unread post"

  @javascript
  Scenario: Tracking jinoforum posts forced (with force disabled) with user tracking off
    Given I set the following administration settings values:
      | Allow forced read tracking | 1 |
    And I follow "Home"
    And I follow "Course 1"
    Given I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
      | Read tracking | Force |
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Test post subject |
      | Message | Test post message |
    And I set the following administration settings values:
      | Allow forced read tracking | 0 |
    And I log out
    When I log in as "student2"
    And I follow "Course 1"
    Then I should not see "1 unread post"
    And I follow "Test jinoforum name"
    And I should not see "Track unread posts"
