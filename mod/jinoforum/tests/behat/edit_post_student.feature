@mod @mod_jinoforum
Feature: Students can edit or delete their jinoforum posts within a set time limit
  In order to refine jinoforum posts
  As a user
  I need to edit or delete my jinoforum posts within a certain period of time after posting

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email |
      | student1 | Student | 1 | student1@asd.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1 | 0 |
    And the following "course enrolments" exist:
      | user | course | role |
      | student1 | C1 | student |
    And I log in as "admin"
    And I expand "Site administration" node
    And I expand "Security" node
    And I follow "Site policies"
    And I set the field "Maximum time to edit posts" to "1 minutes"
    And I press "Save changes"
    And I am on homepage
    And I follow "Course 1"
    And I turn editing mode on
    And I add a "Jinoforum" to section "1" and I fill the form with:
      | Jinoforum name | Test jinoforum name |
      | Jinoforum type | Standard jinoforum for general use |
      | Description | Test jinoforum description |
    And I log out
    And I follow "Course 1"
    And I log in as "student1"
    And I add a new discussion to "Test jinoforum name" jinoforum with:
      | Subject | Jinoforum post subject |
      | Message | This is the body |

  Scenario: Edit jinoforum post
    When I follow "Jinoforum post subject"
    And I follow "Edit"
    And I set the following fields to these values:
      | Subject | Edited post subject |
      | Message | Edited post body |
    And I press "Save changes"
    And I wait to be redirected
    Then I should see "Edited post subject"
    And I should see "Edited post body"

  @javascript
  Scenario: Delete jinoforum post
    When I follow "Jinoforum post subject"
    And I follow "Delete"
    And I press "Continue"
    Then I should not see "Jinoforum post subject"

  @javascript
  Scenario: Time limit expires
    When I wait "70" seconds
    And I follow "Jinoforum post subject"
    Then I should not see "Edit" in the "region-main" "region"
    And I should not see "Delete" in the "region-main" "region"
