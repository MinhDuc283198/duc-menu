<?php

$string['modulename'] = 'Poll';
$string['modulename_help'] = '';
$string['modulename_link'] = 'mod/poll/view';
$string['modulenameplural'] = 'Polls';
$string['pluginadministration'] = 'Poll module administration';
$string['pluginname'] = 'Poll';
$string['poll:addinstance'] = 'Add a new poll';
$string['poll:deleteresponses'] = 'Delete any response';
$string['poll:downloadresponses'] = 'Download responses in a CSV file';
$string['poll:editquestions'] = 'Create and edit poll questions';
$string['poll:manage'] = 'Create and edit polls';
$string['poll:preview'] = 'Preview polls';
$string['poll:readallresponses'] = 'Read response summaries, subject to open times';
$string['poll:readallresponseanytime'] = 'Read all responses any time';
$string['poll:readownresponses'] = 'Read own responses';
$string['poll:submit'] = 'Complete and submit a poll';
$string['poll:view'] = 'View a poll';
$string['poll:viewsingleresponse'] = 'View complete individual responses';

$string['name'] = 'Name';
$string['opendate'] = 'Use Open Date';
$string['opendate_help'] = 'You can specify a date to open the questionnaire here. Check the check box, and select the date and time you want.
 Users will not be able to fill out the questionnaire before that date. If this is not selected, it will be open immediately.';
$string['closedate'] = 'Use Close Date';
$string['closedate_help'] = 'You can specify a date to close the questionnaire here. Check the check box, and select the date and time you want.
 Users will not be able to fill out the questionnaire after that date. If this is not selected, it will never be closed.';
$string['questionandsubmission'] = 'Question and submission settings';
$string['anonymous'] = 'Anonymous';
$string['anonymous_edit'] = 'Record user names';
$string['non_anonymous'] = 'User\'s name will be logged and shown with answers';
$string['multiplesubmit'] = 'Allow multiple submissions';
$string['multiplesubmit_help'] = 'If enabled for multiple submissions, users can submit feedback an unlimited number of times.';
$string['overview'] = 'Overview';
$string['editquestions'] = 'Edit questions';
$string['analysis'] = 'Analysis';
$string['showentries'] = 'Show entries';
$string['addquestion'] = 'Add question';
$string['invalidquestionid'] = 'Invalid question ID!';

$string['qtype:multichoice'] = 'Multiple choice';
$string['qtype:shortanswer'] = 'Short answer';
$string['qtype:scale'] = 'Rank(Scale 1...5)';
$string['qtype:numeric'] = 'Numeric(grade)';
$string['qtype:essay'] = 'Essay';
$string['required'] = 'Required';
$string['question:name'] = 'Name';
$string['question:text'] = 'Question text';
$string['question:sortorder'] = 'Sort order';
$string['multichoice:multiselect'] = 'Multiple selection';
$string['multichoice:radio'] = 'Single answer(radio button)';
$string['multichoice:check'] = 'Multiple answers(check boxes)';
$string['question:choices'] = 'Choices';
$string['choices:alignment'] = 'Choices Alignment';
$string['alignment:horizontal'] = 'Horizontal';
$string['alignment:vertical'] = 'Vertical';
$string['multichoice_choices_help'] = 'Enter one option per line for the user to select one or multiple answers from.';
$string['scale_choices_help'] = 'Enter one option per line for the user to select one or multiple answers from.';

$string['alreadysubmitted'] = 'You\'ve already completed this activity.';
$string['completetheform'] = 'Answer the questions...';
$string['isnotopen'] = 'The poll is not open';
$string['saveentries'] = 'Submit your answers';
$string['editquestion'] = 'Edit question';
$string['deletequestion'] = 'Delete question';
$string['confirmdeletequestion'] = 'Are you sure you want to delete this question?';

$string['question'] = 'Question';
$string['questions'] = 'Questions';
$string['completedpolls'] = 'Submitted answers';
$string['pollopen'] = 'Allow answers from';
$string['pollclose'] = 'Allow answers to';
$string['entriessaved'] = 'Your answers have been saved. Thank you.';
$string['notallowsubmit'] = 'Your are not allowed to answer this poll.';

$string['exporttoexcel'] = 'Export to Excel';
$string['responses'] = 'Responses';
$string['noresponse'] = 'No response';
$string['sum'] = 'Sum';
$string['average'] = 'Average';
$string['standarddeviation'] = 'Standard deviation';
$string['no.'] = 'No.';