<?php

$string['modulename'] = '설문';
$string['modulename_help'] = '';
$string['modulename_link'] = 'mod/poll/view';
$string['modulenameplural'] = '설문';
$string['pluginadministration'] = '설문 관리';
$string['pluginname'] = '설문';
$string['poll:addinstance'] = '새 설문 추가';
$string['poll:deleteresponses'] = '응답 삭제Delete any response';
$string['poll:downloadresponses'] = '응답을 CSV 파일로 다운로드';
$string['poll:editquestions'] = '설문 문제 생성 및 편집';
$string['poll:manage'] = '설문 생성 및 편집';
$string['poll:preview'] = '설문 미리보기';
$string['poll:readallresponses'] = '응답 요약 보기';
$string['poll:readallresponseanytime'] = '언제든지 모든 응답 보기';
$string['poll:readownresponses'] = '자신의 응답 보기';
$string['poll:submit'] = '설문 제출';
$string['poll:view'] = '설문 보기';
$string['poll:viewsingleresponse'] = '개별 응답 보기';

$string['name'] = '이름';
$string['opendate'] = '설문 시작 날짜';
$string['opendate_help'] = '설문 시작 날짜를 지정할 수 있습니다.
사용자는 해당 날짜 이전에 설문지를 작성할 수 없습니다 이 옵션을 선택하지 않으면 즉시 시작됩니다.';
$string['closedate'] = '설문 종료 날짜';
$string['closedate_help'] = '설문 종료 날짜를 지정할 수 있습니다.
 이 날짜 이후에는 사용자는 설문지를 작성할 수 없습니다. 이 옵션을 선택하지 않으면 설문은 닫히지 않습니다.';
$string['questionandsubmission'] = '제출 설정';
$string['anonymous'] = '익명';
$string['anonymous_edit'] = '사용자 이름 기록';
$string['non_anonymous'] = '사용자의 이름을 기록하고 답변과 함께 표시';
$string['multiplesubmit'] = '다중 제출 허용';
$string['multiplesubmit_help'] = '다중 제출을 허용하면 사용자는 설문에 무제한 응답할 수 있습니다.';
$string['overview'] = '요약';
$string['editquestions'] = '질문 편집';
$string['analysis'] = '분석';
$string['showentries'] = '응답 보기';
$string['addquestion'] = '질문 추가';
$string['invalidquestionid'] = '잘못된 질문 ID!';

$string['qtype:multichoice'] = '선택형';
$string['qtype:shortanswer'] = '단답형';
$string['qtype:scale'] = '다점척도';
$string['qtype:numeric'] = '숫자(점수)';
$string['qtype:essay'] = '에세이(논술형)';
$string['required'] = '필수';
$string['question:name'] = '이름';
$string['question:text'] = '질문 내용';
$string['question:sortorder'] = '순서';
$string['multichoice:multiselect'] = '다중 선택';
$string['multichoice:radio'] = '단답(라디오버튼)';
$string['multichoice:check'] = '복수답(체크박스)';
$string['question:choices'] = '선택 항목';
$string['choices:alignment'] = '정렬';
$string['alignment:horizontal'] = '가로';
$string['alignment:vertical'] = '세로';
$string['multichoice_choices_help'] = '한줄에 하나의 항목을 입력하세요. 각 줄이 사용자가 선택할 수 있는 항목으로 표시됩니다.';
$string['scale_choices_help'] = '한줄에 하나의 항목을 입력하세요. 각 줄이 사용자가 선택할 수 있는 항목으로 표시됩니다.
<br/>
<br/>각 선택항목에 점수 부여를 원하는 경우 아래 예제와 같이 "|"로 구분하고 뒤에 점수를 입력하세요. 점수를 입력하지 않은 경우 첫번째 항목 1 점을 시작으로 1 점씩 증가된 값이 부여됩니다.
<br/>
<br/>아주 쉬움|1
<br/>쉬움|2
<br/>보통|3
<br/>어려움|4
<br/>아주 어려움|5';

$string['alreadysubmitted'] = '이미 설문을 작성하셨습니다.';
$string['completetheform'] = '설문하기';
$string['isnotopen'] = '아직 설문에 참여할 수 없습니다.';
$string['saveentries'] = '설문 제출';
$string['editquestion'] = '질문 편집';
$string['deletequestion'] = '질문 삭제';
$string['confirmdeletequestion'] = '이 질문을 삭제하시겠습니까?';

$string['question'] = '질문';
$string['questions'] = '질문들';
$string['completedpolls'] = '제출된 설문';
$string['pollopen'] = '설문 제출 시작';
$string['pollclose'] = '설문 제출 종료';
$string['entriessaved'] = '저장되었습니다. 감사합니다.';
$string['notallowsubmit'] = '이 설문에 응답할 수 없습니다.';

$string['exporttoexcel'] = '엑셀로 내보내기';
$string['responses'] = '응답들';
$string['noresponse'] = '제출된 응답 없음';
$string['sum'] = '합계';
$string['average'] = '평균';
$string['standarddeviation'] = '표준편차';
$string['no.'] = '번호';