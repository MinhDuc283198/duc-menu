<?php


require_once('../../config.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');
require_once($CFG->dirroot.'/mod/poll/poll_form.php');


$id = required_param('id', PARAM_INT);
$delete = optional_param('delete', false, PARAM_INT);

if (! $cm = get_coursemodule_from_id('poll', $id)) {
    print_error('invalidcoursemodule');
}
if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}
if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
    print_error('invalidcoursemodule');
}

$url = new moodle_url('/mod/poll/edit.php', array('id'=>$cm->id));
$current_tab = 'edit';


$context = context_module::instance($cm->id);
require_login($course, false, $cm);
require_capability('mod/poll:editquestions', $context);

if ($delete) {
    require_sesskey();
    $items = poll_get_questions($poll->id);
    if (isset($items[$delete])) {
        poll_delete_question($delete);
    }
    redirect($url);
}


$PAGE->set_url('/mod/poll/edit.php', array('id'=>$cm->id));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($poll->name);

echo $OUTPUT->header();

echo $OUTPUT->heading(format_string($poll->name) );

/// print the tabs
require('tabs.php');


$select = new single_select(new moodle_url('/mod/poll/edit_question.php', array('cmid' => $id, 'sesskey' => sesskey())),
                'type', poll_get_question_types());
$select->label = get_string('addquestion', 'mod_poll');
echo $OUTPUT->render($select);

$form = new poll_form(poll_form::MODE_EDIT, $cm, $poll, 'poll_edit_form');
$form->display();

echo $OUTPUT->footer();
