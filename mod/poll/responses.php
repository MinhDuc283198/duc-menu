<?php


require_once('../../config.php');
require_once('lib.php');


$id = required_param('id', PARAM_INT);

if (! $cm = get_coursemodule_from_id('poll', $id)) {
    print_error('invalidcoursemodule');
}
if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}
if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
    print_error('invalidcoursemodule');
}

$current_tab = 'responses';


$context = context_module::instance($cm->id);
require_login($course, false, $cm);
require_capability('mod/poll:viewreports', $context);

$PAGE->set_url('/mod/poll/responses.php', array('id'=>$cm->id));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($poll->name);

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($poll->name));

/// print the tabs
require('tabs.php');

echo $OUTPUT->footer();
