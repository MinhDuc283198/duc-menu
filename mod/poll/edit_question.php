<?php

require_once('../../config.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');

$id = optional_param('id', false, PARAM_INT);
if (!$id) {
    $cmid = required_param('cmid', PARAM_INT);
    $type = required_param('type', PARAM_ALPHA);
}

if($id) {
    if (! $question = $DB->get_record('poll_question', array('id' => $id))) {
        print_error('invalidquestionid', 'mod_poll');
    }
    if (! $poll = $DB->get_record("poll", array("id" => $question->poll))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $poll->course))) {
        print_error('coursemisconf');
    }
    
    if (! $cm = get_coursemodule_from_instance("poll", $poll->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
} else {
    if (! $cm = get_coursemodule_from_id('poll', $cmid)) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }
    if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }
    
    //
    $question = (object)['id' => 0,
        'poll' => $poll->id,
        'name' => '',
        'questiontype' => $type,
        'questiontext' => '',
        'sortorder' => -1,
        'required' => 0,
        'options' => ''];
}

$current_tab = 'edit';


$context = context_module::instance($cm->id);
require_login($course, false, $cm);
require_capability('mod/poll:editquestions', $context);

$PAGE->set_url('/mod/poll/edit.php', array('id'=>$cm->id));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($poll->name);

echo $OUTPUT->header();

echo $OUTPUT->heading(format_string($poll->name) );

/// print the tabs
require('tabs.php');

$question = poll_get_question_class($question);

$form = $question->build_editform($cm);


if ($form->get_data()) {
    if ($question = $form->save_question()) {
        redirect(new moodle_url('/mod/poll/edit.php', array('id'=>$cm->id)));
    }
} else if($form->is_cancelled()) {
    redirect(new moodle_url('/mod/poll/edit.php', array('id'=>$cm->id)));
    exit;
}


$form->display();

echo $OUTPUT->footer();
