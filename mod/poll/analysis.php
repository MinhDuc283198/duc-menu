<?php


require_once('../../config.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');


$id = required_param('id', PARAM_INT);

if (! $cm = get_coursemodule_from_id('poll', $id)) {
    print_error('invalidcoursemodule');
}
if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}
if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
    print_error('invalidcoursemodule');
}

$current_tab = 'analysis';


$context = context_module::instance($cm->id);
require_login($course, false, $cm);
require_capability('mod/poll:viewreports', $context);

$PAGE->set_url('/mod/poll/analysis.php', array('id'=>$cm->id));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($poll->name);

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($poll->name));

/// print the tabs
require('tabs.php');

// Button "Export to excel".
if (has_capability('mod/poll:viewreports', $context)) {
    echo $OUTPUT->container_start('form-buttons');
    $aurl = new moodle_url('/mod/poll/analysis_to_excel.php', ['sesskey' => sesskey(), 'id' => $id]);
    echo $OUTPUT->single_button($aurl, get_string('exporttoexcel', 'poll'));
    echo $OUTPUT->container_end();
}

$summary = new stdClass();
$summary->completedcount = poll_count_completed_responses($poll->id);
$summary->itemscount = poll_count_questions($poll->id);
echo $OUTPUT->render_from_template('mod_poll/summary', $summary);

$questions = poll_get_questions($poll->id);

foreach($questions as $question) {
    $qobject = poll_get_question_class($question);
    $qobject->print_analysed();
}
        
echo $OUTPUT->footer();
