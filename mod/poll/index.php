<?php

require_once("../../config.php");
require_once($CFG->dirroot . '/mod/poll/lib.php');
require_once($CFG->dirroot . '/mod/poll/locallib.php');
$id = required_param('id', PARAM_INT);

$url = new moodle_url('/mod/poll/index.php', array('id'=>$id));

$PAGE->set_url($url);

if (!$course = $DB->get_record('course', array('id'=>$id))) {
    print_error('invalidcourseid');
}

$context = context_course::instance($course->id);

require_login($course);
$PAGE->set_pagelayout('incourse');

/// Print the page header
$strpolls = get_string("modulenameplural", "poll");
$strpoll  = get_string("modulename", "poll");

$PAGE->navbar->add($strpolls);
$PAGE->set_heading($course->fullname);
$PAGE->set_title(get_string('modulename', 'poll').' '.get_string('activities'));
echo $OUTPUT->header();
echo $OUTPUT->heading($strpolls);

/// Get all the appropriate data

if (! $polls = get_all_instances_in_course("poll", $course)) {
    $url = new moodle_url('/course/view.php', array('id'=>$course->id));
    notice(get_string('thereareno', 'moodle', $strpolls), $url);
    die;
}

$usesections = course_format_uses_sections($course->format);

/// Print the list of instances (your module will probably extend this)

$timenow = time();
$strname  = get_string("name");
$strresponses = get_string('responses', 'poll');

$table = new html_table();

if ($usesections) {
    $strsectionname = get_string('sectionname', 'format_'.$course->format);
    if (has_capability('mod/poll:viewreports', $context)) {
        $table->head  = array ($strsectionname, $strname, $strresponses);
        $table->align = array ("center", "left", 'center');
    } else {
        $table->head  = array ($strsectionname, $strname);
        $table->align = array ("center", "left");
    }
} else {
    if (has_capability('mod/poll:viewreports', $context)) {
        $table->head  = array ($strname, $strresponses);
        $table->align = array ("left", "center");
    } else {
        $table->head  = array ($strname);
        $table->align = array ("left");
    }
}


foreach ($polls as $poll) {
    //get the responses of each poll
    $viewurl = new moodle_url('/mod/poll/view.php', array('id'=>$poll->coursemodule));

    $dimmedclass = $poll->visible ? '' : 'class="dimmed"';
    $link = '<a '.$dimmedclass.' href="'.$viewurl->out().'">'.$poll->name.'</a>';

    if ($usesections) {
        $tabledata = array (get_section_name($course, $poll->section), $link);
    } else {
        $tabledata = array ($link);
    }
    if (has_capability('mod/poll:viewreports', $context)) {
        $completed_poll_count = poll_get_completeds_count($poll);
        $tabledata[] = $completed_poll_count;
    }

    $table->data[] = $tabledata;

}

echo "<br />";

echo html_writer::table($table);

/// Finish the page

echo $OUTPUT->footer();

