<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/poll/backup/moodle2/backup_poll_stepslib.php');
require_once($CFG->dirroot . '/mod/poll/backup/moodle2/backup_poll_settingslib.php');

/**
 * Provides the steps to perform one complete backup of the Feedback instance
 */
class backup_poll_activity_task extends backup_activity_task {

    /**
     * No specific settings for this activity
     */
    protected function define_my_settings() {
    }

    /**
     * Defines a backup step to store the instance data in the poll.xml file
     */
    protected function define_my_steps() {
        // poll only has one structure step
        $this->add_step(new backup_poll_activity_structure_step('poll structure', 'poll.xml'));
    }

    /**
     * Encodes URLs to the index.php and view.php scripts
     *
     * @param string $content some HTML text that eventually contains URLs to the activity instance scripts
     * @return string the content with the URLs encoded
     */
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot, "/");

        // Link to the list of polls
        $search="/(".$base."\/mod\/poll\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@FEEDBACKINDEX*$2@$', $content);

        // Link to poll view by moduleid
        $search="/(".$base."\/mod\/poll\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@FEEDBACKVIEWBYID*$2@$', $content);

        // Link to poll analyis by moduleid
        $search="/(".$base."\/mod\/poll\/analysis.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@FEEDBACKANALYSISBYID*$2@$', $content);

        return $content;
    }
}
