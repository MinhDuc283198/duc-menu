<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_poll
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_poll_activity_task
 */

/**
 * Define the complete poll structure for backup, with file and id annotations
 */
class backup_poll_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $poll = new backup_nested_element('poll', array('id'), array(
                                                'name',
                                                'intro',
                                                'introformat',
                                                'opendate',
                                                'closedate',
                                                'anonymous',
                                                'multiplesubmit',
                                                'timemodified'));

        $questions = new backup_nested_element('questions');

        $question = new backup_nested_element('question', array('id'), array(
                                                'name',
                                                'questiontype',
                                                'questiontext',
                                                'questiontextformat',
                                                'sortorder',
                                                'required',
                                                'options'));
        
        $question_choices = new backup_nested_element('question_choices');
        
        $question_choice = new backup_nested_element('question_choice', array('id'), array(
                                                'choicetext',
                                                'choicetextformat',
                                                'sortorder',
                                                'marks'));
        
        
        
        $responses = new backup_nested_element('responses');

        $response = new backup_nested_element('response', array('id'), array(
                                                'userid',
                                                'submit',
                                                'complete',
                                                'timestart',
                                                'timefinish',
                                                'timemodified'));

        $response_choices = new backup_nested_element('response_choices');
        
        $response_choice = new backup_nested_element('response_choice', array('id'), array(
                                                'response',
                                                'question',
                                                'choice'));
        
        $response_numerics = new backup_nested_element('response_numerics');
        
        $response_numeric = new backup_nested_element('response_numeric', array('id'), array(
                                                'response',
                                                'question',
                                                'choice'));
        
        $response_others = new backup_nested_element('response_others');
        
        $response_other = new backup_nested_element('response_other', array('id'), array(
                                                'response',
                                                'question',
                                                'value'));

        // Build the tree
        $poll->add_child($questions);
        $questions->add_child($question);
        $question->add_child($question_choices);
        $question_choices->add_child($question_choice);

        $poll->add_child($responses);
        $responses->add_child($response);
        
        $response_choices->add_child($response_choice);
        $response_numerics->add_child($response_numeric);
        $response_others->add_child($response_other);

        $response->add_child($response_choices);
        $response->add_child($response_numerics);
        $response->add_child($response_others);

        // Define sources
        $poll->set_source_table('poll', array('id' => backup::VAR_ACTIVITYID));
        $question->set_source_table('poll_question', array('poll' => backup::VAR_PARENTID));
        $question_choice->set_source_table('poll_question_choice', array('question' => backup::VAR_PARENTID));

        // All these source definitions only happen if we are including user info
        if ($userinfo) {
            $response->set_source_sql('
                SELECT *
                  FROM {poll_response}
                 WHERE poll = ?',
                array(backup::VAR_PARENTID));

            $response_choice->set_source_table('poll_response_choice', array('response' => backup::VAR_PARENTID));
            $response_numeric->set_source_table('poll_response_numeric', array('response' => backup::VAR_PARENTID));
            $response_other->set_source_table('poll_response_other', array('response' => backup::VAR_PARENTID));
        }

        // Define id annotations

        $response->annotate_ids('user', 'userid');
        //$response_choice->annotate_ids('poll_question', 'question');
        //$response_numeric->annotate_ids('poll_question', 'question');
        //$response_other->annotate_ids('poll_question', 'question');

        // Define file annotations

        $poll->annotate_files('mod_poll', 'intro', null);
        $question->annotate_files('mod_poll', 'questiontext', 'id');

        // Return the root element (poll), wrapped into standard activity structure
        return $this->prepare_activity_structure($poll);
    }

}
