<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_poll
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_poll_activity_task
 */

/**
 * Structure step to restore one poll activity
 */
class restore_poll_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('poll', '/activity/poll');
        $paths[] = new restore_path_element('poll_question', '/activity/poll/questions/question');
        $paths[] = new restore_path_element('poll_question_choice', '/activity/poll/questions/question/question_choices/question_choice');
        if ($userinfo) {
            $paths[] = new restore_path_element('poll_response', '/activity/poll/responses/response');
            $paths[] = new restore_path_element('poll_response_choice', '/activity/poll/responses/response/response_choices/response_choice');
            $paths[] = new restore_path_element('poll_response_numeric', '/activity/poll/responses/response/response_numerics/response_numeric');
            $paths[] = new restore_path_element('poll_response_other', '/activity/poll/responses/response/response_others/response_other');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_poll($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->opendate = $this->apply_date_offset($data->opendate);
        $data->closedata = $this->apply_date_offset($data->closedata);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the poll record
        $newitemid = $DB->insert_record('poll', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }

    protected function process_poll_question($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->poll = $this->get_new_parentid('poll');

        $newitemid = $DB->insert_record('poll_question', $data);
        $this->set_mapping('poll_question', $oldid, $newitemid, true);
    }
    
    protected function process_poll_question_choice($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->question = $this->get_new_parentid('poll_question');

        $newitemid = $DB->insert_record('poll_question_choice', $data);
        $this->set_mapping('poll_question_choice', $oldid, $newitemid, true);
    }

    protected function process_poll_response($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->poll = $this->get_new_parentid('poll');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('poll_response', $data);
        $this->set_mapping('poll_response', $oldid, $newitemid);
    }

    protected function process_poll_response_choice($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response = $this->get_new_parentid('poll_response');
        $data->question = $this->get_mappingid('poll_question', $data->question);
        $data->choice = $this->get_mappingid('poll_question_choice', $data->choice);

        $newitemid = $DB->insert_record('poll_response_choice', $data);
        $this->set_mapping('poll_response_choice', $oldid, $newitemid);
    }
    
    protected function process_poll_response_numeric($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response = $this->get_new_parentid('poll_response');
        $data->question = $this->get_mappingid('poll_question', $data->question);

        $newitemid = $DB->insert_record('poll_response_numeric', $data);
        $this->set_mapping('poll_response_numeric', $oldid, $newitemid);
    }
    
    protected function process_poll_response_other($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response = $this->get_new_parentid('poll_response');
        $data->question = $this->get_mappingid('poll_question', $data->question);

        $newitemid = $DB->insert_record('poll_response_other', $data);
        $this->set_mapping('poll_response_other', $oldid, $newitemid);
    }

    protected function after_execute() {
        $this->add_related_files('mod_poll', 'intro', null);
        $this->add_related_files('mod_poll', 'question', 'poll_question');
    }
}
