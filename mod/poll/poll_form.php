<?php

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');

class poll_form extends moodleform {
    /** @var int */
    const MODE_COMPLETE = 1;
    /** @var int */
    const MODE_EDIT = 2;
    /** @var int */
    const MODE_VIEW_RESPONSE = 3;
    
    protected $mode;
    protected $cm;
    protected $poll;
    
    public function __construct($mode, $cm, $poll, $formid, $customdata = null) {
        $this->mode = $mode;
        $this->cm = $cm;
        $this->poll = $poll;
        
        parent::__construct(null, $customdata, 'POST', '',
                array('id' => $formid, 'class' => 'poll_form'), true);
        
        // form class에 full-width-labels를 붙인다.
        $this->set_display_vertical();
    }
    
    protected function definition() {
        $mform = $this->_form;
        
        $questions = poll_get_questions($this->poll->id);
        
        foreach($questions as $q) {
            $question = poll_get_question_class($q);
            $question->poll_form_element($this);
        }
        
        
        if ($this->mode == self::MODE_COMPLETE) {
            $buttonarray = array();
            $buttonarray[] = &$mform->createElement('submit', 'savevalues', get_string('saveentries', 'poll'),
                    array('class' => 'form-submit'));
            $buttonarray[] = &$mform->createElement('cancel');
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
            $mform->closeHeaderBefore('buttonar');
        }
        
        $mform->addElement('hidden', 'id', $this->cm->id);
        $mform->setType('id', PARAM_INT);
    }
    
    public function get_mode() {
        return $this->mode;
    }
    
    public function is_frozen() {
        return $this->mode == self::MODE_VIEW_RESPONSE;
    }
    
    public function add_form_element($question, $element, $addrequiredrule = true, $setdefaultvalue = true) {
        global $OUTPUT;
        
        // Add element to the form.
        if (is_array($element)) {
            if ($this->is_frozen() && $element[0] === 'text') {
                // Convert 'text' element to 'static' when freezing for better display.
                $element = ['static', $element[1], $element[2]];
            }
            $element = call_user_func_array(array($this->_form, 'createElement'), $element);
        }
        $element = $this->_form->addElement($element);

        // Prepend standard CSS classes to the element classes.
        $attributes = $element->getAttributes();
        $class = !empty($attributes['class']) ? ' ' . $attributes['class'] : '';
        $attributes['class'] = "poll_questionlist poll-question-{$question->type}" . $class;
        $element->setAttributes($attributes);

        // Add required rule.
        if ($question->required && $addrequiredrule) {
            $this->_form->addRule($element->getName(), get_string('required'), 'required', null, 'client');
        }

        // Freeze if needed.
        if ($this->is_frozen()) {
            $element->freeze();
        }

        // Add red asterisks on required fields.
        if ($question->required) {
            $required = $OUTPUT->pix_icon('req', get_string('requiredelement', 'form'));
            $element->setLabel($element->getLabel() . $required);
            $this->hasrequired = true;
        }


        if ($this->mode == self::MODE_EDIT) {
            $this->enhance_name_for_edit($question, $element);
        }

        return $element;
    }
    
    protected function guess_element_id($question, $element) {
        if (!$id = $element->getAttribute('id')) {
            $attributes = $element->getAttributes();
            $id = $attributes['id'] = 'poll_question_' . $question->id;
            $element->setAttributes($attributes);
        }
        if ($element->getType() === 'group') {
            return 'fgroup_' . $id;
        }
        return 'fitem_' . $id;
    }
    
    protected function enhance_name_for_edit($question, $element) {
        global $OUTPUT;
        $menu = new action_menu();
        $menu->set_owner_selector('#' . $this->guess_element_id($question, $element));
        $menu->set_constraint('.poll_form');
        $menu->set_alignment(action_menu::TR, action_menu::BR);
        $menu->set_menu_trigger(get_string('edit'));
        $menu->prioritise = true;

        $questionobj = poll_get_question_class($question);
        $actions = $questionobj->edit_actions($question, $this->poll, $this->cm);
        foreach ($actions as $action) {
            $menu->add($action);
        }
        $editmenu = $OUTPUT->render($menu);

        $name = $element->getLabel();

        $name = html_writer::span('', 'itemdd', array('id' => 'poll_item_box_' . $question->id)) .
                html_writer::span($name, 'itemname') .
                html_writer::span($editmenu, 'itemactions');
        $element->setLabel(html_writer::span($name, 'itemtitle'));
    }
    
    public function add_form_group_element($question, $groupinputname, $name, $elements, $separator,
            $class = '') {
        $objects = array();
        foreach ($elements as $element) {
            $object = call_user_func_array(array($this->_form, 'createElement'), $element);
            $objects[] = $object;
        }
        $element = $this->add_form_element($question,
                ['group', $groupinputname, $name, $objects, $separator, false],
                false,
                false);
        
        if ($class !== '') {
            $attributes = $element->getAttributes();
            $attributes['class'] .= ' ' . $class;
            $element->setAttributes($attributes);
        }
        return $element;
    }
    
    public function add_validation_rule(callable $callback) {
        if ($this->mode == self::MODE_COMPLETE) {
            $this->_form->addFormRule($callback);
        }
    }
    
    public function set_element_type($element, $type) {
        if ($element instanceof HTML_QuickForm_element) {
            $element = $element->getName();
        }
        $this->_form->setType($element, $type);
    }
    
    public function display() {
        global $OUTPUT, $PAGE;
        // Finalize the form definition if not yet done.
        if (!$this->_definition_finalized) {
            $this->_definition_finalized = true;
            $this->definition_after_data();
        }

        $mform = $this->_form;

        // Add "This form has required fields" text in the bottom of the form.
        if (($mform->_required || $this->hasrequired) &&
               ($this->mode == self::MODE_COMPLETE)) {
            $element = $mform->addElement('static', 'requiredfields', '',
                    get_string('somefieldsrequired', 'form',
                            $OUTPUT->pix_icon('req', get_string('requiredelement', 'form'))));
            $element->setAttributes($element->getAttributes() + ['class' => 'requirednote']);
        }

        // Reset _required array so the default red * are not displayed.
        $mform->_required = array();

        // Move buttons to the end of the form.
        if ($this->mode == self::MODE_COMPLETE) {
            $mform->addElement('hidden', '__dummyelement');
            $buttons = $mform->removeElement('buttonar', false);
            $mform->insertElementBefore($buttons, '__dummyelement');
            $mform->removeElement('__dummyelement');
        }

        $this->_form->display();

        if ($this->mode == self::MODE_EDIT) {
            $PAGE->requires->js_call_amd('mod_poll/edit', 'setup');
        }
    }
}

