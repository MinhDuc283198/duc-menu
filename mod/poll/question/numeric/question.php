<?php

defined('MOODLE_INTERNAL') OR die('not allowed');
require_once($CFG->dirroot.'/mod/poll/question/questionbase.php');
require_once($CFG->dirroot.'/mod/poll/question/numeric/edit_numeric_form.php');

class poll_question_numeric extends poll_question_base {
    protected $type = 'numeric';
    
    public function build_editform($cm) {
        $form = new poll_edit_numeric_form($this, $cm);
        
        return $form;
    }

    public function poll_form_element($form) {
        $name = $this->get_display_name();
        $inputname = $this->type . '_' . $this->id;
        $form->add_form_element($this,
                ['text', $inputname, $name],
                true,
                false
                );
        $form->set_element_type($inputname, PARAM_NOTAGS);

        $form->add_validation_rule(function($values, $files) use ($item) {
            $inputname = $this->type . '_' . $this->id;
            
            $value = unformat_float($values[$inputname], true);
            
            if ($value === false) {
                return array($inputname => get_string('invalidnum', 'error'));
            }
            
            return true;
        });
    }

    public function question_template() {
        return 'mod_poll/question_numeric';
    }
    
    public function save_response($responseid, $value) {
        global $DB;
        
        if(empty($value)) {
            $value = 0;
        }
        
        $answer = new stdClass();
        $answer->response = $responseid;
        $answer->question = $this->id;
        $answer->choice = $value;
        $DB->insert_record('poll_response_numeric', $answer);
    }
    
    public function get_analysed() {
        global $DB;
        
        $context = new stdClass();
        
        $context->type = $this->type;
        $context->name = $this->question->sortorder. '. '.format_string($this->question->questiontext);
        
        $answers = $DB->get_records('poll_response_numeric', array('question' => $this->question->id));
        if($answers) {
            $sum = 0;
            $responses = array();
            foreach($answers as $answer) {
                $value = 0;
                if(!empty($answer->choice)) {
                    $value = $answer->choice;
                }
                $sum += $value;
                
                $responses[] = $value;
            }
            $context->responses = $responses;
            
            $avg = $sum / count($answers);
            $stdv = poll_stats_standard_deviation($responses);
            
            $context->sum = $sum;
            $context->avg = number_format($avg, 1, '.', '');
            $context->stdv = number_format($stdv, 1, '.', '');
        }
        
        return $context;
    }
    
    public function print_analysed() {
        global $OUTPUT;
        
        $context = $this->get_analysed();
        
        echo $OUTPUT->render_from_template('mod_poll/analysis_numeric', $context);
    }
    
    public function print_excel($worksheet, $row) {
        $context = $this->get_analysed();
        
        $worksheet->write_string($row, 0, strval($this->question->sortorder));
        $worksheet->write_string($row, 1, strval($this->question->questiontext));
        $row++;
        
        foreach($context->responses as $response) {
            $worksheet->write_string($row, 2, strval($response));
            $row++;
        }
        $worksheet->write_string($row++, 2, get_string('sum', 'poll').': '.$context->sum);
        $worksheet->write_string($row++, 2, get_string('average', 'poll').': '.$context->avg);
        $worksheet->write_string($row++, 2, get_string('standarddeviation', 'poll').': '.$context->stdv);
        
        return $row;
    }
}