<?php

defined('MOODLE_INTERNAL') OR die('not allowed');

require_once($CFG->dirroot.'/mod/poll/question/questionbase.php');
require_once($CFG->dirroot.'/mod/poll/question/multichoice/edit_multichoice_form.php');

class poll_question_multichoice extends poll_question_base {
    protected $type = 'multichoice';
    protected $choices = null;
    
    public function build_editform($cm) {
        $form = new poll_edit_multichoice_form($this, $cm);
        
        return $form;
    }

    public function get_options() {
        if(!empty($this->question->options)) {
            return unserialize($this->question->options);
        }
        
        return false;
    }
    
    public function get_choices() {
        global $DB;
        
        if(!empty($this->choices)) {
            return $this->choices;
        }
        
        if($this->question->id) {
            $choices = $DB->get_records('poll_question_choice', array('question'=>$this->question->id), 'sortorder ASC');
            
            $arr = array();
            foreach($choices as $choice) {
                $arr[] = $choice->choicetext;
            }
            
            $this->choices = implode("\n", $arr);
            
            return $this->choices;
        }
        
        return false;
    }
    
    private function get_choices_elements() {
        global $DB;
        
        $choices = $DB->get_records('poll_question_choice', array('question'=>$this->question->id), 'sortorder ASC');

        $arr = array();
        foreach($choices as $choice) {
            $arr[$choice->id] = $choice->choicetext;
        }
        
        return $arr;
    }

    public function poll_form_element($form) {
        $name = $this->get_display_name();
        $inputname = $this->type . '_' . $this->id;
        
        $class = '';
        
        $options = $this->get_options();
        
        $choices = $this->get_choices_elements();

        // Display group or radio or checkbox elements.
        $class .= ' multichoice-' . ($options['alignment'] ? 'vertical' : 'horizontal');
        $objs = [];
        if ($options['multiselect']) {
            $objs[] = ['hidden', $inputname.'[0]', 0];
            $form->set_element_type($inputname.'[0]', PARAM_INT);
            foreach ($choices as $idx => $label) {
                $objs[] = ['advcheckbox', $inputname.'['.$idx.']', '', $label, null, array(0, $idx)];
                $form->set_element_type($inputname.'['.$idx.']', PARAM_INT);
            }
            $element = $form->add_form_group_element($this, 'group_'.$inputname, $name, $objs, '<br/>', $class);
        } else {
            // Radio.
            if (!array_key_exists(0, $choices)) {
                // Always add '0' as hidden element, otherwise form submit data may not have this element.
                $objs[] = ['hidden', $inputname.'[0]'];
            }
            foreach ($choices as $idx => $label) {
                $objs[] = ['radio', $inputname.'[0]', '', $label, $idx];
            }
            $element = $form->add_form_group_element($this, 'group_'.$inputname, $name, $objs, ' ', $class);
            $form->set_element_type($inputname.'[0]', PARAM_INT);
        }
    }

    public function question_template() {
        $options = $this->get_options();
        
        if ($options['multiselect']) {
            return 'mod_poll/question_check';
        } else {
            return 'mod_poll/question_radio';
        }
    }
    
    public function save_response($responseid, $value) {
        global $DB;
        
        if(empty($value)) {
            $value = array();
        }
        
        foreach($value as $k => $v) {
            if(!empty($v)) {
                $answer = new stdClass();
                $answer->response = $responseid;
                $answer->question = $this->id;
                $answer->choice = $v;
                
                $DB->insert_record('poll_response_choice', $answer);
            }
        }
    }
    
    protected function get_analysed() {
        global $DB;
        
        $context = new stdClass();
        
        $context->type = $this->type;
        $context->name = $this->question->sortorder. '. '.format_string($this->question->questiontext);
        
        $answers = $DB->get_records_sql("SELECT qc.id, qc.choicetext, rc.choiced
FROM {poll_question_choice} qc
LEFT JOIN (SELECT question, choice, count(choice) AS choiced 
           FROM {poll_response_choice} 
           WHERE question = :questionid1
           GROUP BY question, choice) rc ON (rc.question = qc.question AND rc.choice = qc.id)
WHERE qc.question = :questionid2
ORDER BY qc.sortorder", array('questionid1' => $this->question->id, 'questionid2' => $this->question->id));
        if($answers) {
            $count = $DB->count_records('poll_response', array('poll'=>$this->question->poll));
            
            $responses = array();
            foreach($answers as $answer) {
                $value = 0;
                if(!empty($answer->choiced)) {
                    $value = $answer->choiced;
                }
                
                $percent = 0;
                if($count) {
                    $percent = ($value / $count) * 100;
                }
                if($percent > 0) {
                    $percent = number_format(($value / $count) * 100, 1, '.', '');
                }
                
                $response = new stdClass();
                $response->choicetext = $answer->choicetext;
                $response->choiced = $value;
                $response->percent = $percent;
                
                $responses[] = $response;
            }
            $context->responses = $responses;
        }
        
        return $context;
    }
    
    public function print_analysed() {
        global $OUTPUT;
        
        $context = $this->get_analysed();
        
        echo $OUTPUT->render_from_template('mod_poll/analysis_multichoice', $context);
    }
    
    public function print_excel($worksheet, $row) {
        $context = $this->get_analysed();
        
        $column = 0;
        
        $worksheet->write_string($row, $column++, strval($this->question->sortorder));
        $worksheet->write_string($row++, $column++, $this->question->questiontext);
        foreach($context->responses as $response) {
            $worksheet->write_string($row, $column++, strval($response->choicetext));
        }
        $row++;
        
        $column = 2;
        foreach($context->responses as $response) {
            $worksheet->write_string($row, $column++, strval($response->choiced));
        }
        $row++;
        
        $column = 2;
        foreach($context->responses as $response) {
            $worksheet->write_string($row, $column++, strval($response->percent));
        }
        $row++;
        
        return $row;
    }
}