<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/poll/question/edit_question_form.php');

class poll_edit_multichoice_form extends poll_edit_question_form {
    protected function definition_inner($mform) {
        $alignment = $mform->addElement('select',
                                            'multiselect',
                                            get_string('multichoice:multiselect', 'poll'),
                                            array(POLL_MULTICHOICE_RADIO=>get_string('multichoice:radio', 'poll'),
                                                POLL_MULTICHOICE_CHECK=>get_string('multichoice:check', 'poll')));
        $mform->setDefault('multiselect', POLL_MULTICHOICE_RADIO);
        
        $alignment = $mform->addElement('select',
                                            'alignment',
                                            get_string('choices:alignment', 'poll'),
                                            array(POLL_ALIGNMENT_VERTICAL=>get_string('alignment:vertical', 'poll'),
                                                POLL_ALIGNMENT_HORIZONTAL=>get_string('alignment:horizontal', 'poll')));
        $mform->setDefault('alignment', POLL_ALIGNMENT_VERTICAL);
        
        $mform->addElement('textarea', 'choices', get_string('question:choices', 'poll'), array('rows' => 10));
        $mform->addHelpButton('choices', 'multichoice_choices', 'poll');
        $mform->addRule('choices', null, 'required', null, 'client');
        
        $this->set_data($this->question);
    }
    
    protected function save_options($data) {
        global $DB;
        
        $options = array(
            'multiselect' => $data->multiselect,
            'alignment' => $data->alignment
        );
        $data->options = serialize($options);
        
        $DB->update_record('poll_question', $data);
        
        return true;
    }
    
    protected function save_choices($data) {
        global $DB;
        
        $DB->delete_records('poll_question_choice', array('question' => $data->id));
        
        $choices = explode("\n", $data->choices);
        
        $order = 1;
        foreach($choices as $choice) {
            $choicetext = trim($choice);
            
            $objchoice = new stdClass();
            $objchoice->question = $data->id;
            $objchoice->choicetext = $choicetext;
            $objchoice->choicetextformat = 0;
            $objchoice->sortorder = $order;
            $objchoice->marks = 0;
            
            $DB->insert_record('poll_question_choice', $objchoice);
            
            $order += 1;
        }
    }
}
