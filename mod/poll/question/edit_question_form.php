<?php

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');

abstract class poll_edit_question_form extends moodleform {
    protected $question;
    public $cm;
    
    public function __construct($question, $cm) {
        global $DB;

        $this->question = $question;
        $this->cm = $cm;

        parent::__construct('edit_question.php', null, 'post');
    }
    
    protected function definition() {
        $mform =& $this->_form;
        
        $mform->addElement('header', 'general', get_string('qtype:'.$this->question->type, 'poll'));
        
        $mform->addElement('advcheckbox', 'required', get_string('required', 'poll'), '' , null , array(0, 1));
        
        $mform->addElement('text',
                           'name',
                           get_string('question:name', 'poll'), 
                           array('rows' => 3));
        $mform->addRule('name', null, 'required', null, 'client');
        
        $mform->addElement('textarea',
                           'questiontext',
                           get_string('question:text', 'poll'));
        $mform->addRule('questiontext', null, 'required', null, 'client');
        
        $this->definition_inner($mform);
        
        $sortorderlist = $this->get_sortorder_list($this->cm->instance);
        $sortorder_select = $mform->addElement('select',
                                            'sortorder',
                                            get_string('question:sortorder', 'poll'),
                                            $sortorderlist);
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'cmid');
        $mform->setType('cmid', PARAM_INT);
        
        $mform->addElement('hidden', 'type');
        $mform->setType('type', PARAM_ALPHAEXT);
        
        $this->add_action_buttons(true);
        
        $this->set_data($this->question);
    }
    
    protected function definition_inner($mform) {
    }
    
    public function get_data() {
        $data = parent::get_data();
        
        if($data) {
            $data->poll = $this->cm->instance;
            $data->questiontype = $data->type;
        }
        
        return $data;
    }
    
    public function set_data($question) {
        $data = new stdClass();
        
        $data->id = $question->id;
        $data->cmid = $this->cm->id;
        $data->required = $question->required;
        $data->name = $question->name;
        $data->type = $question->questiontype;
        $data->name = $question->name;
        $data->questiontext = $question->questiontext;
        $data->sortorder = $question->sortorder;
        
        if($question->sortorder == -1) {
            $sortorder = $this->get_sortorder_list($this->cm->instance);
            $data->sortorder = count($sortorder);
        }
        
        if($options = $question->get_options()) {
            foreach($options as $key => $value) {
                $data->{$key} = $value;
            }
        }
        
        if($choices = $question->get_choices()) {
            $data->choices = $choices;
        }
        
        parent::set_data($data);
    }
    
    public function save_question() {
        global $DB;
        
        $data = $this->get_data();
        $data->options = '';
        
        $sortorder = -1;
        if($data->id) { // 수정
            // sortorder가 바뀐 경우 재정렬 해야 함
            $questionold = $DB->get_record('poll_question', array('id'=>$data->id));
            if($questionold->sortorder != $data->sortorder) {
                $sortorder = $data->sortorder;
                $data->sortorder = -1;
            
                $DB->execute("UPDATE {poll_question} 
                    SET sortorder = sortorder - 1 
                    WHERE poll = :poll 
                      AND sortorder > :sortorder", array('poll'=>$questionold->poll, 'sortorder'=>$questionold->sortorder));
            }
            
            $DB->update_record('poll_question', $data);
        } else {
            $sortorder = $data->sortorder;
            $data->sortorder = -1;
            
            $data->id = $DB->insert_record('poll_question', $data);
        }
        
        // sortorder 재정렬
        if($sortorder != -1) {
            $DB->execute("UPDATE {poll_question} 
                SET sortorder = sortorder + 1 
                WHERE poll = :poll 
                  AND sortorder >= :sortorder", array('poll'=>$data->poll, 'sortorder'=>$sortorder));
            
            $data->sortorder = $sortorder;
            
            $DB->set_field('poll_question', 'sortorder', $data->sortorder, array('id'=>$data->id));
        }
        
        $this->save_options($data);
        $this->save_choices($data);
        
        return true;
    }
    
    protected function save_options($data) {}
    protected function save_choices($data) {}
    
    private function get_sortorder_list($id) {
        global $DB;

        $sortorderlist = array();

        $count = $DB->count_records('poll_question', array('poll' => $id));
        if($this->question->sortorder == -1) {
            $count += 1;
        }

        for($i = 1; $i <= $count; $i++) {
            $sortorderlist[$i] = $i;
        }

        return $sortorderlist;
    }


}
