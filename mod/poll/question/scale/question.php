<?php

defined('MOODLE_INTERNAL') OR die('not allowed');

require_once($CFG->dirroot.'/mod/poll/question/questionbase.php');
require_once($CFG->dirroot.'/mod/poll/question/scale/edit_scale_form.php');

class poll_question_scale extends poll_question_base {
    protected $type = 'scale';
    protected $choices = null;
    
    public function build_editform($cm) {
        $form = new poll_edit_scale_form($this, $cm);
        
        return $form;
    }

    public function get_choices() {
        global $DB;
        
        if(!empty($this->choices)) {
            return $this->choices;
        }
        
        if($this->question->id) {
            $choices = $DB->get_records('poll_question_choice', array('question'=>$this->question->id), 'sortorder ASC');
            
            $arr = array();
            foreach($choices as $choice) {
                $arr[] = $choice->choicetext.'|'.$choice->marks;
            }
            
            $this->choices = implode("\n", $arr);
            
            return $this->choices;
        }
        
        return false;
    }
    
    private function get_choices_elements() {
        global $DB;
        
        $inputname = $this->type . '_' . $this->id;
        
        $choices = $DB->get_records('poll_question_choice', array('question'=>$this->question->id), 'sortorder ASC');

        $colwidth = (100 / count($choices)).'%';

        $rvalue = new stdClass();
        $rvalue->header = new stdClass();
        
        $headers = array();
        $rows = array();
        foreach($choices as $choice) {
            $header = new stdClass();
            $header->colwidth = $colwidth;
            $header->coltext = $choice->choicetext;
            $headers[] = $header;
            
            $row = new stdClass();
            $row->name = $inputname.'[0]';
            $row->value = $choice->id;
            $row->colstyle = 'text-align:center;';
            //$row->disabled = 1;
            //$row->checked = 1;
            $rows[] = $row;
        }
        $rvalue->header->cols = $headers;
        $rvalue->rows->cols = $rows;
        
        return $rvalue;
    }

    public function poll_form_element($form) {
        global $OUTPUT;
        
        $name = $this->get_display_name();
        $inputname = $this->type . '_' . $this->id;
        
        
        $options = $this->get_options();
        
        $choices = $this->get_choices_elements();
        $choices->twidth = '100%';

        $objs[] = array('hidden', $inputname.'[0]');
        
        $context = new stdClass();
        $context->choices = $choices;
        $html = $OUTPUT->render_from_template('mod_poll/question_scale', $context);
        $objs[] = array('static', '', $name, $html);
        $element = $form->add_form_group_element($this, 'group_'.$inputname, $name, $objs, ' ');
        $form->set_element_type($inputname.'[0]', PARAM_INT);
    }

    public function question_template() {
        return 'mod_poll/question_scale';
    }
    
    public function save_response($responseid, $value) {
       global $DB;
        
        if(empty($value)) {
            $value = array();
        }
        
        foreach($value as $k => $v) {
            if(!empty($v)) {
                $answer = new stdClass();
                $answer->response = $responseid;
                $answer->question = $this->id;
                $answer->choice = $v;
                
                $DB->insert_record('poll_response_choice', $answer);
            }
        }
    }
    
    protected function get_analysed() {
        global $DB;
        
        $context = new stdClass();
        
        $context->type = $this->type;
        $context->name = $this->question->sortorder. '. '.format_string($this->question->questiontext);
        
        $answers = $DB->get_records_sql("SELECT qc.id, qc.choicetext, qc.marks, rc.choiced
FROM {poll_question_choice} qc
LEFT JOIN (SELECT question, choice, count(choice) AS choiced 
           FROM {poll_response_choice} 
           WHERE question = :questionid1
           GROUP BY question, choice) rc ON (rc.question = qc.question AND rc.choice = qc.id)
WHERE qc.question = :questionid2
ORDER BY qc.sortorder", array('questionid1' => $this->question->id, 'questionid2' => $this->question->id));
        if($answers) {
            $count = $DB->count_records('poll_response', array('poll'=>$this->question->poll));
            
            $sum = 0;
            $responses = array();
            $values = array();
            foreach($answers as $answer) {
                $value = 0;
                if(!empty($answer->choiced)) {
                    $value = $answer->choiced;
                }
                $sum += $value * $answer->marks;
                if($value) { // 0 인 것들은 선택되지 않았으므로 제외한다.
                    $values[] = $value * $answer->marks;
                }
                
                $percent = 0;
                if($count) {
                    $percent = ($value / $count) * 100;
                }
                if($percent > 0) {
                    $percent = number_format(($value / $count) * 100, 1, '.', '');
                }
                
                $response = new stdClass();
                $response->choicetext = $answer->choicetext;
                $response->marks = $answer->marks;
                $response->choiced = $value;
                $response->percent = $percent;
                
                $responses[] = $response;
            }
            $context->responses = $responses;
            
            $avg = 0;
            if($count) {
                $avg = $sum / $count;
            }
            $stdv = poll_stats_standard_deviation($values);
            
            $context->sum = $sum;
            $context->avg = number_format($avg, 1, '.', '');
            $context->stdv = number_format($stdv, 1, '.', '');
            $context->responses = $responses;
            $context->colspan = count($responses);
            $context->width = ceil(100 / count($responses)).'%';
        }
        
        return $context;
    }
    
    public function print_analysed() {
        global $DB, $OUTPUT;
        
        $context = $this->get_analysed();
        
        echo $OUTPUT->render_from_template('mod_poll/analysis_scale', $context);
    }
    
    public function print_excel($worksheet, $row) {
        $context = $this->get_analysed();
        
        $column = 0;
        
        $worksheet->write_string($row, $column++, strval($this->question->sortorder));
        $worksheet->write_string($row++, $column++, strval($this->question->questiontext));
        foreach($context->responses as $response) {
            $worksheet->write_string($row, $column++, $response->choicetext.' ('.$response->marks.')');
        }
        $row++;
        
        $column = 2;
        foreach($context->responses as $response) {
            $worksheet->write_string($row, $column++, strval($response->choiced));
        }
        $row++;
        
        $column = 2;
        foreach($context->responses as $response) {
            $worksheet->write_string($row, $column++, strval($response->percent));
        }
        $row++;
        
        $worksheet->write_string($row++, 2, get_string('sum', 'poll').': '.$context->sum);
        $worksheet->write_string($row++, 2, get_string('average', 'poll').': '.$context->avg);
        $worksheet->write_string($row++, 2, get_string('standarddeviation', 'poll').': '.$context->stdv);
        
        return $row;
    }
}