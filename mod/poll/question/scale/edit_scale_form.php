<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/poll/question/edit_question_form.php');

class poll_edit_scale_form extends poll_edit_question_form {
    protected function definition_inner($mform) {
        $mform->addElement('textarea', 'choices', get_string('question:choices', 'poll'), array('rows' => 10));
        $mform->addHelpButton('choices', 'scale_choices', 'poll');
        $mform->addRule('choices', null, 'required', null, 'client');
    }
    
    protected function save_choices($data) {
        global $DB;
        
        $DB->delete_records('poll_question_choice', array('question' => $data->id));
        
        $choices = explode("\n", $data->choices);
        
        $order = 1;
        $marks = 1;
        foreach($choices as $choice) {
            $arr = explode('|', $choice);
            $choicetext = trim($arr[0]);
            if(count($arr) == 2) {
                $marks = trim($arr[1]);
            }
            
            $objchoice = new stdClass();
            $objchoice->question = $data->id;
            $objchoice->choicetext = $choicetext;
            $objchoice->choicetextformat = 0;
            $objchoice->sortorder = $order;
            $objchoice->marks = $marks;
            
            $DB->insert_record('poll_question_choice', $objchoice);
            
            $order += 1;
            $marks += 1;
        }
    }
}
