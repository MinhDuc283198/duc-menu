<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/poll/lib.php');

abstract class poll_question_base {
    protected $type;
    protected $question;
    
    public function __construct($question) {
        $this->question = $question;
    }
    
    // ->{$key}
    public function __get($key) {
        if (method_exists($this, 'get_'.$key)) {
            return $this->{'get_'.$key}();
        }
        
        if(property_exists($this->question, $key)) {
            return $this->question->{$key};
        }
        
        if (!property_exists($this, $key)) {
            throw new \coding_exception('Undefined property requested');
        }
        
        return $this->{$key};
    }
    
    public function get_options() {
        return false;
    }
    
    public function get_choices() {
        return false;
    }
    
    public function get_input_name() {
        return $this->type . '_' . $this->id;
    }
    
    public function get_display_name() {
        return $this->sortorder.'. '.$this->questiontext;
    }
    
    abstract public function save_response($responseid, $answer);
    
    abstract public function build_editform($cm);
    
    abstract public function poll_form_element($form);
    
    abstract function question_template();
    
    abstract public function print_analysed();
    abstract public function print_excel($worksheet, $row);
    
    public function edit_actions($question, $poll, $cm) {
        $actions = array();

        $strupdate = get_string('editquestion', 'poll');
        $actions['update'] = new action_menu_link_secondary(
            new moodle_url('/mod/poll/edit_question.php', array('id' => $question->id)),
            new pix_icon('t/edit', $strupdate, 'moodle', array('class' => 'iconsmall', 'title' => '')),
            $strupdate,
            array('class' => 'editing_update', 'data-action' => 'update')
        );

        $strdelete = get_string('deletequestion', 'poll');
        $actions['delete'] = new action_menu_link_secondary(
            new moodle_url('/mod/poll/edit.php', array('id' => $cm->id, 'delete' => $question->id, 'sesskey' => sesskey())),
            new pix_icon('t/delete', $strdelete, 'moodle', array('class' => 'iconsmall', 'title' => '')),
            $strdelete,
            array('class' => 'editing_delete', 'data-action' => 'delete')
        );

        return $actions;
    }
}
