<?php

defined('MOODLE_INTERNAL') OR die('not allowed');

require_once($CFG->dirroot.'/mod/poll/question/questionbase.php');
require_once($CFG->dirroot.'/mod/poll/question/shortanswer/edit_shortanswer_form.php');

class poll_question_shortanswer extends poll_question_base {
    protected $type = 'shortanswer';
    
    public function build_editform($cm) {
        $form = new poll_edit_shortanswer_form($this, $cm);
        
        return $form;
    }

    public function poll_form_element($form) {
        $name = $this->get_display_name();
        $inputname = $this->type . '_' . $this->id;
        $form->add_form_element($this,
                array('text', $inputname, $name));
        $form->set_element_type($inputname, PARAM_NOTAGS);
    }
    
    public function question_template() {
        return 'mod_poll/question_shortanswer';
    }

    public function save_response($responseid, $value) {
        global $DB;
        
        if(empty($value)) {
            $value = ' ';
        }
        
        $answer = new stdClass();
        $answer->response = $responseid;
        $answer->question = $this->id;
        $answer->value = $value;
        $DB->insert_record('poll_response_other', $answer);
    }
    
    public function get_analysed($tohtml = true) {
        global $DB;
        
        $context = new stdClass();
        
        $context->type = $this->type;
        $context->name = $this->question->sortorder. '. '.format_string($this->question->questiontext);
        
        $answers = $DB->get_records('poll_response_other', array('question' => $this->question->id));
        if($answers) {
            $responses = array();
            foreach($answers as $answer) {
                if($tohtml) {
                    $responses[] = text_to_html($answer->value);
                } else {
                    $responses[] = $answer->value;
                }
            }
            
            $context->responses = $responses;
        }
        
        return $context;
    }
    
    public function print_analysed() {
        global $OUTPUT;
        
        $context = $this->get_analysed();
        
        echo $OUTPUT->render_from_template('mod_poll/analysis_essay', $context);
    }
    
    public function print_excel($worksheet, $row) {
        $context = $this->get_analysed(false);
        
        $worksheet->write_string($row, 0, strval($this->question->sortorder));
        $worksheet->write_string($row, 1, strval($this->question->questiontext));
        $row++;
        
        foreach($context->responses as $response) {
            $worksheet->write_string($row++, 2, strval($response));
        }
        
        return $row;
    }
}