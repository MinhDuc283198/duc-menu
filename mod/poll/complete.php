<?php


require_once('../../config.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');
require_once($CFG->dirroot.'/mod/poll/poll_form.php');
require_once($CFG->dirroot.'/course/lib.php');


$id = required_param('id', PARAM_INT);

if (! $cm = get_coursemodule_from_id('poll', $id)) {
    print_error('invalidcoursemodule');
}
if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}
if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
    print_error('invalidcoursemodule');
}

$current_tab = 'edit';


$context = context_module::instance($cm->id);
require_login($course, false, $cm);

$PAGE->set_url('/mod/poll/edit.php', array('id'=>$cm->id));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($poll->name);

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($poll->name) );

if (has_capability('mod/poll:submit', $context)) {
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    
    if($DB->record_exists('poll_response', array('poll'=>$poll->id, 'userid'=>$USER->id))) {
        echo $OUTPUT->notification(get_string('alreadysubmitted', 'poll'));
        $url = course_get_url($course->id);
        echo $OUTPUT->continue_button($url);
    } else if (poll_is_open($poll)) {
        $form = new poll_form(poll_form::MODE_COMPLETE, $cm, $poll, 'poll_edit_form');
        if($fromform = $form->get_data()) {
            poll_save_responses($fromform, $poll->id);
            
            echo $OUTPUT->notification(get_string('entriessaved', 'poll'));
            $url = course_get_url($course->id);
            echo $OUTPUT->continue_button($url);
        } else if($form->is_cancelled()) {
            redirect(new moodle_url('view.php', array('id'=>$id)));
        } else {
            $form->display();
        }
    } else {
        echo $OUTPUT->notification(get_string('isnotopen', 'poll'));
        $url = course_get_url($course->id);
        echo $OUTPUT->continue_button($url);
    }
    echo $OUTPUT->box_end();
} else {
    echo $OUTPUT->notification(get_string('notallowsubmit', 'poll'));
    $url = course_get_url($course->id);
    echo $OUTPUT->continue_button($url);
}

echo $OUTPUT->footer();
