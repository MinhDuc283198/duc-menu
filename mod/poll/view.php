<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once("../../config.php");
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/mod/poll/locallib.php');

$id = optional_param('id', null, PARAM_INT);    // Course Module ID.
$a = optional_param('a', null, PARAM_INT);      // Or poll ID.


if ($id) {
    if (! $cm = get_coursemodule_from_id('poll', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }

} else {
    if (! $poll = $DB->get_record("poll", array("id" => $a))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $poll->course))) {
        print_error('coursemisconf');
    }
    if (! $cm = get_coursemodule_from_instance("poll", $poll->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
}

$current_tab = 'view';


// Check login and get context.
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);

$url = new moodle_url($CFG->wwwroot.'/mod/poll/view.php');
if (isset($id)) {
    $url->param('id', $id);
} else {
    $url->param('a', $a);
}

$PAGE->set_url($url);
$PAGE->set_context($context);

$PAGE->set_title(format_string($poll->name));
$PAGE->set_heading(format_string($course->fullname));

echo $OUTPUT->header();

// Print the tabs.
echo $OUTPUT->heading(format_string($poll->name) );

require('tabs.php');

// Show description.
echo $OUTPUT->box_start('generalbox poll_description');
$options = (object)array('noclean' => true);
echo format_module_intro('poll', $poll, $cm->id);
echo $OUTPUT->box_end();

$showsummary = false;

$summary = new stdClass();
//show some infos to the poll
if (has_capability('mod/poll:editquestions', $context)) {
    $completedcount = poll_count_completed_responses($poll->id);
    if(!$completedcount) {
        $completedcount = get_string('noresponse', 'poll');
    }
    $summary->completedcount = $completedcount;
    $summary->itemscount = poll_count_questions($poll->id);
    $showsummary = true;
}
if ($poll->opendate) {
    $summary->opendate = userdate($poll->opendate);
    $showsummary = true;
}
if ($poll->closedate) {
    $summary->closedate = userdate($poll->closedate);
    $showsummary = true;
}
    
if($showsummary) {
    //echo $OUTPUT->heading(get_string('overview', 'poll'), 3);
    echo $OUTPUT->render_from_template('mod_poll/summary', $summary);
}

if (has_capability('mod/poll:submit', $context)) {
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    
    if($DB->record_exists('poll_response', array('poll'=>$poll->id, 'userid'=>$USER->id))) {
        echo $OUTPUT->notification(get_string('alreadysubmitted', 'poll'));
    } else if (poll_is_open($poll)) {
        $completeurl = new moodle_url('/mod/poll/complete.php',
                array('id' => $id));
        
        echo html_writer::div(html_writer::link($completeurl, get_string('completetheform', 'poll')), 'complete-poll');
    } else {
        echo $OUTPUT->notification(get_string('isnotopen', 'poll'));
    }
    echo $OUTPUT->box_end();
}

echo $OUTPUT->footer();
