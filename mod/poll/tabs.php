<?php

defined('MOODLE_INTERNAL') OR die();

$tabs = array();
$row  = array();
$inactive = array();
$activated = array();


$context = context_module::instance($cm->id);

if (!isset($current_tab)) {
    $current_tab = '';
}

$viewurl = new moodle_url('/mod/poll/view.php', array('id' => $cm->id));
$row[] = new tabobject('view', $viewurl->out(), get_string('overview', 'poll'));
$urlparams = ['id' => $cm->id];

if (has_capability('mod/poll:editquestions', $context)) {
    $editurl = new moodle_url('/mod/poll/edit.php', $urlparams);
    $row[] = new tabobject('edit', $editurl->out(), get_string('editquestions', 'poll'));
}

if (has_capability('mod/poll:viewreports', $context)) {
    $analysisurl = new moodle_url('/mod/poll/analysis.php', $urlparams);
    $row[] = new tabobject('analysis', $analysisurl->out(), get_string('analysis', 'poll'));

//    $reporturl = new moodle_url('/mod/poll/responses.php', $urlparams);
//    $row[] = new tabobject('responses', $reporturl->out(), get_string('showentries', 'poll'));
}

if (count($row) > 1) {
    $tabs[] = $row;

    print_tabs($tabs, $current_tab, $inactive, $activated);
}

