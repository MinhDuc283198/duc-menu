<?php

defined('MOODLE_INTERNAL') || die();

define('POLL_ALIGNMENT_HORIZONTAL', 0);
define('POLL_ALIGNMENT_VERTICAL', 1);
define('POLL_MULTICHOICE_RADIO', 0);
define('POLL_MULTICHOICE_CHECK', 1);

function poll_supports($feature) {
    switch($feature) {
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return false;
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_GROUPINGS:
            return true;
        case FEATURE_GROUPS:
            return true;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;

        default:
            return null;
    }
}

function poll_add_instance($poll) {
    global $DB, $CFG;

    $poll->timemodified = time();

    // May have to add extra stuff in here.
    if (empty($poll->useopendate)) {
        $poll->opendate = 0;
    }
    if (empty($poll->useclosedate)) {
        $poll->closedate = 0;
    }

    if (!$poll->id = $DB->insert_record("poll", $poll)) {
        return false;
    }
    $modcontext = context_module::instance($poll->coursemodule);
    
    $completiontimeexpected = !empty($poll->completionexpected) ? $poll->completionexpected : null;
    \core_completion\api::update_completion_date_event($poll->coursemodule, 'poll', $poll->id, $completiontimeexpected);

    return $poll->id;
}

function poll_update_instance($poll) {
    global $DB, $CFG;

    $poll->timemodified = time();
    $poll->id = $poll->instance;

    // May have to add extra stuff in here.
    if (empty($poll->useopendate)) {
        $poll->opendate = 0;
    }
    if (empty($poll->useclosedate)) {
        $poll->closedate = 0;
    }

    $completiontimeexpected = !empty($poll->completionexpected) ? $poll->completionexpected : null;
    \core_completion\api::update_completion_date_event($poll->coursemodule, 'poll', $poll->id, $completiontimeexpected);

    return $DB->update_record("poll", $poll);
}

function poll_delete_instance($id) {
    global $DB, $CFG;
    require_once($CFG->dirroot.'/mod/poll/locallib.php');

    if (! $poll = $DB->get_record('poll', array('id' => $id))) {
        return false;
    }

    $result = true;


    if (! $DB->delete_records('poll', array('id' => $poll->id))) {
        $result = false;
    }

    // 질문 삭제
    $questions = $DB->get_records('poll_question', array('poll' => $poll->id));
    foreach ($questions as $question) {
        $DB->delete_records('poll_question', array('id' => $question->id));
        $DB->delete_records('poll_question_choice', array('question' => $question->id));
    }
    
    // 응답 삭제
    $responses = $DB->get_records('poll_response', array('poll' => $poll->id));
    foreach ($responses as $response) {
        $DB->delete_records('poll_response', array('id' => $response->id));
        $DB->delete_records('poll_response_choice', array('response' => $response->id));
        $DB->delete_records('poll_response_numeric', array('response' => $response->id));
        $DB->delete_records('poll_response_other', array('response' => $response->id));
    }

    return $result;
}

function poll_get_view_actions() {
    return array('view', 'view all');
}

function poll_get_post_actions() {
    return array('submit', 'update');
}


function poll_get_question_types() {
    $types = array(
        'multichoice' => get_string('qtype:multichoice', 'poll'),
        'shortanswer' => get_string('qtype:shortanswer', 'poll'),
        'scale' => get_string('qtype:scale', 'poll'),
        'numeric' => get_string('qtype:numeric', 'poll'),
        'essay' => get_string('qtype:essay', 'poll'),
        );
    
    return $types;
}
