<?php


require_once('../../config.php');
require_once($CFG->dirroot.'/mod/poll/lib.php');
require_once($CFG->dirroot.'/mod/poll/locallib.php');
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT);

if (! $cm = get_coursemodule_from_id('poll', $id)) {
    print_error('invalidcoursemodule');
}
if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}
if (! $poll = $DB->get_record("poll", array("id" => $cm->instance))) {
    print_error('invalidcoursemodule');
}


$context = context_module::instance($cm->id);
require_login($course, false, $cm);
require_capability('mod/poll:viewreports', $context);

$PAGE->set_url('/mod/poll/analysis.php', array('id'=>$cm->id));

// Creating a workbook.
$filename = "poll_" . clean_filename($poll->name) . ".xls";
$workbook = new MoodleExcelWorkbook($filename);

$worksheet1 = $workbook->add_worksheet();

$row = 0;

$summary = new stdClass();
$summary->completedcount = poll_count_completed_responses($poll->id);
$summary->itemscount = poll_count_questions($poll->id);

$worksheet1->write_string($row,
    0,
    get_string('completedpolls', 'poll').': '. strval($summary->completedcount));
$row++;

$worksheet1->write_string($row,
    0,
    get_string('questions', 'poll').': '. strval($summary->itemscount));
$row++;


$row += 2;
$worksheet1->write_string($row, 0, get_string('no.', 'poll'));
$worksheet1->write_string($row, 1, get_string('question', 'poll'));
$worksheet1->write_string($row, 2, get_string('responses', 'poll'));
$row++;

$questions = poll_get_questions($poll->id);
foreach($questions as $question) {
    $qobject = poll_get_question_class($question);
    $row = $qobject->print_excel($worksheet1, $row);
}
        
$workbook->close();