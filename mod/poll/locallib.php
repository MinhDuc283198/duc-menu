<?php

function poll_get_question_class($question) {
    global $CFG;

    $type = $question->questiontype;
    
    $questionclass = 'poll_question_'.$type;
    if (!class_exists($questionclass)) {
        require_once($CFG->dirroot.'/mod/poll/question/'.$type.'/question.php');
    }
    
    return new $questionclass($question);
}

function poll_get_questions($pollid) {
    global $DB;
    
    $questions = $DB->get_records('poll_question', array('poll' => $pollid), 'sortorder ASC');
    
    return $questions;
}

function poll_is_open($poll) {
    $checktime = time();
    return (!$poll->opendate || $poll->opendate <= $checktime) &&
        (!$poll->closedate || $poll->closedate >= $checktime);
}

function poll_delete_question($questionid, $renumber = true) {
    global $DB;

    $question = $DB->get_record('poll_question', array('id'=>$questionid));

    //deleting the files from the question
    $fs = get_file_storage();

    if (!$cm = get_coursemodule_from_instance('poll', $question->poll)) {
        return false;
    }
    $context = context_module::instance($cm->id);

    $questionfiles = $fs->get_area_files($context->id,
                                'mod_poll',
                                'question',
                                $question->id,
                                "id", false);

    if ($questionfiles) {
        $fs->delete_area_files($context->id, 'mod_poll', 'question', $question->id);
    }

    // 응답 삭제
    $DB->delete_records("poll_response_choice", array("question"=>$questionid));
    $DB->delete_records("poll_response_numeric", array("question"=>$questionid));
    $DB->delete_records("poll_response_other", array("question"=>$questionid));
    
    // 질문 삭제
    $DB->delete_records("poll_question", array("id"=>$questionid));
    $DB->delete_records("poll_question_choice", array("id"=>$questionid));
    
    if ($renumber) {
        poll_renumber_questions($question->poll);
    }
}

function poll_renumber_questions($pollid) {
    global $DB;

    $questions = $DB->get_records('poll_question', array('poll'=>$pollid), 'sortorder ASC');
    $order = 1;
    if ($questions) {
        foreach ($questions as $question) {
            $DB->set_field('poll_question', 'sortorder', $order, array('id'=>$question->id));
            $order++;
        }
    }
}

function poll_count_completed_responses($pollid) {
    global $DB;
    
    return $DB->count_records('poll_response', array('poll' => $pollid));
}

function poll_count_questions($pollid) {
    global $DB;
    
    return $DB->count_records('poll_question', array('poll' => $pollid));
}

function poll_save_responses($data, $pollid, $userid = 0) {
    global $DB, $USER;
    
    if(!$userid) {
        $userid = $USER->id;
    }
    
    $response = $DB->get_record('poll_response', array('poll'=>$pollid, 'userid'=>$userid));
    if(!$response) {
        $time = time();
        
        $response = new stdClass();
        $response->poll = $pollid;
        $response->userid = $userid;
        $response->submit = 1;
        $response->complete = 1;
        $response->timestart = $time;
        $response->timefinish = $time;
        $response->timemodified = $time;
        
        $response->id = $DB->insert_record('poll_response', $response);
    }
    
    $questions = poll_get_questions($pollid);

    foreach ($questions as $question) {
        $qobj = poll_get_question_class($question);
        
        $name = $qobj->get_input_name();
        
        $qobj->save_response($response->id, $data->{$name});
    }

    return $response->id;

}

function poll_get_completeds_count($poll) {
    global $DB;

    return $DB->count_records('poll_response', array('id' => $poll->id));
}

function poll_stats_standard_deviation(array $a, $sample = false) {
    $n = count($a);
    if ($n === 0) {
        trigger_error("The array has zero elements", E_USER_WARNING);
        return false;
    }
    if ($sample && $n === 1) {
        trigger_error("The array has only 1 element", E_USER_WARNING);
        return false;
    }
    $mean = array_sum($a) / $n;
    $carry = 0.0;
    foreach ($a as $val) {
        $d = ((double) $val) - $mean;
        $carry += $d * $d;
    };
    if ($sample) {
       --$n;
    }
    return sqrt($carry / $n);
}
