<?php
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$unixtime = optional_param('unixtime', $now, PARAM_RAW);
?>
<!--<h3 class="page_title">기수등록</h3>-->
<form name="" id="course_search" action="edu_class_add.submit.php" method="post">
    <input type="hidden" name="mod" value="">            
    <table cellpadding="0" cellspacing="0" class="haksa_table">                
        <tbody>
            <tr>
                <td class="field_title">외부DB유형</td>
                <td class="field_value">
                    <select name="dbtype">
                        <option value="0">DB 선택</option>
                        <option value="pgsql">pgsql</option>
                        <option value="mariadb">mariadb</option>
                        <option value="mysqli">mysqli</option>
                        <option value="mssql">mssql</option>
                        <option value="sqlsrv">sqlsrv</option>
                        <option value="oci">oci</option>
                    </select>                        
                </td>
            </tr>
            <tr>
                <td class="field_title">DB 주소(호스트 or IP)</td>
                <td class="field_value">
                    <div>
                        <input type="radio" name="hosttype" value="0" checked>호스트 
                        <input type="radio" name="hosttype" value="1">IP
                    </div>
                    <div class="hostaddr">
                        <input type="text" class="hostaddr" name="hostaddr">
                    </div>
                    <div class="ipaddr">
                        <input type="text" class="ipaddr" name="ipaddr1">.
                        <input type="text" class="ipaddr" name="ipaddr2">.
                        <input type="text" class="ipaddr" name="ipaddr3">.
                        <input type="text" class="ipaddr" name="ipaddr4">
                    </div>
                </td>
            </tr>
            <tr id="table_occasional">
                <td class="field_title">DB PORT</td>
                <td class="field_value">
                    <input type="text" class="haksa_input" name="port">
                </td>
            </tr>
            <tr id="table_class">
                <td class="field_title">DB NAME</td>
                <td class="field_value">
                   <input type="text" class="haksa_input" name="dbname">
                </td>
            </tr>
            <tr id="table_courseselect">
                <td class="field_title">DB USER</td>
                <td class="field_value">
                    <input type="text" class="haksa_input" name="dbuser">
                </td>
            </tr>
            <tr id="table_courseselect">
                <td class="field_title">DB PASSWORD</td>
                <td class="field_value">
                    <input type="text" class="haksa_input" name="dbpass">
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area rignt">
        <input type="submit" class="blue_btn" value="저장" onclick="return course_create_submit_check()">
    </div>
</form><!--Search Area2 End-->
