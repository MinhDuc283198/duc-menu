<?php

/**
 * See https://docs.moodle.org/dev/Access_API for details.
 *
 * @package    local_haksa
 * @copyright  2018, CHS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$capabilities = array(

    // Can configure plugin settings.
    'local/haksa:setup' => array(
        'riskbitmask'   => RISK_CONFIG,
        'captype'       => 'write ',
        'contextlevel'  => CONTEXT_SYSTEM,
        'archetypes'    => array(
            'manager'   => CAP_ALLOW
        )
    ),
);

