<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$type = optional_param('type', 0, PARAM_INT); 

use local_lmsdata\external_setup; 

require_login($course, true);
$context = context_system::instance();
$PAGE->set_context($context);

$url = new moodle_url('/local/haksa/index.php', array('id' => $id, 'type' => $type));
$PAGE->set_url($url);
$PAGE->requires->js('/local/haksa/localjs.js');

$PAGE->set_pagelayout('admin');

$strplural = get_string("pluginnameplural", "local_haksa");
$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);

echo $OUTPUT->header();
if(has_capability('local/haksa:setup', $context)) {
    //tab
    $row = array();
    $row[] = new tabobject('0', new moodle_url('/local/haksa/index.php', array('id' => $id, 'type' => 0)), get_string('setup', 'local_haksa'));
//    $row[] = new tabobject('1', new moodle_url('/local/haksa/index.php', array('id' => $id, 'type' => 1)), get_string('attendance:book', 'local_haksa'));
//    $row[] = new tabobject('2', new moodle_url('/local/haksa/index.php', array('id' => $id, 'type' => 2)), get_string('attendance:setup', 'local_haksa'));
    $rows[] = $row;
    print_tabs($rows, $type);
    
    $filename = array(
                    '0' => 'setup',
                    '1' => 'attendance_book',
                    '2' => 'attendance_setup'
                );
} 

if(array_key_exists($type, $filename)) {
    include $CFG->dirroot.'/local/haksa/'.$filename[$type].'.php';
} else {
    redirect($CFG->wwwroot);
}
echo $OUTPUT->footer();
?>

<script type="text/javascript">
</script>