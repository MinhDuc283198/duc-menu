<?php

defined('MOODLE_INTERNAL') || die;

if (is_siteadmin()) {
    
    $ADMIN->add('localplugins', new admin_externalpage('haksa', new lang_string('pluginname', 'local_haksa'), "$CFG->wwwroot/local/haksa/index.php", array('moodle/user:delete')));
}

