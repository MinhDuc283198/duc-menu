<?php
/**
 * 수강신청 submit 페이지(결제로직 포함)
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . "/chamktu/lib.php");
require_once($CFG->dirroot . '/local/lmsdata/lib.php');
require_once($CFG->dirroot . "/local/course_application/lib.php"); 
require_once('../../../INIpay5.3.9/libs/INIStdPayUtil.php');
require_once('../../../INIpay5.3.9/libs/HttpClient.php'); 

require_login();

$util = new INIStdPayUtil();

try {

    //#############################
    // 인증결과 파라미터 일괄 수신
    //#############################
    //		$var = $_REQUEST["data"];

    //#####################
    // 인증이 성공일 경우만
    //#####################
    if (strcmp("0000", $_REQUEST["resultCode"]) == 0) {

        //echo "####인증성공/승인요청####";
        echo "<br/>";

        //############################################
        // 1.전문 필드 값 설정(***가맹점 개발수정***)
        //############################################;

        $mid 			= $_REQUEST["mid"];     					// 가맹점 ID 수신 받은 데이터로 설정
        $signKey 		= "NVN5TVhqUGJYcC9LNFlTYTEwYUpRQT09"; 		// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
        $timestamp 		= $util->getTimestamp();   					// util에 의해서 자동생성
        $charset 		= "UTF-8";        							// 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
        $format 		= "JSON";        							// 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)

        $authToken 		= $_REQUEST["authToken"];   				// 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
        $authUrl 		= $_REQUEST["authUrl"];    					// 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
        $netCancel 		= $_REQUEST["netCancelUrl"];   				// 망취소 API url(수신 받은f값으로 설정, 임의 세팅 금지)

        $mKey 			= hash("sha256", $signKey);					// 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)

        //#####################
        // 2.signature 생성
        //#####################
        $signParam["authToken"] 	= $authToken;  	// 필수
        $signParam["timestamp"] 	= $timestamp;  	// 필수
        // signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
        $signature = $util->makeSignature($signParam);


        //#####################
        // 3.API 요청 전문 생성
        //#####################
        $authMap["mid"] 			= $mid;   		// 필수
        $authMap["authToken"] 		= $authToken; 	// 필수
        $authMap["signature"] 		= $signature; 	// 필수
        $authMap["timestamp"] 		= $timestamp; 	// 필수
        $authMap["charset"] 		= $charset;  	// default=UTF-8
        $authMap["format"] 			= $format;  	// default=XML


        try {

            $httpUtil = new HttpClient();

            //#####################
            // 4.API 통신 시작
            //#####################

            $authResultString = "";

            if ($httpUtil->processHTTP($authUrl, $authMap)) {
                $authResultString = $httpUtil->body;
                //echo "<p><b>RESULT DATA :</b> $authResultString</p>";
            } else {
                echo "Http Connect Error\n";
                echo $httpUtil->errormsg;

                throw new Exception("Http Connect Error");
            }

            //############################################################
            //5.API 통신결과 처리(***가맹점 개발수정***)
            //############################################################
            //echo "## 승인 API 결과 ##";

            $resultMap = json_decode($authResultString, true);

            echo "<pre>";
            echo "<table width='565' border='0' cellspacing='0' cellpadding='0'>";

            /*************************  결제보안 추가 2016-05-18 START ****************************/ 
            $secureMap["mid"]		= $mid;							//mid
            $secureMap["tstamp"]	= $timestamp;					//timestemp
            $secureMap["MOID"]		= $resultMap["MOID"];			//MOID
            $secureMap["TotPrice"]	= $resultMap["TotPrice"];		//TotPrice

            // signature 데이터 생성 
            $secureSignature = $util->makeSignatureAuth($secureMap);
            /*************************  결제보안 추가 2016-05-18 END ****************************/
            
            //print_object($secureSignature);
            //print_object($resultMap["authSignature"]);
            
            /* 장현준 
             * $secureSignature 와 $resultMap["authSignature"]를 비교하는 이유는 결재 승인 후 추가 보안 문제로 작성 된 것 
             * 원인 모르게 계속 해당 부분 오류가 나서 inicis 기술지원팀에 물어보니 꼭 필요한 절차가 아니라고 하여 해당 부분 주석처리
            */
            //if ((strcmp("0000", $resultMap["resultCode"]) == 0) && (strcmp($secureSignature, $resultMap["authSignature"]) == 0) ){	//결제보안 추가 2016-05-18
            if ((strcmp("0000", $resultMap["resultCode"]) == 0)){
            /*****************************************************************************
            * 여기에 가맹점 내부 DB에 결제 결과를 반영하는 관련 프로그램 코드를 구현한다.  

                     [중요!] 승인내용에 이상이 없음을 확인한 뒤 가맹점 DB에 해당건이 정상처리 되었음을 반영함
                                    처리중 에러 발생시 망취소를 한다.
            ******************************************************************************/
            //***************************************************************************************************************************************************************************************************************
                
            /* ★☆★☆★☆★☆★☆★☆ 장현준 ★☆★☆★☆★☆★☆★☆ */
            $ctime = required_param('merchantData', PARAM_RAW);
            $ctime = explode('=', $ctime)[1];
            $cookiename = $USER->id.'_'.$ctime;

            if(isset($_COOKIE[$cookiename])){
                $form_datas = $_COOKIE[$cookiename];

                //form_data json decode
                $form_datas = json_decode($form_datas);

                $datas = new stdClass();
                //클래스 키값에 대괄호가 붙어있는 키값이 있어 제거
                foreach($form_datas as $key => $form_data){
                    $key = str_replace('[','', $key);
                    $key = str_replace(']','', $key);
                    $datas->$key = $form_data;
                }

                $neiscode = str_replace('/','', $datas->neis);
                $locationnumber = str_replace('/','-', $datas->no);
                $phone = $datas->firstnumber;
                $phone .= str_replace('/','', $datas->number);

                $birthday = $datas->year.$datas->month.$datas->day;

                $userinfo = $DB->get_record('lmsdata_user', array('userid'=>$USER->id));
                
                $sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid AND paymentstatus != :paymentstatus';

                $application = $DB->get_record_sql($sql, array('userid'=>$USER->id, 'courseid'=>$datas->courseid, 'paymentstatus'=>'cancel'));
                if(empty($application)){
                    $new = new stdClass();
                    $new->userid = $USER->id;
                    $new->courseid = $datas->courseid;
                    $new->usertypecode = $datas->usertypecode;
                    $new->school = $datas->school;
                    $new->schoolcode = $datas->schoolcode;
                    if($datas->usertypecode == 20) {
                        $datas->qualification = $datas->qualification1;
                    } else {
                        $datas->qualification = $datas->qualification2;
                    }
                    $new->qualification = $datas->qualification;
                    $new->neiscode = $neiscode;
                    $new->locationnumber = $locationnumber;
                    $new->birthday = $birthday;
                    $new->phone = $phone;
                    $new->price = $datas->pay_price;
                    $new->paymentmeans = $datas->pay;
                    $new->paymentid = $resultMap["tid"];
                    $new->ordernumber = $resultMap["MOID"];
                    $new->email = $datas->email;

                    //결제정보 저장
                    if (isset($resultMap["payMethod"]) && strcmp("VBank", $resultMap["payMethod"]) == 0) { //가상계좌
                        $new->account = $resultMap["VACT_Num"];
                        $new->bankcode = $resultMap["VACT_BankCode"];
                        $new->accountname = $resultMap["VACT_Name"];
                        $new->paymentstatus = 'stand';
                        $new->status = 'stand';
                        $new->completionstatus = 0;
                    } else if (isset($resultMap["payMethod"]) && strcmp("DirectBank", $resultMap["payMethod"]) == 0) { //실시간계좌이체
                        $new->bankcode = $resultMap["ACCT_BankCode"];
                        $new->cshrresult = $resultMap["CSHRResultCode"];
                        $new->cshrtype = $resultMap["CSHRResultCode"];
                        if($datas->pay_price == $resultMap["TotPrice"]){
                            $new->paymentstatus = 'complete';
                            $new->paymentdate = time();
                            $new->status = 'apply';
                            $new->completionstatus = 0;
                        }
                    } else { //카드
                        $new->cardcode = $resultMap["CARD_Code"];
                        $new->cardnumber = $resultMap["CARD_Num"];
                        $new->installment = $resultMap["CARD_Quota"];
                        $new->pointuse = $resultMap["point"];
                        $new->allowcancel = $resultMap["CARD_PRTC_CODE"];
                        if($datas->pay_price == $resultMap["TotPrice"]){
                            $new->paymentstatus = 'complete';
                            $new->paymentdate = time();
                            $new->status = 'apply';
                            $new->completionstatus = 0;
                        }
                    }
                    
                    if($datas->discountck == 'on'){
                        $new->status = 'stand';
                        $new->completionstatus = 'no';
                        $new->groupcode = $datas->code;
                        $new->discounttype = 'group';
                        
                        $group = $DB->get_record('lmsdata_grouplist', array('groupcode'=>$code));
                        $group_apply = new stdClass();
                        $group_apply->groupid = $group->id;
                        $group_apply->userid = $USER->id;
                        $group_apply->classid = $datas->courseid;
                        $group_apply->timecreated = time();
                        $DB->insert_record('lmsdata_groupapply', $group_apply);
                    } else if($userinfo->usertypecode == 30){
                        $new->discounttype = 'member';
                    }
                    
                    $new->timecreated = time();

                    $applicationid = $DB->insert_record('lmsdata_course_applications', $new);
                    error_log ("[START : ".date("Y-m-d H:i:s")."]\n", 3, "/home/html/error_log");
                    $error_test = new stdClass();
                    $error_test->paymentstatus = $new->paymentstatus;
                    $error_test->userid = $USER->id;
                    $error_test->courseid = $datas->courseid;
                    $error_test->paymentid = $applicationid;
                    if($new->paymentmeans == 'vbank') {
                        $error_test->complete = 'vbank complete';
                    }
                    error_log (print_r($error_test, true)."\n", 3, "/home/html/error_log");
                    if($new->paymentstatus == 'complete'){
                        $sql = 'select mc.* from {lmsdata_class} lc join {course} mc on mc.id = lc.courseid where lc.id = :id';
                        $class = $DB->get_record_sql($sql, array('id'=>$datas->courseid));
                        $userinfo->roleid = 5;
                        set_assign_user($class, $userinfo);
                        $error_test->complete = 'card complete';
                    }
                    error_log (print_r($error_test->complete, true)."\n", 3, "/home/html/error_log");
                    error_log ("[END : ".date("Y-m-d H:i:s")."]\n", 3, "/home/html/error_log");
                    $application = $DB->get_record('lmsdata_course_applications',array('id'=>$applicationid));
                    
                    $apiresult = eduhope_set_training_history($application);
                    
                    $class = $DB->get_record('lmsdata_class',array('id'=>$application->courseid));
                    
                    $classtype = 1;
        
                    if($class->classtype == 2){
                        $classtype = 2;
                    }
                    
                    $e_templete = $DB->get_record('lmsdata_sendtemplate', array('sendtime'=>1, 'type'=>'email', 'classtype'=>$classtype));
                    $s_templete = $DB->get_record('lmsdata_sendtemplate', array('sendtime'=>1, 'type'=>'sms', 'classtype'=>$classtype));

                    $sql = 'SELECT lca.timecreated, lca.email, lca.userid, lca.price, lca.paymentstatus, lc.learningstart, lc.learningend, lc.evaluationstart, lc.evaluationend, lc.courseid, lc.classnum, lco.coursename, mu.lastname, mu.username, ls.consignmentplace
                            FROM {lmsdata_course_applications} lca
                            JOIN {lmsdata_class} lc ON lc.id = lca.courseid
                            JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
                            JOIN {user} mu ON mu.id = lca.userid
                            JOIN {lmsdata_schedule} ls on ls.id = lc.scheduleid
                            WHERE lca.id = :id AND lca.userid = :userid';
                    $application = $DB->get_record_sql($sql, array('id'=>$applicationid, 'userid'=>$USER->id));

                    $vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
                    $vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');

                    $data = (array)$application;
                    $data['title'] = $e_templete->title;
                    $data['contents'] = $e_templete->contents;

                    $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
                    
                    eduhope_balsong_mail($data['title'], $data['contents'], $application);
                    
                    $data2 = (array)$application;
                    $data2['contents'] = $s_templete->contents;
                    $data2 = local_lmsdata_sendvarsetting($vars, $vartypes, $data2);
                    
                    eduhope_balsong_sms($data2['contents'], $phone);
                    
                    //쿠키삭제
                    setcookie($cookiename, '', time()-3600, '/');

                    echo '<script type="text/javascript">alert("수강신청이 완료되었습니다."); location.href="'.$CFG->wwwroot.'/local/mypage/paymentdetail.php?id='.$applicationid.'";</script>';
                } else {
                    echo '<script type="text/javascript">alert("이미 수강신청이 된 차수입니다."); location.href="'.$CFG->wwwroot.'/local/course_application/course_list.php?menu_gubun='.$datas->menu_gubun.'";</script>';
                }
            } else {
                echo '<script type="text/javascript">alert("수강신청에 실패하였습니다."); location.href="'.$CFG->wwwroot.'/local/course_application/course_list.php?menu_gubun='.$datas->menu_gubun.'";</script>';
            }
            //end
            //***************************************************************************************************************************************************************************************************************
//                echo "<tr><th class='td01'><p>거래 성공 여부</p></th>";
//                    echo "<td class='td02'><p>성공</p></td></tr>";
            } else {
                echo "<tr><th class='td01'><p>거래 성공 여부</p></th>";
                    echo "<td class='td02'><p>실패</p></td></tr>";
                echo "<tr><th class='line' colspan='2'><p></p></th></tr>
                <tr><th class='td01'><p>결과 코드</p></th>
                <td class='td02'><p>" . @(in_array($resultMap["resultCode"] , $resultMap) ? $resultMap["resultCode"] : "null" ) . "</p></td></tr>";

                //결제보안키가 다른 경우.
                if (strcmp($secureSignature, $resultMap["authSignature"]) != 0) {
                        echo "<tr><th class='line' colspan='2'><p></p></th></tr>
                                <tr><th class='td01'><p>결과 내용</p></th>
                                <td class='td02'><p>" . "* 데이터 위변조 체크 실패" . "</p></td></tr>";

                        //망취소
                        if(strcmp("0000", $resultMap["resultCode"]) == 0) {
                                throw new Exception("데이터 위변조 체크 실패");
                        }
                } else {
                        echo "<tr><th class='line' colspan='2'><p></p></th></tr>
                                <tr><th class='td01'><p>결과 내용</p></th>
                                <td class='td02'><p>" . @(in_array($resultMap["resultMsg"] , $resultMap) ? $resultMap["resultMsg"] : "null" ) . "</p></td></tr>";
                }

            }

            //공통 부분만

            // 거래번호 : $resultMap["tid"]
            // 결제방법(지불수단) : $resultMap["payMethod"]
            // 결과 코드 : $resultMap["resultCode"]
            // 결과 내용 : $resultMap["resultMsg"]
            // 결제완료금액 : $resultMap["TotPrice"]
            // 주문 번호 : $resultMap["MOID"]
            // 승인날짜 : $resultMap["applDate"]
            // 승인시간 : $resultMap["applTime"]


            /* ----- 가상계좌(payMethod = VBank) ----- */
            // 입금 계좌번호 : $resultMap["VACT_Num"]
            // 입금 은행코드 : $resultMap["VACT_BankCode"]
            // 입금 은행명 : $resultMap["vactBankName"]
            // 예금주 명 : $resultMap["VACT_Name"]
            // 송금자 명 : $resultMap["VACT_InputName"]
            // 송금 일자 : $resultMap["VACT_Date"]
            // 송금 시간 : $resultMap["VACT_Time"]


            /* ----- 실시간계좌이체(payMethod = DirectBank) ----- */ 
            // 은행코드 : $resultMap["ACCT_BankCode"]
            // 현금영수증 발급결과코드 : $resultMap["CSHRResultCode"]
            // 현금영수증 발급구분코드 (0 - 소득공제용, 1 - 지출증빙용): $resultMap["CSHR_Type"]


            /* ----- 카드(payMethod = Card) ----- */
            // 이벤트 코드 : $resultMap["EventCode"]
            // 카드번호 : $resultMap["CARD_Num"]
            // 할부기간 : $resultMap["CARD_Quota"]
            // 카드 종류 : $resultMap["CARD_Code"]
            // 카드 발급사 : $resultMap["CARD_BankCode"]
            // 부분취소 가능여부 : $resultMap["CARD_PRTC_CODE"]
            // 체크카드 여부 : $resultMap["CARD_CheckFlag"]
            // OK CASHBAG 카드번호 : $resultMap["OCB_Num"]
            // OK CASHBAG 적립 승인번호 : $resultMap["OCB_SaveApplNum"]
            // OK CASHBAG 포인트지불금액 : $resultMap["OCB_PayPrice"]
            // GS&Point 카드번호 : $resultMap["GSPT_Num"]
            // GS&Point 잔여한도 : $resultMap["GSPT_Remains"]
            // GS&Point 승인금액 : $resultMap["GSPT_ApplPrice"]
            // U-Point 카드번호 : $resultMap["UNPT_CardNum"]
            // U-Point 가용포인트 : $resultMap["UPNT_UsablePoint"]
            // U-Point 포인트지불금액 : $resultMap["UPNT_PayPrice"]

//                    할부유형
//                    if (isset($resultMap["EventCode"]) && isset($resultMap["CARD_Interest"]) && (strcmp("1", $resultMap["CARD_Interest"]) == 0 || strcmp("1", $resultMap["EventCode"]) == 0 )) {
//                        echo "무이자";
//                    } else if (isset($resultMap["CARD_Interest"]) && !strcmp("1", $resultMap["CARD_Interest"]) == 0) {
//                        echo "유이자 (유이자로 표시되더라도 EventCode 및 EDI에 따라 무이자 처리가 될 수 있습니다.)";
//                    }

//                    포인트 사용여부
//                    if (isset($resultMap["point"]) && strcmp("1", $resultMap["point"]) == 0) {
//                        echo "사용";
//                    } else {
//
//                        echo "미사용";
//                    }

            echo "</table>
            <span style='padding-left : 100px;'></span>
            <form name='frm' method='post'> 
                    <input type='hidden' name='tid' value='" . @(in_array($resultMap["tid"] , $resultMap) ? $resultMap["tid"] : "null" ) . "'/>
            </form>				
            </pre>";

            // 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
            // 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시
            // payViewType을 popup으로 해서 결제를 하셨을 경우
            // 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요
            //throw new Exception("강제 Exception");
        } catch (Exception $e) {
            // $s = $e->getMessage() . ' (오류코드:' . $e->getCode() . ')';
            //####################################
            // 실패시 처리(***가맹점 개발수정***)
            //####################################
            //---- db 저장 실패시 등 예외처리----//
            $s = $e->getMessage() . ' (오류코드:' . $e->getCode() . ')';
            echo $s;

            //#####################
            // 망취소 API
            //#####################

            $netcancelResultString = ""; // 망취소 요청 API url(고정, 임의 세팅 금지)

            if ($httpUtil->processHTTP($netCancel, $authMap)) {
                $netcancelResultString = $httpUtil->body;
            } else {
                echo "Http Connect Error\n";
                echo $httpUtil->errormsg;

                throw new Exception("Http Connect Error");
            }

                //echo "<br/>## 망취소 API 결과 ##<br/>";

                /*##XML output##*/
                //$netcancelResultString = str_replace("<", "&lt;", $$netcancelResultString);
                //$netcancelResultString = str_replace(">", "&gt;", $$netcancelResultString);

            // 취소 결과 확인
            //echo "<p>". $netcancelResultString . "</p>";
            
            echo '<script type="text/javascript">alert("수강신청에 실패하였습니다."); location.href="'.$CFG->wwwroot.'/local/course_application/course_list.php?menu_gubun='.$datas->menu_gubun.'";</script>';
        }
    } else {

        //#############
        // 인증 실패시
        //#############
        echo "<br/>";
        echo "####인증실패####";

        echo "<pre>" . var_dump($_REQUEST) . "</pre>";
    }
} catch (Exception $e) {
    $s = $e->getMessage() . ' (오류코드:' . $e->getCode() . ')';
    echo $s;
}

?>
</body>
<script type="text/javascript">
    function cancelTid() {
        var form = document.frm;

        var win = window.open('', 'OnLine', 'scrollbars=no,status=no,toolbar=no,resizable=0,location=no,menu=no,width=600,height=400');
        win.focus();
        form.action = "http://walletpaydemo.inicis.com/stdpay/cancel/INIcancel_index.jsp";
        form.method = "post";
        form.target = "OnLine";
        form.submit();

    }
</script>
</html>