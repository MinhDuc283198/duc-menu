<?php
/**
 * 동일한 과정에 수강신청한 이력이있는지 체크(AJAX)
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$courseid = optional_param("courseid", 1, PARAM_INT);
$userid = optional_param("userid", 1, PARAM_INT);
$scheduleid = optional_param("scheduleid", 0, PARAM_INT);

//동일한 과정 수강신청 여부 체크(이수된 과정은 포함하지 않음)
$sql = 'SELECT lca.* FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.id = lca.courseid
        WHERE lca.userid=:userid AND (lca.paymentstatus = :complete OR lca.paymentstatus = :stand) AND (lca.status != :delay OR lca.status != :cancel) AND lc.parentcourseid = :parentcourseid AND !(lc.learningend < :time AND lca.completionstatus=:completionstatus)';

$application = $DB->get_records_sql($sql,array('userid'=>$userid, 'complete'=>'complete', 'stand'=>'stand', 'delay'=>'delay', 'cancel'=>'cancel', 'parentcourseid'=>$courseid, 'time'=>time(), 'completionstatus'=>0));

//위탁연수일 때 수강신청 제한 체크
$schedule_limit = 0;
$limit = 0;

if($scheduleid != 0){
    $schedule_limit = $DB->get_field('lmsdata_schedule', 'application_limit',array('id'=>$scheduleid));
    
    $count_sql = 'SELECT count(lca.id)
                  FROM {lmsdata_course_applications} lca
                  JOIN {lmsdata_class} lc on lc.id = lca.courseid
                  WHERE lca.userid = :userid AND lc.scheduleid = :scheduleid AND (lca.paymentstatus = :complete OR lca.paymentstatus = :stand) AND (lca.status != :delay OR lca.status != :cancel)';
    $consignment_application = $DB->count_records_sql($count_sql, array('userid'=>$userid, 'scheduleid'=>$scheduleid, 'complete'=>'complete', 'stand'=>'stand', 'delay'=>'delay', 'cancel'=>'cancel'));
    
    if($consignment_application >= $schedule_limit){
        $limit = 1;
    }
}

if($schedule_limit != 0 && $limit == 1){
    echo 'limit';
} else if(count($application) == 0){
    echo 'true';
} else {
    echo 'false';
}