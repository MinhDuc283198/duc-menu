<?php
/**
 * 위탁교육 상세보기 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/lib/filelib.php';

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("과정안내");
$PAGE->navbar->add("위탁교육");

echo $OUTPUT->header();

$menu_gubun = optional_param('menu_gubun', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);
$schedule = $DB->get_record('lmsdata_schedule',array('id'=>$id));
$sql = "select lc.id, lc.enrolmentstart, lc.enrolmentend, lco.id as courseid, lco.coursename, lco.price, lco.sampleurl, lco.courseid as lcourseid, ls.counthide,
        (select count(*) from m_lmsdata_course_applications where courseid = lc.id and status = 'apply') as applycount
        from {lmsdata_class} lc 
        join {lmsdata_course} lco on lco.id=lc.parentcourseid 
        left join {lmsdata_schedule} ls on lc.scheduleid = ls.id
        where lc.scheduleid = :scheduleid 
        order by lc.classnum asc";
$contents = $DB->get_records_sql($sql,array('scheduleid'=>$schedule->id));
?>  
<?php 
    $path = '';
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_courselist', 'logo1', $schedule->id, 'id', false);
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_courselist/logo1/' . $schedule->id . '/' . $filename);
    }
    $path2 = '';
    $files2 = $fs->get_area_files($context->id, 'local_courselist', 'logo2', $schedule->id, 'id', false);
    foreach ($files2 as $file2) {
        $filename2 = $file2->get_filename();
        $path2 = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_courselist/logo2/' . $schedule->id . '/' . $filename2);
    }
?>
   
<h3 class="page_title">
    <?php echo get_string('training:commissioned', 'local_course_application'); ?>
    <span class="r_content">
        <input type="button" onclick="location.href='<?php echo $CFG->wwwroot;?>/local/course_application/comsignment_list.php?menu_gubun=2'" class="btn h30 orange" value="목록으로" />
    </span>
</h3>   
<div class="logo_area">
    <?php if($path || $path2){
        echo '<img src="'.$path.'" alt="" class="l_content" />
    <img src="'.$path2.'" alt="" class="r_content" height="60px" />';
    }?>
    
</div>




<div class="on textlist">
    <div class="colorbox">
        <strong class="orange">신청기간 </strong>
        <?php echo date('Y-m-d', $schedule->enrolmentstart) ?> ~ <?php echo date('Y-m-d', $schedule->enrolmentend) ?>
        <strong class="orange margin20"><?php echo get_string('trainingperiod', 'local_course_application'); ?> </strong>
        <?php echo date('Y-m-d', $schedule->learningstart) ?> ~ <?php echo date('Y-m-d', $schedule->learningend) ?>
    </div>

    <div>
        <?php echo $schedule->consignmenttext ?>
    </div>
</div>


<h5 class="div_title orange">
    교육과정
</h5>
<table class="table">
    <thead>
        <tr>
            <th class="w250px">과정명</th>
            <th>교육비</th>
            <th>미리보기</th>
            <th>신청자수</th>
            <th>신청</th>
        </tr>
    </thead>

    <tbody>
        <?php
        if ($contents) {
            foreach ($contents as $content) {
                ?>
                <tr>
                    <td class="text-left"><a href="<?php echo $CFG->wwwroot . '/local/course_application/course_info.php?courseid=' . $content->lcourseid . '&classid=' . $content->id . '&menu_gubun=2'; ?>"><?php echo $content->coursename ?></a></td>
                    <td><?php echo $schedule->deductible ?>원</td>
                    <td>
                        <?php if(!empty($content->sampleurl)){?>
                        <input type="button" onclick="window.open('<?php echo $content->sampleurl;?>')" class="btn brd orange" name="btn01" value="<?php echo get_string('preview', 'local_course_application'); ?>">
                        <?php } else {?>
                        없음
                        <?php }?>
                    </td>
                    <td><?php if(!$content->counthide) echo $content->applycount;?></td>
                    <td>
                        <?php
                        $start = date('Ymd',$content->enrolmentstart);
                        $end = date('Ymd',$content->enrolmentend);
                        if($end < date('Ymd')){
                        ?>
                            <input type="button" value="<?php echo get_string('end', 'local_course_application'); ?>" class="btn gray" />
                        <?php
                        } else if ($start > date('Ymd') || $end < date('Ymd')) {
                        ?>
                            <input type="button" value="<?php echo get_string('commissioned:wait', 'local_course_application'); ?>" class="btn gray" />
                        <?php
                        } else {
                        ?>
                            <input type="button" class="btn brd orange" onclick="application_click('<?php echo $content->id;?>','<?php echo $content->courseid; ?>')" value="수강신청"/>
                        <?php
                        }
                        ?>
                        <a>
                            
                        </a>
                        
                    </td>
                </tr>
        <?php
    }
} else {
    echo '<td colspan="4">등록된 교육가 없습니다.</td>';
}
?>

    </tbody>
</table>
<script>
    /**
     * 동일한 과정에 수강신청 정보가 있는지 체크
     * @param {int} classid
     * @param {int} lcourseid
     * @returns {data}
     */
    function application_click(classid, lcourseid){
        $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/course_application/application_chk.php"?>',
            method: 'POST',
            data : {
                courseid : lcourseid,
                userid : '<?php echo $USER->id;?>',
                menu_gubun : '<?php echo $menu_gubun;?>',
                scheduleid : '<?php echo $schedule->id;?>'
            },
            success: function(data) {
                if(data == 'limit'){
                    alert('해당 위탁교육에서 한 사람이 수강신청할 수 있는 과정은 <?php echo $schedule->application_limit;?>개가 최대입니다.')
                } else if(data == 'true'){
                    location.href = '/local/course_application/application.php?courseid='+classid+'&menu_gubun=<?php echo $menu_gubun;?>';
                } else {
                    call_layerPop("/local/course_application/pop_application.php", "800px", "auto", {id:lcourseid, menu_gubun:<?php echo $menu_gubun;?>, classtype:2, classid:classid});
                }
            }
        });
    }
</script>
<?php
echo $OUTPUT->footer();
?>


