<?php
/**
 * 학교 검색 팝업
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$search = optional_param("search_val", '', PARAM_RAW); // 검색내용
$page = optional_param("page", 1, PARAM_INT); // 페이지
$perpage = 10;
$offset = 0;
$maxdisplay = 15;

if ($page != 0) {
    $offset = ($page-1) * $perpage;
}

$db_host = '222.234.1.238';
$db_id = 'eduhope';
$db_pw = 'eduhope';
$db_name = 'eduhope';
$conn = mysqli_connect($db_host, $db_id, $db_pw, $db_name);

$sql = 'select * from tb_school';
$count_sql = 'select count(*) from tb_school';

$where = " where USE_YN = 'Y' ";

$orderby = ' order by SCHOOL_NM asc';

$limit = " limit $offset, $perpage";

if(!empty($search)){
    $where .= " AND SCHOOL_NM like '%$search%'";
}

$query = mysqli_query($conn, $sql.$where);
$count = mysqli_num_rows($query);

$query = mysqli_query($conn, $sql.$where.$orderby.$limit);
//echo $count;

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
?>  
<link href="/theme/oklassedu/style/common.css" rel="stylesheet" />
<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<div id="popwrap">
    <h3 id="pop_title">
        학교검색
        <img class="close r_content" src="/theme/oklassedu/pix/images/close.png" alt="" />
    </h3>
    
    <div id="pop_content">
        <form id="schoolsearch" onsubmit="return false;">
            <input type="text" class="search" name="search_val" value="<?php echo $search;?>">
            <input type="button" class="btn gray h30" name="search_btn" value="검색">
            <table class="table">
                <thead>
                    <th>학교명</th>
                    <th>학교코드</th>
                    <th>학교주소</th>
                    <th>선택</th>
                </thead>
                <?php
                if(!empty($search)){
                ?>
                <tbody>
                    <?php
                    while($result = mysqli_fetch_assoc($query)){
                    ?>
                    <tr>
                        <td><?php echo (!empty($result['SCHOOL_NM']))?$result['SCHOOL_NM']:'-';?></td>
                        <td><?php echo (!empty($result['SCHOOL_CODE']))?$result['SCHOOL_CODE']:'-';?></td>
                        <td class="text-left"><?php echo (!empty($result['ADDRESS']))?$result['ADDRESS']:'-';?></td>
                        <td><input type="button" value="선택" class="btn orange" onclick="set_school('<?php echo $result['SCHOOL_NM'];?>', '<?php echo $result['SCHOOL_CODE'];?>')"></td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <?php
                } else {
                    echo '<tr><td colspan="4">검색어를 입력해주세요</td></tr>';
                }
                ?>
            </table>
            <?php
            if(!empty($search)){
            ?>
            <div class="center w100">
                <div class="pagination">
                    <?php
                    $lastpage = 1;
                    if($count > 0) {
                        $lastpage = ceil($count / $perpage);
                    }

                    if($page > $lastpage) {
                        $page = $lastpage;
                    }
                    
                    if ($page > round(($maxdisplay/3)*2)) {
                        $currpage = $page - round($maxdisplay/2);
                        if($currpage > ($lastpage - $maxdisplay)) {
                            if(($lastpage - $maxdisplay) > 0){
                                $currpage = $lastpage - $maxdisplay;
                            }
                        }
                    } else {
                        $currpage = 1;
                    }
                    $prevpage = $page-1;
                    $nextpage = $page+1;
                    
                    if($prevpage > 0){
                    ?>
                        <a href="#" class="prev" onclick="move_page('<?php echo $prevpage;?>')">
                            <img alt="prev" src="<?php echo $CFG->wwwroot;?>/chamktu/img/pagination_left.png">
                        </a>
                    <?php
                    }
                    
                    $displaycount = 0;
                    while ($displaycount <= $maxdisplay and $currpage <= $lastpage) {
                        if ($page == $currpage) {
                            echo '<strong>'.$currpage.'</strong>';
                        } else {
                            echo '<a href="#" onclick="move_page(\''.$currpage.'\')">'.$currpage.'</a>';
                        }

                        $displaycount++;
                        $currpage++;
                    }

                    if($nextpage < $lastpage){
                    ?>
                    <a href="#" class="next"  onclick="move_page('<?php echo $nextpage;?>')">
                        <img alt="next" src="<?php echo $CFG->wwwroot;?>/chamktu/img/pagination_right.png">
                    </a>
                    <?php }?>
                </div>
            </div>
            <?php
            }
            ?>
        </form>
    </div>
</div>
<script>
    /**
     * 검색 시 페이지 리로드
     * @type type
     */
    $('input[name=search_btn]').click(function(){
        $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/mypage/schoolsearch.php"?>',
            method: 'POST',
            data : {
                search_val : $('input[name=search_val]').val()
            },
            success: function(data) {
                $('#popwrap').html(data);
            }
        });
    });
    
    /**
    * 엔터키 검색

     * @type type     */
    $("input[name=search_val]").keypress(function (e) {
        if (e.which == 13){
            $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/course_application/schoolsearch.php"?>',
            method: 'POST',
            data : {
                search_val : $('input[name=search_val]').val()
            },
            success: function(data) {
                $('#popwrap').html(data);
            }
        });
        }
    });
    
    /**
    * 페이지 이동함수

     * @param {int} page
     * @returns {undefined}     */
    function move_page(page){
        $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/course_application/schoolsearch.php"?>',
            method: 'POST',
            data : {
                search_val : $('input[name=search_val]').val(),
                page : page
            },
            success: function(data) {
                $('#popwrap').html(data);
            }
        });
    }
    
    /**
    * 학교선택 함수

     * @param {char} name
     * @param {char} code
     * @returns {undefined}     */
    function set_school(name, code){
        $('input[name=school]').val(name);
        $('input[name=schoolcode]').val(code);
        closePop();
    }
</script>