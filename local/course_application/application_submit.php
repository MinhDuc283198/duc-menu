<?php
/**
 * 수강신청 submit 페이지(결제로직 미포함) 결제가 필요하지 않은 수강신청에 사용
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . "/chamktu/lib.php");
require_once($CFG->dirroot . '/local/lmsdata/lib.php');

require_login();

//$courseid = optional_param('courseid', 0, PARAM_INT);
$classid = optional_param('classid', 0, PARAM_INT);
$menu_gubun = optional_param('menu_gubun', 0, PARAM_INT);
$price = optional_param('pay_price', 0, PARAM_INT);
$ordernumber = optional_param('ordernumber', '', PARAM_RAW);
//$usertypecode = optional_param('usertypecode', 0, PARAM_INT);
//if($usertypecode == 20) {$new->usertypecode = $userinfo->usertypecode;
//    $qualification = optional_param('qualification1', 0, PARAM_RAW);
//}else if($usertypecode == 10) {
//    $qualification = optional_param('qualification2', 0, PARAM_RAW);
//}
$school = optional_param('school', '', PARAM_RAW);
$neis = optional_param_array('neis', array(), PARAM_RAW);
$no = optional_param_array('no', array(), PARAM_RAW);
$firstnumber = optional_param('firstnumber', '', PARAM_RAW);
$number = optional_param_array('number', array(), PARAM_RAW);
$year = optional_param('year', '', PARAM_RAW);
$month = optional_param('month', '', PARAM_RAW);
$day = optional_param('day', '', PARAM_RAW);
$email = optional_param('email', '', PARAM_RAW);
$discountck = optional_param('discountck', '', PARAM_RAW);
$code = optional_param('code', '', PARAM_RAW);
$schooltype = optional_param('schooltype', '', PARAM_RAW);
$classlevel = optional_param('classlevel', '', PARAM_RAW);
$classgroup = optional_param('classgroup', '', PARAM_RAW);
$schoolregion = optional_param('schoolregion', '', PARAM_RAW);

$sqldatas = 'SELECT * FROM {lmsdata_class} WHERE id = :id';

$datas = $DB->get_record_sql($sqldatas, array('id'=>$classid));

$neiscode = implode('', $neis);
foreach ($no as $nos){
    if($nos) 
        $count++;
}
if($count)
    $locationnumber = implode('-', $no);

$phone = $firstnumber;
$phone .= implode('', $number);
$birthday = $year.$month.$day;

$userinfo = $DB->get_record('lmsdata_user', array('userid'=>$USER->id));

//$sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid AND paymentstatus != :paymentstatus';
$sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid';

$application = $DB->get_record_sql($sql, array('userid'=>$USER->id, 'courseid'=>$datas->courseid));
if(empty($application) || $application->paymentstatus == 'cancel'){
    if (empty($application)) {
    $new = new stdClass();
    $new->userid = $USER->id;
    $new->courseid = $datas->courseid;
    $new->usertypecode = $userinfo->usertypecode;
    $new->school = $school;
    $new->qualification = $qualification;
    $new->neiscode = $neiscode;
    $new->locationnumber = $locationnumber;
    $new->birthday = $birthday;
    $new->phone = $phone;
    $new->price = $price;
    $new->ordernumber = $ordernumber;
    $new->schooltype = $schooltype;
    $new->classlevel = $classlevel;
    $new->classgroup = $classgroup;
    $new->schoolregion = $schoolregion;
    
    if($paymentmeans == 'unbank'){
        $new->paymentmeans = $paymentmeans;
    }

    //결제정보 저장
        $new->paymentstatus = 'complete';
        $new->status = 'apply';
    
    if($new->paymentstatus == 'complete'){
        $sql = 'select mc.* from {lmsdata_class} lc join {course} mc on mc.id = lc.courseid where lc.id = :id';
        $class = $DB->get_record_sql($sql, array('id'=>$classid));
        $userinfo->roleid = 5;
        $new->courseid = $class->id;
        set_assign_user($class->id, $userinfo);
    }
    
    $new->usertypecode = $userinfo->usertypecode;
    $new->completionstatus = 0;
    $new->timecreated = time();
    
    $applicationid = $DB->insert_record('lmsdata_course_applications', $new);
 } else {
    $new = $application;
    if ($USER->id) {
        $new->userid = $USER->id;
    }
    if ($USER->id) {
    $new->courseid = $datas->courseid;
    }
    if ($USER->id) {
    $new->usertypecode = $userinfo->usertypecode;
    }
    if ($USER->id) {
    $new->qualification = $qualification;
    }
    if ($USER->id) {
    $new->birthday = $birthday;
    }
    if ($USER->id) {
    $new->phone = $phone;
    }
    if ($USER->id) {
    $new->price = $price;
    }
    if ($USER->id) {
    $new->classlevel = $classlevel;
    }
    if ($USER->id) {
    $new->classgroup = $classgroup;
    }
    if($paymentmeans == 'unbank'){
        $new->paymentmeans = $paymentmeans;
    }

    //결제정보 저장
        $new->paymentstatus = 'complete';
        $new->status = 'apply';
    
    if($new->paymentstatus == 'complete'){
        $sql = 'select mc.* from {lmsdata_class} lc join {course} mc on mc.id = lc.courseid where lc.id = :id';
        $class = $DB->get_record_sql($sql, array('id'=>$classid));
        $userinfo->roleid = 5;
        $new->courseid = $class->id;
        set_assign_user($class->id, $userinfo);
    }
    
//    $new->usertypecode = $userinfo->usertypecode;
    $new->completionstatus = 0;
    $new->timemodified = strtotime($lmsdata->timecreated);
    $new->timecreated = time();
    
    $applicationid = $DB->update_record('lmsdata_course_applications', $new);
     
 }
    if($applicationid){        
        $application = $DB->get_record('lmsdata_course_applications',array('id'=>$applicationid));
                    
//        $apiresult = eduhope_set_training_history($application);
        
        $class = $DB->get_record('lmsdata_class',array('id'=>$application->courseid));
        
        $classtype = 1;
        if(!empty($class->classtype)) {
            $classtype = $class->classtype;
        }
        
        $e_templete = $DB->get_record('lmsdata_sendtemplate', array('sendtime'=>1, 'type'=>'email', 'classtype'=>$classtype));
        $s_templete = $DB->get_record('lmsdata_sendtemplate', array('sendtime'=>1, 'type'=>'sms', 'classtype'=>$classtype));

        $sql = 'SELECT lca.timecreated, lca.userid, lca.email, lca.price, lca.paymentstatus, lc.learningstart, lc.learningend, lc.evaluationstart, lc.evaluationend, lc.courseid, lc.classnum, lco.coursename, mu.lastname, mu.username, ls.consignmentplace
                FROM {lmsdata_course_applications} lca
                JOIN {lmsdata_class} lc ON lc.courseid = lca.courseid
                JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
                JOIN {user} mu ON mu.id = lca.userid
                JOIN {lmsdata_schedule} ls on ls.id = lc.scheduleid
                WHERE lca.id = :id AND lca.userid = :userid';
        if ($classtype == 3 || $classtype == 4 ) {
            $sql = 'SELECT lca.timecreated, lca.userid, lca.email, 
                lca.price, lca.paymentstatus, lc.learningstart, 
                lc.learningend, lc.evaluationstart, lc.evaluationend, 
                lc.courseid, lc.classnum, lco.coursename, mu.lastname, mu.username 
                FROM {lmsdata_course_applications} lca
                JOIN {lmsdata_class} lc ON lc.id = lca.courseid
                JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
                JOIN {user} mu ON mu.id = lca.userid
                WHERE lca.id = :id AND lca.userid = :userid ';
        }
        $application = $DB->get_record_sql($sql, array('id'=>$applicationid, 'userid'=>$USER->id));

        $vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
        $vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');

        $data = (array)$application;
        $data['title'] = $e_templete->title;
        $data['contents'] = $e_templete->contents;

//        $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);

//        eduhope_balsong_mail($data['title'], $data['contents'], $application);

        $data2 = (array)$application;
        $data2['contents'] = $s_templete->contents;
//        $data2 = local_lmsdata_sendvarsetting($vars, $vartypes, $data2);

//        eduhope_balsong_sms($data2['contents'], $phone);                    
        
        echo '<script type="text/javascript">alert("수강신청이 완료되었습니다."); location.href="'.$CFG->wwwroot.'/local/course_application/course_list.php?menu_gubun='.$menu_gubun.'";</script>';
    } else {
        echo '<script type="text/javascript">alert("수강신청에 실패하였습니다."); location.href="'.$CFG->wwwroot.'/local/course_application/course_list.php?menu_gubun='.$menu_gubun.'";</script>';
    }
} else {
        echo '<script type="text/javascript">alert("이미 수강신청이 된 차수입니다."); location.href="'.$CFG->wwwroot.'/local/course_application/course_list.php?menu_gubun='.$menu_gubun.'";</script>';
}