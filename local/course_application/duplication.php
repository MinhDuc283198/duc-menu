<?php
/**
 * 네이스번호 중복체크(사용안함)
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$courseid = optional_param('courseid', 0, PARAM_INT);
$neis = optional_param('neis', '', PARAM_RAW);

$class = $DB->get_record('lmsdata_class',array('courseid'=>$courseid));

$sql = 'SELECT lca.* 
        FROM {lmsdata_class} lc
        LEFT JOIN {lmsdata_course_applications} lca ON lca.courseid = lc.courseid
        WHERE lc.parentcourseid=:parentcourseid AND lca.neiscode=:neiscode AND lca.status=:status';

//해당 과정에 입력한 네이스번호로 수강완료된 차수목록
$class_chk = $DB->get_records_sql($sql,array('parentcourseid'=>$class->parentcourseid, 'neiscode'=>$neis, 'status'=>'complete'));

//해당 차수에 입력한 네이스번호로 수강신청된 목록
$current_class_chk = $DB->get_records('lmsdata_course_applications', array('courseid'=>$courseid, 'neiscode'=>$neis));

if(!empty($current_class_chk)){
    echo 'false';
} else if(!empty($class_chk)){
    echo 'warning';
} else {
    echo 'true';
}