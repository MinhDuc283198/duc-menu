<?php
/** 
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

require_login();

$PAGE->set_context($context);

$PAGE->set_url(new moodle_url('/local/mypage/ma1in.php', array('menu_gubun' => $menu_gubun)));
$PAGE->set_pagelayout('edu');
$PAGE->add_body_class('path-local-course_application');


echo $OUTPUT->header();
?>  
<div class="course_info_box">
                        <p class="tit">A Better Life Through Better Education</p>
                        <h3>
                            4차 산업혁명시대의 교육전문가  <strong>멘토</strong>
                        </h3>
                        <div class="ci_bx">
                            <ul>
                                <li>
                                    <p>최고 교육전문가로서 장기 비전 수립</p>
                                    <p>사내강사 및 멘토의 멘토로 활동</p>
                                </li>
                                <li>
                                    <p>교육전문가로의 역량 점검 및 강화</p>
                                    <p>사내외 자격증 취득을 통한 전문역량 향상</p>
                                </li>
                                <li>
                                    <p>재능의 교육철학을 이해하고 실천</p>
                                    <p>멘토 업무 수행을 위한 기본 지식과 스킬 학습</p>
                                </li>

                            </ul>
                            <div class="box-txt">
                                <p>
                                    <span><span>최고멘토 <br/> 과정</span></span>
                                    <span><span>연차별 <br/> 과정</span></span>
                                    <span><span>신임멘토 <br /> 과정</span></span>
                                </p>
                                <p>
                                    <span><span>멘토 <br/> 워크숍</span></span>
                                    <span><span>신임 <br/> 워크숍</span></span>
                                </p>
                                <p><span>선택과정</span></p>
                                <p><span>자격인증</span></p>
                                <p><span><span>Micro <br /> Learning</span></span></p>
                            </div>
                        </div>
                    </div>

<?php

echo $OUTPUT->footer();
?>


