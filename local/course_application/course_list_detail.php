<?php
/**
 * 필수과정, 선택과정, 자격인증 과정 안내 및 신청
 * -> $menu_gubun변수를 활용하여 과정 구분을 해줌
 */
//error_reporting(E_ALL);
//
//ini_set("display_errors", 1);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once dirname(dirname(dirname(__FILE__))) . '/mod/okmedia/lib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/chamktu/lib.php';
require_once dirname(dirname(dirname(__FILE__))) . '/chamktu/contents/lib.php';
require_once $CFG->dirroot . '/chamktu/contents/lib.php';
$context = context_system::instance();

$PAGE->set_context($context);

//현재 유저정보
$sql = 'SELECT mu.lastname, lu.*, mu.phone1 muphone1, mu.phone2 muphone2  
        FROM {user} mu
        JOIN {lmsdata_user} lu ON lu.userid = mu.id
        WHERE mu.id = :userid';

$userinfo = $DB->get_record_sql($sql, array('userid' => $USER->id));
$usertype = ($userinfo->usertypecode / 10);

$menu_gubun = optional_param("menu_gubun", 0, PARAM_INT); // 1:필수과정, 5:선택과정, 6:자격인증
$classid = required_param("classid", PARAM_INT);

$courseid = $DB->get_record('lmsdata_class', array('id' => $classid));
$getcourseidsql = 'SELECT lco.*, ca.name ca1name, ca2.name ca2name, 
                                ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber
                                FROM {lmsdata_course} lco
                                JOIN {course_categories} ca ON ca.id = lco.detailprocess 
                                LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id WHERE lco.id = :id';
$getcourseid = $DB->get_record_sql($getcourseidsql, array('id' => $courseid->parentcourseid));

$type_arrs = array(
    1 => '임직원',
    2 => '멘토',
    3 => '교육생',
    4 => '임직원, 멘토',
    5 => '멘토, 교육생',
    6 => '임직원, 교육생',
    7 => '전체'
);

//if($getcourseid->ca1idnumber != 'trust_course' && $usertype == 3) {
//    redirect('/local/course_application/course_list.php?re=2&menu_gubun='.$menu_gubun, '교육생은 신임과정 관련 정보만 볼 수 있습니다.');
//}

$PAGE->set_url(new moodle_url('/local/course_application/course_list_detail.php', array('menu_gubun' => $menu_gubun, 'classid' => $classid)));
$PAGE->set_pagelayout('edu');
$PAGE->add_body_class('path-local-course_application');

$PAGE->requires->jquery();
$PAGE->requires->css('/mod/okmedia/viewer/flowplayer7.2.7/skin/skin.css');
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.speed-menu.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.hlsjs.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/player.js', true);

$PAGE->navbar->add(get_string('courseguide', 'local_course_application'));

require_login();

echo $OUTPUT->header();
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#thumbvideo ul li").click(function () {
            // /local/publishing/pop_quiz.php
            var id = $(this).attr('class');
            var canwatch = $(this).attr('id');
            var usertype = '<?php echo $usertype; ?>';
            var ca1idnumber = '<?php echo $getcourseid->ca1idnumber; ?>';
            var ca2idnumber = '<?php echo $getcourseid->ca2idnumber; ?>';

            if (usertype == 3) {
                if (canwatch === 'y') {
                    if (ca1idnumber == 'trust_course') {
                        if (confirm('해당 콘텐츠는 강의 내부에서만 볼 수 있습니다.\n\ 강의로 이동하시겠습니까?') == true) {
                            location.href = '<?php echo $CFG->wwwroot . '/course/view.php?id=' . $courseid->courseid; ?>';
                        }
                    } else {
                        alert('교육생은 접근할 수 없습니다.');
                    }
                } else {
                    alert('교육생은 접근할 수 없습니다.');
                }
            } else if (usertype > 0 && usertype != 3) {
                if (canwatch === 'y') {
                    if (confirm('해당 콘텐츠는 강의 내부에서만 볼 수 있습니다.\n\ 강의로 이동하시겠습니까?')) {
                        location.href = '<?php echo $CFG->wwwroot . '/course/view.php?id=' . $courseid->courseid; ?>';
                    }
                } else {
                    if (ca2idnumber == 'required_course') {
                        alert('해당 과정의 수강대상이 아닙니다.');
                    } else if (confirm('해당 콘텐츠는 강의 신청자만 볼 수 있습니다.\n\ 수강신청을 하시겠습니까?')) {
                        location.href = '<?php echo $CFG->wwwroot . '/local/course_application/application.php?menu_gubun=' . $menu_gubun . '&classid=' . $classid; ?>';
                    }
                }
            }
        });
    });

</script>
<?php
if ($menu_gubun == 1) {

    $check_sql = 'SELECT ok.course, lc.id, SUM(ok.point) lecturepoint 
            FROM {okmedia} ok 
            JOIN {lmsdata_class} lc ON lc.courseid = ok.course 
            WHERE lc.id = :lcid 
            GROUP BY ok.course';
    $check_content = $DB->get_record_sql($check_sql, array('lcid' => $classid));
    $course_sql = '';
    if ($check_content) {
        $course_sql = 'SELECT mc.id mcid, lc.id lcid, mc.fullname, lcc.intro, ca.id caid, ca2.id ca2id, lcc.id lccid, 
                            ca.name ca1name, ca2.name ca2name, lc.id lcid, lcc.completestandard, lc.classyear, lc.classnum, 
                            lc.learningend, lc.learningstart, lcc.classobject, lcc.classtype, lcc.objectives, lcc.courseid, 
                            lc.enrolmentend, lc.enrolmentstart, lcc.subjects, lcc.contents, lcc.detailprocess 
                            FROM {course} mc 
                            JOIN {lmsdata_class} lc ON mc.id = lc.courseid 
                            JOIN {lmsdata_course} lcc ON lcc.id = lc.parentcourseid 
                            JOIN {course_categories} ca ON ca.id = mc.category 
                            LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id 
                            JOIN (SELECT course, SUM(point) lecturepoint FROM {okmedia} GROUP BY course) okm ON okm.course = lc.courseid 
                            WHERE lc.id = :lcid';
    } else {
        $course_sql = 'SELECT mc.id mcid, lc.id lcid, mc.fullname, lcc.intro, ca.id caid, ca2.id ca2id, lcc.id lccid, 
                            ca.name ca1name, ca2.name ca2name, lc.id lcid, lcc.completestandard, lc.classyear, lc.classnum, 
                            lc.learningend, lc.learningstart, lcc.classobject, lcc.classtype, lcc.objectives, lcc.courseid, 
                            lc.enrolmentend, lc.enrolmentstart, lcc.subjects, lcc.contents, lcc.detailprocess, lcc.coursetype 
                            FROM {course} mc 
                            JOIN {lmsdata_class} lc ON mc.id = lc.courseid 
                            JOIN {lmsdata_course} lcc ON lcc.id = lc.parentcourseid 
                            JOIN {course_categories} ca ON ca.id = mc.category 
                            LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id 
                            WHERE lc.id = :lcid';
    }

    $params = array('lcid' => $classid);
    $course = $DB->get_record_sql($course_sql, $params);

    $getcontext_sql = 'SELECT con.id conid, con.*, co.*, lco.* 
                                FROM {context} con 
                                JOIN {course} co ON con.contextlevel = 50 AND con.instanceid = co.id 
                                JOIN {lmsdata_course} lco ON lco.courseid = co.id 
                                WHERE lco.id = :lcoid';
    $params_context = array('lcoid' => $course->lccid);
    $getcontext = $DB->get_record_sql($getcontext_sql, $params_context);

    $fileobj = edu_courselist_get_file_img($getcontext->conid, 'thumbnailimg');
    ?>
    <h3 class="page_title">
        <?php
        echo get_string('training:courseno1', 'local_course_application');
        ?>
        <span class="r_content">
            <a href="<?php echo $CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=1'; ?>" class="btn gray h30">목록보기</a>
        </span>
    </h3>


    <div class="colorbox">
        <div class="l_content">
            <!--thumbnailimg-->

            <?php
            if (!empty($fileobj)) {
                ?>
                <img src="<?php echo $fileobj; ?>" alt="" />
                <?php
            } else {
                ?>
                <img src="/theme/oklassedu/pix/ex01.png" alt="" />
                <?php
            }
            ?>
        </div>
        <div class="l_content">
            <?php $categoryname = $course->ca1name; ?>
            <p class="t-gray"><?php echo $course->classyear . '년 ' . $course->classnum . '기' ?></p> 
            <h4><a href="<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->mcid; ?>"><?php echo $course->fullname; ?></a></h4>
            <dl>
                <!--기수의 learningstart, learningend-->
                <dt>학습기간</dt>
                <dd><?php echo date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend); ?></dd>
                <!--lecturepoint-->
                <dt>취득포인트</dt>
                <dd>
                    <?php
                    echo empty($check_content->lecturepoint) ? '0 P' : $check_content->lecturepoint . ' P';
                    ?>
                </dd>
            </dl>
        </div>
    </div>

    <div class="tab_header tab_event"> <!-- tab-event -->
        <span class="on m-col2" data-target=".data01"><a href="#">강의정보</a></span>
        <span class="m-col2" data-target=".data02"><a href="#">교육내용</a></span>
    </div>
    <div class="tab_contents">
        <div class="data01 textlist on">
            <h5>과정명</h5>
            <!--coursename-->
            <p><?php echo $course->fullname; ?></p>

            <h5>대상</h5>
            <p>
                <?php
                $coursetype;
                if ($course->classobject) {
                    $coursetype = $type_arrs[$course->classobject];
                } else {
                    $coursetype = $type_arrs[$course->classtype];
                }
                echo $course->classyear . '년 ' . $course->classnum . '기 ' . $coursetype;
                ?></p> <!--<p>2019년 1기 신임 회원</p>e-->

            <h5>강의소개</h5>
            <p>
                <?php echo $course->intro; ?>
            </p>
            <h5>학습목표</h5>
            <p>
                <?php echo $course->objectives; ?>
            </p> 

            <!--기수의 learningstart, learningend-->
            <h5>학습기간</h5>
            <p><?php echo date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend); ?></p>
            <h5>이수기준</h5>
            <p>
                <?php
                echo $course->completestandard;
//                echo preg_replace("(\<(/?[^\>]+)\>)", "", $course->completestandard);
//                echo strip_tags($course->completestandard, '</br>' );
                ?>
            </p>

            <h5>취득포인트</h5> 
            <p>
                <?php
                echo empty($check_content->lecturepoint) ? '0 P' : $check_content->lecturepoint . ' P';
                ?>
            </p>
        </div>
        <div class="data02">
            <!-- thumb start -->
            <div class="thumb-list point" id="thumbvideo">
                <ul>
                    <?php
                    $lecturesql = 'SELECT cm.id, ok.id okid, ok.name, ok.point, lcf.duration, lcf.filepath, lcf.filename, ot.playtime, cm.instance, 
                                        lcf.con_seq, cs.section cssection, lc.courseid lccourseid, ok.contents , cm.section cmsection, cs.section cssection 
                                        FROM {okmedia} ok 
                                        JOIN {lmsdata_class} lc ON lc.courseid = ok.course 
                                        JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid 
                                        LEFT JOIN {lcms_contents_file} lcf ON lcf.con_seq = ok.contents 
                                        JOIN {course_modules} cm ON cm.instance = ok.id 
                                        LEFT JOIN {course_sections} cs ON cs.section != 0 AND cs.course = lc.courseid and cs.id = cm.section 
                                        JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modulename 
                                        JOIN (SELECT course, SUM(point) lecturepoint FROM {okmedia} GROUP BY course) okm ON okm.course = lc.courseid 
                                        LEFT JOIN {okmedia_track} ot ON ot.okmediaid = ok.id and ot.userid = :userid
                                        WHERE lc.id = :lcid AND cm.visible = 1 
                                        ORDER BY cm.section asc, cm.id asc';
                    $lecturecontents = $DB->get_records_sql($lecturesql, array('lcid' => $classid, 'modulename' => 'okmedia', 'userid' => $USER->id));

                    $ifstudentsql = 'SELECT lca.id lcaid, lc.id lcid, lca.userid 
                                        FROM {lmsdata_course_applications} lca 
                                        JOIN {lmsdata_class} lc ON lc.courseid = lca.courseid 
                                        WHERE lc.id = :lcid AND lca.userid = :lcauserid';
                    $ifstudent = $DB->get_record_sql($ifstudentsql, array('lcid' => $classid, 'lcauserid' => $USER->id));

                    if (!empty($lecturecontents)) {
                        foreach ($lecturecontents AS $lecturecontent) {
                            $thumnail_imge = thumbnail_image($lecturecontent->con_seq);
                            $lecturecontent->duration = okmedia_time_from_seconds($lecturecontent->duration);
                            ?>
                            <li class="<?php echo $lecturecontent->id; ?>" id="<?php echo (empty($ifstudent) && (!is_siteadmin())) ? 'n' : 'y'; ?>">
                                <div class="t-img">
                                    <!--<img src="/theme/oklassedu/pix/images/ex04.png" alt="" />-->
                                    <img src="<?php echo $thumnail_imge; ?>" alt="" />
                                    <span class="t-time"><?php
                                        if ($lecturecontent->duration->h > 0) {
                                            echo $lecturecontent->duration->h . ':' . $lecturecontent->duration->m . ':' . $lecturecontent->duration->s;
                                        } else {
                                            echo $lecturecontent->duration->m . ':' . $lecturecontent->duration->s;
                                        }
                                        ?></span>
                                    <span class="t-play">play</span>
                                </div>
                                <div class="t-txt">
                                    <h4>
                                        <a href="<?php echo $lecturecontent->info->onclick; ?>"><?php echo $lecturecontent->name ?></a>
                                    </h4>
                                    <p class="point"><?php echo empty($lecturecontent->point) ? '0 P' : $lecturecontent->point . ' P'; ?></p>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        echo '해당 강좌에 업로드 된 콘텐츠가 없습니다.';
                    }
                    ?>
                </ul>
            </div>
            <!-- thumb end -->
        </div>
    </div>
    <?php
} else if ($menu_gubun == 5) {
    $course_sql = 'SELECT mc.id mcid, lc.id lcid, mc.fullname, lcc.intro, ca.id caid, ca2.id ca2id,  lcc.id lccid, 
                            ca.name ca1name, ca2.name ca2name, lc.id lcid, lcc.completestandard, lc.classyear, lc.classnum,
                            lc.learningend, lc.learningstart, lcc.classobject, lcc.classtype, lcc.objectives, lcc.courseid,
                            lc.enrolmentend, lc.enrolmentstart, lcc.subjects, lcc.contents
                            FROM {course} mc
                            JOIN {lmsdata_class} lc ON mc.id = lc.courseid
                            JOIN {lmsdata_course} lcc ON lcc.id = lc.parentcourseid
                            JOIN {course_categories} ca ON ca.id = mc.category 
                            LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            WHERE lc.id = :lcid';

    $params = array('lcid' => $classid);
    $course = $DB->get_record_sql($course_sql, $params);

    $getcontext_sql = 'SELECT con.id conid, con.*, co.*, lco.* 
                                FROM {context} con 
                                JOIN {course} co ON con.contextlevel = 50 AND con.instanceid = co.id 
                                JOIN {lmsdata_course} lco ON lco.courseid = co.id 
                                WHERE lco.id = :lcoid';
    $params_context = array('lcoid' => $course->lccid);
    $getcontext = $DB->get_record_sql($getcontext_sql, $params_context);

    $fileobj = edu_courselist_get_file_img($getcontext->conid, 'thumbnailimg'); // 3926

    $sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid AND paymentstatus != :paymentstatus';
    $application = $DB->get_records_sql($sql, array('userid' => $USER->id, 'courseid' => $course->mcid, 'paymentstatus' => 'cancel'));

//    $fileobj = edu_courselist_get_file($context->id, 'thumbnailimg');
    ?>
    <h3 class="page_title">
        <?php
        echo get_string('training:courseno5', 'local_course_application');
        ?>
        <span class="r_content">
            <a href="<?php echo $CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=5'; ?>" class="btn gray h30">목록보기</a>
        </span>
    </h3>

    <div class="colorbox">
        <div class="l_content">
            <?php
            if (!empty($fileobj)) {
                ?>
                <img src="<?php echo $fileobj; ?>" alt="" />
                <?php
            } else {
                ?>
                <img src="/theme/oklassedu/pix/images/ex01.png" alt="" />
                <?php
            }
            ?>

        </div>
        <div class="l_content">
            <?php
            if (is_siteadmin()) {
                ?>
                <p class="t-gray"><?php echo $course->classyear . '년 ' . $course->classnum . '기' ?></p>
                <h4><a href="<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->mcid; ?>"><?php echo $course->fullname; ?></a></h4>
                <?php
            } else {
                ?>
                <h4>선택과정<?php echo $course->classyear . '년 ' . $course->classnum . '기  - ' . $course->fullname; ?></h4>
                <?php
            }
            ?>
            <dl>
                <!--기수의 learningstart, learningend-->
                <dt>학습기간</dt>
                <dd><?php echo date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend); ?></dd>
            </dl>
            <div class="btn_area">
                <?php
                if ($course->enrolmentend > time() && $course->enrolmentstart < time()) {
                    if ($application) {
                        ?>
                        <input type="button" class="btn gray" value="신청취소" onclick="call_layerPop('/local/mypage/pop_payment_cancel.php', '300px', 'auto', {'id': '<?php echo $course->lcid; ?>', 'menu_gubun': '<?php echo $menu_gubun; ?>'})" />
                        <?php
                    } else {
                        if ($usertype != $course->classobject && $course->classobject < 4) {
                            ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                        } else if ($course->classobject == 4 && $usertype == 3) {
                            ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                            } else if ($course->classobject == 5 && $usertype == 1) {
                                ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                            } else if ($course->classobject == 6 && $usertype == 2) {
                                ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                            } else if ($course->ca1idnumber != 'trust_course' && $usertype == 3) {
                                ?>
                            <a href="#none" onclick="alert('교육생은 신청할 수 없습니다.')"><input type="button" class="btn pink" value="신청" /></a>
                            <?php
                        } else {
                            ?>
                            <a href="<?php echo $CFG->wwwroot . '/local/course_application/application.php?menu_gubun=' . $menu_gubun . '&classid=' . $course->lcid; ?>">
                                <input type="button" value="수강신청" class="btn pink" />
                            </a>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="tab_header tab_event"> <!-- tab-event -->
        <span class="on m-col2" data-target=".data01"><a href="#">강의정보</a></span>
        <span class="m-col2" data-target=".data02"><a href="#">교육내용</a></span>
    </div>
    <div class="tab_contents">
        <div class="data01 textlist on">
            <h5>과정명</h5>
            <!--coursename-->
            <p><?php echo $course->fullname; ?></p>

            <h5>대상</h5>
            <p>
                <?php
                $coursetypes;
                if ($course->classobject) {
                    $coursetypes = $type_arrs[$course->classobject];
                } else {
                    $coursetypes = $type_arrs[$course->classtype];
                }
                echo $coursetypes;
                ?>
            </p> 
            <h5>강의소개</h5>
            <p><?php echo $course->intro; ?></p>
            <h5>학습목표</h5>
            <p>
                <?php echo $course->objectives; ?>
            </p>

            <!--기수의 learningstart, learningend-->
            <h5>학습기간</h5>
            <p><?php echo date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend); ?></p>

            <h5>이수기준</h5>
            <p>
                <?php
//                $contstandards = preg_replace("(\<(/?[^\>]+)\>)", "", $course->completestandard);
                echo $course->completestandard;
                ?>

            </p>

        </div>
        <div class="data02">
            <!-- thumb start -->
            <div class="thumb-list point" id="thumbvideo">
                <ul>
                    <?php
                    $lecturesql = 'SELECT cm.id, ok.id okid, ok.name, ok.point, lcf.duration, lcf.filepath, lcf.filename, ot.playtime, cm.instance, 
                                        lcf.con_seq, cs.section cssection, lc.courseid lccourseid, ok.contents , cm.section cmsection, cs.section cssection 
                                        FROM {okmedia} ok 
                                        JOIN {lmsdata_class} lc ON lc.courseid = ok.course 
                                        JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid 
                                        LEFT JOIN {lcms_contents_file} lcf ON lcf.con_seq = ok.contents 
                                        JOIN {course_modules} cm ON cm.instance = ok.id 
                                        LEFT JOIN {course_sections} cs ON cs.section != 0 AND cs.course = lc.courseid and cs.id = cm.section 
                                        JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modulename 
                                        JOIN (SELECT course, SUM(point) lecturepoint FROM {okmedia} GROUP BY course) okm ON okm.course = lc.courseid 
                                        LEFT JOIN {okmedia_track} ot ON ot.okmediaid = ok.id and ot.userid = :userid
                                        WHERE lc.id = :lcid AND cm.visible = 1 
                                        ORDER BY cm.section asc, cm.id asc';
                    $lecturecontents = $DB->get_records_sql($lecturesql, array('lcid' => $classid, 'modulename' => 'okmedia', 'userid' => $USER->id));

                    $ifstudentsql = 'SELECT lca.id lcaid, lc.id lcid, lca.userid 
                                            FROM {lmsdata_course_applications} lca 
                                            JOIN {lmsdata_class} lc ON lc.courseid = lca.courseid 
                                            WHERE lc.id = :lcid AND lca.userid = :lcauserid';
                    $ifstudent = $DB->get_record_sql($ifstudentsql, array('lcid' => $classid, 'lcauserid' => $USER->id));
                    if (!empty($lecturecontents)) {
                        foreach ($lecturecontents AS $lecturecontent) {
                            $thumnail_imge = thumbnail_image($lecturecontent->con_seq);

                            $learnplaytime = okmedia_time_from_seconds($lecturecontent->duration);
                            ?>
                            <li class="<?php echo $lecturecontent->id; ?>" id="<?php echo (empty($ifstudent) && (!is_siteadmin())) ? 'n' : 'y'; ?>">
                                <div class="t-img">
                                    <img src="<?php echo $thumnail_imge; ?>" alt="" />
                                    <span class="t-time"><?php
                                        if ($learnplaytime->h > 0) {
                                            echo $learnplaytime->h . ':' . $learnplaytime->m . ':' . $learnplaytime->s;
                                        } else {
                                            echo $learnplaytime->m . ':' . $learnplaytime->s;
                                        }
                                        ?></span>
                                    <span class="t-play">play</span>
                                </div>
                                <div class="t-txt">
                                    <h4>
                                        <a href="<?php echo $lecturecontent->info->onclick; ?>"><?php echo $lecturecontent->name ?></a>
                                    </h4>
                                    <p class="point">포인트 : <?php echo empty($lecturecontent->point) ? '0 P' : $lecturecontent->point . ' P'; ?></p>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        echo '해당 강좌에 업로드 된 콘텐츠가 없습니다.';
                    }
                    ?>

                </ul>
            </div>
            <!-- thumb end -->
        </div>
    </div>
    <?php
} else if ($menu_gubun == 6) {
    $course_sql = 'SELECT mc.id mcid, lc.id lcid, mc.fullname, ca.id caid, ca2.id ca2id, lcc.id lccid, 
                            ca.name ca1name, ca2.name ca2name, lc.id lcid, lcc.completestandard, lc.classyear, lc.classnum,
                            lc.learningend, lc.learningstart, lcc.classobject, lcc.classtype, lcc.objectives, lcc.courseid,
                            lc.enrolmentend, lc.enrolmentstart, lcc.subjects, lcc.contents
                            FROM {course} mc
                            JOIN {lmsdata_class} lc ON mc.id = lc.courseid
                            JOIN {lmsdata_course} lcc ON lcc.id = lc.parentcourseid
                            JOIN {course_categories} ca ON ca.id = mc.category 
                            LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            WHERE lc.id = :lcid';
    $params = array('lcid' => $classid);
    $course = $DB->get_record_sql($course_sql, $params);

    $getcontext_sql = 'SELECT con.id conid, con.*, co.*, lco.* 
                                FROM {context} con 
                                JOIN {course} co ON con.contextlevel = 50 AND con.instanceid = co.id 
                                JOIN {lmsdata_course} lco ON lco.courseid = co.id 
                                WHERE lco.id = :lccid';
    $params_context = array('lccid' => $course->lccid);
    $getcontext = $DB->get_record_sql($getcontext_sql, $params_context);

    $fileobj = edu_courselist_get_file_img($getcontext->conid, 'thumbnailimg');

    $sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid AND paymentstatus != :paymentstatus';
    $application = $DB->get_records_sql($sql, array('userid' => $USER->id, 'courseid' => $course->mcid, 'paymentstatus' => 'cancel'));

//    $fileobj = edu_courselist_get_file($context->id, 'thumbnailimg');
    ?>
    <h3 class="page_title">
        <?php
        echo get_string('training:courseno6', 'local_course_application');
        ?>
        <span class="r_content">
            <a href="<?php echo $CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=6'; ?>" class="btn gray h30">목록보기</a>
        </span>
    </h3>

    <div class="colorbox">
        <div class="l_content">
            <?php
            if (!empty($fileobj)) {
                ?>
                <img src="<?php echo $fileobj; ?>" alt="" />
                <?php
            } else {
                ?>
                <img src="/theme/oklassedu/pix/images/ex01.png" alt="" />
                <?php
            }
            ?>
        </div>
        <div class="l_content">
            <?php
            if (is_siteadmin()) {
                ?>
                <p class="t-gray"><?php echo $course->classyear . '년 ' . $course->classnum . '기' ?></p>
                <h4><a href="<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->mcid; ?>"><?php echo $course->fullname; ?></a></h4>
                <?php
            } else {
                ?>
                <h4>자격인증<?php echo $course->classyear . '년 ' . $course->classnum . '기  - ' . $course->fullname; ?></h4>
                <?php
            }
            ?>
            <dl>
                <dt>응시일</dt>
                <dd>
                    <?php
                    echo ($course->learningstart && $course->learningend) ? date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend) : '';
                    ?> 
                    <?php
                    $contentobj = preg_replace("(\<(/?[^\>]+)\>)", "", $course->objectives);
                    echo $contentobj;
                    ?>
                </dd>
                <br/>
                <dt>이수기준</dt>
                <dd>
                    <?php echo preg_replace("(\<(/?[^\>]+)\>)", "", $course->completestandard); ?>
                </dd>
            </dl>
            <div class="btn_area">
                <?php
                if ($course->enrolmentend > time() && $course->enrolmentstart < time()) {
                    if ($application) {
                        ?>
                        <input type="button" class="btn gray" value="신청취소" onclick="call_layerPop('/local/mypage/pop_payment_cancel.php', '300px', 'auto', {'id': '<?php echo $course->lcid; ?>', 'menu_gubun': '<?php echo $menu_gubun; ?>'})" />
                        <?php
                    } else {
                        if ($usertype != $course->classobject && $course->classobject != 4) {
                            ?>
                            <a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a>
                            <?php
                        } else if ($course->ca1idnumber != 'trust_course' && $usertype == 3) {
                            ?>
                            <a href="#none" onclick="alert('교육생은 신청할 수 없습니다.')"><input type="button" class="btn pink" value="신청" /></a>
                            <?php
                        } else {
                            ?>
                            <a href="<?php echo $CFG->wwwroot . '/local/course_application/application.php?menu_gubun=' . $menu_gubun . '&classid=' . $course->lcid; ?>">
                                <input type="button" value="응시신청" class="btn pink" />
                            </a>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="tab_header tab_event"> <!-- tab-event -->
        <span class="on m-col2" data-target=".data01"><a href="#">시험정보</a></span>
        <span class="m-col2" data-target=".data02"><a href="#">시험내용</a></span>
    </div>
    <div class="tab_contents">
        <div class="data01 textlist on">
            <h5>시험명</h5>
            <!--coursename-->
            <p><?php echo $course->fullname; ?></p>

            <h5>응시일</h5>
            <p><?php echo ($course->learningstart && $course->learningend) ? date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend) : ''; ?> </p>

            <h5>이수기준</h5>
            <p>
                <?php echo $course->completestandard; ?>
            </p>
        </div>
        <div class="data02 textlist">
            <h5>시험출제 내용</h5>
            <p>
                <?php
//                $contentcontent = preg_replace("(\<(/?[^\>]+)\>)", "", $course->contents);
                echo $course->contents;
                ?>
            </p>

            <h5>범위</h5>
            <p>
                <?php
//                $contentsubject = preg_replace("(\<(/?[^\>]+)\>)", "", $course->subjects);
                echo $course->subjects;
                ?>
            </p>
        </div>
    </div>
    <?php
}
// paging
//$totalcount = $DB->count_records_sql($select_count . $sql, $params);
//$total_pages = course_application_get_total_pages($totalcount, $perpage);
//$page_params = array();
//$page_params['menu_gubun'] = $menu_gubun;
//$page_params['classid'] = $classid;
//course_application_get_paging_bar($CFG->wwwroot . "/local/course_application/course_list_detail.php", $page_params, $total_pages, $page);

echo $OUTPUT->footer();
