<?php

$string['pluginname'] = 'Course Application';
$string['pluginnameplural'] = 'Course Application';

$string['training:courseno1'] = 'Job Training';
$string['training:commissioned'] = 'Commissioned Training';
$string['training:autonomy'] = 'Autonomy Trainig';
$string['training:unionmember'] = 'Union member Training';
$string['training:courseno5'] = '[Eng]선택과정';
$string['training:courseno6'] = '[Eng]자격인증';

//공통
$string['courseguide'] = 'Course guide';
$string['coursename'] = 'Course Name';
$string['trainingcoursename'] = 'Training Course name';
$string['preview'] = 'Preview';
$string['application'] = 'Application';
$string['courseinfo'] = 'Course Information';
$string['trainingperiod'] = 'Training period';
$string['th'] = 'Degree';
$string['lecturer'] = 'Lecturer';
$string['select'] = 'Select';
$string['year'] = 'Year';
$string['month'] = 'Month';
$string['week'] = 'Week';
$string['day'] = 'Day';
$string['hours'] = 'Hours';
$string['won'] = 'won';
$string['email'] = 'E-mail';
$string['coursereview'] = 'Course Review';
$string['educourseinfo'] = 'Course Infomation';
$string['courseintro'] = 'Course Introduction';
$string['teacherintro'] = 'Instructor Introduction';
$string['eduobject'] = 'Learning Objectives';
$string['eduevaluation'] = 'Assessment Methods';
$string['eduevaluation:progress'] = 'Progress {$a} Point,';
$string['eduevaluation:quiz'] = 'Exam {$a} Point,';
$string['eduevaluation:forum'] = 'Discussion Participation {$a} Point';
$string['eduevaluation:grade'] = 'Completion score{$a->min}points or more(Based on {$a->total} points), ';
$string['eduevaluation:minprogress'] = 'Minimum progress rate of {$a}% or more for completion';
$string['completecondition'] = 'Completion condition';
$string['coursesubjects'] = 'Subjects';
$string['recommendbook'] = 'Recommended Books(Individual purchase)';
$string['sellprice'] = 'price';
$string['educontent'] = 'Training content';
$string['revieworder'] = 'No.';
$string['reviewtitle'] = 'title';
$string['reviewwriter'] = 'Writer';
$string['reviewedday'] = 'Registration date';
$string['reviewview'] = 'views';
$string['noreview'] = 'There are no reviews.';
$string['noeducourse'] = 'There are no courses';
$string['looktaste'] = 'gustation';
$string['coursetype'] = 'Type of training';
$string['teacher'] = 'teacher';
$string['recruitnumber'] = 'Recruitment number';
$string['peoples'] = 'people';
$string['courseprice'] = 'Tuition';
$string['memberdiscount'] = 'Combination support';
$string['coursetime'] = 'Application period';
$string['learningtime'] = 'Learning period';
$string['link1'] = 'go to Interpark';
$string['link2'] = 'go to YES24';
$string['link3'] = 'go to Aladdin';
$string['link4'] = 'go to Kyobo Library';
$string['end'] = 'End of application';


// (local/course_application/course_list.php)직무교육-->필수과정 관련 언어
//$string['job:courseno1'] = 'The job training is approved by the Ministry of Education and Human Resources. If you are a teacher, it is reflected in the promotion grade and finalization of the teachers.';
$string['job:courseno1'] = '<div class="t-pink">[Eng]필수 과정</div> 
                                            <p>재능인으로서 기본적으로 갖추어야 할 </p>
                                            <p>지식과 스킬, 태도를 함양하기 위한 교육과정입니다. </p>';
$string['job:grades'] = 'Sort by grade';
$string['job:subjects'] = 'Sort by Subject';
$string['job:classes'] = 'Sort by Class';
$string['job:grade'] = 'Grade';
$string['job:subject'] = 'Subject';

// (local/course_application/course_list.php)직무교육-->선택과정 관련 언어
$string['job:courseno5'] = '<div class="t-pink">[Eng]선택 과정</div>
                                        <p>전문가로서 지속적인 역량향상을 위해 </p>
                                        <p>스스로 신청하고 학습하는 교육과정입니다. </p>';

// (local/course_application/course_list.php)직무교육-->자격인증과정 관련 언어
$string['job:courseno6'] = '<div class="t-pink">[Eng]자격인증</div>
                                     <p>학습내용을 바탕으로 분야별 전문성을 </p>
                                     <p>검증하는 인증과정입니다. </p>';

//위탁연수
$string['commissioned:intro'] = 'Consignment training is approved by the Ministry of Education and Human Resources Development. If you are a teacher, it is reflected in the grades of promotion and the credits of training.';
$string['commissioned:plan'] = 'Consignment Training Schedule';
$string['commissioned:comp'] = 'Agency';
$string['commissioned:status'] = 'condition';
$string['commissioned:accept'] = 'Accepting';
$string['commissioned:wait'] = 'Waiting';
$string['commissioned:price'] = 'Copayment';

//수강신청
$string['price'] = 'Price';
$string['bookprice'] = 'Book fees';
$string['courseapplication'] = 'Course Application';
$string['applicantinfo'] = 'Applicant Information';
$string['teacher'] = 'Teacher(General)';
$string['public'] = 'Public(General, Public service)';
$string['school:classify'] = 'Division of a school';
$string['affiliation'] = 'Organization(by school)';
$string['school:year'] = 'Class level/group';
$string['classlevel'] = 'Class level';
$string['classgroup'] = 'Class group';
$string['selectschool'] = 'Choosing a school';
$string['qualification'] = 'Final qualification';     
$string['neiscode'] = 'NEIS Code';
$string['duplication'] = 'Confirm duplication';
$string['coursecheck'] = 'Course check';
$string['alreadycourse'] = 'There is a report already reported. Please check if you take the course again.';
$string['exneiscode'] = 'NEIS Personal Numbers Area (3 digits) + identity Classification (in English A or the number 1 spot) + serial number (6 digits)';
$string['laterinput'] = 'Can be input and modified afterwards';
$string['traininglocationnumer'] = 'Training Location numer';
$string['exareacode1'] = 'ex) Area-School Name-Year-Serial Number';
$string['exareacode2'] = '    서울-구로중-16-001/전북-익산궁동초-16-002';
$string['dateofbirth'] = 'date of birth';
$string['phonenumber'] = 'Phone number';
$string['neisagreement'] = 'I agree to collect sensitive information items such as NEIS personal information for administrative purposes.';
$string['payinfo'] = 'Payment Information';
$string['groupdiscount'] = 'Group discount';
$string['groupregistration'] = 'Please select when registering group enrollments.';
$string['groupcode'] = 'Group Code';
$string['certification'] = 'certification';
$string['groupconditions'] = 'Discounts are available for groups of 5 or more.';
$string['groupapplication'] = 'Group Registration Application';
$string['paymentamount'] = 'Payment amount';
$string['paymentmethod'] = 'Payment Method';
$string['paymentguide'] = 'Payment Guide';
$string['creditcard'] = 'Credit card';
$string['accounttransfer'] = 'Account Transfer';
$string['virtualaccount'] = 'Virtual Account';
$string['contact'] = 'Contact us call 02-2670-9465/9466    E-mail chamcampus@gmail.com';
$string['help_virtualaccount'] = 'In case of a virtual account, you must deposit it within seven days after signing the application.';
$string['grouppayment'] = 'Please apply for collective application for collective payment.';
$string['ok'] = 'Verified.';
$string['emptygroup'] = 'Group code is not valid.';
$string['groupmaxuser'] = 'This group code was used by all personnel.';
$string['neisfalse'] = 'This is the NEIS code already enrolled in that course.';
$string['validation:agree'] = 'Please agree to the collection of sensitive information items such as NEIS personal information for administrative purposes.';
$string['validation:neis'] = 'Enter the NEIS code correctly.';
$string['validation:neischk'] = 'Please check NEIS code duplication.';
$string['validation:alreadycourse'] = 'Please check the duplicate course agreement.';
$string['validation:locationnumber'] = 'Please enter the  Training Location number.';
$string['validation:phone'] = 'Please enter your Phone number.';
$string['validation:group'] = 'Please certify the group code.';
$string['validation:email'] = 'Please enter your Email.';

$string['category'] = 'Category';
$string['memdiscount'] = 'Membership discount ';
$string['groupdisct'] = 'Group discount ';