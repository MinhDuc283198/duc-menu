<?php

$string['pluginname'] = 'Đăng ký khóa học';
$string['pluginnameplural'] = 'Đăng ký khóa học';

$string['training:courseno1'] = 'Bắt buộc';
$string['training:commissioned'] = 'Ủy thác';
$string['training:autonomy'] = 'Tự học';
$string['training:unionmember'] = 'Học nhóm';
$string['training:courseno5'] = 'Khóa học tự chọn';
$string['training:courseno6'] = 'Chứng nhận';

//공통
$string['courseguide'] = 'Hướng dẫn khóa học';
$string['coursename'] = 'Tên khóa học';
$string['trainingcoursename'] = 'Tên khóa học';
$string['preview'] = 'Đánh giá';
$string['application'] = 'Đăng ký';
$string['courseinfo'] = 'Thông tin khóa học';
$string['trainingperiod'] = 'Thời gian đào tạo';
$string['th'] = 'Cấp bậc';
$string['lecturer'] = 'Giảng viên';
$string['select'] = 'Tùy chọn';
$string['year'] = 'Năm';
$string['month'] = 'Tháng';
$string['week'] = 'Tuần';
$string['day'] = 'Ngày';
$string['hours'] = 'Giờ';
$string['won'] = 'won';
$string['email'] = 'Thư điện thử';
$string['coursereview'] = 'Đánh giá khóa học';
$string['educourseinfo'] = 'Thông tin khóa học';
$string['courseintro'] = 'Giới thiệu khóa học';
$string['teacherintro'] = 'Giới thiệu giảng viên';
$string['eduobject'] = 'Mục tiêu học tập';
$string['eduevaluation'] = 'Phương pháp đánh giá';
$string['eduevaluation:progress'] = 'Tiến trình {$a} điểm,';
$string['eduevaluation:quiz'] = 'Kiểm tra {$a} điểm,';
$string['eduevaluation:forum'] = 'Tham gia thảo luận {$a} điểm';
$string['eduevaluation:grade'] = 'Điểm hoàn thành tối thiểu {$a->min} điểm (Tổng số {$a->total} điểm), ';
$string['eduevaluation:minprogress'] = 'Để hoàn thành, tỷ lệ tiến trình tối thiểu {$a}%';
$string['completecondition'] = 'Điều kiện hoàn thành';
$string['coursesubjects'] = 'Mục tiêu khóa học';
$string['recommendbook'] = 'Giáo trình đề xuất (Mua riêng)';
$string['sellprice'] = 'Giá';
$string['educontent'] = 'Nội dung đào tạo';
$string['revieworder'] = 'No.';
$string['reviewtitle'] = 'Tiêu đề';
$string['reviewwriter'] = 'Tác giả';
$string['reviewedday'] = 'Ngày đăng ký';
$string['reviewview'] = 'Lượt xem';
$string['noreview'] = 'Không có đánh giá';
$string['noeducourse'] = 'Không có khóa học';
$string['looktaste'] = 'gustation';
$string['coursetype'] = 'Hình thức đào tạo';
$string['teacher'] = 'Giảng viên';
$string['recruitnumber'] = 'Số lượng tuyển';
$string['peoples'] = 'học viên';
$string['courseprice'] = 'Học phí';
$string['memberdiscount'] = 'Hỗ trợ học viên';
$string['coursetime'] = 'Thời gian đăng ký';
$string['learningtime'] = 'Thời gian đào tạo';
$string['link1'] = 'Đi đến Interpark';
$string['link2'] = 'Đi đến YES24';
$string['link3'] = 'Đi đến Aladdin';
$string['link4'] = 'Đi đến Kyobo Library';
$string['end'] = 'Kết thúc đăng ký';


// (local/course_application/course_list.php)직무교육-->필수과정 관련 언어
//$string['job:courseno1'] = 'The job training is approved by the Ministry of Education and Human Resources. If you are a teacher, it is reflected in the promotion grade and finalization of the teachers.';
$string['job:courseno1'] = '<div class="t-pink">필수 과정</div>
                                            <p>재능인으로서 기본적으로 갖추어야 할 </p>
                                            <p>지식과 스킬, 태도를 함양하기 위한 교육과정입니다. </p>';

$string['job:grades'] = 'Sắp xếp theo cấp bậc';
$string['job:subjects'] = 'Sắp xếp theo môn học';
$string['job:classes'] = 'Sắp xếp theo lớp học';
$string['job:grade'] = 'Cấp bậc';
$string['job:subject'] = 'Môn học';

// (local/course_application/course_list.php)직무교육-->선택과정 관련 언어
$string['job:courseno5'] = '<div class="t-pink">[Eng]선택 과정</div>
                                        <p>전문가로서 지속적인 역량향상을 위해 </p>
                                        <p>스스로 신청하고 학습하는 교육과정입니다. </p>';

// (local/course_application/course_list.php)직무교육-->자격인증과정 관련 언어
$string['job:courseno6'] = '<div class="t-pink">[Eng]자격인증</div>
                                     <p>학습내용을 바탕으로 분야별 전문성을 </p>
                                     <p>검증하는 인증과정입니다. </p>';

//위탁연수
$string['commissioned:intro'] = 'Đào tạo ủy thác được Bộ Giáo dục và Phát triển nguồn nhân lực phê duyệt. Nếu bạn là giảng viên, điều đó được phản ảnh ở cấp bậc và tín chỉ đào tạo';
$string['commissioned:plan'] = 'Lịch trình đào tạo ủy thác';
$string['commissioned:comp'] = 'Chi nhánh';
$string['commissioned:status'] = 'Điều kiện';
$string['commissioned:accept'] = 'Chấp nhận';
$string['commissioned:wait'] = 'Đang chờ';
$string['commissioned:price'] = 'Giảm giá';

//수강신청
$string['price'] = 'Giá';
$string['bookprice'] = 'Phí giáo trình';
$string['courseapplication'] = 'Đăng ký khóa học';
$string['applicantinfo'] = 'Thông tin người đăng ký';
$string['teacher'] = 'Giảng viên';
$string['public'] = 'Công khai';
$string['school:classify'] = 'Phân hiệu';
$string['affiliation'] = 'Trường liên kết';
$string['school:year'] = 'Bậc/Lớp';
$string['classlevel'] = 'Bậc';
$string['classgroup'] = 'Nhóm lớp';
$string['selectschool'] = 'Chọn trường';
$string['qualification'] = 'Trình độ chuyên môn';     
$string['neiscode'] = 'Số cá nhân NEIS';
$string['duplication'] = 'Kiểm tra trùng lặp';
$string['coursecheck'] = 'Kiểm tra khóa học';
$string['alreadycourse'] = 'Báo cáo được gửi đi. Vui lòng kiểm tra nếu bạn muốn nhận khóa học trở lại';
$string['exneiscode'] = 'Số cá nhân NEIS (3 số) + định danh (1 kí tự) + số sê-ri (6 số)';
$string['laterinput'] = 'Có thể nhập và chỉnh sửa';
$string['traininglocationnumer'] = 'Địa điểm đào tạo';
$string['exareacode1'] = 'ex) Khu vực - Tên trường - Năm - Số sê-ri';
$string['exareacode2'] = '    서울-구로중-16-001/전북-익산궁동초-16-002';
$string['dateofbirth'] = 'Ngày sinh';
$string['phonenumber'] = 'Số điện thoại';
$string['neisagreement'] = 'Tôi đồng ý chia sẻ các mục thông tin nhạy cảm như thông tin cá nhân NEIS cho mục đích quản trị';
$string['payinfo'] = 'Thông tin thanh toán';
$string['groupdiscount'] = 'Giảm giá nhóm';
$string['groupregistration'] = 'Vui lòng lựa chọn khi đăng ký nhóm';
$string['groupcode'] = 'Mã nhóm';
$string['certification'] = 'Chứng chỉ';
$string['groupconditions'] = 'Giảm giá cho nhóm có ít nhất 5 thành viên';
$string['groupapplication'] = 'Đăng ký nhóm';
$string['paymentamount'] = 'Số tiền thanh toán';
$string['paymentmethod'] = 'Phương thức thanh toán';
$string['paymentguide'] = 'Hướng dẫn thanh toán';
$string['creditcard'] = 'Thẻ tín dụng';
$string['accounttransfer'] = 'Chuyển khoản';
$string['virtualaccount'] = 'Thanh toán sau';
$string['contact'] = 'Liên hệ chúng tôi, gọi 02-2670-9465/9466    Thư điện tử: chamcampus@gmail.com';
$string['help_virtualaccount'] = 'Trong trường hợp thanh toán sau, bạn phải hoàn tất thanh toán sau 7 ngày kể từ ngày đăng ký';
$string['grouppayment'] = 'Vui lòng nộp đơn đăng ký nhóm để thanh toán nhóm.';
$string['ok'] = 'Xác nhận';
$string['emptygroup'] = 'Mã nhóm không tồn tại';
$string['groupmaxuser'] = 'Mã nhóm này được sử dụng bởi tất cả các thành viên';
$string['neisfalse'] = 'Đây là mã số cá nhân NEIS được sử dụng đăng ký khó học này';
$string['validation:agree'] = 'Vui lòng đồng ý chia sẻ thông tin cá nhân vì mục đích quản trị';
$string['validation:neis'] = 'Vui lòng nhập đúng mã số cá nhân NEIS';
$string['validation:neischk'] = 'Vui lòng kiểm tra trùng lặp mã số cá nhân NEIS';
$string['validation:alreadycourse'] = 'Vui lòng kiểm tra trùng lặp thỏa thuận khóa học';
$string['validation:locationnumber'] = 'Vui lòng nhập số địa điểm đào tạo';
$string['validation:phone'] = 'Vui lòng nhập số điện thoại của bạn';
$string['validation:group'] = 'Vui lòng xác thực mã số nhóm';
$string['validation:email'] = 'Vui lòng nhập địa chỉ thư điện tử';

$string['category'] = 'Thể loại';
$string['memdiscount'] = 'Giảm giá thành viên';
$string['groupdisct'] = 'Giảm giá nhóm';