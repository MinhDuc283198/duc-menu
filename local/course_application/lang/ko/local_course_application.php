<?php

$string['pluginname'] = '수강신청';
$string['pluginnameplural'] = '수강신청';

$string['training:courseno1'] = '필수과정';
$string['training:commissioned'] = '위탁교육';
$string['training:autonomy'] = '자율교육';
$string['training:unionmember'] = '조합원교육';
$string['training:courseno5'] = '선택과정';
$string['training:courseno6'] = '자격인증';

//공통
$string['courseguide'] = '과정안내';
$string['coursename'] = '과정명';
$string['trainingcoursename'] = '교육과정명';
$string['preview'] = '미리보기';
$string['application'] = '신청';
$string['courseinfo'] = '수강신청과정';
$string['trainingperiod'] = '교육기간';
$string['th'] = '차수';
$string['lecturer'] = '강사';
$string['select'] = '선택';
$string['year'] = '년';
$string['month'] = '월';
$string['week'] = '주';
$string['day'] = '일';
$string['hours'] = '시간';
$string['won'] = '원';
$string['email'] = '이메일';
$string['coursereview'] = '교육후기';
$string['educourseinfo'] = '강의정보';
$string['courseintro'] = '강의소개';
$string['teacherintro'] = '강사소개';
$string['eduobject'] = '학습목표';
$string['eduevaluation'] = '평가방법';
$string['eduevaluation:progress'] = '진도율 {$a}점,';
$string['eduevaluation:quiz'] = '시험 {$a}점,';
$string['eduevaluation:forum'] = '토론참여 {$a}점';
$string['eduevaluation:grade'] = '수료점수{$a->min}점이상({$a->total}점기준), ';
$string['eduevaluation:minprogress'] = '수료를위한 최소진도율 {$a}%이상';
$string['completecondition'] = '이수조건';
$string['coursesubjects'] = '수강대상';
$string['recommendbook'] = '추천도서(개별 구매)';
$string['sellprice'] = '판매가';
$string['educontent'] = '교육내용';
$string['revieworder'] = 'No.';
$string['reviewtitle'] = '제목';
$string['reviewwriter'] = '작성자';
$string['reviewedday'] = '등록일';
$string['reviewview'] = '조회수';
$string['noreview'] = '등록된 후기가 없습니다.';
$string['noeducourses'] = '등록된 교육가 없습니다.';
$string['looktaste'] = '맛보기';
$string['coursetype'] = '교육종별';
$string['teacher'] = '강사';
$string['recruitnumber'] = '모집인원';
$string['peoples'] = '명';
$string['courseprice'] = '수강료';
$string['memberdiscount'] = '조합지원';
$string['coursetime'] = '수강신청기간';
$string['learningtime'] = '학습기간';
$string['link1'] = '인터파크 바로가기';
$string['link2'] = 'YES24 바로가기';
$string['link3'] = '알라딘 바로가기';
$string['link4'] = '교보문고 바로가기';
$string['end'] = '신청종료';


// (local/course_application/course_list.php)직무교육-->필수과정 관련 언어
$string['job:courseno1'] = '<div class="t-pink">필수 과정</div> 
                                            <p>재능인으로서 기본적으로 갖추어야 할 </p>
                                            <p>지식과 스킬, 태도를 함양하기 위한 교육과정입니다. </p>';
$string['job:grades'] = '학점별';
$string['job:subjects'] = '주제별';
$string['job:classes'] = '급별';
$string['job:grade'] = '학점';
$string['job:subject'] = '주제';

// (local/course_application/course_list.php)직무교육-->선택과정 관련 언어
$string['job:courseno5'] = '<div class="t-pink">선택 과정</div>
                                        <p>전문가로서 지속적인 역량향상을 위해 </p>
                                        <p>스스로 신청하고 학습하는 교육과정입니다. </p>';

// (local/course_application/course_list.php)직무교육-->자격인증과정 관련 언어
$string['job:courseno6'] = '<div class="t-pink">자격인증</div>
                                     <p>학습내용을 바탕으로 분야별 전문성을 </p>
                                     <p>검증하는 인증과정입니다. </p>';

//위탁교육-->종합반 관련 언어
$string['commissioned:intro'] = '종합반에 대한 설명이 이곳에 노출됩니다.';
$string['commissioned:plan'] = '종합반 일정';
$string['commissioned:comp'] = '과정명';
$string['commissioned:status'] = '상태';
$string['commissioned:accept'] = '접수중';
$string['commissioned:wait'] = '대기중';
$string['commissioned:price'] = '본인 부담금';

//수강신청
$string['price'] = '교육비';
$string['bookprice'] = '교재비';
$string['courseapplication'] = '수강신청';
$string['applicantinfo'] = '신청자 정보';
$string['teacher'] = '교원(전문직 포함)';
$string['public'] = '일반인(공무직, 일반직 포함)';
$string['school:classify'] = '학교구분';
$string['affiliation'] = '소속학교';
$string['school:year'] = '학년/반';
$string['classlevel'] = '학년';
$string['classgroup'] = '반';
$string['selectschool'] = '학교선택';
$string['qualification'] = '최종자격';
$string['neiscode'] = 'NEIS 개인번호';
$string['duplication'] = '중복체크';
$string['coursecheck'] = '수강체크';
$string['alreadycourse'] = '이미 이수보고된 내역이 있습니다. 다시 수강하실 경우 체크해주세요.';
$string['exneiscode'] = 'NEIS 개인번호는 지역(3자리) + 신분구분(영문A 또는 숫자1자리) + 일련번호(6자리)';
$string['laterinput'] = '추후 입력 및 수정 가능';
$string['traininglocationnumer'] = '교육지명번호';
$string['exareacode1'] = '예: 시도-학교명-연도-일련번호';
$string['exareacode2'] = '    서울-구로중-16-001/전북-익산궁동초-16-002';
$string['dateofbirth'] = '생년월일';
$string['phonenumber'] = '휴대폰번호';
$string['neisagreement'] = '개인정보 항목 수집에 동의합니다.';
$string['payinfo'] = '결제 정보';
$string['groupdiscount'] = '단체할인';
$string['groupregistration'] = '단체수강신청 등록 시 선택하십시오';
$string['groupcode'] = '단체코드입력';
$string['certification'] = '인증';
$string['groupconditions'] = '5인 이상 단체등록 시 할인 혜택을 받으실 수 있습니다.';
$string['groupapplication'] = '단체등록신청';
$string['paymentamount'] = '결제금액';
$string['paymentmethod'] = '결제방법';
$string['paymentguide'] = '결제안내';
$string['creditcard'] = '신용카드';
$string['accounttransfer'] = '실시간계좌이체';
$string['virtualaccount'] = '가상계좌';
$string['contact'] = '문의전화 : 02-2670-9465/9466   문의메일: chamcampus@gmail.com';
$string['help_virtualaccount'] = '가상계좌의 경우 수강신청 후 7일 이내에 입금해주셔야 합니다.';
$string['grouppayment'] = '단체수강 일괄 입금 신청은 단체신청을 통해 신청해주시기 바랍니다.';
$string['ok'] = '인증되었습니다.';
$string['emptygroup'] = '단체코드가 유효하지 않습니다.';
$string['groupmaxuser'] = '해당 단체코드는 모든인원이 사용하였습니다.';
$string['neisfalse'] = '이미 해당 차수에 수강신청 된 NEIS코드입니다.';
$string['validation:agree'] = '개인정보 항목 수집에 동의해 주십시오.';
$string['validation:neis'] = 'NEIS코드를 정확히 입력해 주십시오.';
$string['validation:neischk'] = 'NEIS코드 중복확인을 해 주십시오.';
$string['validation:alreadycourse'] = '중복수강 동의를 체크해 주십시오.';
$string['validation:locationnumber'] = '교육지명번호를 입력해 주십시오.';
$string['validation:phone'] = '휴대폰번호를 입력해 주십시오.';
$string['validation:group'] = '단체코드인증을 해 주십시오.';
$string['validation:email'] = '이메일을 입력해 주십시오.';

$string['category'] = '분류';
$string['memdiscount'] = '조합원 할인 ';
$string['groupdisct'] = '단체 할인 ';