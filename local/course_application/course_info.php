<?php
/**
 * 과정 상세보기 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/course_application/lib.php';
//require_once $CFG->dirroot . '/local/courselist/lib.php';
require_once $CFG->dirroot . '/lib/filelib.php';
require_once $CFG->dirroot . '/chamktu/lib.php';
$context = context_system::instance();

$menu_gubun = optional_param("menu_gubun", 1, PARAM_INT);
$courseid = optional_param("courseid", 0, PARAM_INT);
$classid = optional_param("classid", 0, PARAM_INT);
$page = optional_param("page", 1, PARAM_INT);
$perpage = 10;
$boardid = $DB->get_field_sql("select id from {jinoboard} where name='교육후기'");

$sql = 'SELECT SUM(grademax) as totalgrade FROM {grade_items} WHERE courseid = :courseid AND itemmodule = :itemmodule';
$quiz_grade = $DB->get_record_sql($sql, array('courseid'=>$courseid, 'itemmodule'=>'quiz'));

$sql = 'SELECT SUM(scale) as totalgrade FROM {forum} WHERE course = :course';
$forum_grade = $DB->get_record_sql($sql, array('course'=>$courseid));

$total_grade = $DB->get_record('grade_items', array('courseid'=>$course->id, 'itemtype'=>'course'));

$min_progress = $DB->get_record('lcmsprogress',array('course'=>$courseid));
$min_grade = $DB->get_record('course_completion_criteria',array('course'=>$courseid, 'criteriatype'=>6));

$current_time = time();

$current_class = $DB->get_record('lmsdata_class', array('id'=>$classid));

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string('courseguide', 'local_course_application'));

if ($menu_gubun == 1) {
    $PAGE->navbar->add(get_string('training:job', 'local_course_application'));
    $PAGE->set_title(get_string('training:job', 'local_course_application'));
    $PAGE->set_heading(get_string('training:job', 'local_course_application'));
} else if ($menu_gubun == 2) {
    $PAGE->navbar->add(get_string('training:commissioned', 'local_course_application'));
    $PAGE->set_title(get_string('training:commissioned', 'local_course_application'));
    $PAGE->set_heading(get_string('training:commissioned', 'local_course_application'));
} else if ($menu_gubun == 3) {
    $PAGE->navbar->add(get_string('training:autonomy', 'local_course_application'));
    $PAGE->set_title(get_string('training:autonomy', 'local_course_application'));
    $PAGE->set_heading(get_string('training:autonomy', 'local_course_application'));
} else if ($menu_gubun == 4) {
    $PAGE->navbar->add(get_string('training:unionmember', 'local_course_application'));
    $PAGE->set_title(get_string('training:unionmember', 'local_course_application'));
    $PAGE->set_heading(get_string('training:unionmember', 'local_course_application'));
}
echo $OUTPUT->header();

$cont = $DB->get_record_sql("select * from {lmsdata_course} where courseid=$courseid");
$time = $DB->get_records_sql("select  lc.*,lcl.id as lid, min(lcl.enrolmentstart) as emin,max(lcl.enrolmentend) as emax,min(lcl.learningstart) as lmin,max(lcl.learningend) as lmax from m_lmsdata_course lc join m_lmsdata_class lcl on lc.id=lcl.parentcourseid where lc.courseid=$courseid");
foreach ($time as $ti => $t) {
    $$ti = $t;
}
if($cont->coursegrade){
    $grade = $DB->get_field_sql("select codename from {lmsdata_code} where id=$cont->coursegrade and depth=2");
}
if($cont->courserating){
    $rate = $DB->get_field_sql("select codename from {lmsdata_code} where id=$cont->courserating and depth=2");
}
$teacher = $DB->get_records_sql("select * from {lmsdata_course_teacher} where courseid=$cont->courseid and type=1");
$class = $DB->get_record_sql("select * from {lmsdata_class} lcl where parentcourseid=$cont->id");
$books = $DB->get_records_sql("select lr.id as itemid , lr.* from {lmsdata_course_book} lcb join {lmsdata_rebooks} lr on lcb.bookid=lr.id and lcb.courseid=$courseid");
if($cont->coursetype){
    $catename = $DB->get_field('course_categories','name',array('id'=>$cont->coursetype));
}
?>  

<style>
    div {margin-bottom:1000px;}

    .small { height: 95px; width: 80px; }

</style>


<h3 class="page_title">
    <?php
    if ($cont->classtype == 1) {
        echo get_string('training:job', 'local_course_application');
    } else if ($cont->classtype == 2) {
        echo get_string('training:commissioned', 'local_course_application');
    } else if ($cont->classtype == 3) {
        echo get_string('training:autonomy', 'local_course_application');
    } else if ($cont->classtype == 4) {
        echo get_string('training:unionmember', 'local_course_application');
    }
    ?>
    <span class="r_content">
        <a href='<?php echo $CFG->wwwroot?>/local/course_application/course_list.php?menu_gubun=<?php echo $menu_gubun;?>'><input type="button" class="btn h30 orange" value="<?php echo get_string('detail:list','local_repository');?>" /></a>
    </span>
</h3>   

<div class="wrap-contentsbox">
    <div class="colorbox">
        <div class="l_content">
            <?php
            $context = context_course::instance($cont->courseid);
            $url = theme_oklassedu_get_file($context->id, 'thumbnailimg');
            ?>
            <img src="<?php echo ($url)? $url:'/theme/oklassedu/pix/images/sample.jpg';?>" alt="" width="260px" height="145px" />
            <div class="buttons">
                <!-- 맛보기버튼 -->
                <?php if(!empty($cont->sampleurl)){?>
                <input type="button" onclick="window.open('<?php echo $cont->sampleurl;?>')" value="<?php echo get_string('looktaste','local_course_application');?>" class="btn tab_btn m-col2" />
                <?php } else {?>
                <input type="button" value="<?php echo get_string('looktaste','local_course_application');?>" class="btn tab_btn m-col2" />
                <?php }?>
                <?php
                if(!empty($current_class) && ($current_class->enrolmentstart > $current_time || $current_class->enrolmentend < $current_time)){
                ?>
                    <input type="button" value="<?php echo get_string('courseapplication','local_course_application');?>" class="btn tab_btn gray m-col2"/>
                <?php
                } else {
                    if($menu_gubun == 2){
                        $applyurl = "consignment_application_click('".$classid."', '".$cont->id."');";
                    }else{
                        $applyurl = "application_click('".$cont->id."');";
                    }
                ?>
                    <input type="button" value="<?php echo get_string('courseapplication','local_course_application');?>" class="btn tab_btn orange m-col2" onclick="<?php echo $applyurl;?>"/>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="l_content">
            <h4><?php echo $cont->coursename ?></h4>
            <dl>
                <dt><?php echo get_string('category','local_course_application')?></dt>
                <dd>
                    <?php
                        echo $catename;
                    ?>
                </dd>
                <dt><?php echo get_string('coursetype','local_course_application')?></dt>
                <dd>
                    <?php
                        if ($menu_gubun == 1) {
                            echo get_string('training:job', 'local_course_application');
                        } else if ($menu_gubun == 2) {
                            echo get_string('training:commissioned', 'local_course_application');
                        } else if ($menu_gubun == 3) {
                            echo get_string('training:autonomy', 'local_course_application');
                        } else if ($menu_gubun == 4) {
                            echo get_string('training:unionmember', 'local_course_application');
                        }
                        echo ' / '.$grade 
                    ?>
                </dd>
                <dt><?php echo get_string('lecturer','local_course_application');?></dt>
                <dd>
                    <?php
                        foreach ($teacher as $tea) {//강사 정보 가져옴
                            $result = $DB->get_field_sql("select mu.lastname from {lmsdata_user} lu join {user} mu on mu.id = lu.userid where lu.id=$tea->lmsuserid");
                            echo $result.'&nbsp;&nbsp;&nbsp;';
                        }
                    ?>
                </dd>
                <?php
                if ($class->classlimit == 0 || $class->classlimit == null) {
                    echo '';
                } else {
                    echo '<dt>'.get_string('recruitnumber','local_course_application').'</dt>' .
                    '<dd>' . $class->classlimit . get_string('peoples','local_course_application').'</dd>';
                }
                ?>

                <dt><?php echo get_string('courseprice','local_course_application');?></dt>
                <dd>
                    <?php echo number_format($t->price).' '.get_string('won','local_course_application'); ?>  <?php
                    
                        $memdiscount = $t->memberdiscount; // 조합지원
                        $groupdiscount = $t->groupdiscount;// 단체 할인
                        
                        //get_string('memberdiscount','local_course_application') = 조합지원 
                        //get_string('memberdiscount','local_course_application') = 단체할인
                        
                        if (!empty($memdiscount) && !empty($groupdiscount)) { // 조합지원, 단체할인이 존재할경우
                            echo '('.get_string('memdiscount','local_course_application').$memdiscount.' % - '.number_format($t->price*(1-0.01*$memdiscount)).' '.get_string('won','local_course_application').' / '.get_string('groupdisct', 'local_course_application').$groupdiscount.'% - '.number_format($t->price*(1-0.01*$groupdiscount)).' '.get_string('won','local_course_application').')';
                        } else if (!empty($memdiscount)) { // 조합지원 할인만 존재할경우 
                            echo '('.get_string('memdiscount','local_course_application').$memdiscount.' % - '.number_format($t->price*(1-0.01*$memdiscount)).' '.get_string('won','local_course_application').')';
                        } else if (!empty($groupdiscount)) {// 단체할이만 존재 할경우
                            echo '('.get_string('groupdisct', 'local_course_application').$groupdiscount.' % - '.number_format($t->price*(1-0.01*$groupdiscount)).' '.get_string('won','local_course_application').')';
                        } else {
                            echo '';
                        }
                    ?>
                </dd>
            </dl>
        </div>
    </div>
    
    <div class="tab_header">
        <a href="#list01" onclick="fnMove('1')"><span class="on m-col4"><?php echo get_string('educourseinfo','local_course_application');?></span></a>
        <a href="#list02" onclick="fnMove('2')"><span class="m-col4"><?php echo get_string('educontent','local_course_application');?></span></a>
        <a href="#list03" onclick="fnMove('3')"><span class="m-col4"><?php echo get_string('teacherintro','local_course_application');?></span></a>
        <a href="#list04" onclick="fnMove('4')"><span class="m-col4"><?php echo get_string('coursereview','local_course_application');?></span></a>
    </div>
    <div class="nobg">
        <div class="on textlist">
            <h5 id="list01" class="orange" id="div1"><?php echo get_string('courseintro','local_course_application');?></h5>
            <p>
                <?php echo $t->intro ?>
            </p>

            <h5 class="orange"><?php echo get_string('eduobject','local_course_application');?></h5>
            <p><?php echo $t->objectives ?></p>
            
            <!-- 평가방법 -->
            <h5 class="orange"><?php echo get_string('eduevaluation','local_course_application');?></h5>
            <p>
                <?php if($min_progress->grade > 0) echo get_string('eduevaluation:progress','local_course_application',$min_progress->grade);?>
                <?php if($quiz_grade->totalgrade > 0) echo get_string('eduevaluation:quiz','local_course_application',(int)$quiz_grade->totalgrade);?>
                <?php if($forum_grade->totalgrade > 0) echo get_string('eduevaluation:forum','local_course_application',$forum_grade->totalgrade);?>
            </p>
            
            <!-- 이수조건 -->
            <h5 class="orange"><?php echo get_string('completecondition','local_course_application');?></h5>
            <p>
                <?php if($min_grade->gradepass > 0 && $total_grade->grademax > 0) echo get_string('eduevaluation:grade','local_course_application',array('min'=>(int)$min_grade->gradepass, 'total'=>(int)$total_grade->grademax));?>
                <?php if($min_progress->completionprogress > 0) echo get_string('eduevaluation:minprogress','local_course_application',$min_progress->completionprogress);?>
            </p>

            <h5 class="orange"><?php echo get_string('coursesubjects','local_course_application');?></h5>
            <p><?php echo $t->subjects ?></p>

            <h5 class="orange"><?php echo get_string('recommendbook','local_course_application');?></h5>
            <?php
            $context1 = context_system::instance();
            foreach ($books as $book) {
                $fs = get_file_storage();
                $files = $fs->get_area_files($context1->id, 'local_lmsdata', 'attachment', $book->itemid, 'timemodified', false);
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context1->id . '/local_lmsdata/attachment/' . $book->itemid . '/' . $filename);
                }
                ?>
                <div class="book">
                    <?php if($path){
                        echo '<img src="'.$path.'" class="small" alt="book01" />';
                    }else{
                        echo '<img src="'.$CFG->wwwroot.'/theme/oklassedu/pix/images/noimage.jpg" class="small" alt="book01" />';
                    }?>
                    
                    <div class="binfo">
                        <div class="l_content">
                            <h5><?php echo $book->name ?></h5>
                            <p>
                                <span class="division"><?php echo $book->author ?></span>
                                <span><?php echo $book->publisher ?></span>
                            </p>
                            <p>
                                <?php echo get_string('sellprice','local_course_application').':'.number_format($book->price).get_string('won','local_course_application'); ?>
                            </p>
                        </div>
                        <div class="r_content buttons">
                            <a href="<?php echo $book->link1 ?>" target="_blank"><input type="button" class="btn gray" value="<?php echo get_string('link1','local_course_application');?>" /></a>
                            <a href="<?php echo $book->link2 ?>" target="_blank"><input type="button" class="btn gray" value="<?php echo get_string('link2','local_course_application');?>" /></a>
                            <a href="<?php echo $book->link3 ?>" target="_blank"><input type="button" class="btn gray" value="<?php echo get_string('link3','local_course_application');?>" /></a>
                            <a href="<?php echo $book->link4 ?>" target="_blank"><input type="button" class="btn gray" value="<?php echo get_string('link4','local_course_application');?>" /></a>
                        </div>
                    </div>


                </div>


    <?php
}
?>


            <h5 id="list02" class="orange" id="div2"><?php echo get_string('educontent','local_course_application');?></h5>
            <dl>
                <dd><?php echo $t->contents ?></dd>
            </dl>


        </div>
    </div>
    <div id="list03" class="div_title orange">
        <?php echo get_string('teacherintro','local_course_application');?>
    </div>
    <?php
    foreach ($teacher as $tea) {
        $path2 = '';
        $context = CONTEXT_COURSE::instance($courseid);
        $fs = get_file_storage();
        $files2 = $fs->get_area_files($context->id, 'local_courselist', 'teacherimg', $tea->id, 'id', false);
        foreach ($files2 as $file2) {
            $filename2 = $file2->get_filename();
            $path2 = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_courselist/teacherimg/' . $tea->id . '/' . $filename2);
        }
        $result = $DB->get_field_sql("select mu.lastname from {lmsdata_user} lu join {user} mu on mu.id = lu.userid where lu.id=$tea->lmsuserid");
        ?>

        <ul class="teacherbox">
            <li>
                <div class="l_content">
                    <?php if($path2){
                        echo '<img src="'.$path2.'" width="100" alt="" />';
                    }else{
                        echo '<img src="'.$CFG->wwwroot.'/theme/oklassedu/pix/images/nouser.jpg" width="100" alt="" />';
                    }?>

                </div>
                <div class="r_content">
                    <h5 class="orange name"><?php echo $result ?></h5>
                    <ul>
    <?php echo $tea->teacherhistory ?>
                    </ul>
                </div>
            </li>
        </ul>
<?php }
?>
</div>
<h5 id="list04" class="div_title" id="div3">
    <?php echo get_string('coursereview','local_course_application');?>
    <p class="r_content">
        <a href="<?php echo $CFG->wwwroot . '/local/jinoboard/write_uncore.php?board=6';?>"><input type="button" value="후기작성" name="btn01" class="btn orange h30" /></a>
    </p>
</h5>

<table class="table">
    <thead>
        <tr>
            <th><?php echo get_string('revieworder','local_course_application');?></th>
            <th><?php echo get_string('reviewtitle','local_course_application');?></th>
            <th><?php echo get_string('reviewwriter','local_course_application');?></th>
            <th><?php echo get_string('reviewedday','local_course_application');?></th>
            <th><?php echo get_string('reviewview','local_course_application');?></th>
        </tr>
    </thead>

    <tbody>
        <?php
        $offset = 0;
        if ($page != 0) {
            $offset = ($page-1) * $perpage;
        }
        $params = array();
        $params['type'] = 6;
        $params['lcourseid'] = $cont->id;
        
        $select = 'select jbc.*';
        $select_count = 'select count(jbc.id)';

        $sql = " from {jinoboard} jb
                JOIN {jinoboard_contents} jbc on jbc.board = jb.id
                where jb.type=:type and jbc.lcourseid = :lcourseid order by jbc.id desc, timecreated desc ";

        $reviews = $DB->get_records_sql($select.$sql, $params, $offset, $perpage);

        $totalcount = $DB->count_records_sql($select_count.$sql, $params);
        $total_pages = course_application_get_total_pages($totalcount, $perpage);
        
        if ($reviews) {
            foreach ($reviews as $review) {
                $offset = 0;
                if ($page != 0) {
                    $offset = ($page - 1) * $perpage;
                }
                $num = $totalcount - $offset;

                $sql = "select lastname from {user} where id=$review->userid";
                $username = $DB->get_field_sql($sql);
                ?>

                <tr>
                    <td><?php echo $num; ?></td>
                    <td class="text-left"><a href="<?php echo $CFG->wwwroot . '/local/jinoboard/detail_uncore.php?id=' . $review->id.'&board=6' ?>"><?php echo $review->title ?></a></td>
                    <td><?php echo $username ?></td>
                    <td><?php echo date('y/m/d', $review->timecreated) ?></td>
                    <td><?php echo $review->viewcnt ?></td>
                </tr>
                <?php
                $num--;
            }
        } else {
            echo '<tr><td colspan=5>'.get_string('noreview','local_course_application').'</td></tr>';
        }
        ?>


    </tbody>
</table>
<div class="table-footer-area">
    <?php
    $page_params = array();
    $page_params['menu_gubun'] = $menu_gubun;
    $page_params['courseid'] = $courseid;
    course_application_get_paging_bar($CFG->wwwroot . "/local/course_application/course_info.php", $page_params, $total_pages, $page);
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->









<?php
echo $OUTPUT->footer();
?>


<script>
    /**
     * 교육후기 작성페이지로 이동
     * @returns {undefined}
     */
    function write_review() {
        window.location.href("<?php echo $CFG->wwwroot ?>/local/jinoboard/write_uncore.php?board=6&courseid?<php echo $courseid?>");
    }

    /**
     * 스크롤 이동 함수
     * @param {int} seq
     * @returns {undefined}
     */
    function fnMove(seq) {
        var offset = $("#div" + seq).offset();
        $('html, body').animate({scrollTop: offset.top}, 400);
    }
    
    /**
     * 직무, 자율, 조합원 교육 수강신청 시 동일한 과정에 수강신청 정보가 있는지 체크
     * @param {int} id
     * @returns {undefined}
     */
    function application_click(id){
        $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/course_application/application_chk.php"?>',
            method: 'POST',
            data : {
                courseid : id,
                userid : '<?php echo $USER->id;?>',
                menu_gubun : '<?php echo $menu_gubun;?>'
            },
            success: function(data) {
                if(data == 'true'){
                    location.href = '/local/course_application/application.php?courseid='+id+'&menu_gubun=<?php echo $menu_gubun;?>';
                } else {
                    call_layerPop("/local/course_application/pop_application.php", "800px", "auto", {id:id, menu_gubun:<?php echo $menu_gubun;?>});
                }
            }
        });
    }
    
    /**
    * 위탁 교육 수강신청 시 동일한 과정에 수강신청 정보가 있는지 체크

     * @param {int} classid
     * @param {int} lcourseid
     * @returns {undefined}     */
    function consignment_application_click(classid, lcourseid){
        $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/course_application/application_chk.php"?>',
            method: 'POST',
            data : {
                courseid : lcourseid,
                userid : '<?php echo $USER->id;?>',
                menu_gubun : '<?php echo $menu_gubun;?>',
                scheduleid : '<?php echo $schedule->id;?>'
            },
            success: function(data) {
                if(data == 'limit'){
                    alert('해당 위탁교육에서 한 사람이 수강신청할 수 있는 과정은 <?php echo $schedule->application_limit;?>개가 최대입니다.')
                } else if(data == 'true'){
                    location.href = '/local/course_application/application.php?courseid='+classid+'&menu_gubun=<?php echo $menu_gubun;?>';
                } else {
                    call_layerPop("/local/course_application/pop_application.php", "800px", "auto", {id:lcourseid, menu_gubun:<?php echo $menu_gubun;?>, classtype:2, classid:classid});
                }
            }
        });
    }

    /**
    * 처음 문서를 열었을 때 Top 버튼 위치 설정

     * @type type     */
    $(document).ready(function () {

        //var currentHeight = $(window).scrollTop() + 300; 

        var currentHeight = $(window).scrollTop() + ($(window).height() - 430);

        $(".btn-top-2").css("top", currentHeight);

    })

    /**
    * 스크롤시 Top버튼 위치 변경

     * @type type     */
    $(window).scroll(function () {
        var divHeight = $(".wrap-contentsbox").height();
        // var currentHeight = $(window).scrollTop() + 300; 300을 더한 이유는 위치가 항상 밑에서 30px 정도 높게 
        //나오길 원해서 브라우저 height 값이 바뀔 경우 제대로 대응하지 못함
        var currentHeight = $(window).scrollTop() + ($(window).height() - 430);
        
        // 해당 Div의 높이를 넘어가지 않도록 설정,  
        if (divHeight > currentHeight) {
            $(".btn-top-2").animate({top: currentHeight + "px"}, {queue: false, duration: 300});
        }
        // 넘어갈 경우 div 값으로 고정
        else {
            $(".btn-top-2").animate({top: divHeight + "px"}, {queue: false, duration: 300});
        }

    });


</script>