<?php
/**
 * 위탁교육 리스트 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("과정안내");
$PAGE->navbar->add("위탁교육");

$menu_gubun = optional_param('menu_gubun',0, PARAM_INT);

$year = date('Y');
$firstyeardate = strtotime($year.'-1-1');
$lastyeardate = strtotime($year.'-12-31 23:59:59');
$current_time = time();

echo $OUTPUT->header();
?>  
<h3 class="page_title"><?php echo get_string('training:commissioned', 'local_course_application'); ?></h3>

<div class="textbox">
    <?php echo get_string('commissioned:intro', 'local_course_application'); ?>
</div>

<div class="tableblock">
    <h5 class="div_title"><?php echo get_string('commissioned:plan', 'local_course_application'); ?></h5>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th class="w250px"><?php echo get_string('commissioned:comp', 'local_course_application'); ?></th>
                <th><?php echo get_string('courseapplication', 'local_course_application'); ?></th>
                <th><?php echo get_string('trainingperiod', 'local_course_application'); ?></th>
                <th><?php echo get_string('commissioned:status', 'local_course_application'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sql = "select * from {lmsdata_schedule} where classtype=2 and ((enrolmentstart <= :stime1 AND enrolmentend >= :stime2) OR (enrolmentstart >= :stime3 AND enrolmentstart <= :etime1)) and openyn = 0 order by learningstart, learningend asc";
            $contents = $DB->get_records_sql($sql, array('stime1'=>$firstyeardate, 'stime2'=>$firstyeardate, 'stime3'=>$firstyeardate, 'etime1'=>$lastyeardate));
            foreach ($contents as $content) {
                $red = '';
                $ared = '';
                if($content->enrolmentstart <= $current_time && $content->enrolmentend >= $current_time){
                    $red = ' class="red" ';
                    $ared = ' style="color:#d04d10;"';
                }
                ?>
                <tr <?php echo $red;?>>
                    <td><a href="<?php echo $CFG->wwwroot ?>/local/course_application/comsignment_info.php?id=<?php echo $content->id.'&menu_gubun='.$menu_gubun ?>" <?php echo $ared;?>><?php echo $content->consignmenttitle ?></a></td>
                    <td><?php echo date('Y-m-d', $content->enrolmentstart) ?> ~ <?php echo date('Y-m-d', $content->enrolmentend) ?></td>
                    <td><?php echo date('Y-m-d', $content->learningstart) ?> ~ <?php echo date('Y-m-d', $content->learningend) ?></td>
                    <td>
                        <?php
                        $start = date('Ymd',$content->enrolmentstart);
                        $end = date('Ymd',$content->enrolmentend);
                        if($end < date('Ymd')){
                        ?>
                            <input type="button" value="<?php echo get_string('end', 'local_course_application'); ?>" class="btn gray" />
                        <?php
                        } else if ($start > date('Ymd') || $end < date('Ymd')) {
                            ?>
                            <input type="button" value="<?php echo get_string('commissioned:wait', 'local_course_application'); ?>" class="btn gray" />

                            <?php
                        } else {
                            ?>
                            <a href="<?php echo $CFG->wwwroot ?>/local/course_application/comsignment_info.php?id=<?php echo $content->id.'&menu_gubun='.$menu_gubun ?>"><input type="button" value="<?php echo get_string('commissioned:accept', 'local_course_application'); ?>" class="btn orange" /></a>
                                <?php
                        }
                            ?>

                    </td>

                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>

</div>



<?php
echo $OUTPUT->footer();
?>


