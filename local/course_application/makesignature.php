<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('../../../INIpay5.3.9/libs/INIStdPayUtil.php');

$orderNumber = optional_param('orderNumber', 0, PARAM_RAW);
$price = optional_param('price', 0, PARAM_RAW);
$timestamp = optional_param('timestamp', 0, PARAM_RAW);
$formdata = optional_param('formdata', 0, PARAM_RAW);
$cookiename = optional_param('cookiename', 0, PARAM_RAW);

$SignatureUtil = new INIStdPayUtil();

$params = array(
    "oid" => $orderNumber,
    "price" => $price,
    "timestamp" => $timestamp
);
$sign = $SignatureUtil->makeSignature($params, "sha256");

$retrunval->sign = $sign;

setcookie($cookiename, $formdata, time()+600, '/');


@header('Content-type: application/json; charset=utf-8');
echo json_encode($retrunval);