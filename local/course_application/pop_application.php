<?php
/**
 * 중복 강좌 신청시 팝업 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$id = optional_param('id', 0, PARAM_INT);
$menu_gubun = optional_param("menu_gubun", 1, PARAM_INT); // 1:직무교육 3:자율교육 4:조합원교육
$classtype = optional_param('classtype', 0, PARAM_INT);
$classid = optional_param('classid', 0, PARAM_INT);

$current_time = time();

$course = $DB->get_record('lmsdata_course',array('id'=>$id));

$user = $DB->get_record('user', array('id'=>$USER->id));

$sql = 'SELECT lca.*, lc.classyear, lc.classnum, lc.classtype, lc.learningstart, lc.learningend
        FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.id = lca.courseid
        WHERE lca.userid=:userid AND (lca.paymentstatus = :complete OR lca.paymentstatus = :stand) 
        AND (lca.status != :delay OR lca.status != :cancel) AND lc.parentcourseid = :parentcourseid AND !(lc.learningend < :time AND lca.completionstatus=:completionstatus)';

$params = array('userid'=>$user->id, 'complete'=>'complete', 'stand'=>'stand', 'delay'=>'delay', 'cancel'=>'cancel', 'parentcourseid'=>$id, 'time'=>time(), 'completionstatus'=>0);

$applications = $DB->get_records_sql($sql,$params);

$lately_applications = $DB->get_records_sql($sql.'order by lca.id desc limit 1',$params);

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
?>  
<div id="popwrap">
    <h3 id="pop_title">
        수강신청 중복확인
        <img class="close r_content" src="/theme/oklassedu/pix/images/close.png" alt="" />
    </h3>
    <div id="pop_content">
        <h2>
            <?php echo $course->coursename;?>
        </h2>
        <div>
            <?php echo $user->lastname;?> 회원님께선 
            <?php
            if($lately_applications->status == 'stand'){
                echo '해당과정에 이미 수강신청한 내역이 있습니다.<br>';
                echo '해당 과정에 다른 교육로 변경하시려면 수강취소 후 다시 신청하시기 바랍니다.<br>';
            } else if($lately_applications->completionstatus == 0){
                echo '해당과정을 이수 중 이십니다.<br>';
            } else if($lately_applications->completionstatus == 1){
                echo '해당과정을 이미 이수완료 하였습니다.<br>';
            }
            ?>
            해당과정을 다시 들으시려면 수강신청하기 버튼을 클릭해 주세요.
        </div>
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th class="w250px">교육형태</th>
                    <th class="none">년도/기수</th>
                    <th class="none">교육기간</th>
                    <th class="none">결제상태</th>
                    <th>이수여부</th>
                    <th class="none">요청</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($applications as $application){
                ?>
                <tr>
                    <td><?php if($application->classtype == 1) echo '직무교육'; else if($application->classtype == 2) echo '위탁교육'; else if($application->classtype == 3) echo '자율교육'; else if($application->classtype == 4) echo '조합원교육';?></td>
                    <td><?php echo $application->classyear.'년 / '.$application->classnum.'기';?></td>
                    <td><?php echo date('Y-m-d', $application->learningstart).'~'.date('Y-m-d', $application->learningend);?></td>
                    <td><?php if($application->paymentstatus == 'complete') echo '완료'; else echo '대기';?></td>
                    <td><?php if($application->completionstatus == 1)echo '이수'; else echo '미이수'?></td>
                    <td>
                        <?php if($application->completionstatus == 0 && $application->learningend > time()){ ?>
                        <a href="<?php echo $CFG->wwwroot;?>/local/mypage/payment.php">
                            <input type="button" class="btn brd orange" value="수강취소">
                        </a>
                        <?php } else { echo '-';}?>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div class="table_btn">
            <?php
            if($classid != 0 && $classtype == 2){
                $courseid = $classid;
            } else {
                $courseid = $course->id;
            }
            ?>
            <input type="button" value="수강신청하기" class="btn orange big" onclick="location.href='/local/course_application/application.php?courseid=<?php echo $courseid;?>&menu_gubun=<?php echo $menu_gubun;?>'" />
            <input type="button" value="과정안내 바로가기" class="btn gray big" onclick="closePop()" />
        </div>
    </div>
</div>