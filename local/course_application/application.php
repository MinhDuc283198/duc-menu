<?php
/**
 * 수강신청 정보입력 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
//require_once('../../../INIpay5.3.9/libs/INIStdPayUtil.php');

$context = context_system::instance();

if (isguestuser() || $USER->id == 0) {
    echo '<script type="text/javascript">alert("로그인 후 이용 가능합니다."); '
    . 'location.href="' . $CFG->wwwroot . '"</script>';
}

require_login();

$PAGE->set_context($context);

$PAGE->set_url(new moodle_url('/local/course_application/application.php'));
$PAGE->set_pagelayout('edu');
$PAGE->add_body_class('path-local-course_application');

$PAGE->navbar->add(get_string('courseguide', 'local_course_application'));
$PAGE->navbar->add(get_string('training:job', 'local_course_application'));
$PAGE->navbar->add(get_string('application', 'local_course_application'));
$PAGE->set_title(get_string('training:job', 'local_course_application'));
$PAGE->set_heading(get_string('training:job', 'local_course_application'));

$classid = optional_param('classid', 0, PARAM_INT);
$menu_gubun = optional_param('menu_gubun', 0, PARAM_INT);

$c_year = date('Y');
$c_month = date('n');
$c_day = date('j');

$current_lang = current_language();
$current_time = time();

//현재 신청할 강의정보
$classcontent = $DB->get_record('lmsdata_class', array('id' => $classid));
$sql_course = 'select lco.*, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber '
        . 'from {lmsdata_course} lco '
        . 'join {course_categories} ca on ca.id = lco.detailprocess  '
        . 'join {course_categories} ca2 on ca.parent = ca2.id '
        . 'where lco.id = :id';

$course = $DB->get_record('lmsdata_course', array('id' => $classcontent->parentcourseid));

//현재 유저정보
$sql = 'SELECT mu.lastname, lu.*, mu.phone1 muphone1, mu.phone2 muphone2  
        FROM {user} mu
        JOIN {lmsdata_user} lu ON lu.userid = mu.id
        WHERE mu.id = :userid';

$userinfo = $DB->get_record_sql($sql, array('userid' => $USER->id));
$usertype = ($userinfo->usertypecode / 10);
if (empty($course)) {
    redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "없는 과정입니다.");
}
if ($course->ca1idnumber != 'trust_course' && $usertype == 3) {
    redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "교육생은 신임과정만 학습할 수 있습니다.");
} else if ($course->ca2idnumber == 'required_course') {
    redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "수강대상이 아닙니다.");
} else {
    if ($usertype != $course->classobject && $course->classobject < 4) {
        redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "수강대상이 아닙니다.");
    }
    if ($course->classobject == 4 && $usertype == 3) {
        redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "수강대상이 아닙니다.");
    }
    if ($course->classobject == 5 && $usertype == 1) {
        redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "수강대상이 아닙니다.");
    }
    if ($course->classobject == 6 && $usertype == 2) {
        redirect($CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun, "수강대상이 아닙니다.");
    }
}
echo $OUTPUT->header();
?>
<h3 class="page_title"><?php echo get_string('courseapplication', 'local_course_application'); ?></h3>
<?php
if ($menu_gubun == 1) {
    $gubun = get_string('training:courseno1', 'local_course_application');
} else if ($menu_gubun == 5) {
    $gubun = get_string('training:courseno5', 'local_course_application');
} else if ($menu_gubun == 6) {
    $gubun = get_string('training:courseno6', 'local_course_application');
}
?>
<span class="training_type"><?php echo $gubun; ?></span><strong class="sub_title orange margin_l10"><?php echo $course->coursename; ?></strong>

<h5 class="div_title top"><?php echo get_string('courseinfo', 'local_course_application'); ?></h5>
<form id="application" name="application" method="post" action="application_submit.php">
    <input type="hidden" name="menu_gubun" value="<?php echo $menu_gubun; ?>">
    <input type="hidden" name="pay_price" value="<?php echo $price; ?>">

    <!-- 현재 수강신청 기간인 차수 -->
    <?php
//현재 신청할 강의정보
    $num = 1;
    $where = ' lc.id = :classid ';
    $sql = 'SELECT lc.*, lco.coursename, lco.price, lco.memberdiscount, lco.groupdiscount, lco.week, lco.hours, lc.enrolmentstart, lc.enrolmentend, lco.classobject '
            . 'FROM {lmsdata_course} lco '
            . 'JOIN {lmsdata_class} lc ON lc.parentcourseid = lco.id '
            . 'WHERE ' . $where
            . 'order by lc.classnum asc ';
    $classes = $DB->get_records_sql($sql, array('classid' => $classid, 'enrolmentstart' => $current_time, 'enrolmentend' => $current_time));
    ?>
    <table class="table table-condensed">
        <colgroup>
            <col width="30px" />  
            <col width="/"> 
            <col width="/"> 
            <col width="/"> 
        </colgroup>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><?php echo get_string('coursename', 'local_course_application'); ?></th>
                <th>년도/기수</th>
                <th><?php echo ($menu_gubun == 6) ? '응시일' : get_string('trainingperiod', 'local_course_application'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $num = 1;
            foreach ($classes as $class) {
                $period = $class->classyear . '년도 ' . $class->classnum . '기';
                $checked = ($classid == $class->id || $num == 1) ? 'checked' : '';
                ?>
                <tr>
                    <td><input type="radio" name="classid" value="<?php echo $class->id; ?>" <?php echo $checked; ?>/></td>
                    <td><?php echo $class->coursename; ?><br/>
                        (<?php echo $gubun; ?><?php
                        if ($current_lang == 'ko') {
                            echo $class->codename;
                        } else {
                            $class->english_codename;
                        }
                        ?>)
                    </td>
                    <td><strong class="orange"><?php echo $period; ?></strong></td>
                    <td><?php
                        if ($menu_gubun == 6) {
                            echo date('Y.m.d ', $class->learningstart) . '(' . date('H:i', $class->learningstart) . ' ~ ' . date('H:i', $class->learningend) . ')';
                        } else {
                            echo date('Y.m.d', $class->learningstart) . ' ~ ' . date('Y.m.d', $class->learningend);
                        }
                        ?>
                        <?php
//                        $dates = new DateTime(($class->learningend - $class->learningstart));
//                        echo '(';
//                        echo (empty($class->week)?  (($class->learningend - $class->learningstart)/(60*24*7)) : '') . get_string('week', 'local_course_application').' / '; 
                        // echo (empty($class->hours)?  (($class->learningend - $class->learningstart)/(60)) : '') . get_string('hours', 'local_course_application'); 
//                        echo (empty($class->week)?  '' : $class->week) . get_string('week', 'local_course_application').' / ';
//                        echo (empty($class->hours)?  '' : $class->hours) . get_string('hours', 'local_course_application'); 
//                        echo ')';
                        ?>
                    </td>
                </tr>
                <?php
                $num++;
            }
            ?>
        </tbody>
    </table>

    <div class="table_btn">
        <input type="submit" value="<?php echo get_string('courseapplication', 'local_course_application'); ?>" class="btn pink big"/>
    </div>
</form>

<script>
    /**
     * 강의 중복수강 체크
     * @returns {undefined}     */
    function application_chk() {
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/local/course_application/application_overlap_chk.php" ?>',
            method: 'POST',
            data: {
                classid: $('input[name=classid]:checked').val(),
                userid: '<?php echo $USER->id; ?>'
            },
            success: function (data) {
                if (data == 'true') {
                    applicationchk = true;
                } else {
                    applicationchk = false;
                }
            }
        });
    }

    /**
     * 수강 강좌를 선택할 때마다 해당 강좌 중복신청 체크
     
     * @type type     */
    $('input[name=classid]').change(function () {
        application_chk();
    });

    /**
     * 수강신청 폼체크 및 결제 진행
     * @type type     */
    $('#application').submit(function () {
        var returnvalue = true;

        if (applicationchk == false) {
            alert('이미 수강신청 하신 차수입니다.');
            return false;
        }

        if (returnvalue == false) {
            return returnvalue;
        }
    });

    /**
     * form data json방식으로 변환
     
     * @returns {unresolved}     */
    jQuery.fn.serializeObject = function () {
        var obj = null;
        try {
            if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
                var arr = this.serializeArray();
                if (arr) {
                    obj = {};
                    jQuery.each(arr, function () {
                        if (!obj[this.name]) {
                            obj[this.name] = this.value;
                        } else {
                            obj[this.name] += '/' + this.value;
                        }
                    });
                }//if ( arr ) {
            }
        } catch (e) {
            alert(e.message);
        } finally {
        }
        return obj;
    };
</script>
<?php echo $OUTPUT->footer(); ?>