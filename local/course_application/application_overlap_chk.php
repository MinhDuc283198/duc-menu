<?php
/**
 * 강의 중복 신청 체크
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$classid = optional_param("classid", 0, PARAM_INT);
$userid = optional_param("userid", 0, PARAM_INT);

$sql = 'SELECT lca.* 
        FROM {lmsdata_class} lc
        JOIN {lmsdata_course_applications} lca ON lca.courseid = lc.courseid
        WHERE lca.userid = :userid AND lca.paymentstatus != :cancel AND lca.status != :cancel2 AND lc.id = :classid';

$application = $DB->get_record_sql($sql,array('userid'=>$userid, 'cancel'=>'cancel', 'cancel2'=>'cancel','classid'=>$classid));

if(empty($application)){
    echo 'true';
} else {
    echo 'false';
}