<?php
/**
 * 필수과정, 선택과정, 자격인증 과정 안내 및 신청
 * -> $menu_gubun변수를 활용하여 과정 구분을 해줌
 * -> 과정에 있는 매 강좌별 상세설명은 다른 페이지     
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

require_login();

$PAGE->set_context($context);

$menu_gubun = optional_param("menu_gubun", 1, PARAM_INT); // 1:필수과정, 5:선택과정, 6:자격인증
$perpage = optional_param("perpage", 10, PARAM_INT);
$currpage = optional_param("currpage", 1, PARAM_INT);
$re = optional_param("re", 0, PARAM_INT);

if ($re == 1) {
    echo '<script type="text/javascript">
                        alert("수강할 수 없는 강의입니다.");
                        location.href="/local/course_application/course_list.php?menu_gubun=' . $menu_gubun . '";
                    </script>';
} else if ($re == 2) {
    echo '<script type="text/javascript">
                        alert("교육생은 신임과정만 신청할 수 있습니다.");
                        location.href="/local/course_application/course_list.php?menu_gubun=' . $menu_gubun . '";
                    </script>';
} else if ($re == 3) {
    echo '<script type="text/javascript">
                        alert("수강신청 처리가 되지 않은 강의입니다.");
                        location.href="/local/course_application/course_list.php?menu_gubun=' . $menu_gubun . '";
                    </script>';
}

//현재 유저정보
$sql = 'SELECT mu.lastname, lu.*, mu.phone1 muphone1, mu.phone2 muphone2  
        FROM {user} mu
        JOIN {lmsdata_user} lu ON lu.userid = mu.id
        WHERE mu.id = :userid';

$userinfo = $DB->get_record_sql($sql, array('userid' => $USER->id));
$usertype = ($userinfo->usertypecode / 10);

if ($menu_gubun != 1 && $usertype == 3) {
    echo '<script type="text/javascript">
                        alert("교육생은 접근할 수 없습니다.");
                        location.href="/local/course_application/course_list.php?menu_gubun=1";
                    </script>';
}

$PAGE->set_url(new moodle_url('/local/course_application/course_list.php', array('menu_gubun' => $menu_gubun)));
$PAGE->set_pagelayout('edu');
$PAGE->add_body_class('path-local-course_application');


$current_year = date('Y');
$current_time = time();
$params;

$sql_where = '';

if ($menu_gubun == 1) {
    $sql_where = ' where ca2.name =:ca2name and lca.userid =:lcauserid';
    $params['ca2name'] = '필수과정';
    $params['lcauserid'] = $USER->id;
} else if ($menu_gubun == 5) {
    $sql_where = ' where ca.name =:ca1name';
    $params['ca1name'] = '선택과정';
    $sql_where .= ' and lc.enrolmentstart < ' . time() . ' and lc.learningend > ' . time();
} else if ($menu_gubun == 6) {
    $sql_where = ' where ca.name =:ca1name';
    $params['ca1name'] = '자격인증';
    $sql_where .= ' and lc.enrolmentstart < ' . time() . ' and lc.learningend > ' . time();
}

if (!is_siteadmin()) {
    $sql_objectall = ' or lco.classobject = :classobject4';
    $params['classobject4'] = 7;
    if ($usertype == 1) {
        $sql_where .= ' and (lco.classobject = :classobject1 or lco.classobject = :classobject2 or lco.classobject = :classobject3 ' . $sql_objectall . ')';
        $params['classobject1'] = 1;
        $params['classobject2'] = 4;
        $params['classobject3'] = 6;
    } else if ($usertype == 2) {
        $sql_where .= ' and (lco.classobject = :classobject1 or lco.classobject = :classobject2 or lco.classobject = :classobject3 ' . $sql_objectall . ')';
        $params['classobject1'] = 2;
        $params['classobject2'] = 4;
        $params['classobject3'] = 5;
    } else {
        $sql_where .= ' and (lco.classobject = :classobject1 or lco.classobject = :classobject2 or lco.classobject = :classobject3 ' . $sql_objectall . ')';
        $params['classobject1'] = 3;
        $params['classobject2'] = 5;
        $params['classobject3'] = 6;
    }
}
//$sql_where .= 'and ';

$orderby = ' order by id desc ';

$sql_select = "SELECT lc.*, lco.objectives, ca.name ca1name, ca2.name ca2name, lco.classobject, "
        . "mc.fullname, lco.completestandard, lc.enrolmentstart, lc.enrolmentend, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, "
        . "(select count(*) from {lmsdata_course_applications} where courseid = lc.courseid and status = 'apply' ) as student, "
        . "(select count(*) from {lmsdata_course_applications} where courseid = lc.courseid and status = 'apply' and completionstatus = 1) as complete ";

$sql_from = " FROM {course} mc
                 JOIN {lmsdata_class} lc ON lc.courseid = mc.id
                 LEFT JOIN {lmsdata_course_applications} lca ON lc.courseid = lca.courseid
                 JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
                 JOIN {course_categories} ca ON ca.id = mc.category 
                 LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id";

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $params);
$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);

$status = $DB->get_records('lmsdata_code', array('code_category' => 'status', 'depth' => 2), 'id', 'id, codename, code');

echo $OUTPUT->header();
?>  
<div class="textbox">
    <?php
    if ($menu_gubun == 1) {
        echo get_string('job:courseno1', 'local_course_application');
    } else if ($menu_gubun == 5) {
        echo get_string('job:courseno5', 'local_course_application');
    } else if ($menu_gubun == 6) {
        echo get_string('job:courseno6', 'local_course_application');
    }
    ?>
</div>

<?php
if ($menu_gubun == 1) {
    ?>
    <h5 class="div_title">신임과정</h5>
    <table class="table scroll">
        <colgroup>
            <col width="60%" />
            <col width="30%"/>
            <col width="10%" />
        </colgroup>
        <thead>
            <tr>
                <th>과정명</th>
                <th>학습기간</th>
                <th>취득포인트</th>

            </tr>
        </thead>
        <tbody>
            <?php
            if ($courses) {
                $indexvalue = 0;
                foreach ($courses as $course) {
                    if ($course->ca1idnumber == 'trust_course') {
                        $check_sql = 'SELECT ok.course, lc.id, SUM(ok.point) lecturepoint '
                                . 'FROM {okmedia} ok '
                                . 'JOIN {lmsdata_class} lc on lc.courseid = ok.course '
                                . 'WHERE lc.id = :lcid '
                                . 'GROUP BY ok.course';
                        $check_content = $DB->get_record_sql($check_sql, array('lcid' => $course->id));
                        ?>
                        <tr>
                            <td class="title" onclick="window.location.href = '<?php echo $CFG->wwwroot . '/local/course_application/course_list_detail.php?menu_gubun=1&classid=' . $course->id; ?>'" style="text-align:center"><a><?php echo $course->fullname; ?></a></td>
                            <td><?php echo ($course->learningstart && $course->learningend) ? date('Y.m.d', $course->learningstart) . ' ~ ' . date('Y.m.d', $course->learningend) : '-' ?></td>
                            <td><?php echo empty($check_content->lecturepoint) ? '0 P ' : $check_content->lecturepoint . ' P'; ?></td>
                        </tr>
                        <?php
                        $indexvalue++;
                    }
                }
                if ($indexvalue == 0) {
                    echo '<tr><td colspan="3">현재 수강할 수 있는 과정이 없습니다..</td></tr>';
                }
            } else {
                echo '<tr><td colspan="3">현재 수강할 수 있는 과정이 없습니다.</td></tr>';
            }
            ?>
        </tbody>
    </table>
    <?php
    if ($usertype != 3) {
        ?>
        <h5 class="div_title">연차별과정</h5>
        <table class="table scroll">
            <colgroup>
                <col width="60%" />
                <col width="30%"/>
                <col width="10%" />
            </colgroup>
            <thead>
                <tr>
                    <th>과정명</th>
                    <th>학습기간</th>
                    <th>취득포인트</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($courses) {
                    $indexvalue = 0;
                    foreach ($courses as $course) {
                        if ($course->ca1idnumber == 'year_course') {
                            $check_sql = 'SELECT ok.course, lc.id, SUM(ok.point) lecturepoint '
                                    . 'FROM {okmedia} ok '
                                    . 'JOIN {lmsdata_class} lc on lc.courseid = ok.course '
                                    . 'WHERE lc.id = :lcid '
                                    . 'GROUP BY ok.course';
                            $check_content = $DB->get_record_sql($check_sql, array('lcid' => $course->id));
                            ?>
                            <tr>
                                <td class="title" onclick="window.location.href = '<?php echo $CFG->wwwroot . '/local/course_application/course_list_detail.php?menu_gubun=1&classid=' . $course->id; ?>'" style="text-align:center"><a><?php echo $course->fullname; ?></a></td>
                                <td><?php echo ($course->learningstart && $course->learningend) ? date('Y.m.d', $course->learningstart) . ' ~ ' . date('Y.m.d', $course->learningend) : '-' ?></td>
                                <td><?php echo empty($check_content->lecturepoint) ? '0 P ' : $check_content->lecturepoint . ' P'; ?></td>
                            </tr>
                            <?php
                            $indexvalue++;
                        }
                    }
                    if ($indexvalue == 0) {
                        echo '<tr><td colspan="3">현재 수강할 수 있는 과정이 없습니다.</td></tr>';
                    }
                } else {
                    echo '<tr><td colspan="3">현재 수강할 수 있는 과정이 없습니다.</td></tr>';
                }
                ?>
            </tbody>
        </table>
        <?php
    }
} else if ($menu_gubun == 5) {
    ?>
    <h5 class="div_title">선택과정</h5>
    <table class="table scroll">
        <colgroup>
            <col width="/" />
            <col width="/"/>
            <col width="/"/>
            <col width="10%" />
        </colgroup>
        <thead>
            <tr>
                <th>과정명</th>
                <th>신청기간</th>
                <th>학습기간</th>
                <th>수강신청</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!$courses) {
                echo '<tr><td colspan="4">아직 신청할 수 있는 과정이 없습니다.</td></tr>';
            } else {
                $indexvalue = 0;
                foreach ($courses as $course) {
                    if ($course->learningend + (7 * 86400) < time()) {
                        
                    } else {
                        $sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid AND paymentstatus != :paymentstatus';
                        $application = $DB->get_records_sql($sql, array('userid' => $USER->id, 'courseid' => $course->courseid, 'paymentstatus' => 'cancel'));
                        ?>
                        <tr>
                            <td onclick="window.location.href = '<?php echo $CFG->wwwroot . '/local/course_application/course_list_detail.php?menu_gubun=5&classid=' . $course->id; ?>'" class="title" style="text-align:center"><a><?php echo $course->fullname; ?></a></td>
                            <td><?php echo ($course->enrolmentstart && $course->enrolmentend) ? date('Y.m.d', $course->enrolmentstart) . ' ~ ' . date('Y.m.d', $course->enrolmentend) : '-' ?></td>
                            <td><?php echo ($course->learningstart && $course->learningend) ? date('Y.m.d', $course->learningstart) . ' ~ ' . date('Y.m.d', $course->learningend) : '-' ?></td>
                            <td>
                                <?php
                                if ($application) {
                                    ?>
                                    <input type="button" class="btn black" value="신청취소" onclick="call_layerPop('/local/mypage/pop_payment_cancel.php', '400px', 'auto', {'id': '<?php echo $course->id; ?>', 'menu_gubun': '<?php echo $menu_gubun; ?>'})" />
                                    <?php
                                } else {
                                    if ((empty($course->enrolmentend) && empty($course->enrolmentstart)) || $course->enrolmentend > time() && $course->enrolmentstart < time()) {
                                        if ($usertype != $course->classobject && $course->classobject < 4) {
                                            ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                                        } else if ($course->classobject == 4 && $usertype == 3) {
                                            ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                                            } else if ($course->classobject == 5 && $usertype == 1) {
                                                ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                                            } else if ($course->classobject == 6 && $usertype == 2) {
                                                ?><a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                                            } else if ($course->ca1idnumber != 'trust_course' && $usertype == 3) {
                                                ?><a href="#none" onclick="alert('교육생은 신청할 수 없습니다.')"><input type="button" class="btn pink" value="신청" /></a><?php
                                            } else {
                                                ?>
                                            <a href="<?php echo $CFG->wwwroot . '/local/course_application/application.php?menu_gubun=' . $menu_gubun . '&classid=' . $course->id; ?>"><input type="button" class="btn pink" value="신청" /></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="button" class="btn gray" value="신청" onclick="alert('신청기간이 지났습니다.')">
                                        <?php
                                    }
                                }
                                $indexvalue++;
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                if ($indexvalue == 0) {
                    echo '<tr><td colspan="4">아직 신청할 수 있는 과정이 없습니다.</td></tr>';
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else if ($menu_gubun == 6) {
    ?>
    <h5 class="div_title">자격인증</h5>
    <table class="table scroll">
        <colgroup>
            <col width="/" />
            <col width="/" />
            <col width="/" />
            <col width="/" />
            <col width="10%" />
        </colgroup>
        <thead>
            <tr>
                <th>시험명</th>
                <th>신청기간</th>
                <th>응시일</th>
                <th>이수기준</th>
                <th>응시신청</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!$courses) {
                echo '<tr><td colspan="5">아직 신청할 수 있는 과정이 없습니다.</td></tr>';
            } else {
                $indexvalue = 0;
                foreach ($courses as $course) {
                    if ($course->learningend + (7 * 86400) < time()) {
                        
                    } else {
                        $sql = 'SELECT * FROM {lmsdata_course_applications} WHERE userid = :userid AND courseid = :courseid AND paymentstatus != :paymentstatus';
                        $application = $DB->get_record_sql($sql, array('userid' => $USER->id, 'courseid' => $course->courseid, 'paymentstatus' => 'cancel'));
                        ?>
                        <tr>
                            <td class="title" onclick="window.location.href = '<?php echo $CFG->wwwroot . '/local/course_application/course_list_detail.php?menu_gubun=6&classid=' . $course->id; ?>'" style="text-align:center"><a><?php echo $course->fullname; ?></a></td>
                            <td><?php echo ($course->enrolmentstart && $course->enrolmentend) ? date('Y.m.d', $course->enrolmentstart) . ' ~ ' . date('Y.m.d', $course->enrolmentend) : '-' ?></td>
                            <td>
                                <?php
//                                echo ($course->learningstart && $course->learningend) ? date('Y.m.d(H:i) ', $course->learningstart) . ' ~ ' . date('Y.m.d(H:i) ', $course->learningend) : '';
                                echo ($course->learningstart && $course->learningend) ? date('Y.m.d ', $course->learningstart) . '(' . date('H:i', $course->learningstart) . ' ~ ' . date('H:i', $course->learningend) . ')' : '';
                                ?> 
                            </td>
                            <td>
                                <?php
                                $contentstandard = preg_replace("(\<(/?[^\>]+)\>)", "", $course->completestandard);
                                echo ($contentstandard) ? $contentstandard : '없음';
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($application) {
                                    ?>
                                    <input type="button" class="btn black" value="신청취소" onclick="call_layerPop('/local/mypage/pop_payment_cancel.php', '300px', 'auto', {'id': '<?php echo $course->id; ?>', 'menu_gubun': '<?php echo $menu_gubun; ?>'})" />
                                    <?php
                                } else {
                                    if ((empty($course->enrolmentend) && empty($course->enrolmentstart)) || $course->enrolmentend > time() && $course->enrolmentstart < time()) {
                                        if ($usertype != $course->classobject && $course->classobject < 4) {
                                            ?>
                                            <a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a>
                                            <?php
                                        } else if ($course->ca1idnumber != 'trust_course' && $usertype == 3) {
                                            ?>
                                            <a href="#none" onclick="alert('교육생은 신청할 수 없습니다.')"><input type="button" class="btn pink" value="신청" /></a>
                                            <?php
                                        } else if ($usertype == 3 && $course->classobject == 4) {
                                            ?>
                                            <a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a>
                                            <?php
                                        } else if ($usertype == 1 && $course->classobject == 5) {
                                            ?>
                                            <a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a>
                                            <?php
                                        } else if ($usertype == 2 && $course->classobject == 6) {
                                            ?>
                                            <a href="#none" onclick="alert('수강대상이 아닙니다.')"><input type="button" class="btn pink" value="신청" /></a>
                                        <?php } else {
                                            ?>
                                            <a href="<?php echo $CFG->wwwroot . '/local/course_application/application.php?menu_gubun=' . $menu_gubun . '&classid=' . $course->id; ?>"><input type="button" class="btn pink" value="신청" /></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="button" class="btn gray" value="신청" onclick="alert('신청기간이 지났습니다.')">
                                        <?php
                                    }
                                }
                                $indexvalue++;
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                if ($indexvalue == 0) {
                    echo '<tr><td colspan="5">아직 신청할 수 있는 시험이 없습니다.</td></tr>';
                }
            }
            ?>
        </tbody>
    </table>
    <?php
}
echo $OUTPUT->footer();
?>