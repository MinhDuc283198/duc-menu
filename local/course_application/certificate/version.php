<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017072500;
$plugin->requires  = 2012061700;       
$plugin->component = 'local_certificate'; // Full name of the plugin (used for diagnostics)