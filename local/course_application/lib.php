<?php
/**
 * 학습후기 게시판 id 불러오기 함수
 * @global moodle_database $DB
 * @return stdClass
 */
function get_review_boardid(){
    global $DB;
    $sql = "select id from {jinoboard} where name='학습후기'";
    
    return $DB->get_field_sql($sql);
} 

/**
 * 학습후기 사용자 이름 불러오기 함수
 * @global moodle_database $DB
 * @param int $userid
 * @return stdClass
 */
function get_review_username($userid){
    global $DB;
    $sql = "select eng_name from {lmsdata_user} where userid=$userid ";
    return $DB->get_field_sql($sql);
}

//학습후기 게시글 수 카운트
/**
 * 
 * @global moodle_database $DB
 * @param int $boardid
 * @return stdClass
 */
function get_course_reviews_count($boardid){
    global $DB;    
    $params['board'] = $boardid;
    $sql = " board = :board ";
    return $DB->count_records_select("jinoboard_contents",$sql,$params);
}

/**
 * 과정의 학습후기 가져오기 함수
 * @global moodle_database $DB
 * @param int $page
 * @param int $perpage
 * @param int $boardid
 * @param int $id
 * @return stdClass
 */
function get_course_reviews($page,$perpage,$boardid,$id){
    global $DB;
    
    $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    $params = array();
    $params['boardid']=$boardid;
    $params['id']=$id;
    
    $sql="select * from {jinoboard_contents} where board=:boardid and id=:id";
    
    return $DB->get_records_sql($sql,$params,$offset,$perpage);
}

/**
 * 최대페이지 수 계산함수
 * @param int $rows
 * @param int $limit
 * @return int
 */
function course_application_get_total_pages($rows, $limit = 10) {
	if ($rows == 0) {
		return 1;
	}

	$total_pages = (int) ($rows / $limit);

	if (($rows % $limit) > 0) {
		$total_pages += 1;
	}

	return $total_pages;
}

/**
 * 페이지화 함수
 * @param char $url
 * @param char $params
 * @param int $total_pages
 * @param int $current_page
 * @param int $max_nav
 */
function course_application_get_paging_bar($url, $params, $total_pages, $current_page, $max_nav = 10) {
	$total_nav_pages = course_application_get_total_pages($total_pages, $max_nav);
	$current_nav_page = (int) ($current_page / $max_nav);
	if (($current_page % $max_nav) > 0) {
		$current_nav_page += 1;
	}
	$page_start = ($current_nav_page - 1) * $max_nav + 1;
	$page_end = $current_nav_page * $max_nav;
	if ($page_end > $total_pages) {
		$page_end = $total_pages;
	}
        
	if (!empty($params)) {
		$tmp = array();
		foreach ($params as $key => $value) {
			$tmp[] = $key . '=' . $value;
		}
		$tmp[] = "page=";
		$url = $url . "?" . implode('&', $tmp);
	} else {
		$url = $url . "?page=";
	}
	echo html_writer::start_tag('div', array('class' => 'board-breadcrumbs'));
	if ($current_nav_page > 1) {
            echo '<span class="board-nav-prev"><a class="prev" href="'.$url.(($current_nav_page - 2) * $max_nav + 1).'"><</a></span>';
	} else {
            echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
	}
	if ($current_page > 1) {
		echo '<span class="board-nav-prev"><a class="prev" href="'.$url.($current_page - 1).'"><</a></span>';
	} else {
		echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
	}
        echo '<ul>';
	for ($i = $page_start; $i <= $page_end; $i++) {
		if ($i == $current_page) {
			echo '<li class="current"><a href="#">'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.$url.''.$i.'">'.$i.'</a></li>';
		}
	}
        echo '</ul>';
	if ($current_page < $total_pages) {
		echo '<span class="board-nav-next"><a class="next" href="'.$url.($current_page + 1).'">></a></span>';
	} else {
		echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
	}
	if ($current_nav_page < $total_nav_pages) {
		echo '<a class="next_" href="' . $url . ($current_nav_page * $max_nav + 1) . '"></a>';
	} else {
		echo '<a class="next_" href="#"></a>';
	}
	echo html_writer::end_tag('div');
}


/**
 * 
 * @global moodle_database $DB
 * @global type $USER
 * @global type $CFG
 * @param type $courses
 * @param type $userid
 * @param type $page
 * @param type $perpage
 * @return type
 */
//function course_application_get_contents($courses, $userid = 0, $page=1, $perpage = 10){
//    global $DB, $USER, $CFG;
//    
//    if(empty($courses)) {
//        return array();
//    }
//    
//    require_once($CFG->dirroot.'/mod/okmedia/lib.php');
//    
//    list($sqlIn, $params) = $DB->get_in_or_equal(array_keys($courses), SQL_PARAMS_NAMED, 'co');
//    
//    if($userid === 0) {
//        $userid = $USER->id;
//    }
//    
//    $sql = 'SELECT cm.*, ok.name, ok.timestart, ok.timeend, ot.timeview, '
//            . 'lcf.duration ,lcf.filepath, lcf.filename, lc.id, lco.coursename, '
//            . 'lc.classtype, lc.occasional, lco.coursearea, lco.coursetype, lco.detailprocess, lco.roughprocess '
//            . 'FROM {okmedia} ok '
//            . 'JOIN {course_modules} cm ON cm.instance = ok.id '
//            . 'JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname '
//            . 'JOIN {okmedia_track} ot ON ot.okmediaid = ok.id '
//            . 'JOIN {lcms_contents_file} lcf ON lcf.con_seq = ok.contents '
//            . 'JOIN {lmsdata_class} lc on lc.courseid = cm.course '
//            . 'JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid ';
//    
//    $where[] = ' ok.course '.$sqlIn;
//    $where[] = ' ot.userid = :userid ';
//    $params['userid'] = $userid; 
//    $params['modname'] = 'okmedia'; 
//    
//    $where = ' WHERE '.implode(' AND ', $where);
//    $orderBy = ' ORDER BY ot.timeview DESC ';
//    
//    $offset = ($page-1) * $perpage;
//        
//    $contents = $DB->get_records_sql($sql.$where.$orderBy, $params, $offset, $perpage);
//    
//    foreach($contents AS $content) {
//        $content->info = okmedia_get_coursemodule_info($content);
//        $content->duration = okmedia_time_from_seconds($content->duration);
//    }
//    
//    return $contents;
//}