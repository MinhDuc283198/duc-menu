<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018110801;
$plugin->requires  = 2012061700;       
$plugin->component = 'local_course_application'; // Full name of the plugin (used for diagnostics)