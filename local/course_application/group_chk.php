<?php
/**
 * 그룹코드 사용가능여부 체크
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$courseid = optional_param('courseid', 0, PARAM_INT);
$groupcode = optional_param('groupcode', '', PARAM_RAW);

$class = $DB->get_record('lmsdata_class', array('id'=>$courseid));

$group = $DB->get_record('lmsdata_grouplist', array('groupcode'=>$groupcode, 'scheduleid'=>$class->scheduleid, 'status'=>0));

if(empty($group)){
    $retrunval->alert = get_string('emptygroup','local_course_application');
    $retrunval->status = 'fasle';
} else {
    $retrunval->alert = get_string('ok', 'local_course_application');
    $retrunval->status = 'true';
    $retrunval->paymentmethod = $group->paymentmethod;
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($retrunval);