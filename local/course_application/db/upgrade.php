<?php
function xmldb_local_course_application_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    
    $dbman = $DB->get_manager(); 
    
    if($oldversion < 2017071800) {
        $table = new xmldb_table('lmsdata_course_applications');
        $field = new xmldb_field('paymentstatus', XMLDB_TYPE_CHAR, '20', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('status', XMLDB_TYPE_CHAR, '20', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017072200) {
        $table = new xmldb_table('lmsdata_course_applications');
        $field = new xmldb_field('paymentid', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('ordernumber', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('account', XMLDB_TYPE_CHAR, '30', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('bankcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('accountname', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('cshrresult', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('cshrtype', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('cardcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('cardnumber', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('installment', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('pointuse', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('allowcancel', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('completionstatus', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017072400) {
        $table = new xmldb_table('lmsdata_course_applications');
        $field = new xmldb_field('email', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017072500) {
        $table = new xmldb_table('lmsdata_applications_cancel');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('status', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
    if($oldversion < 2017072501) {
        $table = new xmldb_table('lmsdata_applications_cancel');
        $field = new xmldb_field('applicationid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        
        $field = new xmldb_field('refund', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }

        $field = new xmldb_field('reason', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017072502) {
        $table = new xmldb_table('lmsdata_course_applications');
        $field = new xmldb_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017072800) {
        $table = new xmldb_table('inicode');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('code', XMLDB_TYPE_CHAR, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('name', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
    if($oldversion < 2017073100) {
        $table = new xmldb_table('lmsdata_applications_cancel');
        $field = new xmldb_field('bank', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        
        $field = new xmldb_field('account', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }

        $field = new xmldb_field('accountname', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017073101) {
        $table = new xmldb_table('lmsdata_applications_cancel');
        $field = new xmldb_field('title', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017080700) {
        $table = new xmldb_table('lmsdata_course_applications');
        $field = new xmldb_field('schoolcode', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017081000) {
        $table = new xmldb_table('lmsdata_receipt');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('identitynum', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tradetype', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('taxationtype', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('supplycost', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tax', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('servicefee', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tradeusage', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('customername', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('ordernumber', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('hp', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
    if($oldversion < 2017081001) {
        $table = new xmldb_table('lmsdata_receipt');
        $field = new xmldb_field('application', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('itemname', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017081002) {
        $table = new xmldb_table('lmsdata_receipt');
        $field = new xmldb_field('identitynum', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017082200) {
        $table = new xmldb_table('lmsdata_applications_cancel');        
        $field = new xmldb_field('refund', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017090500) {
        $table = new xmldb_table('lmsdata_receipt');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('identitynum', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('tradetype', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('taxationtype', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('supplycost', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tax', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('servicefee', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tradeusage', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('customername', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('ordernumber', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('hp', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('application', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('itemname', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if($oldversion < 2017112800) {
        $table = new xmldb_table('lmsdata_course_applications');        
        $field = new xmldb_field('adminchange', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2018110800) {
        $table = new xmldb_table('lmsdata_course_applications');        
        $field = new xmldb_field('classlevel', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('classgroup', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2018110801) {
        $table = new xmldb_table('lmsdata_course_applications');        
        $field = new xmldb_field('schooltype', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
        $field = new xmldb_field('schoolregion', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    return true;
}