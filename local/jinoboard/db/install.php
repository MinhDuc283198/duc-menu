<?php

function xmldb_local_jinoboard_install() {
    global $DB, $USER;
    $maketime = time();
    $records = array(
        array_combine(array('type','allowrental', 'name','engname','position', 'maxbytes', 'maxattachments', 'allownotice', 'allowreply', 'allowcomment', 'allowupload', 'allowsecret', 'allowcategory', 'allowperiod','status','required','userid', 'timemodified'),
                array(1,0,'공지사항','Notice','lms'   , 0, 1, 1,  0, 0, 1, 0, 0, 1,  1, 1,$USER->id, $maketime)),
        array_combine(array('type','allowrental', 'name','engname','position', 'maxbytes', 'maxattachments', 'allownotice', 'allowreply', 'allowcomment', 'allowupload', 'allowsecret', 'allowcategory', 'allowperiod','status','required','userid', 'timemodified'), 
                array(2,0, '1:1 질문과 답변','1:1 Q&A','lms', 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,1,$USER->id, $maketime)),
        array_combine(array('type','allowrental', 'name','engname','position', 'maxbytes', 'maxattachments', 'allownotice', 'allowreply', 'allowcomment', 'allowupload', 'allowsecret', 'allowcategory', 'allowperiod','status','required','userid', 'timemodified'),
                array(3,0, '자주 묻는 질문','FAQ','lms'       , 0, 1, 0, 0, 0, 1, 0, 1, 0, 1,1,$USER->id, $maketime)),
        array_combine(array('type','allowrental', 'name','engname','position', 'maxbytes', 'maxattachments', 'allownotice', 'allowreply', 'allowcomment', 'allowupload', 'allowsecret', 'allowcategory', 'allowperiod','status','required','userid', 'timemodified'), 
                array(4,0, '자료공유','Data' ,'lms'   , 102400, 3, 0, 0, 1, 1, 0, 1, 0, 1,1,$USER->id, $maketime)),
        array_combine(array('type','allowrental', 'name','engname','position', 'maxbytes', 'maxattachments', 'allownotice', 'allowreply', 'allowcomment', 'allowupload', 'allowsecret', 'allowcategory', 'allowperiod','status','required','userid', 'timemodified'),
                array(5,0, '자료실','storage','lms'  , 512000, 5, 0, 0, 1, 1, 0, 1, 0, 1,1,$USER->id, $maketime))
    );
    // pr : 임직원, ad : 멘토, rs : 교육생
    foreach ($records as $record) {
        $DB->insert_record('jinoboard', $record);
    }
    
     $notice  = $DB->get_field('jinoboard','id', array('name' => '공지사항'));
     $QNA     = $DB->get_field('jinoboard','id', array('name' => '1:1 질문과 답변'));
     $FAQ     = $DB->get_field('jinoboard','id', array('name' => '자주 묻는 질문'));
     $data    = $DB->get_field('jinoboard','id', array('name' => '자료공유'));
     $storage = $DB->get_field('jinoboard','id', array('name' => '자료실'));
     
        $allows = array(
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($notice, 'pr'  ,'false','true', 'true', 'true', 'false', 'false', 'false', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($notice,'ad','false','false', 'true', 'true', 'false', 'false', 'false', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($notice,'rs','false','false', 'true', 'true', 'false', 'false', 'false', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($notice,'gu','false','false', 'true', 'true', 'false', 'false', 'false', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($notice,'sa','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($notice,'ma','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),            
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($QNA, 'pr'  ,'true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($QNA,'ad','true','false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($QNA,'rs','false','false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($QNA,'gu','false','false', 'true', 'true', 'true', 'false', 'false', 'false', 'true', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($QNA,'sa','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($QNA,'ma','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),            
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($FAQ, 'pr'  ,'false','true', 'true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($FAQ,'ad','false','false', 'true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($FAQ,'rs','false','false', 'true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($FAQ,'gu','false','false', 'true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($FAQ,'sa','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($FAQ,'ma','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),            
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($data, 'pr'  ,'false','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($data,'ad','false','false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($data,'rs','false','false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($data,'gu','false','false', 'true', 'true', 'false', 'false', 'false', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($data,'sa','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($data,'ma','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),            
             array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($storage, 'pr'  ,'false','false', 'true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($storage,'ad','false','false', 'true', 'true', 'true', 'true', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($storage,'rs','false','false', 'true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($storage,'gu','false','false', 'true', 'true', 'false', 'false', 'false', 'false', 'false', 'false', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($storage,'sa','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime)),
            array_combine(
                    array('board','allowrole','allowdeletecomment','allowsecret', 'allowview', 'allowdetail', 'allowwrite', 'allowreply', 'allowcomment', 'allowmodify', 'allowdelete', 'allowupload', 'timemodified'), 
                    array($storage,'ma','true','true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', $maketime))
        );
        foreach ($allows as $allow) {            
             $DB->insert_record('jinoboard_allowd', $allow);
        }
    
    // 게시판 카테고리 추가
    $categories = array(
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($FAQ,'로그인','login','1', $USER->id,'1',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($FAQ,'학습','study','1', $USER->id,'2',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname', 'isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($FAQ,'포인트','point','1', $USER->id,'3',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($FAQ,'기타','etc','1', $USER->id,'4',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($data,'회원관리','Membership','1', $USER->id,'1',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($data,'학부모상담','Parent meeting','1', $USER->id,'2',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($data,'교육트렌드','Trend','1', $USER->id,'3',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($data,'행정업무','Administration','1', $USER->id,'4',$maketime,'10')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($data,'기타','etc','1', $USER->id,'5',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($storage,'회사이해','company','1', $USER->id,'1',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($storage,'상품이해','product','1', $USER->id,'2',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($storage,'회원관리','Membership','1', $USER->id,'3',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($storage,'학부모상담','Parent','1', $USER->id,'4',$maketime,'10/20/30')),
        array_combine(
                array('board','name','engname','isused','userid', 'sortorder', 'timemodified', 'usertype'),
                array($storage,'기타','etc','1', $USER->id,'5',$maketime,'10/20/30'))        
    );
    foreach ($categories as $category) {
    $DB->insert_record('jinoboard_category', $category);
    }
}
