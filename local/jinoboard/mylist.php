<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

$id = optional_param('id', 1, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$searchfield = optional_param('searchfield', 'title', PARAM_RAW);
$search = strip_tags($search);
$search = htmlspecialchars($search);
$search = preg_replace("/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $search);
$searchfield = strip_tags($searchfield);
$searchfield = htmlspecialchars($searchfield);
$searchfield = preg_replace("/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $searchfield);

$boardcavalue = optional_param('boardca', 0, PARAM_INT);
$boardcavalue = strip_tags($boardcavalue);
$boardcavalue = htmlspecialchars($boardcavalue);
$boardcavalue = preg_replace("/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $boardcavalue);
$uid = $USER->id;
$board = $DB->get_record('jinoboard', array('id' => $id));
$cata = optional_param('cata', 0, PARAM_INT);
$context = context_system::instance();
$role = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));

if (is_siteadmin()) {
    $role = 'sa';
} else if (empty($role)) {
    $role = 'gu';
}
$allows = $DB->get_records('jinoboard_allowd', array('board' => $board->id));
$access = array();
foreach ($allows as $allow) {
    $access[$allow->allowrole] = $allow;
}
$myaccess = $access[$role];

if ($myaccess->allowview != 'true') {
    redirect($CFG->wwwroot, 'Permission Denied');
}

$PAGE->set_context($context);

$PAGE->set_url(new moodle_url('/local/jinoboard/mylist.php', array('id' => $id)));
$PAGE->set_pagelayout('edu');
$PAGE->add_body_class('path-local-jinoboard-' . $id);

$boardname = (current_language() == 'ko') ? $board->name : $board->engname;

$set_title = get_title($board->name);
$PAGE->navbar->add(get_string('mypage','local_jinoboard'));
$PAGE->navbar->add(get_string('courseinfo', 'local_mypage'));
$PAGE->navbar->add("1:1 Q&A");
$PAGE->set_title($boardname);
$PAGE->set_heading($boardname);

echo $OUTPUT->header();


$boardcategory = $DB->get_records('jinoboard_category', array('board' => 3));
$like = '';
$field = '';
if ($searchfield == 'title') {
    $field = 'title';
} else if ($searchfield == 'contents') {
    $field = 'jc.contents';
}
if (!empty($search) && !empty($field)) {
    $like .= " and " . $DB->sql_like($field, ':search', false);
}

if ($cata) {
    $list_where = " and lc.id  =  $cata ";
}
if (!empty($boardcavalue)) {
    $wherebocate = ' and jc.category = ' . $boardcavalue;
}
$totalcount;
if (!is_siteadmin()) {
    $totalcountsql = 'SELECT count(id) 
                                    FROM {jinoboard_contents} jc 
                                    WHERE board = :jcboard AND userid = :userid';
    $totalcount = $DB->count_records_sql($totalcountsql, array('jcboard' => $board->id, 'userid' => $USER->id));

//    $countwritersql = 'SELECT count(id) 
//                                    FROM {jinoboard_contents} jc 
//                                    WHERE board = :jcboard AND userid = :userid';
//    $countwritercount = $DB->count_records_sql($countwritersql, array('jcboard' => $board->id, 'userid' => $USER->id));
//
//    $writersql = 'SELECT * 
//                                    FROM {jinoboard_contents} jc 
//                                    WHERE board = :jcboard AND userid = :userid';
//    $writers = $DB->get_records_sql($writersql, array('jcboard' => $board->id, 'userid' => $USER->id));
//
//    $anscount = 0;
//    foreach ($writers as $writerval) {
//        $anscountimsi = $anscount;
//        $ansql = 'SELECT count(id) 
//                                    FROM {jinoboard_contents}
//                                    WHERE board = 2 and ref = :jcboard and lev > 0';
//        $anscounts = $DB->count_records_sql($ansql, array('jcboard' => $writerval->id));
//
//        $anscount = $anscountimsi + $anscounts;
//    }
//$count_sql = "select count(jc.id) from {jinoboard_contents} jc left join {lmsdata_course} lc on jc.lcourseid = lc.id ";
//$where_sql = " where jc.board = :board  and jc.ref = :ref" . $like . " and jc.isnotice = 0 $list_where $wherebocate ";
} else {
    $count_sql = "select count(jc.id) from {jinoboard_contents} jc left join {lmsdata_course} lc on jc.lcourseid = lc.id ";
    $where_sql = " where jc.board = :board " . $like . " and jc.isnotice = 0 $list_where $wherebocate ";
    $totalcount = $DB->count_records_sql($count_sql . $where_sql, array('board' => $board->id, 'ref' => $ref, 'search' => '%' . $search . '%'));
}
$total_pages = jinoboard_get_total_pages($totalcount, $perpage);
?>

<h3 class="page_title"><?php echo $boardname ?></h3>
<div class="tab-table-section" class="white-bg">
    <!-- 검색 폼 시작 -->
    <?php if (is_siteadmin()) { ?>
        <form class="table-search-option">
            <input type="hidden" name="id" value="<?php echo $id; ?>">

            <select name="searchfield" title="title" >
                <option value="title" <?php if ($searchfield == 'title') { ?> selected="selected"<?php } ?>><?php echo get_string('title', 'local_jinoboard'); ?></option>
                <option value="contents" <?php if ($searchfield == 'contents') { ?> selected="selected"<?php } ?>><?php echo get_string('content', 'local_jinoboard'); ?></option>
            </select>
            <input type="text" title="search" src="javascript:alert('XSS');" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="키워드를 입력하세요">
            <input type="submit" value="<?php echo get_string('search', 'local_jinoboard'); ?>" class="board-search"/>
            <br>

        </form>
    <?php } ?>
    <!-- 검색 폼 끝 --> 
    <div class="table-header-area">     
        <!-- 게시글 수 정보 표시 및 게시글 표시 갯수 설정 -->
        <form>
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <input type="hidden" name="search" value="<?php echo $search; ?>">
            <select name="perpage" onchange="this.form.submit();" title="page">
                <?php
                $nums = array(10, 20, 30, 50);
                foreach ($nums as $num) {
                    $selected = ($num == $perpage) ? 'selected' : '';

                    echo '<option value="' . $num . '" ' . $selected . '>' . get_string('showperpage', 'local_jinoboard', $num) . '</option>';
                }
                ?>
            </select>
            <span class="table-count">
                <?php echo '(' . $page . '/' . $total_pages . get_string('page', 'local_jinoboard') . ',' . get_string('total', 'local_jinoboard') . $totalcount . get_string('case', 'local_jinoboard') . ')'; ?>
            </span>
        </form>
        <!-- 게시글 수 정보 표시 및 게시글 표시 갯수 설정 끝 -->
    </div>

    <div class="table-filter-area">
        <?php
        if ($myaccess->allowwrite == 'true') {
            ?>
            <input type="button" class="right" value="<?php echo get_string('writepost', 'local_jinoboard') ?>" onclick="location.href = 'mywrite_uncore.php?board=<?php echo $id; ?>'" />
        <?php } ?>
    </div>

    <?php
    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }
    $list_num = $offset;
    $num = $totalcount - $offset;
    ?>    
    <div class="thread-style">
        <ul class="thread-style-lists">
            <?php
            if (!is_siteadmin()) {
                $sql = "select jc.ref from {jinoboard_contents} jc left join {lmsdata_course} lc on jc.lcourseid = lc.id where jc.board = :board and jc.userid = :userid " . $like . " and jc.isnotice = 0 $list_where $wherebocate order by ref DESC, step ASC";
                $refs = $DB->get_records_sql($sql, array('board' => $board->id, 'userid' => $uid, 'search' => '%' . $search . '%', $catogoryparam), $offset, $perpage);

                foreach ($refs as $ref) {
                    $select_sql = "select jc.*, lc.coursename from {jinoboard_contents} jc left join {lmsdata_course} lc on jc.lcourseid = lc.id where jc.ref = :ref order by ref DESC, step ASC";
//        $contents = $DB->get_records_sql($select_sql, array('ref' => $ref->ref), $offset, $perpage);
                    $contents = $DB->get_records_sql($select_sql, array('ref' => $ref->ref));
                    if ($board->allownotice == 1) {
                        $sql = "select * from {jinoboard_contents} jc where board = :board and userid = :userid " . $like . " and isnotice = 1 order by ref DESC, step ASC";
                        $notices = $DB->get_records_sql($sql, array('board' => $board->id, 'userid' => $uid, 'search' => '%' . $search . '%'));

                        foreach ($notices as $content) {
                            $postuser = $DB->get_record('user', array('id' => $content->userid));
                            $fullname = fullname($postuser);
                            $userdate = userdate($content->timecreated);
                            $by = new stdClass();
                            $by->name = $fullname;
                            $by->date = $userdate;
                            $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
                            if (is_siteadmin()) {
                                $by->name .= '(' . $postuser->username . ')';
                            }
                            $fs = get_file_storage();
                            if (!empty($notice->id)) {
                                $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $notice->id, 'timemodified', false);
                            } else {
                                $files = array();
                            }

                            $filecheck = (count($files) > 0) ? '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">' : "";


                            $step = ($content->lev <= 4) ? $content->lev : 4;
                            $date_left_len = $step * 30;
                            $calcwidth = ($content->lev <= 4) ? $content->lev * 30 : 120;
                            $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                            $step_icon = ($content->lev) ? '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="reply" /> ' : '';
                            $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
                            $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
                            $userpicture = new user_picture($postuser);
                            $userpicture->size = 1; // Size f1.
                            $url = $userpicture->get_url($PAGE)->out(false);
                            echo "<li class='isnotice' " . $date_left . ">";
                            echo "<div class='thread-left'><img src= " . $url . " alt = 'user_img' class='userpicture'></div>";
                            echo "<div class='thread-content'><span class='post-title'>" . $step_icon;
                            if (($content->issecret && $USER->id != $content->userid && !$parent->userid != $USER->id) || ($content->issecret && $myaccess->allowsecret != 'true')) {
                                echo $content->title . $new;
                            } else if ($myaccess->allowdetail != 'true') {
                                echo '<a href="#" class="' . $newClass . '" onclick="alert(' . "' ".get_string('logincontent','local_jinoboard')." '" . ')">' . $content->title . $new . '"</a>"';
                            } else {
                                echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/mydetail_uncore.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&board=" . $id . "&searchfield=" . $searchfield . "' class='" . $newClass . "'>" . $content->title . $new . "</a>";
                            }
                            echo "  " . $filecheck;
                            if ($content->issecret) {
                                echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='15' height='15' alt='" . get_string('secreticon', 'local_jinoboard') . "' title='" . get_string('secreticon', 'local_jinoboard') . "'>";
                            }
                            echo '<br/><span class="post-date"><a href="' . $CFG->wwwroot . '/local/lmsdata/user_info.php?id=' . $postuser->id . '">' . get_string("bynameondate", "local_jinoboard", $by) . '</a></span>';
                            echo "</span></div><div class='thread-right'>";
                            echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
                            echo "</div></li>";
                        }
                    }
                    foreach ($contents as $content) {
                        if ($board->allowrental == '1') {
                            switch ($content->status) {
                                case 0: $content->title .= '&nbsp;[' . get_string('apply', 'local_jinoboard') . ']';
                                    break;
                                case 1: $content->title .= '&nbsp;[' . get_string('yes', 'local_jinoboard') . ']';
                                    break;
                                case 2: $content->title .= '&nbsp;[' . get_string('refuse', 'local_jinoboard') . ']';
                                    break;
                            }
                        }

                        $list_num++;
                        $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
                        $fs = get_file_storage();
                        $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $content->id, 'timemodified', false);
                        if (count($files) > 0) {
                            $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                        } else {
                            $filecheck = "";
                        }

                        $step = ($content->lev <= 4) ? $content->lev : 4;
                        $date_left_len = $step * 30;
                        $calcwidth = ($content->lev <= 4) ? $content->lev * 30 : 120;
                        $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                        $step_icon = ($content->lev) ? '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="reply" /> ' : '';


                        $postuser = $DB->get_record('user', array('id' => $content->userid));
                        $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
                        $fullname = fullname($postuser);
                        $userdate = userdate($content->timecreated);
                        $by = new stdClass();
                        $by->name = $fullname;
                        if (is_siteadmin()) {
                            $by->name .= '(' . $postuser->username . ')';
                        }
                        $by->date = $userdate;
                        $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
                        $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
                        $coursename = '';
                        $userpicture = new user_picture($postuser);
                        $userpicture->size = 1; // Size f1.
                        $url = $userpicture->get_url($PAGE)->out(false);
                        echo "<li class=''" . $date_left . ">";
                        echo "<div class='thread-left'><img src= " . $url . " alt = 'user_img' class='userpicture'></div>";
                        echo "<div class='thread-content'><span class='post-title'>" . $step_icon;
                        if ($board->allowcourseselection == 1) {
                            $coursename = '[' . $DB->get_field('lmsdata_course', 'coursename', array('id' => $content->lcourseid)) . ']';
                        }
                        if (($content->issecret && $USER->id != $content->userid && $parent->userid != $USER->id && $myaccess->allowsecret != 'true') && !is_siteadmin()) {
                            echo $content->title . $new . '1';
                        } else if ($myaccess->allowdetail != 'true') {
                            echo '<a href="#" class="' . $newClass . '" onclick="alert(' . "'".get_string('noright','local_jinoboard')."'" . ')"> ' . $coursename . ' ' . $content->title . $new . '"</a>"';
                        } else {
                            echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/mydetail_uncore.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&board=" . $id . "&searchfield=" . $searchfield . "' class='" . $newClass . "'>" . $coursename . " " . $content->title . $new . "</a>";
                        }
                        echo "  " . $filecheck;
                        if ($content->issecret) {
                            echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='20' height='20' alt='" . get_string('secreticon', 'local_jinoboard') . "' title='" . get_string('secreticon', 'local_jinoboard') . "'>";
                        }

                        echo '<br/><span class="post-date">' . get_string("bynameondate", "local_jinoboard", $by) . '</span>';
                        echo "</span></div><div class='thread-right'>";
                        echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
                        echo "</div></li>";
                        $num--;
                    }
                }
            } else {
                $sql = "select jc.*, lc.coursename from {jinoboard_contents} jc left join {lmsdata_course} lc on jc.lcourseid = lc.id where jc.board = :board " . $like . " and jc.isnotice = 0 $list_where $wherebocate order by ref DESC, step ASC";
                $contents = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%', $catogoryparam), $offset, $perpage);
                foreach ($contents as $content) {
                    if ($board->allowrental == '1') {
                        switch ($content->status) {
                            case 0: $content->title .= '&nbsp;[' . get_string('apply', 'local_jinoboard') . ']';
                                break;
                            case 1: $content->title .= '&nbsp;[' . get_string('yes', 'local_jinoboard') . ']';
                                break;
                            case 2: $content->title .= '&nbsp;[' . get_string('refuse', 'local_jinoboard') . ']';
                                break;
                        }
                    }

                    $list_num++;
                    $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
                    $fs = get_file_storage();
                    $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $content->id, 'timemodified', false);
                    if (count($files) > 0) {
                        $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                    } else {
                        $filecheck = "";
                    }

                    $step = ($content->lev <= 4) ? $content->lev : 4;
                    $date_left_len = $step * 30;
                    $calcwidth = ($content->lev <= 4) ? $content->lev * 30 : 120;
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                    $step_icon = ($content->lev) ? '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="reply" /> ' : '';


                    $postuser = $DB->get_record('user', array('id' => $content->userid));
                    $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
                    $fullname = fullname($postuser);
                    $userdate = userdate($content->timecreated);
                    $by = new stdClass();
                    $by->name = $fullname;
                    if (is_siteadmin()) {
                        $by->name .= '(' . $postuser->username . ')';
                    }
                    $by->date = $userdate;
                    $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
                    $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
                    $coursename = '';
                    $userpicture = new user_picture($postuser);
                    $userpicture->size = 1; // Size f1.
                    $url = $userpicture->get_url($PAGE)->out(false);
                    echo "<li class=''" . $date_left . ">";
                    echo "<div class='thread-left'><img src= " . $url . " alt = 'user_img' class='userpicture'></div>";
                    echo "<div class='thread-content'><span class='post-title'>" . $step_icon;
                    if ($board->allowcourseselection == 1) {
                        $coursename = '[' . $DB->get_field('lmsdata_course', 'coursename', array('id' => $content->lcourseid)) . ']';
                    }
                    if (($content->issecret && $USER->id != $content->userid && $parent->userid != $USER->id && $myaccess->allowsecret != 'true') && !is_siteadmin()) {
                        echo $content->title . $new . '1';
                    } else if ($myaccess->allowdetail != 'true') {
                        echo '<a href="#" class="' . $newClass . '" onclick="alert(' . "'".get_string('noright','local_jinoboard')."'" . ')"> ' . $coursename . ' ' . $content->title . $new . '"</a>"';
                    } else {
                        echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/mydetail_uncore.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&board=" . $id . "&searchfield=" . $searchfield . "' class='" . $newClass . "'>" . $coursename . " " . $content->title . $new . "</a>";
                    }
                    echo "  " . $filecheck;
                    if ($content->issecret) {
                        echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='20' height='20' alt='" . get_string('secreticon', 'local_jinoboard') . "' title='" . get_string('secreticon', 'local_jinoboard') . "'>";
                    }

                    echo '<br/><span class="post-date">' . get_string("bynameondate", "local_jinoboard", $by) . '</span>';
                    echo "</span></div><div class='thread-right'>";
                    echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
                    echo "</div></li>";
                    $num--;
                }
            }
            ?>

            <?php
            if (empty($notices) && empty($contents)) {
                ?>
                <li style="padding-left:0px; width:calc(100% - 0px) !important;">
                    <div class="thread-empty">
                        <?php echo get_string('noquestionsregistered','local_jinoboard') ?>
                    </div>
                </li>        
                <?php
            }
            ?>
        </ul>

    </div>
</div>
<div class="table-footer-area">
    <?php
    $page_params = array();
    $page_params['id'] = $id;
    $page_params['perpage'] = $perpage;
    $page_params['search'] = $search;
    $page_params['searchfield'] = $searchfield;
    $page_params['boardca'] = $boardcavalue;
    jinoboard_get_paging_bar($CFG->wwwroot . "/local/jinoboard/mylist.php", $page_params, $total_pages, $page, 0);
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->
<?php echo $OUTPUT->footer(); ?>
