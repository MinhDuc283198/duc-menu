<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

$type = optional_param('type', 1, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$market = optional_param('market', 3, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);
$board = $DB->get_record('jinoboard', array('type' => $type));
$context = context_system::instance();

if($board->allowrental||$type==2){
    require_login();
}

$PAGE->set_context($context);
if ($type == 1) {
    $PAGE->set_url(new moodle_url('/local/jinoboard/index.php', array('type' => $type)));
} else {
    $PAGE->set_url(new moodle_url('/local/jinoboard/index2.php', array('type' => $type)));
}
if($type==2){
    $PAGE->set_pagelayout('edu02');
} else {
    $PAGE->set_pagelayout('edu03',$type);
}

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');

switch (current_language()){
    case 'ko' :
        $boardname = $board->name;
        break;
    case 'en' :
        $boardname = $board->engname;
        break;
    case 'vi' :
        $boardname = $board->vnname;
        break;
}

$today = time();
$PAGE->navbar->add($boardname);
$PAGE->set_title($boardname);
$PAGE->set_heading($boardname);

echo $OUTPUT->header();
$siteadmin =is_siteadmin();
$addsql = '';
$addsql2='';
if($type==2 && $siteadmin==false){ $addsql = 'AND (userid ='.$USER->id.' OR  ref in ( select id from {jinoboard_contents} where userid ='.$USER->id.' ) )';}

if($category){ $addsql2=' AND category ='.$category; }
$sql = "select count(id) from {jinoboard_contents} where board = :board and isnotice = 0 $addsql2 $addsql order by ref DESC, step ASC";
$totalcount = $DB->count_records_sql($sql, array('board' => $board->type));
$total_pages = jinoboard_get_total_pages($totalcount, $perpage);

//<- 타이틀 시작 ->
if ($type==2) {
            ?>
            <h2 class="pg-tit"><?php echo $boardname.' '.get_string('list2','local_jinoboard');?></h2>
            <div class="tp-tb-area text-right">
                <a href="/local/jinoboard/write.php?type=2" class="btns br_blue h40"><?php echo get_string('add','local_jinoboard');?></a>
            </div>
            <?php
} else if ($type==3){
    $categories = $DB->get_records('jinoboard_category',array('board'=>$board->type),'sortorder asc');
    $lang = current_language();
    $ctitle = $categories
    ?>
     <h2 class="pg-tit"><?php echo $boardname;?></h2>
    <ul class="mk-c-tab col4 tab-event">
        <li <?php if($category==0){ echo 'class="on"'; } ?> data-target=".data0<?php echo $category;?>"><a href="<?php echo $CFG->wwwroot . "/local/jinoboard/index.php?type=3&b_t=custom"; ?>"><?php echo get_string('all_list','local_jinoboard');?></a></li>
        <?php foreach($categories as $cate){
                if ($lang == 'ko') {
                    $ctitle = $cate->name;
                } else if ($lang == 'vi') {
                    $ctitle = $cate->vnname; 
                } else if ($lang == 'en') {
                    $ctitle = $cate->engname;
                }
                ?>  <li data-target=".data0<?php echo $category;?>" <?php if($category==$cate->id){ echo 'class="on"'; } ?>><a href="<?php echo $CFG->wwwroot . "/local/jinoboard/index.php?type=3&category=".$cate->id; ?>&b_t=custom"><?php echo $ctitle;?></a></li> <?php
        } ?>
    </ul>   
     <?php
} else if($type==1){
    ?>  <h2 class="pg-tit"><?php echo $boardname;?></h2> <?php
}
//<- 타이틀 끝 ->
//<-리스트 시작->
        $offset = 0;
        if ($page != 0) {
            $offset = ($page - 1) * $perpage;
        }
        $list_num = $offset;
        $num = $totalcount - $offset;
        //--1:1인경우추가
        if($type == 2 && $siteadmin==false){ $fsql = " AND (userid = $USER->id OR ref IN (SELECT id FROM {jinoboard_contents} WHERE userid = $USER->id)) ";}
        //--
        $sql = "select * from {jinoboard_contents} where board = :board and isnotice = 0 $fsql $addsql2 order by timecreated DESC, ref DESC, step ASC";
        $contents = $DB->get_records_sql($sql, array('board' => $board->type), $offset, $perpage);
        if ($type != 3) {
            ?>
                    <?php //상단에뿌려주는부분
                    if ($board->allownotice == 1) {
                        //$sql = "select * from {jinoboard_contents} where board = :board " . $like . " and isnotice = 1 and timeend > $today order by ref DESC, step ASC";
                        $sql = "select * from {jinoboard_contents} where board = :board " . $like . " and isnotice = 1 and ( timeend > $today or timeend = 0) order by ref DESC, step ASC";
                        $notices = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'));
                        foreach ($notices as $content) {
                            $postuser = $DB->get_record('user', array('id' => $content->userid));
                            $fullname = fullname($postuser);
                            $userdate = userdate($content->timecreated);
                            $by = new stdClass();
                            $by->name = $fullname;
                            $by->date = $userdate;
                            $fs = get_file_storage();
                            if (!empty($notice->id)) {
                                $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $notice->id, 'timemodified', false);
                            } else {
                                $files = array();
                            }
                            if (count($files) > 0) {
                                $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                            } else {
                                $filecheck = "";
                            }
                        }
                    }
?>
<table class="table m-block">
    <thead>
        <tr>
            <th width="10%"><?php echo get_string('num','local_jinoboard');?></th>
            <th><?php echo get_string('title','local_jinoboard');?></th>
            <th width="15%"><?php echo get_string('writer','local_jinoboard');?></th>
            <th width="20%"><?php echo get_string('date','local_jinoboard');?></th>
            <?php if($type!=2){?>
            <th width="10%"><?php echo get_string('view:cnt','local_jinoboard');?></th>
            <?php }?>
        </tr>
    </thead>
    <tbody><?php
    if($contents){
        foreach ($notices as $content) {
           ?>
        <tr>
            <td class="m-hide"><?php echo get_string('notice2','local_jinoboard')?></td>
            <td class="text-left overflow  <?php if($board->newday){ echo ($content->timemodified + (86400*$board->newday))>$today ? 'hasnew' : '' ;} ?> ">
                <?php echo !(empty($step_icon)) ? '<span class="tb-reply">re</span>' : '' ; 
                 if ($content->issecret && $USER->id != $content->userid && !is_siteadmin() && $parent->userid != $USER->id) {
                            echo $content->title;
                        } else {
                            echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/detail.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&type=" . $type . "&searchfield=" . $searchfield . "'>" . $content->title . "</a>";
                        }
            ?>
             <?php if($board->newday){ echo ($content->timemodified + (86400*$board->newday))>$today ? '<span class="ic-new">N</span>' : '' ;} ?></td>
            <td><?php echo $fullname; ?></td>
            <td><?php echo date("Y-m-d", $content->timemodified); ?></td>
            <td class="m-f-r"><span class="m-show inline"><?php echo get_string('view:cnt','local_jinoboard');?></span><?php echo $content->viewcnt;?></td>
        </tr>
         <?php
    } }
    if($contents){
        foreach ($contents as $content) {
            $list_num++;
            $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
            $fs = get_file_storage();
            $files = $fs->get_area_files($context->id, 'local_jinoboard', $filename, $content->id, 'timemodified', false);
            if (count($files) > 0) {
                $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
            } else {
                $filecheck = "";
            }
            $postuser = $DB->get_record('user', array('id' => $content->userid));
            $fullname = fullname($postuser);
            $userdate = userdate($content->timecreated);
            $by = new stdClass();
            $by->name = $fullname;
            $by->date = $userdate;
            ?>
        <tr>
            <td class="m-hide"><?php echo $num;?></td>
            <td class="text-left overflow  <?php if($board->newday){ echo ($content->timemodified + (86400*$board->newday))>$today ? 'hasnew' : '' ;} ?> ">
                <?php echo !(empty($step_icon)) ? '<span class="tb-reply">re</span>' : '' ; 
                 if ($content->issecret && $USER->id != $content->userid && !is_siteadmin() && $parent->userid != $USER->id) {
                            echo $content->title;
                        } else {
                            echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/detail.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&type=" . $type . "&searchfield=" . $searchfield . "'>" . $content->title . "</a>";
                        }
            ?>
             <?php if($board->newday){ echo ($content->timemodified + (86400*$board->newday))>$today ? '<span class="ic-new">N</span>' : '' ;} ?></td>
            <td><?php echo $fullname; ?></td>
            <td><?php echo date("Y-m-d", $content->timemodified); ?></td>
            <?php if($type!=2){?>
            <td class="m-f-r"><span class="m-show inline"><?php echo get_string('view:cnt','local_jinoboard');?></span><?php echo $content->viewcnt;?></td>
            <?php }?>
        </tr>
         <?php
        $num--;
    } } else {
        ?> <tr><td colspan="5"><?php if($type==2){ echo get_string('nodata:qna','local_mypage'); } else {  echo get_string('nocontent','local_jinoboard'); }?></td></tr>
            <?php
    }?>
    </tbody>
</table>
            <?php
        } else if ($type == 3) {
            require_once $CFG->dirroot . '/local/jinoboard/faq.php';
        }
        ?>
    <?php
    if ($type) {
        ?>
        <div class="mk-paging">
            <?php
            $page_params = array();
            $page_params['type'] = $type;
            $page_params['perpage'] = $perpage;
            $page_params['search'] = $search;
            $page_params['searchfield'] = $searchfield;
            $page_params['category'] = $category;
            jinoboard_get_paging_bar($CFG->wwwroot . "/local/jinoboard/index.php", $page_params, $total_pages, $page, $market);
            ?>
            <!-- Breadcrumbs End -->
        </div> <!-- Table Footer Area End -->
    <?php } ?>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
                        $(function () {
                            $("#accordion").accordion({
                                collapsible: true,
                                heightStyle: "content",
                                header: "ul",
                                active: false
                            });
                            $('.div_taps').css('display', 'none');
                            $('#information').css('display', 'block');
                            $('#information_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu1_over.png"; ?>');
                            var previous_menu = 'information_btn'
                            var previous_menu_num = '1'
                            $('#information_btn').click(function () {
                                $('.div_taps').css('display', 'none');
                                $('#information').css('display', 'block');
                                $('#information_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu1_over.png"; ?>');
                                if (previous_menu_num != '1') {
                                    $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
                                    previous_menu = 'information_btn'
                                    previous_menu_num = '1'
                                }
                            });
                            $('#database_btn').click(function () {
                                $('.div_taps').css('display', 'none');
                                $('#database').css('display', 'block');
                                $('#database_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu2_over.png"; ?>');
                                if (previous_menu_num != '2') {
                                    $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
                                    previous_menu = 'database_btn'
                                    previous_menu_num = '2'
                                }
                            });
                            $('#endnote_btn').click(function () {
                                $('.div_taps').css('display', 'none');
                                $('#endnote').css('display', 'block');
                                $('#endnote_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu3_over.png"; ?>');
                                if (previous_menu_num != '3') {
                                    $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
                                    previous_menu = 'endnote_btn'
                                    previous_menu_num = '3'
                                }
                            });
                        });
//	$('#accordion input[type="checkbox"]').click(function(e) {
//		e.stopPropagation();
//	});
</script>
<?php echo $OUTPUT->footer(); ?>