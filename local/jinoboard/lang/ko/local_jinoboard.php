 <?php
$string['title_community'] = '커뮤니티';
$string['pluginname'] = '공용 게시판';
$string['pluginnameplural'] = '공용 게시판';

$string['notice'] = '공지사항';
$string['FAQ'] = 'FAQ';
$string['QnA'] = '질문과 답변';
$string['secret'] = '비밀 게시글';
$string['help'] = '이용자메뉴얼(국문)';
$string['Usedboard'] = '연희문고';
$string['en'] = '이용자메뉴얼(영문)';

$string['cannotviewpostlist'] = '이 게시판을 볼 수 없습니다.';

$string['viewdetail'] = '상세 보기';
$string['list'] = '목록';
$string['list2'] = '내역';
$string['title'] = '제목';
$string['content'] = '내용';
$string['content:file'] = '첨부파일';
$string['add'] = '게시글 작성';
$string['attachment'] = '첨부파일';
$string['attachmentcheck'] = '첨부파일을 등록하시려면 체크해주세요.';
$string['attachmentmsg'] = '대용량 첨부파일은 강좌 종강시까지만 유효합니다.';
$string['issecret'] = '공개여부';
$string['issecretno'] = '공개';
$string['issecretyes'] = '비공개';
$string['notice'] = '상단게시';
$string['notice2'] = '공지';
$string['usermanual'] = '이용자메뉴얼(국문)';
$string['usermanual_en'] = '이용자메뉴얼(영문)';
$string['gettingready'] = '준비중입니다';
$string['sample'] = '샘플강의';
$string['online'] = '학술정보원 온라인 이용교육';
$string['input'] = '키워드를 입력하시오';
$string['search'] = '검색';
$string['page'] = '페이지';
$string['total'] = '총';
$string['showperpage'] = '{$a}개씩 보기';
$string['secretalert'] = '비밀글 입니다.';
$string['secreticon'] = '[비밀글아이콘]';
$string['case'] = '건';
$string['writepost'] = '글쓰기';
$string['bynameondate'] = '{$a->date} / {$a->name} ';
$string['viewcount'] = '조회';
$string['timeend'] = '상단 게시 종료일';
$string['btnmenu'] = '메뉴버튼';
$string['icon:img'] = '아이콘이미지';
$string['information'] = 'DB 접속 안내';
$string['go-site'] = '강의 사이트로 이동';
$string['document_download'] = '교육자료 다운로드';
$string['document-film'] = '교육 동영상 보기';
$string['document-film_download'] = '동영상 다운로드';
$string['lecture'] = '강의';
$string['view'] = '보기';
$string['download'] = '다운로드';
$string['db_guide'] = 'DB 안내';
$string['buy'] = '삽니다';
$string['sell'] = '팝니다';
$string['all_list'] = '전체';
$string['purpose'] = '목적';
$string['contact'] = '연락처';
$string['price'] = '가격';
$string['couresname'] = '강의 이름';
$string['completion'] = '거래완료';
$string['nocompletion'] = '거래완료 취소';
$string['maxlength'] = '댓글 최대글자 수는 300자 입니다.';

$string['format'] = ''
        . '<b>&nbsp;&nbsp;&nbsp; 책제목 : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; 해당강의명 : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; 희망가격 : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; 연락처 : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; 상품의상태 : </b>&nbsp;<br><br>';

$string['title'] = '제목';
$string['writer'] = '작성자';
$string['date'] = '날짜';
$string['view:cnt'] = '조회수';
$string['edit'] = '수정';
$string['delete'] = '삭제';
$string['replies'] = '답글';

$string['comment'] = '댓글';
$string['nocontent'] = '등록된 콘텐츠가 없습니다';
$string['empty'] = 'Empty';

$string['apply'] = 'apply';
$string['refuse'] = '승인';
$string['yes'] = '승인';
$string['no'] = '미승인';

$string['guide'] = '이용안내';

$string['emergency'] = '긴급공지';
$string['num'] = '번호';


$string['community'] = '커뮤니티';
$string['facebook_group'] = '한국어 강좌 학습자들의 페이스북 그룹입니다.';
$string['kimvisang'] = '김비상';
$string['application'] = '신청';
$string['refusal'] = '거절';
$string['delete_comment'] = '정말 삭제하시겠습니까??';
$string['register'] = '등록하기';
$string['consultationdetails'] = '1:1상담내역';
$string['writing'] = '글 작성';
$string['all'] = '전체';
$string['number'] = '번호';
$string['writeday'] = '작성일';
$string['student_all'] = '학생전체';
$string['grade_1'] = '1학년';
$string['grade_2'] = '2학년';
$string['grade_3'] = '3학년';
$string['grade_4'] = '4학년';
$string['professor'] = '교수';
$string['sharedata'] = '자료공유';
$string['sharedata_comment1'] = '동료들에게 도움이 될만한 좋은 자료를 공유하는 게시판입니다.';
$string['sharedata_comment2'] = '재능인이라면 누구나 자료를 올리고 다운받을 수 있습니다.';
$string['logincontent'] = '로그인이 필요한 콘텐츠입니다.';
$string['noright'] = '해당 글을 볼 수 있는 권한이 없습니다.';
$string['nopublishedtext'] = '등록된 글이 없습니다';
$string['manager'] = '관리자';
$string['mypage'] = '마이페이지';
$string['noquestionsregistered'] = '등록된 질문이 없습니다.';
$string['education1'] = '학술정보원 이용교육';
$string['name_1'] = '나하나';
$string['education_help'] = '학술정보원에 대한 안내 및 서비스 이용안내 입니다.';
$string['education2'] = '정보검색교육';
$string['name_2'] = '노정임';
$string['education_help1'] = '학술활동에 필요한 다양한 유형의 자료검색, 이용방법에 대해 보다 구체적으로 안내합니다.';
$string['navigation'] = '도서┃학위논문┃학술데이터베이스┃학술논문';
$string['name_3'] = '성지하';
$string['name_4'] = '신원데이터넷 김정하';
$string['search'] = '검색';
$string['managingsearchresults'] = '검색결과 관리';
$string['provision'] = '제공';
$string['cnki'] = 'CNKI 플랫폼 및 검색 기능 안내';
$string['choice'] = '선택';
$string['category'] = '카테고리';
$string['choicecategory'] = '카테고리를 선택하시오';
$string['setdate'] = '게시 종료일은 오늘 이후의 날짜로 설정되어야 합니다.';
$string['publicationenddate'] = '게시 종료일을 선택해주세요.';
$string['checkbox'] = '상단게시 체크박스를 체크해주세요.';