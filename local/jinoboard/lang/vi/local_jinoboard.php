<?php
$string['title_community'] = 'Cộng đồng';
$string['pluginname'] = 'Bản tin';
$string['pluginnameplural'] = 'Bản tin';

$string['notice'] = 'Thông báo';
$string['QnA'] = 'Hỏi đáp';
$string['FAQ'] = 'FAQ';
$string['help'] = 'Hướng dẫn sử dụng';
$string['Usedboard'] = 'Nhà sách Yonhui';
$string['en'] = 'Hưỡng dẫn sử dụng';

$string['cannotviewpostlist'] = "Bạn không thể xem bản tin này.";

$string['viewdetail'] = 'Xem thêm';
$string['list'] = 'Danh sách';
$string['list2'] = 'Danh sách';
$string['title'] = 'Tiêu đề';
$string['content'] = 'Nội dung';
$string['content:file'] = 'Đính kèm file';
$string['add'] = 'Thêm bài viết';
$string['attachment'] = 'Đính kèm';
$string['attachmentcheck'] = 'Kiểm tra để đăng ký tập tin đính kèm.';
$string['attachmentmsg'] = 'Tập tin đính kèm quá lớn, tệp chỉ có giá trị trong suốt khóa học';
$string['secret'] = 'Bài viết riêng tư';
$string['issecret'] = 'Công khai';
$string['issecretno'] = 'Công khai';
$string['issecretyes'] = 'Riêng tư';
$string['notice'] = 'Thông báo';
$string['notice2'] = 'Thông báo';
$string['usermanual'] = 'Hướng dẫn sử dụng';
$string['usermanual_en'] = 'Hướng dẫn sử dụng';
$string['gettingready'] = 'Chuẩn bị sẵn sàng';
$string['sample'] = 'Bài giảng mẫu';
$string['online'] = 'Dịch vụ thông tin học thuật';
$string['input'] = 'Nhập từ khóa';
$string['search'] = 'Tìm kiếm ';
$string['page'] = 'Trang ';
$string['total'] = 'Tổng ';
$string['showperpage'] = 'Hiển thị {$a} mỗi trang';
$string['secretalert'] = 'Đây là bài viết riêng tư';
$string['secreticon'] = 'Biểu tượng riêng tư';
$string['case'] = '';
$string['writepost'] = 'Bài viết';
$string['bynameondate'] = '{$a->date} / {$a->name}';
$string['viewcount'] = 'Lượt xem';
$string['timeend'] = 'Thông báo kết thúc';
$string['btnmenu'] = 'Thực đơn';
$string['icon:img'] = 'Biểu tượng hình ảnh';
$string['information'] = 'Hướng dẫn kết nối DB';
$string['go-site'] = 'Đi đến trang bài giảng';
$string['document_download'] = 'Tải dữ liệu học tập';
$string['document-film'] = 'Xem video học tập';
$string['document-film_download'] = 'Tải video';
$string['lecture'] = 'Bài giảng';
$string['view'] = 'Lượt xem';
$string['download'] = 'Tải xuống';
$string['db_guide'] = 'Hưỡng dẫn sử dụng DB';
$string['buy'] = 'Mua';
$string['sell'] = 'Bán';
$string['all_list'] = 'Tất cả';
$string['purpose'] = 'Mục tiêu';
$string['contact'] = 'Liên hệ';
$string['price'] = 'Giá';
$string['couresname'] = 'Tên khóa học';
$string['completion'] = 'Hoàn thành giao dịch';
$string['nocompletion'] = 'Hủy bỏ giao dịch';
$string['maxlength'] = 'Nội dung bình luận tối đa 300 kí tự';

$string['format'] = ''
        . '<b>&nbsp;&nbsp;&nbsp; Giáo trình môn học : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Tên khóa học : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Giá ưu đãi : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Số điện thoại : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Sản phẩm : </b>&nbsp;<br><br>';

$string['title'] = 'Tiêu đề';
$string['writer'] = 'Tác giả';
$string['date'] = 'Ngày';
$string['view:cnt'] = 'Lượt xem';
$string['edit'] = 'Sửa';
$string['delete'] = 'Xóa';
$string['replies'] = 'Trả lời';

$string['comment'] = 'Bình luận';
$string['nocontent'] = 'Chưa có bài đăng';
$string['emptycategory'] = 'Không có gì';

$string['yes'] = 'Cho phép';
$string['no'] = 'Không cho phép';

$string['guide'] = 'Hướng dẫn sử dụng';

$string['emergency'] = 'Thông báo khẩn cấp';
$string['num'] = 'Số';
$string['community'] = 'Cộng đồng';
$string['facebook_group'] = 'Khóa học tiếng Hàn, Nhóm Facebook học viên tiếng Hàn';
$string['kimvisang'] = 'Kim Bi-sang';
$string['application'] = 'Đăng ký';
$string['refusal'] = 'Từ chối';
$string['delete_comment'] = 'Bạn muốn xóa bình luận?';
$string['register'] = 'Đăng ký';
$string['consultationdetails'] = '1:1 Chi tiết tư vấn';
$string['writing'] = 'Đang soạn thảo';
$string['all'] = 'Tất cả';
$string['number'] = 'Số';
$string['writeday'] = 'Ngày tạo';
$string['student_all'] = 'Tất cả học viên';
$string['grade_1'] = 'Cấp 1';
$string['grade_2'] = 'Cấp 2';
$string['grade_3'] = 'Cấp 3';
$string['grade_4'] = 'Cấp 4';
$string['professor'] = 'Giáo sư';
$string['sharedata'] = 'Chia sẻ dữ liệu';
$string['sharedata_comment1'] = 'Đây là bản thông báo chia sẻ dữ liệu hữu ích cho thành viên';
$string['sharedata_comment2'] = 'Bất cứ ai cũng có thể tải tải liệu lên và tải tải liệu xuống';
$string['logincontent'] = 'Nội dung yêu cầu đăng nhập';
$string['noright'] = 'Bạn bị hạn chế quyền xem bài viết này';
$string['nopublishedtext'] = 'Không có bài viết nào';
$string['manager'] = 'Quản lý';
$string['mypage'] = 'Trang của tôi';
$string['noquestionsregistered'] = 'Không có câu hỏi nào';
$string['education1'] = 'Dịch vụ thông tin học thuật';
$string['name_1'] = 'Nahana';
$string['education_help'] = 'Dịch vụ thông tin học thuật và hướng dẫn sử dụng dịch vụ.';
$string['education2'] = 'Phục hồi thông tin giáo dục';
$string['name_2'] = 'NoJeongIm';
$string['education_help1'] = 'Hướng dẫn này cung cấp thông tin chi tiết hơn về cách tìm kiếm và sử dụng các loại tài liệu cần thiết cho các hoạt động học thuật.';
$string['navigation'] = 'Giáo trình ┃ Luận án ┃ Dữ liệu học thuật ┃ Chuyên luận học thuật';
$string['name_3'] = 'SeongJiHa';
$string['name_4'] = 'Dữ liệu nhận dạng';
$string['search'] = 'Tệp';
$string['managingsearchresults'] = 'Quảng lý kết quả tìm kím';
$string['provision'] = 'Điều khoản';
$string['cnki'] = 'CNKI khả năng và nền tảng tìm kiếm';
$string['choice'] = 'Lựa chọn';
$string['category'] = 'Thể loại';
$string['choicecategory'] = 'Chọn một thể loại';
$string['setdate'] = 'Ngày kết thúc phải sau ngày hiện tại';
$string['publicationenddate'] = 'Vui lòng chọn ngày kết thúc bài viết';
$string['checkbox'] = 'Vui lòng kiểm tra ghim bài viết';