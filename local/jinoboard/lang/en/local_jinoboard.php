<?php
$string['title_community'] = 'community';
$string['pluginname'] = 'Common board';
$string['pluginnameplural'] = 'Common board';

$string['notice'] = 'Notice';
$string['QnA'] = 'Q&A';
$string['FAQ'] = 'FAQ';
$string['help'] = 'User Manual(ko)';
$string['Usedboard'] = 'Yonhui Bookstore';
$string['en'] = 'User Manual(en)';

$string['cannotviewpostlist'] = "You can't view this board";

$string['viewdetail'] = 'Read More';
$string['list'] = 'List';
$string['list2'] = 'List';
$string['title'] = 'Title';
$string['content'] = 'Content';
$string['content:file'] = 'Attached File';
$string['add'] = 'Add Post';
$string['attachment'] = 'Attachment';
$string['attachmentcheck'] = 'To register the attachments, please check.';
$string['attachmentmsg'] = 'Attachment of large capacity, is valid until the time of course completion.';
$string['secret'] = 'Secret Post';
$string['issecret'] = 'Open Y/N';
$string['issecretno'] = 'Y';
$string['issecretyes'] = 'N';
$string['notice'] = 'Notice';
$string['notice2'] = 'Notice';
$string['usermanual'] = 'User Manual(ko)';
$string['usermanual_en'] = 'User Manual(en)';
$string['gettingready'] = 'Under Construction';
$string['sample'] = 'Sample Lecture';
$string['online'] = 'Educational Research Information Online Use Education';
$string['input'] = 'Input Keyword';
$string['search'] = 'Search';
$string['page'] = 'Page';
$string['total'] = 'Total';
$string['showperpage'] = 'Show {$a} per page';
$string['secretalert'] = 'This post is secret.';
$string['secreticon'] = '[Secret post icon]';
$string['case'] = '';
$string['writepost'] = 'Write';
$string['bynameondate'] = '{$a->date} / {$a->name} ';
$string['viewcount'] = 'VIEW';
$string['timeend'] = 'Notice end';
$string['btnmenu'] = 'ButtonMenu';
$string['icon:img'] = 'IconImages';
$string['information'] = 'DB Access Guide';
$string['go-site'] = 'Movement Lecture Site';
$string['document_download'] = 'Download Data of Education';
$string['document-film'] = 'View Education Video';
$string['document-film_download'] = 'Download Video';
$string['lecture'] = 'Lecture';
$string['view'] = 'View';
$string['download'] = 'Download';
$string['db_guide'] = 'DB Guide';
$string['buy'] = 'Buy';
$string['sell'] = 'Sell';
$string['all_list'] = 'All';
$string['purpose'] = 'Purpose';
$string['contact'] = 'Contact';
$string['price'] = 'Price';
$string['couresname'] = 'Course Name';
$string['completion'] = 'Deal Completion';
$string['nocompletion'] = 'Deal Completion Cancel';
$string['maxlength'] = 'Comments The maximum number of characters is 300 characters.';

$string['format'] = ''
        . '<b>&nbsp;&nbsp;&nbsp; Book Subject : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Course Name : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Price Hopes : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Phone : </b>&nbsp;<br><br>'
        . '<b>&nbsp;&nbsp;&nbsp; Product : </b>&nbsp;<br><br>';

$string['title'] = 'Title';
$string['writer'] = 'Writer';
$string['date'] = 'Date';
$string['view:cnt'] = 'Views';
$string['edit'] = 'Edit';
$string['delete'] = 'Delete';
$string['replies'] = 'Replies';

$string['comment'] = 'Comment';
$string['nocontent'] = 'Empty Content';
$string['emptycategory'] = 'None';

$string['yes'] = 'Accept';
$string['no'] = 'Denied';

$string['guide'] = 'Guide';

$string['emergency'] = 'Emergency Notice';
$string['num'] = 'Num';
$string['community'] = 'Community';
$string['facebook_group'] = 'Korean language courses, the Facebook group of learners.';
$string['kimvisang'] = 'KimViSang';
$string['application'] = 'Application';
$string['refusal'] = 'refusal';
$string['delete_comment'] = 'Are you sure you want to delete it??';
$string['register'] = 'Register';
$string['consultationdetails'] = '1:1 Consultation details';
$string['writing'] = 'Writing';
$string['all'] = 'All';
$string['number'] = 'Number';
$string['writeday'] = 'Date of creation';
$string['student_all'] = 'The whole student body';
$string['grade_1'] = 'first grade';
$string['grade_2'] = 'the second grade';
$string['grade_3'] = '3rd grade';
$string['grade_4'] = '4th grade';
$string['professor'] = 'professor';
$string['sharedata'] = 'ShareData';
$string['sharedata_comment1'] = 'This is a bulletin board that shares good material for your colleagues.';
$string['sharedata_comment2'] = 'Anyone with a talent can upload and download data.';
$string['logincontent'] = 'Content that requires login.';
$string['noright'] = 'You do not have permission to view this article.';
$string['nopublishedtext'] = 'There is no article registered.';
$string['manager'] = 'Manager';
$string['mypage'] = 'MyPage';
$string['noquestionsregistered'] = 'There are no registered questions.';
$string['education1'] = 'Training for the use of academic information centers';
$string['name_1'] = 'NaHaNa';
$string['education_help'] = 'This is a guide to the Academic Information Center and a guide to using the service.';
$string['education2'] = 'Information Retrieval Training';
$string['name_2'] = 'NoJeongIm';
$string['education_help1'] = 'Provides more specific information on how to search and use various types of materials for academic activities.';
$string['navigation'] = 'Books┃A dissertation┃Academic Database┃An academic treatise';
$string['name_3'] = 'SeongJiHa';
$string['name_4'] = 'Identity data network KimJeongHa';
$string['search'] = 'Search';
$string['managingsearchresults'] = 'Managing search results';
$string['provision'] = 'Provision';
$string['cnki'] = 'CNKI Platform and Search Capabilities';
$string['choice'] = 'Choice';
$string['category'] = 'Category';
$string['choicecategory'] = 'Select a category.';
$string['setdate'] = 'The publication end date must be set to a date later than today';
$string['publicationenddate'] = 'Please select a publication end date.';
$string['checkbox'] = 'Please check the top post check box.';