<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string('community','local_jinoboard'));

echo $OUTPUT->header();
$communitys = $DB->get_records_sql('select * from {lmsdata_facebook_group} where isused = 1');
?>
<script type="text/javascript">
    document.title = "Master Korean - <?php echo get_string('title_community','local_jinoboard') ?>"
</script>
<h4 class="cmnt-tit"><?php echo get_string('facebook_group','local_jinoboard') ?></h4>
<ul class="thumb-list style03 brd">
    <?php foreach($communitys as $community){
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'facebook_group', $community->id, "", false);
    if (!empty($files)) {
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . 1 . '/local_lmsdata/facebook_group/' . $community->id . '/' . $filename);
        }
    } else {
        $path = $CFG -> wwwroot.'/theme/oklassedu/pix/images/default_commu.png';
    }
?>
        <li>
        <div class="wp">
            <div class="img">
                <a href="<?php echo $community->linkurl;?>" target="_blank"><img src="<?php echo $path;?>" alt="<?php echo $community->name;?>" /></a>
            </div>

            <div class="txt">
                <div class="tit"><a href="<?php echo $community->linkurl;?>" target="_blank"><?php echo $community->name;?></a></div>
            </div>
        </div>
        </li>    
        <?php
    } ?>
   
</ul>
<?php
echo $OUTPUT->footer();
?>


