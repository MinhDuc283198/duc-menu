<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu03');
$PAGE->navbar->add('게시판리스트');

$type = optional_param('type', 4, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$board = $DB->get_record('jinoboard', array('type' => $type));

if($board->allowrental){
    require_login();
}
switch (current_language()){
    case 'ko' :
        $boardname = $board->name;
        break;
    case 'en' :
        $boardname = $board->engname;
        break;
    case 'vi' :
        $boardname = $board->vnname;
        break;
}
echo $OUTPUT->header();

$today = time();
$totalcount = $DB->count_records_sql('select count(*) from {jinoboard_contents} where board = :board and isnotice = 0 order by id desc', array('board' => $board->type));
$total_pages = jinoboard_get_total_pages($totalcount, $perpage);
 $offset = 0;
        if ($page != 0) {
            $offset = ($page - 1) * $perpage;
        }
$list_num = $offset;
$num = $totalcount - $offset;
 $sql = "select * from {jinoboard_contents} where board = :board and isnotice = 0 order by id desc";
$contents = $DB->get_records_sql($sql, array('board' => $board->type), $offset, $perpage);
?>  
<h2 class="pg-tit"><?php echo $boardname; ?></h2>
<ul class="thumb-board">
    <?php if($contents){
        foreach($contents as $content){
         ?> <li> <?php 
           $fs = get_file_storage();
           $files = $fs->get_area_files($context->id, 'local_jinoboard', 'thumbnail', $content->id, "", false);
           if ($files) {
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();
                    if ($file->get_filesize() > 0) {
                        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_jinoboard/thumbnail/' . $content->id . '/' . $filename);
                        $fileobj = '<img class=\'small\' src="' . $path . '">';
                    }
                }
                 if($fileobj){
                    ?> 
                    <div class="img">
                        <?php echo $fileobj; ?>
                    </div>
                    <?php
                }
            }  ?>
                <div class="txt">
                    <div class="tit"><a href="<?php echo $CFG->wwwroot.'/local/jinoboard/detail.php?type='.$type.'&id='.$content->id; ?>"><?php echo $content->title; ?><?php if($board->newday){ echo ($content->timemodified + (86400*$board->newday))>$today ? '<span class="ic-new">N</span>' : '' ;} ?></a></div>
                    <p class="t-gray"><?php echo strip_tags($content->contents); ?></p>
                    <p><span><?php echo get_string('date','local_jinoboard');?>: <?php echo date("Y-m-d", $content->timemodified); ?></span><span><?php echo get_string('view:cnt','local_jinoboard');?>: <?php echo $content->viewcnt;?></span></p>
                </div>
            </li> 
         <?php
        }
          ?>   <?php
    } else {
        ?> <div class="no-data style03">
            <div><?php echo get_string('nocontent','local_jinoboard');?></div>
        </div> <?php
    } ?>
</ul>

<?php
$page_params = array();
$page_params['type'] = $type;
$page_params['perpage'] = $perpage;
jinoboard_get_paging_bar($CFG->wwwroot . "/local/jinoboard/board.php", $page_params, $total_pages, $page);
?>

<!--<ul class="mk-paging">
    <li class="first"><a href="#">first</a></li>
    <li class="prev"><a href="#">prev</a></li>
    <li class="on"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">6</a></li>
    <li><a href="#">7</a></li>
    <li><a href="#">8</a></li>
    <li><a href="#">9</a></li>
    <li><a href="#">10</a></li>
    <li class="next"><a href="#">next</a></li>
    <li class="end"><a href="#">end</a></li>
</ul>-->
<?php
echo $OUTPUT->footer();
?>


