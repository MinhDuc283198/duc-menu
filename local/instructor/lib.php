<?php
function local_instructor_paging_bar($url, $params, $total_pages, $current_page, $max_nav = 10) {
    $result = '';
    //local_instructor_total_pages
	$total_nav_pages = local_instructor_total_pages($total_pages, $max_nav);
	$current_nav_page = (int) ($current_page / $max_nav);
	if (($current_page % $max_nav) > 0) {
		$current_nav_page += 1;
	}

	$page_start = ($current_nav_page - 1) * $max_nav + 1;
	$page_end = $current_nav_page * $max_nav;


	if ($page_end > $total_nav_pages) {
            $page_end = $total_nav_pages;
	}

	if (!empty($params)) {
            $tmp = array();
            foreach ($params as $key => $value) {
                    $tmp[] = $key . '=' . $value;
            }
            $tmp[] = "page=";
            $url = $url . "?" . implode('&', $tmp);
	} else {
            $url = $url . "?page=";
	}
	//echo html_writer::start_tag('div', array('class' => 'board-breadcrumbs'));
        $result .= '<ul class="mk-paging">';
	if ($current_nav_page < $total_pages) {
            $result .= '<li class="first" onclick="go_page(1,'.$params['userid'].')"><a>first</a></li>';
	} else {
            //<li class="first"><a href="#">first</a></li>
            $result .= '<li class="first"><a>first</a></li>';
	}
	if ($current_page > 1) {
		$result .= '<li class="prev"  onclick="go_page('.($current_page - 1).','.$params['userid'].')"><a>prev</a></li>';
                //$result .= '<li class="prev"><a>prev</a></li>';
	} else {
		$result .= '<li class="prev"><a>prev</a></li>';
                //$result .= '<li class="prev"  onclick="go_page('.($current_page - 1).','.$params['userid'].')"><a>prev</a></li>';
	}
        
	for ($i = $page_start; $i <= $page_end; $i++) {
            if ($i == $current_page) {
                $result .= '<li class="on"><a href="#">'.$i.'</a></li>';
            } else {
                //echo '<li data-url="'.$url.'"><a href="'.$url.''.$i.'">'.$i.'</a></li>';
                $result .= '<li onclick="go_page('.$i.','.$params['userid'].')"><a>'.$i.'</a></li>';
            }
	}
        
	if ($current_page < $total_nav_pages) {
            $result .= '<li class="next" onclick="go_page('.($current_page + 1).','.$params['userid'].')"><a>next</a></li>';
	} else {
            $result .= '<li class="next"><a>next</a></li>';
	}
	if ($current_nav_page < $total_pages) {
            $result .= '<li class="end" onclick="go_page('.$page_end.','.$params['userid'].')"><a>end</a></li>';
	} else {
            $result .= '<li class="end"><a>end</a></li>';
	}
	//echo html_writer::end_tag('div');
        $result .= '</ul>';
        return $result;
}

function local_instructor_total_pages($rows, $limit = 10) {
    if ($rows == 0) {
        return 1;
    }

    $total_pages = (int) ($rows / $limit);

    if (($rows % $limit) > 0) {
        $total_pages += 1;
    }

    return $total_pages;
}

function local_instructor_list(){
    global $DB, $USER;
    
    $select_cnt = " select count(u.id)";
    $select = "select  u.id, lt.oneline, lt.oneline_vi, lt.oneline_en, lt.isused, u.firstname, lt.timecreated ";
    $query = "
            from {lmsdata_teacher} lt
            JOIN {user} u ON u.id = lt.userid AND u.confirmed = 1 AND deleted = 0
            JOIN {lmsdata_user} lu ON u.id = lu.userid AND lu.usergroup = 'pr'
            WHERE lt.isused = 1 ORDER BY lt.timecreated DESC, u.id DESC";
    $list = $DB->get_records_sql($select.$query);
    $total = $DB->count_records_sql($select_cnt.$query);
    
    if($list){
        foreach($list as $key => $val){
            switch (current_language()) {
                case 'ko' :
                    $list[$key]->lang_oneline = $val->oneline;
                    break;
                case 'en' :
                    $list[$key]->lang_oneline = $val->oneline_en;
                    break;
                case 'vi' :
                    $list[$key]->lang_oneline = $val->oneline_vi;
                    break;
            }
        }
    }
    
    $return = new stdClass();
    $return->list = $list;
    $return->cnt = $total;
    
    return $return;
}

function local_instructor_id($id){
    global $DB, $USER;
    
    $select = "select  u.id, lt.oneline, lt.oneline_vi, lt.oneline_en, lt.isused, u.firstname, lt.profile, lt.profile_vi, lt.profile_en, lt.comment, lt.comment_vi, lt.comment_en ";
    $query = "
            from {lmsdata_teacher} lt
            JOIN {user} u ON u.id = lt.userid AND u.confirmed = 1 AND deleted = 0
            JOIN {lmsdata_user} lu ON u.id = lu.userid AND lu.usergroup = 'pr'
            WHERE lt.isused = 1 AND lt.userid = $id";
    $list = $DB->get_record_sql($select.$query);
    
    if($list){
        switch (current_language()) {
            case 'ko' :
                $list->lang_oneline = $list->oneline;
                $list->lang_profile = $list->profile;
                $list->lang_comment = $list->comment;
                break;
            case 'en' :
                $list->lang_oneline = $list->oneline_en;
                $list->lang_profile = $list->profile_en;
                $list->lang_comment = $list->comment_en;
                break;
            case 'vi' :
                $list->lang_oneline = $list->oneline_vi;
                $list->lang_profile = $list->profile_vi;
                $list->lang_comment = $list->comment_vi;
                break;
        }
    }

    return $list;
}

function local_instructor_coruses($id,$type = 1){
    global $DB, $USER;
    
    $now = time();
    $course_select = "select lc.id, lc.isused, co.id as coid ";
    $course_query="
                    from {lmsdata_class}  lc
                    join {lmsdata_course}  lco on lc.parentcourseid = lco.id
                    join {course}  co on co.id = lc.courseid
                    JOIN {course} mc2 ON lco.courseid = mc2.id
                    JOIN {course_categories} ca ON ca.id = mc2.category AND ca.visible = 1
                    left JOIN {course_categories} ca2 ON ca.parent = ca2.id AND ca2.visible = 1
                    join {enrol}  e on e.courseid = co.id
                    join {user_enrolments}  ue on ue.enrolid = e.id
                    JOIN {context}  ctx ON ctx.instanceid = co.id AND contextlevel = 50
                    join {role_assignments}  ra on ra.contextid = ctx.id and ra.userid = ue.userid AND ra.roleid = 3 AND ra.userid = $id 
                    JOIN {role}  ro ON ra.roleid = ro.id ";
    $course_where = '';
    if($type == 1){
        $course_where = "WHERE lc.isused = 0 AND lc.learningstart <= $now AND $now <= lc.learningend ";
    }
    
    $course_order = " ORDER BY lc.timecreated DESC, lc.id DESC ";

    return $courses = $DB->get_records_sql($course_select.$course_query.$course_where.$course_order);
}

function local_instructor_board_list($courseid, $page = 1, $perpage = 10){
    global $DB, $USER;
    
    $wherecourse = implode(',',$courseid);

    $qaquery_select = "select  jc.*, cm.module, cm.course, j.type, u.firstname, lc.title  lctitle, lc.vi_title lcvititle, lc.en_title lcentitle ";
    $qaquery = "
                from {course_modules} cm
                JOIN {jinotechboard} j ON j.id = cm.instance
                JOIN {jinotechboard_contents} jc ON jc.board = cm.instance 
                JOIN {course} c ON c.id = cm.course 
                JOIN {lmsdata_class} lc ON c.id = lc.courseid 
                JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id 
                JOIN {user} u ON jc.userid = u.id 
                JOIN {lmsdata_user} lu ON u.id = lu.userid 
                where cm.module=16 and cm.course IN ($wherecourse) AND j.type=2";
    $qaquery_order = " ORDER BY  jc.ref DESC, jc.step asc";
    $list = $DB->get_records_sql($qaquery_select.$qaquery.$qaquery_order,array(),($page - 1) * $perpage, $perpage);
    $total = $DB->count_records_sql("select count(jc.id) ".$qaquery);
    
    if($list){
        foreach($list as $key => $val){
            switch (current_language()) {
                case 'ko' :
                    $list[$key]->lang_title = $val->lctitle;
                    break;
                case 'en' :
                    $list[$key]->lang_title = $val->lcentitle;
                    break;
                case 'vi' :
                    $list[$key]->lang_title = $val->lcvititle;
                    break;
            }
        }
    }
    
    $return = new stdClass();
    $return->list = $list;
    $return->cnt = $total;
    
    return $return;
}