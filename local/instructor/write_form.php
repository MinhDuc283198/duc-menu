<?php

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/mod/okmedia/locallib.php');
class jinotechboard_write_form extends moodleform {

    function definition() {
        global $DB,$PAGE,$CFG;

        $mform = $this->_form;
        $board = $this->_customdata['board'];
        $boardform = $this->_customdata['boardform'];
        $context = $this->_customdata['context'];
        $mode = $this->_customdata['mode'];
        $lev = $this->_customdata['lev'];
        $type = $this->_customdata['type'];
        $searchfield = $this->_customdata['searchfield'];
        $searchvalue = $this->_customdata['searchvalue'];
        $page = $this->_customdata['page'];
        $perpage = $this->_customdata['perpage'];
        
        if($board->type == 4){
            $PAGE->requires->jquery();
            $PAGE->requires->jquery_plugin('ui');
            $PAGE->requires->jquery_plugin('ui-css');
            $PAGE->requires->js_init_call('M.mod_okmedia.init_edit_form', array($CFG->wwwroot,$mform->_attributes['id']), false, okmedia_get_js_module());
            $contents_no = $this->_customdata['contents_no'];
            $tplantitlestatic = $this->_customdata['tplantitlestatic'];
        }

        $isnotice = $this->_customdata['isnotice'];
        $isprivate = $this->_customdata['isprivate'];
        
        $mform->addElement('header', 'nameforyourheaderelement', get_string('notice:write', 'local_board'));

        //제목영역
        $titlearray = array();
        $titlearray[] = & $mform->createElement('text', 'hide', get_string('content:title', 'local_board'));
        $titlearray[] = & $mform->createElement('text', 'title', get_string('content:title', 'local_board'));
        if ($board->allownotice && $mode != 'reply' && $lev == 0 && has_capability('mod/jinotechboard:noticemanager', $context)) {
            $titlearray[] = & $mform->createElement('advcheckbox', 'isnotice', '', get_string('notice', 'local_jinoboard'), array('group' => 2), array(0, 1));
        }
        $mform->addGroup($titlearray, 'titlear', get_string('title', 'jinotechboard'), ' ', false);
        $mform->setType('title', PARAM_CLEAN);
        if ($board->allownotice && $mode != 'reply' && $lev == 0 && has_capability('mod/jinotechboard:noticemanager', $context))
            $mform->setDefault('isnotice', $isnotice);
        $mform->addRule('titlear', null, 'required');
        $titlegrprules = array();
        $titlegrprules['title'][] = array(null, 'required', null, 'client');
        $mform->addGroupRule('titlear', $titlegrprules);

        if ($board->allowsecret && $mode != 'reply') {
            $radioarray = array();
            $radioarray[] = & $mform->createElement('radio', 'isprivate', '', get_string('issecretno', 'local_jinoboard'), 0);
            $radioarray[] = & $mform->createElement('radio', 'isprivate', '', get_string('issecretyes', 'local_jinoboard'), 1);
            $mform->addGroup($radioarray, 'radioar', get_string('issecret', 'local_jinoboard'), array(' '), false);
            $mform->setDefault('isprivate', $isprivate);
        } else {
            $mform->addElement('hidden', 'isprivate');
            $mform->setType('isprivate', PARAM_INT);
            $mform->setDefault('isprivate', $isprivate);
        }

        $mform->addElement('editor', 'contents', get_string('content', 'jinotechboard'), null, self::editor_options());

        if ($board->allowgigan) {
            $ten_day = strtotime(date("Y-m-d", strtotime("+10 day")));
            $availablefromgroup = array();
            $availablefromgroup[] = & $mform->createElement('date_selector', 'timeend', '');
            $availablefromgroup[] = & $mform->createElement('checkbox', 'availablefromenabled', '', get_string('enable'));
            $mform->addGroup($availablefromgroup, 'availablefromgroup', get_string('timeend', 'local_jinoboard'), ' ', false);
            $mform->disabledIf('availablefromgroup', 'availablefromenabled');
            $mform->setDefault('timeend', $ten_day);
            $mform->setDefault('availablefromenabled', true);
            $mform->setType('timeend', PARAM_INT);
            $mform->setType('availablefromenabled', PARAM_INT);
        }
        if($type != 4){
            $chk = "<input type='checkbox' name='view_filemanager' />" . get_string('attachmentcheck', 'local_jinoboard');

            $mform->addElement('static', 'static', get_string('attachment', 'jinotechboard'), $chk);

//            $staticmsg = "<br><span>" . get_string('attachmentmsg', 'local_jinoboard') . "</span>";
            $mform->addGroup(array(
                $mform->createElement('filemanager', 'attachments', get_string('attachment', 'jinotechboard'), null, self::attachment_options($board)),
                $mform->createElement('static', 'text', '', $staticmsg)
            ), 'filemanager', '', array(''), false);
        }else{
            $tool = '<div class="fcontainer clearfix">
                        <div id="fitem_id_titlestatic" class="fitem">
                            <div class="felement fstatic">
            
                                <input size="58" readonly="readonly" name="tplantitlestatic" type="text" value="'.$tplantitlestatic.'"> 
                                <input id="mod_lcms_btn_select" type="button" value="선택" onclick="javascript:M.mod_okmedia.show('.$board->course.','.$board->type.');">
                            </div>
                        </div>
                    </div>';
            $mform->addElement('static', 'static', get_string('attachment', 'jinotechboard'),$tool);
            $mform->addElement('hidden', 'contents_no');
            
        }
 
//        if($board->type == 1){
//            $chk = '<input name="send_type[sms]" value="true" type="checkbox">SMS<input name="send_type[mail]" value="true" type="checkbox">메일<input name="send_type[message]" value="true" type="checkbox">메시지';
//        
//            $mform->addElement('static', 'static', get_string('send_type', 'jinotechboard'), $chk);
//            $smssender = array();
//            $smssender[] = & $mform->createElement('text', 'smssender', get_string('smssender', 'local_board'));
//            $smssender[] = & $mform->createElement('static', 'des_sender', '', '형식) 010-1111-1111 : 대쉬(-)를 포함하여 작성');
//            $mform->addGroup($smssender, 'sms', get_string('sms_sender', 'jinotechboard'), ' ', false);
//            $mform->addElement('textarea', 'sms_content', get_string('sms_content', 'jinotechboard'));
//        }
        
        $this->add_action_buttons();

        $mform->addElement('hidden', 'b');
        $mform->setType('b', PARAM_INT);
        $mform->setDefault('b', $board->id);

        $mform->addElement('hidden', 'confirmed');
        $mform->setType('confirmed', PARAM_INT);
        $mform->setDefault('confirmed', $this->_customdata['confirmed']);

        $mform->addElement('hidden', 'boardform');
        $mform->setType('boardform', PARAM_INT);
        $mform->setDefault('boardform', $boardform);
        
        $mform->addElement('hidden', 'searchfield');
        $mform->setType('searchfield', PARAM_RAW);
        $mform->setDefault('searchfield', $searchfield);
        
        $mform->addElement('hidden', 'searchvalue');
        $mform->setType('searchvalue', PARAM_RAW);
        $mform->setDefault('searchvalue', $searchvalue);
        
        $mform->addElement('hidden', 'page');
        $mform->setType('page', PARAM_INT);
        $mform->setDefault('page', $page);
        
        $mform->addElement('hidden', 'perpage');
        $mform->setType('perpage', PARAM_INT);
        $mform->setDefault('perpage', $perpage);

        $mform->addElement('hidden', 'itemid');
        $mform->setType('itemid', PARAM_INT);
        $mform->setDefault('itemid', $this->_customdata['itemid']);

        $mform->addElement('hidden', 'boardform');
        $mform->setType('boardform', PARAM_INT);
        $mform->setDefault('mode', '1');

        if ($this->_customdata['mode'] == "edit") {
            $mform->addElement('hidden', 'mode');
            $mform->setType('mode', PARAM_TEXT);
            $mform->setDefault('mode', 'edit');

            $mform->addElement('hidden', 'contentId');
            $mform->setType('contentId', PARAM_INT);
        } else if ($this->_customdata['mode'] == "reply") {
            $mform->addElement('hidden', 'mode', "reply");
            $mform->setType('mode', PARAM_TEXT);

            $mform->addElement('hidden', 'contentId');
            $mform->setType('contentId', PARAM_INT);

            $mform->addElement('hidden', 'ref');
            $mform->setType('ref', PARAM_INT);

            $mform->addElement('hidden', 'step');
            $mform->setType('step', PARAM_INT);

            $mform->addElement('hidden', 'lev');
            $mform->setType('lev', PARAM_INT);
        }
    }

    public static function editor_options() {
        global $COURSE, $PAGE, $CFG;
        require_once($CFG->dirroot . "/repository/lib.php");

        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL
        );
    }

    public static function attachment_options($board) {

        return array(
            'subdirs' => 0,
            'maxbytes' => $board->maxbytes,
            'maxfiles' => $board->maxattachments,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL
        );
    }

}

?>
