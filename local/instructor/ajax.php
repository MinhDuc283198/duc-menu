<?php

define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/instructor/lib.php');
$action = optional_param('action','',PARAM_RAW);

switch ($action) {
    case 'paging':
        $data = required_param_array('data', PARAM_RAW);
        $userid = $data['userid'];
        $courses = local_instructor_coruses($userid);
        
        $courseid = array();
        if($courses){
            foreach($courses as $course){
                $courseid[] = $course->coid;
            }
            $wherecourse = implode(',',$courseid);
            $qalist = local_instructor_board_list($courseid, $data["page"], 10)->list;
            $qatotal = local_instructor_board_list($courseid, $data["page"], 10)->cnt;
            
            $html = "";
            
            $html .= '<div class="mg-bt10">'.get_string('total','local_management',$qatotal).'</div>';
            $html .= '<table class="table m-block qalist">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th width="7%">'.get_string('number','local_management').'</th>';
            $html .= '<th width="20%">'.get_string('goodsname','local_management').'</th>';
            $html .= '<th>'.get_string('title','local_management').'</th>';
            $html .= '<th width="15%">'.get_string('review:writer','local_management').'</th>';
            $html .= '<th width="15%">'.get_string('review:timecreated','local_management').'</th>';
            $html .= '<th width="8%">'.get_string('view:cnt','local_jinoboard').'</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';

            $i = $qatotal - (($data["page"]-1) * 10);
            if($qalist){
                foreach($qalist as $ql){
                    if ($ql->lev) {
                        if ($ql->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
                            $step = $ql->lev;
                            $date_left_len = $step * 15;
                        } else {
                            $step = JINOTECHBOARD_INDENT_DEFAULT;
                            $date_left_len = $step * 15;
                        }

                        switch ($ql->lev) {
                            case 1 : $calcwidth = 15;
                                break;
                            case 2 : $calcwidth = 30;
                                break;
                            case 3 : $calcwidth = 60;
                                break;
                            case 4 : $calcwidth = 90;
                                break;
                            default : $calcwidth = 90;
                                break;
                        }
                        $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                        //$step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="Reply" /> ';
                        $reply = '<span class="tb-reply">re</span>';
                    } else {
                        $date_left = 'style="width:calc(100% - 0px) !important;"';
                        //$step_icon = '';
                        $reply = '';
                    }

                    $html .= '<tr>';
                    $html .= '<td class="m-hide">'.$i--.'</td>';
                    $html .= '<td class="text-left overflow">'.$ql->lang_title.'</td>';
                    $new = ($ql->timecreated + (86400*7))>time() ? '<span class="ic-new">N</span>' : '' ;
                    $secret = ($ql->isprivate == 1) ? '<img src="'.$CFG->wwwroot.'/theme/oklassedu/pix/lock.png" width="15" height="15" alt="비공개" title="비공개">' : '';
                    
                    $onclick = (($ql->isprivate != 1) || ($ql->isprivate == 1 && ($USER->id == $userid || $USER->id == $ql->userid )) ) ? 'onclick="go_detail('.$ql->id.','.$data['page'].','.$userid.')"' : 'onclick="alert(\'비공개로 작성된 게시물입니다.\');return false;" ';
                    
                    $html .= '<td class="text-left overflow hasnew"'. $date_left.$onclick.'  ><a href="#">'. $reply.$ql->title.$secret.'</a>'.$new.'</td>';
                    $html .= '<td>'.$ql->firstname.'</td>';
                    $html .= '<td>'.date("Y.m.d",$ql->timecreated).'</td>';
                    $html .= '<td class="m-f-r"><span class="m-show inline">'.get_string('view:cnt','local_jinoboard').'</span>'.$ql->viewcnt.'</td>';
                    $html .= '</tr>';
                }
            }else{
                $html .= "<tr><td colspan='6'>".get_string('nocontent', 'mod_jinotechboard')." </td></tr>";
            }
            $html .= '</table>';
            $page_params = array();
            $page_params['perpage'] = 10;
            //$page_params['id'] = $id;
            $page_params['course'] = $wherecourse;
            $page_params['userid'] = $userid;
            $paging = local_instructor_paging_bar($CFG->wwwroot . "/local/instructor/detail.php", $page_params, $qatotal, $data["page"]);
        }

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $qalist;
        $rvalue->html = $html;
        $rvalue->paging = $paging;
        echo json_encode($rvalue);
        break;
    case 'detail':
        $data = required_param_array('data', PARAM_RAW);
        $query = "select jc.*, u.firstname 
                  from {jinotechboard_contents} jc 
                  JOIN {user} u ON u.id = jc.userid 
                  JOIN {lmsdata_user} lu ON u.id = lu.userid 
                  where jc.id = ".$data['id'];
        $list = $DB->get_record_sql($query);
        $PAGE->requires->jquery();
        if($list){
            $html = '';
            $reply = '';
            if ($list->lev) {
                $reply = '<span class="tb-reply">re</span>';
            }
            $html .= '<div class="tb-detail-tit">'.$reply.$list->title.'</div>';
            $html .= '<div class="tb-info">';
            $html .= '<p class="f-l">';
            $html .= '<strong>'.$list->firstname.'</strong>'; 
            $userdate = userdate($list->timecreated);
            $html .= '<span>'.$userdate.'</span>';
            $html .= '</p>';
            $html .= '<div class="f-r">';
            $html .= '<span>'.get_string('view:cnt','local_jinoboard').$list->viewcnt.'</span>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="tb-cont">'.$list->contents.'</div>';
            
            
            
            $course = $DB->get_record('course', array('id' => $list->course), '*', MUST_EXIST);
            $board = $DB->get_record('jinotechboard', array('id' => $list->board), '*', MUST_EXIST);
            $cm = get_coursemodule_from_instance('jinotechboard', $board->id, $course->id, false, MUST_EXIST);
            $classname = context_helper::get_class_for_level(CONTEXT_MODULE);

            $contexts[$cm->id] = $classname::instance($cm->id);
            $context = $contexts[$cm->id];
            
            //첨부파일영역
            $fs = get_file_storage();
            $files = $fs->get_area_files($context->id, 'mod_jinotechboard', 'attachment', $list->id, 'id', false);
            $attachments = "";
            if (count($files) > 0) {
                $type = '';
                $attfile = '';

                if ($CFG->enableportfolios)
                    $canexport = $USER->id == $list->userid;
                if ($canexport) {
                    require_once($CFG->libdir . '/portfoliolib.php');
                }
                foreach ($files as $file) {
                    $filepath = $file->get_filepath();
                    if (empty($filepath)) {
                        $filepath = '/';
                    }

                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();

                    $attfile .= '<li>';
                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/mod_jinotechboard/attachment/' . $list->id . $filepath . $filename);
                    if ($file->get_filesize() > 0) {
                        $attfile .= "<a href=\"$path\">" . s($filename) . "</a>";
                    } else {
                        $alertstring = get_string('deletedfile', 'jinotechboard');
                        //$attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . $iconimage . '</a>';
                        $attfile .= '<a href="#" onclick="javascript:alert(\'' . $alertstring . '\');">' . s($filename) . "</a>";
                    }
                    $attfile .= '</li>';
                }

                $attachments .= $attfile;

                $output .= html_writer::tag('ul', $attachments);
            }

            if($output){
                $html .= '<div class="tb-file-area">';
                $html .= '<div class="tit">'.get_string('attachments','local_management').'</div>';
                $html .= $output;
                $html .= '</div>';
            }
            
            $html .= '<div class="tb-btns-area">';
            $html .= '<input type="button" value="'.get_string('jinotechboard:list', 'jinotechboard').'" class="btns br h40" onclick="go_page('.$data["page"].','.$data['userid'].')"/>';
            //if($data["userid"] == $USER->id){
            if ((($board->type == 2 && has_capability('mod/jinotechboard:reply', $context))) && !$list->isnotice && ($list->userid != $USER->id) ) {
                $onclick = "window.open('$CFG->wwwroot/mod/jinotechboard/write.php?b=$list->board&contentId={$data['id']}&mode=reply&type=2&boardform=2&perpage=10')";
                //$html .= '<input type="button" value="'.get_string('replies', 'jinotechboard').'" class="btns br h40" onclick="'.$onclick.'"/>';
                $html .= '<div class="f-r">';
                //$html .= '<input type="button" value="'.get_string('edit', 'jinotechboard').'" class="btns br h40" onclick="go_edit('.$data['id'].')"/>';
                //$right_btns .= html_writer::tag('button', get_string('delete', 'jinotechboard'), array("class" => "btns h40 br", 'style' => 'cursor:pointer;', 'onclick' => 'if(confirm("삭제하시겠습니까?")){}'));
                // location.href="' . $CFG->wwwroot . "/mod/jinotechboard/delete.php?b=" . $board->id . "&contentId=" . $content->id . "&boardform=" . $boardform . '&mode=delete" 
                $delete_confirm = "if(confirm('삭제하시겠습니까?')){go_delete('$list->board','{$data['id']}','{$data['page']}','{$data['userid']}')}";
                //$html .= '<input type="button" value="'.get_string('delete', 'jinotechboard').'" class="btns br h40" onclick="'.$delete_confirm.'"/>';
                $html .= '<input type="button" value="'.get_string('replies', 'jinotechboard').'" class="btns br h40" onclick="'.$onclick.'"/>';
                $html .= '</div>';
            }
            $html .= '</div>';
            
            
            if($html){
                if (isset($list->id)) {
                    if($list->userid != $USER->id){
                        $DB->insert_record('jinotechboard_read', array('userid' => $USER->id, 'courseid' => $list->course, 'jinotechboardid' => $list->board, 'contentsid' => $list->id, 'firstread' => time(), 'lastread' => time()));
                        $DB->set_field_select('jinotechboard_contents', 'viewcnt', intval($list->viewcnt) + 1, " id='$list->id'");
                    }
                }
            }
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->html = $html;
        echo json_encode($rvalue);
        break;
    case 'edit':
        $data = required_param_array('data', PARAM_RAW);
        $detail = $DB->get_record('jinotechboard_contents',array('id' => $data['id']));
        $b = $detail->board;
        $boardform = 2;
        
        require_once($CFG->dirroot . '/mod/jinotechboard/write_form.php');
        require_once $CFG->libdir . '/completionlib.php';
        require_once $CFG->dirroot . '/lib/formslib.php';
        require_once $CFG->dirroot . '/lib/form/filemanager.php';
        require_once($CFG->dirroot . "/repository/lib.php");
        
        if ($b) {
            if (!$board = $DB->get_record("jinotechboard", array("id" => $b))) {
                print_error('invalidboardid', 'jinotechboard');
            }
            if (!$course = $DB->get_record("course", array("id" => $board->course))) {
                print_error('coursemisconf');
            }

            if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
                print_error('missingparameter');
            }

            require_course_login($course, true, $cm);
            $strboards = get_string("modulenameplural", "jinotechboard");
            $strboard = get_string("modulename", "jinotechboard");
        } else {
            print_error('missingparameter');
        }
        $classname = context_helper::get_class_for_level(CONTEXT_MODULE);
        $contexts[$cm->id] = $classname::instance($cm->id);
        $context = $contexts[$cm->id];
        
        $postid = empty($detail->id) ? null : $detail->id;
        $editor_option = jinotechboard_editor_options($context, $postid);
        
        editors_head_setup();
        $editor = editors_get_preferred_editor(FORMAT_HTML);
        $editor->id = "editor_contents";
        $args = new stdClass();
        $args->accepted_types = array('web_image');
        $args->return_types = (FILE_INTERNAL | FILE_EXTERNAL);
        $args->context = $context;
        $args->env = 'filepicker';

        $draftitemid = file_get_submitted_draft_itemid('attachments');
        // advimage plugin
        $image_options = initialise_filepicker($args);
        $image_options->context = $context;
        $image_options->client_id = uniqid();
        $image_options->maxbytes = $editor_option['maxbytes'];
        $image_options->env = 'editor';
        $image_options->itemid = $draftitemid;

        $fpoptions = array();
        $fpoptions['image'] = $image_options;

        $editor->use_editor($editor->id, $editor_option, $fpoptions);
        $draftid_editor = file_get_submitted_draft_itemid('contents');
        $editor_options = array('noclean' => true, 'subdirs' => true, 'maxfiles' => $board->maxattachments, 'maxbytes' => $board->maxbytes, 'context' => $context);
        $mformdata = array('options' => $editor_options,
        'confirmed' => 1,
        'board' => $board,
        'itemid' => $draftid_editor,
        'boardform' => $boardform,
        'context' => $context,
        'type' => $board->type,
        'mode' => $mode);
        $mformdata['lev'] = $detail->lev;
        $mformdata['isnotice'] = $detail->isnotice;
        $mformdata['isprivate'] = $detail->isprivate;
        
        
        $title = $detail->title;
        $contents = $detail->contents;
        
        $category_field = $detail->category;
        $form_data = array(
            'title' => $title,
            'boardform' => $boardform,
            'contents' => array(
                'text' => $contents,
                'format' => 1,
                'itemid' => $draftid_editor,
                'timeend' => null,
                ));
        
        $form_data['timeend'] = $detail->timeend;
        $form_data['attachments'] = $draftitemid;
        $content = $DB->get_record('jinotechboard_contents', array('id' => $data['id']));
        $form_data['lev'] = $detail->lev;
        $form_data['contentId'] = $data['id'];
        $form_data['mode'] = 'edit';
        $form_data['timeend'] = $detail->timeend;
        $form_data['contents_no'] = $contents_no;
        $form_data['tplantitlestatic'] = $tplantitlestatic;

        $mform = new jinotechboard_write_form($url, $mformdata);
        $mform->set_data($form_data);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->html = $mform->render();
        echo json_encode($rvalue);
        break;
    case 'delete':
        $data = required_param_array('data', PARAM_RAW);
        
        $b = $data["b"];
        $contentId = $data["contentId"];
        $content = $DB->get_record('jinotechboard_contents', array('id' => $contentId), '*', MUST_EXIST);
        $page = $data["page"];
        $userid = $data["userid"];

        if ($b) {
            if (!$board = $DB->get_record("jinotechboard", array("id" => $b))) {
                print_error('invalidboardid', 'jinotechboard');
            }
            if (!$course = $DB->get_record("course", array("id" => $board->course))) {
                print_error('coursemisconf');
            }
            if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
                print_error('missingparameter');
            }
        } else {
            print_error('missingparameter');
        }

        require_course_login($course, true, $cm);
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        $PAGE->set_context($context);


        if (!has_capability('mod/jinotechboard:view', $context)) {
            notice('cannotdeletepost', 'jinotechboard');
        }

        $db_userid = $DB->get_field('jinotechboard_contents', 'userid', array('id' => $contentId, 'board' => $b));
        if ($db_userid == $USER->id || has_capability("mod/jinotechboard:delete", $context)) {
            $DB->delete_records_select("jinotechboard_contents", "id=? and board=?", array($contentId, $b));
            $params = array(
                'objectid' => $content->id,
                'context' => $context,
                'other' => array(
                    'jinotechboardid' => $board->id,
                )
            );
            $event = \mod_jinotechboard\event\content_deleted::create($params);
            $event->add_record_snapshot('jinotechboard_contents', $content);
            $event->trigger();
            $msg = get_string('Deletionshavebeencompleted', 'jinotechboard');
            //redirect("view.php?b=$b");
        } else {
            $msg = get_string('no_delete_permission', 'jinotechboard');
            //redirect("view.php?b=$b&contentId=$contentId");
        }

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        //$rvalue->html = $mform->render();
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();