<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/instructor/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('강사소개목록');

echo $OUTPUT->header();
$lang = current_language();
$list = local_instructor_list()->list;
$total = local_instructor_list()->cnt;
?>  
<script type="text/javascript">
    document.title = 'Master Korean - <?php echo get_string('title_seo','local_instructor')?>';
</script>
<h4 class="pg-tit"><?php echo get_string('title','local_instructor')?></h4>
<?php if($total > 0){?>
<ul class="thumb-list style03 teacher brd">
    <?php 
    foreach($list as $li){ 
        $usercontext = context_user::instance($li->id);
        $fs = get_file_storage();
        $files = $fs->get_area_files($usercontext->id, 'local_lmsdata', 'teacher', $li->id, "", false);
        if(!empty($files)){
            foreach ($files as $file) {
                $filename = $file->get_filename();
                $mimetype = $file->get_mimetype();
                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $usercontext->id . '/local_lmsdata/teacher/' . $li->id . '/' . $filename);
            }
        }else{
            $path = $CFG->wwwroot . '/theme/oklassedu/pix/images/default_course.png';
        }
    ?>
    <li onclick="location.href='./detail.php?id=<?php echo $li->id?>'" class="cursor">
        <div class="wp">
            <div class="img">
                <a href="#"><img src="<?php echo $path?>" alt="<?php echo $li->firstname?>" /></a>
            </div>
            <div class="txt">
                <?php if($lang == 'ko'){ ?>
                <p class="t-nm"><a href="#"><strong><?php echo $li->firstname?></strong> <?php echo get_string('teacher','local_course')?></a></p>
                <?php }else{?>
                <p class="t-nm"><a href="#"><?php echo get_string('teacher','local_course')?> <strong> <?php echo $li->firstname?></strong></a></p>
                <?php }?>
                <div class="t-info"><a href="#"><?php echo $li->lang_oneline?></a></div>
            </div>
        </div>
    </li>
    <?php }?>
</ul>
<?php }else{?>

<div class="no-data">
    <div><?php echo get_string('noteacher','local_instructor')?></div>
</div>
<?php }?>
<?php
echo $OUTPUT->footer();
?>


