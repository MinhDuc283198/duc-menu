<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script>
            function call_ajax(action, data, async) {
                var rvalue = false;

                $.ajax({
                    type: 'POST',
                    url: '/local/instructor/ajax.php',
                    dataType: 'JSON',
                    async: async,
                    data: {action: action, data: data},
                    success: function(result) {
                        rvalue = result;
                    },
                    error: function(xhr, status, error) {
                    }
                });

                return rvalue;
            }
            function go_page(page,userid){
                //var page = $(this).data('page');

                //var course = $('.mk-paging').data('course');
                //var course = $('.data02').data('course');

                var result = call_ajax('paging', {page:page,userid:userid}, false);
                if(result.status == "success"){
                    alert("성공!");
                    $('.data02').html(result.html);
                    $('.data02').append(result.paging);
                }
            }
        </script>
    </head>
    <body>
        <?php
        require_once('../../config.php');
        //require_once('./detail.php');

        $b = optional_param('b', 0, PARAM_INT);
        $contentId = optional_param('contentId', 0, PARAM_INT);

        $content = $DB->get_record('jinotechboard_contents', array('id' => $contentId), '*', MUST_EXIST);
        
        $page = optional_param('page', 0, PARAM_INT);
        $userid = optional_param('userid', 0, PARAM_INT);

        if ($b) {

            if (!$board = $DB->get_record("jinotechboard", array("id" => $b))) {
                print_error('invalidboardid', 'jinotechboard');
            }
            if (!$course = $DB->get_record("course", array("id" => $board->course))) {
                print_error('coursemisconf');
            }

            if (!$cm = get_coursemodule_from_instance("jinotechboard", $board->id, $course->id)) {
                print_error('missingparameter');
            }
        } else {
            print_error('missingparameter');
        }

        require_course_login($course, true, $cm);

        $context = get_context_instance(CONTEXT_MODULE, $cm->id);

        $PAGE->set_context($context);


        if (!has_capability('mod/jinotechboard:view', $context)) {
            notice('cannotdeletepost', 'jinotechboard');
        }

	

        $db_userid = $DB->get_field('jinotechboard_contents', 'userid', array('id' => $contentId, 'board' => $b));

        if ($db_userid == $USER->id || has_capability("mod/jinotechboard:delete", $context)) {

            $DB->delete_records_select("jinotechboard_contents", "id=? and board=?", array($contentId, $b));

            $params = array(
                'objectid' => $content->id,
                'context' => $context,
                'other' => array(
                    'jinotechboardid' => $board->id,
                )
            );

            $event = \mod_jinotechboard\event\content_deleted::create($params);
            $event->add_record_snapshot('jinotechboard_contents', $content);
            $event->trigger();
                
            $msg = get_string('Deletionshavebeencompleted', 'jinotechboard');

            redirect("view.php?b=$b");


        } else {
            $msg = get_string('no_delete_permission', 'jinotechboard');

            redirect("view.php?b=$b&contentId=$contentId");
        }
        ?>

    </body>
</html>