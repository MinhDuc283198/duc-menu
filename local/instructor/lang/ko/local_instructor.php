<?php
$string['title_seo'] = '강사';
$string['title'] = '여러분의 한국어 공부를 책임질 master korean의 선생님을 소개합니다!';
$string['noteacher'] = '등록된 강사가 없습니다.';
$string['qa'] = '학습Q&A';
$string['charge'] = '담당 강좌';
$string['comment'] = '제자들에게 한마디';
$string['nocourse'] = '등록된 강좌가 없습니다.';