<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/instructor/lib.php');
$context = context_system::instance();

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('강사소개목록');

echo $OUTPUT->header();
$id = optional_param("id", 0, PARAM_INT);

$list = local_instructor_id($id);
$usercontext = context_user::instance($id);
$fs = get_file_storage();
$files = $fs->get_area_files($usercontext->id, 'local_lmsdata', 'teacher', $id, "", false);
if(!empty($files)){
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $usercontext->id . '/local_lmsdata/teacher/' . $id . '/' . $filename);
    }
}else{
    $path = $CFG->wwwroot . '/theme/oklassedu/pix/images/default_course.png';
}
?>  
<h4 class="pg-tit"><?php echo get_string('title','local_instructor')?></h4>

<div class="teacher-info clearfix">
    <div class="t-img">
        <img src="<?php echo $path?>" alt="<?php echo $list->firstname?> <?php echo get_string('teacher','local_course')?>" />
    </div>
    <div class="t-txt">
        <p><?php echo $list->lang_oneline?></p>
        <div class="t-nm"><strong><?php echo $list->firstname?></strong> <?php echo get_string('teacher','local_course')?></div>
        <script type="text/javascript">
            document.title = `Master Korean - <?php echo get_string('teacher','local_course')?> <?php echo $list->firstname?>`;
        </script>
        <div class="scrl-bx">
            <?php echo $list->lang_profile?>
        </div>
    </div>
</div>


<ul class="crs-tab teacher tab-event">
    <!-- tab event -->
    <li class="on" data-target=".data01"><a href="#"><?php echo get_string('charge','local_instructor')?></a></li>
    <li data-target=".data02"><a href="#"><?php echo get_string('qa','local_instructor')?></a></li>
    <li data-target=".data03"><a href="#"><?php echo get_string('comment','local_instructor')?></a></li>
</ul>

<div class="crs-tab-cont target-area">
    <div class="data01 on">
        <?php
        $courses = local_instructor_coruses($id);
        if($courses){
        ?>
        <ul class="crs-list">
            <?php 
            foreach($courses as $co){
                $course = local_course_class_id2($co->id);
                ?>
            <li>
                <div class="img">
                    <span class="ic-heart <?php echo $course->likes ? 'on' : ''; ?>" id="like<?php echo $course->id ?>" data-target="<?php echo $course->id ?>"><?php echo get_string('like', 'local_course') ?></span>
                    <a href="<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $course->id?>"><img src="<?php echo $course->img ?>" alt="<?php echo $course->lang_title?>"/></a>
                </div>
                <div class="txt" onclick="location.href='<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $course->id?>'">
                    <div><?php echo $course->lang_title?></div>
                    <p><span><?php echo $course->teacher_list ?> <?php echo get_string('teacher', 'local_course') ?></span><span><?php echo get_string('total', 'local_course') ?> <?php echo $course->lecturecnt?><?php echo get_string('class', 'local_course') ?></span></p>
                    <p class="t-gray"><?php echo get_string('courseperiod', 'local_course', $course->courseperiod) ?></p>
                    <?php 
                    $keywords = explode('#', $course->lang_keywords);
                    $keywords = array_filter($keywords);
                    if(!empty($keywords)){
                    ?>
                    <ul class="hash-area">
                        <?php foreach($keywords as $keyword){?>
                        <li data-title="<?php echo $keyword?>"><a href="#"><?php echo $keyword?></a></li>
                        <?php }?>
                    </ul>
                    <?php }?>
                </div>

                <div class="bt-area">
                    <?php 
                    if($course->button_color == ''){
                        $course->button_color = "blue";
                    }
                    ?>
                    <a href="<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $course->id?>" class="btns <?php echo $course->button_color ?>"><?php echo $course->button_txt ?></a>
                </div>

            </li>
            <?php }?>
        </ul>
        <?php }else{?>
        <div class="no-data">
            <div><?php echo get_string('nocourse','local_instructor');?></div>
        </div>
        <?php }?>
    </div>
    
    <div class="data02">
        <?php
        $courseid = array();
        $qatotal = 0;
        $courses = local_instructor_coruses($id,2);
        if($courses){
            foreach($courses as $course){
                $courseid[] = $course->coid;
            }
            $qalist = local_instructor_board_list($courseid, $page, $perpage)->list;
            $qatotal = local_instructor_board_list($courseid, $page, $perpage)->cnt;
        }
        
        ?>
        <div class="mg-bt10"><?php echo get_string('total','local_management',$qatotal)?></div>
        <table class="table m-block qalist">
            <thead>
                <tr>
                    <th width="7%"><?php echo get_string('number','local_management')?></th>
                    <th width="20%"><?php echo get_string('goodsname','local_management')?></th>
                    <th><?php echo get_string('title','local_management')?></th>
                    <th width="15%"><?php echo get_string('review:writer','local_management')?></th>
                    <th width="15%"><?php echo get_string('review:timecreated','local_management')?></th>
                    <th width="8%"><?php echo get_string('view:cnt','local_jinoboard')?></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($qalist){
                    $i = $qatotal;
                    foreach($qalist as $ql){
                        if ($ql->lev) {
                            if ($ql->lev <= JINOTECHBOARD_INDENT_DEFAULT) {
                                $step = $ql->lev;
                                $date_left_len = $step * 15;
                            } else {
                                $step = JINOTECHBOARD_INDENT_DEFAULT;
                                $date_left_len = $step * 15;
                            }

                            switch ($ql->lev) {
                                case 1 : $calcwidth = 15;
                                    break;
                                case 2 : $calcwidth = 30;
                                    break;
                                case 3 : $calcwidth = 60;
                                    break;
                                case 4 : $calcwidth = 90;
                                    break;
                                default : $calcwidth = 90;
                                    break;
                            }
                            $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                            $step_icon = '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="Reply" /> ';
                            $reply = '<span class="tb-reply">re</span>';
                        } else {
                            $date_left = 'style="width:calc(100% - 0px) !important;"';
                            $step_icon = '';
                            $reply = '';
                        }
                        $secret = ($ql->isprivate == 1) ? '<img src="'.$CFG->wwwroot.'/theme/oklassedu/pix/lock.png" width="15" height="15" alt="비공개" title="비공개">' : '';
                        $onclick = (($ql->isprivate != 1) || ($ql->isprivate == 1 && ($USER->id == $id || $USER->id == $ql->userid )) ) ? 'onclick="go_detail('.$ql->id.','.$page.','.$id.')"' : 'onclick="alert(\'비공개로 작성된 게시물입니다.\');return false;" ';
                        $contents = '';
                        $context = context_course::instance($ql->course);
                        if ($ql->isprivate && !has_capability('mod/jinotechboard:secretmanager', $context) && $USER->id != $ql->userid && $parentid != $USER->id && $USER->id) {
                            $contents = '<p class="thread-post-content">' . get_string('content:secret', 'mod_jinotechboard') . '</p>';
                        }
                ?>
                <tr>
                    <td class="m-hide"><?php echo $i--?></td>
                    <td class="text-left overflow"><?php echo $ql->lang_title?></td>
                    <td class="text-left overflow hasnew" <?php echo $date_left?> <?php echo $onclick?>><a href="#"><?php echo $reply?><?php echo $ql->title.$secret?></a><?php echo ($ql->timecreated + (86400*7))>time() ? '<span class="ic-new">N</span>' : '' ; ?></td>
                    <td><?php echo $ql->firstname?></td>
                    <td><?php echo date("Y.m.d",$ql->timecreated)?></td>
                    <td class="m-f-r"><span class="m-show inline"><?php echo get_string('view:cnt','local_jinoboard')?></span><?php echo $ql->viewcnt?></td>
                </tr>
                <?php }
                    }else{?>
                <tr><td colspan="6"><?php echo get_string('nocontent', 'mod_jinotechboard')?> </td></tr>
                <?php }?>
            </tbody>
        </table>
        <?php
        $page_params = array();
        $page_params['perpage'] = $perpage;
        $page_params['course'] = $wherecourse;
        $page_params['userid'] = $id;
        echo local_instructor_paging_bar($CFG->wwwroot . "/local/instructor/detail.php", $page_params, $qatotal, $page);
        ?>
    </div>

    <div class="data03">
        <div class="txt-area">
            <p class="t-gray">
                <?php echo $list->lang_comment?>
            </p>
        </div>
    </div>
</div>

<script>
function call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: '/local/instructor/ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}

function go_page(page,userid){
    var result = call_ajax('paging', {page:page,userid:userid}, false);
    if(result.status == "success"){
        $('.data02').html(result.html);
        $('.data02').append(result.paging);
    }
}
function go_detail(id,page,userid){
    var result = call_ajax('detail', {id:id,page:page,userid:userid}, false);
    if(result.status == "success"){
        $('.data02').html(result.html);
    }
}
function go_edit(id){
   var result = call_ajax('edit', {id:id}, false);
    if(result.status == "success"){
        $('.data02').html(result.html);
    } 
}
function go_delete(b,contentId,page,userid){
    var result = call_ajax('delete', {b:b,contentId:contentId,page:page,userid:userid}, false);
    if(result.status == "success"){
        go_page(page,userid)
    } 
}
$(window).load(function () {
    if ($("input[name=view_filemanager]").prop("checked")) {
        $("#fgroup_id_filemanager").show();
    } else {
        $("#fgroup_id_filemanager").hide();
    }
});
$("input[name=view_filemanager]").click(function () {
    alert("zz");
    if ($("input[name=view_filemanager]").prop("checked")) {
        $("#fgroup_id_filemanager").show();
    } else {
        $("#fgroup_id_filemanager").hide();
    }
});
</script>
<?php
echo $OUTPUT->footer();
?>


