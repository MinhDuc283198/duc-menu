<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">	
	<h3 class="text-center">기업정보</h3>
	<form class="mx-3" action="" method="post">
	<input type="text" name="commited" value="true" hidden>

<!-- start step 1	 -->
	<div id="step-1" class="d-block">
		<div class="d-flex">
    		<a href="#" class="btn btn-primary btn-block rounded-0 m-0 py-2">이메일로 시작하기</a>
    		<a href="#" class="btn btn-outline-primary btn-block rounded-0 m-0 py-2">이메일로 시작하기</a>
    	</div>
    	<div class="row pt-4">
    		<div class="col-4"  id="cinfo">
    			<img class="rounded-circle m-3"  src="../icon/visang.png">
    			<p class="rounded-circle"><i class="fas fa-camera"></i></p>
    		</div>
    		<div class="col-2">
    			<p class="mt-3">기업명</p>
    			<p>산업군</p>
    			<p>국가</p>
    		</div>
    		<div class="col-6">
    			<p class="mt-3"><b>비상교육</b></p>
    			<p><b>교과서 및 학습서적 출판업</b></p>
    			<p><b>대한민국</b></p>
    		</div>
    	</div>
    	<hr><p class="mx-3 text-right mb-0 pb-0">* 필수입력정보입니다.</p>
	
		<div class="form-row">
			<div class="form-group col-12">
				<label for="in-1"><b>기업 규모</b>*</label>
    			<input type="text" class="form-control rounded-0" id="in-1" placeholder="직원수 입력 ">
			</div>
            <div class="form-group col-12">
				<label for="in-2"><b>기업 홈페이지</b>  </label>
    			<input type="text" class="form-control rounded-0" id="in-2" placeholder="URL 입력  ">
			</div>
			<div class="form-group col-12">
				<label for="in-3"><b>페이스북 주소 </b></label>
    			<input type="text" class="form-control rounded-0" id="in-3" placeholder="URL 입력">
			</div>
            <div class="form-group col-12">
                  <label for="select-1"><b>지역</b> *</label>
                  <select id="select-1" class="form-control">
                    <option selected>지역 선택 </option>
                    <option>...</option>
                  </select>
            </div>
            <div class="form-group col-12">
				<label for="in-4"><b>주소</b> * </label>
    			<input type="text" class="form-control rounded-0" id="in-4" placeholder="Vinhome central park . Park 6A 208 nguyễn hữu cảnh phường 22 qu">
			</div>
        </div>
        <div class="ml-4 custom-control custom-checkbox nav justify-content-end pr-2">
            <input type="checkbox" class="custom-control-input rounded-0" id="chkinfo">
            <label class="custom-control-label" for="chkinfo">부적절한 내용이 포함된 경우 통지없이 수정되거나 삭제될 수 있으며, 서비스 이용이 제한될 수  있습니다. </label>
        </div>
		 <div class="row mt-4">
        	<div class=" col-12 nav justify-content-center px-3">
        		<a id="info-next" class="btn btn-primary btn-lg rounded-0 text-white">저장</a>
            </div>
        </div>
	</div>
	
<!-- start step 2	 -->
	<div id="step-2" class="d-none">
		<div class="d-flex mb-3">
    		<a href="#" class="btn btn-outline-primary btn-block rounded-0 m-0 py-2">이메일로 시작하기</a>
    		<a href="#" class="btn btn-primary btn-block rounded-0 m-0 py-2">이메일로 시작하기</a>
    	</div>
    	
		<p class="mx-3 text-right mb-0 pb-0">* 필수입력정보입니다.</p>
		<div class="form-row">
			<div class="form-group col-12">
				<label for="in-5"><b>기업 규모</b>*</label>
    			<input type="text" class="form-control rounded-0 d-inline" id="in-5" placeholder="직원수 입력 ">
    			<p class="d-inline">12/100</p>
			</div>
  			<div class="form-group col-11 txt-len">
                <label for="txt-1"><b>회사/서비스 소개</b>   *</label>
                <textarea class="form-control" id="txt-1" rows="1" maxlength="100">한줄 소개 입력(100byte) </textarea>
            </div>
            <div class="form-group col-1 txt-len-l">
            	<p>12/100</p>
            </div>
            <div class="form-group col-11 txt-len">
                <textarea class="form-control" id="txt-2" rows="4" maxlength="900">회사 및 서비스 소개 입력(900byte)  </textarea>
            </div>
            <div class="form-group col-1 txt-len">
            	<p >12/900</p>
            </div>
            
            <!-- New -->
            <div class="form-group col-12 my-0">
                <label for="txt-3"><b>우리 회사에 입사해야 하는 이유(최대 3가지 입력 가능)</b> * </label>
            </div>
            <div class="form-group col-1 txt-len">
            	<p>1.</p>
            </div>
            <div class="form-group col-11 txt-len">
                <textarea class="form-control" id="txt-3" rows="1" maxlength="100"></textarea>
            </div>
            <div class="form-group col-1 txt-len">
            	<div class="len" >2.</div>
            </div>
            <div class="form-group col-11 txt-len">
                <textarea class="form-control" id="txt-4" rows="1" maxlength="100"></textarea>
            </div>
            <div class="form-group col-1 txt-len">
            	<p>3.</p>
            </div>
            <div class="form-group col-11 txt-len">
                <textarea class="form-control" id="txt-5" rows="1" maxlength="100"></textarea>
            </div>
            
             <!-- New -->
            <div class="form-group col-11 txt-len">
                <label for="txt-6"><b>복지혜택</b>* </label>
                <textarea class="form-control" id="txt-6" rows="4" maxlength="900">복지혜택 입력(900byte)  </textarea>
            </div>
            <div class="form-group col-1 txt-len">
            	<p>12/100</p>
            </div>
            
		</div>
		<div class="ml-4 custom-control custom-checkbox nav justify-content-end pr-2">
            <input type="checkbox" class="custom-control-input rounded-0" id="chkinfo-2">
            <label class="custom-control-label" for="chkinfo-2">부적절한 내용이 포함된 경우 통지없이 수정되거나 삭제될 수 있으며, 서비스 이용이 제한될 수  있습니다. </label>
        </div>
		 <div class="row mt-4">
        	<div class=" col-12 nav justify-content-center px-3">
        		<a id="info-pre" class="btn btn-outline-primary btn-lg rounded-0 mr-3">미리보기</a>
        		<button id="info-submit" type="submit" name="submit" value="true" class="btn btn-primary btn-lg rounded-0">저장</button>
            </div>
        </div>
	</div>
	</form>
	
</body>
<?php include '../component/foot-icl.php';?>
</html>

<?php if(!empty($_POST["commited"])){
    echo "<script>parent.closeDialog();</script>";
}?>
