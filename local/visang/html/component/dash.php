<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="deactive-background">
	<?php include '../component/nav-menu.php';?>
	
	<div class="mx-5 px-5 break-top py-5">
    	<h3 class="text-center"><b>진행중 채용공고 현황</b></h3>
    	<div class="d-flex justify-content-center">
    		<div class="dash-card text-center">
    			<h2><u>2</u></h2>
    			<hr><h3>채용공고</h3>
    		</div>
    		<div class="dash-card text-center">
    			<h2><u>22</u></h2>
    			<hr><h3>지원자</h3>
    		</div>
    		<div class="dash-card text-center">
    			<h2>5</h2>
    			<hr><h3>채용공고</h3>
    		</div>
    		<div class="dash-card text-center">
    			<h2 >421</h2>
    			<hr><h3>조회수 </h3>
    		</div>
    	</div>
    	
    	<h3 class="text-center pt-5"><b>기업 현황</b></h3>
    	<div class="d-flex justify-content-center">
    		<div class="dash-card text-center">
    			<h2 class="text-info">68</h2>
    			<hr><h3>구독자</h3>
    		</div>
    		<div class="dash-card text-center">
    			<h2 class="text-info">8,264</h2>
    			<p>(1일 평균 3.3)</p>
    			<hr><h3>총 조회수 </h3>
    		</div>
    		<div class="dash-card text-center">
    			<h2 class="text-info">924</h2>
    			<p>(1일 평균 8.8)</p>
    			<hr><h3>최근 30일 조회수 </h3>
    		</div>
    		<div class="dash-card text-center">
    			<h2 class="text-info">394</h2>
    			<p>6,704</p>
    			<hr><h3>기업 순위</h3>
    		</div>
    	</div>
    	
	</div>
	
	
	
	<?php include '../component/footer.php';?>
</body>
<?php include '../component/foot-icl.php';?>
</html>