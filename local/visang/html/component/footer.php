<footer class="footer-area">
<div class="row mx-0">
	<div class="col-lg-2"></div>
	<div class="col-lg-8 col-md-12">
    	<div class="row">
    		<div class="col-lg-3 col-md-3 mb-4">
    			<img src="../icon/ft_logo_job.png" style="width: 100%;" alt="">
    		</div>
    		<div class="col-lg-9 col-md-9 mb-5 jino-info">
    			<div class="row ml-2 justify-content-start">
                	<a href="#" class="btn btn-sm btn-link mx-2 mb-4 text-white">서비스소개</a>
                	<a href="#" class="btn btn-sm btn-link mx-2 mb-4 text-white">이용약관</a>
                	<a href="#" class="btn btn-sm btn-link mx-2 mb-4 text-white">개인정보 처리방침</a>
                	<a href="#" class="btn btn-sm btn-link mx-2 mb-4 text-white">고객지원</a>
                	<a href="#" class="btn btn-sm btn-link mx-2 mb-4 text-white">이러닝</a>
                </div>
    			<p>㈜비상교육　        서울시 구로구 구로동 170-10 대륭포스트타워 7차 20층</p>
    			<p>대표자명 : 양태회 사업자 등록 번호 : 119-85-22853 통신 판매업 신시 번호 : 2013- 서울 구로 -0373</p>
    			<p>고객 센터 : 1800-8674 대표 메일 : mastertopik@visang.com</p>
    			<p>COPYRIGHT(C) (주)비상교육 ALL RIGHTS RESERVED.</p>
    		</div>
    		
    	</div>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="row py-4 mx-0"  id="footer-dialog"> 
	<div class="col">
		<div class="d-flex justify-content-end"><button class="btn btn-outline-light mr-4" onclick="closeAction('footer-dialog');"><i class="fas fa-times"></i></button></div>
        <div class="d-flex justify-content-center">
        	<h3 class="m-2">Master Korean Jobs 에서 취업 성공하기!</h3>
        	<a href="../job/" class="btn btn-outline-light" role="button" aria-pressed="true">&ensp;자세히 보기 &ensp;<i class="fas fa-chevron-right"></i></a>
        </div>
	</div>
</div>
</footer>
<!-- ================ End footer Area ================= -->

<!-- Scroll to Top Button-->

<a class="scroll-to-top rounded-0 mr-5" href="#page-top"> 
    <i class="fas fa-angle-up"><br>TOP</i>
</a>