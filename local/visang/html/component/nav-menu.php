
<?php 
$fisrtLogin = false;
?>
<p style="margin: 0 0; padding: 0 0; display: hidden; top: 0;" id="page-top"></p>
<header class="header_area position-fixed">
	<div class="main_menu">
		<div class="menu-head">
			<div class="mx-4 row d-flex justify-content-end">
				<a href="#"><img src="../icon/fb.jpg" style="height: 24px; padding: 3px 3px 0 0;" alt=""></a>
    			<a href="#"><img src="../icon/youtube.png" style="height: 24px; padding-top: 3px;" alt=""></a>
        		<a class="divider">|<a>
        		<a href="#">고객센터</a>
        		<a class="divider">|<a>
        		<a href="#">기업서비스</a>
        		<a class="divider">|<a>
        		<a href="#"> 한국어강좌</a>
        		
        		<a class="ml-4" href="#"> VN</a>
        		<a class="divider">.<a>
        		<a href="#"> KO</a>
        		<a class="divider">.<a>
        		<a href="#"> EN</a>
        	</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-light border-top border-bottom border-dark p-0">
			<div class="container p-0" style="max-width: 100%;">
				<a class="navbar-brand logo_h ml-5" href="../home/"><img
					src="../icon/vslogo.png" style="height: 40px;" alt=""></a>
				<button class="navbar-toggler mr-4" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>		
					<?php if(isset($_COOKIE["USER_LOGINED"])){
					    if($_COOKIE["USER_LOGINED"])
					{?>
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
    					<ul class="nav navbar-nav menu_nav text-lg-left">
    						<li class="nav-item"><a class="nav-link" href="../company/">기업</a></li>
    						<li class="nav-item ml-5"><a class="nav-link" href="../job/notify.php"><b>채용공고&ensp;<i class="fas fa-chevron-down"></i></b></a></li>
    						<li class="nav-item ml-5"><a class="nav-link" href="../job/apply.php"><b>지원자&ensp;<i class="fas fa-chevron-down"></i></b></a></li>
    						<li class="nav-item ml-5"><a class="nav-link" href="../company/"><b>기업정보&ensp;<i class="fas fa-chevron-down"></i></b></a></li>
    						<li class="nav-item ml-5"><a class="nav-link" href="#"><b>커뮤니티&ensp;<i class="fas fa-chevron-down"></i></b></a></li>
    					</ul>
    				
    					<div class="nav-right text-lg-right py-4 py-lg-0">
    					<a href="../search/" class="btn btn-sm btn-link">
    					<i class="fa fa-search ml-2" style="font-size: 24px; padding: 0 16px 0 0;"></i></a>
    					
    					<!-- User logined notification-->
						<div class="nav-full-h as-pointer px-3 dropdown" id="userAlert">
                           <a  class="btn btn-sm btn-link">
    							<i class="far fa-bell ml-2" style="font-size: 24px; padding: 0 16px 0 0;"></i>
                           </a>
                           <a  style="margin: -40px 0 0 -40px !important; position: relative; top: -10px;">
                           		<span class="badge badge-success badge-counter">90+</span>
                           </a>
                       	</div>
                       	
                       	<!-- User logined action-->
					 	<div class="nav-full-h bg-primary as-pointer px-3 dropdown" id="userAction">
					 		<img class="rounded-circle" src="../icon/fb.jpg" style="height: 24px; width: 24px" alt="">
					 		<a href="#" class="btn btn-link btn-sm text-white">안녕하세요. 김비상님^^</a>
					 	</div>
					<?php 
    					include '../user/alert.php'; 
        					if($fisrtLogin){
        					    echo "<script type='text/javascript'>parent.openShareInfoAgreement();</script>";
        					}
					    }
					} else {?>
					<div class="collapse navbar-collapse offset"
    					id="navbarSupportedContent">
    					<ul class="nav navbar-nav menu_nav text-lg-left">
    						<li class="nav-item ml-5"><a class="nav-link" href="../job/">채용공고&ensp;<i class="fas fa-chevron-down"></i></a></li>
    						<li class="nav-item ml-5"><a class="nav-link" href="../company/">기업&ensp;<i class="fas fa-chevron-down"></i></a></li>
    					</ul>
				
					<div class="nav-right text-lg-right py-4 py-lg-0">
					<a href="../search/" class="btn btn-sm btn-link"><i class="fa fa-search ml-2" style="font-size: 24px; padding: 0 16px 0 0;"></i></a>
						<div class="nav-full-h"><a href="#" class="btn btn-primary btn-sm rounded-0 text-white" onclick="openSigninAction();">기업회원 가입</a></div>
						<div class="nav-full-h"><a href="#" class="btn btn-outline-primary btn-sm rounded-0 nv-btn" onclick="openSignupAction();">기업회원 로그인</a></div>
					<?php } ?>			        
					</div>
				</div>
			</div>
		</nav>
	</div>
</header>

<div class="modal fade" id="noTitleDialog" tabindex="-1" role="dialog" aria-labelledby="noTitleDialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<iframe id="noTitleDialogIframe" style="width: 100%;" scrolling="no">
      	</iframe>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="titleDialog" tabindex="-1" role="dialog" aria-labelledby="titleDialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title justify-content-center" id="dialogTitle">Modal title</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<iframe id="titleDialogIframe" style="width: 100%;" scrolling="no">
      	</iframe>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<script>
//* Navbar Fixed  
// var nav_offset_top = $('header').height() + 50; 
// var navbarFixed = function(){
//   if ( $('.header_area').length ){ 
//       $(window).scroll(function() {
//           var scroll = $(window).scrollTop();   
//           if (scroll >= nav_offset_top ) {
//               $(".header_area").addClass("navbar_fixed");
//           } else {
//               $(".header_area").removeClass("navbar_fixed");
//           }
//       });
//   };
// };
// navbarFixed();
</script>

