
<!-- Please give me a list of companies, I will display it. Thanks. -->

<!-- Bellow is template data -->
<?php 
$comImg = "../icon/visang.png";
?>
<div class="row px-4">
<?php for($i=0; $i<20; $i++){?>
	<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 p-2 compa-card">
		<div class="job-card p-3 border border-dark" onclick="viewComInfo('Insert comID <?php echo $i?>');">
			<div class="job-img d-block" style="height: 120px;">
    			<img class="pt-3" alt="" src="<?php echo $comImg;?>" style="max-height: 120px;">
        	</div>
        	<h4><b>비상교육</b></h4>
        	<h6>2002년 1월 21일 설립된 18년 차 베테랑 교과서 및 학습서적 출판업체입니다.</h6>
        	<div class="row p-3 d-flex justify-content-between">
        		<a>호치민</a>
        		<a href="#" class="text-primary">3개의 채용공고 보기 <i class="fas fa-caret-right"></i></a>
        	</div>
		</div>
	</div>
<?php }?>
</div>
<div class="p-4">
	<a href="#" class="btn btn-outline-dark btn-lg btn-block" role="button" aria-pressed="true">더보기 
	<i class="fas fa-chevron-down"></i></a>
	<!-- <button type="button" class="btn btn-outline-dark px-5">더보기</button> -->
</div>