

<?php 
$comImg = "../icon/visang.png";
?>
<div class="border border-dark mx-4">
	<!-- Start general info -->
    <div class="row mt-3">
    	<div class="col-3 border-right border-dark"><img alt="" src="<?php echo $comImg;?>" style="width: 100%;"></div>
    	<div class="col-6 mt-3">
    		<div class="pl-3 pb-2 d-flex justify-content-start">
        		<h4><b>비상교육</b></h4>
        	</div>
        	<div class="pl-3 pb-2 d-flex justify-content-start">
        		<i class="fas fa-map-marker-alt li-icon"></i>&ensp;
        		<h6>호치민</h6>
        	</div>
        	<div class="pl-3 pb-2 d-flex justify-content-start">
        		<i class="fas fa-cog li-icon"></i> &ensp; 
        		<h6>교과서 및 학습서적 출판업</h6>
        	</div>
        	<div class="pl-3 pb-2 d-flex justify-content-start">
        		<i class="fas fa-user-friends li-icon"></i> &ensp;
        		<h6>579명 (2019년 기준)</h6>
        	</div>
        	<div class="pl-3 pb-2 d-flex justify-content-start">
        		<img class="li-icon" alt="" src="../icon/Korea.png">&ensp;
        		<h6>대한민국</h6>
        	</div>
    	</div>
    	<div class="col-3 mt-3">
    		
    		<div class="px-0 pt-4 d-flex justify-content-center">
    			<a href="#" class="btn btn-outline-primary rounded-0" role="button" aria-pressed="true">관심정보 등록</a>
        	</div>
        	<div class="px-0 pt-4 d-flex justify-content-center">
        		<a class="px-2" href="#"><h1 class="text-primary"><i class="fas fa-external-link-alt"></i></h1></a>
        		<a class="px-2" href="#"><h1 class="text-primary"><i class="fab fa-facebook-square"></i></h1></a>
        	</div>
        	<div class="px-0 pt-4 d-flex justify-content-center">
        		<h4><i class="fas fa-eye"></i> 7,109 회</h4>
        	</div>
    	</div>
    	
    </div>
    <div class="row mx-4"><hr class="break-line" /></div>
    <!-- End general info -->
    <!-- Start Explan about this company -->
    <div class="row mx-4">
    <h3 class="col-12 pt-3"><b>비상은 당연한 것도 낯설게 보는 남다른 기상으로 새로운 글로벌 교육 문화를 창조합니다.</b></h3>
    <p class="col-12 pt-3">비상은 지난 20년간 혁신적인 도전과 성장을 거듭해왔으며 <한끝 국어>, <오투 과학>, <개념+유형> 과 같은 강의용 교재의 전형을 만들어 학원 교재 시장을 창출했다는 것과 <완자>와 같은 자율학습용 교재 시장 창출로 선생님 도움 없이도 완전 학습을 할 수 있도록 도왔다는 것을 성과로 꼽을 수 있겠습니다. 그리고 최근에는 전통적인 일방향의 교실 환경을 IT 기술을 활용하여 쌍방향 수업으로 변화시키고 있다는 점을 말씀드리고 싶습니다. 이러한 도전과 성장의 가장 큰 동력은 끊임없는 개선에 있습니다.
    제품과 서비스 개발 후 지속적인 개선을 통해 부족한 점을 보완하고 현장의 목소리를 반영하는 것이 제품의 퀄리티 뿐 아니라, 소비자의 신뢰를 구축하는데 매우 중요한 역할을 했습니다. 결국, 해당 분야의 전형을 만들기 위한 경영 철학과 노력이 원동력이었다고 생각합니다.<br><br>
    
    비상은 '우리 시대에 바람직한 조직 문화'를 '내가 선택한 일과 조직에서 나와 우리가 일하는 의미와 즐거움을 높여주는 공동체의 정서, 프로세스, 시스템'으로 정의했습니다. 이 조직 문화가 완성되면 우리는 노동으로부터 소외되지 않고 자신이 선택한 일에 가치를 더하게 될 뿐 아니라 삶과의 일체감이 높아져 더 행복한 마음으로 활기찬 직장 생활을 누리게 될 것입니다. 행복한 직원이 만드는 제품과 서비스는 분명 고객도 행복하게 하리라 생각합니다.</p>
    </div>
    <!-- End Explan about this company -->
</div>


<!-- Start filter job of this company -->
<div class="row mx-4"><h3 class="col-12 pt-3"><b>진행중인 채용공고</b></h3></div>
<?php include '../job/jobList.php';?>
<!-- End filter job of this company -->

<!-- Start Benefits -->
<div class="row mx-4">
<h3 class="col-12 pt-3"><b>복지혜택</b></h3>
<p class="col-12 pt-3">비상교육 기업은 임직원들을 위해 지원을 아끼지 않습니다.<br>
- 우리 회사만의 특별한 복지! 명절선물/귀향비, 창립일선물, 생일선물/파티, 결혼기념일선물을 지급합니다.<br>
- 직원들의 자기계발을 존중합니다. 석식 제공, 사내동호회 운연, 사우회(경조사회) 음료제공(차, 카피), 우리사주제도를 운영합니다.<br>
- 직원 중심의 조직문화를 만들어갑니다. 수평적 조직문화에 앞장서며 전직원 자유복장입니다.<br>
- 최고의 복지는 휴가죠! 산전 후 휴가,육아휴직, 남성출산휴가, 보건휴가를 제공합니다.<br>
- 안정된 생활을 지원합니다. 건강검진, 의료비지원(본인), 직원대출제도, 각종 경조사 지원, 의료비지원(가족), 문화생활비를 지원합니다.<br>
- 직원들의 열정에 보답하고 싶습니다. 퇴직연금, 인센티브제, 장기근속자 포상, 우수사원포상, 퇴직금, 성과급, 휴일(특근)수당, 직책수당, 가족수당, 4대보험을 제공합니다.<br></p>
</div>
<!-- End Benefits -->
<div class="row mx-4"><hr class="break-line" /></div>
<!-- Start Map -->
<div class="row mx-4">
    <h3 class="col-12 pt-3"><b>위치</b></h3>
    <p class="col-12 pt-3">서울시 구로구 구로동 170-10 대륭포스트타워 7차 20층</p>
    <div class="col-12 d-block" id="map-container">
    	<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&q=Eiffel+Tower+Paris+France" 
    	width="800" height="400" frameborder="0" allowfullscreen id="comMap"></iframe>		
    </div>
</div>
<!-- End map -->

<script>
	$("#comMap").css("width",$("#map-container").width()+"px");
</script>