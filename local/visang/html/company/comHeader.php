
<!-- Start locate menu session -->
<div class="row mx-4 break-top justify-content-start">
    <h3 class="m-0">산업군 전체</h3>
    <h3 class="m-0 ml-2 as-pointer" data-toggle="modal" data-target="#menu-modal"><i class="fas fa-bars"></i></h3>
</div>
<!-- End locate menu session -->

<!-- Start filter session -->
<div class="d-block d-flex justify-content-end">
	<h5><b>인기순</b>&ensp;</h5>
	<h5> &ensp;&ensp;|&ensp;&ensp; 기업명순&ensp;</h5>
	<h5><i class="fas fa-caret-square-down"></i>&ensp;</h5>
</div>
<!-- End filter session -->

<!-- Start note session -->
<?php if($hasNote){?>
<div class="row m-4 justify-content-center">
    <h3 class="m-0"><b>찾고 있는 채용공고가 없습니다.</b></h3>
</div>
<div class="row m-4 justify-content-center">
    <h3 class="m-0"><b>현재 인기 있는 채용공고를 확인하세요.</b></h3>
</div>
 <?php } ?>
<!-- End note session -->