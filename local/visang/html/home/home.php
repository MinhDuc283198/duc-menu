
<!-- Start slider photo -->
<div id="slider-home" class="carousel slide break-top" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#slider-home" data-slide-to="0" class="active"></li>
		<li data-target="#slider-home" data-slide-to="1"></li>
		<li data-target="#slider-home" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="../icon/slider.png" alt="First slide">
			<div class="carousel-caption d-none d-md-block txt-on-slider">
                <h2>나에게 딱 맞는 회사 ○○○ 에서 찾아드립니다.</h2>
                <form>
                    <div class="d-none d-md-block">
                        <input type="text" class="search-key" placeholder="맞춤검색">
                        <button class="btn search-action" type="button">
                            	<i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
                <p># 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 </p>
            </div>
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="../icon/slider.png" alt="First slide">
			<div class="carousel-caption d-none d-md-block txt-on-slider">
                <h2>나에게 딱 맞는 회사 ○○○ 에서 찾아드립니다.</h2>
                <form>
                    <div class="d-none d-md-block">
                        <input type="text" class="search-key" placeholder="맞춤검색">
                        <button class="btn search-action" type="button">
                            	<i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
                <p># 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 </p>
            </div>
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="../icon/slider.png" alt="First slide">
			<div class="carousel-caption d-none d-md-block txt-on-slider">
                <h2>나에게 딱 맞는 회사 ○○○ 에서 찾아드립니다.</h2>
                <form>
                    <div class="d-none d-md-block">
                        <input type="text" class="search-key" placeholder="맞춤검색">
                        <button class="btn search-action" type="button">
                            	<i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
                <p># 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 # 추천검색어 </p>
            </div>
		</div>
	</div>
	<a class="carousel-control-prev" href="#slider-home"
		role="button" data-slide="prev"> <img alt="" src="../icon/btn-previous.png"> 
		<span class="sr-only">Previous</span></a> 
	<a class="carousel-control-next" href="#slider-home"
		role="button" data-slide="next"> <img alt="" src="../icon/btn-next.png"> 
		<span class="sr-only">Next</span>
	</a>
</div>
<!-- End slider -->

<div class="group">
    <div class="green-bx">
        <p class="m-0">
            <span><em>1:1 상담<br>문의</em></span>
            <span><em>0111-111-111 <span class="sm">09:00 ~ 18:00</span></em></span>
            <span><em>페이스북<br>상담</em></span>
        </p>
        <p class="m-0">
            <strong>Master Korean의 다양한<br>콘텐츠를 경험하세요.</strong>
            <a href="#" class="ic-facebook">페이스북</a>
            <a href="#" class="ic-youtube">유튜브</a>
        </p>
    </div>
</div>


<!-- Start new session -->
<div class="row mx-0">
	<div class="col-lg-2"></div>
    <div class="col-lg-8">
    	<!-- Start second menu -->
        <div class="row my-4 justify-content-center">
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무1</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무2</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무3</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무4</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무5</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무6</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무7</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무8</a>
        	<a href="#" class="btn btn-md btn-light m-1">직군/직무 전체보기</a>
        </div>
        
        <div class="row my-4 justify-content-center">
        	<h3 class="m-2">회원가입/로그인 후 맞춤 채용정보 추천받으세요.</h3>
        	<button class="btn btn-outline-dark"> &ensp;한국어 강좌 &ensp;<i class="fas fa-chevron-right"></i> </button>
        </div>
        <div class="row"><hr class="break-line" /></div>
        <!-- End second menu -->
        
        <!-- Start video list 1-->
        <div class="row">
        	<h3 class="col-12 p-2 title"><b>인기 </b>채용공고 </h3>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                    <p><i class="fas fa-heart active-heart-blue"></i> 20</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                	<p><i class="fas fa-heart active-heart-blue"></i> 10</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                	<p><i class="far fa-heart active-heart-blue"></i> 0</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                	<p><i class="far fa-heart active-heart-blue"></i> 0</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        </div>
        <!-- End video list 1-->
    </div>
    <div class="col-lg-2"></div>
</div>

<!-- Start new session -->
<div class="row col-dark mx-0">
	<div class="col-lg-2"></div>
	<div class="col-lg-8 col-md-12">
		<!-- Start video list 2-->
		<br><div class="row">
    		<h3 class="col-12 p-2 title"><b>신규 </b>채용공고   </h3>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                    <p><i class="fas fa-heart active-heart-blue"></i> 20</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                	<p><i class="fas fa-heart active-heart-blue"></i> 10</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                	<p><i class="far fa-heart active-heart-blue"></i> 0</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        	<div class="col-6 col-lg-3 card video-card">
                <img class="card-img-top" src="../icon/thub.png" alt="Include Image">
                <div class="d-flex justify-content-between video-time">
                	<p><i class="fas fa-heart active-heart-blue"></i> 100</p>
    				<p>~2019.12.04</p>
                </div>
                <div class="card-body video-thub">
                	<h6>이러닝 마케팅팀 채용(마케팅  및 기획 업무)</h6>
                	<p>비상교육</p>
                </div>
        	</div>
        </div><br><br>
		<!-- End video list 2-->
	</div>
	<div class="col-lg-2"></div>
</div>

<!-- Start new session -->
<div class="row mx-0">
	<div class="col-lg-2"></div>
	<div class="col-lg-8 col-md-12">
		<div class="d-flex justify-content-between mt-5">
        	<h3 class="col-10 p-2 title"><b>나를 한 단계 업그레이드 </b>시켜줄 한국어 과정  </h3>
    		<a href="#" style="font-size: 40px; margin: 0 0 0 -40px;"><i class="far fa-plus-square"></i></a>
        </div>
		<div class="row"><hr class="break-line" /></div>
		<div class="row">
    		<div class="col-lg-3 col-md-6">
    			<video width="100%" controls>
                  <source src="../video/video.mkv" type="video/mp4">
                </video>
    		</div>
    		<div class="col-lg-3 col-md-6">
    			<h5><b>TOPIKII (5,6급) - 문법편</b></h5>
    			<h6>TOPIK 5.6급 목표 달성을 위한 문법  유형 공략!. </h6>
    			<p class="pt-3" style="font-size: 12px;">수강후기 내용 수강후기 내용 수강후기  내용 수강후기 내용 수강후기 내용 수강후..</p>
    		</div>
    		
    		<div class="col-lg-3 col-md-6">
    			<video width="100%" controls>
                  <source src="../video/video.mkv" type="video/mp4">
                </video>
    		</div>
    		<div class="col-lg-3 col-md-6">
    			<h5><b>TOPIKII (5,6급) - 문법편</b></h5>
    			<h6>TOPIK 5.6급 목표 달성을 위한 문법  유형 공략!. </h6>
    			<p class="pt-3" style="font-size: 12px;">수강후기 내용 수강후기 내용 수강후기  내용 수강후기 내용 수강후기 내용 수강후..</p>
    		</div>
		</div>
		
		<div class="row"><hr class="break-line" /></div>
	</div>
	<div class="col-lg-2"></div>
</div>

<!-- Start new session -->
<div class="row mx-0">
	<div class="col-lg-2"></div>
	<div class="col-lg-8 col-md-12">
    	<div class="row border voice">
        	<div class="col mb-3 mt-4 d-flex justify-content-start">
        		<button class="btn btn-outline-dark audio-play"><i class="fas fa-volume-up"></i></button>
            	<p> &ensp;&ensp;&ensp; 제 67회 TOPIK 시험 접수 안내</p>
        	</div>
        	<div class="col mb-3 mt-4 d-flex justify-content-end">
        		<p><b>관리자</b> </p>
            	<p>&ensp;&ensp;&ensp; | &ensp;&ensp;&ensp; 2019.12.05</p>
        	</div>
    	</div>
	</div>
	<div class="col-lg-2"></div>
</div>