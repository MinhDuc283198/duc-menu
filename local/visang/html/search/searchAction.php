
<?php 

if(!empty($_GET["keySearch"])){
    $keySearch = $_GET["keySearch"];
}else if(!empty($_POST["keySearch"])){
    $keySearch = $_POST["keySearch"];
}else{
    $keySearch = "";
}

$resultCount = 6; 
?>



<div class="row mt-5 pt-5 mx-4">
	<div class="col-12 d-flex justify-content-center">
		<form class="d-block" action="../search/index.php" style="width: 80%;">
    		<div class="input-group mb-3">
                <div class="input-group-prepend">
                	<span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                </div>
                <input type="text" class="form-control" name="keySearch" placeholder="기획" aria-label="기획" aria-describedby="basic-addon1">
                <input type="submit" class="btn btn-outline-dark btn-sm ml-2 px-4" value="검색"> 
            </div>
        </form>
	</div>
	
</div>
<h3 class="pt-3 mx-4"><b><?php echo $keySearch;?></b></h3>
<h3 class="mx-4 mb-3"><b>기업</b></h3>

<?php if($resultCount < 1){?>
<div class="row mx-4" style="height: 600px;">
	<div class="col-12 mx-4 pt-5 text-center">
    	<h2>“기획” 에 대한 검색결과가 없습니다.</h2>
    	<p>검색어를 맞게 입력했는지 확인하시고 다시 검색해주세요.</p>
    </div>
</div>
<!-- /* Please repair list of Job or companies to display */ -->
<?php }else{ 
include '../company/comList.php';
?>
<h3 class="mx-4 mb-3"><b>채용공고</b></h3>
<?php 
include '../job/jobList.php';
}?>