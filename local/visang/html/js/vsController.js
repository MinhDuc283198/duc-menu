/**
 * 
 */

var $ROOTDIR = document.location.hostname + "/vs/";

var resizeDialog = function(width){
	
	if (window.matchMedia('(max-width: 575px)').matches || window.innerWidth < 576) {
        if(width > 574){
        	$(".modal-dialog").css("max-width","100%");
        }else{
        	$(".modal-dialog").css("max-width",width + "px");
        }
    }else if(window.matchMedia('(max-width: 767px)').matches || window.innerWidth < 768) {
    	if(width > 766){
        	$(".modal-dialog").css("max-width","100%");
        }else{
        	$(".modal-dialog").css("max-width",width + "px");
        }
    }else if(window.matchMedia('(max-width: 991px)').matches || window.innerWidth < 992) {
    	if(width > 990){
        	$(".modal-dialog").css("max-width","100%");
        }else{
        	$(".modal-dialog").css("max-width",width + "px");
        }
    }else if(window.matchMedia('(max-width: 1199px)').matches || window.innerWidth < 1200) {
    	if(width > 1198){
        	$(".modal-dialog").css("max-width","100%");
        }else{
        	$(".modal-dialog").css("max-width",width + "px");
        }
    }else{
    	$(".modal-dialog").css("max-width",width + "px");
    }
}

var closeAction = function(obj){
	$("#"+obj).addClass("d-none");
}

var openNoTitleDialog = function(){
	$("#noTitleDialog").modal('toggle');
	$("#noTitleDialog").modal('show');
}

var openTitleDialog = function(){
	$("#titleDialog").modal('toggle');
	$("#titleDialog").modal('show');
}

var closeDialog = function(){
	$("#noTitleDialog").modal('hide');
	$("#titleDialog").modal('hide');
}

var changeDialogHieght = function(h){
	$("#titleDialogIframe").css("height",h+"px");
	$("#noTitleDialogIframe").css("height",h+"px");
}

var openDialog = function(obj, url) {
    var $iframe = $('#' + obj + 'Iframe');
    $iframe.attr('src',url); 
}

var openSigninAction = function(){
	openDialog("noTitleDialog","../user/signinSignup.php?login=true");
	$("#noTitleDialogIframe").css("height","250px");
	resizeDialog(500);
	openNoTitleDialog();
}

var openSignupAction = function(){
	openDialog("noTitleDialog","../user/signinSignup.php?signup=true");
	$("#noTitleDialogIframe").css("height","550px");
	resizeDialog(500);
	openNoTitleDialog();
}

var openCinfoAction = function(){
	openDialog("noTitleDialog","../component/cinfo.php");
	$("#noTitleDialogIframe").css("height","900px");
	resizeDialog(600);
	openNoTitleDialog();
}

var openJobPostAction = function(){
	openDialog("noTitleDialog","../job/jobPost.php");
	$("#noTitleDialogIframe").css("height","1470px");
	resizeDialog(900);
	openNoTitleDialog();
}

var openJobChoiceAction = function(){
	openDialog("titleDialog","../job/jobChoice.php");
	$("#dialogTitle").html("직군/직무 선택 ");
	$("#titleDialogIframe").css("height","500px");
	resizeDialog(500);
	openTitleDialog();
}

var openShareInfoAgreement = function(){
	setTimeout(closeDialog, 10);
	setTimeout(function(){
		openDialog("titleDialog","../user/shareInfoAgreement.php");
		$("#dialogTitle").html("정보공유 동의");
		$("#titleDialogIframe").css("height","500px");
		resizeDialog(500);
		openTitleDialog();
	}, 11);
}

var openInterestSetting = function(){
	var title1 = '관심정보 설정 ';
	var title2 = ' 관심정보를 설정하고 나에게 꼭 맞는 채용정보를 추천 받으세요.';
	openDialog("titleDialog","../job/interest.php?step=1");
	var html = '<p class="dialog-t1 d-inline">' + title1 + 
				'</p><p class="dialog-t2 d-inline">'+title2+'</p>';
	$("#dialogTitle").html(html);
	$("#titleDialogIframe").css("height","500px");
	resizeDialog(900);
	openTitleDialog();
}

var openApplayJob = function(jobID){
	location.href = "../job/applyInfo.php?ID="+jobID;
}

var checkJIS = function(btn){
	if($("#"+btn).hasClass("current")){
		$("#"+btn).removeClass("current");
		$("#v"+btn).val("0");
	}else{
		$("#"+btn).addClass("current");
		$("#v"+btn).val("1");
	}
}

var checkJISL = function(btn, val){
	if($("#"+btn).hasClass("current")){
		$("#"+btn).removeClass("current");
		$("#JISL").val("-1");
	}else{
		$(".jisl").removeClass("current");
		$("#"+btn).addClass("current");
		$("#JISL").val(val);
	}
}

var viewJobInfo = function(jobID){
	//alert(jobID);
	window.location.href = '../job/info.php?jobID='+jobID;
}

var viewComInfo = function(comID){
	//alert(comID);
	window.location.href = '../company/info.php?jobID='+comID;
}

var viewUserProfile = function(obj, view){
	$("#usr-iframe").attr('src',view); 
	$(".list-group-item").removeClass("active");
	obj.classList.add("active");
}
