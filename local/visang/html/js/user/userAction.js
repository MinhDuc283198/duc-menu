/**
 * 
 */

var checkSignupAgree = function(){
	if($("#chkAgree1").is(":checked") && $("#chkAgree2").is(":checked")){
		$("#checkAgree").removeClass("disabled");
	}else{
		$("#checkAgree").addClass("disabled");
	}
}

var checkAgreeShareInfo = function(){
	if($("#chkAgreeShareInfo").is(":checked")){
		$("#checkAgreeShareInfo").prop('disabled', false);
	}else{
		$("#checkAgreeShareInfo").prop('disabled', true);
	}
}

$('#chkAgree1').click(checkSignupAgree);
$('#chkAgree2').click(checkSignupAgree);
$('#chkAgreeShareInfo').click(checkAgreeShareInfo);

$('#checkAgree').click(function(){
	if(!$("#checkAgree").hasClass("disabled")){
		$("#signupAgreement").addClass("d-none");
		$("#signupAction").removeClass("d-none");
		parent.changeDialogHieght(330);
//		parent.closeDialog();
	}
});

$('#userAction').click(function(){
	$('#userActionMenu').removeClass("d-none");
	$('#userActionMenu').addClass("d-block");
});

$('#userActionMenu').hover(function(){
	
}, function(){
	$('#userActionMenu').removeClass("d-block");
	$('#userActionMenu').addClass("d-none");
});

$('#userAction').hover(function(){
	$('#userAlertList').removeClass("d-block");
	$('#userAlertList').addClass("d-none");
}, function(){
	
});

$('#userAlert').click(function(){
	$('#userAlertList').removeClass("d-none");
	$('#userAlertList').addClass("d-block");
});

$('#userAlertList').hover(function(){
	
}, function(){
	$('#userAlertList').removeClass("d-block");
	$('#userAlertList').addClass("d-none");
});

$('#userAlert').hover(function(){
	$('#userActionMenu').removeClass("d-block");
	$('#userActionMenu').addClass("d-none");
}, function(){
	
});

$('#info-next').click(function(){
	$('#step-1').removeClass("d-block");
	$('#step-1').addClass("d-none");
	$('#step-2').addClass("d-block");
	parent.changeDialogHieght(930);
});

$('#info-pre').click(function(){
	$('#step-2').removeClass("d-block");
	$('#step-2').addClass("d-none");
	$('#step-1').addClass("d-block");
	parent.changeDialogHieght(900);
});

$("#inter-tab1").click(function(){
	$("#inter-tab1").removeClass("btn-outline-primary");
	$("#inter-tab1").addClass("btn-primary");
	$("#inter-tab2").removeClass("btn-primary");
	$("#inter-tab2").addClass("btn-outline-primary");
	$("#inter-tab-1").removeClass("d-none");
	$("#inter-tab-1").addClass("d-block");
	$("#inter-tab-2").removeClass("d-block");
	$("#inter-tab-2").addClass("d-none");
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
});

$("#inter-tab2").click(function(){
	$("#inter-tab2").removeClass("btn-outline-primary");
	$("#inter-tab2").addClass("btn-primary");
	$("#inter-tab1").removeClass("btn-primary");
	$("#inter-tab1").addClass("btn-outline-primary");
	$("#inter-tab-2").removeClass("d-none");
	$("#inter-tab-2").addClass("d-block");
	$("#inter-tab-1").removeClass("d-block");
	$("#inter-tab-1").addClass("d-none");
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
});


