<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Job</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">
<form action="" method="post">
	<h3 class="text-center"><b>현재 비밀번호<b></b></h3>
	<input type="text" name="commited" value="true" hidden>
    <p class="mx-3 text-right mb-0 pb-0">* 필수입력정보입니다.</p>
    <div class="form-row px-3">
    	<div class="form-group col-11 txt-len">
            <label for="txt-1"><b>채용공고명 </b>   *</label>
            <textarea class="form-control" id="txt-1" rows="1" maxlength="40">채용공고명 입력(40byte)   </textarea>
        </div>
        <div class="form-group col-1 txt-len-l">
        	<p>12/40</p>
        </div>
        <div class="form-group col-12 ">
            <label class="d-inline"><b>직군/직무  </b>   *</label>
            <a href="#" id="btn" class="btn btn-outline-primary rounded-0 mr-3 d-inline"><i class="fas fa-plus"></i> 추가</a>
        </div>
        <div class="form-group col-12 txt-len">
            <label for="txt-2"><b>담당업무</b>   *</label>
            <textarea class="form-control" id="txt-2" rows="4" maxlength="900">상세 담당업무 입력   </textarea>
        </div>
        <div class="form-group col-12 txt-len">
            <label for="txt-3"><b>자격요건   </b>   *</label>
            <textarea class="form-control" id="txt-3" rows="4" maxlength="900">자격요건 입력(경력여부, 학력, 연령 등)    </textarea>
        </div>
        <strong class="pl-2 pb-3">우대사항</strong>
        <div class="form-group col-12 d-flex justify-content-start">
            <label class="d-block"><b>한국어 활용 수준 </b></label>
            <select id="select-1" class="form-control rounded-0 ml-3" style="width: 50%;">
                <option selected>일상대화 가능 </option>
                <option>...</option>
            </select>
        </div>
        <div class="form-group col-12 txt-len">
            <textarea class="form-control" id="txt-4" rows="4" maxlength="900">기타 우대사항 입력(외국어, 자격증 등) </textarea>
        </div>
        <strong class="pl-2 pb-3">[근무조건]</strong>
        <div class="form-group col-12">
            <label for=""><b>근무형태 </b>   *</label>
            <div class="form-row mx-4 d-flex justify-content-start custom-chk">
            	<div class="form-check col-2 ">
                    <input type="checkbox" class="form-check-input" id="row1">
                    <label class="form-check-label d-inline ml-1" for="row1">정규직</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row2">
                    <label class="form-check-label d-inline ml-1" for="row2">계약직</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row3">
                    <label class="form-check-label d-inline ml-1" for="row3">아르바이트</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row4">
                    <label class="form-check-label d-inline ml-1" for="row4">인턴</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row5">
                    <label class="form-check-label d-inline ml-1" for="row5">프리랜서</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row6">
                    <label class="form-check-label d-inline ml-1" for="row6">파트</label>
                </div>
                
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row7">
                    <label class="form-check-label d-inline ml-1" for="row7">위촉직</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row8">
                    <label class="form-check-label d-inline ml-1" for="row8">파견직</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row9">
                    <label class="form-check-label d-inline ml-1" for="row9">전임</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row10">
                    <label class="form-check-label d-inline ml-1" for="row10">병역특례</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row11">
                    <label class="form-check-label d-inline ml-1" for="row11">교육생</label>
                </div>
                <div class="form-check col-2">
                    <input type="checkbox" class="form-check-input" id="row12">
                    <label class="form-check-label d-inline ml-1" for="row12">해외취업</label>
                </div>
            </div>
		</div>
        <div class="form-group col-12">
            <label class="d-block mt-3"><b>연봉 </b>   *</label>
            <div class="d-flex justify-content-start money">
            	<input type="text" class="form-control rounded-0" id="in-from" placeholder="">
            	<button type="button" class="btn btn-link text-dark disabled"> ~ </button>
            	<input type="text" class="form-control rounded-0" id="in-to" placeholder="">
            	<select id="select-2" class="form-control rounded-0">
                    <option selected>VND</option>
                    <option>...</option>
                </select>
                <div class="form-check col-2 custom-chk">
                    <input type="checkbox" class="form-check-input" id="in-check">
                    <label class="form-check-label d-inline ml-1" for="in-check">면접 시 협의 </label>
                </div>
            </div>
        </div>
        <div class="form-group col-12 txt-len">
            <label class="d-block"><b>지역선택  </b>   *</label>
            <select id="select-3" class="form-control rounded-0 mb-2" style="width: 50%;">
                <option selected>지역선택</option>
                <option>...</option>
            </select>
            <textarea class="form-control" id="txt-5" rows="3" maxlength="900">상세 주소 입력</textarea>
        </div>
        <div class="form-group col-12 txt-len">
            <label class="d-block"><b>근무시간 </b>   *</label>
            <textarea class="form-control" id="txt-6" rows="3" maxlength="900">근무 요일 및 시간 입력(ex. 월~금(주5일) 09:00~18:00, 협의 가능, 탄력근무제 등)</textarea>
        </div>
        <div class="form-group col-12">
            <label class="d-block"><b>접수기간 </b>   *</label>
            <div class="d-flex justify-content-start money">
            	<input type="date" class="form-control rounded-0" id="time-from" placeholder="URL 입력">
            	<button type="button" class="btn btn-link text-dark disabled"> ~ </button>
            	<input type="date" class="form-control rounded-0" id="time-to" placeholder="URL 입력">
            </div>
        </div>
    </div>
    <div class="form-check col-12 mx-4">
        <input type="checkbox" class="form-check-input" id="agree">
        <label class="form-check-label ml-1 small " for="agree">부적절한 내용이 포함된 경우 통지없이 수정되거나 삭제될 수 있으며, 서비스 이용이 제한될 수 있습니다. </label>
    </div>
    <div class="my-3 d-flex justify-content-center">
    	<button class="btn btn-outline-primary  px-4 mx-1 rounded-0">미리보기</button>
    	<button class="btn btn-outline-primary  px-4 mx-1 rounded-0">삭제</button>
    	<button class="btn btn-primary  px-4 mx-1 rounded-0">임시저장</button>
    	<button class="btn btn-primary  px-4 mx-1 rounded-0">저장</button>
    </div>
</form>
</body>
<?php include '../component/foot-icl.php';?>
</html>

<?php if(!empty($_POST["commited"])){
    echo "<script>parent.closeDialog();</script>";
}?>
