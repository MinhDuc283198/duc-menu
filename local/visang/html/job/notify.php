<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Job</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">
	<?php include '../component/nav-menu.php';?>

	<div class="mx-5 px-5 break-top">
		<?php include '../job/jobNotify.php';?>
	</div>	
	
	<?php include '../component/footer.php';?>
</body>
<?php include '../component/foot-icl.php';?>
</html>