<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Custom styles for this template-->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../js/vsController.js"></script>
<?php 
if(!empty($_GET["step"])){
    $step = $_GET["step"];
}else if(!empty($_POST["step"])){
    $step = $_POST["step"];
}else{
    $step = "";
}
?>
</head>
<body class="row">

<div class="col-1">
</div>
<div class="col-10">
<?php if($step == "1"){?>
	<!-- Step number -->
	<div class="row justify-content-center">
		<h2 class="oval-number current">1</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">2</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">3</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">4</h2>
	</div>
	<!-- Title -->
	<div class="row justify-content-center mt-5">
		<h3>관심 직군</h3>
	</div>
	<div class="row justify-content-center">
		<p>관심 직군을 하나만 선택해주세요.</p>
	</div>
	<!-- Job list -->
	<?php
    	$jobID = array("1","2","3","4","5","6","7","8","9","10","11","12","13","14");
    	$jobNames = array("관심정보","관심정보","관심정보","관심정보","관심정보","관심정보","관심정보",
    	                           "관심정보","관심정보","관심정보","관심정보","관심정보","관심정보","관심정보");
    	$jobImg = array("../icon/thub.png","../icon/thub.png","../icon/thub.png","../icon/thub.png","../icon/thub.png",
    	               "../icon/thub.png","../icon/thub.png","../icon/thub.png","../icon/thub.png","../icon/thub.png",
    	               "../icon/thub.png","../icon/thub.png","../icon/thub.png","../icon/thub.png");
    	?>
    	
	<form action="interest.php" method="post">
		<input type="text" name="step" value="2" hidden>
    	<div class="row justify-content-center">    	
    	<?php for ($i = 0; $i < count($jobNames); $i++) {?>
    		<button class=" col-2 text-over p-0 m-2" type="submit" name="JISstep1" value="<?php echo $jobID[$i];?>">
    			<img class="" src="<?php echo $jobImg[$i];?>" alt="Include Image">
    			<div class="overlay-dark">
    				<p class="centered text-white font-weight-bold"><?php echo $jobNames[$i];?></p>
    			</div>
    		</button>
    	<?php }?>
    	
    	</div>
	</form>
<?php }?>

<?php if($step == "2"){?>
<!-- Step number -->
	<div class="row justify-content-center">
		<h2 class="oval-number">1</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number current">2</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">3</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">4</h2>
	</div>
	<!-- Title -->
	<div class="row justify-content-center mt-5">
		<h3>관심 직무</h3>
	</div>
	<div class="row justify-content-center">
		<p>관심 직무를 최대 3개까지 선택해주세요.</p>
	</div>
	<div class="row justify-content-center mb-2">
		<h3>IT∙인터넷</h3>
	</div>
	<!-- Job list -->
	<form action="interest.php" method="post" enctype="multipart/form-data">
		<input type="text" name="step" value="3" hidden>
    	<div class="row justify-content-center pb-4">
    	<?php
    	$jobID = array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17");
    	$jobNames = array("관심정보","관심정보","관심정보","관심정보","관심정보","관심정보","관심정보",
    	                           "관심정보","관심정보","관심정보","관심정보","관심정보","관심정보","관심정보"
    	                           ,"관심정보","관심정보","관심정보");
    	?>
    	
    	<?php for ($i = 0; $i < count($jobNames); $i++) {?>
    		<button type="button" class="btn btn-outline-dark m-2" id="JIS<?php echo $jobID[$i];?>" onclick="checkJIS('JIS<?php echo $jobID[$i]?>')">
    			<?php echo $jobNames[$i];?>
    		</button>
    		<input name="JIS<?php echo $jobID[$i];?>" id="vJIS<?php echo $jobID[$i];?>" value="0" hidden>
    	<?php }?>
    	
    	</div>
    	<div class="row justify-content-center mt-5">
        	<a href="interest.php?step=1" class="btn btn-outline-dark m-2" role="button" aria-pressed="true">이전단계</a>
        	<button type="submit" class="btn btn-outline-dark m-2">선택완료</button>
    	</div>
	</form>
<?php }?>

<?php if($step == "3"){?>
	<!-- Step number -->
	<div class="row justify-content-center">
		<h2 class="oval-number">1</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">2</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number current">3</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">4</h2>
	</div>
	<!-- Title -->
	<div class="row justify-content-center mt-5">
		<h3>취업 희망 지역</h3>
	</div>
	<div class="row justify-content-center">
		<p>취업을 희망하는 지역을 선택해주세요.</p>
	</div>
	<!-- Job list -->
	<form action="interest.php" method="post" enctype="multipart/form-data">
		<input type="text" name="step" value="4" hidden>
    	<div class="row justify-content-center pb-4">
    	<?php
    	$jobID = array("1","2","3","4");
    	$jobNames = array("Ho Chi Minh","Ha Noi","Da Nang","Other");
    	?>
    	
    	<?php for ($i = 0; $i < count($jobNames); $i++) {?>
    		<button type="button" class="btn btn-outline-dark m-2" id="JISA<?php echo $jobID[$i];?>" onclick="checkJIS('JISA<?php echo $jobID[$i]?>')">
    			<?php echo $jobNames[$i];?>
    		</button>
    		<input name="JISA<?php echo $jobID[$i];?>" id="vJISA<?php echo $jobID[$i];?>" value="0" hidden>
    	<?php }?>
    	
    	</div>
    	<div class="row justify-content-center pt-5 mt-5">
        	<a href="interest.php?step=2" class="btn btn-outline-dark m-2" role="button" aria-pressed="true">이전단계</a>
        	<button type="submit" class="btn btn-outline-dark m-2">선택완료</button>
    	</div>
	</form>
<?php }?>

<?php if($step == "4"){?>
	<!-- Step number -->
	<div class="row justify-content-center">
		<h2 class="oval-number">1</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">2</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number">3</h2>
		<h6 class="oval-join"></h6>
		<h2 class="oval-number current">4</h2>
	</div>
	
	<!-- Title -->
	<div class="row justify-content-center mt-5">
		<h3>취업 희망 지역</h3>
	</div>
	<div class="row justify-content-center">
		<p>취업을 희망하는 지역을 선택해주세요.</p>
	</div>
	<!-- Job list -->
	<form action="interest.php" method="post" enctype="multipart/form-data">
		<input type="text" name="step" value="5" hidden>
    	<div class="row justify-content-center pb-4">
    	<?php
    	$jobID = array("1","2","3","4");
    	$jobNames = array("한국어 못함","짧은 대화 가능","일상대화 가능","비즈니스 회화 가능");
    	?>
    	
    	<?php for ($i = 0; $i < count($jobNames); $i++) {?>
    		<button type="button" class="btn btn-outline-dark m-2 jisl" id="JISL<?php echo $jobID[$i];?>" 
    				onclick="checkJISL('JISL<?php echo $jobID[$i]?>','<?php echo $jobID[$i]?>')">
    			<?php echo $jobNames[$i];?>
    		</button>
    		
    	<?php }?>
    		<input name="JISL" id="JISL" value="-1" hidden>
    	</div>
    	<div class="row justify-content-center pt-5 mt-5">
        	<a href="interest.php?step=2" class="btn btn-outline-dark m-2" role="button" aria-pressed="true">이전단계</a>
        	<button type="submit" class="btn btn-outline-dark m-2">선택완료</button>
    	</div>
	</form>
	
<?php }?>

<?php if($step == "5"){ echo "<script type='text/javascript'>parent.openShareInfoAgreement();</script>";}?>
</div>

<div class="col-1">
</div>
</body>
</html>