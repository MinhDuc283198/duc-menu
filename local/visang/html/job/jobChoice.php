<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Job</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">
<form action="" method="post">
	<strong>현재 비밀번호</strong>
	<input type="text" name="commited" value="true" hidden>
    <table class="table table-bordered table-hover">
          <thead class="thead-light">
            <tr>
              <th scope="col" class="text-center">직군 </th>
              <th scope="col" class="text-center">채용공고</th>
            </tr>
          </thead>
          <tbody>
          <?php for($i=0; $i<6; $i++){?>
            <tr>
                <td class="text-center align-middle">직군</td>
                <td class="align-middle section-preview">
                	<div class="form-check">
                      <input type="checkbox" class="form-check-input" id="row<?php echo $i;?>">
                      <label class="form-check-label d-inline ml-3" for="row<?php echo $i;?>">직군1_직무1</label>
                    </div>
                </td>
            </tr>
    	<?php }?>
          </tbody>
    </table>
    <div class="my-3 d-flex justify-content-center">
    	<a href="#" class="btn btn-outline-primary px-4 mr-2 rounded-0">초기화</a>
    	<button class="btn btn-primary  px-4 rounded-0">확인</button>
    </div>
</form>
</body>
<?php include '../component/foot-icl.php';?>
</html>

<?php if(!empty($_POST["commited"])){
    echo "<script>parent.closeDialog();</script>";
}?>
