<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Job</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">
	<?php include '../component/nav-menu.php';?>
	
<div class="mx-5 px-5 break-top mb-5 pb-5">
    <div class="row mx-4 border border-dark">
    	<div class="col-8 p-0 border border-dark">
    		<object data="../component/Untitled.pdf" type="application/pdf" width="100%" height="600px;">
              	<div class="text-center pt-5 mt-5" style="height: 100%;"><h3>PDF 뷰어 <br> (브라우저 기본)</h3></div>
            </object>
    	</div>
    	<div class="col-4 p-0">
    		<div class="col-12 d-flex justify-content-center border border-dark">
            	<button type="button" class="btn btn-outline-dark rounded-0 mx-1 my-4"><i class="fas fa-angle-left"></i>&ensp; 이전 </button> 
            	<button type="button" class="btn btn-outline-dark rounded-0 mx-1 my-4">다음 &ensp;<i class="fas fa-angle-right"></i></button> 
            	<button type="button" class="btn btn-outline-primary rounded-0 ml-3 my-4">다운로드</button> 
            </div>
            <div class="col-12 border border-dark px-4">
                <div class="row pt-3">
                	<div class="col-4">
                    	<p>이름</p>
                    	<p>아이디</p>
                    	<p>이메일</p>
                    	<p>휴대폰 번호</p>
                    </div>
                    <div class="col-8">
                    	<p><b>김비상</b></p>
                    	<p><b>abcde@gmail.com</b></p>
                    	<p><b>0111-000-000</b></p>
                    	<p><b>이러닝 마케팅팀 채용(마케팅... </b></p>
                    </div>
                </div>
            </div>
            <div class="col-12  p-4">
            	<button type="button" class="btn btn-info btn-lg btn-block rounded-0"><i class="fas fa-play-circle"></i> 동영상 프로필 보기 </button>
            	<button type="button" class="btn btn-primary btn-lg btn-block rounded-0"><i class="far fa-file-pdf"></i> 강좌 수료현황 보기 </button>
            </div>
    	</div>
    </div>
</div>

	
	<?php include '../component/footer.php';?>
</body>
<?php include '../component/foot-icl.php';?>
</html>