<h3 class="mx-4 mb-4"><b>지원자</b></h3>
<div class="row mx-4">
	<div class="col-4 pb-3 justify-content-start">
		<a href="#">123건</a>
		<button type="button" class="btn btn-outline-dark btn-sm ml-3 px-3">20개씩 보기  &ensp;&ensp;<i class="fas fa-chevron-down"></i></button>
	</div>
	<div class="col-8 pb-3 d-flex justify-content-end">
	<form method="post" action="../job/apply.php">
		<button type="button" class="btn btn-outline-dark btn-sm ml-2 px-3 rounded-0">채용공고 전체  &ensp;&ensp;<i class="fas fa-chevron-down"></i></button>
		<input type="text" name="XYZ_NAME" class="btn btn-outline-dark btn-sm ml-2 px-3 text-left rounded-0" placeholder="이름"/>
		<button type="button" class="btn btn-dark btn-sm ml-2 px-3 rounded-0">검색 </button>
		<button type="button" class="btn btn-outline-dark btn-sm ml-2 px-3 rounded-0">초기화</button>
	</form>
	</div>
</div>

<div class="row mx-4">
	<table class="table table-bordered table-hover">
      <thead class="thead-light">
        <tr>
          <th scope="col" class="text-center">지원분야 </th>
          <th scope="col" class="text-center">지원자</th>
          <th scope="col" class="text-center">이메일</th>
          <th scope="col" class="text-center">이력서</th>
          <th scope="col" class="text-center">지원일</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0; $i<6; $i++){?>
        <tr>
            <td class="align-middle" onclick="openApplayJob('<?php echo $i;?>')">이러닝 마케팅팀 채용(마케팅 및 기획 업무)</td>
            <td class="text-center align-middle">홍길동</td>
            <td class="text-center align-middle">abcde@gmail.com</td>
            <td class="align-middle">
            	<div class="justify-content-start">
            	<?php if(true){?> 
            		<a>이력서 홍길동</a> 
            	<?php }?>
            	<?php if(true){?> 
            		<a href="#" class="ml-3" style="font-size: 30px;"><i class="fas fa-play-circle"></i></a> 
            	<?php }?>
            	<?php if(true){?> 
            		<button type="button" class="btn btn-outline-dark btn-sm ml-3 px-3">강좌 수료현황</button> 
            	<?php }?>
            	</div>
            </td>
            <td class="text-center align-middle">2019.09.05</td>
        </tr>
	<?php }?>
      </tbody>
    </table>
    
    <div class="col-12 d-flex justify-content-end">
    	<button type="button" class="btn btn-outline-primary rounded-0 mb-4">엑셀 다운로드 </button> 
    </div>
</div>

<div class="row mx-4">
	<div class="col-12 d-flex justify-content-center">
    	<button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-double-left"></i></button> 
    	<button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-left"></i></button> 
    	<?php for($i=1; $i<5; $i++){ ?>
        	<button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><?php echo $i;?></button> 
        <?php }?>
        <button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-right"></i></button> 
        <button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-double-right"></i></button> 
    </div>
</div>