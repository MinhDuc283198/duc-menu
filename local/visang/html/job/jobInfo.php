

<?php 
$jobImg = "../icon/visang.png";
$jobHeart = true;
$jobTitle = "(JAVA, C#)응용프로그램 및 웹 개발자";
$jobCost = "1,200";
$jobExplan = "비상교육 베트남 이러닝 개발팀에서 함께 근무할 인재를 모집합니다. 저희 비상교육은 1998년 한국에서 설립되어 21년의 역사를 자랑하는 교과서 및 학습 …";
$jobDuty = "";
$jobDay = "D-30";
$jobChoice = "하노이";
$jobTime = "16시간 전";
?>

<div class="row break-top">
	<div class="col-4 pl-5">
    	<div class="border border-dark">
    		<div class="job-img d-block">
    			<img class="pt-3" alt="" src="<?php echo $jobImg;?>">
        	</div>
        	<div style="background:rgba(0,0,0,0.1);">
        		<div class="d-flex justify-content-center py-4">
            		<h4><b>비상교육</b></h4>
            	</div>
            	<div class="pl-3 pb-2 d-flex justify-content-start">
            		<i class="fas fa-cog li-icon"></i> &ensp; 
            		<h6>교과서 및 학습서적 출판업</h6>
            	</div>
            	<div class="pl-3 pb-2 d-flex justify-content-start">
            		<i class="fas fa-user-friends li-icon"></i> &ensp;
            		<h6>579명 (2019년 기준)</h6>
            	</div>
            	<div class="pl-3 pb-2 d-flex justify-content-start">
            		<img class="li-icon" alt="" src="../icon/Korea.png">&ensp;
            		<h6>대한민국</h6>
            	</div>
            	<div class="row mx-4 mt-4 pt-5 pb-3 d-flex justify-content-between">
            		<a href="#" style="color: red;">3개의 채용공고 보기 <i class="fas fa-caret-right"></i></a>
            		<a href="#" style="color: red;">기업 정보 상세 보기 <i class="fas fa-caret-right"></i></a>
            	</div>
        	</div>
        	
    	</div>
    	
    	<div class="row justify-content-center pt-4">
    		<div class="job-action">
    			<?php if(false){?>
    				<i class="fas fa-heart pb-3 active-heart-red" id="job-heart"></i>
    			<?php } else{ ?>
    				<i class="far fa-heart pb-3" id="job-heart"></i>
    			<?php }?>
    			<p>관심 채용공고</p>
    		</div>
    		<div class="job-action">
    			<?php if(false){?>
    				<i class="fas fa-bell pb-3 active-heart-red" id="job-bell"></i>
    			<?php } else{ ?>
    				<i class="far fa-bell pb-3" id="job-bell"></i>
    			<?php }?>
    			<p>유사 공고 알림 받기<br>(해당 직무가 관심정보에 등록됩니다).</p>
    		</div>
    	</div>
    	
	</div>
	<div class="col-8 pr-5">
	
	   <!--Start primary job info -->
		<div class="pl-3 pt-3 d-flex justify-content-start">
    		<h3><b>이러닝 마케팅팀 채용(마케팅 및 기획 업무)</b></h3>
    	</div>
    	<div class="pl-3 py-3 d-flex justify-content-start">
    		<?php for($j=0; $j<6; $j++){?>
            <button type="button" class="btn btn-outline-dark btn-sm mr-2">직무 <?php echo $j?></button>
        	<?php }?>
    	</div>
    	<div class="pl-3 pt-2 d-flex justify-content-start">
    		<i class="far fa-address-book li-icon"></i>&ensp;
    		<h6>계약직, 프리랜서</h6>
    	</div>
    	<div class="pl-3 pt-2 d-flex justify-content-start" style="color: green;">
    		<i class="fas fa-dollar-sign li-icon"></i>&ensp;
    		<h6 style="color: green;">800 ~ 1,200 USD</h6>
    	</div>
    	<div class="pl-3 pt-2 d-flex justify-content-start">
    		<i class="fas fa-map-marker-alt li-icon"></i>&ensp;
    		<h6>서울시 구로구 구로동 170-10 대륭포스트타워 7차 20층</h6>
    	</div>
    	<div class="pl-3 pt-2 d-flex justify-content-start">
    		<i class="far fa-clock li-icon"></i>&ensp;
    		<h6>월~금(주5일) 09:00~18:00</h6>
    	</div>
    	<div class="pl-3 pt-2 d-flex justify-content-start" style="color: blue;">
    		<i class="far fa-calendar-alt li-icon"></i>&ensp;
    		<h6 style="color: blue;">2019.11.30까지</h6>
    	</div>
    	<button type="button" class="btn btn-danger btn-lg btn-block rounded-0"><b>지금 지원하기</b></button>
    	<div class="row"><hr class="break-line" /></div>
    	<!--End primary job info -->
    	
    	<!-- Start more job info -->
    	<h3 class="col-12 pt-3"><b>우리 회사에 지원해야 하는 이유</b></h3>
    	<ul class = "ul-bullet">
    		<li>자유로운 연차/월차 제도</li>
    		<li>한국 문화 습득 및 한국에서 일할 수 있는 기회 제공</li>
    		<li>수평적인 조직 문화</li>
    	</ul>
    	
    	<h3 class="col-12 pt-3"><b>담당 업무</b></h3>
    	<ul class = "ul-none">
    		<li>안녕하세요, 비상교육에서 이러닝 마케팅팀 팀원을 모집합니다.</li>
    		<li>- 디지털 영어 학습 콘텐츠 기획 및 개발</li>
    		<li>- 스마트 학습 프로그램 교수 설계</li>
    		<li>저희 비상교육만의 체계적인 스마트 학습 프로그램을 기획하고 마케팅할 신입/경력자를 모집합니다.</li>
    	</ul>
    	
    	<h3 class="col-12 pt-3"><b>자격 요건</b></h3>
    	<ul class = "ul-none">
    		<li>- 4년제 대졸 이상</li>
    		<li>- 신입 지원 가능</li>
    		<li>- 경력자는 유관업무 경력 3년 이상</li>
    		<li>- 영어교육 콘텐츠에 대한 이해가 풍부한자</li>
    	</ul>
    	
    	<h3 class="col-12 pt-3"><b>우대조건</b></h3>
    	<ul class = "ul-none">
    		<li>- 한국어 활용 수준 : 일상대화 가능</li>
    		<li>- TOEIC 900점 이상</li>
    		<li>- TESOL/영어교육 관련학과 전공자</li>
    	</ul>
    	
    	<h3 class="col-12 pt-3"><b>복지혜택</b></h3>
    	<ul class = "ul-none">
    		<li>비상교육 기업은 임직원들을 위해 지원을 아끼지 않습니다.</li>
    		<li>- 우리 회사만의 특별한 복지! 명절선물/귀향비, 창립일선물, 생일선물/파티, 결혼기념일선물을 지급합니다.</li>
    		<li>- 직원들의 자기계발을 존중합니다. 석식 제공, 사내동호회 운연, 사우회(경조사회) 음료제공(차, 카피), 우리사주제도를 운영합니다.</li>
    		<li>- 직원 중심의 조직문화를 만들어갑니다. 수평적 조직문화에 앞장서며 전직원 자유복장입니다.</li>
    		<li>- 최고의 복지는 휴가죠! 산전 후 휴가,육아휴직, 남성출산휴가, 보건휴가를 제공합니다.</li>
    	</ul>
    	<!-- End more job info -->
	</div>
</div>

<div class="row"><hr class="break-line" /></div>
<div class="row mx-4">
	<h3 class="my-4"><b>이 공고와 유사한 채용공고</b></h3>
</div>

<?php include 'jobList.php';?>