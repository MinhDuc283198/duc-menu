
<h3><b>채용공고</b></h3>
<div class="my-3 d-flex justify-content-between">
	<div class="d-block">
		<a class="mr-3 text-dark" href="#">전체(123)</a>|
		<a class="mx-3 text-dark" href="#">진행중(2)</a>|
		<a class="mx-3 text-dark" href="#">대기중(1)</a>|
		<a class="mx-3 text-dark" href="#">임시저장(1)</a>|
		<a class="mx-3 text-dark" href="#">저장(1)</a>
	</div>
	<a  class="btn btn-primary btn-lg rounded-0 text-white">추가하기</a>
</div>

<div class="my-3 d-flex justify-content-between">
	<div class="mx-3 d-block">
		<form>
          <div class="form-group row">
            <label class="mr-3 col-form-label text-primary">123건</label>
            <div class="">
              <select class="form-control">
                <option selected>20개씩 보기 </option>
                <option>...</option>
              </select>
            </div>
          </div>
        </form>
	</div>
	<div class="d-block">
		<input type="text" name="XYZ_NAME" class="btn btn-outline-dark ml-2 px-3 text-left rounded-0" placeholder="채용공고명"/>
		<a  class="btn btn-outline-dark rounded-0">게시일 검색  &ensp;<i class="far fa-calendar-alt"></i></a>
		<a  class="btn btn-dark rounded-0 text-white">검색</a>
		<a  class="btn btn-outline-dark  rounded-0">초기화</a>
	</div>
</div>

<table class="table table-bordered table-hover">
      <thead class="thead-light">
        <tr>
          <th scope="col" class="text-center">상태</th>
          <th scope="col" class="text-center">채용공고</th>
          <th scope="col" class="text-center">직군/직무</th>
          <th scope="col" class="text-center">게시일</th>
          <th scope="col" class="text-center">마감일</th>
          <th scope="col" class="text-center">지원자</th>
          <th scope="col" class="text-center">지원자</th>
          <th scope="col" class="text-center">등록일</th>
          <th scope="col" class="text-center">수정</th>
          <th scope="col" class="text-center">수정</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0; $i<6; $i++){?>
        <tr>
            <td class="text-center align-middle"> 진행중</td>
            <td class="align-middle">이러닝 마케팅팀 채용(마케팅 및 기획 업무)</td>
            <td class="text-center align-middle">직무1, 직무2, 직무3</td>
            <td class="text-center align-middle">2019.09.05</td>
            <td class="align-middle">2019.09.05</td>
            <td class="text-center align-middle">32</td>
            <td class="text-center align-middle">1,234</td>
            <td class="align-middle">2019.09.05</td>
            <td class="text-center align-middle"><a href="#"  class="btn btn-outline-primary rounded-0">초기화</a></td>
            <td class="text-center align-middle"><a href="#"  class="btn btn-outline-dark  rounded-0">초기화</a></td>
        </tr>
	<?php }?>
      </tbody>
</table>

<div class="my-3 d-flex justify-content-end">
	<a href="#" class="btn btn-outline-primary rounded-0">엑셀 다운로드 </a>
</div>

<div class="col-12 d-flex justify-content-center">
	<button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-double-left"></i></button> 
	<button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-left"></i></button> 
	<?php for($i=1; $i<5; $i++){ ?>
    	<button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><?php echo $i;?></button> 
    <?php }?>
    <button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-right"></i></button> 
    <button type="button" class="btn btn-outline-dark rounded-0 ml-1 mb-4"><i class="fas fa-angle-double-right"></i></button> 
</div>