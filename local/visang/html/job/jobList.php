<!-- Please give me a list of job, I will display it. Thanks. -->

<!-- Bellow is template data -->
<?php 
$jobImg = "../icon/visang.png";
$jobHeart = true;
$jobTitle = "(JAVA, C#)응용프로그램 및 웹 개발자";
$jobCost = "1,200";
$jobExplan = "비상교육 베트남 이러닝 개발팀에서 함께 근무할 인재를 모집합니다. 저희 비상교육은 1998년 한국에서 설립되어 21년의 역사를 자랑하는 교과서 및 학습 …";
$jobDuty = "";
$jobDay = "D-30";
$jobChoice = "하노이";
$jobTime = "16시간 전";
?>


<!-- Start filter session -->
<div class="row mx-4 d-flex justify-content-between">
	<div class="d-block d-flex justify-content-start">
		<h5>고용형태 :&ensp;</h5> <h5><b>전체</b></h5>
		<h5> &ensp;&ensp;|&ensp;&ensp; 연봉 :&ensp;</h5> <h5><b>전체</b></h5>
		<h5> &ensp;&ensp;|&ensp;&ensp; 지역 :&ensp;</h5> <h5><b>전체</b> </h5>
	</div>
	<div class="d-block d-flex justify-content-end">
		<h5><b>인기순</b>&ensp;</h5>
		<h5> &ensp;&ensp;|&ensp;&ensp; 등록순 &ensp;</h5>
		<h5> &ensp;&ensp;|&ensp;&ensp; 마감일순 &ensp;</h5>
	</div>
</div>
<!-- End filter session -->

<?php for($i=0; $i<6; $i++){?>
<div class="row mx-4 border border-dark job-item mb-2" onclick="viewJobInfo('Insert jobID <?php echo $i?>');">
	<div class="col-2">
		<div class="d-block img-heart">
			<div class="job-img d-block">
				<img class="pt-3" alt="" src="<?php echo $jobImg;?>">
    		</div>
    		<div class="d-flex justify-content-between job-heart">
    		<?php if($jobHeart && $i%2==0){?>
                <h2><i class="fas fa-heart active-heart-blue"></i></h2>
            <?php } else {?>
                <h2><i class="far fa-heart active-heart-blue"></i></h2>
            <?php }?>
            </div>
		</div>
	</div>
	
	<div class="col-xl-9 col-md-8">
		<h4 class="pt-3 pb-2"><?php echo $jobTitle;?></h4>
		<h5 class="text-success"><span class="border border-success rounded-circle m-0 py-0 px-1">$</span> <?php echo $jobCost;?> USD</h5>
		<p><?php echo $jobExplan;?></p>
		<div class="d-flex justify-content-start mb-4">
		<?php for($j=0; $j<6; $j++){?>
            <button type="button" class="btn btn-outline-dark mr-2">직무 <?php echo $j?></button>
        <?php }?>
        </div>
	</div>
	
	<div class="col-xl-1 col-md-2">
		<div class="d-flex flex-row-reverse">
			<h4 class="pt-3 pb-4 text-primary"><?php echo $jobDay;?></h4>
		</div>
		<div class="d-flex flex-row-reverse">
			<h6 class="pt-3"><?php echo $jobChoice;?></h6>
		</div>
		<div class="d-flex flex-row-reverse">
			<p class="pt-4 m-0 text-primary"><?php echo $jobTime;?></p>
		</div>
	</div>
	
</div>
<?php }?>

<div class="p-4">
	<a href="#" class="btn btn-outline-dark btn-lg btn-block" role="button" aria-pressed="true">더보기 
	<i class="fas fa-chevron-down"></i></a>
	<!-- <button type="button" class="btn btn-outline-dark px-5">더보기</button> -->
</div>