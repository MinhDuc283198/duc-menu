
<!-- Start locate menu session -->
<div class="row mx-4 d-flex pb-3 justify-content-start">
    <h3 class="m-0">전체 > 직군명 > 직무명</h3>
    <h3 class="m-0 ml-2 as-pointer" data-toggle="modal" data-target="#menu-modal"><i class="fas fa-bars"></i></h3>
</div>
<!-- End locate menu session -->

<!-- Start alert session -->
<?php if($hasAlert){?>
<div class="row m-4 justify-content-start">
	<?php if(false){?>
    <h3 class="m-0">추천 채용공고</h3>
    <p class="m-0">&ensp;&ensp; 관심정보 기반의 추천 채용공고입니다.</p>
    <?php } elseif(false){?>
    <h3 class="m-0">채용공고</h3>
    <?php } else {?>
    <h5 class="m-0 border border-danger py-2 pl-3 pr-5 job-alert"><i class="far fa-bell"></i>&ensp; 유사한 공고가 등록되면 알림 받기</h5>
    <?php }?>
</div>
<?php }?>
<!-- End alert session -->

<!-- Start note session -->
<?php if($hasNote){?>
<div class="row m-4 justify-content-center">
    <h3 class="m-0"><b>찾고 있는 채용공고가 없습니다.</b></h3>
</div>
<div class="row m-4 justify-content-center">
    <h3 class="m-0"><b>현재 인기 있는 채용공고를 확인하세요.</b></h3>
</div>
 <?php } ?>
<!-- End note session -->