<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Profile</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">
	<?php include '../component/nav-menu.php';?>
	
	<div class="row mx-5 px-5 break-top">
		<?php include '../user/usrMenu.php'?>
		<div class="col-10">
			<iframe id="usr-iframe" src="../user/usrProfile.php" width="100%" height="100%" scrolling="no">
            </iframe>
		</div>
	</div>
	
	<?php include '../component/footer.php';?>
</body>
<?php include '../component/foot-icl.php';?>
</html>


