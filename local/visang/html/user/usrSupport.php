
<?php 
$hasSupport = false;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Profile</title>
 <?php include '../component/head-icl.php';?>
</head>
<body class="">	
	<h3 class="text-center"><b>지원 현황</b></h3>
	<table class="table table-bordered">
      <thead class="thead-light">
        <tr>
          <th scope="col">번호</th>
          <th scope="col" class="text-center">기업</th>
          <th scope="col" class="text-center">지원내역</th>
          <th scope="col" class="text-center">지원일</th>
        </tr>
      </thead>
      <tbody>
      <?php if($hasSupport){
          for($i=5; $i>0; $i--){
      ?>
        <tr>
            <th scope="row" class="text-center"><?php echo $i?></th>
            <td class="text-center align-middle">비상교육</td>
            <td><ins>이러닝 마케팅팀 채용(마케팅 및 기획 업무)</ins><br>
            	마감일 : 2019.10.30</td>
            <td class="text-center align-middle">2019.10.22</td>
        </tr>
        <?php }
        }else{?>
            <tr>
                <td class="text-center align-middle" colspan="4">지원한 기업이 없습니다.<br>
																나에게 꼭 맞는 채용공고를 확인하고 지원해보세요. <br>
                	<a href="#" class="btn btn-outline-dark mt-3 px-5" role="button" aria-pressed="true">채용공고 보기</a>
                </td>
                
            </tr>
        <?php }?>
      </tbody>
    </table>
    <div class="row d-flex justify-content-center">
    	<a href="#">&lt;</a>
    	
    	<?php for($i=1; $i<11; $i++){?>
    	<a href="#">&ensp;<?php echo $i;?>&ensp;</a>
    	<?php }?>
    	
		<a href="#">&gt;</a>
	</div>
</body>
<?php include '../component/foot-icl.php';?>
</html>

<script>
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
</script>