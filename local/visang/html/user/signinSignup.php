<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <?php include '../component/head-icl.php';?>

</head>
<body class="">
	<?php
	if(!empty($_GET["login"])){
	    $isLogin = $_GET["login"];
	}else if(!empty($_POST["login"])){
	    $isLogin = $_POST["login"];
	}else{
	    $isLogin = "";
	}
	
	if(!empty($_GET["fblogin"])){
	    $isFbLogin = $_GET["fblogin"];
	}else if(!empty($_POST["fblogin"])){
	    $isFbLogin = $_POST["fblogin"];
	}else{
	    $isFbLogin = "";
	}
	
	if(!empty($_GET["gglogin"])){
	    $isGgLogin = $_GET["gglogin"];
	}else if(!empty($_POST["gglogin"])){
	    $isGgLogin = $_POST["gglogin"];
	}else{
	    $isGgLogin = "";
	}
	
	if(!empty($_GET["zllogin"])){
	    $isZlLogin = $_GET["zllogin"];
	}else if(!empty($_POST["zllogin"])){
	    $isZlLogin = $_POST["zllogin"];
	}else{
	    $isZlLogin = "";
	}
	
	if(!empty($_GET["signup"])){
	    $isSignup = $_GET["signup"];
	}else if(!empty($_POST["signup"])){
	    $isSignup = $_POST["signup"];
	}else{
	    $isSignup = "";
	}
	
	if(!empty($_GET["fbsignup"])){
	    $isFbSignup = $_GET["fbsignup"];
	}else if(!empty($_POST["fbsignup"])){
	    $isFbSignup = $_POST["fbsignup"];
	}else{
	    $isFbSignup = "";
	}
	
	if(!empty($_GET["ggsignup"])){
	    $isGgSignup = $_GET["ggsignup"];
	}else if(!empty($_POST["ggsignup"])){
	    $isGgSignup = $_POST["ggsignup"];
	}else{
	    $isGgSignup = "";
	}
	
	if(!empty($_GET["zlsignup"])){
	    $isZlSignup = $_GET["zlsignup"];
	}else if(!empty($_POST["zlsignup"])){
	    $isZlSignup = $_POST["zlsignup"];
	}else{
	    $isZlSignup = "";
	}
	
	if(!empty($_GET["fusr"])){
	    $isFindUser = $_GET["fusr"];
	}else if(!empty($_POST["fusr"])){
	    $isFindUser = $_POST["fusr"];
	}else{
	    $isFindUser = "";
	}
	
	if($isLogin=="true"){
	    include 'signinAction.php';
	    
	}else if($isFbLogin =="true"){
	    include 'facebookLoginAction.php';
	    
	}else if($isGgLogin =="true"){
	    include 'googleLoginAction.php';
	    
	}else if($isZlLogin =="true"){
	    include 'zaloLoginAction.php';
	    
	}else if($isSignup =="true"){
        include 'signupAction.php';
        
	}else if($isFbSignup =="true"){
	    include 'facebookSignupAction.php';
	    
	}else if($isGgSignup =="true"){
	    include 'googleSignupAction.php';
	    
	}else if($isZlSignup =="true"){
	    include 'zaloSignupAction.php';
	    
	}else if($isFindUser =="true"){
	    include 'findUserAction.php';
	}else{
	    echo "<script>parent.closeDialog();</script>";
	}
    
	?>
	
</body>
 <?php include '../component/foot-icl.php';?>
</html>