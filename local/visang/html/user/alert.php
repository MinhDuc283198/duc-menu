<!-- Dropdown - Alerts -->
<div class="user-alert d-none position-fixed" id="userAlertList">
	<ul class="list-group text-left rounded-0">
	<?php for($i=0; $i<3; $i++){?>
      <li class="list-group-item py-1 rounded-0">
      	<div class="row d-flex justify-content-between">
      		<strong>채용공고 추천</strong>
      		<button type="button" class="btn btn-link"><i class="fas fa-times"></i></button>
      	</div>
      	<p class="mb-1">[비상교육] 이러닝 마케팅팀 채용(마케팅 및 기획 업무) | 지원 마감 : ~10/23</p>
      	<p class="mb-1">43분 전</p>
      </li>
      <?php }?>
    </ul>
    <a href="#" class="btn btn-primary btn-lg btn-block rounded-0" role="button" aria-pressed="true">전체보기</a>
</div>

<!-- Dropdown - Action menu -->
<div class="user-menu d-none position-fixed" id="userActionMenu">
	<ul class="list-group text-left rounded-0">
      <li class="list-group-item rounded-0"><a href="../user">내 정보</a></li>
      <li class="list-group-item rounded-0"><a href="../user/signoutAction.php">로그아웃</a></li>
    </ul>
</div>

<!-- <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="far fa-bell"></i></a> -->
