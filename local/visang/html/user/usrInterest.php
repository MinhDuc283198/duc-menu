
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Profile</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">	
	<h3>관심정보</h3>
	
	<div class="d-flex">
		<button id="inter-tab1" class="btn btn-primary btn-block rounded-0 m-0 py-2">관심정보</button>
		<button id="inter-tab2" class="btn btn-outline-primary btn-block rounded-0 m-0 py-2">관심 채용공고</button>
	</div>
<div id="inter-tab-1">
	<p class="mt-3">설정한 관심정보에 새로운 소식이 등록되면 push 알림이 발송됩니다.</p>
	
	<form>
      <div class="form-group row">
        <label class="mr-3 col-form-label">한국어 활용 수준</label>
        <div class="">
          <select class="form-control">
            <option selected>일상대화 가능</option>
            <option>...</option>
          </select>
        </div>
      </div>
    </form>
    
    <div class="border border-dark">
    	<form class="mb-0 mt-5" action="" method="post">
        	<div class="form-row px-3">
        		<div class="form-group col-1 d-flex justify-content-center">
        			<button type="button" class="btn btn-light rounded-0 border-0"><i class="fas fa-times" style="font-size: 28px;"></i></button>
    			</div>
        		<div class="form-group col-9 d-flex justify-content-start">
        			<input type="text" class="form-control rounded-0" id="in-4" placeholder="...">
    			</div>
    			<div class="form-group col-2">
        			<button type="submit"  class="btn btn-light rounded-0 border-0"><i class="fas fa-check"></i> 추가</button>
    			</div>
        	</div>
    	</form>
    	
    	<form class="mb-0" action="" method="post">
        	<div class="form-row px-3">
        		<div class="form-group col-1 d-flex justify-content-center">
        			<button type="button" class="btn btn-light rounded-0 border-0"><i class="fas fa-times" style="font-size: 28px;"></i></button>
    			</div>
        		<div class="form-group col-9 d-flex justify-content-start">
        			<input type="text" class="form-control rounded-0" id="in-4" placeholder="...">
    			</div>
    			<div class="form-group col-2">
        			<button type="submit"  class="btn btn-light rounded-0 border-0"><i class="fas fa-check"></i> 추가</button>
    			</div>
        	</div>
    	</form>
    	
    	<form class="mb-0" action="" method="post">
        	<div class="form-row px-3">
        		<div class="form-group col-1 d-flex justify-content-center">
        			<button type="button" class="btn btn-light rounded-0 border-0"><i class="fas fa-times" style="font-size: 28px;"></i></button>
    			</div>
        		<div class="form-group col-9 d-flex justify-content-start">
        			<input type="text" class="form-control rounded-0" id="in-4" placeholder="...">
    			</div>
    			<div class="form-group col-2">
        			<button type="submit"  class="btn btn-primary rounded-0">추가하기</button>
    			</div>
        	</div>
    	</form>
    	
    	<form class="mb-5" action="" method="post">
        	<div class="form-row px-3">
        		<div class="form-group col-1 d-flex justify-content-center">
        			<button type="button" class="btn btn-light rounded-0 border-0"><i class="fas fa-times" style="font-size: 28px;"></i></button>
    			</div>
        		<div class="form-group col-9 d-flex justify-content-start">
        			<input type="text" class="form-control rounded-0" id="in-4" placeholder="...">
    			</div>
    			<div class="form-group col-2">
        			<button type="submit"  class="btn btn-primary rounded-0">추가하기</button>
    			</div>
        	</div>
    	</form>
    </div>
    
    <div class="mt-3 nav justify-content-center px-3">
		<a  class="btn btn-primary btn-lg rounded-0 text-white">추가하기</a>
    </div>
    <p class="text-center pt-3">최대 5개의 구독 목록을 만들 수 있습니다.</p>  
</div>	
<div id="inter-tab-2" class="d-none">
	<p class="mt-3">※ 마감된 채용공고는 자동으로 삭제됩니다.</p>
	<?php include '../job/jobList.php';?>
</div>	   
            
</body>
<?php include '../component/foot-icl.php';?>
</html>

<script>
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
</script>