
<?php 
if(!empty($_GET["acceptUser"])){
    $acceptUserName = $_GET["acceptUser"];
}else if(!empty($_POST["acceptUser"])){
    $acceptUserName = $_POST["acceptUser"];
}else{
    $acceptUserName = "false";
}
?>

<?php if($acceptUserName == "false"){?>
<div id="signupAgreement" class="">
	<div class="row justify-content-center">
    	<h2 class="oval-number current">1</h2>
    	<h6 class="oval-join"></h6>
    	<h2 class="oval-number">2</h2>
    	<h6 class="oval-join"></h6>
    	<h2 class="oval-number">3</h2>
    </div>
    <!-- Title -->
    <div class="row justify-content-center mt-3">
    	<h3>기업회원 가입에 대한 동의 </h3>
    </div>

	<!-- This is for Agrre 1 -->
	<div class="row mx-4 mt-4">
    	<label class="col-12 pl-0">이용약관 동의(필수)</label>
    	<textarea class="col txt"></textarea>
    	<div class=" col-12 custom-control custom-checkbox nav justify-content-end pr-2">
            <input type="checkbox" class="custom-control-input rounded-0" id="chkAgree1">
            <label class="custom-control-label" for="chkAgree1">동의</label>
        </div>
    </div>
    
    <!-- This is for Agrre 2 -->
    <div class="row mx-4 mt-4">
    	<label class="col-12  pl-0">개인정보 수집 및 이용동의(필수)</label>
    	<textarea class="col txt"></textarea>
    	<div class=" col-12 custom-control custom-checkbox nav justify-content-end pr-2">
            <input type="checkbox" class="custom-control-input rounded-0" id="chkAgree2">
            <label class="custom-control-label" for="chkAgree2">동의</label>
        </div>
    </div>
    
    <!-- Check all Agree -->
    <div class="row mx-4 mt-4">
        <p class="px-0">※ 위 항목에 동의하지 않는 경우 회원가입이 불가합니다.</p>
		<button id="checkAgree" class="btn btn-primary btn-lg btn-block rounded-0 disabled">완료</button>
    </div>
</div>

<!-- Signup action -->

<div id="signupAction" class="d-none">
	<div class="row justify-content-center">
    	<h2 class="oval-number">1</h2>
    	<h6 class="oval-join"></h6>
    	<h2 class="oval-number current">2</h2>
    	<h6 class="oval-join"></h6>
    	<h2 class="oval-number">3</h2>
    </div>
    <!-- Title -->
    <div class="row justify-content-center mt-3">
    	<h3>기업회원 가입</h3>
    </div>
    
	<form class="mx-3" action="">
		<input type="text" name="signup" value="true" hidden>
		<input type="text" name="acceptUser" value="true" hidden>
		<div class="row mt-4 input-group mx-0">
        	<input type="text" class="form-control rounded-0" placeholder="이메일 입력">
        </div>
         <label class="err-mess text-info" >이메일 입력조건 안내 문구</label>
        <div class="row mt-2 input-group mx-0">
        	<input type="password" class="form-control rounded-0" placeholder="비밀번호 입력" aria-label="비밀번호 입력" aria-describedby="basic-addon2">
        </div>
        <label class="err-mess text-info" >비밀번호 입력조건 안내 문구</label>
        <div class="row mt-0">
        	<div class=" col-12 nav justify-content-center px-3">
        		<button id="btn-checkName" type="submit" name="submit" value="true" class="btn btn-primary btn-lg btn-block rounded-0">이메일로 시작하기</button>
            </div>
        </div>
	</form>
</div>	
<!--	<div class="row mx-5"><hr class="break-line" /></div>
	<div class="mx-5">
		<div class="input-group mb-3">
            <div class="input-group-prepend">
            	<img class="input-group-text rounded-0 p-0" id="signupFacebook" src="../icon/ico-facebook.png" style="height: 38px; width: 38px;">
            </div>
            <a class="form-control btn btn-primary rounded-0" href="signinSignup.php?fbsignup=true" aria-label="signupFacebook" aria-describedby="signupFacebook">Facebook 계정으로 로그인</a>
        </div>
        
		<div class="input-group mb-3">
            <div class="input-group-prepend">
            	<img class="input-group-text rounded-0 p-0" id="signupGoogle" src="../icon/ico-google.png" style="height: 38px; width: 38px;">
            </div>
            <a class="form-control btn btn-success rounded-0" href="signinSignup.php?ggsignup=true" aria-label="signupGoogle" aria-describedby="signupGoogle">Google 계정으로 로그인</a>
        </div>
        
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            	<img class="input-group-text rounded-0 p-0" id="signupZalo" src="../icon/ico-zalo.png" style="height: 38px; width: 38px;">
            </div>
            <a class="form-control btn btn-info rounded-0" href="signinSignup.php?zlsignup=true" aria-label="signupZalo" aria-describedby="signupZalo">Zalo 계정으로 로그인</a>
        </div>
	</div>-->

<!-- Signup action -->
<?php }else{?>

<div id="signupInfoAction">
	<div class="row justify-content-center">
    	<h2 class="oval-number">1</h2>
    	<h6 class="oval-join"></h6>
    	<h2 class="oval-number">2</h2>
    	<h6 class="oval-join"></h6>
    	<h2 class="oval-number current">3</h2>
    </div>
    <!-- Title -->
    <div class="row justify-content-center mt-3">
    	<h3>기업정보 입력 </h3>
    </div>
    <p class="mx-3 text-right mb-0 pb-0">* 필수입력정보입니다.</p>
    
    <form class="mx-3" action="">
		<input type="text" name="signup" value="false" hidden>
		<div class="form-row">
			<div class="form-group col-12">
				<label for="in-1">기업명</label>
    			<input type="text" class="form-control rounded-0" id="in-1" placeholder="기업 이름 입력 ">
			</div>
            <div class="form-group col-6">
                  <label for="in-2">국가 *</label>
                  <select id="in-2" class="form-control">
                    <option selected>국가 선택</option>
                    <option>...</option>
                  </select>
            </div>
            <div class="form-group col-6">
                  <label for="in-3">산업군 *</label>
                  <select id="in-3" class="form-control">
                    <option selected>산업군 선택</option>
                    <option>...</option>
                  </select>
            </div>
            <div class="form-group col-12">
                <label for="in-4">사업자등록번호 *</label>
                <input type="text" class="form-control" id="in-4" placeholder="‘-’제외 입력 ">
            </div>
        </div>
        <div class="form-group col-12 mx-0 px-0">
        	<label for="in-5">사업자등록증 첨부 *</label>
        	<div class="row input-group mx-0 px-0">
            	<input type="text" id="in-5" class="form-control rounded-0" placeholder="파일명 파일명 파일명 파일명.pdf ">
                <div class="input-group-append">
                	<button class="btn btn-outline-secondary rounded-0" type="button">첨부파일</button>
                </div>
            </div>
            <input type="file" class="custom-file-input" id="in-file" hidden>
        </div>
        <div class="form-row">
			<div class="form-group col-12">
				<label for="in-6">주소  *</label>
    			<input type="text" class="form-control rounded-0" id="in-6" placeholder="대표 주소 입력">
			</div>
			<div class="form-group col-12">
				<label for="in-7">기업 홈페이지 주소</label>
    			<input type="text" class="form-control rounded-0" id="in-7" placeholder="URL 입력">
			</div>
			<div class="form-group col-6">
				<label for="in-6">담당자 이름   *</label>
    			<input type="text" class="form-control rounded-0" id="in-6" placeholder="담당자 이름 입력">
			</div>
			<div class="form-group col-6">
				<label for="in-7">담당자 부서/직급 *</label>
    			<input type="text" class="form-control rounded-0" id="in-7" placeholder="담당자 부서/직급 입력 ">
			</div>
			
			<div class="form-group col-4">
				<label for="in-8">휴대폰 번호 * </label>
    			<select id="in-8" class="form-control">
                    <option selected>010</option>
                    <option>...</option>
                  </select>
			</div> 
			<div class="form-group col-4">
				<label for="in-9">. </label>
    			<input type="text" class="form-control rounded-0" id="in-9" placeholder="">
			</div> 
			<div class="form-group col-4">
				<label for="in-10">. </label>
    			<input type="text" class="form-control rounded-0" id="in-10" placeholder="">
			</div>
			<p class="text-info mb-3">이메일 부적합 사유 안내</p>
			<div class="col-12 mb-3">
                <label for="in-11">담당자 이메일</label>
                <input type="text" class="form-control" id="in-11" placeholder="sample@email.com">
                <p class="text-info mb-0">이메일 부적합 사유 안내</p>
    		</div>
		</div>
        
		 <div class="row mt-4">
        	<div class=" col-12 nav justify-content-center px-3">
        		<button id="btn-signupSubmit" type="submit" name="submit" value="true" class="btn btn-primary btn-lg btn-block rounded-0">승인요청</button>
            </div>
        </div>
	</form>
	
</div>
<script>parent.changeDialogHieght(1050);</script>
<?php }?>
