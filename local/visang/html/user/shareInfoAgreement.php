<?php 
if(!empty($_POST["agreeShareInfo"])){
    if( $_POST["agreeShareInfo"] =="true"){
        echo "<script type='text/javascript'>parent.closeDialog();</script>";
        // do some things when Agree share personal data
        
    }
}
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php include '../component/head-icl.php';?>
</head>
<body>
	<div class="row nav justify-content-center">
		<img alt="LOGO" src="../icon/jobsite.jpg" style="height: 80px;">
	</div>
	<div class="row mt-4">
    	<label class="col-12 mx-5 pl-0">이용약관 동의(필수)</label>
    	<textarea class="col mx-5 txt-h2"></textarea>
    	<div class=" col-12 custom-control custom-checkbox nav justify-content-end pr-5">
            <input type="checkbox" class="custom-control-input rounded-0" id="chkAgreeShareInfo">
            <label class="custom-control-label" for="chkAgreeShareInfo">동의</label>
        </div>
    </div>
    
    <div class="row mt-4">
    	<form action="" method="post" enctype="multipart/form-data" >
        	<div class=" col-12 nav justify-content-center px-5">
        		<button type="submit" name="agreeShareInfo" value="true" id="checkAgreeShareInfo" class="btn btn-light mx-5 px-5" disabled>확인</button>
        		<h6 id="agreeShareInfo" class="px-0 pt-4">정보 공유에 동의하지 않으면 해당 계정으로 구인구직 사이트를 이용할 수 없습니다. </h6>
            </div>
        </form>
    </div>
    
</body>
<?php include '../component/foot-icl.php';?>
</html>