
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Profile</title>
 <?php include '../component/head-icl.php';?>
</head>
<body class="">	
	<h3 class="text-center"><b>내 정보</b></h3>
	<div class="border border-dark">
		<div class="row mx-4 mt-4 pb-3 d-flex justify-content-between">
    		<h4><b>김비상</b>&ensp;</h4>
    		<a href="#" class="btn btn-outline-dark" role="button" aria-pressed="true">개인정보수정</a>
    	</div>
    	<div class="row ml-3">
    		<div class="col-2">
    			<p><b>가입일</b></p>
        		<p><b>아이디</b></p>
        		<p><b>이메일</b></p>
        		<p><b>휴대폰 번호</b></p>
        	</div>
        	<div class="col-10">
        		<p>2018.07.23</p>
        		<p><i class="fab fa-facebook-square"></i> visang@facebook.com </p>
        		<p>visang@gmail.com </p>
        		<p>0111-222-333</p>
        	</div>
    	</div>
	</div>
	<h4 class="mt-4 pb-3">내 이력서</h4>
	<div class="border border-dark mb-3">
		<div class="row ml-3 mt-3">
    		<div class="col-2">
    			<p><b>이력서  </b></p>
        	</div>
        	<div class="col-10">
        		<form action="../user/usrProfile.php" class="pr-4">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="pdfFile" required>
                        <label class="custom-file-label" for="pdfFile">김비상 이력서.pdf</label>
                    </div>
                    <p>10MB 미만의 .doc .docx .pdf 파일만 업로드 가능합니다.</p>
                    <div class="row mx-2 mt-2 d-flex justify-content-start">
                		<a href="#" class="btn btn-outline-dark mr-2" role="button" aria-pressed="true">파일 업로드</a>
                		<a href="#" class="btn btn-outline-dark mr-2" role="button" aria-pressed="true">삭제</a>
                		<a href="#" class="btn btn-outline-link mr-2" role="button" aria-pressed="true"><ins>이력서 보기</ins></a>
                	</div>
        		</form>
        	</div>
    	</div>
    	<div class="row mx-5"><hr class="break-line" /></div>
    	<div class="row ml-3">
    		<div class="col-2">
    			<p><b>동영상 프로필   </b></p>
        	</div>
        	<div class="col-10">
        		<form action="../user/usrProfile.php" class="pr-4">
                    <input type="text" class="form-control" id="youtubeURL" aria-describedby="youtubeURL" placeholder="유튜브 동영상 URL만 입력">
                    <div class="row mx-2 mt-2 d-flex justify-content-start">
                    	<p>한국어 강좌를 수강한 사용자만 업로드할 수 있습니다.</p>
                    	<a href="#">동영상 프로필 가이드 보기</a>
                    </div>
                    <div class="row mx-2 mt-2 d-flex justify-content-start">
                		<a href="#" class="btn btn-outline-dark mr-2" role="button" aria-pressed="true">등록</a>
                		<a href="#" class="btn btn-outline-dark mr-2" role="button" aria-pressed="true">삭제</a>
                		<a href="#" class="btn btn-outline-link mr-2" role="button" aria-pressed="true"><ins>동영상 보기</ins></a>
                	</div>
        		</form>
        	</div>
    	</div>
	</div>
</body>
<?php include '../component/foot-icl.php';?>
</html>

<script>
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
</script>