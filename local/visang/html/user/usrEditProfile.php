
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Profile</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="ml-1">	
	
	<h3 ><b>개인정보수정 </b></h3>
	<p class="text-right pt-4">* 표시된 항목은 필수입력 항목입니다.</p>
	<div class="border border-dark pt-3">
    	<div class="row ml-3">
    		<div class="col-2">
    			<p><b>이름</b></p>
        		<p><b>ID</b></p>
        		<p><b>사업자등록번호</b></p>
        		<p><b>가입</b></p>
        	</div>
        	<div class="col-10 text-primary">
        		<p>비상교육</p>
        		<p>visang@facebook.com </p>
        		<p>0000-00-0000-00</p>
        		<p>2017.11.11</p>
        	</div>
    	</div>
	</div>
	
	<form class="mt-4" action="">		
        <div class="form-row">
			<div class="form-group col-6">
				<label for="in-6"><b>담당자 이름</b>   *</label>
    			<input type="text" class="form-control rounded-0" id="in-6" placeholder="홍길동">
			</div>
			<div class="form-group col-6">
				<label for="in-7"><b>담당자 부서/직급</b> *</label>
    			<input type="text" class="form-control rounded-0" id="in-7" placeholder="인사팀 과장 ">
			</div>
			
			<div class="form-group col-4">
				<label for="in-8"><b>휴대폰 번호</b> * </label>
    			<select id="in-8" class="form-control">
                    <option selected>010</option>
                    <option>...</option>
                  </select>
			</div> 
			<div class="form-group col-4">
				<label for="in-9">. </label>
    			<input type="text" class="form-control rounded-0" id="in-9" placeholder="">
			</div> 
			<div class="form-group col-4">
				<label for="in-10">. </label>
    			<input type="text" class="form-control rounded-0" id="in-10" placeholder="">
			</div>
			<p class="text-info mb-3">이메일 부적합 사유 안내</p>
			<div class="col-12 mb-3">
                <label for="in-11"><b>담당자 이메일</b></label>
                <input type="text" class="form-control" id="in-11" placeholder="sample@email.com">
                <p class="text-info mb-0">이메일 부적합 사유 안내</p>
    		</div>
    		
    		<div class="col-12 mb-3">
                <label for=""><b>비밀번호 변경 </b></label>
                <a class="btn btn-info btn-lg rounded-0 ml-4 text-white">비밀번호 변경</a>
    		</div>
    		
    		<div class="col-12 mb-3">
                <label class="d-inline" for=""><b>마지막 정보변경일  </b></label>
                <p class="text-primary d-inline ml-4" >2019.10.23</p>
    		</div>
    		<p> ※ 기타 기업정보 변경이 필요한 경우 담당자에게 문의해주세요.  </p>
		</div>
        
        
		 <div class="row mt-4">
        	<div class=" col-12 nav justify-content-center px-3">
        		<button id="checkAgree" type="submit" name="submit" value="true" class="btn btn-primary btn-lg rounded-0">수정 </button>
            </div>
        </div>
	</form>
	
</body>


<?php include '../component/foot-icl.php';?>
</html>

<script>
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
</script>