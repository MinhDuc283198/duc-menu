

<?php 
$hasInterview = true;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visang-Profile</title>
 <?php include '../component/head-icl.php';?>
</head>
	
<body class="">	
	<h3 class="text-center"><b>1:1 상담내역</b></h3>
	<div class="row  justify-content-end">
		<a href="#" class="btn btn-outline-dark btn-sm mt-0 mr-4 mb-3" role="button" aria-pressed="true">글 작성</a>
	</div>
	
	<table class="table table-bordered">
      <thead class="thead-light">
        <tr>
          <th scope="col">번호</th>
          <th scope="col" class="text-center">제목</th>
          <th scope="col" class="text-center">작성자</th>
          <th scope="col" class="text-center">작성일</th>
          <th scope="col" class="text-center">조회수</th>
        </tr>
      </thead>
      <tbody>
      <?php 
      if($hasInterview){
        for($i=5; $i>0; $i--){
      ?>
        <tr>
            <th scope="row" class="text-center"><?php echo $i?></th>
            <td><a href="#" onclick="parent.viewUserProfile(this,'../user/usrReplyInterview.php?reply=true&viewID=<?php $i?>');">동영상 프로필은 아무거나 등록하면 되는건가요?</a></td>
            <td class="text-center align-middle">김비상</td>
            <td class="text-center align-middle">2019.10.22</td>
            <td class="text-center align-middle">2</td>
        </tr>
      <?php 
        }
      }else{?>
          <tr>
            <td class="text-center align-middle" colspan="5">1:1 상담내역이 없습니다.</td>
          </tr>
      <?php }?>
      </tbody>
    </table>
    <div class="row d-flex justify-content-center">
    	<a href="#">&lt;</a>
    	
    	<?php for($i=1; $i<11; $i++){?>
    	<a href="#">&ensp;<?php echo $i;?>&ensp;</a>
    	<?php }?>
    	
		<a href="#">&gt;</a>
	</div>
</body>
<?php include '../component/foot-icl.php';?>
</html>

<script>
	var h = $(document).height();
	parent.$("#usr-iframe").css("height",h+"px");
</script>