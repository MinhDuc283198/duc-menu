<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../../config.php');

function local_visang_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array())
{
    global $CFG, $DB;

    $fileareas = array('images', 'docs');
    if (!in_array($filearea, $fileareas)) return false;

    $itemid = array_shift($args);
    $filename = array_pop($args);

    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_visang', $filearea, $itemid, '/', $filename);
    if (!$file) return false;

    send_stored_file($file, 86400, 0, false, $options);
}
