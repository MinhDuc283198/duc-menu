<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Created by kbjung112@gmail.com on 2021-07-19
 * Github : https://github.com/iland112
 */

/**
 * Event class implementation dispatched when coupons are used.
 *
 * File         job_posted.php
 * Encoding     UTF-8
 *
 * @package     local_visang
 *
 * @copyright   bzcom.vn
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/
namespace local_visang\event;

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__) . '/../../../job/lib.php');

/**
 * local_visang\event\job_posted
 *
 * @package     local_visang
 *
 * @copyright   bzcom.vn
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class job_posted extends \core\event\base {

    protected function init()
    {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'vi_jobs';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "The user with id '$this->userid' has posted the job with id '$this->objectid'.";
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event:jobposted', 'local_visang');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        global $VISANG;
        new moodle_url($VISANG->wwwroot . '/employer/new_job.php');
        $url->set_anchor('p'.$this->objectid);
        return $url;
    }

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();

        if (!isset($this->other['jobid'])) {
            throw new \coding_exception('The \'jobid\' value must be set in other.');
        }

        if ($this->contextlevel != CONTEXT_SYSTEM) {
            throw new \coding_exception('Context level must be CONTEXT_SYSTEM.');
        }
    }
}
