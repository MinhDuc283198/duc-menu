<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Created by kbjung112@gmail.com on 2021-07-19
 * Github : https://github.com/iland112
 */

/**
 * Event observers used in local visang plugin.
 *
 * @package    local_visang
 * @copyright  2021 bzcom.vn
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(__FILE__) . '/../lib.php');
defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for local_visang.
 */

class local_visang_observer
{
    public static function job_posted(\local_visang\event\job_posted $event)
    {
        global $DB, $VISANG,$CFG;
        $email_user = $DB->get_records('user', array('isreceivejob' => 1));
        $v = " /^[A-Za-z0-9_.]{6,32}@([a-zA-Z0-9]{2,12})(.[a-zA-Z]{2,12})+$/";
        $jobid = $event->objectid;
        $message = $CFG->wwwroot . '/local/job/jobs/view.php?jobid=' . $jobid;
        $messagehtml = '<a href="' . $message . '">click here to see new job</a>';
        $supportuser = core_user::get_support_user();
        $subject = 'Master korean has a new job for you';
        foreach ($email_user as $eu) {
            if (!is_siteadmin($eu->id)) {
                $eu->mailformat = 1;  // Always send HTML version as well.
                $eu->email = $DB->get_field('lmsdata_user', 'email2', array('userid' => $eu->id));
                email_to_user($eu, $supportuser, $subject, $message, $messagehtml);
            }
        }
    }
}
