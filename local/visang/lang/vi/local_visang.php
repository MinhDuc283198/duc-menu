<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Visang';
$string['settings'] = 'Settings';
$string['showperpage'] = 'Show {$a} per page';
$string['oftotalrows'] = '({$a->from}-{$a->to} of {$a->total})';
$string['empty:data'] = 'Data is empty';

$string['attachment'] = 'Attachment';

$string['visang:manage'] = 'Visang';
$string['visang:dashboard'] = 'Dashboard';


$string['settings:cdnbaseurl'] = 'cdn_baseurl';
$string['settings:cdnjwttoken'] = 'cdn_JWTtoken';
$string['settings:settings'] = 'Visang settings';
$string['settings:google_map_api'] = 'Google Map API';
$string['settings:partnerCode'] = 'partnerCode';
$string['settings:accessKey'] = 'accessKey';
$string['settings:secretKey'] = 'secretKey';
$string['settings:facebooklogin'] = 'facebook client key';
$string['settings:googlelogin'] = 'google client key';
$string['settings:facebooksecret'] = 'facebook secret key';
$string['settings:googlesecret'] = 'google secret key';
$string['settings:brandname'] = 'Brand name';
$string['settings:clientid'] = 'Client ID';
$string['settings:clientsecret'] = 'Client Secret';


$string['skills'] = 'Kỹ năng';
$string['skills:list'] = 'Danh sách kỹ năng';
$string['skills:group'] = 'Lĩnh vực';
$string['skills:add_skill'] = 'Thêm kỹ năng';
$string['skills:edit_skill'] = 'Sửa kỹ năng';
$string['skills:add_group'] = 'Thêm lĩnh vực';
$string['skills:edit_group'] = 'Sửa lĩnh vực';

$string['employers'] = 'Nhà tuyển dụng';
$string['employers:companysizes'] = 'Quy mô công ty';
$string['employers:companysizes_add'] = 'Thêm quy mô';
$string['employers:companysizes_edit'] = 'Sửa quy mô';
//
$string['employers:workingdatecategories'] = 'Thời gian làm việc';
$string['employers:workingdatecategories_add'] = 'Thêm thời gian làm việc';
$string['employers:workingdatecategories_edit'] = 'Sửa thời gian làm việc';
//
$string['employers:overtimecategories'] = 'Làm thêm giờ';
$string['employers:overtimecategories_add'] = 'Thêm làm thêm giờ';
$string['employers:overtimecategories_edit'] = 'Sửa làm thêm giờ';
//
$string['employers:companytypes'] = 'Loại hình công ty';
$string['employers:companytypes_add'] = 'Thêm loại hình công ty';
$string['employers:companytypes_edit'] = 'Sửa loại hình công ty';
//
$string['employers:countries'] = 'Quốc gia';
$string['employers:countries_add'] = 'Thêm quốc gia';
$string['employers:countries_edit'] = 'Sửa quốc gia';
//
$string['employers:districts'] = 'Quận, huyện';
$string['employers:districts_add'] = 'Thêm quận, huyện';
$string['employers:districts_edit'] = 'Sửa quận, huyện';
//
$string['employers:reasoncategories'] = 'Quan điểm';
$string['employers:reasoncategories_add'] = 'Thêm quan điểm';
$string['employers:reasoncategories_edit'] = 'Sửa quan điểm';
//
$string['employers:jobsalarycategories'] = 'Lương';
$string['employers:jobsalarycategories_add'] = 'Thêm lương';
$string['employers:jobsalarycategories_edit'] = 'Sửa lương';
//
$string['employers:employers'] = 'Nhà tuyển dụng';
$string['employers:employers_add'] = 'Thêm nhà tuyển dụng';
$string['employers:employers_edit'] = 'Sửa nhà tuyển dụng';
$string['employers:employers_company_name'] = 'Tên công ty';
$string['employers:employers_company_address'] = 'Địa chỉ công ty';
$string['employers:employers_company_type'] = 'Loại hình công ty';
$string['employers:employers_company_size'] = 'Quy mô công ty';
$string['employers:employers_country'] = 'Quốc gia';
$string['employers:employers_working_date_category'] = 'Thời gian làm việc';
$string['employers:employers_overtime_category'] = 'Làm thêm giờ';
$string['employers:employers_company_short_description'] = 'Mô tả ngắn';
$string['employers:employers_company_description'] = 'Mô tả đầy đủ';
$string['employers:employers_skills'] = 'Từ khóa kỹ năng';
$string['employers:employers_reasons'] = 'Chúng tôi sẽ tìm công việc tốt cho bạn';
$string['employers:employers_top_reasons'] = 'Ưu điểm';
$string['employers:employers_top_reasons_help'] = 'Chọn quan điểm đã có hoặc thêm mới';
$string['employers:employers_reason_description'] = 'Lợi ích';
$string['employers:employers_photos'] = 'Hình ảnh';
$string['employers:employers_company_logo'] = 'Biểu tượng';
$string['employers:employers_company_cover'] = 'Cover';
$string['employers:employers_company_photos'] = 'Hình ảnh trình chiếu';
$string['employers:employers_locations'] = 'Địa chỉ';
$string['employers:employers_links'] = 'Liên kết';
$string['employers:employers_url'] = 'URL trên hệ thống';
$string['employers:employers_company_website'] = 'Địa chỉ Website';
$string['employers:employers_company_facebook'] = 'Liên kết Facebook';
$string['employers:employers_company_twitter'] = 'Liên kết Twitter';
$string['employers:employers_confirmed'] = 'Nhà tuyển dụng đã xác nhận';
$string['employers:employers_published'] = 'Công khai nhà tuyển dụng';
$string['employers:employers_deleted'] = 'Tạm ẩn nhà tuyển dụng';
$string['employers:employers_admin'] = 'Quản lý';

$string['jobs'] = 'Việc làm';
$string['jobs:add_job'] = 'Thêm việc làm';
$string['jobs:edit_job'] = 'Sửa việc làm';
$string['jobs:title'] = 'Tiêu đề việc làm';
$string['jobs:short_description'] = 'Mô tả ngắn';
$string['jobs:job_skills'] = 'Kỹ năng';
$string['jobs:job_salary'] = 'Lương';
$string['jobs:location'] = 'Địa chỉ';
$string['jobs:job_reasons'] = 'Ưu điểm';
$string['jobs:description'] = 'Mô tả công việc';
$string['jobs:skills_experience'] = 'Yêu cầu công việc';
$string['jobs:culture_description'] = 'Ưu tiên';
$string['jobs:hot_job'] = 'Việc làm phổ biến';
$string['jobs:confirmed'] = 'Xác nhận';
$string['jobs:published'] = 'Công khai';
$string['jobs:deleted'] = 'Xóa';

$string['jobs:work_experience'] = 'Kinh nghiệm làm việc';
$string['jobs:work_type'] = 'Hình thức làm việc';
$string['jobs:korean_level'] = 'Trình độ tiếng Hàn';

$string['jobs:applications'] = 'Ứng viên';
$string['jobs:applications_all'] = 'Tất cả ứng viên';
$string['jobs:applications_candidate_fullname'] = 'Tên đầy đủ';

$string['locations'] = 'Địa chỉ';
$string['locations:add_location'] = 'Thêm địa chỉ';
$string['locations:edit_location'] = 'Sửa địa chỉ';
$string['locations:full_address'] = 'Địa chỉ đầy đủ';
$string['locations:location_on_map'] = 'Vị trí trên bản đồ';
$string['locations:district'] = 'Quận/huyện';
$string['locations:country'] = 'Quốc gia';
$string['locations:edit_on_map'] = 'Sửa trên bản đồ';
$string['locations:view_on_map'] = 'Xem trên bản đồ';

$string['reviews'] = 'Đánh giá';
$string['reviews:title'] = 'Tiêu đề';
$string['reviews:review_content'] = 'Nội dung';
$string['reviews:feedback_content'] = 'Phản hồi';

$string['followers'] = 'Người theo dõi';

$string['users'] = 'Người dùng';
$string['users:add_user'] = 'Thêm người dùng';
$string['users:edit_user'] = 'Sửa người dùng';

$string['ratings'] = 'Xếp hạng';
$string['ratings:categories'] = 'Danh mục xếp hạng';
$string['ratings:add_rating'] = 'Thêm xếp hạng';
$string['ratings:edit_rating'] = 'Sửa xếp hạng';

$string['statitics:period'] = 'Thời gian';
$string['statitics:access'] = 'Thống kê người dùng truy cập';
$string['statitics:year'] = 'Năm';
$string['statitics:month'] = 'Tháng';
$string['statitics:week'] = 'Tuần';
$string['statitics:dayofweek'] = 'Ngày';
$string['statitics:access_count'] = 'Số lượng truy cập';
$string['statitics:total'] = 'Tổng cộng';
$string['statitics:Mon'] = 'Thứ hai';
$string['statitics:Tue'] = 'Thứ ba';
$string['statitics:Wed'] = 'Thứ tư';
$string['statitics:Thu'] = 'Thứ năm';
$string['statitics:Fri'] = 'Thứ sáu';
$string['statitics:Sat'] = 'Thứ bảy';
$string['statitics:Sun'] = 'Chủ nhật';
