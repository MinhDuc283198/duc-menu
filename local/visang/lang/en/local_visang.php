<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Visang';
$string['settings'] = 'Settings';
$string['showperpage'] = 'Show {$a} per page';
$string['oftotalrows'] = '({$a->from}-{$a->to} of {$a->total})';
$string['empty:data'] = 'Data is empty';

$string['attachment'] = 'Attachment';

$string['visang:manage'] = 'Visang';
$string['visang:dashboard'] = 'Dashboard';


$string['settings:cdnbaseurl'] = 'cdn_baseurl';
$string['settings:cdnjwttoken'] = 'cdn_JWTtoken';
$string['settings:settings'] = 'Visang settings';
$string['settings:google_map_api'] = 'Google Map API';
$string['settings:partnerCode'] = 'partnerCode';
$string['settings:accessKey'] = 'accessKey';
$string['settings:secretKey'] = 'secretKey';
$string['settings:facebooklogin'] = 'facebook client key';
$string['settings:googlelogin'] = 'google client key';
$string['settings:facebooksecret'] = 'facebook secret key';
$string['settings:googlesecret'] = 'google secret key';
$string['settings:brandname'] = 'Brand name';
$string['settings:clientid'] = 'Client ID';
$string['settings:clientsecret'] = 'Client Secret';

$string['skills'] = 'Skills';
$string['skills:list'] = 'List of skills';
$string['skills:group'] = 'Group of skills';
$string['skills:add_skill'] = 'Add a new skill';
$string['skills:edit_skill'] = 'Edit skill';
$string['skills:add_group'] = 'Add a new skill group';
$string['skills:edit_group'] = 'Edit skill group';

$string['employers'] = 'Employers';
$string['employers:companysizes'] = 'Company sizes';
$string['employers:companysizes_add'] = 'Add a new company size';
$string['employers:companysizes_edit'] = 'Edit company size';
//
$string['employers:workingdatecategories'] = 'Working date categories';
$string['employers:workingdatecategories_add'] = 'Add a new working date category';
$string['employers:workingdatecategories_edit'] = 'Edit working date category';
//
$string['employers:overtimecategories'] = 'Overtime categories';
$string['employers:overtimecategories_add'] = 'Add a new overtime category';
$string['employers:overtimecategories_edit'] = 'Edit overtime category';
//
$string['employers:companytypes'] = 'Company types';
$string['employers:companytypes_add'] = 'Add a new company type';
$string['employers:companytypes_edit'] = 'Edit company type';
//
$string['employers:countries'] = 'Countries';
$string['employers:countries_add'] = 'Add a new country';
$string['employers:countries_edit'] = 'Edit country';
//
$string['employers:districts'] = 'Districts';
$string['employers:districts_add'] = 'Add a new district';
$string['employers:districts_edit'] = 'Edit district';
//
$string['employers:reasoncategories'] = 'Reason categories';
$string['employers:reasoncategories_add'] = 'Add a new reason category';
$string['employers:reasoncategories_edit'] = 'Edit reason category';
//
$string['employers:jobsalarycategories'] = 'Salary categories';
$string['employers:jobsalarycategories_add'] = 'Add a new salary';
$string['employers:jobsalarycategories_edit'] = 'Edit salary';
//
$string['employers:employers'] = 'Employers';
$string['employers:employers_add'] = 'Add a new employer';
$string['employers:employers_edit'] = 'Edit employer';
$string['employers:employers_company_name'] = 'Company name';
$string['employers:employers_company_address'] = 'Company address';
$string['employers:employers_company_type'] = 'Company type';
$string['employers:employers_company_size'] = 'Company size';
$string['employers:employers_country'] = 'Company of the country';
$string['employers:employers_working_date_category'] = 'Working date';
$string['employers:employers_overtime_category'] = 'About working overtime';
$string['employers:employers_company_short_description'] = 'Short description';
$string['employers:employers_company_description'] = 'Full description';
$string['employers:employers_skills'] = 'Key Skills';
$string['employers:employers_reasons'] = 'We\'ll find the right job for you';
$string['employers:employers_top_reasons'] = 'Top reasons';
$string['employers:employers_top_reasons_help'] = 'Select Top reasons created or add new... (p/s: Separate line breaks.)';
$string['employers:employers_reason_description'] = 'Benefits';
$string['employers:employers_photos'] = 'Photos';
$string['employers:employers_company_logo'] = 'Logo';
$string['employers:employers_company_cover'] = 'Cover';
$string['employers:employers_company_photos'] = 'Photos for slideshow';
$string['employers:employers_locations'] = 'Locations';
$string['employers:employers_links'] = 'Links';
$string['employers:employers_url'] = 'URL address on the system';
$string['employers:employers_company_website'] = 'Website link';
$string['employers:employers_company_facebook'] = 'Facebook link';
$string['employers:employers_company_twitter'] = 'Twitter link';
$string['employers:employers_confirmed'] = 'Employers has been confirmed';
$string['employers:employers_published'] = 'Publish employers on the system';
$string['employers:employers_deleted'] = 'Offline employers on the system';
$string['employers:employers_admin'] = 'Managers';

$string['jobs'] = 'Jobs';
$string['jobs:add_job'] = 'Add a new job';
$string['jobs:edit_job'] = 'Edit job';
$string['jobs:title'] = 'Job title';
$string['jobs:short_description'] = 'Short description';
$string['jobs:job_skills'] = 'Skills';
$string['jobs:job_salary'] = 'Salary';
$string['jobs:location'] = 'Locations';
$string['jobs:job_reasons'] = 'Top reasons';
$string['jobs:description'] = 'Job description';
$string['jobs:skills_experience'] = 'Qualification requirement';
$string['jobs:culture_description'] = 'Give preference to';
$string['jobs:hot_job'] = 'Hot job';
$string['jobs:confirmed'] = 'Confirmed';
$string['jobs:published'] = 'Published';
$string['jobs:deleted'] = 'Deleted';

$string['jobs:work_experience'] = '경력 여부';
$string['jobs:work_type'] = '근무형태';
$string['jobs:korean_level'] = '한국어 활용 수준';

$string['jobs:applications'] = 'Applications';
$string['jobs:applications_all'] = 'All applications';
$string['jobs:applications_candidate_fullname'] = 'Fullname';

$string['locations'] = 'Locations';
$string['locations:add_location'] = 'Add a new location';
$string['locations:edit_location'] = 'Edit location';
$string['locations:full_address'] = 'Address';
$string['locations:location_on_map'] = 'Location on google map';
$string['locations:district'] = 'City/District';
$string['locations:country'] = 'Country';
$string['locations:edit_on_map'] = 'Edit on map';
$string['locations:view_on_map'] = 'View on map';

$string['reviews'] = 'Reviews';
$string['reviews:title'] = 'Title';
$string['reviews:review_content'] = 'Content';
$string['reviews:feedback_content'] = 'Feedback';

$string['followers'] = 'Followers';

$string['users'] = 'Users';
$string['users:add_user'] = 'Add a new user';
$string['users:edit_user'] = 'Edit user';

$string['ratings'] = 'Ratings';
$string['ratings:categories'] = 'Rating categories';
$string['ratings:add_rating'] = 'Add a new rating';
$string['ratings:edit_rating'] = 'Edit rating';

$string['statitics:period'] = 'Period';
$string['statitics:access'] = 'User access statistics';
$string['statitics:year'] = 'Year';
$string['statitics:month'] = 'Month';
$string['statitics:week'] = 'Week';
$string['statitics:dayofweek'] = 'Day of the week';
$string['statitics:access_count'] = 'Access Count';
$string['statitics:total'] = 'Total';
$string['statitics:Mon'] = 'Mon';
$string['statitics:Tue'] = 'Tue';
$string['statitics:Wed'] = 'Wed';
$string['statitics:Thu'] = 'Thu';
$string['statitics:Fri'] = 'Fri';
$string['statitics:Sat'] = 'Sat';
$string['statitics:Sun'] = 'Sun';

