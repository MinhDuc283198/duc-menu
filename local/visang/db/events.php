<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Created by kbjung112@gmail.com on 2021-07-19
 * Github : https://github.com/iland112
 */

/**
 * Event handlers for local_visang
 *
 * File         events.php
 * Encoding     UTF-8
 *
 * @package     local_visang
 *
 * @copyright   bzcom.vn
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/

defined('MOODLE_INTERNAL') || die;

$observers = array(
    array(
        'eventname' => '\local_visang\event\job_posted',
        'callback' => 'local_visang_observer::job_posted',
    ),
);
