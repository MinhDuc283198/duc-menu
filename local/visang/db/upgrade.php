<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_visang_upgrade($oldversion)
{
    global $CFG, $DB, $OUTPUT;
    $dbman = $DB->get_manager();

    if ($oldversion < 2019122601) {
        if (!$DB->record_exists('vi_settings', array('name' => 'google_map_api'))) {
            $DB->execute("INSERT INTO {vi_settings} VALUES (null, 'google_map_api', '')");
        }
        if (!$DB->record_exists('vi_settings', array('name' => 'partnerCode'))) {
            $DB->execute("INSERT INTO {vi_settings} VALUES (null, 'partnerCode', '')");
        }
        if (!$DB->record_exists('vi_settings', array('name' => 'accessKey'))) {
            $DB->execute("INSERT INTO {vi_settings} VALUES (null, 'accessKey', '')");
        }
        if (!$DB->record_exists('vi_settings', array('name' => 'secretKey'))) {
            $DB->execute("INSERT INTO {vi_settings} VALUES (null, 'secretKey', '')");
        }

        $table = new xmldb_table('vi_jobs');
        $field = new xmldb_field('startdate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('enddate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('skill_group_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    // if ($oldversion < 2019123000) {
    //     $table = new xmldb_table('vi_resumes');
    //     $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    //     $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
    //     $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    //     $table->add_field('value', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
    //     $table->add_field('published', XMLDB_TYPE_INTEGER, '4', null, null, null, 0);
    //     $table->add_field('view_count', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
    //     $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
    //     $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

    //     $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

    //     if (!$dbman->table_exists($table)) {
    //         $dbman->create_table($table);
    //     }
    // }

    if ($oldversion < 2020010100) {
        $table = new xmldb_table('vi_korean_levels');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('short_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);

            $DB->execute("INSERT INTO {vi_korean_levels} VALUES (null, '한국어 못함', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_korean_levels} VALUES (null, 'TOPIK 1/2 급', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_korean_levels} VALUES (null, 'TOPIK 3/4 급', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_korean_levels} VALUES (null, 'TOPIK 5/6 급', '', 0, null, null)");
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_korean_levels');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('korean_level_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_skills');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('skill_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_skill_groups');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('skill_group_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_search_keywords');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('search_keyword', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2020010800) {
        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_company_types');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('company_type_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2020013100) {
        $table = new xmldb_table('vi_work_experiences');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('short_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);

            $DB->execute("INSERT INTO {vi_work_experiences} VALUES (null, '신입', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_work_experiences} VALUES (null, '경력', '', 0, null, null)");
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_work_experiences');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('work_experience_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_workplaces');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('district_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2020020300) {
        $table = new xmldb_table('vi_resumes');
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        $table = new xmldb_table('vi_resumes');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('avatar', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('attachment_url', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('video_url', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('draft', XMLDB_TYPE_INTEGER, '4', null, null, null, 0);
        $table->add_field('is_default', XMLDB_TYPE_INTEGER, '4', null, null, null, 0);
        $table->add_field('published', XMLDB_TYPE_INTEGER, '4', null, null, null, 0);
        $table->add_field('view_count', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_resume_fields');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('resume_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('field_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('field_group', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_resume_field_values');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('field_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('field_key', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('field_value', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2020021401) {
        $table = new xmldb_table('vi_employer_skill_groups');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('employer_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('skill_group_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_work_types');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('short_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
            $DB->execute("INSERT INTO {vi_work_types} VALUES (null, '정규직', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_work_types} VALUES (null, '계약직', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_work_types} VALUES (null, '파트타이머', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_work_types} VALUES (null, '인턴', '', 0, null, null)");
            $DB->execute("INSERT INTO {vi_work_types} VALUES (null, '프리랜서', '', 0, null, null)");
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_job_work_types');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('job_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('work_type_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $DB->execute("INSERT INTO {vi_work_experiences} VALUES (null, '무관', '', 0, null, null)");

        $table = new xmldb_table('vi_jobs');
        $field = new xmldb_field('work_experience_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('korean_level_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('special_info', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020022400) {
        $table = new xmldb_table('vi_final_educations');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('short_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);

            $DB->execute("INSERT INTO {vi_final_educations} VALUES (null, '고등학교 졸업', '', 0, null, null)");
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_user_final_educations');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('final_education_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_jobs');
        $field = new xmldb_field('final_education_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('age_from', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('age_to', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020022600) {
        $table = new xmldb_table('vi_final_educations');
        $field = new xmldb_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $DB->execute("DELETE FROM {vi_final_educations} WHERE 1=1");
        $DB->execute("INSERT INTO {vi_final_educations} VALUES (null, '학교', '', 0, null, null, '학교','School','Trường')");
        $DB->execute("INSERT INTO {vi_final_educations} VALUES (null, '고등학교', '', 0, null, null, '고등학교','High school','Phổ thông')");
        $DB->execute("INSERT INTO {vi_final_educations} VALUES (null, '칼리지', '', 0, null, null, '칼리지','College','Cao đẳng')");
        $DB->execute("INSERT INTO {vi_final_educations} VALUES (null, '대학교', '', 0, null, null, '대학교','University','Đại học')");

        // ==============================================================================================================================
        $table = new xmldb_table('vi_work_experiences');
        $field = new xmldb_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_korean_levels');
        $field = new xmldb_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020022601) {
        // ==============================================================================================================================
        $row = new stdClass();
        $row->id = 1;
        $row->name = '한국어 못함';
        $row->name_ko = '한국어 못함';
        $row->name_en = "I can't speak Korean";
        $row->name_vi = 'Tôi không thể nói tiếng hàn';
        $DB->update_record('vi_korean_levels', $row);

        $row = new stdClass();
        $row->id = 2;
        $row->name = 'TOPIK 1/2 급';
        $row->name_ko = 'TOPIK 1/2 급';
        $row->name_en = "TOPIK 1/2";
        $row->name_vi = 'TOPIK 1/2';
        $DB->update_record('vi_korean_levels', $row);

        $row = new stdClass();
        $row->id = 3;
        $row->name = 'TOPIK 3/4 급';
        $row->name_ko = 'TOPIK 3/4 급';
        $row->name_en = "TOPIK 3/4";
        $row->name_vi = 'TOPIK 3/4';
        $DB->update_record('vi_korean_levels', $row);

        $row = new stdClass();
        $row->id = 4;
        $row->name = 'TOPIK 5/6 급';
        $row->name_ko = 'TOPIK 5/6 급';
        $row->name_en = "TOPIK 5/6";
        $row->name_vi = 'TOPIK 5/6';
        $DB->update_record('vi_korean_levels', $row);

        // ==============================================================================================================================
        $row = new stdClass();
        $row->id = 1;
        $row->name = '신입';
        $row->name_ko = '신입';
        $row->name_en = "Newbie";
        $row->name_vi = 'Chưa có kinh nghiệm';
        $DB->update_record('vi_work_experiences', $row);

        $row = new stdClass();
        $row->id = 2;
        $row->name = '경력';
        $row->name_ko = '경력';
        $row->name_en = "Career";
        $row->name_vi = 'Đã đi làm';
        $DB->update_record('vi_work_experiences', $row);

        $row = new stdClass();
        $row->id = 3;
        $row->name = '무관';
        $row->name_ko = '무관';
        $row->name_en = "Other";
        $row->name_vi = 'Khác';
        $DB->update_record('vi_work_experiences', $row);
    }

    if ($oldversion < 2020022700) {
        $rows = $DB->get_records('tool_customlang', array('lang' => 'vi', 'stringid' => 'err_required'));
        foreach ($rows as $row) {
            $row->master = 'Vui lòng nhập giá trị.';
            $DB->update_record('tool_customlang', $row);
        }
    }

    if($oldversion < 2020030500) {
        $table = new xmldb_table('vi_countries');
        $field = new xmldb_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $rows = $DB->get_records('vi_countries');
        foreach ($rows as $row) {
            $row->name_ko = $row->name;
            $row->name_en = $row->name;
            $row->name_vi = $row->name;
            $DB->update_record('vi_countries', $row);
        }

        // ==============================================================================================================================
        $table = new xmldb_table('vi_districts');
        $field = new xmldb_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $rows = $DB->get_records('vi_districts');
        foreach ($rows as $row) {
            $row->name_ko = $row->name;
            $row->name_en = $row->name;
            $row->name_vi = $row->name;
            $DB->update_record('vi_districts', $row);
        }
    }

    if($oldversion < 2020030600) {
        $table = new xmldb_table('vi_ratings');
        $field = new xmldb_field('title_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('title_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('title_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $rows = $DB->get_records('vi_ratings');
        foreach ($rows as $row) {
            $row->title_ko = $row->title;
            $row->title_en = $row->title;
            $row->title_vi = $row->title;
            $DB->update_record('vi_ratings', $row);
        }
    }

    if($oldversion < 2020031000) {
        $table = new xmldb_table('vi_work_types');
        $field = new xmldb_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $rows = $DB->get_records('vi_work_types');
        foreach ($rows as $row) {
            $row->name_ko = $row->name;
            $row->name_en = $row->name;
            $row->name_vi = $row->name;
            $DB->update_record('vi_work_types', $row);
        }

        $table = new xmldb_table('vi_jobs');
        $field = new xmldb_field('other_request_info', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('other_lang_info', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('other_address_info', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('note', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020031200) {
        $row = new stdClass();
        $row->id = 14;
        $row->type = 14;
        $row->name = '법무';
        $row->engname = 'Legal';
        $row->maxbytes = 10485760; // 10MB
        $row->maxattachments = 10;
        $row->allowupload = 1;
        $row->allowrental = 1;
        $row->allownew = 0;
        $row->newday = 0;
        $row->status = 1;
        $row->required = 2;
        $row->userid = 2;
        $row->timemodified = time();
        $row->vnname = 'Luật';
        $row->site = 1; // Job site
        $DB->insert_record('jinoboard', $row);

        $row = new stdClass();
        $row->id = 15;
        $row->type = 15;
        $row->name = '세무';
        $row->engname = 'Tax';
        $row->maxbytes = 10485760; // 10MB
        $row->maxattachments = 10;
        $row->allowupload = 1;
        $row->allowrental = 1;
        $row->allownew = 0;
        $row->newday = 0;
        $row->status = 1;
        $row->required = 2;
        $row->userid = 2;
        $row->timemodified = time();
        $row->vnname = 'Thuế';
        $row->site = 1; // Job site
        $DB->insert_record('jinoboard', $row);
    }

    if($oldversion < 2020032700) {
        $table = new xmldb_table('vi_employers');
        $field = new xmldb_field('approval_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if($oldversion < 2021072000) {
        $table = new xmldb_table('user');
        $field = new xmldb_field('isreceivejob', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if($oldversion < 2021072800) {
        $table = new xmldb_table('avatar_user');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('url', XMLDB_TYPE_TEXT, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('created_at', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('foreign', XMLDB_KEY_FOREIGN_UNIQUE, array('user_id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('banner_video');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('filename', XMLDB_TYPE_TEXT, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // code duplicated with /local/management/db/upgrade.php line 80 ~ 90
//        $table = new xmldb_table('vi_coupon_qrcode');
//        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
//        $table->add_field('coupon_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
//        $table->add_field('qrcodeurl', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
//        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
//
//        if (!$dbman->table_exists($table)) {
//            $dbman->create_table($table);
//        }

    }

    if($oldversion < 2021072805) {
        $table = new xmldb_table('vi_benefit_types');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name_ko', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('name_en', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('name_vi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('short_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('vi_job_benefit_types');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('job_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('benefit_type_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Allowances', '참작', 'Allowances', 'Phụ cấp', 'Allowances',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Birthday', '생신', 'Birthday', 'Sinh nhật', 'Birthday',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Education', '교육', 'Education', 'Giáo dục', 'Education',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Healthcare', '보건 의료', 'Healthcare', 'Chăm sóc sức khỏe', 'Healthcare',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Incentive bonus', '인센티브 보너스', 'Incentive bonus', 'Tiền thưởng', 'Incentive bonus',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Insurance', '보험', 'Insurance', 'bảo hiểm', 'Insurance',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Salary review', '급여 검토', 'Salary review', 'Xem xét lương','Salary review',0,time(),null)");
//        $DB->execute("INSERT INTO {vi_benefit_types} VALUES (null, 'Travel', '여행하다','Travel', 'Du lịch', 'Travel',0,time(),null)");
        $row = new stdClass();
        $row->name = 'Allowances';
        $row->name_ko = '참작';
        $row->name_en = 'Allowances';
        $row->name_vi ='Phụ cấp';
        $row->short_name = 'Allowances';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();

        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }
        $row = new stdClass();
        $row->name = 'Birthday';
        $row->name_ko = '생신';
        $row->name_en = 'Birthday';
        $row->name_vi ='Sinh nhật';
        $row->short_name = 'Birthday';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();

        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }

        $row = new stdClass();
        $row->name = 'Education';
        $row->name_ko = '교육';
        $row->name_en = 'Education';
        $row->name_vi ='Giáo dục';
        $row->short_name = 'Education';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();
        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }

        $row = new stdClass();
        $row->name = 'Healthcare';
        $row->name_ko = '보건 의료';
        $row->name_en = 'Healthcare';
        $row->name_vi ='Chăm sóc sức khỏe';
        $row->short_name = 'Healthcare';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();
        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }

        $row = new stdClass();
        $row->name = 'Incentive bonus';
        $row->name_ko = '인센티브 보너스';
        $row->name_en = 'Incentive bonus';
        $row->name_vi ='Tiền thưởng';
        $row->short_name = 'Incentive bonus';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();
        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }

        $row = new stdClass();
        $row->name = 'Insurance';
        $row->name_ko = '보험';
        $row->name_en = 'Insurance';
        $row->name_vi ='Bảo hiểm';
        $row->short_name = 'Insurance';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();
        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }

        $row = new stdClass();
        $row->name = 'Salary review';
        $row->name_ko = '급여 검토';
        $row->name_en = 'Salary review';
        $row->name_vi ='Xem xét lương';
        $row->short_name = 'Salary review';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();
        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }

        $row = new stdClass();
        $row->name = 'Travel';
        $row->name_ko = '여행하다';
        $row->name_en = 'Travel';
        $row->name_vi ='Du lịch';
        $row->short_name = 'Travel';
        $row->sortorder = 0;
        $row->timecreated = time();
        $row->timemodified = time();
        if (!($DB->get_records('vi_benefit_types',array('id' => $row->id)))) {
            $DB->insert_record('vi_benefit_types', $row);
        }


    }

    if ($oldversion < 2021072812) {
        $table = new xmldb_table('vi_resumes');
        $field = new xmldb_field('resume_cv_id', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('vi_resume_cv');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('url_cv', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('url_image_cv', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        } else {
            $dbman->drop_table($table);
            $dbman->create_table($table);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_01/CV.html';
        $row->url_image_cv = 'cv_templates/image-cv1.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_02/cv2.html';
        $row->url_image_cv = 'cv_templates/image-cv2.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_03/visangcv3.html';
        $row->url_image_cv = 'cv_templates/image-cv3.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_04/cv4.html';
        $row->url_image_cv = 'cv_templates/image-cv4.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_05/cv5.html';
        $row->url_image_cv = 'cv_templates/image-cv5.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_06/cv6.html';
        $row->url_image_cv = 'cv_templates/image-cv6.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }

        $row = new stdClass();
        $row->url_cv ='cv_templates/CV_template_07/cv7.html';
        $row->url_image_cv = 'cv_templates/image-cv7.PNG';
        $row->timecreated = time();

        if (!($DB->get_records('vi_resume_cv',array('id' => $row->id)))) {
            $DB->insert_record('vi_resume_cv', $row);
        }
    }

//    if ($oldversion < 2021072813) {
//        $table = new xmldb_table('vi_common_code');
//        $field = new xmldb_field('point', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
//        if (!$dbman->field_exists($table, $field)) {
//            $dbman->add_field($table, $field);
//        }

//        $table = new xmldb_table('vi_credits_logs');
//        $field = new xmldb_field('object_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
//        if (!$dbman->field_exists($table, $field)) {
//            $dbman->add_field($table, $field);
//        }
//    }

    return true;
}
