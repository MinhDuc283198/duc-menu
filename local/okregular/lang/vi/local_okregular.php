<?php

$string['pluginname'] = 'Khóa học cơ bản';
$string['pluginnameplural'] = 'Không học cơ bản';

$string['regular'] = 'Khóa học cơ bản';
$string['irregular'] = 'Khóa học đặc biệt';
$string['regular:short'] = 'Cơ bản';
$string['irregular:short'] = 'Đặc biệt';
$string['visible'] = 'Hiển thị';
$string['invisible'] = 'Ẩn';

$string['category:all'] = 'Tất cả';
$string['category:sort'] = 'Sắp xếp';

$string['apply'] = 'Đăng ký';
$string['apply:start'] = 'Bắt đầu đăng ký';
$string['apply:end'] = 'Kết thúc đăng ký';
$string['apply:status'] = 'Trạng thái';

$string['course:my'] = 'Khóa học của tôi';
$string['course:apply'] = 'Đăng ký khóa học';
$string['course:applydate'] = 'Ngày đăng ký';
$string['course:apply_input'] = 'Vui lòng nhập thời gian đăng ký';
$string['course:overview'] = 'Thông báo khóa học';
$string['course:name'] = 'Khóa học';
$string['course:empty'] = 'Không có khóa học.';
$string['course:emptyinfo'] = 'Không có thông tin đăng ký';
$string['course:list'] = 'Danh sách khóa học';
$string['course:plan'] = 'Kết hoạch giảng dạy';
$string['course:sititon'] = 'sititon';
$string['course:category'] = "Thể loại";
$string['course:subjectid'] = 'Mã môn học';
$string['course:subjectid_input'] = 'Vui lòng nhập mã môn học';
$string['course:professor'] = 'Giáo sư';
$string['course:add'] = 'Thêm giáo sư';
$string['course:edit'] = 'Sửa khóa học';
$string['course:create'] = 'Tạo khóa học';
$string['course:adding'] = 'Bạn có thể thêm bài giảng trên GTEC.';
$string['course:date'] = 'Học kỳ';
$string['course:date_input'] = 'Vui lòng nhập thời lượng bài giảng';
$string['course:start'] = 'Bắt đầu khóa học';
$string['course:end'] = 'Kết thúc khóa học';
$string['course:image'] = 'Hình ảnh khóa học';
$string['course:image_input'] = 'Chỉ tệp hình ảnh (.jpg, .png).';
$string['course:classes'] = 'Lớp học';
$string['course:classes_drive'] = 'Quản lý lớp học';
$string['course:classes_drive_log'] = 'Nhật ký quản lý lớp';
$string['course:classes_restore'] = 'Phục hồi lớp học';
$string['course:classes_restore_log'] = 'Phục hồi nhật ký lớp học';
$string['course:credit'] = 'Tín chỉ';
$string['course:sel_more'] = 'Bạn bải phải chọn nhiều hơn 2 khóa học';
$string['course:sel_standard'] = 'Chọn khóa học tiêu chuẩn';
$string['course:standard'] = 'Khóa học tiêu chuẩn';
$string['course:standard_name'] = 'Tên khóa học tiêu chuẩn';
$string['course:subject_name'] = 'Di chuyển tên khóa học';
$string['course:invisible'] = 'Ẩn khóa học';
$string['course:visible'] = 'Hiển thị khóa học';
$string['course:restore'] = 'Khôi phục';
$string['course:restore_exe'] = 'Bạn muốn khôi phục?';
$string['course:registered'] = 'Đã đăng ký';
$string['course:delete'] = 'Xóa';
$string['course:app'] = 'Phê duyệt';
$string['course:wait'] = 'Chờ';
$string['course:irregular_list'] = 'Danh sách khóa học đặc biệt';
$string['course:participants_list'] = 'Tuyển sinh và danh sách học viên';
$string['course:open'] = 'Thời gian mở';
$string['course:completion'] = 'Hoàn thành';
$string['course:manage'] = 'Thêm quản lý';
$string['course:ctl'] = 'CTL';
$string['course:other'] = 'Khác';
$string['course:major'] = 'Liên kết';
$string['course:approval'] = 'Yêu cầu phê duyệt';
$string['course:approval_cancel'] = 'Hủy phê duyệt';
$string['course:approval_delete'] = 'Xóa phê duyệt';
$string['course:enrol'] = 'Ghi danh';
$string['course:enrol_check'] = 'Có nên ghi danh';
$string['course:section'] = 'Học phần';

$string['coursename:ko'] = 'Tên khóa học(KO)';
$string['coursename:en'] = 'Tên khoá học(EN)';

$string['edit'] = 'Sủa';
$string['cancel'] = 'Hủy bỏ';
$string['confirm'] = 'Xác thực';
$string['success'] = 'Thành công';
$string['student'] = 'Học viên';
$string['students'] = 'Học viên';
$string['manage'] = 'Quản lý';
$string['always:operate'] = 'Thường mở';

$string['merge:manage'] = 'Hợp nhất quản lý';
$string['merge:sel'] = 'Hợp nhất phân khúc';

$string['apply:possible'] = 'Có thể';
$string['apply:impossible'] = 'Không thể';

$string['subject:lang'] = 'KMôn học O/EN';
$string['subject:lang_input'] = 'Vui lòng chọn khóa học KO/EN';
$string['subject:ko'] = 'KO';
$string['subject:ko_input'] = 'Vui lòng nhập tên khóa học (KO).';
$string['subject:en'] = 'EN';
$string['subject:en_input'] = 'Vui lòng nhập tên khóa học (EN).';

$string['search'] = 'Tim kiếm';
$string['search:keywords'] = 'Nhập từ khóa';
$string['search:coursename'] = 'Nhật tên khóa học';

$string['sititon:loginpage'] = 'Sử dụng sau khi đăng nhập';
$string['sititon:guest'] = 'Người dùng khách không tồn tại';
$string['sititon:enrolled'] = 'Đang ký khóa học';
$string['sititon:progress'] = 'Yêu cầu phê duyệt';
$string['sititon:success'] = 'Đăng ký thành công';
$string['sititon:apply'] = 'Đăng ký khóa học';

$string['apply:doyouwantto'] = 'Bạn muốn đăng ký?';
$string['sititon:doyouwantto'] = 'Bạn muốn đăng ký?';
$string['apply:doyouwantcancel'] = 'Bạn muốn hủy bỏ?';

$string['showperpage'] = 'Hiển thị {$a} trên trang';

$string['selectcourses'] = 'Chọn khóa học để đăng ký';
$string['completedrequestaudit'] = 'Yêu cầu đăng ký thành công';
$string['completedrequestenrol'] = 'Yêu cầu đăng ký thành công';
$string['completedrequestcancel'] = 'Hủy bỏ thành công';
$string['couldnotfindenrolment'] = 'Không tìm thấy thông tin đăng ký';
$string['couldnotrequestenrol'] = 'Không thể đăng ký';

$string['courseinfo'] = 'Thông tin khóa học';
$string['syllabus'] = 'Giáo trình';
$string['close'] = 'Đóng';

$string['viewcourse'] = 'Xem khóa học';

$string['year'] = '{$a} Năm';
$string['year:sel'] = 'Năm';
$string['year:now'] = 'Hiện tại';
$string['term'] = '{$a} điều khoản';
$string['gubun'] = '{$a}';
$string['term:all'] = 'Tất cả điều khoản';
$string['term:sel'] = 'Kỳ hạn';
$string['term:current'] = 'Kỳ hiện tại';
$string['date:execute'] = 'Ngày thực hiện';

$string['constant'] = 'Cố định';
$string['section'] = 'Học phần';
$string['no'] = 'No.';
$string['major'] = 'Liên kết';
$string['students'] = 'Học viên';
$string['user:nomove'] = 'Không di chuyển người dùng';
$string['user:move'] = 'Di chuyển đến người dùng';
$string['user:execute'] = 'Người thực hiện';
$string['course_notice'] = 'Thông báo khóa học';

$string['always:operate'] = 'Thường mở';

$string['merge:manage'] = 'Hợp nhật quản lý';
$string['merge:sel'] = 'Hợp nhất phân khúc';

$string['possible'] = 'Có thể';
$string['impossible'] = 'Không thể';

$string['subject:lang'] = 'Môn học KO/EN';
$string['subject:lang_input'] = 'Vui lòng chọn khóa học KO/EN.';
$string['subject:ko'] = 'KO';
$string['subject:ko_input'] = 'Vui lòng nhập tên khóa học (KO).';
$string['subject:en'] = 'EN';
$string['subject:en_input'] = 'Vui lòng nhập tên khóa học (EN).';

$string['search'] = 'Tìm kiếm';
$string['search:keywords'] = 'Nhập từ khóa';
$string['prof'] = 'GS.';
$string['professor'] = 'Giáo sư';
$string['professorname'] = 'Giáo sư : {$a}';

$string['sititon:loginpage'] = 'Sử dụng sau khi đăng nhập';
$string['sititon:guest'] = 'Người dùng khách không tồn tại';
$string['sititon:enrolled'] = 'Khóa học đã đăng ký';
$string['sititon:progress'] = 'Yêu cầu phê duyệt';
$string['sititon:success'] = 'Đăng ký thành công';
$string['sititon:apply'] = 'Đăng ký khóa học';

$string['showperpage'] = 'Hiển thị {$a} mỗi trang';

$string['courseinfo'] = 'Thông tin khóa học';
$string['syllabus'] = 'Giáo trình';
$string['close'] = 'Đóng';

$string['viewcourse'] = 'Xem khóa học';

$string['constant'] = 'Cố định';
$string['section'] = 'Học kỳ';
$string['no'] = 'No.';
$string['univ'] = 'Đại học';
$string['major'] = 'Liên kết';
$string['student'] = 'Học viên';
$string['students'] = 'Học viên';
$string['user:nomove'] = 'Không di chuyển người dùng';
$string['user:move'] = 'Di chuyển đến người dùng';
$string['user:execute'] = 'Người thực hiện';
$string['user:number'] = 'Số';
$string['user:name'] = 'Tên';
$string['user:major'] = 'Liên kết';
$string['user:email'] = 'Thư điện tử';
$string['user:complete_status'] = 'Trạng thái hoàn thành';
$string['user:complete_success'] = 'Khoá học hoàn thành';
$string['user:complete_cancel'] = 'Hủy hoàn thành';
$string['user:complete'] = 'Hoàn thành';
$string['user:incomplete'] = 'Không hoàn thành';
$string['user:empty'] = 'Không có người dùng';
$string['user:search'] = 'Tìm kiếm mã / tên học viên';
$string['user:sel_empty'] = 'Không có học viên được chọn';
$string['user:unenrol'] = 'Xóa người dùng';
$string['user:info'] = 'Thông tin người dùng';

$string['college'] = 'Khoa';
$string['college:sel'] = 'Chọn khoa / phòng';
$string['subject:college_input'] = 'Vui lòng chọn trường';
$string['student_num'] = 'Tham gia lớp học';
$string['subject:student_num_input'] = 'Vui lòng nhập số thành viên khóa học';
$string['student_num:limit'] = 'Nhập 0 tương ứng không giới hạn số lượng';
$string['course:stunum_input'] = 'Số học viên, chỉ nhập số';
$string['request'] = 'Đăng ký';
$string['subject:request_input'] = 'Vui lòng chọn cách đăng ký';
$string['grade'] = 'Bậc';
$string['grade:score'] = '{$a} bậc';
$string['subject:grade_input'] = 'Vui lòng nhập cấp bậc';
$string['arrival'] = 'Sắp xếp hàng đợi';
$string['approval'] = 'Phê duyệt';
$string['coruse:summary'] = 'Tóm tắt khóa học';
$string['end_date'] = 'Ngày kết thúc';
$string['export:excel'] = 'Tải xuống tệp Excel';

$string['complete:history_list'] = 'Danh sách lịch sử';
$string['complete:history_detail'] = 'Chi tiết lịch sử';
$string['complete:coruse_count'] = 'Khóa học đã hoàn thành';
$string['complete:grade_sum'] = 'Tổng số tín chỉ';
$string['complete:detail'] = 'Chi tiết';
$string['complete:show'] = 'Hiển thị';
$string['complete:grade'] = 'Tín chỉ hoàn thành';
$string['complete:grade_frm'] = 'Số tính chỉ hoàn thành : {$a}';
$string['complete:empty'] = 'Không có bài giảng nào';
$string['complete:certificate'] = 'Chứng chỉ khóa học';

$string['issue'] = 'Vấn đề';
$string['employment_support'] = 'Hỗ trợ việc làm';
$string['print'] = 'In';
$string['return'] = 'Trở về';

$string['certificate:sel'] = 'Giấy chứng nhận';
$string['sel:yes'] = 'Có';
$string['sel:no'] = 'Không';

$string['progress'] = 'Tiến độ /  lịch trình bài giảng';
$string['progress_list'] = 'Danh sách tiến độ / lặp trình bài giảng';
$string['lastlecture'] = 'Bài giảng trước';
$string['lastlecture_list'] = 'Danh sách bài giảng trước';
$string['employment'] = '* Được cấp tính chỉ để đăng ký bộ phận hỗ trợ việc làm';

$string['hakbu'] = 'Khoa';
$string['graduate'] = 'Tốt nghiệp';

$string['ongoing'] = 'Đang diễn ra';
$string['standby'] = 'Chờ';
$string['finish'] = 'Kết thúc';