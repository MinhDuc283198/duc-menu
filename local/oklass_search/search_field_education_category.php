<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('search');
echo $OUTPUT->header();
?>  

 <div id="contents">
            <div class="bg">
                <div class="group">
                    <div class="explain">
                        <span class="title">※ 카테고리는 교육과정/:DTUBE/LETO에서 설정한 분류 Tag 기준으로 집계되며, 한 과정에 다수의 분류 Tag를 설정한 경우 중복 집계 될 수 있음</span>
                    </div>
                    
                    <div class="tab_group full">
                        <div class="tab"> 
                            <div class="my">
                                <h3>카테고리별 보기</h3>
                            </div>
                             <div class="my on">
                                 <h3>가나다 순 보기</h3>
                             </div>
                        </div>
                    </div>
                    <div class="sub_contents tab full">
                        <div class="tab_con">
                            <div class="course_con">
                                <div class="list_group">                                    
                                    <div class="list list2">
                                        <h3>
                                            경영지원
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>경영, 전략, 기획</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>인사, 총무, HRD</span>
                                               <span class="right">10</span>
                                           </li> 
                                           <li>
                                               <span>법무, 회계, 세무, 원가</span>
                                               <span class="right">3</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                    <div class="list list2">
                                        <h3>
                                            리더십
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>사람관리</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>업무관리</span>
                                               <span class="right">5</span>
                                           </li> 
                                           <li>
                                               <span>자기관리</span>
                                               <span class="right">7</span>
                                           </li> 
                                           <li>
                                               <span>조직관리</span>
                                               <span class="right">34</span>
                                           </li> 
                                           <li>
                                               <span>리더십일반</span>
                                               <span class="right">10</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                    <div class="list list2">
                                        <h3>
                                            경영지원
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>마케팅</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>영업</span>
                                               <span class="right">10</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                    <div class="list list2">
                                        <h3>
                                            직무공통
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>장의력, 문제해결, 비지니스</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>커뮤니케이션, 자기혁신</span>
                                               <span class="right">5</span>
                                           </li> 
                                           <li>
                                               <span>SMART OA</span>
                                               <span class="right">7</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                </div>
                            </div>
                            <div class="course_con on">
                                <div class="sub_cate">
                                    <p class="box on">가</p>
                                    <p class="box">나</p>
                                    <p class="box">다</p>
                                    <p class="box">라</p>
                                </div> 
                                <div class="list_group">
                                    <div class="list list2">
                                        <h3>
                                            가
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>경영, 전략, 기획</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>인사, 총무, HRD</span>
                                               <span class="right">10</span>
                                           </li> 
                                           <li>
                                               <span>법무, 회계, 세무, 원가</span>
                                               <span class="right">3</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                    <div class="list list2">
                                        <h3>
                                            나
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>사람관리</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>업무관리</span>
                                               <span class="right">5</span>
                                           </li> 
                                           <li>
                                               <span>자기관리</span>
                                               <span class="right">7</span>
                                           </li> 
                                           <li>
                                               <span>조직관리</span>
                                               <span class="right">34</span>
                                           </li> 
                                           <li>
                                               <span>리더십일반</span>
                                               <span class="right">10</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                    <div class="list list2">
                                        <h3>
                                            다
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul>
                                           <li>
                                               <span>마케팅</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>영업</span>
                                               <span class="right">10</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                    <div class="list list2">
                                        <h3>
                                            라
                                            <span class="right light">과정/동영상 수</span>
                                        </h3>
                                        <ul> 
                                           <li>
                                               <span>장의력, 문제해결, 비지니스</span>
                                               <span class="right">3</span>
                                           </li> 
                                           <li>
                                               <span>커뮤니케이션, 자기혁신</span>
                                               <span class="right">5</span>
                                           </li> 
                                           <li>
                                               <span>SMART OA</span>
                                               <span class="right">7</span>
                                           </li> 
                                        </ul>  
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                           
                            
                    </div>
                        
                </div>
            </div>
        </div>



 <?php
    echo $OUTPUT->footer();
 ?>


