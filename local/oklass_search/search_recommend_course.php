<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('search');
echo $OUTPUT->header();
?>  

 <div id="contents">
            <div class="bg">
                <div class="group">
                    <div class="filter">
                        <span class="title">선택된 Filters</span>
                        <span class="button">Excel <span class="del"></span></span>
                        <span class="button">경영 일반 <span class="del"></span></span>
                    </div>
                    <div class="aside">
                        <div class="sub_menu">
                            <h3>분류 태그</h3>
                            <ul>
                                <li><a href="#">R&amp;(0)</a></li>
                                <li><a href="#">생산(2)</a></li>
                                <li><a href="#">장비(2)</a></li>
                                <li><a href="#">품질경영(2)</a></li>
                                <li><a href="#">구매물류(2)</a></li>
                                <li><a href="#">환경/한전/정보보안(2)</a></li>
                                <li><a href="#">경영일반(2)</a></li>
                                <li><a href="#">영업마케팅(2)</a></li>
                                <li><a href="#">직무공통(2)</a></li>
                                <li><a href="#">Global/어학(2)</a></li>
                                <li><a href="#">리더십(2)</a></li>
                                <li><a href="#">인문교육(2)</a></li>
                                <li><a href="#">직급필수(2)</a></li>
                            </ul>
                        </div>
                    </div>    
                    
                    <div class="sub_contents">
                             <div class="explain">
                                ※ 본 화면에서는 <span class="pink">신청 가능/운영 예정</span>인 정보만 조회횝니다.(전체 과정의 일정은 교육계획 캘더 확인)
                                <p class="br">※ 필수교육은 추천과정에서 제외됨</p>
                            </div>  
                            <div class="pinkblock">
                                <p>
                                    나의 직무는 <span class="pink large">Administration.HR</span>로 설정되어 있습니다.
                                    <br />
                                    <span class="">(본 설정은 "ERP/EMS > HR Self Service"에서 소속 부서장이 수정 가능함)</span>
                                </p>
                            </div>
                            <div class="input_group">
                                <div class="left">
                                    <label for="job">직무:</label>
                                    <select id="job">
                                        <option value="administratorion">Administration</option>
                                    </select>
                                    <select class="hr" title="HR">
                                        <option value="HR">HR</option>
                                    </select>
                                </div>
                                <div class="right">
                                    <select class="year" title="year">
                                        <option value="1년이내">1년 이내</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="course_list topline">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                             <div class="course_list">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level m">중급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level l">초급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list online">
                                <p class="thumb_img">
                                    <img src=<?php echo $OUTPUT->pix_url('images//sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src=<?php echo $OUTPUT->pix_url('images//like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%(동일직무 기준)</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            
                            <div class="paging">
                                <span class="prev"><a href="#">&lt;</a></span>
                                <span class="num on"><a href="#">1</a></span>
                                <span class="num"><a href="#">2</a></span>
                                <span class="num"><a href="#">3</a></span>
                                <span class="num"><a href="#">4</a></span>
                                <span class="num"><a href="#">5</a></span>
                                <span class="next"><a href="#">&gt;</a></span>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>



 <?php
    echo $OUTPUT->footer();
 ?>


