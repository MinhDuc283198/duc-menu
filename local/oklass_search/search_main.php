<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('search');
echo $OUTPUT->header();
?>  

 <div id="contents">
            <div class="bg">
                <div class="group">
                    <div class="filter">
                        <span class="title">선택된 Filters</span>
                        <span class="button">Excel <span class="del"></span></span>
                        <span class="button">경영 일반 <span class="del"></span></span>
                    </div>
                    <div class="aside">
                        <div class="sub_menu">
                            <h3>분류 태그</h3>
                            <ul>
                                <li><a href="#">R&amp;(0)</a></li>
                                <li><a href="#">생산(2)</a></li>
                                <li><a href="#">장비(2)</a></li>
                                <li><a href="#">품질경영(2)</a></li>
                                <li><a href="#">구매물류(2)</a></li>
                                <li><a href="#">환경/한전/정보보안(2)</a></li>
                                <li><a href="#">경영일반(2)</a></li>
                                <li><a href="#">영업마케팅(2)</a></li>
                                <li><a href="#">직무공통(2)</a></li>
                                <li><a href="#">Global/어학(2)</a></li>
                                <li><a href="#">리더십(2)</a></li>
                                <li><a href="#">인문교육(2)</a></li>
                                <li><a href="#">직급필수(2)</a></li>
                            </ul>
                        </div>
                    </div>    
                    <div class="sub_contents">
                        <div class="explain">
                            ※ 본 화면에서는 <span class="pink">신청 가능/운영 예정</span>인 정보만 조회횝니다.(전체 과정의 일정은 교육계획 캘더 확인)
                        </div>   
                        
                        <div class="lists">  
                            <div class="title">
                                <span class="img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/evaluation', 'theme'); ?>" alt="평가" title="평가" />
                                </span> 
                                <b>평가</b><span class="count"> (1건)</span>
                                 <div class="more">
                                     <span>평가 더보기 <img src="<?php echo $OUTPUT->pix_url('images/plus', 'theme'); ?>" alt="more" title="more" /></span>
                                 </div>
                            </div>
                            <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <h3 class="title">나의 Excel 수준 평가</h3>
                                    <p class="info">
                                        <span class="date">1시간</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">평가신청</span></div>
                            </div>
                        </div>
                        
                        <div class="lists">  
                            <div class="title">
                                <span class="img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/course', 'theme'); ?>" alt="교육과정" title="교육과정" />
                                </span> 
                                <b>교육과정</b><span class="count"> (1건)</span>
                                 <div class="more">
                                     <span>교육과정 더보기 <img src="<?php echo $OUTPUT->pix_url('images/plus', 'theme'); ?>" alt="more" title="more" /></span>
                                 </div>
                            </div>
                            
                            <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title">Excel Macro &amp; VBA 과정</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                             <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level m">중급</span>
                                    <h3 class="title">나의 Excel 수준 평가</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                            
                            <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level l">초급</span>
                                    <h3 class="title">나의 Excel 수준 평가</h3>
                                    <p class="info">
                                        <span class="date">3일/24시간 집합과정</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="lists">  
                            <div class="title">
                                <span class="img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/cumputer', 'theme'); ?>" alt="DTUBE" title="DTUBE" />
                                </span> 
                                <b>:DTUBE</b><span class="count"> (1건)</span>
                                 <div class="more">
                                     <span>:DTUBE 더보기 <img src="<?php echo $OUTPUT->pix_url('images/plus', 'theme'); ?>" alt="more" title="more" /></span>
                                 </div>
                            </div>
                            
                            <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">Excel Tips Weekly #1</h3>
                                    <p class="info">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button">
                                    <span class="apply">수강신정</span>
                                    <span class="add">관심과정 추가</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="lists">  
                            <div class="title">
                                <span class="img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/leto', 'theme'); ?>" alt="DTUBE" title="DTUBE" />
                                </span> 
                                <b>LETO</b><span class="count"> (1건)</span>
                                 <div class="more">
                                     <span>LETO 더보기 <img src="<?php echo $OUTPUT->pix_url('images/plus', 'theme'); ?>" alt="more" title="more" /></span>
                                 </div>
                            </div>
                            
                            <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <h3 class="title">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">2016년 8월 10일 ~ 12월 31일</span>
                                        <span class="person">승인구분:개설자 승인</span><br />
                                        <span class="position">개설자: 기본교육팀 황공욱 과장</span>
                                        <span class="like">분류: 기타</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                        </div>
                        
                        <div class="paging">
                            <span class="prev"><a href="#">&lt;</a></span>
                            <span class="num on"><a href="#">1</a></span>
                            <span class="num"><a href="#">2</a></span>
                            <span class="num"><a href="#">3</a></span>
                            <span class="num"><a href="#">4</a></span>
                            <span class="num"><a href="#">5</a></span>
                            <span class="next"><a href="#">&gt;</a></span>
                        </div>
                    </div>
                </div>
            </div>
         </div>



 <?php
    echo $OUTPUT->footer();
 ?>


