<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('search');
echo $OUTPUT->header();
?>  

         <div id="contents">
            <div class="bg">
                <div class="group">
                    <div class="explain">
                        <span class="title">※ 카테고리는 교육과정/:DTUBE/LETO에서 설정한 분류 Tag 기준으로 집계되며, 한 과정에 다수의 분류 Tag를 설정한 경우 중복 집계 될 수 있음</span>
                    </div>
                    <div class="filter bg">
                        <span class="title">선택된 분류 tag</span>
                        <span class="button">Excel <span class="del"></span></span>
                        <span class="button">경영 일반 <span class="del"></span></span>
                    </div>

                    <div class="sub_contents full">
                        <div class="tab_con">
                            <div class="listgroup">
                                <div class="list list4">
                                    <h3>경영지원</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check02" />
                                           <label for="check02">인사, 총무, HRD</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check03" />
                                           <label for="check03">법무, 회계, 세무, 원가</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>리더십</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">사람관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">업무관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">자기관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">조직관리</label>
                                       </li>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">리더십일반</label>
                                       </li>
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>영업마케팅</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>직무공통</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                 <div class="list list4">
                                    <h3>경영지원</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check02" />
                                           <label for="check02">인사, 총무, HRD</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check03" />
                                           <label for="check03">법무, 회계, 세무, 원가</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>리더십</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">사람관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">업무관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">자기관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">조직관리</label>
                                       </li>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">리더십일반</label>
                                       </li>
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>영업마케팅</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>직무공통</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                 <div class="list list4">
                                    <h3>경영지원</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check02" />
                                           <label for="check02">인사, 총무, HRD</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check03" />
                                           <label for="check03">법무, 회계, 세무, 원가</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>리더십</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">사람관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">업무관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">자기관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">조직관리</label>
                                       </li>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">리더십일반</label>
                                       </li>
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>R &amp; D</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>영업마케팅</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list4">
                                    <h3>직무공통</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="vertical_line line01"></div>
                                <div class="vertical_line line02"></div>
                                <div class="vertical_line line03"></div>
                                
                            </div>
                        </div>
                            
                           
                            
                    </div>
                        
                </div>
            </div>
        </div>
 <?php
    echo $OUTPUT->footer();
 ?>


