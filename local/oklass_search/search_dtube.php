<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('search');
echo $OUTPUT->header();
?>  

<div id="contents">
            <div class="bg">
                <div class="group">
                    <div class="filter">
                        <span class="title">선택된 Filters</span>
                        <span class="button">Excel <span class="del"></span></span>
                        <span class="button">경영 일반 <span class="del"></span></span>
                    </div>
                    <div class="aside">
                        <div class="sub_menu">
                            <h3>분류 태그</h3>
                            <ul>
                                <li><a href="#">R&amp;(0)</a></li>
                                <li><a href="#">생산(2)</a></li>
                                <li><a href="#">장비(2)</a></li>
                                <li><a href="#">품질경영(2)</a></li>
                                <li><a href="#">구매물류(2)</a></li>
                                <li><a href="#">환경/한전/정보보안(2)</a></li>
                                <li><a href="#">경영일반(2)</a></li>
                                <li><a href="#">영업마케팅(2)</a></li>
                                <li><a href="#">직무공통(2)</a></li>
                                <li><a href="#">Global/어학(2)</a></li>
                                <li><a href="#">리더십(2)</a></li>
                                <li><a href="#">인문교육(2)</a></li>
                                <li><a href="#">직급필수(2)</a></li>
                            </ul>
                        </div>
                    </div>    
                    
                    <div class="tab_group">
                        <div class="tab"> 
                            <div class="my on">
                                <h3>등록시간 기준</h3>
                            </div>
                             <div class="my">
                                 <h3>좋아요 기준</h3>
                             </div>
                             <div class="my">
                                 <h3>조회수 기준</h3>
                             </div>   
                        </div>
                    </div>
                    <div class="sub_contents tab">
                           
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            <div class="course_list">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            
                            
                            <div class="course_list noline">
                                <p class="thumb_img">
                                    <img src="<?php echo $OUTPUT->pix_url('images/sample', 'theme'); ?>" alt="course01" title="course01" />
                                </p>     
                                <div class="course_text">
                                    <span class="level h">고급</span>
                                    <h3 class="title small">HRD 담당 LETO 3기</h3>
                                    <p class="info col4">
                                        <span class="date">:D튜브 20분</span>
                                        <span class="person">조회수 111건</span><br />
                                        <span class="position">등록일 2016.7.2</span>
                                        <span class="like"><img src="<?php echo $OUTPUT->pix_url('images/like', 'theme'); ?>" alt="좋아요" title="좋아요" /> 78%</span>
                                    </p>  
                                </div>
                                <div class="button"><span class="request">학습하기</span></div>
                            </div>
                            
                            <div class="paging">
                                <span class="prev"><a href="#">&lt;</a></span>
                                <span class="num on"><a href="#">1</a></span>
                                <span class="num"><a href="#">2</a></span>
                                <span class="num"><a href="#">3</a></span>
                                <span class="num"><a href="#">4</a></span>
                                <span class="num"><a href="#">5</a></span>
                                <span class="next"><a href="#">&gt;</a></span>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>



 <?php
    echo $OUTPUT->footer();
 ?>


