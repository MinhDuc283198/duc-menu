<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("연간일정");

$year = date('Y');
$current_time = time();
$firstyeardate = strtotime($year.'-1-1');
$lastyeardate = strtotime($year.'-12-31 23:59:59');

echo $OUTPUT->header();
?>  
<h3 class="page_title">연간일정</h3>

<div class="tab_header style02">
    <span class="on col2">교육일정</span>
    <span class="col2">외부기관교육일정</span>
</div>
<div class="tab_contents nobg">
    <div class="on">
        <div class="text-bold margin_bt15">일정안내</div>
        <table class="table">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>수강신청</th>
                    <th>교육기간</th>
                    <th>기간</th>
                    <th>집합교육</th>
                </tr>
            </thead>
            <?php            
            $sql = ' SELECT * FROM {lmsdata_schedule} '
                . 'WHERE (enrolmentstart <= :stime1 AND enrolmentend >= :stime2) OR (enrolmentstart >= :stime3 AND enrolmentstart <= :etime1) AND classtype = :classtype AND classnum < 100 ORDER BY learningstart, classnum asc';
            $params = array('stime1'=>$firstyeardate, 'stime2'=>$firstyeardate, 'stime3'=>$firstyeardate, 'etime1'=>$lastyeardate, 'classtype'=>1);
            $schedules = $DB->get_records_sql($sql, $params);
            ?>
            <tbody>
                <?php 
                foreach($schedules as $schedule){
                    $week = ceil(($schedule->learningend-$schedule->learningstart) / (60 * 60 * 24 * 7));
                    $red = '';
                    if($schedule->enrolmentstart <= $current_time && $schedule->enrolmentend >= $current_time){
                        $red = ' class="red" ';
                    }
                ?>
                <tr <?php echo $red;?>>
                    <td ><?php echo $schedule->classnum;?>기</td>
                    <td><?php echo date('Y-m-d', $schedule->enrolmentstart).' ~ '.date('Y-m-d', $schedule->enrolmentend);?></td>
                    <td><?php echo date('Y-m-d', $schedule->learningstart).' ~ '.date('Y-m-d', $schedule->learningend);?></td>
                    <td><?php echo $week;?>주</td>
                    <td><?php if($schedule->offlineyn == 0) echo '없음'; else echo '있음';?></td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>

    <div>
        <div class="text-bold margin_bt15">외부교육일정</div>
        <table class="table">
            <thead>
                <tr>
                    <th>기관</th>
                    <th>수강신청</th>
                    <th>교육기간</th>
                </tr>
            </thead>
            <?php            
            $sql = ' SELECT * FROM {lmsdata_schedule} '
                . 'WHERE (enrolmentstart <= :stime1 AND enrolmentend >= :stime2) OR (enrolmentstart >= :stime3 AND enrolmentstart <= :etime1) AND classtype = :classtype AND classnum < 100 ORDER BY learningstart, enrolmentend asc';
            $params = array('stime1'=>$firstyeardate, 'stime2'=>$firstyeardate, 'stime3'=>$firstyeardate, 'etime1'=>$lastyeardate, 'classtype'=>2);
            $schedules = $DB->get_records_sql($sql, $params);
            ?>
            <tbody>
                <?php 
                foreach($schedules as $schedule){
                    $red = '';
                    $link = '';
                    if($schedule->enrolmentstart <= $current_time && $schedule->enrolmentend >= $current_time){
                        $red = ' class="red" ';
                        $link = 'onclick="location.href=\''.$CFG->wwwroot.'/local/course_application/comsignment_info.php?id='.$schedule->id.'&menu_gubun=2\'" class="pointer"';
                    }
                ?>
                <tr<?php echo $red;?>>
                    <td><span <?php echo $link;?>><?php echo $schedule->consignmenttitle;?></span></td>
                    <td><?php echo date('Y-m-d', $schedule->enrolmentstart).' ~ '.date('Y-m-d', $schedule->enrolmentend);?></td>
                    <td><?php echo date('Y-m-d', $schedule->learningstart).' ~ '.date('Y-m-d', $schedule->learningend);?></td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>









<?php
echo $OUTPUT->footer();
?>


