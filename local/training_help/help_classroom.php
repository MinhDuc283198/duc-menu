<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("나의강의실");

echo $OUTPUT->header();
?>  

<h3 class="page_title">나의강의실</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>
<div class="div_title top">
    강의실 메인 입장하기
</div>
<ul class="dash margin_bt50">
    <li class="margin_bt15">
        <p class="margin_bt15"><strong>마이페이지 -> 수강정보 -> '수강진행과정'</strong> 페이지에서 입장 하실 수 있습니다.</p>
<!--                            <img src="/images/theeme/oklassedu/pix/images/ex_mycourse.jpg" alt="" class="img_brd"  /> -->
    </li>

</ul>

<div class="div_title top">
    강의실 메인
</div>
<img src="/theme/oklassedu/pix/images/ex_classroom.jpg" alt="img_brd" class="img_brd margin_bt15" />
<ul class="margin_bt50">
    <li class="margin_bt15">
        1. 사용자 프로필이 표시 되며 개인정보 수정 페이지로 이동 하실 수 있습니다.
    </li>
    <li class="margin_bt15">
        2. 알림사항과 메시지가 오면 표시가 됩니다. 각 아이콘을 누르면 강좌, 알림, 메시지를 최근 순서로 미리 볼 수 있습니다.
    </li>
    <li class="margin_bt15">
        3. 개인정보 중 중요한 정보가 표시되어 있으며 수정하기 버튼을 통해 정보를 수정 가능한 페이지로 이동 가능 합니다.
    </li>
    <li class="margin_bt15">
        4. 강의실의 공지사항이 표시되며 더 보기 버튼을 통해 더 많은 공지사항을 볼 수 있습니다.
    </li>
    <li class="margin_bt15">
        5. 강의정보로 교육기간과 평가기준이 나와있습니다. 교육 기간내에 평가기준의 조건을 완료해야합니다.
    </li>
    <li class="">
        6. 강의실의 전체 진도율과 학습기간이 표시됩니다.
    </li>
</ul>

<div class="div_title top">
    학습하기
</div>  
<img src="/theme/oklassedu/pix/images/ex_classroom02.jpg" alt="img_brd" width="750px" class="margin_bt15 img_brd" />
<ul class="margin_bt50">
    <li class="margin_bt15">
        1. 해당 버튼을 누르면 현재 차시의 콘텐츠 목록이 나오고 클릭하면 해당 콘텐츠로 이동합니다.
    </li>
    <li class="margin_bt15">
        2. 해당 과정의 학습정보를 보여줍니다.
    </li>
    <li class="">
        3. 클릭하면 다음 콘텐츠로 넘어갑니다.
    </li>
 
</ul>



<?php
echo $OUTPUT->footer();
?>


