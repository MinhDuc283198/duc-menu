<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("NEIS&교육지명번호");

echo $OUTPUT->header();
?>  
    <h3 class="page_title">NEIS/교육지명번호</h3>
    <div class="textbox img02">
        <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
        <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
    </div>
    <div class="div_title top">
        NEIS/교육지명번호
    </div>
    <ul class="dash margin_bt50">
        <li class="margin_bt15">
            <strong>NEIS 개인번호 : </strong>
            지역(3자리) + 신분구분(영문A 또는 숫자1자리) + 일련번호(6자리)
        </li>
        <li>
            <strong>교육지명번호 : </strong>
            예&#41;서울-구로중-16-001/전북-익산궁동초-16-002
        </li>

    </ul>
    <div class="div_title top">
        NEIS/교육지명번호 확인
    </div>
    <ul class="dash margin_bt50">
        <li class="margin_bt15">
            <p class="margin_bt15"><strong>마이페이지 -> '마이페이지 홈'</strong> 또는 <strong>'나의강의실'</strong> 에서 NEIS/교육지명번호 확인이 가능합니다.</p>
        </li>
    </ul>

    <div class="div_title top">
        NEIS/교육지명번호 수정
    </div>
    <ul class="dash margin_bt50">
        <li class="margin_bt15">
            <p class="margin_bt15"><strong>마이페이지 -> 수강정보 -> '나의수강현황'</strong>에서 수정할 수 있습니다.</p>
            <img class="img_brd" src="/theme/oklassedu/pix/images/ex_neis02.png" alt="" />
        </li>
    </ul>
<?php
echo $OUTPUT->footer();
?>


