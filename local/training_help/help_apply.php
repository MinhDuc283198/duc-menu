<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("신청방법");

echo $OUTPUT->header();
?>  
<h3 class="page_title">신청방법</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>

<div class="div_title top">
    1단계 : <span class="orange">회원가입</span>
</div>
<div class="colorbox margin_bt50">
    <h3 class="margin_bt15">본교육원에서 교육를 받기위해서는 회원가입을 하셔야하며, 회원가입과 수강에 제한은 없으며, 직무교육과정은 유,초,중고 교원 및 교육전문직 공무원의 경우 교육점수에 반영됩니다.</h3>
    <ul class="dash_small">
        <li>로그인창의 [회원가입]버튼을 클릭한 후 회원가입 양식을 입력합니다.</li>
        <li>교원이신 경우에는 소속학교, 교육청 등 교원정보를 정확히 입력하셔야만 교육에 대한 정확한 통보가 이루어 집니다.</li>
    </ul>
</div>

<div class="div_title top">
    2단계 : <span class="orange">수강신청</span>
</div>
<div class="colorbox margin_bt50">
    <ul class="dash_small">
        <li>
            <strong>[직무교육]</strong> 메뉴에서 교육분류, 신청기간 등을 고려하여 과정을 선택하고 [수강신청] 버튼을 클릭합니다. <br />
        </li>
        <li>
            <strong>[수강신청]</strong>후 선택한 과정에 대해 확인합니다.
        </li>
        <li>선택한 과정에 이상이 없을 시 결제 수단을 선택합니다.</li>
    </ul>
</div>

<div class="div_title top">
    3단계 : <span class="orange">결제</span>
</div>
<div class="colorbox margin_bt50">
    <ul class="dash_small">
        <li>선택하신 결제수단에 따라 결제 완료하시면 됩니다.</li>
<!--        <li>단 계좌이체(인터넷뱅킹, 폰뱅킹,실시간 계좌)와 무통장 입금은 하루 2회 결제확인이 이루어 집니다.</li>-->
        <li>결제수단에는 신용카드, 가상계좌, 실시간 계좌 이체가 있습니다. </li>
        <li>가상계좌의 경우 수강신청 후 7일 이내에 입금해주셔야 합니다. </li>
    </ul>
</div>

<div class="div_title top">
    4단계 : <span class="orange">수강신청 확인</span>
</div>
<div class="colorbox">
    <ul class="dash_small">
        <li>
            결제완료 후 <strong>마이페이지 -> 결제정보</strong> 에서 신청한 과정을 확인 하실 수 있습니다.
        </li>
        <li>영수증은 결제완료 처리 후 언제든지 출력 가능합니다.</li>
    </ul>
</div>

<div class="div_title top">
    5단계 : <span class="orange">수 강</span>
</div>
<div class="colorbox margin_bt50">
    <ul class="dash_small">
        <li>
            수강시작일 부터 <strong>마이페이지 -> 마이페이지 홈 -> 수강 진행 과정</strong> 에서 입장 버튼을 클릭하시고 강의실에 입장해 교육에 임하시면 됩니다.
        </li>
    </ul>
</div>


<?php
echo $OUTPUT->footer();
?>


