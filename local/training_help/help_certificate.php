<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("이수증/영수증 출력");

echo $OUTPUT->header();
?>  

<h3 class="page_title">이수증/영수증 출력</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>
<div class="div_title top">
    이수증발급
</div>
<ul class="dash margin_bt30">
    <li class="margin_bt15">
        <strong>마이페이지 -> 수강정보 -> '이수증발급'</strong>페이지에서 확인 가능합니다. 
    </li>
</ul>
<img src="/theme/oklassedu/pix/images/ex_certificate.jpg" alt="img_brd" class="margin_bt15 img_brd" />
<ul class="margin_bt50">
    <li class="margin_bt15">
        1. 출력 버튼 클릭 시 이수증이 pdf형식으로 보여집니다.
    </li>
</ul>

<div class="div_title top">
    영수증 출력
</div>
<ul class="dash margin_bt30">
    <li class="margin_bt15">
        <strong>마이페이지 -> 결제정보 </strong>페이지에서 확인 가능합니다. 
    </li>
</ul>
<img src="/theme/oklassedu/pix/images/ex_certificate1.png" alt="img_brd" class="margin_bt15 img_brd" />
<ul class="margin_bt50">
    <li class="margin_bt15">
        2. 영수증 출력 버튼 클릭 시 영수증이 팝업 창으로 보여집니다.
    </li>
</ul>
<?php
echo $OUTPUT->footer();
?>


