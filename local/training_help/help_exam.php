<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("평가방법");

echo $OUTPUT->header();
?>  
<h3 class="page_title">평가방법</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>
<div class="div_title top">
    평가기준
</div>
<ul class="dash margin_bt50">
    <li class="margin_bt15">
        <strong>학습진도율 </strong>  : 총 학습차시 대비 학습 완료 차시 <br />
        <span class="blue">*학습진도율이 90% 미만인 경우 2차 평가대상에서 제외</span>
    </li>
    <li class="margin_bt15">
        <strong>토론평가 </strong> : 교육기간 중 교육원 사이트를 통해 원격 참여
        <ul class="dash_small">
            <li>참여시 자동으로 점수 부여</li>
        </ul>
    </li>
    <li class="margin_bt15">
        <strong>온라인 시험 </strong> : 평가기간중 교육원 사이트를 통해 원격 참여
    </li>
</ul>

<div class="div_title top">
    이수기준
</div>
<ul class="dash margin_bt50">
    <li>
        총점 100점 만점에 60점 이상으로 학습진도율 90% 이상 <br>
        <span class="blue">*총점(100점) 산정 대상 : 토론평가 40% + 온라인 시험 60%</span>
    </li>
</ul>

<div class="div_title top">
    직무교육 60시간(4학점) 과정 최종성적 산출
</div>
<ul class="dash margin_bt50">
    <li>
        교육과학기술부(구 교육인적자원부) 관련규정에 의거하여 최종성적은 [상대평가]로 산출되며, 아래 [교육성적분포조건표]에 따라 최종 점수가 부여됩니다.
    </li>
</ul>

<div class="div_title top">
    직무교육성적분포조건표
</div>
<table class="table margin_bt50">
    <thead>
        <tr>
            <th>점수</th>
            <th>백분율</th>
            <th>점수</th>
            <th>백분율</th>
            <th>점수</th>
            <th>백분율</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>100</td>
            <td>2%</td>
            <td>93</td>
            <td>6.2%</td>
            <td>86</td>
            <td>5.7%</td>
        </tr>
        <tr>
            <td>99</td>
            <td>2.6%</td>
            <td>92</td>
            <td>6.6%</td>
            <td>85</td>
            <td>5.7%</td>
        </tr>
        <tr>
            <td>98</td>
            <td>3.2%</td>
            <td>91</td>
            <td>6.8%</td>
            <td>84</td>
            <td>4.5%</td>
        </tr>
        <tr>
            <td>97</td>
            <td>3.8%</td>
            <td>90</td>
            <td>7.0%</td>
            <td>83</td>
            <td>3.8%</td>
        </tr>
        <tr>
            <td>96</td>
            <td>4.5</td>
            <td>89</td>
            <td>6.8%</td>
            <td>82</td>
            <td>3.2%</td>
        </tr>
        <tr>
            <td>95</td>
            <td>5.1%</td>
            <td>88</td>
            <td>6.6%</td>
            <td>81</td>
            <td>2.6%</td>
        </tr>
        <tr>
            <td>94</td>
            <td>5.7%</td>
            <td>87</td>
            <td>6.2%</td>
            <td>80</td>
            <td>2%</td>
        </tr>
    </tbody>
</table>

<div class="div_title top">
    동점자 처리기준
</div>
<div class="margin_bt50">
    <img src="/theme/oklassedu/pix/images/m02_img01.jpg" width="100%" alt="동점자처리기준" />
</div>



<?php
echo $OUTPUT->footer();
?>


