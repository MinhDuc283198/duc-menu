<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("교육연기/취소");

echo $OUTPUT->header();
?>  
<h3 class="page_title">교육연기</h3>

<div class="div_title top">교육취소/환불</div>
<div class="text-bold margin_bt15">
    <p>교육비 결제 전에는 '마이페이지-결제정보'에서 신청을 취소할 수 있습니다.</p>
    <p>교육비를 결제한 경우에는 교육 시작 후 7일까지만 취소가 가능합니다.</p>
</div>
<table class="table">
    <thead>
        <tr>
            <th>취소시점</th>
            <th>변환금액</th>
            <th>비고</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>결승 승인 전(미납상태)</td>
            <td>-</td>
<!--            <td>'나의 강의실'에서 직접 취소가능</td>-->
            <td>'마이페이지 - 결제정보' 에서 직접 취소 가능</td>
        </tr>
        <tr>
            <td>교육 시작전</td>
            <td>전액환불</td>
<!--            <td>교육 시작 전 전액환불 교육원으로 취소 요청(02-2670-9465)</td>-->
            <td>'마이페이지 - 결제정보' 에서 직접 취소 가능</td>
        </tr>
        <tr>
            <td>교육시작후 7일 이내</td>
            <td>80%환불</td>
<!--            <td>교육 시작 후 7일 이내 80% 환불 교육원으로 취소 요청(02-2670-9465)</td>-->
            <td>'마이페이지 - 결제정보' 에서 직접 취소 가능</td>
        </tr>
        <tr>
            <td>교육시작후 7일 경과 후</td>
            <td>-</td>
            <td>취소불가</td>
        </tr>
    </tbody>
</table>
<!--<div class="text orange">
    단, 수강취소 시에는 수강취소신청서를 제출하셔야 합니다. (Fax.02-2670-9305)
</div>
-->
<div class="input_group full margin_bt50">
<!--    <strong class="">수강취소신청</strong>-->
<!--    <input type="button" value="다운로드" class="btn black h30 margin_l10"onclick="location.href='<?php echo $CFG->wwwroot ?>/local/mypage/payment.php'" />-->
    <a href='<?php echo $CFG->wwwroot ?>/local/mypage/payment.php'/><strong class="" style="color: red;">수강 신청 취소하러 가기</strong></a>
    
</div>

<div class="div_title top">교육연기</div>
<div class="margin_bt30">
    <p class="text-bold margin_bt15">교육취소기간 이후에 특별한 사유로 인하여 교육진행이 불가능한 상황이 발생할 경우, 다음차수의 교육로 연기하여 수강할 수 있습니다.</p>
    <p>
        (예) 사고, 결혼식 등으로 출석시험 응시가 불가능한 경우<br />
<!--        단, 연기 신청 시에는 수강연기신청서와 사유에 관한 증명서를 제출하셔야 합니다. (Fax.02-2670-9305)-->
            단, 위탁 교육의 경우에는 교육 연기가 되지 않습니다.
    </p>
</div>

<div class="input_group full margin_bt50">
<!--    <strong class="">수강연기신청 </strong>
    <input type="button" value="다운로드" class="btn black h30 margin_l10" onclick="location.href='<?php echo $CFG->wwwroot ?>/local/mypage/coursepostponement.php'"/>-->
    <a href='<?php echo $CFG->wwwroot ?>/local/mypage/coursepostponement.php'/><strong class="" style="color: red;">교육 연기 신청하러 가기</strong></a> 
</div>


<?php
echo $OUTPUT->footer();
?>


