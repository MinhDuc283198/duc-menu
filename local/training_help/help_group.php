<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("-단체수강신청");

echo $OUTPUT->header();
?>  
<h3 class="page_title">단체수강신청</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>
<div class="div_title top">
    단체수강 신청안내
</div>
<ul class="dash margin_bt50">
    <li>
        <strong>같은 학교 여부와 무관하게 5인이상</strong> 직무교육 과정을 신청할 경우 단체수강 할인혜택을 드립니다.
    </li>
    <li>
        <strong>같은 기수내에서만 해당</strong> 되며 과정은 동일과정이 아니어도 할인을 받으실 수 있습니다.
    </li>
</ul>

<div class="div_title top">
    할인혜택
</div>
<table class="table">
    <thead>
        <tr>
            <th>구분</th>
            <th>할인혜택</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-bold">직무교육과정</td>
            <td>20% 할인</td>
        </tr>
        <tr>
            <td class="text-bold">자율교육과정</td>
            <td>할인혜택없음</td>
        </tr>
        <tr>
            <td class="text-bold">교재비</td>
            <td>할인혜택없음</td>
        </tr>

    </tbody>
</table>
<div class="text gr text-bold margin_bt50">* 전교조 조합원이 아니어도 단체할인혜택 적용이 가능합니다.</div>

<div class="div_title top">
    단체수강 신청방법
</div>
<ul class="dash">
    <li>개별적으로 수강신청을 합니다.</li>
    <li>한장의 단체 수강신청서에 수강인원 모두를 기재한 후 (FAX 02-2670-9305 or Email chamcampus@gmail.com)로 전송합니다.</li>
    <li>단체수강 대표자 이름으로 무통장 일괄 송금합니다.</li>
</ul>

<input type="button" value="단체수강양식" class="btn orange big" />


<?php
echo $OUTPUT->footer();
?>


