<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("강의진도율");

echo $OUTPUT->header();
?>  
<h3 class="page_title">강의진도율</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>

<div class="div_title top">
    진도체크
</div>
<ul class="dash margin_bt30">
    <li class="margin_bt15">
        해당 강의의 최소 진도율 이상을 수강하셔야 이수가 가능합니다.
    </li>
</ul>

<div class="div_title top">
    진도체크 기준
</div>
<ul class="dash margin_bt30">
<!--    <li class="margin_bt15">
        진도체크가 되는 단원의 권장학습시간 이상을 학습하셔야 해당 단원이 "학습완료"로 체크되고, 세부 단원이 모두 학습완료가 되어야 회차(혹은 모듈)이 학습완료로 인정됩니다.
    </li>-->
    <li class="margin_bt15">
        강의 생성 시 설정 한 이수 조건을 확인 해야 합니다.
    </li>
</ul>

<div class="div_title top">
    진도율 확인하기
</div>
<ul class="dash margin_bt30">
    <li class="margin_bt15">
        <p class="margin_bt15"><strong>마이페이지 -> 마이페이지 홈 -> 수강진행과정</strong> 에서 진도율을 확인 하실 수 있습니다.</p>
    </li>

</ul>

<img src="/theme/oklassedu/pix/images/ex_progress.jpg" alt="img_brd" class="margin_bt15 img_brd" />
<ul class="margin_bt50">
    <li class="margin_bt15">
        1. 강의실의 모든 차시를 합친 진도율입니다.
    </li>
    <li class="margin_bt15">
        2. 한 차시 안의 모든 동영상 강의의 진도율을 합친 진도율입니다.
    </li>
    <li class="margin_bt15">
        3. 동영상 강의 하나의 재생 진행도가 표시됩니다.
    </li>
    <li class="">
        4. 동영상 재생되는 상단에도 현재 재생 중인 영상의 진도율이 재생시간에 비례해서 표시됩니다.
    </li>
</ul>

<div class="div_title top">
    진도율이 미완료일 경우
</div>

<img src="/theme/oklassedu/pix/images/ex_progress2.jpg" alt="img_brd" class="margin_bt15 img_brd" />
<ul class="margin_bt50">
    <li class="margin_bt15">
        <strong>혹시 네트워크 에러로 학습 미완료가 뜨는 경우 동영상 플레이 화면 우측 상단을 살펴보시고, 만약 서버에 미이수 구간으로 기록된 부분이 보이면, "E(end)" 부분을 
            클릭하시어(위 그림의 D) 해당 부분부터 교육를 다시 시작하실 수 있습니다.</strong>
    </li>
    
</ul>


<?php
echo $OUTPUT->footer();
?>


