<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("고객지원실");
$PAGE->navbar->add("교육도우미");
$PAGE->navbar->add("학습방법");

echo $OUTPUT->header();
?>  
<h3 class="page_title">학습방법</h3>
<div class="textbox img02">
    <strong class="orange">교육도우미는 교육를 받는데 필요한 필수적인 내용을 안내해드립니다.</strong>
    <p>내용을 확인하시고 필요한 교육를 진행하세요.</p>
</div>

<div class="div_title top">
    학습방법
</div>
<ul class="dash margin_bt50">
<!--    <li class="margin_bt15">
        원격교육는 온라인강의 수강을 기본으로 하며, 각 과정별로 전담강사와 강의운영자가 배치되어 과제, 온라인 시험등을 진행하고 수강생의 질문에24시간 이내에 답변합니다.
    </li>-->
    <li class="margin_bt15">
       강의실이 만들어지면 학습자는 강의실에 입장하여 교육기간 내에 학습하면 됩니다.
    </li>
</ul>

<div class="div_title top">
    학습시간
</div>
<ul class="dash margin_bt50">
    <li class="margin_bt15">
        강의는 교육기간 중에는 24시간 휴일 없이 진행되므로 교육생이 편한 시간에 접속하여 학습할 수 있습니다.
    </li>
<!--    <li class="margin_bt15">
        교육생은 학교나 가정 등의 장소에서 원격교육교육원 홈페이지에 접속하여 하루에 약 2시간 정도 학습하실 것을 권장합니다.
    </li>-->
    <li class="margin_bt15">
         동영상강의 시청시 재생시간에 비례하여 진도율이 반영됩니다.
    </li>
<!--    <li class="margin_bt15">
        교육종료 후에도 수료여부에 관계없이 6개월간 재학습을 하실 수 있습니다.
    </li>-->

</ul>

<div class="div_title top">
    브라우저환경
</div>
<ul class="dash margin_bt50">
    <li class="margin_bt15">
        익스플로러9.0 이상 , 크롬, 파이어폭스 에서 이용가능합니다.
    </li>
    <li class="margin_bt15">
        본 홈페이지는 크롬브라우저에 최적화 되어 있습니다.
    </li>

</ul>



<?php
echo $OUTPUT->footer();
?>


