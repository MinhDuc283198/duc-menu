<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//url을 만들어주는 함수
function local_notice_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
	global $CFG, $DB;
        require_login();
	$fileareas = array('attachment', 'contents', 'feedback_file');
	if (!in_array($filearea, $fileareas)) {
		return false;
	}


	$fs = get_file_storage();
	$relativepath = implode('/', $args);

	$fullpath = "/$context->id/local_notice/$filearea/$relativepath";
	if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
		return false;
	}


	// finally send the file
	send_stored_file($file, 0, 0, true);
};


function noticeboard_editor_options($context, $contentid) {
	global $COURSE, $PAGE, $CFG;
// TODO: add max files and max size support
	$maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
	return array(
		'maxfiles' => EDITOR_UNLIMITED_FILES,
		'maxbytes' => $maxbytes,
		'trusttext' => true,
		'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
		'subdirs' => file_area_contains_subdirs($context, 'local_notice', 'attachment', $contentid)
                             //file_area_contains_subdirs(context $context, $component, $filearea, $itemid)
            );
}

function page_list($list){
    //옵션셀렉트 만드는 함수
    $textary = [get_string('showfive','local_notice'),get_string('showten','local_notice'),get_string('showfifteen','local_notice'),get_string('showtwenty','local_notice'),get_string('showthirty','local_notice')];
    $fqoption = array();
      for($i=0;$i<=4;$i++){
        $arr = array();
        switch ( $i ) {
            case 0:
               $arr['val'] = 5;
              break;
            case 1:
               $arr['val'] = 10;
              break;
            case 2:
               $arr['val'] = 15;
              break;
            case 3:
               $arr['val'] = 20;
              break;
            case 4:
               $arr['val'] = 30;
              break;
        }
        $arr['name']= $textary[$i];
        $arr['is']= 0;
        if($arr['val']==$list){
             $arr['is']=1;
        }
         $fqoption[]=$arr;
    }
    return $fqoption;
}

// 해당 유저가 (어떤과정의)과정담당자인지 확인하는 함수
function fb_user_role_chk($userid){
    global $DB;
    $sql = "SELECT COUNT(*) FROM {dlp_user} du
           JOIN LGAPORTAL.dbo.TCM_USER_PRIV_GRANT tupg ON tupg.mem_cd = du.mem_cd
           WHERE du.userid = :userid AND tupg.priv_code = 'G000000003' AND tupg.use_f = 'Y'
            ";
    $rolelists = $DB->count_records_sql($sql,array('userid'=>$userid));
    
    if($rolelists >= 1){
        return true;
    } else {
        return false;
    }
    
}

//function file_get($context,$boardid){
//     $fs = get_file_storage();
//     $fp = get_file_packer();
//     $files = $fs->get_area_files($context->id, 'local_notice', 'attachment', $boardid, 'timemodified', false);
//     if($files){
//            $filecount = count($files);
//            if(1<$filecount){
//            $fileary = array();
//            foreach ($files as $file) {
//                if($file->get_filename()!="notice.zip"){
//                    $path = $file->get_filepath() . $file->get_filename();
//                    $path = ltrim($path, '/');
//                    $fileary[$path] = $file;
//                } else {
//                    $file->delete();
//                }
//            }
//            $filename ='notice.zip';
//                $filezip=$fp->archive_to_storage($fileary, $context->id, 'local_notice', 'attachment',  $boardid, '/', $filename, $USER->id,true, null);
//               $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $boardid . '/' . $filename);
//               return $path;
//               
//            } else {
//                foreach ($files as $file) {
//                    $filename = $file->get_filename();
//                    $mimetype = $file->get_mimetype();
//                    $icon_img= '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)). '" class="icon" alt="' . $mimetype . '" />';
//                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $boardid . '/' . $filename);
//                    return $path;
//                }
//            }      
//        } else {
//                $boardcontent->filedownload = null;
//        }
//}