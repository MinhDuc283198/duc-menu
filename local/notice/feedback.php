<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once("$CFG->libdir/filestorage/zip_archive.php");
require_once("$CFG->libdir/filestorage/file_packer.php");
require_once("$CFG->libdir/filestorage/zip_packer.php");
require_once $CFG->dirroot . '/local/notice/lib.php';

require_login();

//작성된 피드백을 열람 할 수 있는 게시판
if(!is_siteadmin() && !fb_user_role_chk($USER->id)){ //관리자도 아닌데 과정담당자도 아닌사람
   //관리자가 아닌 사람이 접근할경우 차단하기 
    echo "<script>history.back();</script>";
}

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_url($CFG->wwwroot.'/local/notice/feedback.php');
$PAGE->set_title(get_string('feedbacklist','local_notice'));

$PAGE->set_pagelayout('member'); 


$pageNum = optional_param('page', 1 , PARAM_INT);
$list = optional_param('list', 5, PARAM_INT);
$limit = ($pageNum - 1) * $list;
$search_ = optional_param('search', '', PARAM_RAW);
$fbstatus = optional_param('fbstatus',0,PARAM_INT);
$textary = [get_string('progressstatus','local_notice'),get_string('completion','local_notice'),get_string('hold','local_notice'),get_string('checking','local_notice'),get_string('ignoring','local_notice'),get_string('unconfirmed','local_notice')];

//category=2 가 피드백
if (!empty($search_)) {
    $search = str_replace(' ', '', $search_);
    $query ="select * from {notice_board} where delflag=0 and category=2 and title like N'%".$search."%' or content like N'%".$search."%' order by id desc ";
}
else if(isset($fbstatus) && $fbstatus!= 0){
    $query =" select * from {notice_board} where delflag=0 and category=2 and fbstatus=".$fbstatus." order by id desc";
}
//전체불러오기
else {
    $query =" select * from {notice_board} where delflag=0 and category=2 order by id desc";
}
    $boardcontents = $DB->get_records_sql($query, array (), $limit,$list);

$boardcontent = array();
$page_query = 'select count(*) as pagecount from {notice_board} where delflag=0 and category=2';

if (!empty($search)) {
    $page_query =" select count(*) as pagecount from {notice_board} where delflag=0 and category=2 and title like  N'%".$search."%'or content like  N'%".$search."%'";

} else if(isset($fbstatus) && $fbstatus!= 0){
    $page_query =" select count(*) as pagecount from {notice_board} where delflag=0 and category=2 and fbstatus=".$fbstatus;
}

$fqoption = array();
  for($i=0;$i<=5;$i++){
    $arr = array();
    $arr['val']= $i;
    $arr['name']= $textary[$i];
    $arr['is']= 0;
    if($i==$fbstatus){
         $arr['is']=1;
    }
     $fqoption[]=$arr;
}
    
$total_count = $DB->get_record_sql($page_query);
$total_page = ceil( $total_count->pagecount / $list );
//넘버링 : $맨처음번호 = $전체레코드수 - $페이지당리스트출력수 * ($페이지번호 -1) 
$page_number = $total_count->pagecount - $list *($pageNum-1);
//--------------------------------
$todayis = time();
$today_ = gmdate("Y-m-d",$todayis);
$todayary = explode("-",$today_);

//유저 이름 구하기
$usernamequery = "select u.username from {user} u join {notice_board} b on b.userid = u.id where b.id= ?";

foreach ($boardcontents as $boardcontent) {
$username = $DB->get_record_sql($usernamequery,array($boardcontent->id));//완료 보류 확인 무시
    if($boardcontent->timeedited!=0){
    //timeedited 컬럼이 0이 아닌경우 -> 수정된 글인경우
        $time1 = $boardcontent->timeedited;
        $boardcontent->timeedited = gmdate("Y-m-d",$time1);
    } else {
        $time1 = $boardcontent->timecreated;
        $boardcontent->timecreated = gmdate("Y-m-d",$time1);
    }
    $boardcontent->username = $username->username;
    //배열을 넣기
    //for 0~4
    $option = array();
  
    for($j=1;$j<=5;$j++){
        $arr = array();
        $arr['val']= $j;
        $arr['name']= $textary[$j];
        $arr['is']= 0;
        if($j==$boardcontent->fbstatus){
            $arr['is']=1;
        }
        $option[]=$arr;
    }
    $boardcontent->option = $option;
    
    //파일--------------------
     $fs = get_file_storage();
     $fp = get_file_packer();
     $files = $fs->get_area_files($context->id, 'local_notice', 'feedback_file', $boardcontent->id, 'timemodified', false);
     //$files == > array 인 상태로 받아짐
     
     if($files){
            $filecount = count($files);
            if(1<$filecount){
     //파일이 여러개 일 때 zip 으로 하나로 만들어주기... 
            $fileary = array();
            foreach ($files as $file) {
                if($file->get_filename()!="feedback.zip"){
                    $path = $file->get_filepath() . $file->get_filename();
                    $path = ltrim($path, '/');
                    $fileary[$path] = $file;
                } else {
                    $file->delete();
                }
            }
            $filename ='feedback.zip';
                $filezip=$fp->archive_to_storage($fileary, $context->id, 'local_notice', 'feedback_file',  $boardcontent->id, '/', $filename, $USER->id,true, null);
               $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/feedback_file/' . $boardcontent->id . '/' . $filename);
               $boardcontent->filedownload = $path;
               
            } else {
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();
                    $icon_img= '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)). '" class="icon" alt="' . $mimetype . '" />';
                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/feedback_file/' . $boardcontent->id . '/' . $filename);
                    $boardcontent->filedownload = $path;
                }
            }
        } else {
                $boardcontent->filedownload = null;
        }
     //파일끝------------------
     $boardcontent->cnt=$page_number;
     $page_number--;
     $data[] = $boardcontent;
}
$b_pageNum_list = 9;
$block = ceil($pageNum/$b_pageNum_list);
$b_start_page = ( ($block - 1) * $b_pageNum_list ) + 1; 
$b_end_page = $b_start_page + $b_pageNum_list - 1; 
if ($b_end_page > $total_page) {
    $b_end_page = $total_page;
};

//페이징 셋팅 끝

if($pageNum <=1){
    $pageset_first = "<a class='first'></a>";
} else {
    $pageset_first = "<a class='first' href='/local/notice/feedback.php?page=1&list=$list'></a>";
};

if($block <=1){ //10개씩 뒤로가기
    $pageset_end = ''; // block이 1보다 작거나 같으면 뒤로 갈수 없기때문에 공백으로 둠
} else {
    $pageset_b_start_page = $b_start_page-1;
    $pageset_end = "<font><a class='prev' href='/local/notice/feedback.php?page=$pageset_b_start_page&list=$list'></a></font>";
};


$pageset_j = array();
 for($j = $b_start_page; $j <=$b_end_page; $j++){
     $pageset_j[$j-1] = array();
	if($pageNum == $j){
            $pageset_j[$j-1]['pageset'] = '<a class=on>'.$j.'</a>';
        } else {
            if(!empty($search)){
                $pageset_j[$j-1]['pageset'] ="<font><a href='/local/notice/feedback.php?page=$j&list=$list&limit&search=$search'>$j</a></font>";
            } else {
            $pageset_j[$j-1]['pageset'] ="<font><a href='/local/notice/feedback.php?page=$j&list=$list&limit'>$j</a></font>";
                 }
        }
};

$total_block = ceil($total_page/$b_pageNum_list);

if($block >= $total_block){ 
    $pageset_next_end = ''; 
} else {
    $pageset_b_end_page = $b_start_page+1;
    $pageset_next_end = "<font><a class='next' href='/local/notice/feedback.php?page=$pageset_b_end_page&list=$list'></a></font>";

};

if($pageNum<= $total_page){
    $pageset_next = "<a class='last' href='/local/notice/feedback.php?page=$total_page&list=$list'></a>";
} else {
    $pageset_next = "<a class='last'></a>";
};

$PAGE->requires->js_call_amd('local_notice/feedback','init', array());

$baseurl = "local/notice/feedback.php";
$pagevar = "page";

//  --------------------페이징끝-------------------------

//관리자 권한
if(is_siteadmin()){
    $siteadmin="admin";
}



echo $OUTPUT -> header();
//print_object($data); die();
$templatecontext = [
    'boardcontents' => $data, //게시글 데이터
    'pageset_first' => $pageset_first, //처음으로 가는 버튼
    'pageset_end' => $pageset_end, //10개씩 앞으로 가는 버튼
    'pageset_j' => $pageset_j, //개별페이지버튼
    'pageset_next_end'=> $pageset_next_end, //10개씩 뒤로가는 버튼
    'pageset_next' => $pageset_next,//마지막으로 가는 버튼
    'fqoption' => $fqoption
    ];



echo $OUTPUT->render_from_template('local_notice/feedback', $templatecontext); 
echo $OUTPUT -> footer();