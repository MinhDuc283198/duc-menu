<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
$PAGE->set_context($context);       

$PAGE->set_url('/local/notice/mobile_feedback.php');
$PAGE->set_pagelayout("member");

if(is_siteadmin()){
    $PAGE->set_title(get_string('feedbacklist','local_notice'));
    echo redirect($CFG->wwwroot.'/local/notice/feedback.php');
} else {
    $PAGE->set_title(get_string('sendfeedback','local_mobile_feedback')); // 김성룡 9/20
    $PAGE->requires->js_call_amd('local_notice/feedback','mobile_feedback', array());
    echo $OUTPUT->header();
    echo $OUTPUT->render_from_template('local_notice/feedbackwrite',$context); 
    
}


echo $OUTPUT -> footer();