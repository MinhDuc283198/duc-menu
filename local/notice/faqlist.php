<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/notice/lib.php';
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url($CFG->wwwroot.'/local/notice/faqlist.php');
$PAGE->set_title(get_string('faq','local_notice'));

$PAGE->set_pagelayout('member'); 
//  --------------------페이징---------------------------
$pageNum = optional_param('page', 1 , PARAM_INT);      // 아래에 표시되는 페이지 넘버
$list = optional_param('list', 5, PARAM_INT);  //page : default - 5 화면에 표시되는 글의 갯수
$limit = ($pageNum - 1) * $list; //시작점
$search_ = optional_param('search', '', PARAM_RAW);



// 검색어가 있을경우
if (!empty($search_)) {
    //검색어 공백제거
    $searchw = str_replace(' ', '', $search_);
    $search = '%'.$searchw.'%';
    $query ="select * from {notice_board} where delflag=0 and category=1 and title like :search or delflag=0 and category=1 and content like :search2 order by id desc ";
}
else {
    $query =" select * from {notice_board} where delflag=0 and category=1 order by id desc";
}
    $boardcontents = $DB->get_records_sql($query, array ("search"=>$search,"search2"=>$search), $limit,$list);


//-----총 게시글의 페이지수↓-----
$page_query = 'select count(*) as pagecount from {notice_board} where delflag=0 and category=1';

$boardcontent = array();

// 검색어 있을때
if (!empty($search)) {
    $page_query =" select count(*) as pagecount from {notice_board} where delflag=0 and category=1 and  title like  :search or delflag=0 and category=1 and content like :search2";
    $total_count = $DB->get_records_sql($page_query,array("search"=>$search,"search2"=>$search));
} else {
    $total_count = $DB->get_record_sql($page_query);
}

$total_page = ceil( $total_count->pagecount / $list );

// 게시글 번호 매기기

//넘버링 : $맨처음번호 = $전체레코드수 - $페이지당리스트출력수 * ($페이지번호 -1) 
$page_number = $total_count->pagecount - $list *($pageNum-1);

//--------------------------------
$todayis = time();
$today_ = gmdate("Y-m-d",$todayis);
$todayary = explode("-",$today_);


//불러올 파일 셋팅
foreach ($boardcontents as $boardcontent) {
    if($boardcontent->timeedited!=0){
    //timeedited 컬럼이 0이 아닌경우 -> 수정된 글인경우
        $time1 = $boardcontent->timeedited;
        $boardcontent->timeedited = gmdate("Y-m-d",$time1);
    } else {
        $time1 = $boardcontent->timecreated;
        $boardcontent->timecreated = gmdate("Y-m-d",$time1);
    }
    
   //파일--------------------
     $fs = get_file_storage();
     $fp = get_file_packer();
     $files = $fs->get_area_files($context->id, 'local_notice', 'attachment', $boardcontent->id, 'timemodified', false);
     if($files){
            $filecount = count($files);
            if(1<$filecount){
            $fileary = array();
            foreach ($files as $file) {
                if($file->get_filename()!="FAQ.zip"){
                    $path = $file->get_filepath() . $file->get_filename();
                    $path = ltrim($path, '/');
                    $fileary[$path] = $file;
                } else {
                    $file->delete();
                }
            }
            $filename ='FAQ.zip';
                $filezip=$fp->archive_to_storage($fileary, $context->id, 'local_notice', 'attachment',  $boardcontent->id, '/', $filename, $USER->id,true, null);
               $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $boardcontent->id . '/' . $filename);
               $boardcontent->filedownload = $path;
               $boardcontent->filename = $file->get_filename();
               
            } else {
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();
                    $icon_img= '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)). '" class="icon" alt="' . $mimetype . '" />';
                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $boardcontent->id . '/' . $filename);
                    $boardcontent->filedownload = $path;
                    $boardcontent->filename = $file->get_filename();
                }
            }
        } else {
                $boardcontent->filedownload = null;
        }
     //파일끝------------------
     $boardcontent->cnt=$page_number;
     $page_number--;
     $data[] = $boardcontent;
}

if(!empty($search)){
    if($total_count->pagecount==0){
        $searchcnt->searchcount =" 0";
    } else {
        $searchcnt->searchcount= $total_count->pagecount;
    }
    $searchcnt->searchword = $searchw;
    }

$b_pageNum_list = 9;
//블럭에 나타낼 페이지 번호 갯수
$block = ceil($pageNum/$b_pageNum_list);
$b_start_page = ( ($block - 1) * $b_pageNum_list ) + 1; 
//현재 블럭에서 시작페이지 번호
$b_end_page = $b_start_page + $b_pageNum_list - 1; 
//현재 블럭에서 마지막 페이지 번호





if ($b_end_page > $total_page) { // 총 페이지수와 마지막 페이지를 같을수있게
    // 9 > 3
    $b_end_page = $total_page;
};

//페이징 셋팅 끝

if($pageNum <=1){
    $pageset_first = "<a class='first'></a>";
} else {
    $pageset_first = "<a class='first' href='/local/notice/faqlist.php?page=1&list=$list'></a>";
};

if($block <=1){ //10개씩 뒤로가기
    $pageset_end = ''; // block이 1보다 작거나 같으면 뒤로 갈수 없기때문에 공백으로 둠
} else {
    $pageset_b_start_page = $b_start_page-1;
    $pageset_end = "<font><a class='prev' href='/local/notice/faqlist.php?page=$pageset_b_start_page&list=$list'></a></font>";
};


 // j(페이지시작번호)= 1 ; 1 <= 9(페이지끝나는번호) ; 1++
$pageset_j = array();
 for($j = $b_start_page; $j <=$b_end_page; $j++){
     $pageset_j[$j-1] = array();
	if($pageNum == $j){ //pageNum 와 j 가 같으면 현재 페이지 이므로
            //링크없이 현재페이지만 출력
            $pageset_j[$j-1]['pageset'] = '<a class=on>'.$j.'</a>';
        } else {
            if(!empty($search)){
                $pageset_j[$j-1]['pageset'] ="<font><a href='/local/notice/faqlist.php?page=$j&list=$list&limit&search=$search'>$j</a></font>";
            } else {
            $pageset_j[$j-1]['pageset'] ="<font><a href='/local/notice/faqlist.php?page=$j&list=$list&limit'>$j</a></font>";
                 }
        }
};

// 3/9 = 1
$total_block = ceil($total_page/$b_pageNum_list);

if($block >= $total_block){ 
    $pageset_next_end = ''; 
} else {
    $pageset_b_end_page = $b_start_page+1;
    $pageset_next_end = "<font><a class='next' href='/local/notice/faqlist.php?page=$pageset_b_end_page&list=$list'></a></font>";

};

if($pageNum <= $total_page){
    $pageset_next = "<a class='last' href='/local/notice/faqlist.php?page=$total_page&list=$list'></a>";
} else {
    $pageset_next = "<a class='last'></a>";
};

$PAGE->requires->js_call_amd('local_notice/index','init', array());

$baseurl = "local/notice/faqlist.php";
$pagevar = "page";

//  --------------------페이징끝-------------------------

//관리자 권한
if(is_siteadmin()){
    $siteadmin="admin";
}
$page_option = page_list($list);


echo $OUTPUT -> header();


$templatecontext = [
    'boardcontents' => $data, //게시글 데이터
    'pageset_first' => $pageset_first, //처음으로 가는 버튼
    'pageset_end' => $pageset_end, //10개씩 앞으로 가는 버튼
    'pageset_j' => $pageset_j, //개별페이지버튼
    'pageset_next_end'=> $pageset_next_end, //10개씩 뒤로가는 버튼
    'pageset_next' => $pageset_next, //마지막으로 가는 버튼
    'siteadmin' => $siteadmin, //관리자 권한확인
    'searchcnt' =>$searchcnt, //검색어 있을때 갯수
    'searchdata'=>$searchdata  ,
    'option' => $page_option
];



echo $OUTPUT->render_from_template('local_notice/faq', $templatecontext); 
echo $OUTPUT -> footer();