<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot.'/local/notice/write_mform.php';
require_once $CFG->dirroot . '/local/notice/lib.php';
if(!is_siteadmin()){
   redirect($CFG->wwwroot.'/local/notice/index.php');
}
$title = optional_param('title', '', PARAM_TEXT);
$content = optional_param('content', '', PARAM_TEXT);
$id = optional_param('id', 0, PARAM_INT);
$category= optional_param('category', 0, PARAM_INT); 
// 0으로 넘어올경우 공지사항 / 1은 도움말
function goto_caution($msg, $url){
 $str = "<script>";
 $str .= "alert('{$msg}');";
 $str .= "location.href = '{$url}';";
 $str .= "</script>";
 echo("$str");
 exit;
}

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url('/local/notice/write.php?category='.$category);
$PAGE->set_title(get_string('writeandedit','local_notice'));
//$PAGE->set_pagelayout('member'); 

$mform = new write_mform();

if($mform->is_cancelled()){
    
     if($category==0){
        echo redirect($CFG->wwwroot.'/local/notice/index.php');
    } else if ($category==1){
        echo redirect($CFG->wwwroot.'/local/notice/faqlist.php');
    } 
    
    
} else if ($fromform = $mform->get_data()){
    //submit 버튼을 눌렀을대 데이터를 불러옴
    print_object($fromform);

    $contents = new stdClass();
    $contents->userid = $USER->id;
    $contents->title = $fromform->title;
    $contents->content = $fromform->content['text'];
    $contents->timecreated = time();
    $contents->assesstimefinish = $fromform->assesstimefinish;
    
    if($category==0){
        //공지사항
        $contents->category=0;
    } else if ($category==1){
         $contents->category=1;
    } 
    
    if($id!=0){    
        //id 값이 0이 아니면 이미 쓰여졌던 글이니 업데이트(hidden으로 받은 id값 받아오기)
         $contents->id = $fromform->id;
         $contents->edit_userid = $USER->id;
         $contents->timeedited = time();
         $contents->id = $DB -> update_record('notice_board',$contents);    
         $contents->contents = file_save_draft_area_files($fromform->content['itemid'], $context->id, 'local_notice', 'contents', $contents->id, noticeboard_editor_options($context, $contents->id), $contents->content);
         $DB->set_field('notice_board', 'content', $contents->contents, array('id' => $contents->id));
         $draftitemid = file_get_submitted_draft_itemid('attachments');
         file_save_draft_area_files($draftitemid, $context->id, 'local_notice', 'attachment', $contents->id);
      } else {
          // 새글 등록
          // 게시글 중복검사---
          $chksql = 'select top 1 * from {notice_board} where category= :category and delflag=0 order by id desc';
          $chkcontent = $DB->get_record_sql($chksql,array('category'=>$category));
          if($chkcontent->title==$contents->title && $chkcontent->contnent==$contents->contnent){
              $msg = get_string('overlap','local_notice'); // 김성룡 9/20
              $url = $CFG->wwwroot.'/local/notice/write.php?category='.$category;
              goto_caution($msg, $url);
              
          } else {
            $contents->edit_userid = 0;
            $contents->id =  $DB -> insert_record('notice_board',$contents); //리턴값 = id 
            //file_save_draft_area_files($content->itemid, $context->id, 'local_notice', 'contents', $content->id, jinoboard_editor_options($context, $content->id), $content->contents['text']);

            $contents->contents = file_save_draft_area_files($fromform->content['itemid'], $context->id, 'local_notice', 'contents', $contents->id, noticeboard_editor_options($context, $contents->id), $contents->content);

            $DB->set_field('notice_board', 'content', $contents->contents, array('id' => $contents->id));
            //파일 업로드
            $draftitemid = file_get_submitted_draft_itemid('attachments');
            file_save_draft_area_files($draftitemid, $context->id, 'local_notice', 'attachment', $contents->id);  
                       //              $draftitemid, $contextid,       $component,         $filearea,      $itemid, array $options=null, $text=null, $forcehttps=false)   
          }
     
         }
   if($category==0){
        //공지사항
//       echo '<script type="text/javascript">location.href="'.$CFG->wwwroot.'/local/notice/index.php"</script>';
       redirect($CFG->wwwroot.'/local/notice/index.php');
    } else if ($category==1){
      redirect($CFG->wwwroot.'/local/notice/faqlist.php');
    } 
    
} 

if($id!=0){
    
    $contents = $DB->get_record('notice_board', array('id'=>$id));
    $contents->content = file_rewrite_pluginfile_urls($contents->content, 'pluginfile.php', $context->id, 'local_notice', 'contents', $contents->id);
    
    
    $draftitemid = file_get_submitted_draft_itemid('attachments');
    file_prepare_draft_area($draftitemid, $context->id, 'local_notice', 'attachment', empty($contents->id) ? null : $contents->id, write_mform::attachment_options());
 
    
    $mform->set_data(
         array(
             'attachments' => $draftitemid,
            'id'=>$contents->id,
            'title'=>$contents->title,
            'time' => $contents->timecreated = time(),
            'content' => 
            array('text' => $contents->content,'format' => 1,'itemid' => $contents->id,),
        ));
}

echo $OUTPUT -> header();
$PAGE->requires->js_call_amd('local_notice/index','init', array());
//글 수정일때 셋팅하기..
if($category==0){
    $categorytitle = get_string('notice', 'local_notice');
    
} else if ($category==1){
    $categorytitle = 'FAQ';
}

echo '<div id="ct" class="introduction">'
. '<div class="container">
		<h2 class="title">'.$categorytitle.'</h2><br>
		
                ';
    $mform->display(); // 화면에 폼 양식 표시함
echo '</div></div>';
echo $OUTPUT -> footer();