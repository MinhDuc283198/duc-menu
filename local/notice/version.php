<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019122700; // 추가한 플러그인의 버전 (언제 만들었는지 + 00 01 02..
$plugin->requires  = 2012061700;  // 필요로하는 무들 버전      
$plugin->component = 'local_notice';  //플러그인 풀네임 