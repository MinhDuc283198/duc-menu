<?php


$functions = array(

    'local_notice_getcontentslist' => array(
        'classname' => 'local_notice_external',
        'methodname' => 'get_contentslist',
        'classpath' => 'local/notice/externallib.php',
        'description' => 'Returns a list of board.',
        'type' => 'read',
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
        'loginrequired'=>false,
        'ajax'          => true 
    ),

);