<?php
function xmldb_local_notice_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    $dbman = $DB->get_manager();

        if ($oldversion < 2019011100) {
        $table = new xmldb_table('notice_board');
        $field = new xmldb_field('assesstimefinish', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'timeedited');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019011100, 'local', 'notice');
    }
    if ($oldversion < 2019011500) {
        $table = new xmldb_table('notice_board');
        $field = new xmldb_field('fbstatus', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0', 'category');

        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019011500, 'local', 'notice');
    }

     if ($oldversion < 2019122700) {
        $table = new xmldb_table('notice_board');
        
        $field = new xmldb_field('titlevi', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('titleen', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('contentvi', XMLDB_TYPE_TEXT, 'small', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('contenten', XMLDB_TYPE_TEXT, 'small', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

    }
    return true;
}

