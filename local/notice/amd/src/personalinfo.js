define(['core/ajax', 'jquery', 'core/templates', 'core/notification', 'core/config'],
        function (ajax, $, templates, notification, cfg) {
            return {
                init: function () {
                    this.getcontentslist();
                },

                getcontentslist: function () {
                   ajax.call([{
                            methodname: 'local_notice_getcontentslist'
                            ,args: {page:1}
                        }],true,false)[0].then($.proxy(function (results) {
                        console.log(results); 
                            if (results.boardlist.length > 0) {
                                this.renderboardlist(results);
                                //console.log(results);
                            }
                        }, this)).fail(function (e) {
                        console.log(e);
                        //location.href = e.link;
                    });
                },
                
                renderboardlist: function (data) {
                    var url = 'local_notice/personalinfo';
                    var p_tag = ".right-block";
                    templates.render(url, data)
                            .then($.proxy(function (html, js) {
                                $(p_tag).empty().append(html);
                                utils.loading.hide();
                                utils.init();
                            }, this))
                            .fail(function (ex) {
                                console.log(ex);
                            });
                },
            }
             
        });

