define(['jquery', 'core/templates', 'core/str'],function($,Templates,str) {

            $(function () {
                $(".faqlistdivbox").click(function (e) {
//                        e.preventDefault();
                    var classname= $(this).attr('class');
                    if(classname=="tg-wrp faqlistdivbox"){
                        $(this).addClass("on");
                    } else {
                        $(this).removeClass("on");
                    }
                    ;
                });
            });
            $(function () {
                $(".tg-content").click(function (e) {
                    e.stopPropagation();
                });
            });
    return {   
        init: function(){
           $(".ftoggler").css('display','none');
    var lang = [];
            str.get_strings('suercancel','local_notice').then(function (langString){
                lang.suercancel = langString[0];
            });
            
    //faqlist.php
    $(function() {
      $(".generaltable td.faqtitle").click(function(){
             var tid = $(this).attr('data-target');
             $('tr.'+tid).fadeToggle();
       });
    });
      $(function() {
        $(document).on('click', '.delchkplz', function() {
           var category = $(this).children(".categoryval").val();
            var id = $(this).children(".indexdelval").val();
           var confirmval = confirm("게시글이 삭제됩니다.");
           if(confirmval==true){
                window.location.href='/local/notice/delete.php?status=notice&id='+id+'&category='+category;
            } else {

            }
         });
    });
    
    //체크박스 전체 선택 / 해제 
       $(document).on('click', '.allchk', function() {
            if($(".allchk").prop("checked")){
                $("input[name=delchk]").prop("checked",true);
            }else{
                $("input[name=delchk]").prop("checked",false);
           }
      });
        //multiple delete !! 
    $(document).on('click', '#muldel', function() {
        if(confirm(lang.suercancel)==true){
            //체크박스 체크된 라인 ID 값을 어레이로 받아와서 ajax로 어레이 넘겨주기
            var ids = [];
            $("input[name=delchk]:checked").each(function(index) {
                //each = 선택한 요소가 여러개일때 각각에 대하여 반복하여 함수 실행함.
               ids[index]= $(this).siblings(".idval").val();
             });
            $.ajax({
                url:"delete.php",
                type:'POST',
                dataType:'json',
                data:{id:ids,status:"faq"},
                success:function(data){
                    window.location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                 }
            });
        } else {
            return false;
        }      
    });
        //ajax . faqlist.php 에서 클릭했을때 조회수 +1
        $(document).on('click', '.title', function() {
            var thisval = this;
            var id = $(thisval).find(".idval").val();
            $.ajax({
                url:"faq_viewcnt.php",
                type:'POST',
                dataType:'json',
                data: {id:id},
                success:function(data){
                    $(thisval).children(".view").text(data);
                }
            });
        });

                $('#page_list').change(function(){
                var list = $(this).val();
                location.href="index.php?list="+list;
                });
                $('#faqpage_list').change(function(){
                var list = $(this).val();
                location.href="faqlist.php?list="+list;
                });
         }
    }
});
