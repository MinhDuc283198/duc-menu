define(['jquery', 'core/templates', 'core/modal_factory', 'core/modal_events', 'core/str', 'core/notification'],
        function ($, Templates, ModalFactory, ModalEvents, str, notification) {
            return {
                init: function () {
            var lang = [];
            //sessionStorage.getItem("app") == 'Y'
             $(function() {
                 if(sessionStorage.getItem("app") == 'Y'){
                     $('.fbchkmoba').attr('style', "display:none;");
                 }
               });
            str.get_strings([
                    {'key': 'suercancel', component: 'local_notice'},
                    {'key': 'sendfeedback', component: 'local_notice'}
                    
                    
                ]).then(function (langString) {
                     lang.suercancel= langString[0];
                     lang.sendfeedback = langString[1];
                     
                });
            $(function() {
                 $(".tb-notice td.tit").click(function(){
                        var tid = $(this).attr('data-target');
                        $('tr.'+tid).fadeToggle();
                  });
               });
            $(function () {
                $(".tg-wrp").click(function (e) {
                    e.preventDefault();
                    var classname= $(this).attr('class');
                    if(classname=="tg-wrp"){
                        $(this).addClass("on");
                    } else {
                        $(this).removeClass("on");
                        
                    }
                    ;
                });
            });

            //체크박스 전체 선택 / 해제 
            $(document).on('click', '.allchk', function () {
                if ($(".allchk").prop("checked")) {
                    $("input[name=delchk]").prop("checked", true);
                } else {
                    $("input[name=delchk]").prop("checked", false);
                }
            });
            //multiple delete !! 
            $(document).on('click', '#fbmuldel', function () {
                if (confirm(lang.suercancel) == true) {
                    var ids = [];
                    $("input[name=delchk]:checked").each(function (index) {
                        ids[index] = $(this).siblings(".idval").val();
                    });
                    $.ajax({
                        url: "delete.php",
                        type: 'POST',
                        dataType: 'json',
                        data: {id: ids, status: "status"},
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText);
                        }

                    });
                } else {
                    return false;
                }

            });

            $('.fbstatus').change(function () {
                var value = $(this).val();
                var id = $(this).siblings(".idstatus").val();
                $.ajax({
                    url: "feedbackstatus.php",
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id, status: value},
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR.responseText);
                    }

                });

            });
            //셀렉옵션 진행상황별 리스트가져오기
              $('#status_list').change(function () {
                var statusval = $(this).val();
                if(statusval==0){
                    location.href = "feedback.php";
                } else {
                location.href = "feedback.php?fbstatus=" +statusval;
            }
            });
                    $('#page_list').change(function () {
                        var list = $(this).val();
                        location.href = "feedback.php?list=" + list;
                    });
                    this.feedback_modal();
                },
                feedback_modal: function () {
                    var lang = [];
                   
                    if (sessionStorage.getItem("app") == 'Y') {
                            utils.openTabs();
                    }
    
                    str.get_strings([
                            {'key': 'suercancel', component: 'local_notice'},
                            {'key': 'sendfeedback', component: 'local_notice'},
                            {'key': 'loginplz', component: 'local_notice'},
                            {'key': 'threefile', component: 'local_notice'},
                            {'key': 'isnotfile', component: 'local_notice'},
                            {'key': 'entercontent', component: 'local_notice'},
                            {'key': 'entertitle', component: 'local_notice'},
                            {'key': 'feedbackwriten', component: 'local_notice'},

                        ]).then(function (langString) {
                             lang.suercancel= langString[0];
                             lang.sendfeedback = langString[1];
                             lang.loginplz = langString[2];
                             lang.threefile = langString[3];
                             lang.isnotfile = langString[4];
                             lang.entercontent = langString[5];
                             lang.entertitle = langString[6];
                             lang.feedbackwriten = langString[7];
                        });
//                var trigger = $('#feedbackwrite_footer'); //트리거 걸 객체
                    ModalFactory.create({
                        title: '피드백 보내기',
                        body: Templates.render('local_notice/feedbackwrite', {}), //렌더링
                        footer: '',
                    }).done(function (modal) {
                        $('.feedbackwrite_footer').click(function() {
                            $.ajax({
                                url: M.cfg.wwwroot + "/theme/oklassv3/ajax/loginchk.php",
                                type: "POST",
                                dataType: "json",
                                success: function (data) {
                                    if (data.auth == 'admin' || data.auth == 'po') {
                                        //관리자이거나 과정담당자인경우
                                        location.href = M.cfg.wwwroot + "/local/notice/feedback.php";
                                    } else {
                                        //로그인 한 유저의 경우
                                        modal.show();
                                    } 
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(jqXHR.responseText);
                                }

                            });
                        });
                        //피드백 첨부파일 업로드시 특정영역에 파일이름 나타내기
                        $(document).on('change', '#fbfiles.none', function(){
                            if(window.FileReader){
                                var filename = $(this)[0].files[0].name;
                            } else {
                                var filename = $(this).val().split('/').pop().split('\\').pop();
                            }
                            $('.file-txt').text(filename);
                        });
                        //file 업로드를 위한 form 동적생성
                        //사용자가 파일업로드 버튼을 클릭하면
                        //파일 3개까지 업로드가능..
                        var form = $('<form></form>');
                        var url = '';
                        form.attr('action', url);
                        form.attr('method', 'post');
                        form.attr('enctype', 'multipart/form-data');

                        $(document).on('click', '.filebtn', function () {
                            var fileupload = $("<input multiple='multiple' type='file' name='mulfile[]' id='fbfiles'> ");
                            if ($('input[name="mulfile[]"]').length < 1) {
                                form.append(fileupload);
                                form.appendTo('.insertfile');
                            }
                        });
                        $(document).on("change", "input[name='mulfile']", function () {
                            //파일이 변경될때마다 유효성검사 하기
                            var ext = this.value.match(/\.(.+)$/)[1].toLowerCase();
                            switch (ext) {
                                case 'jpg':
                                case 'jpeg':
                                case 'png':
                                case 'gif':
                                case 'war':
                                case 'zip':
                                case 'xlsx':
                                case 'docx':
                                case 'hwp':
                                case 'pdf':
                                case 'pptx':
                                case 'xml':
                                    break;
                                default:
                                    notification.alert('',lang.isnotfile,'확인');
                                    this.value = '';
                            }
                        });
                        $(document).on('click', '.fbcanclebtn', function () {
                            modal.hide();
                        });
                        
                        $(document).on('click', '.submitbtn', function () {
                            var title = $("#title").val();
                            var content = $('#content').val();
                            var formData = new FormData();
                            if($("input[type='file']").val() != null){
                                var filearylen = document.getElementById('fbfiles').files.length;
                                var file = [];
                                if(filearylen>=4){
                                    notification.alert('',lang.threefile,'확인');
                                    document.getElementById("fbfiles").value = "";
                                    return;
                                 } else {
                                    for(var i=0;filearylen-1>=i;i++){
                                         file[i] = document.getElementById('fbfiles').files[i];
                                         formData.append("feedback_file[]", file[i]);
                                    }
                              }
                          }
                            if (content == "") {
                                notification.alert('', lang.entercontent,'확인');
                                $('#content').focus();
                            } else if (title == "") {
                                notification.alert('', lang.entertitle,'확인');
                                $("#title").focus();
                            } else if (content != "" && title != "") {
                                formData.append("title", title);
                                formData.append("content", content);
                                
                                $.ajax({
                                    type: 'post',
                                    url: M.cfg.wwwroot + '/local/notice/feedbackwrite.php',
                                    data: formData,
                                    dataType: 'json',
                                    processData: false, //true : data의 파일형태가 query String으로 전송. false : non-processed data 
                                    contentType: false, //  false : multipart/form-data 형태로 전송되기 위한 옵션값
                                    success: function (data) {
                                        $(document).on('click', '.confirmation-buttons > .btn-primary', function () {
                                            modal.hide();
                                            location.reload();
                                        });
                                        notification.alert('', lang.feedbackwriten,'확인');
                                    },
                                    error: function (xhr, status, error) {
                                        alert(error);
                                    }
                                });
                            }
                        });
                    });
                },
                mobile_feedback: function (){
                      $(function() {
                           str.get_strings([
                            {'key': 'suercancel', component: 'local_notice'},
                            {'key': 'sendfeedback', component: 'local_notice'},
                            {'key': 'loginplz', component: 'local_notice'},
                            {'key': 'threefile', component: 'local_notice'},
                            {'key': 'isnotfile', component: 'local_notice'},
                            {'key': 'entercontent', component: 'local_notice'},
                            {'key': 'entertitle', component: 'local_notice'},
                            {'key': 'feedbackwriten', component: 'local_notice'},

                        ]).then(function (langString) {
                             lang.suercancel= langString[0];
                             lang.sendfeedback = langString[1];
                             lang.loginplz = langString[2];
                             lang.threefile = langString[3];
                             lang.isnotfile = langString[4];
                             lang.entercontent = langString[5];
                             lang.entertitle = langString[6];
                             lang.feedbackwriten = langString[7];
                        });
                        
                        var form = $('<form></form>');
                        var url = '';
                        form.attr('action', url);
                        form.attr('method', 'post');
                        form.attr('enctype', 'multipart/form-data');
                        $(document).on('click', '.filebtn', function () {
                            var fileupload = $("<input multiple='multiple' type='file' name='mulfile[]' id='fbfiles'> ");
                            if ($('input[name="mulfile[]"]').length < 1) {
                                form.append(fileupload);
                                form.appendTo('.insertfile');
                            }
                        });
              });
                        $(document).on("change", "input[name='mulfile']", function () {
                            //파일이 변경될때마다 유효성검사 하기
                            var ext = this.value.match(/\.(.+)$/)[1].toLowerCase();
                            switch (ext) {
                                case 'jpg':
                                case 'jpeg':
                                case 'png':
                                case 'gif':
                                case 'war':
                                case 'zip':
                                case 'xlsx':
                                case 'docx':
                                case 'hwp':
                                case 'pdf':
                                case 'pptx':
                                case 'xml':
                                    break;
                                default:
                                    alert(lang.isnotfile);
                                    this.value = '';
                            }
                        });
                        $(document).on('click', '.fbcanclebtn', function () {
                            history.back();
                        });
                        
                        $(document).on('click', '.submitbtn .mobafeedback', function () {
                            var title = $("#title").val();
                            var content = $('#content').val();
                            var formData = new FormData();
                            if($("input[type='file']").val() != null){
                                var filearylen = document.getElementById('fbfiles').files.length;
                                var file = [];
                                if(filearylen>=4){
                                    notification.alert('',lang.threefile,'확인');
                                    document.getElementById("fbfiles").value = "";
                                    return;
                                 } else {
                                    for(var i=0;filearylen-1>=i;i++){
                                         file[i] = document.getElementById('fbfiles').files[i];
                                         formData.append("feedback_file[]", file[i]);
                                    }
                              }
                          }
                            if (content == "") {
                                notification.alert('', lang.entercontent,'확인');
                                $('#content').focus();
                            } else if (title == "") {
                                notification.alert('', lang.entertitle,'확인');
                                $("#title").focus();
                            } else if (content != "" && title != "") {
                                formData.append("title", title);
                                formData.append("content", content);
                                
                                $.ajax({
                                    type: 'post',
                                    url: M.cfg.wwwroot + '/local/notice/feedbackwrite.php',
                                    data: formData,
                                    dataType: 'json',
                                    processData: false, //true : data의 파일형태가 query String으로 전송. false : non-processed data 
                                    contentType: false, //  false : multipart/form-data 형태로 전송되기 위한 옵션값
                                    success: function (data) {
                                        $(document).on('click', '.confirmation-buttons > .btn-primary', function () {
                                            location.href = M.cfg.wwwroot+ '/local/notice/faqlist.php';
                                        });
                                         notification.alert('', lang.feedbackwriten,'확인');
                                    },
                                    error: function (xhr, status, error) {
                                        alert(error);
                                    }
                                });
                            }
                        });

              }
            
        }
        
        });
        