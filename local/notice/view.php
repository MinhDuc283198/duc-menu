<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/notice/lib.php';

$context = context_system::instance();

$id = required_param('id', PARAM_INT);
//설정한 $parname() 변수명의 겟/포스트를 획득한다.

$PAGE->set_context($context);
$PAGE->set_url($CFG->wwwroot.'/local/notice/view.php?id='.$id);
$PAGE->set_title(get_string('notice','local_notice'));

// To do 공유하기 
$PAGE->set_pagelayout('member'); 


echo $OUTPUT -> header();



// To do 북마크하기
//$bookmark = new stdClass();
//$bookmark->userid =$USER->id;
//$bookmark->username = $USER->name;
//$bookmark->ctime = time();
//print_object($bookmark);
//$plus_bookmark = $DB->insert_record("library_favorite",$bookmark);


//글쓴이 정보 가져오기 


//글정보 가져오기 (delflag=='0' 삭제된상태X)
$sql ="select * from {notice_board} where id=?";
$contents = $DB->get_record_sql($sql, array($id));
$contents->content = file_rewrite_pluginfile_urls($contents->content, 'pluginfile.php', $context->id, 'local_notice', 'contents', $contents->id);

//$delflag = $contents->delflag;
if ($contents->delflag!=0||$contents==null){
    //delflag!=0 인경우 = 게시글을 삭제한 경우 or 게시글이 없는 경우
    redirect($CFG->wwwroot.'/local/notice/index.php',get_string('delete_back','local_view'),2); // 김성룡 9/20
    
}

//클릭(새로고침) 할때마다 조회수 +1
$updateview = $DB->update_record('notice_board', array('id'=>$id,'view_count'=> $contents->view_count+1));

if($contents->timeedited!=0){
    //수정이 한번이라도 되었던 글은 timeedited 컬럼이 0이 아님
    $time1 = $contents->timeedited;
    $contents->timeedited = gmdate("Y-m-d h:i:s",$time1);
} else {
    $time1 = $contents->timecreated;
    $contents->timecreated = gmdate("Y-m-d h:i:s",$time1);
    
}


//이전글
$beforequery = 
       "SELECT *
        FROM {notice_board}
        WHERE delflag=0 and category=0 and id < ".$id."
        ORDER BY id DESC";
$beforepage =$DB->get_records_sql($beforequery, array(), $limitfrom=0, $limitnum=1);

//다음글
$afterquery = 
       "SELECT *
        FROM {notice_board}
        WHERE delflag=0 and category=0 and id > ".$id."
        ORDER BY id ASC";
$afterepage =$DB->get_records_sql($afterquery, array(),$limitfrom=0, $limitnum=1);

$beforepages = array();
$afterepages = array();
foreach($beforepage as $beforepages){
        $beforepages->before = 1;
        $page[] = $beforepages; 
};
foreach($afterepage as $afterepages){
         $afterepages->after = 1;
        $page[] = $afterepages; 
};

//파일---------------------

   $fs = get_file_storage();
   $files = $fs->get_area_files($context->id, 'local_notice', 'attachment', $contents->id, 'timemodified', false);
   //                            (컨텍스트id,        컴퍼넌트,          파일영역,  temid, 정렬에 사용할sort,디렉토리 포함여부)
   $file_download = array();
   //file을 제대로 불러왔을때 실행↓
   if($files){
       foreach ($files as $file) {
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype(); //파일 타입
            $icon_img= '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)). '" class="icon" alt="' . $mimetype . '" />';
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $contents->id . '/' . $filename);
            $file_download['img'] = "<a href='$path'>$icon_img</a>";
            $file_download['text'] = $filename;
            $file_download['path'] = $path;
            //print_object($file_download) 다운받을수있는 링크 + 아이콘이미지
         }
    // 아니면 에러
    } else {
        
    }
//파일끝-------------------
//관리자 권한
if(is_siteadmin()){
    $siteadmin="admin";
}
$templatecontext = [
    'contents' => $contents,
    'page' =>$page,
    'file_download' => $file_download,
    'siteadmin' =>$siteadmin
];
$PAGE->requires->js_call_amd('local_notice/index','init', array());
echo $OUTPUT->render_from_template('local_notice/view', $templatecontext); 

echo $OUTPUT -> footer();