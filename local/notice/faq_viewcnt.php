<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url('/local/notice/faq_viewcnt.php');

$id = optional_param('id', 0, PARAM_INT);


$sql ="select * from {notice_board} where id=:id";
$contents = $DB->get_record_sql($sql, array('id'=>$id));
//클릭(새로고침) 할때마다 조회수 +1
$updateview = $DB->update_record('notice_board', array('id'=>$id,'view_count'=> $contents->view_count+1));
$result = $DB->get_record_sql($sql, array('id'=>$id));


if($result){
   $view_count= $result->view_count;
} else {
    $view_count = 0;
}

echo $view_count;
