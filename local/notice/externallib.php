<?php

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php");
require_once("$CFG->dirroot/user/externallib.php");
require_once("$CFG->dirroot/local/encourage/lib.php");

class local_notice_external extends external_api {
    
    public static function get_contentslist_parameters() {
        return new external_function_parameters(
                array(
                'page' => new external_value(PARAM_INT, 'page', VALUE_DEFAULT, 0)
                )
        );
    }
    
    public static function get_contentslist($page) {
        global $DB, $PAGE, $CFG;

        $context = context_system::instance();
        $PAGE->set_context($context);

        $arrlist = array();
        $arrlist2 = array();
        
        $params = self::validate_parameters(self::get_contentslist_parameters(),array('page' => $page));

        
        $sql = "select * from {notice_board} where category=3 and delflag=''";
        $boardlist = $DB->get_records_sql($sql);



//        if(!empty($boardlist)){
//            $filename = '/var/www/html/test_'.date('Ymd').'.log';
//            $file = fopen($filename,'a') or die("Unable to open file!");
//            if (flock($file,LOCK_EX)){
//                fwrite($file,date('Ymd H:i:s').'타니??'.PHP_EOL.PHP_EOL);
//                // release lock
//                flock($file,LOCK_UN);
//            }else{
//                $filename = '/var/www/html/test_'.date('Ymd').'_lock.log';
//                $file_lock = fopen($filename,'a') or die("Unable to open file!");
//                fwrite($file,date('Ymd H:i:s').'타니??'.PHP_EOL.PHP_EOL);
//                echo "Error locking file!";
//                fclose($file_lock);
//            }
//            fclose($file);  
//        }
        $i=0;
        foreach ($boardlist as $board) {
            $board->class="";
            if($i == 0){
                $board->class="active";
            }
            $arrlist2[] = $board;
            $i++;
        }
        
//        if(!empty($arrlist2)){
//            $filename = '/var/www/html/test_'.date('Ymd').'.log';
//            $file = fopen($filename,'a') or die("Unable to open file!");
//            if (flock($file,LOCK_EX)){
//                fwrite($file,date('Ymd H:i:s').$arrlist2[0]->id.PHP_EOL.PHP_EOL);
//                // release lock
//                flock($file,LOCK_UN);
//            }else{
//                $filename = '/var/www/html/test_'.date('Ymd').'_lock.log';
//                $file_lock = fopen($filename,'a') or die("Unable to open file!");
//                fwrite($file,date('Ymd H:i:s').$arrlist2[0]->id.PHP_EOL.PHP_EOL);
//                echo "Error locking file!";
//                fclose($file_lock);
//            }
//            fclose($file);  
//        }
          //$arrlist2[0]->id = 1;
         

        //$arrlist3 = array('id'=>1,'userid'=>1,'title'=>'1','content'=>'1','view_count'=>1,'delflag'=>1,'timecreated'=>1,'timeedited'=>1);
        //$arrlist2 = $arrlist3;
       
        $arrlist = array('num'=>123);
        $result = array();
        $result['result_num'] = $arrlist;
        $result['boardlist'] = $arrlist2;
        
        return $result;
    }
    
    public static function get_contentslist_returns() {
        return new external_single_structure(
            array(
                'result_num' => new external_single_structure(
                    array(
                        'num' => new external_value(PARAM_INT, 'num')
                    ), 'result_num'
                ),'boardlist' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'id'),
                            'userid' => new external_value(PARAM_INT, 'userid'),
                            'edit_userid' => new external_value(PARAM_INT, 'edit_userid'),
                            'title' => new external_value(PARAM_RAW, 'title'),
                            'content' => new external_value(PARAM_RAW, 'content'),
                            'delflag' => new external_value(PARAM_INT, 'delflag'),
                            'timecreated' => new external_value(PARAM_INT, 'timecreated'),
                            'timeedited' => new external_value(PARAM_INT, 'timeedited'),
                            'assesstimefinish' => new external_value(PARAM_INT, 'assesstimefinish'),
                            'category' => new external_value(PARAM_INT, 'category'),
                            'fbstatus' => new external_value(PARAM_INT, 'fbstatus'),
                            'class' => new external_value(PARAM_RAW, 'class')
                            
                        ), 'boardlist'
                    )
                ), 
            )
        );
    }
    
}

