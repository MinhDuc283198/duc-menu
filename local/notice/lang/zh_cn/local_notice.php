<?php

$string['pluginname'] = 'Notice';
$string['pluginnameplural'] = 'Notice';

// index 
$string['pickupnotice_help'] ='截止到您输入的日期，该公告都会显示在顶部。';
$string['pickupnotice'] ='公示期';
$string['home'] ='主页';
$string['notice'] ='公告';
$string['entersearch'] ='输入搜索词';
$string['number'] ='号码';
$string['title'] ='标题';
$string['createddate'] ='등록';
$string['view'] ='查看';
$string['attach'] ='添加附件';
$string['download'] ='下载';
$string['none'] ='无';
$string['enternotice'] ='등록';


$string['select'] ='选择';
$string['showfive'] ='显示5行';
$string['showten'] ='显示10行';
$string['showfifteen'] ='显示15行';
$string['showtwenty'] ='显示20行';
$string['showthirty'] ='显示30行';

$string['write'] ='新公告';
$string['faqwrite'] ='新FAQ ';
$string['content'] ='内容';

// view
$string['getit'] ='收藏';
$string['shear'] ='分享';
$string['writer'] ='作者';

$string['previous'] ='前一篇';
$string['next'] ='后一篇';
$string['edit'] ='修改';
$string['delete'] ='删除';
$string['list'] ='目录';

// mfrom
$string['postupdate'] ='公示期';

//write php
$string['writed'] = '公告已上传.';
$string['writeandedit'] ='新建&修改公告';
$string['noticeperiod'] ='公示期';


//feq.mustache
$string['faq'] ='帮助';
$string['search'] ='搜索';
$string['searchresult'] ='的搜索结果';
$string['faqlist'] ='常见问题';
$string['apply'] ='어플리케이션 APP';

//view.php
$string['viewpage'] ='显示详细内容';

//delete
$string['deleted'] ='该公告已删除。.';

//feedback.mustache
$string['status'] = '现状';
$string['checkbox'] = '复选框';
$string['unconfirmed'] = '未确认';
$string['completion'] = '완료';
$string['hold'] = '保留';
$string['checking'] = '正在确认';
$string['ignoring'] = '忽略';
$string['feedbacklist'] = '反馈目录';
$string['progressstatus'] = '现状';


// feedback.js + index.js
$string['suercancel'] = '确定要删除吗？';
$string['sendfeedback'] = '发送反馈';
$string['loginplz'] = '您还没有登录，请先登录。';
$string['threefile'] = '最多可上传3个附件。';
$string['isnotfile'] = '文件格式不支持。';
$string['entercontent'] = '请输入内容。';
$string['entertitle'] = '请输入标题。';
$string['feedbackwriten'] = '反馈已填写完毕。';

//feedbackwrite.mustache
$string['fileupload'] = '添加文件';
$string['submission'] = '提交';
$string['cancel'] = '取消';

$string['nocontents'] = '无法找到结果';
$string['keywordchk'] = '请检查所有单词拼写是否正确或者<br>尝试其它关键词。';
$string['numofthing'] = '个';
$string['attachedfile'] = '附件';

//김성룡 작업 9/20

//write.php
$string['overlap'] = '您发布的帖子已存在。';

//personalinfo.php
$string['privacypolicy'] = '隐私政策';

//write_mform.php
$string['titlenumberover'] = '标题不能超过40字。.';

//mobile_feedback.php
$string['sendfeedback'] = '发送反馈';

//view.php
$string['delete_back'] = '该帖子已被删除，自动返回列表';

/* 2019-10-04 조성원 추가했습니다. */

$string['privacyprocess'] = '隐私政策';
$string['close'] = '关闭';
$string['lgihw'] = 'LG仁和苑';