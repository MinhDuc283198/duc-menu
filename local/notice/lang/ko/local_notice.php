<?php

$string['pluginname'] = 'Notice';
$string['pluginnameplural'] = 'Notice';

// index 
$string['pickupnotice_help'] ='해당 공지가 입력하신 날짜까지 가장 최상단에 노출되게 됩니다.';
$string['pickupnotice'] ='공지 게시 기간이란';
$string['home'] ='홈';
$string['notice'] ='공지사항';
$string['entersearch'] ='검색어를 입력하세요';
$string['number'] ='번호';
$string['title'] ='제목';
$string['createddate'] ='등록';
$string['view'] ='조회';
$string['attach'] ='첨부';
$string['download'] ='다운로드';
$string['none'] ='없음';
$string['enternotice'] ='등록';


$string['select'] ='선택';
$string['showfive'] ='5줄보기';
$string['showten'] ='10줄보기';
$string['showfifteen'] ='15줄보기';
$string['showtwenty'] ='20줄보기';
$string['showthirty'] ='30줄보기';

$string['write'] ='공지 작성';
$string['faqwrite'] ='FAQ 작성';
$string['content'] ='내용';

// view
$string['getit'] ='찜하기';
$string['shear'] ='공유하기';
$string['writer'] ='작성자';

$string['previous'] ='이전글';
$string['next'] ='다음글';
$string['edit'] ='수정';
$string['delete'] ='삭제';
$string['list'] ='목록';

// mfrom
$string['postupdate'] ='공지게시기간';

//write php
$string['writed'] = '게시글이 등록되었습니다.';
$string['writeandedit'] ='공지사항 작성&수정';
$string['noticeperiod'] ='공지 게시 기간';


//feq.mustache
$string['faq'] ='도움말';
$string['search'] ='검색';
$string['searchresult'] ='에 대한 검색 결과';
$string['faqlist'] ='자주 찾는 질문';
$string['apply'] ='어플리케이션';

//view.php
$string['viewpage'] ='글 상세보기';

//delete
$string['deleted'] ='게시글이 삭제되었습니다.';

//feedback.mustache
$string['status'] = '상태';
$string['checkbox'] = '체크박스';
$string['unconfirmed'] = '미확인';
$string['completion'] = '완료';
$string['hold'] = '보류';
$string['checking'] = '확인중';
$string['ignoring'] = '무시';
$string['feedbacklist'] = '피드백 목록';
$string['progressstatus'] = '진행상황';


// feedback.js + index.js
$string['suercancel'] = '삭제 하시겠습니까?';
$string['sendfeedback'] = '의견 보내기';
$string['loginplz'] = '로그인이 필요한 기능입니다.';
$string['threefile'] = '파일은 3개까지 첨부 가능합니다.';
$string['isnotfile'] = '지원하지 않는 파일 형식입니다.';
$string['entercontent'] = '내용을 입력해주세요.';
$string['entertitle'] = '제목을 입력해주세요.';
$string['feedbackwriten'] = '피드백이 작성되었습니다.';

//feedbackwrite.mustache
$string['fileupload'] = '파일 추가';
$string['submission'] = '제출';
$string['cancel'] = '취소';

$string['nocontents'] = '검색 결과에 해당하는 게시물이 없습니다';
$string['keywordchk'] = '모든 단어의 철자가 올바른지 확인하거나<br>다른 키워드를 사용해 보십시오.';
$string['numofthing'] = '개';
$string['attachedfile'] = '첨부파일';

//김성룡 작업 9/20

//write.php
$string['overlap'] = '중복된 게시글 입니다.';

//personalinfo.php
$string['privacypolicy'] = '개인정보 처리방침';

//write_mform.php
$string['titlenumberover'] = '제목은 최대 40글자를 넘길 수 없습니다.';

//mobile_feedback.php
$string['sendfeedback'] = '피드백 보내기';

//view.php
$string['delete_back'] = '이미 삭제된 게시글입니다. 자동으로 목록에 돌아갑니다.';

/* 2019-10-04 조성원 추가했습니다. */

$string['privacyprocess'] = '개인정보 처리방침';
$string['close'] = '닫기';
$string['lgihw'] = 'LG 인화원';