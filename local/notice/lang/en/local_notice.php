<?php

$string['pluginname'] = 'Notice';
$string['pluginnameplural'] = 'Notice';

// index 
$string['pickupnotice_help'] ='EN The notice will be displayed at the top of the page by the date you entered.';
$string['pickupnotice'] ='EN Notice period';
$string['home'] ='HOME';
$string['notice'] ='NOTICE';
$string['entersearch'] ='Enter a search term';
$string['number'] ='Number';
$string['title'] ='Title';
$string['createddate'] ='등록';
$string['view'] ='View';
$string['attach'] ='Attach';
$string['download'] ='Download';
$string['none'] ='None';
$string['enternotice'] ='등록';

$string['select'] ='Select';
$string['showfive'] ='Show five lines';
$string['showten'] ='Show ten lines';
$string['showfifteen'] ='Show fifteen lines';
$string['showtwenty'] ='Show twenty lines';
$string['showthirty'] ='Show thirty lines';

$string['write'] ='Notice';
$string['faqwrite'] ='FAQ';
$string['content'] ='Content';

// view
$string['getit'] ='Collect';
$string['shear'] ='Shear';
$string['writer'] ='Writer';

$string['previous'] ='Previous';
$string['next'] ='Next';
$string['edit'] ='Edit';
$string['delete'] ='Delete';
$string['list'] ='List';

// mfrom
$string['postupdate'] ='Notice period';

//write php
$string['writed'] = 'Posted.';
$string['writeandedit'] ='Notice wirte & edit';
$string['noticeperiod'] ='Notice period';



//feq.mustache
$string['faq'] ='Help';
$string['search'] ='Search';
$string['searchresult'] ='Search result of';
$string['faqlist'] ='FAQ';
$string['apply'] ='어플리케이션 APP';

//delete
$string['deleted'] ='The post has been deleted.';

//view.php
$string['viewpage'] ='View details';

//feedback.mustache
$string['status'] = 'Status';
$string['checkbox'] = 'Check box';
$string['unconfirmed'] = 'Unconfirmed';
$string['completion'] = '완료';
$string['hold'] = 'Hold';
$string['checking'] = 'Checking';
$string['ignoring'] = 'Ignoring';
$string['feedbacklist'] = 'Feedback list';
$string['progressstatus'] = 'Progress status';

// feedback.js + index.js
$string['suercancel'] = 'Are you sure you want to delete it?';
$string['sendfeedback'] = 'Send eedback';
$string['loginplz'] = 'Please login first.';
$string['threefile'] = 'Up to three attachments.';
$string['isnotfile'] = 'File format is not supported.';
$string['entercontent'] = 'Enter content.';
$string['entertitle'] = 'Enter title.';
$string['feedbackwriten'] = 'You have written the feedback.';

//feedbackwrite.mustache
$string['fileupload'] = 'Add file';
$string['submission'] = 'Submission';
$string['cancel'] = 'Cancel';
$string['nocontents'] = 'EN Unable to find the result';
$string['keywordchk'] = 'EN Please check if all words are spelt correctly or<br> try other keywords.';
$string['numofthing'] = '';
$string['attachedfile'] = 'Attache dfile';

//김성룡 작업 9/20

//write.php
$string['overlap'] = 'The post already exists.';

//personalinfo.php
$string['privacypolicy'] = 'Privacy Policy';

//write_mform.php
$string['titlenumberover'] = 'Title cannot exceed 40 characters.';

//mobile_feedback.php
$string['sendfeedback'] = 'Send feedback';

//view.php
$string['delete_back'] = 'The post has already been deleted. Return to list automatically.';

/* 2019-10-04 조성원 추가했습니다. */

$string['privacyprocess'] = 'Privacy Policy';
$string['close'] = 'Close';
$string['lgihw'] = 'LG인화원';