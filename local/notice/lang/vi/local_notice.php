<?php

$string['pluginname'] = 'Thông báo';
$string['pluginnameplural'] = 'Thông báo';

// index 
$string['pickupnotice_help'] ='Thông báo sẽ hiển thị đầu trang dựa vào ngày bạn nhập';
$string['pickupnotice'] ='Ngày thông báo';
$string['home'] ='Trang chủ';
$string['notice'] ='Thông báo';
$string['entersearch'] ='Nhập từ khóa';
$string['number'] ='Số';
$string['title'] ='Tiêu đề';
$string['createddate'] ='Ngày tạo';
$string['view'] ='Xem';
$string['attach'] ='Đính kèm';
$string['download'] ='Tải xuống';
$string['none'] ='Trống';
$string['enternotice'] ='Đăng ký';

$string['select'] ='Chọn';
$string['showfive'] ='Hiển thị 5 dòng';
$string['showten'] ='Hiển thị 10 dòng';
$string['showfifteen'] ='Hieẻn thị 15 dòng';
$string['showtwenty'] ='Hiển thị 20 dòng';
$string['showthirty'] ='Hiển thị 30 dòng';

$string['write'] ='Tạo thông báo';
$string['faqwrite'] ='Tạo FAQ';
$string['content'] ='Nội dung';

// view
$string['getit'] ='Sưu tập';
$string['shear'] ='Chia sẻ';
$string['writer'] ='Tác giả';

$string['previous'] ='Lùi';
$string['next'] ='Tới';
$string['edit'] ='Sửa';
$string['delete'] ='Xóa';
$string['list'] ='Danh sách';

// mfrom
$string['postupdate'] ='Ngày thông báo';

//write php
$string['writed'] = 'Bài viết đã đăng ký';
$string['writeandedit'] ='Thông báo bài viết và sửa đổi';
$string['noticeperiod'] ='Ngày thông báo';



//feq.mustache
$string['faq'] ='Trợ giúp';
$string['search'] ='Tìm kiếm';
$string['searchresult'] ='Tìm kết quả';
$string['faqlist'] ='FAQ';
$string['apply'] ='Ứng dụng';

//delete
$string['deleted'] ='Bài viết đã xóa';

//view.php
$string['viewpage'] ='Thông tin lượt xem';

//feedback.mustache
$string['status'] = 'Trạng thái';
$string['checkbox'] = 'Chọn';
$string['unconfirmed'] = 'Chưa xác nhận';
$string['completion'] = 'Hoàn thành';
$string['hold'] = 'Giữ';
$string['checking'] = 'Đang kiểm tra';
$string['ignoring'] = 'Bỏ qua';
$string['feedbacklist'] = 'Danh sách phản hồi';
$string['progressstatus'] = 'Trạng thái tiến trình';

// feedback.js + index.js
$string['suercancel'] = 'Bạn chắc chắn muốn xóa?';
$string['sendfeedback'] = 'Gửi phản hồi';
$string['loginplz'] = 'Vui lòng đăng nhập';
$string['threefile'] = 'Được tải 3 tệt đính kèm';
$string['isnotfile'] = 'Định dạng tệp không hỗ trợ';
$string['entercontent'] = 'Nhập nội dung.';
$string['entertitle'] = 'Nhập tiêu đề.';
$string['feedbackwriten'] = 'Phản hồi đã được tạo';

//feedbackwrite.mustache
$string['fileupload'] = 'Thêm tệp';
$string['submission'] = 'Gửi';
$string['cancel'] = 'Hủy';
$string['nocontents'] = 'Không tìm thấy kết quả';
$string['keywordchk'] = 'Vui lòng kiểm tra chính tả tất cả các từ hoặc <br> dùng từ khóa khác';
$string['numofthing'] = '';
$string['attachedfile'] = 'Đính kèm file';

//김성룡 작업 9/20

//write.php
$string['overlap'] = 'Bài viết đã tồn tại';

//personalinfo.php
$string['privacypolicy'] = 'Chính sách bảo mật';

//write_mform.php
$string['titlenumberover'] = 'Tiêu đề không quá 40 kí tự';

//mobile_feedback.php
$string['sendfeedback'] = 'Gửi phản hồi';

//view.php
$string['delete_back'] = 'Bài viết đã xóa, tự động chuyển về danh sách';

/* 2019-10-04 조성원 추가했습니다. */

$string['privacyprocess'] = 'Chính sách bảo mật';
$string['close'] = 'Đóng';
$string['lgihw'] = 'Album ảnh';