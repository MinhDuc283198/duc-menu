<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
$PAGE->set_context($context);       
$PAGE->requires->jquery();
//$PAGE->set_url('/local/notice/personalinfo.php');
$PAGE->set_title(get_string('privacypolicy','local_personalinfo.php')); // 김성룡 9/20
$PAGE->set_pagelayout("popuplayout");

$prelogin = optional_param('prelogin', 0, PARAM_INT);
$sql = "select * from {notice_board} where category=3 and delflag=''";
$boardlist = $DB->get_records_sql($sql);

/* 로그인페이지에서 접근시 */
if($prelogin) {
    $layout = [
        'prelogin' => 1,
        'boardlist' => 7,
    ];
}

echo $OUTPUT->header();
$PAGE->requires->js_call_amd('local_notice/personalinfo', 'init');

echo $OUTPUT->render_from_template('local_notice/personalinfo_index',$layout); 
echo $OUTPUT->footer();