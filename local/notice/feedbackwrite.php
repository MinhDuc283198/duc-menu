<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
if(!isloggedin()){
    
   redirect($CFG->wwwroot.'/local/notice/faqlist.php');
}

$context = context_system::instance();
$url = new moodle_url($CFG->wwwroot.'/local/notice/faqlist.php');
$title = optional_param('title', '', PARAM_TEXT);
$content = optional_param('content', '', PARAM_TEXT);
$id = optional_param('id', 0, PARAM_INT);
//$formdata = optional_param('file,','dd', PARAM_FILE);
//첫번째인자는 inputtag에서 받아올 name을 의미함 
$fname = $_FILES['feedback_file']['name'];


$contents = new stdClass();
$contents->userid = $USER->id;
$contents->title = $title;
$contents->content = $content;
$contents->timecreated = time();
$contents->category = 2;
$contents->fbstatus = 5;

$fileid = $DB->insert_record('notice_board',$contents);
//파일이 있을때
if(!empty($fname)){
$fcount = count($fname);
$fs = get_file_storage();

$component = 'local_notice';

    for($i=0;$i<$fcount;$i++){
        if (!empty($_FILES["feedback_file"]['tmp_name'][$i])) {
            $file_record = array(
                'contextid' => $context->id,
                'component' => $component,
                'filearea' => "feedback_file",
                'itemid' => $fileid,
                'filepath' => '/',
                'filename' => $_FILES["feedback_file"]['name'][$i],
                'timecreated' => $thistime,
                'timemodified' => $thistime,
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
                );
            $fs->create_file_from_pathname($file_record, $_FILES["feedback_file"]['tmp_name'][$i]);
        } 
    }
}

echo $fileid;