<?php 

require(dirname(dirname(dirname(__FILE__))) . '/config.php'); 
require_once $CFG->dirroot . '/local/notice/lib.php';

require_login();
$context = context_system::instance();

$url = new moodle_url($CFG->wwwroot.'/local/notice/index.php');
$PAGE->set_context($context);
$PAGE->set_url($url);
$PAGE->set_title(get_string('notice','local_notice'));

$PAGE->set_pagelayout('member'); 
//$PAGE->navbar->ignore_active(true);
//$PAGE->navbar->add('홈',$CFG->wwwroot);
//$PAGE->navbar->add('공지사항',$url);

//  --------------------페이징---------------------------
$pageNum = optional_param('page', 1 , PARAM_INT);      // 아래에 표시되는 페이지 넘버
$list = optional_param('list', 5, PARAM_INT);  //page : default - 5 화면에 표시되는 글의 갯수
$limit = ($pageNum - 1) * $list; //시작점
$search_ = optional_param('search', '', PARAM_RAW);

// 검색어가 있을경우
if (!empty($search_)) {
    //검색어 공백제거
    $searchw = str_replace(' ', '', $search_);
    $search = '%'.$searchw.'%';
    $query ="select * from {notice_board} where delflag=0 and category=0 and title like :search or delflag=0 and category=0 and content like :search2 order by id desc ";
    $boardcontents = $DB->get_records_sql($query, array ("search"=>$search,"search2"=>$search), $limit,$list);
}
else {
    $query =" select * from {notice_board} where delflag=0 and category=0 order by id desc";
    $boardcontents = $DB->get_records_sql($query, array (), $limit,$list);
}
$pickupquery = "select * from {notice_board} where delflag= 0 and category =0 and assesstimefinish > (SELECT DATEDIFF(S, '01/01/1970 09:00:00', GETDATE()))";
$pickupcontents =$DB->get_records_sql($pickupquery, array ());

$boardcontent = array();

//-----총 게시글의 페이지수↓-----
$page_query = 'select count(*) as pagecount from {notice_board} where delflag=0 and category=0';

// 검색어 있을때
if (!empty($search)) {
    $page_query =" select count(*) as pagecount from {notice_board} where delflag=0 and category=0 and title like  :search or delflag=0 and category=0 and content like :search2";
    $total_count = $DB->get_record_sql($page_query, array ("search"=>$search,"search2"=>$search));
} else {
    $total_count = $DB->get_record_sql($page_query);
}

$total_page = ceil( $total_count->pagecount / $list );

// 게시글 번호 매기기

//넘버링 : $맨처음번호 = $전체레코드수 - $페이지당리스트출력수 * ($페이지번호 -1) 
$page_number = $total_count->pagecount - $list *($pageNum-1);

$cut_title = 35;
//불러올 파일 셋팅
foreach ($boardcontents as $boardcontent) {
    if($boardcontent->timeedited!=0){
    //timeedited 컬럼이 0이 아닌경우 -> 수정된 글인경우
        $time1 = $boardcontent->timeedited;
        $boardcontent->timeedited = gmdate("Y-m-d",$time1);
    } else {
        $time1 = $boardcontent->timecreated;
        $boardcontent->timecreated = gmdate("Y-m-d",$time1);
    }
    if(mb_strlen($boardcontent->title,'utf-8') > $cut_title){
        $boardcontent->title = mb_substr($boardcontent->title, '0', '29','utf-8') . "...";
    }
     //파일--------------------
//     $path = file_get($context,$boardcontent->id);
//     if($path){
//         $boardcontent->filedownload = $path;
//     } else {
//         $boardcontent->filedownload = null;
//     }
     $fs = get_file_storage();
     $fp = get_file_packer();
     $files = $fs->get_area_files($context->id, 'local_notice', 'attachment', $boardcontent->id, 'timemodified', false);
     if($files){
            $filecount = count($files);
            if(1<$filecount){
            $fileary = array();
            foreach ($files as $file) {
                if($file->get_filename()!="notice.zip"){
                    $path = $file->get_filepath() . $file->get_filename();
                    $path = ltrim($path, '/');
                    $fileary[$path] = $file;
                } else {
                    $file->delete();
                }
            }
            $filename ='notice.zip';
                $filezip=$fp->archive_to_storage($fileary, $context->id, 'local_notice', 'attachment',  $boardcontent->id, '/', $filename, $USER->id,true, null);
               $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $boardcontent->id . '/' . $filename);
               $boardcontent->filedownload = $path;
               
            } else {
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();
                    $icon_img= '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)). '" class="icon" alt="' . $mimetype . '" />';
                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $boardcontent->id . '/' . $filename);
                    $boardcontent->filedownload = $path;
                }
            }      
        } else {
                $boardcontent->filedownload = null;
        }
     //파일끝------------------
     $boardcontent->cnt=$page_number;
     $page_number--;
     $data[] = $boardcontent;
}
//상단에 표시되는 공지사항
foreach ($pickupcontents as $pickupcontent) {
    if($pickupcontent->timeedited!=0){
        $time1 = $pickupcontent->timeedited;
        $pickupcontent->timeedited = gmdate("Y-m-d",$time1);
    } else {
        $time1 = $pickupcontent->timecreated;
        $pickupcontent->timecreated = gmdate("Y-m-d",$time1);
    }
    if(strlen($pickupcontent->title) > $cut_title){
            $pickupcontent->title = mb_substr($pickupcontent->title, '0', -5,'utf-8') . "...";
        }
     //파일--------------------
     $fs = get_file_storage();
     $fp = get_file_packer();
     $files = $fs->get_area_files($context->id, 'local_notice', 'attachment', $pickupcontent->id, 'timemodified', false);
     if($files){
            $filecount = count($files);
            if(1<$filecount){
            $fileary = array();
            foreach ($files as $file) {
                if($file->get_filename()!="notice.zip"){
                    $path = $file->get_filepath() . $file->get_filename();
                    $path = ltrim($path, '/');
                    $fileary[$path] = $file;
                } else {
                    $file->delete();
                }
            }
            $filename ='notice.zip';
                $filezip=$fp->archive_to_storage($fileary, $context->id, 'local_notice', 'attachment',  $pickupcontent->id, '/', $filename, $USER->id,true, null);
               $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $pickupcontent->id . '/' . $filename);
               $pickupcontent->filedownload = $path;
               
            } else {
                foreach ($files as $file) {
                    $filename = $file->get_filename();
                    $mimetype = $file->get_mimetype();
                    $icon_img= '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)). '" class="icon" alt="' . $mimetype . '" />';
                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_notice/attachment/' . $pickupcontent->id . '/' . $filename);
                    $pickupcontent->filedownload = $path;
                }
            }
        } else {
                $pickupcontent->filedownload = null;
        }
     $pickupcontent->cnt=$page_number;
     $page_number--;
     $pickupdata[] = $pickupcontent;
}


if(!empty($search)){
    if($total_count->pagecount==0){
        $searchcnt->searchcount =" 0";
    } else {
        $searchcnt->searchcount= $total_count->pagecount;
    }
    $searchcnt->searchword = $searchw;
    }

$b_pageNum_list = 9;
//블럭에 나타낼 페이지 번호 갯수
//-----블럭의 조건↓-----
$block = ceil($pageNum/$b_pageNum_list);

//현재 리스트의 블럭을 구하는 식 입니다. 
$b_start_page = ( ($block - 1) * $b_pageNum_list ) + 1; 
//현재 블럭에서 시작페이지 번호
$b_end_page = $b_start_page + $b_pageNum_list - 1; 
//현재 블럭에서 마지막 페이지 번호





if ($b_end_page > $total_page) { // 총 페이지수와 마지막 페이지를 같을수있게
    // 9 > 3
    $b_end_page = $total_page;
};

//페이징 셋팅 끝
$page_option = page_list($list);

if($pageNum <=1){
    $pageset_first = "<a class='first'></a>";
} else {
    $pageset_first = "<a class='first' href='/local/notice/index.php?page=1&list=$list'></a>";
};

if($block <=1){ //10개씩 뒤로가기
    $pageset_end = ''; // block이 1보다 작거나 같으면 뒤로 갈수 없기때문에 공백으로 둠
} else {
    $pageset_b_start_page = $b_start_page-1;
    $pageset_end = "<a class='prev' href='/local/notice/index.php?page=$pageset_b_start_page&list=$list'></a>";
};



//$b_start_page를 $j의 초기값으로 설정 ; $b_end_page보다 작거나 같을때 까지 for문 돌리기 ; $j++하나씩 증가시키기
 // j(페이지시작번호)= 1 ; 1 <= 9(페이지끝나는번호) ; 1++
$pageset_j = array();
 for($j = $b_start_page; $j <=$b_end_page; $j++){
     $pageset_j[$j-1] = array();
	if($pageNum == $j){ //pageNum 와 j 가 같으면 현재 페이지 이므로
            //링크없이 현재페이지만 출력
            $pageset_j[$j-1]['pageset'] = '<a class=on>'.$j.'</a>';
        } else {
            if(!empty($search)){
                $pageset_j[$j-1]['pageset'] ="<font><a href='/local/notice/index.php?page=$j&list=$list&limit&search=$search'>$j</a></font>";
            } else {
            $pageset_j[$j-1]['pageset'] ="<font><a href='/local/notice/index.php?page=$j&list=$list&limit'>$j</a></font>";
                 }
        }
};

// 3/9 = 1
$total_block = ceil($total_page/$b_pageNum_list);
//10개씩 뒤로가기
if($block >= $total_block){ 
    $pageset_next_end = ''; 
} else {
    $pageset_b_end_page = $b_start_page+1;
    $pageset_next_end = "<a class='next' href='/local/notice/index.php?page=$pageset_b_end_page&list=$list'></a>";

};

//현재 페이지 번호가. 토탈페이지 갯수보다 적을때는 맨뒤로 넘어갈수있게  마지막으로 넘어가기/ 
if($pageNum <= $total_page){
    $pageset_next = "<a class='last' href='/local/notice/index.php?page=$total_page&list=$list'></a>";
} else {
    $pageset_next = "<a class='last'></a>";
};

$PAGE->requires->js_call_amd('local_notice/index','init', array());

$baseurl = "local/notice/index.php";
$pagevar = "page";

//  --------------------페이징끝-------------------------

//관리자 권한
if(is_siteadmin()){
    $siteadmin="admin";
}



echo $OUTPUT -> header();

//(총 항목수, 현재 페이지, 페이지당 표시할 항목수 , url )
//echo $OUTPUT->paging_bar($total_count, $pageNum, $b_pageNum_list, $baseurl,$pagevar);

$templatecontext = [
    'boardcontents' => $data, //게시글 데이터
    'pageset_first' => $pageset_first, //처음으로 가는 버튼
    'pageset_end' => $pageset_end, //10개씩 앞으로 가는 버튼
    'pageset_j' => $pageset_j, //개별페이지버튼
    'pageset_next_end'=> $pageset_next_end, //10개씩 뒤로가는 버튼
    'pageset_next' => $pageset_next, //마지막으로 가는 버튼
    'siteadmin' => $siteadmin, //관리자 권한확인
    'pickupcontents' =>$pickupdata,
    'searchcnt' =>$searchcnt,
    'option' => $page_option
];



echo $OUTPUT->render_from_template('local_notice/index', $templatecontext); 
echo $OUTPUT -> footer();