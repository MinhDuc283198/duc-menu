<?php

require_once("$CFG->libdir/formslib.php");

class write_mform extends moodleform {
    
      public static function attachment_options() {
        global $PAGE, $CFG;
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'subdirs' => 0,
            'maxbytes' => $maxbytes,
            'maxfiles' => 5,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL
        );
    }
      public static function editor_options($context, $contentid) {
        global $PAGE, $CFG;
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES, //최대 파일 갯수
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'local_notice', 'contents', $contentid)
        );
    }
    public function definition() {
        global $CFG;
        $category= optional_param('category', 0, PARAM_INT); 
        $id = optional_param('id', 0, PARAM_INT);
        $mform = $this->_form;

        $mform->addElement('hidden','id');
        $mform->setType('id',PARAM_INT);
        $mform->addElement('hidden','userid');
        $mform->setType('userid',PARAM_INT);  
        $mform->addElement('hidden','category');
        $mform->setType('category',PARAM_INT);  
        $mform->setDefault('category',$category);
        $mform->addElement('header', 'general','');
        $mform->addElement('text', 'title', get_string('title','local_notice'));
        $mform->setType('title', PARAM_TEXT);
        $mform->setDefault('title', null);
        $mform->addRule('title', get_string ('entertitle','local_notice'), 'required');
        $mform->addRule('title', get_string('titlenumberover','local_write_mform'), 'maxlength',40); // 김성룡 9/20
        //공지게시기간 지정
        if($category==0){
            $mform->addElement('html','<div class="fdate_time_selector">');
            $mform->addElement('date_selector','assesstimefinish',get_string('noticeperiod','local_notice'),array('startyear' => 2019, 'timezone' => 99, 'optional' => false));
            $mform->addHelpButton('assesstimefinish', 'pickupnotice','local_notice');
            $mform->addElement('html','</div>');
        }
        
        
        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
        $mform -> addElement( 'editor' ,  'content' , get_string ('content','local_notice'), null, $editoroptions); 
//        $mform->addHelpButton('content', 'coursesummary');
        $mform->setType('content', PARAM_RAW);
        $mform->addRule('content', get_string ('entercontent','local_notice'), 'required');
        $mform->addGroup(array(
                $mform->createElement('filemanager', 'attachments', 'attachment', null, self::attachment_options()),
                $mform->createElement('static', 'text', '', '')
                    ), 'filemanager', '', array(''), false);   
        //submit버튼 이름이 상황에따라 다르게 나오게 하기
        if(!empty($id)){
            //수정일때
            $this->add_action_buttons($cancel=true,$submitlabel=get_string('edit','local_notice'));
        } else {
            //새글일때
            $this->add_action_buttons($cancel=true,$submitlabel=get_string('enternotice','local_notice'));
        }
    }
    
    function validation($data, $files) {
        return array();
    }
}
