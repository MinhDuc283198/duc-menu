<?php

require('../../config.php');
if (isset($_POST['emainsend'])) {
  #1
  try {
  function send_feedback_email($user) {
    global $CFG, $DB;
    $email_feedback=$_POST['emainsend'];
    $message1 = $_POST['message'];
    $subject1 = $_POST['subject'];
    $phone = $_POST['phone'];
    $site = get_site();
    $supportuser = core_user::get_support_user();
  
    $data = new stdClass();
    $data->firstname = fullname($user);
    $data->sitename  = format_string($site->fullname);
    $data->admin     = generate_email_signoff();
  
    $subject = 'Feedback cua '.$subject1;
    $message='';
    $messagehtml= 'Feedback: '.$message1.'<br>'.'Phone: '.$phone.'<br>'.'Email: '.$email_feedback;  
    $user->mailformat = 1;   //Always send HTML version as well.
    $user->email = $DB->get_field('lmsdata_user', 'email2', array('userid' => $user->id));
    
     //Directly email rather than using the messaging system to ensure its not routed to a popup or jabber.
    return email_to_user($user, $supportuser, $subject,$message, $messagehtml);
  }
    $user1 = $DB->get_record_sql('SELECT value FROM {vi_settings} WHERE name="emailfeedback";');
    $user=$DB->get_record('user', array('email' => $user1->value));
    send_feedback_email($user);
    echo 'success';
}

  #2
   catch (Exception $e) {
      echo 'Message could not be sent. Mailer Error ';
  }
  }

