<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( $hassiteconfig ){
 
    $settings = new admin_settingpage( 'privacy_log', '개인정보 조회 로그' );

    $ADMIN->add( 'localplugins', $settings );

    $settings->add(
        new admin_setting_configtext(
            'search_log_url',
            '조회 URL',
            '',
            '/chamktu/users/info.php',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'create_log_url',
            '등록 URL',
            '',
            '',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'update_log_url',
            '수정 URL',
            '',
            '',
            PARAM_TEXT
    ) );
    
     $settings->add(
        new admin_setting_configtext(
            'delete_log_url',
            '삭제 URL',
            '',
            '',
            PARAM_TEXT
    ) );
    
}


