<?php

function xmldb_local_lmsdata_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    require_once($CFG->libdir . '/eventslib.php');

    $dbman = $DB->get_manager();

    if ($oldversion < 2017021400) {
        $table = new xmldb_table('lmsdata_user');
        $field = new xmldb_field('usergroup_cd', XMLDB_TYPE_CHAR, '10', null, null, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('univ_cd', XMLDB_TYPE_CHAR, '10', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('major_cd', XMLDB_TYPE_CHAR, '10', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017021401) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('ohakkwa');
        $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, true, null, null);
        try {
            $dbman->change_field_type($table, $field);
        } catch (moodle_exception $e) {
            
        }

        $field = new xmldb_field('domain');
        $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, true, null, null);
        try {
            $dbman->change_field_type($table, $field);
        } catch (moodle_exception $e) {
            
        }
    }

    if ($oldversion < 2017032300) {

        $table = new xmldb_table('ust_sms');
        if ($dbman->table_exists($table)) {
            $dbman->rename_table($table, 'lmsdata_sms');
        }

        $table = new xmldb_table('ust_sms_data');
        if ($dbman->table_exists($table)) {
            $dbman->rename_table($table, 'lmsdata_sms_data');
        }
    }

    if ($oldversion < 2017033100) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('bunban', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017033101) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('tag', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017033102) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('bunban');
        $field->set_attributes(XMLDB_TYPE_CHAR, '10', null, true, null, '00');
        try {
            $dbman->change_field_type($table, $field);
        } catch (moodle_exception $e) {
            
        }
    }
    if ($oldversion < 2017041800) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('siteadmin_loginfo');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('username', XMLDB_TYPE_CHAR, '100', null, null, null, '');
        $table->add_field('ip', XMLDB_TYPE_CHAR, '30', null, null, null, '');
        $table->add_field('mobile', XMLDB_TYPE_CHAR, '100', null, null, null, '');
        $table->add_field('action', XMLDB_TYPE_CHAR, '100', null, null, null, '');
        $table->add_field('log_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2017042400) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('hakjum', XMLDB_TYPE_CHAR, '4', null, null, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017051200) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('ohakkwa_cd', XMLDB_TYPE_CHAR, '30', null, null, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017053000) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('univ_type', XMLDB_TYPE_CHAR, '2', null, null, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017053001) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('excel_user_period');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('adminid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('startdate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('enddate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2017060800) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_rebooks');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('author', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('publisher', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('publication_date', XMLDB_TYPE_CHAR, '10', null, null, null, 0);
        $table->add_field('totalpage', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('link1', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('link2', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('link3', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('link4', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('isbn', XMLDB_TYPE_CHAR, '200', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2017063000) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_course');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('coursearea', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('coursetype', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('coursename', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('coursegrade', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('courserating', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('mainexposure', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('exposuresort', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('sampleurl', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('memberdiscount', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('groupdiscount', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('textbook', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('week', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('day', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('hours', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('learningtime', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('intro', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('objectives', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('subjects', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('contents', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

// Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_class');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('classtype', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('classyear', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('classnum', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('offlineyn', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('classlimit', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('certificateimg', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

// Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_schedule');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('classyear', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('classnum', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('classtype', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('postingstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('postingend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('enrolmentstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('enrolmentend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('learningstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('learningend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('gradeviewstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('gradeviewend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('evaluationstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('evaluationend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('appealstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('appealend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('consignmenttitle', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('consignmenttext', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('deductible', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('logo1', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('logo2', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

// Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_template');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('autoyn', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('sendtime', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('text', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

// Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_contents');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('category', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('openyn', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('url', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2017070300) {
// Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_course_teacher');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('best', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('lmsuserid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
// Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_course_book');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('bookid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2017070500) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('occasional', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017070501) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('certificatepre', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017070502) {
        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('postingstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('postingend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('enrolmentstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('enrolmentend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('learningstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('learningend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('gradeviewstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('gradeviewend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('evaluationstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('evaluationend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('appealstart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('appealend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017071200) {
        $table = new xmldb_table('lmsdata_course_teacher');
        $field = new xmldb_field('teacherhistory', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017071201) {
        $table = new xmldb_table('lmsdata_user');

        $field = new xmldb_field('usergroup_cd');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('b_temp');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('b_mobile');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('b_email');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('univ_cd');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('univ');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('major_cd');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('major');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('hyear');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('b_tel');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('b_univ');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('b_major');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('ehks');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('edhs');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('domain');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('hyhg');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('persg');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('psosok');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('sex');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }

    if ($oldversion < 2017071202) {
        $table = new xmldb_table('lmsdata_user');

        $field = new xmldb_field('userno', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('neiscode', XMLDB_TYPE_CHAR, '50', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('nickname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('usertypecode', XMLDB_TYPE_INTEGER, '10', null, null, null, 10);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('sex', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('birthday', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('phone1', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('phone2', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('allowsms', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('allowemail', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('allowprofile', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('school', XMLDB_TYPE_CHAR, '30', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('statuscode', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('joinstatus', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017071300) {
        $table = new xmldb_table('lmsdata_schedule');
        $field = new xmldb_field('offlineyn', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017071400) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('scheduleid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017071401) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('intro', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        } else {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('objectives', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        } else {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('subjects', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        } else {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('contents', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        } else {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017071402) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('parentcourseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017080100) {

        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_sendtemplate');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('component', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('sendtime', XMLDB_TYPE_INTEGER, '2', null, null, null, '0');
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, '1');
        $table->add_field('contents', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table lmsdata_sendtemplate.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for lmsdata_sendtemplate.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table lmsdata_sendvariable to be created.
        $table = new xmldb_table('lmsdata_sendvariable');

        // Adding fields to table lmsdata_sendvariable.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('tbname', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('fieldname', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('fieldtype', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('varname', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('description', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, '1');

        // Adding keys to table lmsdata_sendvariable.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for lmsdata_sendvariable.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Lmsdata savepoint reached.
        upgrade_plugin_savepoint(true, 2017080100, 'local', 'lmsdata');
    }

    if ($oldversion < 2017080101) {
        $table = new xmldb_table('lmsdata_sendtemplate');
        $field = new xmldb_field('sendtarget', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017080102) {
        $table = new xmldb_table('lmsdata_sendtemplate');
        $field = new xmldb_field('classtype', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2017080102, 'local', 'lmsdata');
    }

    if ($oldversion < 2017080800) {
        $table = new xmldb_table('lmsdata_user');
        $field = new xmldb_field('zipcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('address', XMLDB_TYPE_CHAR, '100', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('address_detail', XMLDB_TYPE_CHAR, '100', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('subject', XMLDB_TYPE_CHAR, '50', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('allownews', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('allowedudata', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('postareacode', XMLDB_TYPE_CHAR, '5', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('educationdata', XMLDB_TYPE_CHAR, '5', null, null, null, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017081000) {

        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_applications_old');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('appcd', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('coursecd', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('grade', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('school', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('qualification', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('neiscode', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('locationnumber', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('phone', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('discounttype', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('paymentid', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('ordernumber', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('paymentmeans', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('bankcode', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('accountname', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('cshrresult', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('cardcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('paymentdate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('paymentstatus', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('completionstatus', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('refundprice', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('refundbank', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('refundbanknr', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('refunddate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('calcyn', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('studystart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('studyend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('restudystart', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('restudyend', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('progress', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('report', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('meet', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('exam', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('offexam', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('hakjum', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('totjum', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('totjumdetail', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('finishnr', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('studyrate', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table lmsdata_sendtemplate.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for lmsdata_sendtemplate.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2017081100) {

        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('notice', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('useprogress', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('notice', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('useprogress', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017081101) {

        $table = new xmldb_table('lmsdata_class');

        $field = new xmldb_field('background', XMLDB_TYPE_CHAR, '10', null, null, null, '1');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('lmsdata_course');

        $field = new xmldb_field('background', XMLDB_TYPE_CHAR, '10', null, null, null, '1');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017081701) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('coursecd', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017081702) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('templateyn', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017081800) {
        $table = new xmldb_table('lmsdata_grouplist');
        $field = new xmldb_field('status', XMLDB_TYPE_TEXT, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017082200) {
        $table = new xmldb_table('lmsdata_applications_old');
        $field = new xmldb_field('chasunr', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017091200) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('old', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017091300) {
        $table = new xmldb_table('lmsdata_schedule');
        $field = new xmldb_field('application_limit', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017091302) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('old', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if ($dbman->field_exists($table, $field)) {
            $dbman->rename_field($table, $field, 'used');
        }
    }
    if ($oldversion < 2017091400) {
        $table = new xmldb_table('lmsdata_schedule');
        $field = new xmldb_field('bannerurl', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017092200) {
        $table = new xmldb_table('lmsdata_sendtemplate');
        $field = new xmldb_field('senddate', XMLDB_TYPE_INTEGER, '2', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017092500) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2017101401) {

        $table = new xmldb_table('lmsdata_banner');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('filename', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('linkurl', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('target', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2017112000) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('trainingarea', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017113000) {
        $table = new xmldb_table('lmsdata_applications_old');
        $field = new xmldb_field('receipt', XMLDB_TYPE_CHAR, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2017121500) {
        $table = new xmldb_table('lmsdata_schedule');
        $field = new xmldb_field('counthide', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2018062501) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('passnum', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103000) {

        $table = new xmldb_table('lmsdata_pushtoken');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('token', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('username', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2018103001) {
        $table = new xmldb_table('lmsdata_pushtoken');
        $field = new xmldb_field('autologin', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2018103002) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('classtype');
        $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, true, null, null);
        try {
            $dbman->change_field_type($table, $field);
        } catch (moodle_exception $e) {
            
        }
    }

    if ($oldversion < 2019013000) {
        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_category');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('depth', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('step', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('ispopup', XMLDB_TYPE_INTEGER, '2', null, null, null, 1);
        $table->add_field('url', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('icon', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
//        $table->add_field('required', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('sub_parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table lmsdata_sendtemplate.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_category_name');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('lang', XMLDB_TYPE_CHAR, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table lmsdata_sendtemplate.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for lmsdata_sendtemplate.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_category_apply');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('usergroup', XMLDB_TYPE_CHAR, '30', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table lmsdata_sendtemplate.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for lmsdata_sendtemplate.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019020800) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('completestandard', XMLDB_TYPE_TEXT, null, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019021101) {
        $table = new xmldb_table('lmsdata_category');

        $field = new xmldb_field('ispopup');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('url');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('icon');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('required');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('sub_parent');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }

    if ($oldversion < 2019021102) {
        $table = new xmldb_table('lmsdata_category');

        $table->add_field('required', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, null);
        $table->add_field('sub_parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019021103) {
        $table = new xmldb_table('lmsdata_category');

        $table->add_field('ispopup', XMLDB_TYPE_INTEGER, '2', null, null, null, 1);
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019021104) {
        $table = new xmldb_table('lmsdata_category');

        $table->add_field('sub_parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    If ($oldversion < 2019021105) {
        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_category');

        // Adding fields to table lmsdata_sendtemplate.

        $table->add_field('ispopup', XMLDB_TYPE_INTEGER, '2', null, null, null, 1);
        $table->add_field('url', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('icon', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('sub_parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);


        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    If ($oldversion < 2019021106) {
        // Define table lmsdata_sendtemplate to be created.
        $table = new xmldb_table('lmsdata_category');


        $field = new xmldb_field('required', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('ispopup', XMLDB_TYPE_INTEGER, '2', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('url', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('icon', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('sub_parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019021107) {
        $table = new xmldb_table('lmsdata_category');

        $field = new xmldb_field('icon');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('url');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2019021300) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('classobject', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('lecturepoint', XMLDB_TYPE_INTEGER, '10', null, null, null, 40);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('roughprocess', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('detailprocess', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019021301) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('cardinalnum', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('coursenum', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019021401) {
        $table = new xmldb_table('lmsdata_pushtoken');

        $field = new xmldb_field('deviceinform', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }


    if ($oldversion < 2019030501) {
        $table = new xmldb_table('lmsdata_category');

        $field = new xmldb_field('required');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2019040400) {
        $table = new xmldb_table('lmsdata_user');
        $field = new xmldb_field('school');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, null, null, '');
            $dbman->rename_field($table, $field, 'rank');
        }
        $field = new xmldb_field('subject');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, null, null, '');
            $dbman->rename_field($table, $field, 'position');
        }
        $field = new xmldb_field('educationdata');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
            $dbman->rename_field($table, $field, 'entrydate');
        }
    }

    if ($oldversion < 2019040401) {
        $table = new xmldb_table('lmsdata_mainmenu');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);


        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019040402) {
        $table = new xmldb_table('lmsdata_mainmenu');

        $field = new xmldb_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019040403) {
        $table = new xmldb_table('lmsdata_mainmenu');

        $field = new xmldb_field('menus', XMLDB_TYPE_CHAR, '30', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019042900) {
        $table = new xmldb_table('lmsdata_user');

        $field = new xmldb_field('psosok', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019051200) {
        $table = new xmldb_table('lmsdata_course');

        $field = new xmldb_field('yeartype', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019060500) {
        $table = new xmldb_table('lmsdata_sendtemplate');
        $field = new xmldb_field('link', XMLDB_TYPE_CHAR, '255', null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019061300) {
        $table = new xmldb_table('lmsdata_pushtoken');
        $field = new xmldb_field('deviceid', XMLDB_TYPE_CHAR, '255', null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('lastlogin', XMLDB_TYPE_INTEGER, '10', null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019061400) {
        $table = new xmldb_table('lmsdata_mainimg');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('filename', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('target', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019061700) {

        // jei_privacy_log created.
        $table = new xmldb_table('jei_privacy_log');

        // Adding fields to table popup.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('url', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('ipaddr', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // jei_privacy_log keys 
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // jei_privacy_log create table 
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // lmsdata savepoint reached.
        upgrade_plugin_savepoint(true, 2019061700, 'local', 'lmsdata');
    }
    if ($oldversion < 2019061701) {
        $table = new xmldb_table('lmsdata_pushtoken');
        $field = new xmldb_field('deviceid', XMLDB_TYPE_CHAR, '255', null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('lastlogin', XMLDB_TYPE_INTEGER, '10', null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    // 로그인 접속 정보 추가
    if ($oldversion < 2019070400) {
        $table = new xmldb_table('siteadmin_loginfo');

        $field = new xmldb_field('log_device_info', XMLDB_TYPE_TEXT, null, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019071000) {
        $table = new xmldb_table('jei_privacy_log');

        $field = new xmldb_field('url_type', XMLDB_TYPE_INTEGER, 1, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019111300) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_teacher');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('profile', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('comment', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019111301) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_introduce_textbook');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('publisher', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('author', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('series', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('text', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2019111302) {
        $table = new xmldb_table('lmsdata_introduce_textbook');

        $field = new xmldb_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019111400) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_productdelivery');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('bookid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('payid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('addresstype', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('address', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('cell', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('delivery', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2019111401) {
        $table = new xmldb_table('lmsdata_introduce_textbook');

        $field = new xmldb_field('code', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019111402) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_payment');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('ref', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('paytype', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019111800) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_productdelivery_log');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('pdid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('delivery', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019111900) {
        $table = new xmldb_table('lmsdata_introduce_textbook');

        $field = new xmldb_field('delete', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019111901) {
        $table = new xmldb_table('lmsdata_course');

        $field = new xmldb_field('teacher', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('courseperiod', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('reviewperiod', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019112000) {
        $table = new xmldb_table('lmsdata_facebook_group');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('filename', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('linkurl', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('target', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('order', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019112200) {
        $table = new xmldb_table('lmsdata_product_log');

        $field = new xmldb_field('type', XMLDB_TYPE_INTEGER, 1, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019112800) {
        $table = new xmldb_table('lmsdata_manage_search');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2019112801) {
        $table = new xmldb_table('lmsdata_manage_search');

        $field = new xmldb_field('titleen', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('titlebn', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('divide', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019120200) {
        $table = new xmldb_table('lmsdata_banner');

        $field = new xmldb_field('startdate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('enddate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('colorcode', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('category', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('search', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019120201) {
        $table = new xmldb_table('lmsdata_banner');
        $field = new xmldb_field('order', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019120202) {
        $table = new xmldb_table('lmsdata_banner');
        $field = new xmldb_field('order');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('orderby', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019120203) {
        $table = new xmldb_table('lmsdata_manage_search');
        $field = new xmldb_field('titlebn');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('titlevn', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019120300) {
        $table = new xmldb_table('lmsdata_like');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('classid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019120301) {
        $table = new xmldb_table('lmsdata_book_categories');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('idnumber', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('description', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('descriptionformat', XMLDB_TYPE_INTEGER, '2', null, null, null, null);
        $table->add_field('parent', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('coursecount', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('visible', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('visibleold', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('depth', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('path', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('theme', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019120500) {
        $table = new xmldb_table('course_categories_lang');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('ko_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('en_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('vi_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019120501) {
        $table = new xmldb_table('course_categories_lang');
        $field = new xmldb_field('categoryid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019120502) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('en_onelineintro', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('vi_onelineintro', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('en_coursename', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('vi_coursename', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('vi_intro', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('en_intro', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('lmsdata_textbook');
        $field = new xmldb_field('vi_title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('en_title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('vi_author', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('en_author', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('vi_text', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('en_text', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('vi_title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('en_title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('discount', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019120600) {
        $table = new xmldb_table('lmsdata_main_course');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019120601) {
        $table = new xmldb_table('lmsdata_copyright');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('titleen', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('titlevn', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2019120601) {
        $table = new xmldb_table('lmsdata_copyright_set');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('dataid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019120900) {
        $table = new xmldb_table('lmsdata_copyright_set');
        $field = new xmldb_field('copyid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019120901) {
        $table = new xmldb_table('lmsdata_copyright_set');
        $field = $table->add_field('category', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019121000) {
        // Define table lmsdata_course_like to be created.
        $table = new xmldb_table('lmsdata_course_like');

        // Adding fields to table lmsdata_course_like.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('moodlecourseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('contents', XMLDB_TYPE_TEXT, '', null, null, null, null);
        $table->add_field('score', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table lmsdata_course_like.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for lmsdata_course_like.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019121100) {
        $table = new xmldb_table('lmsdata_class_relation');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('dataid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('lmsdata_course_professor');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('teacherid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2019121600) {
        $table = new xmldb_table('lmsdata_banner');
        $field = $table->add_field('site', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019121700) {
        $table = new xmldb_table('lmsdata_course');
        $field = new xmldb_field('year', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('ordernum', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_textbook');
        $field = new xmldb_field('year', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('ordernum', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('year', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('ordernum', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019121701) {
        $table = new xmldb_table('lmsdata_main_job');

        // Adding fields to table lmsdata_sendtemplate.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('jobid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019121702) {
        $table = new xmldb_table('lmsdata_main_course');
        $field = new xmldb_field('colorcode', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019121703) {
        $table = new xmldb_table('lmsdata_main_course');
        $field = new xmldb_field('best', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019121704) {
        $table = new xmldb_table('lmsdata_main_course');
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019122300) {
        $table = new xmldb_table('lmsdata_teacher');
        $field = new xmldb_field('isused', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020010900) {
        $table = new xmldb_table('lmsdata_payment');
        $field = new xmldb_field('forced', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020012001) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('keywords', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020012002) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_badge');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('category', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('use', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('code', XMLDB_TYPE_CHAR, 50, null, XMLDB_NOTNULL, null, null);
        $table->add_field('title_vi', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title_ko', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content_vi', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content_ko', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('lmsdata_badge_user');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('badgeid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('course', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('lmsdata_badge_category');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title_vi', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title_ko', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content_ko', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }if ($oldversion < 2020012101) {
        $table = new xmldb_table('lmsdata_badge_category');
        $field = new xmldb_field('content_ko', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('content_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020012102) {
        $table = new xmldb_table('lmsdata_course_like');
        $field = new xmldb_field('isopend', XMLDB_TYPE_INTEGER, 1, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020012103) {
        $table = new xmldb_table('lmsdata_badge');

        $field = new xmldb_field('content_vi', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }
        $field = new xmldb_field('content_ko', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }
        $field = new xmldb_field('content_en', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }
    }
    if ($oldversion < 2020012104) {
        $table = new xmldb_table('lmsdata_badge');

        $field = new xmldb_field('title_vi', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }
        $field = new xmldb_field('title_ko', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }
        $field = new xmldb_field('title_en', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }
    }
    if ($oldversion < 2020012105) {
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('order', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020012106) {
        $table = new xmldb_table('lmsdata_badge_category');
        $field = new xmldb_field('order', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('ordered', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020012107) {
        $table = new xmldb_table('lmsdata_badge_category');
        $field = new xmldb_field('usd', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('using', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020012108) {
        $table = new xmldb_table('lmsdata_badge_category');
        $field = new xmldb_field('using', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('used', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020012109) {
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('using', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('order', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('use', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }

    if ($oldversion < 2020012200) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('keywords_en', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('keywords_vi', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020013000) {
        $table = new xmldb_table('lmsdata_badge_category');
        $field = new xmldb_field('ordered', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('isused', XMLDB_TYPE_INTEGER, 1, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020013001) {
        $table = new xmldb_table('lmsdata_teacher');
        $field = new xmldb_field('profile_en', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('profile_vi', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('comment_en', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('comment_vi', XMLDB_TYPE_TEXT, 'medium', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('oneline', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('oneline_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('oneline_vi', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2020013002) {
        $table = new xmldb_table('lmsdata_teacher');
        $field = new xmldb_field('oneline', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('oneline_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('oneline_vi', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020013003) {
        $table = new xmldb_table('lmsdata_teacher');
        $field = new xmldb_field('oneline', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('oneline_en', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('oneline_vi', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020013101) {
        $table = new xmldb_table('lmsdata_badge');
        $field = new xmldb_field('ordered', XMLDB_TYPE_INTEGER, 10, null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2020020300) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_course_period');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('payid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('reviewperiod', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('starttime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('endtime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2020021200) {
        $table = new xmldb_table('lmsdata_user');
        $field = new xmldb_field('sns', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('snsid', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('position', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('psosok', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('allownews', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('allowedudata', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('postareacode', XMLDB_TYPE_CHAR, '5', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        $field = new xmldb_field('entrydate', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2020021300) {
        $table = new xmldb_table('lmsdata_user');
        $field = new xmldb_field('email2', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('snsid', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2020021400) {
        $table = new xmldb_table('lmsdata_payment');
        $field = new xmldb_field('paycode', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020022000) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('skills', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('klevel', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020022101) {
        $table = new xmldb_table('lmsdata_class_skills');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('classid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('skillid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2020022102) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('klevel', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2020022103) {
        $table = new xmldb_table('lmsdata_class');
        $field = new xmldb_field('klevel', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('skills', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
    }
    if ($oldversion < 2020022104) {
        $table = new xmldb_table('lmsdata_main_course');
        $field = new xmldb_field('recommended', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $table = new xmldb_table('lmsdata_main_job');
        $field = new xmldb_field('recommended', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2020022700) {
        $table = new xmldb_table('lmsdata_user');
        $field = new xmldb_field('agreetime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

    }
    if ($oldversion < 2020030900) {
        $table = new xmldb_table('lmsdata_payment');
        $field = new xmldb_field('paymethod', XMLDB_TYPE_CHAR, '100', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }  
    
    if ($oldversion < 2020031300) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_refund');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('payid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('text', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('bank', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('refundbanknr', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
     if ($oldversion < 2020031301) {
        $table = new xmldb_table('lmsdata_refund');
        $field = new xmldb_field('completedate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }  
    if ($oldversion < 2020031302) {
        $table = new xmldb_table('lmsdata_refund');
        $field = new xmldb_field('refundtype', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    } 
    if ($oldversion < 2020031303) {
        $table = new xmldb_table('lmsdata_refund');
        $field = new xmldb_field('refunddate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    } 
    if ($oldversion < 2020123002) {
        $table = new xmldb_table('course_completions');
        $field = new xmldb_field('progress', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }     
    if ($oldversion < 2021022201) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_certificate');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('certificatenumber', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('usergroup', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);    
        $table->add_field('reconfirm', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if ($oldversion < 2021022201) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_certificateno');
        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('code', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    return true;
}
