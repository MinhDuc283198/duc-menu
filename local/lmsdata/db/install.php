<?php

function xmldb_local_lmsdata_install() {
    
    global $DB, $CFG;
    
    $menu_main = array();

    $menu = new Stdclass();

    $menu->sortorder = 0;
    $menu->default = true;
    $menu->koname = '강의알림사항';
    $menu->enname = 'Course Notice';
    $menu->url = $CFG->wwwroot.'/local/courselist/courseoverview.php';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'course_notice_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

    $menu = new Stdclass();
    $menu->sortorder = 1;
    $menu->default = true;
    $menu->koname = '강의콘텐츠저장소';
    $menu->enname = 'Resources';
    $menu->url = $CFG->wwwroot.'/local/repository/index.php';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'repository_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

    $menu = new Stdclass();
    $menu->sortorder = 2;
    $menu->default = true;
    $menu->koname = 'CDMS';
    $menu->enname = 'CDMS';
    $menu->url = $CFG->wwwroot.'/local/repository/cdms.php';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'cdms_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

    $menu = new Stdclass();
    $menu->sortorder = 3;
    $menu->default = true;
    $menu->koname = '평가/설문';
    $menu->enname = 'Evaluation&Survey';
    $menu->url = $CFG->wwwroot.'/local/evaluation/index.php';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'survey_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

    $menu = new Stdclass();
    $menu->sortorder = 4;
    $menu->default = true;
    $menu->koname = '개인일정표';
    $menu->enname = 'My Schedule';
    $menu->url = $CFG->wwwroot.'/calendar/view.php?view=month';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'calendar_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

                $sql = "
         SELECT DISTINCT
             h.id,
             h.name,
             h.wwwroot,
             a.name as application,
             a.display_name
         FROM
             {mnet_host} h,
             {mnet_application} a,
             {mnet_host2service} h2s_IDP,
             {mnet_service} s_IDP,
             {mnet_host2service} h2s_SP,
             {mnet_service} s_SP
         WHERE
             h.id <> ? AND
             h.id <> ? AND
             h.id = h2s_IDP.hostid AND
             h.deleted = 0 AND
             h.applicationid = a.id AND
             h2s_IDP.serviceid = s_IDP.id AND
             s_IDP.name = 'sso_idp' AND
             h2s_IDP.publish = '1' AND
             h.id = h2s_SP.hostid AND
             h2s_SP.serviceid = s_SP.id AND
             s_SP.name = 'sso_idp' AND
             h2s_SP.publish = '1' AND
             a.name = 'mahara' 
         ORDER BY
             a.display_name,
             h.name";

    $hosts = $DB->get_records_sql($sql, array($CFG->mnet_localhost_id, $CFG->mnet_all_hosts_id));
    if($hosts){
        foreach ($hosts as $host) {
            $url = $CFG->wwwroot . "/auth/mnet/jump.php?hostid={$host->id}";
        }
        $menu = new Stdclass();
        $menu->sortorder = 5;
        $menu->default = true;
        $menu->koname = '이포트폴리오';
        $menu->enname = 'My Profile';
        $menu->url = $url;
        $menu->target = 0;
        $menu->disable = true;
        $menu->icon = 'mahara_gray';
        $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
        $menu_main[$menu->sortorder] = $menu;
    }
    $menu = new Stdclass();
    $menu->sortorder = 6;
    $menu->default = true;
    $menu->koname = '공지사항';
    $menu->enname = 'Notice';
    $menu->url = $CFG->wwwroot . '/local/jinoboard/list.php?id=1';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'notice_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

    $menu = new Stdclass();
    $menu->sortorder = 7;
    $menu->default = true;
    $menu->koname = 'FAQ';
    $menu->enname = 'FAQ';
    $menu->url = $CFG->wwwroot . '/local/jinoboard/list.php?id=3';
    $menu->target = 0;
    $menu->disable = true;
    $menu->icon = 'faq_gray';
    $menu->role = array('sa'=> true, 'pr'=>true, 'ad'=>true, 'rs'=>true, 'gs'=>true);
    $menu_main[$menu->sortorder] = $menu;

    $menu_main = serialize($menu_main);
    set_config('siteadmin_menu_main', $menu_main);
    
    //재능 카테고리 기본값 셋팅
    $dbman = $DB->get_manager();
    $table = new xmldb_table('lmsdata_category');
    if ($dbman->table_exists($table)) {
        //1. 회사 이해
        $category = new stdclass();
        $category->depth = 1;
        $category->step = 1;
        $category->parent = 0;
        $category->type = 1;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $parentId = $DB->insert_record('lmsdata_category', $category);
        $DB->set_field('lmsdata_category', 'parent', $parentId, array('id'=>$parentId));
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'company';
        $categoryName->categoryid = $parentId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '회사이해';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //1-1 JEI 이해
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 0;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'JEI';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = 'JEI 이해';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //1-2 멘토 제도
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 1;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'mento';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '멘토 제도';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //2. 상품 이해
        $category = new stdclass();
        $category->depth = 1;
        $category->step = 2;
        $category->parent = 0;
        $category->type = 1;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $parentId = $DB->insert_record('lmsdata_category', $category);
        $DB->set_field('lmsdata_category', 'parent', $parentId, array('id'=>$parentId));
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'product';
        $categoryName->categoryid = $parentId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '상품 이해';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //2-1 스스로 학습 시스템 4.0
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 0;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'edu';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '스스로 학습 시스템 4.0';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //2-2 수학
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 1;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'math';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '수학';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //3. 회원 관리
        $category = new stdclass();
        $category->depth = 1;
        $category->step = 3;
        $category->parent = 0;
        $category->type = 1;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $parentId = $DB->insert_record('lmsdata_category', $category);
        $DB->set_field('lmsdata_category', 'parent', $parentId, array('id'=>$parentId));
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'Membership';
        $categoryName->categoryid = $parentId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '회원 관리';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //3-1 관리 앱
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 0;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'app';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '관리 앱';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //3-2 계층 별 관리
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 1;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'depth';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '계층 별 관리';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //4. 학부모상담
        $category = new stdclass();
        $category->depth = 1;
        $category->step = 4;
        $category->parent = 0;
        $category->type = 1;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $parentId = $DB->insert_record('lmsdata_category', $category);
        $DB->set_field('lmsdata_category', 'parent', $parentId, array('id'=>$parentId));
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'parent';
        $categoryName->categoryid = $parentId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '학부모상담';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //4-1 가입 상담
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 0;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'join';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '가입 상담';
        $DB->insert_record('lmsdata_category_name', $categoryName);
        
        //4-2 학습 관리
        $category = new stdclass();
        $category->depth = 2;
        $category->step = 1;
        $category->parent = $parentId;
        $category->type = 2;
        $category->userid = 2;
        $category->isused = 1;
        $category->timecreated = time();
        $category->timemodified = time();
        $category->ispopup = 1;
        $category->sub_parent = 0;
        $categoryId = $DB->insert_record('lmsdata_category', $category);
        
        $categoryName = new stdclass();
        $categoryName->lang = 'en';
        $categoryName->name = 'edu';
        $categoryName->categoryid = $categoryId;
        $categoryName->timemodified = time();
        $DB->insert_record('lmsdata_category_name', $categoryName);
        $categoryName->lang = 'ko';
        $categoryName->name = '학습 관리';
        $DB->insert_record('lmsdata_category_name', $categoryName);
    }
    
    
    return true;
}