<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used in forum.
 *
 * @package    local_courselist
 * @copyright  2013 Rajesh Taneja <rajesh@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once $CFG->dirroot . '/local/lmsdata/lib.php';

/**
 * Event observer for local_courselist.
 */
class local_lmsdata_observer {

    public static function user_created(\core\event\user_created $event) {
        global $CFG, $DB;

        $event_data = $event->get_data();
        $userid = $DB->get_field('lmsdata_user', 'userid', array('userid' => $event_data['objectid']));
        if ($userid === FALSE) {
            $user = $DB->get_record('user', array('id' => $userid));

            $lmsdata_user = new stdClass();
            $lmsdata_user = new stdClass();
            $lmsdata_user->userid = $user->id;
            $lmsdata_user->eng_name = $user->lastname . ' ' . $user->firstname;
            $lmsdata_user->usergroup = 'rs';
            $lmsdata_user->b_temp = 1;
            $lmsdata_user->b_mobile = 0;
            $lmsdata_user->b_email = 0;
            $lmsdata_user->univ = null;
            $lmsdata_user->major = null;
            $lmsdata_user->year = 0;
            $lmsdata_user->b_tel = 0;
            $lmsdata_user->b_univ = 0;
            $lmsdata_user->b_major = 0;
            $lmsdata_user->ehks = null;
            $lmsdata_user->edhs = null;
            $lmsdata_user->domain = null;
            $lmsdata_user->hyhg = null;
            $lmsdata_user->persg = null;
            $lmsdata_user->psosok = null;
            $lmsdata_user->sex = null;

            $DB->insert_record('lmsdata_user', $lmsdata_user);
        }
    }

    public static function user_deleted(\core\event\user_deleted $event) {
        global $CFG, $DB;

        $event_data = $event->get_data();

        $DB->delete_records('lmsdata_user', array('userid' => $event_data['objectid']));
    }

    public static function userloggedin(\core\event\user_loggedin $event) {
        global $DB, $CFG, $SESSION;

        $event_data = $event->get_data();
        $ipaddress = local_lmsdata_get_client_ip();

        $user = $DB->get_record('user', array('id' => $event_data['userid']));
        $data = local_lmsdata_browser_check();
        $user_log = new stdClass();
        $user_log->username = $user->username;
        $user_log->ip = $ipaddress;
        $user_log->mobile = $data->device;
        $user_log->action = 'logged';
        $user_log->log_date = time();
        $user_log->log_device_info = $data->device_detail . '[' . $data->browser_name_regex . ']';
        $recentlogin = $DB->get_record_sql("select * from {siteadmin_loginfo} where username  = ?  order by id desc limit 1 ", array($user->username));
        $addsessiontime = strtotime("-$CFG->sessiontimeout seconds");
//        $addsessiontime = strtotime("-30 seconds");
        if ($addsessiontime > $recentlogin->log_date) {
            $DB->insert_record('siteadmin_loginfo', $user_log);
        }
        error_log(' [login insert timeout  :' . $addsessiontime . ' && last ' . $recentlogin->log_date . '-' ."]\n", 3, '/var/www/moodle/loginlog.log');
        //fcm 토큰 등록
        if (!empty($SESSION->fcmtoken)) {

            $ori_pushtoken = $DB->get_record('lmsdata_pushtoken', array('username' => $user->username));
            if (!empty($ori_pushtoken)) {
                if ($ori_pushtoken->token != $SESSION->fcmtoken) {
                    $ori_pushtoken->token = $SESSION->fcmtoken;
                }
                $ori_pushtoken->timeupdated = time();
                $ori_pushtoken->lastlogin = time();
                $ori_pushtoken->lastlogin = time();
                if ($ori_pushtoken->deviceinform != $SESSION->appdevice) {
                    $ori_pushtoken->deviceinform = $SESSION->appdevice;
                }
                if ($ori_pushtoken->deviceid != $SESSION->deviceid) {
                    $ori_pushtoken->deviceid = $SESSION->deviceid;
                }

                $DB->update_record('lmsdata_pushtoken', $ori_pushtoken);
            } else {
                $pushinform = new stdClass();
                $pushinform->token = $SESSION->fcmtoken;
                $pushinform->username = $user->username;
                $pushinform->timecreated = time();
                $pushinform->lastlogin = time();
                $pushinform->deviceid = $SESSION->deviceid;
                $pushinform->deviceinform = $SESSION->appdevice;

                $DB->insert_record('lmsdata_pushtoken', $pushinform);
            }
        }
    }

    public static function user_enrolment_deleted(\core\event\user_enrolment_deleted $event) {
        global $CFG, $DB;

        $event_data = $event->get_data();
        if ($DB->record_exists('lmsdata_course_applications', array('courseid' => $event_data['courseid'], 'userid' => $event_data['relateduserid']))) {
            $DB->delete_records('lmsdata_course_applications', array('courseid' => $event_data['courseid'], 'userid' => $event_data['relateduserid']));
        }
    }

}
