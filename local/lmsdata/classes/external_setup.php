<?php

/**
 * The class \local\lmsdata\external_setup is defined here.
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_lmsdata;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/dml/moodle_database.php'); 

class external_setup {
/**
 * 외부 DB에 conent를 해준다.
 * 
 * @param stdclass $EDB
 * @param stdclass $DBINFO
 *                      $DBINFO->dbtype         - 'pgsql', 'mariadb', 'mysqli', 'mssql', 'sqlsrv' or 'oci'
 *                      $DBINFO->dbhost,        - db host
 *                      $DBINFO->dbuser,        - userid
 *                      $DBINFO->dbpass,        - pwd
 *                      $DBINFO->dbname         - database 이름  
 *                      $DBINFO->prefix = false - 외부데이터베이스 연결 /  
 *                      $DBINFO->dboptions 
 * @return stdclass
 * @throws dml_exception
 * @throws \local_lmsdata\externaldb\moodle_exception
 */
    public function __construct(&$EDB, $DBINFO) {

        if (!isset($DBINFO->dbuser)) {
            $DBINFO->dbuser = '';
        }

        if (!isset($DBINFO->dbpass)) {
            $DBINFO->dbpass = '';
        }

        if (!isset($DBINFO->dbname)) {
            $DBINFO->dbname = '';
        }

        if (!isset($DBINFO->dblibrary)) {
            $DBINFO->dblibrary = 'native';
            // use new drivers instead of the old adodb driver names
            switch ($DBINFO->dbtype) {
                case 'postgres7' :
                    $DBINFO->dbtype = 'pgsql';
                    break;

                case 'mssql_n':
                    $DBINFO->dbtype = 'mssql';
                    break;

                case 'oci8po':
                    $DBINFO->dbtype = 'oci';
                    break;

                case 'mysql' :
                    $DBINFO->dbtype = 'mysqli';
                    break;
            }
        }

        if (!isset($DBINFO->dboptions)) {
            $CFG->dboptions = array();
        }

        if (isset($DBINFO->dbpersist)) {
            $DBINFO->dboptions['dbpersist'] = $DBINFO->dbpersist;
        }
        
        $prefix = true;
        if(!empty(trim($DBINFO->prefix))) {
            $prefix = $DBINFO->prefix;
        }
        
        if (!$EDB = $this->get_driver_instance($DBINFO->dbtype, $DBINFO->dblibrary, $prefix)) {
            throw new dml_exception('dbdriverproblem', "Unknown driver $DBINFO->dblibrary/$DBINFO->dbtype");
        }
        try {
            $EDB->connect($DBINFO->dbhost, $DBINFO->dbuser, $DBINFO->dbpass, $DBINFO->dbname, false, $DBINFO->dboptions);
        } catch (moodle_exception $e) {
            if (empty($CFG->noemailever) and !empty($CFG->emailconnectionerrorsto)) {
                $body = "Connection error: ".$CFG->wwwroot.
                    "\n\nInfo:".
                    "\n\tError code: ".$e->errorcode.
                    "\n\tDebug info: ".$e->debuginfo.
                    "\n\tServer: ".$_SERVER['SERVER_NAME']." (".$_SERVER['SERVER_ADDR'].")";
                if (file_exists($CFG->dataroot.'/emailcount')){
                    $fp = @fopen($CFG->dataroot.'/emailcount', 'r');
                    $content = @fread($fp, 24);
                    @fclose($fp);
                    if((time() - (int)$content) > 600){
                        //email directly rather than using messaging
                        @mail($CFG->emailconnectionerrorsto,
                            'WARNING: Database connection error: '.$CFG->wwwroot,
                            $body);
                        $fp = @fopen($CFG->dataroot.'/emailcount', 'w');
                        @fwrite($fp, time());
                    }
                } else {
                   //email directly rather than using messaging
                   @mail($CFG->emailconnectionerrorsto,
                        'WARNING: Database connection error: '.$CFG->wwwroot,
                        $body);
                   $fp = @fopen($CFG->dataroot.'/emailcount', 'w');
                   @fwrite($fp, time());
                }
            }
            // rethrow the exception
            throw $e;
        }
    }
    
        /**
     * Loads and returns a database instance with the specified type and library.
     *
     * The loaded class is within lib/dml directory and of the form: $type.'_'.$library.'_moodle_database'
     *
     * @param string $type Database driver's type. (eg: mysqli, pgsql, mssql, sqldrv, oci, etc.)
     * @param string $library Database driver's library (native, pdo, etc.)
     * @param bool $external True if this is an external database.
     * @return moodle_database driver object or null if error, for example of driver object see {@link mysqli_native_moodle_database}
     */
    public static function get_driver_instance($type, $library, $external = false) {
        global $CFG;

        $classname = $type.'_'.$library.'_moodle_database';
        $libfile   = "$CFG->libdir/dml/$classname.php";

        if (!file_exists($libfile)) {
            return null;
        }

        require_once($libfile);
        return new $classname($external);
    }
}
