<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();

$PAGE->set_context($context);
require_login();

$id = optional_param('id', 0, PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);

$data = new stdClass();

if (!empty($id)) {
    $userid = $id;
} else {
    $userid = $USER->id;
}
$sql = ' SELECT u.* '        
        . ' FROM {user} u '
        . ' LEFT JOIN {lmsdata_user} yu on u.id = yu.userid '
        . ' WHERE u.id = :id';
$user = $DB->get_record_sql($sql, array('id' => $userid));
$menu = implode( '/' ,$_POST['chkbox']);


if (!$main = $DB->get_record('lmsdata_mainmenu', array('userid' => $edit))) {

    $data->userid = $USER->id;
    $data->menus= $menu;
    $data->timecreated = time();
    $data->timemodified = time();
      
   $DB->insert_record('lmsdata_mainmenu', $data);
 
redirect($CFG->wwwroot);

} else {    
   $data->id = $edit;
      
   $data = new stdClass();
   $data->id = $main->id;
   $data->userid =  $edit;
   $data->timemodified = time();
   $data->menus = $menu;
   
   $mainmenu = $DB->update_record('lmsdata_mainmenu', $data);
   
redirect($CFG->wwwroot); 
}    
