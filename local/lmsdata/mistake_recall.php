<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . "/chamktu/lib.php");

$classid = optional_param('classid', 0, PARAM_INT);
$newclassid = optional_param('newclassid', 0, PARAM_INT);

$applications = $DB->get_records('lmsdata_course_applications', array('courseid'=>$classid));

foreach($applications as $application){
    
    $application->courseid = $newclassid;
    $DB->update_record('lmsdata_course_applications',$application);
    
    if($application->paymentstatus == 'complete' && $application->status == 'apply'){
        $userinfo = $DB->get_record('lmsdata_user', array('userid'=>$application->userid));
        $sql = 'select mc.* from {lmsdata_class} lc join {course} mc on mc.id = lc.courseid where lc.id = :id';
        $class = $DB->get_record_sql($sql, array('id'=>$newclassid));
        $userinfo->roleid = 5;
        set_assign_user($class, $userinfo);
    }
}

print_object($applications);
