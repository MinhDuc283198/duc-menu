<?php
/**
 * 이수 완료 크론
 */
define('CLI_SCRIPT', true);

/** Used by library scripts to check they are being called by Moodle */
define('MOODLE_INTERNAL', true);

// Disables all caching.
define('CACHE_DISABLE_ALL', true);

define('PHPUNIT_TEST', false);

define('IGNORE_COMPONENT_CACHE', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$current_time = time();

$sql = 'SELECT cp.* FROM {course_completions} cp
        JOIN {lmsdata_class} lc ON lc.courseid = cp.course
        WHERE lc.evaluationstart < :ctime1 && lc.learningend+69120 > :ctime2';

$completese = $DB->get_records_sql($sql, array('ctime1'=>$current_time, 'ctime2'=>$current_time));

foreach($completese as $complete){
    $sql = 'SELECT lca.* 
            FROM {lmsdata_course_applications} lca
            JOIN {lmsdata_class} lc on lc.id = lca.courseid
            WHERE lca.userid=:userid AND lc.courseid=:courseid AND lca.status = :apply';
    $application = $DB->get_record_sql($sql, array('userid'=>$complete->userid, 'courseid'=>$complete->course, 'apply'=>'apply'));
    
    if(!empty($application) && $application->completionstatus == 0 && !empty($complete->timecompleted)){
        $application->completionstatus = 1;
        $DB->update_record('lmsdata_course_applications', $application);
        
        $sql = 'SELECT lc.*, cc.name as coursetype, lco.coursename, lco.courseid as lcourseid, cod.codename as gradename
                FROM {lmsdata_class} lc
                JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
                JOIN {course} mc ON mc.id = lco.courseid
                JOIN {course_categories} cc ON cc.id = mc.category
                JOIN {lmsdata_code} cod ON cod.id = lco.coursegrade
                WHERE lc.id = :id';
        $courseinfo = $DB->get_record_sql($sql, array('id'=>$application->courseid));
        
        $sql = 'SELECT mu.lastname as teachername 
                FROM {lmsdata_course_teacher} lct
                JOIN {lmsdata_user} lu ON lu.id = lct.lmsuserid
                JOIN {user} mu ON mu.id = lu.userid
                WHERE lct.courseid = :courseid';
        $teachers = $DB->get_records_sql($sql, array('courseid'=>$courseinfo->lcourseid));
        
        $teachername = '';
        foreach($teachers as $teacher){
            if($teachername != ''){
                $teachername .= ', ';
            }
            $teachername .= $teacher->teachername;
        }
        
        eduhope_update_training_history($application->id, $application->userid, $courseinfo->coursetype, $courseinfo->coursename, $courseinfo->gradename, $courseinfo->learningstart, $courseinfo->learningent, $teachername, 1, $application->paymentstatus);
    } 
    //관리자 이수강제 변경 기능으로 인하여 삭제
//    else if(!empty($application) && $application->completionstatus == 1 && empty($complete->timecompleted)){
//        $application->completionstatus = 0;
//        $DB->update_record('lmsdata_course_applications', $application);
//        eduhope_update_training_history_status($application->id);
//    }
}

