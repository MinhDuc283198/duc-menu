<?php
require('../../config.php');
require_once $CFG->dirroot.'/local/lmsdata/lib.php';
require_once $CFG->dirroot.'/local/course/lib.php';
require_login();
$classid = required_param("classid", PARAM_INT);
$payid = required_param("payid", PARAM_INT);
$courseid = $DB->get_field("lmsdata_class",'courseid',array("id"=>$classid));
$enrolresult = local_course_whether_enrol($courseid, $USER->id, 'student');
if($enrolresult == false){
    $result = local_lmsdata_enrol_user($courseid, $USER->id, 'student');
    $returnvalues = new stdClass();
    $returnvalues -> status = $result; 
    if($result){
        $returnvalues -> msg = get_string("successenrol","local_lmsdata");
    }else{
        $result = local_lmsdata_enrol_edit_user($courseid, $USER->id, 'student');
        $returnvalues -> msg = "재수강이 완료되었습니다.";
    }
    $lmsdata = new stdClass();
    $lmsdata->payid = $payid;
    $lmsdata->userid = $USER->id;
    $lmsdata->reviewperiod = $result->reviewperiod;
    $lmsdata->starttime = $result->timestart;
    $lmsdata->endtime = $result->endtime;
    $lmsdata->timecreated = time();
    $lmsdata->timemodified = time();
       
    $DB->insert_record('lmsdata_course_period', $lmsdata);
}else{
    $returnvalues -> msg = get_string("alreadyenrol","local_lmsdata");
}

echo json_encode($returnvalues);
