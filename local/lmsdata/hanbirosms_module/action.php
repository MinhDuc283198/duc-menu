<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	exit('Access denied');
}

require_once realpath(__DIR__) . '/fptsms.php';

// Phones
$_phones = $_POST['phones'] ? $_POST['phones'] : '';
$_phones = str_replace('
', ';', $_phones);
$_phones = trim($_phones);
$_phones = explode(';', $_phones);

// Message
$_msg = $_POST['msg'] ? $_POST['msg'] : '';

// Brand name
$_brand_name = $_POST['brand_name'] ? $_POST['brand_name'] : 'FTI';

// Client ID
$_client_id = $_POST['client_id'] ? $_POST['client_id'] : '';

// Client secret
$_client_secret = $_POST['client_secret'] ? $_POST['client_secret'] : '';

// Sending Time
$_sending_time = $_POST['sending_time'] ? $_POST['sending_time'] : '';

$fptSms  = new fptSms();
$fptSms->init($_brand_name, $_client_id, $_client_secret);
$arrNotification = $fptSms->send_multi_sms($_phones, $_msg, $_sending_time);

echo "<pre>".print_r($arrNotification, true).'</pre>'; die();