<?php
require_once realpath(__DIR__) . '/Autoload.php';

TechAPIAutoloader::register();

use TechAPI\Constant;
use TechAPI\Client;
use TechAPI\Auth\ClientCredentials;

// config api
Constant::configs(array(
    //'mode'            => Constant::MODE_LIVE,
    'mode'            => Constant::MODE_SANDBOX,
    'connect_timeout' => 30,
    'enable_cache'    => true,
    'enable_log'      => true,
    'log_path'        => realpath(__DIR__) . '/logs'
));


// config client and authorization grant type
function getTechAuthorization($_client_id = '', $_client_secret = '', $_scope = array('send_brandname_otp'))
{    
    $client = new Client(
        $_client_id,
        $_client_secret,
        $_scope
    );
    
    return new ClientCredentials($client);
}