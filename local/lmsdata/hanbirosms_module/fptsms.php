<?php
require_once realpath(__DIR__) . '/TechAPI/init.php';

use TechAPI\Exception;
use TechAPI\Auth\AccessToken;
use TechAPI\Api\SendBrandnameOtp;

class fptSms
{
	protected $_brand_name		= '';
	protected $_client_id 		= '';
	protected $_client_secret 	= '';
	protected $_scope 			= array();

	public function __construct()
    {

    }

    public function init($_brand_name, $_client_id, $_client_secret){
		$this->_brand_name 		= $_brand_name;
		$this->_client_id 		= $_client_id;
		$this->_client_secret 	= $_client_secret;
		$this->_scope 			= array('send_brandname_otp', 'send_brandname');
    }

    /**
     * Send to phone number
     *
     * @param      string  $phone  The phone
     * @param      string  $msg    The message
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function send_sms($phone='', $msg='', $time=''){
    	// Get post data
		$arrData = array(
			'BrandName'  	=> $this->_brand_name,
			'Phone'			=> $phone,
			'Message'    	=> $msg,
			'ScheduleTime'	=> $time
		);

		try
	    {
			// Get ACCESS TOKEN
	    	$oGrantType = getTechAuthorization($this->_client_id, $this->_client_secret, $this->_scope);
                    print_object($oGrantType);
                    
	    	// call api
	        $apiSendBrandname = new SendBrandnameOtp($arrData);
	        $arrResponse = $oGrantType->execute($apiSendBrandname);
                print_object($arrResponse);
	        // kiểm tra kết quả trả về có lỗi hay không
	        if (! empty($arrResponse['error']))
	        {
	            // Xóa cache access token khi có lỗi xảy ra từ phía server
	            AccessToken::getInstance()->clear();
	            
	            // quăng lỗi ra, và ghi log
	            // throw new TechException($arrResponse['error_description'], $arrResponse['error']);
	        }

	        return $arrResponse;
	    }
	    catch (\Exception $ex)
	    {
	    	return array(
	    		'error'	=> $ex->getCode(),
	    		'error_description' => $ex->getMessage()
	    	);
	    }
    }


    /**
     * Send to each phone number
     *
     * @param      array  $phones  The phones
     * @param      string  $msg     The message
     */
    public function send_multi_sms($phones = array(), $msg = '', $time = ''){
    	$arrSuccess = array();
    	$arrError 	= array();

		// Get post data
		$arrData = array(
			'BrandName'  	=> $this->_brand_name,
			'Message'    	=> $msg,
			'ScheduleTime'	=> $time
		);

		if(is_array($phones)){
			foreach ($phones as $key => $phone) {
				$arrData['Phone'] = $phone;
				try
	            {
	            	// Get ACCESS TOKEN
	            	$oGrantType = getTechAuthorization($this->_client_id, $this->_client_secret, $this->_scope);

	                // call api
	                $apiSendBrandname = new SendBrandnameOtp($arrData);
	                $arrData = $oGrantType->execute($apiSendBrandname);

	                if (! empty($arrData['error']))
	                {
	                    // clear access token when error
	                    AccessToken::getInstance()->clear();

	                    throw new Exception($arrData['error_description'], $arrData['error']);
	                }
	                
	                $arrSuccess[] = $phone;
	            }
	            catch (\Exception $ex)
	            {
	            	$arrError[] = $phone;
	            }
			}
		}

		return array(
			'arr_success'	=> $arrSuccess,
			'arr_error'		=> $arrError
		);
    }
}