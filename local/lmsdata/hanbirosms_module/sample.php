<?php
	ini_set('default_charset', 'UTF-8');
?> 


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Send SMS</title>
    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script>

</head>
<body class="clearfix">
    <h4 style="text-align: center;">
        <strong>Send SMS brand name sample!</strong>
    </h4>
    <div class="clearfix container">
        <form method="post" action="action.php">
            <div class="form-group">
                <label for="brand_name">
                    Brand name <span style="color: red">(Customer config).</span>
                </label>
                <input class="form-control" type="text" name="brand_name" value="FTI">
            </div>

            <div class="form-group">
                <label for="brand_name">
                    Client ID <span style="color: red">(Customer config).</span>
                </label>
                <input class="form-control" type="text" name="client_id" value="C793083e486F43f8DEa01667691a293065B36899">
            </div>

            <div class="form-group">
                <label for="brand_name">
                    Client Secret <span style="color: red">(Customer config).</span>
                </label>
                <input class="form-control" type="text" name="client_secret" value="5cc213Fe1c32893084b2d551a9a41e160a34c6472f4731E3c4c01955d9625588844932CF">
            </div>

            <div class="form-group">
                <label for="phones">
                    Phone numbers <span style="color: red">(There may be multiple Phone numbers, one per line).</span>
                </label>
                <br />
                <textarea class="form-control" name="phones" id="phones" rows="5"></textarea>
            </div>

            <div class="form-group">
                <label for="msg">Message</label>
                <textarea class="form-control" name="msg" id="msg" rows="5"></textarea>
            </div>

            <!-- <div class="form-group">
                <label for="sending_time">
                    Sending Time
                </label>
                <input class="form-control" type="text" name="sending_time" value="">
            </div> -->

            <button type="submit" class="btn btn-primary">Send</button>
        </form>
    </div>
</body>
</html>

