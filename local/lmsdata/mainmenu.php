<?php
/**
 * 필수과정, 선택과정, 자격인증 과정 안내 및 신청
 * -> $menu_gubun변수를 활용하여 과정 구분을 해줌
 */
//error_reporting(E_ALL);
//
//ini_set("display_errors", 1);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');


$context = context_system::instance();

$PAGE->set_context($context);
require_login();
$id = optional_param('id', 0, PARAM_INT);
//$classid = required_param("classid", PARAM_INT);

if (!empty($id)) {
    $userid = $id;
} else {
    $userid = $USER->id;
}
$sql = ' SELECT u.* 
                FROM {user} u 
                LEFT JOIN {lmsdata_user} yu on u.id = yu.userid 
                WHERE u.id = :id';
$user = $DB->get_record_sql($sql, array('id' => $USER->id));

$menu_sql = "SELECT lm.userid, lm.menus
              FROM {lmsdata_mainmenu} lm                    
              WHERE lm.userid = :userid ";
$params = array('userid' => $USER->id);
$menu = $DB->get_record_sql($menu_sql, $params);
$quick_menus = explode('/', $menu->menus);

for ($i = 1; $i < 7; $i++) {
    $indexnum[] = $i;
    if (in_array($i, $quick_menus)) {
        
    } else {
        $not_quick_menus[] = $i;
    }
}

$iconarr = array(
    1 => '<img src="/theme/oklassedu/pix/images/icon_link01.png" alt="학습중인과정"/>',
    2 => '<img src="/theme/oklassedu/pix/images/icon_link02.png" alt="나의 학습 콘텐츠"/>',
    3 => '<img src="/theme/oklassedu/pix/images/icon_link03.png" alt="필수과정"/>',
    4 => '<img src="/theme/oklassedu/pix/images/icon_link04.png" alt="선택과정"/>',
    5 => '<img src="/theme/oklassedu/pix/images/icon_link05.png" alt="자격인증"/>',
    6 => '<img src="/theme/oklassedu/pix/images/icon_link06.png" alt="교육콘텐츠"/>'
);
$iconnamearr = array(
    1 => '학습중인과정',
    2 => '나의 학습 콘텐츠',
    3 => '필수과정',
    4 => '선택과정',
    5 => '자격인증',
    6 => '교육콘텐츠'
);
?>
<script type="text/javascript">
    $(document).ready(function () {
        var axix = "x";
        if (window.innerWidth < 768) {
            axix = "y";
        }
        $(".sortable-list").sortable({
            axis: axix,
            cursor: "move",
            opacity: 0.5,
        });
        $(window).resize(function () {
            if (window.innerWidth < 768) {
                $(".sortable-list").sortable("option", "axis", "y");
            } else {
                $(".sortable-list").sortable("option", "axis", "x");
            }
        });

    });

    function count_ck(obj) {

        var chkbox = document.getElementsByName("chkbox[]");
        var chkCnt = 0;
        for (var i = 0; i < chkbox.length; i++) {
            if (chkbox[i].checked) {
                chkCnt++;
            }
        }
        if (chkCnt > 4) {
            alert("네 개까지만 선택해주세요.");
            obj.checked = false;
            return false;
        }
    }
    function check() {
        var chkbox = document.getElementsByName("chkbox[]");
        var chkCnt = 0;
        for (var i = 0; i < chkbox.length; i++) {
            if (chkbox[i].checked) {
                chkCnt++;
            }
        }
        if (chkCnt < 4) {
            alert('네 개를 선택해주세요');
            return false;
        } else
            return true;
    }


</script>
<div class="popwrap">
    <div class="pop_title">
        메뉴설정
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_pop_close.png" alt="" />
    </div>
    <form name="" action="/local/lmsdata/pop_setting.execute.php" method="post" onsubmit="return check()" enctype="multipart/form-data"> 
        <div class="pop_content">      
            <div class="colorbox">
                바로가기 메뉴를 설정합니다. <br />
                아래 메뉴 중 총 4개 선택하세요. 선택한 메뉴를 끌어서 앞이나 뒤로 이동하면 순서를 조정할 수 있습니다.
            </div>
            <input type="hidden" name="edit" value="<?php echo $user->id; ?>">
            <ul class="sortable-list">

                <?php
                if ($menu) {
                    foreach ($quick_menus as $quickmenu) {
                        ?>
                        <li>
                            <label>
                                <input type="checkbox"  name="chkbox[]" onClick="count_ck(this);" value="<?php echo $quickmenu ?>" checked/>
                                <?php echo $iconarr[$quickmenu]; ?> 
                                <span><?php echo $iconnamearr[$quickmenu]; ?> </span> 
                            </label>
                        </li>
                        <?php
                    }
                    foreach ($not_quick_menus as $nquickmenu) {
                        ?>
                        <li>
                            <label>
                                <input type="checkbox"  name="chkbox[]" onClick="count_ck(this);" value="<?php echo $nquickmenu ?>"/>
                                <?php echo $iconarr[$nquickmenu]; ?> 
                                <span><?php echo $iconnamearr[$nquickmenu]; ?> </span> 
                            </label>
                        </li>
                        <?php
                    }
                } else {
                    foreach ($indexnum as $num) {
                        ?>
                        <li>
                            <label>
                                <input type="checkbox"  name="chkbox[]" onClick="count_ck(this);" value="<?php echo $num; ?>"/>
                                <?php echo $iconarr[$num]; ?> 
                                <span><?php echo $iconnamearr[$num]; ?> </span> 
                            </label>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>


        </div>
        <div class="pop_footer text-center">
            <input type="submit" onClick="count();" class="btn big pink" value="저장" /> 
            <input type="button" class="btn big close" value="닫기" /> 
        </div>
    </form>
</div>

