<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . "/chamktu/lib.php");

$sql = 'select lc.*, lco.coursecd from m_lmsdata_class lc 
        join m_lmsdata_course lco on lco.id = lc.parentcourseid
        where lc.classyear = 2017 and lc.classnum = 100';

$private_classes = $DB->get_records_sql($sql, array());
$cnt = 0;
foreach($private_classes as $private_class){
    $sql = "select * 
            from {lmsdata_applications_old}
            where restudyend >= 1511881200 and coursecd = :coursecd and status = 'apply' and (accountname != '연기' || accountname is null)";
    
    $enrole_users = $DB->get_records_sql($sql, array('coursecd'=>$private_class->coursecd));
    
    foreach($enrole_users as $enrole_user){
        $class = $DB->get_record('course', array('id'=>$private_class->courseid));
        $userinfo = $DB->get_record('lmsdata_user', array('userid'=>$enrole_user->userid));
        
        $userinfo->roleid = 5;
        set_assign_user($class, $userinfo);
        echo $class->fullname.' : '.$userinfo->eng_name.'('.$userinfo->userid.') 등록 <br />';
    }
}

echo $cnt;