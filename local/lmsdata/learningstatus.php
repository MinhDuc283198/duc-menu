<?php

require('../../config.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT); //courseid
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$forced = optional_param_array('forced',array(), PARAM_RAW);
$iscomp = optional_param_array('iscomp',array(), PARAM_RAW);
$today = time();
$search = optional_param('search', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);
$order = optional_param('order', 0, PARAM_INT);
$course = get_course($id);
$context = context_course::instance($course->id);
require_login(); 
require_capability('moodle/course:manageactivities', $context);
$PAGE->set_url('/local/lmsdata/learningstatus.php?id='.$id);
$PAGE->set_context($context);
$PAGE->set_title(get_string('learningstatus', 'theme_oklassedu'));
$PAGE->set_heading(get_string('learningstatus', 'theme_oklassedu'));
$PAGE->set_course($course);
$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->set_pagelayout('course');
if($excel==0){
    echo $OUTPUT->header();
}

$sql_where = array();
$params = array();
if($learningstart_str){
    $learningstart = str_replace(".", "-", $learningstart_str); 
    $learningstart = strtotime($learningstart);
}
if($learningend_str){
    $learningend = str_replace(".", "-", $learningend_str); 
    $learningend = strtotime($learningend) + 86399;
}
if($learningstart){
    $sql_where[] = " lcp.timecreated >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if($learningend){
    $sql_where[] = " (ue.timeend - (86400*lcp.reviewperiod)) <= :learningend ";
    $params['learningend'] = $learningend;
}
$forced_array = array('0'=>get_string('learningstatus:free_course','local_mypage'),'1'=>get_string('learningstatus:paid_course','local_mypage'));
$iscomp_array = array('2'=>get_string('learningstatus:iscomplete_2','local_mypage'),'1'=>get_string('learningstatus:iscomplete_1','local_mypage'),'0'=>get_string('learningstatus:iscomplete_0','local_mypage'));
if($forced){
    foreach($forced as $iskey => $isval){
        $sql_where_forced[] = " lcp.forced = :forced".$iskey;
        $params['forced'.$iskey] = $isval;
    }
    if (!empty($sql_where_forced)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_forced) . ')';
    } else {
        $sql_where[] = '';
    }
}
if($iscomp){
    foreach($iscomp as $iskey => $isval){
        if($isval==0){
            $sql_where_comp[] = "  (ue.timeend - (86400*lcp.reviewperiod)) > $today";
        } else if ($isval==1){
            $sql_where_comp[] = " cc.timecompleted is null";
        } else if ($isval==2){
            $sql_where_comp[] = " cc.timecompleted is not null";
        }
    }
    if (!empty($sql_where_comp)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_comp) . ')';
    } else {
        $sql_where[] = '';
    }
}

$like = '';
if (!empty($search)) {
    $like .= " AND ( " . $DB->sql_like('username', ':search1', false) .' OR '. $DB->sql_like('firstname', ':search2', false) . " ) ";
    $params['search1']=$search;
    $params['search2']=$search;
}

$where = '';
if(!empty($sql_where)){
    $where .= ' AND ' . implode(' and ', $sql_where);
}

switch($order){
    case 0 :  $orderby = '';
        break;
    case 1 :  $orderby = ' ORDER BY u.lastaccess DESC';
        break;
    case 2 :  $orderby = ' ORDER BY lcp.timecreated DESC';
        break;
}
//검색조건1 무료수강 / 결제 => lcp.forced
//검색조건2 수료 / 미수료 / 진행중 => enrol 시간
//
$parentcourseid = $DB->get_field_sql('select lc.parentcourseid FROM {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id where mc.id = :id',array('id'=>$id));

//유저 목록
$uselect = "SELECT u.id, u.username, u.firstname, u.lastaccess, lcp.forced, lcp.timecreated, lcp.paytype, (ue.timeend - (86400*lcp.reviewperiod)) as enddate, cc.timecompleted  ";
$ucount ="SELECT COUNT(*) ";

$usql = "FROM {user_enrolments} ue
        JOIN {enrol} e ON (e.id = ue.enrolid)
        JOIN {user} u ON ue.userid = u.id
        LEFT JOIN ( SELECT lp.userid,lc.courseid,lp.forced, lp.timecreated, lp.paytype, lc.reviewperiod FROM {lmsdata_class} lc 
                    JOIN {lmsdata_payment} lp ON lc.id = lp.ref WHERE lc.courseid = $id group by lp.userid ) lcp ON lcp.userid = u.id
        LEFT JOIN {course_completions} cc ON cc.userid = u.id AND cc.course = e.courseid
        WHERE e.courseid = $id and u.id not in (SELECT teacherid FROM {lmsdata_course_professor} WHERE courseid = $parentcourseid ) ";

$offset = ($page - 1) * $perpage;
if($excel==0){
    $userlist = $DB->get_records_sql($uselect.$usql.$where.$like.$orderby,$params, $offset, $perpage);
}else{
    $userlist = $DB->get_records_sql($uselect.$usql.$where.$like.$orderby,$params);    
}
$userlist_cnt = $DB->count_records_sql($ucount.$usql.$where.$like,$params);
$total_pages = jinoboard_get_total_pages($userlist_cnt, $perpage);
$cnt = $userlist_cnt - $offset;
// 검색 영역 추가하기
$perpage_array = [10,20,30,50];

if($excel==0){

?>

<div class="encourage-monitoring-summary">
    <!-- 검색폼 시작 -->
            <form id="frm_content_search" class="search-area">
                <input type="hidden" name="page" value="<?php echo $page; ?>" />
                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>" />
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="order" value="<?php echo $order; ?>">
                <input type="hidden" name="excel" value="<?php echo $excel; ?>">
                <div>
                    <label class="tit"><?php echo get_string('learningstatus:coursetype','local_mypage'); ?></label>
                    <?php foreach($forced_array as $iakey => $iaval){?>
                    <label class="tit"><input type="checkbox" name="forced[]" value="<?php echo $iakey?>" <?php foreach($forced as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                    <?php }?>
                </div>
                <div>
                    <label class="tit"><?php echo get_string('learningstatus:filter_iscomp','local_mypage'); ?></label> 
                     <?php foreach($iscomp_array as $iakey => $iaval){?>
                    <label class="tit"><input type="checkbox" name="iscomp[]" value="<?php echo $iakey?>" <?php foreach($iscomp as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                    <?php }?>
                </div>
                <div>
                    <label class="tit"><?php echo get_string('learningstatus:filter_period','local_mypage'); ?></label>
                    <input type="text" title="<?php echo get_string('time','local_management')?>" name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str?>" autocomplete=off placeholder="<?php echo get_string('click', 'local_management'); ?>  " >
                    <span class="dash">~</span> 
                    <input type="text" title="<?php echo get_string('time','local_management')?>" name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str?>" autocomplete=off placeholder="<?php echo get_string('click', 'local_management'); ?>  " > 
                </div>
                <label class="tit"><?php echo get_string('search','local_management')?></label>
                <input type="text" title="search" name="search"  id="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo '이름, 아이디(ID) 검색'?>">
                <input type="submit" class="btns gray" id="searchbtn" value="<?php echo get_string('search', 'local_jinoboard'); ?>" />
                <input type="button" class="btns br" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>" />   
            </form>
            <!-- 검색폼 종료 -->
            <span>총 <?php echo $userlist_cnt; ?>건</span>
     <select name="perpage2" class="perpage2">
                <?php 
                foreach($perpage_array as $pa){
                    $selected = '';
                    if($pa == $perpage){
                        $selected = 'selected';
                    }
                ?>
                <option value="<?php echo $pa?>" <?php echo $selected?>><?php echo $pa?><?php echo get_string('viewmore','local_management')?>
                <?php
                }
                ?>
   </select>
  <span><input type="button" class="btns br mg-l10 mg-bt0"  value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/></span>      
    <table class="generaltable table">
        <thead>
            <tr>
                <th>NO</th>
                <th><?php echo get_string('learningstatus:name','local_mypage');?></th>
                <th><a href="#" id="order-id"><?php echo get_string('userid','local_mypage');?>(ID)</a><span class="ic-sort down">sort</span></th>
                <th><a href="#"><?php echo get_string('learningstatus:lastaccess','local_mypage');?></a><span class="ic-sort down" id="order-acce">sort</span></th>
                <th><?php echo get_string('learningstatus:coursetype','local_mypage');?></th>
                <th><a href="#" id="order-date"><?php echo get_string('registrationday','local_mypage');?></a><span class="ic-sort down">sort</span></th>
                <th><?php echo get_string('learningstatus:progress','local_mypage'); ?></th>
                <th><?php echo get_string('learningstatus:completed','local_mypage'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($userlist as $user){
                  $pro = format_lguplus_get_course_progress($id,$user->id);
                ?>
                <tr>
                    <td><?php echo $cnt;?></td>
                    <td><?php echo $user->firstname;?></td>
                    <td><?php echo $user->username;?></td>
                    <td><?php echo $user->lastaccess >0 ? date('Y.m.d',$user->lastaccess) : '-'; ?></td>
                    <td><?php echo $user->forced == 1 ? get_string('learningstatus:paid_course','local_mypage') : get_string('learningstatus:free_course','local_mypage'); ?></td>
                    <td><?php echo date('Y.m.d',$user->timecreated); ?>~<?php echo date('Y.m.d',$user->enddate); ?></td>
                    <td><?php echo $pro['course']->courseprogress.'%' ?></td>
                    <td><?php if(!empty($user->timecompleted)){ echo get_string('learningstatus:iscomplete_2','local_mypage'); } else {  echo $user->enddate > $today ? get_string('learningstatus:iscomplete_0','local_mypage') : get_string('learningstatus:iscomplete_1','local_mypage'); } ?></td>
                </tr>
            <?php
           $cnt--; } ?>
        </tbody>
    </table>

</div>
<?php 
// 페이징 추가하기
$page_params = array();
$page_params['id'] = $id;
$page_params['perpage'] = $perpage;
jinoboard_get_paging_bar($CFG->wwwroot . "/local/lmsdata/learningstatus.php", $page_params, $total_pages, $page);

} else {
    require_once $CFG->dirroot . '/local/lmsdata/lib.php';
    $fields = array(
        'NO',
        get_string('learningstatus:name','local_mypage'),
        get_string('userid','local_mypage'),
        get_string('learningstatus:lastaccess','local_mypage'),
        get_string('learningstatus:coursetype','local_mypage'),
        get_string('registrationday','local_mypage'),
        get_string('learningstatus:progress','local_mypage'),
        get_string('learningstatus:completed','local_mypage')
    );

    $date = date('Y-m-d', time());
    $coursename =  local_lmsdata_get_course_title();
    $filename = $coursename.'_참여자리스트_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col++, $fieldname);
    }

    $row = 1;

    foreach ($userlist as $user) {
        $col = 0;
        $pro = format_lguplus_get_course_progress($id,$user->id);

        $lastaccess = $user->lastaccess >0 ? date('Y.m.d',$user->lastaccess) : '-';
        $pay =  $user->forced == 1 ? get_string('learningstatus:paid_course','local_mypage') : get_string('learningstatus:free_course','local_mypage');
        $ddate =  date('Y.m.d',$user->timecreated).'~'.date('Y.m.d',$user->enddate); 
        if(!empty($user->timecompleted)){ $com = get_string('learningstatus:iscomplete_2','local_mypage'); } else { $com = $user->enddate > $today ? get_string('learningstatus:iscomplete_0','local_mypage') : get_string('learningstatus:iscomplete_1','local_mypage'); } 
        
        $worksheet[0]->write($row, $col++, $cnt--);
        $worksheet[0]->write($row, $col++, $user->firstname);
        $worksheet[0]->write($row, $col++, $user->username);
        $worksheet[0]->write($row, $col++, $lastaccess);
        $worksheet[0]->write($row, $col++, $pay);
        $worksheet[0]->write($row, $col++, $ddate);
        $worksheet[0]->write($row, $col++, $pro['course']->courseprogress.'%');
        $worksheet[0]->write($row, $col++, $com);

        $row++;
    }

    $workbook->close();
    die;
    
}

echo $OUTPUT->footer();
?>
<script>
    function start_datepicker_c(mindate, maxdate) {
    $(".s_date").datepicker({
        dateFormat: "yy.mm.dd",
        minDate: mindate,
        maxDate: maxdate,
        onSelect: function (selectedDate) {
            // 시작일 선택시 연결된 종료일 datepicker의 최소값 설정
            $(this).siblings(".e_date").datepicker("option", "minDate", selectedDate);
        }
    });
}

function end_datepicker_c(mindate, maxdate) {
    $(".e_date").datepicker({
        dateFormat: "yy.mm.dd",
        minDate: mindate,
        maxDate: maxdate,
        onSelect: function (selectedDate) {
            // 종료일 선택시 연결된 시작일 datepicker의 최대값 설정
            $(this).siblings(".s_date").datepicker("option", "maxDate", selectedDate);
        }
    });
}

    function course_list_excel() {
        $("input[name='excel']").val(1);
        $('#frm_content_search').submit();
        $("input[name='excel']").val(0);
    }
    $(document).ready(function () {
        $('#reset').on('click',function(){
            $('.s_date').val('');
            $('.e_date').val('');
            $("input:checkbox[name='iscomp[]']").attr("checked", false);
            $("input:checkbox[name='forced[]']").attr("checked", false);
            $('input[name=search]').val('');
            $("form[id=frm_content_search]").submit();
        });
        
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=frm_content_search]").submit();
        });
        
     $('#order-acce').on('click',function(){
                $('input[name=order]').val(1);
                $("form[id=frm_content_search]").submit();
     });
      $('#order-date').on('click',function(){
                $('input[name=order]').val(2);
                $("form[id=frm_content_search]").submit();
     });
     
    start_datepicker_c('', '');
    end_datepicker_c('', '');
});
    </script>
    