<?php
/**
 * email, sms 발송 cron
 */
define('CLI_SCRIPT', true);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once($CFG->libdir . '/clilib.php');
require_once('../lib.php');

cron_setup_user();

global $DB;

ini_set('display_errors', '1');
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);

echo date('Ymd').' start_cron' . "\n";

//변수를 배열로 가져옴.
$vars = $DB->get_records_menu('lmsdata_sendvariable', array('isused' => 1), '', 'fieldname, varname');
$vartypes = $DB->get_records_menu('lmsdata_sendvariable', array('isused' => 1), '', 'fieldname, fieldtype');
$sendtarget = '학습자';

$count = array('enrol' =>0,'start' => 0, 'end' => 0, 'progress' => 0, 'evalstart' => 0, 'evalend' => 0, 'appealstart' =>0);

//lmsdata_sendtemplate->sendtime 
//2: 연수시작일로부터, 3: 연수종료일로부터

$sql = "select concat(lca.id,'_',ls.id) as id,lca.id as applyid, lc.courseid, lc.scheduleid, lca.userid, lca.status, c.fullname as coursename, lc.classtype, lc.classyear, lc.classnum, lc.enrolmentstart
    ,lc.learningstart, lc.learningend,  lc.evaluationstart, lc.evaluationend, lc.appealstart, lca.price, lca.paymentstatus, u.firstname, u.username, lca.phone, lca.email ,lca.neiscode, lca.locationnumber, lca.id as applicationid, 
    lca.birthday, lc.gradeviewstart, lc.gradeviewend, lc.appealstart,
    ls.type, ls.title, ls.contents, ls.senddate, lca.completionstatus ,ls.sendtarget
from {lmsdata_course_applications} lca
join {user} u on u.id = lca.userid
join {lmsdata_class} lc on lc.courseid = lca.courseid
join {course} c on c.id = lc.courseid
join {lmsdata_course} lco on lco.id = lc.parentcourseid
join {lmsdata_sendtemplate} ls on (ls.classtype = lc.classtype or ls.classtype = 0) ";

$where = "where ls.isused = 1 and (ls.sendtarget = '학습자' or ls.sendtarget = '미이수자' ) ";

$orderby = 'order by lca.id asc';
//1.수강신청일
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.enrolmentstart,'%Y%m%d') = :enrolmentstart and lca.status = :status ";
$params2 = array('sendtime' => 1, 'sendtarget' => $sendtarget);
$params2['enrolmentstart'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];
    $count['enrol'] ++;
    
     $history = new stdClass();
     $history->subject = $data['title'];
     $history->text = $data['contents'];  
     $history->targets = $data['username'];
     $history->done_date = time();
     $history->send_status = '1';
     $historyid = $DB->insert_record('mailsend_history', $history);
     
    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {
            $main = 1;
        }
        email_to_user($user1,$user1, $data['title'],$data['contents']); 
    }
}


//2.교육시작일로 부터 
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.learningstart+(ls.senddate*86400),'%Y%m%d') = :learningstart and lca.status = :status ";
$params2 = array('sendtime' => 2, 'sendtarget' => $sendtarget);
$params2['learningstart'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];    
    $count['start'] ++;
    
     $history = new stdClass();
     $history->subject = $data['title'];
     $history->text = $data['contents'];  
     $history->targets = $data['username'];
     $history->done_date = time();
     $history->send_status = '1';
     $historyid = $DB->insert_record('mailsend_history', $history);
    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {
            $main = 1;
        }
        email_to_user($user1,$user1, $data['title'],$data['contents']); 
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}
//3. 교육종료일로부터 (전)
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.learningend-(ls.senddate*86400),'%Y%m%d') = :learningend and lca.status = :status ";
$params2 = array('sendtime' => 3, 'sendtarget' => $sendtarget);
$params2['learningend'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];    
    $count['end'] ++;
    
     $history = new stdClass();
     $history->subject = $data['title'];
     $history->text = $data['contents'];  
     $history->targets = $data['username'];
     $history->done_date = time();
     $history->send_status = '1';
     $historyid = $DB->insert_record('mailsend_history', $history);
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {

            $main = 1;
        }
        email_to_user($user1,$user1, $data['title'],$data['contents']);         

    }
}

if ($main) {
//    eduhope_balsong_mail_update($seq);
}
//4. 교육종료일로부터 (후
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.learningend+(ls.senddate*86400),'%Y%m%d') = :learningend and lca.status = :status ";
$params2 = array('sendtime' => 4, 'sendtarget' => $sendtarget);
$params2['learningend'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];    
    $count['end'] ++;
    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {
//            $seq = eduhope_balsong_mail_template($data['title'], $data['contents']);
            $main = 1;
        }
        email_to_user($user1,$user1, $data['title'],$data['contents']); 
//        eduhope_balsong_mail_detail($seq, $data['applyid']);
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
//    } else if ($data['type'] == 'sms') {
//        eduhope_balsong_sms($data['contents'], $data['phone']);
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}



//6.평가시작일로부터 
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.evaluationstart+(ls.senddate*86400),'%Y%m%d') = :evaluationstart and lca.status = :status ";
$params2 = array('sendtime' => 5, 'sendtarget' => $sendtarget);
$params2['evaluationstart'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];    
    $count['evalstart'] ++;
    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);

     $history = new stdClass();
     $history->subject = $data['title'];
     $history->text = $data['contents'];  
     $history->targets = $data['username'];
     $history->done_date = time();
     $history->send_status = '1';
     $historyid = $DB->insert_record('mailsend_history', $history);
    
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {
//            $seq = eduhope_balsong_mail_template($data['title'], $data['contents']);
            $main = 1;
        }
        email_to_user($user1,$user1, $data['title'],$data['contents']); 
//        eduhope_balsong_mail_detail($seq, $data['applyid']);
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
//    } else if ($data['type'] == 'sms') {
//        eduhope_balsong_sms($data['contents'], $data['phone']);
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}

//6. 평가종료일로부터
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.evaluationend+(ls.senddate*86400),'%Y%m%d') = :evaluationend and lca.status = :status ";
$params2 = array('sendtime' => 6, 'sendtarget' => $sendtarget);
$params2['evaluationend'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];
    $count['evalend'] ++;
    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    
     $history = new stdClass();
     $history->subject = $data['title'];
     $history->text = $data['contents'];  
     $history->targets = $data['username'];
     $history->done_date = time();
     $history->send_status = '1';
     $historyid = $DB->insert_record('mailsend_history', $history);
     
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        $user1 = $DB->get_record_sql($sql3, $param3);
        if (!$main) {
//            $seq = eduhope_balsong_mail_template($data['title'], $data['contents']);
            $main = 1;
        }
     email_to_user($user1,$user1, $data['title'],$data['contents']);   
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
//    } else if ($data['type'] == 'sms') {
//        eduhope_balsong_sms($data['contents'], $data['phone']);
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}


//7. 이의신청기간 시작일로부터
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.appealstart+(ls.senddate*86400),'%Y%m%d') = :appealstart and lca.status = :status ";
$params2 = array('sendtime' => 7, 'sendtarget' => $sendtarget);
$params2['appealstart'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == '미이수자' && $data['completionstatus'] == 1) {
        continue;
    }
    $param3['userid'] = $data['userid'];
    $count['appealstart'] ++;
    
     $history = new stdClass();
     $history->subject = $data['title'];
     $history->text = $data['contents'];  
     $history->targets = $data['username'];
     $history->done_date = time();
     $history->send_status = '1';
     $historyid = $DB->insert_record('mailsend_history', $history);
     
    //$data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
         $user1 = $DB->get_record_sql($sql3, $param3);
        if (!$main) {
//            $seq = eduhope_balsong_mail_template($data['title'], $data['contents']);
            $main = 1;
        }
       email_to_user($user1,$user1, $data['title'],$data['contents']); 
//        eduhope_balsong_mail_detail($seq, $data['applyid']);
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
//    } else if ($data['type'] == 'sms') {
//        eduhope_balsong_sms($data['contents'], $data['phone']);
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}

//print_r($count);
echo date('Ymd').' end_cron' . "\n";
