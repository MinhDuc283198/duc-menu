<?php

define('CLI_SCRIPT', true);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once($CFG->libdir . '/clilib.php');
require_once('../lib.php');

cron_setup_user();

global $DB;

ini_set('display_errors', '1');
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);

echo 'start_cron'."\n";

//변수를 배열로 가져옴.
$vars = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, varname');
$vartypes = $DB->get_records_menu('lmsdata_sendvariable',array('isused'=>1),'','fieldname, fieldtype');
$sendtarget = 'teacher';

$count = array('start'=>0,'end'=>0);

//lmsdata_sendtemplate->sendtime 
//2: 연수시작일로부터, 3: 연수종료일로부터

$sql = "select concat(lca.id,'_',ls.id) as id, lca.courseid, lca.lmsuserid as userid, c.fullname as coursename, lc.classtype, lc.classyear, lc.classnum, lc.learningstart, lc.learningend, lca.id as applicationid, 
    lc.evaluationstart, lc.evaluationend, u.lastname, u.username, lu.phone1,  u.email, 
    lc.gradeviewstart, lc.gradeviewend, 
    ls.type, ls.title, ls.contents 
from {lmsdata_course_teacher} lca 
join {user} u on u.id = lca.lmsuserid  
join {lmsdata_user} lu on lu.userid = u.id  
join {lmsdata_class} lc on lc.courseid = lca.courseid 
join {course} c on c.id = lc.courseid 
join {lmsdata_course} lco on lco.id = lc.parentcourseid 
join {lmsdata_sendtemplate} ls on (ls.classtype = lc.classtype or ls.classtype = 0) ";
$orderby = 'order by lca.id asc';

$where = 'where ls.isused = 1 ';

//sendtarget이 선생님일 경우

//2.연수시작일
$where2 = "and ls.sendtime = :sendtime and ls.sendtarget = :sendtarget ";
$where2 .= "and from_unixtime(lc.learningstart+(ls.senddate*86400),'%Y%m%d') = :learningstart ";
$params2 = array('sendtime'=>2, 'sendtarget'=>$sendtarget);
$params2['learningstart'] = date('Ymd');

$datas = $DB->get_records_sql($sql.$where.$where2.$orderby, $params2);

$values = array();

foreach($datas as $key => $data){
    $values[$key] = (array)$data;
}

foreach($values as $data){
    $count['start']++;
    $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    //print_object($data);
    echo $data['title'].PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if($data['type']=='email'){
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
    }else if($data['type']=='sms'){
        eduhope_balsong_sms($data['contents'], $data['phone1']);
    }
}

//3. 종료일로부터
$where2 = "and ls.sendtime = :sendtime and ls.sendtarget = :sendtarget ";
$where2 .= "and from_unixtime(lc.learningend+(ls.senddate*86400),'%Y%m%d') = :learningend ";
$params2 = array('sendtime'=>3, 'sendtarget'=>$sendtarget);
$params2['learningend'] = date('Ymd'); 

$datas = $DB->get_records_sql($sql.$where.$where2.$orderby, $params2);

$values = array();

foreach($datas as $key => $data){
    $values[$key] = (array)$data;
}

foreach($values as $data){
    $count['end']++;
    $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    //print_object($data);
    echo $data['title'].PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if($data['type']=='email'){
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
    }else if($data['type']=='sms'){
        eduhope_balsong_sms($data['contents'], $data['phone1']);
    }

}

print_object($count);
echo 'end_cron'."\n";