<?php
/**
 * email, sms 발송 cron
 */
define('CLI_SCRIPT', false);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once($CFG->libdir . '/clilib.php');
require_once('../lib.php');

//cron_setup_user();

global $DB;

ini_set('display_errors', '1');
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);

echo date('Ymd').' start_cron' . "\n";

//변수를 배열로 가져옴.
$vars = $DB->get_records_menu('lmsdata_sendvariable', array('isused' => 1), '', 'fieldname, varname');
$vartypes = $DB->get_records_menu('lmsdata_sendvariable', array('isused' => 1), '', 'fieldname, fieldtype');
$sendtarget = 'student';

$count = array('start' => 0, 'end' => 0, 'progress' => 0, 'evalstart' => 0, 'evalend' => 0);

//lmsdata_sendtemplate->sendtime 
//2: 연수시작일로부터, 3: 연수종료일로부터

$sql = "select concat(lca.id,'_',ls.id) as id,lca.id as applyid, lc.courseid, lc.scheduleid, lca.userid, lca.status, c.fullname as coursename, lc.classtype, lc.classyear, lc.classnum, lc.learningstart, lc.learningend, 
    lc.evaluationstart, lc.evaluationend, lca.price, lca.paymentstatus, u.lastname, u.username, lca.phone, lca.email ,lca.neiscode, lca.locationnumber, lca.id as applicationid, 
    lca.birthday, lc.gradeviewstart, lc.gradeviewend, lc.appealstart,
    ls.type, ls.title, ls.contents, ls.senddate, lca.completionstatus ,ls.sendtarget, lsc.consignmentplace, lc.scheduleid    
from {lmsdata_course_applications} lca 
join {user} u on u.id = lca.userid 
join {lmsdata_class} lc on lc.id = lca.courseid 
join {course} c on c.id = lc.courseid 
join {lmsdata_course} lco on lco.id = lc.parentcourseid 
join {lmsdata_sendtemplate} ls on (ls.classtype = lc.classtype or ls.classtype = 0) 
join {lmsdata_schedule} lsc on lsc.id = lc.scheduleid ";
$orderby = 'order by lca.id asc';

$where = "where ls.isused = 1 and (ls.sendtarget = 'student' or ls.sendtarget = 'nocomplete' ) ";

//sendtarget이 학생일 경우
//2.연수시작일로 부터 
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.learningstart+(ls.senddate*86400),'%Y%m%d') = :learningstart and lca.status = :status ";
$params2 = array('sendtime' => 2, 'sendtarget' => $sendtarget);
//$params2['learningstart'] = date('Ymd');
$params2['learningstart'] = 20180626;
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == 'nocomplete' && $data['completionstatus'] == 1) {
        continue;
    }
    $count['start'] ++;
    

    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {
            $seq = eduhope_balsong_mail_template($data['title'], $data['contents']);
            $main = 1;
        }
        eduhope_balsong_mail_detail($seq, $data['applyid']);
        eduhope_balsong_mail($data['title'], $data['contents'], $data);
    } else if ($data['type'] == 'sms') {
        $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
//        eduhope_balsong_sms($data['contents'], $data['phone']);
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}

//3. 연수종료일로부터
$where2 = "and ls.sendtime = :sendtime ";
$where2 .= "and from_unixtime(lc.learningend+(ls.senddate*86400),'%Y%m%d') = :learningend and lca.status = :status ";
$params2 = array('sendtime' => 3, 'sendtarget' => $sendtarget);
$params2['learningend'] = date('Ymd');
$params2['status'] = 'apply';
$main = 0;

$datas = $DB->get_records_sql($sql . $where . $where2 . $orderby, $params2);

$values = array();

foreach ($datas as $key => $data) {
    $values[$key] = (array) $data;
}

foreach ($values as $data) {
    if ($data['sendtarget'] == 'nocomplete' && $data['completionstatus'] == 1) {
        continue;
    }
    $count['end'] ++;
    $data = local_lmsdata_sendvarsetting($vars, $vartypes, $data);
    echo $data['title'] . PHP_EOL;
    //여기서 SMS, 메일 보내기 함수 실행
    if ($data['type'] == 'email') {
        if (!$main) {
//            $seq = eduhope_balsong_mail_template($data['title'], $data['contents']);
            $main = 1;
        }
//        eduhope_balsong_mail_detail($seq, $data['applyid']);
//        eduhope_balsong_mail($data['title'], $data['contents'], $data);
    } else if ($data['type'] == 'sms') {
//        eduhope_balsong_sms($data['contents'], $data['phone']);
    }
}
if ($main) {
//    eduhope_balsong_mail_update($seq);
}


print_r($count);
echo date('Ymd').' end_cron' . "\n";
