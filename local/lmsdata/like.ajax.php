<?php
require_once(__DIR__ . '/../../config.php');
global $DB,$USER;
$userid = optional_param('userid',0, PARAM_INT);
$classid = optional_param('classid',0,PARAM_INT);
    
if(!$userid){
    $userid = $USER->id;
}

$data = new stdClass();
$chk = $DB->get_record('lmsdata_like',array('userid'=>$userid,'classid'=>$classid));
if($chk){
    $DB->delete_records('lmsdata_like',array('userid'=>$userid,'classid'=>$classid));
    $value->value = -1;
} else {
    $data->userid = $userid;
    $data->classid = $classid;
    $data->timecreated = time();
    $value->value = $DB->insert_record('lmsdata_like',$data);
}
$value->score = $DB->count_records('lmsdata_like',array('classid'=>$classid));
@header('Content-type: application/json; charset=utf-8');
echo json_encode($value);
?>

