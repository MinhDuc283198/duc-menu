<?php

require('../../config.php');

use local_lmsdata\external_setup; 

$DBINFO->dbtype    = 'mariadb';
$DBINFO->dblibrary = 'native';
$DBINFO->dbhost    = 'oklass.kr';
$DBINFO->dbname    = 'moodle';
$DBINFO->dbuser    = 'moodle';
$DBINFO->dbpass    = 'lms00';
$DBINFO->prefix    = 'm_';
$DBINFO->dboptions = array (
  'dbpersist' => false,
  'dbport' => '3306',
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_unicode_ci',
);

new external_setup($EDB, $DBINFO); 

//$test = $EDB->get_columns('user_view_test');

//$course = $EDB->get_record_sql('course', array('course'=>1));

//print_object($user);
