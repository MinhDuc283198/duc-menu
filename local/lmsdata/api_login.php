<?php
/**
 * 로그인 대체 페이지(API로그인)
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

if(empty($USER->id) || $USER->id == 1){
    if(isset($_COOKIE['authToken'])){

        $output = eduhope_api_login($_COOKIE['authToken'], $_SERVER['REMOTE_ADDR']);

        if($output->dataResult == '00' && $output->taskResult == '00'){
            $user = $DB->get_record('user', array('username' => $output->returnData[0]->memberId)); 
            if ($user) {
                $user->timemodified = time();
                $user->email = trim($output->returnData[0]->email);
                $user->firstname ='&nbsp;';
                $user->lastname = trim($output->returnData[0]->memberName);
                $user->phone2 = $output->returnData[0]->handPhone;
                if($output->returnData[0]->informationOpenUseYn == 'Y'){
                    $user->mailedisplay = 1;
                } else {
                    $user->mailedisplay = 0;
                }

                $DB->update_record('user', $user);

                $lmsuser = $DB->get_record('lmsdata_user', array('userid' => $user->id));

                $birthday = $output->returnData[0]->memberBirthDate;
                if(mb_strlen($birthday) != 8){
                    $yy = (int)substr($birthday, 0, 2);
                    if($yy <= 45){
                        $birthday = '20'.$birthday;
                    } else {
                        $birthday = '19'.$birthday;
                    }
                }

                $lmsuser->neiscode = $output->returnData[0]->neisCode;
                $lmsuser->nickname = $output->returnData[0]->memberPenName;
                $lmsuser->usertypecode = $output->returnData[0]->memberTypeCode;
                $lmsuser->sex = $output->returnData[0]->sexCode;
                $lmsuser->birthday = $birthday;
                $lmsuser->phone1 = $output->returnData[0]->handPhone;
                if($output->returnData[0]->smsSendUseYn == 'Y'){
                    $lmsuser->allowsms = 1;
                } else { 
                    $lmsuser->allowsms = 0;
                }
                if($output->returnData[0]->emailUseYn == 'Y'){
                    $lmsuser->allowemail = 1;
                } else { 
                    $lmsuser->allowemail = 0;
                }
                if($output->returnData[0]->informationOpenUseYn == 'Y'){
                    $lmsuser->allowprofile = 1;
                } else { 
                    $lmsuser->allowprofile = 0;
                }
                $lmsuser->school = $output->returnData[0]->schoolCodeSeq;
                $lmsuser->statuscode = $output->returnData[0]->stateCode;
                $lmsuser->joinstatus = $output->returnData[0]->joinStateCode;
                $lmsuser->zipcode = $output->returnData[0]->zipcode;
                $lmsuser->address = $output->returnData[0]->address;
                $lmsuser->address_detail = $output->returnData[0]->addressDetail;
                $lmsuser->subject = $output->returnData[0]->subjectCode;

                $DB->update_record('lmsdata_user', $lmsuser);

                complete_user_login($user);
                unset($SESSION->loginerrormsg);

            } else {
                $usernew = new stdClass();
                $usernew->auth = 'manual';
                $usernew->timemodified = time();
                $usernew->timecreated = time();
                $usernew->lang = "ko";
                $usernew->password = hash_internal_user_password(trim($output->returnData[0]->memberPassword));
                $usernew->email = trim($output->returnData[0]->email);
                $usernew->firstname ='&nbsp;';
                $usernew->lastname = trim($output->returnData[0]->memberName);
                $usernew->phone2 = $output->returnData[0]->handPhone;
                if($output->returnData[0]->informationOpenUseYn == 'Y'){
                    $usernew->mailedisplay = 1;
                } else {
                    $usernew->mailedisplay = 0;
                }
                $usernew->suspended = 0;
                $usernew->username = trim(core_text::strtolower($frm->username));
                $usernew->mnethostid = $CFG->mnet_localhost_id; // Always local user.
                $usernew->confirmed  = 1;

                $authplugin = get_auth_plugin($usernew->auth);
                $usernew->id = user_create_user($usernew, false, false);

                $lmsuser = new stdClass();
                $lmsuser->userid = $usernew->id;
                $lmsuser->userno = $output->returnData[0]->memberSeq;
                $lmsuser->neiscode = $output->returnData[0]->neisCode;
                $lmsuser->nickname = $output->returnData[0]->memberPenName;
                $lmsuser->usergroup = 'rs';
                $lmsuser->usertypecode = $output->returnData[0]->memberTypeCode;
                $lmsuser->sex = $output->returnData[0]->sexCode;
                
                $birthday = $output->returnData[0]->memberBirthDate;
                if(mb_strlen($birthday) != 8){
                    $yy = (int)substr($birthday, 0, 2);
                    if($yy <= 45){
                        $birthday = '20'.$birthday;
                    } else {
                        $birthday = '19'.$birthday;
                    }
                }
                
                $lmsuser->birthday = $birthday;
                $lmsuser->phone1 = $output->returnData[0]->handPhone;
                if($output->returnData[0]->smsSendUseYn == 'Y'){
                    $lmsuser->allowsms = 1;
                } else { 
                    $lmsuser->allowsms = 0;
                }
                if($output->returnData[0]->emailUseYn == 'Y'){
                    $lmsuser->allowemail = 1;
                } else { 
                    $lmsuser->allowemail = 0;
                }
                if($output->returnData[0]->informationOpenUseYn == 'Y'){
                    $lmsuser->allowprofile = 1;
                } else { 
                    $lmsuser->allowprofile = 0;
                }
                $lmsuser->school = $output->returnData[0]->schoolCodeSeq;
                $lmsuser->statuscode = $output->returnData[0]->stateCode;
                $lmsuser->joinstatus = $output->returnData[0]->joinStateCode;
                $lmsuser->zipcode = $output->returnData[0]->zipcode;
                $lmsuser->address = $output->returnData[0]->address;
                $lmsuser->address_detail = $output->returnData[0]->addressDetail;
                $lmsuser->subject = $output->returnData[0]->subjectCode;

                $lmsdatauser = $DB->insert_record('lmsdata_user', $lmsuser);

                // Update preferences.
                useredit_update_user_preference($usernew);

                // Save custom profile fields data.
                profile_save_data($usernew);

                // Reload from db.
                $usernew = $DB->get_record('user', array('id' => $usernew->id));

                // Trigger update/create event, after all fields are stored.
                \core\event\user_created::create_from_userid($usernew->id)->trigger();

                complete_user_login($usernew);
                unset($SESSION->loginerrormsg);

                // test the session actually worqks by redirecting to self

            }
            echo "<script>location.href='".$CFG->wwwroot."/index.php'</script>";
        } else {
            switch($output->errData){
                case 1:
                    $errormsg = '길이오류';
                    break;
                case 2:
                    $errormsg = 'NULL데이터 오류';
                    break;
                case 3:
                    $errormsg = '데이터 타입 오류';
                    break;
            }
            echo "<script>alert('".$errormsg."')</script>";
            $errorcode = 3;
        }
    }
}
redirect($CFG->wwwroot); 