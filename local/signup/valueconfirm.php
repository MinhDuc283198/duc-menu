<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$type = optional_param('type', null, PARAM_RAW);
$value = optional_param('value', null, PARAM_RAW);

$typelang = get_string($type,"local_signup");
$returndata = new stdClass();
$returndata ->status = 1;
$returndata ->text = get_string('available', 'local_signup',array("type"=>$typelang));
if($type == 'email'){
    //$value = preg_replace("/\s+/","",strtolower($value)); 
    $value = strtolower($value); 
    if( $value != trim($value) || strpos($value,' ')){
        $returndata ->status = 0;
        $returndata ->text = get_string('blankcheck','local_signup');
    }else{
        $user = $DB ->get_record_sql('select * from {user} where  email = ? or username = ? ',array('email'=>$value,'username'=>$value));
        if($user){
            $returndata ->status = 0;
            $returndata ->text = get_string('notavailable', 'local_signup',array("type"=>$typelang));
        }
    }
}else if($type =='joinpassword'){
    if( $value != trim($value) || strpos($value,' ')){
        $returndata ->status = 0;
        $returndata ->text = get_string('blankcheck','local_signup');
    }else{
        if(!check_password_policy($value, $errmsg)){
            $returndata ->status = 0;
            $returndata ->text = get_string('passvalcheck','local_signup');
        }
    }
}

echo json_encode($returndata);



