<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$type = optional_param('type', 0, PARAM_RAW);
$check = optional_param('check', 0, PARAM_INT);
$postp = optional_param('postp', 0, PARAM_RAW);
$postp = preg_replace("/\s+/","",$postp);
$poste = optional_param('poste', '', PARAM_RAW);
$poste = preg_replace("/\s+/","",strtolower($poste));
$postn = optional_param('postn', '', PARAM_RAW);
$postn = preg_replace("/\s+/","",strtolower($postn));
$auth = optional_param('auth', '', PARAM_RAW);
$username = optional_param('username', '', PARAM_RAW);
$uid = optional_param('uid', '', PARAM_RAW);
$uid = preg_replace("/\s+/","",strtolower($uid));
$code = optional_param('code', '', PARAM_RAW);
$lang = current_language();
if($lang != 'ko'){
    $tlang = $lang;
}else{
    $tlang = "";
}
if ($check == 1) {
    $terms1 =  $DB->get_record_sql("select content$tlang content from {notice_board} where category = ? and delflag = ? and fbstatus = ? order by id desc",array(1,0,1)) ;
    $terms2 =  $DB->get_record_sql("select content from {notice_board} where category = ? and delflag = ? and fbstatus = ? order by id desc",array(1,0,2)) ;
    $terms3 =  $DB->get_record_sql("select content$tlang content from {notice_board} where category = ? and delflag = ? and fbstatus = ? order by id desc",array(1,0,0)) ;
    ?>
	<label class="custom-ck">
		<input type="checkbox" id="agreeAll"/>
    	<span><?php echo get_string("c_agreeAllAll","local_job")?></span>
    </label>

	<div class="login-tit">
	<label class="custom-ck">
		<input type="checkbox" id="agree1"/>
    	<span><?php echo get_string("c_TermsofUse","local_job")?>(<a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=3', '서비스이용방침', 'resizable,height=800,width=800,scrollbars=yes'); return false;" ><?php echo get_string("allview","local_signup")?></a>)</span>
    </label>
    </div>
    <div class="scrl-box"><?php echo $terms1->content?></div>

	<div class="login-tit">
    <label class="custom-ck">
		<input type="checkbox" id="agree2"/>
    	<span><?php echo get_string("c_regulations","local_job")?>(<a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=3', '이용약관', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string("allview","local_signup")?></a>)</span>
    </label>
    </div>
    <div class="scrl-box"><?php echo $terms2->content?></div>

    <div class="login-tit">
    <label class="custom-ck">
		<input type="checkbox" id="agree3"/>
    	<span><?php echo get_string("c_Privacypolicy","local_job")?>(<a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=3', '개인정보취급방침', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string("allview","local_signup")?></a>)</span>
    </label>
    </div>
    <div class="scrl-box"><?php echo $terms3->content?></div>


    <p class="info"><?php echo get_string("agreeallterm","local_signup")?></p>

    <div class="text-center btn-area">
        <input id="checkAgreement" type="button" value="<?php echo get_string("agreeterm","local_signup")?>" class="btns point big" onclick="checkAgreement()"/>
    </div>
    <script type="text/javascript">
		function checkAgreement(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked"))
				change_tab(2);
		}

		$("#agreeAll").click(function(){
			if($('#agreeAll').is(":checked")){
				$('#agree1').prop('checked', true);
				$('#agree2').prop('checked', true);
				$('#agree3').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agree1').prop('checked', false);
				$('#agree2').prop('checked', false);
				$('#agree3').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}

		});

		$('#agree1').click(function(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked")){
				$('#agreeAll').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agreeAll').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}
		});
		$('#agree2').click(function(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked")){
				$('#agreeAll').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agreeAll').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}
		});
		$('#agree3').click(function(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked")){
				$('#agreeAll').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agreeAll').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}
		});
		$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
    </script>

    <?php
} else if ($check == 2) {
    ?>
    <form class="email-bx" action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post">
        <p>
            <input type="text" id="email" name="email" placeholder="<?php echo get_string("writeemail", "local_signup") ?>" />
            <input type="hidden" id="emailconfirm" name="emailconfirm" />
            <input type="button" class="btns gray" onclick="check_val('email')" value="<?php echo get_string("duplicatecheck","local_signup")?>" />
            <span class="emailwarning warning"></span>
        </p>
        <p>
            <input type="password" id="joinpassword" name="joinpassword" title="비밀번호" placeholder="<?php echo get_string("writepassword", "local_signup") ?>" />
            <input type="hidden" id="joinpasswordconfirm" name="joinpasswordconfirm" />
            <span class="joinpasswordwarning warning"><?php echo get_string('passvalcheck','local_signup'); ?></span>
        </p>
        <p>
            <input type="password" id="joinpassword2" name="joinpassword2" title="비밀번호" placeholder="<?php echo get_string("checkpassword", "local_signup") ?>" />
            <span class="joinpassword2warning warning"></span>
        </p>
        <div class="btns-area text-center">
            <input type="button" value="<?php echo get_string("startingemail","local_signup")?>" class="btns br joinemail" />
        </div>

    </form>

    <div class="sns-area">
        <a href="/local/signup/oauth2login.php?providername=facebook" class="ic-fb">
            <span class="ic"><img src="/theme/oklassedu/pix/images//icon_facebook_big.png"  alt="facebook" /></span>
            <span><?php echo get_string('facebooklogin', 'theme_oklassedu') ?></span>
        </a>
        <a href="/local/signup/oauth2login.php?providername=google" class="ic-gl">
            <span class="ic"><img src="/theme/oklassedu/pix/images//icon_google.png" alt="google" /></span>
            <span><?php echo get_string('googlelogin', 'theme_oklassedu') ?></span>
        </a>
    </div>
    <?php
} else {
    ?>
    <!-- contents -->
    <p class="ft-dot"><?php echo get_string("necessaryinfo", "local_signup") ?></p>
    <form class="email-bx join" action="/local/signup/signup_summit.php" method="POST" onsubmit="return chk_value()">
        <input type="hidden" name="password" value="<?php echo $postp ?>" readonly/>
        <input type="hidden" name="snsid" value="<?php echo $uid ? $uid : $poste; ?>" readonly/>
        <input type="hidden" name="username" value="<?php echo $username; ?>" readonly/>
        <input type="hidden" name="code" value="<?php echo $code; ?>" readonly/>
        <?php if($auth){ echo '<input type="hidden" name="auth" value="'.$auth.'" readonly/>'; } ?>
        <p>
            <strong><?php echo get_string("name", "local_signup"); ?></strong>
            <input type="text" name="firstname" value="<?php echo $postn; ?>" placeholder="<?php echo get_string("writename", "local_signup"); ?>"/>
        </p>
        <p>
            <strong><?php echo get_string("birth", "local_signup"); ?></strong>
            <select name="year" id="year">
                <?php
                foreach (range(date('Y'), 1960) as $year) {
                    echo '<option value="' . $year . '">' . $year . '</option>';
                }
                ?>
            </select>
            <span class="dash">-</span>
            <select name="month" id="month">
                <?php
                for ($m = 1; $m <= 12; $m++) {
                    if (strlen($m) == 1)
                        $m = "0" . $m;
                    if ($m == $mo) {
                        $date_month .= "<option value='$m' selected>$m</option>\n";
                    } else {
                        $date_month .= "<option value='$m'>$m</option>\n";
                    }
                }
                echo $date_month;
                ?>
            </select>
            <span class="dash">-</span>
            <select name="days" id="days">
                <?php
                for ($d = 1; $d <= 31; $d++) {
                    if (strlen($d) == 1)
                        $d = "0" . $d;
                    if ($d == $da) {
                        $date_day .= "<option value='$d' selected>$d</option>\n";
                    } else {
                        $date_day .= "<option value='$d'>$d</option>\n";
                    }
                }
                echo $date_day;
                ?>
            </select>
        </p>
        <p>
            <strong><?php echo get_string("gender", "local_signup"); ?></strong>
            <select name="sex" id="sex">
                <option value='1' ><?php echo get_string("male", "local_signup"); ?></option>
                <option value='2' ><?php echo get_string("female", "local_signup"); ?></option>
            </select>
        </p>
        <p>
            <strong><?php echo get_string("email", "local_signup"); ?></strong>
            <input type="text" name="email" placeholder="<?php echo get_string("writeemail", "local_signup"); ?>" value="<?php echo $poste; ?>" <?php if(!$auth){ echo 'readonly'; } ?>/>
        </p>
        <p class="phone">
            <strong><?php echo get_string("phone", "local_signup"); ?></strong>
            <input type="text"  id="" name="phone1" class="w-auto"  placeholder="<?php echo get_string('phone_txt', 'local_my')?>">
            <!--<span class="warning">휴대폰 번호 부적합 사유 안내</span>-->
        </p>

        <div class="text-center btn-area">
            <input type="submit" value="<?php echo get_string("signup", "local_signup") ?>" class="btns point big " />
        </div>
    </form>
    <!-- contents -->

    <?php
}
?>
<script>
    $(document).ready(function () {
        $("input[name=phone1]").on("keyup", function() {
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });
    });
    function chk_value(){

        var firstname = $('input[name=firstname]').val();
        var phone1 = $('input[name=phone1]').val();
        if(firstname==''||phone1==''){
            alert('<?php echo get_string('writeall','local_signup'); ?>');
            return false;
        }

        var year = $("#year").val();
        var month = $("#month").val();
        var days = $("#days").val();
        var date = new Date();
        var today = new Date();
        date.setFullYear(year, month-1, days);

        if(today.getTime() < date.getTime()){
            alert('<?php echo get_string('overdate','local_signup'); ?>');
            return false;
        }
        return true;
    }
</script>