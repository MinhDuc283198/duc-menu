<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');
$firstname = optional_param('firstname', 0, PARAM_RAW);
$phone1 = optional_param('phone1', 0, PARAM_RAW);
$phone2 = optional_param('year', 0, PARAM_RAW) . "-" . optional_param('month', 0, PARAM_RAW) . "-" . optional_param('days', 0, PARAM_RAW);
$userdata = $DB->get_record("user", array('firstname' => $firstname, 'phone1' => $phone1, 'phone2' => $phone2, 'confirmed' => 1, 'deleted' => 0));
echo $OUTPUT->header();
?>  

<div class="login-bx">
    <div class="bg">
        <div class="center-tit"><?php echo get_string("emailfindtitle","local_signup")?></div>
        <p class="center-txt"><?php echo get_string("emailfindtext","local_signup")?></p>
    </div>
    <div class="fd-u-info">
        <?php if ($userdata) { ?>
            <div>
                <strong><?php echo get_string("username","core")?></strong>
                <span><img src="/theme/oklassedu/pix/images/icon_mail.png" alt="" /><?php echo $userdata->email ?></span>
            </div>
            <div>
                <strong><?php echo get_string("password","core")?></strong>
                <a href="#" onclick="send_email(<?php echo $userdata->id?>)"class="btns br"><?php echo get_string("recivetemppass","local_signup")?></a>
                <p class="warning"><?php echo get_string("chanepass","local_signup")?></p>
            </div>
        <?php } else { ?>
            <div>  
                <strong><?php echo get_string("username","core")?></strong>
                <a href="<?php echo $CFG->wwwroot."/local/signup/findaccount.php"?>" class="btns br"><?php echo get_string("notaccount","local_signup")?></a>
            </div>
        <?php } ?>
    </div>

</div>
<script>
    function send_email(userid) {
        utils.loading.show();
        $.ajax({
            url: "/local/signup/send_tempmail.php",
            type: 'post',
            dataType:'json',
            data: {userid:userid},
            success: function (result) {
                utils.loading.hide();
                alert(result.text);
                location.href = "<?php echo $CFG->wwwroot.'/login/index.php' ?>";
            }
        });
    }
</script>

<?php
echo $OUTPUT->footer();
?>


