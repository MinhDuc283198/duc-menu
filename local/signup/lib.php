<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authentication Plugin: Email Authentication
 *
 * @author Martin Dougiamas
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth_email
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/authlib.php');

/**
 * Email authentication plugin.
 */
class local_auth_plugin_email extends auth_plugin_base
{

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->authtype = 'email';
        $this->config = get_config('auth/email');
    }

    /**
     * Old syntax of class constructor. Deprecated in PHP7.
     *
     * @deprecated since Moodle 3.1
     */
    public function auth_plugin_email()
    {
        debugging('Use of class name as constructor is deprecated', DEBUG_DEVELOPER);
        self::__construct();
    }

    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password)
    {
        global $CFG, $DB;
        if ($user = $DB->get_record('user', array('username' => $username, 'mnethostid' => $CFG->mnet_localhost_id))) {
            return validate_internal_user_password($user, $password);
        }
        return false;
    }

    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object  (with system magic quotes)
     * @param  string  $newpassword Plaintext password (with system magic quotes)
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword)
    {
        $user = get_complete_user_data('id', $user->id);
        // This will also update the stored hash to the latest algorithm
        // if the existing hash is using an out-of-date algorithm (or the
        // legacy md5 algorithm).
        update_internal_user_password($user, $newpassword);
        return $this->send_updatepassword_email($user, $newpassword);
    }

    function can_signup()
    {
        return true;
    }

    /**
     * Sign up a new user ready for confirmation.
     * Password is passed in plaintext.
     *
     * @param object $user new user object
     * @param boolean $notify print notice with link and terminate
     */
    function user_signup($user, $notify = true, $auth = '', $email = '')
    {
        global $CFG, $DB;
        require_once($CFG->dirroot . '/user/profile/lib.php');
        require_once($CFG->dirroot . '/user/lib.php');

        $lmsdatauser = new stdClass();
        $plainpassword = $user->password;

        if (empty($user->calendartype)) {
            $user->calendartype = $CFG->calendartype;
        }

        if ($auth == '') {
            $user->password = hash_internal_user_password($plainpassword);
        }

        $user->id = user_create_user($user, false, false);
        $lmsdatauser->userid = $user->id;
        $lmsdatauser->usergroup = $user->usergroup ? $user->usergroup : 'rs';
        $lmsdatauser->birthday = $user->phone2;
        $lmsdatauser->phone1 = $user->phone1;
        $lmsdatauser->joinstatus = $user->joinstatus ? $user->joinstatus : 'e';
        $lmsdatauser->sns = $auth;
        $lmsdatauser->email2 = $email ? $email : $user->email;
        $lmsdatauser->sex = $user->sex;

        $DB->insert_record("lmsdata_user", $lmsdatauser);
        user_add_password_history($user->id, $plainpassword);
//
//        // Save any custom profile field information.
        profile_save_data($user);
//
//        // Trigger event.
        \core\event\user_created::create_from_userid($user->id)->trigger();
        if($user->secret != "CACC") {
            if (!$this->send_confirmation_email($user)) {
                print_error('auth_emailnoemail', 'auth_email');
            }
        }

        return $user->id;
    }

    function send_updatepassword_email($user, $temppass)
    {
        global $CFG, $DB;

        $site = get_site();
        $supportuser = core_user::get_support_user();

        $data = new stdClass();
        $data->firstname =  $user->firstname;
        $data->sitename = format_string($site->fullname);
        $data->admin = generate_email_signoff();

        $subject = get_string('changepasstitle', 'local_signup', $data);

        $username = urlencode($user->firstname);
        $username = str_replace('.', '%2E', $username); // Prevent problems with trailing dots.
        $data->firstname = $user->firstname;
        $data->username = $user->username;

        $redirecttosite = optional_param('redirecttosite', '', PARAM_RAW);
        if ($redirecttosite == 'jobs') {
            $employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id 
                WHERE ad.user_id = :id";
            $employer = $DB->get_record_sql($employer_sql, array('id' => $user->id));

            if ($employer == null) {
                $data->link = $CFG->wwwroot . '/local/job/auth/login.php';
            } else {
                $data->link = $CFG->wwwroot . '/local/job/employer/login.php';
            }
        } else {
            $data->link = $CFG->wwwroot . '/login/index.php';
        }

        $data->temppass = $temppass;
        $message = get_string('changepasstext', 'local_signup', $data);
        $messagehtml = text_to_html(get_string('changepasstext', 'local_signup', $data), false, false, true);

        $user->mailformat = 1;  // Always send HTML version as well.
        // Directly email rather than using the messaging system to ensure its not routed to a popup or jabber.
        return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
    }
    function send_confirmation_email($user)
    {
        global $CFG, $DB, $VISANG;

        $site = get_site();
        $supportuser = core_user::get_support_user();

        $data = new stdClass();
        $data->firstname = $user->firstname;
        //        $data->sitename = format_string($site->fullname);
        $data->admin = generate_email_signoff();

        $subject = get_string('emailconfirmationsubject', '', format_string($site->fullname));

        $username = urlencode($user->username);
        $username = str_replace('.', '%2E', $username); // Prevent problems with trailing dots.
        //         if($CFG->wwwroot == 'https://www.masterkorean.vn'){
        //             $basepath = 'https://www.masterkorean.vn/local/signup';
        //             $data->imgurl = 'theme/oklassedu/pix/images/hd_logo.png';
        //             $data->courl = 'https://www.masterkorean.vn/local/jinoboard/index.php?type=1';
        //             $data->sitename = 'Master Korean';
        //         }else{
        //             $basepath = 'https://job.masterkorean.vn/local/job/auth';
        // //            $basepath = 'http://open.jinotech.com:13080/local/signup';
        //             $data->imgurl = 'theme/oklassedu/pix/images/hd_logo_job.png';
        //             $data->courl = 'https://job.masterkorean.vn/local/job/my/qna/index.php?type=8';
        //              $data->sitename = 'Master Korean jobs';
        //         }
        if (isset($VISANG)) {
            $wwwroot = $VISANG->wwwroot;
            $basepath = $VISANG->wwwroot . '/auth';
            $data->imgurl = 'theme/oklassedu/pix/images/hd_logo_job.png';
            $data->courl = $VISANG->wwwroot . '/my/qna/index.php?type=8';
            $data->sitename = 'Master Korean jobs';
        } else {
            $wwwroot = $CFG->wwwroot;
            $basepath = $CFG->wwwroot . '/local/signup';
            $data->imgurl = 'theme/oklassedu/pix/images/hd_logo.png';
            $data->courl = $CFG->wwwroot . '/local/jinoboard/index.php?type=1';
            $data->sitename = 'Master Korean';
        }
        $data->wwwroot = $wwwroot;
        $data->link = $basepath . '/confirm.php?data=' . $user->secret . '/' . $username;
        $data->timecreated = date('Y.m.d', $user->timecreated);
        $message = get_string('emailconfirmation', 'local_signup', $data);
        $messagehtml = text_to_html(get_string('emailconfirmation', 'local_signup', $data), false, false, true);

        $user->mailformat = 1;  // Always send HTML version as well.
        $user->email = $DB->get_field('lmsdata_user', 'email2', array('userid' => $user->id));
        // Directly email rather than using the messaging system to ensure its not routed to a popup or jabber.
        return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
    }

    /**
     * Returns true if plugin allows confirming of new users.
     *
     * @return bool
     */
    function can_confirm()
    {
        return true;
    }

    /**
     * Confirm the new user as registered.
     *
     * @param string $username
     * @param string $confirmsecret
     */
    function user_confirm($username, $confirmsecret)
    {
        global $DB;
        $user = get_complete_user_data('username', $username);

        if (!empty($user)) {
            if ($user->auth != 'googleoauth2' && $user->auth != $this->authtype) {
                return AUTH_CONFIRM_ERROR;
            } else if ($user->secret == $confirmsecret && $user->confirmed) {
                return AUTH_CONFIRM_ALREADY;
            } else if ($user->secret == $confirmsecret) {   // They have provided the secret key to get in
                $DB->set_field("user", "confirmed", 1, array("id" => $user->id));
                return AUTH_CONFIRM_OK;
            }
        } else {
            return AUTH_CONFIRM_ERROR;
        }
    }

    function prevent_local_passwords()
    {
        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal()
    {
        return true;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password()
    {
        return true;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url()
    {
        return null; // use default internal method
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password()
    {
        return true;
    }

    /**
     * Returns true if plugin can be manually set.
     *
     * @return bool
     */
    function can_be_manually_set()
    {
        return true;
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields)
    {
        include "config.html";
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config)
    {
        // set to defaults if undefined
        if (!isset($config->recaptcha)) {
            $config->recaptcha = false;
        }

        // save settings
        set_config('recaptcha', $config->recaptcha, 'auth/email');
        return true;
    }

    /**
     * Returns whether or not the captcha element is enabled.
     * @return bool
     */
    function is_captcha_enabled()
    {
        return get_config("auth/{$this->authtype}", 'recaptcha');
    }
}
