<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
if (isloggedin()) {
    $token = optional_param('token', '', PARAM_RAW);
    $device= optional_param('device', '', PARAM_RAW);
    $platform= optional_param('platform', '', PARAM_RAW);
    $utokendata = $DB->get_record('lmsdata_pushtoken', array('username' => $USER->id));
    if ($utokendata) {
        $utokendata->token = $token;
        $utokendata->deviceid = $device;
        $utokendata->deviceinform = $platform;
        $utokendata->timeupdated=time();
        $DB->update_record('lmsdata_pushtoken',$utokendata);
    } else {
        $utokendata = new stdClass();
        $utokendata->username = $USER->id;
        $utokendata->token = $token;
        $utokendata->deviceid = $device;
        $utokendata->deviceinform = $platform;
        $utokendata->timecreated=time();
        $DB->insert_record('lmsdata_pushtoken',$utokendata);
    }
}