<?php

$string['emailconfirmation'] = "
    <!DOCTYPE html>
<html lang='ko'>
    <head>
        <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>Document</title>
    </head>
    <body>
        <link href='https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese'  rel='stylesheet' />
        <div style='border: 1px solid #ddd;padding: 50px 30px 0;width: 600px;text-align: center;'>
            <p style='text-align: center;margin: 0 0 10px;'>
                <a href=" . '{$a->wwwroot}' . " target='_blank'><img src=" . 'https://www.masterkorean.vn/{$a->imgurl}' . " alt='master Korean' width='150' /></a>
            </p>
            <div style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;font-size: 25px;font-weight: bold;text-align: center;margin-bottom: 10px;'>" . '{$a->sitename}' . " Sign up is complete.</div>
            <p style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;color: #485cc7;text-align: center;font-size: 16px;margin-bottom: 15px;border: 1px solid #485cc7;width: 200px;margin: 25px auto;padding: 10px;'>Joined : " . '{$a->timecreated}' . " </p>
            <p style='font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;font-size: 20px;text-align: center;'>Click the email verification button below<br /> Please complete the certification.</p>
            <a href=" . '{$a->link}' . " style='display: inline-block;padding: 0 25px;height: 50px;line-height: 48px;background: #00b5e2;text-decoration: none;font-family: Noto Sans KR, Noto Sans SC, 맑은고딕,sans-serif;color: #fff;'>Email Authentication</a>
            <div style='background: #f4f4f4;font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;width: auto; margin: 10px 0; padding: 20px 30px;box-sizing: border-box;border-top: 1px solid #ddd;margin: 30px -30px 0;text-align: left;'>
                <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>This is a one-time outgoing email.</p>
                <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>You cannot reply to this email. If you have any questions, please use our <a href=" . '{$a->courl}' . " target='_blank' style='text-decoration: underline;'>customer center.</a></p>
                <p style='font-family: inherit;font-size: 13px;margin: 0 0 3px;color: #777;margin-top: 7px;'>Copyright © VISANG Education Group Vietnam Company </p>
            </div>
        </div>
    </body>
</html>
";
$string['changepasstext'] = 'Hello.  {$a->firstname},

 Temporary password for Your account {$a->username} 

{$a->temppass} Has been issued

Please log in with a temporary password and change your password.

Get login → {$a->link}';
$string['successstemp'] = 'Your mail has been sent.';
$string['falsesend'] = '{$a} Please contact the administrator who failed to send the mail.';
$string['successsend'] = '{$a} Shipment completed by email.';
$string['sendsuccess'] = 'Completed sending authenticated mail';
$string['sendsuccesstext1'] = 'An authentication email has been sent to your subscribed email address';
$string['sendsuccesstext2'] = 'Please complete authentication after checking mail.';
$string['sendsuccesstext3'] = 'If the mail is not properly checked, please check the spam box.';
$string['sendsuccesstext4'] = 'If it is not found in the spam box, please click the Resend Certified Mail button or contact the person in charge.';
$string['sendsuccesstext5'] = '(Manager 0000-0000)';
$string['resendmail'] = 'Re-sending Authorized';
$string['gohome'] = 'Go Home';
$string['falsetemp'] = 'Failed to send mail Please contact the administrator';
$string['changepasstitle'] = '{$a->sitename} Issue temporary password';
$string['notavailable'] = 'out of use {$a->type}.';
$string['available'] = 'available {$a->type}';
$string['joinpassword'] = 'password';
$string['email'] = 'email';
$string['emailrule'] = 'The email format is {$a->rule} Must be formatted';
$string['checkterm'] = 'Please agree to the terms and conditions.';
$string['checkemail'] = 'Please check your e-mail. ';
$string['checkjoinpassword'] = 'Please check the password rules.';
$string['checkpassword'] = 'Confirm Password';
$string['writepassword'] = 'Enter Password';
$string['writeemail'] = 'Enter Email';
$string['writename'] = 'Enter a name';
$string['matchedpass'] = 'Passwords match.';
$string['notmatchedpass'] = 'Passwords do not match.';
$string['name'] = 'name';
$string['birth'] = 'birth';
$string['phone'] = 'phone';
$string['signup'] = 'signup';
$string['signup'] = 'signup';
$string['necessaryinfo'] = 'Required information';
$string['startingemail'] = 'Starting email';
$string['duplicatecheck'] = 'Check';
$string['agreeallterm'] = 'If you do not agree with the above items, you cannot sign up for membership.';
$string['agree'] = 'agree';
$string['usingterm'] = 'Consent to the Terms and Conditions';
$string['persnalinfo'] = 'Agreeing to collect and use personal information';
$string['serviceterm'] = 'Regulations for Service Operation';
$string['allview'] = 'allview';
$string['agreeterm'] = 'agree';
$string['gotohome'] = 'Goto Home';
$string['emailcertifiedsuccess'] = 'E-mail authentication';
$string['emailcertified'] = 'Email certified';
$string['emailfindtitle'] = 'Find my account';
$string['findmyaccount'] = 'Find my account';
$string['emailfindtext'] = 'You can find your account with the information registered at the time of membership.';
$string['recivetemppass'] = 'Receive temporary password by email';
$string['chanepass'] = 'There is no matching information. Please check again.';
$string['notaccount'] = 'There are no registered accounts';
$string['passvalcheck'] = 'Passwords must meet at least 8 characters (must contain at least one lowercase letter, one special character, and one number)';
$string['blankcheck'] = 'Spaces cannot be entered.';
$string['writeall'] = 'Please enter all items.';
$string['invalidlogin'] = 'The username or password error. Please log in again';
$string['overdate'] = 'You cant sign up for a date that is later than today';
$string['gender'] = 'Gender';
$string['male'] = 'Male';
$string['female'] = 'Female';