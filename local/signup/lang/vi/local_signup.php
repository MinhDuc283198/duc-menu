<?php

$string['emailconfirmation'] = "
    <!DOCTYPE html>
<html lang='ko'>
    <head>
        <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>Document</title>
    </head>
    <body>
        <link href='https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese'  rel='stylesheet' />
        <div style='border: 1px solid #ddd;padding: 50px 30px 0;width: 600px;text-align: center;'>
            <p style='text-align: center;margin: 0 0 10px;'>
                <a href=" . '{$a->wwwroot}' . " target='_blank'><img src=" . 'https://www.masterkorean.vn/{$a->imgurl}' . " alt='master Korean' width='150' /></a>
            </p>
            <div style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;font-size: 25px;font-weight: bold;text-align: center;margin-bottom: 10px;'>" . '{$a->sitename}' . " Đăng ký hoàn tất.</div>
            <p style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;color: #485cc7;text-align: center;font-size: 16px;margin-bottom: 15px;border: 1px solid #485cc7;width: 200px;margin: 25px auto;padding: 10px;'>Đã tham gia : " . '{$a->timecreated}' . " </p>
            <p style='font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;font-size: 20px;text-align: center;'>Nhấp vào nút xác minh email bên dưới<br /> Vui lòng hoàn thành chứng nhận.</p>
            <a href=" . '{$a->link}' . " style='display: inline-block;padding: 0 25px;height: 50px;line-height: 48px;background: #00b5e2;text-decoration: none;font-family: Noto Sans KR, Noto Sans SC, 맑은고딕,sans-serif;color: #fff;'>Xác thực email</a>
            <div style='background: #f4f4f4;font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;width: auto; margin: 10px 0; padding: 20px 30px;box-sizing: border-box;border-top: 1px solid #ddd;margin: 30px -30px 0;text-align: left;'>
                <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>This is a one-time outgoing email.</p>
                <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>You cannot reply to this email. If you have any questions, please use our <a href=" . '{$a->courl}' . " target='_blank' style='text-decoration: underline;'>customer center.</a></p>
                <p style='font-family: inherit;font-size: 13px;margin: 0 0 3px;color: #777;margin-top: 7px;'>Copyright © VISANG Education Group Vietnam Company </p>
            </div>
        </div>
    </body>
</html>
";
$string['changepasstext'] = 'Xin chào.  {$a->firstname},

 Mật khẩu tạm thời cho tài khoản {$a->username} là 

{$a->temppass}

Vui lòng đăng nhập với mật khẩu tạm thời và thay đổi mật khẩu sau đăng nhập.

{$a->link}';
$string['successstemp'] = 'Thư đã được gửi đi';
$string['falsesend'] = '{$a} Gửi thư thất bại, liên hệ quản trị viên ';
$string['successsend'] = '{$a} Thư báo vận chuyển thành công';
$string['sendsuccess'] = 'Hoàn tất xác thực thư điện tử';
$string['sendsuccesstext1'] = 'Một thư xác thực đã được gửi đến hòm thư của bạn';
$string['sendsuccesstext2'] = 'Vui lòng hoàn thành xác thực địa chỉ thư điện tử';
$string['sendsuccesstext3'] = 'Nếu không tìm thấy thư, vui lòng kiểm tra hòm thư Spam';
$string['sendsuccesstext4'] = 'Nếu không tìm thấy trong Spam, Vui lòng nhấn nút gửi lại hoặc liên lạc với quản trị viên';
$string['sendsuccesstext5'] = '(Quản lý 0000-0000)';
$string['resendmail'] = 'Gửi lại';
$string['gohome'] = 'Trang chủ';
$string['falsetemp'] = 'Thư gửi thất bại, vui lòng liên hệ quản trị viên';
$string['changepasstitle'] = '{$a->sitename} Cấp mật khẩu tạm thời';
$string['notavailable'] = 'Đã tồn tại {$a->type}';
$string['available'] = 'Có thể sử dụng {$a->type}';
$string['joinpassword'] = 'Mật khẩu';
$string['email'] = 'Thư điện tử';
$string['emailrule'] = 'Định dạng thư phải là {$a->rule}';
$string['checkterm'] = 'Vui lòng đồng ý với các điều khoản và điều kiện';
$string['checkemail'] = 'Vui lòng kiểm tra hòm thư điện tử';
$string['checkjoinpassword'] = 'Vui lòng kiểm tra quy tắt đặt mật khẩu';
$string['checkpassword'] = 'Xác nhận mật khẩu';
$string['writepassword'] = 'Nhập mật khẩu';
$string['writeemail'] = 'Nhập thư điện tử';
$string['writename'] = 'Nhập tên';
$string['matchedpass'] = 'Mật khẩu trùng khớp';
$string['notmatchedpass'] = 'Mật khẩu không trùng khớp';
$string['name'] = 'Tên';
$string['birth'] = 'Sinh nhật';
$string['phone'] = 'Số điện thoại';
$string['signup'] = 'Đăng nhập';
$string['necessaryinfo'] = 'Thông tin bắt buộc';
$string['startingemail'] = 'Bắt đầu với thư điện tử';
$string['duplicatecheck'] = 'Kiểm tra';
$string['agreeallterm'] = 'Nếu bạn không đồng ý điều khoản, bạn không thể đăng ký thành viên';
$string['agree'] = 'Đồng ý';
$string['usingterm'] = 'Đồng ý với các điều kiện và điều khoản';
$string['persnalinfo'] = 'Đồng ý chia sẻ thông tin cá nhân';
$string['serviceterm'] = 'Quy định hoạt động dịch vụ';
$string['allview'] = 'Xem tất cả';
$string['agreeterm'] = 'Đồng ý';
$string['gotohome'] = 'Trang chủ';
$string['emailcertifiedsuccess'] = 'xác thực thư điện tử thành công';
$string['emailcertified'] = 'Xác thực thư điện tử';
$string['findmyaccount'] = 'Tìm tài khoản';
$string['emailfindtext'] = 'Bạn có thể tìm tài khoản của bạn bằng thông tin bạn đã đăng ký';
$string['recivetemppass'] = 'Nhận mật khẩu tạm thời qua thư điện tử';
$string['chanepass'] = 'Không có thông tin phù hợp. Vui lòng kiểm tra lại';
$string['notaccount'] = 'Chưa có tài khoản đăng ký';
$string['emailfindtitle'] = 'Tìm tài khoản của tôi';
$string['passvalcheck'] = 'Mật khẩu phải đáp ứng ít nhất 8 ký tự (phải chứa ít nhất một chữ cái viết thường, một ký tự đặc biệt và một số).';
$string['blankcheck'] = 'Vui lòng không nhập khoản trắng';
$string['writeall'] = 'Hãy nhập tất cả các hạng mục.';
$string['invalidlogin'] = 'Tên người dùng hoặc mật khẩu bị lỗi. Vui lòng đăng nhập lại';
$string['overdate'] = 'Bạn không thể đăng ký một ngày muộn hơn hôm nay.';
$string['gender'] = 'Giới tính';
$string['male'] = 'Nam';
$string['female'] = 'Nữ';