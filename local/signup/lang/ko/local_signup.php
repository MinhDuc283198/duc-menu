<?php

$string['emailconfirmation'] = "
    <!DOCTYPE html>
<html lang='ko'>
    <head>
        <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>Document</title>
    </head>
    <body>
        <link href='https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese'  rel='stylesheet' />
        <div style='border: 1px solid #ddd;padding: 50px 30px 0;width: 600px;text-align: center;'>
            <p style='text-align: center;margin: 0 0 10px;'>
                <a href=" . '{$a->wwwroot}' . " target='_blank'><img src=" . 'https://www.masterkorean.vn/{$a->imgurl}' . " alt='master Korean' width='150' /></a>
            </p>
            <div style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;font-size: 25px;font-weight: bold;text-align: center;margin-bottom: 10px;'>" . '{$a->sitename}' . " 가입이 완료되었습니다.</div>
            <p style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;color: #485cc7;text-align: center;font-size: 16px;margin-bottom: 15px;border: 1px solid #485cc7;width: 200px;margin: 25px auto;padding: 10px;'>가입일 :" . '{$a->timecreated}' . " </p>
            <p style='font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;font-size: 20px;text-align: center;'>아래 이메일 인증 버튼을 클릭하여<br /> 인증을 완료해주시기 바랍니다.</p>
            <a href=" . '{$a->link}' . " style='display: inline-block;padding: 0 25px;height: 50px;line-height: 48px;background: #00b5e2;text-decoration: none;font-family: Noto Sans KR, Noto Sans SC, 맑은고딕,sans-serif;color: #fff;'>이메일 인증</a>
            <div style='background: #f4f4f4;font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;width: auto; margin: 10px 0; padding: 20px 30px;box-sizing: border-box;border-top: 1px solid #ddd;margin: 30px -30px 0;text-align: left;'>
                <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>This is a one-time outgoing email.</p>
                <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>You cannot reply to this email. If you have any questions, please use our <a href=" . '{$a->courl}' . " target='_blank' style='text-decoration: underline;'>customer center.</a></p>
                <p style='font-family: inherit;font-size: 13px;margin: 0 0 3px;color: #777;margin-top: 7px;'>Copyright © VISANG Education Group Vietnam Company </p>
            </div>
        </div>
    </body>
</html>
";
$string['changepasstext'] = '안녕하세요  {$a->firstname} 님,

회원님의 계정 {$a->username} 에 대한 임시 비밀번호 

{$a->temppass} 가 발급되었습니다. 

임시 비밀번호로 로그인 후 반드시 비밀번호를 변경해주세요.

로그인하기 → {$a->link}';
$string['successstemp'] = '메일 발송이 완료되었습니다.';
$string['falsesend'] = '{$a} 메일 발송 실패 관리자에게 문의해주세요.';
$string['successsend'] = '{$a} 메일로 발송이 완료되었습니다.';
$string['sendsuccess'] = '인증 메일 발송 완료';
$string['sendsuccesstext1'] = '가입한 이메일 주소로 인증 메일이 발송되었습니다.';
$string['sendsuccesstext2'] = '메일 확인 후 인증을 완료하시기 바랍니다.';
$string['sendsuccesstext3'] = '메일이 정상적으로 확인되지 않을 경우 스팸함을 확인해주시고,';
$string['sendsuccesstext4'] = '스팸함에서도 확인되지 않을 경우 인증 메일 재발송 버튼을 클릭하거나 담당자에게 문의바랍니다.';
$string['sendsuccesstext5'] = '(담당자 0000-0000)';
$string['resendmail'] = '인증 메일 재발송';
$string['gohome'] = '홈으로이동';
$string['falsetemp'] = '메일 발송에 실패했습니다 관리자에게 문의해주세요';
$string['changepasstitle'] = '{$a->sitename} 임시 비밀번호 발급';
$string['notavailable'] = '사용할 수 없는  {$a->type} 입니다.';
$string['available'] = '사용 가능한 {$a->type} 입니다';
$string['joinpassword'] = '비밀번호';
$string['email'] = '이메일';
$string['emailrule'] = '이메일 형식은 {$a->rule} 형식이어야 합니다';
$string['checkterm'] = '약관에 동의해주세요';
$string['checkemail'] = '이메일을 확인해주세요 ';
$string['checkjoinpassword'] = '비밀번호 규칙을 확인해주세요';
$string['checkpassword'] = '비밀번호 확인';
$string['writepassword'] = '비밀번호 입력';
$string['writeemail'] = '이메일 입력';
$string['writename'] = '이름 입력';
$string['matchedpass'] = '비밀번호가 일치 합니다';
$string['notmatchedpass'] = '비밀번호가 일치하지 않습니다';
$string['name'] = '이름';
$string['birth'] = '생년월일';
$string['phone'] = '휴대폰 번호';
$string['signup'] = '회원가입';
$string['necessaryinfo'] = '필수정보입니다';
$string['startingemail'] = '이메일로시작하기';
$string['duplicatecheck'] = '중복체크';
$string['agreeallterm'] = '위 항목에 동의하지 않는 경우 회원가입이 불가합니다.';
$string['agree'] = '동의';
$string['usingterm'] = '이용약관 동의';
$string['persnalinfo'] = '개인정보보호방침';
$string['serviceterm'] = '서비스운영규정';
$string['allview'] = '전체보기';
$string['agreeterm'] = '동의합니다';
$string['gotohome'] = '메인으로 이동후 서비스를 이용해 주세요';
$string['emailcertifiedsuccess'] = '이메일 인증이 완료되었습니다.';
$string['emailcertified'] = '이메일 인증';
$string['findmyaccount'] = '나의계정찾기';
$string['emailfindtext'] = '회원가입 시 등록한 정보로 나의 계정을 찾을 수 있습니다.';
$string['recivetemppass'] = '이메일로 임시 비밀번호 받기';
$string['chanepass'] = '임시 비밀번호로 로그인 후 반드시 비밀번호를 변경해주세요.';
$string['notaccount'] = '일치하는 정보가 없습니다 다시 한번 확인해주세요.';
$string['agreeterm'] = '동의합니다';
$string['emailfindtitle'] = '나의 계정 찾기';
$string['passvalcheck'] = '암호는 8자 이상(영어 소문자, 특수문자, 숫자 각 1개 이상 필수)을 충족해야합니다.';
$string['blankcheck'] = '공백은 입력 불가합니다.';
$string['writeall'] = '모든 항목을 입력해주세요.';
$string['invalidlogin'] = '아이디 또는 비밀번호 오류 입니다. 다시 로그인해 주세요';
$string['overdate'] = '오늘보다 미래의 날짜로 가입할 수 없습니다.';
$string['gender'] = '성별';
$string['male'] = '남성';
$string['female'] = '여자';
