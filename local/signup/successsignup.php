<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
if(isloggedin()){
    redirect($CFG->wwwroot);
}
$userid = required_param('id', PARAM_INT);
$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');
echo $OUTPUT->header();
?>  

<div class="mail-end-bx style02">
    <div><?php echo get_string("sendsuccess","local_signup");?></div>
    <p>
        <?php echo get_string("sendsuccesstext1","local_signup");?><br />
        <?php echo get_string("sendsuccesstext2","local_signup");?>
    </p>
    <p class="small">
        <?php echo get_string("sendsuccesstext3","local_signup");?><br/>
        <?php echo get_string("sendsuccesstext4","local_signup");?><br/>
    </p>
    <a href="#" onclick="email_resend(<?php echo $userid ?>)" class="btns point big"><?php echo get_string("resendmail","local_signup");?></a>
    <a href="<?php echo $CFG->wwwroot ?>" class="btns big"><?php echo get_string("gohome","local_signup");?></a>
</div>


<?php
echo $OUTPUT->footer();
?>
<script>
    $(document).ready(function () {
    });
    function email_resend(userid){
            $.ajax({
                url: "/local/signup/signup_summit.php",
                type: 'post',
                dataType: "json",
                data: {resend: 1, userid: userid},
                success: function (result) {
                   alert(result.statustext);
                }
            });  
    }

</script>


