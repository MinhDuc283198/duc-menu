<?php
require('../../config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  

<div class="mail-end-bx">
    <div><?php echo get_string("emailcertified","local_signup")?></div>
    <p><?php echo get_string("emailcertifiedsuccess","local_signup")?></p>
    <p class="small"><?php echo get_string("gotohome","local_signup")?></p>
    <a href="<?php echo $CFG->wwwroot?>" class="btns point big"><?php echo get_string("home","core")?></a>
</div>

<?php
echo $OUTPUT->footer();
?>


