<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/signup/lib.php';
global $USER, $SESSION, $DB;

$resend = optional_param('resend', 0, PARAM_INT);
$snsid = optional_param('snsid', '', PARAM_RAW);
$auth = optional_param('auth', '', PARAM_RAW);
$email = optional_param('email', '', PARAM_RAW);
$sex = optional_param('sex', '', PARAM_RAW);
$authorizationcode = optional_param('code', '', PARAM_RAW);
$emailauth = new local_auth_plugin_email();

if ($resend == 1) {
    $userid = optional_param('userid', 0, PARAM_INT);
    $user = $DB->get_record("user", array("id" => $userid));
    if ($user) {
        $status = $emailauth->send_confirmation_email($user);
        $resenddata = new stdClass();
        $resenddata->status = $status;
        $resenddata->statustext = get_string("falsesend", "local_signup", $user->email);
        if ($status == 1) {
            $resenddata->statustext = get_string("successsend", "local_signup", $user->email);
        }
        echo json_encode($resenddata);
    }
} elseif (!empty($authorizationcode)) {
    $user->username = $auth ? optional_param('username', 0, PARAM_RAW) : optional_param('email', 0, PARAM_RAW);
    $user->password = $auth ? 'not cached' : optional_param('password', 0, PARAM_RAW);
    $user->email = $snsid ? $snsid : $email;
    $user->phone1 = optional_param('phone1', 0, PARAM_RAW);
    $user->phone2 = optional_param('year', 0, PARAM_RAW) . "-" . optional_param('month', 0, PARAM_RAW) . "-" . optional_param('days', 0, PARAM_RAW);
    $user->firstname = optional_param('firstname', 0, PARAM_RAW);
    $user->lastname = '&nbsp;';
    $user->confirmed = 1;
    $user->lang = current_language();
    $user->firstaccess = 0;
    $user->timecreated = time();
    $user->mnethostid = $CFG->mnet_localhost_id;
    $user->secret = random_string(15);
    $user->auth = $auth ? 'googleoauth2' : 'email';
    $user->lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $user->joinstatus = 'e';
    if (!empty($user->email) && !empty($user->password) && !empty($user->phone1) && !empty($user->firstname)) {
        if ($userid = $emailauth->user_signup($user, true, $auth, $email)) {
//            redirect($CFG->wwwroot . "/local/signup/successsignup.php?id=$userid");
            $user = $DB->get_record('user',
                array('id' => $userid));
            var_dump($user->username);
            $user = authenticate_user_login($user->username, null);
            if (isset($user->id)) {

                // Set a cookie to remember what auth provider was selected.
                setcookie('MOODLEGOOGLEOAUTH2_' . $CFG->sessioncookie, $auth,
                    time() + (DAYSECS * 60), $CFG->sessioncookiepath,
                    $CFG->sessioncookiedomain, $CFG->cookiesecure,
                    $CFG->cookiehttponly);

                // Prefill more user information if new user.
                if (!empty($userdetails)) {
                    $userdetails2 = new stdClass();
                    $userdetails2->id = $user->id;
//                        $userdetails2->firstname = $userdetails->firstname;
                    $userdetails2->email = $userdetails->email;
                    $DB->update_record('user', $userdetails2);
                    $user = (object)array_merge((array)$user, (array)$userdetails2);

                }

                complete_user_login($user);
                $token = explode("#", $authorizationcode);

                $accesstoken = $token[0];
                $refreshtoken = $token[1];
                $tokenexpires = $token[2];

                // Let's save/update the access token for this user.

                if (!empty($accesstoken)) {
                    $existingaccesstoken = $DB->get_record('auth_googleoauth2_user_idps',
                        array('userid' => $user->id, 'provider' => $auth));
                    if (empty($existingaccesstoken)) {
                        $accesstokenrow = new stdClass();
                        $accesstokenrow->userid = $user->id;
                        $accesstokenrow->provideruserid = $snsid;
                        $accesstokenrow->provider = $auth;
                        $accesstokenrow->accesstoken = $accesstoken;
                        $accesstokenrow->refreshtoken = $refreshtoken;
                        $accesstokenrow->expires = $tokenexpires;
                        $DB->insert_record('auth_googleoauth2_user_idps', $accesstokenrow);
                    } else {
                        $existingaccesstoken->accesstoken = $accesstoken;
                        $DB->update_record('auth_googleoauth2_user_idps', $existingaccesstoken);
                    }
                }

                // Create event for authenticated user.
                $event = \auth_googleoauth2\event\user_loggedin::create(
                    array('context' => context_system::instance(),
                        'objectid' => $user->id, 'relateduserid' => $user->id,
                        'other' => array('accesstoken' => $accesstoken)));
                $event->trigger();

                // Redirection.
                if (user_not_fully_set_up($USER)) {
                    $urltogo = $CFG->wwwroot . '/user/edit.php';
                    // We don't delete $SESSION->wantsurl yet, so we get there later.
                } else if (isset($SESSION->wantsurl) && (strpos($SESSION->wantsurl, $CFG->wwwroot) === 0)) {
                    $urltogo = $SESSION->wantsurl;    // Because it's an address in this site.
                    unset($SESSION->wantsurl);
                } else {
                    // No wantsurl stored or external - go to homepage.
                    $urltogo = $CFG->wwwroot . '/';
                    unset($SESSION->wantsurl);
                }

//                 Add my record to statistics table with subtype information
                $loginrecord = array('userid' => $USER->id, 'time' => time(),
                    'auth' => 'googleoauth2', 'subtype' => $auth);
                $DB->insert_record('auth_googleoauth2_logins', $loginrecord);

                redirect($urltogo);
            }
        }
    }
} else {
    $user->username = $auth ? optional_param('username', 0, PARAM_RAW) : optional_param('email', 0, PARAM_RAW);
    $user->password = $auth ? 'not cached' : optional_param('password', 0, PARAM_RAW);
    $user->email = $snsid ? $snsid : $email;
    $user->phone1 = optional_param('phone1', 0, PARAM_RAW);
    $user->phone2 = optional_param('year', 0, PARAM_RAW) . "-" . optional_param('month', 0, PARAM_RAW) . "-" . optional_param('days', 0, PARAM_RAW);
    $user->firstname = optional_param('firstname', 0, PARAM_RAW);
    $user->lastname = '&nbsp;';
    $user->confirmed = 0;
    $user->lang = current_language();
    $user->firstaccess = 0;
    $user->timecreated = time();
    $user->mnethostid = $CFG->mnet_localhost_id;
    $user->secret = random_string(15);
    $user->auth = $auth ? 'googleoauth2' : 'email';
    $user->lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $user->joinstatus = 'e';
    $user->sex = $sex;
    if (!empty($user->email) && !empty($user->password) && !empty($user->phone1) && !empty($user->firstname)) {
        if ($userid = $emailauth->user_signup($user, true, $auth, $email)) {
            redirect($CFG->wwwroot . "/local/signup/successsignup.php?id=$userid");
        }
    }
}


