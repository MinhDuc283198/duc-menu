<?php


require_once($CFG->dirroot . '/local/course/lib.php');
$login = isloggedin();
$recommendcourses = local_course_recommend($login);
// $jobs = local_job_recommend();
require_once($CFG->dirroot . '/local/job/lib/common_functions.php');
$jobs = mkjobs_get_recommend_jobs(isloggedin());

?>

<div class="bar-tab-grp bt">
    <ul class="bar-tab tab-event">
        <li class="on" data-target=".data01"><a href="#"><?php echo get_string('recommendedcourse' . $login, 'theme_oklassedu') ?></a></li>
        <li data-target=".data02"><a href="#"><?php echo ($login) ? get_string('jobinfologin', 'theme_oklassedu') : get_string('jobinfo', 'theme_oklassedu') ?></a></li>
    </ul>
    <div class="target-area">
        <!-- 추천강좌 start -->
        <div class="data01 on">
            <ul class="thumb-list style01 slider">
                <?php foreach ($recommendcourses as $recommendcourse) {
                    $context = context_course::instance($recommendcourse->lcocourseid);
                    $path = local_course_get_imgpath($context->id); ?>
                    <li>
                        <div class="wp">
                            <div class="tp">
                                <span class="ic-like <?php echo $recommendcourse->likes ? 'on' : ''; ?>" id="like<?php echo $recommendcourse->id ?>" data-target="<?php echo $recommendcourse->id ?>"><?php echo get_string('like', 'local_course') ?></span>
                                <div class="tit"><a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $recommendcourse->id ?>"><?php echo $recommendcourse->title ?></a></div>
                                <p><a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $recommendcourse->id ?>"><?php echo $recommendcourse->onelineintro ?></a></p>
                            </div>
                            <div class="img">
                                <a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $recommendcourse->id ?>">
                                    <img src="<?php echo $path ?>" alt="<?php echo $recommendcourse->title ?>" />
                                </a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div><!-- 추천강좌 end -->
        <!-- 추천채용공고 start -->
        <div class="data02 ">
            <ul class="cmpny-bx slider">
                <?php foreach ($jobs as $job) {
                    $logopath = $job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/default_emp.png' : $CFG->wwwroot . '/pluginfile.php' . '/' . $job->company_logo; ?>
                    <li class="bx" >
                        <div class="lg">
                            <img class="pointer" onclick="move_detailjob(<?php echo $job->id ?>)" src="<?php echo $logopath ?>" alt="비상교육" />
                        </div>
                        <div class="tx">
                            <strong class="pointer" onclick="move_detailjob(<?php echo $job->id ?>)" ><?php echo $job->company_name; ?></strong>
                            <span class="pointer" onclick="move_detailjob(<?php echo $job->id ?>)" ><?php echo $job->title; ?></span>

                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div><!-- 추천채용공고 end -->
    </div>
</div>
<script>
    $(function() {
        $(".ic-like").click(function() {
            if ($(this).hasClass("on")) {
                $(this).removeClass("on")
            } else {
                $(this).addClass("on")
            }
        });
    });

    function move_detailpage(id) {
        location.href = "/local/course/detail.php?id=" + id;
    }

    function move_detailjob(id) {
        location.href = "<?php echo $CFG->job_wwwroot;?>/local/job/jobs/view.php?jobid=" + id;
    }
</script>