<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');

$id = optional_param('id', 0, PARAM_INT); 
$coid = optional_param('coid', 0, PARAM_INT); 
$course = $DB->get_record('course', array('id' => $id));
$context = context_course::instance($course->id);
$PAGE->set_course($course);
$PAGE->set_context($context);
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add('수강후기');

if ($USER->id == 0) {
    require_login();
}


$avg = $DB->get_record_sql("select AVG(score) avg from {lmsdata_course_like} where moodlecourseid = $id");
$comments = local_lmsdata_get_course_like($id);

//수정
$button_txt = get_string('enrollment', 'local_course');
if($coid != 0){
    $modicomment = $DB->get_record('lmsdata_course_like', array('id'=>$coid));
    $button_txt = get_string('modified', 'local_course');
}

//등록할때 만약 등록 돼 있으면 등록 X
//등록할때 만약 등록 돼 있으면 등록 X
echo $OUTPUT->header();
?>  
<h6 class="has-star pg-tit">
    <?php echo get_string('coursereviews', 'local_course') ?>
    <div class="f-r">
        <div class="star-event big" data-num="<?php echo round($comments->avg->avg)?>"></div>
        <span><?php echo round($comments->avg->avg)?><?php echo get_string('score', 'local_course') ?></span>
        <span class="t-gray">(<?php echo $comments->total_lang?><?php echo get_string('count', 'local_course') ?>)</span>
    </div>
</h6>

<div class="input-review">
    <form action="view_submit.php" method="post" enctype="multipart/form-data"> 
        <input type="hidden" name="moodlecourseid" value="<?php echo $id?>">
        <input type="hidden" name="id" value="<?php echo $coid?>">
        <div class="tp">
            <?php echo get_string('selectstar', 'local_course') ?>
            <div class="star-event hover" id="id_score_star" data-num="<?php echo $modicomment->score?>" onclick="score_star()"></div>
            <input type="hidden" name="score" value="<?php echo $modicomment->score?>">
        </div>
        <div class="bt">
             <textarea placeholder="<?php echo get_string('enterreview', 'local_course') ?>" name="contents"><?php echo $modicomment->contents?></textarea>
             <input type="submit" class="btns" value="<?php echo $button_txt?>"  onclick="return return_check(<?php echo $id?>, <?php echo $USER->id?>, <?php echo $coid?>)"/>
        </div>
    </form>
</div>


<?php if($comments->total > 0){ ?>
<ul class="review-list">
    <?php foreach($comments->comment as $li){?>
    <li id="li_<?php echo $li->id?>">
        <div class="txt-area">
            <div class="tp">
                <strong><?php echo $li->firstname?></strong>
                <span class="tm"><?php echo $li->displaytime?></span>
                <div class="star-event" data-num="<?php echo $li->score?>"></div>
            </div>
            <div class="txt"><?php echo $li->contents?></div>
            <?php if($li->my){?>
            <div class="btn-area text-right">
    <!--            <a href="./coursereview.php?id=<?php echo $id?>&coid=<?php echo $li->id?>" class="btns br"><?php echo get_string('modified', 'local_course') ?></a>-->
                <a href="#" class="btns br edit-mod" data-id="<?php echo $li->id?>" data-moodlecourseid="<?php echo $id?>"><?php echo get_string('modified', 'local_course') ?></a>
                <a href="./view_submit.php?mod=delete&id=<?php echo $li->id?>&moodlecourseid=<?php echo $id?>" class="btns br"><?php echo get_string('deleted', 'local_course') ?></a>
            </div>
        </div>
        
        <!-- 수정 영역 -->
        <div class="edit-area" id="edit-area_<?php echo $li->id?>">
<!--            <form id="edit_review_form" action="view_submit.php" method="post" enctype="multipart/form-data"> -->
            <input type="hidden" name="moodlecourseid" value="<?php echo $id?>">
            <input type="hidden" name="id" value="<?php echo $li->id?>">
                <div class="tp">
                    <strong><?php echo $li->firstname?></strong>
                    <span class="tm"><?php echo $li->displaytime?></span>
                    <div class="star-event hover" id="star-event_<?php echo $li->id?>" data-num="<?php echo $li->score?>"></div>
                </div>
                <textarea name="contents"><?php echo str_replace('<br />','',$li->origincontents);?></textarea>
                <div class="btn-area text-right">
                    <a href="#" class="btns br edit" data-id="<?php echo $li->id?>"><?php echo get_string('modified','local_course')?></a>
<!--                    <input type="submit" class="btns br" value="<?php echo $button_txt?>"/>-->
                    <a href="#" class="btns br close-edit-mod"><?php echo get_string('cancel','local_course')?></a>
                </div>
<!--            </form>-->
        </div><!-- 수정 영역 -->
        <?php }?>
    </li>
    <?php }?>
</ul>
<?php }else{?>
<div class="no-data">
    <div><?php echo get_string('coursereviewsempty', 'local_course') ?></div>
</div>
<?php }?>
<script src="<?php echo $CFG->wwwroot?>/local/course/course.php"></script>  
<script>
    function score_star(){
        var score = $('#id_score_star .on').length;
        $('input[name=score]').val(score);
    }
    function return_check(courseid, userid, status){
        var result = call_ajax('overlap_courseview', {courseid:courseid, userid:userid, status:status}, false);
        if(result.status == "success") {
            if(result.list){
                alert('<?php echo get_string('alreadyreview', 'local_course') ?>');
                return false;
            }
        }else{
            alert(result.error);
            return false;
        }
        return true;
    }
$(document).ready(function(){
    //수정버튼 클릭
    $(".edit-mod").click(function(e){
        e.preventDefault();
       $(this).closest("li").addClass("edit"); 
       $(this).closest("li").find("textarea").focus();
    });
    //취소버튼 클릭
    $(".close-edit-mod").click(function(e){
        e.preventDefault();
       $(this).closest("li").removeClass("edit"); 
    });
    
    $('.edit').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var courseid = $('#edit-area_'+id+' input[name=moodlecourseid]').val();
        var new_text = $('#edit-area_'+id+' textarea[name=contents]').val();
        var new_star = $('#star-event_'+id+' .on').length;
        var result = call_ajax('courseviewedit', {id: id, courseid:courseid, contents:new_text, score:new_star}, false);
        if(result.status == "success"){
            location.reload();
            //$(this).closest("li").removeClass("edit"); 
        }
    });
});
</script>
<?php
echo $OUTPUT->footer();
?>


