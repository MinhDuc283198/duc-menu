<?php

define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');


$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'category':
        $data = required_param_array('data', PARAM_RAW);
        $list = local_course_category_parent($data["id"]);

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'overlap_courseview':
        $data = required_param_array('data', PARAM_RAW);
        if($data['status'] == 0){
            $list = $DB->get_record('lmsdata_course_like',array('moodlecourseid'=>$data['courseid'],'userid'=>$data['userid']));
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;

        echo json_encode($rvalue);
        break;
    case 'payment_course':
        $data = required_param_array('data', PARAM_RAW);
        
        print_object($data);exit;
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'shoppinginfo':
        $data = required_param_array('data', PARAM_RAW);
        $data_typ = $data['type'];
        if($data['type'] == 3){
            $data_typ=1;
        }
        
        //lmsdata_payment 에도 값 입력
        $lmsdata->userid = $USER->id;
        $lmsdata->status = 1;
        $lmsdata->type = $data_typ;
        $lmsdata->ref = $data['bookid'];
        $lmsdata->paytype = 1;
        $lmsdata->timecreated = time();
        $lmsdata->timemodified = time();
        $payid = $DB->insert_record('lmsdata_payment', $lmsdata);

        if($data['type']  != 1){
            $lmsdata2->payid = $payid;
            $lmsdata2->bookid = $data['bookid'];
            $lmsdata2->address = $data['address'];
            $lmsdata2->cell = $data['cell'];
            $lmsdata2->timecreated = time();
            $lmsdata2->timemodified = time();
            $DB->insert_record('lmsdata_productdelivery', $lmsdata2);
        }
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->payid = $payid;
        echo json_encode($rvalue);
        break;
    case 'courseviewedit':
        $data = required_param_array('data', PARAM_RAW);
        $newdata = new stdClass();
        $newdata->id = $data['id'];
        $data['contents'] = nl2br($data['contents']);
        $newdata->contents = $data['contents'];
        $newdata->score = $data['score'];
        $newdata->timemodified = time();
        
        
        $DB->update_record('lmsdata_course_like', $newdata);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();