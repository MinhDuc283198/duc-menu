<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$id = optional_param('id', 0, PARAM_INT); 
$mod = optional_param('mod', '', PARAM_RAW);
$lmsdata = new stdClass();

foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}
$lmsdata->contents = nl2br($lmsdata->contents);
$lmsdata->userid = $USER->id;

if($id){
    if($mod == 'delete'){
        $DB->delete_records('lmsdata_course_like', array('id'=>$id));
    }else{
        $lmsdata->timemodified = time();
        $DB->update_record('lmsdata_course_like', $lmsdata);
    }
}else{
    $lmsdata->timecreated = time();
    $DB->insert_record('lmsdata_course_like', $lmsdata);
}
redirect($CFG->wwwroot . '/local/course/coursereview.php?id='.$lmsdata->moodlecourseid);
