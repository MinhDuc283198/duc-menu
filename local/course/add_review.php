<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', '', PARAM_RAW);
$lmsdata = new stdClass();


foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}
$lmsdata->contents = nl2br($lmsdata->contents);
$lmsdata->userid = $USER->id;


if($id){
    if($mod == 'delete'){
        $DB->delete_records('lmsdata_course_like', array('id'=>$id));
    }else{
        $lmsdata->timemodified = time();
        $DB->update_record('lmsdata_course_like', $lmsdata);
    }
}else{

    $sql = "select * from m_lmsdata_class where id = $lmsdata->moodlecourseid";
    $lmsdata_class = $DB->get_record_sql($sql);

    $sql_enrol = "select * from m_user_enrolments 
                  JOIN m_enrol on m_user_enrolments.enrolid = m_enrol.id where m_user_enrolments.userid = $USER->id AND m_enrol.courseid = $lmsdata_class->courseid";

    $enrol = $DB->get_record_sql($sql_enrol);

    if ($enrol) {
        $lmsdata->timecreated = time();
        $lmsdata->moodlecourseid = $lmsdata_class->courseid;

        $DB->insert_record('lmsdata_course_like', $lmsdata);
    }
}

header("Location: " . $_SERVER["HTTP_REFERER"]);
