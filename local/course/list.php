<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');
require_once $CFG->dirroot.'/local/lmsdata/lib.php';
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('강좌목록');

echo $OUTPUT->header();

$category = optional_param('id', 0, PARAM_INT);
$list = local_course_class_category_id($category)->list;
$category_path = local_course_category_get_pathname_all($category);
?>
<div class="page-nav">
    <?php echo $category_path;
        $path = explode('<p>',$category_path);
        $path = $path[count($path)-1];
        $path = str_replace('</p>','',$path);
    ?>
    <script type="text/javascript">
        document.title=`Master Korean - <?php echo $path ?>`;
    </script>
    <div class="sub-mn"> 
        <span><?php echo get_string('menuicon', 'local_course') ?></span>
        <ul>
            <?php
            $ci = 0;
            $cata = local_course_category_depth(1);
            foreach ($cata as $ct) {
                $selected='';
                if($ci == 0){
                     $selected = 'class="selected"';
                }
                $cata1 = local_course_category_parent($ct->id);
                $url = '#';
                if (!$cata1) {
                    $url = $CFG->wwwroot . '/local/course/list.php?id=' . $ct->id;
                }
                ?>
                <li data-id="<?php echo $ct->id ?>" <?php echo $selected?>>
                    <a href="<?php echo $url ?>"><?php echo $ct->cname ?></a>
                    <ul>
                        <?php
                        if($cata1){
                            foreach($cata1 as $ct1){
                        ?>
                        <li><a href="/local/course/list.php?id=<?php echo $ct1->id?>"><?php echo $ct1->cname?></a></li>
                        <?php }}?>
                    </ul>
                </li> 
            <?php $ci++;} ?>
        </ul>
    </div>
</div>

<?php if($list){?>
<ul class="thumb-list style04">
    <?php 
    foreach($list as $li){  
    ?>
    <li>
        <div class="wp">
            <div class="img">      
                <span class="ic-heart <?php echo $li->likes ? 'on' : ''; ?>" id="like<?php echo $li->id?>" data-target="<?php echo $li->id?>"><?php echo get_string('like', 'local_course') ?></span>
                <?php if($li->badge){?>
                <p class="badge-area">
                    <?php foreach($li->badge as $badge){?>
                    <img src="<?php echo $badge->img?>">
                    <?php }?>
                </p>
                <?php }?>
                <a href='./detail.php?id=<?php echo $li->id?>'><img src="<?php echo $li->img?>" alt="<?php echo $li->lang_title?>" /></a>
            </div>

            <div class="txt">
                <div class="tit"><a href='./detail.php?id=<?php echo $li->id?>'><?php echo $li->lang_title?></a></div>
                <a href='./detail.php?id=<?php echo $li->id?>'>
                    <p>
                        <span><?php echo $li->teacher_list ?> <?php echo get_string('teacher', 'local_course') ?> </span>
                        <span><?php echo get_string('total', 'local_course') ?> <?php echo $li->lecturecnt?><?php echo get_string('class', 'local_course') ?></span>
                    </p>
                </a>
                <p class="t-gray"><?php echo get_string('courseperiod', 'local_course', $li->courseperiod) ?></p>
                    <?php if(partnerDiscount()) { ?>
                        <p class="d-inline-block fix-height"><del><?php echo number_format($li->price); ?><span> VND</span></del></p>
                        <p class="d-inline-block discount fix-height"><?php echo partnerDiscount(); ?><em>%</em></p>
                        <p style="font-weight: bold"><?php echo number_format($li->price - $li->price * partnerDiscount() / 100)?><em> VND</em></p>
                    <?php } ?>

                <?php global $USER;?>
                <?php if($USER->id < 1){?>
                	<a href='./detail.php?id=<?php echo $li->id?>' class="bt blue"><?php echo get_string('enrolment', 'local_course')?></a>
                <?php }else{?>
                    <?php if($li->my == 0){?>
                    <a href='./detail.php?id=<?php echo $li->id?>' class="bt <?php echo $li->button_color?>"><?php echo $li->button_txt?></a>
                    <?php }else{?>
                    <a href='./detail.php?id=<?php echo $li->id?>' class="bt gray"><?php echo get_string('mycourse', 'local_course')?></a>
                    <?php }?>
                <?php }?>
            </div>
        </div>
    </li>
<?php }?>
</ul>
<?php }else{?>
<div class="no-data">
    <div><?php echo get_string('empty', 'local_course') ?></div>
</div>
<?php }?>
<?php require ($CFG->dirroot . '/local/course/recommendpage.php');?>
<?php
echo $OUTPUT->footer();
?>



