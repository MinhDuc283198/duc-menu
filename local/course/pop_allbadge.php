<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

echo $OUTPUT->header();

$category_query = "select *, (SELECT count(*) FROM m_lmsdata_badge lb WHERE lb.category = lbc.id AND lb.used = 1 GROUP BY lb.category) cnt
                    from m_lmsdata_badge_category lbc
                    WHERE lbc.isused = 1 ORDER BY lbc.ordered ASC, lbc.id DESC";
$category = $DB->get_records_sql($category_query);
?>  
<div class="layerpop no-footer">
    <div class="pop-title">
        <?php echo get_string('allbadge','local_course')?>
        <a href="#" class="pop-close"><?php echo get_string('close','local_management')?></a>
    </div>
    
    <?php if($category){require_once($CFG->dirroot . '/local/lmsdata/lib.php');?>
    <div class="pop-contents">
        <?php 
        foreach($category as $ca){
            if($ca->cnt > 0){
            switch (current_language()) {
                case 'ko' :
                    $catitle = $ca->title_ko;
                    break;
                case 'en' :
                    $catitle = $ca->title_en;
                    break;
                case 'vi' :
                    $catitle = $ca->title_vi;
                    break;
            }
        ?>
        <div class="bdg-tit"><?php echo $catitle?></div>
        <ul class="bdg-list">
            <?php
            $badges = $DB->get_records('lmsdata_badge',array('category'=>$ca->id, 'used'=>1), 'ordered ASC, id DESC');
            foreach($badges as $badge){
                
                $now = time();
                $requery = "SELECT lc.*, lcr.type lcrtype, lcr.dataid, lb.id lbid  
                    FROM {lmsdata_class_relation} lcr 
                    JOIN {lmsdata_badge} lb ON lcr.dataid = lb.id 
                    JOIN {lmsdata_class} lc ON lc.id = lcr.classid
                    JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                    JOIN {course} c ON c.id = lco.courseid 
                    JOIN {course} mc2 ON lco.courseid = mc2.id
                    JOIN {course_categories} ca ON ca.id = mc2.category AND ca.visible = 1
                    LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id AND ca2.visible = 1
                    WHERE lcr.type = 3 AND lcr.dataid = :dataid AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0  
                    ORDER BY lc.id DESC, lc.timecreated DESC";
                $relation = $DB->get_records_sql($requery, array('dataid'=>$badge->id));
                $courseid_array = array();
                $courseid_list = '';
                if($relation){
                    foreach($relation as $rl){
                        $courseid_array[] = $rl->courseid;
                    }
                }
                $courseid_list = implode(',', $courseid_array);
                $img_path = local_lmsdata_get_imgpath(1, 'bedge', $badge->id)['path'];
                
                $iscom = $DB->get_fieldset_sql('SELECT timecompleted FROM {course_completions} WHERE userid = :userid and course IN( :courseid )',array('userid'=>$USER->id,'courseid'=>$courseid_list));
                $badgequery = "
                        select lbu.*, lb.id lbid  
                        from m_lmsdata_badge_user lbu
                        JOIN m_lmsdata_badge lb ON lbu.badgeid=lb.id
                        JOIN m_course c ON lbu.course = c.id
                        JOIN m_lmsdata_class lc ON c.id = lc.courseid AND lc.isused = 0  
                        JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id AND lco.isused = 0  
                        JOIN m_course  mc2 ON lco.courseid = mc2.id
                        JOIN m_course_categories  ca ON ca.id = mc2.category AND ca.visible = 1
                        left JOIN m_course_categories  ca2 ON ca.parent = ca2.id AND ca2.visible = 1
                        WHERE lbu.status = 1 AND lb.id = :lbid AND lbu.userid = :userid";
                $badgecompl = $DB->get_record_sql($badgequery, array('lbid'=>$badge->id, 'userid'=>$USER->id));
                if( (empty($iscom) || $iscom[0] <= 0) || !$badgecompl ){$completion_txt = '';} else { $completion_txt = 'on'; }
                
                switch (current_language()) {
                    case 'ko' :
                        $badgetitle = $badge->title_ko;
                        break;
                    case 'en' :
                        $badgetitle = $badge->title_en;
                        break;
                    case 'vi' :
                        $badgetitle = $badge->title_vi;
                        break;
                }
            
            ?>
            <li class="cursor <?php echo $completion_txt?>" data-id="<?php echo $badge->id?>">
                <!-- blue, yellow, green, level01~04-->
                <span class="badge-img">
                     <img src="<?php echo $img_path?>" />
                </span>
                <p><?php echo $badgetitle?></p>
            </li>
            <?php }?>
        </ul>
        <?php }}?>
    </div>
    <?php }?>
    
</div>


<?php
echo $OUTPUT->footer();
?>
