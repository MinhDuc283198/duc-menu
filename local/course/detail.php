<?php
global $DB,$PAGE,$CFG,$USER;
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/book/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('강좌상세');

echo $OUTPUT->header();

$id = optional_param('id', 0, PARAM_INT);

$list = local_course_class_id2($id);

$category_path = local_course_category_get_pathname_all($list->category);
$context = context_course::instance($list->lcocousreid);
$sumprice = $list->lcoprice + $list->ltprice;


$link_url = $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
?>  
<div class="page-nav has-icon">
    <?php echo $category_path ?>
    <div class="sub-mn">
        <span><?php echo get_string('menuicon', 'local_course') ?></span>
        <ul>
            <?php
            $ci = 0;
            $cata = local_course_category_depth(1);
            foreach ($cata as $ct) {
                $selected='';
                if($ci == 0){
                     $selected = 'class="selected"';
                }
                $cata1 = local_course_category_parent($ct->id);
                $url = '#';
                if (!$cata1) {
                    $url = $CFG->wwwroot . '/local/course/list.php?id=' . $ct->id;
                }
                ?>
                <li data-id="<?php echo $ct->id ?>" <?php echo $selected?>>
                    <a href="<?php echo $url ?>"><?php echo $ct->cname ?></a>
                    <ul>
                        <?php
                        if($cata1){
                            foreach($cata1 as $ct1){
                        ?>
                        <li><a href="/local/course/list.php?id=<?php echo $ct1->id?>"><?php echo $ct1->cname?></a></li>
                        <?php }}?>
                    </ul>
                </li> 
            <?php $ci++;} ?>
        </ul>
    </div>

    <div class="f-r">
        <a href="javascript:sharefacebook('<?php echo $link_url?>')" class="ic-share"><?php echo get_string('share', 'local_course') ?></a>
        <a href="#" class="ic-heart <?php echo $list->likes ? 'on' : ''; ?>" id="like<?php echo $id ?>" data-target="<?php echo $id ?>"><?php echo get_string('like', 'local_course') ?></a><!-- on -->
    </div>
</div>


<div class="course-info clearfix">
    <div class="crs-img">
        <?php
        if ($list->samplecontent) {
            $sql = "select "
                    . "rep.id , rep.referencecnt , rep.status, "
                    . "con.id as con_id,con.con_name,con.con_type,con.con_des,con.update_dt,con.data_dir,con.embed_type,con.embed_code, "
                    . "rep_group.name as gname "
                    . "from {lcms_repository} rep "
                    . "join {lcms_contents} con on con.id= rep.lcmsid "
                    . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
                    . "where rep.id= :id";
            $data = $DB->get_record_sql($sql, array('id' => $list->samplecontent));
        }
        if ($data->con_type == 'video') {
            $transcodingurl = get_config('moodle', 'vodurl');
            //동영상 콘텐츠 보기
            //$viewer_url = 'viewer/video_player.php?id=' . $data->con_id;
            $file = $DB->get_record('lcms_contents_file', array('con_seq' => $data->con_id));
            $viewer_url = $CFG->wwwroot . $file->filepath . '/' . $file->filename;
            $viewer_url = $transcodingurl . $data->data_dir . '/' . $file->filename;
            //                $viewer_url = $transcodingurl.'/jmedia/'.$data->data_dir.'/'.$file->filename;
            //                $viewer_url = '/local/repository/viewer/video_player.php?id=' . $data->con_id .'&site=siteadmin';
            //                $attfile .= '<iframe  id="vod_viewer" src="' . $viewer_url . '"  width="900px" height="720px"></iframe>';
            //                $output_lcms .= html_writer::tag('div', $attfile, array('class' => ""));
        } else if ($data->con_type == 'embed' || $data->con_type == 'youtube') {
            $aa = explode('/', $data->embed_code);
            $attfile = '<iframe id="vod_viewer" src="https://www.youtube.com/embed/' . $aa[3] . '" autoplay=0 ></iframe>';
            $output_lcms .= html_writer::tag('div', $attfile, array('class' => ""));
            //$output_lcms .= html_writer::end_tag('div');
        }else if($data->con_type == 'visangurl'){
            require_once($CFG->dirroot . '/local/management/contentsI/lib.php');
            $viewer_url = visang_cdn_tokenmaker($data->embed_code);
        }
        ?>
        <?php if ($data->con_type == 'video' || $data->con_type == 'visangurl') { ?>
            <video id="my-video" controls poster="<?php echo $list->img ?>">
                <source src="<?php echo $viewer_url ?>" type="video/mp4">
            </video>
            <p class="play">
                <span>play</span>
                <a href="#"><?php echo get_string('preview','local_course')?></a>
            </p>
            <!-- 동영상 재생 전 배경 -->
            <div class="bg"></div>
        <?php } ?>
        <?php
        if ($data->con_type == 'embed') {
            echo $output_lcms;
        }
        ?>
        <?php if (empty($data)) { ?>
            <img src="<?php echo $list->img ?>" alt="<?php echo $list->lang_title ?>" />
        <?php } ?>
    </div>
    <div class="crs-txt">
        <div class="tit"><?php echo $list->lang_title ?></div>
        <script type="text/javascript">
            document.title=`Master Korean - <?php echo $list->lang_title ?>`;
        </script>
        <div class="star-area">
            <div class="star-event red" data-num="<?php echo round($list->comments->avg->avg) ?>"></div>
            <span><?php echo round($list->comments->avg->avg) ?><?php echo get_string('score', 'local_course') ?></span><span class="gray"> (<?php echo $list->comments->total_lang ?><?php echo get_string('count', 'local_course') ?>)</span>
        </div>
        <ul>
            <li>
                <span><?php echo get_string('gangsa', 'local_course') ?></span>
                <strong><?php echo $list->teacher_list;?></strong>
            </li>
            <?php if ($list->type == 3) { ?>
                <li>
                    <span><?php echo get_string('textbook', 'local_course') ?></span>
                    <strong><?php echo $list->lang_textbookname ?></strong>
                </li>
            <?php } ?>
            <li>
                <span><?php echo get_string('courseperiod2', 'local_course') ?></span>
                <strong><?php echo $list->courseperiod_txt ?></strong>
            </li>
            <li>
                <span><?php echo get_string('studytime', 'local_course') ?></span>
                <?php
                $csdquery = "select cm.id, lcf.duration as duration  
                                from {course_sections} cs
                                JOIN {course_modules} cm ON cs.id = cm.section
                                JOIN {okmedia} o ON cs.course = o.course 
                                JOIN {lcms_contents_file} lcf ON o.contents = lcf.con_seq
                                WHERE cm.module = (select id from {modules} where name='okmedia') and cs.course=:mcid AND cm.instance = o.id
                                group by cm.id";
                $csdlsit = $DB->get_records_sql($csdquery, array('mcid' => $list->mcid));
                $sum = 0;
                if ($csdlsit) {
                    foreach ($csdlsit as $csl) {
                        $sum += $csl->duration;
                    }
                }

                $time_txt = '';
                if ($sum != 0) {
                    $time_txt .= gmdate('H', $sum) == 00 ? '00:' : gmdate('H', $sum) . ':';
                    $time_txt .= gmdate('i', $sum) == 00 ? '00:' : gmdate('i', $sum) . ':';
                    $time_txt .= gmdate('s', $sum);
                } else {
                    $time_txt .= '00:00:00';
                }
                ?><strong><?php echo $time_txt ?>(<?php echo $list->lecturecnt ?><?php echo get_string('class', 'local_course') ?>)</strong>
            </li>

        </ul>
        <?php 
        if($list->badge){
            foreach($list->badge as $badge){
        ?>
        <div class="badge-alarm">
            <img src="<?php echo $badge->img?>"><!-- yellow blue green --><?php echo get_string('badge1','local_course')?><span class="sub-pop t-blue" data-id="<?php echo $badge->id ?>" > <?php echo $badge->lang_title?></span><?php echo get_string('badge2','local_course')?>
        </div>
        <?php 
            }
        }
        ?>
        <!--<a href="#" class="btns blue">수강신청</a>-->
    </div>
    <?php 
    $keywords = explode('#', $list->lang_keywords);
    $keywords = array_filter($keywords);
        if(!empty($keywords)){
    ?>
    <div class="hash-group">
        <ul class="hash-area">
            <?php foreach($keywords as $keyword){?>
            <li data-title="<?php echo $keyword?>"><a href="#"><?php echo $keyword?></a></li>
            <?php }?>
        </ul>
    </div>
    <?php }?>
    <div class="sel-crs-list">
        <?php if ($list->type == 3) { ?>
            <p class="info"><?php echo get_string('includetextbook', 'local_course') ?></p>
        <?php } ?>
        <ul>
            <li>
                <span><?php echo get_string('course', 'local_course') ?></span>
                <strong><?php echo $list->lang_coursename ?></strong>
            </li>
            <?php if ($list->type == 3) { ?>
                <li>
                    <span><?php echo get_string('textbook', 'local_course') ?></span>
                    <strong><?php echo $list->lang_textbookname ?></strong>
                </li>
            <?php } ?>
        </ul>
        <p class="price">
            <?php if (partnerDiscount() && $list->price) { ?>
                <strong class="p-down"><span><?php echo number_format($list->price) ?> VND</span><span><?php echo partnerDiscount() ?>%</span></strong>
                <?php echo number_format($list->price - $list->price * partnerDiscount() / 100) ?><span>VND</span>
            <?php } else { ?>
            <?php if ($list->discount != 0) { ?>
                <strong class="p-down"><span><?php echo number_format($sumprice) ?> VND</span><span><?php echo $list->discount ?>%</span></strong>
            <?php } ?>
            <?php echo number_format($list->price) ?><span>VND</span>
            <?php } ?>
        </p>
        <?php if($USER->id < 1){?>
        	<input style="font-size: 22px;" type="button" value="<?php echo get_string('payment','local_course')?>" class="bt payment" />
        <?php } else {?>
        	<?php
            if($list->my == 0){
                $result = local_course_whether_enrol($list->courseid, $USER->id, 'student');
                if ($result == false) {
                    ?>
                    <input type="button" value="<?php echo get_string('payment','local_course')?>" class="bt payment" />
                    <?php
                } else {
                    ?>
                    <a href="/course/view.php?id=<?php echo $list->courseid ?>" class="bt gray" target="_blank"><?php echo $result?><br>(<?php echo get_string('classroom', 'local_course') ?>)</a>
                    <?php
                }
            }else{?>
                <a href="/course/view.php?id=<?php echo $list->courseid ?>" class="bt gray" target="_blank"><?php echo get_string('mycourse', 'local_course')?><br>(<?php echo get_string('classroom', 'local_course') ?>)</a>
            <?php }?>
        <?php }?>
        
        
    </div>
</div>

<ul class="crs-tab tab-event">
    <!-- tab event -->
    <li class="on" data-target=".data01"><a href="#"><?php echo get_string('cIntroduction', 'local_course') ?></a></li>
    <li data-target=".data02"><a href="#"><?php echo get_string('Instructor', 'local_course') ?></a></li>
    <?php if ($list->type == 3) { ?>
        <li data-target=".data03"><a href="#"><?php echo get_string('tIntroduction', 'local_course') ?></a></li>
    <?php } ?>
    <li data-target=".data04"><a href="#"><?php echo get_string('courselist', 'local_course') ?></a></li>
    <li data-target=".data05"><a href="#"><?php echo get_string('coursereviews', 'local_course') ?></a></li>
</ul>

<div class="crs-tab-cont target-area">
    <div class="data01 on">
        <h6><?php echo get_string('cIntroduction', 'local_course') ?></h6>
        <?php echo $list->lang_courseintro; ?>
    </div>
    <div class="data02">
        <h6><?php echo get_string('Instructor', 'local_course') ?></h6>

        <?php foreach ($list->teacher as $ta) { ?>
            <div class="teacher-bx">
                <div class="grp clearfix">
                    <div class="f-l">
                        <img src="<?php echo $ta->img ?>" alt="<?php echo $ta->fullname; ?>" />
                        <p><strong><?php echo $ta->fullname ?></strong>
                    </div>
                    <div class="f-r">
                        <div class="bx">
                            <div class="tit"><?php echo get_string('profile', 'local_course') ?></div>
                            <?php echo $ta->lang_profile; ?>
                        </div>
                        <div class="bx">
                            <div class="tit"><?php echo get_string('word', 'local_course') ?></div>
                            <?php echo $ta->lang_comment ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
    <?php if ($list->type == 3) { ?>
        <div class="data03">
            <h6><?php echo get_string('tIntroduction', 'local_course') ?></h6>
            <div class="book-bx">
                <div class="bk">
                    <img src="<?php echo $list->bookimg ?>" alt="<?php echo $list->lang_textbookname ?>" />
                </div>
                <div class="bk-info">
                    <div class="tit"><?php echo $list->lang_textbookname ?></div>
                    <div class="t-gray"><span><?php echo $list->lang_publisher ?></span><span><?php echo $list->lang_author ?></span></div>
                    <p><span><?php echo get_string('price', 'local_course') ?></span><strong><?php echo number_format($list->ltprice) ?>VND</strong></p>

                    <div class="txt">
                        <?php echo $list->lang_booktext ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="data04">
        <h6><?php echo get_string('courselist', 'local_course') ?></h6>
        <?php
        $csquery = "select * from {course_sections} where course=:mcid and section != 0 ORDER BY section ASC";
        $cslsit = $DB->get_records_sql($csquery, array('mcid' => $list->mcid));
        ?>
        <ul class="dt-crs-list">
            <?php
            if ($cslsit) {
                foreach ($cslsit as $cl) {
                    $csdquery = "select cm.id, lcf.duration as duration  
                            from {course_sections} cs
                            JOIN {course_modules} cm ON cs.id = cm.section
                            JOIN {okmedia} o ON cs.course = o.course 
                            JOIN {lcms_contents_file} lcf ON o.contents = lcf.con_seq
                            WHERE cm.module = (select id from {modules} where name='okmedia') and cs.course=:mcid and cs.section = :section  AND cm.instance = o.id
                            group by cm.id";
                    $csdlsit = $DB->get_records_sql($csdquery, array('mcid' => $list->mcid, 'section' => $cl->section));
                    $sum = 0;
                    if ($csdlsit) {
                        foreach ($csdlsit as $csl) {
                            $sum += $csl->duration;
                        }
                    }

                    if ($cl->name == '') {
                        $title = $cl->section . get_string('class2','local_course');
                    } else {
                        $title = $cl->name;
                    }
                    ?>
                    <li>
                        <span class="tit"><?php echo $title ?></span>
                        <span class="tm">
                            <?php
                            if ($sum != 0) {
                                echo gmdate('H', $sum) == 00 ? '' : gmdate('H', $sum) . get_string('time', 'local_course') . ' ';
                                echo gmdate('i', $sum) == 00 ? '' : gmdate('i', $sum) . get_string('minute', 'local_course') . ' ';
                                echo gmdate('s', $sum) . get_string('second', 'local_course');
                            }
                            ?>
                        </span>
                    </li>
                    <?php
                }
            }
            ?>

        </ul>
    </div>
    <div class="data05">
        <h6 class="has-star">
            <?php echo get_string('coursereviews', 'local_course') ?>
            <div class="f-r">
                <div class="star-event big" data-num="<?php echo round($list->comments->avg->avg) ?>"></div>
                <span><?php echo round($list->comments->avg->avg) ?><?php echo get_string('score', 'local_course') ?></span>
                <span class="t-gray">(<?php echo $list->comments->total_lang ?><?php echo get_string('count', 'local_course') ?>)</span>
            </div>
        </h6>

        <div class="input-review">
            <form action="add_review.php" method="post" enctype="multipart/form-data"> 
                <input type="hidden" name="moodlecourseid" value="<?php echo $id?>">
                <input type="hidden" name="id" value="<?php echo $coid?>">
                <div class="tp">
                    <?php echo get_string('selectstar', 'local_course') ?>
                    <div class="star-event hover" id="id_score_star" data-num="<?php echo $modicomment->score?>" onclick="score_star()"></div>
                    <input type="hidden" name="score" value="<?php echo $modicomment->score?>">
                </div>
                <div class="bt">
                    <textarea placeholder="<?php echo get_string('enterreview', 'local_course') ?>" name="contents"><?php echo $modicomment->contents?></textarea>
                    <input style="background: #485cc7; color: #fff; border:0" type="submit" class="btns" value="<?php echo get_string('enrollment', 'local_course'); ?>"  onclick="return return_check(<?php echo $id?>, <?php echo $USER->id?>, <?php echo $coid?>)"/>
                </div>
            </form>
        </div>

        <?php if ($list->comments->total_lang > 0) { ?>
            <ul class="review-list">
                <?php foreach ($list->comments->comment as $comment) { ?>
                    <li>
                        <div class="tp">
                            <strong><?php echo $comment->firstname ?></strong>
                            <span class="tm"><?php echo $comment->displaytime ?></span>
                            <div class="star-event" data-num="<?php echo $comment->score ?>"></div>
                        </div>
                        <div class="txt"><?php echo $comment->contents ?></div>
                    </li>
                <?php } ?>
            </ul>
        <?php } else { ?>


            <div class="no-data">
                <div><?php echo get_string('coursereviewsempty', 'local_course') ?></div>
            </div>
        <?php } ?>

    </div>
</div>

<?php require_once($CFG->dirroot . '/common/course_register.php'); ?>

<?php
//관련강좌
$relate_course_list = local_course_relate_course($id);
if ($relate_course_list) {
    ?>

    <h5 class="pg-tit"><?php echo get_string('rcourse', 'local_course') ?></h5>
    <div class="crs-bx">
        <ul class="crs-list">
            <?php foreach ($relate_course_list as $ra) { ?>
                <li onclick="location.href='./detail.php?id=<?php echo $ra->id ?>'">
                    <div class="img">
                        <span class="ic-heart <?php echo $ra->likes ? 'on' : ''; ?>" id="like<?php echo $ra->id ?>" data-target="<?php echo $ra->id ?>"><?php echo get_string('like', 'local_course') ?></span>
                        <img src="<?php echo $ra->img ?>" alt="<?php echo $ra->lang_title ?>" />
                    </div>
                    <div class="txt">
                        <div><?php echo $ra->lang_title ?></div>
                        <p>
                            <span> <?php echo $ra->teacher_list ?><?php echo get_string('teacher', 'local_course') ?>
                            </span>
                            <span><?php echo get_string('total', 'local_course') ?> <?php echo $ra->lecturecnt ?><?php echo get_string('class', 'local_course') ?></span>
                        </p>
                        <p class="t-gray"><?php echo $ra->courseperiod_txt ?></p>
                    </div>
                    <div class="bt-area">
                        <a href="./detail.php?id=<?php echo $ra->id ?>" class="btns <?php echo $ra->button_color ?>"><?php echo $ra->button_txt ?></a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>


<?php
//관련교재
$relate_course_list = local_book_relate_book($id);
if ($relate_course_list) {
    ?>
    <h5 class="pg-tit"><?php echo get_string('rtextbook', 'local_course') ?></h5>

    <div class="crs-bx">
        <ul class="crs-list book">
            <?php
            foreach ($relate_course_list as $ra) {
                ?>
                <li onclick="location.href='<?php echo $CFG->wwwroot ?>/local/book/detail.php?id=<?php echo $ra->id ?>'">
                    <div class="img">
                        <img src="<?php echo $ra->img ?>" alt="<?php echo $ra->lang_title ?>" />
                    </div>
                    <div class="txt">
                        <div><?php echo $ra->lang_title ?></div>
                        <p><span><?php echo $ra->lang_publisher ?></span><span><?php echo $ra->lang_author ?></span></p>
                        <p> <?php echo get_string('price', 'local_course') ?> 
                            <?php if ($ra->discount) { ?><strong class="p-down"><?php } ?><span><?php echo $ra->originprice_com ?> VND</span><?php if ($ra->discount) { ?><span><?php echo $ra->discount ?>%</span></strong><?php } ?>
                            <?php
                            if ($ra->discount) {
                                echo $ra->price_com
                                ?> VND<?php } ?>
                        </p>
                    </div>
                    <div class="bt-area">
                        <a href="<?php echo $CFG->wwwroot ?>/local/book/detail.php?id=<?php echo $ra->id ?>" class="btns br"><?php echo get_string('readmore', 'local_course') ?> </a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
<?php require ($CFG->dirroot . '/local/course/recommendpage.php');?>
<?php
echo $OUTPUT->footer();
?>
    
<!--    <meta property="og:title" content="<?php echo $list->lang_title?>" />
    <meta property="og:url" content="<?php echo $url?>" />
    <meta property="og:description" content="<?php echo $list->lang_contents?>" />
    <meta property="og:image" content="<?php echo $list->img?>" />-->
    
<script type="text/javascript">
    function sharefacebook(url) {
        window.open("http://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url));
    }
    $(function () {
        //배송정보입력 팝업 
        $(".payment").click(function () {
            if (not_login()) {
                utils.popup.call_layerpop("/local/payment/paypop.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"id": <?php echo $id ?>});
                return false;
            }
        });
    });  
    function pay_resultpop(result){
    utils.popup.close_pop($('.layerpop'));
        if (not_login()) {
            utils.popup.call_layerpop("/local/payment/payokpop.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"data": result});
            return false;
        }
    }
    function not_login() {
<?php if (!isloggedin()) { ?>
            if (confirm('<?php echo get_string('loginconfirm','local_book')?>')) {
                location.href = '/login/index.php';
            } else {
                return false;
            }
<?php } else { ?>
            return true;
<?php } ?>
    }
//    function payment_course(classid,payid) {
//        if (not_login()) {
//            //utils.popup.call_windowpop("/local/payment/init_payment.php?classid="+classid, 1200, 800,'payment');
//            $.ajax({
//                url: "/local/lmsdata/course_enrol_ajax.php",
//                type: 'post',
//                dataType: "json",
//                data: {classid: classid, type: 1,payid: payid},
//                success: function (result) {
//                    alert(result.msg);
//                    location.reload();
//                }
//            });
//        }
//    }

    //function productdelivery(bookid, type) {
    //    var address = $('input[name=address]').val();
    //    var address_detail = $('input[name=address_detail]').val();
    //    var phone1 = $('input[name=phone1]').val();
    //
    //    if(address == '' || address_detail == '' || phone1 == ''){
    //        alert('<?php //echo get_string('productdelivery:msg','local_course')?>//');
    //        return false;
    //    }
    //
    //    var result = call_ajax('shoppinginfo', {bookid: bookid, address: address + ' ' + address_detail, cell: phone1, type: type}, false);
    //    if (result.status == 'success') {
    //        payment_course(bookid,result.payid);
    //        utils.popup.close_pop($('.layerpop'));
    //    }
    //}
    
    $(function () {
        $('.play').click(function () {
            var video = $('#my-video');
            //video.attr('src', 'https://upload.wikimedia.org/wikipedia/en/d/dc/Pac-Man_Atari_2600_footage.ogv');
            video.get(0).play();
            $('.play').hide();
            $('.play').siblings(".bg").hide();
        });
        
        //배지 안내 팝업
        $(".badge-alarm>span").click(function () {
            utils.popup.call_layerpop("./pop_badge.php", {
                "width": "400px",
                "height": "auto",
                "bg": true,
                "callbackFn": function () {}
            }, {
                "id": $(this).data('id')
            });
        });
    });
    //배지 전체보기 팝업
    $(document).on("click", ".open-bdg-pop", function () {
        utils.popup.call_layerpop("./pop_allbadge.php", {
            "width": "800px",
            "height": "auto",
            "callbackFn": function () {}
        }, {
            "param": "11"
        });
    });
    
    //배지 안내 팝업
    $(document).on("click", ".bdg-list>li", function () {
        utils.popup.call_layerpop("./pop_badge_1.php", {
            "width": "400px",
            "height": "auto",
            "bg": true,
            "callbackFn": function () {}
        }, {
            "id": $(this).data('id')
        });
    });

    function score_star(){
        var score = $('#id_score_star .on').length;
        $('input[name=score]').val(score);
    }

    function return_check(courseid, userid, status){
        var result = call_ajax('overlap_courseview', {courseid:courseid, userid:userid, status:status}, false);
        if(result.status == "success") {
            if(result.list){
                alert('<?php echo get_string('alreadyreview', 'local_course') ?>');
                return false;
            }
        }else{
            alert(result.error);
            return false;
        }
        return true;
    }
</script>