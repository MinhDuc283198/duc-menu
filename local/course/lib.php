<?php

//course_categories 사용중인거 id 검색
function local_course_category_id($id) {
    global $DB;
    $query = 'select * from {course_categories} where id = :id and visible = :visible';
    return $DB->get_record_sql($query, array('id' => $id, 'visible' => 1));
}

//카테고리별 강좌
function local_course_class_category_id($category, $perpage = 0) {
    global $DB, $USER, $CFG;
    $now = time();

    $likesql = "";
    if (isloggedin()) {
        $getusersql = 'select * from {lmsdata_user} where userid = :userid';
        $getusertype = $DB->get_record_sql($getusersql, array('userid' => $USER->id));

        $likesql = ",(SELECT id FROM {lmsdata_like} WHERE userid = $USER->id AND classid = a.id ) as likes";
    }

    $lc_select = 'lc.id, lc.parentcourseid, lc.courseid, lc.learningstart, lc.learningend, lc.timecreated, lc.type, lc.price, 
                  lc.courseperiod, lc.reviewperiod, lc.isused, lc.bookid, mc.id as mcid , mc.category,   
                  lco.courseid as lcocousreid, lco.lecturecnt, lc.vi_title, lc.en_title, lc.title ';
    $select_cnt = "SELECT count(a.id) ";
    $select = "SELECT * $likesql ";
    $from = " FROM  
            (select $lc_select 
            from {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 1 AND lco.isused = 0 
            UNION
            select $lc_select 
            from {lmsdata_class} lc
            JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 3 AND lt.isused = 0 AND lco.isused = 0) a ";
    //이부분 아직 정해진거없어. 미정
    $where = " WHERE a.category = :category AND a.learningstart <= $now AND $now <= a.learningend AND a.isused = 0 ";
    $order = " ORDER BY a.id DESC, a.timecreated DESC";
    $params = array('category' => $category);

    if ($perpage) {
        $list = $DB->get_records_sql($select . $from . $where . $order, $params, 0, $perpage);
    } else {
        $list = $DB->get_records_sql($select . $from . $where . $order, $params);
    }

    $return = new stdClass();
    $return->list = $list;
    $return->cnt = $DB->count_records_sql($select_cnt . $from . $where . $order, $params);

    if ($return->cnt > 0) {
        foreach ($return->list as $key => $rl) {
            $teacher_list_array = array();
            $teacher = '';
            $query = "select lcp.teacherid from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $rl->parentcourseid;
            $teacher_array = $DB->get_records_sql($query);

            $context = context_course::instance($rl->lcocousreid);
            $courseimg = local_course_get_imgpath($context->id);
            $teacher_my = 0;
            foreach ($teacher_array as $tak => $ta) {
                $my = $ta->teacherid == $USER->id;
                $teacher_my += $my;
                $user = local_course_teacher_id($ta->teacherid);
                $teacher_list_array[] = $user->firstname;
                $teacher = implode(',', $teacher_list_array);
            }
            $return->list[$key]->my = $teacher_my;
            $return->list[$key]->img = $courseimg;
            $return->list[$key]->teacher_list = $teacher;
            $return->list[$key]->courseperiod_txt = get_string('courseperiod', 'local_course', $rl->courseperiod);
            $return->list[$key]->button_txt = (local_course_whether_enrol($rl->courseid, $USER->id, 'student') == false) ? get_string('enrolment', 'local_course') : local_course_whether_enrol($rl->courseid, $USER->id, 'student');
            $return->list[$key]->button_color = (local_course_whether_enrol($rl->courseid, $USER->id, 'student') == false) ? '' : 'gray';
            
            $badgequery = 'SELECT * FROM {lmsdata_class_relation} lcr JOIN {lmsdata_badge} lb ON lcr.dataid = lb.id WHERE lcr.type = 3 AND lcr.classid = :classid ORDER BY lb.ordered ASC, lb.id DESC';
            $badgelist = $DB->get_records_sql($badgequery, array('classid'=>$rl->id));
            
            if($badgelist){
                require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                foreach($badgelist as $badgekey => $badge){
                    $return->list[$key]->badge[$badgekey]->img = local_lmsdata_get_imgpath(1, 'bedge', $badge->id)['path'];
                }
            }

            
            //print_object($return);
            
            switch (current_language()) {
                case 'ko' :
                    $return->list[$key]->lang_title = $rl->title;
                    break;
                case 'en' :
                    $return->list[$key]->lang_title = $rl->en_title;
                    break;
                case 'vi' :
                    $return->list[$key]->lang_title = $rl->vi_title;
                    break;
            }
        }
    }
    return $return;
}

//강좌 검색
function local_course_class_search_title($search, $page, $perpage = 5) {
    global $DB, $USER, $CFG;
    $now = time();
    $likesql = "";
    if (isloggedin()) {
        $getusersql = 'select * from {lmsdata_user} where userid = :userid';
        $getusertype = $DB->get_record_sql($getusersql, array('userid' => $USER->id));

        $likesql = ",(SELECT id FROM {lmsdata_like} WHERE userid = $USER->id AND classid = a.id ) as likes";
    }

    $select_cnt = "select count(a.id) ";
    $lc_select = 'lc.id, lc.parentcourseid, lc.courseid, lc.learningstart, lc.learningend, lc.timecreated, lc.type, lc.code, lc.price, 
                  lc.courseperiod, lc.reviewperiod, lc.isused, lc.bookid, lc.discount,lco.coursecd as lcocode, lco.price as lcoprice, mc.id as mcid , mc.category,   
                  lco.courseid as lcocousreid, lco.lecturecnt, lco.samplecontent, lc.vi_title, lc.en_title, lc.title, lc.keywords, lc.keywords_vi, lc.keywords_en ';

    $select = "SELECT * $likesql ";
    $from = " FROM  
            (select $lc_select,  '' as ltid, '' as lttitle, '' as ltcode , '' as ltprice, '' as text, '' as author, '' as publisher  
            from {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 1  AND lco.isused = 0  
            UNION
            select $lc_select, lt.id as ltid, lt." . $lang . "title as lttitle, lt.code as ltcode , lt.price as ltprice, lt." . $lang . "text as text, lt." . $lang . "author as author, (select title" . $lang2 . " from {lmsdata_copyright} where id=lt.publisher)  as publisher 
            from {lmsdata_class} lc
            JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 3 AND lt.isused = 0 AND lco.isused = 0) a ";
    //미정
    $where = " WHERE (a.title like :title or a.vi_title like :vi_title or a.en_title like :en_title or a.keywords like :keywords or a.keywords_vi like :keywords_vi or a.keywords_en like :keywords_en ) AND a.learningstart <= $now AND $now <= a.learningend AND a.isused = 0 ";
    $order = " ORDER BY a.id DESC, a.timecreated DESC";
    $params = array('title' => '%' . $search . '%', 'vi_title' => '%' . $search . '%', 'en_title' => '%' . $search . '%', 'keywords' => '%' . $search . '%', 'keywords_en' => '%' . $search . '%', 'keywords_vi' => '%' . $search . '%');

    $return = new stdClass();
    $return->list = $DB->get_records_sql($select . $from . $where . $order, $params, $page * $perpage, $perpage);
    $return->cnt = $DB->count_records_sql($select_cnt . $from . $where . $order, $params);


    if ($return->cnt > 0) {
        foreach ($return->list as $key => $rl) {
            $teacher = '';
            $teacher_list_array = array();
            $query = "select lcp.teacherid from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $rl->parentcourseid;
            $teacher_array = $DB->get_records_sql($query);

            $context = context_course::instance($rl->lcocousreid);
            $courseimg = local_course_get_imgpath($context->id);
            foreach ($teacher_array as $tak => $ta) {
                $user = local_course_teacher_id($ta->teacherid);
                $teacher_list_array[] = $user->firstname;
                $teacher = implode(',', $teacher_list_array);
            }
            $return->list[$key]->img = $courseimg;
            $return->list[$key]->teacher_list = $teacher;
            $return->list[$key]->courseperiod_txt = get_string('courseperiod', 'local_course', $rl->courseperiod);
            $return->list[$key]->button_txt = (local_course_whether_enrol($rl->courseid, $USER->id, 'student') == false) ? get_string('enrolment', 'local_course') : local_course_whether_enrol($rl->courseid, $USER->id, 'student');
            $return->list[$key]->button_color = (local_course_whether_enrol($rl->courseid, $USER->id, 'student') == false) ? '' : 'gray';
            
            
            $badgequery = 'SELECT * FROM {lmsdata_class_relation} lcr JOIN {lmsdata_badge} lb ON lcr.dataid = lb.id WHERE lcr.type = 3 AND lcr.classid = :classid ORDER BY lb.ordered ASC, lb.id DESC';
            $badgelist = $DB->get_records_sql($badgequery, array('classid'=>$rl->id));
            if($badgelist){
                require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                foreach($badgelist as $badgekey => $badge){
                    $return->list[$key]->badge[$badgekey]->img = local_lmsdata_get_imgpath(1, 'bedge', $badge->id)['path'];
                }
            }
            
            switch (current_language()) {
                case 'ko' :
                    $return->list[$key]->lang_title = $rl->title;
                    $return->list[$key]->lang_keywords = $rl->keywords;
                    break;
                case 'en' :
                    $return->list[$key]->lang_title = $rl->en_title;
                    $return->list[$key]->lang_keywords = $rl->keywords_en;
                    break;
                case 'vi' :
                    $return->list[$key]->lang_title = $rl->vi_title;
                    $return->list[$key]->lang_keywords = $rl->keywords_vi;
                    break;
            }
        }
    }

    return $return;
}


function local_course_class_id2($id) {
    global $DB, $USER, $CFG;

    $now = time();
    $likesql = "";
    if (isloggedin()) {
        $getusersql = 'select * from {lmsdata_user} where userid = :userid';
        $getusertype = $DB->get_record_sql($getusersql, array('userid' => $USER->id));

        $likesql = ",(SELECT id FROM {lmsdata_like} WHERE userid = $USER->id AND classid = a.id ) as likes";
    }

    $select_cnt = "select count(a.id) ";
    $lc_select = 'lc.id, lc.parentcourseid, lc.courseid, lc.learningstart, lc.learningend, lc.timecreated, lc.type, lc.code, lc.price, 
                  lc.courseperiod, lc.reviewperiod, lc.isused, lc.bookid, lc.discount,lco.coursecd as lcocode, lco.price as lcoprice, mc.id as mcid , mc.category,   
                  lco.courseid as lcocousreid, lco.lecturecnt, lco.samplecontent, lc.vi_title, lc.en_title, lc.title, lco.intro, lco.vi_intro, lco.en_intro, lco.coursename, lco.vi_coursename, lco.en_coursename,
                  lc.keywords, lc.keywords_en, lc.keywords_vi ';

    $t_select1 = " '' as ltid, '' as lttitle, '' as vi_lttitle, '' as en_lttitle, '' as ltprice, '' as text, '' as vi_text, '' as en_text,'' as author, '' as vi_author,  '' as en_author, '' as publisher, '' as vi_publisher, '' as en_publisher ";

    $t_select2 = " lt.id as ltid, lt.title as lttitle, lt.vi_title as vi_lttitle, lt.en_title as en_lttitle, lt.price as ltprice, lt.text, lt.vi_text, lt.en_text, lt.author, lt.vi_author, lt.en_author, 
                  (select title from {lmsdata_copyright} where id=lt.publisher)  as publisher, 
                  (select titlevn from {lmsdata_copyright} where id=lt.publisher)  as vi_publisher,
                  (select titleen from {lmsdata_copyright} where id=lt.publisher)  as en_publisher ";
    $select = "SELECT * $likesql ";
    $from = " FROM  
            (select $lc_select, $t_select1  
            from {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 1 AND lco.isused = 0 
            UNION
            select $lc_select, $t_select2 
            from {lmsdata_class} lc
            JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 3 AND lco.isused = 0 AND lt.isused = 0 ) a ";
    $where = " WHERE a.id=:id AND a.learningstart <= $now AND $now <= a.learningend AND a.isused = 0 ";
    $order = " ORDER BY a.id DESC, a.timecreated DESC";
    $params = array('id' => $id);


    $return = $DB->get_record_sql($select . $from . $where . $order, $params);

    if ($return) {
        //강사소개
        $teacher = '';
        $teacher_list_array = array();
        $query = "select lcp.teacherid, u.firstname, u.lastname, lt.profile,lt.profile_en,lt.profile_vi, lt.comment, lt.comment_en, lt.comment_vi from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid JOIN {lmsdata_teacher} lt ON lt.userid=lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $return->parentcourseid;
        $return->teacher_list = $teacher_list = $DB->get_records_sql($query);

        $context = context_course::instance($return->lcocousreid);
        $courseimg = local_course_get_imgpath($context->id);
        $teacher_my = 0;
        foreach ($teacher_list as $tak => $ta) {
            $my = $ta->teacherid == $USER->id;
            $teacher_my += $my;
            $user = $DB->get_record('user',array('id'=>$ta->teacherid));
            $teacher_list_array[] = $ta->firstname;
            $teacher = implode(',', $teacher_list_array);
            $teacherimgpath = local_course_teacher_get_imgpath($user); 
            $return->teacher[$tak]->img = $teacherimgpath;
            $return->teacher[$tak]->fullname = $ta->firstname;
//            $return->teacher[$tak]->profile = $ta->profile;
//            $return->teacher[$tak]->comment = $ta->comment;
            
            switch (current_language()) {
                case 'ko' :
                    $return->teacher[$tak]->lang_profile = $ta->profile;
                    $return->teacher[$tak]->lang_comment = $ta->comment;
                    break;
                case 'en' :
                    $return->teacher[$tak]->lang_profile = $ta->profile_en;
                    $return->teacher[$tak]->lang_comment = $ta->comment_en;
                    break;
                case 'vi' :
                    $return->teacher[$tak]->lang_profile = $ta->profile_vi;
                    $return->teacher[$tak]->lang_comment = $ta->comment_vi;
                    break;
            }
        }
        $return->my = $teacher_my;
        //교재소개
        if ($return->type == 3) {
            $return->bookimg = local_course_book_get_imgpath($return->ltid);
        }
        //강좌목록
        //샘플영상
        //수강후기
        $return->comments = local_lmsdata_get_course_like($return->courseid);


        $return->img = $courseimg;
        $return->teacher_list = $teacher;
        $return->courseperiod_txt = get_string('courseperiod3', 'local_course', $return->courseperiod);

        $return->button_txt = (local_course_whether_enrol($return->courseid, $USER->id, 'student') == false) ? get_string('enrolment', 'local_course') : get_string('takecourse', 'local_course');
        $return->button_color = (local_course_whether_enrol($return->courseid, $USER->id, 'student') == false) ? '' : 'gray';
        
        
        $badgequery = 'SELECT * FROM {lmsdata_class_relation} lcr JOIN {lmsdata_badge} lb ON lcr.dataid = lb.id WHERE lcr.type = 3 AND lcr.classid = :classid ORDER BY lb.ordered ASC, lb.id DESC';
        $badgelist = $DB->get_records_sql($badgequery, array('classid'=>$return->id));
        if($badgelist){
            require_once($CFG->dirroot . '/local/lmsdata/lib.php');
            foreach($badgelist as $badgekey => $badge){
                $return->badge[$badgekey]->img = local_lmsdata_get_imgpath(1, 'bedge', $badge->id)['path'];
                $return->badge[$badgekey]->id = $badge->id;
                //$return->badge[$badgekey]->title = local_lmsdata_get_imgpath(1, 'bedge', $badge->id)['path'];
                switch (current_language()) {
                    case 'ko' :
                        $return->badge[$badgekey]->lang_title = $badge->title_ko;
                        break;
                    case 'en' :
                        $return->badge[$badgekey]->lang_title = $badge->title_en;
                        break;
                    case 'vi' :
                        $return->badge[$badgekey]->lang_title = $badge->title_vi;
                        break;
                }
            }
        }

        switch (current_language()) {
            case 'ko' :
                $return->lang_title = $return->title;
                $return->lang_coursename = $return->coursename;
                $return->lang_textbookname = $return->lttitle;
                $return->lang_onelineintro = $return->onelineintro;
                $return->lang_courseintro = $return->intro;
                $return->lang_booktext = $return->text;
                $return->lang_publisher = $return->publisher;
                $return->lang_author = $return->author;
                $return->lang_keywords = $return->keywords;
                break;
            case 'en' :
                $return->lang_title = $return->en_title;
                $return->lang_coursename = $return->en_coursename;
                $return->lang_textbookname = $return->en_lttitle;
                $return->lang_onelineintro = $return->en_onelineintro;
                $return->lang_intro = $return->en_lttitle;
                $return->lang_courseintro = $return->en_intro;
                $return->lang_booktext = $return->en_text;
                $return->lang_publisher = $return->en_publisher;
                $return->lang_author = $return->en_author;
                $return->lang_keywords = $return->keywords_en;
                break;
            case 'vi' :
                $return->lang_title = $return->vi_title;
                $return->lang_coursename = $return->vi_coursename;
                $return->lang_textbookname = $return->vi_lttitle;
                $return->lang_onelineintro = $return->vi_onelineintro;
                $return->lang_intro = $return->vi_lttitle;
                $return->lang_courseintro = $return->vi_intro;
                $return->lang_booktext = $return->vi_text;
                $return->lang_publisher = $return->vi_publisher;
                $return->lang_author = $return->vi_author;
                $return->lang_keywords = $return->keywords_vi;
                break;
        }
    }

    return $return;
}

//관련강좌
function local_course_relate_course($id) {
    global $DB, $USER;

    $now = time();
    $likesql = "";
    if (isloggedin()) {
        $getusersql = 'select * from {lmsdata_user} where userid = :userid';
        $getusertype = $DB->get_record_sql($getusersql, array('userid' => $USER->id));

        $likesql = ",(SELECT id FROM {lmsdata_like} WHERE userid = $USER->id AND classid = lc.id ) as likes";
    }
    //관련강좌
    $relation_course_query = "SELECT lc.id, lc.title, lc.vi_title, lc.en_title  $likesql, lco.courseid as lcocousreid, lc.parentcourseid ,lc.courseperiod,lco.lecturecnt, lc.courseid, cr.type, lc.type as lctype, lc.isused as lcisused, lco.isused,lc.learningstart, lc.learningend    
              FROM {lmsdata_class_relation} cr 
              JOIN {lmsdata_class} lc ON cr.dataid = lc.id
              JOIN {course} mc ON lc.courseid = mc.id
              JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
              JOIN {course} mc2 ON lco.courseid = mc2.id
              JOIN {course_categories} ca ON ca.id = mc2.category
              left JOIN {course_categories} ca2 ON ca.parent = ca2.id
              WHERE ( lc.type = :type1 or lc.type = :type2) AND cr.classid = :classid  AND cr.type = :crtype 
              AND lc.learningstart <= :learningstart AND :learningend <= lc.learningend AND lc.isused = :isused AND lco.isused = :lcoisused ";
    $relation_course_params = array('type1' => 1, 'type2' => 3, 'learningstart' => $now, 'learningend' => $now, 'isused' => 0,'lcoisused'=>0, 'classid' => $id, 'crtype' => 1);
    $relation_course = $DB->get_records_sql($relation_course_query . $order_by, $relation_course_params);
    $return = $relation_course;
    foreach ($relation_course as $rkey => $rc) {
        $context = context_course::instance($return[$rkey]->lcocousreid);
        $return[$rkey]->img = local_course_get_imgpath($context->id);
        
        //강사
        $teacher = '';
        $teacher_list_array = array();
        $query = "select lcp.teacherid, u.firstname, u.lastname, lt.profile, lt.comment from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid JOIN {lmsdata_teacher} lt ON lt.userid=lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=".$return[$rkey]->parentcourseid;
        $return[$rkey]->teacher_list = $teacher_list = $DB->get_records_sql($query);

        foreach ($teacher_list as $tak => $ta) {
            $user = local_course_teacher_id($ta->teacherid);
            $teacher_list_array[] = $user->firstname;
            $teacher = implode(',', $teacher_list_array);
        }
        $return[$rkey]->teacher_list = $teacher;
        $return[$rkey]->courseperiod_txt = get_string('courseperiod','local_course',$return[$rkey]->courseperiod);
        $return[$rkey]->button_txt = (local_course_whether_enrol($return[$rkey]->courseid, $USER->id, 'student') == false) ? get_string('enrolment', 'local_course'):get_string('takecourse', 'local_course') ; 
        $return[$rkey]->button_color = (local_course_whether_enrol($return[$rkey]->courseid, $USER->id, 'student') == false) ? 'blue':'gray' ; 

        switch (current_language()) {
            case 'ko' :
                $return[$rkey]->lang_title = $rc->title;
                break;
            case 'en' :
                $return[$rkey]->lang_title = $rc->en_title;
                break;
            case 'vi' :
                $return[$rkey]->lang_title = $rc->vi_title;
                break;
        }
    }
    return $return;
}

//course_categories 경로 가져오기 ( XX > OO )
function local_course_category_get_pathname($id) {
    global $DB;
    $now = time();
    $currentlang = current_language();
    $cntquery = "select count(lc.id)  
                from {lmsdata_class} lc
                JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                WHERE lco.detailprocess=:id   AND lc.learningstart <= $now AND $now <= lc.learningend  AND lc.isused = 0 AND lco.isused = 0  ";
    $course_cnt = $DB->count_records_sql($cntquery, array('id' => $id));

    $lang = '';
    if ($currentlang == 'ko') {
        $lang = 'ko_';
        $select = 'cc.name as cname';
        $select1 = 'cc1.name as cname';
    } else if ($currentlang == 'vi') {
        $lang = 'vi_';
        $select = 'ccl.' . $lang . "name as cname";
        $select1 = 'ccl.' . $lang . "name as cname";
    } else if ($currentlang == 'en') {
        $lang = 'en_';
        $select = 'ccl.' . $lang . "name as cname";
        $select1 = 'ccl.' . $lang . "name as cname";
    }

    $category_select = 'select cc.*, ' . $select;
    $category_from = ' from {course_categories} cc LEFT JOIN {course_categories_lang} ccl  ON cc.id = ccl.categoryid ';
    $category_where = ' where cc.id = :id and cc.visible = :visible';
    $category_order = ' ORDER BY cc.sortorder ASC';

    $category_list = $DB->get_record_sql($category_select . $category_from . $category_where . $category_order, array('id' => $id, 'visible' => 1));
    $name = '<p>' . $category_list->cname . '</p>';

    if ($category_list->parent != 0) {
        if (local_course_category_get_pathname($category_list->parent) != '<p></p>') {
            //$data = local_course_category_get_pathname($category_list->parent).$data;
            $c_select = 'select cc1.*, ' . $select1;
            $c_from = ' from {course_categories} cc1  LEFT JOIN {course_categories} cc2 ON cc1.id = cc2.parent LEFT JOIN {course_categories_lang} ccl ON cc1.id = ccl.categoryid';
            $c_where = ' where cc1.id = :id and cc1.visible = 1 and cc2.visible =1';
            $c_order = ' ORDER BY cc1.sortorder ASC';

            $c_list = $DB->get_record_sql($c_select . $c_from . $c_where . $c_order, array('id' => $category_list->parent));
            if ($c_list) {
                if ($course_cnt > 0) {
                    $return['path'] = '<p>' . $c_list->cname . '</p>' . $name;
                    $return['id'] = $id;
                }
            }
        }
    } else {
        if ($course_cnt > 0) {
            $return['path'] = $name;
            $return['id'] = $id;
            $return['cnt'] = $course_cnt;
        }
    }

    return $return;
}

//course_categories 경로 가져오기 ( XX > OO )
function local_course_category_get_pathname_all($id) {
    global $DB;
    $currentlang = current_language();
    $lang = '';
    if ($currentlang == 'ko') {
        $lang = 'ko_';
        $select = 'cc.name as cname';
    } else if ($currentlang == 'vi') {
        $lang = 'vi_';
        $select = 'ccl.' . $lang . "name as cname";
    } else if ($currentlang == 'en') {
        $lang = 'en_';
        $select = 'ccl.' . $lang . "name as cname";
    }


    $category_select = 'select cc.*, ' . $select;
    $category_from = ' from {course_categories} cc LEFT JOIN {course_categories_lang} ccl  ON cc.id = ccl.categoryid ';
    $category_where = ' where cc.id = :id and cc.visible = :visible';
    $category_order = ' ORDER BY cc.sortorder ASC';

    $category_list = $DB->get_record_sql($category_select . $category_from . $category_where . $category_order, array('id' => $id, 'visible' => 1));
    $data = '<p>' . $category_list->cname . '</p>';

    if ($category_list->parent != 0) {
        $data = local_course_category_get_pathname_all($category_list->parent) . $data;
    }

    return $data;
}

//course_categories depth 로 검색
function local_course_category_depth($depth) {
    global $DB;

    $currentlang = current_language();
//     $lang = '';
//     if ($currentlang == 'ko') {
//         $lang = 'ko_';
//         $select = 'cc.name as cname';
//     } else if ($currentlang == 'vi') {
//         $lang = 'vi_';
//         $select = 'ccl.' . $lang . "name as cname";
//     } else if ($currentlang == 'en') {
//         $lang = 'en_';
//         $select = 'ccl.' . $lang . "name as cname";
//     }

    $category_select = "select cc.*, ccl.".$currentlang."_name as cname 
                        from m_course_categories cc 
                        JOIN m_course_categories_lang ccl  ON cc.id = ccl.categoryid  where cc.depth = :depth and cc.visible = :visible order by sortorder asc ";
    

    return $category_list = $DB->get_records_sql($category_select, 
        array('depth' => $depth, 'visible' => 1, "lang" =>$currentlang));

    //return $DB->get_records('course_categories', array('visible' => 1, 'depth' => $depth));
}

//course_categories parent 로 검색
function local_course_category_parent($parent) {
    global $DB;

    $currentlang = current_language();
//     $lang = '';
//     if ($currentlang == 'ko') {
//         $lang = 'ko_';
//         $select = 'cc.name as cname';
//     } else if ($currentlang == 'vi') {
//         $lang = 'vi_';
//         $select = 'ccl.' . $lang . "name as cname";
//     } else if ($currentlang == 'en') {
//         $lang = 'en_';
//         $select = 'ccl.' . $lang . "name as cname";
//     }

       $category_select = "select cc.*, ccl.".$currentlang."_name as cname 
                        from m_course_categories cc 
                        JOIN m_course_categories_lang ccl  ON cc.id = ccl.categoryid  where cc.visible = :visible and cc.parent = :parent order by sortorder asc ";
    

    
    return $category_list = $DB->get_records_sql($category_select,
        array('parent' => $parent, 'visible' => 1, "lang" =>$currentlang));

//     $category_select = 'select cc.*, ' . $select;
//     $category_from = ' from {course_categories} cc LEFT JOIN {course_categories_lang} ccl  ON cc.id = ccl.categoryid ';
//     $category_where = ' where cc.parent = :parent and cc.visible = :visible';
//     $category_order = ' ORDER BY cc.sortorder ASC';

//     return $category_list = $DB->get_records_sql($category_select . $category_from . $category_where . $category_order, array('parent' => $parent, 'visible' => 1));
}

//강좌 이미지 가져오기
function local_course_get_imgpath($contextid) {
    global $DB;
    $courseid = $DB->get_field('context','instanceid',array('id'=>$contextid));
    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_courselist', 'thumbnailimg', $courseid, "", false);
    if (!empty($files)) {
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_courselist/thumbnailimg/' . $courseid. '/' . $filename);

            return $path;
        }
    } else {
        $path = $CFG->wwwroot . '/theme/oklassedu/pix/images/default_course.png';
        return $path;
    }
}

//lmsdata_teacher id 로 검색
function local_course_teacher_id($id) {
    global $DB;

    $query = 'select * from  {lmsdata_teacher}  lit 
                JOIN  {lmsdata_user}  lu ON lu.userid = lit.userid
                JOIN {user} u ON lu.userid = u.id
                WHERE lit.userid = :userid';
    $user = $DB->get_record_sql($query, array('userid' => $id));
    $user->fullname = fullname($user);
    return $user;
}

//선생님 사진 가져오기
function local_course_teacher_get_imgpath($user) {
    global $PAGE;
    $userpicture = new user_picture($user);
    $userpicture->size = 101;
    $user->picture = $userpicture->get_url($PAGE)->out(false);
    return $user->picture;
}

//lmsdata_textbook id 로 검색
function local_course_book_id($id) {
    global $DB;
    //return $DB->get_record('lmsdata_textbook',array('id'=>$id, 'isused'=>0));
    $query = 'select lt.*, (select title from {lmsdata_copyright} where id=copyright)  as copyright, (select title from {lmsdata_copyright} where id=publisher)  as publisher  from {lmsdata_textbook} lt WHERE lt.id = :id AND lt.isused=:isused';
    return $DB->get_record_sql($query, array('id' => $id, 'isused' => 0));
}

//교재 이미지 가져오기
function local_course_book_get_imgpath($id) {
    $fs = get_file_storage();
    $files = $fs->get_area_files(1, 'local_lmsdata', 'textbook', $id, "", false);
    if (!empty($files)) {
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . 1 . '/local_lmsdata/textbook/' . $id . '/' . $filename);
            return $path;
        }
    } else {
        $path = $CFG->wwwroot . '/theme/oklassedu/pix/images/default_book.png';
        return $path;
    }
}

//교재 상품
function local_course_book_relation_id($id) {
    global $DB;
    $now = time();
    $query = "select lc.* , '' as lcoprice ,lt.price as ltprice, '' as lconame, '' as lcocode, lt.title as lttitle, lt.code as ltcode, lcp.title as publisher, lt.author  
                from {lmsdata_class} lc
                JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id 
                JOIN {lmsdata_copyright} lcp ON lcp.id = lt.publisher  
                WHERE lc.type = 2  and lc.id=:id and lcp.type = 1   AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0 AND lt.isused = 0 ";
    return $DB->get_record_sql($query, array('id' => $id));
}

//메인에 추천강좌 
function local_course_class_recommend_list($type, $limit = null) {
    global $DB, $USER;
    $currentlang = current_language();

    $now = time();
    $likesql = "";
    if (isloggedin()) {
        $getusersql = 'select * from {lmsdata_user} where userid = :userid';
        $getusertype = $DB->get_record_sql($getusersql, array('userid' => $USER->id));

        $likesql = ",(SELECT id FROM {lmsdata_like} WHERE userid = $USER->id AND classid = a.id ) as likes";
    }


    $lang = '';
    $lang2 = '';
    if ($currentlang == 'ko') {
        $lang = '';
        $lang2 = '';
    } else if ($currentlang == 'vi') {
        $lang = 'vi_';
        $lang2 = 'vn';
    } else if ($currentlang == 'en') {
        $lang = 'en_';
        $lang2 = 'en';
    }
    $lc_select = 'lc.id, lc.parentcourseid, lc.courseid, lc.learningstart, lc.learningend, lc.timecreated, lc.type, lc.code, lc.price,
                (SELECT count(id) FROM {lmsdata_like} WHERE classid = lc.id) as totallikes, 
                  lc.courseperiod, lc.reviewperiod, lc.isused, lc.bookid, lc.discount, lc.' . $lang . 'title as title,  
                  lco.' . $lang . 'coursename as coursename, lco.' . $lang . 'onelineintro as onelineintro, lco.' . $lang . 'intro as intro, lco.coursecd as lcocode, lco.price as lcoprice, mc.id as mcid , mc.category,   
                  lco.courseid as lcocourseid, lco.lecturecnt, lco.samplecontent, lmc.colorcode,lmc.best, lmc.type as lmctype ';
    $select = "SELECT * $likesql ";
    $from = " FROM  
            (select $lc_select,  '' as ltid, '' as lttitle, '' as ltcode , '' as ltprice, '' as text, '' as author, '' as publisher  
            from {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
	JOIN {lmsdata_main_course} lmc ON lc.id = lmc.courseid 
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 1 AND lco.isused = 0 
            UNION
            select $lc_select, lt.id as ltid, lt." . $lang . "title as lttitle, lt.code as ltcode , lt.price as ltprice, lt." . $lang . "text as text, lt." . $lang . "author as author, (select title" . $lang2 . " from {lmsdata_copyright} where id=lt.publisher)  as publisher 
            from {lmsdata_class} lc
            JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
	JOIN {lmsdata_main_course} lmc ON lc.id = lmc.courseid 
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 3 AND lco.isused = 0 AND lt.isused=0 ) a ";
    $where = " WHERE a.lmctype=:lmctype AND a.learningstart <= $now AND $now <= a.learningend AND a.isused = 0";
    if($limit) {
        $order = " ORDER BY RAND() LIMIT $limit";
    } else {
        $order = " ORDER BY RAND() LIMIT 4";
    }

    $params = array('lmctype' => $type);
    return $DB->get_records_sql($select . $from . $where . $order, $params);
}

//강좌 하위 카테고리 불러오기
function local_coruse_categories_last() {
    global $DB;
    $currentlang = current_language();
    $lang = '';
    if ($currentlang == 'ko') {
        $lang = 'ko_';
        $select = 'cc.name as cname';
    } else if ($currentlang == 'vi') {
        $lang = 'vi_';
        $select = 'ccl.' . $lang . "name as cname";
    } else if ($currentlang == 'en') {
        $lang = 'en_';
        $select = 'ccl.' . $lang . "name as cname";
    }
    $query = "select cc.*,$select from {course_categories} cc
              LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid 
              where cc.visible = :visible and cc.coursecount != 0  and cc.depth<3 ORDER BY cc.sortorder ASC";

    $category = $DB->get_records_sql($query, array('visible' => 1));
    $result = array();
    foreach ($category as $ca) {
        $sub = local_course_category_id($ca->id);
        if ($sub) {
            $result[] = $sub;
        } else {
            $result[] = $ca;
        }
    }

    return $result;
}

//강좌 카테고리 불러오기
function local_course_categories_path($category) {
    foreach ($category as $ca) {
        if ($ca->id) {
            $category_path[] = local_course_category_get_pathname($ca->id);
        } else {
            $category_path[] = local_course_categories_path($ca);
        }
    }

    return $category_path;
}

//강좌후기
function local_lmsdata_get_course_like($id) {
    global $DB, $USER, $PAGE;
    $fields = user_picture::fields('u', NULL, 'userid');
    $sql_select = " select lcl.*, u.firstname, case when lcl.userid = $USER->id then 1 else 0 end my, $fields ";
    $sql_select_cnt = "select count(lcl.id) ";
    $sql = "
        from {lmsdata_course_like} lcl 
        Join {user} u on lcl.userid = u.id
        where lcl.moodlecourseid = $id";
    $sql_total = " AND lcl.isopend = 0";
    $orderby = " order by lcl.timecreated desc ";
    $comments = $DB->get_records_sql($sql_select . $sql . $orderby);

    foreach ($comments as $key => $comment) {
        $user = new stdclass();
        $user->id = $comment->userid;
        $user = username_load_fields_from_object($user, $comment, null, array('picture', 'imagealt', 'email'));
        $comment->userfullname = $user->firstname;

        $userpicture = new user_picture($user);
        $userpicture->size = 1; // Size f1.
        $comment->userpictureurl = $userpicture->get_url($PAGE)->out(false);

        $timenow = time();
        if($comment->timemodified != 0){
            $timebefore = $timenow - $comment->timemodified;
        }else{
            $timebefore = $timenow - $comment->timecreated;
        }
        if ($timebefore == 0) {
            $comment->displaytime = '지금';
        } else if (($timebefore > 604800)) { // 7일 이전거는 'yyyy.mm.dd hh:mm' 으로 표시
            $comment->displaytime = date('Y-m-d H:i', $comment->timecreated);
        } else { // 'D일 전 / h시간 전 / m분 전 / s초 전' 으로 표시
            $day = floor($timebefore / 86400);
            if ($day > 0) {
                $comment->displaytime = $day . '일 전';
            } else {
                $hour = floor($timebefore / 3600);
                if ($hour > 0) {
                    $comment->displaytime = $hour . '시간 전';
                } else {
                    $minute = floor($timebefore / 60);
                    if ($minute > 0) {
                        $comment->displaytime = $minute . '분 전';
                    } else {
                        $seconds = $timebefore % 60;
                        $comment->displaytime = $seconds . '초 전';
                    }
                }
            }
        }
        $comment->origincontents = $comment->contents;
        $comment->my = ($comment->userid == $USER->id);
        $comment->contents = $comment->contents;
        if($comment->isopend == 1){
            if($comment->my == 1){
                $comment->contents = get_string('notopend', 'local_course').'<br>'.$comment->origincontents;
            }else{
                unset($comments[$key]);
            }
        }
    }
    
    $data = new stdClass();
    $data->comment = $comments;
    $data->total = $DB->count_records_sql($sql_select_cnt . $sql);
    $data->total_lang = $DB->count_records_sql($sql_select_cnt . $sql.$sql_total);
    $data->avg = $DB->get_record_sql("select AVG(score) avg from {lmsdata_course_like} where moodlecourseid = $id AND isopend = 0");
    return $data;
}

//며칠전 표시
function local_date_notation($time) {
    $t = time() - $time;
    $f = array(
        '31536000' => '년',
        '2592000' => '개월',
        '604800' => '주',
        '86400' => '일',
        '3600' => '시간',
        '60' => '분',
        '1' => '초'
    );
    foreach ($f as $k => $v) {
        if (0 != $c = floor($t / (int) $k)) {
            return $c . $v . '전';
        }
    }
}

//수강중 인지 아닌지 & 수강기간 복습기간에도 들어갈 수 있어야하나?
function local_course_whether_enrol($courseid, $userid, $rolename) {
    global $CFG, $PAGE, $DB;
    $now = time();
    require_once("$CFG->dirroot/enrol/locallib.php");
    //lc.learningstart <= $now AND $now <= lc.learningend
    $role = $DB->get_record('role', array('shortname' => $rolename));
    $course = $DB->get_record('course', array('id' => $courseid));
    $learningday = $DB->get_record_select("lmsdata_class", "", array("courseid" => $courseid), "reviewperiod");

    $manager = new course_enrolment_manager($PAGE, $course);

    $ues = $manager->get_user_enrolments($userid);

    if ($ues) {
        //수강중
        foreach($ues as $ue){
            //$thistime > z.enddate AND $thistime < z.redate
            if($now < $ue->timeend){
                if($ue->timeend - ($learningday->reviewperiod * 86400) < $now){
                    //return $ue->timestart.'////'.$ue->timeend."<br>";
                    return get_string('reviewing','local_course');
                }
                return get_string('takecourse','local_course');
            }else{
                return false;
            }
        }
    } else {
        return false;
    }
}

function local_course_teacher_list($coruseid) {
    global $DB;
    $select = "select lcp.teacherid  ";
    $select_cnt = "select count(lcp.teacherid) ";
    $query = "from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $coruseid;
    $list = $DB->get_records_sql($select . $query);
    $total = $DB->count_records_sql($select_cnt . $query);
    $data = new stdClass();
    $data->list = $list;
    $data->total = $total;

    return $data;
}
function local_course_recommend($login, $limit = null) {
    global $DB, $USER;
    $matchsql = "   select us.*
                    from {lmsdata_user}  lu 
                    join {vi_user_skills} us on lu.userid = us.user_id
                    where lu.statuscode = ? and lu.userid = ? ";
    $matchs = $DB->get_records_sql($matchsql, array(1, $USER->id));
    $lang = current_language();
    $lang = $lang == 'ko' ? '' : $lang . '_';
    $recommendsql = " select lc.id,lc." . $lang . "title title,lcu." . $lang . "onelineintro onelineintro,
                    ll.*,lc.price,lc.discount,lc.courseid,lcu.courseid lcocourseid,st.star,st.total,
                    ml.userid likes,lcu.price lcoprice,lt.price ltprice
                    from {lmsdata_main_course} lmc
                    join {lmsdata_class} lc on lmc.courseid = lc.id 
                    join {lmsdata_course} lcu on lc.parentcourseid  = lcu.id 
                    left join {lmsdata_textbook} lt on lt.id = lc.bookid 
                    left join (SELECT classid,count(classid) totallike FROM {lmsdata_like} group by classid ) ll on ll.classid = lc.id
                    left join (select moodlecourseid,round(sum(score)/count(moodlecourseid)) star,count(moodlecourseid) total from {lmsdata_course_like} group by moodlecourseid ) st 
                               on st.moodlecourseid = lc.courseid
                    left join (select userid,classid from {lmsdata_like} where userid = :userid) ml on ml.classid = lc.id ";

    $params['userid'] = $USER->id;
    $params['rlearningend'] = time();
    $params['rlearningstart'] = time();
    $rwhereparts[] = "lmc.type = :type";
    $rwhereparts[] = " lc.learningstart < :rlearningstart ";
    $rwhereparts[] = " lc.learningend  >  :rlearningend";

    if ($login) {
        $params['type'] = 1;
        $params['recommended'] = 1;
        if ($matchs) {
            $klevel = $DB->get_field("vi_user_korean_levels", 'korean_level_id', array("user_id" => $USER->id));
            $kleveladd = "";
            if ($klevel) {
                $kleveladd = " or b.klevel =:klevel ";
                $params['klevel'] = $klevel;
            }

            foreach ($matchs as $matchs) {
                $skill_ids[] = $matchs->skill_id;
            }
            $skill = "(" . implode(',', $skill_ids) . ")";
            $rwhereparts[] = "lmc.recommended = :recommended";
            $rwhere = "where " . implode(' and ', $rwhereparts);
            $mactchingsql = $recommendsql . $rwhere;
            $mactchingsql .= " UNION
                        select lc.id,lc." . $lang . "title title,lcu." . $lang . "onelineintro onelineintro,
                        ll.*,lc.price,lc.discount,lc.courseid,lcu.courseid lcocourseid,st.star,st.total,
                        ml.userid likes,lcu.price lcoprice,lt.price ltprice
                        from (select b.*,a.skillid from {lmsdata_class_skills} a
                        join {lmsdata_class} b on a.classid = b.id $kleveladd where  a.skillid in $skill group by b.id) lc
                        join {lmsdata_course} lcu on lc.parentcourseid  = lcu.id
                        left join {lmsdata_textbook} lt on lt.id = lc.bookid 
                        left join (SELECT classid,count(classid) totallike FROM {lmsdata_like} group by classid ) ll on ll.classid = lc.id
                        left join (select moodlecourseid,round(sum(score)/count(moodlecourseid)) star,count(moodlecourseid) total from {lmsdata_course_like} group by moodlecourseid ) st 
                                   on st.moodlecourseid = lc.courseid
                        left join (select userid,classid from {lmsdata_like} where userid = :userid2 ) ml on ml.classid = lc.id ";
            $whereparts[] = " lc.isused  = :isused";
            $whereparts[] = " lc.learningstart < :learningstart ";
            $whereparts[] = " lc.learningend  >  :learningend";
            $params['userid2'] = $USER->id;
            $params['isused'] = 0;
            $params['learningstart'] = time();
            $params['learningend'] = time();
            $where = ' where ' . implode(' and ', $whereparts);
            $recommends = $DB->get_records_sql($mactchingsql . $where, $params);
        } else {
            $where = "where " . implode(' and ', $rwhereparts);
            $recommends = $DB->get_records_sql($recommendsql . $where, $params);
        }
    }else{
        $where = ' where ' . implode(' and ', $rwhereparts);
        $params['type'] = 2;
        $recommends = $DB->get_records_sql($recommendsql . $where, $params, 0, $limit);
    }
    return $recommends;
}
function local_job_recommend($login) {
    global $DB, $USER;
    $thistime = time();
    $type = ($login) ? 1 : 0;
    $jobsql = "SELECT vj.*,ve.company_name, ve.company_logo
                FROM {lmsdata_main_job} mj
		JOIN {vi_jobs} vj on mj.jobid = vj.id
                JOIN {vi_employers} ve on vj.employer_id = ve.id
                WHERE mj.type = $type and vj.enddate > $thistime
                ORDER BY RAND() 
                LIMIT 4";
    $jobs = $DB->get_records_sql($jobsql, array());
    return $jobs;
}
function get_coupon_by_code1($couponcode){
    global $DB, $USER;
    $sql="SELECT vc.*,vcr.* FROM {vi_coupons} vc 
    LEFT JOIN {vi_coupons_receivers} vcr ON vc.id=vcr.coupon_id
    WHERE vc.key_code=:key_code";
    $params = array('key_code'=>$couponcode);
    $result=$DB->get_record_sql($sql,$params);
    return $result;

}
function get_coupon_by_id($couponid){
    global $DB, $USER;
    $sql="SELECT vc.* FROM {vi_coupons} vc 
    LEFT JOIN {vi_coupons_receivers} vcr ON vc.id=vcr.coupon_id
    WHERE vc.id=$couponid";
    $result=$DB->get_record_sql($sql);
    return $result;
}
function get_coupon_by_code($couponcode){
    global $DB, $USER;
    $sql="SELECT vc.*,vcr.* FROM {vi_coupons} vc 
    LEFT JOIN {vi_coupons_receivers} vcr ON vc.id=vcr.coupon_id
    WHERE vc.key_code=:key_code AND vcr.is_used=0";
    $params = array('key_code'=>$couponcode);
    $result=$DB->get_record_sql($sql,$params);
    return $result;
}
