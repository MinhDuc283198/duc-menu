function call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: '/local/course/ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}
/*
$(document).ready(function () {
    $('.sub-mn > ul > li').mouseenter(function(){
         var id = $(this).data("id");
         var result = call_ajax('category', {id: id}, false);
         
         if(result.status == "success") {
            $tbody = $('.sub-mn ul li ul');
            $tbody.empty();
            if(result.list) {
                $.each(result.list, function() {
                    $tr = $('<li>');
                    $tr.append('<a href="/local/course/list.php?id='+this.id+'">'+this.cname+'</a>');
                    $tbody.append($tr);
                });
            } else {
                $tbody.append('<li><a href="#">데이터가 없습니다</a></li>');
            }
        } else {
            //alert(result.error);
        }
    })
});
*/
//썸네일 하트 이벤트
$(document).on("click", ".ic-heart", function(){
    var cid = $(this).attr('data-target');
     $.ajax({
        url: "/local/lmsdata/like.ajax.php",
        type: 'POST',
        dataType: 'json',
        data: {classid : cid
            },
        success: function (data) {
            if(data.value>1){
                $("#like"+cid).addClass("on");
            } else {
                $("#like"+cid).removeClass("on");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);
        }
    });
});

$(document).on("click", ".hash-area li", function(){
    var txt = $(this).data('title');
    $('input[name=searchtext]').val(txt);
    $('.main-search').submit();
    $('.hd-fr').submit();
});

$('.hash-area li a').click(function (e) {
    e.preventDefault();
});