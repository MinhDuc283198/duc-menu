<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('배송정보입력');

echo $OUTPUT->header();
$id = optional_param('id', 0, PARAM_INT);
$query = 'select u.id, u.username, u.auth, u.firstname, u.lastname, u.phone1, u.phone2, u.email, lu.address, lu.address_detail from {user} u JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
$user = $DB->get_record_sql($query, array('id'=>$USER->id));

$class = $DB->get_record('lmsdata_class', array('id'=>$id));
?>  
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('shoppinginfo', 'local_course') ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_course') ?></a>
    </div>
    <div class="pop-contents">
        <div class="addr-info">
            <div class="rw">
                <p class="tit"><?php echo get_string('adress', 'local_course') ?></p>
                <input type="text" value="<?php echo $user->address?>"  name="address"/>
                <input type="text" value="<?php echo $user->address_detail?>"  name="address_detail"/>
                <p class="t-gray"><?php echo get_string('shopping_txt', 'local_course') ?></p>
            </div>
            <div class="rw phone">
                <p class="tit"><?php echo get_string('phone', 'local_course') ?></p>
                <input type="text" value="<?php echo $user->phone1?>" name="phone1" class="w-auto"  placeholder="<?php echo get_string('phone_txt', 'local_my')?>"/>


                <p class="t-gray"><?php echo get_string('phone_txt', 'local_course') ?></p>
            </div>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="<?php echo get_string('payment', 'local_course') ?>" class="btns point w100" onclick="productdelivery(<?php echo $id?>, <?php echo $class->type?>)" />
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>

<script>
$(document).ready(function () {
    $("input[name=phone1]").on("keyup", function() {
        $(this).val($(this).val().replace(/[^0-9]/g,""));
    });
});    
</script>
