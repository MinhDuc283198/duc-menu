<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$id = optional_param('id', 0, PARAM_INT);
echo $OUTPUT->header();
$list = $DB->get_record('lmsdata_badge', array('id' => $id));
require_once($CFG->dirroot . '/local/lmsdata/lib.php');
$img_path = local_lmsdata_get_imgpath(1, 'bedge', $list->id)['path'];
switch (current_language()) {
    case 'ko' :
        $lang_title = $list->title_ko;
        $lang_content = $list->content_ko;
        break;
    case 'en' :
        $lang_title = $list->title_en;
        $lang_content = $list->content_en;
        break;
    case 'vi' :
        $lang_title = $list->title_vi;
        $lang_content = $list->content_vi;
        break;
}
$now = time();
$requery = "SELECT lc.*, lcr.type lcrtype, lcr.dataid, lb.id lbid   
    FROM {lmsdata_class_relation} lcr 
    JOIN {lmsdata_badge} lb ON lcr.dataid = lb.id 
    JOIN {lmsdata_class} lc ON lc.id = lcr.classid
    JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
    JOIN {course} c ON c.id = lco.courseid 
    JOIN {course} mc2 ON lco.courseid = mc2.id
    JOIN {course_categories} ca ON ca.id = mc2.category AND ca.visible = 1
    LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id AND ca2.visible = 1
    WHERE lcr.type = 3 AND lcr.dataid = :dataid AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0  
    ORDER BY lc.id DESC, lc.timecreated DESC";
$relation = $DB->get_records_sql($requery, array('dataid'=>$id));

?>  
<div class="layerpop no-footer">
    <div class="pop-title">
        <?php echo get_string('badge','local_course')?>
        <a href="#" class="btns f-r open-bdg-pop"><?php echo get_string('allbadge','local_course')?></a>
        <a href="#" class="pop-close"><?php echo get_string('close','local_management')?></a>
    </div>
    <div class="pop-contents">
        <div class="badge-info">
            <div class="bdg-img">
                <img src="<?php echo $img_path?>" alt="<?php echo $lang_title.' '.get_string('badge','local_management')?> " /><!-- badge_blue04.png, badge_green04.png, badge_yellow04.png -->
            </div>
            <div class="bdg-txt">
                <div><?php echo $lang_title?></div>
                <p><?php echo $lang_content?></p>
            </div>
        </div>
        <?php if($relation){?>
        <div class="badge-course">
            <?php 
            foreach($relation as $rl){
            $iscom = $DB->get_fieldset_sql('SELECT timecompleted FROM {course_completions} WHERE userid = :userid and course = :courseid',array('userid'=>$USER->id,'courseid'=>$rl->courseid));
            $badgequery = "
                        select lbu.*, lb.id lbid  
                        from m_lmsdata_badge_user lbu
                        JOIN m_lmsdata_badge lb ON lbu.badgeid=lb.id
                        JOIN m_course c ON lbu.course = c.id
                        JOIN m_lmsdata_class lc ON c.id = lc.courseid AND lc.isused = 0  
                        JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id AND lco.isused = 0  
                        JOIN m_course  mc2 ON lco.courseid = mc2.id
                        JOIN m_course_categories  ca ON ca.id = mc2.category AND ca.visible = 1
                        left JOIN m_course_categories  ca2 ON ca.parent = ca2.id AND ca2.visible = 1
                        WHERE lbu.status = 1 AND lb.id = :lbid AND lbu.userid = :userid";
            $badge = $DB->get_record_sql($badgequery, array('lbid'=>$rl->lbid, 'userid'=>$USER->id));

            if( (empty($iscom) || $iscom[0] <= 0) || !$badge ){ $completion_txt = ''; } else {$completion_txt = get_string('completion','local_mypage');}
            ?>
            <p><a href="<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $rl->id?>"><?php echo $lang_title?></a><span class="lb"><?php echo $completion_txt?></span></p>
            <?php }?>
        </div>
        <?php }?>
        
    </div>
</div>
<script type="text/javascript">
    
</script>
<?php
echo $OUTPUT->footer();
?>


