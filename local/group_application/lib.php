<?php

/**
 * 플러그인 파일 호출 함수
 * @global type $CFG
 * @global type $DB
 * @param type $course
 * @param type $cm
 * @param type $context
 * @param type $filearea
 * @param type $args
 * @param type $forcedownload
 * @param array $options
 * @return boolean
 */
function local_group_application_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()){
 	global $CFG, $DB;

	$fs = get_file_storage();
	$relativepath = implode('/', $args);

	$fullpath = "/$context->id/local_group_application/$filearea/$relativepath";
	if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
		return false;
	}


	// finally send the file
	send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}
