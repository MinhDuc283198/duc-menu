<?php
function xmldb_local_group_application_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    
    $dbman = $DB->get_manager(); 

    if($oldversion < 2017080200) {
        $table = new xmldb_table('lmsdata_group_applications');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('groupid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('phone', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('paymentid', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('ordernumber', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('account', XMLDB_TYPE_CHAR, '30', null, null, null, null);
        $table->add_field('bankcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('accountname', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('cshrresult', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('cshrtype', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('cardcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('cardnumber', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('installment', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('pointuse', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('allowcancel', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('paymentstatus', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
    if($oldversion < 2017080202) {
        $table = new xmldb_table('lmsdata_group_applications');
        $field = new xmldb_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017080203) {
        $table = new xmldb_table('lmsdata_group_applications');
        $field = new xmldb_field('paymentdate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017080204) {
        $table = new xmldb_table('lmsdata_group_applications');
        $field = new xmldb_field('paymentmeans', XMLDB_TYPE_CHAR, '20', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    if($oldversion < 2017080205) {
        $table = new xmldb_table('lmsdata_group_applications');
        $field = new xmldb_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    if($oldversion < 2017080700) {
        $table = new xmldb_table('lmsdata_group_change');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '20', null, null, null, 0);
        $table->add_field('title', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('contents', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_CHAR, '50', null, null, null, null);  
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    
    if($oldversion < 2017082200) {
        $table = new xmldb_table('lmsdata_group_applications');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('groupid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('phone', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('paymentid', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('ordernumber', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('account', XMLDB_TYPE_CHAR, '30', null, null, null, null);
        $table->add_field('bankcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('accountname', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('cshrresult', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('cshrtype', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('cardcode', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('cardnumber', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('installment', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('pointuse', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('allowcancel', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        $table->add_field('paymentstatus', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);        
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('paymentdate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('paymentmeans', XMLDB_TYPE_CHAR, '20', null, null, null, 0);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if($oldversion < 2017090700) {
        $table = new xmldb_table('lmsdata_grouplist');
        $field = new xmldb_field('status', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
    return true;
}