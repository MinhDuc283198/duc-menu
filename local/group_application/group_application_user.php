<?php
/*
 * 단체신청 상세보기시 신청자 개별 리스트를 호출하는 팝업 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$id = required_param('id', PARAM_INT);
$print = optional_param('print', 0, PARAM_INT);

$info = $DB->get_record('lmsdata_grouplist', array('id' => $id));

//총금액 불러오는 query
$total_sql = 'SELECT sum(lca.price) as price
        FROM {lmsdata_groupapply} lga
        JOIN {lmsdata_course_applications} lca ON lca.courseid = lga.classid AND lca.groupcode = :groupcode AND lca.userid = lga.userid
        WHERE lga.groupid = :groupid';
$total_price = $DB->get_record_sql($total_sql, array('groupcode' => $info->groupcode, 'groupid' => $info->id));

//신청자 정보 불러오는 query
$apply_sql = 'SELECT lga.id, lga.status, lca.price, lc.classyear, lc.classnum, mu.lastname, lco.coursename
        FROM m_lmsdata_groupapply lga
        JOIN m_lmsdata_course_applications lca ON lca.courseid = lga.classid AND lca.groupcode = :groupcode AND lca.userid = lga.userid
        JOIN m_lmsdata_class lc ON lc.id = lca.courseid
        JOIN m_lmsdata_course lco ON lco.id = lc.parentcourseid
        JOIN m_user mu ON mu.id = lga.userid
        WHERE lga.groupid = :groupid ORDER BY lga.timecreated DESC';
$applys = $DB->get_records_sql($apply_sql, array('groupcode' => $info->groupcode, 'groupid' => $info->id));
$apply_count = count($applys);
?>  

<?php if (!$print) { ?>
    <div id="popwrap">
        <h3 id="pop_title">
            <?php echo get_string('applicant_confirmation', 'local_group_application'); ?>
            <img class="close r_content" src="/theme/oklassedu/pix/images/close.png" alt="" />
        </h3>
    <?php } ?>
    <div id="pop_content">
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo get_string('number_of_applicants', 'local_group_application'); ?></th>
                    <th><?php echo get_string('total_payment_amount', 'local_group_application'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $apply_count; ?></td>
                    <td class="red text-bold"><?php echo number_format($total_price->price); ?>원</td>
                </tr>
            </tbody>
        </table>
        <?php if (!$print) { ?>
            <div class="input_group margin r_content">
                <input type="text" placeholder="<?php echo get_string('name_search', 'local_group_application'); ?>" />
                <input type="button" value="<?php echo get_string('search', 'local_group_application'); ?>" class="btn gray" />
            </div>
        <?php } ?>
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo get_string('number', 'local_group_application'); ?></th>
                    <th><?php echo get_string('name_of_applicant', 'local_group_application'); ?></th>
                    <th><?php echo get_string('coursename', 'local_group_application'); ?></th>
                    <th><?php echo get_string('year', 'local_group_application'); ?></th>
                    <th><?php echo get_string('classnum', 'local_group_application'); ?></th>
                    <th><?php echo get_string('price', 'local_group_application'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($apply_count === 0) {
                    ?>
                    <tr>
                        <td colspan="7"><?php echo get_string('empty_data', 'local_group_application'); ?></td>
                    </tr>
                    <?php
                } else {
                    $startnum = $apply_count - (($currpage - 1) * $perpage);
                    foreach ($applys as $apply) {
                        ?>
                        <tr>
                            <td><?php echo $startnum--; ?></td>
                            <td><?php echo $apply->lastname; ?></td>
                            <td><?php echo $apply->coursename; ?></td>
                            <td><?php echo $apply->classyear . get_string('year', 'local_group_application'); ?></td>
                            <td><?php echo $apply->classnum . get_string('classnum', 'local_group_application'); ?></td>
                            <td><?php echo $apply->price; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <?php if (!$print) { ?>
            <input type="button" value="신청내역출력" onclick="window.open('group_application_user.php?id=<?php echo $id; ?>&print=1', '', 'width=800, height=485')" class="btn gray" />
        <?php } else { ?>
            <input type="button" value="프린트" class="btn print r_content" onclick="window.print();" />
        <?php } ?>
        <div class="pagenav">
            <span class="prev"><a href="#">&lt;</a></span>
            <span class="on"><a href="#">1</a></span>
            <span class="next"><a href="#">&gt;</a></span>
        </div>
    </div>
</div>

<script>

</script>