<?php
/**
 * 단체수강신청 신청내역 상세보기 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('../../../INIpay5.3.9/libs/INIStdPayUtil.php');

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string('support_room', 'local_group_application'));
$PAGE->navbar->add(get_string('pluginname', 'local_group_application'));

require_login();

$id = required_param('id', PARAM_INT);

$list = $DB->get_record_sql("select lg.*,ls.classyear,ls.classnum,ls.enrolmentstart,ls.enrolmentend from {lmsdata_grouplist} lg join {lmsdata_schedule} ls on ls.id = lg.scheduleid where lg.id = $id");
$arr = array(1=>get_string('individual_payment', 'local_group_application'),2=>get_string('bill_payment', 'local_group_application'),3=>get_string('administrative_room_settlement', 'local_group_application'));

$sql = 'SELECT sum(lca.price) as price
        FROM {lmsdata_groupapply} lga
        JOIN {lmsdata_course_applications} lca ON lca.courseid = lga.classid AND lca.groupcode = :groupcode AND lca.userid = lga.userid
        WHERE lga.groupid = :groupid and lca.status <> "cancel"';
$apply_price = $DB->get_record_sql($sql, array('groupcode'=>$list->groupcode, 'groupid'=>$list->id));
$apply = $DB->get_records('lmsdata_groupapply', array('groupid'=>$list->id));
$apply_count = count($apply);

$application = $DB->get_record('lmsdata_group_applications', array('groupid'=>$list->id));

//현재 유저정보
$sql = 'SELECT mu.lastname, mu.email, lu.*
        FROM {user} mu
        JOIN {lmsdata_user} lu ON lu.userid = mu.id
        WHERE mu.id = :userid';

$userinfo = $DB->get_record_sql($sql,array('userid'=>$USER->id));

$phone_first_number = substr($userinfo->phone1, 0, 3);
$phone_middle_number = substr($userinfo->phone1, 3, 4);
$phone_last_number = substr($userinfo->phone1, 7, 4);

$phonenumber = $phone_first_number.'-'.$phone_middle_number.'-'.$phone_last_number;

$price = $apply_price->price;

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_group_application', "group_application", $id, 'id');
$fileobj = '';
foreach ($files as $file) {
    $filename = $file->get_filename();
    $mimetype = $file->get_mimetype();
    $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_group_application/group_application/' . $id . '/' . $filename);
    if ($file->get_filesize() > 0) {
        $fileobj = "<span id ='file'><a href=\"$path\">$iconimage</a>";
        $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</span>';
    }
}

//결제모듈
$SignatureUtil = new INIStdPayUtil();

/*
  //*** 위변조 방지체크를 signature 생성 ***

  oid, price, timestamp 3개의 키와 값을

  key=value 형식으로 하여 '&'로 연결한 하여 SHA-256 Hash로 생성 된값

  ex) oid=INIpayTest_1432813606995&price=819000&timestamp=2012-02-01 09:19:04.004


 * key기준 알파벳 정렬

 * timestamp는 반드시 signature생성에 사용한 timestamp 값을 timestamp input에 그대로 사용하여야함
 */

//############################################
// 1.전문 필드 값 설정(***가맹점 개발수정***)
//############################################
// 여기에 설정된 값은 Form 필드에 동일한 값으로 설정
$mid = "campuseduh";  // 가맹점 ID(가맹점 수정후 고정)					
//인증
$signKey = "NVN5TVhqUGJYcC9LNFlTYTEwYUpRQT09"; // 가맹점에 제공된 웹 표준 사인키(가맹점 수정후 고정)
$timestamp = $SignatureUtil->getTimestamp();   // util에 의해서 자동생성

$orderNumber = $mid . "_" . $SignatureUtil->getTimestamp(); // 가맹점 주문번호(가맹점에서 직접 설정)

$cardNoInterestQuota = "11-2:3:,34-5:12,14-6:12:24,12-12:36,06-9:12,01-3:4";  // 카드 무이자 여부 설정(가맹점에서 직접 설정)
$cardQuotaBase = "2:3:4:5:6:11:12:24:36";  // 가맹점에서 사용할 할부 개월수 설정
//###################################
// 2. 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)
//###################################
$mKey = $SignatureUtil->makeHash($signKey, "sha256");

$params = array(
    "oid" => $orderNumber,
    "price" => $price,
    "timestamp" => $timestamp
);
$sign = $SignatureUtil->makeSignature($params, "sha256");

/* 기타 */
$siteDomain = $CFG->wwwroot.'/local/group_application'; //가맹점 도메인 입력
// 페이지 URL에서 고정된 부분을 적는다. 
// Ex) returnURL이 http://localhost:8082/demo/INIpayStdSample/INIStdPayReturn.jsp 라면
//                 http://localhost:8082/demo/INIpayStdSample 까지만 기입한다.

echo $OUTPUT->header();
?>  
<script type="text/javascript">
    $(document).ready(function () {

        /** laeyrpop events **/
        $(".modifybtn").click(function () {
            /** url width height param **/
            call_layerPop("/local/group_application/group_application_user.php?id=<?php echo $id; ?>", "700px", "500px", {});
            $("#poplayer").draggable();
        });
        $("#file_submit").click(function () {
            var form = $('FILE_FORM')[0];
            var formData = new FormData(form);
            formData.append("group_application", $("input[name=group_application]")[0].files[0]);
            formData.append("id", <?php echo $id; ?>);
            $.ajax({ 
                url: 'group_application.file.php', 
                data: formData, 
                processData: false, 
                contentType: false, 
                type: 'POST', 
                success: function(data){
                    alert('<?php echo get_string('group_application_list_detail_alert1', 'local_group_application'); ?>')
                    location.reload();
                } 
            });
        });

    });
</script>
<h3 class="page_title"><?php echo get_string('group_application_details', 'local_group_application'); ?></h3>
<div class="colorbox gray">
    <p class="text-bold"><?php echo get_string('detail_text1', 'local_group_application'); ?></p>
    <p class="text-bold"><?php echo get_string('detail_text2', 'local_group_application'); ?></p>
    <p class="text-bold"><?php echo get_string('detail_text3', 'local_group_application'); ?></p>
</div>

<div class="div_title top">
    <?php echo get_string('representative_name', 'local_group_application'); ?>
</div>
<table class="table">
    <tbody>
        <tr>
            <th><?php echo get_string('representative', 'local_group_application'); ?></th>
            <td class="text-bold text-left"><?php echo $list->repusername ?></td>
        </tr>
        <tr>
            <th><?php echo get_string('school', 'local_group_application'); ?></th>
            <td class="text-left"><?php echo $list->school ?></td>
        </tr>
        <tr>
            <th><?php echo get_string('contact', 'local_group_application'); ?></th>
            <td class="text-left"><?php echo substr($list->repuserphone, 0, 3).'-'.substr($list->repuserphone, 3, 4).'-'.substr($list->repuserphone, 7, 4) ?></td>
        </tr>
        <tr>
            <th><?php echo get_string('email', 'local_group_application'); ?></th>
            <td class="text-left"><?php echo $list->repuseremail ?></td>
        </tr>
    </tbody>
</table>

<div class="div_title top">
    <?php echo get_string('application_information', 'local_group_application'); ?>
</div>
<table class="table">
    <thead>
    <th><?php echo get_string('year', 'local_group_application'); ?></th>
    <th><?php echo get_string('classnum', 'local_group_application'); ?></th>
    <th><?php echo get_string('count', 'local_group_application'); ?></th>
    <th><?php echo get_string('paymentmethod', 'local_group_application'); ?></th>
    <th><?php echo get_string('groupcode', 'local_group_application'); ?></th>
    <th><?php echo get_string('validity', 'local_group_application'); ?></th>
    <th><?php echo get_string('status', 'local_group_application'); ?></th>
</thead>
<tbody>
    <tr>
        <td><?php echo $list->classyear.get_string('year', 'local_group_application') ?></td>
        <td><?php echo $list->classnum.get_string('classnum', 'local_group_application') ?></td>
        <td><?php echo $list->usercount ?></td>
        <td><?php echo $arr[$list->paymentmethod] ?></td>
        <td class="red"><?php echo $list->groupcode ?></td>
        <td><?php echo date('Y-m-d',$list->enrolmentstart).' ~ '.date('Y-m-d',$list->enrolmentend) ?></td>
        <td><?php echo ($list->enrolmentstart <= time() && $list->enrolmentend >= time())? get_string('available', 'local_group_application') : get_string('can_not_be_used', 'local_group_application'); ?></td>
    </tr>
</tbody>
</table>

<div class="div_title top">
    <?php echo get_string('registration_and_payment', 'local_group_application'); ?>
    <div class="r_content">
        <input type="button" value="<?php echo get_string('applicant_confirmation', 'local_group_application'); ?>" class="btn gray big modifybtn"/>
    </div>
</div>

<!-- 수강신청 및 결제 -->
<table class="table">
    <thead>
    <th><?php echo get_string('number_of_applicants', 'local_group_application'); ?></th>
    <th><?php echo get_string('total_payment_amount', 'local_group_application'); ?></th>
    <th><?php echo get_string('submit_an_application', 'local_group_application'); ?></th>
    <th><?php echo get_string('paymentyn', 'local_group_application'); ?></th>
</thead>
<tbody>
    <tr>
        <td><?php echo $apply_count;?></td>
        <td class="red"><?php echo number_format($apply_price->price);?></td>
        <td>
            <?php if(!$fileobj){ ?>
            <?php echo get_string('notmade', 'local_group_application'); ?>
            <?php 
            } else { 
               echo $fileobj; 
            }
            ?>
        </td>
        <td><?php echo $list->status == 0 ? get_string('outstanding', 'local_group_application') : $list->status == 2 ? get_string('standing', 'local_group_application') : '' ?></td>
    </tr>
</tbody>
</table>

<div class="text">
    <?php echo get_string('detail_text4', 'local_group_application'); ?>
</div>

<div class="div_title top">
    <?php echo get_string('submit_an_application', 'local_group_application'); ?>
</div>
<div class="filebox">
    <div class="customfile">
        <form id="FILE_FORM" method="post" enctype="multipart/form-data" action="">
            <input type="text" class="fileName" readonly="readonly">
            <label for="group_application" class="btn_file"><?php echo get_string('find_file', 'local_group_application'); ?></label>
            <input type="file" id="group_application" name="group_application" class="uploadBtn">
        </form>
    </div>
    <input type="button" class="btn orange" id="file_submit" value="<?php echo get_string('submission_complete', 'local_group_application'); ?>" />
    <a href="/local/jinoboard/detail_uncore.php?id=36&page=1&perpage=10&list_num=4&search=&board=5&searchfield=title" class="under margin20"><?php echo get_string('download_application_form', 'local_group_application'); ?></a>
</div>

<?php if($list->paymentmethod != 1 && empty($application) && $fileobj){?>
    <script language="javascript" type="text/javascript" src="https://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
    <form id="payform" name="payform" method="POST">
        <div class="div_title top">
            <?php echo get_string('billing_information', 'local_group_application'); ?>
        </div>
        <table class="table">
            <tbody>
                <tr>
                    <th><?php echo get_string('total_payment_amount', 'local_group_application'); ?></th>
                    <td class="text-left text-bold">
                        <?php echo number_format($apply_price->price);?>원
                    </td>
                </tr>
                <!-- 기본 옵션 -->
                <!-- gopaymethod : 결제 수단 선택
                ex) Card (계약 결제 수단이 존재하지 않을 경우 에러로 리턴)
                사용 가능한 입력 값
                Card,DirectBank,HPP,Vbank,kpay,Swallet,Paypin,EasyPay,PhoneBill,GiftCard,EWallet
                onlypoint,onlyocb,onyocbplus,onlygspt,onlygsptplus,onlyupnt,onlyupntplus
                -->
                <tr>
                    <th><?php echo get_string('paymentmethod', 'local_group_application'); ?></th>
                    <td class="text-left">
                        <input type="radio" name="gopaymethod" value="Card" id="pay01" checked />
                        <label for="pay01"><?php echo get_string('method1', 'local_group_application');?></label>
                        <input type="radio" name="gopaymethod" value="DirectBank" id="pay02" />
                        <label for="pay02"><?php echo get_string('method2', 'local_group_application');?></label>
                        <input type="radio" name="gopaymethod" value="VBank" id="pay03" />
                        <label for="pay03"><?php echo get_string('method3', 'local_group_application');?></label>
                    </td>
                </tr>
                <tr>
                    <th><?php echo get_string('payment_information', 'local_group_application'); ?></th>
                    <td class="text-left">
                        <p><?php echo get_string('detail_text5', 'local_group_application'); ?></p>
                        <p><?php echo get_string('detail_text6', 'local_group_application'); ?></p>
                        <p><?php echo get_string('detail_text7', 'local_group_application'); ?></p>
                    </td>
                </tr>

            </tbody>
        </table>
    
        <!-- 필수 -->
        <input type="hidden"  style="width:100%;" name="version" value="1.0" >
        <input type="hidden" style="width:100%;" name="mid" value="<?php echo $mid ?>" >
        <input type="hidden" style="width:100%;" name="oid" value="<?php echo $orderNumber ?>" >
        <input type="hidden" style="width:100%;" name="price" value="<?php echo $price ?>" >
        <input type="hidden" style="width:100%;" name="currency" value="WON" ><!-- [WON|USD] -->
        <input type="hidden" style="width:100%;" name="timestamp" value="<?php echo $timestamp ?>" >
        <input type="hidden" style="width:100%;" name="signature" value="<?php echo $sign ?>" >
        <input type="hidden" style="width:100%;" name="returnUrl" value="<?php echo $siteDomain ?>/group_application_payment.php" >
        <input type="hidden"  name="mKey" value="<?php echo $mKey ?>" >

        <!-- goodname : -->
        <input type="hidden" style="width:100%;" name="goodname" value="<?php echo $list->repusername;?>" >

        <!-- buyername : -->
        <input type="hidden" style="width:100%;" name="buyername" value="<?php echo $userinfo->lastname;?>" >

        <!-- buyertel : -->
        <input type="hidden" style="width:100%;" name="buyertel" value="<?php echo $phonenumber;?>" >

        <!-- buyeremail : -->
        <input type="hidden" style="width:100%;" name="buyeremail" value="<?php echo $userinfo->email;?>" >

        <!-- offerPeriod : 제공기간
        ex)20150101-20150331, [Y2:년단위결제, M2:월단위결제, yyyyMMdd-yyyyMMdd : 시작일-종료일] -->
        <input type="hidden" style="width:100%;" name="offerPeriod" value="2015010120150331" >

        <!-- acceptmethod : acceptmethod
        acceptmethod  ex) CARDPOINT:SLIMQUOTA(코드-개월:개월):no_receipt:va_receipt:vbanknoreg(0):vbank(20150425):va_ckprice:vbanknoreg: 
        KWPY_TYPE(0):KWPY_VAT(10|0) 기타 옵션 정보 및 설명은 연동정의보 참조 구분자 ":" -->
        <input type="hidden" style="width:100%;" name="acceptmethod" value="HPP(1):no_receipt:va_receipt:vbanknoreg(0):below1000" >

        <!-- 표시 옵션 -->
        <!-- 초기 표시 언어 [ko|en] (default:ko) -->
        <input type="hidden" style="width:100%;" name="languageView" value="<?php echo current_language();?>" >
        <!-- charset : 리턴 인코딩 [UTF-8|EUC-KR] (default:UTF-8)-->
        <input type="hidden" style="width:100%;" name="charset" value="UTF-8" >
        <!-- payViewType : 결제창 표시방법 [overlay] (default:overlay) -->
        <input type="hidden" style="width:100%;" name="payViewType" value="" >
        <!-- closeUrl : payViewType='overlay','popup'시 취소버튼 클릭시 창닥기 처리 URL(가맹점에 맞게 설정)
        close.jsp 샘플사용(생략가능, 미설정시 사용자에 의해 취소 버튼 클릭시 인증결과 페이지로 취소 결과를 보냅니다.) -->
        <input type="hidden" style="width:100%;" name="closeUrl" value="<?php echo $siteDomain ?>/close.php" >

        <!-- 결제 수단별 옵션 -->
        <!-- 카드(간편결제도 사용) -->
        <!-- nointerest : 무이자 할부 개월 ex) 11-2:3:4,04-2:3:4 -->
        <input type="hidden" style="width:100%;" name="nointerest" value="<?php echo $cardNoInterestQuota ?>" >

        <!-- quotabase : 할부 개월 ex) 2:3:4 -->
        <input type="hidden" style="width:100%;" name="quotabase" value="<?php echo $cardQuotaBase ?>" >
        <input type="hidden" style="width:100%;" name="merchantData" value="apply=<?php echo $list->id;?>" >
        
        <div class="table_btn">
            <input type="button" onclick="gogo_pay();" value="결제" class="btn orange big"/>
        </div>
</form>
<?php }?>
<script>
    /**
     * 문서파일 여부 체크
     */
    $("input[name=group_application").change(function () {
        if ($(this).val() != "") {
            var ext = $(this).val().split(".").pop().toLowerCase();
            if ($.inArray(ext, ["hwp", "doc", "docx"]) == -1) {
                alert("hwp, doc, docx 파일만 업로드 해주세요.");
                $(this).val("");
                return;    
            }
        }
    });
    /**
     * 결제모듈 불러오는 함수
     * @returns {undefined}
     */
    function gogo_pay(){
        if($('input[name=price]').val() != 0){
            INIStdPay.pay('payform');
        } else {
            alert('아직 단체 구성원이 수강신청을 하지 않았습니다.');
        }
    }
</script>
<?php
echo $OUTPUT->footer();
?>


