<?php
/**
 * 단체 수강신청 시 단체코드를 랜덤생성하고 수강신청정보를 insert 하는 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$lmsdata = new stdClass();
foreach ($_REQUEST as $key => $val){
    $$key = $val;
    $lmsdata->$key = $val;
}
$lmsdata->groupcode = mt_rand(1000000000,9999999999);
while ($DB->get_record('lmsdata_grouplist',array('groupcode'=>$lmsdata->groupcode))) {
    $lmsdata->groupcode = mt_rand(1000000000,9999999999);
}

$lmsdata->repuserphone = $tel.$tel01.$tel02;
$lmsdata->timecreated = time();
$lmsdata->timeupdated = time();

$lmsdatagroup = $DB->insert_record('lmsdata_grouplist', $lmsdata);


redirect($CFG->wwwroot . '/local/group_application/group_application_list.php');



