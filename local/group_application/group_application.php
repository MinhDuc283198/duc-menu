<?php
/**
 * 단체 수강신청 메인 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string('support_room', 'local_group_application'));
$PAGE->navbar->add(get_string('pluginname', 'local_group_application'));

echo $OUTPUT->header();
?>  
<h3 class="page_title"><?php echo get_string('pluginname', 'local_group_application'); ?></h3>

<h5 class="div_title top">
    <?php echo get_string('group_applicationinfo', 'local_group_application'); ?>
</h5>
<ul class="dash">
    <li><?php echo get_string('group_application_text1', 'local_group_application'); ?></li>
    <li><?php echo get_string('group_application_text2', 'local_group_application'); ?></li>
    <li class="red"><?php echo get_string('group_application_text3', 'local_group_application'); ?></li>
</ul>

<div class="text">
   <?php echo get_string('group_application_text6', 'local_group_application'); ?>
</div>

<h5 class="div_title top">
    <?php echo get_string('discounts', 'local_group_application'); ?>
</h5>
<table class="table">
    <thead>
        <tr>
            <th><?php echo get_string('gubun', 'local_group_application'); ?></th>
            <th><?php echo get_string('discounts', 'local_group_application'); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-bold"><?php echo get_string('job_training_course', 'local_group_application'); ?></td>
            <td><?php echo get_string('group_application_text4', 'local_group_application'); ?></td>
        </tr>
    </tbody>
</table>

<div class="table_btn">
    <input type="button" value="<?php echo get_string('apply', 'local_group_application'); ?>" class="btn orange big" onclick="javascript:location.href='group_application_write.php'" />
    <input type="button" value="<?php echo get_string('view_application_history', 'local_group_application'); ?>" class="btn gray big" onclick="javascript:location.href='group_application_list.php'"/>
</div>

<?php
echo $OUTPUT->footer();
?>


