<?php
/**
 * 단체수강신청 내역 리스트 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string('support_room', 'local_group_application'));
$PAGE->navbar->add(get_string('pluginname', 'local_group_application'));

require_login();

echo $OUTPUT->header();

$lists = $DB->get_records_sql("select lg.*,ls.classyear,ls.classnum,ls.enrolmentstart,ls.enrolmentend from {lmsdata_grouplist} lg join {lmsdata_schedule} ls on ls.id = lg.scheduleid where lg.repuserid = $USER->id");

$arr = array(1=>get_string('individual_payment', 'local_group_application'),2=>get_string('bill_payment', 'local_group_application'),3=>get_string('administrative_room_settlement', 'local_group_application'));
?>  
<script type="text/javascript">
    $(document).ready(function () {
        /**
         * 취소팝업 콜백함수
         */
        $(".cancelbtn").click(function () {
            /** url width height param **/
            call_layerPop("pop_group_application_cancel.php", "700px", "500px", {});
        });

    });
</script>
<h3 class="page_title"><?php echo get_string('group_application_details', 'local_group_application'); ?></h3>
<div class="colorbox gray">
    <p class="text-bold"><?php echo get_string('group_application_list_text1', 'local_group_application'); ?></p>
    <p class="text-bold"><?php echo get_string('group_application_list_text2', 'local_group_application'); ?></p>
    <p class="text-bold"><?php echo get_string('group_application_list_text3', 'local_group_application'); ?></p>
    <p class="text-bold"><?php echo get_string('group_application_list_text4', 'local_group_application'); ?></p>
</div>
<div class="div_title r_content">
    <input type="button" value="<?php echo get_string('change_cancellation', 'local_group_application'); ?>" name="cancel" class="btn gray big cancelbtn" />
</div>
<table class="table">
    <thead>
        <tr>
            <th><?php echo get_string('year', 'local_group_application'); ?></th>
            <th><?php echo get_string('classnum', 'local_group_application'); ?></th>
            <th><?php echo get_string('count', 'local_group_application'); ?></th>
            <th><?php echo get_string('paymentmethod', 'local_group_application'); ?></th>
            <th><?php echo get_string('groupcode', 'local_group_application'); ?></th>
            <th><?php echo get_string('validity', 'local_group_application'); ?></th>
            <th><?php echo get_string('status', 'local_group_application'); ?></th>
            <th><?php echo get_string('request', 'local_group_application'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lists as $list) { ?>
        <tr>
            <td><?php echo $list->classyear.get_string('year', 'local_group_application') ?></td>
            <td><?php echo $list->classnum.get_string('classnum', 'local_group_application') ?></td>
            <td><?php echo $list->usercount.get_string('num', 'local_group_application') ?></td>
            <td><?php echo $arr[$list->paymentmethod] ?></td>
            
            <td class="red"><?php echo $list->groupcode ?></td>
            <td>
                <?php echo date('Y-m-d',$list->enrolmentstart).' ~ '.date('Y-m-d',$list->enrolmentend) ?><br />
            </td>
            <td><?php echo ($list->enrolmentstart < time() && $list->enrolmentend > time())? get_string('available', 'local_group_application') : get_string('can_not_be_used', 'local_group_application'); ?></td>
            <td>
                <?php if($list->status != 1) { ?>
                <input type="button" value="<?php echo get_string('view_details', 'local_group_application'); ?>" name="detail" class="btn gray"  onclick="javascript:location.href='group_application_list_detail.php?id=<?php echo $list->id?>'"  />
                <?php }else { echo get_string('cancel');} ?>
            </td>
        </tr>
        <?php }?>
    </tbody>
</table>

<div class="pagenav">
    <span class="prev"><a href="#">&lt;</a></span>
    <span class="on"><a href="#">1</a></span>
    <span class="next"><a href="#">&gt;</a></span>
</div>

<?php
echo $OUTPUT->footer();
?>


