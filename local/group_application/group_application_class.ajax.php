<?php
/**
 * 단체수강신청 가능한 기수를 리턴하는 ajax페이지
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$year = required_param('year', PARAM_INT);

$returnvalue = new stdClass();
    $today = time();
    $sql = "select id, classnum from {lmsdata_schedule} where classtype = :classtype and classyear= :classyear and openyn = :openyn and enrolmentstart < $today and enrolmentend > $today";
    $orderby = " order by classnum desc ";
    $catagories = $DB->get_records_sql($sql.$orderby, array('classtype'=>1, 'classyear'=>$year, 'openyn'=>0));
    foreach ($catagories as $key=>$val){
        $option .= '<option value="' . $key . '">' . $val->classnum . '기</option>';
    }
    ?>
<span id ='classnum_area' >
<select title="<?php echo get_string('classnum', 'local_group_application'); ?>" name="scheduleid" id ='classnum' class="w_160">
    <?php 
    echo '<option value="0">'.get_string('classnum_select', 'local_group_application').'</option>';
    echo $option ?>
</select>
</span>