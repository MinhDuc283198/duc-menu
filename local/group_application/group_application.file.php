<?php
/**
 * 단체수강신청 ajax 파일 업로드
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/lib/filelib.php');

$id = required_param('id', PARAM_INT);


 $fs = get_file_storage();
$context = context_system::instance();    
    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($id && !($_FILES["group_application"]['name']==null || $_FILES["group_application"]['name']=='')) {
        $overlap_files = $fs->get_area_files($context->id, 'local_group_application', "group_application", $id, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {
            if ($file->get_filesize() > 0) {
                    $filename = $file->get_filename();
                    $file->delete();
                $cnt++;
            }
        }
    }
    if (!empty($_FILES["group_application"]['tmp_name'])) {

        $file_record = array(
            'contextid' => $context->id,
            'component' => 'local_group_application',
            'filearea' => "group_application",
            'itemid' => $id,
            'filepath' => '/',
            'filename' => $_FILES["group_application"]['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $storage_id = $fs->create_file_from_pathname($file_record, $_FILES["group_application"]['tmp_name']);
    }    
