<?php
/*
 * 단체신청 신청폼 양식 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string('support_room', 'local_group_application'));
$PAGE->navbar->add(get_string('pluginname', 'local_group_application'));

require_login();

echo $OUTPUT->header();
$user_data = $DB->get_record('lmsdata_user',array('userid'=>$USER->id));
$user = $DB->get_record('user',array('id'=>$USER->id));
?>  

<h3 class="page_title"><?php echo get_string('pluginname', 'local_group_application'); ?></h3>

<h5 class="div_title top">
    <?php echo get_string('representative_information', 'local_group_application'); ?>
</h5>
<form name="group_form" id="group_form" class="group_form" action="group_application.submit.php"  method="post" enctype="multipart/form-data">
    <table class="table">
        <tbody>
            <tr>
                <th><?php echo get_string('representative_name', 'local_group_application'); ?></th>
                <td class="text-left">
                    <input type="hidden" name="repuserid" value="<?php echo $USER->id ?>"/>
                    <input type="text" name="repusername"  value="<?php echo $user_data->eng_name?>" title="<?php echo get_string('name', 'local_group_application'); ?>" />
                </td>
            </tr>
            <tr>
                <th>학교</th>
                <td class="text-left">
                    <input type="text" name="school" m-width="80" title="<?php echo get_string('school', 'local_group_application'); ?>" />
                    <input type="button" class="btn gray" name="schoolselect" value="<?php echo get_string('school_select', 'local_group_application'); ?>" title="<?php echo get_string('school_select', 'local_group_application'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?php echo get_string('contact', 'local_group_application'); ?></th>
                <td class="text-left">
                    <input type="text" size="6" maxlength="3" name="tel01" value="<?php echo substr($user_data->phone1, 0,3)?>" title="<?php echo get_string('contact', 'local_group_application'); ?>" /> - 
                    <input type="text" size="6" maxlength="4" name="tel01" value="<?php echo substr($user_data->phone1, 3,4)?>" title="<?php echo get_string('contact', 'local_group_application'); ?>" /> - 
                    <input type="text" size="6" maxlength="4" name="tel02" value="<?php echo substr($user_data->phone1, 7,4)?>" title="<?php echo get_string('contact', 'local_group_application'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?php echo get_string('email', 'local_group_application'); ?></th>
                <td class="text-left">
                    <input type="text" name="repuseremail" value="<?php echo $user->email?>" title="<?php echo get_string('email', 'local_group_application'); ?>" />
                </td>
            </tr>

        </tbody>
    </table>


    <h5 class="div_title top">
        <?php echo get_string('application_information', 'local_group_application'); ?>
    </h5>
    <table class="table">
        <tbody>
            <tr>
                <th><?php echo get_string('number_person', 'local_group_application'); ?></th>
                <td class="text-left">
                    <select title="year" name="year" id="year" onchange="change_year();" class="w_160">
                        <?php
                        $yearRange = 1;
                        $currentYear = date('Y');
                        $maxYear = ($currentYear + $yearRange);
                        echo '<option value="0" ' . $disabled . '>' . get_string('year_select', 'local_group_application') . '</option>';
                        foreach (range($currentYear, $maxYear) as $year) {
                            $selected = '';
                            if ($year == $course->classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . ' ' . $disabled . '>' . $year . get_string('year', 'local_group_application') . '</option>';
                        }
                        ?>
                    </select>
                    <span id ='classnum_area' >
                    <select title="<?php echo get_string('classnum', 'local_group_application'); ?>" name="scheduleid" id ='classnum' class="w_160">
                        <option value="0"><?php echo get_string('classnum_select', 'local_group_application'); ?></option>
                    </select>
                    </span>
                    <input type="text" name="usercount"  value="5" class="w_160" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/>
                    <?php echo get_string('num', 'local_group_application') . ' ' . get_string('minimum_number_of_applicants', 'local_group_application'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo get_string('paymentmethod', 'local_group_application'); ?></th>
                <td class="text-left">
                    <input type="radio" name="paymentmethod" id="pay01" value="1" />
                    <label for="pay01"><?php echo get_string('individual_payment', 'local_group_application'); ?></label>
                    <input type="radio" name="paymentmethod" id="pay02" value="2" />
                    <label for="pay02"><?php echo get_string('bill_payment', 'local_group_application'); ?></label>
                    <input type="radio" name="paymentmethod" id="pay03" value="3" />
                    <label for="pay03"><?php echo get_string('administrative_room_settlement', 'local_group_application'); ?></label>
                </td>
            </tr>
            <tr>
                <th><?php echo get_string('payment_information', 'local_group_application'); ?></th>
                <td class="text-left">
                    <p><?php echo get_string('group_application_write_text1', 'local_group_application'); ?></p>
                    <p><?php echo get_string('group_application_write_text2', 'local_group_application'); ?></p>
                    <p><?php echo get_string('group_application_write_text3', 'local_group_application'); ?></p>
                    <p><?php echo get_string('group_application_write_text4', 'local_group_application'); ?></p>
                </td>
            </tr>

        </tbody>
    </table>
    <div class="table_btn">
        <input type="submit" value="<?php echo get_string('apply', 'local_group_application'); ?>" class="btn orange big" onclick="return submit_check()"/>
        <input type="button" value="<?php echo get_string('previous', 'local_group_application'); ?>" class="btn gray big" onclick="javascript:location.href = 'group_application.php'"/>
    </div>
</form>

<?php
echo $OUTPUT->footer();
?>
<script>
    /*
     * 학교정보 호출 함수 실서버에서만 사용가능
     * @type type
     */
    $('input[name=schoolselect]').click(function () { 
        var host = $(location).attr('host');
        if(host.indexOf('eduhope.net') != -1){
            call_layerPop("/local/mypage/schoolsearch.php", "700px", "550px", {});
        } else {
            alert('본 기능은 실서버에서만 사용가능합니다.');
        }
    });
    /*
     * 벨리데이션 체크 함수
     */
    function submit_check() {

        if ($.trim($("input[name='repusername']").val()) == '') {
            alert('<?php echo get_string('group_application_write_alert1', 'local_group_application'); ?>');
            return false;
        }
        if ($.trim($("input[name='school']").val()) == '') {
            alert('<?php echo get_string('group_application_write_alert2', 'local_group_application'); ?>');
            return false;
        }
        if ($.trim($("input[name='tel01']").val()) == '' || $.trim($("input[name='tel02']").val()) == '') {
            alert('<?php echo get_string('group_application_write_alert3', 'local_group_application'); ?>');
            return false;
        }
        if($.trim($("input[name='repuseremail']").val()) == ''){
            alert('<?php echo get_string('group_application_write_alert4', 'local_group_application'); ?>');
            return false;
        } else {
            var reg_email=/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/;
            if(!reg_email.test($.trim($("input[name='repuseremail']").val()))){
                alert('<?php echo get_string('group_application_write_alert9', 'local_group_application'); ?>');
                return false;
            }
        }
        if ($("select[name=year]").val() == '0') {
            alert('<?php echo get_string('group_application_write_alert5', 'local_group_application'); ?>');
            return false;
        }
        if ($("select[name=scheduleid]").val() == '0') {
            alert('<?php echo get_string('group_application_write_alert6', 'local_group_application'); ?>');
            return false;
        }
        if ($.trim($("input[name='usercount']").val()) == '') {
            alert('<?php echo get_string('group_application_write_alert7', 'local_group_application'); ?>');
            return false;
        }
        if ($("input[name=paymentmethod]:radio:checked").length == 0) {
            alert("<?php echo get_string('group_application_write_alert8', 'local_group_application'); ?>");
            return false;
        }
    }
    /*
    * 숫제 체크 함수
     */
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            return false;
    }
    /*
    * 문자열 제거 함수
     */
    function removeChar(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
            return;
        else
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
    /*
    * 년도 변경시 신청가능한 기수를 리턴받는 함수
     */
    function change_year() {
        var year = $("select[name=year]").val();

        $.ajax({
            url: 'group_application_class.ajax.php',
            type: 'POST',
            data: {
                year: year,
            },
            success: function (data) {
                $("#classnum_area").html($(data).html());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR.responseText);
            }
        });
    }
</script>

