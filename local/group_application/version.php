<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017090700;
$plugin->requires = 2012061700;
$plugin->component = 'local_group_application'; // Full name of the plugin (used for diagnostics)
$plugin->cron     = 60;