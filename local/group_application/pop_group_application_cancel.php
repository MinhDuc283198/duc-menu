<?php
/*
 * 단체신청 취소 신청 팝업 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$submit = optional_param('submit', 0, PARAM_INT);
if ($submit == 1) {
    
    
    $data = new stdClass();
    $data->status = optional_param('status', 0, PARAM_INT);
    $data->title = optional_param('title', '', PARAM_RAW);
    $data->contents = optional_param('contents', '', PARAM_RAW);
    $data->userid = $USER->id;
    $data->timecreated = time();
    $DB->insert_record('lmsdata_group_change', $data);
    redirect('group_application_list.php');
}else{
    

?>  
<div id="popwrap">
    <h3 id="pop_title">
        단체등록 변경/취소 신청
        <img class="close r_content" src="/theme/oklassedu/pix/images/close.png" alt="" />
    </h3>

    <div id="pop_content">
        <form name="application_cancel" id="course_search" class="search_area" action="pop_group_application_cancel.php" method="get">
            <input type="hidden" name="submit" value="1">
            <table class="table">
                <tbody>
                    <tr>
                        <th>구분</th>
                        <td class="text-left">
                            <select name="status" title="change">
                                <option value="1">변경</option>
                                <option value="2">취소</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>제목</th>
                        <td class="text-left">
                            <input type="text" name="title" title="title"/>
                        </td>
                    </tr>
                    <tr>
                        <th>신청내용</th>
                        <td class="text-left">
                            <textarea name="contents" cols="70" rows="10"></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="table_btn">
                <input type="submit" value="신청하기" class="btn orange big" />
                <input type="button" value="창닫기" class="btn gray big" />
            </div>

        </form>
    </div>


</div>
<?php }?>

