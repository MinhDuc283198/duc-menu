<?php
require_once '../../config.php';
require_once $CFG->dirroot . '/local/jinoboard/lib.php';


$courseid = optional_param('id', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$searchfield = optional_param('searchfield', 'title', PARAM_RAW);
$search = strip_tags($search);
$search = htmlspecialchars($search);
$search = preg_replace("/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $search);
$searchfield = strip_tags($searchfield);
$searchfield = htmlspecialchars($searchfield);
$searchfield = preg_replace("/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $searchfield);


$like = '';
$field = '';
if ($searchfield == 'title') {
    $field = 'title';
} else if ($searchfield == 'contents') {
    $field = 'contents';
}
if (!empty($search) && !empty($field)) {
    $like .= " and " . $DB->sql_like($field, ':search', false);
}


if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('nocourseid');
}
$PAGE->set_url(new moodle_url('/local/course_objection_application/index.php', array('id' => $courseid)));
require_login($course);

$context = context_course::instance($course->id);

//require_capability('local/course_objection_application:index', $context);

if (has_capability('local/course_objection_application:index', $context)) {
    //redirect($CFG->wwwroot . "/local/course_objection_application/write.php?id=$courseid");$count_sql = "select count(c.id) from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid " . $like . " order by ca.timecreated DESC";
    $count_sql = "select count(c.id) from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid " . $like . " order by ca.timecreated DESC";
    $totalcount = $DB->count_records_sql($count_sql, array('courseid' => $courseid, 'search' => '%' . $search . '%'), $offset, $perpage);
} else {

    $count_sql = "select count(c.id) from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid  and ca.userid=:userid  " . $like . " order by ca.timecreated DESC";
    $totalcount = $DB->count_records_sql($count_sql, array('courseid' => $courseid, 'userid' => $USER->id, 'search' => '%' . $search . '%'), $offset, $perpage);
}
$total_pages = jinoboard_get_total_pages($totalcount, $perpage);

//require_capability('moodle/grade:viewall', $context);
/*
  $count_sql = "select count(c.id) from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid " . $like . " order by ca.timecreated DESC";
  $totalcount = $DB->count_records_sql($count_sql, array('courseid' => $courseid, 'search' => '%' . $search . '%'), $offset, $perpage);
  $total_pages = jinoboard_get_total_pages($totalcount, $perpage);
 */



echo $OUTPUT->header();
?>
<div class="tab-table-section" class="white-bg">
    <!-- 검색 폼 시작 -->

    <form class="table-search-option">
        <input type="hidden" name="id" value="<?php echo $courseid; ?>">
        <select name="searchfield" title="title" >
            <option value="title" <?php if ($searchfield == 'title') { ?> selected="selected"<?php } ?> > <?php echo get_string('title', 'local_jinoboard'); ?> </option>
            <option value="contents" <?php if ($searchfield == 'contents') { ?> selected="selected"<?php } ?>><?php echo get_string('content', 'local_jinoboard'); ?></option>
        </select>
        <input type="text" title="search" src="javascript:alert('XSS');" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('input','local_jinoboard')?>">
        <input type="submit" value="<?php echo get_string('search', 'local_jinoboard'); ?>" class="board-search"/>
        <br>
    </form>
<?php
$rows = array();
$rows[0] = new tabobject(0, "$CFG->wwwroot/local/jinoboard/list.php?id=$board->id", get_string('all'));
if (!is_siteadmin()) {
    foreach ($boardcategory as $boardca) {
        $usertypes = explode('/', $boardca->usertype);
        foreach ($usertypes as $usertype) {
            if ($usertype == $luser->usertypecode) {
                $rows[$boardca->id] = new tabobject($boardca->id, "$CFG->wwwroot/local/jinoboard/list.php?id=$board->id&boardca=$boardca->id", $boardca->name);
            }
        }
    }
} else {
    foreach ($boardcategory as $boardca) {
        $rows[$boardca->id] = new tabobject($boardca->id, "$CFG->wwwroot/local/jinoboard/list.php?id=$board->id&boardca=$boardca->id", $boardca->name);
    }
}
if ($board->allowcategory == 1) {

    print_tabs(array($rows), $boardcavalue);
}
?>
    <!-- 검색 폼 끝 --> 
    <div class="table-header-area">     
        <!-- 게시글 수 정보 표시 및 게시글 표시 갯수 설정 -->
        <form>
            <input type="hidden" name="id" value="<?php echo $courseid; ?>">
            <input type="hidden" name="search" value="<?php echo $search; ?>">
            <select name="perpage" onchange="this.form.submit();" title="page">
<?php
$nums = array(10, 20, 30, 50);
foreach ($nums as $num) {
    $selected = ($num == $perpage) ? 'selected' : '';

    echo '<option value="' . $num . '" ' . $selected . '>' . get_string('showperpage', 'local_jinoboard', $num) . '</option>';
}
?>
            </select>
            <span class="table-count">

<?php echo '(' . $page . '/' . $total_pages . get_string('page', 'local_jinoboard') . ',' . get_string('total', 'local_jinoboard') . $totalcount . get_string('case', 'local_jinoboard') . ')'; ?>
            </span>
        </form>
        <!-- 게시글 수 정보 표시 및 게시글 표시 갯수 설정 끝 -->
    </div>

    <div class="table-filter-area">
<?php
//if ($myaccess->allowwrite == 'true') {
?>
        <input type="button" class="right" value="<?php echo get_string('writepost', 'local_jinoboard') ?>" onclick="location.href = 'write.php?mode=write&courseid=<?php echo $courseid; ?>'" />
        <?php //} ?>
    </div>

<?php
$offset = 0;
if ($page != 0) {
    $offset = ($page - 1) * $perpage;
}
$list_num = $offset;
$num = $totalcount - $offset;
//$sql = "select * from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid";
//$contents = $DB->get_records_sql($sql, array('courseid' => $courseid));
if (has_capability('local/course_objection_application:index', $context)) {
    $sql = "select * from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid " . $like . " order by ca.timecreated DESC";
    $contents = $DB->get_records_sql($sql, array('courseid' => $courseid, 'search' => '%' . $search . '%'), $offset, $perpage);
} else {
    $sql = "select * from {course} as c join {course_appeal}  ca on c.id = ca.courseid where c.id=:courseid and ca.userid=:userid " . $like . " order by ca.timecreated DESC";
    $contents = $DB->get_records_sql($sql, array('courseid' => $courseid, 'userid' => $USER->id, 'search' => '%' . $search . '%'), $offset, $perpage);
}
?>
    <div class="thread-style">
        <ul class="thread-style-lists">
    <?php
    foreach ($contents as $content) {
        $list_num++;

        $postuser = $DB->get_record('user', array('id' => $content->userid));
        $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
        $fullname = fullname($postuser);
        $userdate = userdate($content->timecreated,'%Y-%m-%0d %H:%m');
        $by = new stdClass();
        $by->name = $fullname;

        if (is_siteadmin()) {
            $by->name .= '(' . $postuser->username . ')';
        }
        $by->date = $userdate;
        $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
        $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
        $coursename = '';

        echo "<li class='' " . $date_left . ">";
        echo "<div class='thread-content'><span class='post-title'>" . $step_icon;

        echo "<a href='" . $CFG->wwwroot . "/local/course_objection_application/detail.php?id=" . $content->id . "&courseid=" . $course->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&searchfield=" . $searchfield . "'>" . $coursename . " " . $content->title . $new . "</a>";
        //echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/detail_uncore.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&board=" . $id . "&searchfield=" . $searchfield . "' class='" . $newClass . "'>" . $content->title . $new . "</a>";

        echo '<br/><span class="post-date">' . get_string("bynameondate", "local_jinoboard", $by) . '</span>';
        echo "</span></div>";
        //echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
        echo "</li>";
        $num--;
    }
    if (empty($contents)) {
        ?>
                <li style="padding-left:0px; width:calc(100% - 0px) !important;">
                    <div class="thread-empty">
                        <?php echo get_string('nocontent', 'mod_jinotechboard')?>
                    </div>
                </li>        
    <?php
}
?>

        </ul>

    </div>
</div>
<div class="table-footer-area">
<?php
$page_params = array();
$page_params['id'] = $courseid;
$page_params['perpage'] = $perpage;
$page_params['search'] = $search;
$page_params['searchfield'] = $searchfield;
//$page_params['boardca'] = $boardcavalue;
//echo print_r($page_params)."<br>".$total_pages;exit;
jinoboard_get_paging_bar($CFG->wwwroot . "/local/course_objection_application/index.php", $page_params, $total_pages, $page, 0);
?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->
    <?php
//echo print_r($context);exit;
//echo $OUTPUT->heading(get_string('pluginname', 'gradereport_user') . ' - ' . $studentnamelink);
//echo $OUTPUT->notification(get_string('groupusernotmember', 'error'));
    echo $OUTPUT->footer();
    