<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
require_once $CFG->dirroot . '/lib/form/filemanager.php';
require_once $CFG->dirroot . '/local/course_objection_application/write_form.php';

$mode = optional_param('mode', 'write', PARAM_RAW);
$courseid = optional_param('courseid', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);


if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('nocourseid');
}
require_login($course);

$context = context_course::instance($course->id);

$content = $DB->get_record('course_appeal', array('id' => $id));
if (empty($content)) {
    //$board = $DB->get_record('course', array('id' => $courseid));
    $ten_day = strtotime(date("Y-m-d", strtotime("+10 day")));
    $content = new stdClass();
    $content->contents = '';
    $content->title = '';
    //$content->isnotice = isset($content->isnotice) ? $content->isnotice : 0;
    //$content->isemergency = isset($content->isemergency) ? $content->isemergency : 0;
    //$content->issecret = isset($content->issecret) ? $content->issecret : 0;
    //$content->timeend = $ten_day;
} else {
    //$board = $DB->get_record('course', array('id' => $courseid));
    
}

//echo print_r($content);exit;



if ($mode == "delete") {
    // 권한이 없는 사용자나 본인이 아닐경우 삭제 안되도록 추가 - 2019.02.13 이지환
/*
    if($myaccess->allowdelete != 'true'){
        redirect($CFG->wwwroot, 'Permission Denied');
    }
*/   
    require_capability('local/course_objection_application:delete', $context);
    $DB->delete_records('course_appeal', array('id' => $id));
    //$DB->delete_records('jinoboard_comments', array('contentsid' => $id, 'board' => $board->id));
    redirect("index.php?id=$courseid");
}

$options = array('noclean' => true, 'subdirs' => true, 'maxfiles' => -1, 'maxbytes' => 2048, 'context' => $context);
$mform = new appeal_write_form(null, array('options' => $options,
    'context' => $context,
    'content' => $content
    , 'mode' => $mode)
);



if ($mform->is_cancelled()) {
    redirect("index.php?id=$courseid");
} else if ($fromform = $mform->get_data()) {
    if ($mode == "edit") {
        $content = new stdClass();
        $content->id = $fromform->id;
	$content->title = $fromform->name;
	$content->contents = $fromform->contents;
        $content->itemid = $content->contents['itemid'];
        
	$DB->update_record('course_appeal', $content);
        $content->contents = file_save_draft_area_files($content->itemid, $context->id, 'local_jinoboard', 'contents', $content->id, jinoboard_editor_options($context, $content->id), $content->contents['text']);
        
        $DB->set_field('course_appeal', 'contents', $content->contents, array('id' => $content->id));
        redirect($CFG->wwwroot . "/local/course_objection_application/detail.php?id=$id&courseid=$courseid");
    } else {
        $content = new stdClass();
        $content->title = $fromform->name;
        $content->contents = $fromform->contents;
        $content->itemid = $content->contents['itemid'];

        $content->courseid = $courseid;        
        $content->userid = $USER->id;
        $content->timecreated = time();
        
        
        $mcontent->title = $fromform->name;
        $mcontent->courseid = $courseid;        
        $mcontent->userid = $USER->id;
        $mcontent->timecreated = time();
        $mcontent->contents = $fromform->contents;
        
        $newid = $DB->insert_record('course_appeal', $mcontent);
        $content->id = $newid;
        $content->contents = file_save_draft_area_files($content->itemid, $context->id, 'local_jinoboard', 'contents', $content->id, jinoboard_editor_options($context, $content->id), $content->contents['text']);
        
        $DB->set_field('course_appeal', 'contents', $content->contents, array('id' => $content->id));
		
                
                
        echo "<script>alert('이의신청이 완료되었습니다.');</script>";
        redirect($CFG->wwwroot . "/local/course_objection_application/detail.php?id=$newid&courseid=$courseid");
    }  
    
}

$draftitemid = file_get_submitted_draft_itemid('attachments');
file_prepare_draft_area($draftitemid, $context->id, 'local_jinoboard', 'attachment', empty($content->id) ? null : $content->id);

$draftid_editor = file_get_submitted_draft_itemid('contents');

$boardname="";


if ($mode != 'write') {
    $currenttext = file_prepare_draft_area($draftid_editor, $context->id, 'local_jinoboard', 'contents', $id, appeal_write_form::editor_options($context, $id), $content->contents);
    $mform->set_data(array('attachments' => $draftitemid,
        'general' => $boardname,
        'name' => $content->title,
        'timeend' => $content->timeend,
        'contents' => array(
            'text' => $currenttext,
            'format' => 1,
            'itemid' => $draftid_editor,
        ),
        'board' => $board->id));
}

echo $OUTPUT->header();
?>
<h3 class="page_title"><?php echo $boardname ?></h3>
<?php

$mform->display();


echo $OUTPUT->footer();