<?php
require_once '../../config.php';
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
//require_login();


$courseid = optional_param('courseid', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);


$mode = optional_param('mode', 'write', PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$perpage = optional_param('perpage', 10, PARAM_INT);
$list_num = optional_param('list_num', 10, PARAM_INT);
$searchfield = optional_param('searchfield', 'title', PARAM_RAW);



if (!$course = $DB->get_record('course', array('id' => $courseid))) {
   print_error('nocourseid');
}
//$PAGE->set_url(new moodle_url('/local/course_objection_application/detail.php', array('id' => $courseid)));
require_login($course);
//$PAGE->set_pagelayout('course');


$context = context_course::instance($course->id);


     
        
//require_capability('local/course_objection_application:viewcontent', $context);
echo $OUTPUT->header();

$boardname = "이의신청";
$content = $DB->get_record('course_appeal', array('id' => $id));
/*
if ( !has_capability('local/course_objection_application:viewcontent', $context) || $USER->id !== $content->userid){
    redirect($CFG->wwwroot . "/local/course_objection_application/index.php?id=$courseid");
}
 */

$postuser = $DB->get_record('user', array('id' => $content->userid));
$fullname = fullname($postuser);
$userdate = userdate($content->timecreated,'%Y-%m-%0d %H:%m');


$by = new stdClass();
$by->name = $fullname;
$by->date = $userdate;
$output = html_writer::start_tag('div', array('class' => 'board-detail-area'));
?>
<h3 class="page_title"><?php echo $boardname ?></h3>
<?php

$output .= html_writer::start_tag('div', array('class' => 'detail-title-area'));
$output .= html_writer::tag('span', $coursename.strip_tags($content->title), array('class' => 'detail-title'));
$output .= html_writer::tag('span', '<span class="writer">' . get_string("bynameondate", "local_jinoboard", $by) . '</span>', array('class' => 'detail-date'));
$output .= html_writer::end_tag('div');

//내용영역
$content->contents = file_rewrite_pluginfile_urls($content->contents, 'pluginfile.php', $context->id, 'local_jinoboard', 'contents', $content->id);


$output .= html_writer::tag('div', local_jinoboard_html_filter($content->contents), array('class' => 'detail-contents'));



$left_btns = html_writer::tag('button', get_string('list'), array('class' => 'btn gray h30', 'onclick' => 'location.href="' . $CFG->wwwroot . '/local/course_objection_application/index.php?id=' . $courseid .'&search='.$search.'&page='.$page.'&perpage='.$perpage."&searchfield=".$searchfield.'"'));

//$left_btns .= html_writer::tag('button', get_string('replies', 'local_jinoboard'), array('class' => 'btn br h30', 'onclick' => 'location.href="' . $CFG->wwwroot . '/local/jinoboard/mywrite_uncore.php?id=' . $content->id . '&mode=reply&board=' . $board->id.'"'));
$right_btns = "";

if (has_capability('local/course_objection_application:edit', $context) || $USER->id == $content->userid) {
    $right_btns .= html_writer::tag('button', get_string('edit', 'local_jinoboard'), array('onclick' => 'location.href="' . $CFG->wwwroot . '/local/course_objection_application/write.php?id=' . $content->id . '&mode=edit&courseid=' . $courseid . '"', 'class' => 'btn pink h30'));
}
if (has_capability('local/course_objection_application:delete', $context) || ($USER->id == $content->userid)) {
    $right_btns .= html_writer::tag('button', get_string('delete', 'local_jinoboard'), array('class' => 'btn black h30', 'onclick' => 'if (confirm("정말 삭제하시겠습니까??") == true){location.href="' . $CFG->wwwroot . '/local/course_objection_application/write.php?id=' . $content->id . "&mode=delete&courseid=" . $courseid . '"}'));
}


//$right_btns .= html_writer::tag('button', get_string('edit', 'local_jinoboard'), array('onclick' => 'location.href="' . $CFG->wwwroot . '/local/course_objection_application/write.php?id=' . $content->id . '&mode=edit&courseid=' . $courseid . '"', 'class' => 'btn pink h30'));
//$right_btns .= html_writer::tag('button', get_string('delete', 'local_jinoboard'), array('class' => 'btn black h30', 'onclick' => 'if (confirm("정말 삭제하시겠습니까??") == true){location.href="' . $CFG->wwwroot . '/local/course_objection_application/write.php?id=' . $content->id . "&mode=delete&courseid=" . $courseid . '"}'));

/*
if (has_capability('local/course_objection_application:delete', $context)) {
    $right_btns .= html_writer::tag('button', get_string('edit', 'local_jinoboard'), array('onclick' => 'location.href="' . $CFG->wwwroot . '/local/jinoboard/write_uncore.php?id=' . $content->id . '&mode=edit&board=' . $board->id . '"', 'class' => 'btn pink h30'));
    $right_btns .= html_writer::tag('button', get_string('delete', 'local_jinoboard'), array('class' => 'btn black h30', 'onclick' => 'if (confirm("정말 삭제하시겠습니까??") == true){location.href="' . $CFG->wwwroot . '/local/course_objection_application/write.php?id=' . $content->id . "&mode=delete&courseid=" . $courseid . '"}'));
}
*/
/*
if (has_capability('local/jinoboard:edit', $context) || $USER->id == $content->userid) {
    $right_btns .= html_writer::tag('button', get_string('edit', 'local_jinoboard'), array('onclick' => 'location.href="' . $CFG->wwwroot . '/local/jinoboard/write_uncore.php?id=' . $content->id . '&mode=edit&board=' . $board->id . '"', 'class' => 'btn pink h30'));
}
if (has_capability('local/jinoboard:delete', $context) || ($USER->id == $content->userid)) {
    $right_btns .= html_writer::tag('button', get_string('delete', 'local_jinoboard'), array('class' => 'btn black h30', 'onclick' => 'if (confirm("정말 삭제하시겠습니까??") == true){location.href="' . $CFG->wwwroot . '/local/jinoboard/write_uncore.php?id=' . $content->id . "&mode=delete&type=" . $board->type . '"}'));
}
*/
$cols = html_writer::tag('div', $left_btns, array('class' => "btn-area btn-area-left"));
$cols .= html_writer::tag('div', $right_btns, array('class' => "btn-area btn-area-right"));
$output .= html_writer::tag('div', $cols, array('class' => "table-footer-area"));
$output .= html_writer::end_tag('div');

echo $output;
?>



<?php

//echo print_r($context);exit;
//echo $OUTPUT->heading(get_string('pluginname', 'gradereport_user') . ' - ' . $studentnamelink);
//echo $OUTPUT->notification(get_string('groupusernotmember', 'error'));
echo $OUTPUT->footer();