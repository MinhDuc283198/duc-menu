<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/repository/lib.php');

class appeal_write_form extends moodleform {

    public static function attachment_options($board) {
        global $PAGE, $CFG;
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'subdirs' => 0,
            'maxbytes' => $maxbytes,
            'maxfiles' => $board->maxattachments,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL
        );
    }

    public static function editor_options($context, $contentid) {
        global $PAGE, $CFG;
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'local_jinoboard', 'contents', $contentid)
        );
    }

    function definition() {
        global $CFG, $DB, $USER;

        $id = optional_param('id', 0, PARAM_INT);
        $courseid = optional_param('courseid', 0, PARAM_INT);
        $mode = optional_param('mode', "write", PARAM_CLEAN);

        $board = $DB->get_record('course', array('id' => $courseid));

        $options = $this->_customdata['options'];
        $context = $this->_customdata['context'];
        $content = $this->_customdata['content'];

        $mform = $this->_form;
        
        //$mform->addElement('header', 'nameforyourheaderelement', get_string('add', 'local_jinoboard'));
        //$mform->setExpanded('nameforyourheaderelement', true);
        

        $titlearray = array();
        $titlearray[] = & $mform->createElement('text', 'name', get_string('title', 'local_jinoboard'));
        if ($board->allownotice && $mode != 'reply') {
            if (has_capability('local/jinoboard:write', $context)) {
                $titlearray[] = & $mform->createElement('advcheckbox', 'isnotice', '', get_string('notice', 'local_jinoboard'), array('group' => 1), array(0, 1));
                $titlearray[] = & $mform->createElement('advcheckbox', 'isemergency', '', get_string('emergency', 'local_jinoboard'), array('group' => 1), array(0, 1));
            }
        }
        $mform->addGroup($titlearray, 'titlear', get_string('title', 'local_jinoboard'), ' ', false);
        $mform->setType('name', PARAM_CLEAN);
        
        if ($board->allownotice && $mode != 'reply')
            $mform->setDefault('isnotice', $content->isnotice);
        $mform->addRule('titlear', null, 'required');
        $mform->setDefault('isemergency', $content->isemergency);
        $titlegrprules = array();
        $titlegrprules['name'][] = array(null, 'required', null, 'client');
        $mform->addGroupRule('titlear', $titlegrprules);
        
        if ($board->allowsecret) {
            $radioarray = array();
            $radioarray[] = & $mform->createElement('radio', 'issecret', '', get_string('issecretno', 'local_jinoboard'), 0);
            $radioarray[] = & $mform->createElement('radio', 'issecret', '', get_string('issecretyes', 'local_jinoboard'), 1);
            $mform->addGroup($radioarray, 'radioar', get_string('issecret', 'local_jinoboard'), array(' '), false);
            $mform->setDefault('issecret', $content->issecret);
            $mform->addRule('radioar', null, 'required');
        }

        if ($board->allowcategory) {
            $categorys = $DB->get_records('jinoboard_category', array('isused' => 1,'board'=>$bid),'sortorder');
            $categoryary = array();
            $uid = $USER->id;
            $luser = $DB->get_record('lmsdata_user', array('userid' => $uid));

          $categoryary[''] = '선택';
            
            foreach ($categorys as $category) {
                 $usertypes = explode('/', $category->usertype);          
                 //usertype 추가 => 임직원 문구로 인한 추가임.
                $mform->addElement('hidden', 'usertype'.$category->id);
                $mform->setType('usertype'.$category->id, PARAM_CLEAN);
                $mform->setDefault('usertype'.$category->id,$category->usertype);
                if (!is_siteadmin()) {
                    foreach($usertypes as $usertype){                            
                        if($usertype == $luser->usertypecode) {
                            $categoryary[$category->id] = $category->name;    
                        }
                    }
                }else{
                    $categoryary[$category->id] = $category->name;
                }
            }
            $cataarray = array();
            
            $cataarray[] = & $mform->createElement('select', 'category', '카테고리', $categoryary, array ( 'onchange'=> 'javascript : cc(this);'));
 
            $mform->addGroup($cataarray, 'availablefromgroup', '카테고리', ' ', false);
            $mform->setType('category', PARAM_CLEAN);
            $mform->setDefault('category', $content->category); 
            $mform->addRule('availablefromgroup', '카테고리를 선택하시오', 'required');           
            
            $mform->addElement('html','<script src="/local/jinoboard/form.js"></script>');
    }
       
        
        
        
        $editor = $mform->addElement('editor', 'contents', get_string('content', 'local_jinoboard'), null, self::editor_options($context, (empty($content->id) ? null : $content->id)));
        $mform->setType('contents', PARAM_RAW);
        $mform->addRule('contents', null, 'required', null, 'client');
        
        if ($board->allowperiod) {
            $ten_day = strtotime(date("Y-m-d",strtotime("today")));
//                    strtotime(date("Y-m-d", strtotime("+10 day")));
            $availablefromgroup = array();
            $availablefromgroup[] = & $mform->createElement('date_selector', 'timeend', '');
            $availablefromgroup[] = & $mform->createElement('checkbox', 'availablefromenabled', '', get_string('enable'));
            $mform->addGroup($availablefromgroup, 'availablefromgroup', get_string('timeend', 'local_jinoboard'), ' ', false);
            $mform->disabledIf('availablefromgroup', 'availablefromenabled');
            $mform->setDefault('timeend', $ten_day);
            $mform->setDefault('availablefromenabled', false);
            $mform->setType('timeend', PARAM_INT);
            $mform->setType('availablefromenabled', PARAM_INT);
        }

        if ($board->allowupload) {

            $chk = "<input type='checkbox' title='첨부파일' name='view_filemanager' /> " . get_string('attachmentcheck', 'local_jinoboard');
            $mform->addElement('static', 'static', get_string('attachment', 'local_jinoboard'), $chk);

            $mform->addGroup(array(
                $mform->createElement('filemanager', 'attachments', get_string('attachment', 'local_jinoboard'), null, self::attachment_options($board)),
                $mform->createElement('static', 'text', '', $staticmsg)
                    ), 'filemanager', '', array(''), false);
        }
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $id);
        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);
        $mform->setDefault('courseid', $courseid);
        $mform->addElement('hidden', 'mode');
        $mform->setType('mode', PARAM_CLEAN);
        $mform->setDefault('mode', $mode);

        $this->add_action_buttons();
 
    }

    function validation($data, $files) {
        $data['timeend']= optional_param_array('timeend', 0, PARAM_INT);
        if(strlen($data['timeend']['month'])!=2){
            $month = '0'.$data['timeend']['month'];
        }
        $today = date("Ymd");
        $chkday =$data['timeend']['year'].$month.$data['timeend']['day'];

        //name=isnotice 가 checked면 id_availablefromenabled를 체크해주기
        $errors = parent::validation($data, $files);
        if($chkday!=0){
            if($today>=$chkday){
                $errors['availablefromgroup'] = '게시 종료일은 오늘 이후의 날짜로 설정되어야 합니다.';
            }         
        }
        return $errors;
    }

}
