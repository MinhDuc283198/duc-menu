<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019052402;
$plugin->requires  = 2012061700;       
$plugin->component = 'local_course_objection_application'; // Full name of the plugin (used for diagnostics)