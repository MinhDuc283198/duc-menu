<?php
?>
<div class="thread-style">
        <ul class="thread-style-lists">
            <?php
            if ($board->allownotice == 1) {
                //$sql = "select * from {jinoboard_contents} jc where board = :board " . $like . " and isnotice = 1 order by ref DESC, step ASC";
                $notices = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'));
                foreach ($notices as $content) {
                    $today = time();
                    if($today>=$content->timeend){
                        //상단게시기간이 지난 게시글의 경우 isnotice 칼럼을 0으로 변경
                        $param = new stdClass();
                        $param->id = $content->id;
                        $param->isnotice = 0;
                        $DB->update_record('jinoboard_contents',$param);
                        continue;
                    } else {
                    $postuser = $DB->get_record('user', array('id' => $content->userid));
                    $fullname = fullname($postuser);
                    $userdate = userdate($content->timecreated);
                    $by = new stdClass();
                    $by->name = $fullname;
                    $by->date = $userdate;
                    $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
                    if(is_siteadmin()){
                        $by->name .= '('.$postuser->username.')';
                    }
                    $fs = get_file_storage();
                    if (!empty($notice->id)) {
                        $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $notice->id, 'timemodified', false);
                    } else {
                        $files = array();
                    }

                    $filecheck = (count($files) > 0) ? '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">' : "";


                    $step = ($content->lev <= 4) ? $content->lev : 4;
                    $date_left_len = $step * 30;
                    $calcwidth = ($content->lev <= 4) ? $content->lev * 30 : 120;
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                    $step_icon = ($content->lev) ? '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="reply" /> ' : '';
                    $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
                    $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
                    
                    $userpicture = new user_picture($postuser);
                    $userpicture->size = 1; // Size f1.
                    $url = $userpicture->get_url($PAGE)->out(false);
                    echo "<li class='isnotice' " . $date_left . ">";
                    echo "<div class='thread-content'><span class='post-title'>" . $step_icon;
                    if (($content->issecret && $USER->id != $content->userid && !$parent->userid != $USER->id) || ($content->issecret && $myaccess->allowsecret != 'true')) {
                        echo $content->title . $new;
                    } else if ($myaccess->allowdetail != 'true') {
                        echo '<a href="#" class="' . $newClass . '" onclick="alert(' . "'로그인이 필요한 콘텐츠입니다.'" . ')">' . $content->title . $new . '"</a>"';
                    } else {
                        echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/detail_uncore.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&board=" . $id . "&searchfield=" . $searchfield . "' class='" . $newClass . "'>" . $content->title . $new . "</a>";
                    }
                    echo "  " . $filecheck;
                    if ($content->issecret) {
                        echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='15' height='15' alt='" . get_string('secreticon', 'local_jinoboard') . "' title='" . get_string('secreticon', 'local_jinoboard') . "'>";
                    }
                    echo '<br/><span class="post-date"><a href="' . $CFG->wwwroot . '/local/lmsdata/user_info.php?id=' . $postuser->id . '">' . get_string("bynameondate", "local_jinoboard", $by) . '</a></span>';
                    echo "</span></div><div class='thread-right'>";
                    echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
                    echo "</div></li>";
                    }
                }
            }
            foreach ($contents as $content) {
                

                if ($board->allowrental == '1') {
                    switch ($content->status) {
                        case 0: $content->title .= '&nbsp;[' . get_string('apply', 'local_jinoboard') . ']';
                            break;
                        case 1: $content->title .= '&nbsp;[' . get_string('yes', 'local_jinoboard') . ']';
                            break;
                        case 2: $content->title .= '&nbsp;[' . get_string('refuse', 'local_jinoboard') . ']';
                            break;
                    }
                }
                $list_num++;
                $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
                $fs = get_file_storage();
                $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $content->id, 'timemodified', false);
                if (count($files) > 0) {
                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                } else {
                    $filecheck = "";
                }

                $step = ($content->lev <= 4) ? $content->lev : 4;
                $date_left_len = $step * 30;
                $calcwidth = ($content->lev <= 4) ? $content->lev * 30 : 120;
                $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                $step_icon = ($content->lev) ? '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="reply" /> ' : '';


                $postuser = $DB->get_record('user', array('id' => $content->userid));
                $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
                $fullname = fullname($postuser);
                $userdate = userdate($content->timecreated);
                $by = new stdClass();
                $by->name = $fullname;
                
                if(is_siteadmin()){
                    $by->name .= '('.$postuser->username.')';
                }
                $by->date = $userdate;
                $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
                $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
                $coursename = '';
                    $userpicture = new user_picture($postuser);
                    $userpicture->size = 1; // Size f1.
                    $url = $userpicture->get_url($PAGE)->out(false);
                    echo "<li class='' " . $date_left . ">";
                    echo "<div class='thread-content'><span class='post-title'>" . $step_icon;
                if($board->allowcourseselection == 1){
                    $coursename = '['.$DB->get_field('lmsdata_course', 'coursename', array('id'=>$content->lcourseid)).']';
                }
                if (($content->issecret && $USER->id != $content->userid && $parent->userid != $USER->id && $myaccess->allowsecret != 'true') && !is_siteadmin()) {
                    echo $content->title . $new.'1';
                } else if ($myaccess->allowdetail != 'true') {
                    echo '<a href="#" class="' . $newClass . '" onclick="alert(' . "'해당 글을 볼 수 있는 권한이 없습니다.'" . ')"> '.$coursename.' '. $content->title . $new . '"</a>"';
                } else {
                    echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/detail_uncore.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&board=" . $id . "&searchfield=" . $searchfield . "' class='" . $newClass . "'>".$coursename." ".$content->title . $new . "</a>";
                }
                echo "  " . $filecheck;
                if ($content->issecret) {
                    echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='20' height='20' alt='" . get_string('secreticon', 'local_jinoboard') . "' title='" . get_string('secreticon', 'local_jinoboard') . "'>";
                }

                echo '<br/><span class="post-date">' . get_string("bynameondate", "local_jinoboard", $by) . '</span>';
                echo "</span></div><div class='thread-right'>";
                echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
                echo "</div></li>";
                $num--;
            }
            ?>

            <?php
            if (empty($notices) && empty($contents)) {
                ?>
                    <li style="padding-left:0px; width:calc(100% - 0px) !important;">
                        <div class="thread-empty">
                            등록된 글이 없습니다
                        </div>
                    </li>        
                <?php
            }
            ?>
        </ul>

    </div>

