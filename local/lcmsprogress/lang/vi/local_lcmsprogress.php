<?php
$string['pluginadministration'] = 'Quảng lý tiến trình';
$string['maxgrade'] = 'Bậc cao nhất';
$string['pluginname'] = 'Thống kê nội dung bài giảng';

$string['lcmsprogressname'] = 'Tên hoạt động';
$string['description'] = 'Mô tả';
$string['lcmscontentno'] = 'NO.';
$string['lcmscontentname'] = 'Môn học';
$string['lcmscontent'] = 'Nội dung';
$string['timetotal'] = 'Tổng';
$string['timeview'] = 'Lượt xem';
$string['lastview'] = 'Lượt xem trước';
$string['name'] = 'Học viên';
$string['progress'] = 'Tiến trình';
$string['progressrate'] = 'Tỉ lệ';
$string['progresslog'] = 'Nhật ký';
$string['progressscore'] = 'Điểm: <span class="lcmsprogress_score_text">{$a}%</span>';
$string['viewreport'] = 'Điểm tiến độ';

$string['completionprogressgroup'] = 'Điều kiện tiến độ';
$string['completionprogress'] = 'Tiến độ hoàn thành';
$string['errorcompletionprogressvalue'] = '';
$string['neverseen'] = 'Không tìm thấy';

$string['emptylcmsdata'] = 'Không có dữ liệu';
$string['search'] = 'Tìm';
$string['lcmscontentview'] = 'Xem';
$string['lcmscontentdown'] = 'Tải về';
$string['lcmscontentreg'] = 'Đăng ký';
$string['prevpage'] = 'Lùi';

$string['starttime'] = 'Thời gian bắt đầu';
$string['enddtime'] = 'Thời gian kết thúc';
$string['studytime'] = 'Thời gian học';
$string['progresslog'] = 'Video nhật ký tiến độ';

$string['nocontent'] = 'Không có nội dung';