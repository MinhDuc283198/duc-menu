<?php

require(dirname(dirname(__FILE__)) . '/config.php');
require_once($CFG->libdir.'/moodlelib.php');

$context = context_system::instance();
$job_id = 1;
$data = array('context' => $context, 'objectid' => $job_id,
    'other' => array('jobid' => $job_id));
$event = \local_visang\event\job_posted::create($data);
$event->trigger();

?>
