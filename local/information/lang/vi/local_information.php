<?php

$string['shareagree'] = 'Thỏa thuận chia sẻ thông tin';
$string['txt1'] = 'Bằng việc đồng ý chia sẻ thông tin, bạn có thể sử dụng cả Master Korean và Master Korean Job với cùng 1 tài khoản đăng ký.';
$string['txt2'] = 'Thông tin cơ bản <br/> (tài khoản, tên, ngày sinh, email, số điện thoại di động)';
$string['coursehistroy'] = 'Lịch sử khóa học';
$string['txt3'] = 'Trạng thái hỗ trợ <br/> Thông tin quan tâm';
$string['agree'] = 'tôi đồng ý';
$string['not'] = 'Bạn sẽ không truy cập được vào trang web nếu như bạn không đồng ý chia sẻ thông tin.';
