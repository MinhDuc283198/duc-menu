<?php

$string['shareagree'] = '정보 공유 동의';
$string['txt1'] = '<strong>Master Korean과 Master Korean jobs는</strong><br/> 회원정보 공유를 통해 동일 아이디로 사이트 이용이 가능합니다.';
$string['txt2'] = '회원 기본정보<br/>(계정, 이름, 생년월일, 이메일, 휴대폰 번호)';
$string['coursehistroy'] = '수강이력';
$string['txt3'] = '지원현황<br/>관심정보';
$string['agree'] = '동의합니다';
$string['not'] = '정보 공유에 동의하지 않으면 동일 아이디로 양 사이트 이용이 불가합니다.';