<?php

$string['shareagree'] = 'Information Sharing Agreement';
$string['txt1'] = '<strong> Master Korean and Master Korean jobs </ strong> <br/> Share your information and use the same ID.';
$string['txt2'] = 'Basic Member Information <br/> (account, name, date of birth, email, mobile number)';
$string['coursehistroy'] = 'Course history';
$string['txt3'] = 'Support Status <br/> Interest Information';
$string['agree'] = 'I agree';
$string['not'] = 'If you do not agree to share information, you cannot use the site with the same ID.';