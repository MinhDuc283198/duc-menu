<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('정보 공유 동의');

echo $OUTPUT->header();
//$id = optional_param("id", 0, PARAM_INT);
$id = $USER->id;
?>  
<div class="layerpop info_confirm">
    <div class="pop-title">
        <?php echo get_string('shareagree','local_information')?>
<!--        <a href="#" class="pop-close">닫기</a>-->
    </div>
    <div class="pop-contents">
        <div class="logo-area">
            <img src="/theme/oklassedu/pix/images/hd_logo.png" alt="master korean" /> 
            <img src="/theme/oklassedu/pix/images/hd_logo_job.png" alt="master korean jobs" /> 
        </div>
        <div class="text-center">
            <?php echo get_string('txt1','local_information')?>
        </div>
        <table class="table gray">
            <thead>
                <tr>
                    <th>Master Korean</th>
                    <th>Master Korean jobs</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2"><?php echo get_string('txt2','local_information')?></td>
                </tr>
                <tr>
                    <td><?php echo get_string('coursehistroy','local_information')?></td>
                    <td><?php echo get_string('txt3','local_information')?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="<?php echo get_string('agree','local_information')?>" class="btns big" onclick="agree(<?php echo $id?>)"  />
        <p class="t-red text-center"><span class="t-info"><?php echo get_string('not','local_information')?></span></p>
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


