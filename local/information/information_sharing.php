<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/information/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');


$id = optional_param("id", 0, PARAM_INT);
if($id == 0){
    $id = $USER->id;
}
//$id = 2;
//$id = $USER->id;

if($id == 0){
//    echo "<script>
//        document.location.href='".$CFG->job_url."/local/job';
//        </script>";
//    die();
}else{
    //lu.joinstatus=:joinstatus AND 
    $query = "select lu.* from {user} u JOIN {lmsdata_user} lu ON u.id = lu.userid AND lu.userid=:userid WHERE ( lu.statuscode is null or lu.statuscode = 0 or lu.statuscode = '')";
    $user = $DB->get_record_sql($query,array('userid'=>$id, 'joinstatus'=>'e', 'statuscode'=>'1'));
    //if(!empty($user)){
    if($user->joinstatus == "e"){
        require_once($CFG->dirroot . '/local/job/lib.php');

        global $CFG, $VISANG, $DB, $USER;

        // ===================================================================================================
        // handles
        require_once($VISANG->dirroot . '/lib/common_functions.php');
        require_once($VISANG->dirroot . '/lib/jobs.php');

        $VISANG->theme->title = get_string('pluginname', 'local_job');
        $VISANG->theme->menu = '';
        $VISANG->theme->header();
    }else{
        echo $OUTPUT->header(); 
    }
?>  

<script type="text/javascript">
    $(function () {
        //$(".pop01").click(function () {
        <?php if(!empty($user)){?>
        utils.popup.call_layerpop("./popup.php", {"width": "550px", "height": "auto", "callbackFn": function () {}}, {"id": <?php echo $id?>});
        return false;
        <?php }else{?>
            temp_login('<?php echo $id?>');
        <?php }?>
        //});
    });
    function agree(id){
        $.ajax({
            type: 'POST',
            url: '/local/information/ajax.php',
            dataType: 'JSON',
            data: {action: 'e_agree', data:{id:id} },
            success: function(result) {
                if(result.status == 'sucess'){
                //rvalue = result;
                    //temp_login(id);
                    if(result.site == 'e'){
                        location.href = '<?php echo $CFG->job_url?>/local/job/';
                    }else{
                        location.href = '<?php echo $CFG->wwwroot?>';
                    }
                    utils.popup.close_pop($('.layerpop'));
                }
            },
            error: function(xhr, status, error) {
                
            }
        });
    }
    /*
    function temp_login(id){
         $.ajax({
            type: 'POST',
            url: '/local/information/ajax.php',
            dataType: 'JSON',
            data: {action: 'tmp_login', data:{id:id} },
            success: function(result) {
                if(result.status == 'sucess'){
                    location.href = '<?php echo $CFG->job_url?>/local/job/';
                }
            },
            error: function(xhr, status, error) {
            }
        });
    }
    */
</script>

<?php
    //}

}
if($user->joinstatus == "e"){
?>
<div class="cont"></div>
<?php
    $VISANG->theme->footer();
}else{
    echo $OUTPUT->footer();
}
?>


