<?php

define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('lib.php');

$action = optional_param('action','',PARAM_RAW);

switch ($action) {
    case 'e_agree':
        $data = required_param_array('data', PARAM_RAW);
        $user = $DB->get_record('lmsdata_user', array('userid'=>$data['id']));
        $query = "update {lmsdata_user} set statuscode = 1,agreetime = ".time()." where userid = :userid";
        $DB->execute($query, array('userid'=>$data['id']));
        $rvalue = new stdClass();
        $rvalue ->status = 'sucess';
        $rvalue ->site = $user->joinstatus;
        echo json_encode($rvalue);
        die();
        break;
    case 'tmp_login':
        $data = required_param_array('data', PARAM_RAW);
        $user = $DB->get_record('user',array('id'=>$data['id']));
        $date = date('Y-m-d H:i:s', time());
        //$data = new stdClass();
        $userid= $user->username;
//        $data->msg = 'loginresult';
//        $data->userid = $userid;
//        $data->logintime = $date;
//        $data->result = 0;

        if ($user = local_lmsdata_user_login($userid)) {
            // Let's get them all set up.
            $asd = complete_user_login($user);

            // sets the username cookie
            if (!empty($CFG->nolastloggedin)) {
                // do not store last logged in user in cookie
                // auth plugins can temporarily override this from loginpage_hook()
                // do not save $CFG->nolastloggedin in database!
            } else if (empty($CFG->rememberusername) or ( $CFG->rememberusername == 2 and empty($frm->rememberusername))) {
                // no permanent cookies, delete old one if exists
                set_moodle_cookie('');
            } else {
                set_moodle_cookie($USER->username);
            }
            $urltogo = $SESSION->wantsurl;

        /// check if user password has expired
        /// Currently supported only for ldap-authentication module
            $userauth = get_auth_plugin($USER->auth);
            if (!empty($userauth->config->expiration) and $userauth->config->expiration == 1) {
                if ($userauth->can_change_password()) {
                    $passwordchangeurl = $userauth->change_password_url();
                    if (!$passwordchangeurl) {
                        $passwordchangeurl = $CFG->httpswwwroot . '/login/change_password.php';
                    }
                } else {
                    $passwordchangeurl = $CFG->httpswwwroot . '/login/change_password.php';
                }
                $days2expire = $userauth->password_expire($USER->username);
                $PAGE->set_title("$site->fullname: $loginsite");
                $PAGE->set_heading("$site->fullname");
                if (intval($days2expire) > 0 && intval($days2expire) < intval($userauth->config->expiration_warning)) {
                    echo $OUTPUT->header();
                    echo $OUTPUT->confirm(get_string('auth_passwordwillexpire', 'auth', $days2expire), $passwordchangeurl, $urltogo);
                    echo $OUTPUT->footer();
                    exit;
                } elseif (intval($days2expire) < 0) {
                    echo $OUTPUT->header();
                    echo $OUTPUT->confirm(get_string('auth_passwordisexpired', 'auth'), $passwordchangeurl, $urltogo);
                    echo $OUTPUT->footer();
                    exit;
                }
            }

            // Discard any errors before the last redirect.
            unset($SESSION->loginerrormsg);

            // test the session actually works by redirecting to self
            $SESSION->wantsurl = $urltogo;

            //$data->result = 1;
            $USER->certification_check = 'Y';
        }
        $rvalue = new stdClass();
        $rvalue ->status = 'sucess';
        echo json_encode($rvalue);
        die();
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();