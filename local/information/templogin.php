<?php
header('Access-Control-Allow-Origin: *');  
header('Expires: -1');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once('lib.php');
//require_once $CFG->dirroot . '/local/lmsdata/lib.php';

$date = date('Y-m-d H:i:s', time());
$data = new stdClass();
$userid= 'admin';
$data->msg = 'loginresult';
$data->userid = $userid;
$data->logintime = $date;
$data->result = 0;

if ($user = local_lmsdata_user_login($userid)) {
    // Let's get them all set up.
    $asd = complete_user_login($user);

    // sets the username cookie
    if (!empty($CFG->nolastloggedin)) {
        // do not store last logged in user in cookie
        // auth plugins can temporarily override this from loginpage_hook()
        // do not save $CFG->nolastloggedin in database!
    } else if (empty($CFG->rememberusername) or ( $CFG->rememberusername == 2 and empty($frm->rememberusername))) {
        // no permanent cookies, delete old one if exists
        set_moodle_cookie('');
    } else {
        set_moodle_cookie($USER->username);
    }
    $urltogo = $SESSION->wantsurl;

/// check if user password has expired
/// Currently supported only for ldap-authentication module
    $userauth = get_auth_plugin($USER->auth);
    if (!empty($userauth->config->expiration) and $userauth->config->expiration == 1) {
        if ($userauth->can_change_password()) {
            $passwordchangeurl = $userauth->change_password_url();
            if (!$passwordchangeurl) {
                $passwordchangeurl = $CFG->httpswwwroot . '/login/change_password.php';
            }
        } else {
            $passwordchangeurl = $CFG->httpswwwroot . '/login/change_password.php';
        }
        $days2expire = $userauth->password_expire($USER->username);
        $PAGE->set_title("$site->fullname: $loginsite");
        $PAGE->set_heading("$site->fullname");
        if (intval($days2expire) > 0 && intval($days2expire) < intval($userauth->config->expiration_warning)) {
            echo $OUTPUT->header();
            echo $OUTPUT->confirm(get_string('auth_passwordwillexpire', 'auth', $days2expire), $passwordchangeurl, $urltogo);
            echo $OUTPUT->footer();
            exit;
        } elseif (intval($days2expire) < 0) {
            echo $OUTPUT->header();
            echo $OUTPUT->confirm(get_string('auth_passwordisexpired', 'auth'), $passwordchangeurl, $urltogo);
            echo $OUTPUT->footer();
            exit;
        }
    }

    // Discard any errors before the last redirect.
    unset($SESSION->loginerrormsg);

    // test the session actually works by redirecting to self
    $SESSION->wantsurl = $urltogo;

    $data->result = 1;
    $USER->certification_check = 'Y';
}

redirect($CFG->wwwroot);
die();
?>