<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017021200;
$plugin->requires  = 2012061700;       
$plugin->component = 'local_ipblock'; // Full name of the plugin (used for diagnostics)