<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');

$id = optional_param('id', 0, PARAM_INT);  // Course ID
$search = trim(optional_param('search', '', PARAM_CLEAN));  // Course ID
$selected_users = optional_param_array('selected_users',array(),PARAM_INT);
$context = get_context_instance(CONTEXT_COURSE, $id);

$sql = "select u.* from {role_assignments} ra "
        . "join {user} u on u.id = ra.userid "
        . "join {context} c on c.contextlevel = :contextlevel and c.id = ra.contextid "
        . "where ra.contextid = :contextid ";
$where = '';
if($selected_users){
    $where .= ' and u.id not in (';
    $u = '';
    foreach($selected_users as $selected_user => $val){
        $u .= $val.',';
    }
    $where .= rtrim($u,',').' )';
}
$param = array('contextid' => $context->id, 'contextlevel' => CONTEXT_COURSE);
if ($search) {
    $where = ' and  ((u.firstname like :searchtxt1 or u.lastname like :searchtxt2 or u. firstname||lastname like :searchtxt3) or u.username like :searchtxt4 )';
    $param['searchtxt1'] = $param['searchtxt2'] = $param['searchtxt3'] = $param['searchtxt4'] = '%' . $search . '%';
}

$users = $DB->get_records_sql($sql . $where . ' order by u.username asc', $param, $offset, $perpage);

$index = 0;
foreach ($users as $user) {
    $index++;
    $allCount = count($users);
    $nUser = $allCount - ($allCount % 3);
    if($allCount % 3 == 0){$nUser = $allCount-3;}
    
    $roles = get_user_roles($context, $user->id);
    $rolename = '';
    foreach ($roles as $role) {
        $rolename .= role_get_name($role) . ',';
    }
    $rolename = rtrim($rolename, ',');
    $disabled = FALSE;
    if(empty(trim($user->phone2))){
        $disabled = TRUE;
    }
    
    //마지막 리스트 클래스 추가
    $lastClass = "";
    if($index > $nUser){$lastClass = "lastrow";}
    if($index == $allCount && $allCount % 3 == 1){$lastClass = "lastrow last1";}
    if($index == $allCount && $allCount % 3 == 2){$lastClass = "lastrow last2";}

    echo '<div class="send_users '.$lastClass.'" id="utd'.$user->id.'">';
    if($disabled){
        echo '<div class="send_checkbox"><input type="checkbox" disabled title="Empty Phone Number" class="emptymail" username="'.fullname($user).'"  value="' . $user->id . '"></div>';
    } else {
        echo '<div class="send_checkbox "><input type="checkbox"  class="usercheck" title="User Select" username="'.fullname($user).'"  value="' . $user->id . '"></div>';
    }
    echo '<div  class="send_upicture">' . $OUTPUT->user_picture($user, array('courseid' => $id)) . '</div>';
    echo '<div  class="send_uname">' . fullname($user) . '('.$user->username.')';
    if($role){
        echo $rolename;
    }
    echo '</div>';
    echo '</div>';
}
if(!$users){
    echo '<div>No Searching User</div>';
}