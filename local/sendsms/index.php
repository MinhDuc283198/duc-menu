<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/sendsms/sendsms_form.php';
require_once($CFG->dirroot . '/local/sendsms/lib.php');
require_once $CFG->dirroot . '/message/lib.php';

$id = optional_param('id', 0, PARAM_INT);  // Course ID
$search = optional_param('search', '', PARAM_CLEAN);  // Course ID

$context = get_context_instance(CONTEXT_COURSE, $id);

require_login();

$PAGE->set_context($context);
$PAGE->set_url('/local/sendsms/index.php?id=' . $id);
$PAGE->set_pagelayout('incourse');


$course = get_course($id);
$PAGE->set_course($course);


if (!has_capability('moodle/course:manageactivities', $context)) {
    return;
}

$strplural = get_string("pluginname", "local_sendsms");
$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$mform = new sendsms_form(null);

if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot . "/course/view.php?id=$id");
} else if ($fromform = $mform->get_data()) {
    $users = optional_param_array('users', '', PARAM_CLEAN);
    if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $fromform->sphone) || preg_match('/^[0-9]{10,11}$/', $fromform->sphone)) {
        $sms = new stdClass();

        $sms->subject       = $fromform->subject;
        $sms->contents      = $fromform->contents;
        $sms->sendtime      = time();
        $sms->sender        = fullname($USER);
        $sms->userid        = $USER->id;
        $sms->callback      = str_replace("-","",$fromform->sphone);
        $sms->timecreated   = time();
        $sms->schedule_type = 0;
        
        $newsms = $DB->insert_record('lmsdata_sms', $sms);
        $msg_no = set_smssend_local($sms);
        
        if(!$msg_no){
            echo 'Set Sms Not Working';
            die();
        }
        
        foreach ($users as $user => $uid) {
            $userto = $DB->get_record('user', array('id' => $uid));
            
            if (preg_match("/^(010|011|016|017|018|019|070)-\d{3,4}-\d{4}$/u", $userto->phone2) || preg_match('/^[0-9]{10,11}$/', $userto->phone2)) {

                $userto->phone2 = str_replace("-","",$userto->phone2);

                $sms_user->sms           = $newsms;
                $sms_user->userid        = $userto->id;
                $sms_user->phone         = $userto->phone2;
                $sms_user->fullname      = fullname($userto);
                $sms_user->timecreated   = time();
                
                send_sms_local($userto, $sms,$msg_no);
                $DB->insert_record('lmsdata_sms_data', $sms_user);
            }
        }
        
        redirect($CFG->wwwroot . "/course/view.php?id=$id");
    }  
} else {

    echo $OUTPUT->header();
    ?>
    <div class="userlist">
        <form id="usersearch_form" class="table-search-option stat_form">
            <div class="stat_search_area">
                <input type="text" title="search" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('searchplaceholder', 'coursereport_statistics'); ?>">
                <input type="button" onclick="get_users($('input[name=search]').val())" value="<?php echo get_string('search'); ?>" class="board-search"/>
            </div>
        </form>
        <div id="userlist_tbody">
            <div  class="searching">Searching...</div>
        </div>
        <div class="table-bottom-btnarea buttons"> 
            <input type="button" id="allcheck" value="<?php echo get_string('all_check', 'local_sendsms') ?>">
            <input type="button" id="alluncheck" value="<?php echo get_string('all_uncheck', 'local_sendsms') ?>">
            <input type="submit" id="adduser" value="<?php echo get_string('apply', 'local_sendsms') ?>">
        </div>
    </div>
    <?php echo $mform->display(); ?>
    <script type="text/javascript">
        var selected_users = new Array();
        function get_users(search) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/sendsms/ajax/userlist.php" ?>',
                method: 'POST',
                data: {
                    id: <?php echo $id; ?>,
                    search: search,
                    selected_users: selected_users
                },
                success: function (data) {
                    $('#userlist_tbody').html(data);
                }
            });
        }
        $('#usersearch_form').submit(function () {
            get_users($('input[name=search]').val());
            return false;
        });
        $('.mform').submit(function () {
            if (confirm('Are you want send SMS?')) {
                return true;
            } else {
                return false;
            }
        });
        window.onload = function () {
            get_users('<?php echo $search; ?>');
        };
        $('#allcheck').click(function () {
            $('.usercheck').each(function () {
                $('.usercheck').prop('checked', true);
            });
        });
        $('#alluncheck').click(function () {
            $('.usercheck').prop('checked', false);
        });
        $('#adduser').click(function () {
            $('.usercheck:checked').each(function () {
                selected_users[$(this).val()] = $(this).val();
                $('#user_selected').hide();
                $('input[name=user]').val(1);
                $('#utd' + $(this).val()).remove();
                $('#selected_users').html($('#selected_users').html() + '<div class="selected_user user' + $(this).val() + '">' + $(this).attr('username') + '<input type="hidden" name="users[]" value="' + $(this).val() + '"><span class="deleteX" onclick="delete_user(' + $(this).val() + ')">X</span></div>');
            });
            get_users($('input[name=search]').val());
        });
        function delete_user(userid) {
            $('.user' + userid).remove();
            delete selected_users[userid];
            get_users($('input[name=search]').val());
            if($('.selected_user').length == 0){
                $('#user_selected').show();
                $('input[name=user]').val('');
            }
        }
    </script>
    <?php
    echo $OUTPUT->footer();
}
?>