<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
global $CFG, $DB;
require_once($CFG->dirroot . '/chamktu/support/smsconfig.php');

function local_sendsms_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB;

    $fileareas = array('attachment');
    if (!in_array($filearea, $fileareas)) {
        return false;
    }


    $fs = get_file_storage();
    $relativepath = implode('/', $args);

    $fullpath = "/$context->id/local_sendsms/$filearea/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }


    // finally send the file
    send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}

function set_smssend_local($sms_data) {
    global $CONN_ODBC;
    $select = "select NVL(MAX(TO_NUMBER(IF_MSG_NO)), 0) + 1 AS MSG_NO from EP_PNS.PNS_IF_MSG;";

    $exec = odbc_exec($CONN_ODBC, $select);


    $row = odbc_fetch_array($exec);

    $now = date('Ymdhis', time());

        $query = "INSERT INTO EP_PNS.PNS_IF_MSG (
                                    IF_MSG_NO
                                   ,IF_MSG_TIT
                                   ,IF_MSG_CTNT
                                   ,IF_MSG_SEND_DTTM
                                   ,IF_SYS_GBN
                                   ,IF_SYS_GBN_NM
                                   ,IF_MSG_PROC_YN
                                   ,IF_MSG_SEND_ID
                                   ) VALUES ( 
                                       '" . $row['MSG_NO'] . "',"
            . "'" . iconv('UTF-8','EUC-KR',$sms_data->subject) . "',"
            . "'" . iconv('UTF-8','EUC-KR',$sms_data->contents) . "',"
            . "'" . $now . "',"
            . "'227692',"
            . "'".iconv('UTF-8','EUC-KR','미래융합대학교')."',"
            . "'N',"
            . "'06931'"
            . ")";

    if ($result = odbc_exec($CONN_ODBC, $query) == TRUE) {
        echo 'Run';
    } else {
        echo $query;
        print_object(odbc_error($CONN_ODBC));
    }

    return $row['MSG_NO'];
}

function send_sms_local($user, $sms_data, $msg_no) {
    global $CONN_ODBC;
    if ($user->username == 'admin') {
        $username = 'lms_admin';
    } else {
        $username = $user->username;
    }

    $now = date('Ymdhis', time());
    $send_date = date('Ymdhis', $send_date);

    $userinfo = $user->firstname . $user->lastname . '^' . $user->phone2;


    if (trim($user->phone2)) {
        $query = "INSERT INTO EP_PNS.PNS_IF_MSG_USER (
         IF_MSG_NO
        ,IF_SEND_EMP_NO
        ,IF_SEND_EMP_TEL
        ,IF_RECV_USER_NM
        ,IF_RECV_USER_TEL
        ,IF_SEND_GBN
        ) VALUES ( 
            '" . iconv('UTF-8','EUC-KR',$msg_no) . "',"
            . "'".iconv('UTF-8','EUC-KR',$user->username)."',"
            . "'".iconv('UTF-8','EUC-KR',$sms_data->callback)."',"
            . "'".iconv('UTF-8','EUC-KR',fullname($user))."',"
            . "'".iconv('UTF-8','EUC-KR',$user->phone2)."',"
            . "'N'"
            . ")";

        if ($result = odbc_exec($CONN_ODBC, $query) == TRUE) {
            echo 'run';
        } else {
            echo $query;
            print_object(odbc_error($CONN_ODBC));
        }
    }
}
