<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$string['pluginname'] = 'Gửi tin nhắn SMS';

$string['send'] = 'Gửi tin';
$string['apply'] = 'Gửi';

$string['mailtitle'] = 'Tiêu đề SMS';
$string['matilcontents'] = 'Nội dung SMS';

$string['all_check'] = 'Chọn tất cả';
$string['all_uncheck'] = 'Bỏ chọn tất cả';

$string['user_picture'] = 'Ảnh người dùng';
$string['mobile'] = 'Di động';
 
$string['role'] = 'Quy ước';
$string['student_number'] = 'Số học viên';
$string['affiliation'] = 'Liên kết';
$string['target'] = 'Mục tiêu';


$string['subject'] = 'Môn học';
$string['sender'] = 'Người gửi';
$string['recipients_count'] = 'Số người nhận';
$string['sendtime'] = 'Thời gian gửi';
$string['sendtype'] = 'Hình thức gửi';
$string['reservation'] = 'Gửi theo lịch';
$string['nowsent'] = 'Gửi ngay';
$string['sender_number'] = 'Số điện thoại người gửi';