<?php

$string['pluginname'] = 'Thông báo môn học';
$string['pluginnameplural'] = 'Thông báo môn học';

$string['board'] = 'Thông báo môn học';
$string['no.'] = 'No.';
$string['name'] = 'Tên';
$string['timemodified'] = 'Thời gian chỉnh sửa';

$string['entertitle'] = 'Nhập tiêu đề thông báo';
$string['addedboard'] = 'Thêm thông báo';
$string['couldnotfoundboard'] = 'Không tìm thấy thông báo';
$string['nopermission'] = 'Hạn chế quyền truy cập';


$string['course:notice'] = 'Thông báo';
$string['course:qna'] = 'Hỏi đáp';
$string['course:all'] = 'Tất cả';
$string['course:name'] = 'Tên khóa học';
$string['course:selecttitle'] = 'Chọn khóa học';
$string['course:enter'] = 'Vui lòng kiểm tra khóa học';

$string['content:title'] = 'Tiêu đề';
$string['content:file'] = 'Tập tin';
$string['content:user'] = 'Người dùng';
$string['content:date'] = 'Ngày';
$string['content:hits'] = 'Lượt xem';
$string['content:comment'] = 'Bình luận';
$string['content:recommend'] = 'Đề xuất';
$string['content:empty'] = 'Không có chủ đề thảo luận nào trong thông báo này';
$string['content:perpage'] = '{$a} lượt xem';
$string['content:new'] = 'Bài viết';
$string['content:list'] = 'Danh sách';
$string['content:save'] = 'Lưu';
$string['content:cancel'] = 'Hủy';
$string['content:reply'] = 'Trả lời';
$string['content:re'] = 'Trả lời';
$string['content:delete'] = 'Xóa';
$string['content:edit'] = 'Sửa';
$string['content:enter'] = 'Vui lòng nhập nội dung';
$string['content:private'] = 'Riêng tư';

$string['content:contents'] = 'Nội dung';
$string['content:enddate'] = 'Thời gian kết thúc';
$string['content:attachments'] = 'Đính kèm';
$string['content:managementmode'] = 'Kích hoạt chế độ quản lý';

$string['title:enter'] = 'Vui lòng nhập tiêu đề';

$string['notice:write'] = 'Tạo thông báo';
$string['notice:isnotice'] = 'Ghim thông báo';
$string['notice:list'] = 'Danh sách thông báo khóa học';
$string['notice:explain'] = 'Nhấp chuột vào tên khóa học để xem thông báo';

$string['qna:list'] = 'Danh sách hỏi đáp';
$string['qna:write'] = '{$a} Tạo mục hỏi đáp';

$string['keyword:input'] = 'Nhập từ khóa';

$string['private:alert'] = 'Đây là bài viết riêng tư';

$string['month'] = 'Tháng';

$string['bynameondate'] = 'từ {$a->name} - {$a->date}';

$string['view:cnt'] = 'Lượt xem';
?>
