<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019040503;
$plugin->requires  = 2012061700;    
$plugin->component = 'local_okbooks'; // Full name of the plugin (used for diagnostics) 