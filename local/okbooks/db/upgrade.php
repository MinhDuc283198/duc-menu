<?php

function xmldb_local_okbooks_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    require_once($CFG->libdir . '/eventslib.php');

    $dbman = $DB->get_manager();

    if ($oldversion < 2017061600) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_rebooks');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('author', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('publisher', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('publication_date', XMLDB_TYPE_CHAR, '10', null, null, null, 0);
        $table->add_field('totalpage', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('link1', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('link2', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('link3', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('link4', XMLDB_TYPE_CHAR, '255', null, null, null, 0);
        $table->add_field('isbn', XMLDB_TYPE_CHAR, '200', null, null, null, 0);

        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2019031100) {
        $table = new xmldb_table('lmsdata_rebooks');

        $field = new xmldb_field('recom', XMLDB_TYPE_CHAR, '100', null, null, null, 0);
        // Conditionally add description field to the badge_criteria table.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2019031101) {
        $table = new xmldb_table('lmsdata_rebooks');

        $new_field = new xmldb_field('recom', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        // Conditionally add description field to the badge_criteria table.
        if (!$dbman->field_exists($table, $new_field)) {
            $dbman->change_field_default($table, $new_field);
        }
    }

    if ($oldversion < 2019040500) {

        $table = new xmldb_table('lmsdata_rebooks');
        $field = new xmldb_field('recom', XMLDB_TYPE_CHAR, '200', null, null, null, null);

        // Conditionally launch add field id.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->change_field_default($table, $field);
        }
    }
    if ($oldversion < 2019040501) {

        $table = new xmldb_table('lmsdata_rebooks');
        $new_field = new xmldb_field('recom', XMLDB_TYPE_CHAR, '200', null, null, null, null);

        // Conditionally launch add field id.
        if (!$dbman->field_exists($table, $new_field)) {
            $dbman->change_field_default($table, $new_field);
        }
    }
    if ($oldversion < 2019040503) {

        $table = new xmldb_table('lmsdata_rebooks');
        $field = new xmldb_field('recom');
        $field -> set_attributes( XMLDB_TYPE_CHAR, '200', null, null, null, null);

        try { 
            $dbman->change_field_type($table, $field);
        } catch (moodle_exception $e) {}
    }

    return true;
}
