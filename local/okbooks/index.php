<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
require_once($CFG->dirroot . '/local/jinoboard/lib.php');
require_once($CFG->dirroot . '/chamktu/board/lib.php');
//require_once (dirname(dirname(dirname(__FILE__))) . '/lib/paging.php');
//require_once dirname(dirname(__FILE__)) . '/lib/contents_lib.php';
//$url = '/local/okbooks/index.php';
$page = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$id = optional_param('id', 0, PARAM_INT);
$searchvalue = optional_param('searchvalue', "", PARAM_TEXT);
$rebooks = lmsdata_rebooks_show_main_rebooks($page - 1, $perpage);

$set_title = set_title('/local/okbooks/index.php');
$set_subtitle = set_subtitle('/local/okbooks/index.php');

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add($set_title);
$PAGE->navbar->add($set_subtitle);

//교육생은 해당 페이지에 입장 못하게 처리
$usergroup = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));
if ($usergroup == 'rs') {
    redirect($CFG->wwwroot . '?re=6');
}

echo $OUTPUT->header();
?>  

<div class="textbox">
    <div class="t-pink">Level-up 추천 도서</div>
    <p>전문가로 성장하는 데 도움이 되는 도서를 추천합니다.</p>
</div>


<div class="div_title"><?php echo $set_subtitle ?></div>
<ul class="book_list">
<?php
foreach ($rebooks as $books) {
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'attachment', $books->itemid, 'timemodified', false);
    ?>

        <li>       
        <?php
        $output = '';
        foreach ($files as $file) {
            $filename = $file->get_filename();

            $mimetype = $file->get_mimetype();
            $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $books->itemid . '/' . $filename);
            if ($file->get_filesize() > 0) {
                $fileobj = '<img class=\'small\' src="' . $path . '">';
            }
        }

        if ($books->itemid) {
            ?>
                <img class="bookimg" src="<?php echo $path ?>" alt="<?php echo $books->name ?>"/>
                <?php
            } else {
                ?>
                <img class="bookimg" src="/theme/oklassedu/pix/images/book_default.jpg" alt="<?php echo $books->name ?>"/>

                <?php
            }
            ?>

            <div class="bookinfo">
                <h5><?php echo $books->name ?></h5>
                <p class="writer"><?php echo $books->author ?> | <?php echo $books->publisher ?></p>
                <strong class="price">판매가 : <?php echo number_format($books->price) ?>원</strong>
                <p class="reason"><strong>추천 이유 :</strong> <?php echo $books->recom ?></p>
            </div>
            <div class="book_link">
                <a href="<?php echo $books->link1 ?>" class="btn orange" alt="인터파크 바로가기" target="_blank">인터파크 바로가기</a>
                <a href="<?php echo $books->link2 ?>" class="btn orange" alt="YES24 바로가기" target="_blank">YES24 바로가기</a>
                <a href="<?php echo $books->link3 ?>" class="btn orange" alt="알라딘 바로가기" target="_blank">알라딘 바로가기</a>
                <a href="<?php echo $books->link4 ?>" class="btn orange" alt="교보문고 바로가기" target="_blank">교보문고 바로가기</a>
            </div>
        </li>


    <?php
}
?>
</ul>
<!--
<li>
    <img class="bookimg" src="/theme/oklassedu/pix/images/book02.jpg" alt="성장과 발달을 돕는 초등 평가 혁신"/>
    <div class="bookinfo">
        <h5><a href="#">성장과 발달을 돕는 초등 평가 혁신</a></h5>
        <p class="writer">이형빈 | 맘에드림</p>
        <strong class="price">판매가 : 15,500원</strong>
    </div>
    <div class="book_link">
        <input type="button" class="btn orange" value="인터파크 바로가기" />
        <input type="button" class="btn orange" value="YES24 바로가기" />
        <input type="button" class="btn orange" value="알라딘 바로가기" />
        <input type="button" class="btn orange" value="교보문고 바로가기" />
    </div>
</li>
</ul>
-->

<div class="table-footer-area">
<?php
$totalcount = lmsdata_rebooks_main_rebooks_count();
$total_pages = ceil($totalcount / $perpage);
$page_params = array();
$page_params['perpage'] = $perpage;
if ($total_pages > 1) {
    jinobook_get_paging_bar($CFG->wwwroot . "/local/okbooks/index.php", $page_params, $total_pages, $page);
}
?>
</div>


<?php
echo $OUTPUT->footer();
?>


