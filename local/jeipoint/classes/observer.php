<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used in forum.
 *
 * @package    local_jeipoint
 * @copyright  2013 Rajesh Taneja <rajesh@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once $CFG->dirroot.'/local/jeipoint/lib.php';

/**
 * Event observer for local_courselist.
 */

class local_jeipoint_observer {
    
    /**
     * 강의에 학생 등록 시 신임/연차별 과정이면 jeipoint를 저장함
     * @global type $CFG
     * @global type $DB
     * @param \core\event\role_assigned $event 
     */
    public static function role_assigned(\core\event\role_assigned $event) {
        global $CFG, $DB;
        
        $event_data = $event->get_data();
        $courseid = $event_data['courseid'];
        $userid = $event_data['relateduserid'];

        $courseType = local_jeipoint_check_course($courseid);     
        
        //course의 type이 신임/연차별 과정이며 학생일 경우만 실행함
        if ($courseType && $event_data['objectid'] == 5) {
            // 값이 없을때 insert
            if(!$DB->record_exists('local_jeipoint_user',array('userid'=>$userid, 'courseid'=>$courseid))) {
                $types = array(JEL_COURSE_CATEGORY_TRUST=>0, JEL_COURSE_CATEGORY_YEAR=>1);
                
                $userpoint = new stdclass();
                $userpoint->userid = $userid;
                $userpoint->courseid = $courseid;
                $userpoint->pointtype = $types[$courseType];
                $userpoint->totalpoint = $DB->get_field_sql('SELECT 
                                                                CASE WHEN SUM(POINT) IS NULL THEN 0 ELSE SUM(POINT) END AS totalpoint 
                                                            FROM {okmedia} WHERE course = :courseid', array('courseid'=>$courseid));
                
                $sumpoint = $DB->get_field_sql('SELECT 
                                                    CASE WHEN SUM(POINT) IS NULL THEN 0 ELSE SUM(POINT) END AS sumpoint
                                                FROM {local_jeipoint} 
                                                WHERE userid = :userid AND courseid = :courseid', 
                                                array('userid'=>$userid, 'courseid'=>$courseid)
                                              );
                $userpoint->sumpoint = $sumpoint;
                $userpoint->losttime = time();
                $userpoint->timemodified = time();
                $userpoint->timecreated = time();
                $userpoint->visible = 1;
                $newid = $DB->insert_record('local_jeipoint_user', $userpoint);
            }
        }
    }
    
    /**
     * 강의에 학생 해제 시 신임/연차별 과정이면 jeipoint를 삭제함
     * @global type $CFG
     * @global type $DB
     * @param \core\event\role_unassigned $event
     */
    public static function role_unassigned(\core\event\role_unassigned $event) {
        global $CFG, $DB;
        $event_data = $event->get_data();
        $courseid = $event_data['courseid'];
        $userid = $event_data['relateduserid'];

        $courseType = local_jeipoint_check_course($courseid);
        
        //course의 type이 신임/연차별 과정이며 학생일 경우만 실행함
        if ($courseType && $event_data['objectid'] == 5) {
            // 값이 있을때 visible
            if($userpoint = $DB->get_record('local_jeipoint_user',array('userid'=>$userid, 'courseid'=>$courseid))) {
                $DB->set_field('local_jeipoint_user', 'visible', 0, array('id' => $userpoint->id));
            }
        }
    }
    
    /**
     * 학습활동 이수 시 신임/연차별 과정이면서 이수/미이수 상태 변경에 따라 포인트값을 변경한다.
     * @global type $CFG
     * @global type $DB
     * @param \core\event\course_module_completion_updated $event
     */
    public static function course_module_completion_updated(\core\event\course_module_completion_updated $event) {
        global $CFG, $DB;
        $event_data = $event->get_data();
        $completionId = $event_data['objectid']; 
        $courseid = $event_data['courseid'];
        $userid = $event_data['userid'];
        $cmid = $event_data['contextinstanceid'];

        $courseType = local_jeipoint_check_course($courseid);

        //course의 type이 신임/연차별 과정일때
        if($courseType) {
            $sql = 'SELECT * 
                    FROM {role_assignments} ra
                    JOIN {context} co ON co.id = ra.contextid
                    JOIN {role} ro ON ro.id = ra.roleid AND archetype = :archetype
                    WHERE ra.userid = :userid AND co.instanceid = :courseid ';
            $params = array(
                            'archetype' => 'student',
                            'userid' => $userid,
                            'courseid' => $courseid
                        );
            $isStudent = $DB->record_exists_sql($sql, $params);
            // 학생일때만 처리함
            if($isStudent) {
                $sectionId = $DB->get_field('course_modules', 'section', array('id' => $cmid));
                
                //section 내 모든 활동 완료 여부
                $state = local_jeipoint_completion_state($sectionId, $userid);
                //section 내 포인트를 가지고 있는 okmedia의 cmid
                $pointCmId = local_jeipoint_get_pointCmId($sectionId);
                //포인트 획득 조건 통과
                if($state && !empty($pointCmId)) {
                    local_jeipoint_setPoint($courseid, $pointCmId, $userid);
                }
            }
        }
    }
}
