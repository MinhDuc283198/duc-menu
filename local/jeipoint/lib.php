<?php
require_once($CFG->dirroot . '/local/autoalarm/lib.php');
/** 
 * course_categories 테이블의 idnumber, 신임과정 
 * 신임이 믿는다는 뜻의 신임인줄...
 **/
define('JEL_COURSE_CATEGORY_TRUST', 'trust_course');

/** course_categories 테이블의 idnumber, 연차별과정 */
define('JEL_COURSE_CATEGORY_YEAR', 'year_course');

/**
 * 사용자의 포인트를 저장
 * 최근의 포인트가 양수일 경우 저장하지 않음
 * 음수일경우 저장 - 최근컨텐츠가 변경/삭제되어 포인트가 회수된 이후의 학습이므로
 * 비어있을 경우 저장 - 최초 이수조건을 달성했으므로  
 * @global type $DB
 * @global type $USER
 * @param type $courseid
 * @param type $cmid
 * @param type $userid
 * @param type $obtain
 */
function local_jeipoint_setPoint($courseid, $cmid, $userid = 0) {
    global $DB, $USER;
    
    //userid가 0이면 $USER->id로
    if(empty($userid)) {
        $userid = $USER->id;
    }
    // 해당 활동의 section 정보를 가져옴
    $section = $DB->get_record_sql('SELECT cs.* 
                                    FROM {course_modules} cm
                                    JOIN {course_sections} cs ON cs.id = cm.section
                                    WHERE cm.id = :id ',                                    
                                    array('id' => $cmid));
    
    //해당 section의 사용자 포인트 이력을 가져옴
    $sql = 'SELECT point FROM {local_jeipoint}';
    $params = array(
                    'userid'=>$userid,
                    'courseid'=>$courseid,
                    'section'=>$section->id,
                );
    $where[] = ' userid = :userid ';
    $where[] = ' courseid = :courseid ';
    $where[] = ' section = :section ';
    $where = ' WHERE '.implode(' AND ', $where);
    $orderBy = ' ORDER BY timecreated DESC ';
    $savePoint = $DB->get_field_sql($sql.$where.$orderBy, $params);
                    
    // 최근값이 차감이거나 local_jeipoint에 값이 없을때 insert
    // 획득포인트가 0보다 크면 이미 획득했기 때문에 무시함 
    if(($savePoint < 0) || empty($savePoint)) {
        //해당 과정의 유형(신임/연차별)을 가져옴
        $idnumber = local_jeipoint_check_course($courseid);
        if(JEL_COURSE_CATEGORY_TRUST == $idnumber) {
            $pointtype = 0;
        } else if(JEL_COURSE_CATEGORY_YEAR == $idnumber) {
            $pointtype = 1;
        }
        //과정의 이름을 가져와서 사용자 이력에 넣을 문구를 만듬
        $title = $DB->get_field('course', 'fullname', array('id' => $courseid));
        if(empty($section->name)) {
            $title .= '/'.$section->section.'차시';
        } else {
            $title .= '/'.$section->name;
        }

        $title .= '/'.get_string('completion:msg', 'local_jeipoint');
        
        //현재 활동에 설정된 포인트를 가져옴
        $point = $DB->get_field_sql('SELECT point 
                                     FROM {course_modules} cm
                                     JOIN {okmedia} ok ON ok.id = cm.instance 
                                     WHERE cm.id = :id', 
                                     array('id' => $cmid));
        //포인트 이력을 insert 시킴
        $addPoint = new stdclass();
        $addPoint->userid = $userid;
        $addPoint->courseid = $courseid;
        $addPoint->cmid = $cmid;
        $addPoint->section = $section->id; 
        $addPoint->point = $point;
        $addPoint->pointtype = $pointtype;
        $addPoint->title = $title;
        $addPoint->timecreated = time();
        $DB->insert_record('local_jeipoint', $addPoint);
        
        //사용자의 해당 과정 총 포인트를 증가시킴
        $sumpoint = $DB->get_field('local_jeipoint_user', 'sumpoint', array('courseid'=>$courseid, 'userid'=>$userid));
        $DB->set_field('local_jeipoint_user', 'sumpoint', $sumpoint+$point,  array('courseid'=>$courseid, 'userid'=>$userid));
    }
}

/**
 * okmedia 개선과정 컨텐츠 체크시 포인트 차감하는 함수
 * @global type $DB
 * @param type $okmediaid
 * @param type $type
 */
function local_jeipoint_deduction($okmediaid) {
    global $CFG, $DB;
    //사용자 포인트 이력과 관련 정보 데이터를 가져옴
    $sql = 'SELECT 
                cm.course AS courseid, cm.id AS cmid, cm.section,
                cs.name AS sectionname, cs.section AS sectionnum,
                ok.name AS title, ok.point,
                co.fullname,
                cc.idnumber 
            FROM {okmedia} ok
            JOIN {course_modules} cm ON cm.instance = ok.id
            JOIN {course_sections} cs ON cs.id = cm.section
            JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname
            JOIN {course} co ON co.id = cm.course
            JOIN {course_categories} cc ON cc.id = co.category
            WHERE ok.id = :okmediaid ';
    $params = array(
                    'modname'=>'okmedia',
                    'okmediaid'=>$okmediaid
                );
    $infoData = $DB->get_record_sql($sql, $params);

    //사용자 포인트의 유형을 체크
    if(JEL_COURSE_CATEGORY_TRUST == $infoData->idnumber) {
        $pointtype = 0;
    } else if(JEL_COURSE_CATEGORY_YEAR == $infoData->idnumber) {
        $pointtype = 1;
    }
    //과정의 이름을 가져와서 사용자 이력에 넣을 문구를 만듬 
    $title = $infoData->fullname;
    if(empty($infoData->sectionname)) {
        $title .= '/'.$infoData->sectionnum.'차시';
    } else {
        $title .= '/'.$infoData->sectionname;
    }
    $title .= '/'.get_string('deduction:msg', 'local_jeipoint');
    
    $addPoint = new stdclass();
    $addPoint->courseid = $infoData->courseid;
    $addPoint->section = $infoData->section;
    $addPoint->cmid = $infoData->cmid;
    $addPoint->pointtype = $pointtype;
    $addPoint->title = $title;
    $addPoint->timecreated = time();
    
    //변경할 사용자 목록을 가져옴 
    //해당 과정을 완료했으며
    //최근 포인트가 플러스인 사용자만 가져와서 넣음
    //(마이너스인 경우 이전 차감 후 시청하지 않은 경우)
    $sql = 'SELECT 
                lp.id, lp.userid, lp.point
            FROM {local_jeipoint} lp
            JOIN {course_completions} cc ON cc.userid = lp.userid AND cc.course = lp.courseid AND cc.timecompleted > 0 
            JOIN (
                    SELECT userid, courseid, cmid, MAX(timecreated) AS mtime
                    FROM {local_jeipoint} 
                    WHERE courseid = :courseid AND cmid = :cmid GROUP BY userid
            ) lp1 ON lp1.userid = lp.userid AND lp1.courseid = lp.courseid AND lp1.cmid = lp.cmid AND lp1.mtime = lp.timecreated  AND lp.point > 0';
    $params = array(
                    'courseid'=>$infoData->courseid,
                    'cmid'=>$infoData->cmid,
                );
    $users = $DB->get_records_sql($sql, $params);

    // 활동 이수내역을 변경할 클래스 세팅 
    require_once($CFG->libdir . '/completionlib.php');
    $course = $DB->get_record('course', array('id'=>$infoData->courseid));
    $cm = get_coursemodule_from_instance('okmedia', $okmediaid, $course->id, false, MUST_EXIST);
    $completion = new completion_info($course);
    
    //사용자들의 이수내역 변경 및 포인트 차감
    foreach($users AS $user) {
        //포인트 차감
        $addPoint->point = -$user->point;
        $addPoint->userid = $user->userid;
        $DB->insert_record('local_jeipoint', $addPoint);
    
        //해당 활동이 등록되어있는 과정의 합산 포인트에도 차감함 
        if($userPoint = $DB->get_record('local_jeipoint_user', array('userid'=>$user->userid, 'courseid'=>$addPoint->courseid))) {
            $userPoint->sumpoint = $DB->get_field_sql('SELECT 
                                                            CASE WHEN SUM(POINT) IS NULL THEN 0 ELSE SUM(POINT) END AS sumpoint
                                                       FROM {local_jeipoint} 
                                                       WHERE userid = :userid AND courseid = :courseid', 
                                                       array('userid'=>$user->userid, 'courseid'=>$addPoint->courseid)
                                                      );
            $userPoint->timemodified = time();
            //이번 차감으로 권한을 잃으면 시간 기록
            if($userPoint->totalpoint == ($userPoint->sumpoint + $user->point)) {
                $userPoint->losttime = time();
            }
            
            $DB->update_record('local_jeipoint_user', $userPoint);
        }
        //사용자의 동영상 진도율 삭제
        $DB->delete_records('okmedia_track', array('okmediaid'=>$okmediaid, 'userid'=>$user->userid));
        //사용자의 동영상 시청내역 삭제
        $DB->delete_records('okmedia_playtime', array('okmediaid'=>$okmediaid, 'userid'=>$user->userid));
        //해당 활동의 사용자 이수내역을 비활성화 시킴
        $completion->update_state($cm, COMPLETION_INCOMPLETE, $user->userid);
        //사용자한테 알람 보내기
        right_better_auto_alarm($user, $infoData);
    }
}

/**
 * okmedia 추가 시 점수 변경
 * @global type $DB
 * @param type $okmedia
 */
function local_jeipoint_addTotal($okmedia) {   
    global $DB;    
    // 해당 학습활동이 등록된 과정에서 포인트를 얻은 모든 사용자 가져옴
    $userPoints = $DB->get_records('local_jeipoint_user', array('courseid'=>$okmedia->course));
    foreach($userPoints AS $userPoint) {
        //해당 과정의 획득 가능 포인트를 증가하기 전, 모든 포인트를 획득해 이수조건을 달성했을 경우, 권한 상실 시간 기록
        if($userPoint->totalpoint == $userPoint->sumpoint) {
            $userPoint->losttime = time();
    }
    $userPoint->totalpoint += $okmedia->point ;
    if($userPoint->totalpoint < 0) {
        $userPoint->totalpoint = 0;
    }
    $userPoint->timemodified = time();
    $DB->update_record('local_jeipoint_user', $userPoint);
    }

}

/**
 * okmedia 삭제 시 점수 변경
 * @global type $DB
 * @param type $okmedia
 */
function local_jeipoint_delTotal($okmedia) {
    global $DB;
    // 해당 학습활동이 등록된 과정에서 포인트를 얻은 모든 사용자 가져옴
    $userPoints = $DB->get_records('local_jeipoint_user', array('courseid'=>$okmedia->course));
    //학습활동이 속해 있는 section(주차)의 ID를 가져옴
    $sectionid = $DB->get_field_sql('SELECT 
                                        cm.section
                                FROM {course_modules} cm
                                JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname
                                WHERE cm.instance = :instanceid',
                                array('modname'=>'okmedia', 'instanceid'=>$okmedia->id));
    foreach($userPoints AS $userPoint) {
        //획득 가능 포인트를 차감시킴
        $userPoint->totalpoint -= (int)$okmedia->point ;
        if($userPoint->totalpoint < 0) {
            $userPoint->totalpoint = 0;
        }
        //해당 section(주차)의 모든 활동을 이수했는지 체크 
        $status = local_jeipoint_completion_state($sectionid, $userPoint->userid);
        //해당 section의 모든 활동을 이수했으면 포인트를 획득했으므로 차감
        if($status) {
            $userPoint->sumpoint -= (int)$okmedia->point ;
            if($userPoint->sumpoint < 0) {
                $userPoint->sumpoint = 0;
            }
        }
        $userPoint->timemodified = time();
        $DB->update_record('local_jeipoint_user', $userPoint);
    }
    
}

/**
 * okmedia point 변경 시 전체 점수 변경
 * @global type $DB
 * @param type $okmedia 변경시킬 okmedia
 * @param type $point 기존 포인트와 신규포인트의 차이
 */
function local_jeipoint_setTotal($okmedia, $point) {
    global $DB;
    // 해당 학습활동이 등록된 과정에서 포인트를 얻은 모든 사용자 가져옴 
    $userPoints = $DB->get_records('local_jeipoint_user', array('courseid'=>$okmedia->course));
    //학습활동이 속해 있는 section(주차)의 ID를 가져옴
    $sectionid = $DB->get_field_sql('SELECT 
                                            cm.section
                                    FROM {course_modules} cm
                                    JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname
                                    WHERE cm.instance = :instanceid',
                                    array('modname'=>'okmedia', 'instanceid'=>$okmedia->id));
    
    foreach($userPoints AS $userPoint) {
        //획득 가능 포인트를 변경시킴
        $userPoint->totalpoint += (int)$point;
        if($userPoint->totalpoint < 0) {
            $userPoint->totalpoint = 0;
        }
        //해당 section(주차)의 모든 활동을 이수했는지 체크 
        $status = local_jeipoint_completion_state($sectionid, $userPoint->userid);
        //해당 section의 모든 활동을 이수했으면 포인트를 변경
        if($status) {
            $userPoint->sumpoint += (int)$point;
            if($userPoint->sumpoint < 0) {
                $userPoint->sumpoint = 0;
            }
        }
        $userPoint->timemodified = time();
        $DB->update_record('local_jeipoint_user', $userPoint);
    }
}


/**
 *  사용자의 획득/차감 포인트 내역을 반환함
 * @global type $DB
 * @global type $USER
 * @param type $userid
 * @param type $pointtype
 * @param type $page
 * @param type $perpage
 * @return \stdclass
 */
function local_jeipoint_getUserPoint($userid = 0, $pointtype=0, $page=1, $perpage = 5) {
    global $DB, $USER;
    
    //userid가 0이면 $USER->id로
    if(empty($userid)) {
        $userid = $USER->id;
    }
    //사용자의 신임 or 연차별 포인트 이력을 가져옴
    $offset = ($page-1) * $perpage;
    $sql = ' FROM {local_jeipoint} WHERE userid = :userid AND pointtype = :pointtype ORDER BY timecreated DESC ';
    $params = array(
                    'userid'=>$userid,
                    'pointtype'=>$pointtype,
                );
    $points = $DB->get_records_sql('SELECT * '.$sql, $params, $offset, $perpage);
    //사용자의 신임 or 연차별 포인트 이력 총 갯수를 가져옴
    $count = $DB->count_records_sql('SELECT COUNT(*)'.$sql, $params);
    
    //사용자의 포인트 이력과 총 갯수를 클래스에 저장 후 반환
    $pointClass = new stdclass();
    $pointClass->points = $points;
    $pointClass->count = $count;
    
    return $pointClass;
}

/**
 * 해당 코스가 신임/연차별 과정인지 체크
 * @global type $DB
 * @param type $courseid
 * @return boolean
 */
function local_jeipoint_check_course($courseid) {
    global $DB;
    
    //해당 과정의 카테고리 idnumber를 가져옴
    $idnumber = $DB->get_field_sql('SELECT cc.idnumber 
                                FROM {course_categories} cc
                                JOIN {course} co ON co.category = cc.id
                                WHERE co.id = :courseid', 
                                array('courseid'=>$courseid));
    
    //신임 or 연차별과정일 경우 해당 값을 반환
    $types = array(JEL_COURSE_CATEGORY_TRUST=>0, JEL_COURSE_CATEGORY_YEAR=>1);
    if (array_key_exists($idnumber, $types)) {
        return $idnumber;
    }
    
    //신임 or 연차별과정이 아닐 경우 반환
    return false;
}

/**
 * 특정 사용자가 해당 섹션에 보기 상태인 activity들을 모두 이수했는지 체크하여 true/false 반환
 * @param type $section
 * @param type $userid
 */
function local_jeipoint_completion_state($section, $userid) {
    global $DB;
    
    //해당 section(주차)에 생성된 모든 활동 중, 활성화 되어있고, 
    //자동이수체크로 설정된 활동의 ID와 이수 state를 가져옴    
    $sql = 'SELECT cm.id, cmc.completionstate 
            FROM {course_modules} cm
            LEFT JOIN (
                    SELECT * FROM {course_modules_completion} 
                    WHERE userid = :userid
            ) cmc ON cmc.coursemoduleid = cm.id
            WHERE cm.section = :section AND cm.visible = :visible AND cm.completion = :completion ';
    $params = array(
                    'userid' => $userid,
                    'section' => $section,
                    'visible' => 1,
                    'completion' => 2
                );
    $completions = $DB->get_records_sql($sql, $params);
    
    //모든값이 이수일 경우 true 반환, 하나라도 이수가 아닐 시 false 반환
    $state = true;
    foreach($completions AS $com) {
        if(empty($com->completionstate) || $com->completionstate == 0) {
            $state = false;
        }
    }
    
    return $state;
}

/**
 * 해당 섹션에 포인트를 가지고 있는 okmedia의 cmid를 반환
 * @global type $DB
 * @param type $sectionId
 * @return type
 */
function local_jeipoint_get_pointCmId($sectionId) {
    global $DB;
    
    //해당 section(주차)에 활성화 되어있는 학습활동 중, 포인트를 가지고 있는 동영상 활동의 ID를 가져옴
    $sql = 'SELECT 
                    cm.id 
            FROM {course_modules} cm
            JOIN {okmedia} ok ON ok.id = cm.instance
            JOIN {modules} mo ON mo.id = cm.module 
            WHERE cm.section = :sectionid AND mo.name = :modulename AND cm.visible = :visible ';
    $params = array(
                    'sectionid' => $sectionId,
                    'modulename' => 'okmedia',
                    'visible' => 1
                );
    
    $cmid = $DB->get_field_sql($sql, $params);
    //ID 반환
    return $cmid;
    
}

function local_jeipoint_total_pages($rows, $limit = 10) {
	if ($rows == 0) {
            return 1;
	}

	$total_pages = (int) ($rows / $limit);

	if (($rows % $limit) > 0) {
		$total_pages += 1;
	}

	return $total_pages;
}

function local_jeipoint_paging_bar($url, $params, $total_pages, $current_page, $pagename, $max_nav = 10) {
    $total_nav_pages = local_jeipoint_total_pages($total_pages, $max_nav);
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }
    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = $pagename.'=';
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . '?'.$pagename.'=';
    }
    echo html_writer::start_tag('div', array('class' => 'board-breadcrumbs'));
    if ($current_page > 1) {
        echo '<span class="board-nav-prev"><a class="prev" href="'.$url.($current_page - 1).'"><</a></span>';
    } else {
        echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    echo '<ul>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="current"><a href="#">'.$i.'</a></li>';
        } else {
            echo '<li><a href="'.$url.''.$i.'">'.$i.'</a></li>';
        }
    }
    echo '</ul>';
    if ($current_page < $total_pages) {
        echo '<span class="board-nav-next"><a class="next" href="'.$url.($current_page + 1).'">></a></span>';
    } else {
        echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
    }
    echo html_writer::end_tag('div');
}

/**
 * 평가종료 및 포인트회수 크론
 * @global type $DB
 */
function local_jeipoint_cron() {
    global $DB;
    
    //현재 시간의 년도/달/일 값을 가져옴 - ex)20190505
    $date = date('Ymd', time());
    
    //사용자의 포인트가 활성화 되어있는 과정 중
    //현재 시간 기준 1일전 평가기간이 종료된 과정을 가져오고
    //강의 이수 완료 status를 가져옴
    $sql = 'SELECT 
                lju.id, 
                CASE WHEN cc.timecompleted > 0 THEN 0 ELSE 1 END AS status
            FROM {lmsdata_class} lc 
            JOIN {local_jeipoint_user} lju ON lju.courseid = lc.courseid
            LEFT JOIN {course_completions} cc ON cc.course = lju.courseid AND cc.userid = lju.userid 
            WHERE FROM_UNIXTIME(lc.evaluationend+(60*60*24), "%Y%m%d") = :date AND lju.visible = :visible ';
    $params = array(
                    'date' => $date,
                    'visible' => 1,
                );
    //status 가 1인 경우는 강의 이수 미완료 이므로, 해당 포인트 내역 비활성화 시킴
    $points = $DB->get_records_sql($sql, $params);
    foreach($points AS $pt) {
        if($pt->status == 1) {
            $DB->set_field('local_jeipoint_user', 'visible', 0, array('id' => $pt->id));
        }
    }
     
}

/**
 * 평가종료 날짜를 증가 시켰을때 이미 포인트가 회수 된 내역을 되돌리는 함수
 * @global type $DB
 * @param type $courseid
 */
function local_jeipoint_restore($courseid) {
    global $DB;
        
    $sql = 'SELECT 
                lju.id
            FROM {lmsdata_class} lc 
            JOIN {local_jeipoint_user} lju ON lju.courseid = lc.courseid 
            WHERE lc.courseid = :courseid AND lju.visible = :visible ';
    $params = array(
                    'courseid' => $courseid,
                    'visible' => 0,
                );
    //status 가 1인 경우는 강의 이수 미완료 이므로, 해당 포인트 내역 비활성화 시킴
    $points = $DB->get_records_sql($sql, $params);
    foreach($points AS $pt) {
        $DB->set_field('local_jeipoint_user', 'visible', 1, array('id' => $pt->id));
    }
    
}
