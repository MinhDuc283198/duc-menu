<?php

defined('MOODLE_INTERNAL') || die();

// List of observers.
$observers = array(
    array(
        'eventname' => '\core\event\role_assigned',
        'callback'  => 'local_jeipoint_observer::role_assigned',
    ),
    array(
        'eventname' => '\core\event\role_unassigned',
        'callback'  => 'local_jeipoint_observer::role_unassigned',
    ),
    array(
        'eventname' => '\core\event\course_module_completion_updated',
        'callback'  => 'local_jeipoint_observer::course_module_completion_updated',
    ), 
 );

