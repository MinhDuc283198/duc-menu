<?php

function xmldb_local_jeipoint_upgrade($oldversion) {
    global $DB;
    
    $dbman = $DB->get_manager();

    if ($oldversion < 2019051203) {
        $table = new xmldb_table('local_jeipoint');
        $field = new xmldb_field('section', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $table = new xmldb_table('local_jeipoint_user');
        $field = new xmldb_field('visible', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('yeartype', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    return true;
}
