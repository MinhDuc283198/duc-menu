<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/evaluation/lib.php';
require_once $CFG->libdir . '/formslib.php';
require_once($CFG->dirroot . '/chamktu/lib.php');

$courseid = optional_param('courseid', 0, PARAM_INT);
//$type = optional_param('type', 0, PARAM_INT);
$type = optional_param('type', 2, PARAM_INT);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$year = optional_param('year', '', PARAM_RAW);
$search = optional_param('search', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);

$context = context_system::instance();

require_login();

$PAGE->set_context($context);

$PAGE->set_url('/local/evaluation/index.php', array('type' => $type));
$PAGE->set_pagelayout('edu');
$PAGE->add_body_class('path-local-course_application');

$PAGE->navbar->add(get_string('courseguide', 'local_course_application'));

$PAGE->requires->js('/lib/jquery/jquery-1.12.1.min.js');
$PAGE->requires->js('/lib/jquery/jquery-1.12.1.js');
$PAGE->requires->js('/theme/oklassedu/javascript/theme.js');

echo $OUTPUT->header();

$lcon = "";

$lcon = ($year != '') ? ' and lc.year = :year' : '';

$usertypecode = $DB->get_field('lmsdata_user', 'usertypecode', array('userid' => $USER->id));

$usersql = '';

//if ($type == 1) {
//    $usersql = 'select le.id, le.type, lca.status, u.username 
//                            from {lmsdata_evaluation} le 
//                            join {course} co on le.course = co.id 
//                            join {lmsdata_class} lc on lc.courseid = co.id 
//                            join {lmsdata_course_applications} lca on lca.courseid = lc.courseid 
//                            join {user} u on lca.userid = u.id 
//                            where le.type = 1 and u.id = ' . $USER->id;
//    echo '<div class="tab_header margin_bt30 tab_event">
//                <span class="m-col2"><a href="' . $CFG->wwwroot . '/local/mypage/mycourse.php' . '">나의 수강현황</a></span>
//                <span class="m-col2" ><a href="' . $CFG->wwwroot . '/local/mypage/mypoint.php' . '">나의 학습포인트 현황</a></span>
//                <span class="on m-col2" ><a href="' . $CFG->wwwroot . '/local/evaluation/index.php?type=1' . '">강의평가</a></span>
//            </div>';
//} else {
//    $usersql = 'select id, type, targets '
//            . 'from {lmsdata_evaluation} '
//            . 'where type = 2 and targets like ' . '"%' . $usertypecode . '%"';
//}
//
//$usercontent = $DB->get_records_sql($usersql);
?>
<!--<input type="text" id="input1" style="position: absolute; top:500px;left: 500px;" >-->
<!--<script type="text/javascript" src="../lib/jquery/jquery-1.12.1.min.js"></script>
<script type="text/javascript" src="../lib/jquery/jquery-1.12.1.js"></script>
<script src="/theme/oklassedu/javascript/theme.js" type="text/javascript"></script>-->
<div class="div_title">설문조사</div>
<table class="generaltable" summary="<?php
if ($year != 0) {
    echo $year . get_string('stats_years', 'local_lmsdata');
} else {
    echo '';
}
echo get_string('survey_list', 'local_lmsdata')
?>">
    <caption class="hidden-caption"><?php echo get_string('survey', 'local_lmsdata') ?></caption>
    <thead>
        <tr>
            <th scope="row" width="5%"><?php echo get_string('num', 'local_evaluation') ?></th>
            <th scope="row" width="/" class="title"><?php echo get_string('surveynm', 'local_evaluation') ?></th>
            <th scope="row" with="20%"><?php echo get_string('surveyperiod', 'local_evaluation') ?></th>
            <th scope="row" width="10%"><?php echo get_string('participation', 'local_evaluation') ?></th>
        </tr>   
    </thead>
    <tbody>
        <?php
        $offset = ($currpage - 1) * $perpage;
        $sql_like = "";
        if ($type == 2 && !is_siteadmin()) {
            $sql_like .= 'and e.targets like ' . '"%' . $usertypecode . '%"';
        }
        if (!empty($searchtext)) {
            $sql_like .= 'and f.title like :searchtxt ';
        }
        $params = array('type' => $type, 'searchtxt' => "%" . $searchtext . "%", 'time1' => time(), 'time2' => time());
        $cnt2 = 0;
        $sql = 'select e.*,f.title 
                            from {lmsdata_evaluation} e 
                            join {lmsdata_evaluation_forms} f on f.id = e.formid 
                            where e.timestart <= :time1 and e.timeend > :time2 and e.type = :type ' . $sql_like;
        $orderby = ' ORDER BY e.timecreated DESC ';
        $evaluations = $DB->get_records_sql($sql . $orderby, $params, $offset, $perpage);
        $evaluations_cnt = $DB->count_records_sql('select count(*) from {lmsdata_evaluation} e where e.timestart <= :time1 and e.timeend > :time2  and e.type = :type ' . $sql_like, $params);
        if ($evaluations) {
            foreach ($evaluations as $evaluation) {
                ?>
                <tr>
                    <td scope="col"><?php echo $evaluations_cnt--; ?></td>
                    <td scope="col"><?php echo $evaluation->title; ?></td>
                    <td scope="col"><?php echo date("Y-m-d", $evaluation->timestart) . " ~ " . date("Y-m-d", $evaluation->timeend); ?></td>
                    <?php
                    $answers = $DB->get_records('lmsdata_evaluation_submits', array('evaluation' => $evaluation->id, 'userid' => $USER->id, 'completion' => 1));
                    if ($evaluation->timestart < time() && $evaluation->timeend > time()) {
                        if (!($answers)) { // /local/evaluation/course_evaluation.php?id='.$id.'&evaluation='.$evaluation.'&profid='.$profid 
                            ?>
                            <td><input type="button" class="btn pink" onclick="location.href = '<?php echo $CFG->wwwroot . "/local/evaluation/survey.php?id=" . $evaluation->id; ?>'" value="<?php echo get_string('participation', 'local_evaluation') ?>"></td>
                        <?php } else { ?>
                            <td><input type="button"  disabled="disabled" class="gray_btn_small"  value="참여완료"></td>
                            <?php
                        }
                    } else {
                        ?>
                        <td><input type="button"  disabled="disabled" class="gray_btn_small"  value="기간만료"></td>
                    <?php }
                    ?>
                </tr>
                <?php
                $cnt2++;
            }
            if ($cnt2 <= 0) {
                ?>
                <tr>
                    <td scope="col" colspan="4"><?php echo get_string('investigation', 'local_evaluation') ?></td>
                </tr>
            <?php } ?>
            <?php
        } else {
            echo '<tr>';
            echo '<td colspan="4">';
            if ($type == 1) {
                echo '작성할 강의평가가 없습니다.';
            } else if ($type == 2) {
                echo '작성할 설문조사가 없습니다.';
            }
            echo '</td>';
            echo '</tr>';
        }
        ?>
    </tbody>
</table>
<div class="table-footer-area">
</div>
<?php
echo $OUTPUT->footer();
?>
