<?php

$string['pluginname'] = 'Khảo sát';
$string['pluginnameplural'] = 'Khảo sát';

$string['course_evaluation'] = "Đánh giá khóa học";
$string['survey'] = "Khảo sát";

$string['average'] = 'Trung bìn';
$string['total_answer'] = 'Tổng câu trả lời';
$string['question'] = 'Câu hỏi';
$string['average_point'] = 'Điểm trung bình';
$string['standarddeviation'] = 'Độ lệch chuẩn';
$string['etcanswer'] = 'Vvv...';
$string['back'] = 'Trở lại';
$string['graph'] = 'Đồ thị';
$string['excell_print'] = 'In tập tin Excel';
$string['name'] = 'Tên';

$string['num'] = 'Số';
$string['evaluationnm'] = 'Tên đánh giá';
$string['lecturenm'] = 'Tên bài giảng';
$string['evaluationperiod'] = 'Thời gian thẩm định';
$string['participation'] = 'Tham gia';
$string['viewresults'] = 'Xem kết quả';
$string['Explanation'] = 'Không có đánh giá cho khóa học đăng ký';
$string['investigation'] = 'Không có đánh giá';
$string['surveynm'] = 'Tên khảo sát';
$string['surveyperiod'] = 'Thời gian khảo sát';
$string['participation'] = 'Tham gia';

$string['totalresponses'] = 'Tổng số phản hồi';

$string['required_fil'] ='Vui lòng điền vào mẫu';

$string['force'] = 'Khảo sát hiệu quả';