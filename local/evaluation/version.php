<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019051201;
$plugin->requires  = 2012061700;       
$plugin->component = 'local_evaluation'; // Full name of the plugin (used for diagnostics)