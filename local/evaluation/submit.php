<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/evaluation/lib.php';

$id = optional_param('courseid', 0, PARAM_INT); // corurse->id
$temp = optional_param('temp', 1, PARAM_INT); // 1 : 임시저장,  0: 저장
$formid = optional_param('formid', 0, PARAM_INT); // lmsdata_evaluation_form->id
$type = optional_param('type', 0, PARAM_INT); // 1 : 강의평가, 2: 설문
$evaluationid = optional_param('evaluation', 0, PARAM_INT); // lmsdata_evaluation->id

if ($type == 1) {
    $staranswer = optional_param('staranswer', 0, PARAM_INT);

    $evalcourse = $DB->get_record('lmsdata_class', array('courseid' => $id));

    $evaluationstar = new stdClass();

    $evalsubmit = $DB->get_record('lmsdata_contents_evaluation', array('userid' => $USER->id, 'courseid' => $id));

    if ($evalsubmit->starcomplete != 1 && !empty($staranswer)) {
        if (!$evalsubmit) {
            $evaluationstar->timecreated = time();
        } else {
            $evaluationstar = $evalsubmit;
            $evaluationstar->timemodified = time();
        }
        $evaluationstar->courseid = $id;
        $evaluationstar->stars = $staranswer;
        $evaluationstar->userid = $USER->id;

        if ($temp) {
            $evaluationstar->starcomplete = 0;
        } else {
            $evaluationstar->starcomplete = 1;
        }

        if (time() <= $evalcourse->evaluationend && time() >= $evalcourse->evaluationstart) {
            if ($evalsubmit) {
                $DB->update_record('lmsdata_contents_evaluation', $evaluationstar);
            } else {
                $DB->insert_record('lmsdata_contents_evaluation', $evaluationstar);
            }
        }
    }
} else {
    $evaluation = $DB->get_record('lmsdata_evaluation', array('formid' => $formid, 'course' => $id, 'id' => $evaluationid));

    $submit_history = $DB->get_record('lmsdata_evaluation_submits', array('evaluation' => $evaluation->id, 'userid' => $USER->id));


    $submit = new stdClass();
    $submit->evaluation = $evaluation->id;
    $submit->questions = serialize($_POST);
    $questions = unserialize($submit->questions);

// 0 : 임시저장, 1 : 저장
    $submit->completion = ($temp == 1) ? 0 : 1;

    $submit->userid = $USER->id;
    $submit->timemodified = time();
    if (empty($submit_history)) {
        $submit->timecreated = time();
        $DB->insert_record('lmsdata_evaluation_submits', $submit);
        $history = new stdClass();
        $history->evaluation = $evaluation->id;
        $history->userid = $USER->id;
        $history->timecreated = time();
        $history->timemodified = time();
        $DB->insert_record('lmsdata_evaluation_history', $history);
    } else {
        $submit->id = $submit_history->id;
        $DB->update_record('lmsdata_evaluation_submits', $submit);
        $DB->delete_records('lmsdata_evaluation_answers', array('evaluation' => $evaluation->id, 'userid' => $USER->id, 'course' => $id));
    }
    unset($_POST['temp']);

    $data = new stdClass();
    $data->course = 0;
    $data->evaluation = $evaluation->id;
    $data->userid = $USER->id;
    $data->timecreated = time();
    $data->timemodified = time();

    foreach ($questions as $key => $val) {
        $answer = "";
        $dataquestionnum;
        if (is_array($val)) {
            foreach ($val as $va => $v) {
                if ($va == 'id') {
                    $dataquestionnum = $v;
                } else {
                    if ($v == ETCVALUE) {
                        continue;
                    }
                    if (preg_match('/question_etc/', $key)) {
                        $data->etcanswers = $v;
                        $data->answers = null;
                    } else {
                        $answer .= $v . ",";
                        $data->etcanswers = null;
                    }
                }
                $data->answers = rtrim($answer, ',');
            }
            $data->question = $dataquestionnum;

            $getqtype = $DB->get_record('lmsdata_evaluation_questions', array('id' => $data->question));
            if ($getqtype->qtype != 3) {
                if ((!empty($data->answers) || !empty($data->etcanswers)) && !empty($data->question)) {
                    $DB->insert_record('lmsdata_evaluation_answers', $data);
                }
            } else {
                if (!empty($data->question)) {
                    $DB->insert_record('lmsdata_evaluation_answers', $data);
                }
            }
        } else {// !(is_array($val))
            if ($val == ETCVALUE) {
                continue;
            }
            if (preg_match('/question_etc/', $key)) {
                $data->etcanswers = $val;
                $data->answers = null;
            } else {
                $data->answers = $questions[$key];
                $data->etcanswers = null;
            }

            $getqtype = $DB->get_record('lmsdata_evaluation_questions', array('id' => $data->question));
            if (($getqtype->qtype != 3 && (!empty($data->answers) || !empty($data->etcanswers))) && !empty($data->question)) {
                $DB->insert_record('lmsdata_evaluation_answers', $data);
            }
        }
    }
}

if ($type == 1) {
    redirect('../mypage/mycourse.php');
} else {
    redirect('index.php?type=2');
}    