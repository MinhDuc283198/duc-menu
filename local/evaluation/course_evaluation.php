<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/evaluation/lib.php';

$PAGE->requires->jquery();
$PAGE->requires->js('/local/evaluation/jquery.validation.min.js');
$PAGE->requires->js('/local/evaluation/raty-master/lib/jquery.raty.js');

$courseid = optional_param('courseid', 0, PARAM_INT); // courseid 
//$context = context_system::instance();

require_login();

//$PAGE->set_context($context);
//$PAGE->set_url('/local/evaluation/course_evaluation.php?courseid=' . $id);

$course = $DB->get_record('course', array('id' => $courseid));
$course_lmsdata = $DB->get_record('lmsdata_class', array('courseid' => $courseid));
$course_evaluation = $DB->get_record('lmsdata_contents_evaluation', array('courseid' => $courseid));
?>
<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<script type="text/javascript" src="/lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js"></script>
<script src="/theme/oklassedu/javascript/theme.js" type="text/javascript"></script>
<div class="popwrap">
    <div class="pop_title">
        강의평가
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_pop_close.png" alt="" />
    </div>


    <div class="pop_content">
        <div class="videoinfo inline">
            <p class="circletext">
                <span>미완료</span>
            </p>
            <!--  학습 완료 -->
    <!--            <p class="circletext inverse">
                <span>완료</span>
            </p>    -->
            <!--  학습 완료 -->
            <ul class="info">
                <li>
                    <p class="text-bold"><?php echo $course->fullname; ?>  <span class="square t-pink">?</span></p>
                    <p><?php echo $course_lmsdata->classyear; ?>년도 <?php echo $course_lmsdata->classnum; ?>차</p>
                    <div class="ex_pop">
                        강의평가를 하지 않으면 재학습을 하실 수 없습니다.
                    </div>
                </li>
            </ul>
            <!--<form method="post" action="<?php // echo $CFG->wwwroot . '/local/evaluation/submit.php'      ?>" onsubmit="return fonsubmit();">-->
            <form method="post" action="<?php echo $CFG->wwwroot . '/local/evaluation/submit.php' ?>">
                <input type="hidden" name="temp" />
                <input type="hidden" name="type" value="1" />
                <input type="hidden" name="courseid" value="<?php echo $courseid; ?>">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="text-left">강의의 만족도를 별표로 표현해주세요.<span class="red">*</span></td>
                        </tr>
                        <tr>
                            <td class="text-left">
                                <label class="inline">
                                    <input type="hidden" name="staranswer" id="starRating" value="<?php echo empty($course_evaluation->stars) ? '' : $course_evaluation->stars; ?>"/>
                                    <div id="score-callback" data-score="<?php echo empty($course_evaluation->stars) ? '' : $course_evaluation->stars; ?>"></div>
                                    <div id="displayStarRating"></div>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="table_btn">
                    <input type="submit" id="imsisave"  value="임시저장" class="btn orange big" />
                    <input type="submit" id="savesave" value="제출하기" class="btn orange big" />
                </div>
            </form>        
        </div>

    </div>
</div>

<script src="raty-master/lib/jquery.raty.js"></script>
<script>
    $(document).ready(function () {
        $('form').validate();

        $(function () {
            $('div#score-callback').raty({
                path: "raty-master/lib/images"
                , score: '<?php echo $course_evaluation->stars; ?>'
                , width: 200
                , click: function (score, evt) {
                    $("input[name='staranswer']").attr('value', score);
                }
            });
        });

        $("#imsisave").click(function () {
            if (fonsubmit()) {
                $("input[name='temp']").attr('value', 1);
                return true;
            } else {
                $("input[name=temp]").attr('value', 1);
                return false;
            }
        })
        $("#savesave").click(function () {
            console.log('savesave');
            if (fonsubmit()) {
                $("input[name='temp']").attr('value', 0);
                return true;
            } else {
                $("input[name='temp']").attr('value', 0);
                return false;
            }
        })

    });

    function fonsubmit() {
        var answers = '<?php $course_evaluation->stars; ?>';
        var inputanswers = $('input[name="staranswer"]').val();

        if (inputanswers == 0 || inputanswers == null || inputanswers == undefined || inputanswers == '') {
            if (answers == 0 || answers == null || answers == undefined || answers == '') {
                alert('강의평가를 하셔야 재수강을 할 수 있습니다.');
                $('#score-callback').focus();
                return false;
            }
        }
        return true;
    }

</script>