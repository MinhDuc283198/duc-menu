<?php

function xmldb_local_evaluation_install() {
    global $DB, $USER;
    $maketime = time();
     
    // lmsdata_evaluation_forms
    $recordsf = array(
        array_combine(array('type', 'title', 'contents', 'userid', 'allow_category', 'timecreated', 'timemodified'),
                array(1, '강의평가', '', $USER->id, 2, $maketime, $maketime)),
    );
    foreach ($recordsf as $record) {
        $DB->insert_record('lmsdata_evaluation_forms', $record);
    }
    // lmsdata_evaluation_questions
    $recordsq = array(
        array_combine(array('formid', 'category', 'qtype', 'expression', 'required', 'title', 'contents', 'answers', 'etc', 'etcname', 'sortorder'),
                array(1, 0, 7, 1, 1, '강의의 만족도를 별표로 표현해주세요.', '', 12345, 0, '', 1)),
    );
    foreach ($recordsq as $record) {
        $DB->insert_record('lmsdata_evaluation_questions', $record);
    }
    
}
