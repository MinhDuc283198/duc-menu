<?php

/**
 * The class \local\findschool\findschool is defined here.
 *
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_findschool;

defined('MOODLE_INTERNAL') || die();

class api {
    
    /**
     * 교육부 커리어넷에서 발급받은 api 키 
     * 
     * @var type string
     */
    protected $apikey;

    /**
     * 학교 목록을 요청하는 json url
     * 
     * @var type string
     */
    protected $jsonurl;
    
    /**
     * 학교 분류 값
     * elem_list    -   초등학교
     * midd_list    -   중학교
     * high_list    -   고등학교
     * univ_list    -   대학교
     * seet_list    -   특수/각종기타학교
     * alte_list    -   대안학교
     * 
     * @var type array
     */
    protected $gubun = array(
                        'elem_list',
                        'midd_list',
                        'high_list',
                        'univ_list',
                        'seet_list',
                        'alte_list',
                    );

    public function __construct() {
        $apikey = get_config('local_findschool', 'apikey');
        if(!empty($apikey)) {
            $this->apikey = $apikey;
        }
        
        $this->jsonurl = 'http://www.career.go.kr/cnet/openapi/getOpenApi.json';
    }
    
    public function get_school_list($gubun, $searchSchulNm = '', $thisPage = 1, $perPage = 10) {
        $params = array( 
                        'apiKey='.$this->apikey,
                        'svcType=api',
                        'svcCode=SCHOOL',
                        'gubun='.$gubun,
                        'searchSchulNm='.$searchSchulNm,
                        'thisPage='.$thisPage,
                        'perPage='.$perPage
                        
                    );
        $params = '?'.implode('&', $params);
        $url = $this->jsonurl.$params;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $output = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);
    
        if(!empty($curl_errno)) {
           $output = '요청하신 서비스가 정상적으로 처리되지 않았습니다.';
            
        }
        return $output;
    }
    /**
     * Magic method getter, redirects to read only values
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        return $this->$name;
    }
        
}
