<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018112102;
$plugin->requires  = 2012061700;
$plugin->component = 'local_findschool'; // Full name of the plugin (used for diagnostics)