<?php

$string['pluginname'] = '학교검색';
$string['pluginnameplural'] = '학교검색';

$string['setting:key'] = '학교검색 API 키';
$string['setting:keytext'] = '교육부 커리어넷에서 발급받은 API키를 입력해야 합니다.(http://www.career.go.kr/cnet/front/openapi/openApiMainCenter.do)';
