<?php

$string['pluginname'] = 'Find school';
$string['pluginnameplural'] = 'Find school';

$string['setting:key'] = 'API key';
$string['setting:keytext'] = 'You must enter the API key issued by the Ministry of Education Career Net.(http://www.career.go.kr/cnet/front/openapi/openApiMainCenter.do)';
