<?php

defined('MOODLE_INTERNAL') || die;

if (is_siteadmin()) {
    $settings = new admin_settingpage('local_findschool', new lang_string('pluginname', 'local_findschool'));

    $ADMIN->add('localplugins', $settings);
    
    $name = 'local_findschool/apikey';
    $title = get_string('setting:key', 'local_findschool');
    $description = get_string('setting:keytext', 'local_findschool');
    $settings->add(new admin_setting_configtext($name, $title, $description, '', PARAM_RAW, 50));
}

