<?php

require(dirname(dirname(__FILE__)) . '/config.php');
require_once('zip_lib.php');



$zip = new DirectZip();
$zip->open('압축테스트.zip');

//$zip->addFile("메일발송내역_2017-12-07.xlsx", "메일발송내역_2017-12-07.xlsx");
//$zip->addFile('휴가사용내역서(1_12월).xlsx', '휴가사용내역서(1_12월).xlsx');
//CP949
$zip->addFile('메일발송내역_2017-12-07.xlsx', iconv("UTF-8", "CP949", '메일발송내역_2017-12-07.xlsx'));
$zip->addFile('휴가사용내역서(1_12월).xlsx', iconv("UTF-8", "CP949", '휴가사용내역서(1_12월).xlsx'));
//$zip->addFromString('바로 글쓰기.txt', '파일 내용'); // 압축파일에 '파일 내용'을 '바로 글쓰기.txt'로 추가
$zip->close();
/*
브라우저로 보낼 압축파일 이름.zip
│  바로 글쓰기.txt
│  압축파일 내 파일 이름.jpg
│  추가할 파일3.jpg
│
└─압축파일 내 폴더 이름
        압축파일 내 파일 이름.png
*/
?>
