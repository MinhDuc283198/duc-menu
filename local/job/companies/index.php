<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/employers.php');

// =====================================================================================================
// handles
$comtypeid = optional_param('comtypeid', 0, PARAM_INT);
$comname_sortorder = optional_param('comname_sortorder', -1, PARAM_INT);

$baseurl = new moodle_url($VISANG->wwwroot . '/companies/index.php');

$comtype = null;
if ($comtypeid != 0) {
    $comtype = $DB->get_record('vi_company_types', array('id' => $comtypeid));
    if ($comtype) {
        $baseurl = new moodle_url($baseurl, array('comtypeid' => $comtypeid));
    }
}

if ($comname_sortorder != -1) {
    $comname_sortorder = $comname_sortorder == 1 ? $comname_sortorder : 0;
}

$employers_data = mkjobs_search_employers();
$employers = $employers_data['data'];
$employers_count = $employers_data['total'];


// =====================================================================================================
// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'companies';
$VISANG->theme->header();
?>
<style>
    ul.category-list>li:not(.bt):hover>a,
    ul.category-list:not(.hover)>li.selected>a {
        color: #485cc7;
        background: url("/theme/oklassedu/pix/images/icon_arrow_r.png") no-repeat right center;
    }

    ul.category-list>li>a {
        font-size: 17px;
        font-weight: 500;
        color: #555;
        border-top: 1px solid #eee;
        padding: 13px 16px 13px 5px;
        display: block;
    }

    ul.category-list {
        grid-gap: 0 !important;
        gap: 0 !important;
        grid-column-gap: 1rem !important;
        column-gap: 1rem !important;
        display: grid !important;
    }

    ul.category-list>li>a {
        border-top: 0px !important;
        border-bottom: 1px solid #eee !important;
    }

    @media (max-width: 576px) {
        ul.category-list {
            /* -webkit-columns: 2;
            -moz-columns: 2;
            columns: 2; */
            grid-template-columns: repeat(2, minmax(0, 1fr)) !important;
            top: 30px;
        }

        ul.category-list>li>a {
            font-size: 0.8rem !important;
            padding: 15px !important;
            padding: 8px 10px 8px 0px !important;
        }
    }

    @media (min-width: 576px) {
        ul.category-list {
            /* -webkit-columns: 3;
            -moz-columns: 3;
            columns: 3; */
            grid-template-columns: repeat(3, minmax(0, 1fr)) !important;
            top: 30px;
        }
    }

    @media (min-width: 768px) {
        ul.category-list {
            /* -webkit-columns: 4;
            -moz-columns: 4;
            columns: 4; */
            grid-template-columns: repeat(4, minmax(0, 1fr)) !important;
            top: 40px;
        }
    }

    @media (min-width: 992px) {
        ul.category-list {
            /* -webkit-columns: 5;
            -moz-columns: 5;
            columns: 5; */
            grid-template-columns: repeat(5, minmax(0, 1fr)) !important;
            top: 40px;
        }
    }
</style>
<div class="cont">
    <div class="group">
        <div class="page-nav has-icon position-relative">
            <p><?php echo get_string('Company', 'local_job'); ?></p>
            <?php
            if ($comtype) {
                echo html_writer::tag('p', $comtype->name);
            }
            ?>
            <div class="sub-mn position-static">
                <span>Menu icon</span>
                <ul class="p-jbox w-full z-dropdown position-absolute left-0">
                    <ul class="category-list">
                    	<li>
                            <a class="text-truncate" href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/index.php'); ?>">
                                <b><?php echo get_string('all'); ?></b>
                            </a>
                        </li>
                        <?php
                        $com_types = $DB->get_records('vi_company_types', array(), 'name ASC');
                        ?>
                        <?php foreach ($com_types as $com_type) : ?>
                            <li>
                                <a class="text-truncate" href="<?php echo new moodle_url($baseurl, array('comtypeid' => $com_type->id)); ?>">
                                    <?php echo $com_type->name; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                        
                    </ul>
                </ul>
            </div>
            <div class="f-r">
                <div class="tp-tb-area m-0">
                    <ul class="bar-tab">
                        <li>
                            <div class="d-inline-block <?php echo $comname_sortorder == -1 ? '' : 't-gray'; ?>">
                                <a href="<?php echo $baseurl; ?>"><?php echo get_string('popular', 'local_job'); ?></a>
                            </div>
                        </li>
                        <li>
                            <div class="btn-group">
                                <a href="#" class="dropdown-toggle <?php echo $comname_sortorder == -1 ? 't-gray' : ''; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo get_string('Companyname', 'local_job'); ?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?php echo new moodle_url($baseurl, array('comname_sortorder' => 0)); ?>" class="dropdown-item <?php echo $comname_sortorder == 0 ? 'active' : ''; ?>"><?php echo get_string('Ascendingorder', 'local_job'); ?></a>
                                    <a href="<?php echo new moodle_url($baseurl, array('comname_sortorder' => 1)); ?>" class="dropdown-item <?php echo $comname_sortorder == 1 ? 'active' : ''; ?>"><?php echo get_string('Descendingorder', 'local_job'); ?></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php if (count($employers) < 1) { ?>
            <div class="mx-2 my-4 px-3 py-5 bg-area text-center">
                <p class="h2"><?php echo get_string('companyNotice1', 'local_job'); ?></p>
                <p class="h6"><?php echo get_string('companyNotice2', 'local_job'); ?></p>
            </div>
        <?php } ?>

        <div id="employersGroup">
            <div class="row mx-0 my-3">
                <?php foreach ($employers as $employer) : ?>
                    <div class="col-sm-6 col-md-4 col-lg-3 px-2 mb-3">
                        <div class="compa-card border">
                            <div class="compa-card-img">
                                <div class="position-absolute top-0 right-0 p-2 mr-2" onclick="followEmployer(this, <?php echo $employer->id; ?>)">
                                    <?php echo mkjobs_has_follow_employer($employer->id) ? '<i class="cursor-pointer ic ic-heart-on"></i>' : '<i class="cursor-pointer ic ic-heart"></i>' ?>
                                </div>
                                <a class="d-flex justify-content-center h-100" href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id)); ?>">
                                    <img class="align-self-center contain-image" src="<?php echo $employer->company_logo; ?>">
                                </a>

                            </div>
                            <div class="border-top p-jbox">
                                <div class="tit-jbox text-truncate text-truncate-2 mb-2">
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id)); ?>"><?php echo $employer->company_name; ?></a>
                                </div>
                                <p class="t-gray text-truncate text-truncate-2 mb-3"><?php echo $employer->company_short_description; ?></p>
                                <div class="d-flex justify-content-between">
                                    <div><?php echo $employer->location ? $employer->location->name : ''; ?></div>

                                    <?php if ($employer->job_cnt) : ?>
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id, 'view' => 'our-jobs')); ?>" class="text-primary">
                                            <span><?php echo $employer->job_cnt; ?><?php echo get_string('Jobview', 'local_job'); ?></span>
                                            <span class="ic ic-arrow-right-on"></span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div v-for="item in items" v-cloak class="col-sm-6 col-md-4 col-lg-3 px-2 mb-3">
                    <div class="compa-card border">
                        <div class="compa-card-img d-flex justify-content-center">
                            <div class="position-absolute top-0 right-0 p-2 mr-2" onclick="followEmployer(this, <?php echo $employer->id; ?>)">
                                <?php echo mkjobs_has_follow_employer($employer->id) ? '<i class="cursor-pointer ic ic-heart-on"></i>' : '<i class="cursor-pointer ic ic-heart"></i>' ?>
                            </div>
                            <a class="d-flex justify-content-center h-100" :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id">
                                <img class="align-self-center contain-image" :src="item.company_logo">
                            </a>

                        </div>
                        <div class="border-top p-jbox">
                            <div class="tit-jbox text-truncate text-truncate-2 mb-2">
                                <a :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id">{{ item.company_name }}</a>
                            </div>
                            <p class="t-gray text-truncate text-truncate-2 mb-3"><a :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id">{{ item.company_short_description }}</a></p>
                            <div class="d-flex justify-content-between">
                                <div>{{ item.location.name }}</div>
                                <a :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id + '&view=our-jobs'" class="text-primary" v-if="item.job_cnt">
                                    <span>{{ item.job_cnt }} <?php echo get_string('Jobview', 'local_job'); ?></span>
                                    <span class="ic ic-arrow-right-on"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($employers_count > 20) { ?>
                <div :class="['px-2 my-3', loadmore ? '':'d-none']">
                    <button class="btn btn-block border btn-lg rounded-0 btn-spinner" :disabled="onfetch" @click="fetch">
                        <span><?php echo get_string('ViewMore', 'local_job'); ?></span>
                        <span class="btn-lbl ic ic-arrow-down"></span>
                        <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    </button>
                </div>
            <?php } ?>
        </div>

        <script>
            $(function() {
                new Vue({
                    el: "#employersGroup",
                    mixins: [mkjobs_search_mixin()],
                    data: {
                        action: "mkjobs_search_employers",
                        sesskey: "<?php echo sesskey(); ?>",
                        perpage: 20,
                        fields: "id,company_name,company_logo,company_short_description",
                        filters: {
                            comtypeid: "<?php echo $comtypeid; ?>",
                            comname_sortorder: "<?php echo $comname_sortorder; ?>"
                        }
                    }
                });
            });
        </script>
    </div>
</div>

<script>
    function followEmployer(el, id) {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkjobs_follow_employer",
            employerid: id
        }, function(data) {
            if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
            } else if (data == 2) {
                alert("<?php echo get_string('FibeAdd', 'local_job'); ?>");
            } else if (data == 1) {
                $(el).html('<i class="cursor-pointer ic ic-heart-on"></i>');
            } else {
                $(el).html('<i class="cursor-pointer ic ic-heart"></i>');
            }
        });
    }
</script>

<?php $VISANG->theme->footer();
