<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

// =====================================================================================================
// handles
$employerid = required_param('employerid', PARAM_INT);
$view = optional_param('view', '', PARAM_RAW);
$navtab = optional_param('navtab', 'info', PARAM_RAW);
$employer = mkjobs_get_employer_info($employerid);

$baseurl = new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employerid));

$has_review = $DB->count_records('vi_reviews', array('employer_id' => $employerid, 'user_id' => $USER->id)) > 0;

$action = optional_param('action', '', PARAM_RAW);
if ($action == 'reviewsubmit' && !$has_review) {
    require_sesskey();
    $ratings = $_POST['ratings'];
    mkjobs_save_employer_review($USER->id, $employerid, $ratings);
    // redirect($baseurl . '&navtab=reviews');
    define('AJAX_SCRIPT', true);
    echo json_encode(array('status' => 1));
    die();
}

if ($action != '') {
    redirect($baseurl);
}

// update view count
$viewcout = new stdClass();
$viewcout->id = $employer->id;
$viewcout->view_count = intval($employer->view_count) + 1;
$DB->update_record('vi_employers', $viewcout);

$settings = visang_settings();
$apikey = $settings->google_map_api;
$VISANG->theme->addJS("https://maps.google.com/maps/api/js?key=" . $apikey . "&v=3&libraries=drawing,places&language=" . current_language() . '&callback=initMapLocations');

// =====================================================================================================
// renders
$VISANG->theme->title = $employer->company_name;
$VISANG->theme->menu = 'companies';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-bx">
            <div class="row mx-0">
                <div class="col-md-3 px-0 py-jbox d-md-flex justify-content-center">
                    <div class="align-self-center text-center">
                        <img class="position-relative w-auto h-auto contain-image" src="<?php echo $employer->company_logo; ?>" alt="logo">
                    </div>
                </div>
                <div class="col-md-9 px-0 pl-md-5 border-md-left">
                    <div class="row mx-0">
                        <div class="px-0 col-sm-9">
                            <div class="pg-tit"><?php echo $employer->company_name; ?></div>
                            <div class="mb-1">
                                <span class="ic ic-map-marker mr-2"></span>
                                <span class="t-gray"><?php echo $employer->company_address; ?></span>
                            </div>
                            <div class="mb-1">
                                <span class="ic ic-map-signs mr-2"></span>
                                <span class="t-gray"><?php echo $employer->company_type->name; ?></span>
                            </div>
                            <div class="mb-1">
                                <span class="ic ic-user mr-2"></span>
                                <span class="t-gray"><?php echo $employer->company_size->name; ?></span>
                            </div>
                            <div class="mb-1">
                                <img src="<?php echo $VISANG->wwwroot . '/assets/dist/images/flags/' . $employer->country->short_name . '.png'; ?>" class="mr-1 ic-flag" height="17">
                                <span class="t-gray"><?php echo $employer->country->name; ?></span>
                            </div>
                        </div>
                        <div class="px-0 col-sm-3 text-sm-center mt-3 mt-sm-0 d-flex d-sm-block justify-content-center">
                            <div class="btn py-lg-3 px-lg-4 align-self-center" onclick="followEmployer(this, <?php echo $employer->id; ?>)">
                                <?php echo mkjobs_has_follow_employer($employer->id) ? '<i class="cursor-pointer ic ic-heart02-on"></i>' : '<i class="cursor-pointer ic ic-heart02"></i>' ?>

                            </div>
                            <div class="mx-3 my-0 mt-sm-3 mb-sm-2 align-self-center">
                                <?php if ($employer->company_website != '') : ?>
                                    <a href="#" onclick="openHyperlink('<?php echo $employer->company_website; ?>');" class="d-inline-block">
                                        <span class="ic ic-external-link ic-lg"></span>
                                    </a>
                                <?php endif; ?>
                                <?php if ($employer->company_facebook != '') : ?>
                                    <a href="#" onclick="openHyperlink('<?php echo $employer->company_facebook; ?>');" class="d-inline-block">
                                        <span class="ic ic-facebook ic-lg"></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="align-self-center">
                                <span class="ic ic-eye mr-2"></span>
                                <span class="t-gray"><?php echo number_format($employer->view_count, 0); ?> <?php echo get_string('times', 'local_job'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $ratings = mkjobs_get_employer_ratings($employerid);
        $reviews = mkjobs_get_employer_reviews($employerid);
        ?>
        <ul class="crs-tab tab-event" style="border-top: 0px !important;">
            <!-- tab event -->
            <li hidden class="<?php echo $navtab == 'info' ? 'on' : ''; ?>" data-target=".data01" style="width:50%;"><a href="#"><?php echo get_string('employer:navtab:info', 'local_job'); ?></a></li>
            <li hidden class="<?php echo $navtab == 'reviews' ? 'on' : ''; ?>" data-target=".data02" style="width:50%;"><a href="#"><?php echo get_string('employer:navtab:reviews', 'local_job'); ?>(<?php echo $reviews['total']; ?><?php echo get_string('review:count', 'local_job'); ?>)</a></li>
        </ul>

        <div class="crs-tab-cont target-area" style="border: 0px !important;">

            <div class="data01 <?php echo $navtab == 'info' ? 'on' : ''; ?> article-jbox" style="border: 0px !important; padding: 0px !important;">

                <div class="pg-tit">
                    <?php echo get_string('employer:navtab:info', 'local_job'); ?>
                </div>

                <?php if ($employer->company_short_description != '') : ?>
                    <h2 style="font-weight: 300; margin-bottom: 50px !important;">
                        <?php echo $employer->company_short_description; ?>
                    </h2>
                <?php endif; ?>
                <?php if ($employer->company_description != '') : ?>
                    <?php echo $employer->company_description; ?>
                <?php endif; ?>
                <ul class="cate-list text-left mb-0">
                    <?php foreach ($employer->skill_groups as $skill) : ?>
                        <li>
                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillgroupid' => $skill->id)); ?>">
                                <?php echo $skill->name; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="data02 <?php echo $navtab == 'reviews' ? 'on' : ''; ?>">
                <h6 class="has-star">
                    <div class="star-event big" data-num="<?php echo $ratings['rate']; ?>"></div>
                    <span><?php echo round($ratings['rate'], 2); ?><?php echo get_string('review:point', 'local_job'); ?></span>
                    <span class="t-gray">(<?php echo $reviews['total']; ?><?php echo get_string('review:count', 'local_job'); ?>)</span>

                    <?php if (!$has_review) : ?>
                        <div class="btns br f-r w-auto showpop-review"><?php echo get_string('review:add_register', 'local_job'); ?></div>
                    <?php endif; ?>
                </h6>
                <?php if ($reviews['total'] == 0) : ?>
                    <div class="no-data">
                        <div><?php echo get_string('review:nodata', 'local_job'); ?></div>
                    </div>
                <?php else : ?>
                    <h5 class="h5 mb-3"><?php echo get_string('review:desc', 'local_job'); ?></h5>
                    <?php foreach ($ratings['ratings'] as $rating) : ?>
                        <div class="row my-2">
                            <div class="col-md-3 col-6">
                                <strong><?php echo $rating->title; ?></strong>
                            </div>
                            <div class="col-md-9 col-6">
                                <div class="star-event d-inline-block" data-num="<?php echo $rating->rate; ?>"></div>
                                <span><?php echo round($rating->rate, 2); ?><?php echo get_string('review:point', 'local_job'); ?></span>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <h5 class="h5 mt-5 mb-3"><?php echo get_string('review:toplatest', 'local_job'); ?></h5>
                    <ul class="review-list">
                        <?php foreach ($reviews['reviews'] as $review) : ?>
                            <li class="overflow-initial border-top py-3 mb-0">
                                <div class="tp">
                                    <div class="btn-group">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <div class="star-event d-inline-block float-none" data-num="<?php echo $review->ratings['rate']; ?>"></div>
                                            <strong><?php echo round($review->ratings['rate'], 2); ?><?php echo get_string('review:point', 'local_job'); ?></strong>
                                        </a>
                                        <div class="dropdown-menu">
                                            <div class="px-3" style="min-width: 300px;">
                                                <?php foreach ($review->ratings['ratings'] as $r) : ?>
                                                    <div class="my-3">
                                                        <strong><?php echo $r->title; ?></strong>
                                                        <div class="mt-2">
                                                            <div class="star-event d-inline-block float-none" data-num="<?php echo $r->rate; ?>"></div>
                                                            <strong><?php echo round($r->rate, 2); ?><?php echo get_string('review:point', 'local_job'); ?></strong>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div><span class="text-muted"><?php echo $review->timecreated; ?></span></div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>

        <?php if (count($employer->jobs) > 0) { ?>
            <div class="pg-tit" id="our-jobs">
                <?php echo get_string('Jobsprogress', 'local_job'); ?>
            </div>
        <?php } ?>
        <div class="mb-5">
            <?php foreach ($employer->jobs as $job) : ?>
                <div class="row mx-0 border mb-3">
                    <div class="col-3 py-jbox d-md-flex justify-content-center">
                        <div class="align-self-center">
                            <img class="position-relative w-auto h-auto contain-image" src="<?php echo $job->company_logo; ?>">
                            <div class="mt-2 text-center">
                                <i class="cursor-pointer ic <?php echo mkjobs_has_saved_job($job->id) ? "ic-heart-on" : "ic-heart";  ?>" onclick="savedJob(this, <?php echo $job->id; ?>)"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 pl-0 pr-jbox py-jbox">
                        <div class="row mx-0 tit-jbox">
                            <div class="px-0 col-md-10">
                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>">
                                    <?php echo $job->title; ?>
                                </a>
                            </div>
                            <div class="px-0 col-md-2 d-none d-md-block text-md-right text-primary"><?php echo $job->d_enddate; ?></div>
                        </div>
                        <p class="text-secondary mb-1">
                            <i class="ic ic-usd"></i>
                            <span><?php echo $job->salary; ?></span>
                        </p>
                        <div class="row mx-0">
                            <div class="px-0 col-md-10 d-none d-md-block">
                                <p class="t-gray"><?php echo $job->short_description; ?></p>
                            </div>
                            <div class="px-0 col-md-2 text-md-right text-primary">
                                <p><?php echo $job->location->name; ?></p>
                            </div>
                        </div>
                        <div class="row mx-0">
                            <div class="px-0 col-md-10">
                                <ul class="cate-list text-left mb-2">
                                    <?php foreach ($job->skills as $skill) : ?>
                                        <li>
                                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                                <?php echo $skill->name; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="px-0 col-md-2 text-md-right">
                                <p class="t-gray"><?php echo $job->timecreated; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="pg-tit">
            <?php echo get_string('Benefits', 'local_job'); ?>
        </div>
        <div class="article-jbox mb-5">
            <?php echo $employer->reason_description; ?>
        </div>

        <div class="pg-tit">
            <?php echo get_string('location', 'local_job'); ?>
        </div>
        <div class="article-jbox" id="map-locations">
            <div class="mb-1" v-for="marker in markers" v-cloak>
                <span class="ic ic-map-marker mr-2"></span>
                <span class="t-gray cursor-pointer" @click="view(marker)">{{ marker.customInfo.title }}</span>
            </div>
        </div>
        <div class="map-jbox border bg-gray my-4" id="map-container"></div>

        <script>
            function initMapLocations() {
                new Vue({
                    el: "#map-locations",
                    mixins: [mkjobs_maplocation_mixin()],
                    data: {
                        id: 'map-container',
                        locations: [
                            <?php foreach ($employer->locations as $location) : ?> {
                                    "lat": parseFloat(<?php echo floatval($location->lat); ?>),
                                    "lng": parseFloat(<?php echo floatval($location->lng); ?>),
                                    "info": "<?php echo str_replace("\r\n", "", $location->full_address); ?>"
                                },
                            <?php endforeach; ?>
                        ]
                    }
                });
            }

            <?php if ($view == 'our-jobs') : ?>
                $(function() {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $('#our-jobs').offset().top - $('.wp').height() - ($('.tp').is(':visible') ? 40 : 0) - 15
                    }, 200);
                });
            <?php endif; ?>
        </script>
    </div>
</div>
<script>
    function followEmployer(el, id) {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkjobs_follow_employer",
            employerid: id
        }, function(data) {
            if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
            } else if (data == 2) {
                alert("<?php echo get_string('FibeAdd', 'local_job'); ?>");
            } else if (data == 1) {
                $(el).html('<i class="cursor-pointer ic ic-heart02-on"></i>');
            } else {
                $(el).html('<i class="cursor-pointer ic ic-heart02"></i>');
            }
        });
    }

    function savedJob(el, id, classon, classoff) {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkjobs_saved_job",
            jobid: id
        }, function(data) {
            if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
            } else if (data == 1) {
                $(el).removeClass(classoff || "ic-heart");
                $(el).addClass(classon || "ic-heart-on");
            } else {
                $(el).removeClass(classon || "ic-heart-on");
                $(el).addClass(classoff || "ic-heart");
            }
        });
    }

    $.fn.serializeFormJSON = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function pop_review_submit(frm) {
        if (confirm('<?php echo get_string('review:pop_confirm', 'local_job'); ?>')) {
            var frmData = $(frm).serializeFormJSON();
            $.post("<?php echo $VISANG->wwwroot . '/companies/view.php' ?>", frmData, function() {
                utils.popup.close_pop($('.layerpop'));
                setTimeout(function() {
                    utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/companies/pop_review.php' ?>", {
                        "width": "600px",
                        "height": "auto",
                        "callbackFn": function() {}
                    }, {
                        "employerid": "<?php echo $employer->id; ?>",
                        "action": "success"
                    });
                }, 200);
            });
        }
        return false;
    }

    var mkjobs_rating_mixin = function() {
        return {
            data: {
                point: 0,
                len: 5
            }
        }
    };

    function openHyperlink(url) {
        url = url.match(/^http[s]?:\/\//) ? url : 'http://' + url;
        window.open(url, '_blank');
    }

    $(function() {
        $(".showpop-review").click(function() {
            <?php if (isloggedin()) : ?>
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/companies/pop_review.php' ?>", {
                    "width": "600px",
                    "height": "auto",
                    "callbackFn": function() {
                        $('.rating-component').each(function() {
                            new Vue({
                                el: this,
                                mixins: [mkjobs_rating_mixin()],
                            });
                        });
                    }
                }, {
                    "employerid": "<?php echo $employer->id; ?>"
                });
            <?php else : ?>
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
            <?php endif; ?>
            return false;
        });
    });
</script>
<?php
$VISANG->theme->footer();
