<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/employers.php');

$employerid = required_param('employerid', PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);

$employer = mkjobs_get_employer_info($employerid);

$ratings = $DB->get_records('vi_ratings', null, 'sortorder ASC');

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('review:pop_title', 'local_job'); ?>
        <?php if ($action == '') : ?>
            <a href="#" class="pop-close">닫기</a>
        <?php endif; ?>
    </div>
    <?php if ($action == 'success') : ?>
        <div class="pop-contents">
            <div class="addr-info">
                <ul class="cp-info">
                    <li>
                        <div class="cp-logo">
                            <img src="<?php echo $employer->company_logo; ?>" alt="visang" />
                        </div>
                        <strong><?php echo $employer->company_name; ?></strong>
                    </li>
                </ul>

                <div class="alert alert-success my-4" role="alert">
                    <?php echo get_string('review:pop_success', 'local_job'); ?>
                </div>
            </div>
        </div>
        <div class="btn-area text-center">
            <a href="<?php echo $VISANG->wwwroot . '/companies/view.php?employerid=' . $employerid . '&navtab=reviews'; ?>" class="btns point w100"><?php echo get_string('review:pop_close', 'local_job'); ?></a>
        </div>
    <?php else : ?>
        <form onsubmit="return pop_review_submit(this);" method="post">
            <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
            <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
            <input type="hidden" name="action" value="reviewsubmit">

            <div class="pop-contents">
                <div class="addr-info">
                    <ul class="cp-info">
                        <li>
                            <div class="cp-logo">
                                <img src="<?php echo $employer->company_logo; ?>" alt="visang" />
                            </div>
                            <strong><?php echo $employer->company_name; ?></strong>
                        </li>
                    </ul>

                    <div class="rw mg-bt20">
                        <p class="tit my-4"><?php echo get_string('review:pop_desc', 'local_job'); ?></p>
                        <?php
                        $idx = 0;
                        ?>
                        <?php foreach ($ratings as $rating) : ?>
                            <div class="row my-2">
                                <div class="col-6">
                                    <strong>
                                        <?php
                                        switch (current_language()) {
                                            case 'ko':
                                                echo $rating->title_ko;
                                                break;
                                            case 'en':
                                                echo $rating->title_en;
                                                break;
                                            case 'vi':
                                                echo $rating->title_vi;
                                                break;
                                            default:
                                                echo $rating->title;
                                                break;
                                        }
                                        ?>
                                    </strong>
                                </div>
                                <div class="col-6 rating-component" v-cloak>
                                    <div class="star-event hover d-inline-block">
                                        <span v-for="i in len" :key="i" @click="point = i" :class="point >= i ? 'on':''"></span>
                                    </div>
                                    <span v-if="point > 0">{{ point }}<?php echo get_string('review:point', 'local_job'); ?></span>
                                    <input type="hidden" name="ratings[<?php echo $idx; ?>][id]" value="<?php echo $rating->id; ?>">
                                    <input type="hidden" name="ratings[<?php echo $idx++; ?>][point]" :value="point">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="btn-area text-center">
                <input type="submit" value="<?php echo get_string('review:pop_submit', 'local_job'); ?>" class="btns point w100" />
            </div>
        </form>
    <?php endif; ?>
</div>
<?php
echo $OUTPUT->footer();
