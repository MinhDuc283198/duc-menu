<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/lib.php');

global $CFG, $VISANG, $DB, $USER;

// ===================================================================================================
// handles
require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($VISANG->dirroot . '/lib/employers.php');

$job_search_hashtags = mkjobs_get_searchtext('jobs');
$employers_data = mkjobs_search_employers();
$employers = $employers_data['data'];
$employers_count = $employers_data['total'];
$job_search_hashtags = mkjobs_get_searchtext('jobs');

$top_banners = mkjobs_get_banners('top');

$em = "SELECT * 
FROM {vi_employers} 
LIMIT 12 ";
$emp = $DB->get_records_sql($em);

$employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id 
                WHERE ad.user_id = :id";
$employer = $DB->get_record_sql($employer_sql, array('id' => $USER->id));
$board = 8;
if ($employer != null) $board = 9;
// bottom notice
$nsql = "SELECT jc.id,jc.title,jc.timecreated,u.firstname, u.lastname, jc.board FROM {jinoboard_contents} jc
        JOIN {user} u ON jc.userid = u.id
        WHERE board = $board AND isnotice = 0 ORDER BY jc.timecreated DESC LIMIT 3";
$notice = $DB->get_records_sql($nsql);

$isHomePage = true;

// ===================================================================================================

$sql = "SELECT ad.employer_id as val, em.company_name
                            FROM {vi_admin_employers} ad
                            JOIN {vi_employers} em ON em.id = ad.employer_id
                            WHERE ad.user_id = :id";
$comid = $DB->get_record_sql($sql, array('id' => $USER->id));
if ($comid != null) redirect($VISANG->wwwroot . '/employer/dashboard.php');
// renders

$districts = mkjobs_get_all_location();
$cateories = mkjobs_get_all_categories();

// add slideshows
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');
?>
    <!--    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">-->
<?php
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->header();

?>

    <div class="main">
        <!-- slide begin -->
        <div class="banner-sec">
            <div id="carouselId" class="carousel slide " data-ride="carousel" style="width: 100%;">
                <ol class="carousel-indicators">
                    <?php for ($i = 0; $i < count($top_banners); $i++) { ?>
                        <?php if ($i == 0) { ?>
                            <li data-target="#carouselId" data-slide-to="<?php echo $i ?>" class="active"></li>
                        <?php } else { ?>
                            <li data-target="#carouselId" data-slide-to="<?php echo $i ?>" class=""></li>
                        <?php } ?>
                    <?php } ?>
                </ol>
                <div class="carousel-inner banner_slide" role="listbox">
                    <?php foreach ($top_banners as $banner) : ?>
                        <div class="carousel-item">
                            <img class="d-md-none" src="<?php echo $banner->path_modile; ?>" alt="banner">
                            <img class="d-none d-md-inline-block" src="<?php echo $banner->path; ?>" alt="banner">
                        </div>
                    <?php endforeach; ?>
                </div>
                <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- slide end -->

        <!-- search begin -->
        <div class="search-sec">
            <form action="<?php echo $CFG->wwwroot . '/local/job/search.php' ?>" method="get">
                <div class="col-lg-11 col-md-11" style="margin: 0 auto;">
                    <div class="row px-4 px-md-0">
                        <div class="col-lg-4 col-md-12 col-sm-12 p-1 input-wrapper">
                            <img src="jobs/img/search.png" alt="search-icon">
                            <input type="text" name="searchtext" class="form-control search-slt search-keyworld" id="search-keyworld"
                                   placeholder="<?php echo get_string('searchkeyworld', 'local_job'); ?>">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                            <img src="jobs/img/allcategory.png" alt="search-icon">
                            <select class="form-control search-slt search-category custom-select" id=""
                                    name="skillgroupid">
                                <option value="0"><?php echo get_string('searchcategories', 'local_job'); ?></option>
                                <?php foreach ($cateories as $c) : ?>
                                    <option value="<?php echo $c->id ?>"><?php echo $c->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                            <img src="jobs/img/maps.png" alt="search-icon">
                            <select class="form-control search-slt search-location custom-select" id="" name="city">
                                <option value="0"><?php echo get_string('searchlocation', 'local_job'); ?></option>
                                <?php foreach ($districts as $d) : ?>
                                    <option value="<?php echo $d->id ?>"><?php echo ($current_language == "ko") ? $d->name_ko : (($current_language == "en") ? $d->name_en : $d->name_vi); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-12 p-1" style="margin: 0 auto">
                            <button type="submit" class="btn btn-danger wrn-btn"><i class="fa fa-search"
                                                                                    aria-hidden="true"></i><?php echo get_string('find_now', 'local_job') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- search end -->

        <div class="container">
            <!-- job list begin -->
            <div class="list-job">
                <input type="radio" name="switch-job" id="hot-job" checked>
                <input type="radio" name="switch-job" id="new-job">
                <header>
                    <div class="row switch-job">
                        <div class="col-6 left">
                            <label for="hot-job" class="hot-job">
                                <p><?php echo get_string('hot_job', 'local_job'); ?></p>
                                <hr>
                            </label>
                        </div>
                        <div class="col-6 right">
                            <label for="new-job" class="new-job">
                                <p><?php echo get_string('new_job', 'local_job'); ?></p>
                                <hr>
                            </label>
                        </div>
                    </div>
                </header>
                <div class="job-area">
                    <div class="wrapper-jobs">
                        <div class="jobs">
                            <!-- hot job begin -->
                            <?php
                            $recommend_jobs = mkjobs_get_recommend_jobs(isloggedin());
                            ?>
                            <div class="row row-1">
                                <div id="hot-job-list" class="carousel slide" data-ride="carousel"
                                     data-interval="false">
                                    <div class="carousel-inner list-job-padding" role="listbox">
                                        <?php $count = 0; ?>
                                        <?php foreach ($recommend_jobs as $recommend_job) : ?>
                                            <?php
                                            $saved_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $recommend_job->id));
                                            $location = mkjobs_get_location_recommend_jobs($recommend_job->employer_id);
                                            ?>
                                            <?php
                                            if ($count == 0 || $count % 8 == 0) {
                                                if ($count == 0) {
                                                    echo '<div class="carousel-item active">';
                                                    echo '<div class="row">';
                                                } else {
                                                    echo '<div class="carousel-item">';
                                                    echo '<div class="row">';
                                                }
                                            }
                                            ?>
                                            <?php
                                            if ($count == 0 || $count % 4 == 0) {
                                                echo '<div class="col-xl-6 job-content">';
                                            }
                                            ?>
                                            <a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id . '&employerid=' . $recommend_job->employer_id; ?>">
                                                <div class="one-job row hot-job-item">
                                                    <div class="one-job_imagelink col-3">
                                                        <img src="<?php echo $recommend_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $recommend_job->company_logo; ?>"
                                                             alt="Image of hot job">
                                                    </div>
                                                    <div class="job-detail col-8">
                                                        <h4><?php echo $recommend_job->title; ?></h4>
                                                        <p>
                                                            <?php echo $recommend_job->company_name; ?>
                                                        </p>
                                                        <div class="map-point">
                                                            <p>
                                                                <i class="far fa-map-marker-alt"></i> <?php echo str_replace("\r\n", "", $recommend_job->company_location); ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 hot-job-label">
                                                        <div class="sticker">
                                                            <p>NEW</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <?php $count++; ?>
                                            <?php
                                            if ($count % 4 == 0 || $count == count($recommend_jobs)) {
                                                echo '</div>';
                                            }
                                            ?>
                                            <?php
                                            if ($count % 8 == 0 || $count == count($recommend_jobs)) {
                                                echo '</div>';
                                                echo '</div>';
                                            }
                                            ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="pagination-listjob">
                                        <?php
                                        if (count($recommend_jobs) % 8 == 0) {
                                            $hot_job_slide_dot = count($recommend_jobs) / 8;
                                        } else {
                                            $hot_job_slide_dot = ceil(count($recommend_jobs) / 8);
                                        }
                                        ?>
                                        <?php if ($hot_job_slide_dot != 0) : ?>
                                            <a class="carousel-control-prev hot-job" href="#hot-job-list" role="button"
                                               data-slide="prev"
                                               onclick="paginationSlide('hot-job',<?php echo $hot_job_slide_dot; ?>)">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <ol class="carousel-indicators list-job">
                                                <?php for ($i = 0; $i < $hot_job_slide_dot; $i++) : ?>
                                                    <?php if ($i == 0) : ?>
                                                        <li data-target="#hot-job-list" data-slide-to="0"
                                                            class="hot-job active"
                                                            onclick="paginationSlide('hot-job',<?php echo $hot_job_slide_dot; ?>)"></li>
                                                    <?php else : ?>
                                                        <li data-target="#hot-job-list"
                                                            data-slide-to="<?php echo $i; ?>"
                                                            class="hot-job"
                                                            onclick="paginationSlide('hot-job',<?php echo $hot_job_slide_dot; ?>)"></li>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </ol>
                                            <a class="carousel-control-next hot-job" href="#hot-job-list" role="button"
                                               data-slide="next"
                                               onclick="paginationSlide('hot-job',<?php echo $hot_job_slide_dot; ?>)">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        <?php else : ?>
                                            <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <!-- hot job end -->
                            <!-- new job begin -->
                            <?php
                            $thistime = time();
                            $new_jobsql = "SELECT vj.*,ve.company_name, ve.company_logo, d.name as company_location
							FROM {vi_jobs} vj
							JOIN {vi_employers} ve on vj.employer_id = ve.id
                            LEFT JOIN {vi_job_locations} jl ON jl.job_id = vj.id
                            LEFT JOIN {vi_locations} l ON l.id = jl.location_id
                            LEFT JOIN {vi_districts} d ON d.id = l.district_id
							WHERE vj.published = 1 AND ve.confirmed=1 AND ve.published=1 AND ve.deleted=0 
                            AND ((vj.startdate=0) OR (vj.startdate < $thistime AND $thistime < vj.enddate))
							ORDER BY vj.timecreated DESC, vj.enddate DESC";
                            $new_jobs = $DB->get_records_sql($new_jobsql, array());
                            ?>
                            <div class="row">
                                <div id="new-job-list" class="carousel slide" data-ride="carousel"
                                     data-interval="false">
                                    <div class="carousel-inner list-job-padding" role="listbox">
                                        <?php $count = 0; ?>
                                        <?php foreach ($new_jobs as $new_job) : ?>
                                            <?php
                                            $saved_new_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $new_job->id));
                                            ?>
                                            <?php
                                            if ($count == 0 || $count % 8 == 0) {
                                                if ($count == 0) {
                                                    echo '<div class="carousel-item active">';
                                                    echo '<div class="row">';
                                                } else {
                                                    echo '<div class="carousel-item">';
                                                    echo '<div class="row">';
                                                }
                                            }
                                            ?>
                                            <?php
                                            if ($count == 0 || $count % 4 == 0) {
                                                echo '<div class="col-xl-6 job-content">';
                                            }
                                            ?>
                                            <a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $new_job->id . '&employerid=' . $new_job->employer_id; ?>">
                                                <div class="one-job row hot-job-item">
                                                    <div class="one-job_imagelink col-3">
                                                        <img src="<?php echo $new_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $new_job->company_logo; ?>"
                                                             alt="Image of new job">
                                                    </div>
                                                    <div class="job-detail col-8">
                                                        <h4><?php echo $new_job->title; ?></h4>
                                                        <p>
                                                            <?php echo $new_job->company_name; ?>
                                                        </p>
                                                        <div class="map-point">
                                                            <p>
                                                                <i class="far fa-map-marker-alt"></i> <?php echo str_replace("\r\n", "", $new_job->company_location); ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 hot-job-label">
                                                        <div class="sticker">
                                                            <p>NEW</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>

                                            <?php $count++; ?>
                                            <?php
                                            if ($count % 4 == 0 || $count == count($new_jobs)) {
                                                echo '</div>';
                                            }
                                            ?>
                                            <?php
                                            if ($count % 8 == 0 || $count == count($new_jobs)) {
                                                echo '</div>';
                                                echo '</div>';
                                            }
                                            ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="pagination-listjob">
                                        <?php
                                        if (count($new_jobs) % 8 == 0) {
                                            $new_job_slide_dot = count($new_jobs) / 8;
                                        } else {
                                            $new_job_slide_dot = ceil(count($new_jobs) / 8);
                                        }
                                        ?>
                                        <?php if ($new_job_slide_dot != 0) : ?>
                                            <a class="carousel-control-prev new-job" href="#new-job-list" role="button"
                                               data-slide="prev"
                                               onclick="paginationSlide('new-job',<?php echo $new_job_slide_dot; ?>)">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <ol class="carousel-indicators list-job">
                                                <?php for ($i = 0; $i < $new_job_slide_dot; $i++) : ?>
                                                    <?php if ($i == 0) : ?>
                                                        <li data-target="#new-job-list" data-slide-to="0"
                                                            class="new-job active"
                                                            onclick="paginationSlide('new-job',<?php echo $new_job_slide_dot; ?>)"></li>
                                                    <?php else : ?>
                                                        <li data-target="#new-job-list"
                                                            data-slide-to="<?php echo $i; ?>"
                                                            class="new-job"
                                                            onclick="paginationSlide('new-job',<?php echo $new_job_slide_dot; ?>)"></li>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </ol>
                                            <a class="carousel-control-next new-job" href="#new-job-list" role="button"
                                               data-slide="next"
                                               onclick="paginationSlide('new-job',<?php echo $new_job_slide_dot; ?>)">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        <?php else : ?>
                                            <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <!-- new job end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- job list end -->
        </div>

        <!-- recommend job begin -->
        <div class="recommend-job-custom">
            <div class="title">
                <p><?php echo get_string('recommend_job', 'local_job'); ?></p>
            </div>
            <div class="recommend-job-custom_content">
                <div class="row">
                    <div class="text-left col-3">
                        <p><?php echo get_string('suitable_job', 'local_job'); ?></p>
                    </div>
                    <div class="img-recommend-job col-4">
                        <img src="/theme/oklassedu/pix/images/recommend-job.png" alt="">
                    </div>
                    <div class="login-recommend-job col-lg-5">
                        <p class="login-des"><?php echo get_string('login_to_see_more', 'local_job'); ?></p>
                        <p class="login-button"><a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php"
                                                   type="button"><?php echo get_string('login_now', 'local_job'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- recommend job end -->

        <!-- feedback begin -->
        <div class="feedback">
            <div class="row title-responsive">
                <div class="col-xl-6 feedback-intro">
                    <hr align="left">
                    <p><?php echo get_string('feedback', 'local_job'); ?></p>
                </div>
                <div class="col-xl-6 img-deco-feedback">
                    <img src="/theme/oklassedu/pix/images/background1.png" alt="">
                    <img src="/theme/oklassedu/pix/images/background2.png" alt="" class="eclipse">
                </div>
            </div>
            <div id="feedback-list" class="carousel slide" data-ride="carousel" style="width: 100%;"
                 data-interval="false">
                <div class="row slide-feedback">
                    <div class="col-xl-1 next-prev-feedback">
                        <a class="carousel-control-prev" href="#feedback-list" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                    <div class="col-xl-10">
                        <div class="carousel-inner" role="listbox">
                            <!-- <?php
                            $job_feedback = mjobbs_get_feedback();

                            $count = 0;
                            ?> -->
                            <?php foreach ($job_feedback as $job_feedbacks) : ?>
                                <?php
                                if ($count == 0) {
                                    echo '<div class="carousel-item active">';
                                    echo '<div class="row row-feedback">';
                                } else if ($count % 3 == 0) {
                                    echo '<div class="carousel-item ">';
                                    echo '<div class="row row-feedback">';
                                }


                                ?>
                                <div class="col-xl-4">
                                    <div class="one-feedback">
                                        <div class="row">

                                            <div class="col-xs-2">
                                                <!-- <img src="/theme/oklassedu/pix/images/user-feedback.png" alt=""> -->
                                                <a href="<?php echo $VISANG->wwwroot . '/jobs/feedback_job.php?feedback_job_id=' . $job_feedbacks->id ?>">
                                                    <img src="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $job_feedbacks->user_avatar_url; ?>">
                                                </a>
                                            </div>
                                            <div class="col-xs-10">
                                                <div class="feedback-detail">
                                                    <a href="<?php echo $VISANG->wwwroot . '/jobs/feedback_job.php?feedback_job_id=' . $job_feedbacks->id ?>">
                                                        <p class="name"><?php echo $job_feedbacks->username ?></p>
                                                        <p class="position"><?php echo $job_feedbacks->job_position ?></p>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>

                                        <p class="description"><?php echo $job_feedbacks->title ?></p>
                                    </div>
                                </div>

                                <?php $count++; ?>
                                <?php
                                if ($count % 3 == 0 || $count == count($job_feedback)) {
                                    echo '</div>';
                                    echo '</div>';
                                }

                                ?>


                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-xl-1 next-prev-feedback">
                        <a class="carousel-control-next" href="#feedback-list" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <?php
                if (count($job_feedback) % 3 == 0) {
                    $number_page_fb = count($job_feedback) / 3;
                } else {
                    $number_page_fb = ceil(count($job_feedback) / 3);
                }
                ?>
                <div class="row">
                    <ol class="carousel-indicators list-feedback">
                        <!-- <li data-target="#feedback-list" data-slide-to="0" class="active"></li>
                            <li data-target="#feedback-list" data-slide-to="1"></li>
                            <li data-target="#feedback-list" data-slide-to="2"></li> -->
                        <?php for ($i = 0; $i < $number_page_fb; $i++) { ?>
                            <?php if ($i == 0) { ?>
                                <li data-target="#feedback-list" data-slide-to="<?php echo $i ?>"
                                    class="active"></li>
                            <?php } else { ?>
                                <li data-target="#feedback-list" data-slide-to="<?php echo $i ?>" class=""></li>
                            <?php } ?>
                        <?php } ?>
                    </ol>
                </div>

            </div>
        </div>
        <!-- feedback end -->

        <!-- top-employee -->
        <div class="container">
            <div class="top-employee">
                <h4><?php echo get_string('top-employers', 'local_job'); ?></h4>
                <div class="row">
                    <?php $count = 0 ?>
                    <?php foreach ($employers as $employer) : ?>
                        <div class="col-xl-2 col-md-4 col-6">
                            <div class="top-employee-item">
                                <a class="d-flex justify-content-center h-100"
                                   href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id)); ?>">
                                    <img class="align-self-center contain-image"
                                         src="<?php echo $employer->company_logo; ?>" alt="Image of Employer">
                                </a>
                            </div>
                        </div>
                        <?php if ($count == 11) {
                            break;
                        }
                        ?>
                        <?php $count++; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <!-- end top-employee -->

        <!-- vector -->
        <div class="vector-line">
            <img src="/theme/oklassedu/pix/images/vectorline.PNG">
        </div>
        <!-- vector end -->

        <!-- recomend courses -->
        <div class="container">
            <div class="recomend-course">
                <h4><?php echo get_string('MasterKoreanProposalCourse', 'local_job'); ?></h4>
                <div id="recommend-course" class="carousel slide" data-ride="carousel" data-interval="false">
                    <?php
                    require_once($CFG->dirroot . '/local/course/lib.php');

                    ?>
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $upCourses = local_course_recommend(isloggedin());
                        $count = 0;
                        foreach ($upCourses as $course) {
                            $context = context_course::instance($course->lcocourseid);
                            $courseimg = local_course_get_imgpath($context->id);
                            ?>

                            <?php if ($count == 0 || $count % 4 == 0) {
                                if ($count == 0) {
                                    echo '<div class="carousel-item active">';
                                    echo '<div class="row">';
                                } else {
                                    echo '<div class="carousel-item">';
                                    echo '<div class="row">';
                                }
                            } ?>
                            <div class="col-md-6">
                                <div class="recommend-item">
                                    <a href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $course->id; ?>">
                                        <div class="row">
                                            <div class="col-xl-4 recommend-item_img">
                                                <img style="width: 100%;" src="<?php echo $courseimg; ?>"
                                                     alt="Image of recommend course">
                                            </div>
                                            <div class="col-xl-8">
                                                <div class="recommend-item_content">
                                                    <h2>
                                                        <p class="block-ellipsis-title ellipsis"><?php echo $course->title; ?> </p>
                                                    </h2>
                                                    <p class="block-ellipsis-description ellipsis">
                                                        <span> <?php echo $course->onelineintro; ?></span>
                                                    </p>
                                                    <div class="main-course-cate mb-2">
                                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                            <?php if ($i <= $course->star) { ?>
                                                                <i style="color: #0b93d5 !important;"
                                                                   class="fa fa-star"></i>
                                                            <?php } else { ?>
                                                                <i class="fa fa-star"></i>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <p class="course-price">
                                                            <?php echo number_format($course->price); ?><span>VND</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php $count++; ?>
                            <?php if ($count % 4 == 0 || $count == count($upCourses)) {
                                echo '</div>';
                                echo '</div>';
                            } ?>
                        <?php } ?>
                    </div>
                    <div class="pagination-listjob">
                        <?php
                        if (count($upCourses) % 4 == 0) {
                            $recommend_course_slide_dot = count($upCourses) / 4;
                        } else {
                            $recommend_course_slide_dot = ceil(count($upCourses) / 4);
                        }
                        ?>
                        <?php if ($recommend_course_slide_dot != 0) : ?>
                            <a class="carousel-control-prev course" href="#recommend-course" role="button"
                               data-slide="prev"
                               onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <ol class="carousel-indicators">
                                <?php for ($i = 0; $i < $recommend_course_slide_dot; $i++) : ?>
                                    <?php if ($i == 0) : ?>
                                        <li data-target="#recommend-course" data-slide-to="0" class="course active"
                                            onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)"></li>
                                    <?php else : ?>
                                        <li data-target="#recommend-course" data-slide-to="<?php echo $i; ?>"
                                            class="course"
                                            onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)"></li>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            </ol>
                            <a class="carousel-control-next course" href="#recommend-course" role="button"
                               data-slide="next"
                               onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        <?php else : ?>
                            <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- end recomend-course -->
        </div>


        <!-- last-slider -->
        <div class="last-slider">
            <div id="carouselExampleIndicators-lastslider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators-lastslider" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators-lastslider" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators-lastslider" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner course-popular-slider">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="/theme/oklassedu/pix/images/banner-cv.png"
                             alt="First slide">
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="/theme/oklassedu/pix/images/banner-cv.png"
                             alt="First slide">
                    </div>
                    <div class="carousel-item ">
                        <img class="d-block w-100" src="/theme/oklassedu/pix/images/banner-cv.png"
                             alt="First slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators-lastslider" role="button"
                   data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators-lastslider" role="button"
                   data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- End slider -->
    </div>

    <script>
        function paginationSlide(nameClass, numberPage) {
            if ($("li." + nameClass + ".active").attr("data-slide-to") != 0 && $("li." + nameClass + ".active").attr("data-slide-to") != (numberPage - 1)) {
                $("a." + nameClass).removeClass("arrow-hide");
                $("a." + nameClass).addClass("arrow-show");
            } else {
                if ($("li." + nameClass + ".active").attr("data-slide-to") == 0) {
                    $("a.carousel-control-prev." + nameClass).removeClass("arrow-show");
                    $("a.carousel-control-prev." + nameClass).addClass("arrow-hide");
                } else {
                    $("a.carousel-control-prev." + nameClass).removeClass("arrow-hide");
                    $("a.carousel-control-prev." + nameClass).addClass("arrow-show");
                }
                if ($("li." + nameClass + ".active").attr("data-slide-to") == (numberPage - 1)) {
                    $("a.carousel-control-next." + nameClass).removeClass("arrow-show");
                    $("a.carousel-control-next." + nameClass).addClass("arrow-hide");
                } else {
                    $("a.carousel-control-next." + nameClass).removeClass("arrow-hide");
                    $("a.carousel-control-next." + nameClass).addClass("arrow-show");
                }
            }
        }

        function resetPagination() {
            paginationSlide("course", <?php echo $recommend_course_slide_dot; ?>);
            paginationSlide("hot-job", <?php echo $hot_job_slide_dot; ?>);
            paginationSlide("new-job", <?php echo $new_job_slide_dot; ?>);
        }

        setInterval(resetPagination, 1000);

        document.querySelector('.carousel-item').classList.add('active');

        function savedJob(el, id, classon, classoff) {
            $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkjobs_saved_job",
                jobid: id
            }, function (data) {
                if (data == -1) {
                    if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $VISANG->wwwroot; ?>";
                    }
                } else if (data == 1) {
                    $(el).removeClass(classoff || "ic-heart");
                    $(el).addClass(classon || "ic-heart-on");
                    $('#numSaved_' + id).html(parseInt($('#numSaved_' + id).html()) + 1);
                } else {
                    $(el).removeClass(classon || "ic-heart-on");
                    $(el).addClass(classoff || "ic-heart");
                    $('#numSaved_' + id).html(parseInt($('#numSaved_' + id).html()) - 1);
                }
            });
        }

        <?php if (count($recommend_jobs) < 4) : ?>
        $(function () {
            setTimeout(function () {
                $('.jobs-cmpny-list .slick-list .slick-track').css('width', '100%');
            }, 1000);
        });
        <?php endif; ?>
    </script>
    <script type="text/javascript">
        $(document).ready(showPosition());

        function setDefaultSearchCategory() {
            let categoryBlock = document.querySelector(".search-category");
            let defaultCategory = 'Biên Dịch / Phiên Dịch';

            Array.prototype.forEach.call(categoryBlock.options, function (option, index) {
                if (option.innerText === defaultCategory) {
                    categoryBlock.value = option.attributes.value.value
                }
            });
        }

        function setDefaultPosition() {
            let addressiplocations = [];
            let locationSelector = document.querySelector(".search-location");
            let companylocation = 'Hà Nội';
            <?php foreach ($districts as $district) : ?>
            addressiplocations.push({id: <?php echo $district->id; ?>, name: '<?php echo $district->name; ?>'});
            <?php endforeach; ?>
            addressiplocations.forEach((addressiplocation) => {
                if (addressiplocation.name === companylocation) {
                    locationSelector.value = addressiplocation.id
                }
            })
        }

        function showPosition() {
            setDefaultSearchCategory();
            setDefaultPosition();
            let url = 'https://ipinfo.io/json';
            $.getJSON(url, function (data) {
                let addressiplocations = [];
                let namecity = '';
                let companylocation = removeVietnameseTones(data.city);
                <?php foreach ($districts as $district) : ?>
                namecity = removeVietnameseTones('<?php echo $district->name; ?>')
                addressiplocations.push({id: <?php echo $district->id; ?>, name: namecity});
                <?php endforeach; ?>
                let searchkeyworld= document.getElementById("search-keyworld");
                let categorySelector = document.querySelector(".search-category");
                let locationSelector = document.querySelector(".search-location");
                addressiplocations.forEach((addressiplocation) => {
                    if (addressiplocation.name === companylocation) {
                        locationSelector.value = addressiplocation.id
                    }
                })
                let keyworld='<?php echo !isset($_COOKIE["searchtext"])? '':$_COOKIE["searchtext"]; ?>';
                let cateid=<?php echo !isset($_COOKIE["searchcate"]) ? 71 :$_COOKIE["searchcate"]; ?>;
                let posiid;
                <?php if(!isset($_COOKIE["searchloca"])): ?>
                posiid=addressiplocation.id;
                <?php else: ?>
                posiid=<?php echo $_COOKIE["searchloca"];?>
                <?php endif; ?>


                if(cateid!==null){
                    categorySelector.value=cateid;
                }
                if(posiid!==null){
                    locationSelector.value=posiid;
                }
                if(keyworld!==''){
                    searchkeyworld.value=keyworld;
                }
            });

            function removeVietnameseTones(str) {
                str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                str = str.replace(/đ/g, "d");
                str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
                str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
                str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
                str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
                str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
                str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
                str = str.replace(/Đ/g, "D");
                // Some system encode vietnamese combining accent as individual utf-8 characters
                // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
                str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
                str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
                // Remove extra spaces
                // Bỏ các khoảng trắng liền nhau
                str = str.replace(/ + /g, " ");
                str = str.trim();
                // Remove punctuations
                str = str.toLowerCase()
                str = str.split(' ').join('');
                // Bỏ dấu câu, kí tự đặc biệt
                str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
                str = str.replace("city", "");
                str = str.replace("thanhpho", "");
                str = str.replace("tp", "");
                return str;
            }
        }

        let banner = document.querySelector('.carousel-inner.banner_slide');

        function setBannerWidth() {
            let bannerWidth = banner.offsetWidth;
            banner.style.height = `${bannerWidth * 8 / 21}px`;
        }

        setBannerWidth();
        window.addEventListener('resize', setBannerWidth);
    </script>

<?php
$dateNow = new DateTime();
$endDate    = new DateTime("08/01/2021");
if ($dateNow < $endDate) {
?>
    <style>
        .pr-popup {
            position: fixed;
            width: 100%;
            height: 100%;
            left: 0px;
            top: 0px;
            background: #1513137d;
            z-index: 1000;
        }
        .popup-content {
            margin: auto;
            background: #fff;
            margin-top: 200px;
            width: 50vw;
            height: auto;
            height: auto;
            position: relative;

        }
        @media only screen and (max-width: 600px) {
            .popup-content {
                width: 90vw;

            }
        }
        .fa-close {
            position: absolute;
            font-size: 20px;
            color: #fff;
            border-radius: 50%;
            right: -5px;
            top: -19px;
            padding: 10px;
        }
        .fa-close:before {
            display: inline-block;
            content: "\00d7";
        }
    </style>
<div class="pr-popup">
    <div class="popup-content">
        <i class="fa-close"></i>
        <a href="http://www.amnote.com/">
            <?php
                $path = '/theme/oklassedu/pix/banner/amnote.jpg';
                switch (current_language()) {
                    case 'vi' :
                        $path = '/theme/oklassedu/pix/banner/amnote-vi.jpg';
                        break;
                    case 'en' :
                        $path = '/theme/oklassedu/pix/banner/amnote-en.jpg';
                }

            ?>
            <img src="<?php echo $CFG->wwwroot . $path;?>" alt="">
        </a>
    </div>
</div>
    <?php } ?>
<?php
require_once($VISANG->dirroot . '/lib/popup_lib.php');
$VISANG->theme->footer();
