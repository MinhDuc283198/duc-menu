let mix = require("laravel-mix");

mix
  .js("src/js/app.js", "dist/js/app.js")
  .sass("src/sass/app.scss", "dist/css/app.css")
  .setPublicPath("dist")
  .options({
    processCssUrls: false
  })
  .sourceMaps();
