$(document).ready(function () {
    // notification
    $("li>a.notification").off("click").click(function () {
        var target = $(this).siblings(".alram-list");
        if (!target.is(":visible")) {
            target.slideDown(300);
            if ($(".my-wrap").length == 0) {
                $("body").append("<div class='my-wrap'></div>");
            }
            $(".my-wrap").fadeIn();
            // if (utils.popup.bodyscroll()) {
            //     //$("body").addClass("noScroll");
            // }
            $(document).on("mouseup touchend", function (e) {
                if ($(e.target).closest(".alram").length == 0 && !$(e.target).hasClass(".alram")) {
                    target.slideUp(300);
                    $(".my-wrap").fadeOut();
                    $(this).off("mouseup");
                    $(this).off("touchstart");
                    $("body").removeClass("noScroll");
                }
            });


        } else {
            target.slideUp(300);
            $(".my-wrap").fadeOut();
            $("body").removeClass("noScroll");
        }
    });

    // language
    $("#dropdownFlag").off("click").click(function () {
        var target = $(this).siblings("div.flag");
        if (!target.is(":visible")) {
            target.slideDown(300);
            if ($(".my-wrap").length == 0) {
                $("body").append("<div class='my-wrap'></div>");
            }
            $(".my-wrap").fadeIn();
            // if (utils.popup.bodyscroll()) {
            //     //$("body").addClass("noScroll");
            // }
            $(document).on("mouseup touchend", function (e) {
                if ($(e.target).closest(".alram").length == 0 && !$(e.target).hasClass(".alram")) {
                    target.slideUp(300);
                    $(".my-wrap").fadeOut();
                    $(this).off("mouseup");
                    $(this).off("touchstart");
                    $("body").removeClass("noScroll");
                }
            });


        } else {
            target.slideUp(300);
            $(".my-wrap").fadeOut();
            $("body").removeClass("noScroll");
        }
    });

    // login area
    $("#dropdownLogin").off("click").click(function () {
        var target = $(this).siblings("div.user-area");
        if (!target.is(":visible")) {
            target.slideDown(300);
            if ($(".my-wrap").length == 0) {
                $("body").append("<div class='my-wrap'></div>");
            }
            $(".my-wrap").fadeIn();
            // if (utils.popup.bodyscroll()) {
            //     //$("body").addClass("noScroll");
            // }
            $(document).on("mouseup touchend", function (e) {
                if ($(e.target).closest(".alram").length == 0 && !$(e.target).hasClass(".alram")) {
                    target.slideUp(300);
                    $(".my-wrap").fadeOut();
                    $(this).off("mouseup");
                    $(this).off("touchstart");
                    $("body").removeClass("noScroll");
                }
            });


        } else {
            target.slideUp(300);
            $(".my-wrap").fadeOut();
            $("body").removeClass("noScroll");
        }
    });
});