<?php

/**
 * Visang
 *
 */
defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $USER, $search_hashtags, $PAGE, $job_search_hashtags, $DB;

require_once($VISANG->dirroot . '/lib/common_functions.php');
if (!isset($job_search_hashtags))
    $job_search_hashtags = mkjobs_get_searchtext('jobs');

$langs = get_string_manager()->get_list_of_translations();
$currlang = current_language();

$term_category = 1;
$comid = null;
if (isloggedin()) {
    $sql = "SELECT ad.employer_id as val, em.company_name
            FROM {vi_admin_employers} ad
            JOIN {vi_employers} em ON em.id = ad.employer_id 
            WHERE ad.user_id = :id";
    $comid = $DB->get_record_sql($sql, array('id' => $USER->id));

    if ($comid != null) {
        $term_category = 2;
    }
}

$employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id 
                WHERE ad.user_id = :id";
$employer = $DB->get_record_sql($employer_sql, array('id' => $USER->id));
?>

<!-- header begin -->
<nav class="navbar navbar-expand-xl navbar-light bg-light header-custom
            sticky-top">
    <div class="notification">
        <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="all-btn"></a>
    </div>
    <a class="navbar-brand mkj-logo-header" href="<?php echo $VISANG->wwwroot; ?>">
        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/logo_not_beta_job.png'; ?>" alt="" class="mkj-logo-header">
    </a>
    <button class="navbar-toggler d-xl-none collapsed" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-xl-0 nav-left-custom
                    main-menu">
            <?php if (isloggedin()) { ?>

                <?php if ($comid == null) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/jobs/index.php'; ?>"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/jobs/jobmatching.php'; ?>"><?php echo get_string('job_matching', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $CFG->wwwroot . '/local/course/main.php'; ?>"><?php echo get_string('course', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/companies/index.php'; ?>"><?php echo get_string('Company', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/resumes/index.php'; ?>"><?php echo get_string('Resume_2', 'local_job'); ?></a>
                    </li>
                    <?php if ($employer == null) : ?>
                        <li class="nav-item last-nav">
                            <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a>
                        </li>
                    <?php else : ?>
                        <li class="nav-item last-nav">
                            <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a>
                        </li>
                    <?php endif; ?>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/jobs/index.php'; ?>"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/jobs/jobmatching.php'; ?>"><?php echo get_string('job_matching', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $CFG->wwwroot . '/local/course/main.php'; ?>"><?php echo get_string('course', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/companies/index.php'; ?>"><?php echo get_string('Company', 'local_job'); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/resumes/index.php'; ?>"><?php echo get_string('Resume_2', 'local_job'); ?></a>
                    </li>
                    <?php if ($employer == null) : ?>
                        <li class="nav-item last-nav">
                            <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a>
                        </li>
                    <?php else : ?>
                        <li class="nav-item last-nav">
                            <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a>
                        </li>
                    <?php endif; ?>
                <?php } ?>
            <?php } else { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/jobs/index.php'; ?>"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/jobs/jobmatching.php'; ?>"><?php echo get_string('job_matching', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $CFG->wwwroot . '/local/course/main.php'; ?>"><?php echo get_string('course', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/companies/index.php'; ?>"><?php echo get_string('Company', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/resumes/index.php'; ?>"><?php echo get_string('Resume_2', 'local_job'); ?></a>
                </li>
                <?php if ($employer == null) : ?>
                    <li class="nav-item last-nav">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a>
                    </li>
                <?php else : ?>
                    <li class="nav-item last-nav">
                        <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a>
                    </li>
                <?php endif; ?>
            <?php } ?>
        </ul>
        <ul class="navbar-nav my-2 my-xl-0 nav-right-custom main-menu">
            <li class="nav-item for-employers-custom">
                <a class="nav-link" href="<?php echo $VISANG->wwwroot . '/employer/dashboard.php'; ?>"><?php echo get_string('for_employer', 'local_job'); ?></a>
            </li>
            <li class="nav-item">
                <div class="vertical-line"></div>
            </li>
            <li class="nav-item flag">
                <?php if ($currlang == 'vi') { ?>
                    <a class="nav-link flag" href="#" id="dropdownFlag">
                        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>" alt="" class="flag">
                    </a>
                    <div class="flag">
                        <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'en')); ?>">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt="" class="flag">
                        </a>
                        <br>
                        <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'ko')); ?>">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt="" class="flag">
                        </a>
                    </div>
                <?php } else if ($currlang == 'en') { ?>
                    <a class="nav-link flag" href="#" id="dropdownFlag">
                        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt="" class="flag">
                    </a>
                    <div class="flag">
                        <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'vi')); ?>">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>" alt="" class="flag">
                        </a>
                        <br>
                        <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'ko')); ?>">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt="" class="flag">
                        </a>
                    </div>
                <?php } else { ?>
                    <a class="nav-link flag" href="#" id="dropdownFlag">
                        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt="" class="flag">
                    </a>
                    <div class="flag">
                        <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'vi')); ?>">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>" alt="" class="flag">
                        </a>
                        <br>
                        <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'en')); ?>">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt="" class="flag">
                        </a>
                    </div>
                <?php } ?>
            </li>
            <li class="nav-item flag-mobile">
                <a class="nav-link flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'vi')); ?>">
                    <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>" alt="" class="flag">
                </a>
                <a class="nav-link flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'en')); ?>">
                    <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt="" class="flag">
                </a>
                <a class="nav-link flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'ko')); ?>">
                    <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt="" class="flag">
                </a>
            </li>
            <?php if (isloggedin() && (strpos($_SERVER['REQUEST_URI'], "information_sharing") === false)) : ?>
                <?php if ($comid == null) { ?>
                    <?php
                    $notifications = mkjobs_get_notifications();
                    $ma_cnt = mkjobs_count_notifications();
                    ?>
                    <li class="nav-item">
                        <a class="nav-link notification" href="#">
                            <i class="fas fa-bell"></i>
                            <?php if ($ma_cnt > 0) : ?>
                                <span><?php echo ($ma_cnt < 100) ? intval($ma_cnt) : '99+'; ?></span>
                            <?php endif; ?>
                        </a>
                        <div class="alram-list">
                            <ul>
                                <?php foreach ($notifications as $message) : ?>
                                    <?php $time = mkjobs_notifi_time($message->timecreated); ?>
                                    <li>
                                        <span class="close-al"><?php echo get_string('close', 'local_mypage'); ?></span>
                                        <div><?php echo $message->smallmessage; ?></div>
                                        <p onclick="">
                                            <?php
                                            echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                            ?>
                                        </p>
                                        <span><?php echo $time; ?></span>
                                    </li>
                                <?php endforeach; ?>

                                <?php if (empty($notifications)) : ?>
                                    <div class="no-data" style="margin-bottom: 0px;"><?php echo get_string('nodata:notifi', 'local_mypage'); ?></div>
                                <?php else : ?>
                                    <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="all-btn"><?php echo get_string('Viewall', 'local_job'); ?></a>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </li>
                <?php } ?>
                <li class="nav-item login-area">
                    <?php if ($comid == null) { ?>
                        <a class="nav-link btn btn-login" href="#" id="dropdownLogin"><i class="fas
                                    fa-user"></i><span class="user-name"><?php echo $USER->firstname . ' ' . $USER->lastname; ?></span><i class="fas fa-sort-down"></i></a>
                    <?php } else { ?>
                        <a class="nav-link btn btn-login" href="#" id="dropdownLogin"><i class="fas
                                    fa-user"></i><span class="user-name"><?php echo $comid->company_name; ?></span><i class="fas fa-sort-down"></i></a>
                    <?php } ?>
                    <div class="user-area">
                        <ul>
                            <?php if (is_siteadmin()) : ?>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/management" target="_blank" class="nav-link user-dropdown"><?php echo get_string('gomanagement', 'theme_oklassedu'); ?></a></li>
                            <?php endif; ?>
                            <?php if ($comid == null) { ?>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/info.php" class="nav-link user-dropdown"><?php echo get_string('MyInfo', 'local_job'); ?></a></li>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/apply.php" class="nav-link user-dropdown"><?php echo get_string('SupportStatus', 'local_job'); ?></a></li>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/inter.php" class="nav-link user-dropdown"><?php echo get_string('Interestinformation', 'local_job'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="nav-link user-dropdown"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>" class="nav-link user-dropdown"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>" class="nav-link user-dropdown"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                <li><a href="#" onclick="mkjobs_logout()" class="nav-link user-dropdown"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></li>

                            <?php } else { ?>

                                <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>" class="nav-link user-dropdown"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>" class="nav-link user-dropdown"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>" class="nav-link user-dropdown"><?php echo get_string('credit-status-title', 'local_credit'); ?></a></li>
                                <li><a href="#" onclick="mkjobs_logout()" class="nav-link user-dropdown"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
            <?php else : ?>
                <li class="nav-item">
                    <a class="nav-link notification" href="#">
                        <i class="fas fa-bell"></i>
                        <?php if ($ma_cnt > 0) : ?>
                            <span><?php echo ($ma_cnt < 100) ? intval($ma_cnt) : '99+'; ?></span>
                        <?php endif; ?>
                    </a>
                    <div class="alram-list">
                        <ul>
                            <?php foreach ($notifications as $message) : ?>
                                <?php $time = mkjobs_notifi_time($message->timecreated); ?>
                                <li>
                                    <span class="close-al"><?php echo get_string('close', 'local_mypage'); ?></span>
                                    <div><?php echo $message->smallmessage; ?></div>
                                    <p onclick="">
                                        <?php
                                        echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                        ?>
                                    </p>
                                    <span><?php echo $time; ?></span>
                                </li>
                            <?php endforeach; ?>

                            <?php if (empty($notifications)) : ?>
                                <div class="no-data" style="margin-bottom: 0px;"><?php echo get_string('nodata:notifi', 'local_mypage'); ?></div>
                            <?php else : ?>
                                <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="all-btn"><?php echo get_string('Viewall', 'local_job'); ?></a>
                            <?php endif; ?>
                        </ul>
                    </div>
                </li>
                <li class="nav-item login-area">
                    <a class="nav-link btn btn-login" href="<?php echo $VISANG->wwwroot; ?>/auth/login.php"><i class="fas
                                    fa-user"></i><?php echo get_string('login', 'local_job'); ?><i class="fas fa-sort-down"></i></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
<!-- header end -->
<div class="header-hidden"></div>

