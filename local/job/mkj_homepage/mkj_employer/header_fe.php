<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $USER;

?>
<!DOCTYPE html>
<html lang="<?php echo current_language(); ?>">

<head>
	<?php echo get_visang_config('javascript_header') ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo get_string('sitename:mk_jobs', 'local_management'); ?></title>

	<!-- job theme -->
    <!-- mkj js custom -->

	<link href="/local/job/assets/dist/css/app.css" rel="stylesheet" />
	 <script src="/local/job/assets/dist/js/app.js" type="text/javascript"></script>

	<!-- oklassedu theme -->
	<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
	<link href="/theme/oklassedu/style/media.css" rel="stylesheet" />

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<!-- mkj_homepage-custom -->
	<link rel="stylesheet" href="/local/job/assets/dist/css/style_fe_page.css">

	<!-- jquery -->


    <script src="/local/job/assets/dist/js/mkj.js" type="text/javascript"></script>

	<?php
	foreach ($VISANG->theme->css as $css) {
		echo '<link rel="stylesheet" href="' . $css . '">';
	}
	foreach ($VISANG->theme->styles as $style) {
		echo html_writer::tag('style', $style);
	}
	?>
</head>

<body class="mk-jobs jobs-site <?php echo 'lang-' . current_language(); ?> <?php echo $VISANG->theme->bodyclasses; ?> febody">
	<?php echo get_visang_config('javascript_body') ?>