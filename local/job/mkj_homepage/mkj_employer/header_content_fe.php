<?php

/**
 * Visang
 *
 */
defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $USER, $search_hashtags, $PAGE, $job_search_hashtags, $DB;
//require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
require_once(dirname(__FILE__) . '/../../employer/lib/com_function.php');
require_once($VISANG->dirroot . '/lib/common_functions.php');
if (!isset($job_search_hashtags))
    $job_search_hashtags = mkjobs_get_searchtext('jobs');

$langs = get_string_manager()->get_list_of_translations();
$currlang = current_language();

$term_category = 1;
$comid = null;
if (isloggedin()) {
    $sql = "SELECT ad.employer_id as val, em.company_name
            FROM {vi_admin_employers} ad
            JOIN {vi_employers} em ON em.id = ad.employer_id 
            WHERE ad.user_id = :id";
    $comid = $DB->get_record_sql($sql, array('id' => $USER->id));

    if ($comid != null) {
        $term_category = 2;
    }
}

$employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id
                WHERE ad.user_id = :id";
$employer = $DB->get_record_sql($employer_sql, array('id' => $USER->id));
?>

<!-- header begin -->
<div class="header-custom">
    <nav class="navbar navbar-light navbar-expand-xl bg-faded justify-content-center menu_foremployer">
        <div class="notification">
            <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="all-btn"></a>
        </div>
        <a href="<?php echo $VISANG->wwwroot . '/employer/dashboard.php'; ?>" class="navbar-brand d-flex w-35 mr-auto img-logo"><img
                    src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/logomkj.png'; ?>" alt=""
                    class="mkj-logo-header"></a>
        <button  id="btncollapsingNavbar3" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar3" aria-expanded="false" aria-controls="collapsingNavbar3" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse w-60 order-1 order-xl-0 dual-collapse2 custom-left-menu collapsingNavbar3"
             id="collapsingNavbar3">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link"
                       href="<?php echo $VISANG->wwwroot . '/employer/job_notices.php'; ?>"><?php echo get_string('employer:menuJobs', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="<?php echo $VISANG->wwwroot . '/employer/applicants.php'; ?>"><?php echo get_string('employer:menuApply', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="<?php echo $VISANG->wwwroot . '/employer/legaltax.php';?>"><?php echo get_string('employer:menuOrder', 'local_job'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="<?php echo $VISANG->wwwroot . '/employer/info.php?step=1'; ?>"><?php echo get_string('employer:menuInfo', 'local_job'); ?></a>
                </li>
            </ul>

        </div>
        <div class="navbar-collapse collapse w-100 order-3 order-xl-0 custom-right-menu dual-collapse2 collapsingNavbar3" id="collapsingNavbar3">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item btn_pjob">
                    <a class="nav-link post_job"
                       href="<?php echo $VISANG->wwwroot . '/employer/job_notices.php' ?>">
                        <p><?php echo get_string('post_job', 'local_job'); ?></p></a>
                </li>
                <li class="nav-item btn_recadida">
                    <a class="nav-link recommened_cadidates"
                       href="<?php echo $VISANG->wwwroot . '/employer/applicants_recommend.php?'; ?>">
                        <p><?php echo get_string('recommended_cadidates', 'local_job'); ?></p></a>
                </li>
                <li class="nav-item flag">
                    <?php if ($currlang == 'vi') { ?>
                        <a class="nav-link flag" href="#" id="dropdownFlag">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>" alt=""
                                 class="flag">
                        </a>
                        <div class="flag" style="display: none; position: absolute;margin-left: 8px; background: #ffffff;">
                            <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'en')); ?>">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt=""
                                     class="flag">
                            </a>
                            <br>
                            <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'ko')); ?>">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt=""
                                     class="flag">
                            </a>
                        </div>
                    <?php } else if ($currlang == 'en') { ?>
                        <a class="nav-link flag" href="#" id="dropdownFlag">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt=""
                                 class="flag">
                        </a>
                        <div class="flag" style="display: none; position: absolute;margin-left: 8px; background: #ffffff;">
                            <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'vi')); ?>">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>"
                                     alt=""
                                     class="flag">
                            </a>
                            <br>
                            <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'ko')); ?>">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt=""
                                     class="flag">
                            </a>
                        </div>
                    <?php } else { ?>
                        <a class="nav-link flag" href="#" id="dropdownFlag">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt=""
                                 class="flag">
                        </a>
                        <div class="flag" style="display: none; position: absolute; margin-left: 8px; background: #ffffff;">
                            <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'vi')); ?>">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>"
                                     alt=""
                                     class="flag">
                            </a>
                            <br>
                            <a class="flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'en')); ?>">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt=""
                                     class="flag">
                            </a>
                        </div>
                    <?php } ?>
                </li>
                <li class="nav-item flag-mobile">
                    <a class="nav-link flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'vi')); ?>">
                        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/vietnam.png'; ?>" alt=""
                             class="flag">
                    </a>
                    <a class="nav-link flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'en')); ?>">
                        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/usa.png'; ?>" alt=""
                             class="flag">
                    </a>
                    <a class="nav-link flag" href="<?php echo new moodle_url($PAGE->url, array('lang' => 'ko')); ?>">
                        <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/korea.png'; ?>" alt=""
                             class="flag">
                    </a>
                </li>
                <li class="nav-item">
                    <div class="vertical-line"></div>
                </li>

                <?php if (isloggedin() && (strpos($_SERVER['REQUEST_URI'], "information_sharing") === false)) : ?>
                    <?php if ($comid == null) { ?>
                        <?php
                        $notifications = mkjobs_get_notifications();
                        $ma_cnt = mkjobs_count_notifications();
                        ?>
                        <li class="nav-item">
                            <a class="nav-link notification" href="#">
                                <i class="fas fa-bell"></i>
                                <?php if ($ma_cnt > 0) : ?>
                                    <span><?php echo ($ma_cnt < 100) ? intval($ma_cnt) : '99+'; ?></span>
                                <?php endif; ?>
                            </a>
                            <div class="alram-list">
                                <ul>
                                    <?php foreach ($notifications as $message) : ?>
                                        <?php $time = mkjobs_notifi_time($message->timecreated); ?>
                                        <li>
                                            <span class="close-al"><?php echo get_string('close', 'local_mypage'); ?></span>
                                            <div><?php echo $message->smallmessage; ?></div>
                                            <p onclick="">
                                                <?php
                                                echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                                ?>
                                            </p>
                                            <span><?php echo $time; ?></span>
                                        </li>
                                    <?php endforeach; ?>

                                    <?php if (empty($notifications)) : ?>
                                        <div class="no-data"
                                             style="margin-bottom: 0px;"><?php echo get_string('nodata:notifi', 'local_mypage'); ?></div>
                                    <?php else : ?>
                                        <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>"
                                           class="all-btn"><?php echo get_string('Viewall', 'local_job'); ?></a>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </li>
                    <?php } ?>
                    <li class="nav-item login-area">
                        <a class="nav-link btn btn-toggle" href="#" id="dropdownLogin"><img
                                    src="/local/job/assets/dist/images/Vector.png"></a>
                        <div class="user-area" style="display: none;">
                            <ul>
                                <li>
                                    <a class="nav-link user-dropdown" href="#"
                                       id=""><span><?php echo $comid->company_name; ?></span></a>
                                </li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>"
                                       class="nav-link user-dropdown"><?php echo get_string('qna', 'local_mypage'); ?></a>
                                </li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"
                                       class="nav-link user-dropdown"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a>
                                </li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>"
                                       class="nav-link user-dropdown"><?php echo get_string('credit-status-title', 'local_credit'); ?></a>
                                </li>
                                <li><a href="#" onclick="mkjobs_logout()"
                                       class="nav-link user-dropdown"><?php echo get_string('logout', 'theme_oklassedu'); ?></a>
                                </li>

                            </ul>
                        </div>
                    </li>
                <?php else : ?>
                    <li class="nav-item">
                        <a class="nav-link notification" href="#">
                            <i class="fas fa-bell"></i>
                            <?php if ($ma_cnt > 0) : ?>
                                <span><?php echo ($ma_cnt < 100) ? intval($ma_cnt) : '99+'; ?></span>
                            <?php endif; ?>
                        </a>
                        <div class="alram-list">
                            <ul>
                                <?php foreach ($notifications as $message) : ?>
                                    <?php $time = mkjobs_notifi_time($message->timecreated); ?>
                                    <li>
                                        <span class="close-al"><?php echo get_string('close', 'local_mypage'); ?></span>
                                        <div><?php echo $message->smallmessage; ?></div>
                                        <p onclick="">
                                            <?php
                                            echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                            ?>
                                        </p>
                                        <span><?php echo $time; ?></span>
                                    </li>
                                <?php endforeach; ?>

                                <?php if (empty($notifications)) : ?>
                                    <div class="no-data"
                                         style="margin-bottom: 0px;"><?php echo get_string('nodata:notifi', 'local_mypage'); ?></div>
                                <?php else : ?>
                                    <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>"
                                       class="all-btn"><?php echo get_string('Viewall', 'local_job'); ?></a>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item login-area">
                        <a class="nav-link btn btn-login" href="<?php echo $VISANG->wwwroot; ?>/auth/login.php"><i
                                    class="fas
                                    fa-user"></i><?php echo get_string('login', 'local_job'); ?><i
                                    class="fas fa-sort-down"></i></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
</div>

<!-- header end -->
<div style="height: 102px;"></div>



