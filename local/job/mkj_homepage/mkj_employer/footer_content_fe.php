<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $isHomePage, $DB, $USER;

require_once($VISANG->dirroot . '/lib/common_functions.php');
$footer_banners = mkjobs_get_banners('bottom');

$term_category = 1;
if (isloggedin()) {
    $sql = "SELECT ad.employer_id as val, em.company_name
            FROM {vi_admin_employers} ad
            JOIN {vi_employers} em ON em.id = ad.employer_id 
            WHERE ad.user_id = :id";
    $comid = $DB->get_record_sql($sql, array('id' => $USER->id));
    if ($comid != null) {
        $term_category = 2;
    }
}
$employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id 
                WHERE ad.user_id = :id";
$employer = $DB->get_record_sql($employer_sql, array('id' => $USER->id));
?>

    <!-- footer begin -->
    <footer>
        <div class="footer">
            <div class="ft-padding">
                <div class="ft-up">
                    <div class="row">
                        <div class="col-xl-10 col-md-9">
                            <div class="footer-logo">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo_job.png'; ?>">
                            </div>
                            <div class="row">
<!--                                <div class="col-xl-3 col-md-6">-->
<!--                                    <div class="footer-col">-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="col-xl-4 col-md-4">
                                    <div class="footer-col h5">
                                        <h5><?php echo get_string('for_candidatate', 'local_job'); ?></h5>
                                        <ul class="ft-list-link">
                                            <li>
                                                <a href="<?php echo $VISANG->wwwroot . '/resumes/index.php'; ?>"><?php echo get_string('create_resume', 'local_job'); ?></a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=12'; ?>"><?php echo get_string('career_counseling', 'local_job'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-4">
                                    <div class="footer-col h5">
                                        <h5><?php echo get_string('for_employee', 'local_job'); ?></h5>
                                        <ul class="ft-list-link">
                                            <li>
                                                <a href="<?php echo $VISANG->wwwroot . '/employer/job_notices.php?lang=ko'; ?>"><?php echo get_string('post_job', 'local_job'); ?></a>
                                            </li>
                                            <li>
                                                <a href="#"><?php echo get_string('job_matching', 'local_job'); ?></a >
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-4">
                                    <div class="footer-col h5">
                                        <h5>Mk</h5>
                                        <ul class="ft-list-link">
                                            <?php if ($employer == null) : ?>
                                                <li>
                                                    <a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=12'; ?>"><?php echo get_string('contact_us', 'local_job'); ?></a>
                                                </li>
                                            <?php else : ?>
                                                <li>
                                                    <a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=13'; ?>"><?php echo get_string('contact_us', 'local_job'); ?></a>
                                                </li>
                                            <?php endif; ?>
                                            <li>
                                                <a href="<?php echo $CFG->wwwroot . '/local/management/term/personalinfo.php?fbstatus=1&category=' . $term_category; ?>"><?php echo get_string('c_TermsofUse', 'local_job'); ?></a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=10'; ?>"><?php echo get_string('QNA3', 'local_job'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-3">
                            <div class="footer-col">
                                <div class="f-r pt-2">
                                    <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/"
                                       target="_blank" class="ic-facebook"><img
                                                src="/theme/oklassedu/pix/images/icon_facebook_white.png" alt="sns"></a>
                                    <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank"
                                       class="ic-youtube"><img src="/theme/oklassedu/pix/images/icon_youtube_white.png"
                                                               alt="youtube"></a>
                                    <a href="https://www.masterkorean.vn/" target="_blank"
                                       class="ic-learn"><?php echo get_string('KoreanCourse', 'local_job'); ?></a>
                                    <br><a id="logoCCDV" href="http://online.gov.vn/Home/WebDetails/64659"
                                           target="_blank"><img alt="" title=""
                                                                src="/theme/oklassedu/pix/images/logobctred.png"></a>
                                </div>
                                <div class="ft-app">
                                    <a href="https://play.google.com/store/apps/details?id=com.jinotech.visangedu"><img src="/theme/oklassedu/pix/images/google-play.PNG"></a>
                                    <a href=""><img src="/theme/oklassedu/pix/images/app-store.PNG"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row footer-end">
                    <div class="col-md-5  ft-cpr">
                        <p style="color: #fff"><?php echo get_string('ftcopyright','theme_oklassedu')?></p>
                    </div>
                    <div class="col-md-7 ft-address">
                        <p style="color: #fff"><?php echo get_string('ftaddress','theme_oklassedu')?></p>
                    </div>
                    <div class="col-md-5 ft-telmail">
                    </div>
                    <div class="col-md-7 ft-telmail">
                        <p style="color: #fff"><?php echo get_string('fttel','theme_oklassedu')?> | <?php echo get_string('ftmail','theme_oklassedu')?></p>
                    </div>
                </div>
            </div>
<!--            <div class="ft-padding ft-under">-->
<!--                <div>-->
<!--                    <div class="row">-->
<!--                        <div class="col-md-5">-->
<!--                            <p style="color: #fff">Copyright © VISANG Education Group Vietnam Company</p>-->
<!--                        </div>-->
<!--                        <div class="col-md-7">-->
<!--                            <p style="color: #fff">2nd Floor, FLC Landmark Tower, Le Duc Tho Street, My Dinh 2 Ward, Nam-->
<!--                                Tu Liem District, Hanoi City, Vietnam</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
    </footer>
    <!-- footer end -->


    <!-- mobile menu -->
    <ul class="m-ft-link">
        <li><a href="<?php echo $VISANG->elearning_wwwroot; ?>"
               target="_blank">비상 <?php echo get_string('korean', 'theme_oklassedu'); ?></a></li>
        <li><a href="<?php echo $VISANG->wwwroot; ?>">비상 <?php echo get_string('job', 'theme_oklassedu'); ?></a></li>
    </ul>

<?php if (isset($isHomePage) && $isHomePage) : ?>
    <?php foreach ($footer_banners as $banner) : ?>
        <div class="ft-banner z-10" <?php echo 'style="background: #' . $banner->colorcode . '"'; ?>>
            <a class="close-bn"><?php echo get_string('close', 'local_job'); ?></a>
            <a href="<?php echo $banner->linkurl; ?>" target="_blank">
                <img style="max-width: calc(100% - 4px);" class="d-md-none" src="<?php echo $banner->path_modile; ?>">
                <img style="max-width: calc(100% - 4px);" class="d-none d-md-inline-block"
                     src="<?php echo $banner->path; ?>">
            </a>
            <p class="ck-area"><label class="custom-ck" data-id="<?php echo $banner->id ?>"><input
                            type="checkbox"><span></span><?php echo get_string('offOneDay', 'local_job'); ?></label></p>
        </div>
        <script defer>
            $(document).ready(function () {
                $(".ft-banner .custom-ck").click(function () {
                    var id = $('.custom-ck').data('id');
                    setCookieBanner("bannerCookie", "done", 1);
                    $(".ft-banner").hide();
                });
            });

            function setCookieBanner(name, value, expiredays) {
                var todayDate = new Date();
                todayDate.setDate(todayDate.getDate() + expiredays);
                document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
            }

            function getCookieBanner() {
                var cookiedata = document.cookie;
                if (cookiedata.indexOf("bannerCookie=done") < 0) {
                    $(".ft-banner").show();
                } else {
                    $(".ft-banner").hide();
                }
            }

            getCookieBanner();
        </script>
    <?php endforeach; ?>
<?php endif; ?>

    <script>
        function mkjobs_logout() {
            utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/component/pop_logout_confirm.php' ?>", {
                "width": "500px",
                "height": "auto",
                "callbackFn": function () {
                }
            }, {
                //
            });
        }
    </script>

<?php
//Csedung: Add first login form to get agreement
include_once $VISANG->dirroot . '/component/firstLogin.php';
include_once $VISANG->dirroot . '/component/joinMember.php';
include_once $VISANG->dirroot . '/component/favourite.php';
?>

<?php if (isloggedin()) : ?>
    <?php
    $user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $USER->id));
    $kid = $user_korean_level->korean_level_id;
    $employer = $DB->get_record('vi_admin_employers', array('user_id' => $USER->id));
    if ((int)$kid == 0 && $employer == null) :
        ?>
        <script type="text/javascript">
            openFavourite();
        </script>
    <?php endif; ?>

<?php endif;
