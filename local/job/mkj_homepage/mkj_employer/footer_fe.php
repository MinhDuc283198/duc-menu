<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG;

foreach ($VISANG->theme->js as $js) {
    echo '<script src="' . $js . '"></script>';
}
foreach ($VISANG->theme->scripts as $script) {
    echo html_writer::tag('script', $script);
}
?>
<!-- oklassedu theme -->
<script src="/theme/oklassedu/javascript/common.js" type="text/javascript"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



<?php echo get_visang_config('javascript_footer') ?>
</body>

</html>