<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);

$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');
// =====================================================================================================
// renders
$VISANG->theme->title = get_string('notification', 'local_mypage');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'notification';
$VISANG->theme->header();

$thistime = time();
$type = optional_param('type', 3, PARAM_INT);

if ($type == 1) { // mk 알림만
    $notifications = local_mypage_get_message($USER->id);
} else if ($type == 2) { // mk jobs 알림만
    $notifications = local_mypage_get_message($USER->id);
} else if ($type == 3) {
    $notifications = local_mypage_get_message($USER->id);
}

?>
<?php $sql = "SELECT ad.employer_id as val, em.company_name
                            FROM {vi_admin_employers} ad
                            JOIN {vi_employers} em ON em.id = ad.employer_id 
                            WHERE ad.user_id = :id";
                            $comid = $DB->get_record_sql($sql, array('id' => $USER->id));?>
<script src="<?php echo $CFG->wwwroot ?>/local/job/my/notification.js"></script>

<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <h2 class="pg-tit"> <?php echo get_string('notification', 'local_mypage'); ?></h2>
            <?php if($comid == null ){?>
            <ul class="bar-list f-l">
                <li <?php if ($type == 3) {
                        echo 'class="on"';
                    } ?>><a href="/local/job/my/notification.php?type=3"><?php echo get_string('All', 'local_job'); ?></a></li>
                <li <?php if ($type == 1) {
                        echo 'class="on"';
                    } ?>><a href="/local/job/my/notification.php?type=1">MK</a></li>
                <li <?php if ($type == 2) {
                        echo 'class="on"';
                    } ?>><a href="/local/job/my/notification.php?type=2">MK Jobs</a></li>
            </ul>
            <?php }?>
            <div class="alram-right">
                <input type="button" class="btns br" value="<?php echo get_string('alldel', 'local_mypage'); ?>" onclick='message_delete(0,0,<?php echo $USER->id; ?>,"<?php echo get_string('delallnoti', 'local_mypage'); ?>")' />
            </div>

            <ul class="alram-list">
                <?php
                if (!empty($notifications)) {
                    foreach ($notifications as $message) {
                        $time = notifi_time($message->timecreated);
                ?>
                        <li class="noti<?php echo $message->id; ?> <?php echo $message->timeread ? 'read' : ''; ?>">
                            <div class="tit" onclick="message_read(<?php echo $message->id; ?>,<?php echo $USER->id ?>)">[mk] <?php echo $message->smallmessage; ?></div>
                            <p><?php
                                echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                ?></p>
                            <p class="tm"><?php echo $time; ?></p>
                            <a href="#" class="al-close" onclick='message_delete(<?php echo $message->id; ?>,<?php echo $message->timeread; ?>,<?php echo $USER->id ?>,"<?php echo get_string('delnoti', 'local_mypage'); ?>")'> <?php echo get_string('close', 'local_mypage'); ?></a>
                        </li>
                    <?php
                    }
                } else {
                    ?>
                    <div class="no-data">
                        <?php echo get_string('nodata:notifi', 'local_mypage'); ?>
                    </div>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<?php
$VISANG->theme->footer();
?>
<script>



</script>