<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles

$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

// =====================================================================================================
// renders
$VISANG->theme->title = get_string('ConsultationHistory', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'inq';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <div role="main">
                <h2 class="pg-tit"><?php echo get_string('ConsultationHistory', 'local_job'); ?></h2>
                <div class="alram-right">
                    <input type="button" class="btns br" value="글 작성">
                </div>
                <table class="table m-block mg-bt50">
                    <thead>
                        <tr>
                            <th><?php echo get_string('number', 'local_job'); ?></th>
                            <th><?php echo get_string('title', 'local_job'); ?></th>
                            <th><?php echo get_string('Writer', 'local_job'); ?></th>
                            <th><?php echo get_string('DateCreated', 'local_job'); ?></th>
                            <th><?php echo get_string('views', 'local_job'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5">
                                <div class="no-data style02">
                                    <div><?php echo get_string('Noconsultation', 'local_job'); ?></div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
$VISANG->theme->footer();
