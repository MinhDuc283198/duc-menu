<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles

$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

// =====================================================================================================
// renders
$VISANG->theme->title = get_string('notice', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'push';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <div role="main">
                <h2 class="pg-tit"><?php echo get_string('notice', 'local_job'); ?></h2>
                <div class="alram-right">
                    <input type="button" class="btns br" value="전체삭제">
                </div>
                <ul class="alram-list">
                    <li class="noti53 read">
                        <div class="tit">채용공고 추천</div>
                        <p>[비상교육] 이러닝 마케팅팀 채용(마케팅 및 기획 업무) | 지원 마감 : ~10/23</p>
                        <p class="tm">43분 전</p>
                        <a href="#" class="al-close">닫기</a>
                    </li>
                    <li class="noti53 read">
                        <div class="tit">채용공고 추천</div>
                        <p>[비상교육] 이러닝 마케팅팀 채용(마케팅 및 기획 업무) | 지원 마감 : ~10/23</p>
                        <p class="tm">43분 전</p>
                        <a href="#" class="al-close">닫기</a>
                    </li>
                    <li class="noti53 read">
                        <div class="tit">채용공고 추천</div>
                        <p>[비상교육] 이러닝 마케팅팀 채용(마케팅 및 기획 업무) | 지원 마감 : ~10/23</p>
                        <p class="tm">43분 전</p>
                        <a href="#" class="al-close">닫기</a>
                    </li>
                    <li class="noti53 read">
                        <div class="tit">채용공고 추천</div>
                        <p>[비상교육] 이러닝 마케팅팀 채용(마케팅 및 기획 업무) | 지원 마감 : ~10/23</p>
                        <p class="tm">43분 전</p>
                        <a href="#" class="al-close">닫기</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
$VISANG->theme->footer();
