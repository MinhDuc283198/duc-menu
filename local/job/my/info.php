<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
$baseurl = new moodle_url($VISANG->wwwroot . '/my/info.php', array());

$user = $DB->get_record('user', array('id' => $USER->id));
$lmsdata_user_query = 'select u.id, lu.id as lu_id, lu.address, lu.address_detail, lu.email2, lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
$lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $user->id));
// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->addCSS($CFG->wwwroot . '/local/job/assets/dist/css/employee.css');

$VISANG->theme->title = get_string('MyInfo', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'info';
$avatar_url=$DB->get_record('avatar_user',array('user_id'=>$user->id));
$VISANG->theme->header();
?>
    <div class="homepage-for-employee">
        <section class="employee-info">
            <div class="row">
                <div class="col-xl-3 info-left">
                    <div class="row">
                        <div class="col-xl-12 col-md-5">
                            <?php if($avatar_url==false) :?>
                                <img src="\theme\oklassedu\pix\avtdef.jpg" alt="Image of Employee">
                            <?php else: ?>
                                <img src="<?php echo $CFG->wwwroot . '/pluginfile.php/'.$avatar_url->url;?>" alt="Image of Employee">
                            <?php endif; ?>

                        </div>
                        <div class="col-xl-12 col-md-5">
                            <p class="full-name"><?php echo $user->firstname; ?></p>
                            <p><b><?php echo get_string('Joined', 'local_job'); ?>
                                    :</b> <?php echo date('Y\.m\.d', $user->timecreated); ?></p>
                            <p><b><?php echo get_string('e-mail', 'local_job'); ?>
                                    :</b> <?php echo $lmsdata_user->email2; ?></p>
                            <p><b><?php echo get_string('PhoneNumber', 'local_job'); ?>
                                    :</b> <?php echo $user->phone1; ?></p>
                        </div>
                        <div class="col-xl-12 col-md-2 view-cv">
                            <a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php'; ?>"><?php echo get_string('my:info:resumes', 'local_job'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 info-right">
                    <div class="row">
                        <div class="col-md-6 info-detail-area left">
                            <div class="row">
                                <div class="col-12 info-title">
                                    <div><?php echo get_string('current-info', 'local_job'); ?></div>
<!--                                    <div class="title-info-btn"></div>-->
                                </div>
                                <div class="col-12 info-detail">
                                    <div class="sub-tit">
                                        <span><strong><?php echo $user->firstname; ?></strong></span>
                                        <a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"
                                           class="btns br f-r"><?php echo get_string('PersonalInformationModification', 'local_job'); ?></a>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('Joined', 'local_job'); ?></strong>
                                        <span class="t-blue02"><?php echo date('Y\.m\.d', $user->timecreated); ?></span>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('ID', 'local_job'); ?></strong>
                                        <p>
                                            <?php
                                            $icon = 'icon_mail.png';
                                            if ($user->auth == 'facebook') {
                                                $icon = 'icon_sns.png';
                                            }
                                            ?>
                                            <img src="/theme/oklassedu/pix/images/<?php echo $icon ?>"
                                                 alt="<?php echo $user->auth ?>"/>
                                            <span><?php echo $user->username ?></span>
                                        </p>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('e-mail', 'local_job'); ?></strong>
                                        <p><strong><?php echo $lmsdata_user->email2; ?></strong></p>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('PhoneNumber', 'local_job'); ?></strong>
                                        <p><strong><?php echo $user->phone1; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 info-detail-area right">
                            <div class="row">
                                <div class="col-12 info-title">
                                    <div><?php echo get_string('myinfo:Interestinformation', 'local_job'); ?></div>
<!--                                    <div class="title-info-btn"></div>-->
                                </div>
                                <div class="col-12 info-detail">
                                    <div class="interest-information">
                                        <div class="rw">
                                            <strong><?php echo get_string('my_inter:final_education', 'local_job'); ?></strong>
                                            <p>
                                                <?php
                                                $sql = "SELECT w.name_ko, w.name_en, w.name_vi
                                                                        FROM {vi_user_final_educations} u
                                                                        JOIN {vi_final_educations} w ON w.id=u.final_education_id AND u.user_id=:userid";
                                                $final_educations = $DB->get_record_sql($sql, array('userid' => $USER->id));
                                                switch (current_language()) {
                                                    case 'ko':
                                                        echo $final_educations->name_ko;
                                                        break;
                                                    case 'en':
                                                        echo $final_educations->name_en;
                                                        break;
                                                    case 'vi':
                                                        echo $final_educations->name_vi;
                                                        break;
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <a href="<?php echo $VISANG->wwwroot . '/my/inter.php'; ?>"
                                           class="btns br f-r"><?php echo get_string('EditInterestInfo', 'local_job'); ?></a>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('my_inter:work_experience', 'local_job'); ?></strong>
                                        <p>
                                            <?php
                                            $lang = current_language();
                                            $sql = "SELECT w.name, w.name_en, w.name_vi
                                                                        FROM {vi_user_work_experiences} u
                                                                        JOIN {vi_work_experiences} w ON w.id=u.work_experience_id AND u.user_id=:userid";
                                            $work_experience = $DB->get_record_sql($sql, array('userid' => $user->id));
                                            switch (current_language()) {
                                                case 'ko':
                                                    echo $work_experience->name;
                                                    break;
                                                case 'en':
                                                    echo $work_experience->name_en;
                                                    break;
                                                case 'vi':
                                                    echo $work_experience->name_vi;
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('my_inter:skill_group', 'local_job'); ?></strong>
                                        <p>
                                            <?php
                                            $groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
                                            $skill_groups = array();
                                            foreach ($groups as $group) {
                                                $skill_groups[] = $group->name;
                                            }
                                            echo implode(", ", $skill_groups);
                                            ?>
                                        </p>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('my_inter:skill', 'local_job'); ?></strong>
                                        <p>
                                            <?php
                                            $skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
                                            $user_skills = array();
                                            foreach ($skills as $skill) {
                                                $user_skills[] = $skill->name;
                                            }
                                            echo implode(", ", $user_skills);
                                            ?>
                                        </p>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('my_inter:workplace', 'local_job'); ?></strong>
                                        <p>
                                            <?php
                                            $districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $user->id));
                                            $workplaces = array();
                                            foreach ($districts as $district) {
                                                $workplaces[] = $district->name;
                                            }
                                            echo implode(", ", $workplaces);
                                            ?>
                                        </p>
                                    </div>
                                    <div class="rw">
                                        <strong><?php echo get_string('my_inter:korean_level', 'local_job'); ?></strong>
                                        <p>
                                            <?php
                                            $sql = "SELECT l.*
                                                                        FROM {vi_user_korean_levels} u
                                                                        JOIN {vi_korean_levels} l ON l.id=u.korean_level_id AND u.user_id=:userid";
                                            $korean_level = $DB->get_record_sql($sql, array('userid' => $user->id));
                                            switch (current_language()) {
                                                case 'ko':
                                                    echo $korean_level->name_ko;
                                                    break;
                                                case 'en':
                                                    echo $korean_level->name_en;
                                                    break;
                                                case 'vi':
                                                    echo $korean_level->name_vi;
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        require_once($VISANG->dirroot . '/lib/jobs.php');
                        require_once($VISANG->dirroot . '/lib/employers.php');

                        $applications_count = $DB->count_records_select('vi_job_applications', 'user_id=:userid', array('userid' => $user->id), 'COUNT(*)');
                        $saved_jobs = mkjobs_get_saved_jobs($USER->id);
                        $employers = mkjobs_get_follow_employers($USER->id);
                        ?>
                        <div class="col-lg-4 col-md-6">
                            <div class="statistical">
                                <div class="border-quantity"><span><?php echo $employers['total']; ?></span></div>
                                <div class="statistical-detail">
                                    <img src="/theme/oklassedu/pix/images/viewed.png" alt="">
                                    <p><?php echo get_string('my:info:num_3', 'local_job'); ?></p>
                                    <a href="<?php echo $VISANG->wwwroot . '/my/inter_com.php'; ?>"><?php echo get_string('check-now', 'local_job'); ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="statistical">
                                <div class="border-quantity"><span><?php echo $saved_jobs['total']; ?></span></div>
                                <div class="statistical-detail">
                                    <img src="/theme/oklassedu/pix/images/saved-job.png" alt="">
                                    <p><?php echo get_string('my:info:num_2', 'local_job'); ?></p>
                                    <a href="<?php echo $VISANG->wwwroot . '/my/inter_jobs.php'; ?>"><?php echo get_string('check-now', 'local_job'); ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="statistical">
                                <div class="border-quantity"><span><?php echo $applications_count; ?></span></div>
                                <div class="statistical-detail">
                                    <img src="/theme/oklassedu/pix/images/apply-job.png" alt="">
                                    <p><?php echo get_string('my:info:num_1', 'local_job'); ?></p>
                                    <a href="<?php echo $VISANG->wwwroot . '/my/apply.php'; ?>"><?php echo get_string('check-now', 'local_job'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="employee-expand">
            <div class="row">
                <div class="col-xl-3 col-md-4 employee-dashboard">
                    <div class="employee-dashboard-border">
                        <a class="nav-link dashboard-btn" href="#submenu1" data-toggle="collapse"
                           data-target="#submenu1"
                           aria-expanded="true"
                           onclick="changeIcon()"><?php echo get_string('my-dashboard', 'local_job'); ?></a>
                        <div class="icon-collapse"></div>
                        <ul class="list-unstyled flex-column pl-3 collapse show link-collapse" id="submenu1"
                            aria-expanded="false">
                            <li class="nav-item"><a class="nav-link"
                                                    href="#"><?php echo get_string('new-team-cv', 'local_job'); ?></a>
                            </li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="#"><?php echo get_string('update-profile', 'local_job'); ?></a>
                            </li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="#"><?php echo get_string('my-jobs', 'local_job'); ?></a></li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=12'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a></li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="#"><?php echo get_string('setting', 'local_job'); ?></a></li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="#" onclick="mkjobs_logout()"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-9 col-md-8">
                    <?php
                    require($VISANG->dirroot . '/component/courses_jobs_recommend_group.php');
                    ?>

                    <section class="online-profile">
                        <div class="row">
                            <div class="inner-online-profile">
                                <div class="profile-title">
                                    <p><?php echo get_string('online-profile', 'local_job'); ?></p>
                                </div>

                                <div class="profile-body">
                                    <div class="row">
                                        <div class="col-xl-3 col-md-6">
                                            <div class="profile-image">
                                                <?php if($avatar_url==false) :?>
                                                    <img src="\theme\oklassedu\pix\avtdef.jpg" alt="Image of user's profile">
                                                <?php else: ?>
                                                    <img src="<?php echo $CFG->wwwroot . '/pluginfile.php/'.$avatar_url->url;?>" alt="Image of user's profile">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-xl-9 col-md-6 profile-detail">
                                            <div class="row">
                                                <div class="col-xl-6 col-md-12 col-sm-6">
                                                    <p class="update-at"><?php echo get_string('update-at', 'local_job'); ?>
                                                        : <span>26/6/2021</span><i
                                                                class="far fa-sync-alt icon-profile"
                                                                style="color: #93D630; padding-left: 3vw;"></i>
                                                    </p>
                                                    <hr class="left">

                                                    <a href="#"><?php echo get_string('quick-view', 'local_job'); ?><i
                                                                class="fal fa-eye icon-profile"
                                                                style="color: #93D630; padding-left: 3vw;"></i></a>
                                                    <hr class="left">

                                                    <a href="#"><?php echo get_string('download-file', 'local_job'); ?>
                                                        <i class="fas fa-download icon-profile"
                                                           style="color: #93D630; padding-left: 3vw;"></i></a>
                                                    <hr class="left">
                                                </div>
                                                <div class="col-xl-6 col-md-12 col-sm-6">
                                                    <div class="profile-detail-right">
                                                        <p class="update-at"
                                                           style="display: inline-block; margin: 0; padding-left: 3.75vw;">
                                                            <?php echo get_string('searchchable', 'local_job'); ?></p>
                                                        <div style="float: right">
                                                            <label class="switch">
                                                                <input type="checkbox">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        <hr class="right">

                                                        <p class="update-at"
                                                           style="display: inline-block; margin: 0; padding-left: 3.75vw;">
                                                            <?php echo get_string('job-alert', 'local_job'); ?></p>
                                                        <div style="float: right">
                                                            <label class="switch">
                                                                <input type="checkbox" <?php echo $user->isreceivejob==1? 'checked': '' ;?> id="job-alert">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        <hr class="right">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <p class="complete-level"><?php echo get_string('complete-level', 'local_job'); ?>
                                                    : In complete</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile-update-btn">
                                    <a class="btn btn-primary" href="#"
                                       role="button"><?php echo get_string('update-btn', 'local_job'); ?></a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="online-profile">
                        <div class="row">
                            <div class="inner-online-profile">
                                <div class="profile-title">
                                    <p><?php echo get_string('upload-profile', 'local_job'); ?></p>
                                </div>

                                <div class="profile-body">
                                    <div class="row">
                                        <div class="col-xl-3 col-md-6">
                                            <div class="profile-image">
                                                <img src="/theme/oklassedu/pix/images/available-profile.png"
                                                     alt="Image of available profile">
                                            </div>
                                        </div>
                                        <div class="col-xl-9 col-md-6 profile-detail">
                                            <div class="row">
                                                <div class="col-xl-6 col-md-12 col-sm-6">
                                                    <p class="update-at"><?php echo get_string('update-at', 'local_job'); ?>
                                                        : <span>26/6/2021</span><i
                                                                class="far fa-sync-alt icon-profile"
                                                                style="color: #93D630; padding-left: 3vw;"></i>
                                                    </p>
                                                    <hr class="left">

                                                    <a href="#"><?php echo get_string('quick-view', 'local_job'); ?><i
                                                                class="fal fa-eye icon-profile"
                                                                style="color: #93D630; padding-left: 3vw;"></i></a>
                                                    <hr class="left">

                                                    <a href="#"><?php echo get_string('download-file', 'local_job'); ?>
                                                        <i class="fas fa-download icon-profile"
                                                           style="color: #93D630; padding-left: 3vw;"></i></a>
                                                    <hr class="left">
                                                </div>
                                                <div class="col-xl-6 col-md-12 col-sm-6">
                                                    <div class="profile-detail-right">
                                                        <p class="update-at"
                                                           style="display: inline-block; margin: 0; padding-left: 3.75vw;">
                                                            <?php echo get_string('searchchable', 'local_job'); ?></p>
                                                        <div style="float: right">
                                                            <label class="switch">
                                                                <input type="checkbox">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        <hr class="right">

                                                        <p class="update-at"
                                                           style="display: inline-block; margin: 0; padding-left: 3.75vw;">
                                                            <?php echo get_string('job-alert', 'local_job'); ?></p>
                                                        <div style="float: right">
                                                            <label class="switch">
                                                                <input type="checkbox">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        <hr class="right">

                                                        <p class="update-at"
                                                           style="display: inline-block; margin: 0; padding-left: 3.75vw;">
                                                            <?php echo get_string('show-multi-file', 'local_job'); ?></p>
                                                        <div style="float: right">
                                                            <label class="switch">
                                                                <input type="checkbox">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        <hr class="right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile-update-btn">
                                    <a class="btn btn-primary" href="#"
                                       role="button"><?php echo get_string('upload-btn', 'local_job'); ?></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>

    <script>
        function changeIcon() {
            if ($("a.dashboard-btn").attr("aria-expanded") == "false") {
                $("div.icon-collapse").removeClass("icon-hide");
                $("div.icon-collapse").addClass("icon-show");
            } else {
                $("div.icon-collapse").removeClass("icon-show");
                $("div.icon-collapse").addClass("icon-hide");
            }
        }
    </script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var checkbox = document.getElementById("job-alert");
        checkbox.addEventListener('change', function () {
            if (checkbox.checked) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo $VISANG->wwwroot . '/my/job_alert.php?userid='.$user->id;?>',
                    data: 'value='+1,
                    success: function() {
                        alert('You\'ve enabled notifications received new job')
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: '<?php echo $VISANG->wwwroot . '/my/job_alert.php?userid='.$user->id;?>',
                    data:'value='+0,
                    success: function() {
                        alert('You\'ve unenabled notifications received new job')
                    }
                });
            }
        });
    });
</script>
    <!--<style>-->
    <!--    .btns[disabled] {-->
    <!--        opacity: .3;-->
    <!--    }-->
    <!--</style>-->
    <!--<div class="cont">-->
    <!--    <div class="group">-->
    <!--        <div class="crs-left-block">-->
    <!--            --><?php //include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
    <!--        </div>-->
    <!--        <div class="crs-right-block">-->
    <!--            <div class="my-box imprt jobs">-->
    <!--                <div class="half">-->
    <!--                    <div class="sub-tit">-->
    <!--                        <span>--><?php //echo $user->firstname; ?><!-- </span>-->
    <!--                        <a href="--><?php //echo $VISANG->wwwroot . '/my/edit.php'; ?><!--" class="btns br f-r">--><?php //echo get_string('PersonalInformationModification', 'local_job'); ?><!--</a>-->
    <!--                    </div>-->
    <!--                    <div class="rw">-->
    <!--                        <strong>--><?php //echo get_string('Joined', 'local_job'); ?><!--</strong>-->
    <!--                        <span class="t-blue02">--><?php //echo date('Y\.m\.d', $user->timecreated); ?><!--</span>-->
    <!--                    </div>-->
    <!--                    <div class="rw">-->
    <!--                        <strong>--><?php //echo get_string('ID', 'local_job'); ?><!--</strong>-->
    <!--                        <p>-->
    <!--                            --><?php
//                            $icon = 'icon_mail.png';
//                            if ($user->auth == 'facebook') {
//                                $icon = 'icon_sns.png';
//                            }
//                            ?>
    <!--                            <img src="/theme/oklassedu/pix/images/--><?php //echo $icon ?><!--" alt="--><?php //echo $user->auth ?><!--" />-->
    <!--                            <span>--><?php //echo $user->username ?><!--</span>-->
    <!--                        </p>-->
    <!--                    </div>-->
    <!--                    <div class="rw">-->
    <!--                        <strong>--><?php //echo get_string('e-mail', 'local_job'); ?><!--</strong>-->
    <!--                        <p><strong>--><?php //echo $lmsdata_user->email2; ?><!--</strong></p>-->
    <!--                    </div>-->
    <!--                    <div class="rw">-->
    <!--                        <strong>--><?php //echo get_string('PhoneNumber', 'local_job'); ?><!--</strong>-->
    <!--                        <p><strong>--><?php //echo $user->phone1; ?><!--</strong></p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="half">-->
    <!--                    <div class="rw">-->
    <!--                        <a href="--><?php //echo $VISANG->wwwroot . '/my/inter.php'; ?><!--" class="btns br f-r">--><?php //echo get_string('EditInterestInfo', 'local_job'); ?><!--</a>-->
    <!--                        <strong class="t-black">--><?php //echo get_string('myinfo:Interestinformation', 'local_job'); ?><!--</strong>-->
    <!--                    </div>-->
    <!--                    <div class="rw">-->
    <!--                <strong>--><?php //echo get_string('my_inter:final_education', 'local_job'); ?><!--</strong>-->
    <!--                <p>-->
    <!--                    --><?php
//                    $sql = "SELECT w.name_ko, w.name_en, w.name_vi
//                                FROM {vi_user_final_educations} u
//                                JOIN {vi_final_educations} w ON w.id=u.final_education_id AND u.user_id=:userid";
//                    $final_educations = $DB->get_record_sql($sql, array('userid' => $USER->id));
//                    switch (current_language()) {
//                        case 'ko':
//                            echo $final_educations->name_ko;
//                            break;
//                        case 'en':
//                            echo $final_educations->name_en;
//                            break;
//                        case 'vi':
//                            echo $final_educations->name_vi;
//                            break;
//                    }
//                    ?>
    <!--                </p>-->
    <!--            </div>-->
    <!---->
    <!--            <div class="rw">-->
    <!--                <strong>--><?php //echo get_string('my_inter:work_experience', 'local_job'); ?><!--</strong>-->
    <!--                <p>-->
    <!--                    --><?php
//                    $lang = current_language();
//                    $sql = "SELECT w.name, w.name_en, w.name_vi
//                                FROM {vi_user_work_experiences} u
//                                JOIN {vi_work_experiences} w ON w.id=u.work_experience_id AND u.user_id=:userid";
//                    $work_experience = $DB->get_record_sql($sql, array('userid' => $user->id));
//                    switch (current_language()) {
//                        case 'ko':
//                            echo $work_experience->name;
//                            break;
//                        case 'en':
//                            echo $work_experience->name_en;
//                            break;
//                        case 'vi':
//                            echo $work_experience->name_vi;
//                            break;
//                    }
//                    ?>
    <!--                </p>-->
    <!--            </div>-->
    <!--            <div class="rw">-->
    <!--                <strong>--><?php //echo get_string('my_inter:skill_group', 'local_job'); ?><!--</strong>-->
    <!--                <p>-->
    <!--                    --><?php
//                    $groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
//                    $skill_groups = array();
//                    foreach ($groups as $group) {
//                        $skill_groups[] = $group->name;
//                    }
//                    echo implode(", ", $skill_groups);
//                    ?>
    <!--                </p>-->
    <!--            </div>-->
    <!--            <div class="rw">-->
    <!--                <strong>--><?php //echo get_string('my_inter:skill', 'local_job'); ?><!--</strong>-->
    <!--                <p>-->
    <!--                    --><?php
//                    $skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
//                    $user_skills = array();
//                    foreach ($skills as $skill) {
//                        $user_skills[] = $skill->name;
//                    }
//                    echo implode(", ", $user_skills);
//                    ?>
    <!--                </p>-->
    <!--            </div>-->
    <!--            <div class="rw">-->
    <!--                <strong>--><?php //echo get_string('my_inter:workplace', 'local_job'); ?><!--</strong>-->
    <!--                <p>-->
    <!--                    --><?php
//                    $districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $user->id));
//                    $workplaces = array();
//                    foreach ($districts as $district) {
//                        $workplaces[] = $district->name;
//                    }
//                    echo implode(", ", $workplaces);
//                    ?>
    <!--                </p>-->
    <!--            </div>-->
    <!--            <div class="rw">-->
    <!--                <strong>--><?php //echo get_string('my_inter:korean_level', 'local_job'); ?><!--</strong>-->
    <!--                <p>-->
    <!--                    --><?php
//                    $sql = "SELECT l.*
//                                FROM {vi_user_korean_levels} u
//                                JOIN {vi_korean_levels} l ON l.id=u.korean_level_id AND u.user_id=:userid";
//                    $korean_level = $DB->get_record_sql($sql, array('userid' => $user->id));
//                    switch (current_language()) {
//                        case 'ko':
//                            echo $korean_level->name_ko;
//                            break;
//                        case 'en':
//                            echo $korean_level->name_en;
//                            break;
//                        case 'vi':
//                            echo $korean_level->name_vi;
//                            break;
//                    }
//                    ?>
    <!--                </p>-->
    <!--            </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!--            --><?php
//            require($VISANG->dirroot . '/component/courses_jobs_recommend_group.php');
//            ?>
    <!---->
    <!--            <ul class="my-c-info-num">-->
    <!--                --><?php
//                require_once($VISANG->dirroot . '/lib/jobs.php');
//                require_once($VISANG->dirroot . '/lib/employers.php');
//
//                $applications_count = $DB->count_records_select('vi_job_applications', 'user_id=:userid', array('userid' => $user->id), 'COUNT(*)');
//                $saved_jobs = mkjobs_get_saved_jobs($USER->id);
//                $employers = mkjobs_get_follow_employers($USER->id);
//                ?>
    <!--                <li><a href="--><?php //echo $VISANG->wwwroot . '/my/apply.php'; ?><!--">--><?php //echo $applications_count; ?><!--</a><span>--><?php //echo get_string('my:info:num_1', 'local_job'); ?><!--</span></li>-->
    <!--                <li><a href="--><?php //echo $VISANG->wwwroot . '/my/inter_jobs.php'; ?><!--">--><?php //echo $saved_jobs['total']; ?><!--</a><span>--><?php //echo get_string('my:info:num_2', 'local_job'); ?><!--</span></li>-->
    <!--                <li><a href="--><?php //echo $VISANG->wwwroot . '/my/inter_com.php'; ?><!--">--><?php //echo $employers['total']; ?><!--</a><span>--><?php //echo get_string('my:info:num_3', 'local_job'); ?><!--</span></li>-->
    <!--            </ul>-->
    <!---->
    <!--            <div class="resume-bx">-->
    <!--                <div class="tit">--><?php //echo get_string('myinfo:ResumeStatus', 'local_job'); ?><!-- <a href="--><?php //echo $VISANG->wwwroot . '/resumes/manage.php'; ?><!--" class="btns f-r">--><?php //echo get_string('my:info:resumes', 'local_job'); ?><!--</a></div>-->
    <!--                <ul>-->
    <!--                    <li>-->
    <!--                        <span class="dt">--><?php //echo get_string('myinfo:Basicresume', 'local_job'); ?><!--</span>-->
    <!--                        --><?php
//                        $default_resume = $DB->get_record('vi_resumes', array('user_id' => $user->id, 'is_default' => 1));
//                        ?>
    <!--                        <strong>--><?php //echo $default_resume->title; ?><!--</strong>-->
    <!--                    </li>-->
    <!--                    <li>-->
    <!--                        <span class="ic-file">--><?php //echo get_string('Attachments', 'local_job'); ?><!--: </span>-->
    <!--                        --><?php
//                        $file_count = $DB->count_records_select('vi_resumes', 'user_id=:userid AND type=:type', array('userid' => $user->id, 'type' => 'file'), 'COUNT(*)');
//                        ?>
    <!--                        <strong>--><?php //echo intval($file_count); ?><!----><?php //echo get_string('myinfo:piece', 'local_job'); ?><!--</strong>-->
    <!--                    </li>-->
    <!--                    <li>-->
    <!--                        <span class="dt">--><?php //echo get_string('Completed', 'local_job'); ?><!--</span>-->
    <!--                        --><?php
//                        $published_count = $DB->count_records_select('vi_resumes', 'user_id=:userid AND type=:type AND draft=:draft', array('userid' => $user->id, 'type' => 'resume', 'draft' => 0), 'COUNT(*)');
//                        ?>
    <!--                        <strong>--><?php //echo intval($published_count); ?><!----><?php //echo get_string('myinfo:piece', 'local_job'); ?><!--</strong>-->
    <!--                    </li>-->
    <!--                    <li>-->
    <!--                        <span class="ic-video">--><?php //echo get_string('Videoprofile', 'local_job'); ?><!-- :</span>-->
    <!--                        --><?php
//                        $video_count = $DB->count_records_select('vi_resumes', 'user_id=:userid AND type=:type', array('userid' => $user->id, 'type' => 'video'), 'COUNT(*)');
//                        ?>
    <!--                        <strong>--><?php //echo intval($video_count); ?><!----><?php //echo get_string('myinfo:piece', 'local_job'); ?><!--</strong>-->
    <!--                    </li>-->
    <!--                    <li>-->
    <!--                        <span class="dt">--><?php //echo get_string('Creating', 'local_job'); ?><!--</span>-->
    <!--                        --><?php
//                        $unpublished_count = $DB->count_records_select('vi_resumes', 'user_id=:userid AND type=:type AND draft=:draft', array('userid' => $user->id, 'type' => 'resume', 'draft' => 1), 'COUNT(*)');
//                        ?>
    <!--                        <strong>--><?php //echo intval($unpublished_count); ?><!----><?php //echo get_string('myinfo:piece', 'local_job'); ?><!--</strong>-->
    <!--                    </li>-->
    <!--                </ul>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
<?php
$VISANG->theme->footer();
