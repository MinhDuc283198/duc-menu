<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');

$view = optional_param('view', '', PARAM_RAW);

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles

$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

// =====================================================================================================
// renders
$VISANG->theme->title = get_string('LikeJob', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'inter';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <div role="main">
                <h2 class="pg-tit"><?php echo get_string('Interestinformation', 'local_job'); ?></h2>
                <ul class="mk-c-tab tab-event">
                    <li class="w-30 on"><a href="<?php echo $VISANG->wwwroot . '/my/inter_jobs.php'; ?>"><?php echo get_string('LikeJob', 'local_job'); ?></a></li>
                    <li class="w-30"><a href="<?php echo $VISANG->wwwroot . '/my/inter_com.php'; ?>"><?php echo get_string('companyInInterested', 'local_job'); ?></a></li>
                    <li class="w-30"><a href="<?php echo $VISANG->wwwroot . '/my/inter.php'; ?>"><?php echo get_string('InterestInfo', 'local_job'); ?></a></li>
                </ul>

                <?php
                $saved_jobs = mkjobs_get_saved_jobs($USER->id);
                ?>
                <?php if (count($saved_jobs['data']) < 1) { ?>
                    <p class="t-gray mb-4">※ <?php echo get_string('NoInterJob', 'local_job'); ?></p>
                <?php } ?>

                <?php if ($saved_jobs['total']) : ?>

                    <div id="jobsGroup">
                        <?php foreach ($saved_jobs['data'] as $job) : ?>
                            <div class="row mx-0 border mb-3">
                                <div class="col-3 py-jbox d-md-flex justify-content-center">
                                    <div class="align-self-center">
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $job->employer_id)); ?>">
                                            <img class="position-relative w-auto h-auto contain-image" src="<?php echo $job->company_logo; ?>">
                                        </a>
                                        <div class="mt-2 text-center">
                                            <i class="cursor-pointer ic ic-heart-on" onclick="savedJob(this, <?php echo $job->id; ?>)"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-9 pl-0 pr-jbox py-jbox">
                                    <div class="row mx-0 tit-jbox">
                                        <div class="px-0 col-md-10">
                                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>">
                                                <?php echo $job->title; ?>
                                            </a>
                                        </div>
                                        <div class="px-0 col-md-2 d-none d-md-block text-md-right text-primary"><?php echo $job->d_enddate; ?></div>
                                    </div>
                                    <p class="text-secondary mb-1">
                                        <i class="ic ic-usd"></i>
                                        <span><?php echo $job->salary; ?></span>
                                    </p>
                                    <div class="row mx-0">
                                        <div class="px-0 col-md-10 d-none d-md-block">
                                            <p class="t-gray"><?php echo $job->short_description; ?></p>
                                        </div>
                                        <div class="px-0 col-md-2 text-md-right text-primary">
                                            <p><?php echo $job->location->name; ?></p>
                                        </div>
                                    </div>
                                    <div class="row mx-0">
                                        <div class="px-0 col-md-10">
                                            <ul class="cate-list text-left mb-2">
                                                <?php foreach ($job->skills as $skill) : ?>
                                                    <li>
                                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                                            <?php echo $skill->name; ?>
                                                        </a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <div class="px-0 col-md-2 text-md-right">
                                            <p class="t-gray"><?php echo $job->timecreated; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="row mx-0 border mb-3" v-for="(item, idx) in items" v-cloak>
                            <div class="col-3 py-jbox d-md-flex justify-content-center">
                                <div class="align-self-center">
                                    <a :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.employer_id">
                                        <img class="position-relative w-auto h-auto contain-image" :src="item.company_logo">
                                    </a>
                                    <div class="mt-2 text-center">
                                        <i :class="['cursor-pointer ic', item.has_saved_job==1 ? 'ic-heart-on':'ic-heart']" @click="savedJob(idx)"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-9 pl-0 pr-jbox py-jbox">
                                <div class="row mx-0 tit-jbox">
                                    <div class="px-0 col-md-10">
                                        <a :href="'<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid='; ?>' + item.id">{{ item.title }}</a>
                                    </div>
                                    <div class="px-0 col-md-2 d-none d-md-block text-md-right text-primary">{{ item.d_enddate }}</div>
                                </div>
                                <p class="text-secondary mb-1">
                                    <i class="ic ic-usd"></i>
                                    <span>{{ item.salary }}</span>
                                </p>
                                <div class="row mx-0">
                                    <div class="px-0 col-md-10 d-none d-md-block">
                                        <p class="t-gray">{{ item.short_description }}</p>
                                    </div>
                                    <div class="px-0 col-md-2 text-md-right text-primary">
                                        <p>{{ item.location.name }}</p>
                                    </div>
                                </div>
                                <div class="row mx-0">
                                    <div class="px-0 col-md-10">
                                        <ul class="cate-list text-left mb-2">
                                            <li v-for="(skill, idx) in item.skills" :key="idx">
                                                <a :href="'<?php echo $VISANG->wwwroot . '/search.php'; ?>?skillid=' + skill.id">
                                                    {{ skill.name }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="px-0 col-md-2 text-md-right">
                                        <p class="t-gray">{{ item.timecreated }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($saved_jobs['total'] > 20) : ?>
                            <div :class="['my-3', loadmore ? '':'d-none']">
                                <button class="btn btn-block border btn-lg rounded-0 btn-spinner" :disabled="onfetch" @click="fetch">
                                    <span><?php echo get_string('ViewMore', 'local_job'); ?></span>
                                    <span class="btn-lbl ic ic-arrow-down"></span>
                                    <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                </button>
                            </div>
                        <?php endif; ?>

                    </div>
                    <script>
                        $(function() {
                            new Vue({
                                el: "#jobsGroup",
                                mixins: [mkjobs_search_mixin()],
                                data: {
                                    action: "mkjobs_get_saved_jobs",
                                    sesskey: "<?php echo sesskey(); ?>",
                                    perpage: 20
                                },
                                methods: {
                                    savedJob: function(idx) {
                                        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                                            sesskey: "<?php echo sesskey(); ?>",
                                            action: "mkjobs_saved_job",
                                            jobid: this.items[idx].id
                                        }, function(data) {
                                            if (data == -1) {
                                                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                                                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                                                }
                                            } else if (data == 1) {
                                                this.items[idx].has_saved_job = 1;
                                            } else {
                                                this.items[idx].has_saved_job = 0;
                                            }
                                        }.bind(this));
                                    }
                                }
                            });
                        });
                    </script>
                <?php else : ?>
                    <div class="no-data style02">
                        <div><?php echo get_string('JobAutoDelete', 'local_job'); ?></div>
                        <a href="<?php echo $VISANG->wwwroot . '/jobs/index.php'; ?>" class="btns point arrow"><?php echo get_string('Jobview', 'local_job'); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script>
    function savedJob(el, id, classon, classoff) {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkjobs_saved_job",
            jobid: id
        }, function(data) {
            if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
            } else if (data == 1) {
                $(el).removeClass(classoff || "ic-heart");
                $(el).addClass(classon || "ic-heart-on");
            } else {
                $(el).removeClass(classon || "ic-heart-on");
                $(el).addClass(classoff || "ic-heart");
            }
        });
    }
</script>

<?php
$VISANG->theme->footer();
