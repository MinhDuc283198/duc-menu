<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
$baseurl = new moodle_url($VISANG->wwwroot . '/my/edit.php');

$employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id 
                WHERE ad.user_id = :id";
$employer = $DB->get_record_sql($employer_sql, array('id' => $USER->id));

$id = $USER->id;

if (!empty($_POST)) {
    require_sesskey();
    if ($employer == null) {
        $birth_y = optional_param('birth_y', '1970', PARAM_RAW);
        $birth_m = optional_param('birth_m', '01', PARAM_RAW);
        $birth_d = optional_param('birth_d', '01', PARAM_RAW);
        $phone1 = optional_param('phone1', '', PARAM_RAW);
        $avatarpath=mkj_save_avatar($context->id,$id,$_FILES);

        $avatar_url1=$DB->get_record('avatar_user',array('user_id'=>$id));
        $user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST);

       if(is_null($avatarpath)==false){
           if($avatar_url1==false){
               $avatar_url1->user_id=$id;
               $avatar_url1->url=$avatarpath;
               $DB->insert_record('avatar_user',$avatar_url1);
           }
           else{
               $avatar_url1->user_id=$id;
               $avatar_url1->url=$avatarpath;
               $DB->update_record('avatar_user',$avatar_url1);
           }
       }

        $user->phone1 = $phone1;
        $user->phone2 = $birth_y . '-' . $birth_m . '-' . $birth_d;
        $user->timemodified = time();
        $DB->update_record('user', $user);

        $zipcode = optional_param('zipcode', '', PARAM_RAW);
        $address = optional_param('address', '', PARAM_RAW);
        $email2 = optional_param('email2', '', PARAM_RAW);
        $lmsdata_user_query = 'select u.id, lu.id as lu_id, lu.address, lu.address_detail, lu.email2, lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
        $lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $user->id));
        if(is_null($lmsdata_user->lu_id)){
            $lu = new stdClass();
            $lu->userid = $user->id;
            $lu->zipcode = $zipcode;
            $lu->address = $address;
            $lu->email2 = $email2;
            $DB->insert_record('lmsdata_user', $lu);
        } else {
            $lu = new stdClass();
            $lu->id = $lmsdata_user->lu_id;
            $lu->zipcode = $zipcode;
            $lu->address = $address;
            $lu->email2 = $email2;
            $DB->update_record('lmsdata_user', $lu);
        }
    } else {
        $firstname = optional_param('firstname', '', PARAM_RAW);
        $phone1 = optional_param('phone1', '', PARAM_RAW);
        // $phone2 = optional_param('phone2', '', PARAM_RAW);
        $department = optional_param('department', '', PARAM_RAW);
        $email = optional_param('email', '', PARAM_RAW);

        $user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST);
        $user->firstname = $firstname;
        $user->phone1 = $phone1;
        // $user->phone2 = $phone2;
        $user->department = $department;
        $user->email = $email;
        $user->timemodified = time();
        $DB->update_record('user', $user);

        // $company_name = optional_param('company_name', '', PARAM_RAW);
        // $emp = new stdClass();
        // $emp->id = $employer->id;
        // $emp->company_name = $company_name;
        // $DB->update_record('vi_employers', $emp);
    }

    redirect($baseurl);
}

$user = $DB->get_record('user', array('id' => $id));
$avatar_url=$DB->get_record('avatar_user',array('user_id'=>$id));
// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = get_string('PersonalInformationModification', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'edit';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <div role="main">
                <form enctype="multipart/form-data" action="<?php echo $baseurl; ?>" method="POST">
                    <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
                    <h2 class="pg-tit"><?php echo get_string('PersonalInformationModification', 'local_job'); ?></h2>
                    <?php if ($employer == null) : ?>
                        <?php 
                            $lmsdata_user_query = 'select u.id, lu.address, lu.address_detail, lu.email2, lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
                            $lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $user->id));
                        ?>
                        <div class="my-box">
                            <div class="rw company-info-area rw1" style="width: 20%; margin: 0 auto; float: none;">
                                <div class="ci-img">
                                    <div class="info-txt" style="background-image: url('<?php echo $avatar_url->url; ?>') !important;">
                                    </div>
                                    <p class="logo-img <?php echo ($avatar_url->url != "" && $avatar_url->url != " ") ? "" : "d-none";?>"><img class="rounded-circle" id="logoImg" src="<?php echo $CFG->wwwroot."/pluginfile.php/".$avatar_url->url;?>" alt="" /></p>
                                    <label><input type="file" name="avatar" id="avatar"/></label>
                                </div>
                            </div>
                        </div>
                        <div class="my-box imprt">
                            <div class="rw">
                                <strong><?php echo get_string('id', 'local_my') ?></strong>
                                <p>
                                    <?php
                                    $icon = 'icon_mail.png';
                                    if ($user->auth == 'facebook') {
                                        $icon = 'icon_sns.png';
                                    }
                                    ?>
                                    <img src="/theme/oklassedu/pix/images/<?php echo $icon ?>" alt="<?php echo $user->auth ?>" />
                                    <span><?php echo $user->username ?></span>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('name', 'local_my') ?></strong>
                                <p><strong><?php echo $user->firstname . $user->lastname ?></strong></p>
                            </div>

                            <div class="rw">
                                <strong><?php echo get_string('pw', 'local_my') ?></strong>
                                <p>
                                    <?php if ($user->auth == 'email' || $user->auth == 'manual') : ?>
                                        <a href="#" class="btns br openPop"><?php echo get_string('pwchange', 'local_my') ?></a>
                                    <?php else : ?>
                                        <span class="t-point"><?php echo get_string('EditInfoPass', 'local_job'); ?></span>
                                    <?php endif; ?>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('birth', 'local_my') ?></strong>
                                <?php
                                $birth_array = explode('-', $user->phone2);
                                ?>
                                <p class="birth">
                                    <select name="birth_y">
                                        <?php
                                        foreach (range(date('Y'), 1960) as $year) {
                                            if ($birth_array[0] == $year) {
                                                echo '<option value="' . $year . '" selected>' . $year . '</option>';
                                            } else {
                                                echo '<option value="' . $year . '">' . $year . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <select name="birth_m">
                                        <?php
                                        for ($m = 1; $m <= 12; $m++) {
                                            if (strlen($m) == 1)
                                                $m = "0" . $m;
                                            if (intval($m) == intval($birth_array[1])) {
                                                $date_month .= "<option value='$m' selected>$m</option>\n";
                                            } else {
                                                $date_month .= "<option value='$m'>$m</option>\n";
                                            }
                                        }
                                        echo $date_month;
                                        ?>
                                    </select>
                                    <select name="birth_d">
                                        <?php
                                        for ($d = 1; $d <= 31; $d++) {
                                            if (strlen($d) == 1)
                                                $d = "0" . $d;
                                            if (intval($d) == intval($birth_array[2])) {
                                                $date_day .= "<option value='$d' selected>$d</option>\n";
                                            } else {
                                                $date_day .= "<option value='$d'>$d</option>\n";
                                            }
                                        }
                                        echo $date_day;
                                        ?>
                                    </select>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('email', 'local_my') ?></strong>
                                <p class="e-mail">
                                    <input type="text" class="w-auto" name="email2" value="<?php echo $lmsdata_user->email2; ?>" placeholder="" />
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('phone', 'local_course') ?></strong>
                                <p class="phone">
                                    <input required type="text" class="w-auto format-number" name="phone1" value="<?php echo $user->phone1 ?>" placeholder="" />
                                    <span class="txt"><?php echo get_string('phone_txt', 'local_my') ?></span>
                                </p>
                            </div>
                        </div>
                        
                        <h5 class="bx-tit"><?php echo get_string('my:info:address', 'local_job') ?></h5>
                        <div class="my-box">
                            <div class="rw">
                                <strong><?php echo get_string('adress', 'local_course') ?></strong>
                                <p class="addr">
                                    <input type="text" class="w100" name="zipcode" value="<?php echo $lmsdata_user->zipcode; ?>" placeholder="<?php echo get_string('zipcode', 'local_my'); ?>" />
                                    <input type="text" class="w100" name="address" value="<?php echo $lmsdata_user->address; ?>" placeholder="<?php echo get_string('adress', 'local_my'); ?> " />
                                </p>
                            </div>
                        </div>
                    <?php else : ?>
                        <style>
                            .my-box .rw {
                                display: flex;
                            }

                            input[type="email"] {
                                font-size: 15px;
                                color: #555;
                                height: 35px;
                                box-sizing: border-box;
                                padding: 10px;
                                line-height: 1;
                                margin-bottom: 0;
                            }

                            .my-box .rw>strong {
                                width: 250px !important;
                            }

                            @media (max-width: 576px) {
                                .my-box .rw {
                                    display: block !important;
                                }

                                .my-box .rw>strong {
                                    width: 100% !important;
                                }

                                .my-box .phone input {
                                    max-width: 100% !important;
                                    width: 100% !important;
                                }
                            }

                            .required-star {
                                content: "";
                                width: 6px;
                                height: 6px;
                                display: inline-block;
                                background: #76cb00;
                                border-radius: 100%;
                                margin-left: 5px;
                                vertical-align: text-top;
                                margin-top: 7px;
                            }
                        </style>
                        <div class="my-box">
                            <div class="rw company-info-area rw1" style="width: 20%; margin: 0 auto; float: none;">
                                <div class="ci-img">
                                    <div class="info-txt" style="background-image: url('<?php echo  $avatar_url->url;?>') !important;">
                                    </div>
                                    <p class="avatar-img"><img class="rounded-circle" id="logoImg" src="<?php echo $CFG->wwwroot."/pluginfile.php/". $avatar_url->url;?>" alt="" /></p>
                                    <label><input type="file" name="avatar" id="avatar"/></label>
                                </div>
                            </div>
                        </div>
                        <div class="my-box imprt">
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:company_name', 'local_job'); ?></strong>
                                <p>
                                    <span><?php echo $employer->company_name; ?></span>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('id', 'local_my') ?></strong>
                                <p>
                                    <span><?php echo $user->username; ?></span>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:business_license_number', 'local_job'); ?></strong>
                                <p class="phone">
                                    <span><?php echo $user->phone2; ?></span>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:approval_date', 'local_job'); ?></strong>
                                <p>
                                    <span><?php echo $employer->approval_date ? date('Y.m.d', $employer->approval_date) : '-'; ?></span>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:contact_name', 'local_job'); ?> <span class="required-star"></span></strong>
                                <p class="phone">
                                    <input required type="text" class="w-auto" name="firstname" value="<?php echo $user->firstname; ?>" placeholder="" />
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:department', 'local_job'); ?> <span class="required-star"></span></strong>
                                <p class="phone">
                                    <input required type="text" class="w-auto" name="department" value="<?php echo $user->department; ?>" placeholder="" />
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:contact_number', 'local_job'); ?> <span class="required-star"></span></strong>
                                <p class="phone">
                                    <input required type="text" class="w-auto format-number" name="phone1" value="<?php echo $user->phone1 ?>" placeholder="" />
                                    <span class="txt"><?php echo get_string('phone_txt', 'local_my') ?></span>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:email', 'local_job'); ?></strong>
                                <p class="phone">
                                    <input required type="email" class="w-auto" name="email" value="<?php echo $user->email ?>" placeholder="" />
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('pw', 'local_my') ?></strong>
                                <p>
                                    <?php if ($user->auth == 'email' || $user->auth == 'manual') : ?>
                                        <a href="#" class="btns br openPop"><?php echo get_string('pwchange', 'local_my') ?></a>
                                    <?php else : ?>
                                        <span class="t-point"><?php echo get_string('EditInfoPass', 'local_job'); ?></span>
                                    <?php endif; ?>
                                </p>
                            </div>
                            <div class="rw">
                                <strong><?php echo get_string('employer_user:timemodified', 'local_job'); ?></strong>
                                <p>
                                    <span><?php echo date('Y.m.d', $user->timemodified); ?></span>
                                </p>
                            </div>
                            <p><span class="required-star"></span> <?php echo get_string('FieldRequest', 'local_job'); ?></p>
                            <p><?php echo get_string('employer_user:note', 'local_job'); ?></p>
                        </div>
                    <?php endif; ?>

                    <div class="text-center">
                        <input type="submit" value="<?php echo get_string("save", "local_my") ?>" class="btns point big02">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logoImg').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function() {
        readURL(this);
        $(".avatar-img").removeClass("d-none");
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $(document).on("keyup", "#newpassword", function() {
            check_val('newpassword');
            match_password();
        });
        $(document).on("keyup", "#newpassword2", function() {
            match_password();
        });
        $(document).on("blur", "#cpassword", function() {
            var result = call_ajax('cpassword', {
                pw: $('#cpassword').val()
            }, false);

            if (result.status == 'sucess') {
                $('.cpasswordwarning').text(result.text);
                $('#cpasswordconfirm').val(1);
            } else {
                $('.cpasswordwarning').text(result.text);
                $('#cpasswordconfirm').val(0);
            }
        });

    })

    function check_val(type) {

        var target = $("#" + type);
        var regExpemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (type == 'email' && !regExpemail.test($("#email").val())) {
            var errortext = "<?php echo get_string("emailrule", "local_signup", array('rule' => "visang@visang.com")) ?>";
            alert(errortext);
            $("." + type + "warning").text(errortext);
            $("#" + type + "confirm").val(0);
        } else {
            if (/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/.test(target.val())) {
                $.ajax({
                    url: "/local/my/valueconfirm.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        type: type,
                        value: target.val()
                    },
                    success: function(result) {
                        if (result.status) {
                            $("#" + type + "confirm").val(result.status);
                            $("." + type + "warning").text(result.text);
                        } else {
                            $("#" + type + "confirm").val(result.status);
                            $("." + type + "warning").text(result.text);
                        }
                    }
                });
            } else {
                $("#newpasswordconfirm").val(0);
                $(".newpasswordwarning").text('<?php echo get_string('passvalcheck', 'local_signup') ?>');
            }
        }
    }

    function match_password() {
        if ($("#newpassword2").val() == $("#newpassword").val()) {
            $(".newpassword2warning").text("<?php echo get_string("matchedpass", "local_signup") ?>");
        } else {
            $(".newpassword2warning").text("<?php echo get_string("notmatchedpass", "local_signup") ?>");
        }
    }
    $(function() {
        //비밀번호 변경 팝업 이벤트
        $(".openPop").click(function() {
            utils.popup.call_layerpop("<?php echo $CFG->wwwroot . '/local/my/popup_pwchange.php'; ?>", {
                "width": "400px",
                "height": "auto",
                "callbackFn": function() {}
            }, {
                "param": "11"
            });
            return false;
        })
    });

    //값이 없는지 체크
    function value_null_check(select) {
        if ($(select).val() == null || $(select).val() == 0 || $(select).val() == undefined || $(select).val() == '') {
            return false;
        } else {
            return true;
        }
    }

    function pw_submit_check() {
        var new1 = $('input[name=newpassword]').val();
        var new2 = $('input[name=newpassword2]').val();

        if (value_null_check('input[name=password]') == false) {
            alert('<?php echo get_string('pw1', 'local_my') ?>');
            return false;
        }
        if (value_null_check('input[name=newpassword]') == false) {
            alert('<?php echo get_string('pw2', 'local_my') ?>');
            return false;
        }
        if (value_null_check('input[name=newpassword2]') == false) {
            alert('<?php echo get_string('pw3', 'local_my') ?>');
            return false;
        }

        if (new1 != new2) {
            alert('<?php echo get_string('pw4', 'local_my') ?>');
            return false;
        }

        var check1 = $('#cpasswordconfirm').val();
        var check2 = $('#newpasswordconfirm').val();
        if (check2 != 1) {
            alert('<?php echo get_string('passvalcheck', 'local_signup'); ?>');
            return false;
        }
        if (check1 != 1) {
            alert('<?php echo get_string('pw6', 'local_my') ?>');
            return false;
        }

        if (check1 != 1 || check2 != 1) {
            return false;
        }
        return true;
    }

    function pw_submit() {
        var pw = $('input[name=password]').val();
        var new1 = $('input[name=newpassword]').val();
        var new2 = $('input[name=newpassword2]').val();

        if (pw_submit_check() == true) {

            var result = call_ajax('passwordchange', {
                pw: pw,
                new1: new1,
                new2: new2
            }, false);

            if (result) {
                if (result.status == 'success') {
                    alert('<?php echo get_string('pw5', 'local_my') ?>');
                    utils.popup.close_pop($('.layerpop'));
                    window.location.href = "<?php echo $VISANG->wwwroot; ?>/auth/logout.php?sesskey=<?php echo $USER->sesskey; ?>&urltogo=<?php echo $VISANG->wwwroot; ?>/auth/login.php?urltogo=<?php echo $VISANG->wwwroot; ?>/my/edit.php";
                } else if (result.status == 'not') {
                    $('.not').show();
                    return false;
                } else {
                    alert(result.error);
                }
            }
        }
    }

    function call_ajax(action, data, async) {

        var rvalue = false;
        $.ajax({
            type: 'POST',
            url: '<?php echo $CFG->wwwroot . '/local/my/ajax.php'; ?>',
            dataType: 'JSON',
            async: async,
            data: {
                action: action,
                data: data
            },
            success: function(result) {
                rvalue = result;
            },
            error: function(xhr, status, error) {}
        });

        return rvalue;
    }

    $(".format-number").keydown(function(event) {
        var key = window.event ? event.keyCode : event.which;
        if (key === 8 || key === 190 || key === 191) {
            return true;
        } else if (key > 95 && key < 105) {
            return true;
        } else if (key < 46 || key > 57) {
            return false;
        } else {
            return true;
        }
    });
</script>
<?php
$VISANG->theme->footer();
