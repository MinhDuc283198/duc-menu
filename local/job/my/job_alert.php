<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$userid=$_GET['userid'];
$user = $DB->get_record('user', array('id' => $userid));
if(!is_null($_POST['value'])){
    $isreceivejob=$_POST['value'];

    $user->isreceivejob=$isreceivejob;
    $user->timemodified=time();
    $DB->update_record('user', $user);
}