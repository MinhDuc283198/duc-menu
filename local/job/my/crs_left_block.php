<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $DB, $USER;

?>

<?php $sql = "SELECT ad.employer_id as val
            FROM {vi_admin_employers} ad
            WHERE ad.user_id = :id";
            $comid = $DB->get_record_sql($sql, array('id' => $USER->id));?>
                                        
<ul>
<?php if($comid == null){?>
	<li class="<?php echo $VISANG->theme->submenu == 'info' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/info.php'; ?>"><?php echo get_string('MyInfo', 'local_job'); ?></a></li>
    <li class="<?php echo $VISANG->theme->submenu == 'apply' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/apply.php'; ?>"><?php echo get_string('SupportStatus', 'local_job'); ?></a></li>
    <li class="<?php echo $VISANG->theme->submenu == 'inter' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/inter.php'; ?>"><?php echo get_string('Interestinformation', 'local_job'); ?></a></li>
    <li class="<?php echo $VISANG->theme->submenu == 'notification' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a></li>
<?php }?>

    <li class="<?php echo $VISANG->theme->submenu == 'qna2' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
    <li class="<?php echo $VISANG->theme->submenu == 'edit' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
    <li class=""><a href="<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>"><?php echo get_string('credit-status-title', 'local_credit'); ?></a></li>
</ul>