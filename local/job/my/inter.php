<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');

$view = optional_param('view', '', PARAM_RAW);

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
$baseurl = new moodle_url($VISANG->wwwroot . '/my/inter.php');
$returnLP = optional_param('returnLP', 0, PARAM_INT);
if (!empty($_POST)) {
    require_sesskey();

    $final_education_id = optional_param('final_education_id', 0, PARAM_INT);
    $work_experience_id = optional_param('work_experience_id', 0, PARAM_INT);
    $skill_groups = optional_param_array('skill_groups', array(), PARAM_RAW);
    $user_skills = optional_param_array('user_skills', array(), PARAM_RAW);
    $user_workplaces = optional_param_array('user_workplaces', array(), PARAM_RAW);
    $korean_level_id = optional_param('korean_level_id', 0, PARAM_INT);

    // ====================================================================================================================================
    $DB->delete_records('vi_user_final_educations', array('user_id' => $USER->id));
    if ($final_education_id != 0) {
        $final_education = new stdClass();
        $final_education->user_id = $USER->id;
        $final_education->final_education_id = $final_education_id;
        $DB->insert_record('vi_user_final_educations', $final_education);
    }

    // ====================================================================================================================================
    $DB->delete_records('vi_user_work_experiences', array('user_id' => $USER->id));
    if ($work_experience_id != 0) {
        $work_experience = new stdClass();
        $work_experience->user_id = $USER->id;
        $work_experience->work_experience_id = $work_experience_id;
        $DB->insert_record('vi_user_work_experiences', $work_experience);
    }

    // ====================================================================================================================================
    $DB->delete_records('vi_user_skill_groups', array('user_id' => $USER->id));
    foreach ($skill_groups as $groupid) {
        $user_skill_group = new stdClass();
        $user_skill_group->user_id = $USER->id;
        $user_skill_group->skill_group_id = $groupid;
        $DB->insert_record('vi_user_skill_groups', $user_skill_group);
    }

    // ====================================================================================================================================
    $DB->delete_records('vi_user_skills', array('user_id' => $USER->id));
    foreach ($user_skills as $skillid) {
        $user_skill = new stdClass();
        $user_skill->user_id = $USER->id;
        $user_skill->skill_id = $skillid;
        $DB->insert_record('vi_user_skills', $user_skill);
    }

    // ====================================================================================================================================
    $DB->delete_records('vi_user_workplaces', array('user_id' => $USER->id));
    foreach ($user_workplaces as $districtid) {
        $workplace = new stdClass();
        $workplace->user_id = $USER->id;
        $workplace->district_id = $districtid;
        $DB->insert_record('vi_user_workplaces', $workplace);
    }

    // ====================================================================================================================================
    $DB->delete_records('vi_user_korean_levels', array('user_id' => $USER->id));
    if ($korean_level_id != 0) {
        $level = new stdClass();
        $level->user_id = $USER->id;
        $level->korean_level_id = $korean_level_id;
        $DB->insert_record('vi_user_korean_levels', $level);
    }
    if($returnLP != 0){
        redirect(new moodle_url($VISANG->wwwroot . '/resumes/edit.php?resumeid='.$returnLP));
    }else{
        redirect($baseurl);
    }
    
}

$skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $USER->id));
$user_skills = array();
foreach ($skills as $skill) {
    $user_skills[] = json_encode(array('id' => $skill->id, 'name' => $skill->name));
}

$groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $USER->id));
$skill_groups = array();
foreach ($groups as $group) {
    $skill_groups[] = json_encode(array('id' => $group->id, 'name' => $group->name));
}

$districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $USER->id));
$workplaces = array();
foreach ($districts as $district) {
    $workplaces[] = json_encode(array('id' => $district->id, 'name' => $district->name));
}

$current_language  = current_language();
// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = get_string('Interestinformation', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'inter';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <div role="main">
                <h2 class="pg-tit"><?php echo get_string('Interestinformation', 'local_job'); ?></h2>
                <ul class="mk-c-tab tab-event">
                    <li class="w-30"><a href="<?php echo $VISANG->wwwroot . '/my/inter_jobs.php'; ?>"><?php echo get_string('LikeJob', 'local_job'); ?></a></li>
                    <li class="w-30"><a href="<?php echo $VISANG->wwwroot . '/my/inter_com.php'; ?>"><?php echo get_string('companyInInterested', 'local_job'); ?></a></li>
                    <li class="w-30 on"><a href="<?php echo $VISANG->wwwroot . '/my/inter.php'; ?>"><?php echo get_string('InterestInfo', 'local_job'); ?></a></li>
                </ul>

                <p class="t-gray mb-4"><?php echo get_string('getNotice', 'local_job'); ?></p>
                <form action="<?php echo $baseurl; ?>" method="POST">
                    <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
                    <div class="ck-my-info">
                        <div class="rw">
                            <strong><?php echo get_string('my_inter:final_education', 'local_job'); ?><em>(<?php echo get_string('my_inter:limit_1', 'local_job'); ?>)</em></strong>
                            <p>
                                <?php
                                $final_educations = $DB->get_records('vi_final_educations', array());
                                $user_final_education = $DB->get_record('vi_user_final_educations', array('user_id' => $USER->id));
                                $final_education_id = $user_final_education ? $user_final_education->final_education_id : 0;
                                ?>
                                <select class="under" name="final_education_id">
                                    <option value="" <?php echo $final_education_id == 0 ? 'selected' : ''; ?>><?php echo get_string('my_inter:final_education_desc', 'local_job'); ?></option>
                                    <?php foreach ($final_educations as $final_education) : ?>
                                        <option value="<?php echo $final_education->id; ?>" <?php echo $final_education_id == $final_education->id ? 'selected' : ''; ?>>
                                            <?php
                                            switch ($current_language) {
                                                case 'ko':
                                                    echo $final_education->name_ko;
                                                    break;
                                                case 'en':
                                                    echo $final_education->name_en;
                                                    break;
                                                case 'vi':
                                                    echo $final_education->name_vi;
                                                    break;
                                                default:
                                                    echo $final_education->name;
                                                    break;
                                            }
                                            ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                        </div>
                        <div class="rw">
                            <strong><?php echo get_string('my_inter:work_experience', 'local_job'); ?><em>(<?php echo get_string('my_inter:limit_1', 'local_job'); ?>)</em></strong>
                            <p>
                                <?php
                                $work_experiences = $DB->get_records('vi_work_experiences', array());
                                $user_work_experience = $DB->get_record('vi_user_work_experiences', array('user_id' => $USER->id));
                                $work_experience_id = $user_work_experience ? $user_work_experience->work_experience_id : 0;
                                ?>
                                <select class="under" name="work_experience_id">
                                    <option value="" <?php echo $work_experience_id == 0 ? 'selected' : ''; ?>><?php echo get_string('my_inter:work_experience_desc', 'local_job'); ?></option>
                                    <?php foreach ($work_experiences as $work_experience) : ?>
                                        <option value="<?php echo $work_experience->id; ?>" <?php echo $work_experience_id == $work_experience->id ? 'selected' : ''; ?>>
                                            <?php
                                            switch ($current_language) {
                                                case 'ko':
                                                    echo $work_experience->name_ko;
                                                    break;
                                                case 'en':
                                                    echo $work_experience->name_en;
                                                    break;
                                                case 'vi':
                                                    echo $work_experience->name_vi;
                                                    break;
                                                default:
                                                    echo $work_experience->name;
                                                    break;
                                            }
                                            ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                        </div>

                        <div class="rw" id="skill-groups-advtag" v-cloak>
                            <strong><?php echo get_string('my_inter:skill_group', 'local_job'); ?><em>(<?php echo get_string('my_inter:limit_3', 'local_job'); ?>)</em></strong>
                            <p>
                                <span v-for="(item, index) in items" class="txt-bx">
                                    {{ item.name }}
                                    <a @click="removeItem(index)" href="#" class="ic-close">close</a>
                                </span>
                                <a v-if="maxitems < 0 || (maxitems >=0 && items.length < maxitems)" href="#" @click="add" class="btns ic-plus openPop"><?php echo get_string('my_inter:add', 'local_job'); ?></a>
                            </p>
                            <div class="d-none">
                                <input type="hidden" v-for="(item,i) in items" :key="i" :name="elid+'[]'" :value="item.id">
                            </div>
                        </div>

                        <div class="rw" id="skills-advtag" v-cloak>
                            <strong><?php echo get_string('my_inter:skill', 'local_job'); ?><em>(<?php echo get_string('my_inter:limit_3', 'local_job'); ?>)</em></strong>
                            <p>
                                <span v-for="(item, index) in items" class="txt-bx">
                                    {{ item.name }}
                                    <a @click="removeItem(index)" href="#" class="ic-close">close</a>
                                </span>
                                <a v-if="maxitems < 0 || (maxitems >=0 && items.length < maxitems)" href="#" @click="add" class="btns ic-plus openPop"><?php echo get_string('my_inter:add', 'local_job'); ?></a>
                            </p>
                            <div class="d-none">
                                <input type="hidden" v-for="(item,i) in items" :key="i" :name="elid+'[]'" :value="item.id">
                            </div>
                        </div>

                        <div class="rw" id="workplaces-advtag" v-cloak>
                            <strong><?php echo get_string('my_inter:workplace', 'local_job'); ?><em>(<?php echo get_string('my_inter:limit_3', 'local_job'); ?>)</em></strong>
                            <p>
                                <span v-for="(item, index) in items" class="txt-bx">
                                    {{ item.name }}
                                    <a @click="removeItem(index)" href="#" class="ic-close">close</a>
                                </span>
                                <a v-if="maxitems < 0 || (maxitems >=0 && items.length < maxitems)" href="#" @click="add" class="btns ic-plus openPop"><?php echo get_string('my_inter:add', 'local_job'); ?></a>
                            </p>
                            <div class="d-none">
                                <input type="hidden" v-for="(item,i) in items" :key="i" :name="elid+'[]'" :value="item.id">
                            </div>
                        </div>

                        <div class="rw">
                            <strong><?php echo get_string('my_inter:korean_level', 'local_job'); ?><em>(<?php echo get_string('my_inter:limit_1', 'local_job'); ?>)</em></strong>
                            <p>
                                <?php
                                $korean_levels = $DB->get_records('vi_korean_levels', array(), 'name ASC');
                                $user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $USER->id));
                                $korean_level_id = $user_korean_level ? $user_korean_level->korean_level_id : 0;
                                ?>
                                <select class="under" name="korean_level_id">
                                    <option value="" <?php echo $korean_level_id == 0 ? 'selected' : ''; ?>>
                                        <?php echo get_string('my_inter:korean_desc', 'local_job'); ?>
                                    </option>
                                    <?php foreach ($korean_levels as $korean_level) : ?>
                                        <option value="<?php echo $korean_level->id; ?>" <?php echo $korean_level_id == $korean_level->id ? 'selected' : ''; ?>>
                                            <?php
                                            switch ($current_language) {
                                                case 'ko':
                                                    echo $korean_level->name_ko;
                                                    break;
                                                case 'en':
                                                    echo $korean_level->name_en;
                                                    break;
                                                case 'vi':
                                                    echo $korean_level->name_vi;
                                                    break;
                                                default:
                                                    echo $korean_level->name;
                                                    break;
                                            }
                                            ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="text-center">
                    	<input type="hidden" name="returnLP" value="<?php echo $returnLP;?>"/>
                        <button type="submit" class="btns point big02"><?php echo get_string('Save', 'local_job'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function VisangQuickForm_advtag_mixin() {
        return {
            data: {
                elid: "_VisangQuickForm_advtag",
                items: [],
                url: "<?php echo $VISANG->wwwroot . '/my/pop_interest_info.php'; ?>",
                sesskey: "<?php echo sesskey(); ?>",
                maxitems: -1
            },
            methods: {
                addItem: function(item) {
                    if (this.maxitems >= 0 && this.items.length == this.maxitems) return;
                    var isexist = false;
                    for (var i = 0; i < this.items.length && !isexist && item != undefined; i++) {
                        if (this.items[i].id == item.id) {
                            isexist = true;
                        }
                    }

                    if (item != undefined && !isexist) {
                        this.items.push(item);
                    }
                },
                removeItem: function(index) {
                    this.items.splice(index, 1);
                },
                add: function() {
                    var items = this.items;
                    utils.popup.call_layerpop(this.url, {
                        "width": "700px",
                        "height": "auto",
                        "callbackFn": function() {
                            for (var i = 0; i < items.length; i++) {
                                $("input[name='pop_inter_chk[]'][value='" + items[i].id + "']").prop('checked', true);
                            }
                        }
                    }, {
                        elid: this.elid,
                        sesskey: this.sesskey
                    });
                }
            }
        };
    }

    function btnAddAction(action) {
        window[action + '_advtag'].items = [];
        $("input[name='pop_inter_chk[]']:checked").each(function() {
            window[action + '_advtag'].addItem({
                id: $(this).val(),
                name: $(this).data('name')
            });
        });
        $('.pop-close').click();
    }

    var skill_groups_advtag = null;
    var user_skills_advtag = null;
    var user_workplaces_advtag = null;
    $(function() {
        skill_groups_advtag = new Vue({
            el: "#skill-groups-advtag",
            mixins: [VisangQuickForm_advtag_mixin()],
            data: {
                elid: "skill_groups",
                items: [<?php echo implode(',', $skill_groups); ?>],
                maxitems: 3,
            }
        });

        user_skills_advtag = new Vue({
            el: "#skills-advtag",
            mixins: [VisangQuickForm_advtag_mixin()],
            data: {
                elid: "user_skills",
                items: [<?php echo implode(',', $user_skills); ?>],
                maxitems: 3,
            }
        });

        user_workplaces_advtag = new Vue({
            el: "#workplaces-advtag",
            mixins: [VisangQuickForm_advtag_mixin()],
            data: {
                elid: "user_workplaces",
                items: [<?php echo implode(',', $workplaces); ?>],
                maxitems: 3,
            }
        });
    });

    function count_checked_item(obj){
    	var className = $("."+obj.className);
    	//var all = document.getElememtByClassName(className);
    	//alert(className.length);
    	var count = 0;
    	for(i = 0; i< className.length; i++){
        	var item = className[i];
        	if(item.checked){
				count++;
        	}
    	
    	}
    	if(count > 3){
    		obj.checked = false;
    	}
    }
    
</script>
<?php
$VISANG->theme->footer();
