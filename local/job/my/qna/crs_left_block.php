<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $DB, $USER;
$employer_sql = "SELECT em.*
                FROM {vi_admin_employers} ad
                JOIN {vi_employers} em ON em.id = ad.employer_id 
                WHERE ad.user_id = :id";
$employer = $DB->get_record_sql($employer_sql, array('id' => $USER->id));
?>
<ul>
    <?php if ($employer == null) : ?>
        <li class="<?php echo $VISANG->theme->submenu == 'qna1' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=8'; ?>"><?php echo get_string('QNA1', 'local_job'); ?></a></li>
        <li class="<?php echo $VISANG->theme->submenu == 'qna2' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=12'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a></li>
        <li class="<?php echo $VISANG->theme->submenu == 'qna3' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=10'; ?>"><?php echo get_string('QNA3', 'local_job'); ?></a></li>
    <?php else : ?>
        <li class="<?php echo $VISANG->theme->submenu == 'qna1' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=9'; ?>"><?php echo get_string('QNA1', 'local_job'); ?></a></li>
        <li class="<?php echo $VISANG->theme->submenu == 'qna2' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=13'; ?>"><?php echo get_string('QNA2', 'local_job'); ?></a></li>
        <li class="<?php echo $VISANG->theme->submenu == 'qna3' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=11'; ?>"><?php echo get_string('QNA3', 'local_job'); ?></a></li>
    <?php endif; ?>
</ul>