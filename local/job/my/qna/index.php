<?php
require_once(dirname(__FILE__) . '/../../lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

$context = context_system::instance();

$PAGE->set_context($context);
// =====================================================================================================
// handles

$user = $USER;


$type = optional_param('type', 1, PARAM_INT);
if (!($type == 8 || $type == 9 || $type == 10 || $type == 11)) mkjobs_require_login();
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$market = optional_param('market', 3, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);
$board = $DB->get_record('jinoboard', array('type' => $type));


switch (current_language()) {
    case 'ko':
        $boardname = $board->name;
        break;
    case 'en':
        $boardname = $board->engname;
        break;
    case 'vi':
        $boardname = $board->vnname;
        break;
}

$today = time();
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

// =====================================================================================================
// renders
$VISANG->theme->title = $boardname;
if ($type == 8 || $type == 9) {
    $VISANG->theme->submenu = 'qna1';
} else if ($type == 10 || $type == 11) {
    $VISANG->theme->submenu = 'qna3';
} else if ($type == 12 || $type == 13) {
    $VISANG->theme->submenu = 'qna2';
}
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->header();

$siteadmin = is_siteadmin();
$addsql = '';
$addsql2 = '';
if ($type == 2 && $siteadmin == false) {
    $addsql = 'AND (userid =' . $USER->id . ' OR  ref in ( select id from {jinoboard_contents} where userid =' . $USER->id . ' ) )';
}

if ($category) {
    $addsql2 = ' AND category =' . $category;
}
$sql = "select count(id) from {jinoboard_contents} where board = :board and isnotice = 0 $addsql2 $addsql order by ref DESC, step ASC";
$totalcount = $DB->count_records_sql($sql, array('board' => $board->type));
$total_pages = jinoboard_get_total_pages($totalcount, $perpage);
?>

<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php
            if ($type == 12 || $type == 13) {
                include_once(dirname(__FILE__) . '/../crs_left_block.php');
            } else {
                include_once(dirname(__FILE__) . '/crs_left_block.php');
            }
            ?>
        </div>
        <div class="crs-right-block">

            <?php
            //<- 타이틀 시작 ->
            if ($type == 12 || $type == 13) {
            ?>
                <h2 class="pg-tit">
                    <?php
                    echo get_string('qna', 'local_mypage');
                    // echo $boardname; 
                    ?>
                </h2>
                <div class="tp-tb-area text-right">
                    <a href="/local/job/my/qna/write.php?type=<?php echo $type; ?>" class="btns br_blue h40"><?php echo get_string('add', 'local_jinoboard'); ?></a>
                </div>
            <?php
            } else if ($type == 10 || $type == 11) {
                $categories = array();
                $categories = $DB->get_records('jinoboard_category', array('board' => $board->type), 'sortorder asc');
                $cate_cnt = 1;
                foreach ($categories as $cate) {
                    if ($cate->isused == 1) $cate_cnt++;
                }
            ?>
                <h2 class="pg-tit"><?php echo $boardname; ?></h2>
                <style>
                    @media (min-width: 992px) {
                        .mk-c-tab.col1>li {
                            width: 100% !important;
                        }
                        .mk-c-tab.col2>li {
                            width: 50% !important;
                        }
                        .mk-c-tab.col5>li {
                            width: 20% !important;
                        }
                    }
                </style>
                <ul class="mk-c-tab col<?php echo $cate_cnt; ?> tab-event">
                    <li <?php echo ($category == 0) ? 'class="on"' : ''; ?> data-target=".data0<?php echo $category; ?>">
                        <a href="<?php echo $CFG->wwwroot . "/local/job/my/qna/index.php?type=" . $type; ?>">
                            <?php echo get_string('all_list', 'local_jinoboard'); ?>
                        </a>
                    </li>
                    <?php foreach ($categories as $cate) : ?>
                        <?php if ($cate->isused == 1) : ?>
                            <li data-target=".data0<?php echo $category; ?>" <?php echo ($category == $cate->id) ? 'class="on"' : ''; ?>>
                                <a href="<?php echo $CFG->wwwroot . "/local/job/my/qna/index.php?type=" . $type . "&category=" . $cate->id; ?>">
                                    <?php
                                    switch (current_language()) {
                                        case 'en':
                                            echo $cate->engname;
                                            break;
                                        case 'vi':
                                            echo $cate->vnname;
                                            break;
                                        default:
                                            echo $cate->name;
                                            break;
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>

            <?php
            } else if ($type == 8 || $type == 9) {
            ?> <h2 class="pg-tit"><?php echo $boardname; ?></h2>
            <?php }
            //<- 타이틀 끝 ->
            //<-리스트 시작->
            $offset = 0;
            if ($page != 0) {
                $offset = ($page - 1) * $perpage;
            }
            $list_num = $offset;
            $num = $totalcount - $offset;
            //--1:1인경우추가
            if ($type == 2 && $siteadmin == false) {
                $fsql = " AND (userid = $USER->id OR ref IN (SELECT id FROM {jinoboard_contents} WHERE userid = $USER->id)) ";
            }
            //--
            $sql = "select * from {jinoboard_contents} where board = :board and isnotice = 0 $fsql $addsql2 order by timecreated DESC, ref DESC, step ASC";
            $contents = $DB->get_records_sql($sql, array('board' => $board->type), $offset, $perpage);
            if ($type != 3) {
            ?>
                <?php //상단에뿌려주는부분
                if ($board->allownotice == 1) {
                    $sql = "select * from {jinoboard_contents} where board = :board " . $like . " and isnotice = 1 order by ref DESC, step ASC";
                    $notices = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'));
                    foreach ($notices as $content) {
                        $postuser = $DB->get_record('user', array('id' => $content->userid));
                        $fullname = fullname($postuser);
                        $userdate = userdate($content->timecreated);
                        $by = new stdClass();
                        $by->name = $fullname;
                        $by->date = $userdate;
                        $fs = get_file_storage();
                        if (!empty($notice->id)) {
                            $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $notice->id, 'timemodified', false);
                        } else {
                            $files = array();
                        }
                        if (count($files) > 0) {
                            $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                        } else {
                            $filecheck = "";
                        }
                    }
                }
                ?>
                <table class="table m-block">
                    <thead>
                        <tr>
                            <th width="10%"><?php echo get_string('num', 'local_jinoboard'); ?></th>
                            <th><?php echo get_string('title', 'local_jinoboard'); ?></th>
                            <th width="15%"><?php echo get_string('writer', 'local_jinoboard'); ?></th>
                            <th width="20%"><?php echo get_string('date', 'local_jinoboard'); ?></th>
                            <th width="10%"><?php echo get_string('view:cnt', 'local_jinoboard'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($contents) {
                            foreach ($contents as $content) {
                                $list_num++;
                                $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
                                $fs = get_file_storage();
                                $files = $fs->get_area_files($context->id, 'local_jinoboard', $filename, $content->id, 'timemodified', false);
                                if (count($files) > 0) {
                                    $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                                } else {
                                    $filecheck = "";
                                }
                                $postuser = $DB->get_record('user', array('id' => $content->userid));
                                $fullname = fullname($postuser);
                                $userdate = userdate($content->timecreated);
                                $by = new stdClass();
                                $by->name = $fullname;
                                $by->date = $userdate;
                        ?>
                                <tr>
                                    <td class="m-hide"><?php echo $num; ?></td>
                                    <td class="text-left overflow  
                                    <?php if ($board->newday) {
                                        echo ($content->timemodified + (86400 * $board->newday)) > $today ? 'hasnew' : '';
                                    } ?> ">
                                        <?php echo !(empty($step_icon)) ? '<span class="tb-reply">re</span>' : '';
                                        if ($content->issecret && $USER->id != $content->userid && !is_siteadmin() && $parent->userid != $USER->id) {
                                            echo $content->title;
                                        } else {
                                            echo "<a href='" . $CFG->wwwroot . "/local/job/my/qna/detail.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&type=" . $type . "&searchfield=" . $searchfield . "'>" . $content->title . "</a>";
                                        }
                                        ?>
                                        <?php if ($board->newday) {
                                            echo ($content->timemodified + (86400 * $board->newday)) > $today ? '<span class="ic-new">N</span>' : '';
                                        } ?></td>
                                    <td><?php echo $fullname; ?></td>
                                    <td><?php echo date("Y-m-d", $content->timemodified); ?></td>
                                    <td class="m-f-r"><span class="m-show inline"><?php echo get_string('view:cnt', 'local_jinoboard'); ?></span><?php echo $content->viewcnt; ?></td>
                                </tr>
                            <?php
                                $num--;
                            }
                        } else {
                            ?> <tr>
                                <td colspan="5"><?php if ($type == 2) {
                                                    echo get_string('nodata:qna', 'local_mypage');
                                                } else {
                                                    echo get_string('nocontent', 'local_jinoboard');
                                                } ?></td>
                            </tr>
                        <?php  } ?>
                    </tbody>
                </table>

                <?php
                if (count($contents)) {
                    $page_params = array();
                    $page_params['type'] = $type;
                    $page_params['perpage'] = $perpage;
                    $page_params['search'] = $search;
                    $page_params['searchfield'] = $searchfield;
                    $page_params['category'] = $category;
                    $VISANG->theme->table_pagination($CFG->wwwroot . "/local/job/my/qna/index.php", $page_params, ceil($total_pages / $perpage), $page);
                }
                ?>

            <?php
            } else if ($type == 3) {
                require_once $CFG->dirroot . '/local/job/my/qna/faq.php';
            }
            ?>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
    $(function() {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content",
            header: "ul",
            active: false
        });
        $('.div_taps').css('display', 'none');
        $('#information').css('display', 'block');
        $('#information_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu1_over.png"; ?>');
        var previous_menu = 'information_btn'
        var previous_menu_num = '1'
        $('#information_btn').click(function() {
            $('.div_taps').css('display', 'none');
            $('#information').css('display', 'block');
            $('#information_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu1_over.png"; ?>');
            if (previous_menu_num != '1') {
                $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
                previous_menu = 'information_btn'
                previous_menu_num = '1'
            }
        });
        $('#database_btn').click(function() {
            $('.div_taps').css('display', 'none');
            $('#database').css('display', 'block');
            $('#database_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu2_over.png"; ?>');
            if (previous_menu_num != '2') {
                $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
                previous_menu = 'database_btn'
                previous_menu_num = '2'
            }
        });
        $('#endnote_btn').click(function() {
            $('.div_taps').css('display', 'none');
            $('#endnote').css('display', 'block');
            $('#endnote_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu3_over.png"; ?>');
            if (previous_menu_num != '3') {
                $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
                previous_menu = 'endnote_btn'
                previous_menu_num = '3'
            }
        });
    });
    //	$('#accordion input[type="checkbox"]').click(function(e) {
    //		e.stopPropagation();
    //	});
</script>
<?php $VISANG->theme->footer(); ?>