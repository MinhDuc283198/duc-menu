<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles

$page = optional_param('page', 1, PARAM_INT);
$jobid = optional_param('jobid',-1, PARAM_INT);

$baseurl = new moodle_url($VISANG->wwwroot . '/my/apply.php', array('page' => $page, 'jobid'=>$jobid));


$perpage = 10;
$offset = ($page - 1) * $perpage;
$apply_list = $DB->get_records('vi_job_applications', array('user_id' => $USER->id), 'timecreated DESC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_job_applications', 'user_id=:userid', array('userid' => $USER->id), 'COUNT(*)');

$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

// =====================================================================================================
// renders
$VISANG->theme->title = get_string('apply', 'local_job');
$VISANG->theme->menu = 'myinfo';
$VISANG->theme->submenu = 'apply';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="crs-left-block">
            <?php include_once(dirname(__FILE__) . '/crs_left_block.php'); ?>
        </div>
        <div class="crs-right-block">
            <div role="main">
                <h2 class="pg-tit"><?php echo get_string('SupportStatus', 'local_job'); ?></h2>
                <table class="table m-block mb-4">
                    <thead>
                        <tr>
                            <th><?php echo get_string('Enterprise', 'local_job'); ?></th>
                            <th><?php echo get_string('SupportHistory', 'local_job'); ?></th>
                            <th><?php echo get_string('SupportDate', 'local_job'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($apply_list)) : ?>
                            <?php foreach ($apply_list as $apply) : ?>
                                <?php
                                $job = $DB->get_record('vi_jobs', array('id' => $apply->job_id));
                                $job = mkjobs_get_job_detail($job);
                                ?>
                                <tr <?php echo ($apply->job_id == $jobid) ? 'class="table-success"' : "";?>>
                                    <td class="text-center">
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $job->employer_id)); ?>">
                                            <?php echo $job->company_name; ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>">
                                            <?php echo $job->title; ?>
                                        </a>
                                        <div class="mb-1">
                                            <span class="t-gray"><?php echo date("Y\.m\.d", $job->enddate); ?> <?php echo get_string('DayEnd', 'local_job'); ?></span>
                                        </div>
                                    </td>
                                    <td>
                                        <?php echo mkjobs_notifi_time($apply->timecreated); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="3">
                                    <div class="no-data style02">
                                        <div><?php echo get_string('NoCompaniesSupported', 'local_job'); ?></div>
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/index.php'); ?>" class="btns point arrow"><?php echo get_string('Jobview', 'local_job'); ?></a>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <?php
                if (count($apply_list)) {
                    $VISANG->theme->table_pagination($baseurl, null, ceil($totalcount / $perpage), $page);
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
$VISANG->theme->footer();
