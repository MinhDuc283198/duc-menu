<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');

$id = optional_param('id',0, PARAM_INT);
$timeread = optional_param('timeread',0, PARAM_INT);
$userid = optional_param('userid',0, PARAM_INT);


if($id>0){

    if ($timeread>0) {
            $messagetable = 'message_read';
        } else {
            $messagetable = 'message';
    }

    $message = $DB->get_record($messagetable,array('id'=>$id));

    $value = message_delete_message($message, $userid);
} else {
    $notifications = local_mypage_get_message($userid);
    foreach($notifications as $no){
        if($no->timeread == 0){
            $no->timeread = null;
        }
       $value = message_delete_message($no, $userid);
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($value);
?>
