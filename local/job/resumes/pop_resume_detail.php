<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
// mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// require_sesskey();

$resumeid = required_param('resumeid', PARAM_INT);
$urlcv = required_param('urlcv', PARAM_TEXT);
$resume = $DB->get_record('vi_resumes', array('id' => $resumeid));
if ($resume == null) {
    throw new moodle_exception('invalidarguments');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/resumes/pop_resume_detail.php', array('resumeid' => $resumeid));

include_once($VISANG->dirroot . '/lib/resumes.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');

$baseurl = new moodle_url($VISANG->wwwroot . '/resumes/view.php', array('resumeid' => $resumeid));
$user = $DB->get_record('user', array('id' => $resume->user_id));
$resume_data = mkjobs_resume_get_fields($resumeid);

$lmsdata_user_query = 'select u.id, lu.address, lu.address_detail, lu.email2,lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
$lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $user->id));

$isdownload = optional_param('download', 0, PARAM_INT);
$lang = optional_param('lang', 'vi', PARAM_RAW);
$SESSION->forcelang = $lang;
?>
<?php if ($isdownload) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/oklassedu/style/style.css">
    </head>

    <body id="page-local-job-resumes-pop_resume_detail"
          class="format-site  path-local path-local-job path-local-job-resumes gecko dir-ltr lang-vi yui-skin-sam yui3-skin-sam visang-local pagelayout-popup course-1 context-1 jsenabled">
    <?php
    else :
        echo $OUTPUT->header();
    endif;
    ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous">
    <style>
        @media print {

            body,
            html {
                font-size: 13px;
            }

            .resume-inf-bx {
                padding: 15px 18px;
            }

            .resume-inf-bx.first .tit {
                font-size: 1.2rem;
                padding-bottom: 14px;
                margin-bottom: 15px;
            }

            .resume-inf-bx.first .u-img {
                width: 65px;
                height: 65px;
                margin-right: 15px;
            }

            .resume-inf-bx.first .txt > .rw span {
                width: 60px;
            }

            .resume-inf-bx .tit {
                font-size: 1rem;
                padding-bottom: 10px;
                margin-bottom: 10px;
            }

            .resume-inf-bx .txt > .rw {
                margin-bottom: 0.5rem;
            }

            .resume-inf-bx .rw .t-dot,
            .resume-inf-bx .rw .t-gray,
            .resume-inf-bx div.rw > div > p,
            .file-list > li,
            .my-box .rw > strong,
            .my-box .rw > p,
            .resume-inf-bx .rw .inline-b > span,
            .resume-inf-bx .txt > .rw strong,
            .resume-inf-bx .txt > .rw span {
                font-size: 0.8rem;
            }

            .mg-bt10 {
                margin-bottom: 5px;
            }

            .resume-inf-bx .bdg-area {
                padding: 18px 15px;
            }

            .resume-inf-bx .bdg-area > .t-blue02,
            .my-box .sub-tit {
                font-size: 1rem;
            }

            .file-list > li > a {
                padding-left: 15px;
                line-height: 15px;
                background-size: 13px;
            }

            .my-box {
                padding: 10px 15px 5px;
            }

            .my-box .sub-tit {
                padding-bottom: 14px;
            }

            .my-box .rw > strong {
                width: 60px;
                line-height: 1.5rem;
            }

            .my-box .rw > p {
                width: calc(100% - 60px);
                line-height: 1.5rem;
            }

        }
        /*#capture.resume-wrapper.position-relative {*/
        /*    transform-origin: left top;*/
        /*    transform: scale(0.8375);*/
        /*    width: fit-content;*/
        /*}*/
        /*div#capture.wrapper.mt-lg-5.row {*/
        /*    transform-origin: left top;*/
        /*    transform: scale(0.75);*/
        /*    width: fit-content;*/
        /*}*/
    </style>
    <div class="layerpop window style02">
        <?php if (!$isdownload) : ?>
            <div class="pop-title">
                <span class="pop-close" onclick="window.close()"><?php echo get_string('close', 'local_job'); ?></span>
            </div>
        <?php endif; ?>
        <!--        <div class="pop-contents">-->
        <!--            <div class="resume-inf-bx first">-->
        <!--                <div class="tit">-->
        <!--                    --><?php //echo $resume->title; ?>
        <!--                </div>-->
        <!--                <div class="u-img">-->
        <!--                    <img src="-->
        <?php //echo $resume->avatar == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg' : $CFG->wwwroot . '/pluginfile.php/' . $resume->avatar; ?><!--" alt="-->
        <?php //echo get_string('KimBisang', 'local_job'); ?><!--" />-->
        <!--                </div>-->
        <!--                <div class="txt">-->
        <!--                    <p class="rw">-->
        <!--                        <span>--><?php //echo get_string('name', 'local_job'); ?><!--</span>-->
        <!--                        <strong>--><?php //echo $user->firstname; ?><!--</strong>-->
        <!--                    </p>-->
        <!--                    <p class="rw">-->
        <!--                        <span>--><?php //echo get_string('dateofbirth', 'local_job'); ?><!--</span>-->
        <!--                        --><?php
        //                        $birth_array = explode('-', $user->phone2);
        //                        ?>
        <!--                        <strong>-->
        <?php //echo $birth_array[0] . '-' . $birth_array[1] . '-' . $birth_array[2]; ?><!--</strong>-->
        <!--                    </p>-->
        <!--                    <p class="rw">-->
        <!--                        <span>--><?php //echo get_string('e-mail', 'local_job'); ?><!--</span>-->
        <!--                        <strong>--><?php //echo $lmsdata_user->email2; ?><!--</strong>-->
        <!--                    </p>-->
        <!--                    <p class="rw">-->
        <!--                        <span>--><?php //echo get_string('MobilePhoneNumber', 'local_job'); ?><!--</span>-->
        <!--                        <strong>--><?php //echo $user->phone1; ?><!--</strong>-->
        <!--                    </p>-->
        <!--                    <p class="rw w100">-->
        <!--                        <span>--><?php //echo get_string('address', 'local_job'); ?><!--</span>-->
        <!--                        <strong>-->
        <!--                            --><?php //echo $lmsdata_user->zipcode; ?><!--<br />-->
        <!--                            --><?php //echo $lmsdata_user->address; ?>
        <!--                        </strong>-->
        <!--                    </p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">-->
        <?php //echo get_string('Workingconditions', 'local_job'); ?><!--</div>-->
        <!--                <div class="txt">-->
        <!--                    <div class="rw">-->
        <!--                        <p>-->
        <!--                            <span>-->
        <?php //echo get_string('EmploymentForm', 'local_job'); ?><!--</span>-->
        <!--                            <strong>-->
        <!--                                --><?php
        //                                if ($resume_data->hope_to_work && $resume_data->hope_to_work->type != '') {
        //                                    $work_type = $DB->get_record('vi_work_types', array('id' => $resume_data->hope_to_work->type));
        //                                    switch (current_language()) {
        //                                        case 'ko':
        //                                            echo $work_type->name_ko;
        //                                            break;
        //                                        case 'en':
        //                                            echo $work_type->name_en;
        //                                            break;
        //                                        case 'vi':
        //                                            echo $work_type->name_vi;
        //                                            break;
        //                                    }
        //                                }
        //                                ?>
        <!--                            </strong>-->
        <!--                        </p>-->
        <!---->
        <!--                        <p>-->
        <!--                            <span>--><?php //echo get_string('HopeSalary', 'local_job'); ?><!--</span>-->
        <!--                            <strong>-->
        <!--                                --><?php //if ($resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1') : ?>
        <!--                                    --><?php //echo get_string('Consultationduring', 'local_job'); ?>
        <!--                                --><?php //else : ?>
        <!--                                    -->
        <?php //echo $resume_data->hope_to_work ? number_format($resume_data->hope_to_work->salary_from) : ''; ?><!-- ~ -->
        <?php //echo $resume_data->hope_to_work ? number_format($resume_data->hope_to_work->salary_to) : ''; ?><!-- --><?php //echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_unit : '' ?>
        <!--                                --><?php //endif; ?>
        <!--                            </strong>-->
        <!--                        </p>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">--><?php //echo get_string('Education', 'local_job'); ?><!--</div>-->
        <!---->
        <!--                <div class="txt">-->
        <!--                    --><?php //if ($resume_data->education && is_array($resume_data->education->edu)) : ?>
        <!--                        --><?php //foreach ($resume_data->education->edu as $edu) : ?>
        <!--                            <div class="rw">-->
        <!--                                <p><span>--><?php //echo $edu->from_year; ?><!--.-->
        <?php //echo $edu->from_month; ?><!-- – --><?php //echo $edu->to_year; ?><!--.-->
        <?php //echo $edu->to_month; ?><!--</span></p>-->
        <!--                                <div class="history">-->
        <!--                                    <strong>-->
        <!--                                        --><?php
        //                                        switch ($edu->type) {
        //                                            case 'university':
        //                                                echo 'University';
        //                                                break;
        //                                            case 'college':
        //                                                echo 'College';
        //                                                break;
        //                                            case 'high_school':
        //                                                echo 'High School';
        //                                                break;
        //                                            default:
        //                                                echo get_string('school', 'local_job');
        //                                                break;
        //                                        }
        //                                        ?>
        <!--                                    </strong>-->
        <!--                                    <p class="inline-b">-->
        <!--                                        <span>--><?php //echo $edu->school_name; ?><!--</span>-->
        <!--                                        <span>--><?php //echo $edu->degree; ?><!--</span>-->
        <!--                                    </p>-->
        <!--                                </div>-->
        <!--                            </div>-->
        <!--                        --><?php //endforeach; ?>
        <!--                    --><?php //endif; ?>
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">--><?php //echo get_string('Career', 'local_job'); ?><!--</div>-->
        <!---->
        <!--                <div class="txt">-->
        <!--                    --><?php //if ($resume_data->experience && is_array($resume_data->experience->exp)) : ?>
        <!--                        --><?php //foreach ($resume_data->experience->exp as $exp) : ?>
        <!--                            <div class="rw bt-br">-->
        <!--                                <p><span>--><?php //echo $exp->from_year; ?><!--.-->
        <?php //echo $exp->from_month; ?><!-- – --><?php //echo $exp->to_year; ?><!--.-->
        <?php //echo $exp->to_month; ?><!--</span></p>-->
        <!--                                <div class="history">-->
        <!--                                    <p class="inline-b">-->
        <!--                                        <span>-->
        <!--                                            --><?php //if ($exp->in_office == 1) : ?>
        <!--                                                <em class="t-blue">[-->
        <?php //echo get_string('Office', 'local_job'); ?><!--]</em>-->
        <!--                                            --><?php //endif; ?>
        <!--                                            --><?php //echo $exp->company_name; ?>
        <!--                                        </span>-->
        <!--                                        <span>-->
        <!--                                            --><?php //echo $exp->department; ?>
        <!--                                            --><?php
        //                                            switch ($exp->position) {
        //                                                case 'divisiondirector':
        //                                                    echo '(' . get_string('resume:position:divisiondirector', 'local_job') . ')';
        //                                                    break;
        //                                                case 'divisionmanager':
        //                                                    echo '(' . get_string('resume:position:divisionmanager', 'local_job') . ')';
        //                                                    break;
        //                                                case 'manager':
        //                                                    echo '(' . get_string('resume:position:manager', 'local_job') . ')';
        //                                                    break;
        //                                                case 'teamleader':
        //                                                    echo '(' . get_string('resume:position:teamleader', 'local_job') . ')';
        //                                                    break;
        //                                                case 'staff':
        //                                                    echo '(' . get_string('resume:position:staff', 'local_job') . ')';
        //                                                    break;
        //                                            }
        //                                            ?>
        <!--                                        </span>-->
        <!--                                    </p>-->
        <!--                                    <div>-->
        <!--                                        <strong class="f18 mg-tp10">[-->
        <?php //echo get_string('MajorAchievements', 'local_job'); ?><!--]</strong>-->
        <!--                                        <div class="w-txt">-->
        <!--                                            --><?php //if (is_array($exp->exp_achievement)) : ?>
        <!--                                                --><?php //foreach ($exp->exp_achievement as $exp_achievement) : ?>
        <!--                                                    <p class="t-dot mg-bt10">-->
        <!--                                                        --><?php //echo $exp_achievement->title; ?>
        <!--                                                        (-->
        <?php //echo $exp_achievement->from_year . '.' . $exp_achievement->from_month . ' - ' . $exp_achievement->to_year . '.' . $exp_achievement->to_month; ?><!--)-->
        <!--                                                    </p>-->
        <!--                                                    <p class="t-gray mg-bt10">-->
        <?php //echo $exp_achievement->content; ?><!--</p>-->
        <!--                                                --><?php //endforeach; ?>
        <!--                                            --><?php //endif; ?>
        <!--                                        </div>-->
        <!--                                    </div>-->
        <!--                                </div>-->
        <!--                            </div>-->
        <!--                        --><?php //endforeach; ?>
        <!--                    --><?php //endif; ?>
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!---->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">-->
        <?php //echo get_string('TrainingCompletionCompletion', 'local_job'); ?><!--</div>-->
        <!--                --><?php //if ($resume_data->activity && is_array($resume_data->activity->act)) : ?>
        <!--                    --><?php //foreach ($resume_data->activity->act as $act) : ?>
        <!--                        --><?php //if ($act->type == 'activity') : ?>
        <!--                            <div class="txt">-->
        <!--                                <div class="rw">-->
        <!--                                    <p><span>--><?php //echo $act->from_year; ?><!--.-->
        <?php //echo $act->from_month; ?><!-- – --><?php //echo $act->to_year; ?><!--.-->
        <?php //echo $act->to_month; ?><!--</span></p>-->
        <!--                                    --><?php //if ($act->in_progress == 1) : ?>
        <!--                                        <div class="history">-->
        <!--                                            <p class="inline-b">-->
        <!--                                                <span>-->
        <!--                                                    <em class="t-blue">[-->
        <?php //echo get_string('in_progress', 'local_job'); ?><!--]</em>-->
        <!--                                                </span>-->
        <!--                                                <strong>-->
        <?php //echo $act->activity_name; ?><!--</strong>-->
        <!--                                            </p>-->
        <!--                                            <div>-->
        <!--                                                <p class="t-gray">-->
        <?php //echo $act->activity_content; ?><!--</p>    -->
        <!--                                            </div>-->
        <!--                                        </div>-->
        <!--                                    --><?php //else: ?>
        <!--                                        <div>-->
        <!--                                            <p class="mg-bt10">-->
        <?php //echo $act->activity_name; ?><!--</p>-->
        <!--                                            <p class="t-gray">-->
        <?php //echo $act->activity_content; ?><!--</p>-->
        <!--                                        </div>-->
        <!--                                    --><?php //endif; ?>
        <!--                                </div>-->
        <!--                            </div>-->
        <!--                        --><?php //endif; ?>
        <!--                        --><?php //if ($act->type == 'master_korean_course') : ?>
        <!--                            <div class="bdg-area">-->
        <!--                                <div class="t-blue02">-->
        <?php //echo $act->master_korean_title; ?><!--</div>-->
        <!--                                <table class="table">-->
        <!--                                    <colgroup>-->
        <!--                                        <col width="15%" />-->
        <!--                                        <col width="/" />-->
        <!--                                        <col width="15%" />-->
        <!--                                    </colgroup>-->
        <!--                                    <tbody>-->
        <!--                                        --><?php //if ($act->master_korean_course && is_array($act->master_korean_course)) : ?>
        <!--                                            --><?php //foreach ($act->master_korean_course as $mkc) : ?>
        <!--                                                --><?php //if ($mkc->courseid) : ?>
        <!--                                                    --><?php
        //                                                    $course = $DB->get_record('lmsdata_class', array('id' => $mkc->courseid));
        //                                                    $pro = format_lguplus_get_course_progress($course->courseid);
        //                                                    switch (current_language()) {
        //                                                        case 'ko':
        //                                                            $coursename = $course->title;
        //                                                            break;
        //                                                        case 'en':
        //                                                            $coursename = $course->en_title;
        //                                                            break;
        //                                                        case 'vi':
        //                                                            $coursename = $course->vi_title;
        //                                                            break;
        //                                                    }
        //                                                    ?>
        <!--                                                    <tr>-->
        <!--                                                        <td>-->
        <!--                                                            --><?php //if ($pro['course']->courseprogress < 100) : ?>
        <!--                                                                --->
        <!--                                                            --><?php //else : ?>
        <!--                                                                <img src="-->
        <?php //echo $CFG->wwwroot . '/theme/oklassedu/pix/images/ic_badge.png'; ?><!--" alt="" />-->
        <!--                                                            --><?php //endif; ?>
        <!--                                                        </td>-->
        <!--                                                        <td class="text-left">-->
        <!--                                                            <p>--><?php //echo $coursename; ?><!--</p>-->
        <!--                                                            <p class="t-gray">-->
        <?php //echo $mkc->coursedate; ?><!--</p>-->
        <!--                                                        </td>-->
        <!--                                                        <td>-->
        <?php //echo $pro['course']->courseprogress < 100 ? get_string('Proceeding', 'local_job') : get_string('Completion', 'local_job'); ?><!--</td>-->
        <!--                                                    </tr>-->
        <!--                                                --><?php //endif; ?>
        <!--                                            --><?php //endforeach; ?>
        <!--                                        --><?php //endif; ?>
        <!--                                    </tbody>-->
        <!--                                </table>-->
        <!--                            </div>-->
        <!--                        --><?php //endif; ?>
        <!--                    --><?php //endforeach; ?>
        <!--                --><?php //endif; ?>
        <!--            </div>-->
        <!---->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">--><?php //echo get_string('certificate', 'local_job'); ?><!--</div>-->
        <!--                <div class="txt">-->
        <!--                    <div class="rw">-->
        <!--                        <div class="w100">-->
        <!--                            --><?php //if ($resume_data->certification && is_array($resume_data->certification->cer)) : ?>
        <!--                                --><?php //foreach ($resume_data->certification->cer as $cer) : ?>
        <!--                                    <p class="w100 t-dot">-->
        <!--                                        <span>--><?php //echo $cer->title; ?><!-- (-->
        <?php //echo $cer->content; ?><!--)</span>-->
        <!--                                        <span>-->
        <?php //echo get_string('Issuer', 'local_job'); ?><!-- : -->
        <?php //echo $cer->certificate_authority; ?><!--</span>-->
        <!--                                        <span>-->
        <?php //echo get_string('Acquisitiondate', 'local_job'); ?><!-- : --><?php //echo $cer->date; ?><!--</span>-->
        <!--                                    </p>-->
        <!--                                --><?php //endforeach; ?>
        <!--                            --><?php //endif; ?>
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">--><?php //echo get_string('link', 'local_job'); ?><!--</div>-->
        <!--                <div class="txt">-->
        <!--                    <div class="rw">-->
        <!--                        --><?php //if ($resume_data->links && is_array($resume_data->links->link)) : ?>
        <!--                            --><?php //foreach ($resume_data->links->link as $link) : ?>
        <!--                                <p class="w100 t-dot">-->
        <!--                                    --><?php //echo $link->title; ?><!--<br />-->
        <!--                                    <a href="--><?php //echo $link->url; ?><!--" target="_blank">-->
        <?php //echo $link->url; ?><!--</a>-->
        <!--                                </p>-->
        <!--                            --><?php //endforeach; ?>
        <!--                        --><?php //endif; ?>
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="resume-inf-bx">-->
        <!--                <div class="tit">-->
        <?php //echo get_string('Documentaryevidence', 'local_job'); ?><!--</div>-->
        <!--                <div class="txt">-->
        <!--                    <div class="rw">-->
        <!--                        <ul class="file-list">-->
        <!--                            --><?php //if ($resume_data->attachment && is_array($resume_data->attachment->file)) : ?>
        <!--                                --><?php //foreach ($resume_data->attachment->file as $file) : ?>
        <!--                                    <li><a href="-->
        <?php //echo $CFG->wwwroot . '/pluginfile.php/' . $file->attachmenturl; ?><!--" target="_blank">-->
        <?php //echo $file->resumetitle; ?><!--</a></li>-->
        <!--                                --><?php //endforeach; ?>
        <!--                            --><?php //endif; ?>
        <!--                        </ul>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <style>-->
        <!--                .my-box.interestinformation .rw>strong {-->
        <!--                    width: 200px !important;-->
        <!--                }-->
        <!---->
        <!--                .my-box.interestinformation .rw>p {-->
        <!--                    width: calc(100% - 200px) !important;-->
        <!--                }-->
        <!--            </style>-->
        <!--            <div class="my-box interestinformation bg">-->
        <!--                <div class="sub-tit">-->
        <?php //echo get_string('Interestinformation', 'local_job'); ?><!--</div>-->
        <!--                <div class="rw">-->
        <!--                    <strong>-->
        <?php //echo get_string('my_inter:final_education', 'local_job'); ?><!--</strong>-->
        <!--                    <p>-->
        <!--                        --><?php
        //                        $sql = "SELECT w.name_ko, w.name_en, w.name_vi
        //                                FROM {vi_user_final_educations} u
        //                                JOIN {vi_final_educations} w ON w.id=u.final_education_id AND u.user_id=:userid";
        //                        $final_educations = $DB->get_record_sql($sql, array('userid' => $user->id));
        //                        switch (current_language()) {
        //                            case 'ko':
        //                                echo $final_educations->name_ko;
        //                                break;
        //                            case 'en':
        //                                echo $final_educations->name_en;
        //                                break;
        //                            case 'vi':
        //                                echo $final_educations->name_vi;
        //                                break;
        //                        }
        //                        ?>
        <!--                    </p>-->
        <!--                </div>-->
        <!---->
        <!--                <div class="rw">-->
        <!--                    <strong>-->
        <?php //echo get_string('my_inter:work_experience', 'local_job'); ?><!--</strong>-->
        <!--                    <p>-->
        <!--                        --><?php
        //                        // $lang = current_language();
        //                        $sql = "SELECT w.name, w.name_en, w.name_vi
        //                                FROM {vi_user_work_experiences} u
        //                                JOIN {vi_work_experiences} w ON w.id=u.work_experience_id AND u.user_id=:userid";
        //                        $work_experience = $DB->get_record_sql($sql, array('userid' => $user->id));
        //                        switch (current_language()) {
        //                            case 'ko':
        //                                echo $work_experience->name;
        //                                break;
        //                            case 'en':
        //                                echo $work_experience->name_en;
        //                                break;
        //                            case 'vi':
        //                                echo $work_experience->name_vi;
        //                                break;
        //                        }
        //                        ?>
        <!--                    </p>-->
        <!--                </div>-->
        <!--                <div class="rw">-->
        <!--                    <strong>-->
        <?php //echo get_string('my_inter:skill_group', 'local_job'); ?><!--</strong>-->
        <!--                    <p>-->
        <!--                        --><?php
        //                        $groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
        //                        $skill_groups = array();
        //                        foreach ($groups as $group) {
        //                            $skill_groups[] = $group->name;
        //                        }
        //                        echo implode(", ", $skill_groups);
        //                        ?>
        <!--                    </p>-->
        <!--                </div>-->
        <!--                <div class="rw">-->
        <!--                    <strong>--><?php //echo get_string('my_inter:skill', 'local_job'); ?><!--</strong>-->
        <!--                    <p>-->
        <!--                        --><?php
        //                        $skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
        //                        $user_skills = array();
        //                        foreach ($skills as $skill) {
        //                            $user_skills[] = $skill->name;
        //                        }
        //                        echo implode(", ", $user_skills);
        //                        ?>
        <!--                    </p>-->
        <!--                </div>-->
        <!--                <div class="rw">-->
        <!--                    <strong>-->
        <?php //echo get_string('my_inter:workplace', 'local_job'); ?><!--</strong>-->
        <!--                    <p>-->
        <!--                        --><?php
        //                        $districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $user->id));
        //                        $workplaces = array();
        //                        foreach ($districts as $district) {
        //                            $workplaces[] = $district->name;
        //                        }
        //                        echo implode(", ", $workplaces);
        //                        ?>
        <!--                    </p>-->
        <!--                </div>-->
        <!--                <div class="rw">-->
        <!--                    <strong>-->
        <?php //echo get_string('my_inter:korean_level', 'local_job'); ?><!--</strong>-->
        <!--                    <p>-->
        <!--                        --><?php
        //                        $sql = "SELECT l.*
        //                                FROM {vi_user_korean_levels} u
        //                                JOIN {vi_korean_levels} l ON l.id=u.korean_level_id AND u.user_id=:userid";
        //                        $korean_level = $DB->get_record_sql($sql, array('userid' => $user->id));
        //                        switch (current_language()) {
        //                            case 'ko':
        //                                echo $korean_level->name_ko;
        //                                break;
        //                            case 'en':
        //                                echo $korean_level->name_en;
        //                                break;
        //                            case 'vi':
        //                                echo $korean_level->name_vi;
        //                                break;
        //                        }
        //                        ?>
        <!--                    </p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <?php
        include("$urlcv");
        ?>
        <style>
            .width380{
                width: 380px;
            }
            .width176{
                width: 176px;
            }
            .width338{
                width: 338px;
            }
            .avatar{
                width: 10vw;
                height: 10vw;
                min-height: unset;
            }
            .image{
                padding: 0.5vw;
                height: 11vw;
                width: 11vw;
            }
            .height-jquery{
                margin-left: 15px;
            }
        </style>
    </div>

    <?php if ($isdownload) : ?>
    </body>

    </html>
<?php else : ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.1/jspdf.umd.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        // $(function() {
        //     window.print();
        //     // window.close();
        // });
        window.html2canvas = html2canvas;
        window.jsPDF = window.jspdf.jsPDF;
        window.onload = function () {
            $("#userImage").attr("src", '<?php echo $resume->avatar == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg' : $CFG->wwwroot . '/pluginfile.php/' . $resume->avatar; ?>');
            $("#name").html('<?php echo $user->firstname; ?>');
            $("#dateOfBirth").html('<?php echo $user->phone2; ?>');
            $("#email2").html('<?php echo $lmsdata_user->email2; ?>');
            $("#phone").html('<?php echo $user->phone1; ?>');
            $("#address").html('<?php echo $lmsdata_user->address; ?>');
            $("#employment").html('<?php
                if ($resume_data->hope_to_work && $resume_data->hope_to_work->type != '') {
                    $work_type = $DB->get_record('vi_work_types', array('id' => $resume_data->hope_to_work->type));
                    switch (current_language()) {
                        case 'ko':
                            echo $work_type->name_ko;
                            break;
                        case 'en':
                            echo $work_type->name_en;
                            break;
                        case 'vi':
                            echo $work_type->name_vi;
                            break;
                    }
                }
                ?>');
            $("#hopeSalary").html("<?php if ($resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1') : ?>
            <?php echo get_string('Consultationduring', 'local_job'); ?>
            <?php else : ?>
            <?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_from : ''; ?> ~ <?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_to : ''; ?> <?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_unit : '' ?>
                <?php endif; ?>");

            <?php if ($resume_data->education && is_array($resume_data->education->edu)) : ?>
            var count = 1;
            <?php foreach ($resume_data->education->edu as $edu) : ?>
            var idx = (new Date().valueOf()) + count;
            var previewHtml = $("#education-field").html();
            previewHtml = previewHtml.replaceAll("_idx", 'preview-' + idx);
            $(previewHtml).appendTo("#education-preview .education-area");
            $("#preview-" + idx + " #educationName").html("<?php
                switch ($edu->type) {
                    case 'university':
                        echo 'University';
                        break;
                    case 'college':
                        echo 'College';
                        break;
                    case 'high_school':
                        echo 'High School';
                        break;
                    default:
                        echo get_string('school', 'local_job');
                        break;
                }
                ?>" + ": " + "<?php echo $edu->school_name; ?>");

            $("#preview-" + idx + " #educationFromTo").html("<?php echo $edu->from_month; ?>" + "/" + "<?php echo $edu->from_year; ?>" + " - " + "<?php echo $edu->to_month; ?>" + "/" + "<?php echo $edu->to_year; ?>");
            $("#preview-" + idx + " #educationMajor").html("Major: " + "<?php echo $edu->degree; ?>");
            count++;
            <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($resume_data->experience && is_array($resume_data->experience->exp)) : ?>
            var count = 1;
            <?php foreach ($resume_data->experience->exp as $exp) : ?>
            var idx = (new Date().valueOf()) + count;
            var previewHtml = $("#experience-field").html();
            previewHtml = previewHtml.replaceAll("_idx", 'preview-' + idx);
            $(previewHtml).appendTo("#experience-preview .experience-area");
            $("#preview-" + idx + " #experienceName").html('<?php echo $exp->company_name; ?>');
            $("#preview-" + idx + " #experienceMajor").html("<?php echo $exp->department; ?>" + ": " + "<?php
                switch ($exp->position) {
                    case 'divisiondirector':
                        echo '' . get_string('resume:position:divisiondirector', 'local_job') . '';
                        break;
                    case 'divisionmanager':
                        echo '' . get_string('resume:position:divisionmanager', 'local_job') . '';
                        break;
                    case 'manager':
                        echo '' . get_string('resume:position:manager', 'local_job') . '';
                        break;
                    case 'teamleader':
                        echo '' . get_string('resume:position:teamleader', 'local_job') . '';
                        break;
                    case 'staff':
                        echo '' . get_string('resume:position:staff', 'local_job') . '';
                        break;
                }
                ?>");
            $("#preview-" + idx + " #experienceFromTo").html("<?php echo $exp->from_month; ?>" + "/" + "<?php echo $exp->from_year; ?>" + " - " + "<?php echo $exp->to_month; ?>" + "/" + "<?php echo $exp->to_year; ?>");

            $("#preview-" + idx + " #experienceAttend").html("<?php if ($exp->in_office == 1) : ?>
                <?php echo get_string('Office', 'local_job'); ?>
            <?php endif; ?>");

            <?php if (is_array($exp->exp_achievement)) : ?>
            var countSub = 1;
            <?php foreach ($exp->exp_achievement as $exp_achievement) : ?>
            var subidx = (new Date().valueOf()) + countSub;
            var previewHtml = $("#sub-experience-field").html();
            previewHtml = previewHtml.replaceAll("_subidx", 'preview-' + subidx);
            $(previewHtml).appendTo("#preview-" + idx + " .sub-experience-area");
            $("#preview-" + subidx + " #sub_experienceName").html('<?php echo $exp_achievement->title; ?>');
            $("#preview-" + subidx + " #sub_experienceDescription").html('<?php echo $exp_achievement->content; ?>');
            $("#preview-" + subidx + " #sub_experienceFromTo").html("<?php echo $exp_achievement->from_month; ?>" + "/" + "<?php echo $exp_achievement->from_year; ?>" + " - " + "<?php echo $exp_achievement->to_month; ?>" + "/" + "<?php echo $exp_achievement->to_year; ?>");
            countSub++;
            <?php endforeach; ?>
            <?php endif; ?>
            count++;
            <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($resume_data->activity && is_array($resume_data->activity->act)) : ?>
            var count = 1;
            <?php foreach ($resume_data->activity->act as $act) : ?>
            <?php if ($act->type == 'activity') : ?>
            var idx = (new Date().valueOf()) + count;
            var previewHtml = $("#activity-field").html();
            previewHtml = previewHtml.replaceAll("_idx", 'preview-' + idx);
            $(previewHtml).appendTo("#activity-preview .activity-area");

            $("#preview-" + idx + " #activityName").html('<?php echo $act->activity_name; ?>');
            $("#preview-" + idx + " #activityFromTo").html("<?php echo $act->from_month; ?>" + "/" + "<?php echo $act->from_year; ?>" + " - " + "<?php echo $act->to_month; ?>" + "/" + "<?php echo $act->to_year; ?>");
            $("#preview-" + idx + " #activityAttend").html("<?php if ($act->in_progress == 1) : ?><?php echo get_string('in_progress', 'local_job'); ?><?php endif; ?>");
            $("#preview-" + idx + " #activityDescription").html('<?php echo $act->activity_content; ?>');
            count++;
            <?php endif; ?>
            <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($resume_data->certification && is_array($resume_data->certification->cer)) : ?>
            var count = 1;
            <?php foreach ($resume_data->certification->cer as $cer) : ?>
            var idx = (new Date().valueOf()) + count;
            var previewHtml = $("#certification-field").html();
            previewHtml = previewHtml.replaceAll("_idx", 'preview-' + idx);
            $(previewHtml).appendTo("#certification-preview .certification-area");

            $("#preview-" + idx + " #certificationName").html('<?php echo $cer->title; ?>');
            $("#preview-" + idx + " #certificationDescription").html('<?php echo $cer->content; ?>');
            $("#preview-" + idx + " #certificationMajor").html('<?php echo $cer->certificate_authority; ?>');
            $("#preview-" + idx + " #certificationFromToType").html('<?php echo $cer->date; ?>');
            count++;
            <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($resume_data->links && is_array($resume_data->links->link)) : ?>
            var count = 1;
            <?php foreach ($resume_data->links->link as $link) : ?>
            var idx = (new Date().valueOf()) + count;
            var previewHtml = $("#links-field").html();
            previewHtml = previewHtml.replaceAll("_idx", 'preview-' + idx);
            $(previewHtml).appendTo("#links-preview .links-area");

            $("#preview-" + idx + " #linksName").html('<?php echo $link->title; ?>');
            $("#preview-" + idx + " #linksDescription").html('<?php echo $link->url; ?>');
            count++;
            <?php endforeach; ?>
            <?php endif; ?>

            $("#finalEducation").html("<?php
                $sql = "SELECT w.name_ko, w.name_en, w.name_vi
						FROM {vi_user_final_educations} u
						JOIN {vi_final_educations} w ON w.id=u.final_education_id AND u.user_id=:userid";
                $final_educations = $DB->get_record_sql($sql, array('userid' => $user->id));
                switch (current_language()) {
                    case 'ko':
                        echo $final_educations->name_ko;
                        break;
                    case 'en':
                        echo $final_educations->name_en;
                        break;
                    case 'vi':
                        echo $final_educations->name_vi;
                        break;
                }
                ?>");

            $("#experienceWork").html("<?php
                // $lang = current_language();
                $sql = "SELECT w.name, w.name_en, w.name_vi
						FROM {vi_user_work_experiences} u
						JOIN {vi_work_experiences} w ON w.id=u.work_experience_id AND u.user_id=:userid";
                $work_experience = $DB->get_record_sql($sql, array('userid' => $user->id));
                switch (current_language()) {
                    case 'ko':
                        echo $work_experience->name;
                        break;
                    case 'en':
                        echo $work_experience->name_en;
                        break;
                    case 'vi':
                        echo $work_experience->name_vi;
                        break;
                }
                ?>");

            $("#skillGroups").html("<?php
                $groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
                $skill_groups = array();
                foreach ($groups as $group) {
                    $skill_groups[] = $group->name;
                }
                echo implode(", ", $skill_groups);
                ?>");

            $("#skills").html("<?php
                $skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
                $user_skills = array();
                foreach ($skills as $skill) {
                    $user_skills[] = $skill->name;
                }
                echo implode(", ", $user_skills);
                ?>");

            $("#workplaces").html("<?php
                $districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $user->id));
                $workplaces = array();
                foreach ($districts as $district) {
                    $workplaces[] = $district->name;
                }
                echo implode(", ", $workplaces);
                ?>");

            $("#koreanLanguageLevel").html("<?php
                $sql = "SELECT l.*
						FROM {vi_user_korean_levels} u
						JOIN {vi_korean_levels} l ON l.id=u.korean_level_id AND u.user_id=:userid";
                $korean_level = $DB->get_record_sql($sql, array('userid' => $user->id));
                switch (current_language()) {
                    case 'ko':
                        echo $korean_level->name_ko;
                        break;
                    case 'en':
                        echo $korean_level->name_en;
                        break;
                    case 'vi':
                        echo $korean_level->name_vi;
                        break;
                }
                ?>");

            var divHeight = $('#capture').height();

            // var divHeight = 681;
            // var divWidth = $('#capture').width();
            // var divWidth = 473;
            // var divWidth = 495;
            var divWidth = 960;

            var ratio = divHeight / divWidth;
            html2canvas(document.querySelector("#capture"), {
                allowTaint: true,
                useCORS: true,
                scale: 2,
                height: divHeight,
                width: divWidth,
            }).then(canvas => {
                var doc = new jsPDF({
                    orientation: 'portrait',
                });
                var image = canvas.toDataURL("image/png");
                var doc = new jsPDF(); // using defaults: orientation=portrait, unit=mm, size=A4
                var width = doc.internal.pageSize.getWidth();

                var height = doc.internal.pageSize.getHeight();

                height = ratio * width;

                doc.addImage(image, 'PNG', 0, 0, width, height);
                // doc.addImage(image, 'PNG', 0, 0, 450, 600);
                y = -(doc.internal.pageSize.getHeight());
                // y = 643;
                var numberPage = (height / doc.internal.pageSize.getHeight()) - 1;
                for (let i = 0; i < numberPage; i++) {
                    doc.addPage();

                    doc.addImage(image, 'PNG', 0, y, width, height);
                    y -= doc.internal.pageSize.getHeight();
                }
                doc.save();
            });
        };
    </script>
    <?php
    echo $OUTPUT->footer();
endif;
