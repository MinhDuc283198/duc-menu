<?php
require(dirname(dirname(dirname(__FILE__))) . '/../config.php');
global $USER ,$DB;
$id = $_POST["id"];
$resume_id = $_POST["resume_id"];
$sql="UPDATE {vi_resumes} com
                        SET com.resume_cv_id = $id
                        WHERE com.id=$resume_id";
$DB->execute($sql);

if ($id == 1) {
    echo '<link rel="stylesheet" href="assets/vendors/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.css">

<style>
    ol, ul {
         padding-left: 0;
    }
</style>

<div class="wrapper flex scale-cv" id="capture" style="max-width: 960px;margin: auto;">
    <div class="sidebar" style="display: flex;">


        <!-- CỘT TRÁI -->
        <div class="column_left" style="width: 320px;background-color: #2B3848;font-family: sans-serif;">

            <div class="avatar" style="height: 230px;width: 230px;margin: 32px auto;border: 4px solid #ffffff" >
                <img src="" width="215" height="215" alt="Md Alamin Mir" style="margin: 3%;" id="userImage" />
            </div><!-- .avatar -->


            <!-- HÀNG2 -->
            <h1 style="    font-size: 32px;COLOR: #F8C16B;text-align: center;" id="name"></h1>
            <div class="uidesign" style="background-color: #889AAB;" >
                <h1 style="width: fit-content;width: fit-content;padding: 10px;
margin: 0 auto;color: white;font-size: 24px;" id="employment"></h1>
            </div>

            <div class="module" style="margin-top: 36px;">
                <h3 class="module__title">
                    <i class="fa fa-address-book-o circle circle--medium"></i>

                </h3>
                <div class="module__content">
                    <ul style="list-style:none">
                        <div class="1" style="display: flex;">
                            <div style="    height: 32px;width: 56px;background-color: #F7C16B;">
                                <img src="cv_templates/CV_template_01/aaa.png">
                            </div>
                            <li class="flex">
                                <div style="    font-size: 17px;font-weight: 700;color: #F7C16B;padding: 6% 20%;">PROFILE</div>
                            </li>
                        </div>

                        <div class="1" style="display: flex;margin-top: 20px;">
                            <img src="cv_templates/CV_template_01/2.png" alt="" style="height: 25px;">
                            <li class="flex" style="    font-size: 17px;color: white;">
                                <div style="padding-left: 10px;word-break: break-all;" id="phone"></div>
                            </li>
                        </div>
                        <div class="1" style="display: flex;margin-top: 20px;">
                            <img src="cv_templates/CV_template_01/3.png" alt="" style="height: 25px;">
                            <li class="flex">
                                <div style="padding-left: 10px;color: white;font-size: 17px;"><span id="dateOfBirth"></span></div>
                            </li>
                        </div>
                        <div class="1" style="display: flex;margin-top: 20px;">
                            <img src="cv_templates/CV_template_01/4.png" alt="" style="height: 25px;">
                            <li class="flex">
                                <div style="padding-left: 10px;font-size: 17px;color:white;word-break: break-all;" id="email2"></div>
                            </li>
                        </div>
                        <div class="1" style="display: flex;margin-top: 20px;">
                            <img src="cv_templates/CV_template_01/5.png" alt="" style="height: 25px;">
                            <li class="flex">
                                <div style="    font-size: 17px;color: white;padding-left: 10px;word-break: break-word;" id="address"></div>
                            </li>
                        </div>
                    </ul>
                </div>
            </div><!-- End .module #2 -->


            <div class="1" style="display: flex;margin: 36PX 0PX 0PX;">
                <div style="    height: 32px;width: 56px;background-color: #F7C16B;">
                    <img src="cv_templates/CV_template_01/bbb.png" alt="" style="padding: 5%;">
                </div>
                <li class="flex" style="list-style: none;">
                    <div style="    font-size: 17px;font-weight: 700;color: #F7C16B;padding: 4% 20%;">SALARY</div>
                </li>
            </div>
            <P style="    font-size: 17px;
			margin: 10px;
			color: white;
			LINE-HEIGHT: 1.5;" id="hopeSalary"></P>



            <div class="skill" >
                <h3 class="module__skill">
                    <i class="fa fa-address-book-o circle circle--medium"></i>

                </h3>
                <div class="1" style="display: flex;margin: 36PX 0PX 0PX;">
                    <div style="    height: 32px;width: 56px;background-color: #F7C16B;">
                        <img src="cv_templates/CV_template_01/ccc.png" style="padding: 5%;">
                    </div>
                    <li class="flex" style="list-style: none;">
                        <div style="    font-size: 17px;font-weight: 700;color: #F7C16B;padding: 8% 12%;">INFORMATION</div>
                    </li>
                </div>
                </li>
                <li class="flex" style="list-style: none;    margin: 10px;
						font-size: 17px;
						color: white;">
                    <div style="margin-bottom: 12px;">Final Education</div>
                    <!-- WORDS -->
                    <div class="radio_word" style="display: flex;">
                        <p style="color: #F2F2F2" id="finalEducation"></p>
                    </div>



                </li>
                <li class="flex" style="list-style: none;    margin: 10px;
						font-size: 17px;
						color: white;">
                    <div style="margin-bottom: 12px;">Experience</div>
                    <!-- WORDS -->
                    <div class="radio_word" style="display: flex;">
                        <p style="color: #F2F2F2" id="experienceWork"></p>
                    </div>



                </li>
                <li class="flex" style="list-style: none;    margin: 10px;
						font-size: 17px;
						color: white;">
                    <div style="margin-bottom: 12px;">Skill groups</div>
                    <!-- WORDS -->
                    <div class="radio_word" style="display: flex;">
                        <p style="color: #F2F2F2" id="skillGroups"></p>
                    </div>



                </li>
                <li class="flex" style="list-style: none;    margin: 10px;
						font-size: 17px;
						color: white;">
                    <div style="margin-bottom: 12px;">Skills</div>
                    <!-- WORDS -->
                    <div class="radio_word" style="display: flex;">
                        <p style="color: #F2F2F2" id="skills"></p>
                    </div>



                </li>
                <li class="flex" style="list-style: none;    margin: 10px;
						font-size: 17px;
						color: white;">
                    <div style="margin-bottom: 12px;">Workplaces</div>
                    <!-- WORDS -->
                    <div class="radio_word" style="display: flex;">
                        <p style="color: #F2F2F2" id="workplaces"></p>
                    </div>



                </li>
                <li class="flex" style="list-style: none;    margin: 10px;
						font-size: 17px;
						color: white;">
                    <div style="margin-bottom: 12px;">Korean Language Level</div>
                    <!-- WORDS -->
                    <div class="radio_word" style="display: flex;">
                        <p style="color: #F2F2F2" id="koreanLanguageLevel"></p>
                    </div>



                </li>
                </ul>
            </div>
        </div><!-- End .module #2 -->


        <!-- PHẢI -->

        <div class="column_right" style="font-family: sans-serif;background-color: #F2F2F2;
			width: 640px;">



            <!-- HÀNG1 -->
            <div class="h1-colume2" style="display: flex;margin: 32px 24px 0px 10px;">
                <div style="background-color: #2B3848;height: 40px;width: 40px;">
                    <img src="cv_templates/CV_template_01/new2.png" alt="" style="width: fit-content;padding: 6px 0px 0px 10px;;">
                </div>
                <h1 style="margin: 0;padding: 6px 0px 0px 24px;color: #2B3848;font-size: 24px;">EDUCATION</h1>
            </div>

            <div id="education-preview">
                <div class="education-area">

                </div>
            </div>

            <template id="education-field">
                <div id="_idx">
                    <ul style="list-style:none; display: flex; justify-content: space-between; margin: 1rem 0 0;">
                        <li class="flex" style="    font-size: 21px;
						  font-weight: 700;
						  color: #2B3848;
                          margin-left: 10px">
                            <div id="educationName"></div>
                        </li>
                        <li class="flex" style="    font-size: 17px;
						  color: #555555;
						  margin-top: 6px;">
                            <div id="educationFromTo"></div>
                        </li>
                    </ul>

                    <ul style="list-style:none; display: flex; justify-content: space-between;">
                        <li style="    list-style: none;
						  font-size: 17px;
						  color: #555555;
						  margin-left: 10px;
						  font-weight: 700;" id="educationMajor"></li>
                        <li style="    list-style: none;
						  font-size: 17px;
						  color: #555555;" id="educationAttend"></li>
                    </ul>
                </div>
            </template>


            <!-- HÀNG2 -->

            <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 10px;">
                <div style="background-color: #2B3848;height: 40px;width: 40px;">
                    <img src="cv_templates/CV_template_01/new3.png" alt="" style="width: fit-content;padding: 6px 0px 0px 7px;;">
                </div>
                <h1 style="margin: 0;padding: 6px 0px 0px 24px;color: #2B3848;font-size: 24px;">WORK EXPERIENCE</h1>
            </div>

            <div id="experience-preview">
                <div class="experience-area">

                </div>
            </div>

            <template id="experience-field">
                <div id="_idx">
                    <ul style="list-style:none; display: flex; justify-content: space-between; margin: 1rem 0 0;">
                        <li class="flex" style="    font-size: 21px;
			                font-weight: 700;
			                color: #254670;
                            margin-left: 10px;">
                            <div id="experienceName"></div>
                        </li>
                        <li class="flex" style="    font-size: 17px;
			                color: #555555;
			                margin-top: 6px;">
                            <div id="experienceFromTo"></div>
                        </li>
                    </ul>
                    <ul style="list-style:none; display: flex; justify-content: space-between; margin: 0;">
                        <li style="    list-style: none;
			            font-size: 17px;
			            color: #555555;
			            margin-left: 10px;
			            font-weight: 700;" id="experienceMajor"></li>
                        <li style="    list-style: none;
			            font-size: 17px;
			            color: #555555;" id="experienceAttend"></li>
                    </ul>
                    <div style="list-style: none;
			            color: #b2b2b2;
			            font-size: 17px;
			            margin-left: 10px;
			            margin-top: 5px;
			            line-height: 1.5;" class="sub-experience-area"></div>
                </div>
            </template>
            <template id="sub-experience-field">
                <div id="_subidx">
                    <ul style="list-style:none; display: flex; justify-content: space-between; margin: 0;">
                        <li style="font-size: 16px;" id="sub_experienceName"></li>
                        <li style="font-size: 15px;" id="sub_experienceFromTo"></li>
                    </ul>
                    <li style="display: flex;" id="sub_experienceDescription"></li>
                </div>
            </template>

            <!-- HÀNG3 -->

            <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 10px;">
                <div style="background-color: #2B3848;height: 40px;width: 40px;">
                    <img src="cv_templates/CV_template_01/4a.png" alt="" style="width: fit-content;padding: 0px 0px 0px 8px;;">
                </div>
                <h1 style="margin: 0;padding: 6px 0px 0px 24px;color: #2B3848;font-size: 24px;">ACTIVITIES</h1>
            </div>

            <div id="activity-preview">
                <div class="activity-area">

                </div>
            </div>

            <template id="activity-field">
                <div id="_idx">
                    <ul style="list-style:none; display: flex; justify-content: space-between; margin: 1rem 0 0;">
                        <li class="flex" style="    font-size: 21px;
			  font-weight: 700;
			  color: #2B3848;
              margin-left: 10px;">
                            <div id="activityName"></div>
                        </li>
                        <li class="flex" style="    font-size: 17px;
			  color: #555555;
			  margin-top: 6px;">
                            <div id="activityFromTo"></div>
                        </li>
                    </ul>
                    <li style="    list-style: none;
			  font-size: 17px;
			  color: #555555;
			  margin-left: 10px;
			  font-weight: 700;" id="activityAttend"></li>
                    <div style="list-style: none;
			  color: #b2b2b2;
			  font-size: 17px;
			  margin-left: 10px;
			  line-height: 1.5;">
                        <li style="display: flex;" id="activityDescription"></li>
                    </div>
                </div>
            </template>

            <!-- HÀNG4 -->


            <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 10px;">
                <div style="background-color: #2B3848;height: 40px;width: 40px;">
                    <img src="cv_templates/CV_template_01/new1.png" alt="" style="width: fit-content;padding: 6px 0px 0px 10px;;">
                </div>
                <h1 style="margin: 0;padding: 6px 0px 0px 24px;color: #2B3848;font-size: 24px;">CERTIFICATIONS</h1>
            </div>

            <div id="certification-preview">
                <div class="certification-area">

                </div>
            </div>

            <template id="certification-field">
                <div id="_idx">
                    <div style="margin-top: 24px;">
                        <ul style="list-style:none; display: flex; justify-content: space-between; margin: 0;">
                            <li class="flex" style="    font-size: 19px;
							  font-weight: 600;
							  color: #2B3848;
                              margin-left: 10px;">
                                <div style="" id="certificationName"></div>
                            </li>
                            <li class="flex" style="    font-size: 17px;
							  color: #555555;
							  margin-top: 6px;">
                                <div style="margin-top: -5px;" id="certificationFromToType"></div>
                            </li>
                        </ul>
                        <ul style="list-style:none;">
                            <li class="flex" style="    font-size: 17px;
							  font-weight: 500;
							  color: #2B3848;
                              margin-left: 10px;">
                                <div style="" id="certificationDescription"></div>
                            </li>
                            <li class="flex" style="    font-size: 15px;
							  font-weight: 400;
							  color: #2B3848;
                              margin-left: 10px;">
                                <div style="" id="certificationMajor"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </template>

            <!--            HÀNG5-->

            <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 10px;">
                <div style="background-color: #2B3848;height: 40px;width: 40px;">
                    <img src="cv_templates/CV_template_01/5.png" alt="" style="width: fit-content;padding: 5px;">
                </div>
                <h1 style="margin: 0;padding: 6px 0px 0px 24px;color: #2B3848;font-size: 24px;">LINKS</h1>
            </div>

            <div id="links-preview">
                <div class="links-area">

                </div>
            </div>

            <template id="links-field">
                <div id="_idx">
                    <div style="margin-top: 24px;">
                        <ul style="list-style:none;">
                            <li class="flex" style="    font-size: 19px;
							  font-weight: 500;
							  color: #2B3848;
                              margin-left: 10px;">
                                <div style="" id="linksName"></div>
                            </li>
                            <li class="flex" style="    font-size: 17px;
							  font-weight: 400;
							  color: #2B3848;
                              margin-left: 10px;">
                                <div style="" id="linksDescription"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </template>

        </div>
    </div>
</div>';
}

if ($id == 2) {
    echo '<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="cv_templates/CV_template_02/CurriculumVitae.css">

<div class="wrapper mt-lg-5 row" id="capture">
    <div class="main-wrapper col-8">

        <section class="section summary-section" style="margin-bottom: 40px;">
            <h2 class="section-title"><span class="icon-holder"><i class="fas fa-graduation-cap"
                                                                   style="padding-top: 6px;"></i></span>Education
            </h2>

            <div id="education-preview">
                <div class="education-area">

                </div>
            </div>

            <template id="education-field">
                <div id="_idx">
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">
                                <h3 class="job-title" id="educationName"></h3>
                                <div class="time" id="educationFromTo"></div>
                            </div><!--//upper-row-->
                            <div class="upper-row">
                                <div class="company" style="font-size: 14px;" id="educationMajor"></div>
                                <div class="time" id="educationAttend"></div>
                            </div><!--//upper-row-->
                        </div><!--//meta-->
                    </div><!--//item-->
                </div>
            </template>
        </section><!--//section-->

        <section class="section experiences-section" style="margin-bottom: 40px;">
            <h2 class="section-title"><span class="icon-holder"><i class="fas fa-briefcase"
                                                                   style="padding-top: 7px;"></i></span>Experiences</h2>

            <div id="experience-preview">
                <div class="experience-area">

                </div>
            </div>

            <template id="experience-field">
                <div id="_idx">
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">
                                <h3 class="job-title" id="experienceName"></h3>
                                <div class="time" id="experienceFromTo"></div>
                            </div><!--//upper-row-->
                            <div class="upper-row">
                                <div class="company" style="font-size: 14px;" id="experienceMajor"></div>
                                <div class="time" id="experienceAttend"></div>
                            </div><!--//upper-row-->
                        </div><!--//meta-->
                        <div class="sub-experience-area">

                        </div>
                    </div><!--//item-->
                </div>
            </template>
            <template id="sub-experience-field">
                <div id="_subidx">
                    <div class="details">
                        <div class="upper-row">
                            <h3 class="job-title" id="sub_experienceName" style="font-size: 13px;color: #2d7788;"></h3>
                            <div class="time" id="sub_experienceFromTo" style="font-size: 12px;color: #545E6C;"></div>
                        </div><!--//upper-row-->
                        <p style="font-size: 12px;overflow: hidden;" id="sub_experienceDescription"></p>
                    </div><!--//details-->
                </div>
            </template>
        </section><!--//section-->

        <section class="section projects-section" style="margin-bottom: 40px;">
            <h2 class="section-title"><span class="icon-holder"><i class="fas fa-medal"
                                                                   style="padding-top: 8px;"></i></span>Activities</h2>

            <div id="activity-preview">
                <div class="activity-area">

                </div>
            </div>

            <template id="activity-field">
                <div id="_idx">
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">
                                <h3 class="job-title" id="activityName"></h3>
                                <div class="time" id="activityFromTo"></div>
                            </div><!--//upper-row-->
                            <div class="upper-row">
                                <div class="company" style="font-size: 14px;" id="activityAttend"></div>
                                <p style="font-size: 12px;overflow: hidden;" id="activityDescription"></p>
                            </div><!--//upper-row-->
                        </div><!--//meta-->
                    </div><!--//item-->
                </div>
            </template>
        </section><!--//section-->

        <section class="section projects-section" style="margin-bottom: 40px;">
            <h2 class="section-title"><span class="icon-holder"><i class="fas fa-bookmark"
                                                                   style="padding-top: 8px;"></i></span>CERTIFICATIONS
            </h2>

            <div id="certification-preview">
                <div class="certification-area">

                </div>
            </div>

            <template id="certification-field">
                <div id="_idx">
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">
                                <h3 class="job-title" id="certificationName"></h3>
                                <div class="time" id="certificationFromToType"></div>
                            </div><!--//upper-row-->
                            <div class="upper-row">
                                <div class="company" style="font-size: 14px;width: unset;"
                                     id="certificationMajor"></div>
                                <p style="font-size: 14px;overflow: hidden;" id="certificationDescription"></p>
                            </div><!--//upper-row-->
                        </div><!--//meta-->
                    </div><!--//item-->
                </div>
            </template>
        </section><!--//section-->

        <section class="section projects-section" style="margin-bottom: 40px;">
            <h2 class="section-title"><span class="icon-holder"><i class="fas fa-link"
                                                                   style="padding-top: 8px;"></i></span>LINKS</h2>

            <div id="links-preview">
                <div class="links-area">

                </div>
            </div>

            <template id="links-field">
                <div id="_idx">
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">
                                <h3 class="job-title" id="linksName" style="width: unset;"></h3>
                            </div><!--//upper-row-->
                            <div class="upper-row">
                                <div class="company" style="font-size: 14px;width: unset;" id="linksDescription"></div>
                            </div><!--//upper-row-->
                        </div><!--//meta-->
                    </div><!--//item-->
                </div>
            </template>
        </section><!--//section-->
    </div>

    <div class="sidebar-wrapper col-4">
        <div class="profile-container">
            <img class="profile" src="" alt="" id="userImage">
            <h1 class="name" id="name"></h1>
            <h3 class="tagline" id="employment"></h3>
        </div>

        <div class="education-container container-block">
            <ul class="list-unstyled contact-list">

                <li class="email">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                         class="bi bi-envelope" viewBox="0 0 16 16">
                        <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                    </svg>
                    <a href="#" style="text-decoration: none;color: white;font-size: 14px;" id="email2"></a>
                </li>

                <li class="phone" style="margin-bottom: 15px;">
                    <i class="fas fa-phone" style="font-size: 14px;"></i>
                    <a href="#" style="text-decoration: none;color: white;font-size: 14px;" id="phone"></a>
                </li>


                <li class="dateofbirth" style="margin-bottom: 15px;">
                    <i class="fas fa-calendar" style="font-size: 14px;"></i>
                    <a href="#" style="text-decoration: none;color: white;font-size: 14px;" id="dateOfBirth"></a>
                </li>

                <li class="address" style="margin-bottom: 15px;">
                    <i class="fas fa-map-marker-alt" style="font-size: 14px;"></i>
                    <a href="#" style="text-decoration: none;color: white;font-size: 14px;" id="address"></a>
                </li>

            </ul>

        </div>

        <div class="education-container container-block">
            <h2 class="container-block-title">Salary</h2>
            <div class="item">
                <div class="time" id="hopeSalary"></div>
            </div>
        </div>

        <div class="education-container container-block">
            <h2 class="container-block-title">Information</h2>

            <div class="item">
                <h4 class="degree">Final Education</h4>
                <h5 class="meta" id="finalEducation"></h5>
            </div>

            <div class="item">
                <h4 class="degree">Experience</h4>
                <h5 class="meta" id="experienceWork"></h5>
            </div>

            <div class="item">
                <h4 class="degree">Skill groups</h4>
                <h5 class="meta" id="skillGroups"></h5>
            </div>

            <div class="item">
                <h4 class="degree">Skills</h4>
                <h5 class="meta" id="skills"></h5>
            </div>

            <div class="item">
                <h4 class="degree">Workplaces</h4>
                <h5 class="meta" id="workplaces"></h5>
            </div>

            <div class="item">
                <h4 class="degree">Korean Language Level</h4>
                <h5 class="meta" id="koreanLanguageLevel"></h5>
            </div>
        </div>

    </div>
</div>';
}

if ($id == 3) {
    echo '<link rel="stylesheet" href="assets/vendors/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="cv_templates/CV_template_03/visangcv3.css">

<div class="scale-cv" id="capture" style="width: 960px;margin: auto;">
	<div style="width: auto;margin: auto;">
		<!-- BACKGROUND -->
		<DIV style="height: 280PX;background-color: #072251;width: auto;">
			<DIV style="font-family: sans-serif;color: white;float: right;margin: 100px 100px 0px 0px;max-width: 500px;">
				<H1 class="name" style="margin: 0;color: #FFF;font-size: xx-large;" id="name"></H1>
				<H2 style="margin: 0;font-weight: 400;padding-top: 10px;color: #FFF" id="employment"></H2>
				<P style="    width: 510px;margin: 0;padding-top: 32px;line-height: 1.5;color: #FFF"></P>
			</DIV>

			<!-- CỘT TRÁI -->



			<!-- CỘT PHẢI -->
			<!-- EDUCATION -->





		</div>

		<div class="row">
			<DIV class="col-4 height-jquery" style="float: left;width: 300px; font-family: sans-serif;margin-top: -265px;">

				<!-- AVATAR -->
				<!-- <img src="photo-1507114845806-0347f6150323.jpg" alt="" style="height: 200px;width: 200px;border-radius: 1000px;    margin: 24px 50px;border: 1px solid #ffffff;"> -->
				<div style="background-color: #E7EFFD;height: 100%;">
					<div class="profile-img">
						<img src="" alt="김비상" style="height: 200px;width: 200px;border-radius: 1000px;    margin: 24px 50px;border: 1px solid #ffffff;" id="userImage" >
					</div>
					<H1 style="color: #072251;font-size: 24px;margin-left: 24px;margin-top: 48px;margin-bottom: 10px;">CONTACTS</H1>

					<div style="display: flex; margin-left: 20px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 240px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>


					<DIV CLASS="PHONE" style="display: flex;margin: 15px 20px 0 15px;">
						<DIV CLASS="ẢNH" style="min-width: 32px;"> <img src="cv_templates/CV_template_03/phone1.png" alt=""></DIV>
						<P style="color: #072251;margin-top: 7px;font-size: 19px;margin-left: 16px;word-break: break-all;" id="phone"></P>
					</DIV>

					<DIV CLASS="ẢNH"  style="display: flex;margin: 15px 20px 0 15px;">
						<DIV CLASS="ẢNH"> <img src="cv_templates/CV_template_03/date1.png" alt=""></DIV>
						<P style="color: #072251;margin-top: 7px;font-size: 19px;margin-left: 16px;"><span id="dateOfBirth"></span></P>
					</DIV>


					<DIV CLASS="LETTERS" style="display: flex;margin: 15px 20px 0 15px;">
						<DIV CLASS="LETTERS" style="min-width: 32px;"><img src="cv_templates/CV_template_03/Fram222e.png" alt=""></DIV>
						<P style="    color: #072251;font-size: 19px;padding-left: 16px;padding-top: 6px;margin: 0;word-break: break-all;" class="email2" id="email2"></P>
					</DIV>

					<DIV CLASS="LOCATION" style="display: flex;margin: 15px 20px 0 15px;">
						<DIV CLASS="LOCATION" style="min-width: 28px;"> <img src="cv_templates/CV_template_03/location1.png" alt=""></DIV>
						<P style="color: #072251;    margin: 0;font-size: 19px;padding-top: 4px;padding-left: 20px;word-break: break-word;" id="address"></P>
					</DIV>


					<!-- DÒNG 2 -->
					<H1 style="color: #072251;font-size: 24px;margin-left: 24px;margin-top: 48px;margin-bottom: 10px;">SALARY</H1>

					<div style="display: flex; margin-left: 20px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 240px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>
					<p style="    font-size: 17px;color: #072251;padding-left: 24px;margin: 0;padding-top: 24px;" id="hopeSalary"></p>


					<!-- DÒNG 3 -->
					<H1 style="color: #072251;font-size: 24px;margin-left: 24px;margin-top: 48px;margin-bottom: 10px;">CERTIFICATIONS</H1>

					<div style="display: flex; margin-left: 20px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 240px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 24px 24px 0 24px;">Final Education</p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 0 24px;" id="finalEducation"></p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 24px 24px 0 24px;">Experience</p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 0 24px;" id="experienceWork"></p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 24px 24px 0 24px;">Skill groups</p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 0 24px;" id="skillGroups"></p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 24px 24px 0 24px;">Skills</p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 0 24px;" id="skills"></p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 24px 24px 0 24px;">Workplaces</p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 0 24px;" id="workplaces"></p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 24px 24px 0 24px;">Korean Language Level</p>
					<p style="    font-size: 17px;color: #072251;margin: 0;padding: 0 24px;" id="koreanLanguageLevel"></p>
				</div>

			</DIV>

			<div class="col-8">
				<div style="display: flex;">
					<H1 style="color: #072251;font-size: 24px;margin-top: 48px;margin-bottom: 0px;font-family: sans-serif;">EDUCATION</H1>

					<div style="display: flex; margin-left: 30px;margin-top: 58px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 435px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>

				</div>

				<div id="education-preview">
					<div class="education-area">

					</div>
				</div>

				<template id="education-field">
					<div id="_idx">
						<div style="display: flex;font-family: sans-serif;margin-top: 24px; justify-content: space-between;">
							<div style="max-width: 450px;">
								<p  style="   font-size: 20px;font-weight: 600;color: #0F4096;margin: 0px;margin-bottom: 10px;" id="educationName"></p>
								<p style="    font-size: 18px;margin: 0px;" id="educationMajor"></p>
							</div>
							<div>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;text-align: end;" id="educationFromTo"></p>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;text-align: end;" id="educationAttend"></p>
							</div>
						</div>
					</div>
				</template>


				<div style="display: flex;">
					<H1 style="color: #072251;font-size: 24px;margin-top: 48px;margin-bottom: 0px;font-family: sans-serif;">WORK EXPERIENCE</H1>

					<div style="display: flex; margin-left: 30px;margin-top: 58px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 338px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>

				</div>

				<div id="experience-preview">
					<div class="experience-area">

					</div>
				</div>

				<template id="experience-field">
					<div id="_idx">
						<div style="display: flex;font-family: sans-serif;margin-top: 24px; justify-content: space-between;">
							<div style="max-width: 450px;">
								<p  style="   font-size: 20px;font-weight: 600;color: #0F4096;margin: 0px;margin-bottom: 10px;" id="experienceName"></p>
								<p style="    font-size: 18px;margin: 0px;" id="experienceMajor"></p>
							</div>
							<div>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;text-align: end;" id="experienceFromTo"></p>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;text-align: end;" id="experienceAttend"></p>
							</div>
						</div>

						<div style="padding-left: 40px;padding-top: 20px;" class="sub-experience-area">

						</div>
					</div>
				</template>
				<template id="sub-experience-field">
					<div id="_subidx">
						<ul style="list-style:none; display: flex; justify-content: space-between; margin: 0;padding: 0;">
							<li style="font-size: 18px;font-weight: bold;width: 420px;" id="sub_experienceName"></li>
							<li style="font-size: 16px;" id="sub_experienceFromTo"></li>
						</ul>
						<li style="display: flex;margin-bottom: 10px;" id="sub_experienceDescription"></li>
					</div>
				</template>





				<!-- ACTIVITIES -->
				<div style="display: flex;">
					<H1 style="color: #072251;font-size: 24px;margin-top: 48px;margin-bottom: 0px;font-family: sans-serif;">ACTIVITIES</H1>

					<div style="display: flex; margin-left: 30px;margin-top: 58px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 444px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>

				</div>

				<div id="activity-preview">
					<div class="activity-area">

					</div>
				</div>

				<template id="activity-field">
					<div id="_idx">
						<div style="display: flex;font-family: sans-serif;margin-top: 24px; justify-content: space-between;">
							<div style="max-width: 450px;">
								<p  style="   font-size: 20px;font-weight: 600;color: #0F4096;margin: 0px;margin-bottom: 10px;" id="activityName"></p>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;" id="activityAttend"></p>
							</div>
							<div>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;text-align: end;" id="activityFromTo"></p>
							</div>
						</div>
						<p style="    font-size: 18px;margin: 0px;" id="activityDescription"></p>
					</div>
				</template>

				<!-- CERTIFICATIONS -->
				<div style="display: flex;">
					<H1 style="color: #072251;font-size: 24px;margin-top: 48px;margin-bottom: 0px;font-family: sans-serif;">CERTIFICATIONS</H1>

					<div style="display: flex; margin-left: 30px;margin-top: 58px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 377px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>

				</div>

				<div id="certification-preview">
					<div class="certification-area">

					</div>
				</div>

				<template id="certification-field">
					<div id="_idx">
						<div style="display: flex;font-family: sans-serif;margin-top: 24px; justify-content: space-between;">
							<div style="max-width: 450px;">
								<p  style="   font-size: 20px;font-weight: 600;color: #0F4096;margin: 0px;margin-bottom: 10px;" id="certificationName"></p>
							</div>
							<div>
								<p style="    font-size: 18px;margin: 0px;padding-top: 4px;text-align: end;" id="certificationFromToType"></p>
							</div>
						</div>
						<p style="font-family: sans-serif;font-size: 18px;margin: 0px;padding-top: 4px;" id="certificationMajor"></p>
						<p style="    font-size: 18px;margin: 0px;" id="certificationDescription"></p>
					</div>
				</template>

				<!-- LINKS -->
				<div style="display: flex;">
					<H1 style="color: #072251;font-size: 24px;margin-top: 48px;margin-bottom: 0px;font-family: sans-serif;">LINKS</H1>

					<div style="display: flex; margin-left: 30px;margin-top: 58px;">
						<div class="circle" style="width: 7px;height: 7px;background-color:#072251 ;border-radius: 100px;"></div>
						<DIV class="LINE" style="height: 1px;background-color: #072251;width: 505px;margin-top: 3px;margin-left: -2px;"></DIV>
					</div>

				</div>

				<div id="links-preview">
					<div class="links-area">

					</div>
				</div>

				<template id="links-field">
					<div id="_idx">
						<div style="display: flex;font-family: sans-serif;margin-top: 24px; justify-content: space-between;">
							<div>
								<p  style="   font-size: 20px;font-weight: 600;color: #0F4096;margin: 0px;margin-bottom: 10px;" id="linksName"></p>
							</div>
						</div>
						<p style="    font-size: 18px;margin: 0px;" id="linksDescription"></p>
					</div>
				</template>
			</div>
		</div>

	</div>
</div>';
}

if ($id == 4) {
    echo '<link rel="stylesheet" href="cv_templates/CV_template_04/stevedoe.css">
<link rel="stylesheet" href="assets/css/style.css">

<article class="resume-wrapper position-relative" style="padding-right: 0;max-width: 960px;margin: auto;" id="capture">
    <div class="resume-wrapper-inner mx-auto text-start bg-white shadow-lg" style="max-width: 1000px;">
        <header class="resume-header pt-4 pt-md-0">
            <div class="row">
                <div class="col-3 avatar-cv">
                    <img class="picture" src="" alt=""
                         style="height: 100%;width: 100%;" id="userImage">
                </div>

                <div class="col-9" style="padding-right: 0;margin: auto;">
                    <div class="row">
                        <div class="primary-info col-7">
                            <h1 class="name mt-0 mb-1 text-white text-uppercase text-uppercase" id="name"></h1>
                            <div class="title mb-3" id="employment"></div>
                            <a class="text-link" href="#" style="color: #A5A8AE;text-decoration: none;"
                               id="hopeSalary"></a>
                        </div>

                        <div class="secondary-info col-5">
                            <ul class="list-unstyled">
                                <li class="mb-2">
                                    <i class="fal fa-envelope" style="color: #A5A8AE;"></i>
                                    <a class="text-link" href="#" style="color: #A5A8AE;text-decoration: none;"
                                       id="email2"></a>
                                </li>

                                <li class="mb-2">
                                    <i class="fas fa-mobile-alt" style="color: #A5A8AE;"></i>
                                    <a class="text-link" href="#" style="color: #A5A8AE;text-decoration: none;"
                                       id="phone"></a>
                                </li>

                                <li class="mb-2">
                                    <i class="fal fa-calendar" style="color: #A5A8AE;"></i>
                                    <a class="text-link" href="#" style="color: #A5A8AE;text-decoration: none;"
                                       id="dateOfBirth"></a>
                                </li>

                                <li>
                                    <i class="fal fa-map-marker-alt" style="color: #A5A8AE;"></i>
                                    <a class="text-link" href="#" style="color: #A5A8AE;text-decoration: none"
                                       id="address"></a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>

            </div>
        </header>


        <div class="resume-body">
            <div class="row">
                <div class="col-8 cv-content">
                    <section class="resume-section experience-section">
                        <h2 style="font-size: 18px;position: relative;color: #434E5E; ">EDUCATION</h2>
                        <div class="line"
                             style="width: 100%;height: 2px;background-color: #acb7c6;;margin-bottom: 16px;"></div>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">

                                <div id="education-preview">
                                    <div class="education-area">

                                    </div>
                                </div>

                                <template id="education-field">
                                    <div id="_idx">
                                        <article class="resume-timeline-item position-relative pb-4">
                                            <div class="resume-timeline-item-header mb-2">
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="circlewithheader" style="display: flex;">
                                                        <i class="fas fa-dot-circle"
                                                           style="margin-top: 3px;margin-right: 8px;color: #58677c"></i>
                                                        <h3 class="resume-position-title font-weight-bold mb-1"
                                                            style=" font-size: 15px;color: #434E5E;"
                                                            id="educationName"></h3>
                                                    </div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="educationFromTo"></div>
                                                </div><!--//row-->
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="resume-position-time" style="color:#aab4c3;font-size: 12px;"
                                                         id="educationMajor"></div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="educationAttend"></div>
                                                </div><!--//row-->
                                            </div><!--//resume-timeline-item-header-->
                                        </article><!--//resume-timeline-item-->
                                    </div>
                                </template>

                            </div><!--//resume-timeline-->
                        </div>
                    </section><!--//experience-section-->

                    <section class="resume-section experience-section">

                        <h2 style="font-size: 18px;position: relative;color: #434E5E; ">WORK EXPERIENCE</h2>
                        <div class="line"
                             style="width: 100%;height: 2px;background-color: #acb7c6;;margin-bottom: 16px;"></div>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">

                                <div id="experience-preview">
                                    <div class="experience-area">

                                    </div>
                                </div>

                                <template id="experience-field">
                                    <div id="_idx">
                                        <article class="resume-timeline-item position-relative pb-4">
                                            <div class="resume-timeline-item-header mb-2">
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="circlewithheader" style="display: flex;">
                                                        <i class="fas fa-dot-circle"
                                                           style="margin-top: 3px;margin-right: 8px;color: #58677c"></i>
                                                        <h3 class="resume-position-title font-weight-bold mb-1"
                                                            style=" font-size: 15px;color: #434E5E;"
                                                            id="experienceName"></h3>
                                                    </div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="experienceFromTo"></div>
                                                </div><!--//row-->
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="resume-position-time" style="color:#aab4c3;font-size: 12px;"
                                                         id="experienceMajor"></div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="experienceAttend"></div>
                                                </div><!--//row-->
                                            </div><!--//resume-timeline-item-header-->
                                            <div class="resume-timeline-item-desc">
                                                <h4 class="resume-timeline-item-desc-heading font-weight-bold"
                                                    style="font-size: 14px;color: #58677c;"> Achievements </h4>

                                                <div class="sub-experience-area"></div>
                                            </div><!--//resume-timeline-item-desc-->
                                        </article><!--//resume-timeline-item-->
                                    </div>
                                </template>

                                <template id="sub-experience-field">
                                    <div id="_subidx">
                                        <div class="d-flex flex-column flex-md-row"
                                             style="justify-content: space-between;">
                                            <div class="circlewithheader" style="display: flex;">
                                                <div class="resume-position-time" style="color:#aab4c3;font-size: 12px;"
                                                     id="sub_experienceName"></div>
                                            </div>
                                            <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="sub_experienceFromTo"></div>
                                        </div><!--//row-->
                                        <p style="color: #58677c;font-size: 12px;" id="sub_experienceDescription"></p>
                                    </div>
                                </template>

                            </div><!--//resume-timeline-->
                        </div>
                    </section><!--//experience-section-->

                    <section class="resume-section experience-section">
                        <h2 style="font-size: 18px;position: relative;color: #434E5E; ">ACTIVITIES</h2>
                        <div class="line"
                             style="width: 100%;height: 2px;background-color: #acb7c6;;margin-bottom: 16px;"></div>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">

                                <div id="activity-preview">
                                    <div class="activity-area">

                                    </div>
                                </div>

                                <template id="activity-field">
                                    <div id="_idx">
                                        <article class="resume-timeline-item position-relative pb-4">
                                            <div class="resume-timeline-item-header mb-2">
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="circlewithheader" style="display: flex;">
                                                        <i class="fas fa-dot-circle"
                                                           style="margin-top: 3px;margin-right: 8px;color: #58677c"></i>
                                                        <h3 class="resume-position-title font-weight-bold mb-1"
                                                            style=" font-size: 15px;color: #434E5E;"
                                                            id="activityName"></h3>
                                                    </div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="activityFromTo"></div>
                                                </div><!--//row-->
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="resume-position-time" style="color:#aab4c3;font-size: 12px;"
                                                         id="activityAttend"></div>
                                                </div><!--//row-->
                                                <p style="color: #58677c;font-size: 12px;" id="activityDescription"></p>
                                            </div><!--//resume-timeline-item-header-->
                                        </article><!--//resume-timeline-item-->
                                    </div>
                                </template>

                            </div><!--//resume-timeline-->
                        </div>
                    </section><!--//experience-section-->

                    <section class="resume-section experience-section">
                        <h2 style="font-size: 18px;position: relative;color: #434E5E; ">CERTIFICATIONS</h2>
                        <div class="line"
                             style="width: 100%;height: 2px;background-color: #acb7c6;;margin-bottom: 16px;"></div>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">

                                <div id="certification-preview">
                                    <div class="certification-area">

                                    </div>
                                </div>

                                <template id="certification-field">
                                    <div id="_idx">
                                        <article class="resume-timeline-item position-relative pb-4">
                                            <div class="resume-timeline-item-header mb-2">
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="circlewithheader" style="display: flex;">
                                                        <i class="fas fa-dot-circle"
                                                           style="margin-top: 3px;margin-right: 8px;color: #58677c"></i>
                                                        <h3 class="resume-position-title font-weight-bold mb-1"
                                                            style=" font-size: 15px;color: #434E5E;"
                                                            id="certificationName"></h3>
                                                    </div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="certificationDescription"></div>
                                                </div><!--//row-->
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="resume-position-time" style="color:#aab4c3;font-size: 12px;"
                                                         id="certificationMajor"></div>
                                                    <div class="resume-company-name ms-auto" style="    color: #58677c;
                                    font-size: 12px;
                                    font-weight: 500;" id="certificationFromToType"></div>
                                                </div><!--//row-->
                                            </div><!--//resume-timeline-item-header-->
                                        </article><!--//resume-timeline-item-->
                                    </div>
                                </template>

                            </div><!--//resume-timeline-->
                        </div>
                    </section><!--//experience-section-->

                    <section class="resume-section experience-section">
                        <h2 style="font-size: 18px;position: relative;color: #434E5E; ">LINKS</h2>
                        <div class="line"
                             style="width: 100%;height: 2px;background-color: #acb7c6;;margin-bottom: 16px;"></div>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">

                                <div id="links-preview">
                                    <div class="links-area">

                                    </div>
                                </div>

                                <template id="links-field">
                                    <div id="_idx">
                                        <article class="resume-timeline-item position-relative pb-4">
                                            <div class="resume-timeline-item-header mb-2">
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="circlewithheader" style="display: flex;">
                                                        <i class="fas fa-dot-circle"
                                                           style="margin-top: 3px;margin-right: 8px;color: #58677c"></i>
                                                        <h3 class="resume-position-title font-weight-bold mb-1"
                                                            style=" font-size: 15px;color: #434E5E;"
                                                            id="linksName"></h3>
                                                    </div>
                                                </div><!--//row-->
                                                <div class="d-flex flex-column flex-md-row"
                                                     style="justify-content: space-between;">
                                                    <div class="resume-position-time" style="color:#aab4c3;font-size: 12px;"
                                                         id="linksDescription"></div>
                                                </div><!--//row-->
                                            </div><!--//resume-timeline-item-header-->
                                        </article><!--//resume-timeline-item-->
                                    </div>
                                </template>

                            </div><!--//resume-timeline-->
                        </div>
                    </section><!--//experience-section-->
                </div>
                <div class="col-4 cv-content">
                    <section class="resume-section skills-section mb-5">
                        <h2 style="font-size: 18px;position: relative;color: #434E5E;text-transform: uppercase;">
                            Information</h2>
                        <div class="line"
                             style="width: 100%;height: 2px;background-color: #acb7c6;margin-bottom: 16px;"></div>
                        <div class="resume-section-content">
                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold" style="    font-size: 14px;
                        color: #434E5E;">Final Education</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="mb-2">
                                        <div class="resume-skill-name" id="finalEducation" style="font-size: 12px;word-break: break-word;"></div>
                                    </li>
                                </ul>
                            </div><!--//resume-skill-item-->

                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold" style="    font-size: 14px;
                        color: #434E5E;">Experience</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="mb-2">
                                        <div class="resume-skill-name" id="experienceWork" style="font-size: 12px;word-break: break-word;"></div>
                                    </li>
                                </ul>
                            </div><!--//resume-skill-item-->

                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold" style="    font-size: 14px;
                        color: #434E5E;">Skill groups</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="mb-2">
                                        <div class="resume-skill-name" id="skillGroups" style="font-size: 12px;word-break: break-word;"></div>
                                    </li>
                                </ul>
                            </div><!--//resume-skill-item-->

                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold" style="    font-size: 14px;
                        color: #434E5E;">Skills</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="mb-2">
                                        <div class="resume-skill-name" id="skills" style="font-size: 12px;word-break: break-word;"></div>
                                    </li>
                                </ul>
                            </div><!--//resume-skill-item-->

                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold" style="    font-size: 14px;
                        color: #434E5E;">Workplaces</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="mb-2">
                                        <div class="resume-skill-name" id="workplaces" style="font-size: 12px;word-break: break-word;"></div>
                                    </li>
                                </ul>
                            </div><!--//resume-skill-item-->

                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold" style="    font-size: 14px;
                        color: #434E5E;">Korean Language Level</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="mb-2">
                                        <div class="resume-skill-name" id="koreanLanguageLevel" style="font-size: 12px;word-break: break-word;"></div>
                                    </li>
                                </ul>
                            </div><!--//resume-skill-item-->
                        </div><!--resume-section-content-->
                    </section><!--//skills-section-->
                </div>
            </div>


        </div>


    </div>


</article>';
}

if ($id == 5) {
    echo '<link rel="stylesheet" href="cv_templates/CV_template_05/17092021.css">
<link rel="stylesheet" href="assets/css/style.css">

<div class="all" id="capture">

  <div class="column_left" style="background-color: #b9cced ;">
    <div style="background-color: white;width: 100%;height: 100px;"></div>
    <div class="picture">
      <img src="" alt="" style="width:120px;height:120px;margin-top: 20px;" id="userImage">
    </div>
    <div class="line" style="width: 100%;height: 0.5px;background-color: rgb(219, 219, 219);margin: auto;margin-top: 32px;"></div>

    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;">
      <h2 class="font_1">Profile</h2>
    </div>

    <div class="icons" style="display: flex;margin-left: 16px;margin-top: 16px;">
      <i class="fas fa-birthday-cake" style="font-size: 12px;margin-top: 7px;margin-right: 8px;"></i>
      <li style="list-style: none;" class="font_12" id="dateOfBirth"></li>
    </div>

    <div class="icons" style="display: flex;margin-left: 16px;margin-top: 16px;">
      <i class="fas fa-map-marker-alt" style="font-size: 12px;margin-top: 7px;margin-right: 8px;"></i>
      <li style="list-style: none;word-break: break-word;" class="font_12" id="address"></li>
    </div>

    <div class="icons" style="display: flex; margin-left: 16px;margin-top: 16px;">
      <i class="fas fa-mobile-alt" style="font-size: 12px;margin-top: 7px;margin-right: 8px;"></i>
      <li style="list-style: none;word-break: break-all;" class="font_12" id="phone"></li>
    </div>

    <div class="icons" style="display: flex; margin-left: 16px;margin-top: 16px;">
      <i class="fas fa-envelope" style="font-size: 12px;margin-top: 7px;margin-right: 8px;"></i>
      <li style="list-style: none;word-break: break-all;" class="font_12" id="email2"></li>
    </div>



    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;">
      <h2 class="font_1">Salary</h2>
    </div>

    <div class="icons" style="display: flex;margin-left: 16px;margin-top: 16px;">
      <i class="fas fa-euro-sign" style="font-size: 12px;margin-top: 7px;margin-right: 8px;"></i>
      <li style="list-style: none;word-break: break-word;" class="font_12" id="hopeSalary"></li>
    </div>


    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;">
      <h2 class="font_1">Information</h2>
    </div>
    <div style="display: flex;justify-content: space-between;margin-top: 16px;">
      <h6 style="margin-left: 16px;" class="font_14">Final Education</h6>
    </div>
    <li style="list-style: none;margin-left: 16px;margin-bottom: 16px;" class="font_12" id="finalEducation"></li>

    <div style="display: flex;justify-content: space-between;margin-top: 16px;">
      <h6 style="margin-left: 16px;" class="font_14">Experience</h6>
    </div>
    <li style="list-style: none;margin-left: 16px;margin-bottom: 16px;" class="font_12" id="experienceWork"></li>

    <div style="display: flex;justify-content: space-between;margin-top: 16px;">
      <h6 style="margin-left: 16px;" class="font_14">Skill groups</h6>
    </div>
    <li style="list-style: none;margin-left: 16px;margin-bottom: 16px;" class="font_12" id="skillGroups"></li>

    <div style="display: flex;justify-content: space-between;margin-top: 16px;">
      <h6 style="margin-left: 16px;" class="font_14">Skills</h6>
    </div>
    <li style="list-style: none;margin-left: 16px;margin-bottom: 16px;" class="font_12" id="skills"></li>

    <div style="display: flex;justify-content: space-between;margin-top: 16px;">
      <h6 style="margin-left: 16px;" class="font_14">Workplaces</h6>
    </div>
    <li style="list-style: none;margin-left: 16px;margin-bottom: 16px;" class="font_12" id="workplaces"></li>

    <div style="display: flex;justify-content: space-between;margin-top: 16px;">
      <h6 style="margin-left: 16px;" class="font_14">Korean Language Level</h6>
    </div>
    <li style="list-style: none;margin-left: 16px;margin-bottom: 16px;" class="font_12" id="koreanLanguageLevel"></li>


  </div>


  <div class="column_right" style="background-color: white;width: 780px;">
    <div style="background-color: white;height: 20px;width: 100%;"></div>
    <div class="nameframe" style="background-color: #f6e5f5; height: 160px;width: 90%;margin: auto;">
      <h4 class="adam" style="text-align: center;padding-top: 56px;font-size: 24px;" id="name"></h4>
      <div class="titframe2" style="width: 60%;height: auto;background-color: #b9cced;margin: auto;margin-top: 48px;">
        <h2 class="font_1" style="color: white;font-weight: 500;" id="employment"></h2>
      </div>
    </div>

    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;margin-top: 32px;margin-bottom: 24px;">
      <h2 class="font_1">Education</h2>
    </div>

    <div id="education-preview">
      <div class="education-area">

      </div>
    </div>

    <template id="education-field">
      <div id="_idx">
        <div style="display: flex;justify-content: space-between;margin-top: 16px;">
          <h6 style="margin-left: 16px;" class="font_15 width280" id="educationName"></h6>
          <div style="background-color: #f6e5f5;height: 20px;width: 120px;text-align: center;margin-right: 16px;font-size: 16px;">
            <h6 class="font_14" id="educationFromTo"></h6>
          </div>
        </div>
        <div class="thongtin" style="margin-left: 16px;">
          <li style="list-style: none;font-weight: 500" class="font_12"><span id="educationMajor"></span></li>
          <li style="list-style: none;font-weight: 500" class="font_12"><span id="educationAttend"></span></li>
        </div>
      </div>
    </template>



    <div class="titframe" style="width: 80%;height: auto;background-color:#f6e5f5;margin: auto;margin-top: 32px;margin-top: 64px;margin-bottom: 24px;">
      <h2 class="font_1">Work Experience</h2>
    </div>

    <div id="experience-preview">
      <div class="experience-area">

      </div>
    </div>

    <template id="experience-field">
      <div id="_idx">
        <div style="display: flex;justify-content: space-between;margin-top: 16px;">
          <h6 style="margin-left: 16px;" class="font_15 width280" id="experienceName"></h6>
          <div style="background-color: #f6e5f5;height: 20px;width: 120px;text-align: center;margin-right: 16px;font-size: 16px;">
            <h6 class="font_14" id="experienceFromTo"></h6>
          </div>
        </div>
        <div class="thongtin" style="margin-left: 16px;">
          <li style="list-style: none;font-weight: 500;" class="font_12"><span id="experienceMajor"></span></li>
          <li style="list-style: none;font-weight: 500" class="font_12"><span id="experienceAttend"></span></li>
          <li style="list-style: none;font-weight: 500" class="font_12">Achievements:</li>
          <div class="sub-experience-area"></div>
        </div>
      </div>
    </template>

    <template id="sub-experience-field">
      <div id="_subidx">
        <div style="display: flex;justify-content: space-between;margin-top: 16px;padding-left: 20px;">
          <li style="list-style: none;color: #555;margin-bottom: 8px;" class="font_12 width280" id="sub_experienceName"></li>
          <li style="list-style: none;color: #555;padding-right: 15px;" class="font_12" id="sub_experienceFromTo"></li>
        </div>
        <li style="list-style: none;color: #555;padding-left: 20px;padding-right: 15px;" class="font_12" id="sub_experienceDescription"></li>
      </div>
    </template>



    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;margin-top: 32px;margin-bottom: 24px;">
      <h2 class="font_1">Activities</h2>
    </div>

    <div id="activity-preview">
      <div class="activity-area">

      </div>
    </div>

    <template id="activity-field">
      <div id="_idx">
        <div style="display: flex;justify-content: space-between;margin-top: 16px;">
          <h6 style="margin-left: 16px;" class="font_15 width280" id="activityName"></h6>
          <div style="background-color: #f6e5f5;height: 20px;width: 120px;text-align: center;margin-right: 16px;font-size: 16px;">
            <h6 class="font_14" id="activityFromTo"></h6>
          </div>
        </div>
        <div class="thongtin" style="margin-left: 16px;">
          <li style="list-style: none;font-weight: 500" class="font_12"><span id="activityAttend"></span></li>
          <li style="list-style: none;color: #555;" class="font_12" id="activityDescription"></li>
        </div>
      </div>
    </template>



    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;margin-top: 32px;margin-bottom: 24px;">
      <h2 class="font_1">Certifications</h2>
    </div>

    <div id="certification-preview">
      <div class="certification-area">

      </div>
    </div>

    <template id="certification-field">
      <div id="_idx">
        <div style="display: flex;justify-content: space-between;margin-top: 16px;">
          <h6 style="margin-left: 16px;" class="font_15 width280" id="certificationName"></h6>
          <div style="background-color: #f6e5f5;height: 20px;width: 120px;text-align: center;margin-right: 16px;font-size: 16px;">
            <h6 class="font_14" id="certificationFromToType"></h6>
          </div>
        </div>
        <div class="thongtin" style="margin-left: 16px;">
          <li style="list-style: none;font-weight: 500" class="font_12"><span id="certificationMajor"></span></li>
          <li style="list-style: none;color: #555;" class="font_12" id="certificationDescription"></li>
        </div>
      </div>
    </template>



    <div class="titframe" style="width: 80%;height: auto;background-color: #f6e5f5;margin: auto;margin-top: 32px;margin-top: 32px;margin-bottom: 24px;">
      <h2 class="font_1">Links</h2>
    </div>

    <div id="links-preview">
      <div class="links-area">

      </div>
    </div>

    <template id="links-field">
      <div id="_idx">
        <div class="thongtin" style="margin-left: 16px;margin-bottom: 24px;">
          <li style="list-style: none;font-weight: 500" class="font_15" id="linksName"></li>
          <li style="list-style: none;color: #555;" class="font_12" id="linksDescription"></li>
        </div>
      </div>
    </template>
  </div>
</div>';
}

if ($id == 6) {
    echo '<link rel="stylesheet" href="assets/css/style.css">

<style>
    .width380 {
        width: 375px;
    }

    .width338 {
        width: 350px;
    }
</style>

<div class="wrapper flex scale-cv" style="display:flex;max-width: 960px;margin: auto;" id="capture">
    <div class="sidebar" style="display: flex;background-color: rgb(255, 238, 221);">


        <!-- CỘT TRÁI -->

        <div>
            <div class="column_left"
                 style="font-family: sans-serif;width: 320px;">
                <div class="wrapper flex">
                    <div class="sidebar">


                        <div class="avatar"
                             style="height: 230px;width: 230px;background: red:-moz-border-radius: 25px;-webkit-border-radius: 25px;--border-radius: 25px;border-radius: 25px;margin: 3px auto;border: 4px solid #ffffff">
                            <img src="" alt=""
                                 style="width: 100%; height: 100%; border-radius: 10%;" id="userImage">
                        </div><!-- .avatar -->


                        <!-- HÀNG1 -->


                        <div class="h1-colume2" style="display: flex;margin: 32px 24px 0px 40px;">
                            <div style="background-color: #000000;height: 0px;width: 0px;">

                            </div>
                            <h1 style="margin: 0;padding: 15px 0px 20px 10px;color: rgb(101, 123, 139);font-size: 24px;">
                                ABOUT ME</h1>
                        </div>
                        <div class="line" style="background-color: rgb(101, 123, 139);height: 1px;"></div>


                        <ul style="list-style:none; display: flex;">
                            <li class="flex" style="    font-size: 21px;
                              font-weight: 700;
                              color: #ffffff;">

                            </li>
                        </ul>

                        <div style="list-style: none;
                            color: rgb(101, 123, 139);
                            font-size: 16px;
                            margin: 10px 30px 0;
                            line-height: 1.5;">

                            <li style="    list-style: none;
                            font-size: 20px;
                            color: rgb(101, 123, 139);
                            margin-top: 30px;
                            margin-left: 0px;
                            font-weight: 700;">Phone
                            </li>
                            <li style="display: flex; font-size: 18px; word-break: break-all" id="phone"></li>

                            <li style="    list-style: none;
                            font-size: 20px;
                            color: rgb(101, 123, 139);
                            margin-top: 30px;
                            margin-left: 0px;
                            font-weight: 700;">Date of birth
                            </li>
                            <li style="display: flex; font-size: 18px;" id="dateOfBirth"></li>

                            <li style="    list-style: none;
                            font-size: 20px;
                            color: rgb(101, 123, 139);
                            margin-top: 30px;
                            margin-left: 0px;
                            font-weight: 700;">Email
                            </li>
                            <li style="display: flex; font-size: 18px; word-break: break-all;" id="email2"></li>

                            <li style="    list-style: none;
                            font-size: 20px;
                            color: rgb(101, 123, 139);
                            margin-top: 30px;
                            margin-left: 0px;
                            font-weight: 700;">Address
                            </li>
                            <li style="display: flex; font-size: 18px;" id="address"></li>

                        </div>


                    </div><!-- End .module #1 -->


                    <!-- HÀNG2 -->

                    <div class="h1-colume2" style="display: flex;margin: 32px 24px 0px 40px;">
                        <div style="background-color: #000000;height: 0px;width: 0px;">

                        </div>
                        <h1 style="margin: 0;padding: 15px 0px 20px 10px;color: rgb(101, 123, 139);font-size: 24px;">
                            SALARY</h1>
                    </div>

                    <div class="line" style="background-color: rgb(101, 123, 139);height: 1px;"></div>

                    <li style="    list-style: none;
                                                      font-size: 20px;
                                                      color: rgb(101, 123, 139);
                                                      margin: 10px 30px 0;
                                                      font-weight: 500;" id="hopeSalary"></li>

                    <div class="h1-colume2" style="display: flex;margin: 32px 24px 0px 40px;">
                        <div style="background-color: #000000;height: 0px;width: 0px;">

                        </div>
                        <h1 style="margin: 0;padding: 15px 0px 20px 10px;color: rgb(101, 123, 139);font-size: 24px;">
                            INFORMATION</h1>
                    </div>

                    <div class="line" style="background-color: rgb(101, 123, 139);height: 1px;"></div>


                    <div style="list-style: none;
                            color: rgb(101, 123, 139);
                            font-size: 16px;
                            margin-left: 30px;
                            margin-top: 10px;
                            line-height: 1.5;">

                        <li style="    list-style: none;
                        font-size: 20px;
                        color: rgb(101, 123, 139);
                       margin-top: 5px;
                        margin-left: 0px;
                        font-weight: 700;">Final Education
                        </li>

                        <li style="    list-style: none;
                        font-size: 18px;
                        color: rgb(101, 123, 139);
                        margin-bottom: 30px;
                        margin-left: 0px;
                        font-weight: 500;" id="finalEducation">
                        </li>

                        <li style="    list-style: none;
                        font-size: 20px;
                        color: rgb(101, 123, 139);
                       margin-top: 5px;
                        margin-left: 0px;
                        font-weight: 700;">Experience
                        </li>

                        <li style="    list-style: none;
                        font-size: 18px;
                        color: rgb(101, 123, 139);
                        margin-bottom: 30px;
                        margin-left: 0px;
                        font-weight: 500;" id="experienceWork">
                        </li>

                        <li style="    list-style: none;
                        font-size: 20px;
                        color: rgb(101, 123, 139);
                       margin-top: 5px;
                        margin-left: 0px;
                        font-weight: 700;">Skill groups
                        </li>

                        <li style="    list-style: none;
                        font-size: 18px;
                        color: rgb(101, 123, 139);
                        margin-bottom: 30px;
                        margin-left: 0px;
                        font-weight: 500;" id="skillGroups">
                        </li>

                        <li style="    list-style: none;
                        font-size: 20px;
                        color: rgb(101, 123, 139);
                       margin-top: 5px;
                        margin-left: 0px;
                        font-weight: 700;">Skills
                        </li>

                        <li style="    list-style: none;
                        font-size: 18px;
                        color: rgb(101, 123, 139);
                        margin-bottom: 30px;
                        margin-left: 0px;
                        font-weight: 500;" id="skills">
                        </li>

                        <li style="    list-style: none;
                        font-size: 20px;
                        color: rgb(101, 123, 139);
                       margin-top: 5px;
                        margin-left: 0px;
                        font-weight: 700;">Workplaces
                        </li>

                        <li style="    list-style: none;
                        font-size: 18px;
                        color: rgb(101, 123, 139);
                        margin-bottom: 30px;
                        margin-left: 0px;
                        font-weight: 500;" id="workplaces">
                        </li>

                        <li style="    list-style: none;
                        font-size: 20px;
                        color: rgb(101, 123, 139);
                       margin-top: 5px;
                        margin-left: 0px;
                        font-weight: 700;">Korean Language Level
                        </li>

                        <li style="    list-style: none;
                        font-size: 18px;
                        color: rgb(101, 123, 139);
                        margin-bottom: 30px;
                        margin-left: 0px;
                        font-weight: 500;" id="koreanLanguageLevel">
                        </li>

                    </div>
                </div><!-- End .sidebar -->


            </div>
        </div><!-- End .module #2 -->


    </div>

    <!-- PHẢI -->

    <div class="column_right" style="font-family: sans-serif;background-color: rgb(101, 123, 139);
			width: 640px;">


        <!-- HÀNG1 -->
        <div class="h1-colume2" style="display: flex;margin: 32px 24px 0px 20px;">
            <div style="background-color: #000000;height: 0px;width: 0px;">

            </div>
            <h1 style="margin: 0;padding: 6px 0px 0px 24px;color: rgb(255, 238, 221);"><i id="name"></i></h1>
        </div>


        <ul style="list-style:none; display: flex;">
            <li class="flex" style="    font-size: 21px;
						  font-weight: 700;
						  color: #202020;">

            </li>
        </ul>
        <li style="    list-style: none;
						  font-size: 30px;
						  color: rgb(255, 238, 221);
						  margin-left: 70px;
						  font-weight: 500;" id="employment"></li>


        <!-- HÀNG2 -->


        <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 20px;">
            <div style="background-color: #000000;height: 0px;width: 0px;">
                <div class="line" style="background-color: #581b0f;height: 1px;"></div>

            </div>

        </div>
        <div class="" style="width: 100px: 30px;margin: 40px; background-color: rgb(255, 238, 221) ;">
            <h1 style="margin: 0;padding: 15px 0px 10px 24px;color: rgb(101, 123, 139);font-size: 24px;">EDUCATION</h1>
        </div>

        <div id="education-preview">
            <div class="education-area">

            </div>
        </div>

        <template id="education-field">
            <div id="_idx">
                <div class="row" style="list-style: none; margin: 30px 42px 0; justify-content: space-between;">
                    <li class="flex width380" style="    font-size: 22px;
			  font-weight: 400;line-height: 1.4em;letter-spacing: 0em;
			  color: rgb(255, 238, 221);">

                        <div><i><u id="educationName"></u></i></div>
                    </li>
                    <li class="flex width176" style="    font-size: 20px;
			  color: rgb(255, 238, 221);
			  margin-top: 3px;">

                        <div id="educationFromTo"></div>
                    </li>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 0 42px 30px;
			  line-height: 1.5; justify-content: space-between;" class="row">
                    <li style="display: flex; font-size: 20px;" class="width380" id="educationMajor"></li>
                    <li style="display: flex; font-size: 20px;" class="width176" id="educationAttend"></li>
                </div>
            </div>
        </template>

        <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 20px;">
            <div style="background-color: #000000;height: 0px;width: 0px;">
                <div class="line" style="background-color: #581b0f;height: 1px;"></div>

            </div>

        </div>
        <div class="" style="width: 100px: 30px;margin: 40px; background-color: rgb(255, 238, 221) ;">
            <h1 style="margin: 0;padding: 15px 0px 10px 24px;color: rgb(101, 123, 139);font-size: 24px;">WORK
                EXPERIENCE</h1>
        </div>

        <div id="experience-preview">
            <div class="experience-area">

            </div>
        </div>

        <template id="experience-field">
            <div id="_idx">
                <div class="row" style="list-style: none; margin: 30px 42px 0; justify-content: space-between;">
                    <li class="flex width380" style="    font-size: 22px;
			  font-weight: 400;line-height: 1.4em;letter-spacing: 0em;
			  color: rgb(255, 238, 221);">

                        <div><i><u id="experienceName"></u></i></div>
                    </li>
                    <li class="flex width176" style="    font-size: 20px;
			  color: rgb(255, 238, 221);
			  margin-top: 3px;">

                        <div id="experienceFromTo"></div>
                    </li>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 0 42px 10px;
			  line-height: 1.5; justify-content: space-between;" class="row">
                    <li style="display: flex; font-size: 20px;" class="width380" id="experienceMajor"></li>
                    <li style="display: flex; font-size: 20px;" class="width176" id="experienceAttend"></li>
                </div>

                <div class="row" style="list-style: none; margin: 10px 42px 0 84px;">
                    <li class="flex" style="    font-size: 20px;
			  font-weight: 400;line-height: 1.4em;letter-spacing: 0em;
			  color: rgb(255, 238, 221);">
                        <div>Achievements:</div>
                    </li>
                </div>

                <div class="sub-experience-area"></div>
            </div>
        </template>

        <template id="sub-experience-field">
            <div id="_subidx">
                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 15px 42px 5px 84px;
			  line-height: 1.5; justify-content: space-between;" class="row">
                    <li style="display: flex; font-size: 20px;" class="width338" id="sub_experienceName"></li>
                    <li style="display: flex; font-size: 18px;color: rgb(255, 238, 221);" class="width176" id="sub_experienceFromTo"></li>
                </div>
                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 5px 42px 15px 84px;
			  line-height: 1.5;" class="row">
                    <li style="display: flex; font-size: 20px;" id="sub_experienceDescription"></li>
                </div>
            </div>
        </template>

        <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 20px;">
            <div style="background-color: #000000;height: 0px;width: 0px;">
                <div class="line" style="background-color: #581b0f;height: 1px;"></div>

            </div>

        </div>
        <div class="" style="width: 100px: 30px;margin: 40px; background-color: rgb(255, 238, 221) ;">
            <h1 style="margin: 0;padding: 15px 0px 10px 24px;color: rgb(101, 123, 139);font-size: 24px;">ACTIVITIES</h1>
        </div>

        <div id="activity-preview">
            <div class="activity-area">

            </div>
        </div>

        <template id="activity-field">
            <div id="_idx">
                <div class="row" style="list-style: none; margin: 30px 42px 0; justify-content: space-between;">
                    <li class="flex width380" style="    font-size: 22px;
			  font-weight: 400;line-height: 1.4em;letter-spacing: 0em;
			  color: rgb(255, 238, 221);">

                        <div><i><u id="activityName"></u></i></div>
                    </li>
                    <li class="flex width176" style="    font-size: 20px;
			  color: rgb(255, 238, 221);
			  margin-top: 3px;">

                        <div id="activityFromTo"></div>
                    </li>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 0 42px 10px;
			  line-height: 1.5;" class="row">
                    <div id="activityAttend" style="font-size: 20px;"></div>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 10px 42px 30px;
			  line-height: 1.5;" class="row">
                    <li style="display: flex; font-size: 20px;" id="activityDescription"></li>
                </div>
            </div>
        </template>

        <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 20px;">
            <div style="background-color: #000000;height: 0px;width: 0px;">
                <div class="line" style="background-color: #581b0f;height: 1px;"></div>

            </div>

        </div>
        <div class="" style="width: 100px: 30px;margin: 40px; background-color: rgb(255, 238, 221) ;">
            <h1 style="margin: 0;padding: 15px 0px 10px 24px;color: rgb(101, 123, 139);font-size: 24px;">
                CERTIFICATIONS</h1>
        </div>

        <div id="certification-preview">
            <div class="certification-area">

            </div>
        </div>

        <template id="certification-field">
            <div id="_idx">
                <div class="row" style="list-style: none; margin: 30px 42px 0; justify-content: space-between;">
                    <li class="flex width380" style="    font-size: 22px;
			  font-weight: 400;line-height: 1.4em;letter-spacing: 0em;
			  color: rgb(255, 238, 221);">

                        <div><i><u id="certificationName"></u></i></div>
                    </li>
                    <li class="flex width176" style="    font-size: 20px;
			  color: rgb(255, 238, 221);
			  margin-top: 3px;">

                        <div id="certificationFromToType"></div>
                    </li>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 0 42px 10px;
			  line-height: 1.5;" class="row">
                    <div id="certificationMajor" style="font-size: 20px;"></div>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 10px 42px 30px;
			  line-height: 1.5;" class="row">
                    <li style="display: flex; font-size: 20px;" id="certificationDescription"></li>
                </div>
            </div>
        </template>

        <div class="h1-colume2" style="display: flex;margin: 40px 24px 0px 20px;">
            <div style="background-color: #000000;height: 0px;width: 0px;">
                <div class="line" style="background-color: #581b0f;height: 1px;"></div>

            </div>

        </div>
        <div class="" style="width: 100px: 30px;margin: 40px; background-color: rgb(255, 238, 221) ;">
            <h1 style="margin: 0;padding: 15px 0px 10px 24px;color: rgb(101, 123, 139);font-size: 24px;">LINKS</h1>
        </div>

        <div id="links-preview">
            <div class="links-area">

            </div>
        </div>

        <template id="links-field">
            <div id="_idx">
                <div class="row" style="list-style: none; margin: 30px 42px 0;">
                    <li class="flex" style="    font-size: 22px;
			  font-weight: 400;line-height: 1.4em;letter-spacing: 0em;
			  color: rgb(255, 238, 221);">
                        <div><i><u id="linksName"></u></i></div>
                    </li>
                </div>

                <div style="list-style: none;
			  color: rgb(255, 238, 221);
			  font-size: 17px;
			  margin: 10px 42px 30px;
			  line-height: 1.5;" class="row">
                    <li style="display: flex; font-size: 20px;" id="linksDescription"></li>
                </div>
            </div>
        </template>
    </div>
</div>';
}

if ($id == 7) {
    echo '<link rel="stylesheet" href="cv_templates/CV_template_07/20092021.css">
<link rel="stylesheet" href="assets/css/style.css">

<div class="all" id="capture">
    <div class="column_left">
        <div class="hehe">
            <div class="image">
                <img class="avatar" src="" alt="" id="userImage">
            </div>
        </div>
        <h3 style="font-family: helvetica;color: white; text-align: center;font-size: 20px;margin-top: 20px;" id="name"></h3>
        <H5 style="color: #ccc; text-align: center;font-size: 16px;" id="employment"></H5>
        <div style="display: flex;margin: 40px 8px 0;">
            <div class="icon" style=";border-radius: 50px; width: 28px; height: 28px;">
                <i class="fab fa-artstation" style="color: white;padding-left: 7px;padding-top: 7px;"></i>
            </div>
            <h5 style="color:white;margin-left: 8px;font-size: 16px;">Salary</h5>
        </div>

        <h5 style="font-size: 14px; color:white;margin: 0 8px;font-weight: 400;margin-top:24px;" id="hopeSalary"></h5>

        <div style="display: flex;margin: 40px 8px 0;">
            <div class="icon" style=";border-radius: 50px; width: 28px; height: 28px;">
                <i class="far fa-atom" style="color: white;padding-left: 7px;padding-top: 7px;"></i>
            </div>
            <h5 style="color:white;margin-left: 8px;font-size: 16px;">Information</h5>

        </div>

        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: bold;margin-top:24px;">Final Education</h5>
        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: 400;" id="finalEducation"></h5>

        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: bold;margin-top:24px;">Experience</h5>
        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: 400;" id="experienceWork"></h5>

        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: bold;margin-top:24px;">Skill groups</h5>
        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: 400;" id="skillGroups"></h5>

        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: bold;margin-top:24px;">Skills</h5>
        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: 400;" id="skills"></h5>

        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: bold;margin-top:24px;">Workplaces</h5>
        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: 400;" id="workplaces"></h5>

        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: bold;margin-top:24px;">Korean Language Level</h5>
        <h5 style="font-size: 14px;color:white;margin-left: 8px;margin-right: 8px;font-weight: 400;" id="koreanLanguageLevel"></h5>
    </div>

    <div class="column_right" style="width: 100%;">

        <div class="headerall" style="background-color:#282C40;MARGIN: 16PX;border-radius: 10PX;">
            <h4 style="color: white;    padding: 24px 24px 0;font-size: 18px;">PROFILE</h4>
            <div class="header" style=" ;max-width: 100%;">
                <div style="max-width: 45%;" class="header1">
                    <div style="display: flex;">
                        <i class="fas fa-birthday-cake" style="color: #ccc;font-size: 12px;padding-top: 4px"></i>
                        <h5 style="color: white;font-size: 14px;margin-left: 8px;" id="dateOfBirth"></h5>
                    </div>

                    <div style="display: flex;">
                        <i class="fas fa-phone-alt" style="color: #ccc;font-size: 12px;padding-top: 4px"></i>
                        <h5 style="color: white;font-size: 14px;margin-left: 8px;word-break: break-all;" id="phone"></h5>
                    </div>

                </div>


                <div style="max-width: 45%;" class="header2">
                    <div style="display: flex;">
                        <i class="fas fa-envelope" style="color: #ccc;font-size: 12px;padding-top: 4px"></i>
                        <h5 style="color: white;font-size: 14px;margin-left: 8px;word-break: break-all;" id="email2"></h5>
                    </div>



                    <div style="display: flex;">
                        <i class="fas fa-map-marker-alt" style="color: #ccc;font-size: 12px;padding-top: 4px"></i>
                        <h5 style="color: white;font-size: 14px;margin-left: 8px;" id="address"></h5>
                    </div>

                </div>
            </div>

        </div>

        <div class="headerrow2" style="background-color:#282C40 ;max-width: 100%;MARGIN:16PX;border-radius: 10PX;">

            <div style="display: flex; margin: 0 24px;padding-top: 24px;">
                <div class="icon" style="border-radius: 50px;width: 28px; height: 28px;">
                    <i class="fas fa-graduation-cap"
                       style="color: white;margin: auto;padding-left: 6px;padding-top: 6px;"></i>
                </div>
                <h4 class="title_right" style="font-size: 18px;">EDUCATION</h4>
            </div>

            <div class="experience_left" style="margin: 16px 24px 0;padding-bottom: 24px;">

                <div id="education-preview">
                    <div class="education-area">

                    </div>
                </div>

                <template id="education-field">
                    <div id="_idx">
                        <div class="yearcity">
                            <h5 style="color: white;font-size: 16px;max-width: 260px;" id="educationName"></h5>
                            <div class="icon" style="width: 80px;border-radius: 2px;">
                                <h5 class="year1" id="educationFromTo"></h5>
                            </div>
                        </div>
                        <h5 style="color: white;font-size: 14px;" id="educationMajor"></h5>
                        <h5 style="color: white;font-size: 14px;" id="educationAttend"></h5>
                    </div>
                </template>

            </div>

        </div>

        <div class="headerrow2" style="background-color:#282C40 ;max-width: 100%;MARGIN:16PX;border-radius: 10PX;">

            <div style="display: flex; margin: 0 24px;padding-top: 24px;">
                <div class="icon" style="border-radius: 50px;width: 28px; height: 28px;">
                    <i class="fas fa-briefcase"
                       style="color: white;margin: auto;padding-left: 7px;padding-top: 7px;"></i>
                </div>
                <h4 class="title_right" style="font-size: 18px;">WORK EXPERIENCE</h4>
            </div>

            <div class="experience_left" style="margin: 16px 24px;">

                <div id="experience-preview">
                    <div class="experience-area">

                    </div>
                </div>

                <template id="experience-field">
                    <div id="_idx">
                        <div class="yearcity">
                            <h5 style="color: white;font-size: 16px;max-width: 260px;" id="experienceName"></h5>
                            <div class="icon" style="width: 80px;border-radius: 2px;">
                                <h5 class="year1" id="experienceFromTo"></h5>
                            </div>
                        </div>
                        <h5 style="color: white;font-size: 14px;" id="experienceMajor"></h5>
                        <p style="color: white;font-weight: 300;font-size: 14px;" id="experienceAttend"></p>

                        <div class="experience_right">
                            <h5 style="color: white;font-size: 14px;">Achievements:</h5>

                            <div class="sub-experience-area"></div>
                        </div>
                    </div>
                </template>

                <template id="sub-experience-field">
                    <div id="_subidx">
                        <div style="display: flex;justify-content: space-between;margin-top: 24px;align-items: flex-start;">
                            <h5 style="color: white;font-size: 12px;max-width: 240px;" id="sub_experienceName"></h5>
                            <div class="icon" style="width: 80px;border-radius: 2px;">
                                <h5 class="year2" style="font-size: 12px; text-align: center;color: white;margin: auto;" id="sub_experienceFromTo"></h5>
                            </div>
                        </div>
                        <p style="color: white;font-weight: 300;font-size: 12px;" id="sub_experienceDescription"></p>
                    </div>
                </template>

            </div>

        </div>

        <div class="headerrow2" style="background-color:#282C40 ;max-width: 100%;MARGIN:16PX;border-radius: 10PX;">

            <div style="display: flex; margin: 0 24px;padding-top: 24px;">
                <div class="icon" style="border-radius: 50px;width: 28px; height: 28px;">
                    <i class="fas fa-crosshairs"
                       style="color: white;margin: auto;padding-left: 7px;padding-top: 7px;"></i>
                </div>
                <h4 class="title_right" style="font-size: 18px;">ACTIVITIES</h4>
            </div>

            <div class="experience_left" style="margin: 16px 24px 0;">

                <div id="activity-preview">
                    <div class="activity-area">

                    </div>
                </div>

                <template id="activity-field">
                    <div id="_idx">
                        <div class="yearcity">
                            <h5 style="color: white;font-size: 16px;max-width: 260px;" id="activityName"></h5>
                            <div class="icon" style="width: 80px;border-radius: 2px;">
                                <h5 class="year1" id="activityFromTo"></h5>
                            </div>
                        </div>
                        <h5 style="color: white;font-size: 14px;" id="activityAttend"></h5>
                        <p style="color: white;font-weight: 300;padding-bottom: 32px;font-size: 14px;" id="activityDescription"></p>
                    </div>
                </template>

            </div>
        </div>

        <div class="headerrow2" style="background-color:#282C40 ;max-width: 100%;MARGIN:16PX;border-radius: 10PX;">

            <div style="display: flex; margin: 0 24px;padding-top: 24px;">
                <div class="icon" style="border-radius: 50px;width: 28px; height: 28px;">
                    <i class="fas fa-certificate"
                       style="color: white;margin: auto;padding-left: 7px;padding-top: 7px;"></i>
                </div>
                <h4 class="title_right" style="font-size: 18px;">CERTIFICATIONS</h4>
            </div>

            <div class="experience_left" style="margin: 16px 24px 0;">

                <div id="certification-preview">
                    <div class="certification-area">

                    </div>
                </div>

                <template id="certification-field">
                    <div id="_idx">
                        <div class="yearcity">
                            <h5 style="color: white;font-size: 16px;max-width: 260px;" id="certificationName"></h5>
                            <div class="icon" style="width: 80px;border-radius: 2px;">
                                <h5 class="year1" id="certificationFromToType"></h5>
                            </div>
                        </div>
                        <h5 style="color: white;font-size: 14px;" id="certificationMajor"></h5>
                        <p style="color: white;font-weight: 300;padding-bottom: 32px;font-size: 14px;" id="certificationDescription"></p>
                    </div>
                </template>

            </div>
        </div>

        <div class="headerrow2" style="background-color:#282C40 ;max-width: 100%;MARGIN:16PX;border-radius: 10PX;">

            <div style="display: flex; margin: 0 24px;padding-top: 24px;">
                <div class="icon" style="border-radius: 50px;width: 28px; height: 28px;">
                    <i class="fas fa-link"
                       style="color: white;margin: auto;padding-left: 7px;padding-top: 7px;"></i>
                </div>
                <h4 class="title_right" style="font-size: 18px;">LINKS</h4>
            </div>

            <div class="experience_left" style="margin: 16px 24px 0;">

                <div id="links-preview">
                    <div class="links-area">

                    </div>
                </div>

                <template id="links-field">
                    <div id="_idx">
                        <h5 style="color: white;font-size: 16px;" id="linksName"></h5>
                        <p style="color: white;font-weight: 300;padding-bottom: 32px;font-size: 14px;word-break: break-all;" id="linksDescription"></p>
                    </div>
                </template>

            </div>
        </div>
    </div>
</div>';
}

?>