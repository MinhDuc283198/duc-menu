<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

// $context = context_system::instance();

// require_sesskey();

$resumeid = required_param('resumeid', PARAM_INT);
$resume = $DB->get_record('vi_resumes', array('id' => $resumeid));
if ($resume == null) {
    // throw new moodle_exception('invalidarguments');
    die();
}

include_once($VISANG->dirroot . '/lib/resumes.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');

$user = $DB->get_record('user', array('id' => $resume->user_id));
$resume_data = mkjobs_resume_get_fields($resumeid);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo $CFG->wwwroot; ?>/theme/oklassedu/style/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet" />
    <link href="<?php echo $CFG->wwwroot; ?>/theme/styles.php/oklassedu/1581190129/all" rel="stylesheet" />
</head>

<body>
    <div class="layerpop window style02">
        <div class="pop-contents">
            <div class="resume-inf-bx first">
                <div class="tit">
                    안녕하세요. 김비상입니다.
                </div>
                <div class="u-img">
                    <img src="<?php echo $resume->avatar == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg' : $CFG->wwwroot . '/pluginfile.php/' . $resume->avatar; ?>" alt="김비상" />
                </div>
                <div class="txt">
                    <p class="rw">
                        <span>이름</span>
                        <strong><?php echo $resume_data->info ? $resume_data->info->fullname : $user->firstname . $user->lastname; ?></strong>
                    </p>
                    <p class="rw">
                        <span>생년월일</span>
                        <?php
                        $birth_array = explode('-', $user->phone2);
                        ?>
                        <strong><?php echo $birth_array[0] . '-' . $birth_array[1] . '-' . $birth_array[2]; ?></strong>
                    </p>
                    <p class="rw">
                        <span>이메일</span>
                        <strong><?php echo $user->email; ?></strong>
                    </p>
                    <p class="rw">
                        <span>휴대폰번호</span>
                        <strong><?php echo $user->phone1; ?></strong>
                    </p>
                    <p class="rw w100">
                        <span>주소</span>
                        <strong>
                            <?php echo $resume_data->info ? $resume_data->info->address1 : ''; ?><br />
                            <?php echo $resume_data->info ? $resume_data->info->address2 : ''; ?>
                        </strong>
                    </p>
                </div>
            </div>

            <div class="resume-inf-bx">
                <div class="tit">희망근무조건</div>
                <div class="txt">
                    <div class="rw">
                        <p>
                            <span>고용형태</span>
                            <strong><?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->type : ''; ?></strong>
                        </p>

                        <p>
                            <span>희망연봉</span>
                            <strong>
                                <?php if ($resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1') : ?>
                                    면접 시 협의
                                <?php else : ?>
                                    <?php echo $resume_data->hope_to_work ? number_format($resume_data->hope_to_work->salary_from) : ''; ?> ~ <?php echo $resume_data->hope_to_work ? number_format($resume_data->hope_to_work->salary_to) : ''; ?> <?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_unit : '' ?>
                                <?php endif; ?>
                            </strong>
                        </p>
                    </div>
                </div>
            </div>

            <div class="resume-inf-bx">
                <div class="tit">학력</div>

                <div class="txt">
                    <?php if ($resume_data->education && is_array($resume_data->education->edu)) : ?>
                        <?php foreach ($resume_data->education->edu as $edu) : ?>
                            <div class="rw">
                                <p><span><?php echo $edu->from_year; ?>.<?php echo $edu->from_month; ?> – <?php echo $edu->to_year; ?>.<?php echo $edu->to_month; ?></span></p>
                                <div class="history">
                                    <strong>
                                        <?php
                                        switch ($edu->type) {
                                            case 'university':
                                                echo 'University';
                                                break;
                                            case 'college':
                                                echo 'College';
                                                break;
                                            case 'high_school':
                                                echo 'High School';
                                                break;
                                            default:
                                                echo '학교구분';
                                                break;
                                        }
                                        ?>
                                    </strong>
                                    <p class="inline-b">
                                        <span><?php echo $edu->school_name; ?></span>
                                        <span><?php echo $edu->degree; ?></span>
                                    </p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>


            <div class="resume-inf-bx">
                <div class="tit">경력</div>

                <div class="txt">
                    <?php if ($resume_data->experience && is_array($resume_data->experience->exp)) : ?>
                        <?php foreach ($resume_data->experience->exp as $exp) : ?>
                            <div class="rw bt-br">
                                <p><span><?php echo $exp->from_year; ?>.<?php echo $exp->from_month; ?> – <?php echo $exp->to_year; ?>.<?php echo $exp->to_month; ?></span></p>
                                <div class="history">
                                    <p class="inline-b">
                                        <span>
                                            <?php if ($exp->in_office == 1) : ?>
                                                <em class="t-blue">[재직중]</em>
                                            <?php endif; ?>
                                            <?php echo $exp->company_name; ?>
                                        </span>
                                        <span><?php echo $exp->department; ?></span>
                                    </p>
                                    <div>
                                        <strong class="f18 mg-tp10">[주요성과]</strong>
                                        <div class="w-txt">
                                            <?php if (is_array($exp->exp_achievement)) : ?>
                                                <?php foreach ($exp->exp_achievement as $exp_achievement) : ?>
                                                    <p class="t-dot mg-bt10"><?php echo $exp_achievement->title; ?></p>
                                                    <p class="t-gray mg-bt10"><?php echo $exp_achievement->content; ?></p>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>



            <div class="resume-inf-bx">
                <div class="tit">교육 수료/이수 및 기타 활동</div>
                <?php if ($resume_data->activity && is_array($resume_data->activity->act)) : ?>
                    <?php foreach ($resume_data->activity->act as $act) : ?>
                        <?php if ($act->type == 'activity') : ?>
                            <div class="txt">
                                <div class="rw">
                                    <p><span><?php echo $act->from_year; ?>.<?php echo $act->from_month; ?> – <?php echo $act->to_year; ?>.<?php echo $act->to_month; ?></span></p>
                                    <div>
                                        <p class="mg-bt10"><?php echo $act->activity_name; ?></p>
                                        <p class="t-gray"><?php echo $act->activity_content; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($act->type == 'master_korean_course') : ?>
                            <div class="bdg-area">
                                <div class="t-blue02"><?php echo $act->master_korean_title; ?></div>
                                <table class="table">
                                    <colgroup>
                                        <col width="15%" />
                                        <col width="/" />
                                        <col width="15%" />
                                    </colgroup>
                                    <tbody>
                                        <?php if ($act->master_korean_course && is_array($act->master_korean_course)) : ?>
                                            <?php foreach ($act->master_korean_course as $mkc) : ?>
                                                <?php if ($mkc->courseid) : ?>
                                                    <?php
                                                    $course = $DB->get_record('lmsdata_class', array('id' => $mkc->courseid));
                                                    $pro = format_lguplus_get_course_progress($course->courseid);
                                                    switch (current_language()) {
                                                        case 'ko':
                                                            $coursename = $course->title;
                                                            break;
                                                        case 'en':
                                                            $coursename = $course->en_title;
                                                            break;
                                                        case 'vi':
                                                            $coursename = $course->vi_title;
                                                            break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($pro['course']->courseprogress < 100) : ?>
                                                                -
                                                            <?php else : ?>
                                                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/ic_badge.png'; ?>" alt="" />
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-left">
                                                            <p><?php echo $coursename; ?></p>
                                                            <p class="t-gray"><?php echo $mkc->coursedate; ?></p>
                                                        </td>
                                                        <td><?php echo $pro['course']->courseprogress < 100 ? '진행중' : '수료'; ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>


            <div class="resume-inf-bx">
                <div class="tit">자격증</div>
                <div class="txt">
                    <div class="rw">
                        <div class="w100">
                            <?php if ($resume_data->certification && is_array($resume_data->certification->cer)) : ?>
                                <?php foreach ($resume_data->certification->cer as $cer) : ?>
                                    <p class="w100 t-dot">
                                        <span><?php echo $cer->title; ?></span>
                                        <span>발행처 : <?php echo $cer->certificate_authority; ?></span>
                                        <span>취득일 : <?php echo $cer->date; ?></span>
                                    </p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="resume-inf-bx">
                <div class="tit">링크</div>
                <div class="txt">
                    <div class="rw">
                        <?php if ($resume_data->links && is_array($resume_data->links->link)) : ?>
                            <?php foreach ($resume_data->links->link as $link) : ?>
                                <p class="w100 t-dot">
                                    <?php echo $link->title; ?><br />
                                    <a href="<?php echo $link->url; ?>" target="_blank"><?php echo $link->url; ?></a>
                                </p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="resume-inf-bx">
                <div class="tit">증빙서류</div>
                <div class="txt">
                    <div class="rw">
                        <ul class="file-list">
                            <?php if ($resume_data->attachment && is_array($resume_data->attachment->file)) : ?>
                                <?php foreach ($resume_data->attachment->file as $file) : ?>
                                    <li><a href="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $file->attachmenturl; ?>" target="_blank"><?php echo $file->resumetitle; ?></a></li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="my-box bg">
                <div class="sub-tit">관심정보</div>
                <div class="rw">
                    <strong>경력 유무</strong>
                    <p>
                        <?php
                        $sql = "SELECT w.name
                                FROM {vi_user_work_experiences} u
                                JOIN {vi_work_experiences} w ON w.id=u.work_experience_id AND u.user_id=:userid";
                        $work_experience = $DB->get_record_sql($sql, array('userid' => $user->id));
                        echo $work_experience ? $work_experience->name : '';
                        ?>
                    </p>
                </div>
                <div class="rw">
                    <strong>관심 직군</strong>
                    <p>
                        <?php
                        $groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
                        $skill_groups = array();
                        foreach ($groups as $group) {
                            $skill_groups[] = $group->name;
                        }
                        echo implode(", ", $skill_groups);
                        ?>
                    </p>
                </div>
                <div class="rw">
                    <strong>관심 직무</strong>
                    <p>
                        <?php
                        $skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $user->id));
                        $user_skills = array();
                        foreach ($skills as $skill) {
                            $user_skills[] = $skill->name;
                        }
                        echo implode(", ", $user_skills);
                        ?>
                    </p>
                </div>
                <div class="rw">
                    <strong>희망 지역</strong>
                    <p>
                        <?php
                        $districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $user->id));
                        $workplaces = array();
                        foreach ($districts as $district) {
                            $workplaces[] = $district->name;
                        }
                        echo implode(", ", $workplaces);
                        ?>
                    </p>
                </div>
                <div class="rw">
                    <strong>한국어 활용 수준</strong>
                    <p>
                        <?php
                        $sql = "SELECT l.name
                                FROM {vi_user_korean_levels} u
                                JOIN {vi_korean_levels} l ON l.id=u.korean_level_id AND u.user_id=:userid";
                        $korean_level = $DB->get_record_sql($sql, array('userid' => $user->id));
                        echo $korean_level ? $korean_level->name : '';
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>