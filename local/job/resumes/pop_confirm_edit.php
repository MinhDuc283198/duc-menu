<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

$action = required_param('action', PARAM_RAW);

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
    <?php echo get_string('continue', 'local_job'); ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>

    <div class="pop-contents">
    </div>
    <div class="btn-area text-center">
        <input type="button" value="<?php echo get_string('cancel', 'local_job'); ?>" class="btns" onclick="$('input[name=action]').val(''); $('.pop-close').click();" />
        <input type="button" onclick="save('<?php echo $action; ?>')" value="<?php echo get_string('Confirm', 'local_job'); ?>" class="btns point" />
    </div>

</div>
<?php
echo $OUTPUT->footer();
