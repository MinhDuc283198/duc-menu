<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// $baseurl = new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array());
$resumeid = optional_param('resumeid', 0, PARAM_INT);
$resume = null;

if ($resumeid != 0) {
    $resume = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));
}

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
    <?php echo get_string('YouTubevideo', 'local_job'); ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>
    <form method="post" enctype="multipart/form-data" onsubmit="return validateVideoUpload(this);">
        <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
        <input type="hidden" name="action" value="videoupload">
        <input type="hidden" name="resumeid" value="<?php echo $resumeid; ?>">

        <div class="pop-contents">
            <div class="addr-info">
                <div class="rw mg-bt0">
                    <input type="text" name="cvvideo" value="<?php echo $resume ? $resume->video_url : ''; ?>" required id="cvvideo" placeholder="<?php echo get_string('EnterYouTubeURL', 'local_job'); ?>" />
                </div>
                <div class="radio-grp">
                    <label><input type="radio" name="published" <?php echo $resume && $resume->published == 1 ? 'checked' : ''; ?> value="1" /><?php echo get_string('Exposure', 'local_job'); ?></label>
                    <label><input type="radio" name="published" <?php echo $resume && $resume->published == 0 ? 'checked' : ''; ?> value="0" /><?php echo get_string('Non-exposure', 'local_job'); ?></label>
                </div>
            </div>
        </div>
        <div class="btn-area text-center">
            <input type="button" value="<?php echo get_string('cancel', 'local_job'); ?>" class="btns" onclick="$('.pop-close').click();" />
            <input type="submit" value="<?php echo get_string('Save', 'local_job'); ?>" class="btns point" />
        </div>
    </form>
</div>
<?php
echo $OUTPUT->footer();
