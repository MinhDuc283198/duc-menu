<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');
// require_once($CFG->dirroot . '/local/course/lib.php');

// $type = 0; // 0 = 수강중인 강좌 / 1 = 담당 강좌 
$courses = [];
$ing_courses = local_mypage_get_mycourse('ing');
$end_courses = local_mypage_get_mycourse('end');
foreach ($ing_courses as $ing_course) {
    // $iscom = $DB->get_fieldset_sql('SELECT timecompleted FROM {course_completions} WHERE userid = :userid and course = :courseid', array('userid' => $USER->id, 'courseid' => $ing_course->lcid));
    // if (!(empty($iscom) || $iscom[0] <= 0)) {
    array_push($courses, $ing_course);
    // }
}
foreach ($end_courses as $end_course) {
    $iscom = $DB->get_fieldset_sql('SELECT timecompleted FROM {course_completions} WHERE userid = :userid and course = :courseid', array('userid' => $USER->id, 'courseid' => $end_course->lcid));
    if (!(empty($iscom) || $iscom[0] <= 0)) {
        array_push($courses, $end_course);
    }
}

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('ImportCourseHistory', 'local_job'); ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>

    <div class="pop-contents">
        <div class="f16 mg-bt10">※ <?php echo get_string('progressorcompleted', 'local_job'); ?></div>
        <table class="table">
            <thead>
                <tr>
                    <th width="30px"></th>
                    <th><?php echo get_string('cv:edit:namecourse', 'local_job'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($courses as $course) : ?>
                    <?php
                    $pro = format_lguplus_get_course_progress($course->lcid);
                    switch (current_language()) {
                        case 'ko':
                            $coursename = $course->title;
                            break;
                        case 'en':
                            $coursename = $course->en_title;
                            break;
                        case 'vi':
                            $coursename = $course->vi_title;
                            break;
                    }
                    ?>
                    <tr>
                        <td><input type="checkbox" class="mkcourse" data-courseid="<?php echo $course->id; ?>" data-coursename="<?php echo $coursename; ?>" data-coursedate="<?php echo date('Y.m.d', $course->timemodified); ?>~<?php echo date('Y.m.d', $course->enddate); ?>" data-courseprogress="<?php echo $pro['course']->courseprogress; ?>" /></td>
                        <td class="text-left">
                            <p><?php echo $coursename; ?></p>
                            <p class="t-gray"><?php echo date('Y.m.d', $course->timemodified); ?>~<?php echo date('Y.m.d', $course->enddate); ?></p>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if (count($courses) == 0) : ?>
                    <tr>
                        <td colspan="2"><?php echo get_string('cv:edit:nocourse', 'local_job'); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="btn-area text-center">
        <?php if (count($courses) > 0) : ?>
            <input type="button" onclick="addMasterKoreanCourse()" value="<?php echo get_string('Confirm', 'local_job'); ?>" class="btns point w100" />
        <?php endif; ?>
    </div>

</div>
<?php
echo $OUTPUT->footer();
