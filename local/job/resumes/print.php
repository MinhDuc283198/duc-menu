<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_sesskey();

require_once(dirname(__FILE__) . '/../lib/dompdf/autoload.inc.php');

use Dompdf\Dompdf;
use Dompdf\Options;

$resumeid = required_param('resumeid', PARAM_INT);

$html = file_get_contents($VISANG->wwwroot . '/resumes/print_resume_content.php?resumeid=' . $resumeid . '&sesskey=' . sesskey());

$options = new Options();
$options->set('isRemoteEnabled', TRUE);
$options->set('tempDir', dirname(__FILE__) . '/../lib/dompdf/tmp');
$options->set('debugKeepTemp', TRUE);
$options->set('isHtml5ParserEnabled', true);

$dompdf = new Dompdf($options);
$dompdf->loadHtml($html);
// $dompdf->setPaper('A4', 'landscape');
$dompdf->render();
$dompdf->stream("dompdf_out.pdf", array("Attachment" => 0));
