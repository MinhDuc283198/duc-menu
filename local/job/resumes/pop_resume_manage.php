<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

$resumetitle = optional_param('resumetitle', '', PARAM_RAW);

$resumes = $DB->get_records_select('vi_resumes', 'user_id=:userid AND (type=:cvfile OR type=:file)', array('userid' => $USER->id, 'cvfile' => 'cvfile', 'file' => 'file'), '{vi_resumes}.timemodified DESC');

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('Selectevidencefile', 'local_job'); ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>

    <div class="pop-contents">
        <div class="f16 mg-bt10"><?php echo get_string('Resumeintitle', 'local_job'); ?>: <?php echo $resumetitle; ?>.</div>
        <table class="table">
            <thead>
                <tr>
                    <th width="30px"></th>
                    <th><?php echo get_string('Filetitle', 'local_job'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resumes as $resume) : ?>
                    <tr>
                        <td><input type="checkbox" class="attachment-file" data-resumeid="<?php echo $resume->id; ?>" data-resumetitle="<?php echo $resume->title; ?>" data-attachmenturl="<?php echo $resume->attachment_url; ?>" /></td>
                        <td class="text-left"><?php echo $resume->title; ?></td>
                    </tr>
                <?php endforeach; ?>
                <?php if (count($resumes) == 0) : ?>
                    <tr>
                        <td colspan="2"><?php echo get_string('resume:notfileuploaded', 'local_job'); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="btn-area text-center">
        <?php if (count($resumes) > 0) : ?>
            <input type="button" value="<?php echo get_string('Fileselection', 'local_job'); ?>" onclick="addAttachment()" class="btns point w100" />
        <?php endif; ?>
    </div>
</div>
<?php
echo $OUTPUT->footer();
