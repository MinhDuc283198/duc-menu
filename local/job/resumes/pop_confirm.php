<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// $baseurl = new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array());
$resumeid = required_param('resumeid', PARAM_INT);
$action = required_param('action', PARAM_RAW);

$resume = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));

if ($resume == null) {
    throw new moodle_exception('invalidarguments');
}

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php
        switch ($action) {
            case 'setasdefault':
                echo get_string('tosetasdefault', 'local_job');
                break;
            case 'deleteresume':
                echo get_string('wanttodelete', 'local_job');
                break;
            default:
                echo get_string('continue', 'local_job');
                break;
        }
        ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>
    <form method="post">
        <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
        <input type="hidden" name="action" value="<?php echo $action; ?>">
        <input type="hidden" name="resumeid" value="<?php echo $resumeid; ?>">

        <div class="pop-contents">
        </div>
        <div class="btn-area text-center">
            <input type="button" value="<?php echo get_string('cancel', 'local_job'); ?>" class="btns" onclick="$('.pop-close').click();" />
            <input type="submit" value="<?php echo get_string('Confirm', 'local_job'); ?>" class="btns point" />
        </div>
    </form>
</div>
<?php
echo $OUTPUT->footer();
