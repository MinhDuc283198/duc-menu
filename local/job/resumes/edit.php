<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
$resumeid = required_param('resumeid', PARAM_INT);
$resume = $DB->get_record('vi_resumes', array('id' => $resumeid));
$resume_cv=$DB->get_records('vi_resume_cv');
$sql="SELECT
            *
            FROM {vi_resume_cv} cre
            WHERE cre.id=$resume->resume_cv_id
            ";

$resume_main=$DB->get_record_sql($sql);
if ($resume == null || $resume->user_id != $USER->id) {
    throw new moodle_exception('invalidarguments');
}

include_once($VISANG->dirroot . '/lib/resumes.php');

$baseurl = new moodle_url($VISANG->wwwroot . '/resumes/edit.php', array('resumeid' => $resumeid));
$action = optional_param('action', '', PARAM_RAW);

if ($action == 'attachmenturl') {
    define('AJAX_SCRIPT', true);
    echo json_encode(array('attachment_url' => $resume->attachment_url != '' ? $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url : ''));
    die();
}

if ($action == 'draft' || $action == 'save' || $action == 'auto-draft') {
    require_sesskey();
    $fs = get_file_storage();
    $title = required_param('title', PARAM_RAW);

    $firstname = required_param('firstname', PARAM_RAW);
    $dateofbirth = optional_param('dateofbirth', '1970-01-01', PARAM_RAW);
    $phone1 = optional_param('phone1', '', PARAM_RAW);
    // $email = required_param('email', PARAM_RAW);

    $user = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
    $user->firstname = $firstname;
    // $user->email = $email;
    $user->phone1 = $phone1;
    $user->phone2 = $dateofbirth;
    $user->timemodified = time();
    $DB->update_record('user', $user);

    $zipcode = optional_param('zipcode', '', PARAM_RAW);
    $address = optional_param('address', '', PARAM_RAW);
    $email2 = optional_param('email2', '', PARAM_RAW);
    $lmsdata_user_query = 'select u.id, lu.id as lu_id, lu.address, lu.address_detail, lu.email2, lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
    $lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $user->id));
    if (is_null($lmsdata_user->lu_id)) {
        $lu = new stdClass();
        $lu->userid = $user->id;
        $lu->zipcode = $zipcode;
        $lu->address = $address;
        $lu->email2 = $email2;
        $DB->insert_record('lmsdata_user', $lu);
    } else {
        $lu = new stdClass();
        $lu->id = $lmsdata_user->lu_id;
        $lu->zipcode = $zipcode;
        $lu->address = $address;
        $lu->email2 = $email2;
        $DB->update_record('lmsdata_user', $lu);
    }

    $avatar_upload = false;
    $avatar = $_FILES['avatar'];
    $avatar_path = '';
    if ($avatar['name'] != '') {
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $context->id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $avatar['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id
        );
        $fs->create_file_from_pathname($file_record, $avatar['tmp_name']);

        $avatar_path = implode('/', array($context->id, 'local_visang', 'images', $itemid, $avatar['name']));

        $avatar_upload = true;
    }

    $cv = $_POST['cv'];

    mkjobs_resume_save_fields($resumeid, $cv);

    $res = new stdClass();
    $res->id = $resumeid;
    $res->title = $title;
    if ($avatar_path != '') {
        $res->avatar = $avatar_path;
    }
    $res->draft = $action == 'save' ? 0 : 1;
    $res->published = $action == 'save' ? 1 : 0;
    $res->timemodified = time();
    $DB->update_record('vi_resumes', $res);

    // save html to file
    if ($resume->attachment_url != '') {
        $fileinfo = explode("/", $resume->attachment_url);
        $file = $fs->get_file($fileinfo[0], $fileinfo[1], $fileinfo[2], $fileinfo[3], '/', $fileinfo[4]);
        if ($file) {
            $file->delete();
        }
    }
    $itemid = file_get_unused_draft_itemid();
    $file_record = array(
        'contextid' => $context->id,
        'component' => 'local_visang',
        'filearea' => 'docs',
        'itemid' => $itemid,
        'filepath' => '/',
        'filename' => $title . '.html',
        'timecreated' => time(),
        'timemodified' => time(),
        'userid' => $USER->id
    );
    $d_url = $CFG->wwwroot . '/local/job/resumes/pop_resume_detail.php?resumeid=' . $resume->id . '&sesskey=' . sesskey() . '&download=1&lang=' . current_language();
    $fs->create_file_from_string($file_record, file_get_contents($d_url));
    $attachment_url = implode('/', array($context->id, 'local_visang', 'docs', $itemid, $title . '.html'));

    $res = new stdClass();
    $res->id = $resume->id;
    $res->attachment_url = $attachment_url;
    $DB->update_record('vi_resumes', $res);

    if ($action == 'auto-draft') {
        define('AJAX_SCRIPT', true);
        echo json_encode(array('status' => 1));
        die();
    }

    if (!$avatar_upload) redirect($VISANG->wwwroot . '/resumes/manage.php');
}

if ($action != '') {
    redirect($baseurl);
}

$user = $DB->get_record('user', array('id' => $USER->id));
$lmsdata_user_query = 'select u.id, lu.address, lu.address_detail, lu.email2,lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
$lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $user->id));

$resume_data = mkjobs_resume_get_fields($resumeid);

require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');

// =====================================================================================================
// renders
$VISANG->theme->title = $job->title;
$VISANG->theme->menu = 'resumes';
$VISANG->theme->header_edit_cv();
?>
    <style>
        .avatar-cv{
            padding: 0;
        }
    </style>
    <link href="assets/css/edit.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.5.10/cleave.min.js"></script>
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.1/lumen/bootstrap.min.css">-->
    <div class="cont">
        <div class="group">
            <form method="post" action="<?php echo $baseurl; ?>" enctype="multipart/form-data" id="frmresume">
                <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
                <input type="hidden" name="resumeid" value="<?php echo $resumeid; ?>">
                <input type="hidden" name="action" required>

                <div class="select-template">
                    <a class="btn btn-primary" href="#ex1" role="button" rel="modal:open">
                        <i class="far fa-list-alt"></i>
                        <?php echo get_string('select_template', 'local_job'); ?>
                    </a>
                </div>

                <div class="section-margin section-title">
                    <div class="resume-title">
                        <div class="title-content">
                            <div class="title-input">
                                <input type="text" id="resumetitle" name="title" required
                                       value="<?php echo $resume->title; ?>"
                                       placeholder="<?php echo get_string('Enterresumetitle', 'local_job'); ?>"/>
                                <div class="sc-gykZtl"></div>
                                <div class="sc-tVThF kEtocA"></div>
                            </div>
                        </div>
                    </div>
                    <div class="title-label">
                        <div class="label-content">
                            <div class="label-display"><?php echo get_string('resume_title', 'local_job'); ?></div>
                        </div>
                    </div>
                </div>

                <div class="section-margin personal-detail">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="field-title"><?php echo get_string('personal_details', 'local_job'); ?></h5>
                        </div>
                        <div class="col-md-6 order-md-4 avatar-field">
                            <div class="profile-img logo-img <?php echo ($resume->avatar != "" && $resume->avatar != " ") ? "$CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg' " : ""; ?>">
                                <img class="rounded-circle" id="userAvatar"
                                     src="<?php echo $resume->avatar == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg' : $CFG->wwwroot . '/pluginfile.php/' . $resume->avatar; ?>"
                                     alt=""/>
                                <label class="u-btn"><input type="file" name="avatar" id="avatar"/></label>
                            </div>
                            <div class="upload-photo">
                                <label for="avatar">
                                    <i class="fas fa-upload"></i>
                                    Upload photo
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-1">
                            <label class="label-field"><?php echo get_string('name', 'local_job'); ?></label>
                            <p><input type="text" name="firstname" value="<?php echo $user->firstname; ?>"
                                      class="input-field"/></p>
                        </div>
                        <div class="col-md-6 order-md-2">
                            <label class="label-field"><?php echo get_string('dateofbirth', 'local_job'); ?></label>
                            <p><input type="date" name="dateofbirth" value="<?php echo $user->phone2; ?>"
                                      class="input-field input-date"/></p>
                        </div>
                        <div class="col-md-6 order-md-5">
                            <label class="label-field"><?php echo get_string('e-mail', 'local_job'); ?></label>
                            <p><input type="text" name="email2" value="<?php echo $lmsdata_user->email2; ?>"
                                      class="input-field"/></p>
                        </div>
                        <div class="col-md-6 order-md-6">
                            <label class="label-field"><?php echo get_string('PhoneNumber', 'local_job'); ?></label>
                            <p><input type="text" name="phone1" value="<?php echo $user->phone1; ?>"
                                      class="input-field"/></p>
                        </div>
                        <div class="col-md-6 order-md-3">
                            <label class="label-field"><?php echo get_string('address', 'local_job'); ?></label>
                            <p><input type="text" name="zipcode" value="<?php echo $lmsdata_user->zipcode; ?>"
                                      class="input-field"
                                      placeholder="<?php echo get_string('cv:edit:address', 'local_job'); ?>" numberOnly
                                      maxlength="20" oninput="numberMaxLength(this);"/></p>
                            <p><input type="text" name="address" value="<?php echo $lmsdata_user->address; ?>"
                                      class="input-field"
                                      placeholder="<?php echo get_string('cv:edit:address2', 'local_job'); ?>"
                                      maxlength="1000" oninput="numberMaxLength(this);"/></p>
                        </div>
                    </div>
                </div>

                <div class="section-margin desired-condition">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="field-title"><?php echo get_string('Desiredworkingconditions', 'local_job'); ?></h5>
                        </div>
                        <div class="col-md-6">
                            <label class="label-field"><?php echo get_string('EmploymentForm', 'local_job'); ?></label>
                            <p>
                                <?php
                                $work_types = $DB->get_records('vi_work_types');
                                ?>
                                <select name="cv[hope_to_work][type]" id="hope_to_work"
                                        class="input-field input-select">
                                    <option value="" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->type == '' ? 'selected' : ''; ?>><?php echo get_string('EmploymentForm', 'local_job'); ?></option>
                                    <?php foreach ($work_types as $work_type) : ?>
                                        <option value="<?php echo $work_type->id; ?>" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->type == $work_type->id ? 'selected' : ''; ?>>
                                            <?php
                                            switch (current_language()) {
                                                case 'ko':
                                                    echo $work_type->name_ko;
                                                    break;
                                                case 'en':
                                                    echo $work_type->name_en;
                                                    break;
                                                case 'vi':
                                                    echo $work_type->name_vi;
                                                    break;
                                            }
                                            ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <label class="label-field"><?php echo get_string('HopeSalary', 'local_job'); ?></label>
                            <p>
                                <input type="text" name="cv[hope_to_work][salary_from]" id="salary_from"
                                       numberonly <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1' ? 'disabled' : ''; ?>
                                       value="<?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_from : ''; ?>"
                                       class="input-field format-number" placeholder="From"/>
                                <input type="text" name="cv[hope_to_work][salary_to]" id="salary_to"
                                       numberonly <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1' ? 'disabled' : ''; ?>
                                       value="<?php echo $resume_data->hope_to_work ? $resume_data->hope_to_work->salary_to : ''; ?>"
                                       class="input-field format-number" placeholder="To"/>
                                <select name="cv[hope_to_work][salary_unit]" id="salary"
                                        class="salary input-field input-select" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1' ? 'disabled' : ''; ?>>
                                    <option value="VND" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_unit == 'VND' ? 'checked' : ''; ?>>
                                        VND
                                    </option>
                                    <option value="KRW" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_unit == 'KRW' ? 'checked' : ''; ?>>
                                        KRW
                                    </option>
                                    <option value="USD" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_unit == 'USD' ? 'checked' : ''; ?>>
                                        USD
                                    </option>
                                </select>
                                <label class="custom-ck mg-l10">
                                    <input type="checkbox" id="salary_deal"
                                           name="cv[hope_to_work][salary_deal]" <?php echo $resume_data->hope_to_work && $resume_data->hope_to_work->salary_deal == '1' ? 'checked' : ''; ?>
                                           value="1"/>
                                    <span><?php echo get_string('exchangeInInterview', 'local_job'); ?></span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="section-margin section-education">
                    <div id="education">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="field-title"><?php echo get_string('Education', 'local_job'); ?><span
                                            class="btns add-button"
                                            id="btn-add"><?php echo get_string('Add', 'local_job'); ?> <i
                                                class="fas fa-plus-circle"></i></span></h5>
                            </div>
                            <div class="col-12">
                                <div class="sortable-area">
                                    <?php if ($resume_data->education && is_array($resume_data->education->edu)) : ?>
                                        <?php
                                        $idx = mt_rand();

                                        ?>

                                        <?php foreach ($resume_data->education->edu as $edu) : ?>
                                            <div class="my-box move resume edu-history" id="<?php echo $idx++; ?>">


                                                <span class="ic-mv"><?php echo get_string('move', 'local_job'); ?></span>
                                                <span class="ic-close"
                                                      onclick="removeItem(this)"><?php echo get_string('close', 'local_job'); ?></span>
                                                <div class="row">
                                                    <div class="col-md-6 col-12 order-md-3 col-add-field">
                                                        <p>
                                                            <input type="text"
                                                                   class="sortable-idx education_name input-field margin-field"
                                                                   value="<?php echo $edu->school_name; ?>"
                                                                   data-name="cv[education][edu][idx][school_name]"
                                                                   placeholder="<?php echo get_string('Schoolname', 'local_job'); ?>"/>
                                                            <input type="text"
                                                                   class="sortable-idx education_major input-field"
                                                                   value="<?php echo $edu->degree; ?>"
                                                                   data-name="cv[education][edu][idx][degree]"
                                                                   placeholder="<?php echo get_string('MajorandDegree', 'local_job'); ?>"/>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 col-6 order-md-1 col-add-field">
                                                        <input type="text" name="from_year"
                                                               class="year sortable-idx education_from_year input-field input-range margin-from"
                                                               data-id="from_year" placeholder="YYYY" maxlength="4"
                                                               value="<?php echo $edu->from_year; ?>"
                                                               data-name="cv[education][edu][idx][from_year]"
                                                               onchange="checkdate(this);"/>
                                                        <input type="text" name=""
                                                               class="month sortable-idx education_from_month input-field input-range margin-field"
                                                               data-id="from_month" placeholder="MM" maxlength="2"
                                                               value="<?php echo $edu->from_month; ?>"
                                                               data-name="cv[education][edu][idx][from_month]"
                                                               onchange="checkdate(this);"/>
                                                        <input type="text" name=""
                                                               class="year sortable-idx education_to_year input-field input-range margin-from"
                                                               data-id="to_year"
                                                               placeholder="YYYY" maxlength="4"
                                                               value="<?php echo $edu->to_year; ?>"
                                                               data-name="cv[education][edu][idx][to_year]"
                                                               onchange="checkdate(this);"/>
                                                        <input type="text" name=""
                                                               class="month sortable-idx education_to_month input-field input-range"
                                                               data-id="to_month" placeholder="MM" maxlength="2"
                                                               value="<?php echo $edu->to_month; ?>"
                                                               data-name="cv[education][edu][idx][to_month]"
                                                               onchange="checkdate(this);"/>
                                                    </div>
                                                    <div class="col-md-3 col-6 order-md-2 col-add-field">
                                                        <p>
                                                            <select class="w100 sortable-idx education_type margin-field input-field input-select"
                                                                    data-name="cv[education][edu][idx][type]">
                                                                <option value="" <?php echo $edu->type == '' ? 'selected' : ''; ?>><?php echo get_string('school', 'local_job'); ?></option>
                                                                <option value="university" <?php echo $edu->type == 'university' ? 'selected' : ''; ?>><?php echo get_string('dh', 'local_job'); ?></option>
                                                                <option value="college" <?php echo $edu->type == 'college' ? 'selected' : ''; ?>><?php echo get_string('cd', 'local_job'); ?></option>
                                                                <option value="vocational" <?php echo $edu->type == 'vocational' ? 'selected' : ''; ?>><?php echo get_string('cpn', 'local_job'); ?></option>
                                                                <option value="high_school" <?php echo $edu->type == 'high_school' ? 'selected' : ''; ?>><?php echo get_string('c3', 'local_job'); ?></option>
                                                            </select>
                                                            <label class="custom-ck">
                                                                <input type="checkbox"
                                                                       class="sortable-idx education_attend"
                                                                       data-name="cv[education][edu][idx][attend]"
                                                                       value="1" <?php echo $edu->attend == 1 ? 'checked' : ''; ?> />
                                                                <span><?php echo get_string('Attending', 'local_job'); ?></span>
                                                            </label>
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>


                                        <?php endforeach; ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <template id="education-template">
                        <div class="my-box move resume edu-history" id="_idx">
                            <span class="ic-mv"><?php echo get_string('move', 'local_job'); ?></span>
                            <span class="ic-close"
                                  onclick="removeItem(this)"><?php echo get_string('close', 'local_job'); ?></span>
                            <div class="row">
                                <div class="col-md-6 col-12 order-md-3 col-add-field">
                                    <p>
                                        <input type="text"
                                               class="sortable-idx education_name input-field margin-field"
                                               data-name="cv[education][edu][idx][school_name]"
                                               placeholder="<?php echo get_string('Schoolname', 'local_job'); ?>"/>
                                        <input type="text"
                                               class="sortable-idx education_major input-field"
                                               data-name="cv[education][edu][idx][degree]"
                                               placeholder="<?php echo get_string('MajorandDegree', 'local_job'); ?>"/>
                                    </p>
                                </div>
                                <div class="col-md-3 col-6 order-md-1 col-add-field">
                                    <input type="text" name="from_year"
                                           class="year sortable-idx education_from_year input-field input-range margin-from"
                                           data-id="from_year" placeholder="YYYY" maxlength="4"
                                           data-name="cv[education][edu][idx][from_year]"
                                           onchange="checkdate(this);"/>
                                    <input type="text" name=""
                                           class="month sortable-idx education_from_month input-field input-range margin-field"
                                           data-id="from_month" placeholder="MM" maxlength="2"
                                           data-name="cv[education][edu][idx][from_month]"
                                           onchange="checkdate(this);"/>
                                    <input type="text" name=""
                                           class="year sortable-idx education_to_year input-field input-range margin-from"
                                           data-id="to_year"
                                           placeholder="YYYY" maxlength="4"
                                           data-name="cv[education][edu][idx][to_year]"
                                           onchange="checkdate(this);"/>
                                    <input type="text" name=""
                                           class="month sortable-idx education_to_month input-field input-range"
                                           data-id="to_month" placeholder="MM" maxlength="2"
                                           data-name="cv[education][edu][idx][to_month]"
                                           onchange="checkdate(this);"/>
                                </div>
                                <div class="col-md-3 col-6 order-md-2 col-add-field">
                                    <p>
                                        <select class="w100 sortable-idx education_type margin-field input-field input-select"
                                                data-name="cv[education][edu][idx][type]">
                                            <option value=""><?php echo get_string('school', 'local_job'); ?></option>
                                            <option value="university"><?php echo get_string('dh', 'local_job'); ?></option>
                                            <option value="college"><?php echo get_string('cd', 'local_job'); ?></option>
                                            <option value="vocational"><?php echo get_string('cpn', 'local_job'); ?></option>
                                            <option value="high_school"><?php echo get_string('c3', 'local_job'); ?></option>
                                        </select>
                                        <label class="custom-ck">
                                            <input type="checkbox" class="sortable-idx education_attend"
                                                   data-name="cv[education][edu][idx][attend]"
                                                   value="1"/>
                                            <span><?php echo get_string('Attending', 'local_job'); ?></span>
                                        </label>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </template>
                </div>
                <div class="section-margin section-career">
                    <div id="experience">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="field-title"><?php echo get_string('Career', 'local_job'); ?><span
                                            class="btns add-button"
                                            id="btn-add"><?php echo get_string('Add', 'local_job'); ?> <i
                                                class="fas fa-plus-circle"></i></span></h5>
                            </div>
                            <div class="col-12">
                                <div class="sortable-area">
                                    <?php if ($resume_data->experience && is_array($resume_data->experience->exp)) : ?>
                                        <?php
                                        $idx = mt_rand();
                                        ?>
                                        <?php foreach ($resume_data->experience->exp as $exp) : ?>
                                            <div class="my-box move resume cp-history" id="<?php echo $idx++; ?>">
                                                <span class="ic-mv"><?php echo get_string('move', 'local_job'); ?></span>
                                                <span class="ic-close"
                                                      onclick="removeItem(this)"><?php echo get_string('close', 'local_job'); ?></span>
                                                <div class="row">
                                                    <div class="col-3 col-add-field">
                                                        <input type="text"
                                                               class="year sortable-idx experience_from_year input-field input-range margin-from"
                                                               data-id="from_year" placeholder="YYYY" maxlength="4"
                                                               value="<?php echo $exp->from_year; ?>"
                                                               data-name="cv[experience][exp][idx][from_year]"
                                                               onchange="checkdate(this);"/>
                                                        <input type="text"
                                                               class="month sortable-idx experience_from_month input-field input-range margin-field"
                                                               data-id="from_month" placeholder="MM" maxlength="2"
                                                               value="<?php echo $exp->from_month; ?>"
                                                               data-name="cv[experience][exp][idx][from_month]"
                                                               onchange="checkdate(this);"/>
                                                        <input type="text"
                                                               class="year sortable-idx cv__in_office_off experience_to_year input-field input-range margin-from"
                                                               data-id="to_year" placeholder="YYYY" maxlength="4"
                                                               value="<?php echo $exp->to_year; ?>"
                                                               data-name="cv[experience][exp][idx][to_year]"
                                                               onchange="checkdate(this);"/>
                                                        <input type="text"
                                                               class="month sortable-idx cv__in_office_off experience_to_month input-field input-range margin-field"
                                                               data-id="to_month" placeholder="MM" maxlength="2"
                                                               value="<?php echo $exp->to_month; ?>"
                                                               data-name="cv[experience][exp][idx][to_month]"
                                                               onchange="checkdate(this);"/>
                                                        <label class="custom-ck">
                                                            <input type="checkbox" id="cv__in_office"
                                                                   class="sortable-idx experience_attend"
                                                                   data-name="cv[experience][exp][idx][in_office]"
                                                                   value="1" <?php echo $exp->in_office == 1 ? 'checked' : ''; ?> />
                                                            <span><?php echo get_string('Office', 'local_job'); ?></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-9 col-add-field">
                                                        <p>
                                                            <input type="text"
                                                                   placeholder="<?php echo get_string('cv:edit:companyName', 'local_job'); ?>"
                                                                   class="sortable-idx experience_name input-field margin-field"
                                                                   value="<?php echo $exp->company_name; ?>"
                                                                   data-name="cv[experience][exp][idx][company_name]"/>
                                                            <span class="row">
                                                                <span class="col-8 no-padding experience_major_padding">
                                                                    <input type="text"
                                                                           class="sortable-idx experience_major input-field"
                                                                           value="<?php echo $exp->department; ?>"
                                                                           data-name="cv[experience][exp][idx][department]"
                                                                           placeholder="<?php echo get_string('Department', 'local_job'); ?>"/>
                                                                </span>
                                                                <span class="col-4 no-padding">
                                                                    <select class="sortable-idx experience_type input-field input-select"
                                                                            data-name="cv[experience][exp][idx][position]">
                                                                <option value="" <?php echo $exp->position == '' ? 'selected' : ''; ?>><?php echo get_string('resume:position:title', 'local_job'); ?></option>
                                                                <option value="divisiondirector" <?php echo $exp->position == 'divisiondirector' ? 'selected' : ''; ?>><?php echo get_string('resume:position:divisiondirector', 'local_job'); ?></option>
                                                                <option value="divisionmanager" <?php echo $exp->position == 'divisionmanager' ? 'selected' : ''; ?>><?php echo get_string('resume:position:divisionmanager', 'local_job'); ?></option>
                                                                <option value="manager" <?php echo $exp->position == 'manager' ? 'selected' : ''; ?>><?php echo get_string('resume:position:manager', 'local_job'); ?></option>
                                                                <option value="teamleader" <?php echo $exp->position == 'teamleader' ? 'selected' : ''; ?>><?php echo get_string('resume:position:teamleader', 'local_job'); ?></option>
                                                                <option value="staff" <?php echo $exp->position == 'staff' ? 'selected' : ''; ?>><?php echo get_string('resume:position:staff', 'local_job'); ?></option>
                                                            </select>
                                                                </span>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="add-bx-wrap">
                                                    <input type="button"
                                                           value="+ <?php echo get_string('Keyachievementsadded', 'local_job'); ?>"
                                                           class="btns br add-button" id="sub-btn-add"/>
                                                    <div class="sub-sortable-area" style="width: 100%">
                                                        <?php if (is_array($exp->exp_achievement)) : ?>
                                                            <?php
                                                            $subidx = mt_rand();
                                                            ?>
                                                            <?php foreach ($exp->exp_achievement as $exp_achievement) : ?>
                                                                <div class="add-bx" id="<?php echo $subidx++; ?>">
                                                                    <span class="ic-mv-sub"><?php echo get_string('move', 'local_job'); ?></span>
                                                                    <span class="ic-close"
                                                                          onclick="removeItem(this)"><?php echo get_string('close', 'local_job'); ?></span>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <input type="text"
                                                                                   placeholder="<?php echo get_string('mainachievement', 'local_job'); ?>"
                                                                                   class="w100 sub-sortable-idx sub_experience_name input-field"
                                                                                   value="<?php echo $exp_achievement->title; ?>"
                                                                                   data-name="cv[experience][exp][idx][exp_achievement][subidx][title]"/>
                                                                        </div>
                                                                        <div class="col-5">
                                                                            <input type="text"
                                                                                   class="year sub-sortable-idx sub_experience_from_year input-field sub-input-range margin-from"
                                                                                   data-id="from_year" placeholder="YYYY"
                                                                                   maxlength="4"
                                                                                   value="<?php echo $exp_achievement->from_year; ?>"
                                                                                   data-name="cv[experience][exp][idx][exp_achievement][subidx][from_year]"
                                                                                   onchange="checkdate(this);"/>
                                                                            <input type="text"
                                                                                   class="month sub-sortable-idx sub_experience_from_month input-field sub-input-range"
                                                                                   data-id="from_month" placeholder="MM"
                                                                                   maxlength="2"
                                                                                   value="<?php echo $exp_achievement->from_month; ?>"
                                                                                   data-name="cv[experience][exp][idx][exp_achievement][subidx][from_month]"
                                                                                   onchange="checkdate(this);"/>
                                                                            <input type="text"
                                                                                   class="year sub-sortable-idx sub_experience_to_year input-field sub-input-range margin-from"
                                                                                   data-id="to_year" placeholder="YYYY"
                                                                                   maxlength="4"
                                                                                   value="<?php echo $exp_achievement->to_year; ?>"
                                                                                   data-name="cv[experience][exp][idx][exp_achievement][subidx][to_year]"
                                                                                   onchange="checkdate(this);"/>
                                                                            <input type="text"
                                                                                   class="month sub-sortable-idx sub_experience_to_month input-field sub-input-range"
                                                                                   data-id="to_month" placeholder="MM"
                                                                                   maxlength="2"
                                                                                   value="<?php echo $exp_achievement->to_month; ?>"
                                                                                   data-name="cv[experience][exp][idx][exp_achievement][subidx][to_month]"
                                                                                   onchange="checkdate(this);"/>
                                                                        </div>
                                                                        <div class="col-7">
                                                                            <textarea
                                                                                    class="w100 sub-sortable-idx sub_experience_description input-field"
                                                                                    placeholder="<?php echo get_string('mainachievementdesc', 'local_job'); ?>"
                                                                                    data-name="cv[experience][exp][idx][exp_achievement][subidx][content]"><?php echo $exp_achievement->content; ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <template id="experience-template">
                        <div class="my-box move resume cp-history" id="_idx">
                            <span class="ic-mv"><?php echo get_string('move', 'local_job'); ?></span>
                            <span class="ic-close"
                                  onclick="removeItem(this)"><?php echo get_string('close', 'local_job'); ?></span>
                            <div class="row">
                                <div class="col-3 col-add-field">
                                    <input type="text"
                                           class="year sortable-idx experience_from_year input-field input-range margin-from"
                                           data-id="from_year" placeholder="YYYY" maxlength="4"
                                           data-name="cv[experience][exp][idx][from_year]"
                                           onchange="checkdate(this);"/>
                                    <input type="text"
                                           class="month sortable-idx experience_from_month input-field input-range margin-field"
                                           data-id="from_month" placeholder="MM" maxlength="2"
                                           data-name="cv[experience][exp][idx][from_month]"
                                           onchange="checkdate(this);"/>
                                    <input type="text"
                                           class="year sortable-idx cv__in_office_off experience_to_year input-field input-range margin-from"
                                           data-id="to_year" placeholder="YYYY" maxlength="4"
                                           data-name="cv[experience][exp][idx][to_year]"
                                           onchange="checkdate(this);"/>
                                    <input type="text"
                                           class="month sortable-idx cv__in_office_off experience_to_month input-field input-range margin-field"
                                           data-id="to_month" placeholder="MM" maxlength="2"
                                           data-name="cv[experience][exp][idx][to_month]"
                                           onchange="checkdate(this);"/>
                                    <label class="custom-ck">
                                        <input type="checkbox" id="cv__in_office"
                                               class="sortable-idx experience_attend"
                                               data-name="cv[experience][exp][idx][in_office]"
                                               value="1"/>
                                        <span><?php echo get_string('Office', 'local_job'); ?></span>
                                    </label>
                                </div>
                                <div class="col-9 col-add-field">
                                    <p>
                                        <input type="text"
                                               placeholder="<?php echo get_string('cv:edit:companyName', 'local_job'); ?>"
                                               class="sortable-idx experience_name input-field margin-field"
                                               data-name="cv[experience][exp][idx][company_name]"/>
                                        <span class="row">
                                                                <span class="col-8 no-padding experience_major_padding">
                                                                    <input type="text"
                                                                           class="sortable-idx experience_major input-field"
                                                                           data-name="cv[experience][exp][idx][department]"
                                                                           placeholder="<?php echo get_string('Department', 'local_job'); ?>"/>
                                                                </span>
                                                                <span class="col-4 no-padding">
                                                                    <select class="sortable-idx experience_type input-field input-select"
                                                                            data-name="cv[experience][exp][idx][position]">
                                                                <option value=""><?php echo get_string('resume:position:title', 'local_job'); ?></option>
                                        <option value="divisiondirector"><?php echo get_string('resume:position:divisiondirector', 'local_job'); ?></option>
                                        <option value="divisionmanager"><?php echo get_string('resume:position:divisionmanager', 'local_job'); ?></option>
                                        <option value="manager"><?php echo get_string('resume:position:manager', 'local_job'); ?></option>
                                        <option value="teamleader"><?php echo get_string('resume:position:teamleader', 'local_job'); ?></option>
                                        <option value="staff"><?php echo get_string('resume:position:staff', 'local_job'); ?></option>
                                                            </select>
                                                                </span>
                                                            </span>
                                    </p>
                                </div>
                            </div>
                            <div class="add-bx-wrap">
                                <input type="button"
                                       value="+ <?php echo get_string('Keyachievementsadded', 'local_job'); ?>"
                                       class="btns br add-button" id="sub-btn-add"/>
                                <div class="sub-sortable-area" style="width: 100%"></div>
                            </div>
                        </div>
                    </template>
                    <template id="sub-experience-template">
                        <div class="add-bx" id="_subidx">
                            <span class="ic-mv-sub"><?php echo get_string('move', 'local_job'); ?></span>
                            <span class="ic-close"
                                  onclick="removeItem(this)"><?php echo get_string('close', 'local_job'); ?></span>
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" placeholder="<?php echo get_string('mainachievement', 'local_job'); ?>"
                                           class="w100 sub-sortable-idx sub_experience_name input-field"
                                           data-name="cv[experience][exp][idx][exp_achievement][subidx][title]"/>
                                </div>
                                <div class="col-5">
                                    <input type="text" class="year sub-sortable-idx sub_experience_from_year input-field sub-input-range margin-from"
                                           data-id="from_year" placeholder="YYYY" maxlength="4"
                                           data-name="cv[experience][exp][idx][exp_achievement][subidx][from_year]"
                                           onchange="checkdate(this);"/>
                                    <input type="text" class="month sub-sortable-idx sub_experience_from_month input-field sub-input-range"
                                           data-id="from_month" placeholder="MM" maxlength="2"
                                           data-name="cv[experience][exp][idx][exp_achievement][subidx][from_month]"
                                           onchange="checkdate(this);"/>
                                    <input type="text" class="year sub-sortable-idx sub_experience_to_year input-field sub-input-range margin-from"
                                           data-id="to_year"
                                           placeholder="YYYY" maxlength="4"
                                           data-name="cv[experience][exp][idx][exp_achievement][subidx][to_year]"
                                           onchange="checkdate(this);"/>
                                    <input type="text" class="month sub-sortable-idx sub_experience_to_month input-field sub-input-range"
                                           data-id="to_month"
                                           placeholder="MM" maxlength="2"
                                           data-name="cv[experience][exp][idx][exp_achievement][subidx][to_month]"
                                           onchange="checkdate(this);"/>
                                </div>
                                <div class="col-7">
                                    <textarea class="w100 sub-sortable-idx sub_experience_description input-field"
                                              placeholder="<?php echo get_string('mainachievementdesc', 'local_job'); ?>"
                                              data-name="cv[experience][exp][idx][exp_achievement][subidx][content]"></textarea>
                                </div>
                            </div>

                        </div>
                    </template>
                </div>

                <div class="section-margin section-activity">
                    <div id="activity">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="field-title">
                                    <?php echo get_string('TrainingCompletionCompletion', 'local_job'); ?>
                                </h5>
                                <div>
                                    <span class="btns br f-r mg-l10 m-w100 add-button"
                                          id="btn-add-master-korean-course"><?php echo get_string('ImportCourseHistory', 'local_job'); ?></span>
                                    <span
                                            class="btns add-button"
                                            id="btn-add"><?php echo get_string('Add', 'local_job'); ?> <i
                                                class="fas fa-plus-circle"></i></span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="sortable-area">
                                    <?php if ($resume_data->activity && is_array($resume_data->activity->act)) : ?>
                                        <?php
                                        $idx = mt_rand();
                                        ?>
                                        <?php foreach ($resume_data->activity->act as $act) : ?>
                                            <?php if ($act->type == 'activity') : ?>
                                                <div class="my-box move resume cp-history" id="<?php echo $idx++; ?>">
                                                    <span class="ic-mv">move</span>
                                                    <span class="ic-close" onclick="removeItem(this)">close</span>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <p class="dt-grp">
                                                                <input type="text" class="year sortable-idx activity_from_year input-field input-range"
                                                                       data-id="from_year" placeholder="YYYY" maxlength="4"
                                                                       value="<?php echo $act->from_year; ?>"
                                                                       data-name="cv[activity][act][idx][from_year]"
                                                                       onchange="checkdate(this);"/>
                                                                <input type="text" class="month sortable-idx activity_from_month input-field input-range"
                                                                       data-id="from_month" placeholder="MM" maxlength="2"
                                                                       value="<?php echo $act->from_month; ?>"
                                                                       data-name="cv[activity][act][idx][from_month]"
                                                                       onchange="checkdate(this);"/>
                                                                <input type="text" class="year sortable-idx activity_to_year input-field input-range"
                                                                       data-id="to_year" placeholder="YYYY" maxlength="4"
                                                                       value="<?php echo $act->to_year; ?>"
                                                                       data-name="cv[activity][act][idx][to_year]"
                                                                       onchange="checkdate(this);"/>
                                                                <input type="text" class="month sortable-idx activity_to_month input-field input-range"
                                                                       data-id="to_month" placeholder="MM" maxlength="2"
                                                                       value="<?php echo $act->to_month; ?>"
                                                                       data-name="cv[activity][act][idx][to_month]"
                                                                       onchange="checkdate(this);"/>
                                                            </p>
                                                            <label class="custom-ck">
                                                                <input type="checkbox" class="sortable-idx activity_attend"
                                                                       data-name="cv[activity][act][idx][in_progress]" <?php echo $act->in_progress == 1 ? 'checked' : ''; ?>
                                                                       value="1"/>
                                                                <span><?php echo get_string('in_progress', 'local_job'); ?></span>
                                                            </label>
                                                        </div>
                                                        <p class="col-9">
                                                            <input type="text" class="sortable-idx activity_name input-field"
                                                                   value="<?php echo $act->activity_name; ?>"
                                                                   data-name="cv[activity][act][idx][activity_name]"
                                                                   placeholder="<?php echo get_string('andotheractivities', 'local_job'); ?>"/>
                                                            <textarea class="sortable-idx activity_description input-field"
                                                                      data-name="cv[activity][act][idx][activity_content]"
                                                                      placeholder="<?php echo get_string('Pleasefillinthedetails', 'local_job'); ?>"><?php echo $act->activity_content; ?></textarea>
                                                            <input type="hidden" class="sortable-idx"
                                                                   data-name="cv[activity][act][idx][type]" value="activity">
                                                        </p>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if ($act->type == 'master_korean_course') : ?>
                                                <div class="my-box move resume cp-history" id="<?php echo $idx++; ?>">
                                                    <span class="ic-mv">move</span>
                                                    <span class="ic-close" onclick="removeItem(this)">close</span>
                                                    <div>
                                                        <p>
                                                            <input type="text" class="sortable-idx"
                                                                   data-name="cv[activity][act][idx][master_korean_title]"
                                                                   value="<?php echo $act->master_korean_title; ?>"/>
                                                            <span class="t-gray">※ <?php echo get_string('progressorcompleted', 'local_job'); ?>.</span>
                                                        </p>
                                                        <div>
                                                            <table class="table">
                                                                <colgroup>
                                                                    <col width="15%"/>
                                                                    <col width="/"/>
                                                                    <col width="15%"/>
                                                                </colgroup>
                                                                <tbody id="mk-course-lists">
                                                                <?php if ($act->master_korean_course && is_array($act->master_korean_course)) : ?>
                                                                    <?php
                                                                    $cidx = 0;
                                                                    ?>
                                                                    <?php foreach ($act->master_korean_course as $mkc) : ?>
                                                                        <?php if ($mkc->courseid) : ?>
                                                                            <?php
                                                                            $course = $DB->get_record('lmsdata_class', array('id' => $mkc->courseid));
                                                                            $pro = format_lguplus_get_course_progress($course->courseid);
                                                                            switch (current_language()) {
                                                                                case 'ko':
                                                                                    $coursename = $course->title;
                                                                                    break;
                                                                                case 'en':
                                                                                    $coursename = $course->en_title;
                                                                                    break;
                                                                                case 'vi':
                                                                                    $coursename = $course->vi_title;
                                                                                    break;
                                                                            }
                                                                            $cidx++;
                                                                            ?>
                                                                            <tr>
                                                                                <td>-</td>
                                                                                <td class="text-left">
                                                                                    <input type="hidden" class="sortable-idx"
                                                                                           data-name="cv[activity][act][idx][master_korean_course][<?php echo $cidx; ?>][courseid]"
                                                                                           value="<?php echo $mkc->courseid; ?>">
                                                                                    <input type="hidden" class="sortable-idx"
                                                                                           data-name="cv[activity][act][idx][master_korean_course][<?php echo $cidx; ?>][coursedate]"
                                                                                           value="<?php echo $mkc->coursedate; ?>">
                                                                                    <p><?php echo $coursename; ?></p>
                                                                                    <p class="t-gray"><?php echo $mkc->coursedate; ?></p>
                                                                                </td>
                                                                                <td><?php echo $pro['course']->courseprogress < 100 ? '진행중' : ''; ?></td>
                                                                            </tr>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                                </tbody>
                                                            </table>
                                                            <input type="hidden" class="sortable-idx"
                                                                   data-name="cv[activity][act][idx][type]"
                                                                   value="master_korean_course">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <template id="activity-template">
                        <div class="my-box move resume cp-history" id="_idx">
                            <span class="ic-mv">move</span>
                            <span class="ic-close" onclick="removeItem(this)">close</span>
                            <div class="row">
                                <div class="col-3">
                                    <p class="dt-grp">
                                        <input type="text" class="year sortable-idx activity_from_year input-field input-range" data-id="from_year"
                                               placeholder="YYYY" maxlength="4"
                                               data-name="cv[activity][act][idx][from_year]" onchange="checkdate(this);"/>
                                        <input type="text" class="month sortable-idx activity_from_month input-field input-range"
                                               data-id="from_month" placeholder="MM" maxlength="2"
                                               data-name="cv[activity][act][idx][from_month]" onchange="checkdate(this);"/>
                                        <input type="text" class="year sortable-idx activity_to_year input-field input-range" data-id="to_year"
                                               placeholder="YYYY" maxlength="4" data-name="cv[activity][act][idx][to_year]"
                                               onchange="checkdate(this);"/>
                                        <input type="text" class="month sortable-idx activity_to_month input-field input-range" data-id="to_month"
                                               placeholder="MM" maxlength="2" data-name="cv[activity][act][idx][to_month]"
                                               onchange="checkdate(this);"/>
                                    </p>
                                    <label class="custom-ck">
                                        <input type="checkbox" class="sortable-idx activity_attend"
                                               data-name="cv[activity][act][idx][in_progress]" value="1"/>
                                        <span><?php echo get_string('in_progress', 'local_job'); ?></span>
                                    </label>
                                </div>
                                <p class="col-9">
                                    <input type="text" class="sortable-idx activity_name input-field"
                                           data-name="cv[activity][act][idx][activity_name]"
                                           placeholder="<?php echo get_string('andotheractivities', 'local_job'); ?>"/>
                                    <textarea class="sortable-idx activity_description input-field"
                                              data-name="cv[activity][act][idx][activity_content]"
                                              placeholder="<?php echo get_string('Pleasefillinthedetails', 'local_job'); ?>"></textarea>
                                    <input type="hidden" class="sortable-idx" data-name="cv[activity][act][idx][type]"
                                           value="activity">
                                </p>
                            </div>
                        </div>
                    </template>
                    <template id="master-korean-course-template">
                        <div class="my-box move resume cp-history" id="_idx">
                            <span class="ic-mv">move</span>
                            <span class="ic-close" onclick="removeItem(this)">close</span>
                            <div>
                                <p>
                                    <input type="text" class="sortable-idx"
                                           data-name="cv[activity][act][idx][master_korean_title]" value=""/>
                                    <span class="t-gray">※ <?php echo get_string('progressorcompleted', 'local_job'); ?>.</span>
                                </p>
                                <div>
                                    <table class="table">
                                        <colgroup>
                                            <col width="15%"/>
                                            <col width="/"/>
                                            <col width="15%"/>
                                        </colgroup>
                                        <tbody id="mk-course-lists">
                                        </tbody>
                                    </table>
                                    <input type="hidden" class="sortable-idx" data-name="cv[activity][act][idx][type]"
                                           value="master_korean_course">
                                </div>
                            </div>
                        </div>
                    </template>
                </div>

                <div class="section-margin section-certification">
                    <div id="certification">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="field-title"><?php echo get_string('certificate', 'local_job'); ?><span
                                            class="btns add-button"
                                            id="btn-add"><?php echo get_string('Add', 'local_job'); ?> <i
                                                class="fas fa-plus-circle"></i></span></h5>
                            </div>
                            <div class="col-12">
                                <div class="sortable-area">
                                    <?php if ($resume_data->certification && is_array($resume_data->certification->cer)) : ?>
                                        <?php
                                        $idx = mt_rand();
                                        ?>
                                        <?php foreach ($resume_data->certification->cer as $cer) : ?>
                                            <div class="my-box move resume" id="<?php echo $idx++; ?>">
                                                <span class="ic-mv">move</span>
                                                <span class="ic-close" onclick="removeItem(this)">close</span>
                                                <div class="row">
                                                    <p class="w200px width-certification col-6">
                                                        <input type="text" class="sortable-idx certification_name input-field"
                                                               placeholder="<?php echo get_string('Enteryourcredentials', 'local_job'); ?>"
                                                               value="<?php echo $cer->title; ?>"
                                                               data-name="cv[certification][cer][idx][title]"/>
                                                    </p>
                                                    <p class="w150px width-certification col-6">
                                                        <input type="text" class="sortable-idx certification_description input-field"
                                                               placeholder="<?php echo get_string('Qualificationnumber', 'local_job'); ?>"
                                                               value="<?php echo $cer->content; ?>"
                                                               data-name="cv[certification][cer][idx][content]"/>
                                                    </p>
                                                    <p class="w200px width-certification col-6">
                                                        <input type="text" class="sortable-idx certification_major input-field"
                                                               placeholder="<?php echo get_string('Issuer', 'local_job'); ?>"
                                                               value="<?php echo $cer->certificate_authority; ?>"
                                                               data-name="cv[certification][cer][idx][certificate_authority]"/>
                                                    </p>
                                                    <p class="w140px width-certification col-6">
                                                        <input type="text"
                                                               class="sortable-idx cleave-date certification_fromto_type input-field"
                                                               placeholder="<?php echo get_string('AcquisitionDateYYYY.MM.DD', 'local_job'); ?>"
                                                               value="<?php echo $cer->date; ?>"
                                                               data-name="cv[certification][cer][idx][date]"/>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <template id="certification-template">
                        <div class="my-box move resume" id="_idx">
                            <span class="ic-mv">move</span>
                            <span class="ic-close" onclick="removeItem(this)">close</span>
                            <div class="row">
                                <p class="w200px width-certification col-6">
                                    <input type="text" class="sortable-idx certification_name input-field"
                                           placeholder="<?php echo get_string('Enteryourcredentials', 'local_job'); ?>"
                                           data-name="cv[certification][cer][idx][title]"/>
                                </p>
                                <p class="w150px width-certification col-6">
                                    <input type="text" class="sortable-idx certification_description input-field"
                                           placeholder="<?php echo get_string('Qualificationnumber', 'local_job'); ?>"
                                           data-name="cv[certification][cer][idx][content]"/>
                                </p>
                                <p class="w200px width-certification col-6">
                                    <input type="text" class="sortable-idx certification_major input-field"
                                           placeholder="<?php echo get_string('Issuer', 'local_job'); ?>"
                                           data-name="cv[certification][cer][idx][certificate_authority]"/>
                                </p>
                                <p class="w140px width-certification col-6">
                                    <input type="text" class="sortable-idx cleave-date certification_fromto_type input-field"
                                           placeholder="<?php echo get_string('AcquisitionDateYYYY.MM.DD', 'local_job'); ?>"
                                           data-name="cv[certification][cer][idx][date]"/>
                                </p>
                            </div>
                        </div>
                    </template>
                </div>

                <div class="section-margin section-links">
                    <div id="links">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="field-title"><?php echo get_string('link', 'local_job'); ?><span
                                            class="btns add-button"
                                            id="btn-add"><?php echo get_string('Add', 'local_job'); ?> <i
                                                class="fas fa-plus-circle"></i></span></h5>
                            </div>
                            <div class="col-12">
                                <div class="sortable-area">
                                    <?php if ($resume_data->links && is_array($resume_data->links->link)) : ?>
                                        <?php
                                        $idx = mt_rand();
                                        ?>
                                        <?php foreach ($resume_data->links->link as $link) : ?>
                                            <div class="my-box move resume" id="<?php echo $idx++; ?>">
                                                <span class="ic-mv">move</span>
                                                <span class="ic-close" onclick="removeItem(this)">close</span>
                                                <div>
                                                    <p class="w100">
                                                        <input type="text" class="sortable-idx links_name input-field"
                                                               placeholder="<?php echo get_string('LinkNameDescription', 'local_job'); ?>"
                                                               value="<?php echo $link->title; ?>"
                                                               data-name="cv[links][link][idx][title]"/>
                                                        <input type="text" class="sortable-idx links_description input-field"
                                                               placeholder="http://" value="<?php echo $link->url; ?>"
                                                               data-name="cv[links][link][idx][url]"/>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <template id="links-template">
                        <div class="my-box move resume" id="_idx">
                            <span class="ic-mv">move</span>
                            <span class="ic-close" onclick="removeItem(this)">close</span>
                            <div>
                                <p class="w100">
                                    <input type="text" class="sortable-idx links_name input-field"
                                           placeholder="<?php echo get_string('LinkNameDescription', 'local_job'); ?>"
                                           data-name="cv[links][link][idx][title]"/>
                                    <input type="text" class="sortable-idx links_description input-field" placeholder="http://"
                                           data-name="cv[links][link][idx][url]"/>
                                </p>
                            </div>
                        </div>
                    </template>
                </div>

                <div id="attachment" style="padding: 0 15px;">
                    <h5 class="field-title"><?php echo get_string('Attachproof', 'local_job'); ?><span
                                class="btns br f-r m-w100 openPop add-button">
                            <?php echo get_string('Add', 'local_job'); ?> <i
                                    class="fas fa-plus-circle"></i></span></h5>
                    <h5>
                        <p class="f16 t-gray">※ <?php echo get_string('Youcanattachafile', 'local_job'); ?>.</p>
                    </h5>
                    <div class="tb-file-area bx">
                        <ul id="attachment-lists">
                            <?php if ($resume_data->attachment && is_array($resume_data->attachment->file)) : ?>
                                <?php foreach ($resume_data->attachment->file as $file) : ?>
                                    <li id="attachment_resumeid_<?php echo $file->resumeid; ?>">
                                        <input type="hidden" class="attachment-idx"
                                               data-name="cv[attachment][file][idx][resumeid]"
                                               value="<?php echo $file->resumeid; ?>">
                                        <input type="hidden" class="attachment-idx"
                                               data-name="cv[attachment][file][idx][resumetitle]"
                                               value="<?php echo $file->resumetitle; ?>">
                                        <input type="hidden" class="attachment-idx"
                                               data-name="cv[attachment][file][idx][attachmenturl]"
                                               value="<?php echo $file->attachmenturl; ?>">
                                        <a href="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $file->attachmenturl; ?>"><?php echo $file->resumetitle; ?></a><a
                                                onclick="$(this).parent().remove();" class="ic-close">close</a>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

                <style>
                    .my-box.interestinformation .rw > strong {
                        width: 200px !important;
                    }

                    .my-box.interestinformation .rw > p {
                        width: calc(100% - 200px) !important;
                    }
                </style>
                <div class="sub-tit">
                    <h5 class="field-title">
                        <?php echo get_string('Interestinformation', 'local_job'); ?>
                        <div onclick="editInterestInfo('<?php echo $VISANG->wwwroot . '/my/inter.php?returnLP=' . $resumeid; ?>', this)"
                             class="btns f-r add-button"><?php echo get_string('EditInterestInfo', 'local_job'); ?></div>
                    </h5>


                </div>
                <div class="my-box interestinformation" style="padding: 0 15px; margin-left: 15px; margin-right: 15px;">


                    <div class="rw">
                        <strong><?php echo get_string('my_inter:final_education', 'local_job'); ?></strong>
                        <p id="final_education">
                            <?php
                            $sql = "SELECT w.name_ko, w.name_en, w.name_vi
                                FROM {vi_user_final_educations} u
                                JOIN {vi_final_educations} w ON w.id=u.final_education_id AND u.user_id=:userid";
                            $final_educations = $DB->get_record_sql($sql, array('userid' => $USER->id));
                            switch (current_language()) {
                                case 'ko':
                                    echo $final_educations->name_ko;
                                    break;
                                case 'en':
                                    echo $final_educations->name_en;
                                    break;
                                case 'vi':
                                    echo $final_educations->name_vi;
                                    break;
                            }
                            ?>
                        </p>
                    </div>

                    <div class="rw">
                        <strong><?php echo get_string('my_inter:work_experience', 'local_job'); ?></strong>
                        <p id="work_experience">
                            <?php
                            $sql = "SELECT w.name, w.name_en, w.name_vi
                                FROM {vi_user_work_experiences} u
                                JOIN {vi_work_experiences} w ON w.id=u.work_experience_id AND u.user_id=:userid";
                            $work_experience = $DB->get_record_sql($sql, array('userid' => $USER->id));
                            switch (current_language()) {
                                case 'ko':
                                    echo $work_experience->name;
                                    break;
                                case 'en':
                                    echo $work_experience->name_en;
                                    break;
                                case 'vi':
                                    echo $work_experience->name_vi;
                                    break;
                            }
                            ?>
                        </p>
                    </div>
                    <div class="rw">
                        <strong><?php echo get_string('my_inter:skill_group', 'local_job'); ?></strong>
                        <p id="skill_group">
                            <?php
                            $groups = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skill_groups} us JOIN {vi_skill_groups} s WHERE us.skill_group_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $USER->id));
                            $skill_groups = array();
                            foreach ($groups as $group) {
                                $skill_groups[] = $group->name;
                            }
                            echo implode(", ", $skill_groups);
                            ?>
                        </p>
                    </div>
                    <div class="rw">
                        <strong><?php echo get_string('my_inter:skill', 'local_job'); ?></strong>
                        <p id="skill">
                            <?php
                            $skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_user_skills} us JOIN {vi_skills} s WHERE us.skill_id=s.id AND us.user_id=:userid ORDER BY s.name ASC", array('userid' => $USER->id));
                            $user_skills = array();
                            foreach ($skills as $skill) {
                                $user_skills[] = $skill->name;
                            }
                            echo implode(", ", $user_skills);
                            ?>
                        </p>
                    </div>
                    <div class="rw">
                        <strong><?php echo get_string('my_inter:workplace', 'local_job'); ?></strong>
                        <p id="workplace">
                            <?php
                            $districts = $DB->get_records_sql("SELECT d.id,d.name FROM {vi_user_workplaces} wp JOIN {vi_districts} d WHERE wp.district_id=d.id AND wp.user_id=:userid ORDER BY d.name ASC", array('userid' => $USER->id));
                            $workplaces = array();
                            foreach ($districts as $district) {
                                $workplaces[] = $district->name;
                            }
                            echo implode(", ", $workplaces);
                            ?>
                        </p>
                    </div>
                    <div class="rw">
                        <strong><?php echo get_string('my_inter:korean_level', 'local_job'); ?></strong>
                        <p id="korean_level">
                            <?php
                            $sql = "SELECT l.*
                                FROM {vi_user_korean_levels} u
                                JOIN {vi_korean_levels} l ON l.id=u.korean_level_id AND u.user_id=:userid";
                            $korean_level = $DB->get_record_sql($sql, array('userid' => $USER->id));
                            switch (current_language()) {
                                case 'ko':
                                    echo $korean_level->name_ko;
                                    break;
                                case 'en':
                                    echo $korean_level->name_en;
                                    break;
                                case 'vi':
                                    echo $korean_level->name_vi;
                                    break;
                            }
                            ?>
                        </p>
                    </div>
                </div>

                <div class="btns-area text-center">
                    <div class="btns h60 showpop-confirm btn-temporary"
                         data-action="draft"><?php echo get_string('Temporary', 'local_job'); ?></div>
                    <div class="btns point h60 showpop-confirm"
                         data-action="save"><?php echo get_string('Completed', 'local_job'); ?></div>

                    <?php if ($resume->attachment_url != '') : ?>
                        <input type="button" value="<?php echo get_string('Download', 'local_job'); ?>" class="btns h60 btn-download"
                               onclick="download_cv()"/>
                    <?php endif; ?>
                </div>
            </form>
            <button class="sc-mszFc ueHab">
                <div class="sc-gBIyv iVeUyN"><a>Preview & Download</a></div>
                <div class="sc-eFTCDY fuDLrM"></div>
            </button>

        </div>
        <div class="preview-area">
            <div class="preview-inner">
                <div class="preview-content">
                    <div class="preview-cv" style="height: inherit;">
                        <?php
                        include("$resume_main->url_cv");
                        ?>
                    </div>
                </div>
            </div>
            <div class="action-cv">
                <button class="dowload-pdf"
                        onclick="makePDF()"><?php echo get_string('make_pdf', 'local_job'); ?></button>
                <div class="sl-template"><a>Back</a></div>
            </div>
            <!-- Modal -->
            <div class="choose-cv">
                <!-- Modal HTML embedded directly into document -->
                <div id="ex1" class="modal modal2">
                    <div class="list-cv row">
                        <?php foreach ($resume_cv as $cv) : ?>
                            <div class="col-xl-4 col-md-6">
                                <div class="item-cv<?php echo $cv->id?> item-cv" data-id="<?php echo $cv->id?>">
                                    <a rel="modal:close"><img src="<?php echo $cv->url_image_cv?>"></a>
                                    <input type="hidden" value="<?php echo $cv->id?>" name="id-cv<?php echo $cv->id?>">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- <a href="#" rel="modal:close">Close</a> -->
            </div>
        </div>
    </div>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css"/>
    <!-- plugin -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="/theme/oklassedu/javascript/jquery.ui.touch-punch.min.js"></script>
    <!-- canvas import -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.1/jspdf.umd.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script type="text/javascript">

        // download PDF
        window.html2canvas = html2canvas;
        window.jsPDF = window.jspdf.jsPDF;

        function makePDF() {
            if ($('.scale-cv').height()) {
                var divHeight = $('#capture').height() * 0.62;
            } else {
                var divHeight = $('#capture').height();
            }
            // var divHeight = 681;
            // var divWidth = $('#capture').width();
            // var divWidth = 473;
            // var divWidth = 495;
            var divWidth = 595;

            var ratio = divHeight / divWidth;
            html2canvas(document.querySelector("#capture"), {
                allowTaint: true,
                useCORS: true,
                scale: 2,
                height: divHeight,
                width: divWidth,
            }).then(canvas => {
                var doc = new jsPDF({
                    orientation: 'portrait',
                });
                var image = canvas.toDataURL("image/png");
                var doc = new jsPDF(); // using defaults: orientation=portrait, unit=mm, size=A4
                var width = doc.internal.pageSize.getWidth();

                var height = doc.internal.pageSize.getHeight();

                height = ratio * width;

                doc.addImage(image, 'PNG', 0, 0, width, height);
                // doc.addImage(image, 'PNG', 0, 0, 450, 600);
                y = -(doc.internal.pageSize.getHeight());
                // y = 643;
                var numberPage = (height / doc.internal.pageSize.getHeight()) - 1;
                for (let i = 0; i < numberPage; i++) {
                    doc.addPage();

                    doc.addImage(image, 'PNG', 0, y, width, height);
                    y -= doc.internal.pageSize.getHeight();
                }
                doc.save();
            });

        }

        String.prototype.replaceAll = function (search, replacement) {
            var target = this;
            return target.split(search).join(replacement);
        };

        $(".item-cv").click(function () {
            var id_cv = $(this).data('id');

            var id = $('input[name=id-cv' + id_cv + ']').val();
            var resume_id=<?php echo $resumeid?>;
            $.ajax({
                type: 'POST',
                url: '/local/job/resumes/ajax_cv.php',
                data: {id: id,resume_id:resume_id},
                dataType: "html",
                success: function (data) {

                    $("div[class=preview-cv]").html(data);
                    $('.item-cv').removeClass("active-cv");
                    $('.item-cv' + id + '').addClass("active-cv");
                    loadAllData();
                },
                error: function (xhr, status, error) {
                    alert('that bai');
                    return false;
                }
            });
        });

        $('#ex1').modal('show');

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#userAvatar').attr('src', e.target.result);
                    $("#userImage").attr("src", e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatar").change(function () {
            readURL(this);
            $(".userAvatar").removeClass("d-none");
        });

        $('.sc-mszFc').click(function () {
            $('.preview-area').addClass("preview-area-resposive");
        });
        $('.sl-template').click(function () {
            $('.preview-area').removeClass("preview-area-resposive");
        });

        $('input[name=email2]').keyup(function () {
            $(function () {
                var a = $('input[name=email2]').val();


                $("p[class=email2]").html(a);
            });
        });


        function removeItem(self) {
            var id = $(self).parent().attr('id');
            $(self).parent().fadeOut("fast", function () {
                $(self).parent().remove();
                $('#preview-' + id).remove();
            });
            return false;
        }

        function sortableInit(id) {
            //box sotable event
            $("#" + id + " .sortable-area").sortable({
                handle: ".ic-mv",
                axis: "y"
            });

            $("#" + id + " #btn-add").click(function () {
                var templateHtml = $("#" + id + "-template").html();
                var idx = new Date().valueOf();
                templateHtml = templateHtml.replaceAll("_idx", idx);

                var template = $(templateHtml).appendTo("#" + id + " .sortable-area");

                if (template.find(".sub-sortable-area").length) {
                    sortableSubInit(template.find(".sub-sortable-area"), "sub-" + id + "-template", id);
                }

                $("#" + id + " .sortable-area").sortable("refresh");

                $("#" + id + " input").change(function () {
                    sortableIdxs();
                    var frmData = $('#frmresume').serializeFormJSON();
                    frmData.action = 'auto-draft';
                    $.post('<?php echo $baseurl; ?>', frmData);
                });

                $("#" + id + " .cleave-date").each(function (i, el) {
                    new Cleave(el, {
                        date: true,
                        delimiter: '.',
                        datePattern: ['Y', 'm', 'd']
                    });
                });

                autoGenerateField(id, idx);
            });

            var data = $("#" + id + " .sortable-area").sortable("toArray");
            for (var i = 0; i < data.length; i++) {
                sortableSubInit($("#" + data[i] + " .sub-sortable-area"), "sub-" + id + "-template", id);
            }

            if ($("#" + id + " .sortable-area").html().trim() == '') {
                $("#" + id + " #btn-add").click();
            }

            $("#" + id + " .cleave-date").each(function (i, el) {
                new Cleave(el, {
                    date: true,
                    delimiter: '.',
                    datePattern: ['Y', 'm', 'd']
                });
            });
        }

        function sortableSubInit(el, templateId, id) {
            //box sotable event
            el.sortable({
                handle: ".ic-mv-sub",
                axis: "y"
            });

            el.parent().find("#sub-btn-add").click(function () {
                var idx = $(this).parents('div.my-box.move.resume').attr('id');
                var templateHtml = $("#" + templateId).html();
                var subidx = new Date().valueOf();
                templateHtml = templateHtml.replaceAll("_subidx", subidx);

                var template = $(templateHtml).appendTo($(el));
                el.sortable("refresh");

                el.parent().find("input").change(function () {
                    sortableIdxs();
                    var frmData = $('#frmresume').serializeFormJSON();
                    frmData.action = 'auto-draft';
                    $.post('<?php echo $baseurl; ?>', frmData);
                });

                autoGenerateSubField(id, idx, subidx);
            });
        }

        function save(action) {
            $('input[name=action]').val(action);
            $('#frmresume').submit();
        }

        function addMasterKoreanCourse() {
            if (!$('#mk-course-lists').length) {
                var templateHtml = $("#master-korean-course-template").html();
                templateHtml = templateHtml.replaceAll("_idx", new Date().valueOf());
                var template = $(templateHtml).appendTo("#activity .sortable-area");
                $("#activity .sortable-area").sortable("refresh");
            }

            var html = '';
            var idx = 0;
            $('.mkcourse').each(function () {
                if (this.checked) {
                    html += `<tr>
                            <td>-</td>
                            <td class="text-left">
                                <input type="hidden" class="sortable-idx" data-name="cv[activity][act][idx][master_korean_course][` + idx + `][courseid]" value="` + $(this).data('courseid') + `">
                                <input type="hidden" class="sortable-idx" data-name="cv[activity][act][idx][master_korean_course][` + idx + `][coursedate]" value="` + $(this).data('coursedate') + `">
                                <p>` + $(this).data('coursename') + `</p>
                                <p class="t-gray">` + $(this).data('coursedate') + `</p>
                            </td>
                            <td>` + (parseInt($(this).data('courseprogress')) < 100 ? '진행중' : '') + `</td>
                        </tr>`;
                    idx++;
                }
            });
            $("#mk-course-lists").html(html);
            $('.pop-close').click();
        }

        function addAttachment() {
            var html = '';
            $('.attachment-file').each(function () {
                if (this.checked && $('#attachment_resumeid_' + $(this).data('resumeid')).length == 0) {
                    html += `<li id="attachment_resumeid_` + $(this).data('resumeid') + `">
                            <input type="hidden" class="attachment-idx" data-name="cv[attachment][file][idx][resumeid]" value="` + $(this).data('resumeid') + `">
                            <input type="hidden" class="attachment-idx" data-name="cv[attachment][file][idx][resumetitle]" value="` + $(this).data('resumetitle') + `">
                            <input type="hidden" class="attachment-idx" data-name="cv[attachment][file][idx][attachmenturl]" value="` + $(this).data('attachmenturl') + `">
                            <a target="_blank" href="<?php echo $CFG->wwwroot . '/pluginfile.php'; ?>/` + $(this).data('attachmenturl') + `">` + $(this).data('resumetitle') + `</a><a onclick="$(this).parent().remove();" class="ic-close">close</a>
                        </li>`;
                }
            });
            // $("#attachment-lists").html(html);
            $(html).appendTo("#attachment-lists");
            $('.pop-close').click();
        }

        function sortableIdxs() {
            $('.sortable-area').each(function () {
                var data = $(this).sortable("toArray");
                for (var i = 0; i < data.length; i++) {
                    var id = data[i];
                    $("#" + id).find('.sortable-idx').each(function () {
                        $(this).attr('name', $(this).data('name').replaceAll('[idx]', '[' + i + ']'));
                    });

                    var subData = $("#" + id + " .sub-sortable-area").sortable("toArray");
                    for (var j = 0; j < subData.length; j++) {
                        var subId = subData[j];
                        $("#" + subId).find('.sub-sortable-idx').each(function () {
                            var subName = $(this).data('name');
                            subName = subName.replaceAll('[idx]', '[' + i + ']');
                            subName = subName.replaceAll('[subidx]', '[' + j + ']');
                            $(this).attr('name', subName);
                        });
                    }
                }
            });

            $("#attachment-lists li").each(function (i, val) {
                $(this).find('.attachment-idx').each(function () {
                    $(this).attr('name', $(this).data('name').replaceAll('[idx]', '[' + i + ']'));
                });
            });
        }

        $(function () {
            sortableInit('education');
            sortableInit('experience');
            sortableInit('activity');
            sortableInit('certification');
            sortableInit('links');

            $('#btn-add-master-korean-course').click(function () {
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_master_korean_course.php' ?>", {
                    "width": "600px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {});
                return false;
            });

            //add file popup event
            $(".openPop").click(function () {
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_resume_manage.php' ?>", {
                    "width": "600px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {
                    // "param": "11"
                    "resumetitle": $('#resumetitle').val()
                });
                return false;
            });

            $(".showpop-confirm").click(function () {
                var loading = $('<img src="<?php echo $CFG->wwwroot; ?>/theme/oklassedu/pix/images/icon_loading.gif" />');
                $(this).append(loading);
                sortableIdxs();
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_confirm_edit.php' ?>", {
                    "width": "500px",
                    "height": "auto",
                    "callbackFn": function () {
                        loading.remove();
                    }
                }, {
                    "action": $(this).data('action')
                });
                return false;
            });

            // $('#avatar').change(function() {
            //     if (this.files.length) {
            //         save('draft');
            //     }


            // });

            $('#salary_deal').change(function () {
                var deal = this.checked;
                $('.salary').each(function () {
                    $(this).prop('disabled', deal);
                });
            });
        });

        $("input:text[numberOnly]").on("keyup", function () {
            $(this).val($(this).val().replace(/[^0-9]/g, ""));
        });

        function numberMaxLength(e) {
            if (e.value.length > e.maxLength) {
                e.value = e.value.slice(0, e.maxLength);
            }
        }

        $('#cv__in_office').click(function () {
            if ($("#cv__in_office").is(':checked')) {
                $(".cv__in_office_off").prop('disabled', true);
            } else {
                $(".cv__in_office_off").prop('disabled', false);
            }
        });

        function checkdate(inp) {
            var today = new Date();
            var valid = false;
            var from_year = parseInt($(inp).parent().find('[data-id=from_year]').val()) || -1;
            var from_month = parseInt($(inp).parent().find('[data-id=from_month]').val()) || -1;
            var to_year = parseInt($(inp).parent().find('[data-id=to_year]').val()) || -1;
            var to_month = parseInt($(inp).parent().find('[data-id=to_month]').val()) || -1;

            if (from_year > 0) {
                valid = from_year <= today.getFullYear();
            }

            if (from_month >= 0) {
                valid = (from_month <= 12) && (new Date(from_year, from_month - 1, today.getDate()) <= today);
            }

            if (to_year > 0) {
                // valid = (from_year <= to_year) && (to_year <= today.getFullYear());
                valid = (from_year <= to_year);
            }

            if (to_month >= 0) {
                // valid = (to_month <= 12) && (new Date(to_year, to_month - 1, today.getDate()) <= today) && (new Date(from_year, from_month - 1, today.getDate()) <= new Date(to_year, to_month - 1, today.getDate()));
                valid = (to_month <= 12) && (new Date(from_year, from_month - 1, today.getDate()) <= new Date(to_year, to_month - 1, today.getDate()));
            }

            if (!valid) {
                alert("<?php echo get_string('resume:edit:datenotvaild', 'local_job'); ?>");
                $(inp).val('');
            }
        }

        $.fn.serializeFormJSON = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

        $('#frmresume input').change(function () {
            sortableIdxs();
            var frmData = $('#frmresume').serializeFormJSON();
            frmData.action = 'auto-draft';
            $.post('<?php echo $baseurl; ?>', frmData);
        });

        $(".format-number").keyup(function (event) {
            var val = this.value;
            val = val.replace(/,/g, "");
            var count = 0;
            for (var i = val.length - 1; i > 0; i--) {
                if ((val.length - i - count) % 3 == 0) {
                    val = val.slice(0, i) + "," + val.slice(i);
                    count++;
                    i = i - count;
                }
            }
            this.value = val;
        });

        $(".format-number").keydown(function (event) {
            var key = window.event ? event.keyCode : event.which;
            if (key === 8 || key === 190 || key === 191) {
                return true;
            } else if (key > 95 && key < 105) {
                return true;
            } else if (key < 46 || key > 57) {
                return false;
            } else {
                return true;
            }
        });

        function download_cv() {
            $.get("<?php echo $baseurl; ?>&action=attachmenturl", function (data) {
                $.get(JSON.parse(data).attachment_url, function (text) {
                    var element = document.createElement('a');
                    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
                    element.setAttribute('download', "<?php echo $resume->title; ?>.html");
                    element.setAttribute('target', "_blank");
                    element.style.display = 'none';
                    document.body.appendChild(element);
                    element.click();
                    setTimeout(function () {
                        document.body.removeChild(element);
                    }, 200);
                });
            });
        }

        function editInterestInfo(url, el) {
            sortableIdxs();
            var frmData = $('#frmresume').serializeFormJSON();
            frmData.action = 'auto-draft';

            $(el).append('<img src="<?php echo $CFG->wwwroot; ?>/theme/oklassedu/pix/images/icon_loading.gif" />');
            $.post('<?php echo $baseurl; ?>', frmData).always(function () {
                window.location.href = url;
            });
        }

        // CV builder function

        // Auto load CV data to preview after load page
        $(function () {
            loadAllData();
        });

        //Load all data form to CV preview
        function loadAllData() {
            getName();
            getEmail();
            getEmployment();
            getUserInfo();
            getPhone();
            getBirthday();
            getAddress();
            getSalary();
            callExportLoad();
            getTitleCV();
            setDefaultTitle();
        }

        // Load CV data to preview after change data in form

        $('input[name=firstname]').keyup(function () {
            getName();
        });

        $('input[name=email2]').keyup(function () {
            getEmail();
        });

        $('#hope_to_work').change(function () {
            getEmployment();
        });

        $('input[name=phone1]').keyup(function () {
            getPhone();
        });

        $('input[name=dateofbirth]').change(function () {
            getBirthday();
        });

        $('input[name=address]').keyup(function () {
            getAddress();
        });

        $('#salary_from').keyup(function () {
            getSalary();
        });

        $('#salary_to').keyup(function () {
            getSalary();
        });

        $('#salary').change(function () {
            getSalary();
        });

        $('#salary_deal').change(function () {
            if (this.checked) {
                var a = '<?php echo get_string('exchangeInInterview', 'local_job'); ?>';
            } else {
                var salaryFrom = $('#salary_from').val();
                var salaryTo = $('#salary_to').val();
                var salaryCurrency = $('#salary :checked').text();
                if (salaryFrom == '') {
                    var a = salaryTo + salaryCurrency;
                }
                if (salaryTo == '') {
                    var a = salaryFrom + salaryCurrency;
                }
                if (salaryFrom == '' && salaryTo == '') {
                    var a = '';
                }
                if (salaryFrom != '' && salaryTo != '') {
                    var a = salaryFrom + salaryCurrency + ' - ' + salaryTo + salaryCurrency;
                }
            }
            $("#hopeSalary").html(a);
        });

        $("input[name=title]").keyup(function () {
            getTitleCV();
        });

        $("input[name=title]").click(function () {
            $("div.sc-gykZtl").removeClass("exited");
            $("div.sc-gykZtl").addClass("entered");
        });

        $("input[name=title]").blur(function () {
            setDefaultTitle();
            $("div.sc-gykZtl").removeClass("entered");
            $("div.sc-gykZtl").addClass("exited");
        });

        function getName() {
            var a = $('input[name=firstname]').val();
            $("#name").html(a);
        }

        function getEmail() {
            var a = $('input[name=email2]').val();
            $("#email2").html(a);
        }

        function getEmployment() {
            var selectVal = $('#hope_to_work :selected').val();
            if (selectVal == "") {
                var a = "";
            } else {
                var a = $('#hope_to_work :selected').text();
            }
            $("#employment").html(a);
        }

        function getUserInfo() {
            var image = $("#userAvatar").attr("src");
            ;
            $("#userImage").attr("src", image);

            var final_education = $('#final_education').text();
            $("#finalEducation").html(final_education);

            var work_experience = $('#work_experience').text();
            $("#experienceWork").html(work_experience);

            var skill_group = $('#skill_group').text();
            $("#skillGroups").html(skill_group);

            var skill = $('#skill').text();
            $("#skills").html(skill);

            var workplace = $('#workplace').text();
            $("#workplaces").html(workplace);

            var korean_level = $('#korean_level').text();
            $("#koreanLanguageLevel").html(korean_level);
        }

        function getPhone() {
            var a = $('input[name=phone1]').val();
            $("#phone").html(a);
        }

        function getBirthday() {
            var a = $('input[name=dateofbirth]').val();
            $("#dateOfBirth").html(a);
        }

        function getAddress() {
            var a = $('input[name=address]').val();
            $("#address").html(a);
        }

        function getSalary() {
            if ($('#salary_deal').attr('checked') == 'checked') {
                var a = '<?php echo get_string('exchangeInInterview', 'local_job'); ?>';
            } else {
                var salaryFrom = $('#salary_from').val();
                var salaryTo = $('#salary_to').val();
                var salaryCurrency = $('#salary :checked').text();
                if (salaryFrom == '') {
                    var a = salaryTo + salaryCurrency;
                }
                if (salaryTo == '') {
                    var a = salaryFrom + salaryCurrency;
                }
                if (salaryFrom == '' && salaryTo == '') {
                    var a = '';
                }
                if (salaryFrom != '' && salaryTo != '') {
                    var a = salaryFrom + salaryCurrency + ' - ' + salaryTo + salaryCurrency;
                }
            }
            $("#hopeSalary").html(a);
        }

        // Load CV data to preview after change data in form END

        // Render data to CV preview
        function renderDataPreview(id, idx) {
            var fromYear = $('#' + idx + ' .' + id + '_from_year').val();
            var fromMonth = $('#' + idx + ' .' + id + '_from_month').val();
            var toYear = $('#' + idx + ' .' + id + '_to_year').val();
            var toMonth = $('#' + idx + ' .' + id + '_to_month').val();
            var type = $('#' + idx + ' .' + id + '_type :selected').text();
            var name = $('#' + idx + ' .' + id + '_name').val();
            var major = $('#' + idx + ' .' + id + '_major').val();
            var description = $('#' + idx + ' .' + id + '_description').val();
            var fromtoType = $('#' + idx + ' .' + id + '_fromto_type').val();

            if (id == 'education') {
                $("#preview-" + idx + " #" + id + "Name").html(type + ": " + name);
                $("#preview-" + idx + " #" + id + "Major").html("Major: " + major);
            } else if (id == 'experience') {
                $("#preview-" + idx + " #" + id + "Name").html(name);
                $("#preview-" + idx + " #" + id + "Major").html(major + ": " + type);
            } else {
                $("#preview-" + idx + " #" + id + "Name").html(name);
                $("#preview-" + idx + " #" + id + "Major").html(major);
            }

            $("#preview-" + idx + " #" + id + "FromTo").html(fromMonth + "/" + fromYear + " - " + toMonth + "/" + toYear);

            $("#preview-" + idx + " #" + id + "Description").html(description);

            $("#preview-" + idx + " #" + id + "FromToType").html(fromtoType);

            checkboxStatus(id, idx);
        }

        // Check checkbox status
        function checkboxStatus(id, idx) {
            $('#' + idx + ' .' + id + '_attend').change(function () {
                var checkThis = this.checked;
                checkboxPreview(id, idx, checkThis);
            });
        }

        // Show checkbox data on CV preview
        function checkboxPreview(id, idx, checkThis) {
            if (checkThis) {
                if (id == 'education') {
                    var attend = '<?php echo get_string('Attending', 'local_job'); ?>';
                } else if (id == 'experience') {
                    var attend = '<?php echo get_string('Office', 'local_job'); ?>';
                } else {
                    var attend = '<?php echo get_string('in_progress', 'local_job'); ?>';
                }
            } else {
                var attend = '';
            }
            $("#preview-" + idx + " #" + id + "Attend").html(attend);
        }

        // Export available data from CV form to CV preview when reloading the page
        function exportDataWhenReload(id) {
            for (var i = 0; i < $('#' + id + ' div.my-box.move.resume').length; i++) {
                var idx = $('#' + id + ' div.my-box.move.resume')[i].id;
                autoGenerateField(id, idx);
                if ($('#' + idx + ' .' + id + '_attend').attr('checked') == 'checked') {
                    var checkThis = true;
                } else {
                    var checkThis = false;
                }
                checkboxPreview(id, idx, checkThis);
            }

            for (var i = 0; i < $('#' + id + ' div.add-bx').length; i++) {
                var subidx = $('#' + id + ' div.add-bx')[i].id;
                var idx = $($('#' + id + ' div.add-bx')[i]).parents('div.my-box.move.resume').attr('id');
                autoGenerateSubField(id, idx, subidx);
                // if ($('#' + idx + ' .' + id + '_attend').attr('checked')=='checked'){
                //     var checkThis = true;
                // }else{
                //     var checkThis = false;
                // }
                // checkboxPreview(id, idx, checkThis);
            }
        }

        // Call exportDataWhenReload function on page load
        function callExportLoad() {
            exportDataWhenReload('education');
            exportDataWhenReload('experience');
            exportDataWhenReload('activity');
            exportDataWhenReload('certification');
            exportDataWhenReload('links');
        }

        // Automatically generate fields in CV preview
        function autoGenerateField(id, idx) {
            var previewHtml = $("#" + id + "-field").html();
            previewHtml = previewHtml.replaceAll("_idx", 'preview-' + idx);
            $(previewHtml).appendTo("#" + id + "-preview ." + id + "-area");
            $('div.my-box.move.resume').bind('change', function () {
                var idx = this.id;
                renderDataPreview(id, idx);
            })
            checkboxStatus(id, idx);
            renderDataPreview(id, idx);
        }

        // Automatically generate sub fields in CV preview
        function autoGenerateSubField(id, idx, subidx) {
            var previewHtml = $("#sub-" + id + "-field").html();
            previewHtml = previewHtml.replaceAll("_subidx", 'preview-' + subidx);
            $(previewHtml).appendTo("#preview-" + idx + " .sub-" + id + "-area");
            $('div.my-box.move.resume').bind('change', function () {
                renderDataPreview("sub_" + id, subidx);
            })
            renderDataPreview("sub_" + id, subidx);
        }

        // CV builder function END

        // Get title of CV
        function getTitleCV() {
            if ($("input[name=title]").val() == '') {
                var a = $("input[name=title]").attr("placeholder");
                $("div.sc-tVThF.kEtocA").html(a);
            } else {
                var a = $("input[name=title]").val();
                $("div.sc-tVThF.kEtocA").html(a);
            }
        }

        // Set default title for CV
        function setDefaultTitle() {
            if ($("input[name=title]").val() == '') {
                var a = $("input[name=title]").attr("placeholder");
                $("input[name=title]").val(a);
                $("div.sc-tVThF.kEtocA").html(a);
            }
        }

    </script>

<?php
$VISANG->theme->footer_edit_cv();