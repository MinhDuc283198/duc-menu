<?php
/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

// ===================================================================================================
// handles
require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($VISANG->dirroot . '/lib/employers.php');

// add slideshows
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->addCSS($CFG->wwwroot . '/local/job/resumes/assets/css/style.css');


$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->header();
?>
    <div class="container-fluid">
        <section class="sec-intro">
            <div class="row">
                <div class="row row-title1">
                    <p class="title1"><?php echo get_string('banner-resume', 'local_job'); ?></p>
                </div>
                <div class="row row-title2">
                    <p class="title2">Master Korean, Master your jobs</p>
                </div>
            </div>
        </section>
        <section class="sec-btncv">
            <div class="row">
                <div class="col-lg-6 btn-create-upload">
<!--                    <a href="--><?php //echo new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array('sesskey' => sesskey(), 'action' => 'newresume')); ?><!--"-->
<!--                       class="btn btn-large btn-createcv btncv">--><?php //echo get_string('create-cv', 'local_job'); ?><!--</a>-->
                    <a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php'; ?>"
                       class="btn btn-large btn-createcv btncv"><?php echo get_string('create-cv', 'local_job'); ?></a>
                </div>
                <div class="col-lg-6 btn-create-upload">
                    <a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php'; ?>"
                       class="btn btn-large btn-fileupload1 btncv showpop-fileupload2"><?php echo get_string('upload-file', 'local_job'); ?>
                    </a>
<!--                    <button  class="btn btn-large btn-fileupload1 btncv showpop-fileupload">--><?php //echo get_string('upload-file', 'local_job'); ?><!--</button>-->
                </div>
            </div>
        </section>
        <section class="slide-cv">
            <div class="slider-cv multiple-items">
                <div class="slider-item"><img src="/local/job/resumes/assets/img/cv.png"></div>
                <div class="slider-item"><img src="/local/job/resumes/assets/img/cv.png"></div>
                <div class="slider-item"><img src="/local/job/resumes/assets/img/cv.png"></div>
                <div class="slider-item"><img src="/local/job/resumes/assets/img/cv.png"></div>
                <div class="slider-item"><img src="/local/job/resumes/assets/img/cv.png"></div>
                <div class="slider-item"><img src="/local/job/resumes/assets/img/cv.png"></div>
            </div>
        </section>
        <section class="sec-dowload">
            <div class="row">
                <div class="col-md-3 dowload-item1">
                    <img src="/local/job/resumes/assets/img/pdf1.png">
                </div>
                <div class="col-md-6 dowload-item2">
                    <div class="row">
                        <a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php'; ?>"
                           class="btn btnfreedowload"><img src="/local/job/resumes/assets/img/Vector.png"><?php echo get_string('free-download', 'local_job'); ?>
                        </a>
                    </div>
                    <div class="row">
                        <p class="titledowload">
                            <?php echo get_string('download-des', 'local_job'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-3 dowload-item3">
                    <img src="/local/job/resumes/assets/img/pdf2.png">
                </div>
            </div>
        </section>
        <section class="sec-steps">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 img-step">
                    <img src="/local/job/resumes/assets/img/ba01.png">
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 text-step">
                    <p class="steps-title">
                        <?php echo get_string('guide-cv-title', 'local_job'); ?>
                    </p>
                    <ul class="steps">
                        <li class="listep"><?php echo get_string('guide-cv-1', 'local_job'); ?>
                        </li>
                        <li class="listep">
                            <?php echo get_string('guide-cv-2', 'local_job'); ?>
                        </li>
                        <li class="listep">
                            <?php echo get_string('guide-cv-3', 'local_job'); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="sec-stylecv recomend-course">
            <div class="row stylecv-title">
                <p class="scv-tit"><?php echo get_string('style-cv', 'local_job'); ?></p>
            </div>
            <div id="stylecvId" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="row row1">
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                        </div>
                        <div class="row row2">
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row row1">
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                        </div>
                        <div class="row row2">
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row row1">
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                        </div>
                        <div class="row row2">
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                            <div class="col-sm-4 imgstyle">
                                <img src="/local/job/resumes/assets/img/stylecv.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-listjob">
                    <a class="carousel-control-prev style-cv" href="#stylecvId" role="button" data-slide="prev"
                       onclick="resetPagination()">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <ol class="carousel-indicators">
                        <li data-target="#stylecvId" data-slide-to="0" class="style-cv active"
                            onclick="resetPagination()"></li>
                        <li data-target="#stylecvId" data-slide-to="1" class="style-cv"
                            onclick="resetPagination()"></li>
                        <li data-target="#stylecvId" data-slide-to="2" class="style-cv"
                            onclick="resetPagination()"></li>
                    </ol>
                    <a class="carousel-control-next style-cv" href="#stylecvId" role="button" data-slide="next"
                       onclick="resetPagination()">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>
        <section class="sec-createcv">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 createcv-img ">
                    <img src="/local/job/resumes/assets/img/imgcreatecv.png">
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 create-text">
                    <div class="row cv-title">
                        <p class="createcv-title">
                            <?php echo get_string('create-cv-title', 'local_job'); ?>
                        </p>
                    </div>
                    <div class="row cv-tit">
                        <p class="createcv-tit">
                            <?php echo get_string('create-cv-des', 'local_job'); ?>
                        </p>
                    </div>
                    <div class="row cv-btncreate">
                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array('sesskey' => sesskey(), 'action' => 'newresume')); ?>"
                           class="btn btncreatecv2"><?php echo get_string('create-cv-btn', 'local_job'); ?></a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(document).ready(function () {
            $('.multiple-items').slick({
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoPlay: true,
                autoplaySpeed: 5000,
                prevArrow: '<button type="button" class="prev1"><i class="fas fa-chevron-left"></i></button>',
                nextArrow: '<button type="button" class="next1"><i class="fas fa-chevron-right"></i></button>',
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            arrows: false,
                            centerMode: false,
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 426,
                        settings: {
                            arrows: false,
                            centerMode: false,
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 376,
                        settings: {
                            arrows: false,
                            centerMode: false,
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 321,
                        settings: {
                            arrows: false,
                            centerMode: false,
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    </script>
    <script type="text/javascript">
        function paginationSlide(nameClass, numberPage) {
            if ($("li." + nameClass + ".active").attr("data-slide-to") != 0 && $("li." + nameClass + ".active").attr("data-slide-to") != (numberPage - 1)) {
                $("a." + nameClass).removeClass("arrow-hide");
                $("a." + nameClass).addClass("arrow-show");
            } else {
                if ($("li." + nameClass + ".active").attr("data-slide-to") == 0) {
                    $("a.carousel-control-prev." + nameClass).removeClass("arrow-show");
                    $("a.carousel-control-prev." + nameClass).addClass("arrow-hide");
                } else {
                    $("a.carousel-control-prev." + nameClass).removeClass("arrow-hide");
                    $("a.carousel-control-prev." + nameClass).addClass("arrow-show");
                }
                if ($("li." + nameClass + ".active").attr("data-slide-to") == (numberPage - 1)) {
                    $("a.carousel-control-next." + nameClass).removeClass("arrow-show");
                    $("a.carousel-control-next." + nameClass).addClass("arrow-hide");
                } else {
                    $("a.carousel-control-next." + nameClass).removeClass("arrow-hide");
                    $("a.carousel-control-next." + nameClass).addClass("arrow-show");
                }
            }
        }

        function resetPagination() {
            paginationSlide("style-cv", 3);
        }

        setInterval(resetPagination, 1000);

        function validateFileUpload(self) {
            if (self.files.length) {
                if (self.files[0].size > 10485760) {
                    $('#validateFileUploadModal').modal('show');
                    $('#cvfile').val('');
                    $('#inplbl').val('');
                } else {
                    $('#inplbl').val(self.files[0].name);
                }
            } else {
                $('#inplbl').val('');
            }
        }

        function validateVideoUpload(self) {
            var videourl = $('#cvvideo').val();
            var matches = videourl.match(/watch\?v=([a-zA-Z0-9\-_]+)/);
            if (matches) {
                return true;
            } else {
                alert(
                    '<?php echo get_string('OnlyYouTubevideo', 'local_job'); ?>');
            }
            return false;
        }

        function printResume(id) {
            // var w = 880;
            // var h = 570;
            var w = $(window).width() * 0.8;
            var h = $(window).height() * 0.8;
            var l = Math.floor((screen.width - w) / 2);
            var t = Math.floor((screen.height - h) / 2);
            var win = window.open('<?php echo $VISANG->wwwroot; ?>/resumes/pop_resume_detail.php?resumeid=' + id + '&sesskey=<?php echo sesskey(); ?>', '', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
        }

        $(function () {
            // menu more button event
            $(".block-bx>li .bk-ic .bk-mr-area .ic-sub").click(function () {
                $(this).parent().toggleClass("on");
                if ($(this).parent().hasClass("on")) {
                    utils.body_close($(this).parent(), ".bk-ic", "on", false);
                }
                return false;
            });

            //file upload popup
            $(".showpop-fileupload").click(function () {
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_file_upload.php'; ?>", {
                    "width": "660px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {
                    // "param": "11"
                });
                return false;
            });

            //youtube profile popup
            $(".showpop-video").click(function () {
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_youtube_profile.php' ?>", {
                    "width": "500px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {
                    "resumeid": $(this).data('resumeid')
                });
                return false;
            });

            $(".showpop-confirm").click(function () {
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_confirm.php' ?>", {
                    "width": "500px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {
                    "resumeid": $(this).data('resumeid'),
                    "action": $(this).data('action')
                });
                return false;
            });
        });
    </script>
<style>
    a.btn.btn-large.btn-fileupload1.btncv.showpop-fileupload2 {
        background: #485BC6;
        border-radius: 6px;
        font-family: 'Roboto', system-ui;
        font-style: normal;
        font-weight: bold;
        font-size: 36px;
        line-height: 32px;
        color: #ffffff;
        margin-right: 48%;
    }
</style>
<?php
require_once($VISANG->dirroot . '/lib/popup_lib.php');
$VISANG->theme->footer();
