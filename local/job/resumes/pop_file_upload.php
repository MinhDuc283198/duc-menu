<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// $baseurl = new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array());

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('Fileupload_3', 'local_job'); ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
        <input type="hidden" name="action" value="fileupload">
        <input name="cvfile" id="cvfile" type="file" accept=".doc,.docx,.pdf" required onchange="validateFileUpload(this)" class="d-none">

        <div class="pop-contents">
            <div class="addr-info">
                <div class="rw mg-bt20">
                    <p class="tit f-l"><?php echo get_string('Filetitle', 'local_job'); ?></p>
                    <input type="text" name="title" required />
                </div>
                <div class="rw mg-bt20">
                    <div class="custom-file-box">
                        <input type="text" id="inplbl" readonly style="width: calc(100% - 120px);" />
                        <label for="cvfile" class="btns gray" style="width: 120px;"><?php echo get_string('Fileselection', 'local_job'); ?></label>
                    </div>
                    <p class="t-gray f16">※ <?php echo get_string('LimitFile', 'local_job'); ?>.</p>
                </div>
                <label class="custom-ck mg-bt10"><input type="checkbox" name="iscvfile" /><span><?php echo get_string('Registerasresume', 'local_job'); ?></span></label>
                <p class="t-gray f16">※ <?php echo get_string('checkresumeregistration', 'local_job'); ?>.</p>

            </div>
        </div>
        <div class="btn-area text-center">
            <input type="button" value="<?php echo get_string('cancel', 'local_job'); ?>" class="btns" onclick="$('.pop-close').click();" />
            <input type="submit" value="<?php echo get_string('Save', 'local_job'); ?>" class="btns point" />
        </div>
    </form>
</div>
<?php
echo $OUTPUT->footer();
