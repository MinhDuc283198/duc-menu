<?php
/**
* Visang
*
* @package    Hoang.N <nvhoangag@gmail.com>
* @copyright  2019 Jinotech
*/
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
$filterresume = optional_param('filterresume', 0, PARAM_INT);
$filterfile = optional_param('filterfile', 0, PARAM_INT);
$filtervideo = optional_param('filtervideo', 0, PARAM_INT);

if ($filterresume == 0 && $filterfile == 0 && $filtervideo == 0) {
$filterresume = $filterfile = $filtervideo = 1;
}

$baseurl = new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array('filterresume' => $filterresume, 'filterfile' => $filterfile, 'filtervideo' => $filtervideo));

$action = optional_param('action', '', PARAM_RAW);

if ($action == 'fileupload') {
require_sesskey();
$cvfile = $_FILES['cvfile'];
$title = optional_param('title', '', PARAM_RAW);
$iscvfile = optional_param('iscvfile', false, PARAM_BOOL);

if ($cvfile) {
$context = context_system::instance();
$fs = get_file_storage();
$itemid = file_get_unused_draft_itemid();

$file_record = array(
'contextid' => $context->id,
'component' => 'local_visang',
'filearea' => 'docs',
'itemid' => $itemid,
'filepath' => '/',
'filename' => $cvfile['name'],
'timecreated' => time(),
'timemodified' => time(),
'userid' => $USER->id
);
$fs->create_file_from_pathname($file_record, $cvfile['tmp_name']);

$resume = new stdClass();
$resume->title = $title;
$resume->user_id = $USER->id;
$resume->type = $iscvfile ? 'cvfile' : 'file';
$resume->attachment_url = implode('/', array($context->id, 'local_visang', 'docs', $itemid, $cvfile['name']));
$resume->published = 1;
$resume->timecreated = time();
$resume->timemodified = time();
$DB->insert_record('vi_resumes', $resume);
}
}

if ($action == 'videoupload') {
require_sesskey();
$resumeid = optional_param('resumeid', 0, PARAM_INT);
$cvvideo = optional_param('cvvideo', '', PARAM_RAW);
$published = optional_param('published', 0, PARAM_INT);

// $has_cvvideo = $DB->get_record_select('vi_resumes', 'type=:type AND user_id=:userid', array('type' => 'video', 'userid' => $USER->id));
$has_cvvideo = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));

if ($has_cvvideo) {
$resume = new stdClass();
$resume->id = $has_cvvideo->id;
$resume->video_url = $cvvideo;
$resume->published = $published;
$resume->timemodified = time();
$DB->update_record('vi_resumes', $resume);
} else {
$resume = new stdClass();
$resume->title = get_string('YouTubevideoprofile', 'local_job');
$resume->user_id = $USER->id;
$resume->type = 'video';
$resume->video_url = $cvvideo;
$resume->published = $published;
$resume->timecreated = time();
$resume->timemodified = time();
$DB->insert_record('vi_resumes', $resume);
}
}

if ($action == 'setaspublish') {
require_sesskey();
$resumeid = optional_param('resumeid', 0, PARAM_INT);
$has_resume = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));
if ($has_resume) {
$resume = new stdClass();
$resume->id = $has_resume->id;
$resume->published = 1;
$resume->timemodified = time();
$DB->update_record('vi_resumes', $resume);
}
}

if ($action == 'unsetaspublish') {
require_sesskey();
$resumeid = optional_param('resumeid', 0, PARAM_INT);
$has_resume = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));
if ($has_resume) {
$resume = new stdClass();
$resume->id = $has_resume->id;
$resume->published = 0;
$resume->timemodified = time();
$DB->update_record('vi_resumes', $resume);
}
}

if ($action == 'setasdefault') {
require_sesskey();
$resumeid = optional_param('resumeid', 0, PARAM_INT);
$has_resume = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));
if ($has_resume) {
$sql = "UPDATE {vi_resumes}
SET is_default = 0
WHERE user_id=:userid";
$DB->execute($sql, array('userid' => $USER->id));

$resume = new stdClass();
$resume->id = $has_resume->id;
$resume->is_default = 1;
$resume->timemodified = time();
$DB->update_record('vi_resumes', $resume);
}
}

if ($action == 'deleteresume') {
require_sesskey();
$resumeid = optional_param('resumeid', 0, PARAM_INT);
$has_resume = $DB->get_record('vi_resumes', array('id' => $resumeid, 'user_id' => $USER->id));
if ($has_resume && $has_resume->is_default != 1) {
if ($has_resume->type == 'cvfile' || $has_resume->type == 'file') {
$context = context_system::instance();
$fs = get_file_storage();
$itemid = explode("/", $has_resume->attachment_url)[3];
$files = $fs->get_area_files($context->id, 'local_visang', 'docs', $itemid, 'timemodified', true);
foreach ($files as $f) {
$f->delete();
}
}
$DB->delete_records('vi_resumes', array('id' => $resumeid));
}
}

if ($action == 'newresume') {
require_sesskey();
$resume = new stdClass();
$resume->title = get_string('Basicresume', 'local_job');
$resume->user_id = $USER->id;
$resume->type = 'resume';
$resume->draft = 1;
$resume->is_default = 0;
$resume->timecreated = time();
$resume->timemodified = time();
$newresume_id = $DB->insert_record('vi_resumes', $resume);
redirect(new moodle_url($VISANG->wwwroot . '/resumes/edit.php', array('resumeid' => $newresume_id)));
}

if ($action != '') {
redirect($baseurl);
}

// create a default resume if not exists
if (!$DB->count_records('vi_resumes', array('user_id' => $USER->id, 'type' => 'resume'))) {
$resume = new stdClass();
$resume->title = get_string('Basicresume', 'local_job');
$resume->user_id = $USER->id;
$resume->type = 'resume';
$resume->draft = 1;
$resume->is_default = 1;
$resume->timecreated = time();
$resume->timemodified = time();
$DB->insert_record('vi_resumes', $resume);
}

$filters = array();
$params = array('userid' => $USER->id);
if ($filterresume == 1) {
$filters[] = 'type=:resume';
$params['resume'] = 'resume';
$filters[] = 'type=:cvfile';
$params['cvfile'] = 'cvfile';
}
if ($filterfile == 1) {
$filters[] = 'type=:file';
$params['file'] = 'file';
}
if ($filtervideo == 1) {
$filters[] = 'type=:video';
$params['video'] = 'video';
}

$resumes_sql = "SELECT rsm.*, cv.url_cv
                FROM {vi_resumes} rsm
                JOIN {vi_resume_cv} cv
                ON rsm.resume_cv_id = cv.id
                WHERE rsm.user_id=:userid
                ORDER BY rsm.is_default DESC,rsm.timemodified DESC";

$resumes = $DB->get_records_sql($resumes_sql, $params);

//$resumes = $DB->get_records_select('vi_resumes', 'user_id=:userid AND (' . implode(" OR ", $filters) . ')', $params, '{vi_resumes}.is_default DESC,{vi_resumes}.timemodified DESC');

$resume_list = array();
$cvfile_list = array();
$file_list = array();
$video_list = array();

foreach ($resumes as $resume) {
switch ($resume->type) {
case 'resume':
array_push($resume_list, $resume);
break;
case 'cvfile':
array_push($cvfile_list, $resume);
break;
case 'file':
array_push($file_list, $resume);
break;
case 'video':
array_push($video_list, $resume);
break;
}
}

require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');

$courses_cnt2 = local_mypage_get_mycourse('all', true); //All courses
// =====================================================================================================
// renders
$VISANG->theme->title = $job->title;
$VISANG->theme->menu = 'resumes';
$VISANG->theme->header();
?>
<style>
    .ic-view-bold {
        background: url("/theme/oklassedu/pix/images/icon_eye_bold.png") no-repeat center !important;
        background-size: auto 19px !important;
    }
</style>
<div class="cont">
    <div class="group">
        <div class="ck-tab">
            <div class="f-r">
                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/resumes/manage.php', array('sesskey' => sesskey(), 'action' => 'newresume')); ?>" class="btns"><?php echo get_string('Resumewriting', 'local_job'); ?></a>
                <a href="#" class="btns showpop-fileupload"><?php echo get_string('Fileupload_3', 'local_job'); ?></a>
            </div>
            <form action="<?php echo $baseurl; ?>" method="get">
                <label class="custom-ck"><input type="checkbox" <?php echo $filterresume == 1 ? 'checked' : ''; ?> name="filterresume" onchange="this.form.submit()" value="1" /><span><?php echo get_string('resume', 'local_job'); ?></span></label>
                <label class="custom-ck"><input type="checkbox" <?php echo $filterfile == 1 ? 'checked' : ''; ?> name="filterfile" onchange="this.form.submit()" value="1" /><span><?php echo get_string('Attachments', 'local_job'); ?></span></label>
                <label class="custom-ck"><input type="checkbox" <?php echo $filtervideo == 1 ? 'checked' : ''; ?> name="filtervideo" onchange="this.form.submit()" value="1" /><span><?php echo get_string('Videoprofile', 'local_job'); ?></span></label>
            </form>
        </div>
        <div class="info-txt">
            <p><?php echo get_string('recommendedtoa', 'local_job'); ?></p>
            <p><?php echo get_string('CanregisteraYouTube', 'local_job'); ?></p>
        </div>

        <ul class="block-bx">
            <?php foreach ($resume_list as $resume) : ?>
                <li>
                    <div class="bk-txt">
                        <p class="tit">
                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/resumes/view.php', array('resumeid' => $resume->id)); ?>"><?php echo $resume->title; ?></a>
                        </p>
                        <p class="t-gray"><?php echo mkjobs_notifi_time($resume->timemodified); ?></p>
                        <?php if ($resume->is_default) : ?>
                            <p class="t-blue02"><?php echo get_string('Basicresume', 'local_job'); ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="bk-ic">
                        <?php if ($resume->draft) : ?>
                            <span class="ic-write"><?php echo get_string('Creating', 'local_job'); ?></span>
                        <?php else : ?>
                            <span class="ic-write"><?php echo get_string('Completed', 'local_job'); ?></span>
                        <?php endif; ?>
                        <div class="bk-mr-area">
                            <a href="#" class="ic-sub"><?php echo get_string('more', 'local_job'); ?></a>
                            <ul>
                                <?php if ($resume->is_default == 0) : ?>
                                    <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="setasdefault" class="showpop-confirm"><?php echo get_string('Setyourdefaultresume', 'local_job'); ?></a></li>
                                <?php endif; ?>
                                <li><a href="<?php echo new moodle_url($VISANG->wwwroot . '/resumes/edit.php', array('resumeid' => $resume->id)); ?>"><?php echo get_string('Modify', 'local_job'); ?></a></li>
                                <li><a href="#" onclick="printResume('<?php echo $resume->id; ?>', '<?php echo $resume->url_cv; ?>')"><?php echo get_string('Downloadpdf', 'local_job'); ?></a></li>
                                <?php if ($resume->is_default == 0) : ?>
                                    <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="deleteresume" class="showpop-confirm"><?php echo get_string('delete', 'local_job'); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>

            <?php foreach ($cvfile_list as $resume) : ?>
                <li>
                    <div class="bk-txt">
                        <p class="tit"><?php echo $resume->title; ?></p>
                        <p class="t-gray"><?php echo mkjobs_notifi_time($resume->timemodified); ?></p>
                        <?php if ($resume->is_default) : ?>
                            <p class="t-blue02"><?php echo get_string('Basicresume', 'local_job'); ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="bk-ic">
                        <span class="ic-doc"><?php echo get_string('resume', 'local_job'); ?></span>
                        <div class="bk-mr-area">
                            <a href="#" class="ic-sub"><?php echo get_string('more', 'local_job'); ?></a>
                            <ul>
                                <?php if ($resume->is_default == 0) : ?>
                                    <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="setasdefault" class="showpop-confirm"><?php echo get_string('Setyourdefaultresume', 'local_job'); ?></a></li>
                                <?php endif; ?>
                                <li><a href="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url; ?>" download><?php echo get_string('Download', 'local_job'); ?></a></li>
                                <?php if ($resume->is_default == 0) : ?>
                                    <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="deleteresume" class="showpop-confirm"><?php echo get_string('delete', 'local_job'); ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>

            <?php foreach ($file_list as $resume) : ?>
                <li>
                    <div class="bk-txt">
                        <p class="tit"><?php echo $resume->title; ?></p>
                        <p class="t-gray"><?php echo mkjobs_notifi_time($resume->timemodified); ?></p>
                    </div>
                    <div class="bk-ic">
                        <span class="ic-file"><?php echo get_string('Attachments', 'local_job'); ?></span>
                        <div class="bk-mr-area">
                            <a href="#" class="ic-sub"><?php echo get_string('more', 'local_job'); ?></a>
                            <ul>
                                <li><a href="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url; ?>" download><?php echo get_string('Download', 'local_job'); ?></a></li>
                                <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="deleteresume" class="showpop-confirm"><?php echo get_string('delete', 'local_job'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>

            <?php if (count($video_list)) : ?>
                <?php foreach ($video_list as $resume) : ?>
                    <li>
                        <div class="bk-txt">
                            <p class="tit"><?php echo $resume->title; ?></p>
                            <p class="t-gray"><?php echo mkjobs_notifi_time($resume->timemodified); ?></p>
                            <?php if ($resume->is_default) : ?>
                                <p class="t-blue02"><?php echo get_string('Basicresume', 'local_job'); ?></p>
                            <?php endif; ?>
                            <a class="t-blue02" href="<?php echo $resume->video_url; ?>" target="_blank"><?php echo $resume->video_url; ?></a>
                        </div>
                        <div class="bk-ic">
                            <span class="ic-video"><?php echo get_string('Finishedappointment', 'local_job'); ?></span>
                            <?php if ($resume->published == 0) : ?>
                                <a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php?sesskey=' . sesskey() . '&action=setaspublish&resumeid=' . $resume->id; ?>" class="ic-view none"><?php echo get_string('Exposure', 'local_job'); ?></a>
                            <?php else : ?>
                                <a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php?sesskey=' . sesskey() . '&action=unsetaspublish&resumeid=' . $resume->id; ?>" class="ic-view ic-view-bold"><?php echo get_string('Exposure', 'local_job'); ?></a>
                            <?php endif; ?>
                            <div class="bk-mr-area">
                                <a href="#" class="ic-sub"><?php echo get_string('more', 'local_job'); ?></a>
                                <ul>
                                    <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" class="showpop-video"><?php echo get_string('Modify', 'local_job'); ?></a></li>
                                    <?php if ($resume->published == 0) : ?>
                                        <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="setaspublish" class="showpop-confirm"><?php echo get_string('Changetoimpression', 'local_job'); ?></a></li>
                                    <?php endif; ?>
                                    <li><a href="#" data-resumeid="<?php echo $resume->id; ?>" data-action="deleteresume" class="showpop-confirm"><?php echo get_string('delete', 'local_job'); ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            <?php else : ?>
                <?php if ($courses_cnt2) : ?>
                    <li>
                        <div class="bk-txt">
                            <p class="tit no-h"><?php echo get_string('YouTubevideoprofile', 'local_job'); ?></p>
                            <div class="text-center">
                                <a href="#" class="btns ic-plus showpop-video"><?php echo get_string('registration', 'local_job'); ?></a>
                            </div>
                            <p class="t-small">※ <?php echo get_string('Availablewhenyoutakea', 'local_job'); ?></p>
                        </div>
                        <div class="bk-ic">
                            <span class="ic-video"><?php echo get_string('Canregistration', 'local_job'); ?></span>
                        </div>
                    </li>
                <?php else : ?>
                    <li>
                        <div class="bk-txt">
                            <p class="tit no-h"><?php echo get_string('YouTubevideoprofile', 'local_job'); ?></p>
                            <div class="text-center">
                                <a class="btns ic-plus" style="pointer-events: none; opacity: .4;" disabled><?php echo get_string('registration', 'local_job'); ?></a>
                            </div>
                            <p class="t-small">※ <?php echo get_string('Availablewhenyoutakea', 'local_job'); ?></p>
                        </div>
                        <div class="bk-ic">
                            <span class="ic-video"><?php echo get_string('Notregistered', 'local_job'); ?></span>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>

<div id="validateFileUploadModal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="z-index: 99999999;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="h5 text-center"><?php echo get_string('LimitFile', 'local_job'); ?></h5>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-outline-dark border" data-dismiss="modal">
                    <?php echo get_string('close', 'form'); ?>
                </button>
            </div>
        </div>
    </div>
</div>

<style>
    .modal-backdrop {
        z-index: 9999999;
        background-color: #45454570 !important;
    }
</style>

<script type="text/javascript">
    function validateFileUpload(self) {
        if (self.files.length) {
            if (self.files[0].size > 10485760) {
                $('#validateFileUploadModal').modal('show');
                $('#cvfile').val('');
                $('#inplbl').val('');
            } else {
                $('#inplbl').val(self.files[0].name);
            }
        } else {
            $('#inplbl').val('');
        }
    }

    function validateVideoUpload(self) {
        var videourl = $('#cvvideo').val();
        var matches = videourl.match(/watch\?v=([a-zA-Z0-9\-_]+)/);
        if (matches) {
            return true;
        } else {
            alert(
                '<?php echo get_string('OnlyYouTubevideo', 'local_job'); ?>');
        }
        return false;
    }

    function printResume(id, url_cv) {
        // var w = 880;
        // var h = 570;
        var w = $(window).width() * 0.8;
        var h = $(window).height() * 0.8;
        var l = Math.floor((screen.width - w) / 2);
        var t = Math.floor((screen.height - h) / 2);
        var win = window.open('<?php echo $VISANG->wwwroot; ?>/resumes/pop_resume_detail.php?resumeid=' + id + '&urlcv=' + url_cv + '&sesskey=<?php echo sesskey(); ?>', '', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
    }

    $(function() {
        // menu more button event
        $(".block-bx>li .bk-ic .bk-mr-area .ic-sub").click(function() {
            $(this).parent().toggleClass("on");
            if ($(this).parent().hasClass("on")) {
                utils.body_close($(this).parent(), ".bk-ic", "on", false);
            }
            return false;
        });

        //file upload popup
        $(".showpop-fileupload").click(function() {
            utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_file_upload.php'; ?>", {
                "width": "660px",
                "height": "auto",
                "callbackFn": function() {}
            }, {
                // "param": "11"
            });
            return false;
        });

        //youtube profile popup
        $(".showpop-video").click(function() {
            utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_youtube_profile.php' ?>", {
                "width": "500px",
                "height": "auto",
                "callbackFn": function() {}
            }, {
                "resumeid": $(this).data('resumeid')
            });
            return false;
        });

        $(".showpop-confirm").click(function() {
            utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/resumes/pop_confirm.php' ?>", {
                "width": "500px",
                "height": "auto",
                "callbackFn": function() {}
            }, {
                "resumeid": $(this).data('resumeid'),
                "action": $(this).data('action')
            });
            return false;
        });
    });
</script>
<?php
$VISANG->theme->footer();
