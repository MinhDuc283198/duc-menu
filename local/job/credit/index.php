<?php
// Standard GPL and phpdocs
require_once(dirname(__FILE__) . '/../lib.php');
//require_once(__DIR__ . '/../../../config.php');
//require_once($CFG->libdir.'/adminlib.php');

//admin_externalpage_setup('tooldemo');
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER,$PAGE;
require_once($VISANG->dirroot . '/lib/jobs.php');
// Set up the page.
$title = get_string('pluginname', 'local_credit');
$pagetitle = $title;
$url = new moodle_url("/local/job/credit/index.php");
$context = context_system::instance();
 mkcom_require_login();
//$comid = mkcom_get_companyID($userID);
$PAGE->set_context($context);
$PAGE->set_url($url);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$output = $PAGE->get_renderer('local_credit');

echo $VISANG->theme->header_foremployer();
//echo $output->heading($pagetitle);

$renderable = new \local_credit\output\index_page();
echo $output->render($renderable);

echo $VISANG->theme->footer_foremployer();