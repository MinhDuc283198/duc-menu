<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

define('AJAX_SCRIPT', true);

require_once(dirname(__FILE__) . '/lib.php');

global $VISANG, $CFG, $PAGE;

$action = required_param('action', PARAM_RAW);

$context = context_system::instance();
// require_login();
require_sesskey();
$PAGE->set_context($context);

switch ($action) {
    case 'mkjobs_search_employers':
        include_once($VISANG->dirroot . '/lib/employers.php');
        echo json_encode(mkjobs_search_employers());
        die();
        break;
    case 'mkjobs_follow_employer':
        include_once($VISANG->dirroot . '/lib/employers.php');
        echo json_encode(mkjobs_follow_employer());
        die();
        break;
    case 'mkjobs_search_jobs':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        echo json_encode(mkjobs_search_jobs());
        die();
        break;
    case 'mkjobs_saved_job':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        echo json_encode(mkjobs_saved_job());
        die();
        break;
    case 'mkjobs_get_saved_jobs':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        require_login();
        global $USER;
        echo json_encode(mkjobs_get_saved_jobs($USER->id));
        die();
        break;
    case 'search_skills':
        include_once($VISANG->dirroot . '/lib/skills.php');
        echo json_encode(array('data' => array_values(mkjobs_search_skills())));
        die();
        break;
    case 'search_skill_groups':
        include_once($VISANG->dirroot . '/lib/skills.php');
        echo json_encode(array('data' => array_values(mkjobs_search_skill_groups())));
        die();
        break;
    case 'search_employers':
        include_once($VISANG->dirroot . '/lib/employers.php');
        echo json_encode(mkjobs_search_employers(false));
        die();
        break;
    case 'mkjobs_get_follow_employers':
        include_once($VISANG->dirroot . '/lib/employers.php');
        require_login();
        global $USER;
        echo json_encode(mkjobs_get_follow_employers($USER->id));
        die();
        break;
    case 'search_company_types':
        include_once($VISANG->dirroot . '/lib/employers.php');
        echo json_encode(array('data' => array_values(mkjobs_search_company_types())));
        die();
        break;
    case 'mkcom_resume_saved':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_resume_saved());
        die();
        break;
//        View contact details of CV
    case 'mkcom_resume_viewed':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_resume_viewed());
        die();
        break;
//        Check if CV has been viewed before
    case 'mkcom_resume_viewed_status':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_resume_viewed_status());
        die();
        break;
//        Download CV when user is an employer
    case 'mkcom_resume_download':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_resume_download());
        die();
        break;
//        Check if CV has been downloaded before
    case 'mkcom_resume_download_status':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_resume_download_status());
        die();
        break;
    case 'mkcom_update_resume_status':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_update_resume_status());
        die();
        break;
    case 'mkcom_update_multi_resume_status':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_update_multi_resume_status());
        die();
        break;
    case 'mkcom_job_noti_manager':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_job_noti_manager());
        die();
        break;
    case 'mkcom_job_post_noti_manager':
        include_once($VISANG->dirroot . '/employer/lib/com_function.php');
        echo json_encode(mkcom_job_post_noti_manager());
        die();
        break;
    default:
        throw new moodle_exception('invalidarguments');
        break;
}
