<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/classes/mkjobs_theme.php');


if (visang_sharing_user() == true) {
    echo "<script>
            location.href = '/local/information/information_sharing.php';
        </script>";
    die();
}

// $context = context_system::instance();
// $PAGE->set_context($context);

$VISANG = new stdClass();
$VISANG->wwwroot = $CFG->wwwroot . '/local/job';
//$VISANG->elearning_wwwroot = $CFG->job_wwwroot;
$VISANG->elearning_wwwroot = $CFG->wwwroot;
$VISANG->dirroot = $CFG->dirroot . '/local/job';
$VISANG->theme = new \local_visang\mkjobs_theme();

function mkjobs_require_login()
{
    global $CFG, $PAGE, $SESSION, $VISANG;
    if (!isloggedin()) {
        $SESSION->wantsurl = $PAGE->url;
        redirect($VISANG->wwwroot . '/auth/login.php');
    }
}

function visang_settings()
{
    global $DB;
    return (object)$DB->get_records_select_menu('vi_settings', '', array(), '', 'name, value');
}

function visang_sharing_user()
{
    global $DB, $USER;
    $query = "select lu.* from {user} u JOIN {lmsdata_user} lu ON u.id = lu.userid AND lu.userid=:userid WHERE ( lu.statuscode is null or lu.statuscode = 0 or lu.statuscode = '') AND lu.joinstatus = :joinstatus";
    $user = $DB->get_record_sql($query, array('userid' => $USER->id, 'joinstatus' => 'e'));
    //if($USER->id == 33){
    if (((strpos($_SERVER['SCRIPT_NAME'], '/local/job/') !== false) || $_SERVER['HTTP_HOST'] == 'job.masterkorean.vn')) {
        if (!empty($user)) {
            if (strpos($_SERVER['REQUEST_URI'], "information_sharing") === false) {
                return true;
            }
        }
    }
    //}
    return false;
}


function mkjobs_add_pagination($s, $f, $jobs_data)
{
    global $VISANG;
    for ($i = $s; $i < $f; $i++) {
        if ($jobs_data['page'] == $i) {
            echo '<li class="search-list_paginationitem active"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $i)) . '"><span>' . $i . '</span></a></li>';
        } else {
            echo '<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $i)) . '"><span>' . $i . '</span></a></li>';
        }
    }
}

function mkjobs_first_pagination($jobs_data)
{
    global $VISANG;
    echo '<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => 1)) . '"><span>' . 1 . '</span></a></li>
    <li class="search-list_paginationitem"><span>...</span></li>';
}

function mkjobs_last_pagination($total_page, $jobs_data)
{
    global $VISANG;
    echo '<li class="search-list_paginationitem"><span>...</span></li>
<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $total_page)) . '"><span>' . $total_page . '</span></a></li>';
}

function mkjobs_start_pagination($total_page, $current_page, $jobs_data)
{
    $page_step = 2;
    if ($total_page < $page_step * 2 + 4) {
        mkjobs_add_pagination(1, $total_page + 1, $jobs_data);
    } elseif ($current_page < $page_step * 2 + 1) {
        mkjobs_add_pagination(1, $page_step * 2 + 2, $jobs_data);
        mkjobs_last_pagination($total_page, $jobs_data);
    } elseif ($current_page > $total_page - $page_step * 2) {
        mkjobs_first_pagination($jobs_data);
        mkjobs_add_pagination($total_page - $page_step * 2, $total_page + 1, $jobs_data);
    } else {
        mkjobs_first_pagination($jobs_data);
        mkjobs_add_pagination($current_page - $page_step, $current_page + $page_step + 1, $jobs_data);
        mkjobs_last_pagination($total_page, $jobs_data);
    }
}
//=====search company pagination======
function mkjobs_company_add_pagination($s, $f, $jobs_data)
{
    global $VISANG;
    for ($i = $s; $i < $f; $i++) {
        if ($jobs_data['company_page'] == $i) {
            echo '<li class="search-list_paginationitem active"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $i)) . '"><span>' . $i . '</span></a></li>';
        } else {
            echo '<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'company_page' => $i)) . '"><span>' . $i . '</span></a></li>';
        }
    }
}

function mkjobs_company_first_pagination($jobs_data)
{
    global $VISANG;
    echo '<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'company_page' => 1)) . '"><span>' . 1 . '</span></a></li>
    <li class="search-list_paginationitem"><span>...</span></li>';
}

function mkjobs_company_last_pagination($total_page, $jobs_data)
{
    global $VISANG;
    echo '<li class="search-list_paginationitem"><span>...</span></li>
<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'company_page' => $total_page)) . '"><span>' . $total_page . '</span></a></li>';
}

function mkjobs_company_start_pagination($total_page, $current_page, $jobs_data)
{
    $page_step = 2;
    if ($total_page < $page_step * 2 + 4) {
        mkjobs_company_add_pagination(1, $total_page + 1, $jobs_data);
    } elseif ($current_page < $page_step * 2 + 1) {
        mkjobs_company_add_pagination(1, $page_step * 2 + 2, $jobs_data);
        mkjobs_company_last_pagination($total_page, $jobs_data);
    } elseif ($current_page > $total_page - $page_step * 2) {
        mkjobs_company_first_pagination($jobs_data);
        mkjobs_company_add_pagination($total_page - $page_step * 2, $total_page + 1, $jobs_data);
    } else {
        mkjobs_company_first_pagination($jobs_data);
        mkjobs_company_add_pagination($current_page - $page_step, $current_page + $page_step + 1, $jobs_data);
        mkjobs_company_last_pagination($total_page, $jobs_data);
    }
}

//=====index page=====
function mkjobs_index_add_pagination($s, $f, $jobs_data)
{
    global $VISANG;
    for ($i = $s; $i < $f; $i++) {
        if ($jobs_data['page'] == $i) {
            echo '<li class="search-list_paginationitem active"><a href="' . new moodle_url($VISANG->wwwroot . '/jobs/index.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $i)) . '"><span>' . $i . '</span></a></li>';
        } else {
            echo '<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/jobs/index.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $i)) . '"><span>' . $i . '</span></a></li>';
        }
    }
}

function mkjobs_index_first_pagination($jobs_data)
{
    global $VISANG;
    echo '<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/jobs/index.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => 1)) . '"><span>' . 1 . '</span></a></li>
    <li class="search-list_paginationitem"><span>...</span></li>';
}

function mkjobs_index_last_pagination($total_page, $jobs_data)
{
    global $VISANG;
    echo '<li class="search-list_paginationitem"><span>...</span></li>
<li class="search-list_paginationitem"><a href="' . new moodle_url($VISANG->wwwroot . '/jobs/index.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $total_page)) . '"><span>' . $total_page . '</span></a></li>';
}

function mkjobs_index_start_pagination($total_page, $current_page, $jobs_data)
{
    $page_step = 2;
    if ($total_page < $page_step * 2 + 4) {
        mkjobs_index_add_pagination(1, $total_page + 1, $jobs_data);
    } elseif ($current_page < $page_step * 2 + 1) {
        mkjobs_index_add_pagination(1, $page_step * 2 + 2, $jobs_data);
        mkjobs_index_last_pagination($total_page, $jobs_data);
    } elseif ($current_page > $total_page - $page_step * 2) {
        mkjobs_index_first_pagination($jobs_data);
        mkjobs_index_add_pagination($total_page - $page_step * 2, $total_page + 1, $jobs_data);
    } else {
        mkjobs_index_first_pagination($jobs_data);
        mkjobs_index_add_pagination($current_page - $page_step, $current_page + $page_step + 1, $jobs_data);
        mkjobs_index_last_pagination($total_page, $jobs_data);
    }
}
