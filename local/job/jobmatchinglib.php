<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Created by kbjung112@gmail.com on 2021-07-01
 * Github : https://github.com/iland112
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Get current user's korean level id (id of m_vi_korean_levels table)
 *
 * @param int $userid optional userid
 * @return false|int|mixed
 * @throws dml_exception
 */
function get_user_korean_levels(int $userid = 0) {
    global $DB;

    $kl = $DB->get_field(
        'vi_user_korean_levels',
        'korean_level_id',
        ['user_id' => $userid]);
    if (!$kl) {
        $kl = 0;
    }
    return $kl;
}

/**
 * Get current user's final education
 *
 * @param int $userid optional userid
 * @return false|int|mixed final_education_id (id of m_vi_final_educations table)
 * @throws dml_exception
 */
function get_user_final_educations(int $userid = 0) {
    global $DB;

    $final_edu = $DB->get_field(
        'vi_user_final_educations',
        'final_education_id',
        ['user_id' => $userid]);
    if (!$final_edu) {
        $final_edu = 0;
    }
    return $final_edu;
}

/**
 * Get current user work experience id (id of m_vi_work_experiences table)
 *
 * @param int $userid optional userid
 * @return false|int|mixed
 * @throws dml_exception
 */
function get_user_work_experiences(int $userid = 0) {
    global $DB;

    $work_exp = $DB->get_field(
        'vi_user_work_experiences',
        'work_experience_id',
        ['user_id' => $userid]);
    if (!$work_exp) {
        $work_exp = 0;
    }
    return $work_exp;
}

/**
 * Get current user's skill ids ( id of m_vi_skills table)
 *
 * @param int $userid optional userid
 * @return array skill ids as integer array or empty array if it does not have ids of skill
 * @throws dml_exception
 */
function get_user_skills(int $userid = 0) {
    global $DB;

    $skills = $DB->get_fieldset_select(
        'vi_user_skills',
        'skill_id',
        'user_id = :user_id',
        ['user_id' => $userid]);
    if (!$skills) {
        $skills = array();
    }
    print_r(var_dump($skills));
    return $skills;
}

/**
 * Get current user's skill group ids ( id of m_vi_skill_groups table)
 *
 * @param int $userid optional userid
 * @return array skill group ids as integer array or empty array if it does not have ids of skill
 * @throws dml_exception
 */
function get_user_skill_groups(int $userid = 0) {
    global $DB;

    $skills = $DB->get_fieldset_select(
        'vi_user_skill_groups',
        'skill_group_id',
        'user_id = :user_id',
        ['user_id' => $userid]);
    if (!$skills) {
        $skills = array();
    }
    print_r(var_dump($skills));
    return $skills;
}

/**
 * Get current user skill group ids (id of m_vi_skill_groups)
 *
 * @param int $userid optional userid
 * @return array integer array or empty array if does not have matched skill group id of current user
 * @throws dml_exception
 */
function get_vi_user_skill_groups(int $userid = 0) {
    global $DB;

    $skillgroup = $DB->get_fieldset_select(
        'vi_user_skill_groups',
        'skill_group_id',
        'user_id = :user_id',
        ['user_id' => $userid]);
    if (!$skillgroup) {
        $skills = array();
    }
    print_r(var_dump($skillgroup));
    return $skillgroup;
}

/**
 * Get current user's interested work places (district ids)
 * id of m_vi_districts table
 *
 * @param int $userid optional userid
 * @return array district ids (integer array)
 * @throws dml_exception
 */
function get_user_workplaces(int $userid = 0) {
    global $DB;

    $workplaces = $DB->get_fieldset_select(
        'vi_user_workplaces',
        'district_id',
        'user_id = :user_id',
        ['user_id' => $userid]);
    if (!$workplaces) {
        $workplaces = array();
    }
    print_r(var_dump($workplaces));
    return $workplaces;
}

/**
 * Get current user's interested company type ids (id of m_vi_company_types)
 *
 * @param int $userid optional userid
 * @return array integer array of company type id or empty array if does not have matched company id
 * @throws dml_exception
 */
function get_user_company_types(int $userid = 0) {
    global $DB;

    $comptypes = $DB->get_fieldset_select(
        'vi_user_company_types',
        'company_type_id',
        'user_id = :user_id',
        ['user_id' => $userid]);
    if (!$comptypes) {
        $comptypes = array();
    }
    print_r(var_dump($comptypes));
    return $comptypes;
}

/**
 * Get recommended job ids (id of m_vi_jobs table)
 *
 * @param int $userid
 * @return array
 * @throws dml_exception
 * @throws moodle_exception
 */
function get_recommended_jobs(int $userid = 0) {
    global $DB;

    $jobids = array();

    if ($userid == 0) {
        $rs = $DB->get_recordset_sql(
            "SELECT id FROM {vi_jobs} WHERE deleted = 0 ORDER BY timecreated DESC"
        );
        if (!$rs->valid()) {
            print_error("Can't find records");
            $rs->close();
            die();
        }
        foreach ($rs as $record) {
            $jobids[] = $record->id;
        }
        $rs->close();
        return $jobids;
    } else {
        // Get korean level, final education, work experience
        $koreanlevel = get_user_korean_levels($userid);
        $finaleducation = get_user_final_educations($userid);
        $workexperience = get_user_work_experiences($userid);

//        $qyeryparams = array(
//            'korean_level_id' => $koreanlevel,
//            'final_education_id' => $finaleducation,
//            'work_experiance_id' => $workexperience);

        // Get interested company types (company type ids)
        $companytypes = get_user_company_types($userid);
        if (!empty($companytypes)) {
            list($insql_comp_types, $in_comp_types_params) = $DB->get_in_or_equal($companytypes);
        }
        // Get interested work places(district ids)
        $workplaces = get_user_workplaces($userid);
        if (!empty($workplaces)) {
            list($insql_wps, $in_wps_params) = $DB->get_in_or_equal($companytypes);
        }
        // Get user skills (skill ids)
        $skills = get_user_skills($userid);
        if (!empty($skills)) {
            list($insql_skills, $in_skills_params) = $DB->get_in_or_equal($skills);
        }
        // Get user skill groups(skill group ids
        $skillgroups = get_user_skill_groups($userid);
        if (!empty($skillgroups)) {
            list($insql_sgs, $in_sgs_params) = $DB->get_in_or_equal($skillgroups);
        }

        $sql = "SELECT DISTINCT j.id
                FROM {vi_jobs} j
//                LEFT JOIN {vi_job_skills} js ON j.id = js.job_id
//                LEFT JOIN {vi_skills} s ON js.skill_id = s.id  
//                LEFT JOIN {vi_job_locations} jl ON j.id = jl.job_id
//                LEFT JOIN {vi_locations} l on jl.id = j.id
//                LEFT JOIN {vi_districts} d on l.district_id = d.id  
//                LEFT JOIN {vi_job_work_types} jwt ON j.id = jwt.job_id
//                LEFT JOIN {vi_work_types} wt ON jwt.work_type_id = wt.id  
//                LEFT JOIN {vi_employers} e ON j.employer_id = e.id     
//                LEFT JOIN {vi_job_salary_categories} jsc ON j.job_salary_category_id = jsc.id
                WHERE j.deleted = 0 ";

        if (is_numeric($koreanlevel)) {
            $sql .= " AND korean_level_id = {$koreanlevel}";
        }

        if (is_numeric($finaleducation)) {
            $sql .= " AND final_education_id = {$finaleducation}";
        }

        if (is_numeric($workexperience)) {
            $sql .= " AND work_experience_id = {$workexperience}";
        }

        if (!empty($skillgroups)) {
            $sql .= " AND skill_group_id IN (";
            foreach ($skillgroups as $key => $value) {
                $sql .= "{$value},";
            }
            $sql = substr($sql, 0, -1);
            $sql .= ")";
        }
        echo $sql;

        $rs = $DB->get_recordset_sql($sql);
        if (!$rs->valid()) {
//            print_error("Can't find records");
            $rs->close();
            die();
        }
        foreach ($rs as $record) {
            $jobids[] = $record->id;
        }
        $rs->close();
        return $jobids;
    }
}

function get_recommended_courses(int $userid = 0) {
    global $DB;

    $koreanlevel = get_user_korean_levels($userid);
    $courseids = array();

    switch ($koreanlevel) {
        case 1:
            break;
        case 2:
            $courseids = array(104, 125, 124, 87, 88, 117, 123);
            break;
        case 3:
            $courseids = array(89, 90, 116, 115, 91, 93, 92);
            break;
        case 4:
            $courseids = array(116, 115, 91);
            break;
        default:
            break;
    }
//    echo var_dump($courseids);
    return $courseids;
}
