<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$jobid = required_param('jobid', PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$job = $DB->get_record('vi_jobs', array('id' => $jobid));
$thistime = time();
if ($job == null || $job->published == 0 || !($job->startdate < $thistime && $thistime < $job->enddate)) {
    throw new moodle_exception('notfound');
}

$job = mkjobs_get_job_detail($job);

$resumes = $DB->get_records_select('vi_resumes', 'user_id=:userid AND published=:published AND (type=:resume OR type=:cvfile)', array('userid' => $USER->id, 'published' => 1, 'resume' => 'resume', 'cvfile' => 'cvfile'), '{vi_resumes}.is_default DESC, {vi_resumes}.timemodified DESC');
$video_resume = $DB->get_record_select('vi_resumes', 'user_id=:userid AND type=:type', array('userid' => $USER->id, 'type' => 'video'));

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('job_apply:pop_title', 'local_job'); ?>
        <a href="#" class="pop-close">닫기</a>
    </div>
    <?php if ($action == 'success') : ?>
        <div class="pop-contents">
            <div class="addr-info">
                <ul class="cp-info">
                    <li>
                        <div class="cp-logo">
                            <img src="<?php echo $job->company_logo; ?>" alt="visang" />
                        </div>
                        <strong><?php echo $job->company_name; ?></strong>
                    </li>
                    <li><?php echo $job->title; ?></li>
                </ul>

                <div class="alert alert-success my-4" role="alert">
                    <?php echo get_string('job_apply:pop_send_success', 'local_job'); ?>
                </div>
            </div>
        </div>
    <?php else : ?>
        <form action="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $jobid ?>" method="post">
            <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
            <input type="hidden" name="jobid" value="<?php echo $jobid; ?>">
            <input type="hidden" name="action" value="recruitsubmit">

            <div class="pop-contents">
                <div class="addr-info">
                    <ul class="cp-info">
                        <li>
                            <div class="cp-logo">
                                <img src="<?php echo $job->company_logo; ?>" alt="visang" />
                            </div>
                            <strong><?php echo $job->company_name; ?></strong>
                        </li>
                        <li><?php echo $job->title; ?></li>
                    </ul>

                    <div class="rw mg-bt20">
                        <p class="tit f-l" style="width: 35%;"><?php echo get_string('job_apply:pop_select', 'local_job'); ?></p>
                        <select id="resumeid" name="resumeid" class="w100" style="width: 65%">
                            <option value="">---</option>
                            <?php foreach ($resumes as $resume) : ?>
                                <option value="<?php echo $resume->id; ?>"><?php echo $resume->title; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php if ($video_resume) : ?>
                        <label class="custom-ck mg-bt10"><input type="checkbox" id="video_resume" name="video_resume" value="<?php echo $video_resume->id; ?>" /><span><?php echo get_string('job_apply:pop_youtube_checkbox', 'local_job'); ?></span></label>
                        <p class="f16 t-gray">※ <?php echo get_string('job_apply:pop_desc', 'local_job'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="btn-area text-center">
                <input type="submit" value="<?php echo get_string('job_apply:pop_submit', 'local_job'); ?>" <?php echo count($resumes) ? '' : 'disabled style="opacity: .3;"'; ?> class="btns point w100" />
            </div>
        </form>
    <?php endif; ?>
</div>
<?php
echo $OUTPUT->footer();
