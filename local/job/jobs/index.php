<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');

// =====================================================================================================
// handles
// $skillgroupid = optional_param('skillgroupid', 0, PARAM_INT);
$skillid = optional_param('skillid', 0, PARAM_INT);
$job_sortorder = optional_param('job_sortorder', '', PARAM_RAW);

$baseurl = new moodle_url($VISANG->wwwroot . '/jobs/index.php');

// $skillgroup = null;
// if ($skillgroupid != 0) {
//     $skillgroup = $DB->get_record('vi_skill_groups', array('id' => $skillgroupid));
//     if ($skillgroup) {
//         $baseurl = new moodle_url($baseurl, array('skillgroupid' => $skillgroupid));
//     }
// }
$skill = null;
if ($skillid != 0) {
    $skill = $DB->get_record('vi_skills', array('id' => $skillid));
    if ($skill) {
        $baseurl = new moodle_url($baseurl, array('skillid' => $skillid));
    }
}

if (!in_array($job_sortorder, array('register', 'deadline'))) {
    $job_sortorder = '';
}


$jobs_data = mkjobs_search_jobs();
$jobs = $jobs_data['data'];
$jobs_count = $jobs_data['total'];

// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'jobs';
$VISANG->theme->header();
?>
    <style>
        ul.category-list > li:not(.bt):hover > a,
        ul.category-list:not(.hover) > li.selected > a {
            color: #485cc7;
            background: url("/theme/oklassedu/pix/images/icon_arrow_r.png") no-repeat right center;
        }

        ul.category-list > li > a {
            font-size: 17px;
            font-weight: 500;
            color: #555;
            border-top: 1px solid #eee;
            padding: 13px 16px 13px 5px;
            display: block;
        }

        ul.category-list {
            grid-gap: 0 !important;
            gap: 0 !important;
            grid-column-gap: 1rem !important;
            column-gap: 1rem !important;
            display: grid !important;
        }

        ul.category-list > li > a {
            border-top: 0px !important;
            border-bottom: 1px solid #eee !important;
        }

        @media (max-width: 576px) {
            ul.category-list {
                /* -webkit-columns: 2;
                -moz-columns: 2;
                columns: 2; */
                grid-template-columns: repeat(2, minmax(0, 1fr)) !important;
                top: 30px;
            }

            ul.category-list > li > a {
                font-size: 0.8rem !important;
                padding: 15px !important;
                padding: 8px 10px 8px 0px !important;
            }
        }

        @media (min-width: 576px) {
            ul.category-list {
                /* -webkit-columns: 3;
                -moz-columns: 3;
                columns: 3; */
                grid-template-columns: repeat(3, minmax(0, 1fr)) !important;
                top: 30px;
            }
        }

        @media (min-width: 768px) {
            ul.category-list {
                /* -webkit-columns: 4;
                -moz-columns: 4;
                columns: 4; */
                grid-template-columns: repeat(4, minmax(0, 1fr)) !important;
                top: 40px;
            }
        }

        @media (min-width: 992px) {
            ul.category-list {
                /* -webkit-columns: 5;
                -moz-columns: 5;
                columns: 5; */
                grid-template-columns: repeat(5, minmax(0, 1fr)) !important;
                top: 40px;
            }
        }
    </style>

<?php $sql = "SELECT ad.employer_id as val
            FROM {vi_admin_employers} ad
            WHERE ad.user_id = :id";
$comid = $DB->get_record_sql($sql, array('id' => $USER->id)); ?>

    <div class="cont">
        <div class="group pt-1">
            <div class="pg-tit"><?php echo get_string('RecommendedJobs', 'local_job'); ?></div>
            <div class="page-nav has-icon position-relative">
                <p><?php echo get_string('Category', 'local_job'); ?></p>
                <?php
                if ($skill) {
                    echo html_writer::tag('p', $skill->name);
                }
                ?>
                <div class="sub-mn position-static">
                    <span>Menu icon</span>
                    <ul class="p-jbox w-full z-dropdown position-absolute left-0"
                        style="max-height: 500px; overflow: auto;">
                        <ul class="category-list">
                            <li>
                                <a class="text-truncate"
                                   href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/index.php'); ?>">
                                    <b><?php echo get_string('all'); ?></b>
                                </a>
                            </li>
                            <?php
                            // $skill_groups = array_values($DB->get_records('vi_skill_groups', array(), 'name ASC'));
                            $skills = array_values($DB->get_records('vi_skills', array(), 'name ASC'));
                            ?>
                            <?php foreach ($skills as $_skill) : ?>
                                <?php if ($_skill->name != '' && $_skill->name != ' ') { ?>
                                    <li>
                                        <a class="text-truncate"
                                           href="<?php echo new moodle_url($baseurl, array('skillid' => $_skill->id)); ?>">
                                            <?php echo $_skill->name; ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php endforeach; ?>
                        </ul>
                    </ul>
                </div>
            </div>
            <div class="tp-tb-area d-md-flex">
                <ul class="d-none bar-tab mr-3">
                    <li>
                        <div class="d-inline-block">
                            <span class="t-gray"><?php echo get_string('Employment', 'local_job'); ?> : </span>
                            <span><?php echo get_string('All', 'local_job'); ?></span>
                        </div>
                    </li>
                    <li>
                        <div class="d-inline-block">
                            <span class="t-gray"><?php echo get_string('Salary', 'local_job'); ?> : </span>
                            <span><?php echo get_string('All', 'local_job'); ?></span>
                        </div>
                    </li>
                    <li>
                        <div class="d-inline-block">
                            <span class="t-gray"><?php echo get_string('Area', 'local_job'); ?> : </span>
                            <span><?php echo get_string('All', 'local_job'); ?></span>
                        </div>
                    </li>
                </ul>
                <div class="d-none nav-filters-button page-nav m-0 p-0 mr-auto position-relative">
                    <div class="sub-mn ml-0">
                        <span>Menu icon</span>
                        <ul>
                            <li>
                                <div>form filters...</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mr-auto"></div>
                <ul class="bar-tab d-block">
                    <li>
                        <div class="d-inline-block <?php echo $job_sortorder == '' ? '' : 't-gray'; ?>">
                            <a href="<?php echo $baseurl; ?>"><?php echo get_string('popular', 'local_job'); ?></a>
                        </div>
                    </li>
                    <li>
                        <div class="d-inline-block <?php echo $job_sortorder == 'register' ? '' : 't-gray'; ?>">
                            <a href="<?php echo new moodle_url($baseurl, array('job_sortorder' => 'register')); ?>"><?php echo get_string('register', 'local_job'); ?></a>
                        </div>
                    </li>
                    <li>
                        <div class="d-inline-block <?php echo $job_sortorder == 'deadline' ? '' : 't-gray'; ?>">
                            <a href="<?php echo new moodle_url($baseurl, array('job_sortorder' => 'deadline')); ?>"><?php echo get_string('deadline', 'local_job'); ?></a>
                        </div>
                    </li>
                </ul>
            </div>

            <?php if (count($jobs) < 1) { ?>
                <div class="mx-2 my-4 px-3 py-5 bg-area text-center">
                    <p class="h2"><?php echo get_string('nojobsavailable', 'local_job'); ?></p>
                    <p class="h6"><?php echo get_string('popularjobs_2', 'local_job'); ?></p>
                </div>
            <?php } ?>

            <div id="jobsGroup">
                <?php foreach ($jobs as $job) : ?>
                    <div class="row mx-0 border mb-3">
                        <div class="col-3 py-jbox d-md-flex justify-content-center" style="max-height: 220px">
                            <div class="align-self-center" style="height: 100%">
                                <div style="height: 85%;">
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>">
                                        <div style="display: flex; align-items: center; justify-content: center; height: 100%;">
                                            <img class="position-relative w-auto h-auto contain-image"
                                                 src="<?php echo $job->company_logo; ?>">
                                        </div>
                                    </a>
                                </div>
                                <div class="mt-2 text-center" style="height: 15%">
                                    <i class="<?php echo ($comid != null) ? "d-none" : ""; ?> cursor-pointer ic <?php echo mkjobs_has_saved_job($job->id) ? "ic-heart-on" : "ic-heart"; ?>"
                                       onclick="savedJob(this, <?php echo $job->id; ?>)"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-9 pl-0 pr-jbox py-jbox">
                            <div class="row mx-0 tit-jbox">
                                <div class="px-0 col-md-10">
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>">
                                        <?php echo $job->title; ?>
                                    </a>
                                </div>
                                <div class="px-0 col-md-2 d-none d-md-block text-md-right text-primary"><?php echo $job->d_enddate; ?></div>
                            </div>
                            <p class="text-secondary mb-2">
                                <span class="t-gray"><?php echo $job->company_name; ?></span>
                            </p>
                            <div class="row mx-0">
                                <div class="px-0 col-md-9 d-none d-md-block">
                                    <i class="ic ic-usd"></i>
                                    <span><?php echo $job->salary; ?></span>
                                </div>
                                <div class="px-0 col-md-3 text-md-right text-primary">
                                    <p><?php echo $job->location->name; ?></p>
                                </div>
                            </div>
                            <div class="row mx-0">
                                <div class="px-0 col-md-10">
                                    <ul class="cate-list text-left mb-2">
                                        <?php foreach ($job->skills as $skill) : ?>
                                            <li>
                                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                                    <?php echo $skill->name; ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="px-0 col-md-2 text-md-right">
                                    <p class="t-gray"><?php echo $job->timecreated; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="search-list_pagination">
                <ul>
                    <li class="search-list_paginationitem-first <?php if ($jobs_data['page'] == 1) echo 'disabled' ?>">
                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/index.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'] - 1)) ?>">
                            <i class="far fa-angle-left"></i>
                        </a>
                    </li>
                    <?php mkjobs_index_start_pagination($jobs_data['total_page'], $jobs_data['page'], $jobs_data) ?>
                    <li class="search-list_paginationitem-last <?php if ($jobs_data['page'] == $jobs_data['total_page']) echo 'disabled' ?>">
                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/index.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'] + 1)) ?>">
                            <i class="far fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <script>
        function savedJob(el, id, classon, classoff) {
            $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkjobs_saved_job",
                jobid: id
            }, function (data) {
                if (data == -1) {
                    if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                    }
                } else if (data == 1) {
                    $(el).removeClass(classoff || "ic-heart");
                    $(el).addClass(classon || "ic-heart-on");
                } else {
                    $(el).removeClass(classon || "ic-heart-on");
                    $(el).addClass(classoff || "ic-heart");
                }
            });
        }
    </script>
<?php
$VISANG->theme->footer();
