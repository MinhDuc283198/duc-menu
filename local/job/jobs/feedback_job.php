<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $PAGE;

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

// =====================================================================================================
// handles
$feeckback_job_id = required_param('feedback_job_id', PARAM_INT);

// =====================================================================================================
// renders



$VISANG->theme->title = 'Job feedback';
$VISANG->theme->menu = 'Job feedback';
$VISANG->theme->header();
?>

<?php $sql = "SELECT ad.employer_id as val
            FROM {vi_admin_employers} ad
            WHERE ad.user_id = :id";
$comid = $DB->get_record_sql($sql, array('id' => $USER->id));
$job_feedback=mjobbs_get_feedback_detail($feeckback_job_id);
$more_feedback=mjobbs_get_feedback();

?>


<div class="job-feedback-detail ">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="jmsf-main-content">
                    <div>
                        <h1 class="title-jmsf"><?php echo $job_feedback->title?></h1>
                    </div>
                    <div class="avatar-jmsf">
                        <img alt="<?php echo $job_feedback->username?>" class="s ft fm fl" src="<?php echo  $CFG->wwwroot . '/pluginfile.php/' . $job_feedback->user_avatar_url; ?>" width="48" height="48">
                        <div class="up">
                            <span class="jmsf-name"><?php echo $job_feedback->username?></span>
                            <p><span><?php echo $job_feedback->timecreated.' · '?></span><span><?php echo $job_feedback->company_name?></span></sp>
                        </div>
                    </div>
                    <div class="jmsf-content">
                        <?php echo $job_feedback->content?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            <div class="jmsf-more">
                <div class="jmsf-title-more">
                    <h3 class="title-main">More feedback</h3>
                </div>
            <div class="jmsf-more-main">
                <?php foreach ($more_feedback as $job_feedbacks) : ?>
                    <a href="<?php echo $VISANG->wwwroot . '/jobs/feedback_job.php?feedback_job_id=' . $job_feedbacks->id ?>"><p><?php  echo $job_feedbacks->title ?></p></a>
                <?php endforeach; ?>
            </div>
            </div>

            </div>
        </div>
    </div>
</div>

    <!--custom js start-->

    <style>
.jmsf-more-main p {
    overflow: hidden;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    display: -webkit-box;
}
h3.title-main {
    font-size: 24px;
    padding: 10px 0 10px 15px;
    width: 85%;
    color: #fff;
    background: #93D630;
    border-radius: 0px 50px 50px 0px;
    font-weight: 600 !important;
}
.jmsf-more {
    box-shadow: -1px -1px 3px rgb(0 0 0 / 8%), 1px 1px 3px rgb(0 0 0 / 8%);

}
.jmsf-more-main {
    padding: 15px;
}
.jmsf-title-more {
    background: #F4F4F4;
}
.jmsf-main-content {
box-shadow: -1px -1px 3px rgb(0 0 0 / 8%), 1px 1px 3px rgb(0 0 0 / 8%);
padding: 30px;
}
.avatar-jmsf {
display: flex;
}
h1.title-jmsf {
    padding: 15px 0;
}
img.s.ft.fm.fl {
    width: 60px;
    height: 60px;
}
    span.jmsf-name {
    color: #485bc6;
}

h1.title-jmsf {
    font-weight: 700;
    color: #2337A4;
    font-size: 24px;
    letter-spacing: -0.011em;
   
}
.avatar-jmsf img {
    border: 2px solid #a3db4f;
    border-radius: 50%;
    padding: 2px;
    margin-right: 10px;
    object-fit: cover;
}
.job-feedback-detail {
    padding: 30px 0;
}
.job-feedback-detail h5 {
    font-weight: bold;
    font-size: 23px;
    color: #000000 !important;
}
.job-feedback-detail th {
    font-size: 19px;
    width: 20%;
    background: #F4F4F4;
    text-align: inherit;
    font-weight: 700;
    color: #000000;
}
.job-feedback-detail td {
    font-size: 19px;
    font-weight: 400;
    color: #000000;
}
.job_feeback_ava {
    text-align: center;
}
.job-feedback-detail table, th, td {
  border: 1px solid black;
}

.job-feedback-detail table, th, td {
    border: 1px solid #E0E0E0;
    box-sizing: border-box;
    box-shadow: 2px 2px 3px rgb(0 0 0 / 8%);
    padding: 30px;
    border: 1px solid #E0E0E0;
    background: #ffffff;
}
.jmsf-content img {
    width: 100%;
    object-fit: contain;
}
.jmsf-more-main a:first-child {
    display: none;
}
@media screen and (max-width: 1199px){
h3.title-main {
    font-size: 20px;
}
}

</style>
    <!--custom js end-->
<?php
$VISANG->theme->footer();
