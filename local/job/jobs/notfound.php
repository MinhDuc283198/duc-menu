<?php

require_once(dirname(__FILE__) . '/../lib.php');
global $CFG, $VISANG, $DB, $USER, $SESSION;

$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->header();

$returnurl = optional_param('returnurl', $VISANG->wwwroot, PARAM_RAW);
$error = optional_param('error', 0, PARAM_INT);
?>
<div class="cont">
    <div class="group">
        <div role="main">
            <div class="mail-end-bx style02">
            <?php if($error == 1){?>
            	<div><?php echo get_string("employer:jobnotfound", "local_job"); ?></div>
                <p>
                    <?php echo get_string("employer:jobnotfound2", "local_job"); ?><br />
                </p>
            <?php }else if($error == 2){?>
            	<div><?php echo get_string("employer:jobend", "local_job"); ?></div>
                <p>
                    <?php echo get_string("employer:jobend2", "local_job"); ?><br />
                </p>
            <?php }?>
                
                <a href="<?php echo $returnurl; ?>" class="btns big"><?php echo get_string("employer:backurl", "local_job"); ?></a>
            </div>
        </div>
    </div>
</div>
<?php
$VISANG->theme->footer(false);
