<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();
mkjobs_require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$jobid = required_param('jobid', PARAM_INT);
$job = $DB->get_record('vi_jobs', array('id' => $jobid));
$thistime = time();
if ($job == null || $job->published == 0 || !($job->startdate < $thistime && $thistime < $job->enddate)) {
    throw new moodle_exception('notfound');
}

$job = mkjobs_get_job_detail($job);

$resumes = $DB->get_records_select('vi_resumes', 'user_id=:userid AND (type=:resume OR type=:cvfile)', array('userid' => $USER->id, 'resume' => 'resume', 'cvfile' => 'cvfile'), '{vi_resumes}.is_default DESC, {vi_resumes}.timemodified DESC');

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        지원하기
        <a href="#" class="pop-close">닫기</a>
    </div>
    <form method="post" onsubmit="return confirm('이력서를 제출하시겠습니까? 제출 후에는 수정 및 제출 취소가 불가합니다.')">
        <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
        <input type="hidden" name="jobid" value="<?php echo $jobid; ?>">
        <input type="hidden" name="action" value="recruitsubmit">

        <div class="pop-contents">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            <?php echo $job->company_name; ?>
                        </th>
                        <th>
                            <?php echo $job->title; ?>
                        </th>
                    </tr>
                </thead>
            </table>
            <div class="addr-info">
                <div class="rw mg-bt20">
                    <p class="tit f-l">이력서 선택</p>
                    <select name="resumeid">
                        <?php foreach ($resumes as $resume) : ?>
                            <option value="<?php echo $resume->id; ?>"><?php echo $resume->title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="btn-area text-center">
            <input type="button" value="취소" class="btns" onclick="$('.pop-close').click();" />
            <input type="submit" value="저장" class="btns point" <?php echo count($resumes) ? '' : 'disabled'; ?> />
        </div>
    </form>
</div>
<?php
echo $OUTPUT->footer();
