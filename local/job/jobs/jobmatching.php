<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');

mkjobs_require_login();

// =====================================================================================================
// handles
// $skillgroupid = optional_param('skillgroupid', 0, PARAM_INT);
$skillid = optional_param('skillid', 0, PARAM_INT);
$job_sortorder = optional_param('job_sortorder', '', PARAM_RAW);

$baseurl = new moodle_url($VISANG->wwwroot . '/jobs/index.php');

$districts = mkjobs_get_all_location();
$cateories = mkjobs_get_all_categories();

// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'jobs';


require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($CFG->dirroot . '/local/course/lib.php');

$VISANG->theme->header();
?>
    <style>
        .list-recommend-employee {
            /*margin: 5% auto;*/
        }
    </style>
    <div class="main">
        <div class="search-wrapper">
            <div class="search-sec">
                <form action="<?php echo $CFG->wwwroot . '/local/job/search.php' ?>" method="get">
                    <div class="col-lg-11 col-md-11" style="margin: 0 auto;">
                        <div class="row px-4 px-md-0">
                            <div class="col-lg-4 col-md-12 col-sm-12 p-1 input-wrapper">
                                <img src="img/search.png" alt="search-icon">
                                <input type="text" name="searchtext" class="form-control search-slt search-keyworld" id="search-keyworld"
                                       placeholder="<?php echo get_string('searchkeyworld', 'local_job'); ?>">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                                <img src="img/allcategory.png" alt="search-icon">
                                <select class="form-control search-slt search-category custom-select" id=""
                                        name="skillgroupid">
                                    <option value="0"><?php echo get_string('searchcategories', 'local_job'); ?></option>
                                    <?php foreach ($cateories as $c) : ?>
                                        <option value="<?php echo $c->id ?>"><?php echo $c->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                                <img src="img/maps.png" alt="search-icon">
                                <select class="form-control search-slt search-location custom-select" id=""
                                        name="city">

                                    <option value="0"><?php echo get_string('searchlocation', 'local_job'); ?></option>
                                    <?php foreach ($districts as $d) : ?>
                                        <option value="<?php echo $d->id ?>"><?php echo ($current_language == "ko") ? $d->name_ko : (($current_language == "en") ? $d->name_en : $d->name_vi); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12 p-1" style="margin: 0 auto">
                                <button type="submit" class="btn btn-danger wrn-btn"><i class="fa fa-search"
                                                                                        aria-hidden="true"></i><?php echo get_string('find_now', 'local_job') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container full-content">
            <div class="recommended-jobs-area">
                <div class="recommended-jobs_title">
                    <h2>RECOMMENDED JOBS</h2>
                </div>
                <div id="recommend-employee-job-list" class="recommended-jobs_list carousel slide" data-ride="carousel"
                     data-interval="false">
                    <div class="recommended-jobs_items carousel-inner" role="listbox">
                        <?php $count = 0; ?>
                        <?php
                        $recommend_jobs = mkjobs_get_recommend_jobs(isloggedin());
                        ?>

                        <?php foreach ($recommend_jobs as $recommend_job) : ?>
                            <?php
                            $saved_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $recommend_job->id));
                            ?>
                            <?php
                            if ($count == 0 || $count % 4 == 0) {
                                if ($count == 0) {
                                    echo '<div class="carousel-item active">';
                                    echo '<div class="row">';
                                } else {
                                    echo '<div class="carousel-item">';
                                    echo '<div class="row">';
                                }
                            }
                            ?>

                            <div class="recommended-job_item col-12">
                                <div class="job-item_wrapper">
                                    <a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id; ?>">
                                        <div class="row">
                                            <div class="col-3 col-md-2">
                                                <div class="job-item_company-logo-wrapper">
                                                    <div class="job-item_company-logo">
                                                        <img src="<?php echo $recommend_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $recommend_job->company_logo; ?>"
                                                             alt="Image of recommend job" class="img-recommend-employee"
                                                             style="object-fit: cover;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-9">
                                                <div class="job-item_content">
                                                    <h3 class="job-item_jobtitle"><?php echo $recommend_job->title; ?></h3>
                                                    <p class="job-item_companyname"><?php echo $recommend_job->company_name; ?></p>
                                                    <div class="jobmatchingstar-container">
                                                        <div class="result-container">
                                                            <div class="rate-bg" style="width:<?php echo $recommend_job->matchingstar->percentstar; ?>%; max-width: 100%;"></div>
                                                            <div class="rate-stars"></div>
                                                        </div>
                                                        <span class="reviewScore"><?php echo $recommend_job->matchingstar->star; ?>% matching</span>
                                                    </div>
                                                    <div class="job-item_info">
                                                        <div class="job-item_info-item">
                                                            <img src="../assets/dist/images/salary-icon.png"
                                                                 alt="salary icon">
                                                            <span><?php echo $recommend_job->job_salary; ?></span>
                                                        </div>
                                                        <div class="job-item_info-item">
                                                            <img src="../assets/dist/images/location-icon.png"
                                                                 alt="location icon">
                                                            <span><?php echo $recommend_job->company_location; ?></span>
                                                        </div>
                                                        <div class="job-item_info-item">
                                                            <img src="../assets/dist/images/experience-icon.png"
                                                                 alt="experience icon">
                                                            <span><?php echo $recommend_job->name_vi; ?></span>
                                                        </div>
                                                        <div class="job-item_info-item">
                                                            <img src="../assets/dist/images/timestamp-icon.png"
                                                                 alt="timestamp icon">
                                                            <span><?php echo $recommend_job->timenotificreated; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-3 job-item_savejob">
                                                <div class="job-item_savejob-wrapper">
                                                    <div class="row">
                                                        <button type="button"
                                                                class="btn btn-apply showpop-apply"><?php echo get_string('applynow', 'local_job') ?></button>
                                                    </div>
                                                    <div class="row">
                                                        <button type="button" class="btn btn-save saved-job"
                                                                id="saved-job"
                                                                onclick="savedJob('.save-icon<?php echo $job->id; ?>', '<?php echo $job->id; ?>', 'save-on', 'save-off')"><?php echo get_string('savejob', 'local_job') ?>
                                                            <span class="<?php echo ($comid != null) ? "save-none" : ""; ?> save-icon<?php echo $job->id; ?> <?php echo mkjobs_has_saved_job($job->id) ? "save-on" : "save-off"; ?>"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php $count++; ?>
                            <?php
                            if ($count % 4 == 0 || $count == count($recommend_jobs)) {
                                echo '</div>';
                                echo '</div>';
                            }
                            ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="pagination-listjob">
                        <?php
                        if (count($recommend_jobs) % 4 == 0) {
                            $recommend_job_slide_dot = count($recommend_jobs) / 4;
                        } else {
                            $recommend_job_slide_dot = ceil(count($recommend_jobs) / 4);
                        }
                        ?>
                        <?php if ($recommend_job_slide_dot != 0) : ?>
                            <a class="carousel-control-prev recommend-job-control pc"
                               href="#recommend-employee-job-list"
                               role="button"
                               data-slide="prev"
                               onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <ol class="carousel-indicators list-dot">
                                <?php for ($i = 0; $i < $recommend_job_slide_dot; $i++) : ?>
                                    <?php if ($i == 0) : ?>
                                        <li data-target="#recommend-employee-job-list" data-slide-to="0"
                                            class="recommend-job-control pc active"
                                            onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                            1
                                        </li>
                                    <?php else : ?>
                                        <li data-target="#recommend-employee-job-list"
                                            data-slide-to="<?php echo $i; ?>"
                                            class="recommend-job-control pc"
                                            onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                            <?php echo $i + 1; ?>
                                        </li>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            </ol>
                            <a class="carousel-control-next recommend-job-control pc"
                               href="#recommend-employee-job-list"
                               role="button"
                               data-slide="next"
                               onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        <?php else : ?>
                            <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="recommended-course-area">
                <div class="recomend-course">
                    <h4><?php echo get_string('MasterKoreanProposalCourse', 'local_job'); ?></h4>
                    <div id="recommend-course" class="carousel slide" data-ride="carousel" data-interval="false">
                        <?php
                        require_once($CFG->dirroot . '/local/course/lib.php');

                        ?>
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $upCourses = local_course_recommend(isloggedin());
                            $count = 0;
                            foreach ($upCourses as $course) {
                                $context = context_course::instance($course->lcocourseid);
                                $courseimg = local_course_get_imgpath($context->id);
                                ?>

                                <?php if ($count == 0 || $count % 4 == 0) {
                                    if ($count == 0) {
                                        echo '<div class="carousel-item active">';
                                        echo '<div class="row">';
                                    } else {
                                        echo '<div class="carousel-item">';
                                        echo '<div class="row">';
                                    }
                                } ?>
                                <div class="col-md-6">
                                    <div class="recommend-item">
                                        <a href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $course->id; ?>">
                                            <div class="row">
                                                <div class="col-xl-4 recommend-item_img">
                                                    <img style="width: 100%;" src="<?php echo $courseimg; ?>"
                                                         alt="Image of recommend course">
                                                </div>
                                                <div class="col-xl-8">
                                                    <div class="recommend-item_content">
                                                        <h2>
                                                            <p class="block-ellipsis-title ellipsis"><?php echo $course->title; ?> </p>
                                                        </h2>
                                                        <p class="block-ellipsis-description ellipsis">
                                                            <span> <?php echo $course->onelineintro; ?></span>
                                                        </p>
                                                        <div class="main-course-cate mb-2">
                                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                <?php if ($i <= $course->star) { ?>
                                                                    <i style="color: #0b93d5 !important;"
                                                                       class="fa fa-star"></i>
                                                                <?php } else { ?>
                                                                    <i class="fa fa-star"></i>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <p class="course-price">
                                                                <?php echo number_format($course->price); ?>
                                                                <span>VND</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <?php $count++; ?>
                                <?php if ($count % 4 == 0 || $count == count($upCourses)) {
                                    echo '</div>';
                                    echo '</div>';
                                } ?>
                            <?php } ?>
                        </div>
                        <div class="pagination-listjob">
                            <?php
                            if (count($upCourses) % 4 == 0) {
                                $recommend_course_slide_dot = count($upCourses) / 4;
                            } else {
                                $recommend_course_slide_dot = ceil(count($upCourses) / 4);
                            }
                            ?>
                            <?php if ($recommend_course_slide_dot != 0) : ?>
                                <a class="carousel-control-prev course" href="#recommend-course" role="button"
                                   data-slide="prev"
                                   onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <ol class="carousel-indicators">
                                    <?php for ($i = 0; $i < $recommend_course_slide_dot; $i++) : ?>
                                        <?php if ($i == 0) : ?>
                                            <li data-target="#recommend-course" data-slide-to="0" class="course active"
                                                onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)"></li>
                                        <?php else : ?>
                                            <li data-target="#recommend-course" data-slide-to="<?php echo $i; ?>"
                                                class="course"
                                                onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)"></li>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </ol>
                                <a class="carousel-control-next course" href="#recommend-course" role="button"
                                   data-slide="next"
                                   onclick="paginationSlide('course',<?php echo $recommend_course_slide_dot ?>)">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            <?php else : ?>
                                <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        function paginationSlide(nameClass, numberPage) {
            if ($("li." + nameClass + ".active").attr("data-slide-to") != 0 && $("li." + nameClass + ".active").attr("data-slide-to") != (numberPage - 1)) {
                $("a." + nameClass).removeClass("arrow-hide");
                $("a." + nameClass).addClass("arrow-show");
            } else {
                if ($("li." + nameClass + ".active").attr("data-slide-to") == 0) {
                    $("a.carousel-control-prev." + nameClass).removeClass("arrow-show");
                    $("a.carousel-control-prev." + nameClass).addClass("arrow-hide");
                } else {
                    $("a.carousel-control-prev." + nameClass).removeClass("arrow-hide");
                    $("a.carousel-control-prev." + nameClass).addClass("arrow-show");
                }
                if ($("li." + nameClass + ".active").attr("data-slide-to") == (numberPage - 1)) {
                    $("a.carousel-control-next." + nameClass).removeClass("arrow-show");
                    $("a.carousel-control-next." + nameClass).addClass("arrow-hide");
                } else {
                    $("a.carousel-control-next." + nameClass).removeClass("arrow-hide");
                    $("a.carousel-control-next." + nameClass).addClass("arrow-show");
                }
            }
        }

        function resetPagination() {
            paginationSlide("recommend-course-control.pc",<?php echo $recommend_course_slide_dot; ?>);
            paginationSlide("recommend-job-control.pc",<?php echo $recommend_job_slide_dot; ?>);
            paginationSlide("course", <?php echo $recommend_course_slide_dot; ?>);
        }

        setInterval(resetPagination, 1000);
    </script>
    <script type="text/javascript">
        $(document).ready(showPosition());
        function setDefaultSearchCategory() {
            let categoryBlock = document.querySelector(".search-category");
            let defaultCategory = 'Biên Dịch / Phiên Dịch';

            Array.prototype.forEach.call(categoryBlock.options, function (option, index) {
                if (option.innerText === defaultCategory) {
                    categoryBlock.value = option.attributes.value.value
                }
            });
        }

        function setDefaultPosition() {
            let addressiplocations = [];
            let locationSelector = document.querySelector(".search-location");
            let companylocation = 'Hà Nội';
            <?php foreach ($districts as $district) : ?>
            addressiplocations.push({id: <?php echo $district->id; ?>, name: '<?php echo $district->name; ?>'});
            <?php endforeach; ?>
            addressiplocations.forEach((addressiplocation) => {
                if (addressiplocation.name === companylocation) {
                    locationSelector.value = addressiplocation.id
                }
            })
        }

        function showPosition() {
            setDefaultSearchCategory();
            setDefaultPosition();
            let url = 'https://ipinfo.io/json';
            $.getJSON(url, function (data) {
                let addressiplocations = [];
                let namecity = '';
                let companylocation = removeVietnameseTones(data.city);
                <?php foreach ($districts as $district) : ?>
                namecity = removeVietnameseTones('<?php echo $district->name; ?>')
                addressiplocations.push({id: <?php echo $district->id; ?>, name: namecity});
                <?php endforeach; ?>
                let searchkeyworld= document.getElementById("search-keyworld");
                let categorySelector = document.querySelector(".search-category");
                let locationSelector = document.querySelector(".search-location");
                addressiplocations.forEach((addressiplocation) => {
                    if (addressiplocation.name === companylocation) {
                        locationSelector.value = addressiplocation.id
                    }
                })
                let keyworld='<?php echo !isset($_COOKIE["searchtext"])? '':$_COOKIE["searchtext"]; ?>';
                let cateid=<?php echo !isset($_COOKIE["searchcate"]) ? 71 :$_COOKIE["searchcate"]; ?>;
                let posiid;
                <?php if(!isset($_COOKIE["searchloca"])): ?>
                posiid=addressiplocation.id;
                <?php else: ?>
                posiid=<?php echo $_COOKIE["searchloca"];?>
                <?php endif; ?>


                if(cateid!==null){
                    categorySelector.value=cateid;
                }
                if(posiid!==null){
                    locationSelector.value=posiid;
                }
                if(keyworld!==''){
                    searchkeyworld.value=keyworld;
                }
            });

            function removeVietnameseTones(str) {
                str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                str = str.replace(/đ/g, "d");
                str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
                str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
                str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
                str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
                str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
                str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
                str = str.replace(/Đ/g, "D");
                // Some system encode vietnamese combining accent as individual utf-8 characters
                // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
                str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
                str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
                // Remove extra spaces
                // Bỏ các khoảng trắng liền nhau
                str = str.replace(/ + /g, " ");
                str = str.trim();
                // Remove punctuations
                str = str.toLowerCase()
                str = str.split(' ').join('');
                // Bỏ dấu câu, kí tự đặc biệt
                str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
                str = str.replace("city", "");
                str = str.replace("thanhpho", "");
                str = str.replace("tp", "");
                return str;
            }
        }
    </script>

<?php
$VISANG->theme->footer();
