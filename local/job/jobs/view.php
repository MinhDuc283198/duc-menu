<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $PAGE;

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

// =====================================================================================================
// handles
$jobid = required_param('jobid', PARAM_INT);
$returnurl = optional_param('returnurl', $VISANG->wwwroot, PARAM_RAW);
$job = $DB->get_record('vi_jobs', array('id' => $jobid));
$benefit_types_sql = "SELECT bt.*
                FROM {vi_job_benefit_types} jbt
                JOIN {vi_benefit_types} bt ON jbt.benefit_type_id = bt.id
                WHERE jbt.job_id = :jobid";
$benefit_types = $DB->get_records_sql($benefit_types_sql, array('jobid' => $jobid));
$benefit_types_all = $DB->get_records("vi_benefit_types", array());
$thistime = time();
if ($job == null || $job->published == 0) {
//     throw new moodle_exception('notfound');
    redirect(new moodle_url($VISANG->wwwroot . '/jobs/notfound.php', array('returnurl' => $returnurl, 'error' => 1)));
}
if (!($job->startdate < $thistime && $thistime < $job->enddate)) {
    //     throw new moodle_exception('notfound');
    redirect(new moodle_url($VISANG->wwwroot . '/jobs/notfound.php', array('returnurl' => $returnurl, 'error' => 2)));
}

$baseurl = new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $jobid));

$action = optional_param('action', '', PARAM_RAW);

$applied = $DB->get_record('vi_job_applications', array('user_id' => $USER->id, 'job_id' => $job->id));

if ($action == 'recruitsubmit') {
    require_sesskey();

    $resumeid = optional_param('resumeid', 0, PARAM_INT);
    $video_resume = optional_param('video_resume', 0, PARAM_INT);

    if ($applied == null) {
        $apply = new stdClass();
        $apply->user_id = $USER->id;
        $apply->job_id = $job->id;
        $apply->candidate_fullname = $USER->firstname . $USER->lastname;
        $apply->candidate_cv = ($video_resume == 0 ? $resumeid : $video_resume);
        $apply->timecreated = time();
        $apply->timemodified = time();

        $DB->insert_record('vi_job_applications', $apply);
        redirect($baseurl);
    } else {
        $apply = new stdClass();
        $apply->id = $applied->id;
        $apply->candidate_cv = ($video_resume == 0 ? $resumeid : $video_resume);
        $apply->timemodified = time();
        $DB->update_record('vi_job_applications', $apply);
        redirect($baseurl);
    }
}

$job = mkjobs_get_job_detail($job);
$jobb = $DB->get_record('vi_jobs', array('id' => $jobid));
$company = $DB->get_record('vi_employers', array('id' => $jobb->employer_id));
$current_language = current_language();

$employer = mkjobs_get_employer_info($job->employer_id);

$districts = mkjobs_get_all_location();
$cateories = mkjobs_get_all_categories();

// update view count
$viewcout = new stdClass();
$viewcout->id = $job->id;
$viewcout->view_count = intval($job->view_count) + 1;
$DB->update_record('vi_jobs', $viewcout);

// =====================================================================================================
// renders

$VISANG->theme->title = $job->title;
$VISANG->theme->menu = 'jobs';
$VISANG->theme->header();
?>

<?php $sql = "SELECT ad.employer_id as val
            FROM {vi_admin_employers} ad
            WHERE ad.user_id = :id";
$comid = $DB->get_record_sql($sql, array('id' => $USER->id)); ?>

    <style>
        /* section content start */
        .bg-area {
            position: relative;
        }

        .content .bg-content {
            width: 100%;
            height: auto;
        }

        .detail-title {
            position: absolute;
            z-index: 10;
            bottom: 0;
            transform: translateY(50%);
            width: 100%;
        }

        .detail-title-fixed {
            z-index: 11;
            top: 101px;
            position: fixed;
            width: 100%;
            height: 200px;
        }

        .detail {
            background: #FFFFFF;
            border-radius: 10px;
            box-shadow: 4px 4px 10px 0px rgba(0, 0, 0, 0.1);
        }

        .bg-area__share,
        .bg-area__flag {
            position: absolute;
            z-index: 10;
            width: 44px;
            height: 44px;
        }

        .bg-area__flag {
            top: 54px;
            right: 100px;
            width: 33px;
            height: 43px;
        }

        .bg-area__share {
            top: 52px;
            right: 21px;
        }

        .bg-area__detail-height {
            height: 100%;
        }

        .detail-title__title {
            font-weight: bold;
            font-family: 'Roboto', system-ui;
            font-size: 25px;
            line-height: 32px;
            font-style: normal;
            color: #4f4f4f;
        }

        .detail-title__desc {
            font-style: normal;
            font-weight: normal;
            font-size: 19px;
            line-height: 22px;
            text-transform: uppercase;
            color: #828282;
            font-family: 'Roboto', system-ui;
        }

        .detail-title__address {
            font-style: normal;
            font-weight: normal;
            font-size: 19px;
            line-height: 22px;
            color: #828282;
            font-family: 'Roboto', system-ui;
        }

        .detail-title__salary {
            font-style: normal;
            font-weight: 700;
            line-height: 32px;
            color: #69AF00;
            font-size: 25px;
            font-family: 'Roboto', system-ui;
        }

        .btn-apply,
        .btn-save {
            border: 1px solid #485BC6;
            height: 75px;
            width: 96%;
            padding: 0;
            margin-left: 40px;
        }

        .btn-apply {
            background: #485BC6;
            color: #ffffff;
            font-weight: bold;
            font-size: 26px;
            line-height: 30px;
        }

        .btn-apply:hover {
            background: #7f8bce;
            color: #ffffff;
        }

        .btn-save:hover {
            background: #dbdce4;
        }

        .btn-save {
            margin-top: 5px;
            color: #485BC6;
            font-size: 19px;
            line-height: 32px;
        }

        .btn-apply-fixed,
        .btn-save-fixed {
            border: 1px solid #485BC6;
            height: 75px;
            width: 100%;
        }

        .btn-apply-fixed {
            background: #485BC6;
            color: #ffffff;
        }

        .btn-apply-fixed:hover {
            background: #7f8bce;
            color: #ffffff;

        }

        .btn-save-fixed:hover {
            background: #dbdce4;

        }

        .detail-title .img {
            padding: 15px 15px;
        }

        .detail-title .img {
            width: 100%;
            height: 100%;
        }

        .tab {
            width: 100%;
            padding-bottom: 32px;
        }

        .map-company {
            background: #FFFFFF;
            box-shadow: 4px 3px 9px 1px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
        }

        .company-address iframe {
            box-shadow: 4px 3px 9px 1px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
        }

        .company-address {
            width: 100%;
            height: 192px;
            padding-left: 0;
            padding-right: 0;
            background: rgba(0, 0, 0, 0.2);
            box-shadow: 4px 3px 9px 1px rgb(0 0 0 / 10%);
            border-radius: 10px;

        }

        .sort-infor .row {
            height: auto;
            padding-top: 20px;
            padding-left: 30px;
            font-style: normal;
            font-weight: normal;
            font-family: 'Roboto', system-ui;
            font-size: 17px;
            line-height: 32px;
        }

        .sort-infor .row img {
            object-fit: cover;
        }

        .sort-infor .row .col-lg-8 {
            white-space: nowrap;
        }

        .job-detail1 {
            height: auto;
            margin-top: 34px;
            background: #FFFFFF;
            border: 0.25px solid #dcdcdc;
            box-shadow: 4px 3px 9px 1px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
        }

        .job-detail1 h2 {
            font-style: normal;
            font-weight: 700;
            font-size: 23px;
            line-height: 32px;
            color: #000000;
            font-family: 'Roboto', system-ui;
            margin-bottom: 20px;
            text-transform: uppercase;
        }

        .job-detail1 .contentj {
            font-style: normal;
            font-weight: 400;
            font-size: 19px;
            line-height: 32px;
            color: #000000;
            font-family: 'Roboto', system-ui;
        }

        .jobdetail1 {
            padding: 55px 40px;
        }

        .jobdetail1 .row {
            margin-bottom: 60px;
        }

        .jobdetail {
            padding: 25px;
        }

        .similar-job {
            height: 100%;
            background: #FFFFFF;
            box-shadow: 4px 3px 9px 1px rgb(0, 0, 0, 0.1);
            border-radius: 10px;
            margin-left: 1%;
        }

        .similar-job-title {
            height: auto;
            font-style: normal;
            font-weight: 700;
            font-family: 'Roboto', system-ui;
            font-size: 20px;
            line-height: 30px;
            color: #FFFFFF;
            background: #485BC6;
            box-shadow: 10px 7px 10px rgba(0, 0, 0, 0.25);
            margin-top: 29px;
            padding-top: 12px;
            padding-left: 40px;
            /* padding-right: 46px; */
            padding-bottom: 11px;
        }

        .similar-job-title span {
            white-space: nowrap;
        }

        .contentj p {
            margin-top: 0;
            margin-bottom: 0;
            text-align: justify;
            font-size: 19px;
        }

        .similar__title {
            font-style: normal;
            font-weight: bold;
            font-family: 'Roboto', system-ui;
            font-size: 25px;
            line-height: 32px;
            color: #4F4F4F;
            display: -webkit-box;
            width: auto;
            overflow: hidden;
            text-overflow: ellipsis;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }

        .similar-title-oneline {
            width: auto;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .similar__company {
            font-style: normal;
            font-weight: normal;
            font-family: 'Roboto', system-ui;
            font-size: 19px;
            line-height: 22px;
            text-transform: uppercase;
            color: #828282;
            width: auto;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .similar__job {
            margin-top: 70px;
            width: 100%;
        }

        .similar__seemore p {
            font-style: normal;
            font-weight: bold;
            font-family: 'Roboto', system-ui;
            font-size: 23px;
            line-height: 32px;
            color: #000000;
        }

        .similar__seemore {
            margin-top: 90px;
            margin-bottom: 75px;
        }

        .benefits-tag-title span {
            font-style: normal;
            font-weight: 700;
            font-family: 'Roboto', system-ui;
            font-size: 23px;
            line-height: 32px;
            color: #2337A4;
            text-transform: uppercase;
            white-space: nowrap;
        }

        .benefits-tag {
            margin-top: 70px;
            flex-wrap: nowrap;
        }

        .libene span {
            margin-left: 12px;
            font-size: 15px;
        }

        .benefits-tag ul {
            display: inline-flex;
        }

        .benefits-tag ul li {
            width: auto;
        }

        .benefits-tag img {
            object-fit: contain;
            Width: 42px;
            Height: 42px;
        }

        .job-infor header {
            display: flex;
        }

        .lbl + .lbl {
            padding-left: 80px;
        }

        .job-infor input[type="radio"] {
            display: none;
        }

        header .tab-job-detail p,
        header .tab-comp-infor p,
        header .tab-comp-job-other p {

            font-style: normal;
            font-weight: 700;
            font-family: 'Roboto', system-ui;
            font-size: 29px;
            line-height: 41px;
            text-transform: uppercase;
            color: #CCCCCC;
        }

        header label hr {
            border: 1px solid #CCCCCC;
            background-color: #CCCCCC;

        }


        #job-detail1:checked ~ header .tab-job-detail hr {
            border: 2px solid #4F4F4F;
            background-color: #4F4F4F;
        }

        #company-infor:checked ~ header .tab-comp-infor hr {
            border: 2px solid #4F4F4F;
            background-color: #4F4F4F;
        }

        #more-job-company:checked ~ header .tab-comp-job-other hr {
            border: 2px solid #4F4F4F;
            background-color: #4F4F4F;
        }

        #job-detail1:checked ~ header .tab-job-detail p {
            color: #4F4F4F;
        }

        #company-infor:checked ~ header .tab-comp-infor p {
            color: #4F4F4F;
        }

        #more-job-company:checked ~ header .tab-comp-job-other p {
            color: #4F4F4F;
        }

        .switch-job1 {
            flex-wrap: nowrap;
            margin-bottom: 3%;
        }

        .skill-tag-title span {
            font-style: normal;
            font-weight: 700;
            font-family: 'Roboto', system-ui;
            font-size: 23px;
            line-height: 32px;
            text-transform: uppercase;
            white-space: nowrap;
        }

        .skill-tag {
            margin-top: 75px;
        }

        .skill-tags ul {
            display: inline-flex;
        }

        .skill-tags ul li {
            margin-right: 10px;
        }

        .btn-skill {
            border: 0.5px solid #000000;
            box-sizing: border-box;
            border-radius: 6px;
            bottom: 0;
            padding: 15px;
            width: 100%;
            white-space: nowrap;
        }

        .signup-now {
            padding-top: 72px;
        }

        .signup-now hr {
            border: 1px solid #000000;
            background-color: #000000;
            width: 30%;
            margin-left: 0;
        }

        .signup-col {
            transform: translateY(30%);
            margin: 0 auto;
        }

        .signup-notice span {
            font-style: normal;
            font-weight: 300;
            font-family: 'Roboto', system-ui;
            font-size: 48px;
            line-height: 55px;
            color: #4F4F4F;

        }

        .btnsignupnow {
            background: #485BC6;
            border-radius: 6px;
            font-style: normal;
            font-weight: bold;
            font-family: 'Roboto', system-ui;
            font-size: 26px;
            line-height: 30px;
            color: #FFFFFF;
            margin-top: 5%;
            text-transform: uppercase;
            padding: 3%;
        }

        .btnsignupnow:hover {
            background: #7f8bce;
            color: #ffffff;
            margin-bottom: 5px;
        }

        .bgfoo {
            padding-bottom: 74px;
        }

        .bgfoo img {
            position: relative;

        }

        .bgfoo span {
            position: absolute;
            font-style: normal;
            font-weight: 700;
            font-family: 'Roboto', system-ui;
            font-size: 300%;
            text-align: center;
            color: #fff;
            z-index: 10;
            width: 60%;
            transform: translate(30%, 100%);
            text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
        }

        .save-none {
            display: none !important;
        }

        .save-off {
            background-image: url(/theme/oklassedu/pix/images/icon_heart02.png) !important;
            background-size: contain;
            width: 34px;
            height: 30px;
            position: relative;
            background-repeat: no-repeat;
            display: inline-block;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            box-sizing: border-box;
            margin-bottom: -8px;
        }

        .save-on {
            background-image: url(/theme/oklassedu/pix/images/icon_heart02_on.png) !important;
            background-size: contain;
            width: 34px;
            height: 30px;
            position: relative;
            background-repeat: no-repeat;
            display: inline-block;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            box-sizing: border-box;
            margin-bottom: -8px;
        }

        .text-detail-job-nofixed {
            padding: 20px 0;
        }

        ul.nav-left-custom > li > a {
            font-weight: 400;
        }

        .detail-mobile {
            display: none;
        }

        .sort-infor .row .col-lg-2.col-md-2 {
            padding-right: 5px;
            padding-left: 5px;
        }

        .similar__job1 {
            box-shadow: 4px 3px 9px 1px rgb(0 0 0 / 10%);
            border-radius: 10px;
            display: flex;
            align-items: center;
            min-height: 250px;
        }

        .job-detail-search-btn {
            padding: 0 !important;
        }

        .save-on,
        .save-off {
            margin-left: 10px;
        }

        header .switch-job1 label {
            white-space: nowrap;
        }

        .col-xl-2.col-sm-6.libene {
            white-space: nowrap;
        }

        .skill-tag-title {
            margin: auto;
        }

        .skill-tags .col-xl-2 {
            padding: 0 5px;
        }

        .bgfoo .col-lg-12.col-md-12 {
            margin-top: -1%;
            z-index: -1;
        }

        .item-content_skills {
            margin-top: 0;
        }

        .item-content_skills span {
            padding: 12px 15px;
        }

        @media only screen and (max-width: 1880px) {
            header .tab-job-detail p,
            header .tab-comp-infor p,
            header .tab-comp-job-other p {
                font-size: 25px;
                line-height: 37px;
            }
        }

        @media only screen and (max-width: 1700px) {
            .detail-title__title {
                font-size: 23px;
                line-height: 30px;
            }

            .detail-title__desc {
                font-size: 17px;
                line-height: 20px;
            }

            .detail-title__address {
                font-size: 17px;
                line-height: 20px;
            }

            .detail-title__address img {
                width: 17px;
                height: 22px;
            }

            .detail-title__salary {
                line-height: 30px;
                font-size: 23px;
            }

            .btn-apply, .btn-save {
                height: 70px;
            }

            .btn-apply {
                font-size: 21px;
                line-height: 25px;
            }

            .btn-save {
                font-size: 16px;
                line-height: 29px;
            }

            .save-on,
            .save-off {
                width: 30px;
                height: 26px;
            }

            header .tab-job-detail p,
            header .tab-comp-infor p,
            header .tab-comp-job-other p {
                font-size: 22px;
                line-height: 34px;
            }

            .similar-job-title {
                font-size: 17px;
                line-height: 27px;
            }

            .similar__title {
                font-size: 23px;
                line-height: 30px;
            }

            .similar__company {
                font-size: 17px;
                line-height: 20px;
            }

            .similar__seemore {
                margin-top: 75px;
            }

            .similar__seemore p {
                font-size: 21px;
                line-height: 30px;
            }

        }

        @media only screen and (max-width: 1650px) {
            .benefits-tag img {
                width: 30px;
                height: 40px;
            }

            .libene span {
                font-size: 12px;
            }

            .benefits-tag-title span {
                font-size: 20px;
                line-height: 30px;
            }

            .skill-tag-title span {
                font-size: 20px;
                line-height: 30px;
            }

            .btn-skill {
                font-size: 14px;
            }
        }

        @media only screen and (max-width: 1500px) {
            .detail-title__title {
                font-size: 21px;
                line-height: 28px;
            }

            .detail-title__desc {
                font-size: 15px;
                line-height: 18px;
            }

            .detail-title__address {
                font-size: 15px;
                line-height: 18px;
            }

            .detail-title__address img {
                width: 15px;
                height: 20px;
            }

            .detail-title__salary {
                line-height: 28px;
                font-size: 21px;
            }

            .btn-apply, .btn-save {
                height: 60px;
            }

            .btn-apply {
                font-size: 18px;
                line-height: 22px;
            }

            .btn-save {
                font-size: 13px;
                line-height: 26px;
            }

            .save-on,
            .save-off {
                width: 26px;
                height: 22px;
            }

            header .tab-job-detail p,
            header .tab-comp-infor p,
            header .tab-comp-job-other p {
                font-size: 20px;
                line-height: 32px;
            }

            .sort-infor .row .col-lg-8.col-md-8 {
                font-size: 14px;
            }

            .similar-job-title {
                font-size: 15px;
                line-height: 25px;
            }

            .similar__title {
                font-size: 21px;
                line-height: 28px;
            }

            .similar__company {
                font-size: 15px;
                line-height: 18px;
            }

            .similar__seemore {
                margin-top: 50px;
                margin-bottom: 50px;
            }

            .similar__seemore p {
                font-size: 19px;
                line-height: 28px;
            }

        }

        @media only screen and (max-width: 1395px) {
            header .tab-job-detail p,
            header .tab-comp-infor p,
            header .tab-comp-job-other p {
                font-size: 16px;
                line-height: 28px;
            }

            .lbl p {
                margin-bottom: -10px;
            }

            #job-detail1:checked ~ header .tab-job-detail hr {
                border: 1px solid #4F4F4F;
            }

            #company-infor:checked ~ header .tab-comp-infor hr {
                border: 1px solid #4F4F4F;
            }

            #more-job-company:checked ~ header .tab-comp-job-other hr {
                border: 1px solid #4F4F4F;
            }

            .signup-col {
                transform: translateY(15%);
            }

            .bgfoo span {
                font-size: 250%;
                transform: translate(30%, 60%);
            }

            .similar-job {
                margin-left: 0%;
            }

            .similar-job-title {
                font-size: 13px;
                line-height: 23px;
            }

            .similar__title {
                font-size: 19px;
                line-height: 26px;
            }

            .similar__company {
                font-size: 13px;
                line-height: 16px;
            }

            .similar__seemore {
                margin-top: 25px;
                margin-bottom: 25px;
            }

            .similar__seemore p {
                font-size: 17px;
                line-height: 26px;
            }

            .benefits-tag img {
                width: 25px;
                height: 35px;
            }

            .libene span {
                font-size: 10px;
            }

            .benefits-tag-title span {
                font-size: 18px;
                line-height: 28px;
            }

            .skill-tag-title span {
                font-size: 18px;
                line-height: 28px;
            }

            .btn-skill {
                font-size: 12px;
            }

            .signup-notice span {
                font-size: 40px;
                line-height: 50px;
            }

            .btnsignupnow {
                font-size: 23px;
                line-height: 27px;
            }
        }

        @media only screen and (max-width: 1199px) {
            .btn-save {
                margin-top: 0;
            }

            .btn-apply, .btn-save {
                width: 140px;
                height: 50px;
                margin-left: 0px;
                margin-bottom: 24px;
            }

            .text-detail-job-nofixed {
                padding: 20px 0 0 0;
            }

            .bg-area__flag {
                right: 6%;
            }

            .save-off,
            .save-on {
                margin-bottom: -5px;
            }

            .job-infor header {
                justify-content: center;
            }

            .similar-job-title {
                text-align: center;
            }

            .similar__title {
                font-size: 25px;
                line-height: 32px;
            }

            .similar__company {
                font-size: 19px;
                line-height: 22px;
            }

            .similar__seemore {
                margin-top: 75px;
                margin-bottom: 75px;
            }

            .similar__seemore p {
                font-size: 23px;
                line-height: 32px;
                text-align: center;
            }

            .benefits-tag-title {
                margin-bottom: 25px;
                text-align: center;
            }

            .benefits-tag {
                margin-top: 50px;
            }

            .bg-area__share {
                width: 30px;
                height: 30px;
                top: 30px;
                right: 20px;
            }

            .bg-area__flag {
                width: 22px;
                height: 28px;
                top: 30px;
                right: 70px;
            }

            .benefits-tag img {
                Width: 37.38px;
                Height: 44px;
            }

            .libene span {
                font-size: 14px;
            }

            .benefits-tag-title span {
                font-size: 23px;
                line-height: 32px;
            }

            .skill-tag-title span {
                font-size: 23px;
                line-height: 32px;
            }

            .btn-skill {
                font-size: 16px;
            }

            .skill-tag-title {
                text-align: center;
            }

            .signup-col {
                margin-bottom: 100px;
            }

            .signup-row {
                justify-content: center;
            }

            .signup-notice span {
                text-align: center;
            }

            .bgfoo .col-lg-12.col-md-12 {
                margin-top: -2%;
            }

            .bgfoo span {
                font-size: 200%;
            }

            .signup-now img {
                display: none;
            }
        }

        @media only screen and (max-width: 991px) {
            .similar__title {
                font-size: 23px;
                line-height: 30px;
            }

            .similar__company {
                font-size: 17px;
                line-height: 20px;
            }

            .similar__seemore {
                margin-top: 50px;
                margin-bottom: 50px;
            }

            .similar__seemore p {
                font-size: 21px;
                line-height: 30px;
            }

            .skill-tags .col-xl-2 {
                padding: 15px 30px 0;
            }

            .item-content_skills {
                margin: 0 auto;
                text-align: center;
            }

            .item-content_skills span {
                margin-top: 10px;
            }

            .benefits-tag {
                flex-wrap: wrap;
            }
        }

        @media only screen and (max-width: 890px) {
            .bgfoo span {
                font-size: 150%;
            }
        }

        @media only screen and (max-width: 767px) {
            .detail-mobile {
                display: block;
            }

            .detail-title {
                display: none;
            }

            .row-mobile {
                height: 270px !important;
                margin-bottom: 50px;
            }

            .lbl + .lbl {
                padding-left: 50px;
            }

            .sort-infor {
                margin-top: 0px;
                margin-bottom: 20px;
            }
        }

        @media only screen and (max-width: 670px) {
            .bgfoo span {
                font-size: 100%;
            }
        }

        @media only screen and (max-width: 650px) {
            .lbl + .lbl {
                padding-left: 30px;
            }

            header .tab-job-detail p,
            header .tab-comp-infor p,
            header .tab-comp-job-other p {
                font-size: 12px;
                line-height: 24px;
            }

            .similar__title {
                font-size: 21px;
                line-height: 28px;
            }

            .similar__company {
                font-size: 15px;
                line-height: 18px;
            }

            .similar__seemore p {
                font-size: 19px;
                line-height: 28px;
            }

            .signup-notice span {
                font-size: 30px;
                line-height: 40px;
            }

            .btnsignupnow {
                font-size: 20px;
                line-height: 24px;
            }
        }

        @media only screen and (max-width: 575px) {
            .col-xl-2.col-sm-6.libene {
                text-align: center;
            }

            .skill-tags .col-xl-2 {
                padding: 15px 60px 0;
            }
        }

        @media only screen and (max-width: 500px) {
            .detail-title__title {
                font-size: 19px;
                line-height: 26px;
            }

            .detail-title__desc {
                font-size: 13px;
                line-height: 16px;
            }

            .detail-title__address {
                font-size: 13px;
                line-height: 16px;
            }

            .detail-title__address img {
                width: 13px;
                height: 18px;
            }

            .detail-title__salary {
                line-height: 26px;
                font-size: 19px;
            }

            .btn-apply, .btn-save {
                width: 96%;
                font-size: 12px;
            }

            .save-on,
            .save-off {
                width: 22px;
                height: 18px;
                margin-bottom: -5px;
            }

            .text-detail-job-nofixed {
                padding: 0;
            }

            .sort-infor .row {
                padding-left: 0;
            }

            .sort-infor .row .col-lg-8.col-md-8 {
                font-size: 12px;
                padding: 0 !important;
            }

            .switch-job1 {
                overflow: scroll;
            }
        }

        @media only screen and (max-width: 445px) {
            .bgfoo span {
                font-size: 70%;
            }
        }

        @media only screen and (max-width: 405px) {
            .detail-title__title {
                font-size: 17px;
                line-height: 24px;
            }

            .detail-title__desc {
                font-size: 11px;
                line-height: 14px;
            }

            .detail-title__address {
                font-size: 11px;
                line-height: 14px;
            }

            .detail-title__address img {
                width: 11px;
                height: 16px;
            }

            .detail-title__salary {
                line-height: 24px;
                font-size: 17px;
            }

            .btn-apply, .btn-save {
                width: 90px;
                font-size: 10px;
                height: 40px;
            }

            .btn-save {
                line-height: 15px;
            }

            .save-on,
            .save-off {
                width: 18px;
                height: 14px;
                margin-bottom: -2px;
            }

            .similar__title {
                font-size: 19px;
                line-height: 26px;
            }

            .similar__company {
                font-size: 13px;
                line-height: 16px;
            }

            .similar__seemore {
                margin-top: 25px;
                margin-bottom: 25px;
            }

            .similar__seemore p {
                font-size: 17px;
                line-height: 26px;
            }

            .signup-notice span {
                font-size: 25px;
                line-height: 35px;
            }

            .btnsignupnow {
                font-size: 18px;
                line-height: 22px;
            }
        }

        @media only screen and (max-width: 361px) {
            .save-on, .save-off {
                margin-left: 0px;
            }
        }
        .cominfo-row{
            margin-top: 60px;
        }
        .item-content_skills span {
            border: 0.5px solid #000;
            border-radius: 4px;
            display: inline-block;
            padding: 2px 9px;
            margin-right: 6px;
            margin-bottom: 4px;
        }
        .item-content_skills--link {
            font-size: 11px;
            color: #000;
            line-height: 19px;
        }
        /* section content end */
    </style>
    <div class="search-sec">
        <form action="<?php echo $CFG->wwwroot . '/local/job/search.php' ?>" method="get">
            <div class="col-lg-11 col-md-11" style="margin: 0 auto;">
                <div class="row px-4 px-md-0">
                    <div class="col-lg-4 col-md-12 col-sm-12 p-1 input-wrapper">
                        <img src="img/search.png" alt="search-icon">
                        <input type="text" name="searchtext" class="form-control search-slt search-keyworld" id="search-keyworld"
                               placeholder="<?php echo get_string('searchkeyworld', 'local_job'); ?>">
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                        <img src="img/allcategory.png" alt="search-icon">
                        <select class="form-control search-slt search-category custom-select" id=""  name="skillgroupid">>
                            <option value="0"><?php echo get_string('searchcategories', 'local_job'); ?></option>
                            <?php foreach ($cateories as $c) : ?>
                                <option value="<?php echo $c->id ?>"><?php echo $c->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                        <img src="img/maps.png" alt="search-icon">
                        <select class="form-control search-slt search-location custom-select" id="" name="city">

                            <option value="0"><?php echo get_string('searchlocation', 'local_job'); ?></option>
                            <?php foreach ($districts as $d) : ?>
                                <option value="<?php echo $d->id ?>"><?php echo ($current_language == "ko") ? $d->name_ko : ($current_language == "en") ? $d->name_en : $d->name_vi; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-12 p-1" style="margin: 0 auto">
                        <button type="submit" class="btn btn-danger wrn-btn"><i class="fa fa-search"
                                                                                aria-hidden="true"></i><?php echo get_string('find_now', 'local_job') ?>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="content">
    <div class="bg-area">
        <img class="bg-content" src="img/bgdetail.png" alt="">
        <a href="<?php echo $VISANG->wwwroot; ?>/my/inter_jobs.php"><img src="img/flagbg.png" alt="" class="bg-area__flag"></a>
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $baseurl?>" target="_blank"><img src="img/sharebg.png" alt="" class="bg-area__share"></a>
        <div class="row detail-title" id="detail-job">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-8 col-md-8 col-sm-8 detail">
                <div class="row align-items-center bg-area__detail-height">
                    <div class="col-xl-3 col-md-4">
                        <div class="img">
                            <img src="<?php echo $job->company_logo; ?>" style="height: 100%; width: 100%;"/>
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-8 text-detail-job-nofixed">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <p class="detail-title__title"><?php echo $job->title; ?></p>
                                <p class="detail-title__desc"><?php echo $job->company_name; ?></p>
                                <?php foreach ($job->locations as $location) : ?>
                                    <p class="detail-title__address" id="detail-title__address"><img
                                                src="img/maps2.png"/> <?php echo str_replace("\r\n", "", $job->location->name); ?>
                                    </p>
                                <?php endforeach; ?>
                                <?php if(isloggedin()): ?>
                                    <p class="detail-title__salary" id="detail-title__salary">
                                        <?php echo get_string('expriedday','local_job') ?> : <?php echo date('Y\-m\-d',$job->enddate);?>
                                    </p>
                                    <p class="detail-title__salary" id="detail-title__salary">
                                        <?php echo get_string('loginsalary','local_job') ?> : <?php echo ($job->salary == "Negotiate") ? get_string('employer:Negotiate', 'local_job') : $job->salary; ?>
                                    </p>
                                <?php else :?>
                                    <a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php">
                                        <p class="detail-title__salary" id="detail-title__salary">
                                            <?php echo get_string('logtoview', 'local_job') ?>
                                        </p>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-12" id="bun_app">
                        <div class="row">
                            <div class="col-xl-12 col-md-2"></div>
                            <div class="col-xl-12 col-md-4">
                                <button type="button"
                                        class="btn btn-apply showpop-apply"><?php echo get_string('applynow', 'local_job') ?></button>
                            </div>
                            <div class="col-xl-12 col-md-4">
                                <button type="button" class="btn btn-save" id="saved-job"
                                        onclick="savedJob('.save-icon', '<?php echo $job->id; ?>', 'save-on', 'save-off')"><?php echo get_string('savejob', 'local_job') ?>
                                    <span class="<?php echo ($comid != null) ? "save-none" : ""; ?> save-icon <?php echo mkjobs_has_saved_job($job->id) ? "save-on" : "save-off"; ?>"></span>
                                </button>
                            </div>
                            <div class="col-xl-12 col-md-2"></div>
                        </div>
                    </div>
                    <div class="col-xl-1 col-md-12"></div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
        </div>
    </div>
    <!-- job-infor start -->
    <div class="row row-mobile" style="height: 200px;">
        <div class="detail detail-mobile">
            <div class="row align-items-center bg-area__detail-height">
                <div class="col-4">
                    <div class="img">
                        <img src="<?php echo $job->company_logo; ?>" style="height: 100%; width: 100%;"/>
                    </div>
                </div>
                <div class="col-8 text-detail-job-nofixed">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <p class="detail-title__title"><?php echo $job->title; ?></p>
                            <p class="detail-title__desc"><?php echo $job->company_name; ?></p>
                            <?php foreach ($job->locations as $location) : ?>
                                <p class="detail-title__address" id="detail-title__address"><img
                                            src="img/maps2.png"/> <?php echo str_replace("\r\n", "", $job->location->name); ?>
                                </p>
                            <?php endforeach; ?>
                            <?php if(isloggedin()): ?>
                                <p class="detail-title__salary" id="detail-title__salary">
                                    Ngày hết hạn: <?php echo $job->enddate;?>
                                </p>
                                <p class="detail-title__salary" id="detail-title__salary">
                                    Lương: <?php echo ($job->salary == "Negotiate") ? get_string('employer:Negotiate', 'local_job') : $job->salary; ?>
                                </p>
                            <?php else :?>
                            <a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php">
                                <p class="detail-title__salary" id="detail-title__salary">
                                    <?php echo get_string('logtoview', 'local_job') ?>
                                </p>
                            </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-12" id="bun_app">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-4">
                            <button type="button"
                                    class="btn btn-apply showpop-apply"><?php echo get_string('applynow', 'local_job') ?></button>
                        </div>
                        <div class="col-4">
                            <button type="button" class="btn btn-save" id="saved-job"
                                    onclick="savedJob('.save-icon', '<?php echo $job->id; ?>', 'save-on', 'save-off')"><?php echo get_string('savejob', 'local_job') ?>
                                <span class="<?php echo ($comid != null) ? "save-none" : ""; ?> save-icon <?php echo mkjobs_has_saved_job($job->id) ? "save-on" : "save-off"; ?>"></span>
                            </button>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="job-infor">
    <div class="row">
        <div class="col-xl-1"></div>
        <div class="col-xl-11">
            <input type="radio" name="switch-job1" id="job-detail1" checked>
            <input type="radio" name="switch-job1" id="company-infor">
            <input type="radio" name="switch-job1" id="more-job-company">
            <header>
                <div class="row switch-job1">
                    <label for="job-detail1" class="tab-job-detail lbl">
                        <p><?php echo get_string('jobdetail', 'local_job') ?></p>
                        <hr id="hr-1">
                    </label>
                    <label for="company-infor" class="tab-comp-infor lbl">
                        <p><?php echo get_string('companyinfor', 'local_job') ?></p>
                        <hr style="display: none;" id="hr-2">
                    </label>
                    <label for="more-job-company" class="tab-comp-job-other lbl">
                        <p><?php echo get_string('compmorejob', 'local_job') ?></p>
                        <hr style="display: none;" id="hr-3">
                    </label>
                </div>
            </header>
        </div>
        <div class="col-xl-1"></div>
        <div class="col-xl-7">
            <div class="tab">
                <?php $sg = $DB->get_record('vi_skill_groups', array('id' => $skill_groups));
                echo $sg->name; ?></span>
                <?php $sql = "SELECT wt.*
                                            FROM {vi_job_work_types} jwt
                                            JOIN {vi_work_types} wt ON jwt.work_type_id = wt.id
                                            WHERE jwt.job_id = :jobid";
                $worktypes = $DB->get_records_sql($sql, array('jobid' => $jobid));
                if (sizeof($worktypes) == 0) {
                    $worktypes = $DB->get_records('vi_work_types', array('id' => 1));
                }
                ?>
                <?php $kolevel = $DB->get_record('vi_korean_levels', array('id' => $jobb->korean_level_id)) ?>
                <div class="cards-area">
                    <div class="cards">
                        <!-- detail-tab start -->
                        <div class="row row-1" id="row-1">
                            <div class="col-lg-12 col-md-12">
                                <div class="row map-company">
                                    <div class="col-lg-4 col-md-4 col-12 company-address">
                                        <?php $address = $location->full_address;
                                        echo '<iframe frameborder="0" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . str_replace(",", "", str_replace(" ", "+", $address)) . '&z=14&output=embed" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>';
                                        ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-6 sort-infor">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/age.png" alt=""></div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php echo $jobb->age_from . ' - ' . $jobb->age_to; ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/salary1.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo ($job->salary == "Negotiate") ? get_string('employer:Negotiate', 'local_job') : $job->salary; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/experience.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php $p = $DB->get_record('vi_work_experiences', array('id' => $jobb->work_experience_id)); ?>
                                                <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-6 sort-infor">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/langage.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo $employer->country->name; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/categoty.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php foreach ($worktypes as $p) { ?>
                                                    <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?>
                                                <?php } ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/joblevel.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo ($current_language == "ko") ? $kolevel->name_ko : ($current_language == "en") ? $kolevel->name_en : $kolevel->name_vi ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row job-detail1">
                                    <div class="col-lg-12 col-md-12 jobdetail1">
                                        <?php
                                        $sql = "SELECT r.name FROM {vi_reason_categories} r 
                                JOIN {vi_employer_reasons} er ON r.id=er.reason_category_id	
                                WHERE er.employer_id=:comid";
                                        $items = $DB->get_records_sql($sql, array("comid" => $jobb->employer_id));
                                        ?>
                                        <?php if ($jobb->description != null) : ?>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <h2>
                                                    <?php echo get_string('Responsibilities', 'local_job'); ?>
                                                </h2>
                                                <?php
                                                $p = $DB->get_record('vi_final_educations', array('id' => $jobb->final_education_id));
                                                ?>
                                                <div class="contentj">
                                                    <?php echo $jobb->description; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php if ($jobb->final_education_id != 0) : ?>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">

                                                    <h2>
                                                        <?php echo get_string('c_Eligibility', 'local_job'); ?>
                                                    </h2>
                                                    <?php
                                                    $p = $DB->get_record('vi_final_educations', array('id' => $jobb->final_education_id));
                                                    ?>
                                                    <div class="contentj">
                                                        <p>- <?php echo get_string('c_FinalEducation', 'local_job'); ?>
                                                            : <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi ?></p>
                                                        <?php echo $jobb->other_request_info; ?>
                                                        <?php if ($jobb->korean_level_id != 0) : ?>
                                                            <p>- <?php echo get_string('KoreanLevel', 'local_job'); ?>
                                                                : <?php echo ($current_language == "ko") ? $kolevel->name_ko : ($current_language == "en") ? $kolevel->name_en : $kolevel->name_vi ?></p>
                                                        <?php endif; ?>
                                                        <?php echo $jobb->other_lang_info; ?>
                                                    </div>

                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php if($company->reason_description!=null) : ?>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <h2>
                                                    <?php echo get_string('Benefits', 'local_job'); ?>
                                                </h2>
                                                <div class="contentj">
                                                    <?php echo $company->reason_description; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- detail-tab end -->
                        <!-- company-tab start -->
                        <div class="row row-2" id="row-2" style="display: none;">
                            <div class="col-lg-12 col-md-12">
                                <div class="row map-company">
                                    <div class="col-lg-4 col-md-4 company-address">
                                        <?php $address = $location->full_address;
                                        echo '<iframe frameborder="0" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . str_replace(",", "", str_replace(" ", "+", $address)) . '&z=14&output=embed" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>';
                                        ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-6 sort-infor">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/age.png" alt=""></div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php echo $jobb->age_from . ' - ' . $jobb->age_to; ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/salary1.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo ($job->salary == "Negotiate") ? get_string('employer:Negotiate', 'local_job') : $job->salary; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/experience.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php $p = $DB->get_record('vi_work_experiences', array('id' => $jobb->work_experience_id)); ?>
                                                <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-6 sort-infor">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/langage.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo $employer->country->name; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/categoty.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php foreach ($worktypes as $p) { ?>
                                                    <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?>
                                                <?php } ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/joblevel.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo ($current_language == "ko") ? $kolevel->name_ko : ($current_language == "en") ? $kolevel->name_en : $kolevel->name_vi ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row job-detail1">
                                    <div class="col-lg-12 col-md-12 jobdetail">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <h2>
                                                    <?php echo $employer->company_name; ?>
                                                </h2>
                                                <div class="contentj">
                                                    <p>
                                                    <div class="row">
                                                        <div class="col-lg-1 col-md-1 col-1"><i
                                                                    class="fas fa-map-marked-alt"></i>
                                                        </div>
                                                        <div class="col-lg-10 col-md-10 col-10"><?php echo $employer->company_address; ?></div>
                                                    </div>
                                                    </p>
                                                    <p>
                                                    <div class="row">
                                                        <div class="col-lg-1 col-md-1 col-1"><i
                                                                    class="fas fa-building"></i>
                                                        </div>
                                                        <div class="col-lg-10 col-md-10 col-10"><?php echo $employer->company_type->name; ?></div>
                                                    </div>

                                                    </p>
                                                    <p>
                                                    <div class="row">
                                                        <div class="col-lg-1 col-md-1 col-1"><i
                                                                    class="fas fa-users"></i>
                                                        </div>
                                                        <div class="col-lg-10 col-md-10 col-10"><?php echo $employer->company_size->name; ?></div>
                                                    </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row cominfo-row">
                                            <div class="col-lg-12 col-md-12">
                                                <h2>
                                                    <?php echo get_string('employer:navtab:info', 'local_job'); ?>
                                                </h2>
                                                <div class="contentj">
                                                    <?php if ($employer->company_short_description != '') : ?>
                                                        <p>
                                                            <?php echo $employer->company_short_description; ?>
                                                        </p>
                                                    <?php endif; ?>
                                                    <?php if ($employer->company_description != '') : ?>
                                                        <p><?php echo $employer->company_description; ?></p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- company-tab end -->
                        <!--morejob-tab start -->
                        <div class="row row-3" id="row-3" style="display: none;">
                            <div class="col-lg-12 col-md-12">
                                <div class="row map-company">
                                    <div class="col-lg-4 col-md-4 company-address">
                                        <?php $address = $location->full_address;
                                        echo '<iframe frameborder="0" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . str_replace(",", "", str_replace(" ", "+", $address)) . '&z=14&output=embed" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>';
                                        ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-6 sort-infor">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/age.png" alt=""></div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php echo $jobb->age_from . ' - ' . $jobb->age_to; ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/salary1.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo ($job->salary == "Negotiate") ? get_string('employer:Negotiate', 'local_job') : $job->salary; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/experience.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8">
                                                <?php $p = $DB->get_record('vi_work_experiences', array('id' => $jobb->work_experience_id)); ?>
                                                <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-6 sort-infor">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/langage.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo $employer->country->name; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/categoty.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php foreach ($worktypes as $p) { ?>
                                                    <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?>
                                                <?php } ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"><img src="img/joblevel.png" alt="">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-8"><?php echo ($current_language == "ko") ? $kolevel->name_ko : ($current_language == "en") ? $kolevel->name_en : $kolevel->name_vi ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row job-detail1">
                                    <div class="col-lg-12 col-md-12 jobdetail">
                                        <?php
                                        $sql = "SELECT r.name FROM {vi_reason_categories} r 
                                JOIN {vi_employer_reasons} er ON r.id=er.reason_category_id	
                                WHERE er.employer_id=:comid";
                                        $items = $DB->get_records_sql($sql, array("comid" => $jobb->employer_id));
                                        ?>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <h2>
                                                    <?php if (count($employer->jobs) > 0) { ?>
                                                        <div class="pg-tit" id="our-jobs">
                                                            <?php echo get_string('Jobsprogress', 'local_job'); ?>
                                                        </div>
                                                    <?php } ?>
                                                </h2>

                                                <div class="contentj">
                                                    <?php foreach ($employer->jobs as $joba) : ?>
                                                        <div class="row similar__job1">
                                                            <div class="col-xl-12 col-lg-2 col-md-1 col-12"></div>
                                                            <div class="col-xl-4 col-lg-2 col-md-3 col-4">
                                                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $joba->id)); ?>">
                                                                    <img style="width: 100%; height: 100%; object-fit: contain;"
                                                                         src="<?php echo $joba->company_logo; ?>">
                                                                </a>
                                                            </div>
                                                            <div class="col-xl-8 col-lg-6 col-md-7 col-8">
                                                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $joba->id)); ?>">
                                                                    <p class="similar__title similar-title-oneline"> <?php echo $joba->title; ?></p>
                                                                </a>
                                                                <p class="similar__company"><?php echo $joba->company_name; ?></p>
                                                            </div>
                                                            <div class="col-xl-12 col-lg-2 col-md-1 col-12"></div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- morejob-tab end -->
                    </div>
                </div>
            </div>
        </div>

        <?php
        $recommend_jobs = mkjobs_get_job_recommend_by_skills_all($job);
        ?>
        <div class="col-xl-3 similar-job">
            <div class="row">
                <div class="col-xl-8 similar-job-title">
                    <span><?php echo get_string('similarjob', 'local_job'); ?></span>
                </div>
                <div class="col-xl-4"></div>
                <?php foreach ($recommend_jobs as $recommend_job) : ?>
                    <div class="row similar__job">
                        <div class="col-xl-12 col-lg-2 col-md-1 col-12"></div>
                        <div class="col-xl-4 col-lg-2 col-md-3 col-4">
                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $recommend_job->id, 'employerid' => $recommend_job->employer_id)); ?>">
                                <img style="width: 100%; height: 100%; object-fit: contain;"
                                     src="<?php echo $recommend_job->company_logo; ?>">
                            </a>
                        </div>
                        <div class="col-xl-8 col-lg-6 col-md-7 col-8">
                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $recommend_job->id, 'employerid' => $recommend_job->employer_id)); ?>">
                                <p class="similar__title"> <?php echo $recommend_job->title; ?></p>
                            </a>
                            <p class="similar__company"><?php echo $recommend_job->company_name; ?></p>
                        </div>
                        <div class="col-xl-12 col-lg-2 col-md-1 col-12"></div>
                    </div>
                <?php endforeach; ?>

            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-2 col-md-1"></div>
                <div class="col-xl-12 col-lg-8 col-md-10 similar__seemore">
                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php'); ?>">
                        <p> <?php echo get_string('seemore', 'local_job') ?> <i class="fas fa-chevron-right"></i></p>
                    </a>
                </div>
                <div class="col-xl-12 col-lg-2 col-md-1"></div>
            </div>
        </div>
        <div class="col-xl-1"></div>
    </div>
    <div class="row">
        <div class="col-xl-1"></div>
        <div class="col-xl-7">
            <div class="row benefits-tag">
                <div class="col-lg-auto col-12 benefits-tag-title">
                  <span class="">
                     <?php echo get_string('benefits', 'local_job') ?>
                  </span>
                </div>
                    <?php foreach ($benefit_types as $p) : ?>
                        <div class="col-lg-auto col-md-4 col-sm-6 libene">
                            <img class="benefit-icon-<?php echo $p->id ?>" src="img/insurance.png">
                            <span><?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; ?></span>
                        </div>
                    <?php endforeach; ?>
            </div>
            <!-- job-infor end -->
        </div>
        <div class="col-xl-4">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-1"></div>
        <div class="col-xl-7">
            <div class="row skill-tag">
                <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 skill-tag-title">
                  <span class="">
                     <?php echo get_string('skilltags', 'local_job') ?>
                  </span>
                </div>
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 skill-tags">
                    <div class="row">
                        <div class="item-content_skills">
                            <?php foreach ($job->skills as $skill) : ?>
                                <span>
                                            <a class="item-content_skills--link"
                                               href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                                <?php echo $skill->name; ?>
                                            </a>
                                        </span>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- job-infor end -->
        </div>
        <div class="col-xl-4">
        </div>
    </div>
    <div class="row signup-now">
        <div class="col-xl-1">
        </div>
        <div class="col-xl-5 col-sm-10 signup-col">
            <div class="row signup-row">
                <hr/>
            </div>
            <div class="row signup-notice">
                <span><?php echo get_string('signupalert', 'local_job') ?></span>
            </div>
            <div class="row signup-row">
                <?php if(isloggedin()) :?>
                    <a href="<?php echo $CFG->wwwroot; ?>/local/job/my/info.php"
                       class="btn btnsignupnow"><?php echo get_string('signupnow', 'local_job') ?></a>
                  <?php else: ?>
                    <a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php"
                       class="btn btnsignupnow"><?php echo get_string('signupnow', 'local_job') ?></a>
                  <?php endif;  ?>
            </div>
        </div>
        <div class="col-xl-6 col-12">
            <img src="img/signupnow.png" style="width: 100%; height: 100%;"/>
        </div>
    </div>
<!--    <div class="row bgfoo">-->
<!--        <div class="col-lg-12 col-md-12">-->
<!--            <span>-->
<!--               --><?php //echo get_string('sologan', 'local_job') ?>
<!--            </span>-->
<!--            <img src="img/bgfoo.png" style="width: 100%; height: 100%;">-->
<!--        </div>-->
<!--    </div>-->
    <!--custom js start-->
    <script type="text/javascript">
        function savedJob(el, id, classon, classoff) {
            $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkjobs_saved_job",
                jobid: id
            }, function (data) {
                if (data == -1) {
                    if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                    }
                } else if (data == 1) {
                    $(el).removeClass(classoff || "save-off");
                    $(el).addClass(classon || "save-on");
                } else {
                    $(el).removeClass(classon || "save-on");
                    $(el).addClass(classoff || "save-off");
                }
            });
        }
    </script>
    <script>
        $(function () {
            $(".btn-search").click(function () {
                let keyworld = $("#seakey").val();
                let cateid = $("#seacategories").find(":selected").val();
                let locaid = $("#sealocation").find(":selected").val();
                window.location.href = "<?php echo $VISANG->wwwroot . '/search.php?keyworld';?>=&cateid=&locaid='; ?>";
            });
        });
        $(function () {
            $(".showpop-apply").click(function () {
                <?php if (isloggedin()) : ?>
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/jobs/pop_resume_submit.php' ?>", {
                    "width": "600px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {
                    "jobid": "<?php echo $job->id; ?>"
                });
                <?php else : ?>
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
                <?php endif; ?>
                return false;
            });
        });
        $(document).ready(function () {
            $('input[name="switch-job1"]').change(function () {
                if ($('#job-detail1').prop('checked')) {
                    document.getElementById("row-1").style.display = "";
                    document.getElementById("row-2").style.display = "none";
                    document.getElementById("row-3").style.display = "none";
                    document.getElementById("hr-1").style.display = "";
                    document.getElementById("hr-2").style.display = "none";
                    document.getElementById("hr-3").style.display = "none";
                }
                if ($('#company-infor').prop('checked')) {
                    document.getElementById("row-1").style.display = "none";
                    document.getElementById("row-2").style.display = "";
                    document.getElementById("row-3").style.display = "none";
                    document.getElementById("hr-1").style.display = "none";
                    document.getElementById("hr-2").style.display = "";
                    document.getElementById("hr-3").style.display = "none";
                }
                if ($('#more-job-company').prop('checked')) {
                    document.getElementById("row-1").style.display = "none";
                    document.getElementById("row-2").style.display = "none";
                    document.getElementById("row-3").style.display = "";
                    document.getElementById("hr-1").style.display = "none";
                    document.getElementById("hr-2").style.display = "none";
                    document.getElementById("hr-3").style.display = "";
                }
            });
        });
        $(function () {
            $(".benefit-icon-1").attr('src','img/allowances.png');
            $(".benefit-icon-2").attr('src','img/incentive_bonus.png');
            $(".benefit-icon-4").attr('src','img/insurance.png');
            $(".benefit-icon-5").attr('src','img/incentive_bonus.png');
            $(".benefit-icon-6").attr('src','img/insurance.png');
            $(".benefit-icon-7").attr('src','img/salary_review.png');
            $(".benefit-icon-8").attr('src','img/travel.png');
        });

    </script>
    <script type="text/javascript">
        $(document).ready(showPosition());

        function setDefaultSearchCategory() {
            let categoryBlock = document.querySelector(".search-category");
            let defaultCategory = 'Biên Dịch / Phiên Dịch';
            Array.prototype.forEach.call(categoryBlock.options, function (option, index) {
                if (option.innerText === defaultCategory) {
                    categoryBlock.value = option.attributes.value.value
                }
            });
        }

        function setDefaultPosition() {
            let addressiplocations = [];
            let locationSelector = document.querySelector(".search-location");
            let companylocation = 'Hà Nội';
            <?php foreach ($districts as $district) : ?>
            addressiplocations.push({id: <?php echo $district->id; ?>, name: '<?php echo $district->name; ?>'});
            <?php endforeach; ?>
            addressiplocations.forEach((addressiplocation) => {
                if (addressiplocation.name === companylocation) {
                    locationSelector.value = addressiplocation.id
                }
            })
        }

        function showPosition() {
            setDefaultSearchCategory();
            setDefaultPosition();
            let url = 'https://ipinfo.io/json';
            $.getJSON(url, function (data) {
                let addressiplocations = [];
                let namecity = '';
                let companylocation = removeVietnameseTones(data.city);
                <?php foreach ($districts as $district) : ?>
                namecity = removeVietnameseTones('<?php echo $district->name; ?>')
                addressiplocations.push({id: <?php echo $district->id; ?>, name: namecity});
                <?php endforeach; ?>
                let searchkeyworld= document.getElementById("search-keyworld");
                let categorySelector = document.querySelector(".search-category");
                let locationSelector = document.querySelector(".search-location");
                addressiplocations.forEach((addressiplocation) => {
                    if (addressiplocation.name === companylocation) {
                        locationSelector.value = addressiplocation.id
                    }
                })
                let keyworld='<?php echo !isset($_COOKIE["searchtext"])? '':$_COOKIE["searchtext"]; ?>';
                let cateid=<?php echo !isset($_COOKIE["searchcate"]) ? 71 :$_COOKIE["searchcate"]; ?>;
                let posiid;
                <?php if(!isset($_COOKIE["searchloca"])): ?>
                posiid=addressiplocation.id;
                <?php else: ?>
                posiid=<?php echo $_COOKIE["searchloca"];?>
                <?php endif; ?>


                if(cateid!==null){
                    categorySelector.value=cateid;
                }
                if(posiid!==null){
                    locationSelector.value=posiid;
                }
                if(keyworld!==''){
                    searchkeyworld.value=keyworld;
                }
            });

            function removeVietnameseTones(str) {
                str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                str = str.replace(/đ/g, "d");
                str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
                str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
                str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
                str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
                str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
                str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
                str = str.replace(/Đ/g, "D");
                // Some system encode vietnamese combining accent as individual utf-8 characters
                // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
                str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
                str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
                // Remove extra spaces
                // Bỏ các khoảng trắng liền nhau
                str = str.replace(/ + /g, " ");
                str = str.trim();
                // Remove punctuations
                str = str.toLowerCase()
                str = str.split(' ').join('');
                // Bỏ dấu câu, kí tự đặc biệt
                str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
                str = str.replace("city", "");
                str = str.replace("thanhpho", "");
                str = str.replace("tp", "");
                return str;
            }
        }
    </script>
    <!--custom js end-->
<?php
$VISANG->theme->footer();
