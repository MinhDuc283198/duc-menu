<?php

function get_mycoupons()
{
    global $DB, $USER;
    $sql_where = "SELECT cr.coupon_id FROM {vi_coupons_receivers} cr WHERE cr.user_id = $USER->id";
//    $coupon_receivers = $DB->get_record_sql("SELECT cr.coupon_id FROM {vi_coupons_receivers} cr WHERE cr.user_id = $USER->id");
    $mycoupons = $DB->get_records_select("vi_coupons", "id IN ($sql_where)");
//    $mycoupons = $DB->get_records_select('vi_coupons', 'id = 1');
    return $mycoupons;
}
function get_coupon_by_userid()
{
    global $DB, $USER;
    $sql="SELECT cr.* FROM {vi_coupons_receivers} cr  WHERE cr.user_id= $USER->id";
    $result=$DB->get_records_sql($sql);
    return $result;
}
function count_coupon_by_userid()
{
    global $DB, $USER;
    $sql="SELECT count(*) FROM {vi_coupons_receivers} cr  WHERE cr.user_id= $USER->id ";
    $result=$DB->count_records_sql($sql);
    return $result;
}
function get_coupon_by_id($couponid){
    global $DB, $USER;
    $sql="SELECT vc.* FROM {vi_coupons} vc  WHERE vc.id= $couponid";
    $result=$DB->get_record_sql($sql);
    return $result;
}
function get_course_by_id($id){
    global $DB, $USER;
    $sql="SELECT co.* FROM {lmsdata_course} co  WHERE co.id= $id";
    $result=$DB->get_record_sql($sql);
    return $result;
}
function get_course_coupon_by_id($id){
    global $DB, $USER;
    $sql="SELECT vcc.* FROM {vi_coupons_courses} vcc  WHERE vcc.coupon_id= $id";
    $result=$DB->get_record_sql($sql);
    return $result;
}
function count_coupon_by_userid_search($searchtext){
    global $DB, $USER;
    $sql_select = "FROM {vi_coupons_receivers} vcr
                    LEFT JOIN {vi_coupons} vc ON vcr.coupon_id = vc.id 
                    LEFT JOIN {vi_coupons_courses} vcc ON vcc.coupon_id = vc.id 
                    LEFT JOIN {course} co ON vcc.course_id = co.id
                    ";
    $sql = array();
    $params = array();
    $sql[]=$DB->sql_like('vc.title',':title',false);
    $params['title']='%'.$searchtext.'%';
    $sql[]=$DB->sql_like('co.fullname',':fullname',false);
    $params['fullname']='%'.$searchtext.'%';
    $sql[]=$DB->sql_like('vc.key_code',':key_code',false);
    $params['key_code']='%'.$searchtext.'%';
    $sql_str = implode(" OR ", $sql)." AND vcr.user_id=$USER->id";
    $totalitem = $DB->count_records_sql("SELECT COUNT(DISTINCT vcr.id) $sql_select WHERE $sql_str", $params);

    return $totalitem;
}

function search_coupon_by_userid($searchtext){
    global $DB, $USER;
    $sql_select = "FROM {vi_coupons_receivers} vcr
                    LEFT JOIN {vi_coupons} vc ON vcr.coupon_id = vc.id 
                    LEFT JOIN {vi_coupons_courses} vcc ON vcc.coupon_id = vc.id 
                    LEFT JOIN {course} co ON vcc.course_id = co.id
                    ";
    $sql = array();
    $params = array();
    $sql[]=$DB->sql_like('vc.title',':title',false);
    $params['title']='%'.$searchtext.'%';
    $sql[]=$DB->sql_like('co.fullname',':fullname',false);
    $params['fullname']='%'.$searchtext.'%';
    $sql[]=$DB->sql_like('vc.key_code',':key_code',false);
    $params['key_code']='%'.$searchtext.'%';
    $sql_str = implode(" OR ", $sql)." AND vcr.user_id=$USER->id";
    $coupons = $DB->get_records_sql("SELECT vcr.* $sql_select WHERE $sql_str", $params);

    return $coupons;
}
function remove_mycoupon($id)
{
    global $DB, $USER;
    $DB->delete_records("vi_coupons_receivers", array("id"=>$id));
}