<?php

// Standard GPL and phpdocs
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once(__DIR__ . '/couponlib.php');


// Set up the page.
//$title = get_string('pluginname', 'local_job');
//$pagetitle = $title;
$pagenum=optional_param('pagenum',1,PARAM_INT);
$perpage=optional_param('perpage',5,PARAM_INT);
$searchtext=optional_param(searchtext,'',PARAM_RAW_TRIMMED);
$url = new moodle_url("/local/job/coupon/mycoupons.php");
$PAGE->set_url($url);
$PAGE->set_title('My coupons');
$PAGE->set_heading('My coupons');
$PAGE->set_pagelayout('coupon');
require_login();

$output = $PAGE->get_renderer('local_job');

echo $output->header();



//$my_coupons = get_mycoupons();

$templatecontext = (object)[
    'coupon_title'=> get_string('cp_mycoupons','local_job'),
    'pagenum'=>$pagenum,
    'perpage'=>$perpage,
    'baseurl'=>$url,
    'searchtext'=>$searchtext,
];

$renderable = new \local_job\output\my_coupon_page($templatecontext);
echo $output->render($renderable);

echo $output->footer();

