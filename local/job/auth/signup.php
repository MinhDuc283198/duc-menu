<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $SESSION;

// ===================================================================================================
// handles

// ===================================================================================================
// renders
$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->title = '';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div role="main">
            <div class="login-bx position-relative top-0 left-0 my-0 mx-auto">
                <ul class="login-tab tab-event">
                    <li><a href="<?php echo $VISANG->wwwroot . '/auth/login.php'; ?>"><?php echo get_string('login', 'theme_oklassedu') ?></a></li>
                    <li class="on"><a href="<?php echo $VISANG->wwwroot . '/auth/signup.php'; ?>"><?php echo get_string('signup', 'theme_oklassedu') ?></a></li>
                </ul>
                <div class="target-area">
                    <div class="box02 on">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($errormsg != '') : ?>
    <script>
        $(document).ready(function() {
            alert("<?php echo $errormsg; ?>");
        });
    </script>
<?php endif; ?>
<?php
$VISANG->theme->footer();
