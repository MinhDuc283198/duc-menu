<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $SESSION, $SITE, $PAGE;

// ===================================================================================================
// handles
$sesskey = optional_param('sesskey', '__notpresent__', PARAM_RAW);
$urltogo = optional_param('urltogo', '', PARAM_RAW);

$redirect = $VISANG->wwwroot . '/';
if ($urltogo != '') {
    $redirect = $urltogo;
}

if (!isloggedin()) {
    // no confirmation, user has already logged out
    require_logout();
    redirect($redirect);
} else if (!confirm_sesskey($sesskey)) {
    $VISANG->theme->title = get_string('logoutconfirm');
    $VISANG->theme->header();
    echo '<div class="cont">
            <div class="group">
                <div role="main">
                    <div class="text-center">
                        <div class="h3 mb-4">' . get_string('logoutconfirm') . '</div>
                    ' . html_writer::link(new moodle_url($VISANG->wwwroot . '/auth/logout.php', array('sesskey' => sesskey())), get_string('continue'), array('class' => 'btn btn-primary')) . '
                    ' . html_writer::link($VISANG->wwwroot, get_string('cancel'), array('class' => 'btn btn-outline-primary')) . '
                    </div>
                </div>
            </div>
        </div>';
    $VISANG->theme->footer();
    die;
}

$authsequence = get_enabled_auth_plugins(); // auths, in sequence
foreach ($authsequence as $authname) {
    $authplugin = get_auth_plugin($authname);
    $authplugin->logoutpage_hook();
}
require_logout();
redirect($redirect);

?>
<script>
    //autologin 제거를 위해 처리
    var token = localStorage.getItem('autologinval');
    if (token) {
        localStorage.removeItem('autologinval');
    }
    location.href = '<?php echo $redirect ?>';
</script>