<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $SESSION;

// ===================================================================================================
// handles
if (isloggedin()) {
    redirect($VISANG->wwwroot);
}

$firstname = optional_param('firstname', 0, PARAM_RAW);
// $phone1 = optional_param('p1', 0, PARAM_RAW) . optional_param('p2', 0, PARAM_RAW) . optional_param('p3', 0, PARAM_RAW);
$phone1 = optional_param('p1', 0, PARAM_RAW);
$phone2 = optional_param('year', 0, PARAM_RAW) . "-" . optional_param('month', 0, PARAM_RAW) . "-" . optional_param('days', 0, PARAM_RAW);
$userdata = $DB->get_record("user", array('firstname' => $firstname, 'phone1' => $phone1, 'phone2' => $phone2, 'confirmed' => 1, 'deleted' => 0));

// ===================================================================================================
// renders
$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->title = get_string("findmyaccount", "local_signup");
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div class="login-bx position-relative top-0 left-0 my-0 mx-auto">
            <div class="bg bg-area">
                <div class="center-tit"><?php echo get_string("emailcertified", "local_job") ?></div>
                <p class="center-txt"><?php echo get_string("emailfindtext", "local_signup") ?></p>
            </div>
            <div class="fd-u-info">
                <?php if ($userdata) { ?>
                    <div>
                        <strong><?php echo get_string("username", "core") ?></strong>
                        <span><img src="/theme/oklassedu/pix/images/icon_mail.png" alt="" /><?php echo $userdata->email ?></span>
                    </div>
                    <div>
                        <strong><?php echo get_string("password", "core") ?></strong>
                        <a href="#" onclick="send_email(<?php echo $userdata->id ?>)" class="btns br"><?php echo get_string("recivetemppass", "local_signup") ?></a>
                        <p class="warning"><?php echo get_string("chanepass", "local_signup") ?></p>
                    </div>
                <?php } else { ?>
                    <div>
                        <strong><?php echo get_string("username", "core") ?></strong>
                        <a href="<?php echo $VISANG->wwwroot . "/auth/login.php" ?>" class="btns br"><?php echo get_string("notaccount", "local_signup") ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    function send_email(userid) {
        openwaitpopup();
        $.ajax({
            url: "/local/signup/send_tempmail.php",
            type: 'post',
            dataType: 'json',
            data: {
                userid: userid,
                redirecttosite: 'jobs'
            },
            success: function(result) {
                closewaitpopup();
                alert(result.text);
                location.href = "<?php echo $VISANG->wwwroot ?>";
            }
        });
    }
</script>
<?php
$VISANG->theme->footer();
?>






<div class="d-none" id="popup_wait" style="width:350px; height:140px; background: #fff;border: 1px solid #ccc;position: fixed;top: 50%;left: 50%; -webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);">
    <div class="pop-title" style="background: #485cc7;">
        <a href="#" onclick="closewaitpopup();"></a><a href="#" class="pop-close" onclick="closewaitpopup();">닫기</a>
    </div>
    <div class="pop-contents" style=" height:80px;text-align: center; ">
        <p><?php echo get_string("WaitPass", "local_job") ?></p>
    </div>
</div>

<script>
    var closewaitpopup = function() {
        $("#popup_wait").addClass("d-none");
        $("#popup_wait").removeClass("d-block");
    }

    var openwaitpopup = function() {
        $("#popup_wait").removeClass("d-none");
        $("#popup_wait").addClass("d-block");
    }
</script>