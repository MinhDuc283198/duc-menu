<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');
require_once($CFG->dirroot.'/login/lib.php');
global $CFG, $VISANG, $DB, $USER, $SESSION;

// ===================================================================================================
// handles
$urltogo = optional_param('urltogo', '', PARAM_RAW);
if ($urltogo != '') {
    $SESSION->wantsurl = $urltogo;
}

$urltogo = $VISANG->wwwroot;
if (isloggedin()) {
    if (isset($SESSION->wantsurl)) {
        $urltogo = $SESSION->wantsurl;
        unset($SESSION->wantsurl);
    }
    redirect($urltogo);
}

// google facebook 2020 02 28 
$authsequence = get_enabled_auth_plugins(true); // auths, in sequence
foreach($authsequence as $authname) {
    $authplugin = get_auth_plugin($authname);
    $authplugin->loginpage_hook();
}
$potentialidps = array();
foreach($authsequence as $authname) {
    $authplugin = get_auth_plugin($authname);
    $potentialidps = array_merge($potentialidps, $authplugin->loginpage_idp_list($SESSION->wantsurl));
}
$firstname = optional_param('firstname', '', PARAM_RAW); // 20.02.12 user firstname by googleouath2 
$email = optional_param('id', '', PARAM_RAW); // 20.02.12 user email by googleouath2 
$uid = optional_param('uid', '', PARAM_RAW);
$username = optional_param('username', '', PARAM_RAW); 
$auth = optional_param('auth', '', PARAM_RAW);
// google facebook 2020 02 28 

$frm = data_submitted();
$errormsg = '';
if ($frm) {
    $username = core_user::clean_field($frm->username, 'username');
    $password = trim($frm->password);

    if (!empty($username) && !empty($password)) {
        require_once($CFG->dirroot . '/user/lib.php');

        $rememberusername = optional_param('rememberusername', 0, PARAM_INT);

        $errorcode = 0;
        $user = authenticate_user_login($username, $password, false, $errorcode);
        if ($user) {
            // language setup
            if (isguestuser($user)) {
                // no predefined language for guests - use existing session or default site lang
                unset($user->lang);
            } else if (!empty($user->lang)) {
                // unset previous session language - use user preference instead
                unset($SESSION->lang);
            }

            if (empty($user->confirmed)) {
                redirect($VISANG->wwwroot . "/auth/successsignup.php?id=$user->id");
                die;
            }
        }
        switch ($errorcode) {
            case AUTH_LOGIN_OK:
                complete_user_login($user);
                if (isset($SESSION->wantsurl)) {
                    $urltogo = $SESSION->wantsurl;
                    unset($SESSION->wantsurl);
                }

                if (empty($CFG->rememberusername) or ($CFG->rememberusername == 2 and $rememberusername == 0)) {
                    set_moodle_cookie('');
                } else {
                    set_moodle_cookie($user->username);
                }

                redirect($urltogo);
                break;
            case AUTH_LOGIN_UNAUTHORISED:
                $errormsg = get_string("unauthorisedlogin", "", $username);
                break;
            default:
                $errormsg = get_string("invalidlogin","local_signup");
                break;
        }
    }
}

$rememberusername = get_moodle_cookie();

// ===================================================================================================
// renders
$loginsite = get_string("loginsite");
$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->title = $loginsite;
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div role="main">
            <div class="login-bx position-relative top-0 left-0 my-0 mx-auto">
                <ul class="login-tab tab-event">
                    <li data-target=".box01" id="logintab" <?php if($auth==''){ echo 'class="on"';} ?>><a href="#"><?php echo get_string('login', 'theme_oklassedu') ?></a></li>
                    <li data-target=".box02" id="signuptab" <?php if($auth!=''){ echo 'class="on"';} ?> ><a href="#"><?php echo get_string('signup', 'theme_oklassedu') ?></a></li>
                </ul>
                <div class="target-area">
                    <div class="box01 <?php if($auth==''){ echo 'on';} ?>">
                        <form class="email-bx login" action="<?php echo $VISANG->wwwroot; ?>/auth/login.php" method="post" id="login1">
                            <p>
                                <input required type="text" class="w100" id="username" name="username" value="<?php echo $rememberusername; ?>" title="<?php echo get_string('LoginName', 'local_job'); ?>" placeholder="<?php echo get_string('LoginName', 'local_job'); ?>" />
                            </p>
                            <p>
                                <div class="pass-input" style="position: relative; width: 100%">
                                    <input require type="password" id="password" name="password" title="<?php echo get_string('LoginPass', 'local_job'); ?>" placeholder="<?php echo get_string('LoginPass', 'local_job'); ?>"/>
                                    <span class="fas fa-eye " id="btnShowpass"></span>
                                </div>
                            <style>
                                #password{
                                    width: 100%;
                                    float: none;
                                    height: 55px;
                                    font-size: 18px;
                                }
                                .pass-input span{
                                    position: absolute;
                                    z-index: 100;
                                    right: 20px;
                                    top: 20px;
                                }

                            </style>
                            <script>
                                const ipnElement = document.querySelector('#password')
                                const btnElement = document.querySelector('#btnShowpass')
                                btnElement.addEventListener('click', function() {
                                    const currentType = ipnElement.getAttribute('type')
                                    if(currentType==='password'){
                                        btnElement.classList.remove('fa-eye');
                                        btnElement.classList.add('fa-eye-slash');
                                    } else {
                                        btnElement.classList.remove('fa-eye-slash');
                                        btnElement.classList.add('fa-eye');
                                    }
                                    ipnElement.setAttribute(
                                        'type',
                                        currentType === 'password' ? 'text' : 'password'
                                    )
                                })
                            </script>
                            </p>
                            <?php if (isset($CFG->rememberusername) and $CFG->rememberusername == 2) : ?>
                                <p>
                                    <div class="rememberpass">
                                        <label for="rememberusername" class="custom-ck">
                                            <input type="checkbox" name="rememberusername" id="rememberusername" value="1" <?php echo empty($rememberusername) ? '' : 'checked'; ?> />
                                            <span></span>
                                            <strong><?php print_string('rememberusername', 'local_job') ?></strong>
                                        </label>
                                    </div>
                                </p>
                            <?php endif; ?>
                            <div class="btns-area text-center">
                                <input type="submit" id="loginbtn1" class="btns point big" value="<?php echo get_string('login', 'theme_oklassedu') ?>" />
                            </div>
                            <p class="find-id"><a href="<?php echo $VISANG->wwwroot . '/auth/findaccount.php'; ?>"><?php echo get_string("findmyaccount", "local_signup") ?></a></p>
                        </form>
                        <div class="sns-area">
                            <a href="oauth2login.php?providername=facebook" class="ic-fb">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_facebook_big.png"  alt="facebook" /></span>
                                <span><?php echo get_string('facebooklogin', 'theme_oklassedu') ?></span>
                            </a>
                            <a href="oauth2login.php?providername=google" class="ic-gl">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_google.png" alt="google" /></span>
                                <span><?php echo get_string('googlelogin', 'theme_oklassedu') ?></span>
                            </a>
<!--                            <a href="#" class="ic-zl">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_zalo.png" alt="zalo" /></span>
                                <span><?php echo get_string('zalologin', 'theme_oklassedu') ?></span>
                            </a>-->
                        </div>
                    </div>
                    <div class="box02 <?php if($auth!=''){ echo 'on';} ?>"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var passp = "";
        var passi = "";
        var errormsg = "<?php echo $errormsg ?>";
        if (errormsg != "") {
            alert(errormsg);
        }
        $(".login-tab li").click(function() {
            if (!$(this).hasClass("on")) {
                change_tab(1);
            }
        });
        $(document).on("click", ".termcheck", function() {
            change_tab(2);
        });
        $(document).on("keyup", "#joinpassword", function() {
            check_val('joinpassword');
            match_password();
        });
        $(document).on("keyup", "#joinpassword2", function() {
            match_password();
        });
        $(document).on("click", ".joinemail", function() {
            if ($("#emailconfirm").val() != 1) {
                alert("<?php echo get_string("checkemail", "local_signup") ?>");
                return false;
            }
            if ($("#joinpasswordconfirm").val() != 1) {
                alert("<?php echo get_string("checkjoinpassword", "local_signup") ?>");
                return false;
            }
            if ($("#joinpassword2").val() == $("#joinpassword").val()) {
                change_tab(3);
            } else {
                alert("<?php echo get_string("notmatchedpass", "local_signup") ?>");
                return false;
            }
        });
        $(document).on("change", "#year, #month", function() {
            get_day();
        });
        var auth = '<?php echo $auth;?>'; 
        if(auth!=''){
             var data = {"type": 1, "check": 3};
               data["postn"] = '<?php echo $firstname; ?>';
               data["poste"] = '<?php echo $email; ?>';
               data["username"] = '<?php echo $username; ?>';
               data["uid"] = '<?php echo $uid; ?>';
                data["auth"] = auth;
                $.ajax({
                    url: "/local/job/auth/logintab_form.php",
                    type: 'post',
                    data: data,
                    success: function (result) {
                        $(".box02").empty().append(result);
                        $("#logintab").removeClass("on");
                        $("#signuptab").addClass("on");
                    }
                });
        }
    });

    function change_tab(check) {
        $(".login-tab li").each(function(i, v) {
            if ($(v).hasClass("on")) {
                get_loginform($(v).attr("id"), check);
            };
        });
    }

    function match_password() {
        if ($("#joinpassword2").val() == $("#joinpassword").val()) {
            $(".joinpassword2warning").text("<?php echo get_string("matchedpass", "local_signup") ?>");
        } else {
            $(".joinpassword2warning").text("<?php echo get_string("notmatchedpass", "local_signup") ?>");
        }
    }

    function get_loginform(type, check) {
        var data = {
            "type": type,
            "check": check
        };
        if (check == 3) {
            data["postp"] = $("#joinpassword").val();
            data["poste"] = $("#email").val();
        }
        $.ajax({
            url: "<?php echo $VISANG->wwwroot . '/auth/logintab_form.php'; ?>",
            type: 'post',
            data: data,
            success: function(result) {
                $(".box02").empty().append(result);
            }
        });
    }

    function get_day() {
        var Year = document.getElementById('year').value;
        var Month = document.getElementById('month').value;
        var day = new Date(new Date(Year, Month, 1) - 86400000).getDate();
        var text = '';
        for (var i = 1; i <= day; i++) {
            var selected = '';
            if (i == 1) {
                selected = 'selected';
            }
            var i_length = i.toString().length;
            if (i_length == 1) {
                i = '0' + i.toString();
            }
            text += "<option value='" + i + "' " + selected + ">" + i + "</option>\n"
        }
        $("#days").empty().append(text);

    }

    function check_val(type) {
        var target = $("#" + type);
        var regExpemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (type == 'email' && !regExpemail.test($("#email").val())) {
            var errortext = "<?php echo get_string("emailrule", "local_signup", array('rule' => "visang@visang.com")) ?>";
            alert(errortext);
            $("." + type + "warning").text(errortext);
            $("#" + type + "confirm").val(0);
        } else {
            $.ajax({
                url: "/local/signup/valueconfirm.php",
                type: 'post',
                dataType: "json",
                data: {
                    type: type,
                    value: target.val()
                },
                success: function(result) {
                    if (result.status) {
                        $("#" + type + "confirm").val(result.status);
                        $("." + type + "warning").text(result.text);
                    } else {
                        $("#" + type + "confirm").val(result.status);
                        $("." + type + "warning").text(result.text);
                    }
                }
            });
        }
    }
</script>
<?php
$VISANG->theme->footer(false);
