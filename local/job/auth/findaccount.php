<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $SESSION;

// ===================================================================================================
// handles
if (isloggedin()) {
    redirect($VISANG->wwwroot);
}

// ===================================================================================================
// renders
$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->title = get_string("findmyaccount", "local_signup");
$VISANG->theme->header();
?>
<div class="m-prev mg-bt20">
    <a href="javascript:history.back();" class="ic-arrow">prev</a>
    <span><?php echo get_string('Findaccount', 'local_job'); ?></span>
</div>
<div class="cont">
    <div class="group">
        <div class="login-bx position-relative top-0 left-0 my-0 mx-auto">
            <div class="bg bg-area">
                <div class="center-tit"><?php echo get_string('Findaccount', 'local_job'); ?></div>
                <p class="center-txt"><?php echo get_string('FindaccountNotice', 'local_job'); ?></p>
            </div>
            <p class="ft-dot"><?php echo get_string('Requiredinput', 'local_job'); ?></p>
            <form class="email-bx join" action="<?php echo $VISANG->wwwroot . '/auth/send_tempp.php' ?>" method="POST">
                <input type="hidden" name="password" value="<?php echo $postp ?>" readonly />
                <p>
                    <strong><?php echo get_string("name", "local_signup"); ?></strong>
                    <input type="text" name="firstname" placeholder="<?php echo get_string("writename", "local_signup"); ?>" />
                </p>
                <p>
                    <strong><?php echo get_string("birth", "local_signup"); ?></strong>
                    <select name="year" id="year">
                        <?php
                        foreach (range(date('Y'), 1960) as $year) {
                            echo '<option value="' . $year . '">' . $year . '</option>';
                        }
                        ?>
                    </select>
                    <select name="month" id="month">
                        <?php
                        for ($m = 1; $m <= 12; $m++) {
                            if (strlen($m) == 1)
                                $m = "0" . $m;
                            if ($m == $mo) {
                                $date_month .= "<option value='$m' selected>$m</option>\n";
                            } else {
                                $date_month .= "<option value='$m'>$m</option>\n";
                            }
                        }
                        echo $date_month;
                        ?>
                    </select>
                    <select name="days" id="days">
                        <?php
                        for ($d = 1; $d <= 31; $d++) {
                            if (strlen($d) == 1)
                                $d = "0" . $d;
                            if ($d == $da) {
                                $date_day .= "<option value='$d' selected>$d</option>\n";
                            } else {
                                $date_day .= "<option value='$d'>$d</option>\n";
                            }
                        }
                        echo $date_day;
                        ?>
                    </select>
                </p>
                <p class="phone">
                    <strong><?php echo get_string("phone", "local_signup"); ?></strong>
                    <input type="text" id="" name="p1" class="w-auto" placeholder="<?php echo get_string('phone_txt', 'local_my') ?>" required style="width: 100% !important">
                </p>

                <div class="text-center btn-area">
                    <input type="submit" value="<?php echo get_string("confirm", "core") ?>" class="btns point big " />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(document).on("change", "#year, #month", function() {
            get_day();
        });
    });

    function get_day() {
        var Year = document.getElementById('year').value;
        var Month = document.getElementById('month').value;
        var day = new Date(new Date(Year, Month, 1) - 86400000).getDate();
        var text = '';
        for (var i = 1; i <= day; i++) {
            var selected = '';
            if (i == 1) {
                selected = 'selected';
            }
            var i_length = i.toString().length;
            if (i_length == 1) {
                i = '0' + i.toString();
            }
            text += "<option value='" + i + "' " + selected + ">" + i + "</option>\n"
        }
        $("#days").empty().append(text);

    }
</script>
<?php
$VISANG->theme->footer(false);
