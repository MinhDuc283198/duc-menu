<?php

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');

$providername = required_param('providername', PARAM_RAW);

require_once($CFG->dirroot . '/auth/googleoauth2/lib.php');
require_once($CFG->dirroot . '/auth/googleoauth2/classes/provider/' . $providername . '.php');

// Load the provider plugin.
$providerclassname = 'provideroauth2' . $providername;
$provider = new $providerclassname();
$authurl = $provider->getAuthorizationUrl();
set_state_token($providername, $provider->getState());


redirect($authurl);
