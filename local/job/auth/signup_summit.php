<?php

require_once(dirname(__FILE__) . '/../lib.php');
global $CFG, $VISANG, $DB, $USER, $SESSION;
require_once $CFG->dirroot . '/local/signup/lib.php';

$resend = optional_param('resend', 0, PARAM_INT);
$snsid = optional_param('snsid', '', PARAM_RAW);
$auth = optional_param('auth', '', PARAM_RAW); 
$email = optional_param('email', '', PARAM_RAW); 
$emailauth = new local_auth_plugin_email();
if ($resend == 1) {
    $userid = optional_param('userid', 0, PARAM_INT);
    $user = $DB->get_record("user", array("id" => $userid));
    if ($user) {
        $status = $emailauth->send_confirmation_email($user);
        $resenddata = new stdClass();
        $resenddata ->status = $status;
        $resenddata ->statustext = get_string("falsesend","local_signup",$user->email);
        if($status == 1){
        $resenddata ->statustext = get_string("successsend","local_signup",$user->email);
        }
        echo json_encode($resenddata);
    }
} else {
    $user->username = $auth ? optional_param('username', 0, PARAM_RAW) : optional_param('email', 0, PARAM_RAW);
    $user->password =  $auth ? 'not cached' : optional_param('password', 0, PARAM_RAW);
    $user->email = $snsid ? $snsid : $email;
    $user->phone1 = optional_param('phone1', 0, PARAM_RAW);
    $user->phone2 = optional_param('year', 0, PARAM_RAW) . "-" . optional_param('month', 0, PARAM_RAW) . "-" . optional_param('days', 0, PARAM_RAW);
    $user->firstname = optional_param('firstname', 0, PARAM_RAW);
    $user->lastname = '&nbsp;';
    $user->confirmed = 0;
    $user->lang = current_language();
    $user->firstaccess = 0;
    $user->timecreated = time();
    $user->mnethostid = $CFG->mnet_localhost_id;
    $user->secret = random_string(15);
    $user->auth = $auth ? 'googleoauth2' : 'email';
    $user->lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
    $user->joinstatus = 'j';
    if (!empty($user->email) && !empty($user->password) && !empty($user->phone1) && !empty($user->firstname)) {
        if ($userid = $emailauth->user_signup($user,true,$auth,$email)) {
            redirect($VISANG->wwwroot . "/auth/successsignup.php?id=$userid");
        }
    }

    echo '<script>
        alert("Registration failed. Please try again.");
        window.location.href = "' . $VISANG->wwwroot . '/auth/login.php"; 
    </script>';
}
