<?php

require_once(dirname(__FILE__) . '/../lib.php');
global $CFG, $VISANG, $DB, $USER, $SESSION;

if (isloggedin()) {
    redirect($VISANG->wwwroot);
}
$userid = required_param('id', PARAM_INT);
$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div role="main">
            <div class="mail-end-bx style02">
                <div><?php echo get_string("sendsuccess", "local_signup"); ?></div>
                <p>
                    <?php echo get_string("sendsuccesstext1", "local_signup"); ?><br />
                    <?php echo get_string("sendsuccesstext2", "local_signup"); ?>
                </p>
                <p class="small">
                    <?php echo get_string("sendsuccesstext3", "local_signup"); ?><br />
                    <?php echo get_string("sendsuccesstext4", "local_signup"); ?><br />
                </p>
                <a href="#" onclick="email_resend(<?php echo $userid ?>)" class="btns point big"><?php echo get_string("resendmail", "local_signup"); ?></a>
                <a href="<?php echo $VISANG->wwwroot ?>" class="btns big"><?php echo get_string("gohome", "local_signup"); ?></a>
            </div>
        </div>
    </div>
</div>

<script>

    function email_resend(userid) {
        $.ajax({
            url: "<?php echo $VISANG->wwwroot . '/auth/signup_summit.php'; ?>",
            type: 'post',
            dataType: "json",
            data: {
                resend: 1,
                userid: userid
            },
            success: function(result) {
                alert(result.statustext);
            }
        });
    }
</script>
<?php
$VISANG->theme->footer(false);
