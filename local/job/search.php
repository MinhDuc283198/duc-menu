<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

// =====================================================================================================
// handles
$skillid = optional_param('skillid', 0, PARAM_INT);
$_skill_search = null;
if ($skillid != 0) {
    $_skill_search = $DB->get_record('vi_skills', array('id' => $skillid));
    if ($_skill_search) unset($_GET['searchtext']);
}

$searchtext = $DB->sql_like_escape(optional_param('searchtext', '', PARAM_RAW_TRIMMED));

$view = optional_param('view', '', PARAM_RAW);
$skillgroupidparam = optional_param('skillgroupid', 0, PARAM_INT);
$positionparam = optional_param('city', 0, PARAM_RAW);

$baseurl = new moodle_url($VISANG->wwwroot . '/search.php', array('searchtext' => $searchtext));

$comname_sortorder = optional_param('comname_sortorder', -1, PARAM_INT);
if ($comname_sortorder != -1) {
    $comname_sortorder = $comname_sortorder == 1 ? $comname_sortorder : 0;
}

$job_sortorder = optional_param('job_sortorder', '', PARAM_RAW);
if (!in_array($job_sortorder, array('register', 'deadline'))) {
    $job_sortorder = '';
}

$employers_data = mkjobs_search_employers();
$employers = $employers_data['data'];
$employers_count = $employers_data['total'];

$jobs_data = mkjobs_search_jobs();
$jobs = $jobs_data['data'];
$jobs_count = $jobs_data['total'];

if ($_skill_search) {
    $employers_count = 0;
}
//recommend course
require_once($CFG->dirroot . '/local/course/lib.php');
$course = mkjobs_get_recommend_courses_random_1();
$context = context_course::instance($course->lcocousreid);
$courseimg = local_course_get_imgpath($context->id);


$districts = mkjobs_get_all_location();
$cateories = mkjobs_get_all_categories();

// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = get_string('search', 'local_job');
setcookie('searchtext', $searchtext, time() + 86400, '/');
setcookie('searchcate', $skillgroupidparam, time() + 86400, '/');
setcookie('searchloca', $positionparam, time() + 86400, '/');
$_COOKIE['searchtext'] = $searchtext;
$_COOKIE['searchcate'] = $skillgroupidparam;
$_COOKIE['searchloca'] = $positionparam;
$VISANG->theme->header();

?>
    <div class="main">
        <div class="search-area">
            <div class="search-sec">
                <form action="<?php echo $CFG->wwwroot . '/local/job/search.php' ?>" method="get">
                    <div class="col-lg-11 col-md-11" style="margin: 0 auto;">
                        <div class="row px-4 px-md-0">
                            <div class="col-lg-4 col-md-12 col-sm-12 p-1 input-wrapper">
                                <img src="jobs/img/search.png" alt="search-icon">
                                <input type="text" name="searchtext" class="form-control search-slt search-keyworld"
                                       id="search-keyworld"
                                       placeholder="<?php echo get_string('searchkeyworld', 'local_job'); ?>">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                                <img src="jobs/img/allcategory.png" alt="search-icon">
                                <select class="form-control search-slt search-category custom-select" id=""
                                        name="skillgroupid">
                                    <option value="0"><?php echo get_string('searchcategories', 'local_job'); ?></option>
                                    <?php foreach ($cateories as $c) : ?>
                                        <option value="<?php echo $c->id ?>"><?php echo $c->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 p-1 input-wrapper input-wrapper_select">
                                <img src="jobs/img/maps.png" alt="search-icon">
                                <select class="form-control search-slt search-location custom-select" id="" name="city">

                                    <option value="0"><?php echo get_string('searchlocation', 'local_job'); ?></option>
                                    <?php foreach ($districts as $d) : ?>
                                        <option value="<?php echo $d->id ?>"><?php echo ($current_language == "ko") ? $d->name_ko : (($current_language == "en") ? $d->name_en : $d->name_vi); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12 p-1" style="margin: 0 auto">
                                <button type="submit" class="btn btn-danger wrn-btn"><i class="fa fa-search"
                                                                                        aria-hidden="true"></i><?php echo get_string('find_now', 'local_job') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container">
            <!--search result list-->
            <?php if ($jobs_count) : ?>
                <div class="search-list-wrapper">
                    <div class="row">
                        <div class="col-lg-9 col-md-12">
                            <div class="search-list_header">
                                <?php if (!empty($jobs)): ?>
                                    <div class="search-list_sort">
                                    <span>
                                        Sort
                                    </span>
                                        <div class="sort-dropdown">
                                            <p><?php echo $jobs_data['job_sortorder'] == '' ? 'randomly' : ($jobs_data['job_sortorder'] == 'register' ? 'latest' : 'deadline'); ?></p>
                                            <ul class="sort-dropdown_menu">
                                                <li class="sort-dropdown_menu-item">
                                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'], 'job_sortorder' => '')) ?>">
                                                        randomly
                                                    </a>
                                                </li>
                                                <li class="sort-dropdown_menu-item">
                                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'], 'job_sortorder' => 'register')) ?>">
                                                        latest
                                                    </a>
                                                </li>
                                                <li class="sort-dropdown_menu-item">
                                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'], 'job_sortorder' => 'deadline')) ?>">
                                                        deadline
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
<!--                            --><?php //if (empty($jobs)): ?>
<!--                                <div class="search-list_no-result-notify">-->
<!--                                    <p>--><?php //echo get_string('nosearchresults', 'local_job') ?><!--</p>-->
<!--                                    <button><a href="--><?php //echo new moodle_url($VISANG->wwwroot); ?><!--">OK</a></button>-->
<!--                                </div>-->
<!--                            --><?php //endif; ?>
                            <div class="search-list">
                                <?php foreach ($jobs as $job) : ?>
                                    <div class="search-list-item">
                                        <a class="search-list-item_image"
                                           href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>">
                                            <img src="<?php echo $job->company_logo; ?>" alt="company logo">
                                        </a>
                                        <div class="search-item-content">
                                            <a class="search-item-content_companyname"
                                               href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $job->id)); ?>"><?php echo $job->title; ?></a>
                                            <p><?php echo $job->company_name; ?></p>
                                            <div class="search-item-content_info">
                                                <div class="search-item-content_location">
                                                    <img src="assets/dist/images/location-icon.png" alt="location icon">
                                                    <span><?php echo $job->location ? $job->location->name : ''; ?></span>
                                                </div>
                                                <div class="search-item-content_timestamp">
                                                    <img src="assets/dist/images/timestamp-icon.png"
                                                         alt="timestamp icon">
                                                    <span><?php echo $job->timenotificreated ?></span>
                                                </div>
                                            </div>
                                            <div class="search-item-content_skills">
                                                <?php foreach ($job->skills as $skill) : ?>
                                                    <span>
                                            <a class="search-item-content_skills--link"
                                               href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                                <?php echo $skill->name; ?>
                                            </a>
                                        </span>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <div class="search-list-item_savejob">
                                            <?php if (isloggedin()) : ?>
                                                <div class="search-list-item_savejob--islogin">
                                                    <div class="row">
                                                        <button type="button"
                                                                class="btn btn-apply showpop-apply" data-id="<?php echo $job->id; ?>"><?php echo get_string('applynow', 'local_job') ?></button>
                                                    </div>
                                                    <div class="row">
                                                        <button type="button" class="btn btn-save saved-job"
                                                                id="saved-job"
                                                                onclick="savedJob('.save-icon<?php echo $job->id; ?>', '<?php echo $job->id; ?>', 'save-on', 'save-off')"><?php echo get_string('savejob', 'local_job') ?>
                                                            <span class="<?php echo ($comid != null) ? "save-none" : ""; ?> save-icon<?php echo $job->id; ?> <?php echo mkjobs_has_saved_job($job->id) ? "save-on" : "save-off"; ?>"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            <?php else : ?>
                                                <div class="search-list-item_savejob--islogout col-md-0">
                                                    <p>
                                                        <a href="<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>"><?php echo get_string('logintosee', 'local_job'); ?></a>
                                                    </p>
                                                    <img src="assets/dist/images/locksavejob-icon.png"
                                                         alt="hidden icon">
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="search-list_pagination">
                                <ul>
                                    <li class="search-list_paginationitem-first <?php if ($jobs_data['page'] == 1) echo 'disabled' ?>">
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'] - 1)) ?>">
                                            <i class="far fa-angle-left"></i>
                                        </a>
                                    </li>
                                    <?php mkjobs_start_pagination($jobs_data['total_page'], $jobs_data['page'], $jobs_data) ?>
                                    <li class="search-list_paginationitem-last <?php if ($jobs_data['page'] == $jobs_data['total_page']) echo 'disabled' ?>">
                                        <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['searchtext'], 'page' => $jobs_data['page'] + 1)) ?>">
                                            <i class="far fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 hidden-md col-lg-0">
                            <div class="topCompany">
                                <h3 class="topCompany_title"><?php echo get_string('topcompany', 'local_job') ?></h3>
                                <div class="topCompany_content">
                                    <div class="topCompany_content--img">
                                        <img src="assets/dist/images/topcompanyimg.png" alt="topcompanyimg">
                                    </div>
                                    <div class="topCompany_logo">
                                        <img src="assets/dist/images/image1.png" alt="company-logos">
                                    </div>
                                </div>
                                <p class="topCompany_name">Hyundai Aluminum Vina</p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <!--            company search result-->

            <?php if ($employers_count) : ?>
                <div class="group" id="employersGroup">
                    <h4 class="pg-tit mb-0"><?php echo get_string('Company', 'local_job'); ?>
                        (<?php echo $employers_count; ?>)</h4>
                    <div class="tp-tb-area text-right">
                        <ul class="bar-tab">
                            <li>
                                <div class="d-inline-block <?php echo $comname_sortorder == -1 ? '' : 't-gray'; ?>">
                                    <a href="<?php echo $baseurl; ?>"><?php echo get_string('popular', 'local_job'); ?></a>
                                </div>
                            </li>
                            <li>
                                <div class="btn-group">
                                    <a href="#"
                                       class="dropdown-toggle <?php echo $comname_sortorder == -1 ? 't-gray' : ''; ?>"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php echo get_string('Companyname', 'local_job'); ?>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="<?php echo new moodle_url($baseurl, array('comname_sortorder' => 0, 'category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['search'])); ?>"
                                           class="dropdown-item <?php echo $comname_sortorder == 0 ? 'active' : ''; ?>"><?php echo get_string('Ascendingorder', 'local_job'); ?></a>
                                        <a href="<?php echo new moodle_url($baseurl, array('comname_sortorder' => 1, 'category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['search'])); ?>"
                                           class="dropdown-item <?php echo $comname_sortorder == 1 ? 'active' : ''; ?>"><?php echo get_string('Descendingorder', 'local_job'); ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="row mx-0">
                        <?php foreach ($employers as $employer) : ?>
                            <div class="col-sm-6 col-md-4 col-lg-3 px-2 mb-3">
                                <div class="compa-card border">
                                    <div class="compa-card-img d-flex justify-content-center">
                                        <a class="d-flex justify-content-center h-100"
                                           href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id)); ?>">
                                            <img class="align-self-center contain-image"
                                                 src="<?php echo $employer->company_logo; ?>">
                                        </a>
                                    </div>
                                    <div class="border-top p-jbox">
                                        <div class="tit-jbox text-truncate text-truncate-2 mb-2">
                                            <a href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id)); ?>"><?php echo $employer->company_name; ?></a>
                                        </div>
                                        <p class="t-gray text-truncate text-truncate-2 mb-3"><?php echo $employer->company_short_description; ?></p>
                                        <div class="d-flex justify-content-between">
                                            <div><?php echo $employer->location ? $employer->location->name : ''; ?></div>

                                            <?php if ($employer->job_cnt) : ?>
                                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/companies/view.php', array('employerid' => $employer->id, 'view' => 'our-jobs')); ?>"
                                                   class="text-primary">
                                                    <span><?php echo $employer->job_cnt; ?><?php echo get_string('Jobview', 'local_job'); ?></span>
                                                    <span class="ic ic-arrow-right-on"></span>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        <div v-for="item in items" v-cloak class="col-sm-6 col-md-4 col-lg-3 px-2 mb-3">
                            <div class="compa-card border">
                                <div class="compa-card-img d-flex justify-content-center">
                                    <a class="d-flex justify-content-center h-100"
                                       :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id">
                                        <img class="align-self-center contain-image" :src="item.company_logo">
                                    </a>
                                </div>
                                <div class="border-top p-jbox">
                                    <div class="tit-jbox text-truncate text-truncate-2 mb-2">
                                        <a :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id">{{
                                            item.company_name }}</a>
                                    </div>
                                    <p class="t-gray text-truncate text-truncate-2 mb-3">{{
                                        item.company_short_description }}</p>
                                    <div class="d-flex justify-content-between">
                                        <div>{{ item.location.name }}</div>
                                        <a :href="'<?php echo $VISANG->wwwroot . '/companies/view.php?employerid='; ?>' + item.id + '&view=our-jobs'"
                                           class="text-primary" v-if="item.job_cnt">
                                            <span>{{ item.job_cnt }} <?php echo get_string('Jobview', 'local_job'); ?></span>
                                            <span class="ic ic-arrow-right-on"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    company-pagination-->
                    <div class="search-list_pagination">
                        <ul>
                            <li class="search-list_paginationitem-first <?php if ($employers_data['company_page'] == 1) echo 'disabled' ?>">
                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['search'], 'company_page' => $employers_data['company_page'] - 1)) ?>">
                                    <i class="far fa-angle-left"></i>
                                </a>
                            </li>
                            <?php mkjobs_company_start_pagination($employers_data['total_page'], $employers_data['company_page'], $employers_data) ?>
                            <li class="search-list_paginationitem-last <?php if ($employers_data['company_page'] == $employers_data['total_page']) echo 'disabled' ?>">
                                <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('category' => $jobs_data['category'], 'city' => $jobs_data['city'], 'searchtext' => $jobs_data['search'], 'company_page' => $employers_data['company_page'] + 1)) ?>">
                                    <i class="far fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
<!--                    --><?php //if ($employers_count > 8) : ?>
<!--                        <div :class="['px-2 my-3', loadmore ? '':'d-none']">-->
<!--                            <button class="btn btn-block border btn-lg rounded-0 btn-spinner" :disabled="onfetch"-->
<!--                                    @click="fetch">-->
<!--                                <span>--><?php //echo get_string('ViewMore', 'local_job'); ?><!--</span>-->
<!--                                <span class="btn-lbl ic ic-arrow-down"></span>-->
<!--                                <span class="spinner spinner-border spinner-border-sm" role="status"-->
<!--                                      aria-hidden="true"></span>-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    --><?php //endif; ?>
                </div>
                <script>
                    $(function () {
                        new Vue({
                            el: "#employersGroup",
                            mixins: [mkjobs_search_mixin()],
                            data: {
                                action: "mkjobs_search_employers",
                                sesskey: "<?php echo sesskey(); ?>",
                                searchtext: $('#searchInput').val(),
                                perpage: 8,
                                fields: "id,company_name,company_logo,company_short_description",
                                filters: {
                                    comname_sortorder: "<?php echo $comname_sortorder; ?>"
                                }
                            }
                        });
                    });
                </script>
            <?php endif; ?>

            <!--            end company search result-->

            <?php if ($employers_count == 0 && $jobs_count == 0) : ?>
                <div class="group">
                    <div class="no-data style02">
                        <div class="f24">"<strong class="t-blue"><?php echo $searchtext; ?></strong>"<?php echo get_string('searcn_empty', 'local_course'); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="container" style="margin-top: 90px;">
                <div class="search-list-wrapper">
                    <!-- recommend course -->
                    <div class="recommend-course">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <a class="recommend-course_image">
                                    <img src="<?php echo $courseimg; ?>"
                                         alt="<?php echo $course->coursename; ?>">
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="recommend-course_info">
                                    <h4 class="recommend-course_title"><a class="block-ellipsis-title ellipsis"
                                                                          href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $course->id; ?>"><?php echo $course->coursename; ?> </a>
                                    </h4>
                                    <p class="recommend-course_name"><?php echo $course->onelineintro; ?></p>
                                    <p class="recommend-course_rating">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($i <= $course->star) { ?>
                                                <i style="color: #0b93d5 !important;" class="fa fa-star"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-star"></i>
                                            <?php } ?>
                                        <?php } ?>
                                    </p>
                                    <strong class="recommend-course_price"><?php echo number_format($course->price); ?>
                                        <span>VND</span></strong>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="recommend-course_social">
                                    <img src="assets/dist/images/image31.png" alt="image31">
                                    <div class="recommend-course_btn">
                                        <a href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $course->id; ?>"><?php echo get_string('btnclickhere', 'local_job') ?> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end search result list-->
    <script>
        function savedJob(el, id, classon, classoff) {
            $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkjobs_saved_job",
                jobid: id
            }, function (data) {
                if (data == -1) {
                    if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                    }
                } else if (data == 1) {
                    $(el).removeClass(classoff || "save-off");
                    $(el).addClass(classon || "save-on");
                } else {
                    $(el).removeClass(classon || "save-on");
                    $(el).addClass(classoff || "save-off");
                }
            });
        }
        <?php if ($view == 'jobs') : ?>
        $(function () {
            $([document.documentElement, document.body]).animate({
                scrollTop: $('#jobs-wrap').offset().top - $('.wp').height() - ($('.tp').is(':visible') ? 40 : 0) - 15
            }, 200);
        });
        <?php endif; ?>
    </script>
    <script type="text/javascript">
        $(document).ready(showPosition());
        $(function () {
            $(".showpop-apply").click(function () {
                <?php if (isloggedin()) : ?>
                utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/jobs/pop_resume_submit.php' ?>", {
                    "width": "600px",
                    "height": "auto",
                    "callbackFn": function () {
                    }
                }, {
                    "jobid": $(this).data("id")
                });
                <?php else : ?>
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
                <?php endif; ?>
                return false;
            });
        });

        function setDefaultSearchCategory() {
            let categoryBlock = document.querySelector(".search-category");
            let defaultCategory = 'Biên Dịch / Phiên Dịch';

            Array.prototype.forEach.call(categoryBlock.options, function (option, index) {
                if (option.innerText === defaultCategory) {
                    categoryBlock.value = option.attributes.value.value
                }
            });
        }

        function setDefaultPosition() {
            let addressiplocations = [];
            let locationSelector = document.querySelector(".search-location");
            let companylocation = 'Hà Nội';
            <?php foreach ($districts as $district) : ?>
            addressiplocations.push({id: <?php echo $district->id; ?>, name: '<?php echo $district->name; ?>'});
            <?php endforeach; ?>
            addressiplocations.forEach((addressiplocation) => {
                if (addressiplocation.name === companylocation) {
                    locationSelector.value = addressiplocation.id
                }
            })
        }

        function showPosition() {
            setDefaultSearchCategory();
            setDefaultPosition();
            let url = 'https://ipinfo.io/json';
            $.getJSON(url, function (data) {
                let addressiplocations = [];
                let namecity = '';
                let companylocation = removeVietnameseTones(data.city);
                <?php foreach ($districts as $district) : ?>
                namecity = removeVietnameseTones('<?php echo $district->name; ?>')
                addressiplocations.push({id: <?php echo $district->id; ?>, name: namecity});
                <?php endforeach; ?>
                let searchkeyworld = document.getElementById("search-keyworld");
                let categorySelector = document.querySelector(".search-category");
                let locationSelector = document.querySelector(".search-location");
                addressiplocations.forEach((addressiplocation) => {
                    if (addressiplocation.name === companylocation) {
                        locationSelector.value = addressiplocation.id
                    }
                })
                let keyworld = '<?php echo !isset($_COOKIE["searchtext"]) ? '' : $_COOKIE["searchtext"]; ?>';
                let cateid =<?php echo !isset($_COOKIE["searchcate"]) ? 71 : $_COOKIE["searchcate"]; ?>;
                let posiid;
                <?php if(!isset($_COOKIE["searchloca"])): ?>
                posiid = addressiplocation.id;
                <?php else: ?>
                posiid =<?php echo $_COOKIE["searchloca"];?>
                <?php endif; ?>

                if (cateid !== null) {
                    categorySelector.value = cateid;
                }
                if (posiid !== null) {
                    locationSelector.value = posiid;
                }
                if (keyworld !== '') {
                    searchkeyworld.value = keyworld;
                }

            });

            function removeVietnameseTones(str) {
                str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                str = str.replace(/đ/g, "d");
                str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
                str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
                str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
                str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
                str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
                str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
                str = str.replace(/Đ/g, "D");
                // Some system encode vietnamese combining accent as individual utf-8 characters
                // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
                str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
                str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
                // Remove extra spaces
                // Bỏ các khoảng trắng liền nhau
                str = str.replace(/ + /g, " ");
                str = str.trim();
                // Remove punctuations
                str = str.toLowerCase()
                str = str.split(' ').join('');
                // Bỏ dấu câu, kí tự đặc biệt
                str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
                str = str.replace("city", "");
                str = str.replace("thanhpho", "");
                str = str.replace("tp", "");
                return str;
            }
        }
    </script>

<?php $VISANG->theme->footer();
