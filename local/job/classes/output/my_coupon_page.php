<?php

// Standard GPL and phpdocs
namespace local_job\output;

use renderable;
use renderer_base;
use templatable;
use stdClass;

class my_coupon_page implements renderable, templatable
{
    /** @var string $sometext Some text to show how to pass data to a template. */
    var $datapage = null;

    public function __construct($datapage)
    {
        $this->datapage = $datapage;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output)
    {
        global $CFG;

        $baseurl = $this->datapage->baseurl;
        $searchtext = $this->datapage->searchtext;

        $data = new stdClass();
        //set title and heading table
        $data->coupon_title = $this->datapage->coupon_title;
        $data->confirmdelete=get_string('confirmdelete','local_job');
        $data->succdel=get_string('succdel','local_job');
        $data->faildel=get_string('faildel','local_job');
        $data->tablehead = new stdClass();
        $data->tablehead->no = get_string('cp_no','local_job');
        $data->tablehead->title = get_string('cp_title','local_job');
        $data->tablehead->course = get_string('cp_course','local_job');
        $data->tablehead->discount_rate = get_string('cp_dcrate','local_job');
        $data->tablehead->receiveddate = get_string('cp_receiveddate','local_job');
        $data->tablehead->subscriptioncode = get_string('cp_subscriptioncode','local_job');
        $data->tablehead->used = get_string('cp_used','local_job');
        $data->tablehead->useddate = get_string('cp_useddate','local_job');
        $data->tablehead->expired = get_string('cp_expired','local_job');
        $data->tablehead->expire_date = get_string('cp_expire_date','local_job');
        $data->tablehead->actions = get_string('cp_actions','local_job');

        //get coupon receivers
        if ($searchtext==""){
            $user_coupons = get_coupon_by_userid();
        }else{
            $user_coupons = search_coupon_by_userid($searchtext);
        }

        $data->url = $baseurl;
        // set rows data
        $data->rows = [];
        if (count($user_coupons) <= 0) {
            $data->nodata = get_string('cp_nodata','local_job');
        } else {
            $data->stylenodata = "none";
            $no = 1;
            foreach ($user_coupons as $coupon) {
                $coupon_infor = get_coupon_by_id($coupon->coupon_id);
                $item = new stdClass();
                $item->no = $no;
                $item->id = $coupon->id;
                $item->title = $coupon_infor->title;
                $coupon_course = get_course_coupon_by_id($coupon->coupon_id);
                $course_infor = get_course_by_id($coupon_course->course_id);
                $item->course = $course_infor->coursename;
                $item->linkcourse=$CFG->job_wwwroot .'/local/course/detail.php?id='.$course_infor->id;
                $item->discount_rate = $coupon_infor->discount_rate;
                $item->send_date = date_format_string($coupon->received_time, '%Y/%m/%d');
                $item->key_code = $coupon_infor->key_code;
                if ($coupon->is_used == 0) {
                    $item->is_used = get_string('cp_noque','local_job');
                    $item->used_date = '-';
                } else if ($coupon->is_used == 1) {
                    $item->is_used = get_string('cp_yesque','local_job');
                    $item->used_date = date_format_string($coupon->used_time, '%Y/%m/%d');
                }

                $today = time();
                if ($coupon_infor->expire_date - $today <= 0) {
                    $item->is_expire = get_string('cp_yesque','local_job');
                } else {
                    $item->is_expire = get_string('cp_noque','local_job');
                }
                $item->expire_date = date_format_string($coupon_infor->expire_date, '%Y/%m/%d');
                $data->rows[] = $item;
                $no++;
            }
        }

        return $data;
    }


}