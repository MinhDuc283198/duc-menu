<?php

// Standard GPL and phpdocs
namespace local_job\output;

defined('MOODLE_INTERNAL') || die;

use plugin_renderer_base;

class renderer extends plugin_renderer_base
{
    /**
     * Defer to template.
     *
     * @param index_page $page
     *
     * @return string html for the page
     */
    public function render_index_page($page)
    {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_job/index_page', $data);
    }
    public function render_my_coupon_page($page)
    {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_job/my_coupon_page', $data);
    }
}