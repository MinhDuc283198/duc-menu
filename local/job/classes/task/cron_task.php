<?php


namespace local_job\task;
class cron_task extends \core\task\scheduled_task
{


    public function get_name()
    {
        //show on screen admin
        return get_string('send_list_job', 'local_job');
    }

    public function execute()
    {
        global $CFG, $DB, $USER, $VISANG;
        $dataheader="<!DOCTYPE html>
<html lang='ko'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
</head>

<body>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese' rel='stylesheet' />
    <div style='border: 1px solid #ddd;padding: 50px 30px 0;width: 750px;text-align: center; margin: 0 auto;'>
        <p style='text-align: center;margin: 0 0 10px;'>
            <a href='https://job.masterkorean.vn/local/job' target='_blank'><img src='https://job.masterkorean.vn/theme/oklassedu/pix/images/logo_not_beta_job.png' alt='master Korean' width='250px' /></a>
        </p>
        <div style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;font-size: 25px;font-weight: bold;text-align: center;margin-bottom: 10px;'>
                We recommend new recruit information for you with a higher job matching prospect</div>";
        $datafooter="<div style='background: #f4f4f4;font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;width: auto; margin: 10px 0; padding: 20px 30px;box-sizing: border-box;border-top: 1px solid #ddd;margin: 30px -30px 0;text-align: left;'>
            <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>This is a one-time outgoing email.</p>
            <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>You cannot reply to this email. If you have
                any questions, please use our customer center</p>
            <p style='font-family: inherit;font-size: 13px;margin: 0 0 3px;color: #777;margin-top: 7px;'>Copyright ©
                VISANG Education Group Vietnam Company </p>
        </div>
    </div>
</body>

</html>";
        $taskrate=$DB->get_record('vi_rate_send_email_job', array('id'=>1));
        $email_users = $DB->get_records('user', array('isreceivejob'=>'1'));
        $thistime = time();
        $supportuser = \core_user::get_support_user();
        $supportuser->firstname='Master Korean Jobs';
        foreach ($email_users as $eu) {
            if (!is_siteadmin($eu->id)) {
                $recommend_jobs = $this->mkjobs_get_recommend_jobs1($eu->id);
                $jobs = array();
                foreach ($recommend_jobs as $rj) {
                    if ($rj->matchingstar->star >= 0 && $rj->timenotificreated <= (3 * 30 * 24 * 60 * 60)) {
                        $jobs[] = $rj;
                    }
                }
                if (count($jobs) > 0) {
                    usort($jobs,function ($a,$b){return $a->matchingstar->star<$b->matchingstar->star;});
                    $eu->mailformat = 1;
                    $eu->email = $DB->get_field('lmsdata_user', 'email2', array('userid' => $eu->id));
                    $subject = 'We recommend new recruit information for you with a higher job matching prospect';
                    $count=1;
                    foreach ($jobs as $job) {
                        $url = $CFG->job_url . '/local/job/jobs/view.php?jobid=' . $job->id;
                        $logo = $job->company_logo == '' ? 'https://www.masterkorean.vn/theme/oklassedu/pix/images/ft_logo01.png' : 'https://job.masterkorean.vn/pluginfile.php/' . $job->company_logo;
                        $data=$data."<table style='width: 720px; margin: 30px auto; text-align: left; border: 1px solid #485BC6; border-radius: 6px;'>
                        <tr>
                            <td style='text-align: center; width:200px;'>
                                    <img src='".$logo."' height='130px' width='130px' style='margin: 20px 10px;'/>
                            </td>
                            <td style='width: 450px;'>
                                <table style=''>
                                    <tr>    
                                        <td>
                                            <p style='margin-bottom: 5px;margin-top: 5px;font-family: inherit;font-style: normal;font-weight: bold;font-size: 25px;line-height: 29px;'><a style='text-decoration: none; color: #4F4F4F;' href='".$url."'>" . $job->company_name . "</a></p>
                                            <p style='margin-bottom: 5px;margin-top: 5px;font-family: inherit;font-style: normal;font-weight: normal;font-size: 19px;line-height: 22px;'><a style='text-decoration: none; color: #828282;' href='".$url."'>" . $job->title . "</a></p>
                                            <p style='margin-bottom: 5px;margin-top: 5px;'><span style='font-family: inherit;font-size: 14px;'>Degree: " . $job->final_educations . "</span><span style='font-family: inherit;font-size: 14px; margin-left: 50px;'>Experience: " . $job->work_experiences . "</span></p>
                                            <p style='margin-bottom: 5px;margin-top: 5px;'><span style='font-family: inherit;font-size: 14px;'>Update: " . $job->timeexpred . "</span><span style='font-family: inherit;font-size: 14px; margin-left: 50px;'>Address: ".$job->company_location."</span></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                </table>";
                        if($count>=10){
                            break;
                        }else{
                            $count++;
                        }
                    }
                    $jobs=null;
                    $messagehtml = $dataheader.$data.$datafooter;
                    $data=null;
                    $check=email_to_user($eu, $supportuser, $subject, '', $messagehtml);
                }
            }
        }
    }

    function mkjobs_get_jobmatching_star1($job, $user_workplaces, $user_fields, $user_salary)
    {
        $matchingstar = new \stdClass();
        $star = 0;
        $percentstar = 0;
        if (in_array($job->skill_group_id, $user_fields)) {
            $star += 25;
        }
        if (in_array($job->district_id, $user_workplaces)) {
            $star += 35;
        }

        $star += 40;

        if ($star <= 20) {
            $percentstar = $star * 8 / 10;
        } elseif ($star > 20 && $star <= 40) {
            $percentstar = 20 + ($star - 20) * 8 / 10;
        } elseif ($star > 40 && $star <= 60) {
            $percentstar = 40 + ($star - 40) * 8 / 10;
        } elseif ($star > 60 && $star <= 80) {
            $percentstar = 60 + ($star - 60) * 8 / 10;
        } else {
            $percentstar = 80 + ($star - 80) * 8 / 10;
        }

        $matchingstar->star = $star;
        $matchingstar->percentstar = $percentstar;

        return $matchingstar;
    }

    function mkjobs_notifi_time_created1($timecreated, $timemodified, $enddate)
    {
        if (!empty($timemodified)) {
            $btime = $timemodified;
        } else {
            $btime = $timecreated;
        }
        $thistime = time();
        $diff = $thistime - $btime;
        return $diff;
    }

    function mkjobs_notifi_time_created($timecreated, $timemodified, $enddate)
    {
        if (!empty($timemodified)) {
            $btime = $timemodified;
        } else {
            $btime = $timecreated;
        }
        $thistime = time();
        if ($thistime < $enddate) {
            $diff = $thistime - $btime;
            $s = 60; //1분 = 60초
            $h = $s * 60; //1시간 = 60분
            $d = $h * 24; //1일 = 24시간
            $w = $d * 7;
            $m = $d * 30;
            $y = $d * 365;

//        $y = $d * 10; //1년 = 1일 * 10일

            if ($diff < $s) {
                $time = get_string('notifi_time:second' . ($diff >= 2 ? 's' : ''), 'local_job', sprintf("%02d", $diff));
            } elseif ($h > $diff && $diff >= $s) {
                $time = get_string('notifi_time:minute' . (round($diff / $s) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $s)));
            } elseif ($d > $diff && $diff >= $h) {
                $time = get_string('notifi_time:hour' . (round($diff / $h) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $h)));
            } elseif ($w > $diff && $diff >= $d) {
                $time = get_string('notifi_time:day' . (round($diff / $d) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $d)));
            } elseif ($m > $diff && $diff >= $w) {
                $time = get_string('notifi_time:week' . (round($diff / $w) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $w)));
            } elseif ($y > $diff && $diff >= $m) {
                $time = get_string('notifi_time:month' . (round($diff / $m) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $m)));
            } else {
                $time = get_string('notifi_time:year' . (round($diff / $y) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $y)));
            }
        } else {
            $time = get_string('DayEnd', 'local_job');
        }

        return $time;
    }

    function mkjobs_get_recommend_jobs1($userid, $limit = 20)
    {
        global $DB, $USER;
        $jobs = array();
        $thistime = time();

        $user_workplaces = $DB->get_fieldset_select('vi_user_workplaces', 'district_id', 'user_id = :user_id', array('user_id' => $userid));
        $user_fields = $DB->get_fieldset_select('vi_user_skill_groups', 'skill_group_id', 'user_id = :user_id', array('user_id' => $userid));
        $user_salary = 2;

        $by_skills = "SELECT vj.id 
                    FROM {vi_jobs} vj
                    JOIN {vi_employers} em ON em.id = vj.employer_id
                    JOIN {vi_job_skills} js ON js.job_id=vj.id
                    JOIN {vi_user_skills} us ON js.skill_id=us.skill_id
                    WHERE us.user_id=$userid AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
                    AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                    GROUP BY vj.id";

        $by_groups = "SELECT vj.id 
                    FROM {vi_jobs} vj
                    JOIN {vi_employers} em ON em.id = vj.employer_id
                    JOIN {vi_user_skill_groups} usg ON vj.skill_group_id = usg.skill_group_id
                    WHERE usg.user_id=$userid AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
                    AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                    GROUP BY vj.id";

        $by_workplaces = "SELECT vj.id 
                    FROM {vi_jobs} vj
                    JOIN {vi_employers} em ON em.id = vj.employer_id
                    JOIN {vi_job_locations} jl ON jl.job_id = vj.id 
                    JOIN {vi_locations} l ON jl.location_id = l.id 
                    JOIN {vi_user_workplaces} uw ON l.district_id = uw.district_id
                    WHERE uw.user_id=$userid AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
                    AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                    GROUP BY vj.id";

        $user_final_education = $DB->get_record('vi_user_final_educations', array('user_id' => $userid));
        $user_work_experience = $DB->get_record('vi_user_work_experiences', array('user_id' => $userid));
        $user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $userid));

        $where = array("vj.id IN (" . $by_skills . ")", "vj.id IN (" . $by_groups . ")", "vj.id IN (" . $by_workplaces . ")");
        if ($user_final_education) $where[] = 'vj.final_education_id=' . $user_final_education->final_education_id;
        if ($user_work_experience) $where[] = 'vj.work_experience_id=' . $user_work_experience->work_experience_id;
        if ($user_korean_level) $where[] = 'vj.korean_level_id=' . $user_korean_level->korean_level_id;

        $jobs = $DB->get_records_sql(
            "SELECT vj.*, ve.company_name, ve.company_logo,fe.name_en as final_educations, we.name_en as work_experiences, d.name as company_location, sa.name as job_salary, d.id as district_id
            FROM {vi_jobs} vj
            JOIN {vi_employers} em ON em.id = vj.employer_id
            JOIN {vi_employers} ve on vj.employer_id = ve.id
            LEFT JOIN {vi_work_experiences} we on vj.work_experience_id = we.id
            LEFT JOIN {vi_job_locations} jl ON jl.job_id = vj.id
            LEFT JOIN {vi_locations} l ON l.id = jl.location_id
            LEFT JOIN {vi_districts} d ON d.id = l.district_id
            LEFT JOIN {vi_job_salary_categories} sa ON sa.id = vj.job_salary_category_id
            LEFT JOIN {vi_final_educations} as fe  ON vj.final_education_id=fe.id
            WHERE vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
            AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate)) AND (" . implode(" OR ", $where) . ")
            ORDER BY RAND()
            LIMIT $limit"
        );
        foreach ($jobs as $job) {
            $job->timenotificreated = $this->mkjobs_notifi_time_created1($job->timecreated, $job->timemodified, $job->enddate);
            $job->timeexpred = $this->mkjobs_notifi_time_created($job->timecreated, $job->timemodified, $job->enddate);
            $job->matchingstar = $this->mkjobs_get_jobmatching_star1($job, $user_workplaces, $user_fields, $user_salary);
        }
        return $jobs;
    }

}