<?php


namespace local_job\task;
use core_availability\result;
use gradereport_singleview\local\screen\user;

class employer_task extends \core\task\scheduled_task
{


    public function get_name()
    {
        //show on screen admin
        return get_string('send_list_cv', 'local_job');
    }

    public function execute()
    {
        global $CFG, $DB, $USER, $VISANG;
        $thistime = time();
        $dataheader="<!DOCTYPE html>
<html lang='ko'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
</head>

<body>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese' rel='stylesheet' />
    <div style='border: 1px solid #ddd;padding: 50px 30px 0;width: 850px;text-align: center; margin: 0 auto;'>
        <p style='text-align: center;margin: 0 0 10px;'>
            <a href='https://job.masterkorean.vn/local/job' target='_blank'><img src='https://job.masterkorean.vn/theme/oklassedu/pix/images/logo_not_beta_job.png' alt='master Korean' width='250px' /></a>
        </p>
        <div style='font-family: Noto Sans KR, Noto Sans SC, 맑은고딕, sans-serif;font-size: 25px;font-weight: bold;text-align: center;margin-bottom: 10px;'>
                We recommend new CV for you with a higher job matching prospect</div>";
        $datafooter="<div style='background: #f4f4f4;font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;width: auto; margin: 10px 0; padding: 20px 30px;box-sizing: border-box;border-top: 1px solid #ddd;margin: 30px -30px 0;text-align: left;'>
            <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>This is a one-time outgoing email.</p>
            <p style='font-family: inherit;font-size: 14px;margin: 0 0 3px;'>You cannot reply to this email. If you have
                any questions, please use our customer center</p>
            <p style='font-family: inherit;font-size: 13px;margin: 0 0 3px;color: #777;margin-top: 7px;'>Copyright ©
                VISANG Education Group Vietnam Company </p>
        </div>
    </div>
</body>
</html>";
        $supportuser = \core_user::get_support_user();
        $supportuser->firstname='Master Korean Jobs';
        $employers= $this->task_get_employer();
        foreach( $employers as $item){
            $listcvs=null;
            $subject='We recommend new CV for you with a higher job matching prospect';
            $listcvs=$this->task_get_jobid_uercv(714);
            usort($listcvs,function ($a,$b){return $a->machinhrate<$b->machinhrate;});
            if(count($listcvs)>0){
                $count=1;
                foreach ($listcvs as $cv){
                    $cv_infor=$DB->get_record('vi_resumes',array('id'=>$cv->cvid));
                    $job_infor=$DB->get_record('vi_jobs',array('id'=>$cv->jobid));
                    $user_infor=$DB->get_record('user',array('id'=>$cv_infor->user_id));
                    $user_infor->mailformat=1;
                    $user_avatar=$DB->get_record('avatar_user',array('user_id'=>$user_infor->id));
                    $urlavatar=!$user_avatar ? 'https://job.masterkorean.vn/theme/oklassedu/pix/avtdef.jpg' : $CFG->wwwroot .'/pluginfile.php/' . $user_avatar->url;
                    $urlcv=$CFG->job_url . '/local/job/resumes/view.php?resumeid='.$cv_infor->id;
                    $urljob=$CFG->job_url . '/local/job/jobs/view.php?jobid='.$job_infor->id;
                    $firstname=$user_infor->firstname;
                    $jobtitle=$job_infor->title;
                    $education=$this->task_get_user_final_education($user_infor->id);
                    $edutitle=$education->name_en;
                    $experience=$this->task_get_user_experience($user_infor->id);
                    $exptitle=$experience->name_en;
                    $koreanlevel=$this->task_get_user_korean_levels($user_infor->id);
                    $koreanleveltitle=$koreanlevel->name_en;
                    $locations=$this->task_get_user_locations($user_infor->id);
                    $skills=$this->task_get_user_skills($user_infor->id);

                    $data=$data."<table style='width: 830px; margin: 30px auto; text-align: left; border: 1px solid #485BC6; border-radius: 6px;'>
                        <tr>
                            <td style='text-align: center; width: 150px;'>
                                    <img src='".$urlavatar."' height='130px' width='130px' style='border-radius: 100%; margin-left: 20px  ;'/>
                            </td>
                            <td style='width: 450px;'>
                                <table style=''>
                                    <tr>    
                                        <td>
                                            <p style='margin-bottom: 5px;margin-top: 5px; font-family: inherit;font-style: normal;font-weight: bold;font-size: 28px;line-height: 29px;margin-left: 30px;color: blue;'>" . $firstname . "</p>
                                            <p style='margin-bottom: 5px;margin-top: 5px;width: 450px; text-overflow: ellipsis; white-space: nowrap;overflow: hidden;font-family: inherit;font-style: normal;font-weight: normal;font-size: 22px;line-height: 22px;margin-left: 30px;'><a style='text-decoration: none; color: #828282;' href='".$urljob." '> ". $jobtitle ."</a></p>
                                            <p style='margin-bottom: 5px;margin-top: 5px;font-family: inherit;font-size: 18px;margin-left: 30px;'>Educations: " . $edutitle . "</p>
                                            <p style='margin-bottom: 5px;margin-top: 5px;font-family: inherit;font-size: 18px; margin-left: 30px;'>Year of experience: " . $exptitle. "</p>
                                            <p style='margin-bottom: 5px;margin-top: 5px; font-family: inherit;font-size: 18px; margin-left: 30px;'>Korean level: ". $koreanleveltitle."</p>
                                            <p style='margin-bottom: 5px;margin-top: 5px; width: 450px; text-overflow: ellipsis; white-space: nowrap;overflow: hidden;font-family: inherit;font-size: 18px; margin-left: 30px;'>Locations: ". $locations ."</p>
                                            <p style='margin-bottom: 5px;margin-top: 5px; width: 450px; text-overflow: ellipsis; white-space: nowrap;overflow: hidden;font-family: inherit;font-size: 18px; margin-left: 30px;'>Skills: ". $skills."</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 230px;'>
                                    <a href='".$urlcv."' style='font-family: inherit;font-size: 18px; text-decoration: none; padding: 5% 7%; border: 1px solid #f39830; color: #f39830;'>View detail</a>
                            </td>
                        </tr>
                </table>";
                    if($count>=10){
                        break;
                    }else{
                        $count++;
                    }
                }
                $messagehtml = $dataheader.$data.$datafooter;
                $data=null;
                $check=email_to_user($item, $supportuser, $subject, '', $messagehtml);
            }
        }

    }
    function task_get_employer(){
        global $DB;
        $employers=$DB->get_records('vi_employers',array('published'=>1),'','id');
        $timenow= time();
        $results=array();
        foreach($employers as $item){
            $sqlcountjob="SELECT COUNT(*) FROM {vi_jobs} vj WHERE vj.employer_id=:employer_id AND vj.published=1 AND $timenow>= vj.startdate AND $timenow<=vj.enddate";
            $jobcount=$DB->count_records_sql($sqlcountjob,array('employer_id'=>$item->id));
            if($jobcount>0){
                $employerinfor=$DB->get_record_sql("SELECT u.*,vae.employer_id as employer_id FROM {user} u LEFT JOIN {vi_admin_employers} vae ON u.id=vae.user_id WHERE vae.employer_id=$item->id");
                if($employerinfor){
                    $results[]=$employerinfor;
                }
            }
        }
        return $results;
    }
    function task_get_jobid_uercv($comid){
        global $DB;
        $taskrate=$DB->get_record('vi_rate_send_email_job', array('id'=>2));
        $listcv=array();
       $jobs=$this->task_get_job_company($comid );
       $users=$this->task_get_users();
       foreach($jobs as $job){
           foreach ($users as $user){
               $machinhrate=0;
               foreach($user->districts as $userlocation){
                   if(in_array($userlocation,$job->districts)){
                       $machinhrate+=5;
                       break;
                   }
               }
               foreach($user->skills as $userskill){
                   if(in_array($userskill,$job->skills)){
                       $machinhrate+=5;
                   }
               }
               if($user->eduid==$job->eduid){
                   $machinhrate+=20;
               }
               if($user->kolevelid==$job->kolevelid){
                   $machinhrate+=35;
               }
               if($user->expyearid==$job->expyearid){
                   $machinhrate+=25;
               }
               if($machinhrate>=0){
                   $jobcv= new \stdClass();
                   $jobcv->jobid=  $job->jobid;
                   $jobcv->cvid= $user->cvid;
                   $jobcv->machinhrate=$machinhrate;
                   $listcv[]=$jobcv;
               }
           }
       }
        return $listcv;
    }
    function task_get_job_company($comid){
        global $DB;
        $thistime=time();
        $sql="SELECT vj.title as jobtitl,vj.id as jobid,vfe.id as eduid,vfe.name_en as edu,vkl.id as kolevelid, vkl.name_en as kolevel,vwe.id as expyearid,vwe.name_en as expyear
            FROM {vi_jobs} vj
            JOIN {vi_employers} em ON em.id=vj.employer_id
            JOIN {vi_final_educations} vfe ON vj.final_education_id=vfe.id
            JOIN {vi_korean_levels} vkl ON vj.korean_level_id=vkl.id
            JOIN {vi_work_experiences} vwe ON vj.work_experience_id=vwe.id
            WHERE vj.employer_id=:employer_id AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
            AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))";
        $jobs=$DB->get_records_sql($sql,array('employer_id'=>$comid));

        foreach ($jobs as $job){
            $sqlworkplace="SELECT l.district_id as districtid  FROM  {vi_locations} l JOIN {vi_job_locations} jl ON jl.location_id=l.id WHERE jl.job_id=$job->jobid";
            $districts=$DB->get_records_sql($sqlworkplace);
            $job->districts=array();
            $job->districts=$districts;
            $skills=$DB->get_records('vi_job_skills',array('job_id'=>$job->jobid),'','skill_id');
            $job->skills= array();
            $job->skills= $skills;
        }
        return $jobs;
    }
    function task_get_users(){
        global $DB;
        $sql = "SELECT us.id as userid, re.id as cvid, vufe.final_education_id as eduid,vukl.korean_level_id as kolevelid,vuwe.work_experience_id as expyearid
                FROM {user} us 
                JOIN {vi_resumes} re ON us.id = re.user_id 
                JOIN {vi_user_korean_levels} vukl ON us.id= vukl.user_id
                JOIN {vi_user_final_educations} vufe ON us.id= vufe.user_id
                JOIN {vi_user_work_experiences} vuwe ON us.id= vuwe.user_id
                WHERE re.is_default=1 AND  (:timenow-re.timemodified)<:limittime
                ";
        $timenow=time();
        $limittime=    3 * 30 * 24 * 60 * 60;
        $users=$DB->get_records_sql($sql,array('timenow'=>$timenow,'limittime'=>$limittime));
        foreach ($users as $user){
            $sqlworkplace="SELECT uw.district_id as districtid  FROM  {vi_user_workplaces} uw  WHERE uw.user_id=$user->userid";
            $districts=$DB->get_records_sql($sqlworkplace);
            $user->districts=array();
            $user->districts=$districts;
            $skills=$DB->get_records('vi_user_skills',array('user_id'=>$user->userid),'','skill_id');
            $user->skills= array();
            $user->skills= $skills;
        }
        return $users;
    }
    function task_get_user_final_education($userid){
        global $DB;
        $sql="SELECT fe.id,fe.name_en FROM {vi_user_final_educations} ufe JOIN {vi_final_educations} fe ON ufe.final_education_id=fe.id WHERE ufe.user_id=$userid";
        $result=$DB->get_record_sql($sql);
        return $result;
    }
    function task_get_user_experience($userid){
        global $DB;
        $sql="SELECT we.id,we.name_en FROM {vi_user_work_experiences} uwe JOIN {vi_work_experiences} we ON uwe.work_experience_id=we.id WHERE uwe.user_id=$userid";
        $result=$DB->get_record_sql($sql);
        return $result;
    }
    function task_get_user_korean_levels($userid){
        global $DB;
        $sql="SELECT kl.id,kl.name_en FROM {vi_user_korean_levels} ukl JOIN {vi_korean_levels} kl ON ukl.korean_level_id=kl.id WHERE ukl.user_id=$userid";
        $result=$DB->get_record_sql($sql);
        return $result;
    }
    function task_get_user_skills($userid){
        global $DB;
        $sql="SELECT s.name FROM {vi_user_skills} us JOIN {vi_skills} s ON us.skill_id=s.id WHERE us.user_id=$userid";
        $userskill=$DB->get_records_sql($sql);
        $skill=array();
        foreach($userskill as $us){
            $skill[]=$us->name;
        }
        $result=implode(', ',$skill);
        return $result;
    }
    function task_get_user_locations($userid){
        global $DB;
        $sql="SELECT d.name FROM {vi_user_workplaces} uw JOIN {vi_districts} d ON uw.district_id=d.id WHERE uw.user_id=$userid";
        $userlocation=$DB->get_records_sql($sql);
        $userlo=array();
        foreach($userlocation as $ul){
            $userlo[]=$ul->name;
        }
        $result=implode(', ',$userlo);
          return $result;
    }




}