<?php

namespace local_visang;

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

class mkjobs_theme
{
    public $title = '';
    public $css = array();
    public $js = array();
    public $styles = array();
    public $scripts = array();
    public $bodyclasses = '';
    public $menu = '';

    function addJS($js)
    {
        if (!in_array($js, $this->js)) $this->js[] = $js;
    }

    function addCSS($css)
    {
        if (!in_array($css, $this->css)) $this->css[] = $css;
    }

    function addStyle($style)
    {
        if (!in_array($style, $this->styles)) $this->styles[] = $style;
    }

    function addScript($script)
    {
        if (!in_array($script, $this->scripts)) $this->scripts[] = $script;
    }

    function addNavbar($navbar)
    {
        if (!in_array($navbar, $this->navbar)) $this->navbar[] = $navbar;
    }

    function header($include_content = true)
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/mkj_homepage/header.php');
        if ($include_content) {
            include_once($VISANG->dirroot . '/mkj_homepage/header_content.php');
        }
    }
    function footer($include_content = true)
    {
        global $VISANG, $USER;
        if ($include_content) {
            include_once($VISANG->dirroot . '/mkj_homepage/footer_content.php');
        }
        include_once($VISANG->dirroot . '/mkj_homepage/footer.php');
    }
    function footer1($include_content = true)
    {
        global $VISANG, $USER;
        if ($include_content) {
            include_once($VISANG->dirroot . '/component/footer_content.php');
        }
        include_once($VISANG->dirroot . '/component/footer.php');
    }
    function header1($include_content = true)
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/component/header.php');
        if ($include_content) {
            include_once($VISANG->dirroot . '/component/header_content.php');
        }
    }
    function header_foremployer($include_content = true)
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/mkj_homepage/mkj_employer/header_fe.php');
        if ($include_content) {
            include_once($VISANG->dirroot . '/mkj_homepage/mkj_employer/header_content_fe.php');
        }
    }

    function footer_foremployer($include_content = true)
    {
        global $VISANG, $USER;
        if ($include_content) {
            include_once($VISANG->dirroot . '/mkj_homepage/mkj_employer/footer_content_fe.php');
        }
        include_once($VISANG->dirroot . '/mkj_homepage/mkj_employer/footer_fe.php');
    }

    function header_edit_cv($include_content = true)
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/mkj_homepage/header.php');
    }

    function footer_edit_cv($include_content = true)
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/mkj_homepage/footer.php');
    }

    function table_pagination($url, $params = array(), $total_pages = 1, $current_page = 1, $max_nav = 10)
    {
        $padding = floor($max_nav / 2);
        $page_start = max(1, $current_page - $padding);
        $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

        echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
        if ($current_page > 1) {
            $p = $params;
            $p['page'] = $current_page - 1;
            echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&laquo;</span></a></li>';
        } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
        for ($i = $page_start; $i <= $page_end; $i++) {
            if ($i == $current_page) {
                echo '<li class="active"><span>' . $i . '</span></li>';
            } else {
                $p = $params;
                $p['page'] = $i;
                echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '">' . $i . '</a></li>';
            }
        }
        if ($current_page < $total_pages) {
            $p = $params;
            $p['page'] = $current_page + 1;
            echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&raquo;</span></a></li>';
        } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
        echo \html_writer::end_tag('ul');
    }
}
