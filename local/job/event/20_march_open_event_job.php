<?php
require_once(dirname(__FILE__) . '/../lib.php');
$VISANG->theme->header();
?>  
<div class="img-grp">
    <div class="wrp img01">
        <ul class="link-tp">
            <li>
                <a href="https://www.masterkorean.vn/local/event/20_march_open_event_lms.php">MASTER KOREAN</a>
            </li>
            <li class="on">
                <a href="https://job.masterkorean.vn/local/job/event/20_march_open_event_job.php">MASTER YOUR JOB</a>
            </li>
        </ul>
        <img src="/theme/oklassedu/pix/images/img_evt01.png" class="bg m-hide" alt="" /><!-- pc 이미지 -->
        <img src="/theme/oklassedu/pix/images/img_evt01_m.png" class="bg m-show" alt="" /><!-- mobile 이미지 -->
    </div> 
    <div class="wrp img02">
        <img src="/theme/oklassedu/pix/images/img_evt02.png" class="bg m-hide" alt="" /><!-- pc 이미지 -->
        <img src="/theme/oklassedu/pix/images/img_evt02_m.png" class="bg m-show" alt="" /><!-- mobile 이미지 -->
    </div>
    <div class="wrp video">
        <p class="v-thumb">썸네일</p>
        <iframe width="100%" height="auto" class="m-show" src="https://www.youtube.com/embed/WKnbT83ycK8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //동영상 썸네일 클릭 시 동영상 재생 이벤트
        $(".v-thumb").click(function () {
            $(this).fadeOut("fast");
                $(this).after('<iframe width="100%" height="auto" src="https://www.youtube.com/embed/WKnbT83ycK8?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        });
    });
</script>

<?php
$VISANG->theme->footer();
?>


