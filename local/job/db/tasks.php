<?php
defined('MOODLE_INTERNAL') || die();

$tasks = [
    [
        'classname' => 'local_job\task\cron_task',
        'blocking' => 0,
        'minute' => '0',
        'hour' => '0',
        'day' => '*',
        'month' => '*',
        'dayofweek' => '6',
    ],
    [
        'classname' => 'local_job\task\employer_task',
        'blocking' => 0,
        'minute' => '0',
        'hour' => '12',
        'day' => '*',
        'month' => '*',
        'dayofweek' => '5',
    ],
];
?>