<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

include_once(dirname(__FILE__) . '/jobs.php');

function mkjobs_search_employers($detailinfo = true)
{
    global $DB, $CFG;
    $company_page = optional_param('company_page', 1, PARAM_INT);
    $company_perpage = optional_param('company_perpage', 8, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('searchtext', '', PARAM_RAW_TRIMMED));
    $fields = optional_param('fields', '*', PARAM_RAW);
    $city = optional_param('city', 0, PARAM_RAW);
    $skillgroupid = optional_param('skillgroupid', 0, PARAM_INT);
    $sortorder_sql = '';
    $comname_sortorder = optional_param('comname_sortorder', -1, PARAM_INT);
    if ($comname_sortorder != -1) {
        $sortorder_sql = $comname_sortorder == 1 ? "em.company_name DESC" : "em.company_name ASC";
    } else {
        $sortorder_sql = "em.view_count DESC, em.company_name ASC";
    }

    $sql = array();
    $params = array();
    $searchfields = array('company_name', 'company_short_description', 'company_address');

    foreach ($searchfields as $searchfield) {
        $sql[] = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params[$searchfield] = '%' . $search . '%';
    }
    $sql_join = "FROM {vi_employers} em 
        LEFT JOIN {vi_employer_skill_groups} esg ON em.id = esg.employer_id
        RIGHT JOIN {vi_skill_groups} sg ON esg.skill_group_id=sg.id
    ";

    $offset = ($company_page - 1) * $company_perpage;
    $sql_str = "published = 1 AND (" . implode(" OR ", $sql) . ")";
    if ($skillgroupid != 0) {
        $sql_str = 'esg.skill_group_id=:skillgroupid AND ' . $sql_str;
        $params['skillgroupid'] = $skillgroupid;
    }
    if ($city != 0) {
        $by_district = "SELECT lo.employer_id FROM {vi_locations} lo WHERE lo.district_id = $city";
        $sql_str = "(em.id IN (" . $by_district . ")) AND " . $sql_str;
    }
    $sqlqqqqq="SELECT COUNT(em.id) $sql_join WHERE $sql_str";
    $totalcount = $DB->count_records_sql($sqlqqqqq, $params);
//    $employers = $DB->get_records_select('vi_employers', $sql_str, $params, $sortorder_sql, $fields, $offset, $perpage);
//    $totalcount = $DB->count_records_select('vi_employers', $sql_str, $params, 'COUNT(*)');

    $total_page = ceil($totalcount / $company_perpage);

    if ($company_page > $total_page){
        if($total_page!= 0) {
            $company_page = $total_page;
        }
    }
    else if ($company_page < 1){
        $company_page = 1;
    }

    $offset = ($company_page - 1) * $company_perpage;
    $employers = $DB->get_records_sql("SELECT em.* $sql_join WHERE $sql_str ORDER BY $sortorder_sql LIMIT $company_perpage OFFSET $offset", $params);


    $employers_list = array();
    $thistime = time();
    foreach ($employers as $employer) {
        $e = $employer;
        if ($detailinfo) {
            $e->company_logo = mkjobs_get_employer_logo($e->company_logo);
            $e->location = mkjobs_get_employer_location($employer->id);

            $_job_cnt_sql = "SELECT COUNT(*) FROM {vi_jobs} j WHERE j.employer_id=:employerid AND j.published = 1 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))";
            $e->job_cnt = $DB->count_records_sql($_job_cnt_sql, array("employerid" => $employer->id));
        }

        $employers_list[] = $e;
    }
    return array('data' => $employers_list, 'total' => $totalcount, 'search' => $search, 'city' => $city, 'skillgroupid' => $skillgroupid, 'total_page' => $total_page, 'company_page' => $company_page);
}

function mkjobs_get_employer_logo($logo)
{
    global $CFG;
    return $logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $logo;
}

function mkjobs_get_employer_info($employerid)
{
    global $DB;
    $employer = $DB->get_record('vi_employers', array('id' => $employerid));
    if ($employer == null || $employer->published != 1) {
        throw new moodle_exception('notfound');
    }

    $employer->jobs = mkjobs_get_employer_jobs($employer);

    $employer->company_logo = mkjobs_get_employer_logo($employer->company_logo);

    $employer->company_type = $DB->get_record('vi_company_types', array('id' => $employer->company_type_id), 'id,name,short_name');
    $employer->company_size = $DB->get_record('vi_company_sizes', array('id' => $employer->company_size_id), 'id,name,short_name');
    $employer->country = $DB->get_record('vi_countries', array('id' => $employer->country_id), 'id,name,short_name');
    // $employer->working_date_category = $DB->get_record('vi_working_date_categories', array('id' => $employer->working_date_category_id), 'id,name,short_name');
    // $employer->overtime_category = $DB->get_record('vi_overtime_categories', array('id' => $employer->overtime_category_id), 'id,name,short_name');

    $employer->locations = $DB->get_records('vi_locations', array('employer_id' => $employerid), 'full_address ASC');

    $employer->skill_groups = mkjobs_get_employer_skill_groups($employerid);

    return $employer;
}

function mkjobs_has_follow_employer($employerid)
{
    global $DB, $VISANG, $USER;
    if (isloggedin()) {
        return $DB->count_records('vi_follow_employers', array('employer_id' => $employerid, 'user_id' => $USER->id)) > 0;
    }
    return false;
}

function mkjobs_follow_employer()
{
    global $DB, $VISANG, $USER;
    $employerid = required_param('employerid', PARAM_INT);
    if (isloggedin()) {
        if (mkjobs_has_follow_employer($employerid)) {
            $DB->delete_records('vi_follow_employers', array('employer_id' => $employerid, 'user_id' => $USER->id));
            return 0;
        } else {
            if ($DB->count_records('vi_follow_employers', array('user_id' => $USER->id)) == 5) {
                return 2;
            }

            $follow = new stdClass();
            $follow->employer_id = $employerid;
            $follow->user_id = $USER->id;
            $follow->timecreated = time();
            $follow->timemodified = time();

            $DB->insert_record('vi_follow_employers', $follow);
            return 1;
        }
    }
    return -1;
}

function mkjobs_get_follow_employers($userid)
{
    global $DB, $VISANG, $USER;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 20, PARAM_INT);

    $sql = "FROM {vi_follow_employers} f
            JOIN {vi_employers} e ON f.employer_id = e.id
            WHERE f.user_id=:userid AND e.published = 1
            ORDER BY f.timecreated DESC, e.company_name ASC";

    $offset = ($page - 1) * $perpage;

    $follow_employers = $DB->get_records_sql("SELECT f.* " . $sql, array('userid' => $USER->id), $offset, $perpage);
    $totalcount = $DB->count_records_sql("SELECT COUNT(*) " . $sql, array('userid' => $USER->id));

    $thistime = time();

    $employers = array();
    foreach ($follow_employers as $follow_employer) {
        $e = $DB->get_record('vi_employers', array('id' => $follow_employer->employer_id));
        if ($e) {
            $e->company_logo = mkjobs_get_employer_logo($e->company_logo);
            $e->location = mkjobs_get_employer_location($e->id);

            $_job_cnt_sql = "SELECT COUNT(*) FROM {vi_jobs} j WHERE j.employer_id=:employerid AND j.published = 1 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))";
            $e->job_cnt = $DB->count_records_sql($_job_cnt_sql, array("employerid" => $e->id));

            $e->has_follow_employer = 0;
            if (isloggedin()) {
                $e->has_follow_employer = 1;
            }

            $employers[] = $e;
        }
    }
    return array('data' => $employers, 'total' => $totalcount);
}

function mkjobs_search_company_types()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();
    if ($search != '') {
        $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params = array($searchfield => '%' . $search . '%');
    }
    $offset = ($page - 1) * $perpage;
    $company_types = $DB->get_records_select('vi_company_types', $sql, $params, '{vi_company_types}.name ASC', $fields, $offset, $perpage);

    return $company_types;
}

function mkjobs_save_employer_review($userid, $employerid, $ratings)
{
    global $DB;

    $thistime = time();

    $review = new stdClass();
    $review->user_id = $userid;
    $review->employer_id = $employerid;
    $review->timecreated = $thistime;
    $review->timemodified = $thistime;
    $review_id = $DB->insert_record('vi_reviews', $review);

    foreach ($ratings as $rating) {
        $review_rating = new stdClass();
        $review_rating->review_id = $review_id;
        $review_rating->rating_id = $rating['id'];
        $review_rating->point = $rating['point'];
        $review_rating->timecreated = $thistime;
        $review_rating->timemodified = $thistime;
        $DB->insert_record('vi_review_ratings', $review_rating);
    }
}

function mkjobs_get_employer_ratings($employerid, $userid = 0)
{
    global $DB;
    $ratings = array();
    $lang = current_language();
    $where = "rv.employer_id=$employerid";
    if ($userid != 0) $where .= " AND rv.user_id=$userid";
    $query = "SELECT vr.id, vr.title_$lang as title, AVG(rvr.point) as rate
                FROM {vi_ratings} vr
                LEFT OUTER JOIN {vi_review_ratings} rvr ON rvr.rating_id = vr.id
                LEFT OUTER JOIN {vi_reviews} rv ON rvr.review_id = rv.id
                WHERE $where
                GROUP BY vr.id
                ORDER BY vr.sortorder";
    $ratings = $DB->get_records_sql($query);
    $rate = 0.00;
    if (count($ratings)) {
        foreach ($ratings as $rating) {
            $rate += floatval($rating->rate);
        }
        $rate = $rate / count($ratings);
    } else {
        $ratings = $DB->get_records_sql("SELECT vr.id, vr.title_$lang as title, COALESCE(0) as rate
                                        FROM {vi_ratings} vr
                                        ORDER BY vr.sortorder");
    }
    return array('ratings' => $ratings, 'rate' => $rate);
}

function mkjobs_get_employer_reviews($employerid, $limit = 10)
{
    global $DB;
    $lang = current_language();
    $query = "FROM {vi_reviews} rv
                LEFT OUTER JOIN {user} u ON rv.user_id = u.id
                WHERE rv.employer_id=$employerid
                ORDER BY rv.timecreated DESC";
    $reviews = $DB->get_records_sql("SELECT rv.*, u.firstname as user_firstname " . $query . " LIMIT " . $limit);
    $totalcount = $DB->count_records_sql("SELECT COUNT(*) " . $query);
    foreach ($reviews as $review) {
        $review->ratings = mkjobs_get_employer_ratings($employerid, $review->user_id);
        $review->timecreated = mkjobs_notifi_time($review->timecreated);
    }
    return array('reviews' => $reviews, 'total' => $totalcount);
}
