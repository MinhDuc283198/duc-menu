<?php
defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $DB, $USER;

require_once($CFG->dirroot . '/local/popup/lib.php');
require_once($CFG->libdir . '/filelib.php');

$where = "";
$site = 1; // Jobs(personal)
$popups = $DB->get_records_select('popup', "(timeavailable < ? or timeavailable=0) and (timedue > ? or timedue=0 ) and isactive = 1 and site = ? " . $where, array(time(), time(), $site));
?>

<script type="text/javascript">
    function getCookie(name) {
        var Found = false;
        var start, end;
        var i = 0;
        while (i <= document.cookie.length) {
            start = i;
            end = start + name.length;
            if (document.cookie.substring(start, end) == name) {
                Found = true;
                break;
            }
            i++;
        }

        if (Found) {
            start = end + 1;
            end = document.cookie.indexOf(";", start);
            if (end < start)
                end = document.cookie.length;
            return document.cookie.substring(start, end);
        }
        return "";
    }

    var newwin = null;

    function openPopup(url, name, param) {
        var noticeCookie = getCookie(name);
        if (noticeCookie != "no") {

            newwin = window.open(url, name, param);
            newwin.focus();
        }
    }

    function setCookie(name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + 1);
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
    }

    function closeWin(name, value, expiredays) {
        //if ( document.popupform.confirmed.checked ) 
        setCookie(name, value, expiredays);
        //self.close(); 
        document.getElementById(name).style.visibility = 'hidden';
    }

    function closepopup(name) {
        //self.close(); 
        document.getElementById(name).style.visibility = 'hidden';
    }


    $(document).ready(function() {
        $('.main_popup_header').each(function() {
            var $el = $(this);
            var $parent = $el.parent();

            var dragging = false;
            var startY = 0;
            var startX = 0;
            var startT = 0;
            var startL = 0;

            $el.mousedown(function(ev) {
                dragging = true;
                startY = ev.clientY;
                startX = ev.clientX;
                startT = $parent.css('top');
                startL = $parent.css('left');
            });

            $(window).mousemove(function(ev) {
                if (dragging) {
                    // calculate new top
                    var newTop = parseInt(startT) + (ev.clientY - startY);
                    var newLeft = parseInt(startL) + (ev.clientX - startX);

                    $parent.css('top', newTop);
                    $parent.css('left', newLeft);
                }
            }).mouseup(function() {
                dragging = false;
            });

        });
    });
</SCRIPT>


<?php
$pageurl = $_SERVER['SCRIPT_NAME'];
$popuppage = "";
switch ($pageurl) {
    case "/lmsdata/index.php":
        $popuppage = "1";
        break;
    default:
        $popuppage = "2";
        break;
}
$context = get_system_context();
foreach ($popups as $popup) {
    if (isset($_COOKIE['popup_' . $popup->id]) || $popuppage != $popup->type) {
    } else {
?>
        <div class="layerpop main-pop" id="<?php echo "popup_" . $popup->id; ?>" style="width:<?php echo $popup->popupwidth; ?>px; height:<?php echo $popup->popupheight; ?>px; left:<?php echo $popup->popupx; ?>px; top:<?php echo $popup->popupy; ?>px;">

            <div class="pop-title">
                <?php echo $popup->title; ?><a href="#" onclick="<?php echo 'closepopup(\'popup_' . $popup->id . '\')' ?>">
                    <a href="#" class="pop-close">닫기</a>
            </div>
            <div class="pop-contents" style="<?php if ($popup->availablescroll == 1) { ?>overflow: auto; <?php } ?> height:<?php echo $popup->popupheight - 46; ?>px; ">
                <?php echo file_rewrite_pluginfile_urls($popup->description, 'pluginfile.php', $context->id, 'local_popup', 'popup', $popup->id); ?>
            </div>
            <div class="ft">
                <form class="popup_btn_area">
                    <?php
                    if ($popup->cookieday > 0) {
                        echo '<label class="custom-ck"><input type="checkbox" name="confirmed" title="confirmed" id="confirmed" onclick="closeWin(\'popup_' . $popup->id . '\', \'no\', \'' . $popup->cookieday . '\')"> <span>' . get_string("closepopupdays", "local_popup", $popup->cookieday) . "</span></label>";
                    }
                    ?>
                    <a href="#" class="pop-close">닫기</a>
                </form>

            </div>
        </div>

<?php
    }
}
?>