<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

include_once(dirname(__FILE__) . '/common_functions.php');
include_once(dirname(__FILE__) . '/skills.php');
include_once(dirname(__FILE__) . '/locations.php');

function mkjobs_search_jobs()
{
    global $DB, $CFG;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 20, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('searchtext', '', PARAM_RAW_TRIMMED));
    $fields = optional_param('fields', '*', PARAM_RAW);
    $city = optional_param('city', 0, PARAM_RAW);

    $sql_select = "FROM {vi_jobs} j
                    LEFT JOIN {vi_job_skills} js ON js.job_id = j.id 
                    LEFT JOIN {vi_employers} em ON j.employer_id = em.id 
                    LEFT JOIN {vi_skill_groups} vsg ON j.skill_group_id = vsg.id
                    LEFT JOIN {vi_job_locations} jl ON jl.job_id = j.id
                    LEFT JOIN {vi_locations} l ON jl.location_id = l.id
                    LEFT JOIN {vi_districts} d ON l.district_id = d.id
                    ";

    $skillgroupid = optional_param('skillgroupid', 0, PARAM_INT);
    $skillid = optional_param('skillid', 0, PARAM_INT);

    $sortorder_sql = '';
    $job_sortorder = optional_param('job_sortorder', '', PARAM_RAW);
    if (in_array($job_sortorder, array('register', 'deadline'))) {
        $sortorder_sql = $job_sortorder == 'register' ? 'j.timecreated DESC' : 'j.enddate ASC';
    } else {
        $sortorder_sql = 'j.view_count DESC, j.timecreated ASC, j.enddate ASC';
    }

    $sql = array();
    $params = array();
    $searchfields = array('title', 'short_description');

    foreach ($searchfields as $searchfield) {
        $sql[] = $DB->sql_like('j.'.$searchfield, ':' . $searchfield, false);
        $params[$searchfield] = '%' . $search . '%';
    }
    $sql[]=$DB->sql_like('em.company_name',':company_name',false);
    $params['company_name']='%'.$search.'%';

    $thistime = time();


    $sql_str = "j.published = 1 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate)) AND (" . implode(" OR ", $sql) . ")";
    if ($skillgroupid != 0) {
        $sql_str = 'j.skill_group_id=:skillgroupid AND ' . $sql_str;
        $params['skillgroupid'] = $skillgroupid;
    }
    if ($skillid != 0) {
        $sql_str = 'js.skill_id=:skillid AND ' . $sql_str;
        $params['skillid'] = $skillid;
    }
//    if ($category != 0) {
//        $sql_str = 'ct.id=:category AND ' . $sql_str;
//        $params['category'] = $category;
//    }
    if ($city != 0) {
        $sql_str = 'd.id=:city AND ' . $sql_str;
        $params['city'] = $city;
    }

    $totalcount = $DB->count_records_sql("SELECT COUNT(DISTINCT j.id) $sql_select WHERE $sql_str ORDER BY $sortorder_sql", $params);

    $total_page = ceil($totalcount / $perpage);

    if ($page > $total_page){
        if($total_page!= 0) {
            $page = $total_page;
        }
    }
    else if ($page < 1){
        $page = 1;
    }

    $offset = ($page - 1) * $perpage;
    $jobs = $DB->get_records_sql("SELECT j.* $sql_select WHERE $sql_str ORDER BY $sortorder_sql LIMIT $perpage OFFSET $offset", $params);


    $jobs_list = array();
    foreach ($jobs as $job) {
        $jobinfo = $DB->get_record('vi_jobs', array('id' => $job->id));
        $comInfo = $DB->get_record("vi_employers", array('id' => $jobinfo->employer_id));
        if($comInfo->confirmed == 1 && $comInfo->published == 1 && $comInfo->deleted == 0){
            $jobs_list[] = mkjobs_get_job_detail($job);
        }else{
            $totalcount--;
        }
    }
    return array('data' => $jobs_list, 'total' => $totalcount, 'search' => $search, 'city' => $city,  'skillgroupid' => $skillgroupid, 'skillid' =>$skillid, 'total_page' => $total_page, 'page' => $page, 'job_sortorder' => $job_sortorder);
}

function mkjobs_get_job_detail($job, $employer = null)
{
    global $DB, $CFG, $USER;
    $j = $job;
    if ($employer == null) {
        $employer = $DB->get_record('vi_employers', array('id' => $job->employer_id));
    }

    $j->company_logo = $employer->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $employer->company_logo;
    $j->company_name = $employer->company_name;
    $j->company_type = $DB->get_record('vi_company_types', array('id' => $employer->company_type_id), 'id,name,short_name');
    $j->company_size = $DB->get_record('vi_company_sizes', array('id' => $employer->company_size_id), 'id,name,short_name');
    $j->country = $DB->get_record('vi_countries', array('id' => $employer->country_id), 'id,name,short_name');
    $j->working_date_category = $DB->get_record('vi_working_date_categories', array('id' => $employer->working_date_category_id), 'id,name,short_name');
    // $j->overtime_category = $DB->get_record('vi_overtime_categories', array('id' => $employer->overtime_category_id), 'id,name,short_name');
    $j->skill_group = $DB->get_record('vi_skill_groups', array('id' => $job->skill_group_id), 'id,name,short_name');

    $thistime = time();
    $_job_cnt_sql = "SELECT COUNT(*) FROM {vi_jobs} j WHERE j.employer_id=:employerid AND j.published = 1 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))";
    $j->company_job_cnt = $DB->count_records_sql($_job_cnt_sql, array("employerid" => $employer->id));

    $salary = $DB->get_record('vi_job_salary_categories', array('id' => $job->job_salary_category_id));
    $j->salary = $salary ? $salary->name : '';

    $j->job_reasons = mkjobs_get_job_reasons($job->id);

    $j->skills = mkjobs_get_job_skills($job->id);
//    $j->timecreated = mkjobs_notifi_time($j->timecreated);
    $j->timenotificreated = mkjobs_notifi_time_created($job->timecreated, $job->timemodified, $job->enddate);
    $j->location = mkjobs_get_job_location($job->id);
    $j->locations = mkjobs_get_job_locations($job->id);

    // $j->d_enddate = date('\D-d', $j->enddate-$thistime);
    $daysleft = round(((($j->enddate - $thistime) / 24) / 60) / 60);
    $j->d_enddate = $daysleft > 0 ? 'D-' . $daysleft : '';


    $j->has_saved_job = 0;
    if (isloggedin() && mkjobs_has_saved_job($job->id)) {
        $j->has_saved_job = 1;
    }

    return $j;
}

function mkjobs_get_employer_jobs($employer, $limitnum = 0)
{
    global $DB;
    $thistime = time();
    $employerid = $employer->id;
    $sql_str = "employer_id = $employerid AND published = 1 AND ((startdate=0)OR(startdate < $thistime AND $thistime < enddate))";
    $jobs = $DB->get_records_select('vi_jobs', $sql_str, array(), '{vi_jobs}.timecreated DESC', '*', 0, $limitnum);
    $jobs_list = array();
    foreach ($jobs as $job) {
        $jobs_list[] = mkjobs_get_job_detail($job, $employer);
    }
    return $jobs_list;
}

function mkjobs_get_job_reasons($jobid)
{
    global $DB;
    $sql = "SELECT cat.*
    FROM {vi_reason_categories} cat
    JOIN {vi_job_reasons} j ON cat.id = j.reason_category_id
    WHERE j.job_id = :jobid
    ORDER BY cat.name ASC";
    return $DB->get_records_sql($sql, array('jobid' => $jobid));
}

function mkjobs_get_job_recommend_by_skills($job)
{
    global $DB;
    $jobid = $job->id;
    $skill_ids = array();
    foreach ($job->skills as $skill) {
        $skill_ids[] = $skill->id;
    }

    list($dsql, $dparams) = $DB->get_in_or_equal($skill_ids, SQL_PARAMS_NAMED);

    $thistime = time();
    $sql = "SELECT j.*
            FROM {vi_jobs} j
            JOIN {vi_job_skills} js ON j.id = js.job_id
            JOIN {vi_employers} em ON em.id = j.employer_id
            WHERE js.skill_id $dsql AND j.id <> $jobid AND j.published = 1 
                AND em.published = 1 AND em.confirmed=1 AND em.deleted=0 
                AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))
            GROUP BY j.id
            ORDER BY j.timecreated DESC
            LIMIT 3";

    $results = $DB->get_records_sql($sql, $dparams);

    $jobs_list = array();
    foreach ($results as $res) {
        $jobs_list[] = mkjobs_get_job_detail($res);
    }
    return $jobs_list;
}

function mkjobs_get_job_recommend_by_skills_all($job)
{
    global $DB;
    $jobid = $job->id;
    $skill_ids = array();
    if(count($job->skills)>0){
        foreach ($job->skills as $skill) {
            $skill_ids[] = $skill->id;
        }

        list($dsql, $dparams) = $DB->get_in_or_equal($skill_ids, SQL_PARAMS_NAMED);

        $thistime = time();
        $sql = "SELECT j.*
            FROM {vi_jobs} j
            JOIN {vi_job_skills} js ON j.id = js.job_id
            JOIN {vi_employers} em ON em.id = j.employer_id
            WHERE js.skill_id $dsql AND j.id <> $jobid AND j.published = 1 
                AND em.published = 1 AND em.confirmed=1 AND em.deleted=0 
                AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))
            GROUP BY j.id
            ORDER BY j.timecreated DESC LIMIT 5";

        $results = $DB->get_records_sql($sql, $dparams);

        $jobs_list = array();
        foreach ($results as $res) {
            $jobs_list[] = mkjobs_get_job_detail($res);
        }
    }
    return $jobs_list;
}

function mkjobs_has_saved_job($jobid)
{
    global $DB, $VISANG, $USER;
    if (isloggedin()) {
        return $DB->count_records('vi_saved_jobs', array('job_id' => $jobid, 'user_id' => $USER->id)) > 0;
    }
    return false;
}

function mkjobs_saved_job()
{
    global $DB, $VISANG, $USER;
    $jobid = required_param('jobid', PARAM_INT);
    if (isloggedin()) {
        if (mkjobs_has_saved_job($jobid)) {
            $DB->delete_records('vi_saved_jobs', array('job_id' => $jobid, 'user_id' => $USER->id));
            return 0;
        } else {
            $sj = new stdClass();
            $sj->job_id = $jobid;
            $sj->user_id = $USER->id;
            $sj->timecreated = time();
            $sj->timemodified = time();

            $DB->insert_record('vi_saved_jobs', $sj);
            return 1;
        }
    }
    return -1;
}

function mkjobs_get_saved_jobs($userid)
{
    global $DB, $VISANG, $USER;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 20, PARAM_INT);

    $thistime = time();

    $sql = "FROM {vi_saved_jobs} sj
            JOIN {vi_jobs} j ON sj.job_id = j.id
            WHERE sj.user_id=:userid AND j.published = 1 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))
            ORDER BY sj.timecreated DESC";

    $offset = ($page - 1) * $perpage;

    $saved_jobs = $DB->get_records_sql("SELECT sj.* " . $sql, array('userid' => $USER->id), $offset, $perpage);
    $totalcount = $DB->count_records_sql("SELECT COUNT(*) " . $sql, array('userid' => $USER->id));

    $jobs_list = array();
    foreach ($saved_jobs as $saved_job) {
        $job = $DB->get_record('vi_jobs', array('id' => $saved_job->job_id));
        $jobs_list[] = mkjobs_get_job_detail($job);
    }
    return array('data' => $jobs_list, 'total' => $totalcount);
}
