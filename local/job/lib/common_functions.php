<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

function mkjobs_notifi_time($btime)
{
    $thistime = time();

    if (($thistime - 86400) < $btime) {
        $diff = $thistime - $btime;
        $s = 60; //1분 = 60초
        $h = $s * 60; //1시간 = 60분
        $d = $h * 24; //1일 = 24시간
        $y = $d * 10; //1년 = 1일 * 10일

        if ($diff < $s) {
            $time = get_string('notifi_time:second' . ($diff >= 2 ? 's' : ''), 'local_job', sprintf("%02d", $diff));
        } elseif ($h > $diff && $diff >= $s) {
            $time = get_string('notifi_time:minute' . (round($diff / $s) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $s)));
        } elseif ($d > $diff && $diff >= $h) {
            $time = get_string('notifi_time:hour' . (round($diff / $h) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $h)));
        } elseif ($y > $diff && $diff >= $d) {
            $time = get_string('notifi_time:day' . (round($diff / $d) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $d)));
        } else {
            $time = date('Y.m.d.', strtotime($btime));
        }
    } else {
        $time = date('Y.m.d', $btime);
    }

    return $time;
}

function mkjobs_notifi_time_created($timecreated, $timemodified, $enddate)
{
    if (!empty($timemodified)) {
        $btime = $timemodified;
    } else {
        $btime = $timecreated;
    }
    $thistime = time();
    if ($thistime < $enddate) {
        $diff = $thistime - $btime;
        $s = 60; //1분 = 60초
        $h = $s * 60; //1시간 = 60분
        $d = $h * 24; //1일 = 24시간
        $w = $d * 7;
        $m = $d * 30;
        $y = $d * 365;

//        $y = $d * 10; //1년 = 1일 * 10일

        if ($diff < $s) {
            $time = get_string('notifi_time:second' . ($diff >= 2 ? 's' : ''), 'local_job', sprintf("%02d", $diff));
        } elseif ($h > $diff && $diff >= $s) {
            $time = get_string('notifi_time:minute' . (round($diff / $s) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $s)));
        } elseif ($d > $diff && $diff >= $h) {
            $time = get_string('notifi_time:hour' . (round($diff / $h) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $h)));
        } elseif ($w > $diff && $diff >= $d) {
            $time = get_string('notifi_time:day' . (round($diff / $d) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $d)));
        } elseif ($m > $diff && $diff >= $w) {
            $time = get_string('notifi_time:week' . (round($diff / $w) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $w)));
        } elseif ($y > $diff && $diff >= $m) {
            $time = get_string('notifi_time:month' . (round($diff / $m) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $m)));
        } else {
            $time = get_string('notifi_time:year' . (round($diff / $y) >= 2 ? 's' : ''), 'local_job', sprintf("%02d", round($diff / $y)));
        }
    } else {
        $time = get_string('DayEnd', 'local_job');
    }

    return $time;
}

function mkjobs_get_notifications()
{
    global $DB, $USER, $CFG;
    $notifications = array();
    if (isloggedin()) {
        require_once($CFG->dirroot . '/message/lib.php');
        $notifications = $DB->get_records_sql('SELECT * FROM {message} WHERE useridto = :useridto ORDER BY timecreated DESC limit 2', array('useridto' => $USER->id));
    }
    return $notifications;
}

function mkjobs_count_notifications()
{
    global $DB, $USER, $CFG;
    $ma_cnt = 0;
    if (isloggedin()) {
        require_once($CFG->dirroot . '/message/lib.php');
        $ma_cnt_sql = "select count(*) from {message} where useridto = :user_id";
        $ma_cnt = $DB->count_records_sql($ma_cnt_sql, array("user_id" => $USER->id));
    }
    return $ma_cnt;
}

function mkjobs_get_searchtext($site = 'elearning')
{
    global $DB;
    $divide = $site == 'jobs' ? 1 : 0;
    $sql = "SELECT ms.id,ms.title,ms.titleen,ms.titlevn, ms.isused, ms.timecreated, ms.divide FROM {lmsdata_manage_search} ms WHERE ms.isused = 1 and divide = $divide ORDER BY id DESC";
    $searchtext = $DB->get_records_sql($sql, array());
    return $searchtext;
}

function mkjobs_get_banners($position = 'top')
{
    global $DB, $CFG;
    $site = 1; // default for jobs site
    $thistime = time();
    $category = 0;
    switch ($position) {
        case 'bottom':
            $category = 1;
            break;
    }

    $sql = "SELECT * FROM {lmsdata_banner} WHERE isused = 1 AND site=:site AND category=:category AND ((startdate=0)OR(startdate < $thistime AND $thistime < enddate))  ORDER BY orderby ASC";
    $banners = $DB->get_records_sql($sql, array('site' => $site, 'category' => $category));

    $paths = array();
    $fs = get_file_storage();
    // $filearea = core_useragent::get_device_type() == 'mobile' ? 'bannermobi' : 'banner';
    foreach ($banners as $banner) {
        $path = new stdClass();
        $path->id = $banner->id;
        $path->search = $banner->search;
        $path->linkurl = $banner->linkurl;
        $path->colorcode = $banner->colorcode;

        $files = $fs->get_area_files(1, 'local_lmsdata', 'banner', $banner->id, '', false);
        if ($files) {
            foreach ($files as $file) {
                $filename = $file->get_filename();
                $path->path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . 1 . '/local_lmsdata/banner/' . $banner->id . '/' . $filename);
            }
        }

        $files = $fs->get_area_files(1, 'local_lmsdata', 'bannermobi', $banner->id, '', false);
        if ($files) {
            foreach ($files as $file) {
                $filename = $file->get_filename();
                $path->path_modile = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . 1 . '/local_lmsdata/bannermobi/' . $banner->id . '/' . $filename);
            }
        }

        $paths[] = $path;
    }

    return $paths;
}

function mkjobs_get_popular_jobs($limit = 20)
{
    global $DB;
    $query = "SELECT vj.*,mj.timecreated as maintime, ve.company_name, ve.company_logo, mj.id as mainid
                FROM {lmsdata_main_job} mj
                JOIN {vi_jobs} vj on vj.id = mj.jobid
                JOIN {vi_employers} ve on vj.employer_id = ve.id WHERE mj.type = 0
                ORDER BY RAND()
                LIMIT $limit";
    return $DB->get_records_sql($query);
}

function mkjobs_get_all_location()
{
    global $DB;
    $sql = "SELECT vd.* FROM {vi_districts} vd ORDER  BY vd.name ASC";

    return $DB->get_records_sql($sql);
}

function mkjobs_get_all_categories()
{
    global $DB;
    $sql = "SELECT vsg.* FROM {vi_skill_groups} vsg WHERE vsg.name IS  NOT NULL ORDER  BY  vsg.name ASC";

    return $DB->get_records_sql($sql);
}

function mkjobs_get_all_job($limit = 40)
{
    global $DB, $USER;
    $jobs = array();
    $thistime = time();

    $jobs = $DB->get_records_sql(
        "SELECT vj.*, ve.company_name, ve.company_logo, we.name_vi, d.name as company_location, sa.name as job_salary
            FROM {vi_jobs} vj
            JOIN {vi_employers} em ON em.id = vj.employer_id
            JOIN {vi_employers} ve on vj.employer_id = ve.id
            LEFT JOIN {vi_work_experiences} we on vj.work_experience_id = we.id
            LEFT JOIN {vi_job_locations} jl ON jl.job_id = vj.id
            LEFT JOIN {vi_locations} l ON l.id = jl.location_id
            LEFT JOIN {vi_districts} d ON d.id = l.district_id
            LEFT JOIN {vi_job_salary_categories} sa ON sa.id = vj.job_salary_category_id
            WHERE vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
            AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
            ORDER BY RAND()
            LIMIT $limit"
    );

    return $jobs;
}

function mkjobs_get_jobmatching_star($job, $user_workplaces, $user_fields, $user_salary)
{
    $matchingstar = new stdClass();
    $star = 0;
    $percentstar = 0;
    if (in_array($job->skill_group_id, $user_fields)) {
        $star += 25;
    }
    if (in_array($job->district_id, $user_workplaces)) {
        $star += 35;
    }

    $star += 40;

    if ($star <= 20) {
        $percentstar = $star * 8 / 10;
    } elseif ($star > 20 && $star <= 40) {
        $percentstar = 20 + ($star - 20) * 8 / 10;
    } elseif ($star > 40 && $star <= 60) {
        $percentstar = 40 + ($star - 40) * 8 / 10;
    } elseif ($star > 60 && $star <= 80) {
        $percentstar = 60 + ($star - 60) * 8 / 10;
    } else {
        $percentstar = 80 + ($star - 80) * 8 / 10;
    }

    $matchingstar->star = $star;
    $matchingstar->percentstar = $percentstar;

    return $matchingstar;
}

function mkjobs_get_recommend_jobs($isloggedin = false, $limit = 20)
{
    global $DB, $USER;
    $jobs = array();
    $thistime = time();

    $user_workplaces = $DB->get_fieldset_select('vi_user_workplaces', 'district_id', 'user_id = :user_id', array('user_id' => $USER->id));
    $user_fields = $DB->get_fieldset_select('vi_user_skill_groups', 'skill_group_id', 'user_id = :user_id', array('user_id' => $USER->id));
    $user_salary = 2;

    if ($isloggedin) {
        $userid = $USER->id;
        $by_skills = "SELECT vj.id 
                    FROM {vi_jobs} vj
                    JOIN {vi_employers} em ON em.id = vj.employer_id
                    JOIN {vi_job_skills} js ON js.job_id=vj.id
                    JOIN {vi_user_skills} us ON js.skill_id=us.skill_id
                    WHERE us.user_id=$userid AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
                    AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                    GROUP BY vj.id";

        $by_groups = "SELECT vj.id 
                    FROM {vi_jobs} vj
                    JOIN {vi_employers} em ON em.id = vj.employer_id
                    JOIN {vi_user_skill_groups} usg ON vj.skill_group_id = usg.skill_group_id
                    WHERE usg.user_id=$userid AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
                    AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                    GROUP BY vj.id";

        $by_workplaces = "SELECT vj.id 
                    FROM {vi_jobs} vj
                    JOIN {vi_employers} em ON em.id = vj.employer_id
                    JOIN {vi_job_locations} jl ON jl.job_id = vj.id 
                    JOIN {vi_locations} l ON jl.location_id = l.id 
                    JOIN {vi_user_workplaces} uw ON l.district_id = uw.district_id
                    WHERE uw.user_id=$userid AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
                    AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                    GROUP BY vj.id";

        $user_final_education = $DB->get_record('vi_user_final_educations', array('user_id' => $userid));
        $user_work_experience = $DB->get_record('vi_user_work_experiences', array('user_id' => $userid));
        $user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $userid));

        $where = array("vj.id IN (" . $by_skills . ")", "vj.id IN (" . $by_groups . ")", "vj.id IN (" . $by_workplaces . ")");
        if ($user_final_education) $where[] = 'vj.final_education_id=' . $user_final_education->final_education_id;
        if ($user_work_experience) $where[] = 'vj.work_experience_id=' . $user_work_experience->work_experience_id;
        if ($user_korean_level) $where[] = 'vj.korean_level_id=' . $user_korean_level->korean_level_id;

        $jobs = $DB->get_records_sql(
            "SELECT vj.*, ve.company_name, ve.company_logo, we.name_vi, d.name as company_location, sa.name as job_salary, d.id as district_id
            FROM {vi_jobs} vj
            JOIN {vi_employers} em ON em.id = vj.employer_id
            JOIN {vi_employers} ve on vj.employer_id = ve.id
            LEFT JOIN {vi_work_experiences} we on vj.work_experience_id = we.id
            LEFT JOIN {vi_job_locations} jl ON jl.job_id = vj.id
            LEFT JOIN {vi_locations} l ON l.id = jl.location_id
            LEFT JOIN {vi_districts} d ON d.id = l.district_id
            LEFT JOIN {vi_job_salary_categories} sa ON sa.id = vj.job_salary_category_id
            WHERE vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
            AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate)) AND (" . implode(" OR ", $where) . ")
            ORDER BY RAND()
            LIMIT $limit"
        );
    }

    if (!$isloggedin || count($jobs) == 0) {
        $query = "SELECT vj.*,mj.timecreated as maintime, ve.company_name, ve.company_logo, mj.id as mainid,mj.recommended, we.name_vi, d.name as company_location, sa.name as job_salary, d.id as district_id
                FROM {lmsdata_main_job} mj
                JOIN {vi_jobs} vj on vj.id = mj.jobid
                JOIN {vi_employers} ve on vj.employer_id = ve.id 
                LEFT JOIN {vi_work_experiences} we on vj.work_experience_id = we.id
                LEFT JOIN {vi_job_locations} jl ON vj.id = jl.job_id 
                LEFT JOIN {vi_locations} l ON jl.location_id = l.id
                LEFT JOIN {vi_districts} d ON l.district_id = d.id
                LEFT JOIN {vi_job_salary_categories} sa ON sa.id = vj.job_salary_category_id
                WHERE ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))
                ORDER BY RAND()
                LIMIT $limit";
        $jobs = $DB->get_records_sql($query);
    }

//    if (count($jobs) == 0 || $isloggedin == false) {
//        $jobs = mkjobs_get_popular_jobs($limit);
//    }

    foreach ($jobs as $job) {
        $job->timenotificreated = mkjobs_notifi_time_created($job->timecreated, $job->timemodified, $job->enddate);
        $job->matchingstar = mkjobs_get_jobmatching_star($job, $user_workplaces, $user_fields, $user_salary);
    }

    return $jobs;
}

function mkjobs_get_location_recommend_jobs($employer_id)
{
    global $DB;
    $query = "SELECT vl.* FROM {vi_locations} vl WHERE vl.employer_id = $employer_id";
    return $DB->get_record_sql($query);
}

function mkjobs_get_recommend_courses()
{
    global $DB, $CFG;
    $type = 2; // default: 4 - site management > management to promotion > job & e-learning
    if (!isloggedin()) $type = 1;
    $now = time();
    $query = "SELECT
            lc.id,
            lco.courseid AS lcocousreid,
            lco.coursename,
            lco.intro,
            lco.onelineintro,
            lco.en_onelineintro,
            lco.vi_onelineintro,
            lco.en_coursename,
            lco.vi_coursename,
            lco.vi_intro,
            lco.en_intro
            FROM {lmsdata_main_course} lmc
            JOIN {lmsdata_class} lc ON lmc.courseid = lc.id 
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            WHERE lmc.type = $type AND lmc.recommended = 1";
    $courses = $DB->get_records_sql($query, array());
    $list = array();
    foreach ($courses as $course) {
        $c = new stdClass();
        $c->id = $course->id;
        $c->lcocousreid = $course->lcocousreid;

        switch (current_language()) {
            case 'ko':
                $c->coursename = $course->coursename;
                $c->onelineintro = $course->onelineintro;
                $c->intro = $course->intro;
                break;
            case 'en':
                $c->coursename = $course->en_coursename;
                $c->onelineintro = $course->en_onelineintro;
                $c->intro = $course->en_intro;
                break;
            case 'vi':
                $c->coursename = $course->vi_coursename;
                $c->onelineintro = $course->vi_onelineintro;
                $c->intro = $course->vi_intro;
                break;
        }

        $list[] = $c;
    }
    return $list;
}

function mkjobs_get_recommend_courses_2()
{
    global $DB, $CFG;
    $type = 2; // default: 4 - site management > management to promotion > job & e-learning
    if (!isloggedin()) $type = 1;
    $now = time();
    $query = "SELECT
            lc.id,
            lco.price,
            lco.courseid AS lcocousreid,
            lco.coursename,
            lco.intro,
            lco.onelineintro,
            lco.en_onelineintro,
            lco.vi_onelineintro,
            lco.en_coursename,
            lco.vi_coursename,
            lco.vi_intro,
            lco.en_intro
            FROM {lmsdata_main_course} lmc
            JOIN {lmsdata_class} lc ON lmc.courseid = lc.id 
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            WHERE lmc.type = $type AND lmc.recommended = 1";
    $courses = $DB->get_records_sql($query, array());


    $list = array();
    foreach ($courses as $course) {
        $c = new stdClass();
        $c->id = $course->id;
        $c->lcocousreid = $course->lcocousreid;
        $c->price = $course->price;

        switch (current_language()) {
            case 'ko':
                $c->coursename = $course->coursename;
                $c->onelineintro = $course->onelineintro;
                $c->intro = $course->intro;
                break;
            case 'en':
                $c->coursename = $course->en_coursename;
                $c->onelineintro = $course->en_onelineintro;
                $c->intro = $course->en_intro;
                break;
            case 'vi':
                $c->coursename = $course->vi_coursename;
                $c->onelineintro = $course->vi_onelineintro;
                $c->intro = $course->vi_intro;
                break;
        }

        $list[] = $c;
    }
    return $list;
}

function mkjobs_get_recommend_courses_random_1()
{
    global $DB, $CFG;
    $type = 2; // default: 4 - site management > management to promotion > job & e-learning
    $now = time();
    $query = "SELECT
            lc.id,
            lco.price,
            lco.courseid AS lcocousreid,
            lco.coursename,
            lco.intro,
            lco.onelineintro,
            lco.en_onelineintro,
            lco.vi_onelineintro,
            lco.en_coursename,
            lco.vi_coursename,
            lco.vi_intro,
            lco.en_intro
            FROM {lmsdata_main_course} lmc
            JOIN {lmsdata_class} lc ON lmc.courseid = lc.id 
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            WHERE lmc.recommended = 1
            ORDER BY RAND() LIMIT 1";
    $course = $DB->get_record_sql($query);
    $c = new stdClass();
    $c->id = $course->id;
    $c->lcocousreid = $course->lcocousreid;
    $c->price = $course->price;

    switch (current_language()) {
        case 'ko':
            $c->coursename = $course->coursename;
            $c->onelineintro = $course->onelineintro;
            $c->intro = $course->intro;
            break;
        case 'en':
            $c->coursename = $course->en_coursename;
            $c->onelineintro = $course->en_onelineintro;
            $c->intro = $course->en_intro;
            break;
        case 'vi':
            $c->coursename = $course->vi_coursename;
            $c->onelineintro = $course->vi_onelineintro;
            $c->intro = $course->vi_intro;
            break;
    }

    return $c;
}

function mjobbs_get_feedback()
{
    global $DB, $CFG;
    $query = "SELECT 
    jf.id,
    jf.username,
    jf.job_position,
    jf.content,
    jf.user_avatar_url,
    jf.title
    FROM {vi_job_feedback} jf
    ";
    $course = $DB->get_records_sql($query, array());
    $list = array();
    foreach ($course as $course) {
        $c = new stdClass();
        $c->id = $course->id;
        $c->username = $course->username;
        $c->job_position = $course->job_position;
        $c->content = $course->content;
        $c->user_avatar_url = $course->user_avatar_url;
        $c->title = $course->title;
        $list[] = $c;
    }
    return $list;

}

function mjobbs_get_feedback_detail($feeback_job_id)
{

    global $DB, $CFG;
    $query = "SELECT 
    jf.id,
    jf.username,
    jf.job_position,
    jf.content,
    jf.user_avatar_url,
    jf.title,
    jf.company_name,
    jf.timecreated
    FROM {vi_job_feedback} jf
    WHERE jf.id=$feeback_job_id
    ";
    $course = $DB->get_record_sql($query);
    $c = new stdClass();
    $c->id = $course->id;
    $c->username = $course->username;
    $c->job_position = $course->job_position;
    $c->content = $course->content;
    $c->user_avatar_url = $course->user_avatar_url;
    $c->title = $course->title;
    $c->company_name = $course->company_name;
    $c->timecreated = date('Y.m.d', $course->timecreated);


    return $c;
}

