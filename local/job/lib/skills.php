<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

function mkjobs_get_job_skills($jobid)
{
    global $DB;
    $sql = "SELECT s.id, s.name, s.short_name
    FROM {vi_skills} s
    JOIN {vi_job_skills} j ON s.id = j.skill_id
    WHERE j.job_id = :jobid
    ORDER BY s.sortorder ASC, s.name ASC";
    return $DB->get_records_sql($sql, array('jobid' => $jobid), 'id,name,short_name');
}

function mkjobs_get_employer_skills($employerid)
{
    global $DB;
    $sql = "SELECT s.id, s.name, s.short_name
    FROM {vi_skills} s
    JOIN {vi_employer_skills} e ON s.id = e.skill_id
    WHERE e.employer_id=:employerid
    ORDER BY s.sortorder ASC, s.name ASC";
    return $DB->get_records_sql($sql, array('employerid' => $employerid), 'id,name,short_name');
}

function mkjobs_get_employer_skill_groups($employerid)
{
    global $DB;
    $sql = "SELECT s.*
    FROM {vi_skill_groups} s
    JOIN {vi_employer_skill_groups} e ON s.id = e.skill_group_id
    WHERE e.employer_id = :employerid
    ORDER BY s.sortorder ASC, s.name ASC";
    return $DB->get_records_sql($sql, array('employerid' => $employerid));
}

function mkjobs_search_skills()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();
    if ($search != '') {
        $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params = array($searchfield => '%' . $search . '%');
    }
    $offset = ($page - 1) * $perpage;
    $skills = $DB->get_records_select('vi_skills', $sql, $params, '{vi_skills}.sortorder ASC, {vi_skills}.name ASC', $fields, $offset, $perpage);

    return $skills;
}

function mkjobs_search_skill_groups()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();
    if ($search != '') {
        $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params = array($searchfield => '%' . $search . '%');
    }
    $offset = ($page - 1) * $perpage;
    $skill_groups = $DB->get_records_select('vi_skill_groups', $sql, $params, '{vi_skill_groups}.sortorder ASC, {vi_skill_groups}.name ASC', $fields, $offset, $perpage);

    return $skill_groups;
}
