<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();


function mkjobs_get_job_location($jobid)
{
    global $DB;
    $sql = "SELECT d.id, d.name
                    FROM {vi_jobs} j 
                    LEFT JOIN {vi_job_locations} jl ON jl.job_id = j.id
                    LEFT JOIN {vi_locations} l ON jl.location_id = l.id
                    LEFT JOIN {vi_districts} d ON l.district_id = d.id
                    WHERE j.id=:jobid
                    LIMIT 1";
    $location = $DB->get_record_sql($sql, array('jobid' => $jobid));
    return $location;
}

function mkjobs_get_job_locations($jobid)
{
    global $DB;
    $sql = "SELECT l.*, d.name as district_name
                    FROM {vi_jobs} j 
                    LEFT JOIN {vi_job_locations} jl ON jl.job_id = j.id
                    LEFT JOIN {vi_locations} l ON jl.location_id = l.id
                    LEFT JOIN {vi_districts} d ON l.district_id = d.id
                    WHERE j.id=:jobid
                    ORDER BY l.full_address ASC";
    $locations = $DB->get_records_sql($sql, array('jobid' => $jobid));
    return $locations;
}

function mkjobs_get_employer_location($employerid)
{
    global $DB;
    $sql = "SELECT d.id, d.name
                    FROM {vi_jobs} j 
                    LEFT JOIN {vi_job_locations} jl ON jl.job_id = j.id
                    LEFT JOIN {vi_locations} l ON jl.location_id = l.id
                    LEFT JOIN {vi_districts} d ON l.district_id = d.id
                    WHERE j.employer_id=:employerid
                    ORDER BY j.timecreated DESC
                    LIMIT 1";
    $location = $DB->get_record_sql($sql, array('employerid' => $employerid));
    return $location;
}
