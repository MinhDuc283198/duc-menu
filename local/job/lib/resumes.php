<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

include_once(dirname(__FILE__) . '/common_functions.php');


function mkjobs_resume_findOrCreate_fieldID($resume_id, $field_name, $field_group = 0, $sortorder = 0)
{
    global $DB;
    $field = $DB->get_record_select('vi_resume_fields', 'resume_id=:resumeid AND field_name=:fieldname AND field_group=:fieldgroup', array('resumeid' => $resume_id, 'fieldname' => $field_name, 'fieldgroup' => $field_group));
    if ($field == null) {
        $field = new stdClass();
        $field->resume_id = $resume_id;
        $field->field_name = $field_name;
        $field->field_group = $field_group;
        $field->sortorder = $sortorder;
        $field->timecreated = time();
        $field->timemodified = time();
        return $DB->insert_record('vi_resume_fields', $field);
    } else return $field->id;
}

function mkjobs_resume_get_field($field_id)
{
    global $DB;
    $f = new stdClass();
    $f->_id = $field_id;

    $field_props = $DB->get_records_select('vi_resume_field_values', 'field_id=:fieldid', array('fieldid' => $field_id));
    foreach ($field_props as $prop) {
        $f->{$prop->field_key} = $prop->field_value;
    }

    $sql = "SELECT f.field_name
            FROM {vi_resume_fields} f
            WHERE f.field_group=:fieldgroup
            GROUP BY f.field_name
            ORDER BY f.field_name";

    $childrens = $DB->get_records_sql($sql, array('fieldgroup' => $field_id));
    foreach ($childrens as $children) {
        $field_lists = $DB->get_records_select('vi_resume_fields', 'field_name=:fieldname AND field_group=:fieldgroup', array('fieldname' => $children->field_name, 'fieldgroup' => $field_id), '{vi_resume_fields}.sortorder');
        $lists = array();
        foreach ($field_lists as $field_list) {
            array_push($lists, mkjobs_resume_get_field($field_list->id));
        }
        $f->{$children->field_name} = $lists;
    }

    return $f;
}

function mkjobs_resume_get_fields($resume_id)
{
    global $DB;
    $field_lists = $DB->get_records_select('vi_resume_fields', 'resume_id=:resumeid AND field_group=:fieldgroup', array('resumeid' => $resume_id, 'fieldgroup' => 0), '{vi_resume_fields}.sortorder');

    $fields = new stdClass();
    foreach ($field_lists as $field) {
        $fields->{$field->field_name} = mkjobs_resume_get_field($field->id);
    }

    return $fields;
}

function mkjobs_resume_delete_field($field_id)
{
    global $DB;
    $DB->delete_records('vi_resume_field_values', array('field_id' => $field_id));
    $DB->delete_records('vi_resume_fields', array('id' => $field_id));

    $field_lists = $DB->get_records('vi_resume_fields', array('field_group' => $field_id));
    foreach ($field_lists as $field_list) {
        mkjobs_resume_delete_field($field_list->id);
    }
}

function mkjobs_resume_save_field($resume_id, $field_name, $field_props = array(), $field_group = 0, $sortorder = 0)
{
    global $DB;
    $fieldid = 0;
    if ($field_group == 0) {
        $fieldid = mkjobs_resume_findOrCreate_fieldID($resume_id, $field_name);
    } else {
        $f = new stdClass();
        $f->resume_id = $resume_id;
        $f->field_name = $field_name;
        $f->field_group = $field_group;
        $f->sortorder = $sortorder;
        $f->timecreated = time();
        $f->timemodified = time();
        $fieldid = $DB->insert_record('vi_resume_fields', $f);
    }

    $DB->delete_records('vi_resume_field_values', array('field_id' => $fieldid));

    foreach ($field_props as $prop_key => $prop_value) {
        if (is_array($prop_value)) {
            $field_lists = $DB->get_records('vi_resume_fields', array('resume_id' => $resume_id, 'field_name' => $prop_key, 'field_group' => $fieldid));
            foreach ($field_lists as $field_list) {
                mkjobs_resume_delete_field($field_list->id);
            }

            for ($i = 0; $i < count($prop_value); $i++) {
                mkjobs_resume_save_field($resume_id, $prop_key, $prop_value[$i], $fieldid, $i);
            }
        } else {
            $prop = new stdClass();
            $prop->field_id = $fieldid;
            $prop->field_key = $prop_key;
            $prop->field_value = $prop_value;
            $prop->timecreated = time();
            $prop->timemodified = time();
            $DB->insert_record('vi_resume_field_values', $prop);
        }
    }
}

function mkjobs_resume_save_fields($resume_id, $fields)
{
    global $DB;
    foreach ($fields as $field_name => $field_props) {
        mkjobs_resume_save_field($resume_id, $field_name, $field_props);
    }
}
