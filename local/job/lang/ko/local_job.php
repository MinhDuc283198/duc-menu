<?php
$string['pluginname'] = '직업';
$string['pluginnameplural'] = '직업';
$string['sitename'] = "Master Korean jobs";

$string['Corporateservice'] = '기업서비스';
$string['MasterKorean'] = '한국어강좌';
$string['Popularjobs'] = '<strong>인기</strong> 채용정보';
$string['PopularjobsTitle'] = '<strong>인기</strong> 채용정보';
$string['PopularjobsTitle2'] = '<strong>추천</strong> 채용공고';
$string['NewJobs'] = '<strong>신규</strong> 채용정보';
$string['MasterKoreanProposalCourse'] = '한국어 추천 강좌';
$string['InterestInfo'] = '관심정보 등록';
$string['DeInterestInfo'] = '관심있는 정보를 탈퇴';
$string['InprogressJobs'] = '진행중인 채용 공고';
$string['welfare'] = '복지혜택';
$string['popular'] = '인기순';
$string['register'] = '등록순';
$string['deadline'] = '마감일순';
$string['All'] = '전체';
$string['RecommendedJobs'] = '추천채용공고';
$string['Employment'] = '고용형태';
$string['Salary'] = '연봉';
$string['Area'] = '지역';
$string['nojobsavailable'] = '찾고 있는 채용정보가 없습니다';
$string['popularjobs_2'] = '현재 인기 있는 채용공고를 확인하세요';
$string['AllJob'] = '전체보기';
$string['FindJob'] = '우리 회사에 지원해야하는 이유';
$string['KoreanCourse'] = '한국어강좌';
$string['RecruitmentNotice'] = '채용공고';
$string['Company'] = '기업';
$string['login'] = '로그인';
$string['close'] = '닫기';
$string['Category'] = '전체';
$string['RecommendedJobs2'] = '추천';
$string['ViewMore'] = '더보기';
$string['Viewjobs'] = '기업 정보 상세 보기';
$string['Jobview'] = '채용공고보기';
$string['LikeJob'] = '관심 채용공고';
$string['Notifi'] = '유사공고알림받기';
$string['DayEnd'] = '까지';
$string['ApplyNow'] = '지금 지원하기';
$string['Applied'] = '지원 완료';
$string['WhyApply'] = '우리 회사에 지원해야 하는 이유?';
$string['Eligibility'] = '우대사항';
$string['Benefits'] = '복지혜택';
$string['KoreanLevel'] = '한국어 활용 수준';
$string['TheJob'] = '직업 정보';
$string['SimilarJobs'] = '이 공고와 유사한 채용공고';
$string['resume'] = '이력서';
$string['FileUpload_2'] = '파일 업로드';
$string['LimitFile'] = '10MB 미만의 .doc .docx .pdf 파일만 업로드 가능합니다';
$string['SubmitResume'] = '이력서 제출하기';
$string['ListSent'] = '보낸 목록';
$string['Date'] = '데이트';
$string['Filename'] = '파일 이름';
$string['Pleaselogin'] = '계속하려면 로그인하십시오!';
$string['SupportStatus'] = '지원 현황';
$string['number'] = '번호';
$string['Enterprise'] = '기업';
$string['SupportHistory'] = '지원내역';
$string['SupportDate'] = '지원일';
$string['NoCompaniesSupported'] = '지원한 기업이 없습니다. 나에게 꼭 맞는 채용공고를 확인하고 지원해보세요.';
$string['MyInfo'] = '내 정보';
$string['SupportStatus'] = '지원 현황';
$string['Interestinformation'] = '관심정보';
$string['notice'] = '알림';
$string['ConsultationHistory'] = '1:1 상담내역';
$string['PersonalInformationModification'] = '개인정보수정';
$string['Withdrawal'] = '회원탈퇴';
$string['Joined'] = '가입일';
$string['ID'] = '아이디';
$string['e-mail'] = '이메일';
$string['PhoneNumber'] = '휴대폰 번호';
$string['Myresume'] = '내 이력서';
$string['Viewresume'] = '이력서 보기';
$string['Videoprofile'] = '동영상 프로필';
$string['delete'] = '삭제';
$string['OnlyUpIt'] = '한국어 강좌를 수강한 사용자만 업로드할 수 있습니다.';
$string['ViewVideoGuide'] = '동영상 프로필 가이드 보기';
$string['Enrollment'] = '등록';
$string['Watchvideo'] = '동영상 보기';
$string['title'] = '제목';
$string['Writer'] = '작성자';
$string['DateCreated'] = '작성일';
$string['views'] = '조회수';
$string['Noconsultation'] = '1:1 상담내역이 없습니다.';
$string['getNotice'] = '설정한 관심정보에 새로운 소식이 등록되면 push 알림이 발송됩니다.';
$string['Duedate'] = '마감일순';
$string['Registrationorder'] = '등록순';
$string['RecruitmentNotice'] = '채용공고';
$string['Companyname'] = '기업명';
$string['Ascendingorder'] = '오름차순';
$string['Descendingorder'] = '내림차순';
$string['Findaccount'] = '나의 계정 찾기';
$string['FindaccountNotice'] = '회원가입 시 등록한 정보로 나의 계정을 찾을 수 있습니다.';
$string['Requiredinput'] = '필수정보입니다';
$string['companyNotice1'] = '찾고 있는 기업이 없습니다.';
$string['companyNotice2'] = '현재 인기 있는 기업을 확인하세요.';
$string['Jobsprogress'] = '진행중인 채용공고';
$string['location'] = '위치';
$string['NoticeA'] = '관심있는 정보를 탈퇴';
$string['NoticeB'] = '관심정보 등록';
$string['times'] = ' 회';
$string['apply'] = '지원 현황';
$string['apply2'] = '지원';
$string['search'] = '검색';
$string['searchBox'] = '검색 회사, 직업...';
$string['SharingAgreement'] = '정보공유 동의';
$string['jobreferrals'] = '채용정보를 추천 받으세요.';
$string['Interestgroup'] = '관심 직군';
$string['selectonlyonejob'] = '관심 직군을 3개까지 선택해주세요.';
$string['JobofInterest'] = '관심 직무';
$string['selectuptothreejobs'] = '관심 직무를 최대 3개까지 선택해주세요.';
$string['ITandInternet'] = 'IT∙인터넷';
$string['Previous'] = '이전단계';
$string['Next'] = '선택완료';
$string['Workarea'] = '취업 희망 지역';
$string['selectthearea'] = '최대 3개의 영역을 선택하십시오.';
$string['Interestsettings'] = '관심정보 설정';
$string['KoreanLevel'] = '한국어 활용 수준';
$string['KoreanLevelInfo'] = '한국어 활용 수준에 따라 취업 확률이 높아집니다.';
$string['KoreanLevelValue'] = '급';
$string['CanNotKro'] = '한국어 못함';
$string['Agree'] = '동의';
$string['sir'] = '님';
$string['Expeer'] = '경력 유무';
$string['ExpeerExplan'] = '업무 경험이 있습니까?';
$string['Fresher'] = '신입';
$string['Career'] = '경력';

$string['NoInterJob'] = '관심 채용공고가 없습니다.';
$string['JobAutoDelete'] = '마감된 채용공고는 자동으로 삭제됩니다.';
$string['InterAdd'] = '추가하기';
$string['FibeAdd'] = '최대 5개의 구독 목록을 만들 수 있습니다.';
$string['KeyWordsThree'] = '직무, 기업명 등 최대 3개의 키워드를 입력할 수 있습니다...';
$string['EnterSkill'] = '직무 입력...';
$string['EnterCompa'] = '회사를 입력하십시오...';
$string['EnterJobs'] = '직군 입력...';
$string['EnterCompanyType'] = '산업군 입력...';

$string['LoginName'] = '사용자 아이디 (이메일 주소)';
$string['LoginPass'] = '비밀번호';
$string['emailcertified'] = '내계정 찾기 ';

$string['EditInfoPass'] = '로그인한 SNS에서 비밀번호 변경 가능합니다.';
$string['Loginrequired'] = '이 서비스는 로그인이 필요합니다. 로그인 하시겠습니까?';
$string['emailcertified'] = '내계정 찾기 ';
$string['rememberusername'] = '아이디저장';

$string['Viewall'] = '전체보기';
$string['Elearning'] = '이러닝';
$string['QNA1'] = '공지사항';
$string['QNA2'] = '1:1 상담';
$string['QNA3'] = 'FAQ';

$string['WaitPass'] = '임시비밀번호를 발급중입니다.';
$string['Save'] = '저장';

$string['killInTitle'] = '직무';
$string['jobInTitle'] = '직군';
$string['companyInTitle'] = '산업군';
$string['companyInInterested'] = '관심있는 회사';
$string['companyInInterestedNone'] = '관심있는 회사가 없습니다';
$string['wiewCompany'] = '회사보기';

$string['Setinterestinfo'] = '관심정보 설정';
$string['SetinterestinfoSS'] = '관심정보 설정하고 채용정보 추천 받으세요';
$string['offOneDay'] = '1일동안 보지 않기';
$string['not_registerd_korean_level'] = "한국어 수준 미등록";

$string['ApplyStatus'] = '진행중 채용공고 현황';
$string['Jobsinprogress']  = '진행중인 채용공고';
$string['Numberofapplicants'] = '지원자 수';
$string['Jobssetforinterest'] = '관심 채용공고 설정 수';
$string['TalentList'] = '인재 현황';
$string['TalentMatch'] = '추천 인재';
$string['FeaturedTalents'] = '관심 인재';
$string['Bookmarks'] = '우리회사 관심 설정 인재';
$string['CompanyInfo'] = '기업정보';
$string['BasicInformation'] = '기본정보';
$string['Details'] = '세부정보';
$string['FieldRequest'] = '표시된 항목은 필수입력 항목입니다.';
$string['countryName'] = '국가';
$string['Companyscale'] = '기업 규모';
$string['SelectOne'] = '직원수 선택';
$string['SelectRegion'] = '지역선택';
$string['Region'] = '지역선택';
$string['address'] = '주소';
$string['Homepage'] = '기업 홈페이지';
$string['Facebookaddress'] = '페이스북 주소';
$string['ServiceIntroduction'] = '회사/서비스 소개';
$string['ThreeReasons'] = '우리 회사에 입사해야 하는 이유(최대 3가지 입력 가능)';
$string['Viewtalented'] = '추천 인재 보기';
$string['Jobposting'] = '채용공고 등록';
$string['rowsrow'] = ' 명';
$string['display5'] = '5개씩보기';
$string['display10'] = '10개씩보기';
$string['display20'] = '20개씩보기';
$string['display30'] = '30개씩보기';
$string['reset'] = '초기화';
$string['stt'] = '상태';
$string['aplicant'] = '지원자';
$string['Recommended'] = '직무';
$string['Interest'] = '조회수';
$string['Postdate'] = '등록일';
$string['Startdate'] = '시작일';
$string['Enddate'] = '마감일';
$string['Exposure'] = '노출';
$string['copy'] = '복사';
$string['nodatafound'] = '등록된 채용공고가 없습니다';
$string['ExcelDownload'] = '엑셀다운로드';
$string['Resume_2'] = '이력서 관리';
$string['pop_info_1'] = '기업정보를 작성해야만';
$string['pop_info_2'] = '채용공고를 등록할 수 있습니다';
$string['pop_edit'] = '기업정보 등록하기';
$string['pop_edit_2'] = '나중에하기';
$string['skill'] = '관심 직무';
$string['skillgroup'] = '관심 직군';
$string['select_one'] = '최대 1개 선택 가능합니다';
$string['select_three'] = '최대 3개 선택 가능합니다.';
$string['enterJobName'] = '채용공고명 입력';
$string['job_skillgroup'] = '직군';
$string['job_skill'] = '직무';
$string['skill_add'] = '추가';
$string['Responsibilities'] = '담당업무';
$string['detailedtask'] = '상세 담당업무 입력';
$string['job_request'] = '자격요건';
$string['job_request_v'] = '자격요건 입력(경력여부, 학력, 연령 등)';
$string['job_Benefits'] = '우대사항';
$string['job_ortherLang'] = '기타 우대사항 입력(외국어, 자격증 등)';
$string['Workingcondition'] = '근무조건';
$string['job_work'] = '경력 여부';
$string['Worktype'] = '근무형태';
$string['exchangeInInterview'] = '면접 시 협의';
$string['job_workarea'] = '근무지역';
$string['job_worktime'] = '근무시간';
$string['Enterstreetaddress'] = '상세 주소 입력';
$string['Enterifcommentisneeded'] = '설명이 필요한 경우 입력';
$string['Receptionperiod'] = '접수기간';
$string['Publishing'] = '노출여부';
$string['Nopublishing'] = '비노출';
$string['cancel'] = '취소';
$string['passed'] = '합격';
$string['failed'] = '불합격';
$string['InterviewApplication'] = '면접신청';
$string['name'] = '이름';
$string['talens_students'] = 'Master Korean 수강생 우선 보기';
$string['talens_applydate'] = '지원일순';
$string['talens_byname'] = '이름순';
$string['Batch'] = '일괄처리';
$string['pdfDownload'] = '선택 이력서 다운로드';
$string['ViewResume_2'] = '이력서 보기';
$string['talensFind'] = '인재검색';
$string['select_template'] = '템플릿 선택';
$string['Temporary'] = '임시저장';
$string['Completed'] = '작성완료';
$string['Download-for-employer'] = "다운로드(-".'{$a}'." 포인트)";
$string['Enterresumetitle'] = '무제';
$string['resume_title'] = '이력서 제목';
$string['dateofbirth'] = '생년월일';
$string['personal_details'] = '개인 정보';
$string['Desiredworkingconditions'] = '희망 근무 조건';
$string['EmploymentForm'] = '고용형태';
$string['HopeSalary'] = '희망 급여';
$string['Fulltime'] = '정규직';
$string['Irregularworkers'] = '비정규직';
$string['Education'] = '학력';
$string['more'] = '더';
$string['move'] = '움직임';
$string['Attending'] = '재학중';
$string['in_progress'] = '진행중';
$string['school'] = '학교구분';
$string['dh'] = '대학원';
$string['cd'] = '대학교';
$string['cpn'] = '전문대학';
$string['c3'] = '고등학교';
$string['Schoolname'] = '학교 명';
$string['MajorandDegree'] = '전공 및 학위';
$string['Office'] = '재직중';
$string['RankSelection'] = '직급선택';
$string['employee'] = '사원';
$string['substitute'] = '대리';
$string['Exaggeration'] = '과장';
$string['conductor'] = '차장';
$string['Director'] = '부장';
$string['Executives'] = '임원';
$string['Keyachievementsadded'] = '주요성과 추가';
$string['mainachievement'] = '주요 성과를 작성해주세요.';
$string['mainachievementdesc'] = '세부 사항을 입력하십시오.';
$string['workandachievements'] = '업무내용과 성과를 작성해주세요.';
$string['TrainingCompletionCompletion'] = '교육 수료/이수 및 기타 활동';
$string['Department'] = '부서';
$string['contentsanda'] = '업무내용과 성과를 작성해주세요.';
$string['ImportCourseHistory'] = 'Master Korean 수강이력 불러오기';
$string['Add'] = '추가';
$string['andotheractivities'] = '교육명 및 기타 활동명';
$string['Pleasefillinthedetails'] = '상세 내용을 작성해주세요.';
$string['progressorcompleted'] = '진행중이거나 수료한 이력만 노출됩니다.';
$string['Proceeding'] = '진행중';
$string['Coursehistory'] = '수강이력';
$string['certificate'] = ' 자격증';
$string['Enteryourcredentials'] = '자격증명 입력';
$string['Qualificationnumber'] = '자격번호(\'-\'없이 입력)';
$string['Issuer'] = '발행처';
$string['AcquisitionDateYYYY.MM.DD'] = '취득일 YYYY.MM.DD';
$string['link'] = '링크';
$string['LinkNameDescription'] = '링크명(설명)';
$string['Attachproof'] = '증빙서류 첨부';
$string['Youcanattachafile'] = '이력서 관리 메뉴에서 관리되고 있는 파일을 첨부할 수 있습니다';
$string['resume:notfileuploaded'] = '파일이 업로드되지 않았습니다.';
$string['Editinterestinformation'] = '관심 정보 편집';
$string['Hopearea'] = '희망 지역';
$string['KoreanLanguageLevel'] = '한국어 활용 수준';
$string['temporarystorage'] = '임시저장';
$string['Download'] = '다운로드';
$string['Enterpostalcode'] = '우편번호 입력';
$string['Enteraddress'] = '주소를 입력하';
$string['YouTubevideoprofile'] = '유튜브 동영상 프로필';
$string['Basicresume'] = '기본 이력서 ';
$string['Allcourses'] = '전체 강좌';
$string['Fileupload_3'] = '파일 업로드';
$string['Attachments'] = '첨부파일';
$string['Videoprofile'] = '동영상 프로필';
$string['recommendedtoa'] = '기업에 인재로 추천되거나 인재 검색 시, 기본 이력서로 설정된 이력서가 인사담당자에게 제공됩니다.';
$string['CanregisteraYouTube'] = '유튜브 동영상 프로필은 Master Korean의 강좌 수강이력이 있는 경우 등록 가능하고, 인재 추천 또는 이력서 제출 시 인사담당자가 조회할 수 있습니다.';
$string['Creating'] = '작성중';
$string['Completed'] = '작성완료';
$string['Setyourdefaultresume'] = '기본 이력서 설정';
$string['Modify'] = 'Modify';
$string['Downloadpdf'] = '다운로드(pdf)';
$string['make_pdf'] = 'PDF 만들기';
$string['preview&download'] = '미리보기 및 다운로드';
$string['delete'] = '삭제';
$string['Finishedappointment'] = '등록완료';
$string['Changetoimpression'] = '노출로 변경';
$string['Modify'] = '수정';
$string['YouTubevideoprofile'] = '유튜브 동영상 프로필';
$string['registration'] = '등록하기';
$string['Availablewhenyoutakea'] = 'Master Korean 강좌 수강 시 이용 가능.';
$string['Notregistered'] = '등록불가';
$string['Canregistration'] = '등록 가능';
$string['OnlyYouTubevideo'] = '유튜브 동영상만 등록 가능합니다. URL을 다시 확인해주세요.';
$string['tosetasdefault'] = '기본값으로 설정 하시겠습니까?';
$string['wanttodelete'] = '정말 삭제 하시겠습니까?';
$string['continue'] = '계속하려면 확인하십시오.';
$string['Confirm'] = '확인';
$string['Filetitle'] = '파일 제목';
$string['Fileselection'] = '파일선택';
$string['Registerasresume'] = '이력서로 등록';
$string['checkresumeregistration'] = '이력서 등록에 체크할 경우, 이력서로 관리됩니다';
$string['Coursesbeingtaken'] = '수강중인 강좌 / 1 = 담당 강좌 ';
$string['Hello'] = ' 안녕하세요. 김비상입니다.';
$string['KimBisang'] = '김비상';
$string['MobilePhoneNumber'] = '휴대폰번호';
$string['Workingconditions'] = '희망근무조건';
$string['Consultationduring'] = '면접 시 협의';
$string['MajorAchievements'] = '주요성과';
$string['Completion'] = '수료';
$string['Acquisitiondate'] = '취득일';
$string['Documentaryevidence'] = '증빙서류';
$string['Selectevidencefile'] = '증빙서류 파일 선택';
$string['Resumeintitle'] = '이력서 제목 ';
$string['Selected'] = '선택완료';
$string['YouTubevideo'] = ' 유튜브 동영상 프로필 등록';
$string['EnterYouTubeURL'] = '유튜브 URL 입력';
$string['Non-exposure'] = '비노출';
$string['print'] = '인쇄';
$string['Resumewriting'] = '이력서 작성';
$string['noapplicants'] = '지원자가 없습니다.';
$string['noapplicants2'] = '조건에 맞는 인재가 없습니다';
$string['EditInterestInfo'] = '관심정보수정';

$string['wait'] = '대기';
$string['ended'] = '마감';
$string['Interesting'] = '관심 설정';
$string['RecommendedTalent'] = '추천인재';

$string['c_joinAgreement'] = '기업회원 가입에 대한 동의';
$string['c_accountwiththeinformation'] = '회원가입 시 등록한 정보로 계정을 찾을 수 있습니다';
$string['c_Entercompanyname'] = '기업명 입력';
$string['c_CompanyRegistration'] = '사업자등록번호';
$string['c_CompanyAccount'] = '기업 계정 찾기';
$string['c_EmailID'] = '이메일(아이디)';
$string['c_temporarypassword'] = '이메일로 임시 비밀번호 받기';
$string['c_yourtemporarypassword'] = '임시 비밀번호로 로그인 후 반드시 비밀번호를 변경해주세요.';
$string['c_Termstobeadded'] = '약관 내용 추가 예정';
$string['c_abletoregister'] = '위 항목에 동의하지 않는 경우 회원가입이 불가합니다.';
$string['c_Iagree'] = '동의합니다';
$string['c_JoinCorporate'] = '기업회원 가입';
$string['c_IDConditions'] = '아이디 입력조건 안내 문구';
$string['c_PasswordEntryConditions'] = '비밀번호 입력조건 안내 문구';
$string['c_conditiontext'] = '비밀번호 확인조건 안내 문구';
$string['c_Entercompanyinformation'] = '기업정보 입력';
$string['c_Itemsshown'] = '표시된 항목은 필수입력 항목입니다';
$string['c_Industry'] = '산업군';
$string['c_Selectindustry'] = '산업군 선택';
$string['c_SelectCountry'] = '국가를 선택';
$string['c_registrationcertificate'] = '사업자 등록증 첨부';
$string['c_homepage'] = '기업 홈페이지 주소';
$string['c_Contactname'] = '담당자 이름';
$string['c_ContactPerson'] = '담당자 부서/직급';
$string['c_Contactpersons'] = '담당자 연락처';
$string['c_Nonconformity'] = '휴대폰 번호 부적합 사유 안내';
$string['c_Contactemail'] = '담당자 이메일';
$string['c_Nonconformance'] = '이메일 부적합 사유 안내';
$string['c_CompanyMemberLogin'] = '기업회원 로그인';
$string['c_saveID'] = '아이디저장';
$string['c_Requestforapproval'] = '승인요청';
$string['c_Accountregistration'] = '계정등록';
$string['c_TermsofUse'] = '이용약관';
$string['c_regulations'] = '분쟁 해결 정책';
$string['c_Privacypolicy'] = '개인정보 보호 방침';
$string['c_lginEmail'] = '아이디 입력(이메일 주소)';
$string['c_EnterPassword'] = '비밀번호 입력';
$string['c_startJoinEmail'] = '회사 이메일 입력(아이디로 사용됩니다.)';
$string['c_retypePass'] = '비밀번호 확인';
$string['c_enterComName'] = '기업 이름 입력';
$string['c_enterRepAddress'] = '대표 주소 입력';
$string['c_enterContactName'] = '담당자 이름 입력';
$string['c_enterRoomTitle'] = '담당자 부서/직급 입력';
$string['c_Eligibility'] = '자격요건';
$string['c_FinalEducation'] = '최종학력';
$string['c_end'] = '이상';
$string['c_age'] = '연령';
$string['c_blablabla'] = '자격요건 입력(경력여부, 학력, 연령 등)';
$string['c_agreeAllAll'] = '모든 이용 약관에 동의';

$string['my_inter:add'] = '추가';
$string['my_inter:save'] = '저장';
$string['my_inter:limit_1'] = '1개 선택';
$string['my_inter:limit_3'] = '최대 3개 선택';
$string['my_inter:limit_3_desc'] = '최대 3개 선택 가능합니다';
$string['my_inter:unlimited'] = '다중 선택 가능';
$string['my_inter:final_education'] = '최종 학력';
$string['my_inter:final_education_desc'] = '최종 학력 선택';
$string['my_inter:work_experience'] = '경력유무';
$string['my_inter:work_experience_desc'] = '경력 유무 선택';
$string['my_inter:skill_group'] = '관심 직군';
$string['my_inter:skill'] = '관심 직무';
$string['my_inter:workplace'] = '희망 지역';
$string['my_inter:korean_level'] = '한국어 활용 수준';
$string['my_inter:korean_desc'] = '한국어 능력을 선택하십시오';

$string['myinfo:Fullmanager'] = '전체관리자';
$string['myinfo:Interestinformation'] = '관심정보';
$string['myinfo:Interest'] = '관심직무';
$string['myinfo:ResumeStatus'] = '이력서 현황';
$string['myinfo:Basicresume'] = '기본 이력서';
$string['myinfo:piece'] = '개';
$string['myinfo:Recommendedcourse'] = '추천강좌';

$string['job_apply:pop_title'] = '지원하기';
$string['job_apply:pop_select'] = '이력서 선택';
$string['job_apply:pop_youtube_checkbox'] = '유튜브 동영상 프로필 보내기';
$string['job_apply:pop_desc'] = '유튜브 동영상 프로필 보내기 체크시 이력서와 함께 제출됩니다.';
$string['job_apply:pop_submit'] = '이력서 제출하기';
$string['job_apply:pop_send_success'] = '프로필이 성공적으로 전송되었습니다.';

$string['talensearch:text1'] = "좋아요";
$string['resume:changeStatus'] = "변경 사항이 적용되었습니다 !";

$string['notifi_time:second'] = '{$a} 초전';
$string['notifi_time:seconds'] = '{$a} 초전';
$string['notifi_time:minute'] = '{$a} 분전';
$string['notifi_time:minutes'] = '{$a} 분전';
$string['notifi_time:hour'] = '{$a} 시간전';
$string['notifi_time:hours'] = '{$a} 시간전';
$string['notifi_time:day'] = '{$a} 일전';
$string['notifi_time:days'] = '{$a} 일전';
$string['notifi_time:week'] = '{$a} week ago';
$string['notifi_time:weeks'] = '{$a} weeks ago';
$string['notifi_time:month'] = '{$a} month ago';
$string['notifi_time:months'] = '{$a} months ago';
$string['notifi_time:year'] = '{$a} year ago';
    $string['notifi_time:years'] = '{$a} years ago';

$string['cv:edit:address'] = '우편번호 입력(숫자를 입력 해 주세요.)';
$string['cv:edit:address2'] = '주소 입력';
$string['cv:edit:companyName'] = '회사명';

$string['cv:edit:namecourse'] = '코스 명';
$string['cv:edit:nocourse'] = '등록 된 코스가 없습니다';

$string['my:info:num_1'] = '지원완료';
$string['my:info:num_2'] = '관심 채용공고';
$string['my:info:num_3'] = '관심 기업';
$string['my:info:resumes'] = '이력서 관리';

$string['resume:edit:datenotvaild'] = '값이 유효하지 않거나 종료 날짜가 시작 날짜보다 작습니다.';
$string['my:info:avatar']='아바타 변경';
$string['resume:position:title'] = '직책';
$string['resume:position:divisiondirector'] = '부장';
$string['resume:position:divisionmanager'] = '차장';
$string['resume:position:manager'] = '과장';
$string['resume:position:teamleader'] = '대리';
$string['resume:position:staff'] = '사원';

$string['employer:navtab:info'] = '기업정보';
$string['employer:navtab:reviews'] = '리뷰';

$string['review:pop_title'] = '기업 리뷰 작성';
$string['review:pop_confirm'] = '별점을 등록하시겠습니까?';
$string['review:pop_desc'] = '각 영역별 별점을 선택해주세요.';
$string['review:pop_submit'] = '등록';
$string['review:pop_success'] = '검토해 주셔서 감사합니다.';
$string['review:pop_close'] = '닫기';
$string['review:desc'] = '영역별 평균 별점';
$string['review:point'] = '점';
$string['review:count'] = '개';
$string['review:add_register'] = '리뷰 작성';
$string['review:toplatest'] = '최신 리뷰';
$string['review:nodata'] = '등록 된 리뷰가 없습니다.';

$string['employer:Talentedpeople'] = '관심 인재';
$string['employer:CareersJobs'] = '관심 채용공고 설정 인재';
$string['employer:InterestedCompany'] = '관심 기업 설정 인재';
$string['employer:RecommendedTalent'] = '추천 인재';

$string['employer:menuJobs'] = '채용공고';
$string['employer:menuApply'] = '지원자';
$string['employer:menuInfo'] = '기업정보';
$string['employer:menuOrder'] = '뉴스';
$string['employer:confirn1'] = '기업회원 승인 대기 상태입니다.';
$string['employer:confirn2'] = '담당자에게 문의해주세요.';
$string['employer:denied1'] = '기업회원 승인이 반려되었습니다';
$string['employer:denied2'] = '담당자에게 문의해주세요.';
$string['employer:menuLegalTax'] = '뉴스';
$string['employer:legalTax_legal'] = '법무';
$string['employer:legalTax_tax'] = '세무';
$string['employer:legalTax_all'] = '전체';

$string['employer:reviewJob'] = '미리보기';
$string['employer:logoCom1'] = ' ';
$string['employer:logoCom2'] = '기업로고 등록';
$string['employer:logoCom3'] = '180*180';
$string['employer:logoCom4'] = '10Mb';
$string['employer:logoCom5'] = 'jpg,png만 가능';
$string['employer:logoCom6'] = '한줄 소개 입력';
$string['employer:logoCom7'] = '회사 및 서비스 소개 입력';
$string['employer:logoCom8'] = '복지혜택 입력';
$string['employer:searchSort'] = '업데이트순';
$string['employer:searchSort2'] = '오름차순';
$string['employer:searchSort3'] = '내림차순';
$string['employer:searchSort4'] = '관심설정일순';
$string['employer:searchSort5'] = '추천일순';
$string['employer:viewClass1'] = '강좌 수료현황';
$string['employer:viewClass2'] = 'Master Korean에서 한국어 강좌를 수료한 이력입니다.';
$string['employer:courseperiod'] = ' 일';
$string['employer:Negotiate'] = ' 면접 시 협의';
$string['employer:backurl'] = '이전으로';
$string['employer:jobnotfound'] = '본 채용공고는 마감되었습니다';
$string['employer:jobnotfound2'] = '';
$string['employer:jobend'] = '내 직업은 구식이었다';
$string['employer:jobend2'] = '이 직업은 문을 닫았습니다. 다른 직업을 방문하십시오';

$string['employer_user:company_name'] = '기업명순';
$string['employer_user:business_license_number'] = '사업자등록번호';
$string['employer_user:approval_date'] = '승인일';
$string['employer_user:contact_name'] = '담당자 이름';
$string['employer_user:department'] = '담당자 부서/직급';
$string['employer_user:contact_number'] = '담당자 연락처';
$string['employer_user:email'] = '담당자 이메일';
$string['employer_user:timemodified'] = '마지막 변경 날짜';
$string['employer_user:note'] = '※ 다른 회사 정보를 변경해야하는 경우 담당자에게 문의하십시오.';

$string['my:info:address'] = '부가정보 - 주소';

$string['for_employer'] = '고용주 용';
$string['course'] = '강좌';
$string['job_matching'] = '추천 채용';
$string['hot_job'] = '인기 채용';
$string['new_job'] = '신규 채용';
$string['recommend_job'] = '추천 채용';
$string['suitable_job'] = '당신에게 적합한 직업';
$string['login_to_see_more'] = '더 많은 추천 작업을 보려면 로그인하세요';
$string['login_now'] = '로그인';
$string['feedback'] = 'MASTER KOREAN JOB에 대한 사람들의 이야기 더보기';
$string['find_now'] = '지금 찾기';

$string['searchkeyworld'] = '구직, 회사, 기술, ...';
$string['searchcategories'] = '모든 카테고리';
$string['searchlocation'] = '모든 위치';
$string['applynow'] = '지원하기';
$string['savejob'] = '저장';
$string['logtoview'] = '급여를 보려면 로그인하십시오.';
$string['jobdetail'] = '채용정보';
$string['companyinfor'] = '회사 정보';
$string['compmorejob'] = '채용리스트';
$string['similarjob'] = '유사 채용 공고';
$string['benefits'] = '혜택';
$string['skilltags'] = '스킬 태그';
$string['seemore'] = '더보기';
$string['signupalert'] = '개인 이메일을 통해 새 작업 알림을 받으려면 작업 알림을 등록하십시오.';
$string['signupnow'] = '지금 등록하세요';
$string['sologan'] = '여기에서 가장 적합한 일자리를 찾아 Master Korean Jobs와 함께 미래를 만드십시오.';

$string['about_us'] = '회사소개';
$string['contact_us'] = '문의하기';
$string['for_employee'] = '구인자';
$string['post_job'] = '채용 등록';
$string['search_resume'] = '프로필 검색';
$string['product_service'] = '서비스';
$string['contact'] = '접촉';
$string['jobs_by_location'] = '지역별';
$string['see_all_location'] = '모든 지역 보기 >';
$string['jobs_by_category'] = '직무별';
$string['see_all_category'] = '모든 직무 보기 >';
$string['top-employers'] = 'TOP채용관';
$string['no-data'] = '표시 할 내용이 없습니다';

$string['logintosee'] = '급여를보고 작업을 저장하려면 로그인하십시오.';
$string['seemore'] = '더보기';
$string['btnclickhere'] = '여기를 클릭하세요';
$string['topcompany'] = '최고의 회사';

$string['for_candidatate']='후보자 용';
$string['create_resume']='이력서 작성';
$string['career_counseling']='진로 상담';

$string['recommended_cadidates']='추천 후보자보기';
$string['managejob']='채용관리';

$string['online']='채용중';
$string['expired']='기한완료';
$string['draff']='임시저장';
$string['applied']='지원자 수';
$string['suggested']='입사제안 수';
$string['marked']='별도 표시';
$string['managecandidates']='지원자 관리';


$string['title'] = '표제';
$string['content'] = '함유량';
$string['infomation_feedback'] = '정보 피드백';

$string['banner-resume'] = '한국어를 구사하는 후보자를위한 최고의 이력서 작성 사이트';
$string['create-cv'] = 'CV 만들기';
$string['upload-file'] = '파일 업로드';
$string['free-download'] = '무료 다운로드';
$string['download-des'] = '한 번의 클릭으로 원하는 PDF 형식으로 완벽한 이력서를 만들 수 있습니다.';
$string['guide-cv-title'] = '03 완벽한 이력서를 만드는 단계';
$string['guide-cv-1'] = '좋아하는 템플릿을 선택하세요. 가장 적합한 것을 선택하십시오.';
$string['guide-cv-2'] = '세부 정보를 업데이트하십시오. 시스템이 자동으로 현재 정보를 입력합니다. 정보가 변경된 경우 Master Korea Jobs에서도 수정할 수 있습니다.';
$string['guide-cv-3'] = '다운로드하여 신청하십시오. 완료 후 이력서를 저장하고 지금 신청할 수 있습니다.';
$string['style-cv'] = '우리는 당신을 위해 다양한 CV를 가지고 있습니다.';
$string['create-cv-title'] = '어디에서나 적용';
$string['create-cv-des'] = '이제 언제 어디서나 지원하고 모든 고용주에게 인상을 남길 수 있습니다.';
$string['create-cv-btn'] = 'CV 만들기';

$string['current-info'] = '현재 정보';
$string['check-now'] = '지금 확인';
$string['my-dashboard'] = '내 대시 보드';
$string['new-team-cv'] = '새로운 팀 CV';
$string['update-profile'] = '온라인 프로필 업데이트';
$string['my-jobs'] = '내 직업';
$string['setting'] = '환경';
$string['online-profile'] = '온라인 프로필';
$string['upload-profile'] = '파일 업로드';
$string['update-at'] = '최신 정보';
$string['quick-view'] = '퀵뷰';
$string['download-file'] = '파일 다운로드';
$string['searchchable'] = '검색 가능';
$string['job-alert'] = '직업 알림';
$string['show-multi-file'] = '여러 파일 표시';
$string['complete-level'] = '완성도';
$string['update-btn'] = '프로필 업데이트';
$string['upload-btn'] = '더 많은 파일 업로드';
$string['expriedday']='만료일';
$string['loginsalary']='급여';
$string['nosearchresults']='검색 항목에 대한 결과가 없습니다';
$string['send_list_job']='구독자에게 작업 목록 보내기';
$string['benefit_type']='혜택';


$string['view-contact-cv']="이력서에서 연락처 보기(-".'{$a}'." 포인트)";
$string['buy-credit-require'] = '크레딧 점수가 만료되었습니다. 더 구매해주세요';
$string['save-post-job']='등록(-{$a} 포인트)';
$string['noti-post-job']='해당 작업을 게시할 때마다 2포인트가 차감됩니다.';
$string['buy-credit-require'] = '크레딧 점수가 만료되었습니다. 더 구매해주세요';

$string['noti-post-job']='해당 작업을 게시할 때마다 2포인트가 차감됩니다.';
//my coupon page
$string['cp_mycoupons']='내 쿠폰';
$string['cp_no']='No';
$string['cp_title']='제목';
$string['cp_image']='영상';
$string['cp_course']='강의';
$string['cp_dcrate']='할인율';
$string['cp_receiveddate']='접수일';
$string['cp_subscriptioncode']='암호';
$string['cp_used']='사용 된';
$string['cp_useddate']='사용 날짜';
$string['cp_expired']='만료됨';
$string['cp_expire_date']='마감 기한';
$string['cp_actions']='행위';
$string['cp_nodata']='쿠폰이 없습니다.';
$string['cp_noque']='NO';
$string['cp_yesque']='YES';
$string['confirmdelete']='이 할인 코드를 삭제하시겠습니까?';
$string['succdel']='쿠폰 1개 삭제됨';
$string['faildel']='이 쿠폰은 삭제할 수 없습니다';

$string['down-cv-file']="이 이력서를 다운로드하는 데 ".'{$a}'."크레딧을 사용하시겠습니까?";
$string['view-cv-file']="이 이력서의 연락처를 보기 위해 ".'{$a}'."크레딧을 사용하시겠습니까?";

$string['send_list_cv']="고용주에게 이력서 목록 보내기";
