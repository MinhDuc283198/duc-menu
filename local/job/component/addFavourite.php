<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');

mkjobs_require_login();

if (!empty($_POST["fav-korea-level"])) {
    $DB->delete_records('vi_user_skill_groups', array('user_id' => $USER->id));
    if($_POST["fav-step1-txt1"] != ""){
        $user_skill_group = new stdClass();
        $user_skill_group->user_id = $USER->id;
        $id = str_replace("fav-step1-","",$_POST["fav-step1-txt1"]);
        $user_skill_group->skill_group_id = (int)$id;
        $DB->insert_record('vi_user_skill_groups', $user_skill_group);
    }
    if($_POST["fav-step1-txt2"] != ""){
        $user_skill_group = new stdClass();
        $user_skill_group->user_id = $USER->id;
        $id = str_replace("fav-step1-","",$_POST["fav-step1-txt2"]);
        $user_skill_group->skill_group_id = (int)$id;
        $DB->insert_record('vi_user_skill_groups', $user_skill_group);
    }
    if($_POST["fav-step1-txt3"] != ""){
        $user_skill_group = new stdClass();
        $user_skill_group->user_id = $USER->id;
        $id = str_replace("fav-step1-","",$_POST["fav-step1-txt3"]);
        $user_skill_group->skill_group_id = (int)$id;
        $DB->insert_record('vi_user_skill_groups', $user_skill_group);
    }
    
    $DB->delete_records('vi_user_skills', array('user_id' => $USER->id));
    if($_POST["fav-step2-txt1"] != ""){
        $user_skill = new stdClass();
        $user_skill->user_id = $USER->id;
        $id = str_replace("fav-step2-","",$_POST["fav-step2-txt1"]);
        $user_skill->skill_id = (int)$id;
        $DB->insert_record('vi_user_skills', $user_skill);
    }
    
    if($_POST["fav-step2-txt2"] != ""){
        $user_skill = new stdClass();
        $user_skill->user_id = $USER->id;
        $id = str_replace("fav-step2-","",$_POST["fav-step2-txt2"]);
        $user_skill->skill_id = (int)$id;
        $DB->insert_record('vi_user_skills', $user_skill);
    }
    
    if($_POST["fav-step2-txt3"] != ""){
        $user_skill = new stdClass();
        $user_skill->user_id = $USER->id;
        $id = str_replace("fav-step2-","",$_POST["fav-step2-txt3"]);
        $user_skill->skill_id = (int)$id;
        $DB->insert_record('vi_user_skills', $user_skill);
    }
    
    $DB->delete_records('vi_user_korean_levels', array('user_id' => $USER->id));
    $korean_level_id = $_POST["fav-korea-level"];
    $korean_level_id = (int)$korean_level_id;
    if ($korean_level_id != 0) {
        $level = new stdClass();
        $level->user_id = $USER->id;
        $level->korean_level_id = $korean_level_id;
        $DB->insert_record('vi_user_korean_levels', $level);
    }
    
    $DB->delete_records('vi_user_workplaces', array('user_id' => $USER->id));
    $places = optional_param_array('fav-step3', array(), PARAM_INT); 
    foreach ($places as $place){
        if($place > 0){
            $p = new stdClass();
            $p->user_id = $USER->id;
            $p->district_id = $place;
            $DB->insert_record('vi_user_workplaces', $p);
        }
    }
    
    $expe = optional_param('fav-experience',0, PARAM_INT);
    $DB->delete_records('vi_user_work_experiences', array('user_id' => $USER->id));
    if($expe != 0){
        $ex = new stdClass();
        $ex->user_id = $USER->id;
        $ex->work_experience_id	= $expe;
        $DB->insert_record('vi_user_work_experiences', $ex);
    }
}