<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $USER, $search_hashtags, $PAGE, $job_search_hashtags, $DB;

// ===================================================================================================
// handles
require_once($VISANG->dirroot . '/lib/common_functions.php');
// if (!isset($search_hashtags)) $search_hashtags = mkjobs_get_searchtext();
if (!isset($job_search_hashtags))
    $job_search_hashtags = mkjobs_get_searchtext('jobs');

$langs = get_string_manager()->get_list_of_translations();
$currlang = current_language();

$term_category = 1;
$comid = null;
if(isloggedin()) {
    $sql = "SELECT ad.employer_id as val, em.company_name
            FROM {vi_admin_employers} ad
            JOIN {vi_employers} em ON em.id = ad.employer_id 
            WHERE ad.user_id = :id";
    $comid = $DB->get_record_sql($sql, array('id' => $USER->id));

    if($comid != null) {
        $term_category = 2;
    }
}
// ===================================================================================================
// renders
?>

<!-- header start -->
<div class="vis-hd main clearfix">
    <?php if (!isloggedin()) : ?>
        <a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php" class="ic-login"><?php echo get_string('login', 'local_job'); ?></a>
    <?php endif; ?>

    <div class="tp">
        <ul>
            <!--            <li>
                <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><?php echo get_string('facebook', 'theme_oklassedu'); ?></a>
                <a href="https://www.youtube.com/channel/UCcU6RCjsoPqJ7HtiIpLmyyw" target="_blank" class="ic-youtube"><?php echo get_string('youtube', 'theme_oklassedu'); ?></a>
            </li>-->

            <li><a href="<?php echo $VISANG->elearning_wwwroot; ?>" target="_blank" class="t-blue02"><?php echo get_string('KoreanCourse', 'local_job'); ?></a></li>
            <li class=""><a href="<?php echo $VISANG->wwwroot . '/employer/dashboard.php'; ?>"><?php echo get_string('Corporateservice', 'local_job'); ?></a></li>
            <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=8'; ?>"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
            <!-- <li><a class="test-class3">TEST</a></li> -->
        </ul>
        <p>
            <?php foreach (array('vi' => 'VN', 'en' => 'EN', 'ko' => 'KO') as $lang => $val) : ?>
                <a href="<?php echo new moodle_url($PAGE->url, array('lang' => $lang)); ?>" class="<?php echo ($currlang == $lang ? 'on' : ''); ?>">
                    <?php echo $val;  ?>
                </a>
            <?php endforeach; ?>
        </p>
    </div>
    <div class="wp">
        <div class="mx">
            <div class="logo">
                <a href="<?php echo $VISANG->wwwroot; ?>">
                    <!--  -->
                    <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/logo_not_beta_job.png'; ?>" />
                </a>
            </div>

            <div class="m-menu-area">
                <a class="m-close cursor-pointer"><?php echo get_string('close', 'local_mypage'); ?></a>
                <div class="m-tp">
                    <?php if (isloggedin()) : ?>
                        <?php if($comid == null){?>
                            <div><?php echo $USER->firstname . ' ' . $USER->lastname; ?></div>
                            <a href="<?php echo $VISANG->wwwroot; ?>/my/info.php" class="btns br"><?php echo get_string('MyInfo', 'local_job'); ?></a>
                        <?php }else{?>
                            <div><?php echo $comid->company_name; ?></div>
                            <a href="<?php echo $VISANG->wwwroot; ?>/my/info.php" class="btns br"><?php echo get_string('MyInfo', 'local_job'); ?></a>
                        <?php }?>

                    <?php else : ?>
                        <div><?php echo get_string('getlogin', 'theme_oklassedu'); ?></div>
                        <a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php" class="btns br"><?php echo get_string('login', 'theme_oklassedu'); ?>/<?php echo get_string('signup', 'theme_oklassedu'); ?></a>
                    <?php endif; ?>
                </div>
                <div class="m-scrl">
                    <div>


                        <?php if (isloggedin()) { ?>

                            <?php if($comid == null){?>
                            <div class="hidden-menu custom-select bs-disabled <?php echo $VISANG->theme->menu == 'jobs' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/jobs/index.php'; ?>"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'companies' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/companies/index.php'; ?>"><?php echo get_string('Company', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'resumes' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/resumes/manage.php'; ?>"><?php echo get_string('Resume_2', 'local_job'); ?></a></strong>
                            </div>

                            <div class="custom-select no-pc d-lg-none bs-disabled <?php echo $VISANG->theme->menu == 'myinfo' ? 'active' : ''; ?>">
                                <strong><?php echo get_string('menu:mypage', 'theme_oklassedu'); ?></strong>
                                <ul class="d-block">
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/info.php"><?php echo get_string('MyInfo', 'local_job'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/apply.php"><?php echo get_string('SupportStatus', 'local_job'); ?></a></li>
                                    <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/inter.php"><?php echo get_string('Interestinformation', 'local_job'); ?></a></li>
                                    <li><a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a></li>
                                    <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                    <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                </ul>
                            </div>
                        <?php } else{?>
                            <script type="text/javascript">$(".hidden-menu").addClass("d-none");</script>
                            <style>
                                .custom-select > strong > a {
                                    padding: 0 16px !important;
                                    font-size: 16px !important;
                                }
                            </style>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'menuJobs' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/employer/job_notices.php';?>"><?php echo get_string('employer:menuJobs', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'menuApply' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/employer/applicants.php';?>"><?php echo get_string('employer:menuApply', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'menuInfo' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/employer/info.php?step=1';?>"><?php echo get_string('employer:menuInfo', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'menuLegalTax' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/employer/legaltax.php';?>"><?php echo get_string('employer:menuLegalTax', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select no-pc d-lg-none bs-disabled <?php echo $VISANG->theme->menu == 'myinfo' ? 'active' : ''; ?>">
                                <strong><?php echo get_string('menu:mypage', 'theme_oklassedu'); ?></strong>
                                <ul class="d-block">

                                    <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                    <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                </ul>
                            </div>
                        <?php }?>
                        <?php } else { ?>
                            <div class="hidden-menu custom-select bs-disabled <?php echo $VISANG->theme->menu == 'jobs' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/jobs/index.php'; ?>"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></a></strong>
                            </div>
                            <div class="custom-select bs-disabled <?php echo $VISANG->theme->menu == 'companies' ? 'active' : ''; ?>">
                                <strong><a href="<?php echo $VISANG->wwwroot . '/companies/index.php'; ?>"><?php echo get_string('Company', 'local_job'); ?></a></strong>
                            </div>

                        <?php }?>

                        <div class="bt">
                            <a href="/" target="_blank" class="ic-learn"><?php echo get_string('Elearning', 'local_job'); ?></a> <span class=""><a href="#" class="tx d-none"><?php echo get_string('Corporateservice', 'local_job'); ?></a></span>
                            <ul>
                                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=<?php echo $term_category; ?>', '<?php echo get_string('terms', 'theme_oklassedu'); ?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms', 'theme_oklassedu'); ?></a></li>
                                <!--<li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2', '<?php echo get_string('terms3', 'theme_oklassedu'); ?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms3', 'theme_oklassedu'); ?></a></li>-->
                                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=<?php echo $term_category; ?>', '<?php echo get_string('terms2', 'theme_oklassedu'); ?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms2', 'theme_oklassedu'); ?></a></li>
                            </ul>
                            <p class="cp">Copyright © VISANG Education Group Vietnam Company</p>
                        </div><!-- mobile footer -->
                    </div>

                </div>
                <div class="m-bt">
                    <p class="bg">
                        <span><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=8'; ?>"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></span>
                        <span><a href="#" onclick="mkjobs_logout()"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></span>
                    </p>

                    <p class="ic">
                        <span class="ic01">
                            <strong><a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=12'; ?>" target="_blank"><?php echo get_string('qna', 'local_mypage'); ?></a></strong>
                        </span>
                        <span class="ic02">
                            <strong><?php echo get_string('facebook', 'local_mypage'); ?><br /><?php echo get_string('counseling', 'local_mypage'); ?></strong>
                        </span>
                    </p>

                    <div>
                        <form action="<?php echo $VISANG->wwwroot; ?>" method="get" class="d-flex">
                            <div class="mr-auto">
                                <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank"><img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/icon_sns.png'; ?>" alt="sns" /></a>
                                <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank"><img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/icon_youtube.png'; ?>" alt="youtube" /></a>
                            </div>
                            <select name="lang" onchange="this.form.submit();">
                                <?php foreach ($langs as $lang => $val) : ?>
                                    <option value="<?php echo $lang; ?>" <?php echo ($currlang == $lang ? 'selected' : '') ?>>
                                        <?php echo strtoupper($lang) ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div class="f-r">


                <?php if (isloggedin() && (strpos($_SERVER['REQUEST_URI'], "information_sharing") === false)) : ?>
                    <?php if($comid == null){?>
                        <?php
                        $notifications = mkjobs_get_notifications();
                        $ma_cnt = mkjobs_count_notifications();
                        ?>
                        <div class="search-evt cursor-pointer">
                            <a class="pointer-events-none">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/icon_search.png'; ?>" />
                            </a>
                        </div>
                        <div class="alram">
                            <a href="#">
                                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/icon_bell.png'; ?>" alt="<?php echo get_string('notice', 'theme_oklassedu'); ?>" />
                                <?php if ($ma_cnt > 0) : ?>
                                    <span><?php echo ($ma_cnt < 100) ? intval($ma_cnt) : '99+'; ?></span>
                                <?php endif; ?>
                            </a>
                            <div class="alram-list">
                                <ul>
                                    <?php foreach ($notifications as $message) : ?>
                                        <?php $time = mkjobs_notifi_time($message->timecreated); ?>
                                        <li>
                                            <span class="close-al"><?php echo get_string('close', 'local_mypage'); ?></span>
                                            <div><?php echo $message->smallmessage; ?></div>
                                            <p onclick="">
                                                <?php
                                                echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                                ?>
                                            </p>
                                            <span><?php echo $time; ?></span>
                                        </li>
                                    <?php endforeach; ?>

                                    <?php if (empty($notifications)) : ?>
                                        <div class="no-data" style="margin-bottom: 0px;"><?php echo get_string('nodata:notifi', 'local_mypage'); ?></div>
                                    <?php else : ?>
                                        <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="all-btn"><?php echo get_string('Viewall', 'local_job'); ?></a>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    <?php }?>

                    <div class="user-info">

                        <?php if($comid == null){?>
                            <p>
                                <span><?php echo $USER->firstname . ' ' . $USER->lastname; ?></span>
                            </p>

                        <?php }else{?>
                            <p>
                                <span><?php echo $comid->company_name; ?></span>
                            </p>
                        <?php }?>
                        <ul>
                            <?php if (is_siteadmin()) : ?>
                                <li><a href="<?php echo $VISANG->elearning_wwwroot; ?>/local/management" target="_blank"><?php echo get_string('gomanagement', 'theme_oklassedu'); ?></a></li>
                            <?php endif; ?>
                            <?php if($comid == null){?>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/info.php"><?php echo get_string('MyInfo', 'local_job'); ?></a></li>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/apply.php"><?php echo get_string('SupportStatus', 'local_job'); ?></a></li>
                                <li><a href="<?php echo $CFG->wwwroot; ?>/local/job/my/inter.php"><?php echo get_string('Interestinformation', 'local_job'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>"><?php echo get_string('notice2', 'theme_oklassedu'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=12'; ?>"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                <li><a href="#" onclick="mkjobs_logout()"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></li>

                            <?php } else{?>

                                <li><a href="<?php echo $VISANG->wwwroot . '/my/qna/index.php?type=13'; ?>"><?php echo get_string('qna', 'local_mypage'); ?></a></li>
                                <li><a href="<?php echo $VISANG->wwwroot . '/my/edit.php'; ?>"><?php echo get_string('myinfoedit', 'local_mypage'); ?></a></li>
                                <li><a href="#" onclick="mkjobs_logout()"><?php echo get_string('logout', 'theme_oklassedu'); ?></a></li>
                            <?php }?>
                        </ul>
                    </div>
                <?php else : ?>
                    <div class="search-evt cursor-pointer">
                        <a class="pointer-events-none">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/icon_search.png'; ?>" />
                        </a>
                    </div>
                    <div class="alram">
                        <a href="#">
                            <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/icon_bell.png'; ?>" alt="<?php echo get_string('notice', 'theme_oklassedu'); ?>" />
                            <?php if ($ma_cnt > 0) : ?>
                                <span><?php echo ($ma_cnt < 100) ? intval($ma_cnt) : '99+'; ?></span>
                            <?php endif; ?>
                        </a>
                        <div class="alram-list">
                            <ul>
                                <?php foreach ($notifications as $message) : ?>
                                    <?php $time = mkjobs_notifi_time($message->timecreated); ?>
                                    <li>
                                        <span class="close-al"><?php echo get_string('close', 'local_mypage'); ?></span>
                                        <div><?php echo $message->smallmessage; ?></div>
                                        <p onclick="">
                                            <?php
                                            echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
                                            ?>
                                        </p>
                                        <span><?php echo $time; ?></span>
                                    </li>
                                <?php endforeach; ?>

                                <?php if (empty($notifications)) : ?>
                                    <div class="no-data" style="margin-bottom: 0px;"><?php echo get_string('nodata:notifi', 'local_mypage'); ?></div>
                                <?php else : ?>
                                    <a href="<?php echo $VISANG->wwwroot . '/my/notification.php'; ?>" class="all-btn"><?php echo get_string('Viewall', 'local_job'); ?></a>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="login-area">
                        <a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php" class="btns br"><?php echo get_string('signup', 'theme_oklassedu'); ?>/<?php echo get_string('login', 'theme_oklassedu'); ?></a>
                    </div>
                <?php endif; ?>
            </div>
            <span class="m-menu"></span>
        </div>
    </div>
    <!-- 검색 영역 -->
    <?php if($comid==null){?>
        <div class="hd-search-area">
            <a href="#" class="close-search"> <?php echo get_string('close', 'local_mypage'); ?></a>
            <form class="hd-fr" action="<?php echo $VISANG->wwwroot . '/search.php'; ?>" method="get">
                <input type="text" name="searchtext" id="searchinputMain" placeholder="<?php echo get_string('searchBox', 'local_job'); ?>" />
                <input type="submit" value="<?php echo get_string('search', 'theme_oklassedu') ?>" />
            </form>
            <ul class="hash-area">
                <?php foreach ($job_search_hashtags as $text) : ?>
                    <li>
                        <a href="<?php echo $VISANG->wwwroot . '/search.php?searchtext=' . $text->title; ?>"><?php echo $text->title; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php }?>
    <!-- 검색 영역 -->



</div><!-- header end -->
