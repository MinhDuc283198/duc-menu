<?php
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG;

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

echo $OUTPUT->header();
?>
<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('logout:confirm', 'theme_oklassedu'); ?>
        <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    </div>

    <div class="pop-contents">
    </div>
    <div class="btn-area text-center">
        <a href="<?php echo $VISANG->wwwroot; ?>/auth/logout.php?sesskey=<?php echo $USER->sesskey; ?>" class="btns point">
            <?php echo get_string('confirm', 'local_oklearning'); ?>
        </a>
        <a href="#" class="btns pop-close">
            <?php echo get_string('cancel', 'local_oklearning'); ?>
        </a>
    </div>
</div>
<?php
echo $OUTPUT->footer();
