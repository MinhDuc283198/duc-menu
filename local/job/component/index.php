<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/lib.php');

global $CFG, $VISANG, $DB, $USER;

// ===================================================================================================
// handles
require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$job_search_hashtags = mkjobs_get_searchtext('jobs');

$top_banners = mkjobs_get_banners('top');

// bottom notice
$nsql = "SELECT jc.id,jc.title,jc.timecreated,u.firstname, u.lastname, jc.board FROM {jinoboard_contents} jc
        JOIN {user} u ON jc.userid = u.id
        WHERE board = 8 AND isnotice = 0 ORDER BY jc.timecreated DESC LIMIT 3";
$notice = $DB->get_records_sql($nsql);

$isHomePage = true;

// ===================================================================================================
// renders

// add slideshows
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->header();
?>
<!-- Start slider photo -->
<div class="main-slider">
	<?php foreach ($top_banners as $banner) : ?>
		<div class="sd" style="background: #<?php echo $banner->colorcode; ?>">
			<?php if ($banner->linkurl != '') : ?>
				<a href="<?php echo $banner->linkurl; ?>" target="_blank">
					<img class="d-md-none" src="<?php echo $banner->path_modile; ?>">
					<img class="d-none d-md-inline-block" src="<?php echo $banner->path; ?>">
				</a>
			<?php else : ?>
				<img class="d-md-none" src="<?php echo $banner->path_modile; ?>">
				<img class="d-none d-md-inline-block" src="<?php echo $banner->path; ?>">
			<?php endif; ?>
			<?php if ($banner->search == 1) : ?>
				<div class="group">
					<h2><?php echo get_string('FindJob', 'local_job'); ?></h2>
					<form class="main-search" action="<?php echo $VISANG->wwwroot . '/search.php'; ?>" method="get">
						<input type="text" name="searchtext" placeholder="<?php echo get_string('searchBox', 'local_job'); ?>" id="searchinputSlide<?php echo $banner->id; ?>" />
						<input type="submit" value="<?php echo get_string('search', 'local_job'); ?>" />
					</form>

					<ul class="hash-area">
						<?php foreach ($job_search_hashtags as $text) : ?>
							<li>
								<a href="<?php echo $VISANG->wwwroot . '/search.php?searchtext=' . $text->title; ?>"><?php echo $text->title; ?></a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
</div>
<!-- End slider -->
<div class="main-contents">
	<div class="white-area hasbx">
		<div class="group">
			<div class="green-bx">
				<p>
					<span><em><a href="<?php echo $VISANG->wwwroot . '/my/qna/write.php?type=12'; ?>"><?php echo get_string('qna', 'local_mypage'); ?></a></em></span>
					<span><em><a href="https://www.messenger.com/t/111453797011701" target="_blank"><?php echo get_string('facebook', 'theme_oklassedu'); ?><br /><?php echo get_string('counseling', 'theme_oklassedu'); ?></a></em></span>
				</p>
				<p>
					<strong><?php echo get_string('viewmasterkorea', 'theme_oklassedu'); ?></strong>
					<a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><?php echo get_string('facebook', 'theme_oklassedu'); ?></a>
					<a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank" class="ic-youtube"><?php echo get_string('youtube', 'theme_oklassedu'); ?></a>
				</p>
			</div>
		</div>
		<div class="group">
			<div class="overflow-auto">
				<ul class="cate-list w-100 d-nowrap">
					<?php
					$skill_tags_sql = "SELECT s.*
						FROM {vi_skills} s
						JOIN {vi_job_skills} js ON s.id = js.skill_id
						GROUP BY s.id
						ORDER BY js.id DESC, s.name
						LIMIT 8";
					$skill_tags = $DB->get_records_sql($skill_tags_sql, array());
					if (count($skill_tags) == 0) {
						$skill_tags = $DB->get_records('vi_skills', array(), 'name', '*', 0, 8);
					}
					?>
					<?php foreach ($skill_tags as $skill_tag) : ?>
						<li><a href="<?php echo $VISANG->wwwroot . '/search.php?searchtext=' . $skill_tag->name; ?>"><?php echo $skill_tag->name; ?></a></li>
					<?php endforeach; ?>
					<li><a href="<?php echo $VISANG->wwwroot . '/search.php?searchtext='; ?>"><?php echo get_string('AllJob', 'local_job'); ?></a></li>
				</ul>
			</div>

			<?php if (!isloggedin()) : ?>
				<div class="login-area">
					<span><?php echo get_string('getlogin2', 'theme_oklassedu'); ?></span>
					<a href="<?php echo $VISANG->wwwroot; ?>/auth/login.php" class="btns br"><?php echo get_string('signup', 'theme_oklassedu'); ?>/ <?php echo get_string('login', 'theme_oklassedu'); ?></a>
				</div>
			<?php endif; ?>

			<?php if (isloggedin()) : ?>
				<?php
				$user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $USER->id));
				$kid = $user_korean_level->korean_level_id;
				if ((int) $kid == 0) :
				?>
					<div class="login-area">
						<span><?php echo get_string('SetinterestinfoSS', 'local_job'); ?></span>
						<a onclick="openFavourite();" class="btns br"><?php echo get_string('Setinterestinfo', 'local_job'); ?></a>
					</div>
				<?php endif; ?>

			<?php endif; ?>

		</div>
		<div class="group">
			<h5 class="pg-tit t-blue02"><?php echo get_string('PopularjobsTitle', 'local_job'); ?></h5>
			<div class="row jobs-cmpny-list slider-evt">
				<!-- add class "jobs-cmpny-list slider-evt" -->
				<?php
				$recommend_jobs = mkjobs_get_recommend_jobs(isloggedin());
				?>
				<?php foreach ($recommend_jobs as $recommend_job) : ?>
					<?php
					$saved_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $recommend_job->id));
					?>
					<div class="col-sm-6 col-md-4 col-lg-3 px-0">
						<div class="video-card">
							<div class="position-relative video-card-img border">
								<a class="d-flex justify-content-center" style="height: 200px;" href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id; ?>">
									<img class="align-self-center contain-image" src="<?php echo $recommend_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $recommend_job->company_logo; ?>">
								</a>
								<div class="d-flex justify-content-between video-time px-3 position-absolute bottom-0 w-100">
									<p>
										<span class="cursor-pointer ic-sm ic <?php echo mkjobs_has_saved_job($recommend_job->id) ? "ic-heart-on" : "ic-heart";  ?>" onclick="savedJob(this, '<?php echo $recommend_job->id; ?>')"></span>
										<span style="padding-left: 6px;" id="numSaved_<?php echo $recommend_job->id; ?>"><?php echo $saved_jobs_cnt; ?></span>
									</p>
									<p>~<?php echo date('Y\.m\.d', $recommend_job->enddate); ?></p>
								</div>
							</div>
							<div class="video-thub px-0 bg-white">
								<h6>
									<a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id; ?>">
										<?php echo $recommend_job->title; ?>
									</a>
								</h6>
								<p>
									<a href="<?php echo $VISANG->wwwroot . '/companies/view.php?employerid=' . $recommend_job->employer_id; ?>">
										<?php echo $recommend_job->company_name; ?>
									</a>
								</p>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="bg-area">
		<div class="group">
			<h5 class="pg-tit t-blue02"><?php echo get_string('NewJobs', 'local_job'); ?></h5>
			<div class="row mx-0">
				<?php
				$thistime = time();
				$new_jobsql = "SELECT vj.*,ve.company_name, ve.company_logo
							FROM {vi_jobs} vj
							JOIN {vi_employers} ve on vj.employer_id = ve.id
							WHERE vj.published = 1 AND ((vj.startdate=0) OR (vj.startdate < $thistime AND $thistime < vj.enddate))
							ORDER BY vj.timecreated DESC, vj.enddate DESC
							LIMIT 4";
				$new_jobs = $DB->get_records_sql($new_jobsql, array());
				?>
				<?php foreach ($new_jobs as $new_job) : ?>
					<?php
					$saved_new_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $new_job->id));
					?>
					<div class="col-sm-6 col-md-4 col-lg-3 px-0">
						<div class="video-card">
							<div class="position-relative video-card-img bg-white">
								<a class="d-flex justify-content-center" style="height: 200px;" href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $new_job->id; ?>">
									<img class="align-self-center contain-image" src="<?php echo $new_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $new_job->company_logo; ?>">
								</a>
								<div class="d-flex justify-content-between video-time px-3 position-absolute bottom-0 w-100">
									<p>
										<span class="cursor-pointer ic-sm ic <?php echo mkjobs_has_saved_job($new_job->id) ? "ic-heart-on" : "ic-heart";  ?>" onclick="savedJob(this, '<?php echo $new_job->id; ?>')"></span>
										<span style="padding-left: 6px;" id="numSaved_<?php echo $new_job->id; ?>"><?php echo $saved_new_jobs_cnt; ?></span>
									</p>
									<p>~<?php echo date('Y\.m\.d', $new_job->enddate); ?></p>
								</div>
							</div>
							<div class="video-thub bg-white">
								<h6>
									<a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $new_job->id; ?>">
										<?php echo $new_job->title; ?>
									</a>
								</h6>
								<p>
									<a href="<?php echo $VISANG->wwwroot . '/companies/view.php?employerid=' . $new_job->employer_id; ?>">
										<?php echo $new_job->company_name; ?>
									</a>
								</p>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="white-area scd">
		<div class="group">
			<h5 class="pg-tit t-blue02"><?php echo get_string('MasterKoreanProposalCourse', 'local_job'); ?></h5>
			<div class="ply-list slider">
				<ul>
					<?php
					require_once($CFG->dirroot . '/local/course/lib.php');
					$recommend_courses = mkjobs_get_recommend_courses();
					?>
					<?php foreach ($recommend_courses as $course) : ?>
						<?php
						$context = context_course::instance($course->lcocousreid);
						$courseimg = local_course_get_imgpath($context->id);
						?>
						<li>
							<a href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $course->id; ?>" target="_blank">
								<div class="img">
									<img src="<?php echo $courseimg; ?>" alt="<?php echo $course->coursename; ?>" />
									<span class="ply">play</span>
								</div>
								<div class="txt">
									<div class="tit"><?php echo $course->coursename; ?></div>
									<p><?php echo $course->onelineintro; ?></p>
								</div>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>

		<div class="group">
			<ul class="noti-list border-left border-bottom border-right py-1 px-3">
				<?php foreach ($notice as $noti) : ?>
					<li>
						<strong><?php echo get_string('notice', 'local_job'); ?></strong>
						<a href="<?php echo $VISANG->wwwroot . '/my/qna/detail.php?id=' . $noti->id . '&type=' . $noti->board; ?>"><?php echo $noti->title; ?></a>
						<span class="info">
							<span><?php echo $noti->firstname . $noti->lastname; ?></span>
							<span class="dt"><?php echo date('Y-m-d', $noti->timecreated); ?></span>
						</span>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
<script>
	function savedJob(el, id, classon, classoff) {
		$.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
			sesskey: "<?php echo sesskey(); ?>",
			action: "mkjobs_saved_job",
			jobid: id
		}, function(data) {
			if (data == -1) {
				if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
					window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $VISANG->wwwroot; ?>";
				}
			} else if (data == 1) {
				$(el).removeClass(classoff || "ic-heart");
				$(el).addClass(classon || "ic-heart-on");
				$('#numSaved_' + id).html(parseInt($('#numSaved_' + id).html()) + 1);
			} else {
				$(el).removeClass(classon || "ic-heart-on");
				$(el).addClass(classoff || "ic-heart");
				$('#numSaved_' + id).html(parseInt($('#numSaved_' + id).html()) - 1);
			}
		});
	}

	<?php if (count($recommend_jobs) < 4) : ?>
		$(function() {
			setTimeout(function() {
				$('.jobs-cmpny-list .slick-list .slick-track').css('width', '100%');
			}, 1000);
		});
	<?php endif; ?>
</script>
<?php
include_once $VISANG->dirroot . '/component/favourite.php'; 
?>

<?php if (isloggedin()) : ?>
	<?php
	$user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $USER->id));
	$kid = $user_korean_level->korean_level_id;
	if ((int) $kid == 0 ) :
	?>
<script type="text/javascript">  
        openFavourite();
</script>
	<?php endif; ?>

<?php endif; 

require_once($VISANG->dirroot . '/lib/popup_lib.php');
$VISANG->theme->footer();
