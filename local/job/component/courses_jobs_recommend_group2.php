<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$recommendcourses = local_course_recommend(isloggedin());
?>
<style>
    .title-job, .title-course {
        font-style: normal;
        font-weight: bold;
        font-size: 23px;
        line-height: 27px;
        text-align: center;
        color: #828282;
    }

    #switch-job:checked ~ header .lbl-job p {
        color: #000000;
    }

    #switch-course:checked ~ header .lbl-course p {
        color: #000000;
    }

    .rbswitch {

    }

    .lbl-jc + .lbl-jc {
        margin-left: 40px;
    }

    .sliders-job, .sliders-course {
        width: 100%;
        margin: 0 auto;
        height: 100px;
    }

    .sliders-job {
        background: #0c5460;
    }

    .sliders-course {
        background: #0b0b0b;
    }

    .card-area {
        overflow: hidden;
    }

    .card-area .cards {
        display: flex;
        width: 200%;
    }

    .cards .row-1 {
        transition: all 0.3s ease;
    }

    .cards .row {
        width: 50%;
    }

    #switch-job:checked ~ .card-area .cards .row-1 {
        margin-left: 0%;
    }

    #switch-course:checked ~ .card-area .cards .row-1 {
        margin-left: -50%;
    }
    .carousel-control-prev,.carousel-control-next{
        border-radius: 50%;
        width: 35px;
        height: 35px;
    }
    .carousel-control-prev-icon, .carousel-control-next-icon{
        width: 9.28px;
        height: 15.19px;
    }
    .carousel-control-next-icon{
        transform: rotate(180deg);
    }

</style>
<div class="row">
    <div class="col-xl-12">
        <input type="radio" class="rbswitch" name="switch-job-course" id="switch-job" checked>
        <input type="radio" class="rbswitch" name="switch-job-course" id="switch-course">
        <header>
            <div class="row switch-job-course">
                <label for="switch-job" class="lbl-job lbl-jc">
                    <p class="title-job">Recomended Jobs</p>
                </label>
                <label for="switch-course" class="lbl-course lbl-jc">
                    <p class="title-course">Recomended courses</p>
                </label>
            </div>
        </header>
        <div class="card-area">
            <div class="cards">
                <div class="row row-1">
                    <div id="recommend-employee-job-list" class="carousel slide recommend-employee-pc"
                         data-ride="carousel"
                         data-interval="false">
                        <div class="carousel-inner list-recommend-employee-padding" role="listbox">
                            <?php $count = 0; ?>
                            <?php
                            $recommend_jobs = mkjobs_get_recommend_jobs(isloggedin());
                            ?>
                            <?php foreach ($recommend_jobs as $recommend_job) : ?>
                                <?php
                                $saved_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $recommend_job->id));
                                ?>
                                <?php
                                if ($count == 0 || $count % 4 == 0) {
                                    if ($count == 0) {
                                        echo '<div class="carousel-item active">';
                                        echo '<div class="row">';
                                    } else {
                                        echo '<div class="carousel-item">';
                                        echo '<div class="row">';
                                    }
                                }
                                ?>
                                <div class="col-xl-3 col-sm-6 recommend-item">
                                    <div style="position: relative">
                                        <a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id; ?>">
                                            <img src="<?php echo $recommend_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $recommend_job->company_logo; ?>"
                                                 alt="Image of recommend job" class="img-recommend-employee">
                                        </a>
                                        <!--                                    <div class="carousel-caption d-none d-md-block star-point">-->
                                        <!--                                        --><?php //for ($i = 1; $i <= 5; $i++) { ?>
                                        <!--                                            --><?php //if ($i <= $recommend_job->star) { ?>
                                        <!--                                                <i style="color: #FFE000 !important; padding: 8px 5px;"-->
                                        <!--                                                   class="fa fa-star"></i>-->
                                        <!--                                            --><?php //} else { ?>
                                        <!--                                                <i style="color: #FFE840; padding: 8px 5px;"-->
                                        <!--                                                   class="fal fa-star"></i>-->
                                        <!--                                            --><?php //} ?>
                                        <!--                                        --><?php //} ?>
                                        <!--                                    </div>-->
                                    </div>
                                </div>
                                <?php $count++; ?>
                                <?php
                                if ($count % 4 == 0 || $count == count($recommend_jobs)) {
                                    echo '</div>';
                                    echo '</div>';
                                }
                                ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="pagination-listjob">
                            <?php
                            if (count($recommend_jobs) % 4 == 0) {
                                $recommend_job_slide_dot = count($recommend_jobs) / 4;
                            } else {
                                $recommend_job_slide_dot = ceil(count($recommend_jobs) / 4);
                            }
                            ?>
                            <?php if ($recommend_job_slide_dot != 0) : ?>
                                <a class="carousel-control-prev recommend-job-control pc"
                                   href="#recommend-employee-job-list"
                                   role="button"
                                   data-slide="prev"
                                   onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <ol class="carousel-indicators list-dot">
                                    <?php for ($i = 0; $i < $recommend_job_slide_dot; $i++) : ?>
                                        <?php if ($i == 0) : ?>
                                            <li data-target="#recommend-employee-job-list" data-slide-to="0"
                                                class="recommend-job-control pc active"
                                                onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                                1
                                            </li>
                                        <?php else : ?>
                                            <li data-target="#recommend-employee-job-list"
                                                data-slide-to="<?php echo $i; ?>"
                                                class="recommend-job-control pc"
                                                onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                                <?php echo $i + 1; ?>
                                            </li>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </ol>
                                <a class="carousel-control-next recommend-job-control pc"
                                   href="#recommend-employee-job-list"
                                   role="button"
                                   data-slide="next"
                                   onclick="paginationSlide('recommend-job-control.pc',<?php echo $recommend_job_slide_dot ?>)">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            <?php else : ?>
                                <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="recommend-employee-course-list" class="carousel slide recommend-employee-pc"
                         data-ride="carousel"
                         data-interval="false">
                        <div class="carousel-inner list-recommend-employee-padding" role="listbox">
                            <?php $count = 0; ?>
                            <?php foreach ($recommendcourses

                                           as $recommendcourse) : ?>
                                <?php
                                $context = context_course::instance($recommendcourse->lcocourseid);
                                $path = local_course_get_imgpath($context->id);
                                ?>
                                <?php
                                if ($count == 0 || $count % 4 == 0) {
                                    if ($count == 0) {
                                        echo '<div class="carousel-item active">';
                                        echo '<div class="row">';
                                    } else {
                                        echo '<div class="carousel-item">';
                                        echo '<div class="row">';
                                    }
                                }
                                ?>
                                <div class="col-xl-3 col-sm-6 recommend-item">
                                    <div style="position: relative">
                                        <a href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $recommendcourse->id; ?>">
                                            <img src="/theme/oklassedu/pix/images/employee-recommend.png"
                                                 alt="Image of recommend course" class="img-recommend-employee">
                                        </a>
                                        <div class="carousel-caption d-none d-md-block star-point">
                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                <?php if ($i <= $recommendcourse->star) { ?>
                                                    <i style="color: #FFE000 !important; padding: 8px 5px;"
                                                       class="fa fa-star"></i>
                                                <?php } else { ?>
                                                    <i style="color: #FFE840; padding: 8px 5px;"
                                                       class="fal fa-star"></i>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $count++; ?>
                                <?php
                                if ($count % 4 == 0 || $count == count($recommendcourses)) {
                                    echo '</div>';
                                    echo '</div>';
                                }
                                ?>
                            <?php endforeach; ?>
                            <?php if (count($recommendcourses) < 1) {
                                $likes = $DB->get_records("lmsdata_course_like", array('userid' => $USER->id));
                                foreach ($likes as $like) {
                                    $course = $DB->get_record("lmsdata_course", array('courseid' => $like->moodlecourseid));
                                    $currentlang = current_language();
                                    $context = context_course::instance($course->courseid);
                                    $path = local_course_get_imgpath($context->id); ?>
                                    <?php
                                    if ($count == 0 || $count % 4 == 0) {
                                        if ($count == 0) {
                                            echo '<div class="carousel-item active">';
                                            echo '<div class="row">';
                                        } else {
                                            echo '<div class="carousel-item">';
                                            echo '<div class="row">';
                                        }
                                    }
                                    ?>
                                    <div class="col-xl-3 col-sm-6 recommend-item">
                                        <div style="position: relative">
                                            <a href="
                            <?php echo $CFG->wwwroot . '/local/course/detail.php?id=' . $course->courseid; ?>">
                                                <img src="<?php echo $path; ?>"
                                                     alt="Image of recommend course">
                                            </a>
                                            <div class="carousel-caption d-none d-md-block star-point">
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                    <?php if ($i <= $course->star) { ?>
                                                        <i style="color: #FFE000 !important; padding: 8px 5px;"
                                                           class="fa fa-star"></i>
                                                    <?php } else { ?>
                                                        <i style="color: #FFE840; padding: 8px 5px;"
                                                           class="fal fa-star"></i>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $count++; ?>
                                    <?php
                                    if ($count % 4 == 0 || $count == count($recommendcourses)) {
                                        echo '</div>';
                                        echo '</div>';
                                    }
                                    ?>
                                <?php }
                            } ?>
                        </div>
                        <div class="pagination-listjob">
                            <?php
                            if (count($recommendcourses) % 4 == 0) {
                                $recommend_course_slide_dot = count($recommendcourses) / 4;
                            } else {
                                $recommend_course_slide_dot = ceil(count($recommendcourses) / 4);
                            }
                            ?>
                            <?php if ($recommend_course_slide_dot != 0) : ?>
                                <a class="carousel-control-prev recommend-course-control pc"
                                   href="#recommend-employee-course-list" role="button"
                                   data-slide="prev"
                                   onclick="paginationSlide('recommend-course-control.pc',<?php echo $recommend_course_slide_dot ?>)">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <ol class="carousel-indicators list-dot">
                                    <?php for ($i = 0; $i < $recommend_course_slide_dot; $i++) : ?>
                                        <?php if ($i == 0) : ?>
                                            <li data-target="#recommend-employee-course-list" data-slide-to="0"
                                                class="recommend-course-control pc active"
                                                onclick="paginationSlide('recommend-course-control.pc',<?php echo $recommend_course_slide_dot ?>)">
                                                1
                                            </li>
                                        <?php else : ?>
                                            <li data-target="#recommend-employee-course-list"
                                                data-slide-to="<?php echo $i; ?>"
                                                class="recommend-course-control pc"
                                                onclick="paginationSlide('recommend-course-control.pc',<?php echo $recommend_course_slide_dot ?>)">
                                                <?php echo $i + 1; ?>
                                            </li>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </ol>
                                <a class="carousel-control-next recommend-course-control pc"
                                   href="#recommend-employee-course-list" role="button"
                                   data-slide="next"
                                   onclick="paginationSlide('recommend-course-control.pc',<?php echo $recommend_course_slide_dot ?>)">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            <?php else : ?>
                                <h3><?php echo get_string('no-data', 'local_job'); ?></h3>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function paginationSlide(nameClass, numberPage) {
        if ($("li." + nameClass + ".active").attr("data-slide-to") != 0 && $("li." + nameClass + ".active").attr("data-slide-to") != (numberPage - 1)) {
            $("a." + nameClass).removeClass("arrow-hide");
            $("a." + nameClass).addClass("arrow-show");
        } else {
            if ($("li." + nameClass + ".active").attr("data-slide-to") == 0) {
                $("a.carousel-control-prev." + nameClass).removeClass("arrow-show");
                $("a.carousel-control-prev." + nameClass).addClass("arrow-hide");
            } else {
                $("a.carousel-control-prev." + nameClass).removeClass("arrow-hide");
                $("a.carousel-control-prev." + nameClass).addClass("arrow-show");
            }
            if ($("li." + nameClass + ".active").attr("data-slide-to") == (numberPage - 1)) {
                $("a.carousel-control-next." + nameClass).removeClass("arrow-show");
                $("a.carousel-control-next." + nameClass).addClass("arrow-hide");
            } else {
                $("a.carousel-control-next." + nameClass).removeClass("arrow-hide");
                $("a.carousel-control-next." + nameClass).addClass("arrow-show");
            }
        }
    }

    function resetPagination() {
        paginationSlide("recommend-course-control.pc",<?php echo $recommend_course_slide_dot; ?>);
        paginationSlide("recommend-job-control.pc",<?php echo $recommend_job_slide_dot; ?>);
    }

    setInterval(resetPagination, 1000);
</script>