<?php 
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/jobs.php');
//$context = context_system::instance();
//mkjobs_require_login();
//$PAGE->set_context($context);
$baseurl = new moodle_url($VISANG->wwwroot . '/component/addFavourite.php');
$skills_group = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_skill_groups} s");
$skills = $DB->get_records_sql("SELECT s.id,s.name FROM {vi_skills} s ORDER BY s.sortorder, s.timecreated");
$places = $DB->get_records("vi_districts", array());
?>


<div class="vis-favourite-pop">
    <a href="#" class="close-dialog"> Close</a>
    <div class="title-dialog">
    	<strong><?php echo get_string('Interestsettings', 'local_job'); ?></strong>
    	<p> <?php echo get_string('jobreferrals', 'local_job'); ?></p>
    </div>
    <hr>
    <form id="fav-fr">
    	<div id="fav-step0" class="session d-block">
        	<div class="row justify-content-center oval-number">
        		<h2 class="current">1</h2><h6></h6><h2>2</h2><h6></h6><h2>3</h2><h6></h6><h2>4</h2><h6></h6><h2>5</h2>
            </div>
            <h3><?php echo get_string('Expeer', 'local_job'); ?></h3>
            <p><?php echo get_string('ExpeerExplan', 'local_job'); ?></p>
            
<!--             <input type="text" name="fav-step0-txt1" id="fav-step0-txt1" value="" hidden> -->
<!--             <input type="text" name="fav-step0-txt2" id="fav-step0-txt2" value="" hidden> -->
<!--             <div class="btn-area"> -->
<!--                <button type="button" id="fav-step0-nxt1" class="btns big"><?php echo get_string('Fresher', 'local_job'); ?></button> -->
<!--                <button type="button" id="fav-step0-nxt2" class="btns big"><?php echo get_string('Career', 'local_job'); ?></button> -->
<!--             </div> -->
            <div class="row justify-content-center">
            	<?php $lang  = current_language(); $work_experiences = $DB->get_records('vi_work_experiences', array(), 'sortorder ASC');?>
            	<?php foreach ($work_experiences as $work_experiences) : ?>
            		<button type="button" class="btn btn-outline-dark m-2 fav-experi" id="<?php echo 'fav-experi'.$work_experiences->id; ?>"><?php echo ($lang=='ko') ? $work_experiences->name_ko : ($lang=='en') ? $work_experiences->name_en : $work_experiences->name_vi; ?></button>
            	<?php endforeach; ?>

                <input type="text" name="fav-experience" id="fav-experience" value="" hidden>
            </div>
            <div class="btn-area mt-5">
                <button type="button" id="fav-step0-pre" class="btns big "><?php echo get_string('cancel', 'local_job'); ?></button>
                <button type="button" id="fav-step0-nxt" class="btns point big " disabled><?php echo get_string('Next', 'local_job'); ?></button>
            </div>
        </div>
        
        <div id="fav-step1" class="session d-none">
        	<div class="row justify-content-center oval-number">
        		<h2>1</h2><h6></h6><h2 class="current">2</h2><h6></h6><h2>3</h2><h6></h6><h2>4</h2><h6></h6><h2>5</h2>
            </div>
            <h3><?php echo get_string('Interestgroup', 'local_job'); ?></h3>
            <p><?php echo get_string('selectonlyonejob', 'local_job'); ?></p>
            <div class="option-container row pb-4" >
			<?php foreach ($skills_group as $jp) {?>
            	<div class="option-item" id="fav-step1-<?php echo $jp->id;?>">
            		<img class="" src="/local/job/assets/dist/images/thub.png" alt="Include Image">
        			<div class="overlay-dark">
        				<p><?php echo $jp->name;?></p>
        			</div>
            	</div>
			<?php }?>
            </div>
<!--             <input type="text" id="fav-step1-txt" name="fav-step1-txt" value="3" hidden> -->
                <input type="text" name="fav-step1-txt1" id="fav-step1-txt1" value="" hidden>
                <input type="text" name="fav-step1-txt2" id="fav-step1-txt2" value="" hidden>
                <input type="text" name="fav-step1-txt3" id="fav-step1-txt3" value="" hidden>
            <div class="btn-area">
                <button type="button" id="fav-step1-pre" class="btns big "><?php echo get_string('Previous', 'local_job'); ?></button>
                <button type="button" id="fav-step1-nxt" class="btns point big" disabled><?php echo get_string('Next', 'local_job'); ?></button>
            </div>
        </div>
    	
    	<div id="fav-step2" class="session d-none ">
    		<div class="row justify-content-center oval-number">
        		<h2>1</h2><h6></h6><h2>2</h2><h6></h6><h2 class="current">3</h2><h6></h6><h2>4</h2><h6></h6><h2>5</h2>
            </div>
            <h3><?php echo get_string('JobofInterest', 'local_job'); ?></h3>
            <p><?php echo get_string('selectuptothreejobs', 'local_job'); ?></p>
            <!-- <h4><?php echo get_string('ITandInternet', 'local_job'); ?></h4>  -->
            
            <div class="row justify-content-center pb-4 px-4 option-container">
            <?php foreach ($skills as $skill) {?>
                <button type="button" class="btn btn-outline-dark m-2 fav-choi d-block clss-<?php echo $skill->skill_group_id?>" id="fav-step2-<?php echo $skill->id;?>"><?php echo $skill->name;?></button>
            <?php }?>
                <input type="text" name="fav-step2-txt1" id="fav-step2-txt1" value="" hidden>
                <input type="text" name="fav-step2-txt2" id="fav-step2-txt2" value="" hidden>
                <input type="text" name="fav-step2-txt3" id="fav-step2-txt3" value="" hidden>
            </div>
            <div class="btn-area">
                <button type="button" id="fav-step2-pre" class="btns big "><?php echo get_string('Previous', 'local_job'); ?></button>
                <button type="button" id="fav-step2-nxt" class="btns point big" disabled><?php echo get_string('Next', 'local_job'); ?></button>
            </div>
    	</div>
    
    	<div id="fav-step3" class="session d-none">
    		<div class="row justify-content-center oval-number">
        		<h2>1</h2><h6></h6><h2>2</h2><h6></h6><h2>3</h2><h6></h6><h2 class="current">4</h2><h6></h6><h2>5</h2>
            </div>
            <h3><?php echo get_string('Workarea', 'local_job'); ?></h3>
            <p><?php echo get_string('selectthearea', 'local_job'); ?></p>
            
            <div class="row justify-content-center">
            <?php foreach ($places as $place){?>
            	<button type="button" class="btn btn-outline-dark m-2 fav-pla" id="fav-step3-<?php echo $place->id;?>"><?php echo $place->name;?></button>
            	<input type="text" name="fav-step3[]" id="fav-step3-<?php echo $place->id;?>-txt" value="" hidden>
            <?php }?>
            </div>
            <div class="btn-area mt-5">
                <button type="button" id="fav-step3-pre" class="btns big "><?php echo get_string('Previous', 'local_job'); ?></button>
                <button type="button" id="fav-step3-nxt" class="btns point big " disabled><?php echo get_string('Next', 'local_job'); ?></button>
            </div>
    	</div>
    	
    	<div id="fav-step4" class="session d-none">
    		<div class="row justify-content-center oval-number">
        		<h2>1</h2><h6></h6><h2>2</h2><h6></h6><h2>3</h2><h6></h6><h2>4</h2><h6></h6><h2 class="current">5</h2>
            </div>
            <h3><?php echo get_string('KoreanLevel', 'local_job'); ?></h3>
            <p><?php echo get_string('KoreanLevelInfo', 'local_job'); ?></p>
            
            <div class="row justify-content-center">
            	<?php $lang  = current_language(); $korean_levels = $DB->get_records('vi_korean_levels', array(), 'name ASC');?>
            	<?php foreach ($korean_levels as $korean_level) : ?>
            		<button type="button" class="btn btn-outline-dark m-2 fav-lev" id="<?php echo 'fav-lev'.$korean_level->id; ?>"><?php echo ($lang=='ko') ? $korean_level->name_ko : ($lang=='en') ? $korean_level->name_en : $korean_level->name_vi; ?></button>
            	<?php endforeach; ?>

                <input type="text" name="fav-korea-level" id="fav-korea-level" value="" hidden>
            </div>
            <div class="btn-area mt-5">
                <button type="button" id="fav-step4-pre" class="btns big "><?php echo get_string('Previous', 'local_job'); ?></button>
                <button type="submit" id="fav-step4-nxt" class="btns point big " disabled><?php echo get_string('Next', 'local_job'); ?></button>
            </div>
    	</div>        
    </form>
</div>

<style>

.vis-favourite-pop {display: none;z-index: 0;position: absolute;width: 770px;left: 50%;transform: translate(-50%, 0);top: -300px;background: #fff;border-bottom: 1px solid #ddd;padding: 60px 0 0;box-sizing: border-box;min-height: 300px;transition: 0.4s;}
.vis-favourite-pop.on {top: 160px; display: block;z-index: 99998;}
.vis-favourite-pop>.title-dialog{font-size: 2em;position: absolute;left: 26px;top: 20px;display: inline-block;}
.vis-favourite-pop>.title-dialog>strong{font-size: 20px;display: inline-block;}
.vis-favourite-pop>.title-dialog>p{font-size: 16px;display: inline-block;}
.vis-favourite-pop>.close-dialog{font-size: 0;position: absolute;right: 20px;top: 20px;display: inline-block;background: url("/theme/oklassedu/pix/images/icon_close.png") no-repeat center;width: 40px;height: 40px;}
.vis-favourite-pop>#fav-fr>.session>.oval-number{padding-bottom: 30px;}
.vis-favourite-pop>#fav-fr>.session>.oval-number>h2{border-radius: 50%;border: 1px solid #3399ff;padding: 4px 9px;margin: 0 0;font-size: 16px;color: #3399ff; background: #fff; width: 30px; height: 30px;}
.vis-favourite-pop>#fav-fr>.session>.oval-number>h6{border-bottom: 3px solid #3399ff;width: 20px;height: 18px;font-size: 10px;}
.vis-favourite-pop>#fav-fr>.session>.oval-number>.current{background: #485cc7 !important; color: #fff !important;}
.vis-favourite-pop>#fav-fr>.session>.row>.current{background: #485cc7 !important; color: #fff !important;}
.vis-favourite-pop>#fav-fr>.session>.option-container>.current-img>.overlay-dark{background:rgba(0,0,0,0.9) !important;}
.vis-favourite-pop>#fav-fr>.session>h3{text-align: center; font-size: 26px;font-waight: bold; margin-bottom: 10px;}
.vis-favourite-pop>#fav-fr>.session>h4{text-align: center; font-size: 24px;font-waight: bold; margin-bottom: 10px;color: #485cc7;}
.vis-favourite-pop>#fav-fr>.session>p{text-align: center; font-size:16px; margin-bottom: 40px;}
.vis-favourite-pop>#fav-fr>.session>.option-container{overflow-y: scroll;height: 400px; margin-right: 0px; margin-left: 3rem;}
.vis-favourite-pop>#fav-fr>.session>.option-container>.option-item{position: relative;text-align: center;color: white;width: 120px; height: 80px; margin: 10px;}
.vis-favourite-pop>#fav-fr>.session>.option-container>.option-item>img{width: 120px; height: 80px;}
.vis-favourite-pop>#fav-fr>.session>.option-container>.option-item>.overlay-dark {content:"";display:block;position:absolute;top:0;bottom:0;left:0;right:0;background:rgba(0,0,0,0.4);}
.vis-favourite-pop>#fav-fr>.session>.option-container>.option-item>.overlay-dark:hover{background:rgba(0,0,0,0.8); cursor: pointer;}
.vis-favourite-pop>#fav-fr>.session>.option-container>.option-item>.overlay-dark>p{position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);opacity: 1;color: #fff; font-weight: bold;text-align: center;width: 100%;}
.vis-favourite-pop>#fav-fr>.session>.btn-area>button, .vis-favourite-pop>#fav-fr>.session>.btn-area>input{width: 50%; margin: 0 0 !important;}
@media screen and (max-width:991px){.vis-favourite-pop{width: 90%;}}
@media screen and (max-width:767px){.vis-favourite-pop{width: 100%;}}

</style>

<script>

var openFavourite = function () {
	$("html,body").animate({"scrollTop": 0});
    var target = $(".vis-favourite-pop");
    if (!target.hasClass("on")) {
        target.addClass("on");
        if ($(".my-wrap").length == 0) {
            $("body").append("<div class='my-wrap'></div>");
        }
        $(".my-wrap").fadeIn();
        if (utils.popup.bodyscroll()) {
            $("body").addClass("noScroll");
        }
        $(document).on("mouseup touchend", function (e) {
            if ($(e.target).closest(".search-evt").length == 0 && !$(e.target).hasClass(".vis-favourite-pop") && $(e.target).closest(".vis-favourite-pop").length == 0.) {
                target.removeClass("on");
                $(".my-wrap").fadeOut();
                $(this).off("mouseup");
                $(this).off("touchstart");
                $("body").removeClass("noScroll");
            }
        });

    } else {
        target.removeClass("on");
        $(".my-wrap").fadeOut();
        $("body").removeClass("noScroll");
    }
}

var closeFavourite = function () {
    $(".vis-favourite-pop.on").removeClass("on");
    $(".my-wrap").fadeOut();
    $("body").removeClass("noScroll");
}

$(".test-class3").off("click").click(openFavourite);
$(".vis-favourite-pop>.close-dialog").click(closeFavourite);
// $("#fav-fr").on("submit", function(){
// 	closeFavourite();
//     return false;
// })

$(".option-item").click(function(){
// 	var Id = jQuery(this).attr("id");
// 	$("#fav-step1-txt").val(Id.replace("fav-step1-",""));
// 	$("#fav-step1").removeClass("d-block");
// 	$("#fav-step1").addClass("d-none");
// 	$("#fav-step2").removeClass("d-none");
// 	$("#fav-step2").addClass("d-block");
//  window.alert($("#fav-step1-txt").val());
// 	$(".clss-"+$("#fav-step1-txt").val()).removeClass("d-none");
// 	$(".clss-"+$("#fav-step1-txt").val()).addClass("d-block");
	var Id = jQuery(this).attr("id");
	var choice1 = $("#fav-step1-txt1").val();
	var choice2 = $("#fav-step1-txt2").val();
	var choice3 = $("#fav-step1-txt3").val();

	if($("#"+Id).hasClass("current-img")){
		$("#"+Id).removeClass("current-img");
		if(choice1 == Id) $("#fav-step1-txt1").val("");
		if(choice2 == Id) $("#fav-step1-txt2").val("");
		if(choice3 == Id) $("#fav-step1-txt3").val("");
	}else{
		if(choice1.length < 1 || choice2.length < 1 || choice3.length < 1){
			$("#"+Id).addClass("current-img");
			if(choice1.length == 0) $("#fav-step1-txt1").val(Id);
			else if(choice2.length == 0) $("#fav-step1-txt2").val(Id);
			else if(choice3.length == 0) $("#fav-step1-txt3").val(Id);
		}else{
			alert("<?php echo get_string('my_inter:limit_3', 'local_job'); ?>");
		}
	}
	choice1 = $("#fav-step1-txt1").val();
	choice2 = $("#fav-step1-txt2").val();
	choice3 = $("#fav-step1-txt3").val();
	$(".fav-choi").removeClass("current-img");
	if(choice1.length > 0){
		$("#"+choice1).addClass("current-img");
	}
	if(choice2.length > 0){
		$("#"+choice2).addClass("current-img");
	}
	if(choice3.length > 0){
		$("#"+choice3).addClass("current-img");
	}
	if(choice1.length > 0 || choice2.length > 0 || choice3.length > 0){
		$("#fav-step1-nxt").prop('disabled', false);
	}else{
		$("#fav-step1-nxt").prop('disabled', true);
	}
});

// $("#fav-step0-nxt1").click(function(){
// 	$("#fav-step0").removeClass("d-block");
// 	$("#fav-step0").addClass("d-none");
// 	$("#fav-step1").removeClass("d-none");
// 	$("#fav-step1").addClass("d-block");
// 	$("#fav-step0-txt1").val("1");
// 	$("#fav-step0-txt2").val("0");
// 	$("#fav-step0-nxt1").addClass("point");
// 	$("#fav-step0-nxt2").removeClass("point");
// });

// $("#fav-step0-nxt2").click(function(){
// 	$("#fav-step0").removeClass("d-block");
// 	$("#fav-step0").addClass("d-none");
// 	$("#fav-step1").removeClass("d-none");
// 	$("#fav-step1").addClass("d-block");
// 	$("#fav-step0-txt1").val("0");
// 	$("#fav-step0-txt2").val("1");
// 	$("#fav-step0-nxt2").addClass("point");
// 	$("#fav-step0-nxt1").removeClass("point");
// });

$(".fav-choi").click(function(){
	var Id = jQuery(this).attr("id");
	var choice1 = $("#fav-step2-txt1").val();
	var choice2 = $("#fav-step2-txt2").val();
	var choice3 = $("#fav-step2-txt3").val();

	if($("#"+Id).hasClass("current")){
		$("#"+Id).removeClass("current");
		if(choice1 == Id) $("#fav-step2-txt1").val("");
		if(choice2 == Id) $("#fav-step2-txt2").val("");
		if(choice3 == Id) $("#fav-step2-txt3").val("");
	}else{
		if(choice1.length < 1 || choice2.length < 1 || choice3.length < 1){
			$("#"+Id).addClass("current");
			if(choice1.length == 0) $("#fav-step2-txt1").val(Id);
			else if(choice2.length == 0) $("#fav-step2-txt2").val(Id);
			else if(choice3.length == 0) $("#fav-step2-txt3").val(Id);
		}else{
			alert("<?php echo get_string('my_inter:limit_3', 'local_job'); ?>");
		}
	}
	choice1 = $("#fav-step2-txt1").val();
	choice2 = $("#fav-step2-txt2").val();
	choice3 = $("#fav-step2-txt3").val();
	$(".fav-choi").removeClass("current");
	if(choice1.length > 0){
		$("#"+choice1).addClass("current");
	}
	if(choice2.length > 0){
		$("#"+choice2).addClass("current");
	}
	if(choice3.length > 0){
		$("#"+choice3).addClass("current");
	}

	if(choice1.length > 0 || choice2.length > 0 || choice3.length > 0){
		$("#fav-step2-nxt").prop('disabled', false);
	}else{
		$("#fav-step2-nxt").prop('disabled', true);
	}
});

$("#fav-step1-pre").click(function(){
	$("#fav-step1").removeClass("d-block");
	$("#fav-step1").addClass("d-none");
	$("#fav-step0").removeClass("d-none");
	$("#fav-step0").addClass("d-block");
});

$("#fav-step1-nxt").click(function(){
	$("#fav-step1").removeClass("d-block");
	$("#fav-step1").addClass("d-none");
	$("#fav-step2").removeClass("d-none");
	$("#fav-step2").addClass("d-block");
});


$("#fav-step2-pre").click(function(){
	$("#fav-step2").removeClass("d-block");
	$("#fav-step2").addClass("d-none");
	$("#fav-step1").removeClass("d-none");
	$("#fav-step1").addClass("d-block");

//	$(".fav-choi").removeClass("d-block");
//	$(".fav-choi").addClass("d-none");
});

$("#fav-step2-nxt").click(function(){
	$("#fav-step2").removeClass("d-block");
	$("#fav-step2").addClass("d-none");
	$("#fav-step3").removeClass("d-none");
	$("#fav-step3").addClass("d-block");
	
});

$("#fav-step3-pre").click(function(){
	$("#fav-step3").removeClass("d-block");
	$("#fav-step3").addClass("d-none");
	$("#fav-step2").removeClass("d-none");
	$("#fav-step2").addClass("d-block");
});

$("#fav-step3-nxt").click(function(){
	$("#fav-step3").removeClass("d-block");
	$("#fav-step3").addClass("d-none");
	$("#fav-step4").removeClass("d-none");
	$("#fav-step4").addClass("d-block");
});

$("#fav-step4-pre").click(function(){
	$("#fav-step4").removeClass("d-block");
	$("#fav-step4").addClass("d-none");
	$("#fav-step3").removeClass("d-none");
	$("#fav-step3").addClass("d-block");
});

$("#fav-fr").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
    type: 'post',
    url: '<?php echo $baseurl; ?>',
    data: $("#fav-fr").serialize(),
    success: function () {
          //alert('form was submitted');
          $(".login-area").addClass("d-none");
          closeFavourite();
        }
    });
});

$(".fav-pla").click(function(){
	

	var className = $(".fav-pla");
	var count = 0;
	for(i = 0; i< className.length; i++){
    	var item = className[i];
    	if(item.classList.contains("current")){
			count++;
    	}
	
	}
	
	if(count > 2){
		alert("<?php echo get_string('my_inter:limit_3', 'local_job'); ?>");
	}else{
		var Id = jQuery(this).attr("id");
		$("#"+Id+"-txt").val(Id.substring(10));
		if($("#"+Id).hasClass("current")){
			$("#"+Id).removeClass("current");
		}else{
			$("#"+Id).addClass("current");
		}
	}
	

	if($(".fav-pla").hasClass("current")) {
		$("#fav-step3-nxt").prop('disabled', false);
	}else{
		$("#fav-step3-nxt").prop('disabled', true);
	}
});


$(".fav-lev").click(function(){
	var Id = jQuery(this).attr("id");
	$("#fav-korea-level").val(Id.substring(7));
	if($("#"+Id).hasClass("current")){
		$("#"+Id).removeClass("current");
	}else{
		$(".fav-lev").removeClass("current");
		$("#"+Id).addClass("current");
	}

	if($(".fav-lev").hasClass("current")) {
		$("#fav-step4-nxt").prop('disabled', false);
	}else{
		$("#fav-step4-nxt").prop('disabled', true);
	}
});

$(".fav-experi").click(function(){
	var Id = jQuery(this).attr("id");
	$("#fav-experience").val(Id.substring(10));
	if($("#"+Id).hasClass("current")){
		$("#"+Id).removeClass("current");
	}else{
		$(".fav-experi").removeClass("current");
		$("#"+Id).addClass("current");
	}

	if($(".fav-experi").hasClass("current")) {
		$("#fav-step0-nxt").prop('disabled', false);
	}else{
		$("#fav-step0-nxt").prop('disabled', true);
	}
});

$("#fav-step0-nxt").click(function(){
	$("#fav-step0").removeClass("d-block");
	$("#fav-step0").addClass("d-none");
	$("#fav-step1").removeClass("d-none");
	$("#fav-step1").addClass("d-block");
});

$("#fav-step0-pre").click(function(){
	closeFavourite();
});
</script>