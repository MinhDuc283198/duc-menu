<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG;

foreach ($VISANG->theme->js as $js) {
    echo '<script src="' . $js . '"></script>';
}
foreach ($VISANG->theme->scripts as $script) {
    echo html_writer::tag('script', $script);
}
?>
<!-- oklassedu theme -->
<script src="/theme/oklassedu/javascript/common.js" type="text/javascript"></script>
<?php echo get_visang_config('javascript_footer') ?>
</body>

</html>
