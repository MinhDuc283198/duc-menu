<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $DB, $USER;

require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/jobs.php');



?>
    <div class="bar-tab-grp bt">
        <ul class="bar-tab tab-event">
            <li data-target=".data01"><a href="#"><?php echo get_string('myinfo:Recommendedcourse', 'local_job'); ?></a></li>
            <li class="on" data-target=".data02"><a href="#"><?php echo get_string('RecommendedJobs', 'local_job'); ?></a></li>
        </ul>
        <div class="target-area main-contents">
            <!-- 추천강좌 start -->
            <div class="data01">
                <ul class="thumb-list style01 slider">
                    <?php
                    require_once($CFG->dirroot . '/local/course/lib.php');
                    $recommendcourses = local_course_recommend(isloggedin());
                    ?>
                    <?php foreach ($recommendcourses as $recommendcourse) : ?>
                        <?php
                        $context = context_course::instance($recommendcourse->lcocourseid);
                        $path = local_course_get_imgpath($context->id);
                        ?>

                        <li>
                            <a href="<?php echo $VISANG->elearning_wwwroot . '/local/course/detail.php?id=' . $recommendcourse->id; ?>" target="_blank">
                                <div class="wp">
                                    <div class="tp">
                                        <div class="tit"><?php echo $recommendcourse->title; ?></div>
                                        <p><?php echo $recommendcourse->onelineintro; ?></p>
                                    </div>
                                    <div class="img">
                                        <img src="<?php echo $path; ?>" alt="<?php echo $recommendcourse->title; ?>" />
                                    </div>
                                </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <?php if (count($recommendcourses) < 1) {
                        $likes = $DB->get_records("lmsdata_course_like", array('userid' => $USER->id));
                        foreach ($likes as $like) {
                            $course = $DB->get_record("lmsdata_course", array('courseid' => $like->moodlecourseid));
                            $currentlang = current_language();
                            $context = context_course::instance($course->courseid);
                            $path = local_course_get_imgpath($context->id); ?>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/course/detail.php?id=' . $course->courseid; ?>" target="_blank">
                                    <div class="wp">
                                        <div class="tp">
                                            <div class="tit"><?php echo ($currentlang == "ko") ?  $course->coursename : ($currentlang == "en") ? $course->en_coursename : $course->vi_coursename; ?></div>
                                            <p><?php echo ($currentlang == "ko") ?  $course->onelineintro : ($currentlang == "en") ? $course->en_onelineintro : $course->vi_onelineintro; ?></p>
                                        </div>
                                        <div class="img">
                                            <img src="<?php echo $path; ?>" alt="<?php echo $course->coursename; ?>" />
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div><!-- 추천강좌 end -->
            <!-- 추천채용공고 start -->
            <div class="data02 on">
                <div class="row jobs-cmpny-list slider-evt">
                    <!-- add class "jobs-cmpny-list slider-evt" -->
                    <?php
                    $recommend_jobs = mkjobs_get_recommend_jobs(isloggedin());
                    ?>
                    <?php foreach ($recommend_jobs as $recommend_job) : ?>
                        <?php
                        $saved_jobs_cnt = $DB->count_records('vi_saved_jobs', array('job_id' => $recommend_job->id));
                        ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 px-0">
                            <div class="video-card">
                                <div class="position-relative video-card-img border">
                                    <a class="d-flex justify-content-center" style="height: 200px;" href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id; ?>" target="_blank">
                                        <img class="align-self-center contain-image" src="<?php echo $recommend_job->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo01.png' : $CFG->wwwroot . '/pluginfile.php/' . $recommend_job->company_logo; ?>">
                                    </a>
                                    <div class="d-flex justify-content-between video-time px-3 position-absolute bottom-0 w-100">
                                        <p>
                                            <span class="cursor-pointer ic-sm ic <?php echo mkjobs_has_saved_job($recommend_job->id) ? "ic-heart-on" : "ic-heart";  ?>" onclick="savedJob(this, '<?php echo $recommend_job->id; ?>')"></span>
                                            <span style="padding-left: 6px;" id="numSaved_<?php echo $recommend_job->id; ?>"><?php echo $saved_jobs_cnt; ?></span>
                                        </p>
                                        <p>~<?php echo date('Y\.m\.d', $recommend_job->enddate); ?></p>
                                    </div>
                                </div>
                                <div class="video-thub px-0 bg-white">
                                    <h6>
                                        <a href="<?php echo $VISANG->wwwroot . '/jobs/view.php?jobid=' . $recommend_job->id; ?>" target="_blank">
                                            <?php echo $recommend_job->title; ?>
                                        </a>
                                    </h6>
                                    <p>
                                        <a href="<?php echo $VISANG->wwwroot . '/companies/view.php?employerid=' . $recommend_job->employer_id; ?>" target="_blank">
                                            <?php echo $recommend_job->company_name; ?>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div><!-- 추천채용공고 end -->
        </div>
    </div>
    <script>
        function savedJob(el, id, classon, classoff) {
            $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkjobs_saved_job",
                jobid: id
            }, function(data) {
                if (data == -1) {
                    if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $VISANG->wwwroot; ?>";
                    }
                } else if (data == 1) {
                    $(el).removeClass(classoff || "ic-heart");
                    $(el).addClass(classon || "ic-heart-on");
                    $('#numSaved_' + id).html(parseInt($('#numSaved_' + id).html()) + 1);
                } else {
                    $(el).removeClass(classon || "ic-heart-on");
                    $(el).addClass(classoff || "ic-heart");
                    $('#numSaved_' + id).html(parseInt($('#numSaved_' + id).html()) - 1);
                }
            });
        }

        $("body").addClass("mk-jobs jobs-site");

        <?php if (count($recommend_jobs) < 4) {
            echo "setTimeout(function(){ $('.jobs-cmpny-list .slick-list .slick-track').css('width', '100%');}, 2000);";
        } ?>
    </script>

<?php if (count($recommend_jobs) < 4) : ?>
    <style>
        .jobs-cmpny-list .slick-list .slick-track {
            width: 100% !important;
        }
    </style>
<?php endif; ?>
<?php
