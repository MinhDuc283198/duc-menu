<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $isHomePage, $DB;

require_once($VISANG->dirroot . '/lib/common_functions.php');
$footer_banners = mkjobs_get_banners('bottom');

$term_category = 1;
if(isloggedin()) {
    $sql = "SELECT ad.employer_id as val, em.company_name
            FROM {vi_admin_employers} ad
            JOIN {vi_employers} em ON em.id = ad.employer_id 
            WHERE ad.user_id = :id";
    $comid = $DB->get_record_sql($sql, array('id' => $USER->id));
    if($comid != null) {
        $term_category = 2;
    }
}
?>
    <!-- footer start -->
    <div class="vis-footer">
        <div class="group">
            <div class="tp">
            <span class="ft-logo">
                <img src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/ft_logo_job.png'; ?>" />
            </span>
                <ul class="lnk">
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=<?php echo $term_category; ?>')"><?php echo get_string('c_TermsofUse', 'local_job'); ?></li>
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=<?php echo $term_category; ?>')"><?php echo get_string('c_regulations', 'local_job'); ?></li>
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=<?php echo $term_category; ?>')"><?php echo get_string('c_Privacypolicy', 'local_job'); ?></li>
                    <li><a href="/local/job/my/qna/index.php?type=8'?>"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
                    <!--<li><a href="<?php echo $VISANG->elearning_wwwroot; ?>" target="_blank"><?php echo get_string('KoreanCourse', 'local_job'); ?></a></li>-->
                </ul>
            </div>
            <div class="bt">
                <?php echo get_string('footer', 'theme_oklassedu'); ?>
                <br>Đây là trang web chạy thử nghiệm.
            </div>

            <div class="f-r pt-2">
                <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><img src="/theme/oklassedu/pix/images/icon_facebook_white.png" alt="sns"></a>
                <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank" class="ic-youtube"><img src="/theme/oklassedu/pix/images/icon_youtube_white.png" alt="youtube"></a>
                <a href="<?php echo $VISANG->elearning_wwwroot; ?>" target="_blank" class="ic-learn"><?php echo get_string('KoreanCourse', 'local_job'); ?></a>
                <br/><a id="logoCCDV" href='http://online.gov.vn/Home/WebDetails/64659' target="_blank"><img alt='' title='' src='/theme/oklassedu/pix/images/logobctred.png'></a>
            </div>
        </div>
    </div>
    <!-- footer end -->

    <!-- mobile footer start -->
    <div class="mobi-footer">
        <div class="group">
            <div class="row">
            <span class="ft-logo">
                <img src="/theme/oklassedu/pix/images/ft_logo_job.png" alt="master korean" />
            </span>
            </div>
            <div class="row tp">
                <ul class="lnk">
                    <!--                <li><a href="#"><?php echo get_string('service', 'theme_oklassedu'); ?></a></li>-->
                    <!--                <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1', '<?php echo get_string('terms', 'theme_oklassedu'); ?>', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string('terms', 'theme_oklassedu'); ?></a></li>-->
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=<?php echo $term_category; ?>')" ><?php echo get_string('c_TermsofUse', 'local_job'); ?></li>
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=<?php echo $term_category; ?>')" ><?php echo get_string('c_regulations', 'local_job'); ?></li>
                    <li><a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=<?php echo $term_category; ?>')" ><?php echo get_string('c_Privacypolicy', 'local_job'); ?></li>
                    <li><a href="/local/jinoboard/index.php?type=1"><?php echo get_string('customerservice', 'theme_oklassedu'); ?></a></li>
                    <!--<li><a href="<?php echo $CFG->job_url; ?>/local/job" target="_blank">mk jobs</a></li>-->
                </ul>
            </div>
            <div class="row bt">
                <?php echo get_string('footer', 'theme_oklassedu'); ?>
            </div>
            <div class="row">
                <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" target="_blank" class="ic-facebook"><img src="/theme/oklassedu/pix/images/icon_facebook_white.png" alt="sns"></a>
                <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank" class="ic-youtube"><img src="/theme/oklassedu/pix/images/icon_youtube_white.png" alt="youtube"></a>
                <a href="<?php echo $VISANG->elearning_wwwroot; ?>" target="_blank" class="ic-learn"><?php echo get_string('KoreanCourse', 'local_job'); ?></a>
                <br/><a id="logoCCDV" href='http://online.gov.vn/Home/WebDetails/64659' target="_blank"><img alt='' title='' src='/theme/oklassedu/pix/images/logobctred.png'></a>
            </div>
        </div>
    </div>

    <style>
        @media screen and (min-width: 1025px){
            .mobi-footer {
                display: none;
            }
        }
        .mobi-footer{clear: both;background: #485cc7;padding: 20px 0 30px;}
        .mobi-footer .tp{margin-bottom: 5px;overflow: hidden;}
        .mobi-footer .tp .ft-logo{float: left;margin-right: 50px;}
        .mobi-footer *{color: #fff;}
        .mobi-footer .tp>.f-r>.ic-facebook{display: inline-block;font-size: 0;background: url("/theme/oklassedu/pix/images/icon_facebook.png") no-repeat center;width: 27px;height: 27px;margin-right: 7px;vertical-align: middle;}
        .mobi-footer .tp>.f-r>.ic-youtube{display: inline-block;font-size: 0;background: url("/theme/oklassedu/pix/images/icon_youtube.png") no-repeat center;width: 27px;height: 27px;vertical-align: middle;}

        .mobi-footer .group{position: relative; padding-left: 20px;}
        .mobi-footer .group .f-r{position: absolute;right: 20px;top: 0;font-size: 0;}
        .mobi-footer .group .f-r a.ic-facebook{margin-right: 5px;}
        .mobi-footer a.ic-jobs{font-weight: bold;font-size: 0;background: url(/theme/oklassedu/pix/images/logo_btn_footer.png) no-repeat center;background-size: auto 15px;width: 110px;height: 28px;display: inline-block;border-radius: 5px;padding: 0px 13px;vertical-align: middle;margin-left: 10px;border: 1px solid #c8ceee;box-sizing: border-box;}
        .mobi-footer a.ic-learn{font-weight: bold;font-size: 0;background: url(/theme/oklassedu/pix/images/logo_btn_footer02.png) no-repeat center;background-size: auto 13px;width: 85px;height: 28px;display: inline-block;border-radius: 5px;padding: 0px 13px;vertical-align: middle;margin-left: 10px;border: 1px solid #c8ceee;box-sizing: border-box;}

        .mobi-footer .tp>.f-r>select{float: right;font-size: 14px;height: 28px;border: 0;width: 165px;box-sizing: border-box;padding: 4px;color: #666;line-height: 20px;background: #dadef4;color: #485cc7;margin-left: 15px;}

        .mobi-footer .tp .lnk{font-size: 0;float: left;width: 100%; over-flow}
        .mobi-footer .tp .lnk>li{font-size: 16px;margin: 0 20px 3px 0;display: inline-block;line-height: 35px;}
        .mobi-footer .tp .lnk>li:last-child{margin-right: 0;}
        .mobi-footer .bt{margin: 20px 0;}
        .mobi-footer .bt p{font-size: 12px;line-height: 1.5;font-weight: 300;margin-bottom: 0;}
        .mobi-footer .bt p.copy{font-size: 11px;margin-top: 13px;font-weight: 300;}

        #logoCCDV {width: 100% !important;}
        #logoCCDV img{width: 200px !important; margin-top: 20px;/*
    border-bottom-left-radius: 40px;
    border-top-left-radius: 40px;
    border-bottom-right-radius: 30px;
    border-top-right-radius: 30px; */}
    </style>
    <!-- mobile footer end -->
    <!-- mobile menu -->
    <ul class="m-ft-link">
        <li><a href="<?php echo $VISANG->elearning_wwwroot; ?>" target="_blank">비상 <?php echo get_string('korean', 'theme_oklassedu'); ?></a></li>
        <li><a href="<?php echo $VISANG->wwwroot; ?>">비상 <?php echo get_string('job', 'theme_oklassedu'); ?></a></li>
    </ul>

<?php if (isset($isHomePage) && $isHomePage) : ?>
    <?php foreach ($footer_banners as $banner) : ?>
        <div class="ft-banner z-10" <?php echo 'style="background: #' . $banner->colorcode . '"'; ?>>
            <a class="close-bn"><?php echo get_string('close', 'local_job'); ?></a>
            <a href="<?php echo $banner->linkurl; ?>" target="_blank">
                <img style="max-width: calc(100% - 4px);" class="d-md-none" src="<?php echo $banner->path_modile; ?>">
                <img style="max-width: calc(100% - 4px);" class="d-none d-md-inline-block" src="<?php echo $banner->path; ?>">
            </a>
            <p class="ck-area"><label class="custom-ck" data-id="<?php echo $banner->id ?>"><input type="checkbox"><span></span><?php echo get_string('offOneDay', 'local_job'); ?></label></p>
        </div>
        <script defer>
            $(document).ready(function() {
                $(".ft-banner .custom-ck").click(function() {
                    var id = $('.custom-ck').data('id');
                    setCookieBanner("bannerCookie", "done", 1);
                    $(".ft-banner").hide();
                });
            });

            function setCookieBanner(name, value, expiredays) {
                var todayDate = new Date();
                todayDate.setDate(todayDate.getDate() + expiredays);
                document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
            }

            function getCookieBanner() {
                var cookiedata = document.cookie;
                if (cookiedata.indexOf("bannerCookie=done") < 0) {
                    $(".ft-banner").show();
                } else {
                    $(".ft-banner").hide();
                }
            }
            getCookieBanner();
        </script>
    <?php endforeach; ?>
<?php endif; ?>

    <script>
        function mkjobs_logout() {
            utils.popup.call_layerpop("<?php echo $VISANG->wwwroot . '/component/pop_logout_confirm.php' ?>", {
                "width": "500px",
                "height": "auto",
                "callbackFn": function() {}
            }, {
                //
            });
        }
    </script>

<?php
//Csedung: Add first login form to get agreement
include_once $VISANG->dirroot . '/component/firstLogin.php';
include_once $VISANG->dirroot . '/component/joinMember.php';
include_once $VISANG->dirroot . '/component/favourite.php';
?>

<?php if (isloggedin()) : ?>
    <?php
    $user_korean_level = $DB->get_record('vi_user_korean_levels', array('user_id' => $USER->id));
    $kid = $user_korean_level->korean_level_id;
    $employer = $DB->get_record('vi_admin_employers', array('user_id' => $USER->id));
    if ((int) $kid == 0 && $employer == null) :
        ?>
        <script type="text/javascript">
            openFavourite();
        </script>
    <?php endif; ?>

<?php endif;
