<div class="vis-first-login">
    <a href="#" class="close-dialog"> Close</a>
    <strong class="title-dialog"><?php echo get_string('SharingAgreement', 'local_job'); ?></strong>
    <img alt="VISANG" src="<?php echo $CFG->wwwroot . '/theme/oklassedu/pix/images/hd_logo_job.png'; ?>">
    <div class="scrl-box">
    	<p>정보공유 동의 내용 정보공유 동의 내용</p>
    	<p>정보공유 동의 내용 정보공유 동의 내용</p>
    	<p>정보공유 동의 내용 정보공유 동의 내용</p>
    	<p>정보공유 동의 내용 정보공유 동의 내용</p><br>
    	<h4>공유하는 정보: 계정, 이름, 휴대폰 번호, 이메일 주소, 한국어 활용 수준  </h4>
    </div>
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input rounded-0" id="chkAgree">
        <label class="custom-control-label" for="chkAgree"><?php echo get_string('Agree', 'local_job'); ?></label>
   </div>
    <form id="fl-fr" action="" method="get">
        <div class="btns-area text-center">
            <input type="submit" id="btn-firstLogin" class="btns point big disabled" value="Agree" disabled>
        </div>         
    </form>
</div>

<style>

.vis-first-login {display: none;z-index: 0;position: absolute;width: 60%;left: 20%;top: -300px;background: #fff;border-top: 1px solid #ddd;padding: 100px 40px 20px;box-sizing: border-box;min-height: 300px;transition: 0.4s;}
.vis-first-login.on {top: 160px; display: block;z-index: 99998;}
.vis-first-login>.title-dialog{font-size: 2em;position: absolute;left: 20px;top: 30px;display: inline-block;}
.vis-first-login>.close-dialog{font-size: 0;position: absolute;right: 20px;top: 20px;display: inline-block;background: url("/theme/oklassedu/pix/images/icon_close.png") no-repeat center;width: 40px;height: 40px;}
.vis-first-login>img{display: block;margin: 0 auto; margin-bottom: 60px;}
.vis-first-login>.scrl-box>p{text-align: center; font-size: 12pt;}
.vis-first-login>.scrl-box>h4{text-align: center; font-size: 16pt; font-weight: bold;}
.vis-first-login>.custom-checkbox{position: absolute;right: 60px;}
.vis-first-login>#fl-fr>.btns-area>.disabled{background: #888a85 !important;}
@media screen and (max-width:991px){.vis-first-login{width: 90%; left: 5%;}}
@media screen and (max-width:767px){.vis-first-login{width: 100%; left: 0;}}

.main-contents>.white-area>.group>.overflow-auto{}
.main-contents>.white-area>.group>.overflow-auto>.cate-list{overflow-x: scroll;scrollbar-width: none;-ms-overflow-style: none;}
.main-contents>.white-area>.group>.overflow-auto>.cate-list::-webkit-scrollbar{width: 0;height: 0;}
</style>

<script>

var openFirstLogin = function () {
	$("html,body").animate({"scrollTop": 0});
    var target = $(".vis-first-login");
    if (!target.hasClass("on")) {
        target.addClass("on");
        if ($(".my-wrap").length == 0) {
            $("body").append("<div class='my-wrap'></div>");
        }
        $(".my-wrap").fadeIn();
        if (utils.popup.bodyscroll()) {
            $("body").addClass("noScroll");
        }
        $(document).on("mouseup touchend", function (e) {
            if ($(e.target).closest(".search-evt").length == 0 && !$(e.target).hasClass(".vis-first-login") && $(e.target).closest(".vis-first-login").length == 0.) {
                target.removeClass("on");
                $(".my-wrap").fadeOut();
                $(this).off("mouseup");
                $(this).off("touchstart");
                $("body").removeClass("noScroll");
            }
        });

    } else {
        target.removeClass("on");
        $(".my-wrap").fadeOut();
        $("body").removeClass("noScroll");
    }
}

var closeFirstLogin = function () {
    $(".vis-first-login.on").removeClass("on");
    $(".my-wrap").fadeOut();
    $("body").removeClass("noScroll");
}

$(".test-class").off("click").click(openFirstLogin);

$(".vis-first-login>.close-dialog").click(closeFirstLogin);
$('#chkAgree').click(function(){
	if($("#chkAgree").is(":checked")){
		$("#btn-firstLogin").removeClass("disabled");
		$("#btn-firstLogin").prop('disabled', false);
	}else{
		$("#btn-firstLogin").addClass("disabled");
		$("#btn-firstLogin").prop('disabled', true);
	}
});
$("#fl-fr").on("submit", function(){
	closeFirstLogin();
    return false;
})
</script>