<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $VISANG, $USER;

?>
<!DOCTYPE html>
<html lang="<?php echo current_language(); ?>">

<head>
    <?php echo get_visang_config('javascript_header') ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo get_string('sitename:mk_jobs', 'local_management'); ?></title>

    <!-- job theme -->
    <link href="/local/job/assets/dist/css/app.css" rel="stylesheet" />
    <script src="/local/job/assets/dist/js/app.js" type="text/javascript"></script>

    <!-- oklassedu theme -->
    <link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
    <link href="/theme/oklassedu/style/media.css" rel="stylesheet" />

    <?php
    foreach ($VISANG->theme->css as $css) {
        echo '<link rel="stylesheet" href="' . $css . '">';
    }
    foreach ($VISANG->theme->styles as $style) {
        echo html_writer::tag('style', $style);
    }
    ?>
</head>

<body class="mk-jobs jobs-site <?php echo 'lang-'.current_language(); ?> <?php echo $VISANG->theme->bodyclasses; ?>">
<?php echo get_visang_config('javascript_body') ?>
