<div class="vis-join-member">
	<div id="step1" class="session d-block">
		<div class="row justify-content-center oval-number">
    		<h2 class="current">1</h2><h6></h6><h2>2</h2><h6></h6><h2>3</h2>
        </div>
        <h3 class="title-dialog">정보공유 동의</h3>
        <div class="content">
        	<strong class="mb-3">이용약관 동의(필수)</strong>
            <div class="scrl-box mt-3">
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            </div>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input rounded-0" id="chkAgree1">
                <label class="custom-control-label" for="chkAgree1">동의</label>
           </div>
           <br><strong class="mb-5">개인정보 수집 및 이용동의(필수)</strong>
           <div class="scrl-box mt-3">
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            	<p>정보공유 동의 내용 정보공유 동의 내용</p>
            </div>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input rounded-0" id="chkAgree2">
                <label class="custom-control-label" for="chkAgree2">동의</label>
           </div>
           <br><p class="mb-3">※ 위 항목에 동의하지 않는 경우 회원가입이 불가합니다.</p>
            <div class="btns-area text-center">
                <input type="button" id="btn-step1" class="btns point big disabled" value="Agree" disabled>
            </div>         
        </div> 
	</div>
	<div id="step2" class="session d-none">
		<div class="row justify-content-center oval-number">
    		<h2>1</h2><h6></h6><h2 class="current">2</h2><h6></h6><h2>3</h2>
        </div>
        <h3 class="title-dialog">정보공유 동의</h3>
        <div class="content pt-5">
        	<form class="email-bx" action="" method="post">
        		<p>
                    <input class="w100" type="text" id="jmemEmail" name="email" placeholder="Email">
                    <span class="warning">아이디 입력조건 안내 문구</span>
                </p>
                <p>
                    <input type="password" id="jmemPass" name="joinpassword" title="비밀번호" placeholder="Password">
                    <span class="warning">아이디 입력조건 안내 문구</span>
                </p>
                <div class="btns-area text-center">
                	<input type="button" id="btn-step2" class="btns point big" value="Next">
            	</div>
            </form>
        </div>
	</div>
	
	
	<div id="step3" class="session d-none">
		<div class="row justify-content-center oval-number">
    		<h2>1</h2><h6></h6><h2>2</h2><h6></h6><h2 class="current">3</h2>
        </div>
        <h3 class="title-dialog">정보공유 동의</h3>
        <div class="login-bx lg-bx">
            <p class="ft-dot">필수정보입니다_EN</p>
            <form class="email-bx join" action="" method="POST">
                
                <p>
                    <strong>이름_EN</strong>
                    <input type="text" name="name1" placeholder="이름 입력_EN">
                </p>
                
                <p class="two-col">
                    <strong>이름_EN</strong>
                    <select class="w100" name="name2">
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                    </select>
                </p>
                 <p class="two-col bre">
                    <strong>이름_EN</strong>
                    <select class="w100"  name="name3">
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                    </select>
                </p>
				<p style="clear: left;">
                    <strong>이름_EN</strong>
                    <input type="text" name="name4" placeholder="이름 입력_EN">
                </p>
                <p >
                	<strong>이름_EN</strong>
                    <input class="email-check" type="text"  name="name5" placeholder="이메일 입력_EN">
                    <!-- <input type="hidden" id="emailconfirm" name="emailconfirm"> -->
                    <input type="button" class="btns gray" onclick="check_val('email')" value="Check">
                    <span class="emailwarning warning"></span>
                </p>
                <p>
                    <strong>이름_EN</strong>
                    <input type="text" name="name6" placeholder="이름 입력_EN">
                </p>
                <p>
                    <strong>이름_EN</strong>
                    <input type="text" name="name7" placeholder="이름 입력_EN">
                </p>
                 <p class="two-col">
                    <strong>이름_EN</strong>
                    <input type="text" name="name8" placeholder="이름 입력_EN">
                </p>
                 <p class="two-col bre">
                    <strong>이름_EN</strong>
                    <input type="text" name="name9" placeholder="이름 입력_EN">
                </p>            
                <p class="phone">
                    <strong>휴대폰 번호_EN</strong>
                    <select   name="name13">
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                    <span class="dash">-</span>
                    <input type="text"  name="name10">
                    <span class="dash">-</span>
                    <input type="text" name="name11"> 
                    <span class="warning">휴대폰 번호 부적합 사유 안내</span>
                </p>
        		<p>
                    <strong>email</strong>
                    <input type="text" name="name12" placeholder="이메일 입력_EN" value="admin@gmail.com" readonly="">
                    <span class="warning">이메일 부적합 사유 안내</span>
                </p>
                <div class="text-center btn-area">
                    <input type="submit" value="회원가입_EN" class="btns point big ">
                </div>
            </form>        	
        </div>
	</div>
       
</div>

<style>

.vis-join-member {display: none;z-index: 0;position: absolute;width: 40%;left: 30%;top: -300px;background: none; padding: 0 0;box-sizing: border-box;min-height: 300px;transition: 0.4s;}
.vis-join-member.on {top: 160px; display: block;z-index: 99998;}
.vis-join-member>.session>.title-dialog{font-size: 2em;text-align: center;color:#fff;font-weight: bold; }
.vis-join-member>.session>.content{width: 100%; margin-top: 10px;background: #fff; padding: 20px 40px 20px;}

.vis-join-member>.session>.content>.scrl-box>p{text-align: center; font-size: 12pt;}
.vis-join-member>.session>.content>.scrl-box>h4{text-align: center; font-size: 16pt; font-weight: bold;}
.vis-join-member>.session>.content>.custom-checkbox{position: absolute;right: 60px;}
.vis-join-member>.session>.content>.btns-area>.disabled{background: #888a85 !important;}

.vis-join-member>.session>.oval-number{padding-bottom: 30px;}
.vis-join-member>.session>.oval-number>h2{border-radius: 50%;border: 1px solid #3399ff;padding: 0 8px;margin: 0 0;font-size: 20px;color: #3399ff; background: #fff;}
.vis-join-member>.session>.oval-number>h6{border-bottom: 3px solid #3399ff;width: 20px;height: 14px;font-size: 10px;}
.current{background: #3399ff !important; color: #fff !important;}
.vis-join-member>.session>.lg-bx{top: 100px; important;}
.email-bx>p>.email-check{width: calc(100% - 105px) !important;float: left !important;}
.email-bx>.two-col{width: 49% !important;float: left !important;}
.email-bx>.bre{margin-left: 2%;}
.email-bx>.phone{clear: left;}
.email-bx>p>.w100{width: 100% !important;}
@media screen and (max-width:991px){.vis-join-member{width: 90%; left: 5%;}}
@media screen and (max-width:767px){.vis-join-member{width: 100%; left: 0;}}
</style>

<script>

var openJoinMember = function () {
	$("html,body").animate({"scrollTop": 0});
    var target = $(".vis-join-member");
    if (!target.hasClass("on")) {
        target.addClass("on");
        if ($(".my-wrap").length == 0) {
            $("body").append("<div class='my-wrap'></div>");
        }
        $(".my-wrap").fadeIn();
        if (utils.popup.bodyscroll()) {
            $("body").addClass("noScroll");
        }
        $(document).on("mouseup touchend", function (e) {
            if ($(e.target).closest(".search-evt").length == 0 && !$(e.target).hasClass(".vis-join-member") && $(e.target).closest(".vis-join-member").length == 0.) {
                target.removeClass("on");
                $(".my-wrap").fadeOut();
                $(this).off("mouseup");
                $(this).off("touchstart");
                $("body").removeClass("noScroll");
            }
        });

    } else {
        target.removeClass("on");
        $(".my-wrap").fadeOut();
        $("body").removeClass("noScroll");
    }
}

var closeJoinMember = function () {
    $(".vis-join-member.on").removeClass("on");
    $(".my-wrap").fadeOut();
    $("body").removeClass("noScroll");
}

var checkJoin = function(){
	if($("#chkAgree1").is(":checked") && $("#chkAgree2").is(":checked")){
		$("#btn-step1").removeClass("disabled");
		$("#btn-step1").prop('disabled', false);
	}else{
		$("#btn-step1").addClass("disabled");
		$("#btn-step1").prop('disabled', true);
	}
}

$(".test-class2").off("click").click(openJoinMember);

$(".vis-join-member>.close-dialog").click(closeFirstLogin);
$('#chkAgree1').click(checkJoin);
$('#chkAgree2').click(checkJoin);
$('#btn-step1').click(function(){
	$('#step1').removeClass("d-block");
	$('#step1').addClass("d-none");
	$('#step2').removeClass("d-none");
	$('#step2').addClass("d-block");
});

$('#btn-step2').click(function(){
	$('#step2').removeClass("d-block");
	$('#step2').addClass("d-none");
	$('#step3').removeClass("d-none");
	$('#step3').addClass("d-block");
});

$("#jm-fr").on("submit", function(){
	closeFirstLogin();
    return false;
})
</script>