<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/lib.php');

global $CFG, $VISANG, $DB, $USER;

// ===================================================================================================
// handles

// ===================================================================================================
// renders
$VISANG->theme->title = 'Login';
$VISANG->theme->header();
$VISANG->theme->footer();
