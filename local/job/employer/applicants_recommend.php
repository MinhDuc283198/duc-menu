<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');

global $CFG, $VISANG, $DB, $USER;

$baseurl = new moodle_url($VISANG->wwwroot . '/employer/applicants_recommend.php');
$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('job');

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$filter = optional_param('filter', "All", PARAM_RAW);
$timer = optional_param('timer', 'I', PARAM_RAW);
$mkstudent = optional_param('mkstudent', 0, PARAM_INT);
$order = optional_param('order', 10, PARAM_INT);
$keyword = optional_param('resume_keyword', "", PARAM_RAW);

$dowloadCSV = optional_param('downloadCSV', 0, PARAM_INT);
if ($dowloadCSV != 0) mkcom_recommend_applicant2CSV($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent);

$total_pages = mkcom_count_recommend_applicants_new($comid);
$applicants = mkcom_get_recommend_applicants_new($comid, $perpage, $page);
$nearests = mkcom_get_nearest_job($comid);
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'menuApply';
$VISANG->theme->header_foremployer();
$pageAction = "aplicant_recommend";
$current_language = current_language();
$point_price = $DB->get_field('vi_common_code', 'point', array('code' => 2003));
?>
<div class="cont">
    <div class="group">
		
		<?php require_once(dirname(__FILE__) . '/../employer/employer_menu.php');?>
        
        <div class="crs-right-block">
            <h2 class="pg-tit"><?php echo get_string('employer:RecommendedTalent', 'local_job'); ?></h2>
			<form method="POST" action="<?php echo $baseurl?>" id="filterForm">
            <div class="list-num clearfix">
                    <span class="t-blue02"><?php echo $total_pages. get_string('rowsrow', 'local_job');?></span>  
                    <select class="under" onchange="this.form.submit()" name="perpage">
                        <option value="5" <?php echo ($perpage == 5) ? "selected" : ""; ?>><?php echo get_string('display5', 'local_job'); ?></option>
                        <option value="10" <?php echo ($perpage == 10) ? "selected" : ""; ?>><?php echo get_string('display10', 'local_job'); ?></option>
                        <option value="20" <?php echo ($perpage == 20) ? "selected" : ""; ?>><?php echo get_string('display20', 'local_job'); ?></option>
                        <option value="30" <?php echo ($perpage == 30) ? "selected" : ""; ?>><?php echo get_string('display30', 'local_job'); ?></option>
                    </select>

            </div>
            	<input type="hidden" name="order" id="order" value="">
            	<input type="hidden" name="downloadCSV" id="downloadCSV" value="0">
             </form>
            <?php if (count($applicants) < 1) { ?>
                <div class="no-data">
                    <p><?php echo get_string('noapplicants', 'local_job'); ?></p>
                </div>
            <?php } else { ?>               
                <ul class="talent-list has-check">
                    <?php foreach ($applicants as $apply) { ?>
                        <li>
                            
                            <?php
                            $uinfo = $DB->get_record("user", array('id' => $apply->uid));
                            $resume = $DB->get_record('vi_resumes', array('id' => $apply->candidate_cv));
                            $resume_url = '';
                            $cvvideo = $DB->get_record('vi_resumes', array('type' => 'video', 'user_id' => $apply->uid, 'published'=>'1'));
                            switch ($resume->type) {
                                case 'resume':
                                    $resume_url = $CFG->wwwroot . '/local/job/resumes/view.php?resumeid=' . $resume->id;
                                    break;
                                case 'cvfile':
                                    $resume_url = $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url;
                                    break;
                                case 'video':
                                    $resume_url = $resume->video_url;
                                    break;
                            }
                            ?>
                            <input type="submit" id="stt_big_cvid" hidden/>
                            <label class="custom-ck">
                            	<input type="hidden" id="cvHuman-<?php echo $apply->uid?>" value="<?php echo $uinfo->firstname . " " . $uinfo->lastname; ?>"/>
                            	<input type="hidden" id="cvPDF-<?php echo $apply->uid?>" value="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url;?>"/>
                            	<input type="checkbox" class="cv_selection" name="cv_selection[]" value="<?php echo $resume->id;?>" id="checkBox-<?php echo $apply->uid?>"/>
                            <span></span></label>
                            <div class="t-img">
                                <span class="saved_resume_icon <?php echo mkcom_resume_has_saved($comid,$resume->id) ? "ic-heart on" : "ic-heart";  ?>" id="savedHeart-<?php echo $resume->id.'-'.$apply->id;?>" onclick="savedResume('savedHeart-<?php echo $resume->id.'-'.$apply->id?>', <?php echo $resume->id; ?>, <?php echo $comid?>)" ><?php echo get_string('talensearch:text1', 'local_job'); ?></span>
                                <?php if($cvvideo->video_url != null){?>
                                <span class="ic-play" onclick="viewVideo('<?php echo $cvvideo->video_url;?>')">play</span>
                                <?php }?>
                                <img src="<?php echo ($resume->avatar != "") ? $CFG->wwwroot . '/pluginfile.php/'.$resume->avatar : $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg'; ?>" alt="김비상" />
                            </div>
                            <div class="t-u-info">
                                <strong><?php echo $uinfo->firstname . " " . $uinfo->lastname; ?></strong>
                                <span class="t-blue02"><?php echo substr($uinfo->phone2,0,4);?></span>
                                <?php $userbadge = mkcom_count_user_badge($apply->uid); if($userbadge > 0) {?>
                                <span class="ic-badge" onclick="mkClassCompleted(<?php echo $apply->uid;?>);">+<?php echo$userbadge;?></span>
                                <?php }?>
                            </div>
                            <div class="t-c-info">
                                <div class="tit"><a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $apply->jobid, 'returnurl'=>$baseurl)); ?>">[<?php echo get_string('RecommendedJobs2', 'local_job'); ?>] <?php echo $apply->title; ?></a></div>
                                <p><span><?php $rows = mkcom_get_user_work_experience($apply->uid);
                                            $html = "";
                                            foreach ($rows as $p) $html = $html . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
                                            echo rtrim($html, ", "); ?></span>
                                    <span><?php $rows = mkcom_get_user_workplace($apply->uid);
                                            $html = "";
                                            foreach ($rows as $p) $html = $html . $p->name . ", ";
                                            echo rtrim($html, ", "); ?></span>
                                    <span><?php $rows = mkcom_get_user_korean_level($apply->uid);
                                            $html = "";
                                            foreach ($rows as $p) $html = $html . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
                                            echo rtrim($html, ", "); ?></span></p>
                                <p><?php $rows = mkcom_get_user_skill_group($apply->uid);
                                    $html = "";
                                    foreach ($rows as $p) $html = $html . $p->name . ", ";
                                    echo rtrim($html, ", "); ?></p>
                                <p><?php $rows = mkcom_get_user_skill($apply->uid);
                                    $html = "";
                                    foreach ($rows as $p) $html = $html . $p->name . ", ";
                                    echo rtrim($html, ", "); ?></p>
                            </div>
                            <div class="t-right no-pd">
                                <span><?php echo date("Y-m-d", $apply->timecreated); ?></span>

                                <a href="#" class="btns" onclick="visang_view_cv('<?php echo $resume_url; ?>', '<?php echo $resume->type; ?>', '<?php echo $resume->id; ?>')"><?php echo get_string('ViewResume_2', 'local_job'); ?></a>
<!--                                 This is sub form -->
                                    <input type="submit" id="stt_cvid" hidden>
                                    <input type="hidden" name="stt_cvid" value="<?php echo $apply->id;?>" />
                                    
<!--                                End subform -->
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                
                <div class="bt-ck-list">
                    <input type="button" value="<?php echo get_string('All', 'local_job'); ?>" class="btns" id="cv_selectAllCV"/>
                    

                    <div class="f-r">
                        <a href="#" id="downloadMultiCV" class="btns point ic-down"><?php echo get_string('pdfDownload', 'local_job'); ?></a>
                        <a href="#" onclick="applicants2CSV()" class="btns br ic-down"><?php echo get_string('ExcelDownload', 'local_job'); ?></a>
                    </div>
                </div>
            <?php } ?>
            <?php 
            if (count($applicants)) {
                $page_params = array();
                $page_params['comid'] = $comid;
                $page_params['perpage'] = $perpage;
                $page_params['filter'] = $filter;
                $page_params['resume_keyword'] = $keyword;
                $page_params['page'] = $page;
                $page_params['order'] = $order;
                $VISANG->theme->table_pagination($baseurl, $page_params, ceil($total_pages / $perpage), $page);
            }
            ?>
        </div>
    </div>
</div>




<script type="text/javascript">
function applicants2CSV(){
	$("#downloadCSV").val("1");
	$("#filterForm").submit();
	$("#downloadCSV").val("0");
}
    function mkClassCompleted(uid) {
        utils.popup.call_layerpop("./pop_course_completion_list.php", {
            "width": "600px",
            "height": "auto",
            "callbackFn": function() {}
        }, {
            "elid": uid
        });
        return false;
    };

    function viewVideo(url){
    	var windowpopup = utils.popup.call_windowpop(url, 560, 360, "video-pop"); //url, width, height, popupname;
        return false;
    }

// Download Uploaded CV
function visang_view_cv(url, type, resumeid) {
    if (type == 'cvfile') {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkcom_resume_download_status",
            resumeid: resumeid,
            appid: <?php echo $comid; ?>
        }, function (data) {
            if (data == 1) {
                window.open(url);
            } else {
                if (confirm("<?php echo get_string('down-cv-file', 'local_job', $point_price); ?>")) {
                    $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                        sesskey: "<?php echo sesskey(); ?>",
                        action: "mkcom_resume_download",
                        resumeid: resumeid,
                        appid: <?php echo $comid; ?>
                    }, function (data) {
                        if (data == 1) {
                            if (confirm("<?php echo get_string('buy-credit-require', 'local_job'); ?>")) {
                                window.location.href = "<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>";
                            }
                        }else{
                            window.open(url);
                        }
                    });
                }
            }
        });
    } else {
        window.open(url, '_blank');
    }
}

    function savedResume(el, id,aid) {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
             sesskey: "<?php echo sesskey(); ?>",
             action: "mkcom_resume_saved",
             resumeid: id,
             appid: aid
         }, function(data) {
             if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                 }
             } else if (data == 1) {
            	 $("#"+el).addClass("on"); 
             } else if (data == 2){
            	 $("#"+el).removeClass("on");
             }             
         });
     }

    function changeApplicationStatus(el,appid, cvid, multi=false) {
        if(multi){
        	$.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkcom_update_resume_status",
                stt_cvid: cvid,
                stt_appid: appid,
                stt_value: el
            }, function(data) {
                //alert(data);
            });
        }
        else{
        	$.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                sesskey: "<?php echo sesskey(); ?>",
                action: "mkcom_update_resume_status",
                stt_cvid: cvid,
                stt_appid: appid,
                stt_value: el.options[el.selectedIndex].value
            }, function(data) {
                //alert(data);
            });
        }
        

     }

    function filterSubmit(check) {
        $(".filter").prop('checked', false);
        check.checked = true;
        check.form.submit();
    }

    function orderChanged(val){
        $("#order").val(val);
 		$("#filterForm").submit();
    }

    $("#cv_selectAllCV").click(function(){
        if($(".cv_selection").is(':checked')){
        	$(".cv_selection").prop('checked', false);
        	$("#cv_selectAllCV").removeClass("current");
        }else{
        	$(".cv_selection").prop('checked', true);
        	$("#cv_selectAllCV").addClass("current");
        }
    }); 

    function ChangeStatusAsChecking(){
        var applicants = $(".cv_selection");
        var stt = $("#sel01").val();
        for(var i=0; i<applicants.length; i++){
            var apply = applicants[i];
            if(apply.checked){
            	var id = jQuery(apply).attr("id");
            	var cvid = $("#"+id).val();
            	var appid = id.substring(9);
               	$("#selectStatus-"+appid).val(stt);

               	changeApplicationStatus(stt,appid, cvid, true);
            }
        }
    }

// Download multi file BEGIN
$("#downloadMultiCV").click(function(){
    var applicants = $(".cv_selection");
    for(var i=0; i<applicants.length; i++){
        var apply = applicants[i];
        if(apply.checked){
            var resumeid = jQuery(apply).attr("value");
            downloadMultiCV(resumeid, apply);
        }
    }
});

function downloadMultiCV(resumeid, apply) {
    $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
        sesskey: "<?php echo sesskey(); ?>",
        action: "mkcom_resume_download_status",
        resumeid: resumeid,
        appid: <?php echo $comid; ?>
    }, function (data) {
        if (data == 1) {
            var id = jQuery(apply).attr("id");
            var link = id.replace("checkBox","cvPDF");
            var name = id.replace("checkBox","cvHuman");
            var cvURL = $("#"+link).val();
            name = $("#"+name).val();
            downloadCV(cvURL, name);
        } else {
            if (confirm("<?php echo get_string('down-cv-file', 'local_job', $point_price); ?>")) {
                $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                    sesskey: "<?php echo sesskey(); ?>",
                    action: "mkcom_resume_download",
                    resumeid: resumeid,
                    appid: <?php echo $comid; ?>
                }, function (data) {
                    if (data == 1) {
                        if (confirm("<?php echo get_string('buy-credit-require', 'local_job'); ?>")) {
                            window.location.href = "<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>";
                        }
                    }else{
                        var id = jQuery(apply).attr("id");
                        var link = id.replace("checkBox","cvPDF");
                        var name = id.replace("checkBox","cvHuman");
                        var cvURL = $("#"+link).val();
                        name = $("#"+name).val();
                        downloadCV(cvURL, name);
                    }
                });
            }
        }
    });
}
// Download multi file END

    function downloadCV(uri, name) {
        if(uri != "" && uri != " "){
        	var link = document.createElement("a");
            link.download = name;
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        }
    } 
</script>


<?php
$VISANG->theme->footer_foremployer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
