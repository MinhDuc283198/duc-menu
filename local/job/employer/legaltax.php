<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

global $CFG, $VISANG, $USER, $DB;

$context = context_system::instance();

$PAGE->set_context($context);
// =====================================================================================================
// handles

$user = $USER;


$type = optional_param('type', 0, PARAM_INT);
mkjobs_require_login();
$sql = "SELECT ad.employer_id as val
        FROM {vi_admin_employers} ad
        WHERE ad.user_id = :id";
$comid = $DB->get_record_sql($sql, array('id' => $USER->id));
if ($comid == null) {
  redirect($VISANG->wwwroot);
}
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$market = optional_param('market', 3, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);

if ($type != 0) {
  $board = $DB->get_record('jinoboard', array('type' => $type));
  switch (current_language()) {
    case 'ko':
      $boardname = $board->name;
      break;
    case 'en':
      $boardname = $board->engname;
      break;
    case 'vi':
      $boardname = $board->vnname;
      break;
  }
} else {
  $boardname = get_string('employer:legalTax_all', 'local_job');
}

$today = time();
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

// =====================================================================================================
// renders
$VISANG->theme->title = $boardname;
if ($type == 14) {
  $VISANG->theme->submenu = 'legal';
} else if ($type == 15) {
  $VISANG->theme->submenu = 'tax';
}
$VISANG->theme->menu = 'menuLegalTax';
$VISANG->theme->header_foremployer();

$siteadmin = is_siteadmin();
$boardsql = 'board=:board';
if ($type == 0) {
  $boardsql = '(board=14 or board=15)';
}
$sql = "select count(id) from {jinoboard_contents} where $boardsql and isnotice = 0 order by ref DESC, step ASC";
$totalcount = $DB->count_records_sql($sql, array('board' => $board->type));
$total_pages = jinoboard_get_total_pages($totalcount, $perpage);
?>

<div class="cont">
  <div class="group">
    <div class="crs-left-block">
      <ul>
        <li class="<?php echo $VISANG->theme->submenu == 'legal' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/legaltax.php?type=14'; ?>"><?php echo get_string('employer:legalTax_legal', 'local_job'); ?></a></li>
        <li class="<?php echo $VISANG->theme->submenu == 'tax' ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/legaltax.php?type=15'; ?>"><?php echo get_string('employer:legalTax_tax', 'local_job'); ?></a></li>
      </ul>
    </div>
    <div class="crs-right-block">
      <ul class="mk-c-tab col3 tab-event">
        <li <?php echo ($type == 0) ? 'class="on"' : ''; ?>>
          <a href="<?php echo $VISANG->wwwroot . "/employer/legaltax.php"; ?>">
            <?php echo get_string('employer:legalTax_all', 'local_job'); ?>
          </a>
        </li>
        <li <?php echo ($type == 14) ? 'class="on"' : ''; ?>>
          <a href="<?php echo $VISANG->wwwroot . "/employer/legaltax.php?type=14"; ?>">
            <?php echo get_string('employer:legalTax_legal', 'local_job'); ?>
          </a>
        </li>
        <li <?php echo ($type == 15) ? 'class="on"' : ''; ?>>
          <a href="<?php echo $VISANG->wwwroot . "/employer/legaltax.php?type=15"; ?>">
            <?php echo get_string('employer:legalTax_tax', 'local_job'); ?>
          </a>
        </li>
      </ul>
      <?php
      //<- 타이틀 끝 ->
      //<-리스트 시작->
      $offset = 0;
      if ($page != 0) {
        $offset = ($page - 1) * $perpage;
      }
      $list_num = $offset;
      $num = $totalcount - $offset;
      $sql = "select * from {jinoboard_contents} where $boardsql and isnotice = 0 order by timecreated DESC, ref DESC, step ASC";
      $contents = $DB->get_records_sql($sql, array('board' => $board->type), $offset, $perpage);
      ?>
      <?php //상단에뿌려주는부분
      if ($board->allownotice == 1) {
        $sql = "select * from {jinoboard_contents} where $boardsql " . $like . " and isnotice = 1 order by ref DESC, step ASC";
        $notices = $DB->get_records_sql($sql, array('board' => $board->id, 'search' => '%' . $search . '%'));
        foreach ($notices as $content) {
          $postuser = $DB->get_record('user', array('id' => $content->userid));
          $fullname = fullname($postuser);
          $userdate = userdate($content->timecreated);
          $by = new stdClass();
          $by->name = $fullname;
          $by->date = $userdate;
          $fs = get_file_storage();
          if (!empty($notice->id)) {
            $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $notice->id, 'timemodified', false);
          } else {
            $files = array();
          }
          if (count($files) > 0) {
            $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
          } else {
            $filecheck = "";
          }
        }
      }
      ?>
      <table class="table m-block">
        <thead>
          <tr>
            <th width="10%"><?php echo get_string('num', 'local_jinoboard'); ?></th>
            <th><?php echo get_string('title', 'local_jinoboard'); ?></th>
            <th width="15%"><?php echo get_string('writer', 'local_jinoboard'); ?></th>
            <th width="20%"><?php echo get_string('date', 'local_jinoboard'); ?></th>
            <th width="10%"><?php echo get_string('view:cnt', 'local_jinoboard'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($contents) {
            foreach ($contents as $content) {
              $list_num++;
              $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
              $fs = get_file_storage();
              $files = $fs->get_area_files($context->id, 'local_jinoboard', $filename, $content->id, 'timemodified', false);
              if (count($files) > 0) {
                $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklasscampus/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
              } else {
                $filecheck = "";
              }
              $postuser = $DB->get_record('user', array('id' => $content->userid));
              $fullname = fullname($postuser);
              $userdate = userdate($content->timecreated);
              $by = new stdClass();
              $by->name = $fullname;
              $by->date = $userdate;
          ?>
              <tr>
                <td class="m-hide"><?php echo $num; ?></td>
                <td class="text-left overflow  
                                    <?php if ($board->newday) {
                                      echo ($content->timemodified + (86400 * $board->newday)) > $today ? 'hasnew' : '';
                                    } ?> ">
                  <?php echo !(empty($step_icon)) ? '<span class="tb-reply">re</span>' : '';
                  if ($content->issecret && $USER->id != $content->userid && !is_siteadmin() && $parent->userid != $USER->id) {
                    echo $content->title;
                  } else {
                    echo "<a href='" . $VISANG->wwwroot . "/employer/legaltax_detail.php?id=" . $content->id . "&page=" . $page . "&perpage=" . $perpage . "&list_num=" . $list_num . "&search=" . $search . "&type=" . $type . "&searchfield=" . $searchfield . "'>" . $content->title . "</a>";
                  }
                  ?>
                  <?php if ($board->newday) {
                    echo ($content->timemodified + (86400 * $board->newday)) > $today ? '<span class="ic-new">N</span>' : '';
                  } ?></td>
                <td><?php echo $fullname; ?></td>
                <td><?php echo date("Y-m-d", $content->timemodified); ?></td>
                <td class="m-f-r"><span class="m-show inline"><?php echo get_string('view:cnt', 'local_jinoboard'); ?></span><?php echo $content->viewcnt; ?></td>
              </tr>
            <?php
              $num--;
            }
          } else {
            ?> <tr>
              <td colspan="5"><?php if ($type == 2) {
                                echo get_string('nodata:qna', 'local_mypage');
                              } else {
                                echo get_string('nocontent', 'local_jinoboard');
                              } ?></td>
            </tr>
          <?php  } ?>
        </tbody>
      </table>

      <?php
      if (count($contents)) {
        $page_params = array();
        $page_params['type'] = $type;
        $page_params['perpage'] = $perpage;
        $page_params['search'] = $search;
        $page_params['searchfield'] = $searchfield;
        $page_params['category'] = $category;
        $VISANG->theme->table_pagination($VISANG->wwwroot . "/employer/legaltax.php", $page_params, ceil($total_pages / $perpage), $page);
      }
      ?>
    </div>
  </div>
</div>
<script>
  $(function() {
    $("#accordion").accordion({
      collapsible: true,
      heightStyle: "content",
      header: "ul",
      active: false
    });
    $('.div_taps').css('display', 'none');
    $('#information').css('display', 'block');
    $('#information_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu1_over.png"; ?>');
    var previous_menu = 'information_btn'
    var previous_menu_num = '1'
    $('#information_btn').click(function() {
      $('.div_taps').css('display', 'none');
      $('#information').css('display', 'block');
      $('#information_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu1_over.png"; ?>');
      if (previous_menu_num != '1') {
        $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
        previous_menu = 'information_btn'
        previous_menu_num = '1'
      }
    });
    $('#database_btn').click(function() {
      $('.div_taps').css('display', 'none');
      $('#database').css('display', 'block');
      $('#database_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu2_over.png"; ?>');
      if (previous_menu_num != '2') {
        $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
        previous_menu = 'database_btn'
        previous_menu_num = '2'
      }
    });
    $('#endnote_btn').click(function() {
      $('.div_taps').css('display', 'none');
      $('#endnote').css('display', 'block');
      $('#endnote_btn').attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu3_over.png"; ?>');
      if (previous_menu_num != '3') {
        $('#' + previous_menu).attr('src', '<?php echo $CFG->wwwroot . "/local/jinoboard/images/btnMenu" ?>' + previous_menu_num + '.png');
        previous_menu = 'endnote_btn'
        previous_menu_num = '3'
      }
    });
  });
  //	$('#accordion input[type="checkbox"]').click(function(e) {
  //		e.stopPropagation();
  //	});
</script>

<?php $VISANG->theme->footer_foremployer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
