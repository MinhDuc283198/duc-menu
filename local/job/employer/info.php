<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER,$PAGE;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/info.php');
$context = context_system::instance();
$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);


$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

$step = optional_param('step',1, PARAM_INT);
$reasons = mkcom_get_reasons($comid);

// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'menuInfo';
$VISANG->theme->header_foremployer();



//echo $OUTPUT->header1();
$currentUI = $step;
if(empty($_POST["stepp"])){
    if($step == 1){
        $currentUI = 1;
        $company = mkcom_get_company_info($comid);
        $company_size = mkcom_get_company_size();
        $districts = mkcom_get_districts();
    }elseif($step == 2){
        $currentUI = 2;
        $company = mkcom_get_company_info($comid);
        $reasons = mkcom_get_employer_reasons($comid);
        $reason_array = array();
        foreach ($reasons as $reason):
            $reason_array[] = $reason;
        endforeach;
    }
}else{
    if($_POST["stepp"] == "2"){
        $currentUI = 2;
        $reasons = mkcom_get_employer_reasons($comid);
        $reason_array = array();
        foreach ($reasons as $reason):
            $reason_array[] = $reason;
        endforeach;

        $sizeid = optional_param('com_size', -1, PARAM_INT);
        $dictristid = optional_param('com_district', '', PARAM_RAW);
        $address = optional_param('com_address', '', PARAM_RAW);
        $homepage = optional_param('com_homepage', '', PARAM_RAW);
        $facebook = optional_param('com_facebook', '', PARAM_RAW);
        $logoPath = mkcom_save_logo($context->id, $comid, $_FILES);

        $company = mkcom_get_company_info($comid);
        if($sizeid > -1)
            $company->company_size_id = $sizeid;
        $company->company_website = $homepage;
        $company->company_facebook = $facebook;
        $company->company_address = $address;
        $company->company_logo = $logoPath;
        $DB->update_record('vi_employers', $company);

    }elseif($_POST["stepp"] == "1"){
        $currentUI = 1;
        $company = mkcom_get_company_info($comid);
        $company_size = mkcom_get_company_size();
        $districts = mkcom_get_districts();

    }elseif($_POST["stepp"] == "3"){
        $currentUI = 3;
        $shortDescri = optional_param('com_short_descri', '', PARAM_RAW);
        $descri = optional_param('com_descri', '', PARAM_RAW);
        $reason1 = optional_param('com_reason_1', '', PARAM_RAW);
        $reason2 = optional_param('com_reason_2', '', PARAM_RAW);
        $reason3 = optional_param('com_reason_3', '', PARAM_RAW);
        $benafit = optional_param('com_benafit', '', PARAM_RAW);

        $reason1_id = mkcom_findOrCreate_reasonCaregoryID($reason1,$comid);
        $reason2_id = mkcom_findOrCreate_reasonCaregoryID($reason2,$comid);
        $reason3_id = mkcom_findOrCreate_reasonCaregoryID($reason3,$comid);

        $DB->delete_records('vi_employer_reasons', array('employer_id' => $comid));
        $com_reason = new stdClass();
        $com_reason->reason_category_id = $reason1_id;
        $com_reason->employer_id = $comid;
        if($reason1 != '' && $reason1 != ' ')
            $DB->insert_record('vi_employer_reasons', $com_reason);
        $com_reason->reason_category_id = $reason2_id;
        if($reason2 != '' && $reason2 != ' ')
            $DB->insert_record('vi_employer_reasons', $com_reason);
        $com_reason->reason_category_id = $reason3_id;
        if($reason3 != '' && $reason3 != ' ')
            $DB->insert_record('vi_employer_reasons', $com_reason);

//         mkcom_set_company_reason($reason1_id, $comid);
//         mkcom_set_company_reason($reason2_id, $comid);
//         mkcom_set_company_reason($reason3_id, $comid);

        $company = mkcom_get_company_info($comid);
        $company->company_short_description = $shortDescri;
        $company->company_description = $descri;
        $company->reason_description = $benafit;
        $DB->update_record('vi_employers', $company);
        return 'true';
    }
}

?>
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo $VISANG->wwwroot . '/employer/lib';?><!--/css/bootstrap.css">-->
<link rel="stylesheet" type="text/css" href="<?php echo $VISANG->wwwroot . '/employer/lib';?>/css/prettify.css">
<link rel="stylesheet" type="text/css" href="<?php echo $VISANG->wwwroot . '/employer/lib';?>/bootstrap-wysihtml5.css">
<div class="center-tit"><?php echo get_string('CompanyInfo', 'local_job'); ?></div>

<!-- Starting step 1 -->
<?php if($currentUI == 1){?>
    <form method="POST" enctype="multipart/form-data">
        <div class="w-bx-tp style02">
            <ul class="login-tab tab-event">
                <li class="on"><a href="#"><?php echo get_string('BasicInformation', 'local_job'); ?></a></li>
                <li><a href="<?php echo $VISANG->wwwroot . '/employer/info.php?step=2'?>"><?php echo get_string('Details', 'local_job'); ?></a></li>
            </ul>
            <div class="company-info-area">
                <div class="ci-img">
                    <div class="info-txt" style="background-image: url('<?php echo $company->company_logo;?>') !important;">
                        <span><?php echo get_string('employer:logoCom1', 'local_job'); ?></span>
                        <span><?php echo get_string('employer:logoCom2', 'local_job'); ?></span>
                        <span><?php echo get_string('employer:logoCom3', 'local_job'); ?></span>
                        <span><?php echo get_string('employer:logoCom4', 'local_job'); ?></span>
                        <span><?php echo get_string('employer:logoCom5', 'local_job'); ?></span>
                    </div>
                    <p class="logo-img <?php echo ($company->company_logo != "" && $company->company_logo != " ") ? "" : "d-none";?>"><img class="rounded-circle" id="logoImg" src="<?php echo $CFG->wwwroot."/pluginfile.php/".$company->company_logo;?>" alt="" /></p>
                    <label><input type="file" name="com_logo" id="com_logo"/></label>
                </div>
                <div class="ci-txt">
                    <p>
                        <span><?php echo get_string('Companyname', 'local_job'); ?></span>
                        <strong><?php echo $company->company_name;?></strong>
                    </p>
                    <p>
                        <span><?php echo get_string('companyInTitle', 'local_job'); ?></span>
                        <strong><?php echo $company->company_type->name;?></strong>
                    </p>
                    <p>
                        <span><?php echo get_string('countryName', 'local_job'); ?></span>
                        <strong><?php echo $company->country->name;?></strong>
                    </p>
                </div>
            </div>

            <p class="imprt-txt"><?php echo get_string('FieldRequest', 'local_job'); ?></p>
            <div class="rw">
                <p class="tit hasdot"><?php echo get_string('Companyscale', 'local_job'); ?></p>
                <select class="w100" name="com_size">
                    <option value="-1"><?php echo get_string('SelectOne', 'local_job'); ?></option>
                    <?php foreach ($company_size as $com_size) : ?>
                        <option value="<?php echo $com_size->id;?>" <?php echo ($com_size->id == $company->company_size_id) ? "selected" : "";?>><?php echo $com_size->name;?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="rw">
                <p class="tit hasdot"><?php echo get_string('Region', 'local_job'); ?></p>
                <select class="w100" name="com_district">
                    <option value="-1"><?php echo get_string('SelectRegion', 'local_job'); ?></option>
                    <?php foreach ($districts as $district) : ?>
                        <option value="<?php echo $district->id;?>" <?php echo ($district->id == $company->country_id) ? "selected" : "";?>><?php echo $district->name;?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="rw">
                <p class="tit hasdot" ><?php echo get_string('address', 'local_job'); ?></p>
                <input type="text" name="com_address" placeholder="<?php if($company->company_address == null) echo 'Company_address'; else echo $company->company_address;?>" value="<?php echo $company->company_address;?>" class="w100" />
            </div>
            <div class="rw">
                <p class="tit"><?php echo get_string('Homepage', 'local_job'); ?></p>
                <input type="text" name="com_homepage" placeholder="<?php if($company->company_website == null) echo 'URL'; else echo $company->company_website;?>" value="<?php echo $company->company_website;?>" class="w100" />
            </div>
            <div class="rw">
                <p class="tit"><?php echo get_string('Facebookaddress', 'local_job'); ?></p>
                <input type="text" name="com_facebook" placeholder="<?php if($company->company_facebook == null) echo 'URL'; else echo $company->company_facebook;?>" value="<?php echo $company->company_facebook;?>" class="w100" />
            </div>
        </div>
        <div class="btn-area text-center">
            <input type="hidden" name="stepp" value="2"/>
            <input type="hidden" name="comid" value="<?php echo $comid;?>"/>
            <!-- <input type="button" value="미리보기" class="btns h60" /> -->
            <input type="submit" value="<?php echo get_string('Save', 'local_job'); ?>" class="btns point h60" />
        </div>
    </form>

    <!-- Starting step 2 -->
<?php }elseif($currentUI == 2){?>
    <form method="POST" id="info-form">

        <div class="w-bx-tp style02" style="max-width: 750px !important;">
            <ul class="login-tab tab-event">
                <li><a href="<?php echo $VISANG->wwwroot . '/employer/info.php?step=1'?>"><?php echo get_string('BasicInformation', 'local_job'); ?></a></li>
                <li class="on"><a href="#"><?php echo get_string('Details', 'local_job'); ?></a></li>
            </ul>

            <p class="imprt-txt"><?php echo get_string('FieldRequest', 'local_job'); ?></p>
            <div class="rw">
                <p class="tit hasdot"><?php echo get_string('ServiceIntroduction', 'local_job'); ?></p>
                <input type="text" placeholder="<?php echo get_string('employer:logoCom6', 'local_job'); ?>(100byte)" class="max-txt mb-3" name="com_short_descri" id="com_short_descri" value="<?php echo $company->company_short_description;?>"/>
                <span class="byte"><em id="com_short_descri_v">0</em>/100</span>
                <textarea class="mg-tp10 my-0" placeholder="<?php echo get_string('employer:logoCom7', 'local_job'); ?>(900byte)" name="com_descri" id="com_descri"><?php echo $company->company_description;?></textarea>
                <blockquote class="blockquote text-right"><p><em id="com_descri_v">0</em>/900</p></blockquote>
            </div>
            <div class="rw">
                <p class="tit hasdot"><?php echo get_string('ThreeReasons', 'local_job'); ?></p>
                <p class="mg-bt10">
                    <span class="num">1.</span><input type="text" id="com_reason_1" name="com_reason_1" value="<?php echo $reason_array[0]->name;?>"/>
                </p>
                <p class="mg-bt10">
                    <span class="num">2.</span><input type="text" id="com_reason_2" name="com_reason_2" value="<?php echo $reason_array[1]->name;?>"/>
                </p>
                <p>
                    <span class="num">3.</span><input type="text" id="com_reason_3" name="com_reason_3" value="<?php echo $reason_array[2]->name;?>"/>
                </p>
            </div>
            <div class="rw">
                <p class="tit hasdot"> <?php echo get_string('Benefits', 'local_job'); ?></p>
                <textarea class="my-0" placeholder="<?php echo get_string('employer:logoCom8', 'local_job'); ?>(900byte)" name="com_benafit" id="com_benafit"><?php echo $company->reason_description;?></textarea>
                <blockquote class="blockquote text-right"><p><em id="com_benafit_v">0</em>/900</p></blockquote>
            </div>
        </div>
        <div class="btn-area text-center">
            <input type="hidden" name="stepp" value="3"/>
            <input type="hidden" name="comid" value="<?php echo $comid;?>"/>
            <input type="button" value="<?php echo get_string('Previous', 'local_job'); ?>" class="btns h60" onclick="window.location.href = '<?php echo $VISANG->wwwroot . '/employer/info.php?step=1'?>';"/>
            <input type="button" value="<?php echo get_string('Save', 'local_job'); ?>" class="btns point h60" id="submit-form" />
        </div>
    </form>
<?php }elseif($currentUI == 3){?>
    <!-- Do some thing when done -->

    <?php redirect($VISANG->wwwroot . '/employer/dashboard.php'); }?>
<script type="text/javascript">
    $("body").addClass("bgblue");

    $("#submit-form").click(function(){
        if($("#com_short_descri").val() != "" &&
            $("#com_descri").val() != "" &&
            $("#com_benafit").val() != "" &&
            ($("#com_reason_1").val() != "" ||
                $("#com_reason_2").val() != "" ||
                $("#com_reason_3").val() != "")) {
            $.ajax({
                type: 'post',
                url: '<?php echo $baseurl;?>',
                data: $('#info-form').serialize(),
                enctype: 'multipart/form-data',
                success: function (data) {
                    window.location.href = '<?php echo $VISANG->wwwroot . '/employer/info.php?step=1'?>';
                }
            });
        }
        if($("#com_short_descri").val() == "") $("#com_short_descri").addClass("error-field");
        if($("#com_descri").val() == "") $("#com_descri").addClass("error-field");
        if($("#com_benafit").val() == "") $("#com_benafit").addClass("error-field");
        if($("#com_reason_1").val() == "") $("#com_reason_1").addClass("error-field");
    });

    $("#com_short_descri").keyup(function(){
        var content = $("#com_short_descri").val();
        var len = content.length;
        $("#com_short_descri_v").html(len);
        if(len > 99){
            $("#com_short_descri").val(content.slice(0, -1));
        }
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#logoImg').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#com_logo").change(function() {
        readURL(this);
        $(".logo-img").removeClass("d-none");
    });
</script>

<script src="<?php echo $VISANG->wwwroot . '/employer/lib';?>/js/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $VISANG->wwwroot . '/employer/lib';?>/js/prettify.js"></script>
<script src="<?php echo $VISANG->wwwroot . '/employer/lib';?>/bootstrap-wysihtml5.js"></script>
<script>
    $('#com_benafit').wysihtml5({
        events: {
            load: function() {
                $('.wysihtml5-sandbox').contents().find('body').on("keydown",function(event) {
                    //your key event
                    var content = $('.wysihtml5-sandbox')[1].contentWindow.document.body.innerText;
                    var len = content.length;
                    $("#com_benafit_v").html(len);
                    var key = window.event ? event.keyCode : event.which;
                    if(len > 889 && key != 8 && key != 46){
                        return false;
                    }
                });
            }
        }
    });
    $('#com_descri').wysihtml5({
        events: {
            load: function() {
                $('.wysihtml5-sandbox').contents().find('body').on("keydown",function(event) {
                    //your key event
                    var content = $('.wysihtml5-sandbox')[0].contentWindow.document.body.innerText;
                    var len = content.length;
                    $("#com_descri_v").html(len);
                    var key = window.event ? event.keyCode : event.which;
                    if(len > 889 && key != 8 && key != 46){
                        return false;
                    }
                });
            }
        }
    });
</script>

<style>
    .error-field{border-color: red !important;}
</style>

<?php
$VISANG->theme->footer_foremployer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
