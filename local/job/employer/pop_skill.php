<?php
require_once(dirname(__FILE__) . '/../lib.php');
global $VISANG, $DB;

$elid = optional_param('elid', '', PARAM_RAW);
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// renders

?>
<div class="layerpop no-header">
    <a href="#" class="pop-close"><?php echo get_string('close', 'local_job'); ?></a>
    <div class="pop-contents">
        <?php
        if ($elid == 'skill_groups') :
        ?>
            <div class="cont-tit">
                <strong><?php echo get_string('my_inter:skill_group', 'local_job'); ?></strong>
                <span><?php echo get_string('select_one', 'local_job'); ?></span>
            </div>

            <div class="chk-label-group">
                <?php
                $sql = "SELECT s.id, s.name
                		FROM {vi_skill_groups} s
                		ORDER BY s.id";
                $skill_groups = $DB->get_records_sql($sql, array());
                ?>
                <?php foreach ($skill_groups as $skill_group) : ?>
                	<?php if($skill_group->name != "" || $skill_group->name != " "){?>
                    <label>
                        <input name="pop_inter_chk[]" data-name="<?php echo $skill_group->name; ?>" value="<?php echo $skill_group->id; ?>" type="checkbox" />
                        <span><?php echo $skill_group->name; ?></span>
                    </label>
                    <?php }?>
                <?php endforeach; ?>
            </div>
        <?php elseif ($elid == 'skills') : ?>
            <div class="cont-tit">
                <strong><?php echo get_string('my_inter:skill', 'local_job'); ?></strong>
                <span><?php echo get_string('select_three', 'local_job'); ?></span>
            </div>

            <div class="chk-label-group">
                <?php
                $sql = "SELECT s.*
                    FROM {vi_skills} s
                    ORDER BY s.id";
                $skills = $DB->get_records_sql($sql, array());
                ?>
                <?php foreach ($skills as $skill) : ?>
                    <label>
                        <input name="pop_inter_chk[]" data-name="<?php echo $skill->name; ?>" value="<?php echo $skill->id; ?>" type="checkbox" />
                        <span><?php echo $skill->name; ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

    </div>
    <div class="btn-area text-center">
        <input onclick="btnAddAction('<?php echo $elid; ?>')" type="button" value="<?php echo get_string('Save', 'local_job'); ?>" class="btns point w100" />
    </div>
</div>


