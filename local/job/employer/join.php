<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
require_once $CFG->dirroot . '/local/signup/lib.php';

global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/join.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

$step = required_param('step', PARAM_INT);
$commitS2 = optional_param('commitS2', 0, PARAM_INT);
$email = optional_param('email', "", PARAM_RAW);
$password = optional_param('password', "", PARAM_RAW);
$emailauth = new local_auth_plugin_email();

if($commitS2 == 1){
//     $user = $DB->get_record("user", array("id" => $userID));
    $user = new stdClass();
    $user->username = optional_param('email', "", PARAM_RAW);
    $user->email = optional_param('email', "", PARAM_RAW);
    $user->password = optional_param('password', "", PARAM_RAW);

    if (!empty($user->email) && !empty($user->password)) {
        if ($uid = $emailauth->user_signup($user,true,"",$user->email)) {

            $user = $DB->get_record('user',array('id'=>$uid));
            $user->auth = 'email';
            $user->lastname = '&nbsp;';
            $user->confirmed = 1;
            $user->lang = current_language();
            $user->timecreated = time();
            $user->mnethostid = $CFG->mnet_localhost_id;
            $user->secret = "CACC";
            $user->firstaccess = 1;
            $user->joinstatus = 'j';
            $user->phone1 = optional_param('c_contact_person', 0, PARAM_RAW);
            $user->phone2 = optional_param('c_itax', 0, PARAM_RAW);
            $user->firstname = optional_param('c_contact_name', 0, PARAM_RAW);
            $DB->update_record('user',$user);

            $company = new stdClass();
            $company->company_name = optional_param('c_name', 0, PARAM_RAW);
            $company->company_website = optional_param('c_homepage', 0, PARAM_RAW);
            $company->country_id = optional_param('c_country', 0, PARAM_INT);
            $company->company_type_id = optional_param('c_industry', 0, PARAM_INT);
            $company->published = 0;
            $company->confirmed = 0;
            $company->timecreated = time();
            $company->company_address = optional_param('c_address', 0, PARAM_RAW);
            $comID = $DB->insert_record("vi_employers", $company);

            $certificatePath = mkcom_save_certificate($context->id, $uid, $_FILES);
            $com = $DB->get_record("vi_employers", array('id'=> $comID));
            $com->company_cover = $certificatePath;
            $DB->update_record("vi_employers", $com);

            $adem = new stdClass();
            $adem->employer_id = $comID;
            $adem->user_id = $uid;
            $DB->insert_record("vi_admin_employers", $adem);
            redirect($VISANG->wwwroot . '/employer/waiting-confirm.php');
        }else{
            redirect($VISANG->wwwroot . "/employer/join.php?userID=".$uid."&step=2");
        }
    }
//     $DB->update_record("user",$user);
}

// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'jobs';

echo $VISANG->theme->header();
$currentUI = $step;
$lang = current_language();
if($lang != 'ko'){
    $tlang = $lang;
}else{
    $tlang = "";
}
$terms1 =  $DB->get_record_sql("select content$tlang content from {notice_board} where category = ? and delflag = ? and fbstatus = ? order by id desc",array(2,0,1)) ;
$terms2 =  $DB->get_record_sql("select content$tlang content from {notice_board} where category = ? and delflag = ? and fbstatus = ? order by id desc",array(1,0,2)) ;
$terms3 =  $DB->get_record_sql("select content$tlang content from {notice_board} where category = ? and delflag = ? and fbstatus = ? order by id desc",array(2,0,0)) ;

?>
<!-- Starting step 1 -->
<?php if($currentUI == 1){?>
<ul class="nav-rd">
    <li class="on"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
</ul>
<div class="join-tit"><?php echo get_string('c_joinAgreement', 'local_job'); ?></div>
<div class="login-bx company">
	<label class="custom-ck">
		<input type="checkbox" id="agreeAll"/>
    	<span><?php echo get_string("c_agreeAllAll","local_job")?></span>
    </label>

	<div class="login-tit">
	<label class="custom-ck">
		<input type="checkbox" id="agree1"/>
    	<span><?php echo get_string("c_TermsofUse","local_job")?>(<a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=1&category=2', '서비스이용방침', 'resizable,height=800,width=800,scrollbars=yes'); return false;" ><?php echo get_string("allview","local_signup")?></a>)</span>
    </label>
    </div>
    <div class="scrl-box"><?php echo $terms1->content?></div>

	<div class="login-tit">
    <label class="custom-ck">
		<input type="checkbox" id="agree2"/>
    	<span><?php echo get_string("c_regulations","local_job")?>(<a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=2&category=2', '이용약관', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string("allview","local_signup")?></a>)</span>
    </label>
    </div>
    <div class="scrl-box"><?php echo $terms2->content?></div>

    <div class="login-tit">
    <label class="custom-ck">
		<input type="checkbox" id="agree3"/>
    	<span><?php echo get_string("c_Privacypolicy","local_job")?>(<a href="#" onclick="window.open('/local/management/term/personalinfo.php?fbstatus=0&category=2', '개인정보취급방침', 'resizable,height=800,width=800,scrollbars=yes'); return false;"><?php echo get_string("allview","local_signup")?></a>)</span>
    </label>
    </div>
    <div class="scrl-box"><?php echo $terms3->content?></div>

    <p class="info"><?php echo get_string('c_abletoregister', 'local_job'); ?></p>

    <div class="text-center btn-area">
        <input id="checkAgreement" type="button" value="<?php echo get_string('c_Iagree', 'local_job'); ?>" class="btns point big" onclick="checkAgreement()"/>
    </div>
    <script type="text/javascript">
		function checkAgreement(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked"))
				location.href = '<?php echo $baseurl. "?step=2";?>';
		}

		$("#agreeAll").click(function(){
			if($('#agreeAll').is(":checked")){
				$('#agree1').prop('checked', true);
				$('#agree2').prop('checked', true);
				$('#agree3').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agree1').prop('checked', false);
				$('#agree2').prop('checked', false);
				$('#agree3').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}

		});

		$('#agree1').click(function(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked")){
				$('#agreeAll').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agreeAll').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}
		});
		$('#agree2').click(function(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked")){
				$('#agreeAll').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agreeAll').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}
		});
		$('#agree3').click(function(){
			if($('#agree1').is(":checked") && $('#agree2').is(":checked") && $('#agree3').is(":checked")){
				$('#agreeAll').prop('checked', true);
				$('#checkAgreement').attr('style', 'background: #485cc7 !important; color: #fff !important;');
			}else{
				$('#agreeAll').prop('checked', false);
				$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
			}
		});
		$('#checkAgreement').attr('style', 'background: #fff !important; color: #485cc7 !important;');
    </script>
</div>

<!-- Starting step 2 -->
<?php }elseif($currentUI == 2){?>
<ul class="nav-rd">
    <li><a href="#">1</a></li>
    <li class="on"><a href="#">2</a></li>
    <li><a href="#">3</a></li>
</ul>
<div class="join-tit"><?php echo get_string('c_JoinCorporate', 'local_job'); ?></div>
<div class="login-bx company">
    <form class="email-bx login" method="POST" id="emailSignup" action="<?php echo $baseurl."?step=3"?>">
        <p>
            <input type="text" class="w100" placeholder="<?php echo get_string('c_startJoinEmail', 'local_job'); ?>" name="email" id="email"/>
            <span class="warning" id="emailwarning"><!-- <?php echo get_string('c_IDConditions', 'local_job'); ?> --></span>
        </p>
        <p>
            <input type="password" placeholder="<?php echo get_string('c_EnterPassword', 'local_job'); ?>" name="password" id="joinpassword"/>
            <!-- <span class="warning" id="joinpasswordwarning"><?php echo get_string('c_PasswordEntryConditions', 'local_job'); ?></span> -->
        </p>
        <p>
            <input type="password" placeholder="<?php echo get_string('c_retypePass', 'local_job'); ?>"  id="joinpassword2"/>
            <span class="warning" id="joinpassword2warning"><!-- <?php echo get_string('c_conditiontext', 'local_job'); ?>--></span>
        </p>
        <div class="btns-area text-center">
            <input type="button" value="<?php echo get_string('c_Accountregistration', 'local_job'); ?>" class="btns br" onclick="check_val('email');"/>
        </div>
    </form>
</div>
<!-- Starting step 3 -->
<?php }elseif($currentUI == 3){?>
<ul class="nav-rd">
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li class="on"><a href="#">3</a></li>
</ul>
<div class="join-tit"><?php echo get_string('c_Entercompanyinformation', 'local_job'); ?></div>
<div class="login-bx company">
    <p class="ft-dot"><?php echo get_string('c_Itemsshown', 'local_job'); ?>.</p>
    <form class="email-bx join" method="POST" enctype="multipart/form-data" id="join-form">
        <p>
            <strong><?php echo get_string('Companyname', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="<?php echo get_string('c_enterComName', 'local_job'); ?>" name="c_name" id="c_name"/>
        </p>

		<?php $countries = $DB->get_records("vi_countries", array());?>
        <p class="half">
            <strong><?php echo get_string('countryName', 'local_job'); ?></strong>
            <select class="w100 not-null" name="c_country" id="c_country">
            	<option value=""><?php echo get_string('c_SelectCountry', 'local_job'); ?></option>
            	<?php foreach ($countries as $con){?>
                <option value="<?php echo $con->id;?>"><?php echo $con->name;?></option>
                <?php }?>
            </select>
        </p>

        <?php $Industry = $DB->get_records('vi_company_types',array());?>
        <p class="half mg">
            <strong><?php echo get_string('c_Industry', 'local_job'); ?></strong>
            <select class="not-null"  name="c_industry" id="c_industry">
                <option value=""><?php echo get_string('c_Selectindustry', 'local_job'); ?></option>
                <?php foreach ($Industry as $in){?>
                <option value="<?php echo $in->id;?>"><?php echo $in->name;?></option>
                <?php }?>
            </select>
        </p>

        <p>
            <strong><?php echo get_string('c_CompanyRegistration', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="000000000" name="c_itax" id="c_itax"/>
        </p>

        <div>
            <strong><?php echo get_string('c_registrationcertificate', 'local_job'); ?></strong>
            <div class="custom-file-box">
                <input class="not-null" type="text" id="file-name"/>
                <input type="file" id="file01" name="c_file"/>
                <label for="file01" class="btns gray"><?php echo get_string('Fileselection', 'local_job'); ?></label>
            </div>
        </div>
        <p>
            <strong><?php echo get_string('address', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="<?php echo get_string('c_enterRepAddress', 'local_job'); ?>" name="c_address" id="c_address"/>
        </p>
        <p>
            <strong class="no-dot"><?php echo get_string('c_homepage', 'local_job'); ?></strong>
            <input type="text" placeholder="URL" name="c_homepage" id="c_homepage"/>
        </p>
        <p class="half">
            <strong><?php echo get_string('c_Contactname', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="<?php echo get_string('c_enterContactName', 'local_job'); ?>" name="c_contact_name" id="c_contact_name"/>
        </p>
        <p class="half mg">
            <strong><?php echo get_string('c_ContactPerson', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="<?php echo get_string('c_enterRoomTitle', 'local_job'); ?>" name="c_contact_place" id="c_contact_place"/>
        </p>
        <p class="half">
            <strong><?php echo get_string('c_Contactpersons', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="" name="c_contact_person" id="c_contact_person"/>
            <span class="warning"><?php echo get_string('c_Nonconformity', 'local_job'); ?></span>
        </p>
        <p class="half mg">
            <strong><?php echo get_string('c_Contactemail', 'local_job'); ?></strong>
            <input class="not-null" type="text" placeholder="sample@email.com" name="c_contact_email" id="c_conact_email"/>
            <span class="warning"><?php echo get_string('c_Nonconformance', 'local_job'); ?></span>
        </p>

        <div class="text-center btn-area">
        	<input type="hidden" name="email" value="<?php echo $email;?>"/>
        	<input type="hidden" name="password" value="<?php echo $password;?>"/>
        	<input type="hidden" name="commitS2" value="1"/>
            <input type="button" value="<?php echo get_string('c_Requestforapproval', 'local_job'); ?>" class="btns point big" onclick="check_notnull();"/>
        </div>
	</form>
</div>
<?php }?>
<script type="text/javascript">
    $("body").addClass("bgblue");
</script>

<script>
    $(document).ready(function() {
        $(document).on("keyup", "#joinpassword", function() {
            match_password();
        });
        $(document).on("keyup", "#joinpassword2", function() {
            match_password();
        });
    });

    $('#file01').change(function() {
        var filename = $('#file01').val();
        $('#file-name').val(filename);
    });

    function match_password() {
        if ($("#joinpassword2").val() == $("#joinpassword").val()) {
            $("#joinpassword2warning").html("Password is matched.");
        } else {
            $("#joinpassword2warning").html("Password is not matched.");
        }

    }

    function check_val(type) {
        var target = $("#" + type);
        var regExpemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (type == 'email' && !regExpemail.test($("#email").val())) {
            var errortext = "<?php echo get_string("emailrule", "local_signup", array('rule' => "visang@visang.com")) ?>";
//             alert(errortext);
            $("#" + type + "warning").text(errortext);
//             $("#" + type + "confirm").val(0);
        } else {
            $.ajax({
                url: "/local/signup/valueconfirm.php",
                type: 'post',
                dataType: "json",
                data: {
                    type: type,
                    value: target.val()
                },
                success: function(result) {
                    if (result.status) {
//                         $("#" + type + "confirm").val(result.status);
                        $("#" + type + "warning").text(result.text);
                        $("#emailSignup").submit();
                    } else {
//                         $("#" + type + "confirm").val(result.status);
                        $("#" + type + "warning").text(result.text);
                    }
                }
            });
        }
    }

    function check_notnull(){
		var items = $(".not-null");
		var error = 0;
		for(var i=0; i<items.length; i++){
			var item = items[i];
			if(item.value =="" || item.value == " "){
				item.classList.add("error-field");
				error++;
			}
		}
		if(error==0) $("#join-form").submit();
	}
</script>

<style>
.custom-file-box input[type=text] {width: calc(100% - 130px) !important;}
.error-field{border-color: red !important;}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
<?php
//$VISANG->theme->footer();
?>


