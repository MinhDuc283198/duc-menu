<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER;
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/talent_search.php');
$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);
$elid = optional_param('elid', '', PARAM_RAW);
$context = context_system::instance();

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$filter = optional_param('filter', 0, PARAM_INT);
$mkstudent = optional_param('mkstudent', 0, PARAM_INT);
$order = optional_param('order', 10, PARAM_INT);
$keyword = optional_param('resume_keyword', "", PARAM_RAW);
$hasFilter = optional_param('hasFilter', "", PARAM_INT);

$ex_filter = optional_param('ex_chk2', "", PARAM_RAW);
$skg_filter = optional_param('skillgroup_chk2', "", PARAM_RAW);
$sk_filter = optional_param('skill_chk2', "", PARAM_RAW);
$area_filter = optional_param('area_chk2', "", PARAM_RAW);
$kre_filter = optional_param('korean_chk2', "", PARAM_RAW);
$ex_filter = mkcom_string_to_array($ex_filter);
$skg_filter = mkcom_string_to_array($skg_filter);
$sk_filter = mkcom_string_to_array($sk_filter);
$area_filter = mkcom_string_to_array($area_filter);
$kre_filter  = mkcom_string_to_array($kre_filter );
$ex_filter = optional_param_array('ex_chk', $ex_filter, PARAM_RAW); 
$skg_filter = optional_param_array('skillgroup_chk', $skg_filter, PARAM_RAW); 
$sk_filter = optional_param_array('skill_chk', $sk_filter, PARAM_RAW); 
$area_filter = optional_param_array('area_chk', $area_filter, PARAM_RAW); 
$kre_filter = optional_param_array('korean_chk', $kre_filter , PARAM_RAW); 

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$areas = mkcom_get_districts();
$koreaLevels = mkcom_get_krea_levels();
$experiences = mkcom_get_experiences();

$total_pages = mkcom_count_talen_filter($comid, $ex_filter, $sk_filter, $skg_filter, $kre_filter, $area_filter, $mkstudent,$hasFilter) ;
$applicants = mkcom_talen_filter($comid, $ex_filter, $sk_filter, $skg_filter, $kre_filter, $area_filter, $mkstudent,$perpage, $page, $order,$hasFilter) ;
// renders
$VISANG->theme->header_foremployer();
$VISANG->theme->menu = 'menuApply';
$pageAction = "talensFind";
$current_language = current_language();
$point_price = $DB->get_field('vi_common_code', 'point', array('code' => 2003));
?>
<div class="cont">
	<div class="group">
 
        <?php require_once(dirname(__FILE__) . '/../employer/employer_menu.php');?>
        
        <div class="crs-right-block">
            <h2 class="pg-tit"><?php echo get_string('talensFind', 'local_job'); ?></h2>
            <form method="post" id="filterForm">
            <div class="ck-scrl-bx">
                <div class="bx">
                    <div class="bx-th"><label class="custom-ck"><input type="checkbox" class="ex_chk" onclick="checkClass(this);" /><span><?php echo get_string('job_work', 'local_job'); ?></span></label></div>
                    <div class="bx-scrl">
                    <?php foreach($experiences as $ex){?>
                    	<p><label class="custom-ck"><input name="ex_chk[]" type="checkbox" class="ex_chk" value="<?php echo $ex->id;?>" <?php echo mkcom_isexist_andchecked($ex_filter, $ex->id)?>/><span><?php echo ($current_language == "ko") ? $ex->name_ko : ($current_language == "en") ? $ex->name_en : $ex->name_vi;?></span></label></p>
                    <?php }?>
                    </div>
                </div>
        
        		<?php
                $sql = "SELECT s.id, s.name
                		FROM {vi_skill_groups} s
                		ORDER BY s.name";
                $skill_groups = $DB->get_records_sql($sql, array());
                ?>    
                <div class="bx">
                    <div class="bx-th"><label class="custom-ck"><input class="skillgroup_chk" onclick="checkClass(this);" type="checkbox"  /><span><?php echo get_string('job_skillgroup', 'local_job'); ?></span></label></div>
                    <div class="bx-scrl">
                    <?php foreach($skill_groups as $hg){ if($hg->name != ""){?>
                    	<p><label class="custom-ck"><input name="skillgroup_chk[]" class="skillgroup_chk" type="checkbox" value="<?php echo $hg->id;?>" <?php echo mkcom_isexist_andchecked($skg_filter, $hg->id);?>/><span><?php echo $hg->name;?></span></label></p>
                    <?php }}?>
                    </div>
                </div>
        		<?php
                $sql = "SELECT s.*
                    FROM {vi_skills} s
                    ORDER BY s.name";
                $skills = $DB->get_records_sql($sql, array());
                ?>
                <div class="bx">
                    <div class="bx-th"><label class="custom-ck"><input class="skill_chk" onclick="checkClass(this);" type="checkbox" /><span><?php echo get_string('job_skill', 'local_job'); ?></span></label></div>
                    <div class="bx-scrl">
                        <?php foreach($skills as $hg){?>
                        <p><label class="custom-ck"><input name="skill_chk[]" class="skill_chk" type="checkbox" value="<?php echo $hg->id;?>" <?php echo mkcom_isexist_andchecked($sk_filter, $hg->id);?>/><span><?php echo $hg->name;?></span></label></p>
                        <?php }?>
                            
                    </div>
                </div>
        
                <div class="bx">
                    <div class="bx-th"><label class="custom-ck"><input class="area_chk" onclick="checkClass(this);" type="checkbox" /><span><?php echo get_string('job_workarea', 'local_job'); ?></span></label></div>
                    <div class="bx-scrl">
                    <?php foreach($areas as $area){?>
                    	<p><label class="custom-ck"><input name="area_chk[]" class="area_chk" type="checkbox" value="<?php echo $area->id;?>" <?php echo mkcom_isexist_andchecked($area_filter, $area->id);?>/><span><?php echo $area->name;?></span></label></p>
                    <?php }?>
        
                    </div>
                </div>
        
                <div class="bx">
                    <div class="bx-th"><label class="custom-ck"><input class="korean_chk" onclick="checkClass(this);" type="checkbox" /><span><?php echo get_string('KoreanLevel', 'local_job'); ?></span></label></div>
                    <div class="bx-scrl">
                    <?php foreach($koreaLevels as $le){?>
                    	<p><label class="custom-ck"><input name="korean_chk[]" class="korean_chk" type="checkbox" value="<?php echo $le->id;?>" <?php echo mkcom_isexist_andchecked($kre_filter, $le->id);?>/><span><?php echo ($current_language == "ko") ? $le->name_ko : ($current_language == "en") ? $le->name_en : $le->name_vi;?></span></label></p>
                    <?php }?>
                    </div>
                </div>
        
            </div>
        
            <div class="ck-btns text-right clearfix">
                <div class="f-r">
                	<input type="hidden" name="hasFilter" value="1"/>
                    <input type="submit" value="<?php echo get_string('search', 'local_job'); ?>" class="btns gray"/>
                    <input type="reset" value="<?php echo get_string('reset', 'local_job'); ?>" class="btns" onclick="window.location.href = '<?php echo $baseurl;?>';" />
                </div>
                <label class="custom-ck mg-r20"><input <?php echo ($mkstudent != 1) ? '' : 'checked'; ?> name="mkstudent" value="1" type="checkbox" onclick="this.form.submit()"/><span><?php echo get_string('talens_students', 'local_job'); ?></span></label>
            </div>
        	
        
            <div class="list-num">
                 	<span class="t-blue02"><?php echo $total_pages. get_string('rowsrow', 'local_job');?></span>  
                    <select class="under" onchange="this.form.submit()" name="perpage">
                        <option value="5" <?php echo ($perpage == 5) ? "selected" : "";?>><?php echo get_string('display5', 'local_job'); ?></option>
                        <option value="10" <?php echo ($perpage == 10) ? "selected" : "";?>><?php echo get_string('display10', 'local_job'); ?></option>
                        <option value="20" <?php echo ($perpage == 20) ? "selected" : "";?>><?php echo get_string('display20', 'local_job'); ?></option>
                        <option value="30" <?php echo ($perpage == 30) ? "selected" : "";?>><?php echo get_string('display30', 'local_job'); ?></option>
                    </select>
                    

                    <div class="ck-btns f-r">
                        <ul class="sort-list">
                            <li class="<?php if ($order == 10) echo 'down on ' . $order;
                                        else if ($order == 20) echo 'up on ' . $order;
                                        else echo 'down'; ?>" onclick="orderChanged(<?php echo ($order == 10) ? 20 : 10;?>)"><a><?php echo get_string('employer:searchSort', 'local_job'); ?></a></li>
                            
                        </ul>
                        
                    </div>
            </div>
            <input type="hidden" name="order" id="order" value="">
            </form>
        	<?php if(count($applicants) < 1){?>
            	<div class="no-data">
                    <p><?php  echo get_string('noapplicants2', 'local_job');?></p>
                </div>
            <?php }else{?>
            <ul class="talent-list">
                <?php foreach($applicants as $apply){?>
                	 <li>         
                        <?php
                        $uinfo = $DB->get_record("user", array('id' => $apply->uid));
                        $resume = $DB->get_record('vi_resumes', array('id' => $apply->candidate_cv));
                        $resume_url = '';
                        $cvvideo = $DB->get_record('vi_resumes', array('type' => 'video', 'user_id' => $apply->uid, 'published'=>'1'));
                        switch ($resume->type) {
                            case 'resume':
                                $resume_url = $CFG->wwwroot . '/local/job/resumes/view.php?resumeid=' . $resume->id;
                                break;
                            case 'cvfile':
                                $resume_url = $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url;
                                break;
                            case 'video':
                                $resume_url = $resume->video_url;
                                break;
                        }
                        ?>
                        <input type="submit" id="stt_big_cvid" hidden/>
                        <!-- CV Checking was deleted -->
                        <div class="t-img">
                                <span class="saved_resume_icon <?php echo mkcom_resume_has_saved($comid,$resume->id) ? "ic-heart on" : "ic-heart";  ?>" id="savedHeart-<?php echo $resume->id.'-'.$apply->id;?>" onclick="savedResume('savedHeart-<?php echo $resume->id.'-'.$apply->id?>', <?php echo $resume->id; ?>, <?php echo $comid?>)" ><?php echo get_string('talensearch:text1', 'local_job'); ?></span>
                            <?php if($cvvideo->video_url != null){?>
                            <span class="ic-play" onclick="viewVideo('<?php echo $cvvideo->video_url;?>')">play</span>
                            <?php }?>
                            <img src="<?php echo ($resume->avatar != "") ? $CFG->wwwroot . '/pluginfile.php/'.$resume->avatar : $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg'; ?>" alt="김비상" />
                        </div>
                        <div class="t-u-info">
                            <strong><?php echo $uinfo->firstname . " " . $uinfo->lastname; ?></strong>
                            <span class="t-blue02"><?php echo substr($uinfo->phone2,0,4);?></span>
                            <?php $userbadge = mkcom_count_user_badge($apply->uid); if($userbadge > 0) {?>
                                <span class="ic-badge" onclick="mkClassCompleted(<?php echo $apply->uid;?>);">+<?php echo$userbadge;?></span>
                            <?php }?>
                        </div>
                        <div class="t-c-info">
                            <div class="tit"><a href="<?php echo new moodle_url($VISANG->wwwroot . '/jobs/view.php', array('jobid' => $apply->jobid)); ?>"><?php echo $apply->title; ?></a></div>
                            <p><span><?php $rows = mkcom_get_user_work_experience($apply->uid);
                                            $html = "";
                                            foreach ($rows as $p) $html = $html . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
                                            echo rtrim($html, ", "); ?></span>
                                    <span><?php $rows = mkcom_get_user_workplace($apply->uid);
                                            $html = "";
                                            foreach ($rows as $p) $html = $html . $p->name . ", ";
                                            echo rtrim($html, ", "); ?></span>
                                    <span><?php $rows = mkcom_get_user_korean_level($apply->uid);
                                            $html = "";
                                            foreach ($rows as $p) $html = $html . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
                                            echo rtrim($html, ", "); ?></span></p>
                            <p><?php $rows = mkcom_get_user_skill_group($apply->uid);
                                $html = "";
                                foreach ($rows as $p) $html = $html . $p->name . ", ";
                                echo rtrim($html, ", "); ?></p>
                            <p><?php $rows = mkcom_get_user_skill($apply->uid);
                                $html = "";
                                foreach ($rows as $p) $html = $html . $p->name . ", ";
                                echo rtrim($html, ", "); ?></p>
                        </div>
                        <div class="t-right no-pd">
                            <span><?php echo date("Y-m-d h:i", $apply->timecreated); ?></span>
                            <a href="#" class="btns" onclick="visang_view_cv('<?php echo $resume_url; ?>', '<?php echo $resume->type; ?>', '<?php echo $resume->id; ?>')"><?php echo get_string('ViewResume_2', 'local_job'); ?></a>
                        </div>
                    </li>
                <?php }?>
            </ul>
            <?php }?>
        	<?php
            if (count($applicants)) {
                    $page_params = array();
                    $page_params['comid'] = $comid;
                    $page_params['perpage'] = $perpage;
                    $page_params['page'] = $page;
                    $page_params['ex_chk2'] = mkcom_array_to_string($ex_filter);
                    $page_params['skillgroup_chk2'] = mkcom_array_to_string($skg_filter);
                    $page_params['skill_chk2'] = mkcom_array_to_string($sk_filter);
                    $page_params['area_chk2'] = mkcom_array_to_string($area_filter);
                    $page_params['korean_chk2'] = mkcom_array_to_string($kre_filter);
                    $page_params['mkstudent'] = $mkstudent;
                    $page_params['order'] = $order;
                    $page_params['hasFilter'] = $hasFilter;
//                     var_dump( $page_params);
                    $VISANG->theme->table_pagination($baseurl, $page_params, ceil($total_pages / $perpage), $page);
                }
                ?>
        </div>
	</div></div> 
<script type="text/javascript">
function mkClassCompleted(uid) {
    utils.popup.call_layerpop("./pop_course_completion_list.php", {
        "width": "600px",
        "height": "auto",
        "callbackFn": function() {}
    }, {
        "elid": uid
    });
    return false;
};

    function viewVideo(url){
    	var windowpopup = utils.popup.call_windowpop(url, 560, 360, "video-pop"); //url, width, height, popupname;
        return false;
    }

// Download Uploaded CV
function visang_view_cv(url, type, resumeid) {
    if (type == 'cvfile') {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkcom_resume_download_status",
            resumeid: resumeid,
            appid: <?php echo $comid; ?>
        }, function (data) {
            if (data == 1) {
                window.open(url);
            } else {
                if (confirm("<?php echo get_string('down-cv-file', 'local_job', $point_price); ?>")) {
                    $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
                        sesskey: "<?php echo sesskey(); ?>",
                        action: "mkcom_resume_download",
                        resumeid: resumeid,
                        appid: <?php echo $comid; ?>
                    }, function (data) {
                        if (data == 1) {
                            if (confirm("<?php echo get_string('buy-credit-require', 'local_job'); ?>")) {
                                window.location.href = "<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>";
                            }
                        }else{
                            window.open(url);
                        }
                    });
                }
            }
        });
    } else {
        window.open(url, '_blank');
    }
}

    function savedResume(el, id,aid) {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
             sesskey: "<?php echo sesskey(); ?>",
             action: "mkcom_resume_saved",
             resumeid: id,
             appid: aid
         }, function(data) {
             if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                 }
             } else if (data == 1) {
            	 $("#"+el).addClass("on");
             } else if (data == 2){
            	 $("#"+el).removeClass("on");
             }
         });
     }
   

    function checkClass(obj){
//         alert(obj.checked);
        if(obj.checked == "true" || obj.checked == true){
        	$("."+obj.className).prop('checked', true);
        }else{
        	$("."+obj.className).prop('checked', false);
        }
        
    }

  //badge popup event
    $(".talent-list>li .t-u-info .ic-badge").click(function() {
        utils.popup.call_layerpop("./pop_course_completion_list.php", {
            "width": "600px",
            "height": "auto",
            "callbackFn": function() {}
        }, {
            "param": "11"
        });
        return false;
    });

    function orderChanged(val){
        $("#order").val(val);
 		$("#filterForm").submit();
    }
</script>       
<style>
.bx-scrl{overflow-x: hidden !important;}
</style>

<?php
$VISANG->theme->footer_foremployer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />


