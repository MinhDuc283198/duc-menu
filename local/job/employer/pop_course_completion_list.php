<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');
global $VISANG, $DB;

$elid = optional_param('elid', '', PARAM_RAW);
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

$sql = "SELECT cc.course as id, lc.title, lc.vi_title, lc.en_title, cc.timestarted, cc.timecompleted, lc.learningstart, lc.learningend, cc.reaggregate, lc.courseperiod, lbu.badgeid
        FROM {course_completions} cc 
        JOIN {lmsdata_class} lc ON cc.course = lc.courseid
        LEFT JOIN {lmsdata_badge_user} lbu ON lc.id = lbu.course
        WHERE cc.userid=:usrid AND lc.learningstart > 0";
$class = $DB->get_records_sql($sql,array("usrid"=>$elid));

$sql = "SELECT us.firstname, us.lastname, us.phone2, re.avatar 
        FROM {user} us 
        JOIN {vi_resumes} re ON re.user_id = us.id AND re.is_default=1
        WHERE us.id=:usrid";
$user = $DB->get_record_sql($sql,array("usrid"=>$elid));
$curlang = current_language();

// renders
$VISANG->theme->header_foremployer();
?>

<div class="layerpop">
    <div class="pop-title">
        <?php echo get_string('employer:viewClass1', 'local_job'); ?>
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents" style="height: 500px;">
        <p class="f16 mg-bt10 t-blue02"><?php echo get_string('employer:viewClass2', 'local_job'); ?>
        </p>
        <div class="u-info">
            <img src="<?php echo ($user->avatar != "") ? $CFG->wwwroot . '/pluginfile.php/'.$user->avatar : $CFG->wwwroot . '/theme/oklassedu/pix/images/nouser.jpg'; ?>" alt="김비상"  />
            <strong>&nbsp;<?php echo $user->firstname." ".$user->lastname;?> (<?php echo substr($user->phone2,0,4);?>)</strong>
        </div>
        <table class="table bdg-tb">
            <colgroup>
                <col width="15%" />
                <col width="/" />
                <col width="25%" />
            </colgroup>
            <tbody>
            	<?php foreach($class as $l){?>
            	<tr>
                    <td><?php echo ($l->badgeid > 0) ? '<img src="/theme/oklassedu/pix/images/ic_badge.png"/>' : '-';?></td>
                    <td class="text-left">
                    	<?php if($curlang =='ko') $tit = $l->title;
                    	else if($curlang =='en') $tit = $l->en_title;
                    	else $tit = $l->vi_title;?>
                        <p><?php echo $tit;?></p>
                        <p class="t-gray"><?php echo date("Y-m-d", $l->learningstart); echo ($l->timecompleted > 0) ? ' ~ '.date("Y-m-d", $l->timecompleted) :'';?> (<?php echo $l->courseperiod . " " . get_string('employer:courseperiod', 'local_job');?>)</p>
                    </td>
                    <?php $pro = format_lguplus_get_course_progress($l->id);?>
                    <td><?php echo ($l->timecompleted > 0) ? get_string('Completion', 'local_job') : get_string('Proceeding', 'local_job'); ?></td>
                </tr>
            	<?php }?>
            </tbody>
        </table>
    </div>
</div>
<?php
echo $VISANG->theme->footer_foremployer();
?>

<script type="text/javascript">
<!--

//-->
</script>


