<?php
require_once(dirname(__FILE__) . '/../lib.php');
global $VISANG;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
// renders
$VISANG->theme->header_foremployer();
?> 
<div id="wrap">
<div class="layerpop no-header" style="width: 600px;height: auto;top: 283.5px;left: calc(50% - 300px);position: fixed;margin-bottom: 70px;">
    <div class="pop-contents">
        <div class="cont-tit no-mg">
            <strong><?php echo get_string('pop_info_1', 'local_job'); ?><br /><?php echo get_string('pop_info_2', 'local_job'); ?>.</strong>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="<?php echo get_string('pop_edit_2', 'local_job'); ?>" class="btns br h40" onclick="window.location.href = '<?php echo $VISANG->wwwroot;?>';"/>
        <input type="button" value="<?php echo get_string('pop_edit', 'local_job'); ?>" class="btns point h40" onclick="window.location.href = '<?php echo $VISANG->wwwroot . '/employer/info.php?step=1';?>';"/>
    </div>
</div>
</div> 

