<?php
require_once(dirname(__FILE__) . '/../lib.php');
global $VISANG, $DB;

$elid = optional_param('elid', '', PARAM_RAW);
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// renders
$VISANG->theme->header_foremployer();
?>  
<div class="layerpop window style02 video">
        <div class="pop-title">
            동영상 프로필
            <a href="#" class="pop-close">닫기</a>
        </div>
        <div class="pop-contents no-pd">
           <div class="y-video-area">
               <iframe  src="https://www.youtube.com/embed/5g7dEUNG36U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           </div>
        </div>
    </div>
<?php
echo $VISANG->theme->footer_foremployer();
?>


