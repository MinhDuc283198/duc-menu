<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $PAGE;

require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
require_once($VISANG->dirroot . '/lib/common_functions.php');
require_once($VISANG->dirroot . '/lib/employers.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

// =====================================================================================================
// handles
if(!empty($_POST["comid"])){
    $comid = $_POST["comid"];
    $title = $_POST["job_title"]; //Added TITLE
    $responsibility = $_POST["job_responsibility"];//Added RESPONSIBILITIES
    $request = $_POST["job_request"];//Added RESQEST
    $krlevel = $_POST["job_krlevel"]; // Added KORREAN LEVEL
    $olang = $_POST["job_flang"]; //Added
    $salary1 = $_POST["job_salaryfrom"];//Added SALARY FROM 1
    $salary2 = $_POST["job_salaryto"];//Added SALARY TO 2
    $unit = $_POST["job_salaryunit"];//Added SALARY UNIT 3 1+2+3
    $exchange = $_POST["job_salaryinter"];//Added
    $area = $_POST["job_workingarea"];//Added WORK AREA => LOCATION
    $adress = $_POST["job_adress"]; //Added
    $hour = $_POST["job_workinghour"];  //Added
    $hcmt = $_POST["job_hourcmt"]; //Added
    $start = $_POST["job_start"];//Added START TIME
    $end = $_POST["job_end"];//Added END TIME
    $skill_groups = $_POST["skill_groups"]; //Added GROUP CATEGORY
    
    $finalEdu = $_POST["job_finalEdu"];  //Added FINAL EDUCATION
    $finalEduFrom = $_POST["job_finalEduFrom"]; // Added
    $finalEduTo = $_POST["job_finalEduTo"]; //Added
    $finalEduInfo = $_POST["job_finalEduInfo"];
    $Jskills = optional_param_array('skills', array(), PARAM_RAW); //Added
    $experiences = optional_param('job_career', 0, PARAM_INT); //Added
    $worktypes = optional_param_array('job_worktype', array(), PARAM_INT); //Added
    $isReview = optional_param('isReview', 0, PARAM_INT); //Added
    
    if($_POST["job_exposure"] == "0"){
        $exposure = 0; //Added PUBLISH
    }else{
        $exposure = 1; //Added PUBLISH
    }

    
    $job = new stdClass();
    $job->title = $title;
    $job->description = $responsibility;
    $job->skills_experience = $request;
    $job->employer_id = $comid;
    $job->job_salary_category_id = $salaryID;
    $job->startdate = strtotime($start);
    $job->enddate = strtotime($end);
    $job->skill_group_id = $skill_groups;
    $job->work_experience_id = $experiences;
    $job->korean_level_id= $krlevel;
    $job->final_education_id = $finalEdu;
    $job->age_from = $finalEduFrom;
    $job->age_to = $finalEduTo;
    $job->confirmed = 1;
    $job->timecreated = time();
    
    $job->special_info = $hour;    //Save working time ID
    $job->note = $hcmt;             // Save note for working time
    $job->other_request_info = $finalEduInfo; // Save final education info (other request)
    $job->other_lang_info = $olang; // Save other language
    $job->other_address_info = $adress; // Save note address
    
    $job->published = $exposure;
    
    $company = mkcom_get_company_info($comid);
}

// =====================================================================================================
// renders
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick.css');
$VISANG->theme->addCSS($CFG->wwwroot . '/theme/oklassedu/style/slick-theme.css');
$VISANG->theme->addJS($CFG->wwwroot . '/theme/oklassedu/javascript/slick.min.js');

$VISANG->theme->title = $job->title;
$VISANG->theme->menu = 'jobs';
$VISANG->theme->header_foremployer();
$current_language = current_language();
?>

<div class="cont mb-5 mb-md-0">
    <div class="group">
        <div class="crs-bx p-0">
            <div class="row mx-0">
                
                <div class="col-md-9 px-0 px-md-5 py-5 border-top border-md-top-0">
                    <div class="pb-4 mb-4 border-bottom">
                        <div class="d-flex">
                            <div class="w-100">
                                <div class="h2 mb-3"><?php echo $job->title; ?></div>
                            </div>
                            
                        </div>
                        <ul class="cate-list text-left mb-2 d-none d-md-block">
                        	<?php $group = $DB->get_record('vi_skill_groups',array('id'=>$job->skill_group_id));?>
                        		<li>
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillgroupid' => $group->id)); ?>">
                                        <?php echo $group->name; ?>
                                    </a>
                                </li>
                            <?php foreach ($Jskills as $s) : ?>
                                <li>
                                	<?php $skill = $DB->get_record('vi_skills',array('id'=>$s));?>
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                        <?php echo $skill->name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="mb-1">
                            <span class="ic ic-work mr-2"></span>
                            <!-- <span class="t-gray"><?php $sg = $DB->get_record('vi_skill_groups',array('id'=>$skill_groups)); echo $sg->name; ?></span> -->
                        	
                        	<?php $sql = "SELECT wt.*
                                            FROM {vi_job_work_types} jwt
                                            JOIN {vi_work_types} wt ON jwt.work_type_id = wt.id
                                            WHERE jwt.job_id = :jobid";
                            ?>
                            <span class="t-gray">
                        	<?php $p = $DB->get_record('vi_work_experiences',array('id'=>$experiences));?>
                        	<?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; echo " / " ?>
                            
                            <?php foreach ($worktypes as $worktype){?>
                            	<?php $p = $DB->get_record('vi_work_types',array('id'=>$worktype));?>
                            	<?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi; echo  ". " ?>
                            <?php }?>
                            </span>
                        </div>
                        <div class="mb-1">
                            <i class="ic ic-usd mr-2"></i>
                            <span class="text-secondary"><?php echo ($salary1 == "Negotiate") ? get_string('employer:Negotiate', 'local_job') : $salary1."-".$salary2." ".$unit; ?></span>
                            
                        </div>
                        
                        <div class="mb-1">
                            <span class="ic ic-map-marker mr-2"></span>
                            <span class="t-gray cursor-pointer">
                                <?php $sg = $DB->get_record('vi_districts',array('id'=>$area)); echo str_replace("\r\n", "", $sg->name); ?>
                            </span>
                        </div>
                        
                        <div class="mb-1">
                            <span class="ic ic-time mr-2"></span>
                            <span class="t-gray"><?php $wt = $DB->get_record('vi_working_date_categories',array('id'=>$hour)); echo $wt->name; ?></span>
                        </div>
                        <div class="mb-1">
                            <span class="ic ic-calendar-on mr-2"></span>
                            <span class="text-primary"><?php echo date("Y\.m\.d", $job->enddate); ?> <?php echo get_string('DayEnd', 'local_job'); ?></span>
                        </div>

                        <ul class="cate-list text-left mb-2 d-md-none">
                        	<?php $group = $DB->get_record('vi_skill_groups',array('id'=>$job->skill_group_id));?>
                        		<li>
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillgroupid' => $group->id)); ?>">
                                        <?php echo $group->name; ?>
                                    </a>
                                </li>
                                
                            <?php foreach ($job->skills as $s) : ?>
                                <li>
                                    <?php $skill = $DB->get_record('vi_skills',array('id'=>$s));?>
                                    <a href="<?php echo new moodle_url($VISANG->wwwroot . '/search.php', array('skillid' => $skill->id)); ?>">
                                        <?php echo $skill->name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="article-jbox">
                        <?php 
                    	   $sql = "SELECT r.name FROM {vi_reason_categories} r 
                                    JOIN {vi_employer_reasons} er ON r.id=er.reason_category_id	
                                    WHERE er.employer_id=:comid";
                    	   $items = $DB->get_records_sql($sql, array("comid" => $comid));
                    	   ?>
                    	   
                    	<h3><?php echo get_string('FindJob', 'local_job'); ?></h3>   
                        <?php if ($items != null) : ?>
                            
                            <div class="">
                                <?php foreach ($items as $reason) : ?>
                                    <p class="mb-1">
                                        <span>- <?php echo $reason->name; ?></span>
                                    </p>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        
                        
                         <h3 class="pt-4"><?php echo get_string('Responsibilities', 'local_job'); ?></h3>
                        <?php echo $job->description; ?>
                        
                        <h3 class="pt-4"><?php echo get_string('c_Eligibility', 'local_job'); ?></h3>
                        <?php if ($job->final_education_id != 0) : ?>
                        
                            <?php
                            $p = $DB->get_record('vi_final_educations', array('id' => $job->final_education_id));
                            ?>
                            <p class="select-rw">
                                <span class="f16"><?php echo get_string('c_FinalEducation', 'local_job'); ?></span>
                                <span style="display: inline-block;padding: 5px 10px;margin-left: 10px;; border: 1px solid #ddd;">
                                <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi ?>
                                </span>
                            </p>
                            
                        <?php endif; ?>
                        <p class="select-rw">
                            <span class="f16"><?php echo get_string('c_age', 'local_job'); ?></span>
                            <span style="display: inline-block;padding: 5px 10px;margin-left: 10px;; border: 1px solid #ddd;">
                            <?php echo $job->age_from . ' - ' . $job->age_to; ?>
                            </span>
                        </p>
                        <?php echo $job->other_request_info; ?>
                        
                        
                       
                        

                        <h3 class="pt-4"><?php echo get_string('Eligibility', 'local_job'); ?></h3>
                        <?php if ($job->korean_level_id != 0) : ?>
                        
                            <?php
                            $p = $DB->get_record('vi_korean_levels', array('id' => $job->korean_level_id));
                            ?>
                            <p class="select-rw">
                                <span class="f16"><?php echo get_string('KoreanLevel', 'local_job'); ?></span>
                                <span style="display: inline-block;padding: 5px 10px;margin-left: 10px;; border: 1px solid #ddd;">
                                <?php echo ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi ?>
                                </span>
                            </p>
                            
                        <?php endif; ?>
                        <?php  echo $job->other_lang_info; ?>
                          
                            
                        <h3 class="pt-4"><?php echo get_string('Benefits', 'local_job'); ?></h3>
                        <?php echo $company->reason_description ;?>    
                            
                            
                             <?php //echo $job->skills_experience; ?>
                        
                        <?php // echo $job->culture_description.'</br>'; ?>
                        <?php // echo $job->note.'</br>'; ?>
                        
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>
