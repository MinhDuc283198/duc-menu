<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');

global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/job_notices.php');
$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('job');
$sql_common_code="SELECT
            point
            FROM {vi_common_code} com
            WHERE com.code=2001
            ";
$common_code=$DB->get_record_sql($sql_common_code);

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$filter = optional_param('filter', 1, PARAM_INT);
$dowload = optional_param('download', 0, PARAM_INT);
$order = optional_param('order', 0, PARAM_INT);
$runcopy = optional_param('coppyy', 0, PARAM_INT);

if($runcopy == 100){
    $runcopyID = optional_param('coppyyID', 0, PARAM_INT);
    redirect($VISANG->wwwroot . '/employer/copy_job.php?jobid='.$runcopyID);
}

if($dowload != 0) mkcom_export_jobs_notices($comid, $keyword, $datefilter, $filter);

$keyword = optional_param('filter_name', "", PARAM_RAW);
$datefilter = optional_param('filter_date', "", PARAM_RAW);


$total_pages = mkcom_get_totalcount($comid);
$totalWaiting = mkcom_get_totalwaiting($comid);
$totalRunning = mkcom_get_totalrunning($comid);
$totalEnded = mkcom_get_totalended($comid);
//$total_pages = mkcom_get_total_pages($totalcount, $perpage);
$contents = mkcom_get_jobs_notices($comid, $keyword, $datefilter, $filter, $perpage, $page, $order);

$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'menuJobs';
$current_language = current_language();
$VISANG->theme->header_foremployer();
?>
<div class="cont">
	<div class="group">
        <h2 class="pg-tit"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></h2>
        <div class="tab-btn clearfix">
            <div class="btn-area f-r">
                <input type="button" value="<?php echo get_string('Viewtalented', 'local_job'); ?>" class="btns point h50" onclick="window.location.href = '<?php echo $VISANG->wwwroot. '/employer/applicants_recommend.php'?>';"/>
                <input type="button" value="<?php echo get_string('Jobposting', 'local_job'); ?> (<?php echo $common_code->point ?> point)" class="job_posting btns br h50" onclick="pay_point_post_job1()" />
            </div>
            <div class="tp-tb-area f-l">
                <ul class="bar-tab">
                    <li <?php echo ($filter == 1) ? "class=\"on\"" : "";?> ><a href="<?php echo $baseurl."?filter=1"?>"><?php echo get_string('All', 'local_job'). "(".$total_pages.")";?></a></li>
                    <li <?php echo ($filter == 2) ? "class=\"on\"" : "";?>><a href="<?php echo $baseurl."?filter=2"?>"><?php echo get_string('InprogressJobs', 'local_job'). "(".$totalRunning.")"?></a></li>
                    <li <?php echo ($filter == 3) ? "class=\"on\"" : "";?>><a href="<?php echo $baseurl."?filter=3"?>"><?php echo get_string('wait', 'local_job'). "(".$totalWaiting.")"?></a></li>
                    <li <?php echo ($filter == 4) ? "class=\"on\"" : "";?>><a href="<?php echo $baseurl."?filter=4"?>"><?php echo get_string('ended', 'local_job'). "(".$totalEnded.")"?></a></li>
                </ul>
            </div>
        </div>

        <div class="tb-tp">
            <div class="srch-area f-r">
                <form method="POST">
                    <input type="text" placeholder="<?php echo get_string('RecruitmentNotice', 'local_job'); ?>" name="filter_name" value="<?php echo $keyword?>"/>
                    <input type="text" id="searchDate" class="has-datepicker" name="filter_date" autocomplete="off" placeholder="" value="<?php echo ($datefilter != "") ?  $datefilter : "";?>"/>                   
                    <input type="submit" value="<?php echo get_string('search', 'local_job'); ?>" class="btns gray" />
                    <input type="reset" value="<?php echo get_string('reset', 'local_job'); ?>" class="btns br" onclick="window.location.href = '<?php echo $baseurl."?filter=1";?>';"/>
                </form>
            </div>
        
       	     
            <form method="POST" action="<?php echo $baseurl."?filter=".$filter?>">
            <span class="t-blue02 d-block"><?php echo $total_pages. get_string('rowsrow', 'local_job');?></span>
                <select name="perpage" onchange="this.form.submit()">
                    <option value="5" <?php echo ($perpage == 5) ? "selected" : "";?>><?php echo get_string('display5', 'local_job'); ?></option>
                    <option value="10" <?php echo ($perpage == 10) ? "selected" : "";?>><?php echo get_string('display10', 'local_job'); ?></option>
                    <option value="20" <?php echo ($perpage == 20) ? "selected" : "";?>><?php echo get_string('display20', 'local_job'); ?></option>
                    <option value="30" <?php echo ($perpage == 30) ? "selected" : "";?>><?php echo get_string('display30', 'local_job'); ?></option>
                </select>
            </form>
        </div>
        
        <table class="table m-block">
            <thead>
                <tr>
                    <th width="5%"><?php echo get_string('stt', 'local_job'); ?></th>
                    <th width="20%" onclick="window.location.href = '<?php $oval = ($order == 11) ? 10 : 11; echo $baseurl."?filter=".$filter."&page=".$page."&perpage=".$perpage."&filter_name=".$keyword."&filter_date".$datefilter."&order=".$oval;?>';"><?php echo get_string('RecruitmentNotice', 'local_job'); ?><span <?php echo ($order % 10 == 0) ? "class=\"ic-sort down\"" : "class=\"ic-sort\"";?>>sort</span></th>
                    <th width="12%"><?php echo get_string('Recommended', 'local_job'); ?></th>
                    <th width="/"><?php echo get_string('RecommendedTalent', 'local_job'); ?></th>
                    <th width="/"><?php echo get_string('aplicant', 'local_job'); ?></th>
                    <th width="/"><?php echo get_string('Interesting', 'local_job'); ?></th>
                    <th width="/"><?php echo get_string('views', 'local_job'); ?></th>
                    <th width="/" onclick="window.location.href = '<?php $oval = ($order == 71) ? 70 : 71; echo $baseurl."?filter=".$filter."&page=".$page."&perpage=".$perpage."&filter_name=".$keyword."&filter_date".$datefilter."&order=".$oval;?>';"><?php echo get_string('Postdate', 'local_job'); ?><span <?php echo ($order % 10 == 0) ? "class=\"ic-sort down\"" : "class=\"ic-sort\"";?>>sort</span></th>
                    <th width="/" onclick="window.location.href = '<?php $oval = ($order == 81) ? 80 : 81; echo $baseurl."?filter=".$filter."&page=".$page."&perpage=".$perpage."&filter_name=".$keyword."&filter_date".$datefilter."&order=".$oval;?>';"><?php echo get_string('Startdate', 'local_job'); ?><span <?php echo ($order % 10 == 0) ? "class=\"ic-sort down\"" : "class=\"ic-sort\"";?>>sort</span></th>
                    <th width="/" onclick="window.location.href = '<?php $oval = ($order == 91) ? 90 : 91; echo $baseurl."?filter=".$filter."&page=".$page."&perpage=".$perpage."&filter_name=".$keyword."&filter_date".$datefilter."&order=".$oval;?>';"><?php echo get_string('Enddate', 'local_job'); ?><span <?php echo ($order % 10 == 0) ? "class=\"ic-sort down\"" : "class=\"ic-sort\"";?>>sort</span></th>
                    <th width="/" onclick="window.location.href = '<?php $oval = ($order == 101) ? 100 : 101; echo $baseurl."?filter=".$filter."&page=".$page."&perpage=".$perpage."&filter_name=".$keyword."&filter_date".$datefilter."&order=".$oval;?>';"><?php echo get_string('Exposure', 'local_job'); ?><span <?php echo ($order % 10 == 0) ? "class=\"ic-sort down\"" : "class=\"ic-sort\"";?>>sort</span></th>
                    <th width="/"><?php echo get_string('copy', 'local_job'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php
                if ($contents) {
                    foreach ($contents as $content) {?>
                    	<tr>
                            <td><?php echo ($content->startdate < time()) ? ($content->enddate > time())? "Started" : "Ended" : "Wait";?></td>
                            <td class="text-left"><a href="<?php echo new moodle_url($VISANG->wwwroot . '/employer/edit_job.php', array('jobid' => $content->jobid)); ?>"><?php echo $content->title;?></a></td>
                            <td class="m-w100"><?php $skillg = $DB->get_record("vi_skill_groups", array('id'=>$content->skill_group_id)); echo ($skillg->name != '') ? $skillg->name.', ' : '';$skills = mkcom_get_job_skill($content->jobid); foreach ($skills as $skill) echo $skill->name.", ";?></td>
                            <td><span class="m-show inline">지원자</span> <a href="<?php echo new moodle_url($VISANG->wwwroot . '/employer/applicants_recommend.php', array('timer' => $content->jobid));?>"><?php echo mkcom_count_recommend_applicants_jobid($comid, 10, "All", "",$content->jobid);?></a></td>
                            <td><span class="m-show inline">추천인재</span><a href="<?php echo new moodle_url($VISANG->wwwroot . '/employer/applicants.php', array('timer' => $content->jobid));?>"><?php echo mkcom_count_applications($content->jobid);?></a></td>
                            <td><span class="m-show inline">관심설정</span><a href="<?php echo new moodle_url($VISANG->wwwroot . '/employer/applicants_career.php', array('timer' => $content->jobid));?>"><?php echo mkcom_count_career_applicants_jobid($comid, 10, "All", "",$content->jobid);?></a></td>
                            <td><span class="m-show inline">조회수</span><?php echo $content->view_count;?></td>
                            <td><span class="m-show inline">게시일</span> <?php echo date("Y.m.d",$content->timecreated);?></td>
                            <td><span class="m-show inline">마감일</span> <?php echo date("Y.m.d",$content->startdate);?></td>
                            <td><span class="m-show inline">등록일</span> <?php echo date("Y.m.d",$content->enddate);?></td>
                            <td><?php echo ($content->published) ? "Yes" : "No";?></td>
                            <td><form method="POST"><input type="hidden" name="coppyyID" value="<?php echo $content->jobid;?>" class="btns" /><input type="hidden" name="coppyy" value="100" class="btns" /><input type="submit" value="<?php echo get_string('copy', 'local_job'); ?>" class="btns" /></form></td>
                        </tr>
                    <?php }
                }else{?>
                	<tr>
                        <td colspan="12"><?php echo get_string('nodatafound', 'local_job'); ?></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
        <div class="text-right mg-bt10">
            <a href="<?php echo $baseurl."?download=1"?>" class="btns br_blue h50"><?php echo get_string('ExcelDownload', 'local_job'); ?></a>
        </div>
        
        <?php
        
        if (count($contents)) {
            $page_params = array();
            $page_params['comid'] = $comid;
            $page_params['perpage'] = $perpage;
            $page_params['filter_date'] = $datefilter;
            $page_params['filter_name'] = $keyword;
            $page_params['filter'] = $filter;
            $page_params['page'] = $page;
            $VISANG->theme->table_pagination($baseurl, $page_params, ceil($total_pages / $perpage), $page);
        }
        ?>

	</div>
</div>



<script type="text/javascript">
    $(document).ready(function() {

        $("#searchDate").datepicker({
            showOn: this,
            buttonText: 'Show Date',
            buttonImageOnly: true,
            buttonImage: ''
        });

        $("#searchDate").click(function() {
            $("#searchDate").datepicker("show");
        });



    });

    function pay_point_post_job1() {
        $.post("<?php echo $VISANG->wwwroot . '/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkcom_job_noti_manager",
            employer_id: <?php echo $comid?>
        },
            function (data) {
                if (data == 1) {
                    if (confirm("<?php echo get_string('buy-credit-require', 'local_job'); ?>")) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/credit/index.php'; ?>";
                    }
                }else{
                    if (confirm(data)) {
                        window.location.href = "<?php echo $VISANG->wwwroot . '/employer/new_job.php'; ?>";
                    }
                }

            });
    };



</script>
<style>
.tb-tp .srch-area input[type=date]{border: 1px solid #ddd;height: 40px; width: 180px;box-sizing: border-box;font-size: 15px;margin-bottom: 5px;}
.tb-tp .srch-area input[type=date]::placeholder{display: none;}
.tb-tp .srch-area input[type=date]:-ms-input-placeholder{display: none;}
</style>

<?php
$VISANG->theme->footer_foremployer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />


