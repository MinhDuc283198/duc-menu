<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/new_job.php');
$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);
if(!mkcom_company_completed_info($comid)) redirect($VISANG->wwwroot . '/employer/pop_company_info.php');
$context = context_system::instance();

$PAGE->set_context($context);

$jobid = required_param('jobid',PARAM_INT);
$jobedit = mkcom_get_job($jobid);

$Jskills = mkcom_get_job_skill($jobid);
$skills = array();
foreach ($Jskills as $skill) {
    $skills[] = json_encode(array('id' => $skill->id, 'name' => $skill->name));
}

$groups = $DB->get_records('vi_skill_groups', array("id"=>$jobedit->skill_group_id));
$skill_groups = array();
foreach ($groups as $group) {
    $skill_groups[] = json_encode(array('id' => $group->id, 'name' => $group->name));
}


$areas = mkcom_get_districts();
$dates = mkcom_get_working_date();
$times = mkcom_get_working_time();
$experiences = mkcom_get_experiences();
$worktypes = $DB->get_records("vi_work_types",array());
$benefit_types = $DB->get_records("vi_benefit_types", array());
$final_educations = $DB->get_records('vi_final_educations', array());
$korean_levels = $DB->get_records('vi_korean_levels', array(), 'name ASC');

if(!empty($_POST["comid"])){
    $title = $_POST["job_title"]; //Added TITLE
    $responsibility = $_POST["job_responsibility"];//Added RESPONSIBILITIES
    $request = $_POST["job_request"];//Added RESQEST
    $krlevel = $_POST["job_krlevel"]; // Added KORREAN LEVEL
    $olang = $_POST["job_flang"];
    $salary1 = $_POST["job_salaryfrom"];//Added SALARY FROM 1
    $salary2 = $_POST["job_salaryto"];//Added SALARY TO 2
    $unit = $_POST["job_salaryunit"];//Added SALARY UNIT 3 1+2+3
    $exchange = $_POST["job_salaryinter"];//Added
    $area = $_POST["job_workingarea"];//Added WORK AREA => LOCATION
    $adress = $_POST["job_adress"];
    $hour = $_POST["job_workinghour"];
    $hcmt = $_POST["job_hourcmt"];
    $start = $_POST["job_start"];//Added START TIME
    $end = $_POST["job_end"];//Added END TIME
    $skill_groups = $_POST["skill_groups"]; //Added GROUP CATEGORY
    
    $finalEdu = $_POST["job_finalEdu"];  //Added FINAL EDUCATION
    $finalEduFrom = $_POST["job_finalEduFrom"]; //Added
    $finalEduTo = $_POST["job_finalEduTo"]; //Added
    $finalEduInfo = $_POST["job_finalEduInfo"];
    $Jskills = optional_param_array('skills', array(), PARAM_RAW); //Added
    $experiences = optional_param('job_career', 0, PARAM_INT); //Added
    $worktype = optional_param_array('job_worktype', array(), PARAM_INT); //Added
    $benefit_types = optional_param_array('job_benefittype', array(), PARAM_INT); //Added

    if($_POST["job_exposure"] == "0"){
        $exposure = 0; //Added PUBLISH
    }else{
        $exposure = 1; //Added PUBLISH
    }
    
    if($exchange == "1"){
        $salaryID = mkcom_findOrCreate_salaryID("Negotiate", $comid);
    }else{
        $salaryID = mkcom_findOrCreate_salaryID(str_replace(" ","",$salary1)."-".str_replace(" ","",$salary2)." ".$unit, $comid);
    }
    
    $job = $jobedit;
    $job->title = $title;
    $job->description = $responsibility;
    $job->skills_experience = $request;
    $job->employer_id = $comid;
    $job->job_salary_category_id = $salaryID;
    $job->startdate = strtotime($start);
    $job->enddate = strtotime($end);
    $job->skill_group_id = $skill_groups;
    $job->work_experience_id = $experiences;
    $job->korean_level_id= $krlevel;
    $job->final_education_id = $finalEdu;
    $job->age_from = $finalEduFrom;
    $job->age_to = $finalEduTo;
    $job->published = $exposure;
    $job->confirmed = 1;
    $job->timecreated = time();
    
    if(strlen($finalEduInfo) > 2000) $finalEduInfo = substr($finalEduInfo,0, 2000).'...';
    if(strlen($olang) > 2000) $olang = substr($olang,0, 2000).'...';
    if(strlen($adress) > 2000) $adress = substr($adress,0, 2000).'...';
    
    $job->special_info = $hour;    //Save working time ID
    $job->note = $hcmt;             // Save note for working time
    $job->other_request_info = $finalEduInfo; // Save final education info (other request)
    $job->other_lang_info = $olang; // Save other language
    $job->other_address_info = $adress; // Save note address
    
    $DB->update_record('vi_jobs', $job);
    $job_id = $jobedit->id;
    
    $job_skill = new stdClass();
    $job_skill->job_id = $job_id;
    $DB->delete_records('vi_job_skills', array('job_id'=>$job_id));
    
    foreach ($Jskills as $skillid) {
        $job_skill->skill_id = $skillid;
        $DB->insert_record('vi_job_skills', $job_skill);
    }
    
    $job_wt = new stdClass();
    $job_wt->job_id = $job_id;
    $DB->delete_records('vi_job_work_types', array('job_id'=>$job_id));
    foreach ($worktype as $wt) {
        $job_wt->work_type_id = $wt;
        $job_bt->timecreated = time();
        $job_bt->timemodified = time();
        $DB->insert_record('vi_job_work_types', $job_wt);
    }

    $job_bt = new stdClass();
    $job_bt->job_id = $job_id;
    $DB->delete_records('vi_job_benefit_types', array('job_id'=>$job_id));
    foreach ($benefit_types as $bt) {
        $job_bt->benefit_type_id = $bt;
        $job_bt->timecreated = time();
        $job_bt->timemodified = time();
        $DB->insert_record('vi_job_benefit_types', $job_bt);
    }
    
    $comInfo = $DB->get_record("vi_employers", array('id' => $comid));
    $location = $DB->get_record_sql('SELECT * FROM {vi_locations} l WHERE l.full_address =:addd', array('addd'=>$comInfo->company_address));
    if($location == null){
        $location2 = new stdClass();
        $location2->employer_id = $comid;
        $location2->full_address = $comInfo->company_address;
        $location2->district_id = $area;
        $location2->lat = 10.773394520364000;
        $location2->lng = 106.700786738030000;
        $location2->timecreated = time();
        $location2->timemodified = time();
        $location_id = $DB->insert_record('vi_locations', $location2);
    }else{
        $location_id = $location->id;
    }
    $DB->delete_records('vi_job_locations', array('job_id'=>$job_id));
    $job_location = new stdClass();
    $job_location->job_id = $job_id;
    $job_location->location_id = $location_id;
    $DB->insert_record('vi_job_locations', $job_location);
    redirect(new moodle_url($VISANG->wwwroot . '/employer/job_notices.php'));
}
//echo ">>". $baseurl;
// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'menuJobs';
$VISANG->theme->header_foremployer();


$current_language = current_language();
?>

<!--<link rel="stylesheet" type="text/css" href="--><?php //echo $VISANG->wwwroot . '/employer/lib';?><!--/css/bootstrap.css"></link>-->
<link rel="stylesheet" type="text/css" href="<?php echo $VISANG->wwwroot . '/employer/lib';?>/css/prettify.css"></link>
<link rel="stylesheet" type="text/css" href="<?php echo $VISANG->wwwroot . '/employer/lib';?>/bootstrap-wysihtml5.css"></link>

<div class="center-tit"><?php echo get_string('Jobposting', 'local_job'); ?></div>
<form method="POST" id="edit-job-form">
<div class="w-bx-tp">
    <p class="imprt-txt"><?php echo get_string('FieldRequest', 'local_job'); ?>.</p>
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('RecruitmentNotice', 'local_job'); ?></p>
        <input type="text" placeholder="<?php echo get_string('enterJobName', 'local_job'); ?>" class="max-txt not-null" name="job_title" id="job_title" value="<?php echo $jobedit->title;?>"/>
        <span class="byte"><em id="job_title_v"><?php echo strlen($jobedit->title);?></em>/40</span>
    </div>
    
    <div class="rw" id="skill-groups-advtag" v-cloak>
    	<p class="tit hasdot"><?php echo get_string('job_skillgroup', 'local_job'); ?></p>
        <p>
            <span v-for="(item, index) in items" class="txt-bx">
                {{ item.name }}
                <a @click="removeItem(index)" href="#" class="ic-close">close</a>
            </span>
            <a v-if="maxitems < 0 || (maxitems >=0 && items.length < maxitems)" href="#" @click="add" class="btns ic-plus openPop"><?php echo get_string('skill_add', 'local_job'); ?></a>
        </p>
        <div class="d-none">
            <input type="hidden" v-for="(item,i) in items" :key="i" :name="elid" :value="item.id">
        </div>
    </div>
    
    <div class="rw" id="skills-advtag" v-cloak>
    	<p class="tit hasdot"><?php echo get_string('job_skill', 'local_job'); ?></p>
        <p>
            <span v-for="(item, index) in items" class="txt-bx">
                {{item.name}} Value
                <a @click="removeItem(index)" href="#" class="ic-close">close</a>
            </span>
            <a v-if="maxitems < 0 || (maxitems >=0 && items.length < maxitems)" href="#" @click="add" class="btns ic-plus openPop"><?php echo get_string('skill_add', 'local_job'); ?></a>
        </p>
        <div class="d-none">
            <input type="hidden" v-for="(item,i) in items" :key="i" :name="elid+'[]'" :value="item.id">
        </div>
    </div>
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('Responsibilities', 'local_job'); ?></p>
        <textarea class="not-null" id="job_responsibility" value="" placeholder="<?php echo get_string('detailedtask', 'local_job'); ?>" name="job_responsibility"><?php echo $jobedit->description;?></textarea>
    </div>
    
    <div class="rw" hidden>
        <p class="tit"><?php echo get_string('job_request', 'local_job'); ?></p>
        <textarea value="" id="job_request" placeholder="<?php echo get_string('job_request_v', 'local_job'); ?>" name="job_request" ><?php echo $jobedit->skills_experience;?></textarea>
    </div>
    
    <div class="rw">
        <p class="tit"><?php echo get_string('c_Eligibility', 'local_job'); ?></p>
        <div class="select-rw">
            <span class="f16"><?php echo get_string('c_FinalEducation', 'local_job'); ?></span>
            <select class="under" name="job_finalEdu">
                <option value="" ><?php echo get_string('my_inter:final_education_desc', 'local_job'); ?></option>
                <?php foreach ($final_educations as $final_education) : ?>
                    <option value="<?php echo $final_education->id; ?>" <?php echo ($jobedit->final_education_id == $final_education->id) ? 'selected' : '';?>>
                        <?php
                        switch ($current_language) {
                            case 'ko':
                                echo $final_education->name_ko;
                                break;
                            case 'en':
                                echo $final_education->name_en;
                                break;
                            case 'vi':
                                echo $final_education->name_vi;
                                break;
                            default:
                                echo $final_education->name;
                                break;
                        }
                        ?>
                    </option>
                <?php endforeach; ?>
            </select>
           <!-- <span><?php echo get_string('c_end', 'local_job'); ?></span>  --> 
        </div>
        <div class="select-rw">
            <span class="f16"><?php echo get_string('c_age', 'local_job'); ?></span>
            <input type="text"class="w100px" name="job_finalEduFrom" value="<?php echo $jobedit->age_from;?>"/>
<!--             <span>세</span> -->
            <span>~</span>
            <input type="text" class="w100px" name="job_finalEduTo" value="<?php echo $jobedit->age_to;?>"/>
<!--             <span>세</span> -->
        </div>
        <textarea id="job_finalEduInfo" placeholder="<?php echo get_string('c_blablabla', 'local_job'); ?>" name="job_finalEduInfo"><?php echo $jobedit->other_request_info;?></textarea>
    </div>
    
    <div class="rw">
        <p class="tit"><?php echo get_string('Eligibility', 'local_job'); ?></p>
        <p class="select-rw">
            <span class="f16"><?php echo get_string('KoreanLevel', 'local_job'); ?></span>

            <select class="under" name="job_krlevel">

                <?php foreach ($korean_levels as $korean_level) : ?>
                    <option value="<?php echo $korean_level->id; ?>" <?php echo ($jobedit->korean_level_id == $korean_level->id) ? 'selected' : '';?>>
                        <?php
                        switch ($current_language) {
                            case 'ko':
                                echo $korean_level->name_ko;
                                break;
                            case 'en':
                                echo $korean_level->name_en;
                                break;
                            case 'vi':
                                echo $korean_level->name_vi;
                                break;
                            default:
                                echo $korean_level->name;
                                break;
                        }
                        ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </p>
        <textarea id="job_flang"  placeholder="<?php echo get_string('job_ortherLang', 'local_job'); ?>" name="job_flang"><?php echo $jobedit->other_lang_info;?></textarea>
    </div>
    
    <div class="sub-tit"><?php echo get_string('Workingcondition', 'local_job'); ?></div>
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('job_work', 'local_job'); ?></p>
        <div class="rd-group">
        	<?php foreach ($experiences as $experience) : ?>
            	<label>
                    <input type="radio" name="job_career" value="<?php echo $experience->id;?>" <?php echo ($jobedit->work_experience_id == $experience->id) ? 'checked' : '';?>/>
                    <span>
                    <?php
                        switch ($current_language) {
                            case 'ko':
                                echo $experience->name_ko;
                                break;
                            case 'en':
                                echo $experience->name_en;
                                break;
                            case 'vi':
                                echo $experience->name_vi;
                                break;
                            default:
                                echo $experience->name;
                                break;
                        }
                        ?>
                    </span>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
    
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('Worktype', 'local_job'); ?></p>
        <div class="ck-group">
        	<?php foreach ($worktypes as $wt) : ?>
            	<label class="custom-ck">
                    <input type="checkbox" name="job_worktype[]" value="<?php echo $wt->id;?>" <?php echo (count($DB->get_records('vi_job_work_types',array('job_id'=>$jobedit->id,'work_type_id'=>$wt->id)))==1) ? 'checked' : '';?>/>

                    <span>
                    <?php
                        switch ($current_language) {
                            case 'ko':
                                echo $wt->name_ko;
                                break;
                            case 'en':
                                echo $wt->name_en;
                                break;
                            case 'vi':
                                echo $wt->name_vi;
                                break;
                            default:
                                echo $wt->name;
                                break;
                        }
                        ?>
                    </span>
                </label>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('benefit_type', 'local_job'); ?></p>
        <div class="ck-group">
            <?php foreach ($benefit_types as $wt) : ?>
                <label class="custom-ck">
                    <input type="checkbox" name="job_benefittype[]" value="<?php echo $wt->id;?>" <?php echo (count($DB->get_records('vi_job_benefit_types',array('job_id'=>$jobedit->id,'benefit_type_id'=>$wt->id)))==1) ? 'checked' : '';?>/>

                    <span>
                    <?php
                    switch ($current_language) {
                        case 'ko':
                            echo $wt->name_ko;
                            break;
                        case 'en':
                            echo $wt->name_en;
                            break;
                        case 'vi':
                            echo $wt->name_vi;
                            break;
                        default:
                            echo $wt->name;
                            break;
                    }
                    ?>
                    </span>
                </label>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('Salary', 'local_job'); ?></p>
        <div class="pay">
            <input class="format-number"  type="text" name="job_salaryfrom" value="<?php $sala = $DB->get_record('vi_job_salary_categories', array('id'=>$jobedit->job_salary_category_id)); echo mkcom_getSalaryFrom($sala->name);?>"/>
            <span>~</span>
            <input class="format-number"  type="text" class="under" name="job_salaryto" value="<?php echo mkcom_getSalaryTo($sala->name); $salaUnit = mkcom_getSalaryUnit($sala->name);?>"/>
            <select name="job_salaryunit">
                <option value="USD" <?php echo ($salaUnit=="USD") ? "selected": "";?>>USD</option>
                <option value="VND" <?php echo ($salaUnit=="VND") ? "selected": "";?>>VND</option>
                <option value="WON" <?php echo ($salaUnit=="WON") ? "selected": "";?>>WON</option>
            </select>
            <label class="custom-ck">
                <input type="checkbox" name="job_salaryinter" value="1" <?php echo ($sala == "" || $sala == " ") ? "checked" : "" ;?>/>
                <span><?php echo get_string('exchangeInInterview', 'local_job'); ?></span>
            </label>
        </div>
    </div>
    
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('job_workarea', 'local_job'); ?></p>
        <select class="f-l" name="job_workingarea"> <!-- design class is mg-bt10 -->
            <?php foreach ($areas as $area) : ?>
            	<option value="<?php echo $area->id;?>" <?php echo (count($DB->get_records('vi_job_locations',array('job_id'=>$jobedit->id,'location_id'=>$area->id)))==1) ? 'selected' : '';?> ><?php echo $area->name;?></option>
            <?php endforeach; ?>
        </select>
        <input type="text" class="w100" value="<?php echo $jobedit->other_address_info;?>" placeholder="<?php echo get_string('Enterstreetaddress', 'local_job'); ?>" name="job_adress"/>
    </div>
    
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('job_worktime', 'local_job'); ?></p>
        <select class="" name="job_workinghour">
            <?php foreach ($dates as $time) : ?>
            	<option value="<?php echo $time->id;?>" <?php echo ($jobedit->special_info == $time->id) ? "selected" : "";?>><?php echo $time->name;?></option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" value="<?php echo $jobedit->note;?>" placeholder="<?php echo get_string('Enterifcommentisneeded', 'local_job'); ?>" name="job_hourcmt"/>
    </div>
    
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('Receptionperiod', 'local_job'); ?></p>
        <input type="text" class="has-datepicker not-null" name="job_start" id="job_start" autocomplete="off" value="<?php echo date('Y-m-d',$jobedit->startdate)?>"/>
        <span class="dash">-</span>
        <input type="text" class="has-datepicker not-null" name="job_end" id="job_end" autocomplete="off" value="<?php echo date('Y-m-d',$jobedit->enddate)?>"/>
    </div>
    
    <div class="rw">
        <p class="tit hasdot"><?php echo get_string('Publishing', 'local_job'); ?></p>
        <div class="rd-group">
            <label>
                <input type="radio" name="job_exposure" value="1" <?php echo ($jobedit->published == 1) ? 'checked' : '';?>/>
                <span><?php echo get_string('Exposure', 'local_job'); ?></span>
            </label>
            <label>
                <input type="radio" name="job_exposure" value="0" <?php echo ($jobedit->published == 0) ? 'checked' : '';?>/>
                <span><?php echo get_string('Nopublishing', 'local_job'); ?></span>
            </label>
        </div>
    </div>
</div>
<div class="btn-area text-center">
	<input type="hidden" name="comid" value="<?php echo $comid?>"/>
	<input type="button" value="<?php echo get_string('employer:reviewJob', 'local_job'); ?>" class="btns h60" id="reviewJob" onclick="reviewPostJob();" />
    <input type="button" value="<?php echo get_string('cancel', 'local_job'); ?>" class="btns h60" onclick="window.location.href = '<?php echo $VISANG->wwwroot . '/employer/job_notices.php';?>';" />
    <input type="button" value="<?php echo get_string('Save', 'local_job'); ?>" class="btns point h60" onclick="check_notnull();"/>
</div>
</form>

<script>
    function VisangQuickFormNewJob_advtag_mixin() {
        return {
            data: {
                elid: "_VisangQuickFormNewJob_advtag",
                items: [],
                url: "<?php echo $VISANG->wwwroot . '/employer/pop_skill.php'; ?>",
                sesskey: "<?php echo sesskey(); ?>",
                maxitems: -1
            },
            methods: {
                addItem: function(item) {
                    if (this.maxitems >= 0 && this.items.length == this.maxitems) return;
                    var isexist = false;
                    for (var i = 0; i < this.items.length && !isexist && item != undefined; i++) {
                        if (this.items[i].id == item.id) {
                            isexist = true;
                        }
                    }

                    if (item != undefined && !isexist) {
                        this.items.push(item);
                    }
                },
                removeItem: function(index) {
                    this.items.splice(index, 1);
                },
                add: function() {
                    var items = this.items;
                    utils.popup.call_layerpop(this.url, {
                        "width": "700px",
                        "height": "auto",
                        "callbackFn": function() {
                            for (var i = 0; i < items.length; i++) {
                                $("input[name='pop_inter_chk[]'][value='" + items[i].id + "']").prop('checked', true);
                            }
                        }
                    }, {
                        elid: this.elid,
                        sesskey: this.sesskey
                    });
                }
            }
        };
    }

    function btnAddAction(action) {
        $("input[name='pop_inter_chk[]']:checked").each(function() {
            window[action + '_advtag'].addItem({
                id: $(this).val(),
                name: $(this).data('name')
            });
        });
        $('.popbg').click(); //Close popup
    }    

    var skill_groups_advtag = null;
    var skills_advtag = null;
    $(function() {
        skill_groups_advtag = new Vue({
            el: "#skill-groups-advtag",
            mixins: [VisangQuickFormNewJob_advtag_mixin()],
            data: {
                elid: "skill_groups",
                items: [<?php echo implode(',', $skill_groups); ?>],
                maxitems: 1,
            }
        });

        skills_advtag = new Vue({
            el: "#skills-advtag",
            mixins: [VisangQuickFormNewJob_advtag_mixin()],
            data: {
                elid: "skills",
                items: [<?php echo implode(',', $skills); ?>],
                maxitems: 3,
            }
        });
    });

    $("body").addClass("bgblue");

    $("#job_title").keyup(function(){
    	  var content = $("#job_title").val();
    	  var len = content.length;
    	  $("#job_title_v").html(len);
    	  if(len > 39){
    		  $("#job_title").val(content.slice(0, -1));
    	  }
    });

    function check_notnull(){
		var items = $(".not-null");
		var error = 0;
		for(var i=0; i<items.length; i++){
			var item = items[i];
			if(item.value =="" || item.value == " "){ 
				item.classList.add("error-field");
				error++;
			}
		}
		if(error==0) $("#edit-job-form").submit();
	}

    $(".format-number").keyup(function(event){
    	var val = this.value;
        val = val.replace(/,/g, "");
        var count = 0;
        for(var i = val.length-1; i>0; i--){
			if((val.length-i-count) % 3 == 0){
				val= val.slice(0, i) + "," + val.slice(i);
				count++;
				i = i-count;
			}
		}
		this.value = val;
    });

    $(".format-number").keydown(function(event){
    	var key = window.event ? event.keyCode : event.which;
        if (key === 8 || key === 190 || key === 191) {
            return true;
        } else if (key >95 && key < 105) {
            return true;
        } else if (key < 46 || key > 57) {
            return false;
        } else {
            return true;
        }
     });

    function reviewPostJob(){
    	$.ajax({
    		url: '<?php echo $VISANG->wwwroot . '/employer/review_job.php';?>',
            type: 'post',
            data: $('form').serialize(),
            success: function (data) {
              //alert(data);
              var newWindow = utils.popup.call_windowpop("", 1400, 800, "job-pop");
              newWindow.document.write(data);
            }
          });
    }

</script>


<script src="<?php echo $VISANG->wwwroot . '/employer/lib';?>/js/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $VISANG->wwwroot . '/employer/lib';?>/js/prettify.js"></script>
<script src="<?php echo $VISANG->wwwroot . '/employer/lib';?>/bootstrap-wysihtml5.js"></script>
<script>
	$('#job_responsibility').wysihtml5();
	$('#job_request').wysihtml5();
	$('#job_finalEduInfo').wysihtml5();
	$('#job_flang').wysihtml5();
</script>


<style>
.w-bx-tp .rw input[type=date]{border: 1px solid #ddd;height: 50px;box-sizing: border-box;font-size: 15px;}
.w-bx-tp .rw input[type=date]::placeholder{color: #ccc;}
.w-bx-tp .rw input[type=date]:-ms-input-placeholder{color: #ccc;}
.w-bx-tp .rw select.f-l{width: 200px;margin-right: 10px;}
.w-bx-tp .rw select.f-l + input[type=text]{width: calc(100% - 210px);}
.chk-label-group{height: 300px;overflow-y: scroll;}
</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {

    $("#job_start").datepicker({
        showOn: this,
        buttonText: 'Show Date',
        buttonImageOnly: true,
        buttonImage: '',
        dateFormat: 'yy-mm-dd'
    });

    $("#job_start").click(function() {
        $("#job_start").datepicker("show");
    });

    $("#job_end").datepicker({
        showOn: this,
        buttonText: 'Show Date',
        buttonImageOnly: true,
        buttonImage: '',
        dateFormat: 'yy-mm-dd'
    });

    $("#job_end").click(function() {
        $("#job_end").datepicker("show");
    });
});
</script>

<?php
$VISANG->theme->footer_foremployer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
