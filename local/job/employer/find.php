<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/find.php');
$context = context_system::instance();

$step = required_param('step', PARAM_INT);
$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'jobs';
$VISANG->theme->header();

$company_name = optional_param('c_comname', "", PARAM_RAW);
$itax = optional_param('c_itax', "", PARAM_RAW);

//$com = $DB->get_record("vi_employers", array("company_name"=>$username));

$employe = $DB->get_record("vi_employers", array("company_name"=>$company_name));
$admin = $DB->get_record("vi_admin_employers", array("employer_id"=>$employe->id));
$com = $DB->get_record("user", array("id"=>$admin->user_id, "phone2"=>$itax));
$errormsg = "";
if(($com == null || $employe == null) && ($itax != "" || $company_name !="")){
    $step = 1;
    $errormsg = "Sorry ! Can not find your account with the information you registered !";
}else if($com->phone2 == $itax && $employe->company_name == $company_name && $com->phone2 !="" && $employe->company_name != ""){
    $step = 2;   
}else{
    $step = 1;
}

echo $VISANG->theme->header_foremployer();
?>

<?php if($step == 1) { ?>
<div class="login-bx">
    <div class="bg">
        <div class="center-tit"><?php echo get_string('c_CompanyAccount', 'local_job'); ?></div>
        <p class="center-txt"><?php echo get_string('c_accountwiththeinformation', 'local_job'); ?>.</p>
    </div>
    <form class="email-bx join" method="POST">
            <p>
                <strong><?php echo get_string('Companyname', 'local_job'); ?></strong>
                <input type="text" name="c_comname" placeholder="<?php echo get_string('c_Entercompanyname', 'local_job'); ?>" value=""/>
            </p>
            <p>
                <strong><?php echo get_string('c_CompanyRegistration', 'local_job'); ?></strong>
                <input type="text" name="c_itax" placeholder="0000000000" value=""/>
            </p>
            <p><span class="text-danger" id="emailwarning"><?php echo $errormsg;?></span></p>
    
       
        <div class="text-center btn-area">
            <input type="submit" value="<?php echo get_string('Confirm', 'local_job'); ?>" class="btns point big" />
        </div>
    </form>
</div>
<?php } elseif($step == 2){?>

<div class="login-bx">
    <div class="bg">
        <div class="center-tit"><?php echo get_string('c_CompanyAccount', 'local_job'); ?></div>
        <p class="center-txt"><?php echo get_string('c_accountwiththeinformation', 'local_job'); ?></p>
    </div>
    <div class="fd-u-info">
        <div>
            <strong><?php echo get_string('c_EmailID', 'local_job'); ?></strong>
            <span><img src="/theme/oklassedu/pix/images/icon_mail.png" alt="" /><?php echo $com->email;?></span>
        </div>
        <div>
            <strong><?php echo get_string('LoginPass', 'local_job'); ?></strong>
            <a href="#" onclick="send_email(<?php echo $com->id;?>)" class="btns br"><?php echo get_string('c_temporarypassword', 'local_job'); ?></a>
            <p class="warning"><?php echo get_string('c_yourtemporarypassword', 'local_job'); ?></p>
        </div>

    </div>
</div>
<?php }?>
<script type="text/javascript">
    $("body").addClass("bgblue");
    
</script>

<script>
    function send_email(userid) {
        openwaitpopup();
        $.ajax({
            url: "/local/signup/send_tempmail.php",
            type: 'post',
            dataType: 'json',
            data: {
                userid: userid,
                redirecttosite: 'jobs'
            },
            success: function(result) {
                closewaitpopup();
                alert(result.text);
                location.href = "<?php echo $VISANG->wwwroot . '/employer/login.php' ?>";
            }
        });
    }
</script>

<?php
$VISANG->theme->footer_foremployer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

<div class="d-none" id="popup_wait" style="width:350px; height:140px; background: #fff;border: 1px solid #ccc;position: fixed;top: 50%;left: 50%; -webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);">
    <div class="pop-title" style="background: #485cc7;">
        <a href="#" onclick="closewaitpopup();"></a><a href="#" class="pop-close" onclick="closewaitpopup();">닫기</a>
    </div>
    <div class="pop-contents" style=" height:80px;text-align: center; ">
        <p><?php echo get_string("WaitPass", "local_job") ?></p>
    </div>
</div>

<script>
    var closewaitpopup = function() {
        $("#popup_wait").addClass("d-none");
        $("#popup_wait").removeClass("d-block");
    }

    var openwaitpopup = function() {
        $("#popup_wait").removeClass("d-none");
        $("#popup_wait").addClass("d-block");
    }
</script>
