<?php
require_once(dirname(__FILE__) . '/../lib.php');
$PAGE->set_url('/local/management/term/personalinfo.php');
$fbstatus = optional_param('fbstatus', 0, PARAM_INT);
$category = optional_param('category', 1, PARAM_INT);
$sql = "select *from {notice_board} where category= $category and delflag=0 and fbstatus = $fbstatus order by id desc";
$boardlist = $DB->get_records_sql($sql);
$PAGE->set_pagelayout("popup");
echo $OUTPUT->header_foremployer();
if($fbstatus == 0){
    $title=  get_string('service1','local_management');
} else if($fbstatus == 1){
    $title=  get_string('service2','local_management');
} else {
    $title=  get_string('service3','local_management');
}

?>

<!DOCTYPE html>
<html  dir="ltr" lang="ko" xml:lang="ko">
    <head>
        <title><?php echo $title;?></title>
        <link rel="shortcut icon" href="" />
        <!-- config 파일에서 정의한 css및 js 뿌리는곳 -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple.css" />
        <script type="text/javascript" src='/theme/oklassedu/javascript/jquery-ui.min.js'></script>
        <script type="text/javascript" src='/theme/oklassedu/javascript/theme.js'></script>
        <script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script>

        <script type="text/javascript">
            function personalinfoclose() {
                if (sessionStorage.getItem("app") == 'Y') {
                    utils.closePopup();
                } else {
                    self.close();
                }
            }
            $(document).ready(function () {
                //탭 이벤트
                $(".tab-area ul li a").click(function () {
                    var txt = $(this).text();
                    $("#pop_head h1").text(txt);//팝업타이틀 변경

                    $(this).parent().addClass("active");
                    $(this).parent().siblings().removeClass("active");

                    var target = $(this).attr("href");
                    $(target).addClass("active");
                    $(target).siblings().removeClass("active");
                    return false;
                });
            });
        </script>
    </head><body>
        <div class="body_pop">
            <div id="pop_news_wrap">
                <div id="pop_head">
                     Master Korean <?php echo get_string('term','local_management');?>
                    <p class="pop_close">
                        <a href="javascript:personalinfoclose()"><?php echo get_string('close','local_management');?></a>
                    </p>
                </div> 
                <div id="pop_content1"> 
                <ul class="mk-c-tab tab-event">
                        <li <?php if($fbstatus == 1){echo 'class="on"';} ?>><a href="/local/job/employer/view_agree.php?fbstatus=1&category=2"><?php echo get_string('c_TermsofUse', 'local_job'); ?></a></li>
                        <li <?php if($fbstatus == 2){echo 'class="on"';} ?>><a href="/local/job/employer/view_agree.php?fbstatus=2&category=2"><?php echo get_string('c_regulations', 'local_job'); ?></a></li>
                        <li <?php if($fbstatus ==0){echo 'class="on"';} ?>><a href="/local/job/employer/view_agree.php?fbstatus=0&category=2"><?php echo get_string('c_Privacypolicy', 'local_job'); ?></a></li>
                    </ul>
                    <div id="contents1"> 

                    </div>
                    <div class="tab-cont-area">
                       <?php $i = 0;
                        foreach($boardlist as $board){
                             switch (current_language()){
                                case 'ko' :
                                    $content_g = $board->content;
                                    break;
                                case 'en' :
                                    $content_g = $board->contenten;
                                    break;
                                case 'vi' :
                                    $content_g = $board->contentvi;
                                    break;
                            }
                            $class = "";
                            if($i==0){
                                $class = "active";
                            }
                           ?>
                            <div id="data<?php echo $board->id; ?>" class="<?php echo $class;?>">
                                <div class="policy_Wrap" >
                                <?php echo $content_g; ?>
                                </div>    
                            </div>
                            <?php   $i++;
                       }   ?>
                    </div>
                    <div class="tab-area">
                        <ul>
                             <?php $i = 0;
                             foreach($boardlist as $board){
                                  switch (current_language()){
                                case 'ko' :
                                    $title = $board->title;
                                    break;
                                case 'en' :
                                    $title = $board->titleen;
                                    break;
                                case 'vi' :
                                    $title = $board->titlevi;
                                    break;
                            }
                                  $class = "";
                                    if($i==0){
                                        $class = "active";
                                    }
                           ?>
                            <li  class="<?php echo $class;?>"><a href="#data<?php echo $board->id;?>">     <?php echo $title; ?></a></li>
                            <?php   $i++;
                       } ?>
                        </ul>
                    </div>
                </div>

                <div id="pop_footer">
                    <p class="pop_close">
                      
                    </p>
                </div>

                <!-- //contents -->
            </div>
        </div>
    </body>

<?php
echo $OUTPUT->footer_foremployer();
?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
