<?php
//defined('MOODLE_INTERNAL') || die();

function mkcom_require_login()
{
    global $CFG, $PAGE, $SESSION, $VISANG;
    if (!isloggedin()) {
        $SESSION->wantsurl = $PAGE->url;
        redirect($VISANG->wwwroot . '/employer/login.php');
    }
}

function mkcom_get_userID(){
    mkcom_require_login();
    global $USER;
    foreach ($USER as $id) {
        return $id;
    }
}

function mkcom_get_companyID($userID){
    global $DB, $VISANG, $USER;
    $sql = "SELECT ad.employer_id as val
    FROM {vi_admin_employers} ad
    WHERE ad.user_id = :id";
    $id = $DB->get_record_sql($sql, array('id' => $userID));
    if($id != null){
//         if(!mkcom_company_completed_info($id->val)) redirect($VISANG->wwwroot . '/employer/info.php?step=1');
        if(!mkcom_company_denied($id->val)){
            $authsequence = get_enabled_auth_plugins(); // auths, in sequence
            foreach ($authsequence as $authname) {
                $authplugin = get_auth_plugin($authname);
                $authplugin->logoutpage_hook();
            }
            require_logout();
            redirect($VISANG->wwwroot . '/employer/denied.php');
        }
        if(!mkcom_company_comfirmed($id->val)){
            $authsequence = get_enabled_auth_plugins(); // auths, in sequence
            foreach ($authsequence as $authname) {
                $authplugin = get_auth_plugin($authname);
                $authplugin->logoutpage_hook();
            }
            require_logout();
            redirect($VISANG->wwwroot . '/employer/waiting-confirm.php');
        }
        return $id->val;
    }
    redirect($VISANG->wwwroot . '/employer/join.php?step=1');
}

function mkcom_company_completed_info($comid){
    $company = mkcom_get_company_info($comid);
    if($company->company_description == "") return false;
    if($company->company_address == "") return false;
    if($company->company_type_id == 0) return false;
    if($company->company_size_id == 0) return false;
    if($company->country_id == 0) return false;
    //if($company->working_date_category_id == 0) return false;
    //if($company->overtime_category_id == 0) return false;
    $reasons = mkcom_get_employer_reasons($comid);
    if(count($reasons) < 1) return false;
    return true;
}

function mkcom_company_published($comid){
    $company = mkcom_get_company_info($comid);
    if($company->published == 0) return false;
    return true;
}

function mkcom_company_comfirmed($comid){
    $company = mkcom_get_company_info($comid);
    if($company->confirmed == 0 || $company->confirmed == -1) return false;
    return true;
}

function mkcom_company_denied($comid){
    $company = mkcom_get_company_info($comid);
    if($company->confirmed == -1) return false;
    return true;
}

function mkcom_get_company_info($comid)
{
    global $DB;
    $company = $DB->get_record('vi_employers', array('id' => $comid));
    if ($company == null) {
        throw new moodle_exception('notfound');
    }

    $company->company_type = $DB->get_record('vi_company_types', array('id' => $company->company_type_id), 'id,name,short_name');
    $company->country = $DB->get_record('vi_countries', array('id' => $company->country_id), 'id,name,short_name');

    return $company;
}

function mkcom_get_reasons($comid){
    global $DB;
    $reasons = $DB->get_records('vi_reason_categories', array('employer_id' => $comid));
    return $reasons;
}

function mkcom_get_employer_reasons($comid)
{
    global $DB;
    $sql = "SELECT cat.*
    FROM {vi_reason_categories} cat
    JOIN {vi_employer_reasons} e ON cat.id = e.reason_category_id
    WHERE e.employer_id = :employerid
    ORDER BY cat.name ASC";
    return $DB->get_records_sql($sql, array('employerid' => $comid));
}

function mkcom_set_company_reason($reasonid, $comid){
    $com_reason = new stdClass();
    $com_reason->reason_category_id = $reasonid;
    $com_reason->employer_id = $comid;
    $DB->insert_record('vi_employer_reasons', $com_reason);
}

function mkcom_findOrCreate_reasonCaregoryID($name, $comid){
    global $DB;
    $reason = $DB->get_record('vi_reason_categories',array('name' => $name, 'employer_id' => $comid));
    if ($reason == null) {
        $reason = new stdClass();
        $reason->name = $name;
        $reason->employer_id = $comid;
        return $DB->insert_record('vi_reason_categories', $reason);
    }
    return $reason->id;
}

function mkcom_findOrCreate_salaryID($name, $comid)
{
    global $DB;
    $salary = $DB->get_record('vi_job_salary_categories', array('name' => $name, 'employer_id' => $comid));
    if ($salary == null) {
        $salary = new stdClass();
        $salary->name = $name;
        $salary->employer_id = $comid;
        return $DB->insert_record('vi_job_salary_categories', $salary);
    } else return $salary->id;
}

function mkcom_get_company_size(){
    global $DB;
    $company_size =  $DB->get_records('vi_company_sizes', array(), 'sortorder ASC');
    return $company_size;
}

function mkcom_get_districts(){
    global $DB;
    $districts =  $DB->get_records('vi_districts', array(), 'name ASC');
    return $districts;
}

function mkcom_get_working_date(){
    global $DB;
    $works =  $DB->get_records('vi_working_date_categories', array(), 'sortorder ASC');
    return $works;
}

function mkcom_get_working_time(){
    global $DB;
    $works =  $DB->get_records('vi_overtime_categories', array(), 'sortorder ASC');
    return $works;
}

function mkcom_get_krea_levels(){
    global $DB;
    $krs =  $DB->get_records('vi_korean_levels', array(), 'sortorder ASC');
    return $krs;
}

function mkcom_get_experiences(){
    global $DB;
    $krs =  $DB->get_records('vi_work_experiences', array(), 'sortorder ASC');
    return $krs;
}

function mkcom_save_logo($id,$comid, $data){
//     $info = pathinfo($data['com_logo']['name']);
//     $ext = $info['extension'];
//     $newname = time().'.'.$ext;
//     $target = $id.'/local_visang/images/'.$comid.'/logo/'.$newname;
//     move_uploaded_file($data['com_logo']['tmp_name'], $target);
//     return $target;    
    $avatar = $_FILES['com_logo'];
    $avatar_path = '';
    if ($avatar['name'] != '') {
        $fs = get_file_storage();
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $avatar['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $comid
        );
        $fs->create_file_from_pathname($file_record, $avatar['tmp_name']);

        $avatar_path = implode('/', array($id, 'local_visang', 'images', $itemid, $avatar['name']));
        return $avatar_path;
    }
}
function mkj_save_avatar($id,$userid, $data){
    $avatar = $_FILES['avatar'];
    $avatar_path = '';
    if ($avatar['name'] != '') {
        $fs = get_file_storage();
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $avatar['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $userid
        );
        $fs->create_file_from_pathname($file_record, $avatar['tmp_name']);

        $avatar_path = implode('/', array($id, 'local_visang', 'images', $itemid, $avatar['name']));
        return $avatar_path;
    }
}
function mkcom_save_certificate($id,$comid, $data){
//     $info = pathinfo($data['c_file']['name']);
//     $ext = $info['extension'];
//     $newname = time().'.'.$ext;
//     $target = $id.'/local_visang/images/'.$comid.'/certificate/'.$newname;
//     move_uploaded_file($data['c_file']['tmp_name'], $target);
//     return $target;

    $avatar = $_FILES['c_file'];
    $avatar_path = '';
    if ($avatar['name'] != '') {
        $fs = get_file_storage();
        $itemid = time();

        $file_record = array(
            'contextid' => $id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $avatar['name'],
            'timecreated' => time(),
            'timemodified' => time()
        );
        $fs->create_file_from_pathname($file_record, $avatar['tmp_name']);

        $avatar_path = implode('/', array($id, 'local_visang', 'images', $itemid, $avatar['name']));
        return $avatar_path;
    }
}
function mkcom_copy_job($jobid){
    global $DB,$VISANG;
    $job = $DB->get_record("vi_jobs", array("id" => $jobid));
    $job->id = "";
    $job->view_count = 0;
    $newID = $DB->insert_record("vi_jobs", $job);
    $items = $DB->get_records("vi_job_skills", array("job_id" => $jobid));
    foreach ($items as $item){
        $item->id= "";
        $item->job_id= $newID;
        $DB->insert_record("vi_job_skills", $item);
    }
    $items = $DB->get_records("vi_job_work_types", array("job_id" => $jobid));
    foreach ($items as $item){
        $item->id= "";
        $item->job_id= $newID;
        $DB->insert_record("vi_job_work_types", $item);
    }
    $items = $DB->get_records("vi_job_reasons", array("job_id" => $jobid));
    foreach ($items as $item){
        $item->id= "";
        $item->job_id= $newID;
        $DB->insert_record("vi_job_reasons", $item);
    }
    $items = $DB->get_records("vi_job_locations", array("job_id" => $jobid));
    foreach ($items as $item){
        $item->id= "";
        $item->job_id= $newID;
        $DB->insert_record("vi_job_locations", $item);
    }
    redirect($VISANG->wwwroot . '/employer/edit_job.php?jobid='.$newID);
}

function mkcom_job_published($comid){
    global $DB;
    $sql = "SELECT COUNT(*) as val 
    FROM {vi_jobs} job
    WHERE job.employer_id = :employerid AND job.published=1";
    return $DB->get_record_sql($sql, array('employerid' => $comid));
}

function mkcom_count_job_views($comid){
    global $DB;
    $sql = "SELECT SUM(job.view_count) as val
    FROM {vi_jobs} job
    WHERE job.employer_id = :employerid";// AND job.published=1";
    $val = $DB->get_record_sql($sql, array('employerid' => $comid));
    if($val->val == null) return 0;
    return $val->val;
}

function mkcom_count_applicants($comid){
    global $DB;
    $sql = "SELECT COUNT(*) as val
    FROM {vi_jobs} job
    JOIN {vi_job_applications} ja ON job.id = ja.job_id
    WHERE job.employer_id = :employerid";
    return $DB->get_record_sql($sql, array('employerid' => $comid));
}

function mkcom_count_save_jobs($comid){
    global $DB;
    $sql = "SELECT COUNT(*)
    FROM {vi_jobs} job
    JOIN {vi_saved_jobs} sj ON job.id = sj.job_id
    WHERE job.employer_id = :employerid";
    return $DB->count_records_sql($sql, array('employerid' => $comid));
}

function mkcom_count_applications($jobid){
    global $DB;
    $sql = "SELECT count(*)
    FROM {vi_job_applications} sj
    WHERE sj.job_id = :jid";
    return $DB->count_records_sql($sql, array('jid' => $jobid));
}

function mkcom_get_total_pages($total, $perpage){
    if($total % $perpage == 0) return $total / $perpage;
    return $total / $perpage + 1;
}

function mkcom_get_totalcount($comid){
    global $DB;
    $sql = "SELECT count(*)
    FROM {vi_jobs} vj
    WHERE vj.employer_id = :comid";
    return $DB->count_records_sql($sql, array('comid' => $comid));
}

function mkcom_get_totalwaiting($comid){
    global $DB;
    $now = time();
    $sql = "SELECT count(*)
    FROM {vi_jobs} vj
    WHERE vj.employer_id = :comid AND vj.startdate > :now";
    return $DB->count_records_sql($sql, array('comid' => $comid, 'now' => $now));
}

function mkcom_get_totalrunning($comid){
    global $DB;
    $now = time();
    $sql = "SELECT count(*)
    FROM {vi_jobs} vj
    WHERE vj.employer_id = :comid AND vj.startdate < :now AND vj.enddate > :now2";
    return $DB->count_records_sql($sql, array('comid' => $comid, 'now' => $now, 'now2' => $now));
}

function mkcom_get_totalended($comid){
    global $DB;
    $now = time();
    $sql = "SELECT count(*)
    FROM {vi_jobs} vj
    WHERE vj.employer_id = :comid AND vj.enddate <= :now2";
    return $DB->count_records_sql($sql, array('comid' => $comid, 'now2' => $now));
}

function mkcom_get_job($jobid){
    global $DB;
    $sql = "SELECT vj.*
    FROM {vi_jobs} vj
    WHERE vj.id = :jobid";
    return $DB->get_record_sql($sql, array('jobid' => $jobid));
}

function mkcom_get_job_skill($jobid){
    global $DB;
    $sql = "SELECT vj.id, vj.name
    FROM {vi_skills} vj
    JOIN {vi_job_skills} js ON vj.id = js.skill_id
    WHERE js.job_id = :jobid";
    return $DB->get_records_sql($sql, array('jobid' => $jobid));
}

function mkcom_get_jobs_notices($comid, $keyword, $datefilter, $filter, $perpage, $page, $order){
    global $DB;
    $sql = "SELECT vj.id jobid, vj.title, vj.timecreated, vj.startdate, vj.enddate, vj.published, vj.view_count, vj.skill_group_id
    FROM {vi_jobs} vj";

    if($datefilter != ""){
        if($keyword != ""){
            $sql = $sql." WHERE vj.employer_id = :comid AND vj.startdate >= :date AND vj.title LIKE :key";
        }else{
            $sql = $sql." WHERE vj.employer_id = :comid AND vj.startdate >= :date";
        }
    }else{
        if($keyword != ""){
            $sql = $sql." WHERE vj.employer_id = :comid AND vj.title LIKE :key";
        }else{
            $sql = $sql." WHERE vj.employer_id = :comid";
        }
    }
    if($filter == 2) $sql = $sql. " AND vj.startdate < :now AND vj.enddate > :now2";
    else if($filter == 3) $sql = $sql. " AND vj.startdate > :now";
    else if($filter == 4) $sql = $sql. " AND vj.enddate < :now2";

    $sql = $sql. " GROUP BY jobid ";
    if($order==10) $sql = $sql."ORDER BY vj.title ASC";
    else if($order==11) $sql = $sql."ORDER BY vj.title DESC";
    else if($order==70) $sql = $sql."ORDER BY vj.startdate ASC";
    else if($order==71) $sql = $sql."ORDER BY vj.startdate DESC";
    else if($order==80) $sql = $sql."ORDER BY vj.enddate ASC";
    else if($order==81) $sql = $sql."ORDER BY vj.enddate DESC";
    else if($order==90) $sql = $sql."ORDER BY vj.timecreated ASC";
    else if($order==91) $sql = $sql."ORDER BY vj.timecreated DESC";
    else if($order==100) $sql = $sql."ORDER BY vj.published ASC";
    else if($order==101) $sql = $sql."ORDER BY vj.published DESC";

    return $DB->get_records_sql($sql, array('comid' => $comid, 'date' => strtotime($datefilter),
        'key' => "%".$keyword."%", 'now'=>time(), 'now2'=>time()),($page-1)*$perpage, $perpage);
}

function generateCsv($data, $delimiter = ',', $enclosure = '"') {
    $handle = fopen('php://temp', 'r+');
    foreach ($data as $line) {
        fputcsv($handle, $line, $delimiter, $enclosure);
    }
    fclose($handle);
}

function mkcom_export_jobs_notices($comid, $keyword, $datefilter, $filter){
    $contents = mkcom_get_jobs_notices($comid, $keyword, $datefilter, $filter);
    global $DB;
    $fileName = 'JobNotice.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
    array_push($outdata, array(0=>get_string('stt', 'local_job'),
        1=>get_string('RecruitmentNotice', 'local_job'),
        2=>get_string('Recommended', 'local_job'),
        3=>get_string('RecommendedTalent', 'local_job'),
        4=>get_string('aplicant', 'local_job'),
        5=>get_string('Interesting', 'local_job'),
        6=>get_string('views', 'local_job'),
        7=>get_string('Postdate', 'local_job'),
        8=> get_string('Startdate', 'local_job'),
        9=>get_string('Enddate', 'local_job'),
        10=>get_string('Exposure', 'local_job')));
    foreach ( $contents as $content ) {
        $skillg = $DB->get_record("vi_skill_groups", array('id'=>$content->skill_group_id));
        $skill_string = ($skillg->name != '') ? $skillg->name.', ' : '';
        $skills = mkcom_get_job_skill($content->jobid); foreach ($skills as $skill) $skill_string = $skill_string.$skill->name.", ";
        array_push($outdata, array(0=>($content->startdate < time()) ? ($content->enddate > time())? "Started" : "Ended" : "Wait",
            1=>$content->title,
            2=>$skill_string,
            3=>mkcom_count_recommend_applicants_jobid($comid, 10, "All", "",$content->jobid),
            4=>mkcom_count_applications($content->jobid),
            5=>mkcom_count_career_applicants_jobid($comid, 10, "All", "",$content->jobid),
            6=>$content->view_count,
            7=>date("Y.m.d",$content->timecreated),
            8=>date("Y.m.d",$content->startdate),
            9=>date("Y.m.d",$content->enddate),
            10=>($content->published) ? "Yes" : "No"));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    // Close the file
    fclose($fh1);
    // Make sure nothing else is sent, our file is done
    exit;
}

function mkcom_count_saved_job($jobid){
    global $DB;
    $sql = "SELECT count(*)
    FROM {vi_saved_jobs} js
    WHERE js.job_id = :jobid";
    return $DB->count_records_sql($sql, array('jobid' => $jobid));
}

function mkcom_get_totaltalent_skills($jobid){
    global $DB;
    $sql = "SELECT count(*)
    FROM {vi_job_skills} js
    JOIN {vi_user_skills} us ON js.skill_id = us.skill_id
    WHERE js.job_id = :jobid";
    return $DB->count_records_sql($sql, array('jobid' => $jobid));
}

function mkcom_get_totaltalent_matchs($comid){
    global $DB;
    $sql = "SELECT count(*)
    FROM {vi_job_skills} js
    JOIN {vi_user_skills} us ON js.skill_id = us.skill_id
    JOIN {vi_jobs} j ON j.id = js.job_id
    WHERE j.id = :comid";
    $total = $DB->count_records_sql($sql, array('comid' => $comid));
    return $total;
}

function mkcom_get_totaltalent_feature($comid){
    global $DB; $total=0;
    $sql = "SELECT j.id, js.skill_id, us.user_id as uid
    FROM {vi_job_skills} js
    JOIN {vi_user_skills} us ON js.skill_id = us.skill_id
    JOIN {vi_jobs} j ON j.id = js.job_id
    WHERE j.employer_id = :comid
    GROUP BY us.user_id";
    $skill = $DB->get_records_sql($sql, array('comid' => $comid));
    $total = count($skill);

    $sql = "SELECT j.id, j.skill_group_id, usg.user_id as uid
    FROM {vi_user_skill_groups} usg
    JOIN {vi_jobs} j ON j.skill_group_id = usg.skill_group_id
    WHERE j.employer_id = :comid
    GROUP BY usg.user_id";
    $skill_group = $DB->get_records_sql($sql, array('comid' => $comid));

    foreach($skill_group as $kg){
        if(!mkcom_matching_user($kg, $skill)) $total++;
    }

    return $total;
}

function mkcom_matching_user($user, $list){
    foreach($list as $l){
        if($l->uid == $user->uid) return true;
    }
    return false;
}

function mkcom_get_totaltalent_bookmark($comid){
    global $DB;
    $sql = "SELECT count(*)
    FROM {vi_follow_employers} fe
    WHERE fe.employer_id = :comid";
    $total = $DB->count_records_sql($sql, array('comid' => $comid));
    return $total;
}

function mkcom_get_nearest_job($comid){
    global $DB;
    $sql = "SELECT fe.id, fe.title, fe.startdate, fe.enddate
    FROM {vi_jobs} fe
    WHERE fe.employer_id = :comid AND fe.startdate <= UNIX_TIMESTAMP(NOW()) AND fe.enddate > UNIX_TIMESTAMP(NOW())
    ORDER BY fe.startdate DESC LIMIT 10";

    $ls = $DB->get_records_sql($sql, array('comid' => $comid));
    foreach ($ls as $l){
        $l->title = "[Started] ". $l->title;
    }

    $sql = "SELECT fe.id, fe.title, fe.startdate, fe.enddate
    FROM {vi_jobs} fe
    WHERE fe.employer_id = :comid AND fe.enddate < UNIX_TIMESTAMP(NOW())
    ORDER BY fe.startdate DESC LIMIT 10";

    $ls2 = $DB->get_records_sql($sql, array('comid' => $comid));
    foreach ($ls2 as $l){
        $l->title = "[Ended] ". $l->title;
    }

    $sql = "SELECT fe.id, fe.title, fe.startdate, fe.enddate
    FROM {vi_jobs} fe
    WHERE fe.employer_id = :comid AND fe.startdate > UNIX_TIMESTAMP(NOW())
    ORDER BY fe.startdate DESC LIMIT 10";

    $ls3 = $DB->get_records_sql($sql, array('comid' => $comid));
    foreach ($ls3 as $l){
        $l->title = "[Wait] " . $l->title;
    }
    return array_merge($ls, $ls2, $ls3);
}

function mkcom_get_all_applicant($comid, $order, $filter, $keyword, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($filter != "All"){
        if($mkstudent == 1){
            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {vi_resume_field_values} fv ON fv.field_value = ja.id
                JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0 ";
            $sql = $sql. " WHERE vj.employer_id = :comid AND fv.field_key = :filter ";
        }else{
            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {vi_resume_field_values} fv ON fv.field_value = ja.id";
            $sql = $sql. " WHERE vj.employer_id = :comid AND fv.field_key = :filter ";
        }

    }else{
        if($mkstudent == 1){
            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {role_assignments} ra on us.id=ra.userid";
            $sql = $sql. " WHERE vj.employer_id = :comid ";
        }else{
            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id";
            $sql = $sql. " WHERE vj.employer_id = :comid ";
        }

    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql." AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql."GROUP BY ja.id ";
    if($order==20) $sql = $sql."ORDER BY vj.title ASC";
    else if($order==21) $sql = $sql."ORDER BY vj.title DESC";
    else if($order==10) $sql = $sql."ORDER BY ja.timecreated ASC";
    else if($order==11) $sql = $sql."ORDER BY ja.timecreated DESC";
    return $DB->get_records_sql($sql, array('comid' => $comid,
        'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter,
        'now'=>$now, 'now2'=>$now, 'jobid'=>$timer
    ));
}

function mkcom_count_applicants_page($comid, $order, $filter, $keyword, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($filter != "All"){
        if($mkstudent == 1){
            $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
            $DB->execute($sql, array());

            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {vi_resume_field_values} fv ON fv.field_value = ja.id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
            $sql = $sql. " WHERE vj.employer_id = :comid AND fv.field_key = :filter ";
        }else{
            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {vi_resume_field_values} fv ON fv.field_value = ja.id";
            $sql = $sql. " WHERE vj.employer_id = :comid AND fv.field_key = :filter ";
        }

    }else{
        if($mkstudent == 1){
            $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
            $DB->execute($sql, array());
            $sql = "
                SELECT ja.id, ja.user_id as uid, us.firstname, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
            $sql = $sql. " WHERE vj.employer_id = :comid ";
        }else{
            $sql = "SELECT ja.id, ja.user_id as uid,us.firstname, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id";
            $sql = $sql. " WHERE vj.employer_id = :comid ";
        }

    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
        $sql = $sql."GROUP BY ja.id ";
        if($mkstudent == 1){
            $sql = $sql." ORDER BY cb.cbb DESC ";
            if($order==20) $sql = $sql.",us.firstname ASC";
            else if($order==21) $sql = $sql.",us.firstname DESC";
            else if($order==10) $sql = $sql.",ja.timecreated ASC";
            else if($order==11) $sql = $sql.",ja.timecreated DESC";
        }else{
            if($order==20) $sql = $sql."ORDER BY us.firstname ASC";
            else if($order==21) $sql = $sql."ORDER BY us.firstname DESC";
            else if($order==10) $sql = $sql."ORDER BY ja.timecreated ASC";
            else if($order==11) $sql = $sql."ORDER BY ja.timecreated DESC";
        }

        $final = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
            'name2'=>'%'.$keyword.'%',
            'name3'=>'%'.$keyword.'%',
            'filter'=>$filter,'now'=>$now, 'now2'=>$now, 'jobid'=>$timer));
        if($mkstudent == 1){
            $sql = "DROP TEMPORARY TABLE m_countB";
            $DB->execute($sql, array());
        }
        return count($final);
}

function mkcom_get_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($filter != "All"){
        if($mkstudent == 1){
            $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
            $DB->execute($sql, array());

            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {vi_resume_field_values} fv ON fv.field_value = ja.id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
            $sql = $sql. " WHERE vj.employer_id = :comid AND fv.field_key = :filter ";
        }else{
            $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                JOIN {vi_resume_field_values} fv ON fv.field_value = ja.id";
            $sql = $sql. " WHERE vj.employer_id = :comid AND fv.field_key = :filter ";
        }

    }else{
        if($mkstudent == 1){
            $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
            $DB->execute($sql, array());
            $sql = "
                SELECT ja.id, ja.user_id as uid, us.firstname, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
            $sql = $sql. " WHERE vj.employer_id = :comid ";
        }else{
            $sql = "SELECT ja.id, ja.user_id as uid,us.firstname, ja.job_id as jobid, vj.title, ja.timecreated, ja.candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_job_applications} ja ON ja.job_id = vj.id
                JOIN {user} us ON us.id = ja.user_id";
            $sql = $sql. " WHERE vj.employer_id = :comid ";
        }

    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql."GROUP BY ja.id ";
    if($mkstudent == 1){
        $sql = $sql." ORDER BY cb.cbb DESC ";
        if($order==20) $sql = $sql.",us.firstname ASC";
        else if($order==21) $sql = $sql.",us.firstname DESC";
        else if($order==10) $sql = $sql.",ja.timecreated ASC";
        else if($order==11) $sql = $sql.",ja.timecreated DESC";
    }else{
        if($order==20) $sql = $sql."ORDER BY us.firstname ASC";
        else if($order==21) $sql = $sql."ORDER BY us.firstname DESC";
        else if($order==10) $sql = $sql."ORDER BY ja.timecreated ASC";
        else if($order==11) $sql = $sql."ORDER BY ja.timecreated DESC";
    }

    $final = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter,'now'=>$now, 'now2'=>$now, 'jobid'=>$timer),
        ($page-1)*$perpage, $perpage);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }
    return $final;
}


function mkcom_applicant2CSV($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $current_language = current_language();
    $apps = mkcom_get_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent);
    $fileName = 'Applicants.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
            array_push($outdata, array(0=>get_string('LoginName', 'local_job'),
                1=>get_string('name', 'local_job'),
                2=>get_string('dateofbirth', 'local_job'),
                3=>get_string('c_Contactemail', 'local_job'),
                4=>get_string('employer_user:contact_number', 'local_job'),
                5=>get_string('my_inter:work_experience', 'local_job'),
                6=>get_string('my_inter:korean_level', 'local_job'),
                7=>get_string('my_inter:skill_group', 'local_job'),
                8=>get_string('my_inter:skill', 'local_job'),
                9=>get_string('my_inter:workplace', 'local_job')));
    foreach($apps as $apply){
        $uinfo = $DB->get_record("user", array('id' => $apply->uid));

                $rows = mkcom_get_user_work_experience($apply->uid);
                $html1="";
                foreach($rows as $p) $html1 = $html1 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
                $html1= rtrim($html1,". ");

                $rows = mkcom_get_user_workplace($apply->uid);
                $html2="";
                foreach($rows as $p) $html2 = $html2. $p->name.". ";
                $html2= rtrim($html2,". ");

                $rows = mkcom_get_user_korean_level($apply->uid);
                $html3="";
                foreach($rows as $p) $html3 = $html3 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
                $html3= rtrim($html3,". ");

                $rows = mkcom_get_user_skill_group($apply->uid);
                $html4="";
                foreach($rows as $p) $html4 = $html4. $p->name.". ";
                $html4= rtrim($html4,". ");

                $rows = mkcom_get_user_skill($apply->uid);
                $html5="";
                foreach($rows as $p) $html5 = $html5. $p->name.". ";
                $html5= rtrim($html5,". ");
                array_push($outdata, array(
                    0=>$uinfo->username,
                    1=>$uinfo->firstname . " ". $uinfo->lastname,
                    2=>$uinfo->phone2,
                    3=>$uinfo->email,
                    4=>$uinfo->phone1,
                    5=>$html1,
                    6=>$html3,
                    7=>$html4,
                    8=>$html5,
                    9=>$html2));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    fclose($fh1);
    exit;
}

function mkcom_count_recommend_applicants($comid, $order, $filter, $keyword, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());
        $sql = "SELECT re.user_id as uid, vj.startdate as timecreated, re.id as candidate_cv, vj.id as jobid, vj.title
                    FROM {vi_job_skills} js
                    JOIN {vi_user_skills} usk ON js.skill_id = usk.skill_id
                    JOIN {user} us ON us.id = usk.user_id
                    JOIN {vi_resumes} re ON us.id = re.user_id AND re.is_default=1
                    LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                    LEFT JOIN m_countB cb ON cb.userid = us.id
                    JOIN {vi_jobs} vj ON vj.id = js.job_id";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }else{
        $sql = "SELECT re.user_id as uid, vj.startdate as timecreated, re.id as candidate_cv, vj.id as jobid, vj.title
                    FROM {vi_job_skills} js
                    JOIN {vi_user_skills} usk ON js.skill_id = usk.skill_id
                    JOIN {user} us ON us.id = usk.user_id
                    JOIN {vi_resumes} re ON us.id = re.user_id AND re.is_default=1
                    JOIN {vi_jobs} vj ON vj.id = js.job_id";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql."GROUP BY vj.id ";


    $final = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter, 'now'=>$now, 'now2'=>$now, 'jobid'=>$timer));
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }

    return count($final);
}


function mkcom_count_recommend_applicants_jobid($comid, $order, $filter, $keyword, $jobid){
    global $DB;
    $sql="";
    if($mkstudent == 1){
        $sql = "SELECT re.user_id as uid, vj.startdate as timecreated, re.id as candidate_cv, vj.id as jobid, vj.title
                    FROM {vi_job_skills} js
                    JOIN {vi_user_skills} usk ON js.skill_id = usk.skill_id
                    JOIN {user} us ON us.id = usk.user_id
                    JOIN {vi_resumes} re ON us.id = re.user_id AND re.is_default=1
                    JOIN {vi_jobs} vj ON vj.id = js.job_id";
        $sql = $sql. " WHERE vj.employer_id = :comid AND vj.id= :jobid ";
    }else{
        $sql = "SELECT re.user_id as uid, vj.startdate as timecreated, re.id as candidate_cv, vj.id as jobid, vj.title
                    FROM {vi_job_skills} js
                    JOIN {vi_user_skills} usk ON js.skill_id = usk.skill_id
                    JOIN {user} us ON us.id = usk.user_id
                    JOIN {vi_resumes} re ON us.id = re.user_id AND re.is_default=1
                    JOIN {vi_jobs} vj ON vj.id = js.job_id";
        $sql = $sql. " WHERE vj.employer_id = :comid AND vj.id= :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
        $sql = $sql."GROUP BY vj.id ";
        return count($DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
            'name2'=>'%'.$keyword.'%',
            'name3'=>'%'.$keyword.'%',
            'filter'=>$filter,
            'jobid'=>$jobid
        )));
}

function mkcom_get_recommend_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());
        $sql = "SELECT re.user_id as uid, vj.startdate as timecreated, re.id as candidate_cv, vj.id as jobid, vj.title
                    FROM {vi_job_skills} js
                    JOIN {vi_user_skills} usk ON js.skill_id = usk.skill_id
                    JOIN {user} us ON us.id = usk.user_id
                    JOIN {vi_resumes} re ON us.id = re.user_id AND re.is_default=1
                    LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                    LEFT JOIN m_countB cb ON cb.userid = us.id
                    JOIN {vi_jobs} vj ON vj.id = js.job_id";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }else{
        $sql = "SELECT re.user_id as uid, vj.startdate as timecreated, re.id as candidate_cv, vj.id as jobid, vj.title
                    FROM {vi_job_skills} js
                    JOIN {vi_user_skills} usk ON js.skill_id = usk.skill_id
                    JOIN {user} us ON us.id = usk.user_id
                    JOIN {vi_resumes} re ON us.id = re.user_id AND re.is_default=1
                    JOIN {vi_jobs} vj ON vj.id = js.job_id";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
        $sql = $sql."GROUP BY vj.id ";
    if($mkstudent == 1){
        $sql = $sql." ORDER BY cb.cbb DESC ";
        if($order==20) $sql = $sql.", vj.title ASC";
        else if($order==21) $sql = $sql.", vj.title DESC";
        else if($order==10) $sql = $sql.", vj.startdate ASC";
        else if($order==11) $sql = $sql.", vj.startdate DESC";
    }else{
        if($order==20) $sql = $sql."ORDER BY vj.title ASC";
        else if($order==21) $sql = $sql."ORDER BY vj.title DESC";
        else if($order==10) $sql = $sql."ORDER BY vj.startdate ASC";
        else if($order==11) $sql = $sql."ORDER BY vj.startdate DESC";
    }

    $final = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter, 'now'=>$now, 'now2'=>$now, 'jobid'=>$timer),
        ($page-1)*$perpage, $perpage);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }

    return $final;
}

function mkcom_recommend_applicant2CSV($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $current_language = current_language();
    $apps = mkcom_get_recommend_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent);
    $fileName = 'Applicants.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
    array_push($outdata, array(0=>get_string('LoginName', 'local_job'),
        1=>get_string('name', 'local_job'),
        2=>get_string('dateofbirth', 'local_job'),
        3=>get_string('c_Contactemail', 'local_job'),
        4=>get_string('employer_user:contact_number', 'local_job'),
        5=>get_string('my_inter:work_experience', 'local_job'),
        6=>get_string('my_inter:korean_level', 'local_job'),
        7=>get_string('my_inter:skill_group', 'local_job'),
        8=>get_string('my_inter:skill', 'local_job'),
        9=>get_string('my_inter:workplace', 'local_job')));
    foreach($apps as $apply){
        $uinfo = $DB->get_record("user", array('id' => $apply->uid));

        $rows = mkcom_get_user_work_experience($apply->uid);
        $html1="";
        foreach($rows as $p) $html1 = $html1 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html1= rtrim($html1,". ");

        $rows = mkcom_get_user_workplace($apply->uid);
        $html2="";
        foreach($rows as $p) $html2 = $html2. $p->name.". ";
        $html2= rtrim($html2,". ");

        $rows = mkcom_get_user_korean_level($apply->uid);
        $html3="";
        foreach($rows as $p) $html3 = $html3 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html3= rtrim($html3,". ");

        $rows = mkcom_get_user_skill_group($apply->uid);
        $html4="";
        foreach($rows as $p) $html4 = $html4. $p->name.". ";
        $html4= rtrim($html4,". ");

        $rows = mkcom_get_user_skill($apply->uid);
        $html5="";
        foreach($rows as $p) $html5 = $html5. $p->name.". ";
        $html5= rtrim($html5,". ");
        array_push($outdata, array(
            0=>$uinfo->username,
            1=>$uinfo->firstname . " ". $uinfo->lastname,
            2=>$uinfo->phone2,
            3=>$uinfo->email,
            4=>$uinfo->phone1,
            5=>$html1,
            6=>$html3,
            7=>$html4,
            8=>$html5,
            9=>$html2));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    fclose($fh1);
    exit;
}

function mkcom_count_saved_applicants($comid, $order, $filter, $keyword, $timer, $mkstudent){
    global $DB, $USER;
    $sql="";
    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());

        $sql = "SELECT us.id as uid, rf.timecreated, re.id as candidate_cv
                FROM  {vi_resumes} re
                JOIN {vi_resume_fields} rf ON rf.resume_id = re.id
                JOIN {user} us ON us.id = re.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id";
        $sql = $sql. " WHERE rf.field_name = 'resume_saved' AND rf.field_group = :comid  ";
    }else{
        $sql = "SELECT us.id as uid, rf.timecreated, re.id as candidate_cv
                FROM  {vi_resumes} re
                JOIN {vi_resume_fields} rf ON rf.resume_id = re.id
                JOIN {user} us ON us.id = re.user_id ";
        $sql = $sql. " WHERE rf.field_name = 'resume_saved' AND rf.field_group = :comid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql." AND ( us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql . " GROUP BY us.id ";

    $saveds = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%','name3'=>'%'.$keyword.'%','filter'=>$filter, 'now'=>$now, 'now2'=>$now),0, 100000);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }
    $applieds = mkcom_get_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);
    $recomendeds = mkcom_get_recommend_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);
    return count($saveds);
}

function mkcom_count_saved_applicants_jobid($comid, $order, $filter, $keyword, $jobid){
    global $DB;
    $sql = "SELECT us.id as uid, rf.timecreated, re.id as candidate_cv
                FROM  {vi_resumes} re
                JOIN {vi_resume_fields} rf ON rf.resume_id = re.id
                JOIN {user} us ON us.id = re.user_id
                JOIN {vi_job_applications} ja ON ja.user_id_id = us.id";
    $sql = $sql. " WHERE rf.field_name = 'resume_saved' AND rf.field_group = :comid AND ja.job_id =:jobid ";
    $sql = $sql."GROUP BY ja.id ";
    return count($DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter,
        'jobid'=>$jobid
    )));
}

function mkcom_get_saved_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB, $USER;
    $sql="";
    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());

        $sql = "SELECT us.id as uid, rf.timecreated, re.id as candidate_cv
                FROM  {vi_resumes} re
                JOIN {vi_resume_fields} rf ON rf.resume_id = re.id
                JOIN {user} us ON us.id = re.user_id 
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id";
        $sql = $sql. " WHERE rf.field_name = 'resume_saved' AND rf.field_group = :comid  ";
    }else{
        $sql = "SELECT us.id as uid, rf.timecreated, re.id as candidate_cv
                FROM  {vi_resumes} re
                JOIN {vi_resume_fields} rf ON rf.resume_id = re.id
                JOIN {user} us ON us.id = re.user_id ";
        $sql = $sql. " WHERE rf.field_name = 'resume_saved' AND rf.field_group = :comid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql." AND ( us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql . " GROUP BY us.id ";
    if($mkstudent == 1){
        $sql = $sql." ORDER BY cb.cbb DESC ";
        if($order==20) $sql = $sql.", us.firstname ASC";
        else if($order==21) $sql = $sql.", us.firstname DESC";
        else if($order==10) $sql = $sql.", re.timecreated ASC";
        else if($order==11) $sql = $sql.", re.timecreated DESC";
    }else{
        if($order==20) $sql = $sql."ORDER BY us.firstname ASC";
        else if($order==21) $sql = $sql."ORDER BY us.firstname DESC";
        else if($order==10) $sql = $sql."ORDER BY rf.timecreated ASC";
        else if($order==11) $sql = $sql."ORDER BY rf.timecreated DESC";
    }
//     return $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
//         'name2'=>'%'.$keyword.'%',
//         'name3'=>'%'.$keyword.'%',
//         'filter'=>$filter, 'now'=>$now, 'now2'=>$now),
//         ($page-1)*$perpage, $perpage);  


    $saveds = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%','name3'=>'%'.$keyword.'%','filter'=>$filter, 'now'=>$now, 'now2'=>$now),0, 100000);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }
    $applieds = mkcom_get_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);
    $recomendeds = mkcom_get_recommend_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);

    foreach($saveds as $save){
        foreach($applieds as $applied){
            if($save->uid == $applied->uid){
                $save->jobid = $applied->jobid;
                $save->title = '[' . get_string('apply2', 'local_job') ."] ".  $applied->title;
                break;
            }
        }
    }
    foreach($saveds as $save){
        foreach($recomendeds as $recomended){
            if($save->uid == $recomended->uid){
                $save->jobid = $recomended->jobid;
                $save->title = '[' . get_string('RecommendedJobs2', 'local_job') ."] ". $recomended->title;
                break;
            }
        }
    }
    return array_slice($saveds, ($page-1)*$perpage,$perpage, true);
}

function mkcom_saved_applicant2CSV($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $current_language = current_language();
    $apps = mkcom_get_saved_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent);
    $fileName = 'Applicants.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
    array_push($outdata, array(0=>get_string('LoginName', 'local_job'),
        1=>get_string('name', 'local_job'),
        2=>get_string('dateofbirth', 'local_job'),
        3=>get_string('c_Contactemail', 'local_job'),
        4=>get_string('employer_user:contact_number', 'local_job'),
        5=>get_string('my_inter:work_experience', 'local_job'),
        6=>get_string('my_inter:korean_level', 'local_job'),
        7=>get_string('my_inter:skill_group', 'local_job'),
        8=>get_string('my_inter:skill', 'local_job'),
        9=>get_string('my_inter:workplace', 'local_job')));
    foreach($apps as $apply){
        $uinfo = $DB->get_record("user", array('id' => $apply->uid));

        $rows = mkcom_get_user_work_experience($apply->uid);
        $html1="";
        foreach($rows as $p) $html1 = $html1 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html1= rtrim($html1,". ");

        $rows = mkcom_get_user_workplace($apply->uid);
        $html2="";
        foreach($rows as $p) $html2 = $html2. $p->name.". ";
        $html2= rtrim($html2,". ");

        $rows = mkcom_get_user_korean_level($apply->uid);
        $html3="";
        foreach($rows as $p) $html3 = $html3 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html3= rtrim($html3,". ");

        $rows = mkcom_get_user_skill_group($apply->uid);
        $html4="";
        foreach($rows as $p) $html4 = $html4. $p->name.". ";
        $html4= rtrim($html4,". ");

        $rows = mkcom_get_user_skill($apply->uid);
        $html5="";
        foreach($rows as $p) $html5 = $html5. $p->name.". ";
        $html5= rtrim($html5,". ");
        array_push($outdata, array(
            0=>$uinfo->username,
            1=>$uinfo->firstname . " ". $uinfo->lastname,
            2=>$uinfo->phone2,
            3=>$uinfo->email,
            4=>$uinfo->phone1,
            5=>$html1,
            6=>$html3,
            7=>$html4,
            8=>$html5,
            9=>$html2));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    fclose($fh1);
    exit;
}

function mkcom_get_job_candidateCV($cvid, $comid){
    global $DB;
    $sql = "SELECT vj.*
            FROM {vi_jobs} vj 
            JOIN {vi_job_applications} ja ON vj.id = ja.job_id AND ja.candidate_cv=".$cvid;
    $sql = $sql . " WHERE vj.employer_id=".$comid;
    $sql = $sql . " ORDER BY vj.startdate DESC";
    return $DB->get_record_sql($sql, array());
}

function mkcom_count_interest_applicants($comid, $order, $filter, $keyword, $timer, $mkstudent){
    global $DB;
    $sql="";

    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());
        $sql = "SELECT fe.user_id as uid, fe.timecreated, re.id as candidate_cv
                FROM {vi_follow_employers} fe
                JOIN {vi_resumes} re ON fe.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = fe.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
        $sql = $sql. " WHERE fe.employer_id = :comid ";
    }else{
        $sql = "SELECT fe.user_id as uid, fe.timecreated, re.id as candidate_cv
                FROM {vi_follow_employers} fe
                JOIN {vi_resumes} re ON fe.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = fe.user_id ";
        $sql = $sql. " WHERE fe.employer_id = :comid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql." us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
        $sql = $sql."GROUP BY re.id ";

    $saveds =  $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter, 'now'=>$now, 'now2'=>$now),
        ($page-1)*$perpage, $perpage);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }

    return count($saveds);
}


function mkcom_get_interest_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $sql="";

    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());
        $sql = "SELECT fe.user_id as uid, fe.timecreated, re.id as candidate_cv
                FROM {vi_follow_employers} fe
                JOIN {vi_resumes} re ON fe.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = fe.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
        $sql = $sql. " WHERE fe.employer_id = :comid ";
    }else{
        $sql = "SELECT fe.user_id as uid, fe.timecreated, re.id as candidate_cv
                FROM {vi_follow_employers} fe
                JOIN {vi_resumes} re ON fe.user_id = re.user_id AND re.is_default=1 
                JOIN {user} us ON us.id = fe.user_id ";
        $sql = $sql. " WHERE fe.employer_id = :comid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql." us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql."GROUP BY re.id ";
    if($mkstudent == 1){
        $sql = $sql." ORDER BY cb.cbb DESC ";
        if($order==20) $sql = $sql.", us.firstname ASC";
        else if($order==21) $sql = $sql.", us.firstname DESC";
        else if($order==10) $sql = $sql.", re.timecreated ASC";
        else if($order==11) $sql = $sql.", re.timecreated DESC";
    }else{
        if($order==20) $sql = $sql."ORDER BY us.firstname ASC";
        else if($order==21) $sql = $sql."ORDER BY us.firstname DESC";
        else if($order==10) $sql = $sql."ORDER BY fe.timecreated ASC";
        else if($order==11) $sql = $sql."ORDER BY fe.timecreated DESC";
    }

    $saveds =  $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter, 'now'=>$now, 'now2'=>$now),
        ($page-1)*$perpage, $perpage);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }
    $applieds = mkcom_get_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);
    $recomendeds = mkcom_get_recommend_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);

    foreach($saveds as $save){
        foreach($applieds as $applied){
            if($save->uid == $applied->uid){
                $save->jobid = $applied->jobid;
                $save->title = '[' . get_string('apply2', 'local_job') ."] ".  $applied->title;
                break;
            }
        }
    }
    foreach($saveds as $save){
        foreach($recomendeds as $recomended){
            if($save->uid == $recomended->uid){
                $save->jobid = $recomended->jobid;
                $save->title = '[' . get_string('RecommendedJobs2', 'local_job') ."] ". $recomended->title;
                break;
            }
        }
    }

    return $saveds;
}

function mkcom_interest_applicant2CSV($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $current_language = current_language();
    $apps = mkcom_get_interest_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent);
    $fileName = 'Applicants.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
    array_push($outdata, array(0=>get_string('LoginName', 'local_job'),
        1=>get_string('name', 'local_job'),
        2=>get_string('dateofbirth', 'local_job'),
        3=>get_string('c_Contactemail', 'local_job'),
        4=>get_string('employer_user:contact_number', 'local_job'),
        5=>get_string('my_inter:work_experience', 'local_job'),
        6=>get_string('my_inter:korean_level', 'local_job'),
        7=>get_string('my_inter:skill_group', 'local_job'),
        8=>get_string('my_inter:skill', 'local_job'),
        9=>get_string('my_inter:workplace', 'local_job')));
    foreach($apps as $apply){
        $uinfo = $DB->get_record("user", array('id' => $apply->uid));

        $rows = mkcom_get_user_work_experience($apply->uid);
        $html1="";
        foreach($rows as $p) $html1 = $html1 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html1= rtrim($html1,". ");

        $rows = mkcom_get_user_workplace($apply->uid);
        $html2="";
        foreach($rows as $p) $html2 = $html2. $p->name.". ";
        $html2= rtrim($html2,". ");

        $rows = mkcom_get_user_korean_level($apply->uid);
        $html3="";
        foreach($rows as $p) $html3 = $html3 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html3= rtrim($html3,". ");

        $rows = mkcom_get_user_skill_group($apply->uid);
        $html4="";
        foreach($rows as $p) $html4 = $html4. $p->name.". ";
        $html4= rtrim($html4,". ");

        $rows = mkcom_get_user_skill($apply->uid);
        $html5="";
        foreach($rows as $p) $html5 = $html5. $p->name.". ";
        $html5= rtrim($html5,". ");
        array_push($outdata, array(
            0=>$uinfo->username,
            1=>$uinfo->firstname . " ". $uinfo->lastname,
            2=>$uinfo->phone2,
            3=>$uinfo->email,
            4=>$uinfo->phone1,
            5=>$html1,
            6=>$html3,
            7=>$html4,
            8=>$html5,
            9=>$html2));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    fclose($fh1);
    exit;
}

function mkcom_count_career_applicants($comid, $order, $filter, $keyword, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());

        $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, ja.timecreated, re.id as candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_saved_jobs} ja ON ja.job_id = vj.id
                JOIN {vi_resumes} re ON ja.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = ja.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }else{
        $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, ja.timecreated, re.id as candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_saved_jobs} ja ON ja.job_id = vj.id
                JOIN {vi_resumes} re ON ja.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = ja.user_id ";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql."GROUP BY re.id ";

    $saveds = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter, 'now'=>$now, 'now2'=>$now, 'jobid'=>$timer),
        ($page-1)*$perpage, $perpage);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }
    return count($saveds);
}

function mkcom_count_career_applicants_jobid($comid, $order, $filter, $keyword, $jobid){
    global $DB;
    $sql="";
    $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, vj.title, ja.timecreated, re.id as candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_saved_jobs} ja ON ja.job_id = vj.id
                JOIN {vi_resumes} re ON ja.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = ja.user_id ";
    $sql = $sql. " WHERE vj.employer_id = :comid AND vj.id=:jobid ";

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
        $sql = $sql."GROUP BY ja.id ";
        return count($DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
            'name2'=>'%'.$keyword.'%',
            'name3'=>'%'.$keyword.'%',
            'filter'=>$filter,
            'jobid'=>$jobid
        )));
}


function mkcom_get_career_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $sql="";
    if($mkstudent == 1){
        $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
        $DB->execute($sql, array());

        $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, ja.timecreated, re.id as candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_saved_jobs} ja ON ja.job_id = vj.id
                JOIN {vi_resumes} re ON ja.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = ja.user_id
                LEFT JOIN {course_completions} ra ON us.id=ra.userid AND ra.timestarted > 0
                LEFT JOIN m_countB cb ON cb.userid = us.id ";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }else{
        $sql = "SELECT ja.id, ja.user_id as uid, ja.job_id as jobid, ja.timecreated, re.id as candidate_cv
                FROM {vi_jobs} vj
                JOIN {vi_saved_jobs} ja ON ja.job_id = vj.id
                JOIN {vi_resumes} re ON ja.user_id = re.user_id AND re.is_default=1
                JOIN {user} us ON us.id = ja.user_id ";
        $sql = $sql. " WHERE vj.employer_id = :comid ";
    }

    $now = time();
    if($timer == 'W'){
        $sql = $sql . " AND vj.startdate > :now ";
    }else if($timer == 'I'){
        $sql = $sql . " AND vj.startdate < :now AND vj.enddate > :now2" ;
    }else if($timer == 'E'){
        $sql = $sql . " AND vj.enddate <= :now ";
    }else if($timer == 'A'){

    }else{
        $sql = $sql . " AND vj.id = :jobid ";
    }

    if($keyword != "" && $keyword != " ")
        $sql = $sql."AND (vj.title LIKE :name OR us.firstname LIKE :name2 OR us.lastname LIKE :name3) ";
    $sql = $sql."GROUP BY re.id ";
    if($mkstudent == 1){
        $sql = $sql." ORDER BY cb.cbb DESC ";
        if($order==20) $sql = $sql.", vj.title ASC";
        else if($order==21) $sql = $sql.", vj.title DESC";
        else if($order==10) $sql = $sql.", ja.timecreated ASC";
        else if($order==11) $sql = $sql.", ja.timecreated DESC";
    }else{
        if($order==20) $sql = $sql."ORDER BY vj.title ASC";
        else if($order==21) $sql = $sql."ORDER BY vj.title DESC";
        else if($order==10) $sql = $sql."ORDER BY ja.timecreated ASC";
        else if($order==11) $sql = $sql."ORDER BY ja.timecreated DESC";
    }

    $saveds = $DB->get_records_sql($sql, array('comid' => $comid, 'name'=>'%'.$keyword.'%',
        'name2'=>'%'.$keyword.'%',
        'name3'=>'%'.$keyword.'%',
        'filter'=>$filter, 'now'=>$now, 'now2'=>$now, 'jobid'=>$timer),
        ($page-1)*$perpage, $perpage);
    if($mkstudent == 1){
        $sql = "DROP TEMPORARY TABLE m_countB";
        $DB->execute($sql, array());
    }
    $applieds = mkcom_get_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);
    $recomendeds = mkcom_get_recommend_applicants($comid, $order, $filter, $keyword, 100000, 1, 'A', $mkstudent);

    foreach($saveds as $save){
        foreach($applieds as $applied){
            if($save->uid == $applied->uid){
                $save->jobid = $applied->jobid;
                $save->title = '[' . get_string('apply2', 'local_job') ."] ".  $applied->title;
                break;
            }
        }
    }
    foreach($saveds as $save){
        foreach($recomendeds as $recomended){
            if($save->uid == $recomended->uid){
                $save->jobid = $recomended->jobid;
                $save->title = '[' . get_string('RecommendedJobs2', 'local_job') ."] ". $recomended->title;
                break;
            }
        }
    }

    return $saveds;
}

function mkcom_career_applicant2CSV($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent){
    global $DB;
    $current_language = current_language();
    $apps = mkcom_get_career_applicants($comid, $order, $filter, $keyword, $perpage, $page, $timer, $mkstudent);
    $fileName = 'Applicants.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
    array_push($outdata, array(0=>get_string('LoginName', 'local_job'),
        1=>get_string('name', 'local_job'),
        2=>get_string('dateofbirth', 'local_job'),
        3=>get_string('c_Contactemail', 'local_job'),
        4=>get_string('employer_user:contact_number', 'local_job'),
        5=>get_string('my_inter:work_experience', 'local_job'),
        6=>get_string('my_inter:korean_level', 'local_job'),
        7=>get_string('my_inter:skill_group', 'local_job'),
        8=>get_string('my_inter:skill', 'local_job'),
        9=>get_string('my_inter:workplace', 'local_job')));
    foreach($apps as $apply){
        $uinfo = $DB->get_record("user", array('id' => $apply->uid));

        $rows = mkcom_get_user_work_experience($apply->uid);
        $html1="";
        foreach($rows as $p) $html1 = $html1 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html1= rtrim($html1,". ");

        $rows = mkcom_get_user_workplace($apply->uid);
        $html2="";
        foreach($rows as $p) $html2 = $html2. $p->name.". ";
        $html2= rtrim($html2,". ");

        $rows = mkcom_get_user_korean_level($apply->uid);
        $html3="";
        foreach($rows as $p) $html3 = $html3 . ($current_language == "ko") ? $p->name_ko : ($current_language == "en") ? $p->name_en : $p->name_vi . ", ";
        $html3= rtrim($html3,". ");

        $rows = mkcom_get_user_skill_group($apply->uid);
        $html4="";
        foreach($rows as $p) $html4 = $html4. $p->name.". ";
        $html4= rtrim($html4,". ");

        $rows = mkcom_get_user_skill($apply->uid);
        $html5="";
        foreach($rows as $p) $html5 = $html5. $p->name.". ";
        $html5= rtrim($html5,". ");
        array_push($outdata, array(
            0=>$uinfo->username,
            1=>$uinfo->firstname . " ". $uinfo->lastname,
            2=>$uinfo->phone2,
            3=>$uinfo->email,
            4=>$uinfo->phone1,
            5=>$html1,
            6=>$html3,
            7=>$html4,
            8=>$html5,
            9=>$html2));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    fclose($fh1);
    exit;
}

function mkcom_applicants_toCSV($comid, $order, $filter, $keyword, $type){
    global $DB;
    $applicants = mkcom_get_all_applicant($comid, $order, $filter, $keyword);
    if($type == "recommend") {

    }else if($type == "saved"){

    }else if($type == "interest"){

    }else if($type == "career"){

    }
    $fileName = 'Applicants.csv';
    header('Content-Encoding: UCS-2LE');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=UCS-2LE");
    header("Content-Disposition: attachment; filename={$fileName}");
    header("Expires: 0");
    header("Pragma: public");
    $fh1 = fopen( 'php://output', 'w');
    fputs($fh1, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    $outdata = array();
//     array_push($outdata, array(0=>get_string('stt', 'local_job'),
//         1=>get_string('RecruitmentNotice', 'local_job'),
//         2=>get_string('Recommended', 'local_job'),
//         3=>get_string('Interest', 'local_job'),
//         4=>get_string('aplicant', 'local_job'),
//         5=>get_string('Recommended', 'local_job'),
//         6=>get_string('Interest', 'local_job'),
//         7=>get_string('Postdate', 'local_job'),
//         8=> get_string('Startdate', 'local_job'),
//         9=>get_string('Enddate', 'local_job'),
//         10=>get_string('Exposure', 'local_job')));
    foreach($applicants as $apply){
        $uinfo = $DB->get_record("user", array('id'=>$apply->uid));

        $rows = mkcom_get_user_work_experience($apply->uid);
        $html1="";
        foreach($rows as $p) $html1 = $html1. $p->name.". ";
        $html1= rtrim($html1,". ");

        $rows = mkcom_get_user_workplace($apply->uid);
        $html2="";
        foreach($rows as $p) $html2 = $html2. $p->name.". ";
        $html2= rtrim($html2,". ");

        $rows = mkcom_get_user_korean_level($apply->uid);
        $html3="";
        foreach($rows as $p) $html3 = $html3. $p->name.". ";
        $html3= rtrim($html3,". ");

        $rows = mkcom_get_user_skill_group($apply->uid);
        $html4="";
        foreach($rows as $p) $html4 = $html4. $p->name.". ";
        $html4= rtrim($html4,". ");

        $rows = mkcom_get_user_skill($apply->uid);
        $html5="";
        foreach($rows as $p) $html5 = $html5. $p->name.". ";
        $html5= rtrim($html5,". ");
        array_push($outdata, array(0=>$uinfo->username,
            1=>$uinfo->firstname,
            2=>$uinfo->lastname,
            3=>$uinfo->email,
            4=>$html1,
            5=>$html2,
            6=>$html3,
            7=>$html4,
            8=>$html5,
            9=>$uinfo->phone1,
            10=>$uinfo->phone2));
    }
    foreach ($outdata as $data ) {
        fputcsv($fh1, $data);
    }
    fclose($fh1);
    exit;
}

function mkcom_get_user($uid){
    return $DB->get_record("user", array('id'=>$uid));
}

function mkcom_get_user_workplace($uid){
    global $DB;
    $sql = "SELECT vd.name
    FROM {vi_user_workplaces} uwp
    JOIN {vi_districts} vd ON vd.id = uwp.district_id
    WHERE uwp.user_id = :uid";
    return $DB->get_records_sql($sql, array('uid' => $uid));
}

function mkcom_get_user_skill($uid){
    global $DB;
    $sql = "SELECT vs.name
    FROM {vi_user_skills} uk
    JOIN {vi_skills} vs ON vs.id = uk.skill_id
    WHERE uk.user_id = :uid";
    return $DB->get_records_sql($sql, array('uid' => $uid));
}

function mkcom_get_user_skill_group($uid){
    global $DB;
    $sql = "SELECT vs.name
    FROM {vi_user_skill_groups} uk
    JOIN {vi_skill_groups} vs ON vs.id = uk.skill_group_id
    WHERE uk.user_id = :uid";
    return $DB->get_records_sql($sql, array('uid' => $uid));
}

function mkcom_get_user_work_experience($uid){
    global $DB;
    $sql = "SELECT vs.name_ko, vs.name_en, vs.name_vi
    FROM {vi_user_work_experiences} uk
    JOIN {vi_work_experiences} vs ON vs.id = uk.work_experience_id
    WHERE uk.user_id = :uid";
    return $DB->get_records_sql($sql, array('uid' => $uid));
}

function mkcom_get_user_korean_level($uid){
    global $DB;
    $sql = "SELECT vs.name_ko, vs.name_en, vs.name_vi
    FROM {vi_user_korean_levels} uk
    JOIN {vi_korean_levels} vs ON vs.id = uk.korean_level_id
    WHERE uk.user_id = :uid";
    return $DB->get_records_sql($sql, array('uid' => $uid));
}

function mkcom_count_user_badge($uid){
    global $DB;
    return count($DB->get_records("lmsdata_badge_user",array("userid"=>$uid)));
}

function mkcom_resume_has_saved($comid, $cvid)
{
    global $DB, $VISANG, $USER;
    if (isloggedin()) {
        global $DB;
        $sql = "SELECT count(*)
            FROM {vi_resume_fields} rf
            WHERE rf.field_group =:comid AND rf.resume_id = :cvid AND rf.field_name = 'resume_saved'";
        return $DB->count_records_sql($sql, array('comid'=>$comid, 'cvid'=>$cvid)) > 0;
    }
    return false;
}

function mkcom_resume_saved(){
    global $DB, $VISANG, $USER;
    $id = required_param('resumeid', PARAM_INT);
    $comid = required_param('appid', PARAM_INT);
    if (isloggedin()) {
        $checkfield = $DB->get_record('vi_resume_fields', array('resume_id'=>$id,'field_name'=>'resume_saved', 'field_group'=>$comid));
        if($checkfield == null){
            $field = new stdClass();
            $field->field_name = 'resume_saved';
            $field->resume_id = $id;
            $field->field_group = $comid;
            $field->sortorder = 0;
            $field->timecreated = time();
            $field->timemodified= time()-1;
            $checkfield = $DB->insert_record('vi_resume_fields', $field);
            return 1;
        }else{
            $DB->delete_records('vi_resume_fields', array('resume_id'=>$id,'field_name'=>'resume_saved', 'field_group'=>$comid));
            return 2;
        }
    }
    return -1;
}

//Deduct Credit points and add a new history of using Credit points when there are enough points left to view CV
function mkcom_resume_viewed(){
    global $DB;
    $id = required_param('resumeid', PARAM_INT);
    $comid = required_param('appid', PARAM_INT);
    if (isloggedin()) {
        $sql="SELECT 
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$comid
            ";
        $credit=$DB->get_record_sql($sql);

        $point_price = $DB->get_field('vi_common_code', 'point', array('code' => 2002));

        if (!$credit) {
            $data = new stdClass();
            $data->employer_id=$comid;
            $data->credit_point=0;
            $data->expire_date=null;
            $data->timecreated = time();
            $DB->insert_record('vi_credits',$data);
        }

        $credit=$DB->get_record_sql($sql);

        if($credit->credit_point < $point_price){
            return 2;
        }else{
            $credit_after_reduce = $credit->credit_point - $point_price;
            $credit->credit_point = $credit_after_reduce;
            $DB->update_record('vi_credits',$credit);

            $data_log = new stdClass();
            $data_log->member_credit_id = $credit->id;
            $data_log->transaction_code = 1002;
            $data_log->withdraw_code = 2002;
            $data_log->point = $point_price;
            $data_log->timelogged = time();
            $data_log->object_id = $id;
            $DB->insert_record('vi_credits_logs', $data_log);

            return 1;
        }
    }
    return -1;
}

//Check if current user has seen this CV contact details before
function mkcom_resume_viewed_status(){
    global $DB;
    $id = required_param('resumeid', PARAM_INT);
    $comid = required_param('appid', PARAM_INT);
    $sql = "SELECT *
            FROM {vi_credits_logs} crdl
            JOIN {vi_common_code} cmc ON crdl.withdraw_code = cmc.code
            JOIN {vi_credits} crd ON crdl.member_credit_id = crd.id
            WHERE crdl.object_id = :id AND cmc.code = 2002 AND crd.employer_id = :comid";
    $checkfield = $DB->get_record_sql($sql, array('id' => $id, 'comid' => $comid));
    if($checkfield != null){
        return 1;
    }
}

//Deduct Credit points and add a new history of using Credit points when there are enough points left to download CV
function mkcom_resume_download(){
    global $DB;
    $id = required_param('resumeid', PARAM_INT);
    $comid = required_param('appid', PARAM_INT);
    $sql="SELECT 
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$comid
            ";
    $credit=$DB->get_record_sql($sql);

    if (!$credit) {
        $data = new stdClass();
        $data->employer_id=$comid;
        $data->credit_point=0;
        $data->expire_date=null;
        $data->timecreated = time();
        $DB->insert_record('vi_credits',$data);
    }

    $credit=$DB->get_record_sql($sql);

    $cv_downloaded = mkcom_resume_download_status();
    if ($cv_downloaded == 1) {
        return 2;
    } else {
        $point_price = $DB->get_field('vi_common_code', 'point', array('code' => 2003));

        if(!$credit || $credit->credit_point < $point_price){
            return 1;
        }else{
            $credit_after_reduce = $credit->credit_point - $point_price;
            $credit->credit_point = $credit_after_reduce;
            $DB->update_record('vi_credits',$credit);

            $data_log = new stdClass();
            $data_log->member_credit_id = $credit->id;
            $data_log->transaction_code = 1002;
            $data_log->withdraw_code = 2003;
            $data_log->point = $point_price;
            $data_log->timelogged = time();
            $data_log->object_id = $id;
            $DB->insert_record('vi_credits_logs', $data_log);
        }
    }
}

//Check if the current user has downloaded this CV before
function mkcom_resume_download_status(){
    global $DB;
    $id = required_param('resumeid', PARAM_INT);
    $comid = required_param('appid', PARAM_INT);
    $sql = "SELECT *
            FROM {vi_credits_logs} crdl
            JOIN {vi_common_code} cmc ON crdl.withdraw_code = cmc.code
            JOIN {vi_credits} crd ON crdl.member_credit_id = crd.id
            WHERE crdl.object_id = :id AND cmc.code = 2003 AND crd.employer_id = :comid";
    $checkfield = $DB->get_record_sql($sql, array('id' => $id, 'comid' => $comid));
    if($checkfield != null){
        return 1;
    }
}

function mkcom_talen_filter($comid, $exper, $skill, $gskill, $krlevel, $places, $visang_filter,$perpage, $page, $order, $hasFilter){
    global $DB;
    $rOld = array();
    $aOld = array(); $nullFill = 0;
    foreach ($exper as $item){
        $aNew = mkcom_talen_filter_byExper($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($skill as $item){
        $aNew = mkcom_talen_filter_bySkill($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($gskill as $item){
        $aNew = mkcom_talen_filter_byGroupSkill($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($krlevel as $item){
        $aNew = mkcom_talen_filter_byKLevel($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($places as $item){
        $aNew = mkcom_talen_filter_byPleace($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }

    $result = array();
    if($hasFilter != 1 || $nullFill == 0){
        $rOld = $DB->get_records('vi_jobs',array());
        foreach ($rOld as $item){
            $temp = mkcom_get_searching_applicants($comid, $item->id);
            $result = mkcom_get_merge_searching_applicants($result,$temp);
        }
    }else{
        foreach ($rOld as $item){
            $temp = mkcom_get_searching_applicants($comid, $item->user_id);
            $result = mkcom_get_merge_searching_applicants($result,$temp);
        }
    }
    if($visang_filter == 1){
        $sort = array_multisort( array_column($result, 'cbb'), SORT_DESC, $result );
        array_multisort($sort, SORT_ASC, $result);
    }
    $sort = array_multisort( array_column($result, 'timecreated'), SORT_DESC, $result );
    if($order==20) $sort = array_multisort( array_column($result, 'timecreated'), SORT_ASC, $result );
    array_multisort($sort, SORT_ASC, $result);
//     return $result;
    return array_slice($result, ($page-1)*$perpage,$perpage, true);
}

function mkcom_array_to_string($arr){
    $out="";
    foreach ($arr as $item){
        $out = $out.$item."$";
    }
    return $out;
}

function mkcom_string_to_array($str){
    return explode("$",$str);
}

function mkcom_count_talen_filter($comid, $exper, $skill, $gskill, $krlevel, $places, $visang_filter,$hasFilter){
    global $DB;
    $rOld = array();
    $aOld = array(); $nullFill = 0;
    foreach ($exper as $item){
        $aNew = mkcom_talen_filter_byExper($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($skill as $item){
        $aNew = mkcom_talen_filter_bySkill($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($gskill as $item){
        $aNew = mkcom_talen_filter_byGroupSkill($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($krlevel as $item){
        $aNew = mkcom_talen_filter_byKLevel($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }
    $rOld = mkcom_mergeAND_talen_search($rOld, $aOld);
    $aOld = array();
    foreach ($places as $item){
        $aNew = mkcom_talen_filter_byPleace($item); $nullFill = $nullFill + count($aNew);
        $aOld = mkcom_mergeOR_talen_search($aOld, $aNew);
    }

    $result = array();
    if($hasFilter != 1 || $nullFill == 0){
        $rOld = $DB->get_records('vi_jobs',array());
        foreach ($rOld as $item){
            $temp = mkcom_get_searching_applicants($comid, $item->id);
            $result = mkcom_get_merge_searching_applicants($result,$temp);
        }
    }else{
        foreach ($rOld as $item){
            $temp = mkcom_get_searching_applicants($comid, $item->user_id);
            $result = mkcom_get_merge_searching_applicants($result,$temp);
        }
    }
    return count($result);
}

function mkcom_get_searching_applicants($comid, $uid){
    global $DB;
    $sql = "CREATE TEMPORARY TABLE m_countB
                    SELECT lbu.userid, count(lbu.userid) as cbb
                    FROM {lmsdata_badge_user} lbu
                    GROUP BY lbu.userid";
    $DB->execute($sql, array());

    $sql = "SELECT re.id as candidate_cv, re.user_id as uid, re.timemodified as timecreated, cb.cbb
            FROM {vi_resumes} re
            LEFT JOIN m_countB cb ON cb.userid = re.user_id
            WHERE  re.user_id = :uid AND re.is_default=1 AND re.published=1";
    $temp = $DB->get_records_sql($sql, array('comid' => $comid, 'uid'=>$uid));

    $sql = "DROP TEMPORARY TABLE m_countB";
    $DB->execute($sql, array());
    return $temp;
}

function mkcom_get_merge_searching_applicants($aOld, $aNew){
    foreach ($aNew as $check){
        $add=true;
        foreach ($aOld as $saved){
            if($check->candidate_cv == $saved->candidate_cv){
                $add=false;
                break;
            }

        }
        if($add) array_push($aOld, $check);
    }
    return $aOld;
}

function mkcom_talen_filter_byExper($expoerID){
    global $DB;
    $sql = "SELECT ja.id, ja.job_id, ja.user_id
    FROM {vi_job_applications} ja
    JOIN {vi_user_work_experiences} we ON ja.user_id = we.user_id
    WHERE we.work_experience_id = :id";
    return $DB->get_records_sql($sql, array('id' => $expoerID));
}

function mkcom_talen_filter_bySkill($skillID){
    global $DB;
    $sql = "SELECT ja.id, ja.job_id, ja.user_id
    FROM {vi_job_applications} ja
    JOIN {vi_user_skills} we ON ja.user_id = we.user_id
    WHERE we.skill_id = :id";
    return $DB->get_records_sql($sql, array('id' => $skillID));
}

function mkcom_talen_filter_byGroupSkill($GskillID){
    global $DB;
    $sql = "SELECT ja.id, ja.job_id, ja.user_id
    FROM {vi_job_applications} ja
    JOIN {vi_user_skill_groups} we ON ja.user_id = we.user_id
    WHERE we.skill_group_id = :id";
    return $DB->get_records_sql($sql, array('id' => $GskillID));
}

function mkcom_talen_filter_byPleace($placeID){
    global $DB;
    $sql = "SELECT ja.id, ja.job_id, ja.user_id
    FROM {vi_job_applications} ja
    JOIN {vi_user_workplaces} we ON ja.user_id = we.user_id
    WHERE we.district_id = :id";
    return $DB->get_records_sql($sql, array('id' => $placeID));
}

function mkcom_talen_filter_Visang(){
    global $DB;
    $sql = "SELECT ja.id, ja.job_id, ja.user_id
    FROM {vi_job_applications} ja
    JOIN {course_completions} ra ON ja.user_id=ra.userid AND ra.timestarted > 0";
    return $DB->get_records_sql($sql, array());
}

function mkcom_talen_filter_byKLevel($levelID){
    global $DB;
    $sql = "SELECT ja.id, ja.job_id, ja.user_id
    FROM {vi_job_applications} ja
    LEFT JOIN {vi_user_korean_levels} we ON ja.user_id = we.user_id
    WHERE we.korean_level_id = :id";
    return $DB->get_records_sql($sql, array('id' => $levelID));
}

function mkcom_mergeOR_talen_search($aOld, $aNew){
    $same = 0;
    foreach ($aNew as $check){
        $same = 0;
        foreach ($aOld as $save){
            if($check->id == $save->id){
                $same = 1;
                break;
            }
        }
        if($same == 0) array_push($aOld, $check);
    }

    return $aOld;
}

function mkcom_mergeAND_talen_search($aOld, $aNew){
    $same = 0;
    if(count($aOld) < 1){
        foreach ($aNew as $check){
            $same = 0;
            foreach ($aOld as $save){
                if($check->user_id == $save->user_id){
                    $same = 1;
                    break;
                }
            }
            if($same == 0) array_push($aOld, $check);
        }
    }else{
//         foreach ($aNew as $check){
//             $same = 0;
//             foreach ($aOld as $save){
//                 if($check->user_id == $save->user_id){
//                     $same = 1;
//                     break;
//                 }
//             }
//             if($same == 1) array_push($aOld, $check);
//         }

        $temp = array(); $hasNew = 0;
        foreach ($aNew as $check){
            $same = 0; $hasNew = 1;
            foreach ($aOld as $save){
                if($check->user_id == $save->user_id){
                    $same = 1;
                    break;
                }
            }
            if($same == 1) array_push($temp, $check);
        }
        if(count($temp) > 0){
            return $temp;
        }else if($hasNew == 1){
            return array();
        }
    }

    return $aOld;
}

function mkcom_filter_talen_search($aOld, $aNew){
    $same = 0;
    $newR = Array();
    foreach ($aNew as $check){
        $same = 0;
        foreach ($aOld as $save){
            if($check->id == $save->id){
                $same = 1;
                break;
            }
        }
        if($same == 1) array_push($newR, $check);
    }
    return $newR;
}

function mkcom_isexist_andchecked($list, $value){
    foreach ($list as $item){
        if($item == $value) return "checked";
    }
    return "";
}

function mkcom_update_resume_status(){
    global $DB;
    $appid = required_param('stt_appid', PARAM_INT);
    $status= required_param('stt_value', PARAM_RAW);
    $cvid = required_param('stt_cvid', PARAM_INT);

    $temp = $DB->get_record("vi_resume_fields", array('resume_id' => $cvid, 'field_name'=> 'Status'));
    $fid = $temp->id;
    if($temp == null){
        $newr = new stdClass();
        $newr->resume_id = $cvid;
        $newr->field_name = "Status";
        $fid = $DB->insert_record("vi_resume_fields", $newr);
        $news = new stdClass();
        $news->field_value = $appid;
        $news->field_id = $fid;
        $news->field_key = "select_one";
        $DB->insert_record("vi_resume_field_values", $news);
    }

    $temp = $DB->get_record("vi_resume_field_values", array('field_id' => $fid, 'field_value'=>$appid));
    if($temp == null){
        $news = new stdClass();
        $news->field_value = $appid;
        $news->field_id = $fid;
        $news->field_key = $status;
        $DB->insert_record("vi_resume_field_values", $news);
    }
    $temp->field_value = $appid;
    $temp->field_key = $status;
    $DB->update_record("vi_resume_field_values", $temp);
    return $fid;
}

function mkcom_update_multi_resume_status(){
    // Update multi
}

function mkcom_get_resume_status($appid, $cvid){
    global $DB;
    $temp = $DB->get_record("vi_resume_fields", array('resume_id' => $cvid, 'field_name'=> 'Status'));
    $fid = $temp->id;
    if($fid == null){
        $newr = new stdClass();
        $newr->resume_id = $cvid;
        $newr->field_name = "Status";
        $fid = $DB->insert_record("vi_resume_fields", $newr);
        $news = new stdClass();
        $news->field_value = $appid;
        $news->field_id = $fid;
        $news->field_key = "select_one";
        $DB->insert_record("vi_resume_field_values", $news);
    }
    $temp = $DB->get_record("vi_resume_field_values", array('field_id' => $fid, 'field_value'=>$appid));
    return $temp->field_key;
}

function mkcom_getSalaryFrom($string){
    list($from, $end) = explode("-", $string);
    return $from;
}

function mkcom_getSalaryTo($string){
    list($from, $end) = explode("-", $string);
    list($start, $to) = explode(" ", $string);
    $string = str_replace($from,"",$string);
    $string = str_replace($to,"",$string);
    $string = str_replace("-","",$string);
    $string = str_replace(" ","",$string);
    return $string;
}
function mkcom_getSalaryUnit($string){
    list($start, $to) = explode(" ", $string);
    return $to;
}

function mkcom_job_noti_manager(){
    global $DB, $VISANG, $USER;
    $employer_id = required_param('employer_id', PARAM_INT);
    $sql_common_code="SELECT
            point
            FROM {vi_common_code} com
            WHERE com.code=2001
            ";
    $common_code=$DB->get_record_sql($sql_common_code);

    $sql="SELECT
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$employer_id
            ";
    $credit=$DB->get_record_sql($sql);
    if (!$credit) {
        $data = new stdClass();
        $data->employer_id=$employer_id;
        $data->credit_point=0;
        $data->expire_date=null;
        $data->timecreated = time();
        $DB->insert_record('vi_credits',$data);
    }
    $credit=$DB->get_record_sql($sql);
    if($credit->credit_point<$common_code->point){
        return json_encode(1);
    }
    else {

        return get_string('noti_post_job1', 'local_credit') . json_encode($credit->credit_point) . get_string('noti_post_job2', 'local_credit', $common_code->point);
    }
}

function mkcom_job_post_noti_manager(){
    global $USER ,$DB;
//handel save job
    $employer_id = required_param('employer_id', PARAM_INT);
    $sql="SELECT
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$employer_id
            ";

    $credit=$DB->get_record_sql($sql);
    if (!$credit) {
        $data = new stdClass();
        $data->employer_id=$employer_id;
        $data->credit_point=0;
        $data->expire_date=null;
        $data->timecreated = time();
        $DB->insert_record('vi_credits',$data);
    }
    $credit=$DB->get_record_sql($sql);
    $sql_common_code="SELECT
            point
            FROM {vi_common_code} com
            WHERE com.code=2001
            ";
    $common_code=$DB->get_record_sql($sql_common_code);


    if($credit->credit_point<($common_code->point)){

        echo json_encode(1);
    }
    else{
//update table vi_credits
        $credit->credit_point=$credit->credit_point-($common_code->point);
        $credit->timemodified=time();
        $DB->update_record('vi_credits',$credit);
//insert table vi_credits_logs
        $data = new stdClass();
        $data->member_credit_id=$credit->id;
        $data->transaction_code='1002';
        $data->withdraw_code='2001';
        $data->point=$common_code->point;
        $data->timelogged = time();
        $DB->insert_record('vi_credits_logs',$data);

        return 0;

    }
}
function mkcom_count_recommend_applicants_new($comid){
    global $CFG, $DB, $USER, $VISANG;
    $thistime = time();
    $listcvs=null;
    $listcvs=task_get_jobid_uercv($comid);
    return count($listcvs);
}
function mkcom_get_recommend_applicants_new($comid, $perpage, $page){
    global $CFG, $DB, $USER, $VISANG;
    $thistime = time();
    $listcvs=null;
    $listcvs=task_get_jobid_uercv($comid);
    $listcvresults=array();
    $total_item=count($listcvs);
    $total_page=ceil($total_item / $perpage);
    $from=($page-1)*$perpage;
    if($page==$total_page){
        $to= $from+$total_item%$perpage;
    }else{
        $to= $from+$perpage;
    }
    if(count($listcvs)>0){
        for ($i=$from;$i<$to;$i++){
            $cv_infor=$DB->get_record('vi_resumes',array('id'=>$listcvs[$i]->cvid));
            $user_infor=$DB->get_record('user',array('id'=>$cv_infor->user_id));
            $job_infor=$DB->get_record('vi_jobs',array('id'=>$listcvs[$i]->jobid));
            $result= new stdClass();
            $result->uid=$user_infor->id;
            $result->timecreated=$job_infor->startdate;
            $result->candidate_cv=$cv_infor->id;
            $result->jobid=$job_infor->id;
            $result->title=$job_infor->title;
            $result->machinhrate=$listcvs[$i]->machinhrate;
            $listcvresults[]= $result;
        }
    }
    usort($listcvresults,function ($a,$b){return $a->machinhrate<$b->machinhrate;});
    return $listcvresults;
}

function task_get_employer(){
    global $DB;
    $employers=$DB->get_records('vi_employers',array('published'=>1),'','id');
    $timenow= time();
    $results=array();
    foreach($employers as $item){
        $sqlcountjob="SELECT COUNT(*) FROM {vi_jobs} vj WHERE vj.employer_id=:employer_id AND vj.published=1 AND $timenow>= vj.startdate AND $timenow<=vj.enddate";
        $jobcount=$DB->count_records_sql($sqlcountjob,array('employer_id'=>$item->id));
        if($jobcount>0){
            $employerinfor=$DB->get_record_sql("SELECT u.*,vae.employer_id as employer_id FROM {user} u LEFT JOIN {vi_admin_employers} vae ON u.id=vae.user_id WHERE vae.employer_id=$item->id");
            if($employerinfor){
                $results[]=$employerinfor;
            }
        }
    }
    return $results;
}
function task_get_jobid_uercv($comid){
    global $DB;
    $listcv=array();
    $jobs=task_get_job_company($comid);
    $users=task_get_users();
    foreach($jobs as $job){
        foreach ($users as $user){
            $machinhrate=0;
            foreach($user->districts as $userlocation){
                if(in_array($userlocation,$job->districts)){
                    $machinhrate+=5;
                    break;
                }
            }
            foreach($user->skills as $userskill){
                if(in_array($userskill,$job->skills)){
                    $machinhrate+=5;
                }
            }
            if($user->eduid==$job->eduid){
                $machinhrate+=20;
            }
            if($user->kolevelid==$job->kolevelid){
                $machinhrate+=35;
            }
            if($user->expyearid==$job->expyearid){
                $machinhrate+=25;
            }
            if($machinhrate>=60){
                $jobcv= new \stdClass();
                $jobcv->jobid=  $job->jobid;
                $jobcv->cvid= $user->cvid;
                $jobcv->machinhrate=$machinhrate;
                $listcv[]=$jobcv;
            }
        }
    }
    return $listcv;
}
function task_get_job_company($comid){
    global $DB;
    $thistime=time();
    $sql="SELECT vj.title as jobtitl,vj.id as jobid,vfe.id as eduid,vfe.name_en as edu,vkl.id as kolevelid, vkl.name_en as kolevel,vwe.id as expyearid,vwe.name_en as expyear
            FROM {vi_jobs} vj
            JOIN {vi_employers} em ON em.id=vj.employer_id
            JOIN {vi_final_educations} vfe ON vj.final_education_id=vfe.id
            JOIN {vi_korean_levels} vkl ON vj.korean_level_id=vkl.id
            JOIN {vi_work_experiences} vwe ON vj.work_experience_id=vwe.id
            WHERE vj.employer_id=:employer_id AND vj.published = 1 AND em.confirmed=1 AND em.published=1 AND em.deleted=0 
            AND ((vj.startdate=0)OR(vj.startdate < $thistime AND $thistime < vj.enddate))";
    $jobs=$DB->get_records_sql($sql,array('employer_id'=>$comid));

    foreach ($jobs as $job){
        $sqlworkplace="SELECT l.district_id as districtid  FROM  {vi_locations} l JOIN {vi_job_locations} jl ON jl.location_id=l.id WHERE jl.job_id=$job->jobid";
        $districts=$DB->get_records_sql($sqlworkplace);
        $job->districts=array();
        $job->districts=$districts;
        $skills=$DB->get_records('vi_job_skills',array('job_id'=>$job->jobid),'','skill_id');
        $job->skills= array();
        $job->skills= $skills;
    }
    return $jobs;
}
function task_get_users(){
    global $DB;
    $sql = "SELECT us.id as userid, re.id as cvid, vufe.final_education_id as eduid,vukl.korean_level_id as kolevelid,vuwe.work_experience_id as expyearid
                FROM {user} us 
                JOIN {vi_resumes} re ON us.id = re.user_id 
                JOIN {vi_user_korean_levels} vukl ON us.id= vukl.user_id
                JOIN {vi_user_final_educations} vufe ON us.id= vufe.user_id
                JOIN {vi_user_work_experiences} vuwe ON us.id= vuwe.user_id
                WHERE re.is_default=1 AND  (:timenow-re.timemodified)<:limittime
                ";
    $timenow=time();
    $limittime=    60*60*24*30*3;
    $users=$DB->get_records_sql($sql,array('timenow'=>$timenow,'limittime'=>$limittime));
    foreach ($users as $user){
        $sqlworkplace="SELECT uw.district_id as districtid  FROM  {vi_user_workplaces} uw  WHERE uw.user_id=$user->userid";
        $districts=$DB->get_records_sql($sqlworkplace);
        $user->districts=array();
        $user->districts=$districts;
        $skills=$DB->get_records('vi_user_skills',array('user_id'=>$user->userid),'','skill_id');
        $user->skills= array();
        $user->skills= $skills;
    }
    return $users;
}








