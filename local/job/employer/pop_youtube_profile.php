<?php
require_once(dirname(__FILE__) . '/../lib.php');
global $VISANG, $DB;

$elid = optional_param('elid', '', PARAM_RAW);
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

// renders
$VISANG->theme->header_foremployer();
?>  
<div class="layerpop">
    <div class="pop-title">
        유튜브 동영상 프로필 등록
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <div class="addr-info">
            <div class="rw mg-bt0">
                <input type="text" placeholder="유튜브 URL 입력" />
            </div>
            <div class="radio-grp">
                <label><input type="radio" name="rd01" />노출</label>
                <label><input type="radio" name="rd01" />비노출</label>
            </div>

        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="취소" class="btns" />
        <input type="button" value="저장" class="btns point" />
    </div>
</div>

<?php
echo $VISANG->theme->footer_foremployer();
?>

