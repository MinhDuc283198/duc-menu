<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/login.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

$frm = data_submitted();
$errormsg = '';
if ($frm) {
    $username = core_user::clean_field($frm->username, 'username');
    $password = trim($frm->password);
    
    if (!empty($username) && !empty($password)) {
        require_once($CFG->dirroot . '/user/lib.php');
        
        $rememberusername = optional_param('rememberusername', 0, PARAM_INT);
        
        $errorcode = 0;
        $user = authenticate_user_login($username, $password, false, $errorcode);
        if ($user) {
            // language setup
            if (isguestuser($user)) {
                // no predefined language for guests - use existing session or default site lang
                unset($user->lang);
            } else if (!empty($user->lang)) {
                // unset previous session language - use user preference instead
                unset($SESSION->lang);
            }
            
            if (empty($user->confirmed)) {
                redirect($VISANG->wwwroot . "/auth/successsignup.php?id=$user->id");
                die;
            }
        }
        switch ($errorcode) {
            case AUTH_LOGIN_OK:
                complete_user_login($user);
                if (isset($SESSION->wantsurl)) {
                    $urltogo = $SESSION->wantsurl;
                    unset($SESSION->wantsurl);
                }
                
                if (empty($CFG->rememberusername) or ($CFG->rememberusername == 2 and $rememberusername == 0)) {
                    set_moodle_cookie('');
                } else {
                    set_moodle_cookie($user->username);
                }
                
                redirect($urltogo);
                break;
            case AUTH_LOGIN_UNAUTHORISED:
                $errormsg = get_string("unauthorisedlogin", "", $username);
                //echo "<script>alert('".$errormsg."');</script>";
                break;
            default:
                $errormsg = "Wrong password ! Please try again !";
                //echo "<script>alert('".$errormsg."');</script>";
                break;
        }
    }
}

$rememberusername = get_moodle_cookie();

// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'jobs';
$VISANG->theme->header();

echo $VISANG->theme->header();
?>

<div class="join-tit"><?php echo get_string('c_CompanyMemberLogin', 'local_job'); ?></div>
<div class="login-bx company">
    <form class="email-bx login" method="post" id="loginForm">
        <p>
            <input type="text" class="w100" placeholder="<?php echo get_string('c_lginEmail', 'local_job'); ?>" name="username" id="username"/>
        </p>
        <p>
            <div class="pass-input" style="position: relative; width: 100%">
                <input type="password" placeholder="<?php echo get_string('c_EnterPassword', 'local_job'); ?>" name="password" id="inpPassword"/>
                <span class="fas fa-eye " id="btnShowpass"></span>
            </div>

        <style>
            #inpPassword{
                width: 100%;
                float: none;
                height: 55px;
                font-size: 18px;
            }
            .pass-input span{
                position: absolute;
                z-index: 100;
                right: 20px;
                top: 20px;
            }

        </style>
        <script>
            const ipnElement = document.querySelector('#inpPassword')
            const btnElement = document.querySelector('#btnShowpass')
            btnElement.addEventListener('click', function() {
                const currentType = ipnElement.getAttribute('type')
                if(currentType==='password'){
                    btnElement.classList.remove('fa-eye');
                    btnElement.classList.add('fa-eye-slash');
                } else {
                    btnElement.classList.remove('fa-eye-slash');
                    btnElement.classList.add('fa-eye');
                }
                ipnElement.setAttribute(
                    'type',
                    currentType === 'password' ? 'text' : 'password'
                )
            })
        </script>
        </p>
        <div class="d-flex justify-content-between">
        <label class="custom-ck"><input type="checkbox" name="rememberusername"><span></span><strong><?php echo get_string('rememberusername', 'local_job'); ?></strong></label>
        <span class="text-danger" id="emailwarning"><?php echo $errormsg;?></span>
        </div>
        
        <div class="btns-area text-center">
            <input type="button" value="<?php echo get_string('login', 'local_job'); ?>" class="btns point" onclick="checkvalue('email');"/>
            
        </div>
        <div class="no-mg clearfix">
            <p class="find-id f-l"><a href="<?php echo $VISANG->wwwroot . '/employer/join.php?step=1'?>" ><?php echo get_string('c_JoinCorporate', 'local_job'); ?></a></p>
            <p class="find-id f-r"><a href="<?php echo $VISANG->wwwroot . '/employer/find.php?step=1'?>" ><?php echo get_string('c_CompanyAccount', 'local_job'); ?></a></p>
        </div>
    </form>
</div>

<!-- 
<div class="cont">
    <div class="group">
        <div role="main">
            <div class="login-bx position-relative top-0 left-0 my-0 mx-auto">
                <ul class="login-tab tab-event">
                    <li data-target=".box01" id="logintab" class="on"><a href="#"><?php echo get_string('login', 'theme_oklassedu') ?></a></li>
                    <li data-target=".box02" id="signuptab"><a href="#"><?php echo get_string('signup', 'theme_oklassedu') ?></a></li>
                </ul>
                <div class="target-area">
                    <div class="box01 on">
                        <form class="email-bx login" action="<?php echo $VISANG->wwwroot; ?>/auth/login.php" method="post" id="login1">
                            <p>
                                <input required type="text" class="w100" id="username" name="username" value="<?php echo $rememberusername; ?>" title="<?php echo get_string('LoginName', 'local_job'); ?>" placeholder="<?php echo get_string('LoginName', 'local_job'); ?>" />
                            </p>
                            <p>
                                <input require type="password" id="password" name="password" title="<?php echo get_string('LoginPass', 'local_job'); ?>" placeholder="<?php echo get_string('LoginPass', 'local_job'); ?>" />
                            </p>
                            <?php if (isset($CFG->rememberusername) and $CFG->rememberusername == 2) : ?>
                                <p>
                                    <div class="rememberpass">
                                        <label for="rememberusername" class="custom-ck">
                                            <input type="checkbox" name="rememberusername" id="rememberusername" value="1" <?php echo empty($rememberusername) ? '' : 'checked'; ?> />
                                            <span></span>
                                            <strong><?php print_string('rememberusername', 'local_job') ?></strong>
                                        </label>
                                    </div>
                                </p>
                            <?php endif; ?>
                            <div class="btns-area text-center">
                                <input type="submit" id="loginbtn1" class="btns point big" value="<?php echo get_string('login', 'theme_oklassedu') ?>" />
                            </div>
                            <p class="find-id"><a href="<?php echo $VISANG->wwwroot . '/auth/findaccount.php'; ?>"><?php echo get_string("findmyaccount", "local_signup") ?></a></p>
                        </form>
                        <div class="sns-area">
                            <a href="#" class="ic-fb">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_facebook_big.png" alt="facebook" /></span>
                                <span><?php echo get_string('facebooklogin', 'theme_oklassedu') ?></span>
                            </a>
                            <a href="#" class="ic-gl">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_google.png" alt="google" /></span>
                                <span><?php echo get_string('googlelogin', 'theme_oklassedu') ?></span>
                            </a>
                            <a href="#" class="ic-zl">
                                <span class="ic"><img src="/theme/oklassedu/pix/images//icon_zalo.png" alt="zalo" /></span>
                                <span><?php echo get_string('zalologin', 'theme_oklassedu') ?></span>
                            </a>
                        </div>
                    </div>
                    <div class="box02"></div>
                </div>
            </div>
        </div>
    </div>
</div>
 -->
<script type="text/javascript">
    $("body").addClass("bgblue");


    function checkvalue(type) {
        var target = $("#username");
        var regExpemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (type == 'email' && !regExpemail.test($("#username").val())) {
            var errortext = "<?php echo get_string("emailrule", "local_signup", array('rule' => "visang@visang.com")) ?>";
            $("#" + type + "warning").text(errortext);
        } else {
            $.ajax({
                url: "/local/signup/valueconfirm.php",
                type: 'post',
                dataType: "json",
                data: {
                    type: type,
                    value: target.val()
                },
                success: function(result) {
                    if (result.status) {
                        $("#" + type + "warning").text("Your email was not joined !");
                    } else {
                        $("#loginForm").submit();                        
                    }
                }
            });
        }
    }
</script>

</script>


<?php
//$VISANG->theme->footer();
?>


