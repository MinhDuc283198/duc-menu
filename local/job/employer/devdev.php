<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');

global $CFG, $VISANG, $DB, $USER;

$baseurl = new moodle_url($VISANG->wwwroot . '/employer/applicants.php');

$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('job');


$csdl = optional_param('CSDL', "", PARAM_RAW);
if($csdl == "clearCSDL_SAVEDCV"){
    $DB->delete_records('vi_resume_field_values', array(
        'field_key' => 'resume_saved'));
    $DB->delete_records('vi_resume_field_values', array(
        'field_key' => 'select_one'));
    $DB->delete_records('vi_resume_field_values', array(
        'field_key' => 'passed'));
    $DB->delete_records('vi_resume_field_values', array(
        'field_key' => 'failed'));
    $DB->delete_records('vi_resume_field_values', array(
        'field_key' => 'InterviewApplication'));
    $DB->delete_records('vi_resume_fields', array(
        'field_name' => 'Status'));
    $DB->delete_records('vi_resume_fields', array(
        'field_name' => 'resume_saved'));
}

$csdl2 = optional_param('CSDL2', "", PARAM_RAW);
$email = optional_param('emailValue', "", PARAM_RAW);
if($csdl2 == "DELUSERBYEMAIL" && $email!= ""){
    $user = $DB->get_record('user',array('email'=>$email));
    $employe = $DB->get_record('vi_admin_employers',array('user_id'=>$user->id));
    
    $DB->delete_records('vi_admin_employers', array('user_id'=>$user->id));
    $DB->delete_records('vi_employers', array('id'=>$employe->employer_id));
    $DB->delete_records('user',array('email'=>$email));
}


?>

<form method="post">
<input type="submit" name="CSDL" value="clearCSDL_SAVEDCV"/>
</form>

<!-- <form method="post"> -->
<!-- <input type="text" name="emailValue" value=""/> -->
<!-- <input type="submit" name="CSDL2" value="DELUSERBYEMAIL"/> -->
<!-- </form> -->
