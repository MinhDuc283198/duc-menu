<?php

require_once(dirname(__FILE__) . '/../lib.php');
global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/company_info.php');

$context = context_system::instance();
$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);
$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'jobs';
$VISANG->theme->header_foremployer();
?>  
<script type="text/javascript">
    $(function () {
        utils.popup.call_layerpop("./pop_company_info.php", {
            "width": "600px",
            "height": "auto",
            "callbackFn": function () {}
        }, {
            "param": "11"
        });
        return false;
    });

</script>



