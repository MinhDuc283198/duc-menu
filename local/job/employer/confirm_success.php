<?php

require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER, $SESSION;

$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->header_foremployer();
?>

<div class="mail-end-bx">
    <div><?php echo get_string("emailcertified", "local_signup") ?></div>
    <p><?php echo get_string("emailcertifiedsuccess", "local_signup") ?></p>
    <p class="small"><?php echo get_string("gotohome", "local_signup") ?></p>
    <a href="<?php echo $VISANG->wwwroot ?>" class="btns point big"><?php echo get_string("home", "core") ?></a>
</div>

<?php
$VISANG->theme->footer_foremployer(false);
