<?php
?>

<div class="crs-left-block">
    <ul>
        <li class="<?php echo  ($pageAction == 'aplicant') ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/applicants.php';?>"><?php echo get_string('aplicant', 'local_job'); ?></a></li>
        <li class="<?php echo  ($pageAction == 'aplicant_recommend') ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/applicants_recommend.php';?>"><?php echo get_string('employer:RecommendedTalent', 'local_job'); ?></a></li>
        <li class="<?php echo  ($pageAction == 'aplicant_saved') ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/applicants_saved.php';?>"><?php echo get_string('employer:Talentedpeople', 'local_job'); ?></a></li>
        <li class="<?php echo  ($pageAction == 'aplicant_career') ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/applicants_career.php';?>"><?php echo get_string('employer:CareersJobs', 'local_job'); ?></a></li>
        <li class="<?php echo  ($pageAction == 'aplicant_interest') ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/applicants_interest.php';?>"><?php echo get_string('employer:InterestedCompany', 'local_job'); ?></a></li>
        <li class="<?php echo  ($pageAction == 'talensFind') ? 'on' : ''; ?>"><a href="<?php echo $VISANG->wwwroot . '/employer/talent_search.php'; ?>"><?php echo get_string('talensFind', 'local_job'); ?></a></li>
    </ul>
</div>