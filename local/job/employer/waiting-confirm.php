<?php

require_once(dirname(__FILE__) . '/../lib.php');
global $CFG, $VISANG, $DB, $USER, $SESSION;

$VISANG->theme->bodyclasses = 'bg-area';
$VISANG->theme->header();
?>
<div class="cont">
    <div class="group">
        <div role="main">
            <div class="mail-end-bx style02">
                <div><?php echo get_string("employer:confirn1", "local_job"); ?></div>
                <p>
                    <?php echo get_string("employer:confirn2", "local_job"); ?><br />
                </p>
                <a href="<?php echo $VISANG->wwwroot ?>" class="btns big"><?php echo get_string("gohome", "local_signup"); ?></a>
            </div>
        </div>
    </div>
</div>


<?php
$VISANG->theme->footer(false);?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

