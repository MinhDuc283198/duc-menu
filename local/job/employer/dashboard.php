<?php
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../employer/lib/com_function.php');
global $CFG, $VISANG, $DB, $USER;
require_once($VISANG->dirroot . '/lib/jobs.php');
$baseurl = new moodle_url($VISANG->wwwroot . '/employer/dashboard.php');
$context = context_system::instance();

$userID = mkcom_get_userID();
$comid = mkcom_get_companyID($userID);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$company = mkcom_get_company_info($comid);
$totalEnded = mkcom_get_totalended($comid);
$totalWaiting = mkcom_get_totalwaiting($comid);
$totalRunning = mkcom_get_totalrunning($comid);
$published = $totalWaiting + $totalRunning;
$applicants = mkcom_count_applicants_page($comid, 10, "All", "", "A");
$saveds = mkcom_count_save_jobs($comid);
$jobViews = mkcom_count_job_views($comid);

// renders
$VISANG->theme->title = get_string('pluginname', 'local_job');
$VISANG->theme->menu = 'companies';
$VISANG->theme->header_foremployer();
//$VISANG->theme->header();
?>
<div class="container">
    <div class="row manage_job">
        <div class="row row-manage-title">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="manage_title">
                    <span><?php echo get_string('managejob', 'local_job') ?></span>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <div class="row manage-job-list">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="row infor">
                    <div class="manage-items">
                        <div class="row manage-item-num">
                            <div class="col-lg-12">
                                <a href="<?php echo $VISANG->wwwroot . '/employer/job_notices.php?filter=2'; ?>">
                                    <p><?php echo $totalRunning ?></p></a>

                                <hr>
                            </div>
                        </div>
                        <div class="row row-line"></div>
                        <div class="row manage-item-title">
                            <div class="col-lg-12">
                                <p><?php echo get_string('online', 'local_job') ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="manage-items">
                        <div class="row manage-item-num">
                            <div class="col-lg-12">
                                <a href="<?php echo $VISANG->wwwroot . '/employer/job_notices.php?filter=4'; ?>">
                                    <p><?php echo $totalEnded; ?></p></a>

                                <hr>
                            </div>
                        </div>
                        <div class="row row-line"></div>
                        <div class="row manage-item-title">
                            <div class="col-lg-12">
                                <p><?php echo get_string('expired', 'local_job') ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="manage-items">
                        <div class="row manage-item-num">
                            <div class="col-lg-12">
                                <a href="<?php echo $VISANG->wwwroot . '/employer/applicants_career.php?timer=A'; ?>">
                                    <p><?php echo mkcom_count_career_applicants($comid, 10, "All", "", 'A'); ?></p>
                                </a>

                                <hr>
                            </div>
                        </div>
                        <div class="row row-line"></div>
                        <div class="row manage-item-title">
                            <div class="col-lg-12">
                                <p><?php echo get_string('draff', 'local_job') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <div class="row manage_job manage_candidates">
        <div class="row row-manage-title">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="manage_title">
                    <span><?php echo get_string('managecandidates', 'local_job') ?></span>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <div class="row manage-job-list">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="row infor">
                    <div class="manage-items">
                        <div class="row manage-item-num">
                            <div class="col-lg-12">
                                <a class="" href="<?php echo $VISANG->wwwroot . '/employer/applicants.php'; ?>">
                                    <p><?php echo $applicants; ?></p></a>

                                <hr>
                            </div>
                        </div>
                        <div class="row row-line"></div>
                        <div class="row manage-item-title">
                            <div class="col-lg-12">
                                <p><?php echo get_string('applied', 'local_job') ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="manage-items">
                        <div class="row manage-item-num">
                            <div class="col-lg-12">
                                <a href="<?php echo $VISANG->wwwroot . '/employer/applicants_recommend.php'; ?>">
                                    <p><?php echo mkcom_count_recommend_applicants($comid, 10, "All", "", "A"); ?></p>
                                </a>

                                <hr>
                            </div>
                        </div>
                        <div class="row row-line"></div>
                        <div class="row manage-item-title">
                            <div class="col-lg-12">
                                <p><?php echo get_string('suggested', 'local_job') ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="manage-items">
                        <div class="row manage-item-num">
                            <div class="col-lg-12">
                                <a href="<?php echo $VISANG->wwwroot . '/employer/applicants_interest.php'; ?>">
                                    <p><?php echo mkcom_count_interest_applicants($comid, 10, "All", ""); ?></p></a>

                                <hr>
                            </div>
                        </div>
                        <div class="row row-line"></div>
                        <div class="row manage-item-title">
                            <div class="col-lg-12">
                                <p><?php echo get_string('marked', 'local_job') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>


    </div>
</div>

<!--<div class="center-tit">--><?php //echo get_string('ApplyStatus', 'local_job'); ?><!--</div>-->
<!--<ul class="dhboard-list">-->
<!--    <li>-->
<!--        <div class="dash-num"><a-->
<!--                    href="--><?php //echo $VISANG->wwwroot . '/employer/job_notices.php'; ?><!--">--><?php //echo $published; ?><!--</a>-->
<!--        </div>-->
<!--        <p>--><?php //echo get_string('Jobsinprogress', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--    <li>-->
<!--        <div class="dash-num"><a-->
<!--                    href="--><?php //echo $VISANG->wwwroot . '/employer/applicants.php'; ?><!--">--><?php //echo $applicants; ?><!--</a>-->
<!--        </div>-->
<!--        <p>--><?php //echo get_string('Numberofapplicants', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--    <li>-->
<!--        <div class="dash-num"><a-->
<!--                    href="--><?php //echo $VISANG->wwwroot . '/employer/applicants_career.php?timer=A'; ?><!--">--><?php //echo mkcom_count_career_applicants($comid, 10, "All", "", 'A'); ?><!--</a>-->
<!--        </div>-->
<!--        <p>--><?php //echo get_string('Jobssetforinterest', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--    <li>-->
<!--        <div class="dash-num">--><?php //echo $jobViews; ?><!--</div>-->
<!--        <p>--><?php //echo get_string('views', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--</ul>-->
<!---->
<!--<div class="center-tit">--><?php //echo get_string('TalentList', 'local_job'); ?><!--</div>-->
<!--<ul class="dhboard-list style02 mb-5">-->
<!--    <li>-->
<!--        <div class="dash-num"><a-->
<!--                    href="--><?php //echo $VISANG->wwwroot . '/employer/applicants_recommend.php'; ?><!--">--><?php //echo mkcom_count_recommend_applicants($comid, 10, "All", "", "A"); ?><!--</a>-->
<!--        </div>-->
<!--        <p>--><?php //echo get_string('TalentMatch', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--    <li>-->
<!--        <div class="dash-num"><a-->
<!--                    href="--><?php //echo $VISANG->wwwroot . '/employer/applicants_saved.php'; ?><!--">--><?php //echo mkcom_count_saved_applicants($comid, 10, "All", ""); ?><!--</a>-->
<!--        </div>-->
<!--        <p>--><?php //echo get_string('FeaturedTalents', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--    <li>-->
<!--        <div class="dash-num"><a-->
<!--                    href="--><?php //echo $VISANG->wwwroot . '/employer/applicants_interest.php'; ?><!--">--><?php //echo mkcom_count_interest_applicants($comid, 10, "All", ""); ?><!--</a>-->
<!--        </div>-->
<!--        <p>--><?php //echo get_string('Bookmarks', 'local_job'); ?><!--</p>-->
<!--    </li>-->
<!--</ul>-->


<script type="text/javascript">
    $("body").addClass("bgblue");
</script>
<?php
$VISANG->theme->footer_foremployer();
//$VISANG->theme->footer()
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />




