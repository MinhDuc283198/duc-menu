<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/local/tag_admin/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance(); 
require_capability('moodle/site:config', $context);

$mod = optional_param('mod',"",PARAM_TEXT);
$id = optional_param('id', "" , PARAM_TEXT);
$category = get_tag_category();



include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php if($category->$tag_depth==1 || $mod=="top"){
                echo '대분류 등록';
            }else{echo '소분류 등록';}?></h3>

        <div class="page_navbar">
            <a>운영관리</a> > 
            <a href="<?php echo $CFG->wwwroot . '/local/tag_admin/tag_list.php'; ?>" >태그관리</a> > 
            <strong><?php if($category->tag_depth==1 || $mod=="top"){
                echo '대분류 등록';
            }else{echo '소분류 등록';}?>
            </strong>
        </div>
        <div>
        <form id="frm_tag_submit" name="f" action="tag_submit.php">
        
            <table>
                <tr>
                    <td bgcolor="#f2f2f2">태그분류</td><?php if($id){
                     ?>   
                <td style="text-align:left"><select id="parent_tag" name="parent_tag">
                    <?php foreach($category as $cate){
                                             ?>
                    <option value="<?php echo $cate->id?>"><?php echo $cate->tag_category?></option>
                    <?php                    
                    }
                    ?>
                    
                    
                    
                    </select></td>
                    <input type="hidden" name="tag_category" id="tag_category" value="">
                <input type="hidden" name="tag_depth" value="2">
                    <?php
                    }else{?><td style="text-align:left"><input type="text" id="tag_category" name="tag_category" size="10" maxlength="10" required="required"><input type="hidden" name="tag_depth" value="1"></td>
                </tr>
                <?php
                    }
                    ?>
                <tr>
                    <td bgcolor="#f2f2f2">태그명</td><td style="text-align:left"><input type="text" id="tagname" name="tagname" size="10" maxlength="10" required="required"></td>
                </tr>
                <tr>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" name="usable" value="1" checked="checked">사용&nbsp;&nbsp;&nbsp; <input type="radio" name="usable" value="0">미사용</td>
                </tr>
                <input type="hidden" name="mod" value="add">                
            </table>
            <div id="btn_area">
                        <input type="submit" id="tag_submit"  class="blue_btn" value="<?php echo get_string('save', 'local_lmsdata'); ?>" style="float:right" /> 
                        <input type="button" id="tag_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                    </div><!--Btn Area End-->        
        </form>
            </div>
    </div>
</div>
        <script>
            var target = document.getElementById("parent_tag");
var cc = target.options[target.selectedIndex].text
$('#tag_category').val(cc);
            
            
            $(document).ready(function () {
        $('#tag_list').click(function () {
            location.href = "./tag_list.php";
        });
        $('#tag_submit').click(function () {
            $('#frm_tag_submit').submit();
        });


          //  $("#parentid").val($("#code_category option:selected").val());
        
        

        $('#frm_tag_submit').submit(function (event) {
            var title = $("#tag_category").val();
            if (title.trim() == '') {
                alert("태그분류를 입력해 주세요");
                return false;
            };
            
            var content = $("#tagname").val(); 
            if (content.trim() == '') {
                alert("태그명을 입력해 주세요");
                return false;
            };
           
        });
    }); 
            </script>