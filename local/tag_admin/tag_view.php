<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/local/tag_admin/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance(); 
require_capability('moodle/site:config', $context);

$id = required_param('id',PARAM_TEXT);

$view = get_tag_view($id);

include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php if($view->tag_depth==1){
            echo '대분류';
        }else{
            echo '소분류';
        }?> 태그</h3>

        <div class="page_navbar">
            <a>운영관리</a> > 
            <a href="<?php echo $CFG->wwwroot . '/local/tag_admin/tag_list.php'; ?>" >태그관리</a> > 
            <strong><?php if($view->tag_depth==1){
            echo '대분류 태그';
        }else{
            echo '소분류 태그';
        }?> </strong>
        </div>
        <div>
        <form id="frm_tag_submit" action="tag_submit.php">
        <table>
                <tr>
                    <td bgcolor="#f2f2f2">태그분류</td><td style="text-align:left"><input type="text" value="<?php echo $view->tag_category?>" readonly="readonly" name="tag_category" size="10" maxlength="20"></td>
                </tr>
                <tr>
                    <td bgcolor="#f2f2f2">태그명</td><td style="text-align:left"><input type="text" value="<?php echo $view->tagname?>" readonly="readonly" name="tagname" size="50" maxlength="100"></td>
                </tr>
                <tr>
                    <?php if($view->usable){
                        ?>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" name="usable" value="1" checked="checked">사용&nbsp;&nbsp;&nbsp; <input type="radio" disabled="disabled" name="usable" value="0">미사용</td>
                    <?php
                    }else{?>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" disabled="disabled" name="usable" value="1" >사용&nbsp;&nbsp;&nbsp; <input type="radio" name="usable" value="0" checked="checked">미사용</td>
                
                </tr>
                    <?php
                    
                    
            
                    }?>
                            
            </table>
            <div id="btn_area">
                <?php
                if($view->tag_depth==1){
                    ?>
                <input type="button" id="tag_add"  class="blue_btn" value="소분류 등록" style="float:right" />
                <?php
                }
                ?>
                        
                        <input type="button" id="tag_edit"  class="blue_btn" value="수정하기" style="float:center" />
                        <input type="button" id="tag_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                    </div><!--Btn Area End-->        
        </form>
            </div>
    </div>
</div>
        <script>
            $(document).ready(function () {
        $('#tag_list').click(function () {
            location.href = "./tag_list.php";
        });
        $('#tag_edit').click(function () {
            location.href = "./tag_edit.php?id="+<?php echo $id?>;
        });
        $('#tag_add').click(function () {
            location.href = "./tag_add.php?id="+<?php echo $id?>;
        });
    }); 
            </script>
            
