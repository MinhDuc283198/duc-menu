<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/local/tag_admin/lib.php';
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
//로그인 안되어있을때 로그인페이지로 이동.
if (!isloggedin()) {//로그인 되어있는지 확인
    echo '<script type="text/javascript">
            alert("로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.");
            document.location.href="' . $SITECFG->wwwroot . '/main/user/login.php";
        </script>';
}
//파라미터 가져옴

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$tag_category = optional_param('tag_category',"",PARAM_TEXT);
$tag_depth = required_param('tag_depth', PARAM_INT); // 1 : 대분류,  2: 소분류
$tagname = required_param('tagname', PARAM_TEXT); // 코드값
$usable = required_param('usable', PARAM_INT); //
$parent_tag = optional_param('parent_tag', 0, PARAM_INT); //

if($tag_depth==2){
    $tag_category=get_tag_category2($parent_tag);
}








//배열을 새로 만듬
$newdata = new stdClass();//stdClass()와 object()의 차이

//값을 지정
$newdata->id = $id;
$newdata->tag_category = $tag_category;
$newdata->tag_depth = $tag_depth;
$newdata->tagname = $tagname;
$newdata->usable = $usable;
$newdata->parent_tag =$parent_tag;


   if ($mod == "edit") {//수정시
    $newdata->timemodified = time();
    $DB->update_record('course_tag', $newdata);//table을 update

} else if ($mod == "add") {//등록시
    if($tag_depth==1){
    $ccck = tag_category_check($tag_category);
    if($ccck){
    echo '<script type="text/javascript">
            alert("태그분류는 중복될 수 없습니다.");
            document.location.href="' . $SITECFG->wwwroot . '/local/tag_admin/tag_add.php?mod=top";
        </script>';
    return false;
}
}
if($tag_depth==2){
    $ccc = tag_check($tagname);
    if($ccc){
    echo '<script type="text/javascript">
            alert("대분류 안에서 태그명은 중복될 수 없습니다.");
            document.location.href="' . $SITECFG->wwwroot . '/local/tag_admin/tag_add.php?id='.$parent_tag.'";
        </script>';
    return false;
}
}

   $newdata->timecreated = time();
   $newdata->timemodified = time();   

   $temp = new stdClass();
   $temp->id = $DB->insert_record('course_tag', $newdata);//table에 insert
   $temp->parent_tag = $temp->id;
   //$sql= "select * from {lmsdata_code} where id=$temp";
   if($tag_depth==1){
   //$sql="update lmsdata_code set parentid=".$temp.""." where id=".$temp."";
   $DB->update_record('course_tag',$temp);
   }


}
redirect($CFG->wwwroot . '/local/tag_admin/tag_list.php');//목록페이지로 돌아감
?>