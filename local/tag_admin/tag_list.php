<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/local/tag_admin/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance(); 
require_capability('moodle/site:config', $context);

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$top_list= get_tag_list($page-1,$perpage);



include_once ($CFG->dirroot . '/chamktu/inc/header.php'); //추후변경
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); //추후변경 ?> 
    <div id="content">
        <h3 class="page_title">태그관리</h3>

        <div class="page_navbar">
            <a href="<?php echo $CFG->dirroot . '/support/notices.php'; ?>" ><?php echo get_string('site_management', 'local_lmsdata'); ?></a> > 
            
            <strong>태그관리</strong>
        </div>
        <form method="post" id="formid" action="<?php echo $CFG->wwwroot ?>/local/tag_admin/tag_del.php">
        <div>
            <table>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                     <th style="width: 10%">일련번호</th>
                     <th style="width: 10%">태그분류</th>
                     <th>태그명</th>
                     <th style="width: 10%">사용여부</th>
                     <th style="width: 15%">날짜</th>
                    </tr>
            </thead>
            <tbody>
                <?php                 
                foreach($top_list as $top){
                    ?>
                <tr>
                    <td class="chkbox"><input type="checkbox" title="check"  name="tagid<?php echo $top->id; ?>" value="<?php echo $top->id; ?>" /></td>
                    <td><?php echo $top->id?></td>
                    <td><?php echo $top->tag_category?></td>
            <td style="text-align:left"><?php if($top->tag_depth==2){
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        }?><a href="<?php echo'/local/tag_admin/tag_view.php?id='.$top->id?>"><?php echo $top->tagname?></a></td>
                    <?php if($top->usable){
                        echo '<td>사용</td>';
                    }else{
                        echo '<td>미사용</td>';
                    }?>                    
                    <td><?php echo date("Y-m-d", $top->timecreated)?></td>
                </tr>
              
                <?php
                }
                
 		?>
                
                
             </tbody>
            </table>
            
        </div>
            <div id="btn_area">
                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/tag_admin/tag_add.php?mod=top"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                <input type="button" id="tag_delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left;"/>
            </div>
    </form>
        <div class="pagination">
        <?php
        $totalcount= get_tag_totalcount();
        print_paging_navbar($totalcount, $page, $perpage, $CFG->wwwroot . '/chamktu/support/code_manage.php', array("search" => $search));
        ?>
        </div>
        </div>
</div>
        
        <?php include_once ($CFG->dirroot . '/chamktu/inc/footer.php'); ?>

<script>
    $(function() {
        $("#allcheck").click(function() {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function() {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function() {
                    this.checked = false;
                });
            }
        });
        
        $("#tag_delete_button").click(function() {
            if(confirm("<?php echo get_string('delete_confirm', 'local_lmsdata');?>")){
                $('#formid').submit();
            }
        });
    });
</script>