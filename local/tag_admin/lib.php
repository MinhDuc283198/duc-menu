<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function get_tag_list($page,$perpage){
    global $DB;
        $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    
    $sql="select * from {course_tag} order by parent_tag asc,tag_depth asc";
    return $DB->get_records_sql($sql,$params,$offset,$perpage);
}

function get_tag_view($id){
    global $DB;
    $sql="select * from {course_tag} where id=".$id."";
    return $DB->get_record_sql($sql);
}

function get_tag_totalcount(){
    global $DB;
    
    return $DB->count_records_select("course_tag");
}

function get_tag_category(){
    global $DB;
    $sql="select distinct id,tag_category from {course_tag} where tag_depth=1";
    return $DB->get_records_sql($sql);
}
function get_tag_category2($id){
    global $DB;
    $sql="select tag_category from {course_tag} where id=$id and tag_depth=1";
    return $DB->get_field_sql($sql);
}

function get_parent_name($parent_tag){
    global $DB;
    $sql="select tagname from {course_tag} where parent_tag=".$parent_tag."";
    return $DB->get_records_sql($sql);
    
}

function tag_category_check($tag_category){
    global $DB;
    return $DB->record_exists("course_tag",array("tag_category"=>$tag_category));
    
}
function tag_check($tagname){
    global $DB;

    return $DB->record_exists("course_tag",array("tagname"=>$tagname,"tag_depth"=>1));
   
}
function depth_check($id){
    global $DB;
    
    $sql = "select tag_depth from {course_tag} where id=$id";

    return $DB->get_field_sql($sql);
}
function get_tag_parent($id){
    global $DB;
    
    $sql = "select tag_parent from {course_tag} where id=$id";
    
    return $DB->get_field_sql($sql);
}

