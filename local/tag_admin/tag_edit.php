<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once $CFG->dirroot . '/chamktu/lib/paging.php';
require_once $CFG->dirroot . '/local/tag_admin/lib.php';
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/chamktu/support/main_menu.php');
    redirect(get_login_url());
}
$context = context_system::instance(); 
require_capability('moodle/site:config', $context);

$id = required_param('id',PARAM_TEXT);

$view = get_tag_view($id);

include_once ($CFG->dirroot . '/chamktu/inc/header.php');
?>
<div id="contents">
    <?php include_once ($CFG->dirroot . '/chamktu/inc/sidebar_support.php'); ?>
    <div id="content">
        <h3 class="page_title"><?php if($view->tag_depth==1){
            echo '대분류';
        }else{
            echo '소분류';
        }?> 수정</h3>

        <div class="page_navbar">
            <a>운영관리</a> > 
            <a href="<?php echo $CFG->wwwroot . '/local/tag_admin/tag_list.php'; ?>" >코드관리</a> > 
            <strong><?php if($view->tag_depth==1){
            echo '대분류';
        }else{
            echo '소분류';
        }?> 수정</strong>
        </div>
        <div>
        <form id="tag_submit" action="tag_submit.php">
        <table>
                <tr>
<input type="hidden" name="mod" value="edit">
<input type="hidden" name="tag_depth" value="<?php echo $view->tag_depth?>">
<!--<input type="hidden" name="parentid" value="<?php //echo $view->id?>">-->
<input type="hidden" name="id" value="<?php echo $view->id?>">
                    <td bgcolor="#f2f2f2">태그분류</td><td style="text-align:left"><input type="text" value="<?php echo $view->tag_category?>" readonly="readonly" name="tag_category" size="10" maxlength="20"></td>

                </tr>
                <tr>
                    <td bgcolor="#f2f2f2">태그명</td><td style="text-align:left"><input type="text" maxlength="100" value="<?php echo $view->tagname?>" name="tagname" size="50" ></td>
                </tr>
                <tr>
                    <?php if($view->usable){
                        ?>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" name="usable" value="1" checked="checked">사용&nbsp;&nbsp;&nbsp; <input type="radio" name="usable" value="0">미사용</td>
                    <?php
                    }else{?>
                    <td bgcolor="#f2f2f2">사용여부</td><td style="text-align:left"><input type="radio" name="usable" value="1" >사용&nbsp;&nbsp;&nbsp; <input type="radio" name="usable" value="0" checked="checked">미사용</td>
                
                </tr>
                    <?php
                    
                    
            
                    }?>
                            
            </table>
            <div id="btn_area">
                        <input type="submit" id="tag_submit"  class="blue_btn" value="<?php echo get_string('save', 'local_lmsdata'); ?>" style="float:right" /> 
                        <input type="button" id="tag_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                    </div><!--Btn Area End-->     
                    </div><!--Btn Area End-->        
        </form>
            </div>
    </div>
</div>

        <script>
            $(document).ready(function () {
        $('#tag_list').click(function () {
            location.href = "./tag_list.php";
        });
    }); 
            </script>
            