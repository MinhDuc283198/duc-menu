<?php

function xmldb_local_tag_admin_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    require_once($CFG->libdir . '/eventslib.php');

    $dbman = $DB->get_manager();

if($oldversion < 2017062700) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('course_tag');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('tag_category', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tagname', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('tag_depth', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('parent_tag', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('usable', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null,0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        
        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    return true;
}