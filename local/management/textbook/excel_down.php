<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/local/management/textbook/lib.php');

$books = lmsdata_introduce_textbook($sql_where, $params,$currpage - 1, $perpage);

    $fields = array(
        get_string('number', 'local_management'),
        get_string('bookname', 'local_management'),
        get_string('bookcode', 'local_management'),
        get_string('publisher', 'local_management'),
        get_string('author', 'local_management'),
        get_string('publisheddate', 'local_management'),
        get_string('copyright', 'local_management'),
        get_string('price', 'local_management') .'('. get_string('regularprice', 'local_management').')',
        get_string('timecreated', 'local_management'),
        get_string('isused', 'local_management')
    );

    $date = date('Y-m-d', time());
    $filename = '교재리스트_' . $date. '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    $num = 1;
    foreach ($books as $book) {
        $col = 0;

        $worksheet[0]->write($row, $col++, $num++);
        $worksheet[0]->write($row, $col++, $book->vi_title.'<br>'.$book->title);
        $worksheet[0]->write($row, $col++, $book->code);
        switch (current_language()){
            case 'ko' :
                $langpublisher = $book->publisher;
                break;
            case 'en' :
                $langpublisher = $book->en_publisher;
                break;
            case 'vi' :
                $langpublisher = $book->vi_publisher;
                break;
        }
        $worksheet[0]->write($row, $col++, $langpublisher );
        switch (current_language()){
            case 'ko' :
                $langauthor = $book->author;
                break;
            case 'en' :
                $langauthor = $book->en_author;
                break;
            case 'vi' :
                $langauthor = $book->vi_author;
                break;
        }
        $worksheet[0]->write($row, $col++, $langauthor);
        $worksheet[0]->write($row, $col++, date('Y.m.d',$book->publisheddate));
        switch (current_language()){
            case 'ko' :
                $langcopyright = $book->copyright;
                break;
            case 'en' :
                $langcopyright = $book->en_copyright;
                break;
            case 'vi' :
                $langcopyright = $book->vi_copyright;
                break;
        }
        $worksheet[0]->write($row, $col++, $langcopyright);
        $worksheet[0]->write($row, $col++, number_format($book->price));
        $worksheet[0]->write($row, $col++, date('Y.m.d',$book->timecreated));
        $worksheet[0]->write($row, $col++, $book->isused == 0  ? get_string('used', 'local_management') : get_string('notused', 'local_management'));
        
        $row++;
    }

    $workbook->close();
    die;

    