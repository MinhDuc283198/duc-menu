<?php

function lmsdata_introduce_textbook_get_count($sql_where='', $params=array()) {
    global $DB;    
    
    $sql="select count(id) from {lmsdata_textbook}  $sql_where ";

    return $DB->count_records_sql($sql, $params);
}

function lmsdata_introduce_textbook($sql_where='', $params=array(),$page,$perpage){
    global $DB; 
    $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    $sql="select * from {lmsdata_textbook} 
        $sql_where
          order by timecreated desc , id desc";
    
    $select='select *, (select title from m_lmsdata_copyright where id=copyright)  as copyright,(select titlevn from m_lmsdata_copyright where id=copyright)  as vi_copyright,(select titleen from m_lmsdata_copyright where id=copyright)  as en_copyright, (select title from m_lmsdata_copyright where id=publisher)  as publisher , (select titleen from m_lmsdata_copyright where id=publisher)  as en_publisher, (select titlevn from m_lmsdata_copyright where id=publisher)  as vi_publisher ';
    $from = ' from {lmsdata_textbook} ';
    $where = $sql_where;
    $order = 'order by timecreated desc , id desc';
            
    return $DB->get_records_sql($select.$from.$where.$order, $params, $offset, $perpage);
}

