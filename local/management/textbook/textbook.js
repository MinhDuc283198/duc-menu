function cata_page(page) {
    $('[name=page]').val(page);
    $('#course_search').submit();
}
//select box 변경시 이벤트
function cata_changed_c(sel) {
    var selCata2 = $('#course_search_cata2');
    cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = cata_get_child_cata_c($(sel).val());
    if (categories !== null) {
        $('#course_search_cata2').show();
        $('#cata2_end').val(0);
        cata_add_select_detail_options(selCata2, categories);
    }else{
        $('#course_search_cata2').hide();
        $('#cata2_end').val(1);
    }
}

//select box 변경시 이벤트
function cata2_changed_c(sel) {
    var selCata2 = $('#course_search_cata3');
    cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = cata_get_child_cata_c($(sel).val());
    if (categories !== null) {
        $('#course_search_cata3').show();
        cata_add_select_detail_options(selCata2, categories);
    }else{
        $('#course_search_cata3').hide();
    }
}


//select box 변경시 이벤트
function cata_get_child_cata_c(pid) {
    var categories = null;
    $.ajax({
        url: '/local/management/course/child_detailcata.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                categories = data.categories;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return categories;
}

function cata_add_select_detail_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.name
        }));
    });
}
//select box 초기화
function cata_clean_select(sel) {
    $(sel[0].options).each(function () {
        if ($(this).val() != 0) {
            $(this).remove();
        }
        ;
    });
}

//값이 없는지 체크
function local_management_value_null_check(select){
    if( $(select).val() == null || $(select).val() == 0 || $(select).val() == undefined || $(select).val() == ''){
        return false;
    }else{
        return true;
    }
}