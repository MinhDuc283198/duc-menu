<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/textbook/lib.php');
require_once($CFG->dirroot . '/local/management/courses/lib.php');
require_once($CFG->dirroot . '/local/book/lib.php');

$pagesettings = array(
    'title' => get_string('book:title','local_management'),
    'heading' => get_string('book:title','local_management'),
    'subheading' => '',
    'menu' => 'textbook',
    'js' => array($CFG->wwwroot . '/local/management/textbook/textbook.js'),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

include_once($CFG->dirroot . '/local/management/header.php');


$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$copyright = optional_param('copyright', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$isused = optional_param_array('isused',array(), PARAM_RAW);
$searchtext = optional_param('searchtext', '', PARAM_RAW);

if($learningstart_str){
    $learningstart = str_replace(".", "-", $learningstart_str); 
    $learningstart = strtotime($learningstart);
}
if($learningend_str){
    $learningend = str_replace(".", "-", $learningend_str); 
    $learningend = strtotime($learningend) + 86399;
}

if($learningstart){
    $sql_where[] = " publisheddate >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if($learningend){
    $sql_where[] = " publisheddate <= :learningend ";
    $params['learningend'] = $learningend;
}


if($copyright){
    $sql_where[] = " copyright = :copyright ";
    $params['copyright'] = $copyright;
}
if($isused){
    foreach($isused as $iskey => $isval){
        $sql_where_isused[] = " isused = :isused".$iskey;
        $params['isused'.$iskey] = $isval;
    }
    if (!empty($sql_where_isused)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($searchtext)) {
    $sql_where_search[] = $DB->sql_like('title', ':title');
    $params['title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('author', ':author');
    $params['author'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('vi_title', ':vi_title');
    $params['vi_title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('vi_author', ':vi_author');
    $params['vi_author'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('en_title', ':en_title');
    $params['en_title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('en_author', ':en_author');
    $params['en_author'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('code', ':code');
    $params['code'] = '%' . $searchtext . '%';
    if (!empty($sql_where_search)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_search) . ')';
    } else {
        $sql_where[] = '';
    }
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}


$totalcount = lmsdata_introduce_textbook_get_count($sql_where, $params);
$books = lmsdata_introduce_textbook($sql_where, $params,$currpage - 1, $perpage);
$perpage_array = [10,20,30,50];
$isused_array = array(get_string('used', 'local_management'),get_string('notused', 'local_management'));
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
            <form name="course_search" id="course_search" class="search_area" action="index.php" method="get">
                <input type="hidden" name="page" value="1" />
                <input type="hidden" name="perpage" value="<?php echo $perpage?>" />
                <div>
                    <label><?php echo get_string('copyright', 'local_management')?></label>
                    <select name='copyright' id='copyright' class="w_160">
                        <option value="0"><?php echo get_string('copyright0', 'local_management')?></option>
                        <?php
                        $copyright_array = searchCopyrightList(0);
                        foreach($copyright_array as $ca){
                            $select = '';
                            if($ca->id == $copyright){
                                $select = 'selected';
                            }
                            switch (current_language()){
                                case 'ko' :
                                    $categorycname = $ca->title;
                                    break;
                                case 'en' :
                                    $categorycname = $ca->titleen;
                                    break;
                                case 'vi' :
                                    $categorycname = $ca->titlevn;
                                    break;
                            }
                            echo '<option value='.$ca->id.' '.$select.' >'.$categorycname.'</option>';
                        }

                        ?>
                    </select>
                </div>
                <div>
                    <label><?php echo get_string('publisheddate', 'local_management')?></label>
                    <input type="text" title="time" name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str?>" placeholder="<?php echo get_string('click', 'local_management'); ?>" > 
                    <span class="dash">~</span> 
                    <input type="text" title="time" name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str?>" placeholder="<?php echo get_string('click', 'local_management'); ?>" > 
                </div>
                <div>
                    <label><?php echo get_string('isused', 'local_management')?></label>
                    <?php foreach($isused_array as $iakey => $iaval){?>
                    <label class="mg-bt10"><input type="checkbox" name="isused[]" value="<?php echo $iakey?>" <?php foreach($isused as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                    <?php }?>      
                </div>

                <div>
                    <label><?php echo get_string('search', 'local_management')?></label>
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('input_book', 'local_management')?>  "  class="search-text w100"/>
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_management'); ?>"/> 
                    <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>"/>   
                </div>
            </form><!--Search Area2 End-->
            
            <span><?php echo get_string('total','local_management', $totalcount )?></span>
            <select name="perpage2" class="perpage2">
                <?php 
                foreach($perpage_array as $pa){
                    $selected = '';
                    if($pa == $perpage){
                        $selected = 'selected';
                    }
                ?>
                <option value="<?php echo $pa?>" <?php echo $selected?>><?php echo $pa?><?php echo get_string('viewmore', 'local_management')?>
                <?php
                }
                ?>
            </select>
               <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="textbook_excel_down();" /> 
               <div style="float:right;"> 
               <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/textbook/form.php"' value="<?php echo get_string('bookregist', 'local_management'); ?>" />
               </div>
            <table>
                <caption class="hidden-caption"><?php echo get_string('book', 'local_management') . get_string('intro', 'local_management') ?></caption>
                <thead>
                    <tr>
                        <th scope="row" width="5%"><?php echo get_string('number', 'local_management') ?></th>
                        <th scope="row" width="45%"><?php echo get_string('bookname', 'local_management') ?></th>
                        <th scope="row" width="45%"><?php echo get_string('bookcode', 'local_management') ?></th>
                        <th scope="row" width="10%"><?php echo get_string('publisher', 'local_management') ?></th>
                        <th scope="row" width="10%"><?php echo get_string('author', 'local_management') ?></th>
                        <th scope="row" width="10%"><?php echo get_string('publisheddate', 'local_management')?></th>
                        <th scope="row" width="10%"><?php echo get_string('copyright', 'local_management')?></th>
                        <th scope="row" width="15%"><?php echo get_string('price', 'local_management') .'('. get_string('regularprice', 'local_management').')'; ?></th>
                        <th scope="row" width="10%"><?php echo get_string('timecreated', 'local_management') ?></th>
                        <th scope="row" width="10%"><?php echo get_string('isused', 'local_management') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $offset = 0;
                    if ($currpage != 0) {
                        $offset = ($currpage - 1) * $perpage;
                    }
                    $num = $totalcount - $offset;
                    if ($totalcount) {
                        foreach ($books as $book) {
                            //$fs = get_file_storage();
                            //$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $book->itemid, "", false);
                            ?>
                            <tr>
                                <td class="number">
                                    <?php echo $num; ?>
                                </td>   
                                <td>
                                    <a href='<?php echo $CFG->wwwroot ?>/local/management/textbook/form.php?mod=edit&id=<?php echo $book->id ?>'><?php echo $book->vi_title.'<br>'.$book->title ?></a>
                                </td>   

                                <td><?php echo $book->code;?></td>
                                <?php
                                switch (current_language()){
                                    case 'ko' :
                                        $langpublisher = $book->publisher;
                                        break;
                                    case 'en' :
                                        $langpublisher = $book->en_publisher;
                                        break;
                                    case 'vi' :
                                        $langpublisher = $book->vi_publisher;
                                        break;
                                }
                                ?>
                                <td><?php echo $langpublisher ?></td>
                                <?php
                                switch (current_language()){
                                    case 'ko' :
                                        $langauthor = $book->author;
                                        break;
                                    case 'en' :
                                        $langauthor = $book->en_author;
                                        break;
                                    case 'vi' :
                                        $langauthor = $book->vi_author;
                                        break;
                                }
                                ?>
                                <td><?php echo $langauthor ?></td>
                                <td><?php echo date('Y.m.d',$book->publisheddate)?></td>
                                <?php
                                switch (current_language()){
                                    case 'ko' :
                                        $langcopyright = $book->copyright;
                                        break;
                                    case 'en' :
                                        $langcopyright = $book->en_copyright;
                                        break;
                                    case 'vi' :
                                        $langcopyright = $book->vi_copyright;
                                        break;
                                }
                                ?>
                                <td><?php echo $langcopyright?></td>
                                <td><?php echo number_format($book->price) ?></td>
                                <td><?php echo date('Y.m.d',$book->timecreated)?></td>
                                <td><?php  echo ($book->isused == 0) ? get_string('used', 'local_management') : get_string('notused', 'local_management');?></td>
                            </tr>
                            <?php
                            $num--;
                        }
                    } else if (!$totalcount) {
                        ?>
                        <tr><td colspan="12"><span><?php echo get_string('empty_book', 'local_management'); ?></span></td></tr>
<?php } ?>
                </tbody>
            </table>
            <div id="btn_area">
                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/textbook/form.php"' value="<?php echo get_string('bookregist', 'local_management'); ?>" />
                <!--<input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>"  />-->
            </div>
                <?php
                if (($totalcount / $perpage) > 1) {
                    print_paging_navbar_script($totalcount, $currpage, $perpage, 'javascript:cata_page(:page);');
                }
                ?>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
$(document).ready(function () {
    $(".perpage2").change(function () {
        var perpage = $('select[name=perpage2]').val();
        $('input[name=perpage]').val(perpage);
        $("form[id=course_search]").submit();
    });
    
    $('#reset').on('click',function(){
        $('#copyright').val('0');
        $('.s_date').val('');
        $('.e_date').val('');
        $("input:checkbox[name='isused[]']").attr("checked", false);
        $('input[name=searchtext]').val('');
        $("form[id=course_search]").submit();
    })
});

    start_datepicker_c('', '');
    end_datepicker_c('', '');
    function textbook_excel_down() {
        var form = $('#course_search');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
         return false;
    }
</script>

