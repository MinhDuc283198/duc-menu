<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/textbook/lib.php');
require_once($CFG->dirroot . '/local/book/lib.php');

$pagesettings = array(
    'title' => get_string('book:title','local_management'),
    'heading' => get_string('book:title','local_management'),
    'subheading' => '',
    'menu' => 'textbook',
    'js' => array(
        $CFG->wwwroot . '/chamktu/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot . '/chamktu/js/ckfinder-2.4/ckfinder.js',
        $CFG->wwwroot . '/local/management/textbook/textbook.js'
    ),
    'css' => array(),
    'nav_bar' => array()
);


$type = optional_param('type', 1, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$perpage = optional_param('perpage', 10, PARAM_INT);
$create = optional_param('create', false, PARAM_BOOL);
$id = optional_param("id", 0, PARAM_INT);
$mod = optional_param("mod", "write", PARAM_TEXT);

$temp = new stdclass();
if ($mod == 'edit') {
    $sql = "SELECT * FROM {lmsdata_textbook} WHERE id=$id";
    $temp = $DB->get_record_sql($sql);

    $fullname = fullname($temp);


    //$fs = get_file_storage();
    //$files = $fs->get_area_files($context->id, 'local_textbook', 'thumbnail', $temp->itemid, "", false);
    $fileobj = lmsdata_book_get_file(1, 'local_lmsdata', $id, 1);
    //print_object($fileobj);exit;
}
$copysql = "SELECT lc.id,cs.id as csid,lc.title,lc.titleen,lc.titlevn
FROM m_lmsdata_copyright lc 
LEFT JOIN (SELECT id,copyid FROM m_lmsdata_copyright_set cs WHERE cs.dataid = $id ) cs on cs.copyid = lc.id
where lc.type = :type";
$copyright_array = $DB->get_records_sql($copysql,array('type'=>0));
$bookpub_array = $DB->get_records_sql($copysql,array('type'=>1));
include_once($CFG->dirroot . '/local/management/header.php');
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">

                <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./submit.php?id=<?php echo $id ?>" method="POST">

                    <table cellpadding="0" cellspacing="0" class="detail">    
                        <input type="hidden" readonly="" id="cata2_end" name="cata2_end" value="<?php echo ($temp->cata1 == $temp->category) ? '1' : '0' ?>"/>  
                        <tbody>

                            <tr>
                                <td class="field_title"  rowspan="3"><?php echo get_string('bookname', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_title"><?php echo get_string('vi', 'local_management'); ?></td>
                                <td class="field_value" colspan="3">
                                    <input type="text" title="id" class="w100" name="vi_title" value="<?php echo (!empty($temp->vi_title)) ? $temp->vi_title : ""; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title"><?php echo get_string('ko', 'local_management'); ?></td>
                                <td class="field_value"  colspan="3">
                                    <input type="text" title="id" class="w100" name="title" value="<?php echo (!empty($temp->title)) ? $temp->title : ""; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title"><?php echo get_string('en', 'local_management'); ?></td>
                                <td class="field_value"  colspan="3">
                                    <input type="text" title="id" class="w100" name="en_title" value="<?php echo (!empty($temp->en_title)) ? $temp->en_title : ""; ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title" colspan="2"><?php echo get_string('bookcode', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value"  colspan="3">
                                    <span><?php echo ($temp->code) ? $temp->code : '' ?>※ <?php echo get_string('code_text', 'local_management') ?></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title" colspan="2"><?php echo get_string('publisher', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value">
                                    <select name='publisher' id='publisher' class="w_160">
                                        <?php
                                        foreach ($bookpub_array as $ca) {
                                            $select = '';
                                            if ($ca->id == $temp->publisher) {
                                                $select = 'selected';
                                            }
                                            switch (current_language()){
                                                case 'ko' :
                                                    $categorycname = $ca->title;
                                                    break;
                                                case 'en' :
                                                    $categorycname = $ca->titleen;
                                                    break;
                                                case 'vi' :
                                                    $categorycname = $ca->titlevn;
                                                    break;
                                            }
                                            
                                            echo '<option value=' . $ca->id . ' ' . $select . ' >' . $categorycname . '</option>';
                                            if($ca->csid){ $set =  '<input type="hidden" readonly=""  name="pub_setid" value="'.$ca->csid.'"/> '; }
                                        }
                                         echo $set;
                                        ?>
                                    </select>
                                </td>
                                <td class="field_title"><?php echo get_string('copyright', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value">
                                    <select name='copyright' id='copyright' class="w_160">
                                        <?php
                                        foreach ($copyright_array as $ca) {
                                            $select = '';
                                            if ($ca->id == $temp->copyright) {
                                                $select = 'selected';
                                            }
                                            switch (current_language()){
                                                case 'ko' :
                                                    $categorycname = $ca->title;
                                                    break;
                                                case 'en' :
                                                    $categorycname = $ca->titleen;
                                                    break;
                                                case 'vi' :
                                                    $categorycname = $ca->titlevn;
                                                    break;
                                            }
                                            echo '<option value=' . $ca->id . ' ' . $select . ' >' . $categorycname . '</option>';
                                            if($ca->csid){  $set =  '<input type="hidden" readonly=""  name="copy_setid" value="'.$ca->csid.'"/> '; }
                                        }
                                        echo $set;
                                        ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title"  rowspan="3"><?php echo get_string('author', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_title" ><?php echo get_string('vi', 'local_management'); ?></td>
                                <td class="field_value"  colspan="3">
                                    <input type="text" title="name" class="w_200" name ="vi_author" value="<?php echo (!empty($temp->vi_author)) ? $temp->vi_author : ""; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('ko', 'local_management'); ?></td>
                                <td class="field_value" colspan="3">
                                    <input type="text" title="name" class="w_200" name ="author" value="<?php echo (!empty($temp->author)) ? $temp->author : ""; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('en', 'local_management'); ?></td>
                                <td class="field_value" colspan="3">
                                   <input type="text" title="name" class="w_200" name ="en_author" value="<?php echo (!empty($temp->en_author)) ? $temp->en_author : ""; ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title"  colspan="2"><?php echo get_string('publisheddate', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value"  colspan="3">
                                    <input type="text" title="name" class="w_200 date s_date" name ="publisheddate" value="<?php echo (!empty($temp->publisheddate)) ? date('Y.m.d',$temp->publisheddate) : '' ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title"  colspan="2"><?php echo get_string('price', 'local_management') . '(' . get_string('regularprice', 'local_management') . ')'; ?> <span class="red">*</span></td>
                                <td class="field_value"  colspan="3">
                                    <div class='input-txt'><input type="text"  class='pricenumber'  style = "text-align:center;" onkeyup="inputNumberFormat(this)" title="name" class="w_200" name ="price" value="<?php echo (!empty($temp->price)) ? $temp->price : ""; ?>"/> VND</div>
                                </td>
                            </tr>

                            <tr class = "thumbnailcl">                
                                <td class="field_title" colspan="2"> <?php echo get_string('bookimg', 'local_management') ?>(jpg,png) <span class="red">*</span><td>
                                    <div class="fileBox" > 
                                        <input type="file" title="thumnail" name="script" size="100" value="" id="captionFile" class="uploadBtn" accept="image/jpeg, image/png, image/jpg">(370*500px)
                                        <?php echo $fileobj; ?>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title"  rowspan="3"><?php echo get_string('bookintro', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_title" ><?php echo get_string('vi', 'local_management'); ?></td>
                                <td class="field_value"  colspan="3">
                                    <textarea class="ckeditor" rows="7" name="vi_text"><?php echo (!empty($temp->vi_text)) ? $temp->vi_text : ""; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('ko', 'local_management'); ?></td>
                                <td class="field_value" colspan="3">
                                    <textarea class="ckeditor" rows="7" name="text"><?php echo (!empty($temp->text)) ? $temp->text : ""; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('en', 'local_management'); ?></td>
                                <td class="field_value" colspan="3">
                                   <textarea class="ckeditor" rows="7" name="en_text"><?php echo (!empty($temp->en_text)) ? $temp->en_text : ""; ?></textarea>
                                </td>
                            </tr>
                    
                            <tr>
                                <td class="field_title"  colspan="2"><?php echo get_string('isused', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value" colspan="3"> 
                                    <input type="radio" name="isused" value="0" <?php echo ($temp->isused == 0) ? "checked" : "" ?> ><?php echo get_string('used', 'local_management') ?>
                                    <input type="radio" name="isused" value="1"<?php echo ($temp->isused == 1) ? "checked" : "" ?>><?php echo get_string('notused', 'local_management') ?>   
                                </td>
                            </tr>

                            <?php
                            if ($id) {
                                $modi_query = "select * from {lmsdata_product_log} where type=0 and productid=:id order by timecreated desc limit 1";
                                $modi_sql = $DB->get_record_sql($modi_query, array("id" => $id));
                                ?>
                                <tr>
                                    <td class="field_title"  colspan="2"><?php echo get_string('insert', 'local_management') ?>/<?php echo get_string('modify', 'local_management') . get_string('day', 'local_management') ?></td>
                                    <td class="field_value" colspan="3"> 
                                        <?php echo date("Y.m.d", $temp->timecreated) ?>  (<?php echo local_management_return_user_info($temp->userid) ?>)
                                        <?php
                                        if ($modi_sql) {
                                            echo get_string('modify', 'local_management') . ':' . date("Y.m.d", $modi_sql->timecreated) . '(' . local_management_return_user_info($modi_sql->userid) . ')';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>

                    </table>

                    <div id="btn_area" class="mg-bt30">
                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_management'); ?>" style="float: right;" onclick="return book_create_submit_check()"   />
                        <?php
                        if ($mod == 'edit') {
                            ?>
                               <!-- <input type="button" id="add_delete" class="red_btn" onclick="delete_admin_user('<?php echo $id; ?>');" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: right; margin-left: 15px;" />-->
                            <?php
                        }
                        ?>
                        <input type="button" id="admin_list" class="normal_btn" value="<?php echo get_string('list', 'local_management'); ?>" style="float: left;" />
                    </div> <!-- Bottom Button Area -->

                </form>


                <?php
                $product_sql = "select lc.* , lt.price as originprice
                from m_lmsdata_class lc
                JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
                WHERE (lc.type = 2 or lc.type = 3) and lc.bookid = :id";

                $product = $DB->get_records_sql($product_sql . $orderby, array("id" => $id));
                if ($product) {
                    ?>
                    <table cellpadding="0" cellspacing="0" class="detail">    
                        <tr>
                            <td class="field_title"><?php echo get_string('productstats', 'local_management'); ?></td>

                            <td class="field_value">
                                <?php
                                foreach ($product as $pr) {
                                    echo '<a href="/local/management/goods/form.php?id=' . $pr->id . '">' . $pr->title . '|' . $pr->code . '|' . $pr->price . "</a><br>";
                                }
                                ?>
                            </td>
                        </tr>
                    </table>

                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script type="text/javascript">
    start_datepicker_c('', '<?php echo date("Y-m-d",time()); ?>');
    
    
    /*
    function fileCheck(obj) {
        pathpoint = obj.value.lastIndexOf('.');
        filepoint = obj.value.substring(pathpoint+1,obj.length);
        filetype = filepoint.toLowerCase();
        if(filetype=='jpg' || filetype=='png' || filetype=='jpeg') {

            // 정상적인 이미지 확장자 파일일 경우 ...

        } else {
            alert('이미지 파일만 선택할 수 있습니다.');
            $('input[name=script]').val('');
            return false;
        }

    }
*/
    $(document).ready(function () {
        
        /**
        * 썸네일 등록시 이미지 파일 체크
        * @type type
        */
        $("input[name=script]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["jpg", "jpeg", "png"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2','local_management')?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });
    
    
        var check_true = 0;
        $('.field_value input[name=pw_edit]').click(function () {
            if ($('.field_value input[name=pw_edit]').prop('checked') == true) {
                $(".field_value input[name=password]").prop('disabled', false);
                $(".field_value input[name=repassword]").prop('disabled', false);
            } else {
                $(".field_value input[name=password]").prop('disabled', true);
                $(".field_value input[name=repassword]").prop('disabled', true);
            }
        });

        $('#admin_list').click(function () {
            location.href = "./index.php";
        });


        $(".field_value input[name=userid]").keyup(function () {
            check_true = 0;
        });

        $(".field_value input[name=userid]").click(function () {
            check_true = 0;
        });

        $(".field_value input[name=id_chk]").click(function () {
            if ($(".field_value input[name=userid]").val().trim() == '') {
                alert("아이디를 입력해 주세요");
                return false;
            } else {
                $.ajax({
                    type: 'POST',
                    data: {
                        type: 'username',
                        sort: 'usertoadmin',
                        chkdata: $(".field_value input[name=userid]").val()
                    },
                    url: './user_data_chk.php',
                    success: function (data) {
                        console.log(data);
                        if (data == -1) {
                            check_true = 0;
                            alert("존재하지 않는 아이디입니다.");
                        } else if (data == -2) {
                            check_true = 0;
                            alert("이미 등록된 강사입니다.");
                        } else {
                            check_true = 1;
                            $('input[name="username"]').val(data.firstname);
                            $('input[name="user_id"]').val(data.userid);
                            alert("사용가능한 아이디입니다.");
                        }
                    },
                });
            }
        });

        $('#frm_popup_submit').submit(function (event) {
            var userid = $(".field_value input[name=userid]").val();
            if (userid.trim() == '') {
                alert("아이디를 입력해 주세요");
                return false;
            }
            ;
<?php
if ($mod == 'write') {
    ?>
                if (check_true == 0) {
                    alert("중복조회를 해주세요");
                    return false;
                }
    <?php
}
?>
        });
    });
    function remove_file() {

        $("span[name='file_link']").remove();
        $("input[name='remove_button']").remove();
        $("input[name='file_del']").val(1);

    }

    function delete_admin_user(did) {
        if (confirm("정말 삭제하시겠습니까??") == true) {
            location.href = '<?php echo 'submit.php?id='; ?>' + did + '<?php echo '&mod=delete'; ?>';
        } else {
            return false;
        }
    }
    $(".modal-dialog").keydown(function (event){
    if (event.keyCode == '13') {
        if (window.event) {
            event.preventDefault();
            return;
        }
    }
 });
 
 //값 체크
function book_create_submit_check(){
    //교재명
    if(local_management_value_null_check('input[name=title]') == false){
        alert('<?php echo get_string('book:valtitle','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=vi_title]') == false){
        alert('<?php echo get_string('book:valtitle','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=en_title]') == false){
        alert('<?php echo get_string('book:valtitle','local_management')?>');
        return false;
    }
    //교재코드
//    if(local_management_value_null_check('input[name=code]') == false){
//        alert('교재코드를 입력해주세요.');
//        return false;
//    }
    //출판사
    if(local_management_value_null_check('#publisher') == false){
        alert('<?php echo get_string('book:valpublisher','local_management')?>');
        return false;
    }
    //저작권
    if(local_management_value_null_check('#copyright') == false){
        alert('<?php echo get_string('goods:valcopy','local_management')?>');
        return false;
    }
    //저자
    if(local_management_value_null_check('input[name=author]') == false){
        alert('<?php echo get_string('book:valauthor','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=vi_author]') == false){
        alert('<?php echo get_string('book:valauthor','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=en_author]') == false){
        alert('<?php echo get_string('book:valauthor','local_management')?>');
        return false;
    }
    //발행일
    if(local_management_value_null_check('input[name=publisheddate]') == false){
        alert('<?php echo get_string('book:valdate','local_management')?>');
        return false;
    }
    //금액
    if(local_management_value_null_check('input[name=price]') == false){
        alert('<?php echo get_string('goods:valprice','local_management')?>');
        return false;
    }
    //교재이미지
    if(local_management_value_null_check('input[name=script]') == false && $('#file').length == false){
        alert('<?php echo get_string('goods:valimg','local_management')?>');
        return false;
    }
    //소개
    if(CKEDITOR.instances.text.getData() == ''){
        alert('<?php echo get_string('goods:valintro','local_management')?>');
        return false;
    }
    if(CKEDITOR.instances.vi_text.getData() == ''){
        alert('<?php echo get_string('goods:valintro','local_management')?>');
        return false;
    }
    if(CKEDITOR.instances.en_text.getData() == ''){
        alert('<?php echo get_string('goods:valintro','local_management')?>');
        return false;
    }
    //사용여부
    if($('input:radio[name=isused]').is(':checked') == false){
        alert('<?php echo get_string('goods:valisused','local_management')?>');
        return false;
    }
    return true;
}
</script>