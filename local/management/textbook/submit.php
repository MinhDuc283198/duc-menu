<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot . '/local/management/lib/contents_lib.php';
require_once $CFG->dirroot . '/lib/form/filemanager.php';
require_once $CFG->dirroot . '/lib/filelib.php';
require_once $CFG->dirroot . '/lib/phpexcel/PHPExcel/Shared/PCLZip/pclzip.lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
  $SESSION->wantsurl = (string) new moodle_url('submit.php');
  redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);
$fs = get_file_storage();
$id = optional_param("id", 0, PARAM_INT);

$booknew = new stdClass();
$copy = new stdClass();
$copy->userid = $USER->id;
$copy->timecreated = time();   
$copy->category = 'book';

foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $booknew->$key = $val;
}
$booknew->price = str_replace(',','',$booknew->price);
//$booknew->cata1 = $cata1;
//if($cata2){
//    $booknew->category = $cata2;
//}else{
//    $booknew->category = $cata1;
//}
$booknew->publisheddate = str_replace(".", "-", $booknew->publisheddate); 
$booknew->publisheddate = strtotime($booknew->publisheddate);

if(!$id){
    //코드 가져오기
    $ordernum = $DB->get_record('lmsdata_textbook', array('year'=>date('y',time())),'max(ordernum) as max');
    $booknew->ordernum = $ordernum->max+1;
    $booknew->year = date('y',time());

    //코드 중복체크
    $booknew->code = $booknew->year.'T'.sprintf("%04d",$booknew->ordernum);
    //코드 중복체크
    $overlap_cd1 = $DB->get_record('lmsdata_course', array('coursecd'=>$booknew->code));
    $overlap_cd2 = $DB->get_record('lmsdata_class', array('code'=>$booknew->code));
    $overlap_cd3 = $DB->get_record('lmsdata_textbook', array('code'=>$booknew->code));
    if($overlap_cd1 || $overlap_cd2 || $overlap_cd3){
        echo "코드가 중복되었습니다.";
        exit;
    }
    $booknew->timecreated = time();
    $booknew->userid = $USER->id;
    $newconid = $DB->insert_record('lmsdata_textbook', $booknew);
    //-저작권 / 출판사 저장
    if($newconid){
        $copy->dataid = $newconid;
        $copy->copyid = $booknew->copyright;
        $newcopy =  $DB->insert_record('lmsdata_copyright_set', $copy);
        $copy->copyid =$booknew->publisher;
        $newpub =  $DB->insert_record('lmsdata_copyright_set', $copy);
    }
    //-
    if(!empty($_FILES['script']['tmp_name'])){
        $file_record = array(
                            'contextid'   => $context->id,
                            'component'   => 'local_lmsdata',
                            'filearea'    => 'textbook',
                            'itemid'      => $newconid,
                            'filepath'    => '/',
                            'filename'    => $_FILES['script']['name'],
                            'timecreated' => time(),
                            'timemodified'=> time(),
                            'userid'      => $USER->id,
                            'author'      => fullname($USER),
                            'license'     => 'allrightsreserved',
                            'sortorder'   => 0
                        );
        //print_object($_FILES['script']['tmp_name']);exit;
        $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['script']['tmp_name']);
    }
}else{
    //코드 중복체크
    $overlap_cd1 = $DB->get_record('lmsdata_course', array('coursecd'=>$booknew->code));
    $overlap_cd2 = $DB->get_record('lmsdata_class', array('code'=>$booknew->code));
    $overlap_cd3 = $DB->get_record('lmsdata_textbook', array('code'=>$booknew->code));
    if($overlap_cd1 || $overlap_cd2 || $overlap_cd3){
        if($overlap_cd3->id != $id){
            echo "코드가 중복되었습니다.";
            exit;
        }
    }
    $booknew->timemodified = time();
    $booknew->id = $id;
    $DB->update_record('lmsdata_textbook', $booknew);
    
    $logdata = new stdClass();
    $logdata->productid = $id;
    $logdata->userid = $USER->id;
    $logdata->timecreated = time();
    $logdata->type = 0;

    $DB->insert_record('lmsdata_product_log', $logdata);
    //-저작권 / 출판사 저장
    if($booknew->pub_setid){
        $copy->id = $booknew->pub_setid;
        $copy->copyid = $booknew->publisher;
        $DB->update_record('lmsdata_copyright_set', $copy);
    }
    if ($booknew->copy_setid){
        $copy->id = $booknew->copy_setid;
        $copy->copyid = $booknew->copyright;
        $DB->update_record('lmsdata_copyright_set', $copy);
    }
    //-
    if(!($_FILES['script']['name']==null || $_FILES['script']['name']=='')){
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'textbook', $id, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
        
        if (!empty($_FILES['script']['tmp_name'])) {
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_lmsdata',
                'filearea' => 'textbook',
                'itemid' => $id,
                'filepath' => '/',
                'filename' => $_FILES['script']['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
            );
            $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['script']['tmp_name']);
        }
    }
}


echo '<script type="text/javascript">document.location.href="./index.php"</script>';
