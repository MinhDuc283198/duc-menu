<?php

define('LMSDATA_CLASS_DRIVE_EXECUTE', 0);
define('LMSDATA_CLASS_MERGE_EXECUTE', 2);
define('LMSDATA_CLASS_DELETE_EXECUTE', 4);
define('SYSTEM_START_YEAR', 2013);
define('SYSTEM_MAX_WEEK', 5);
define('ENROL_MENTOR_SUBJECT', 'MED7185');

define('JEI_USER_TYPECODE_10', '임직원');
define('JEI_USER_TYPECODE_20', '멘토');
define('JEI_USER_TYPECODE_30', '교육생');

require_once($CFG->dirroot . '/local/management/lib/paging.php');

/**
 * 
 * @global type $CFG
 */
function lmsdata_get_years() {
    global $DB;

    $current = date("Y");


    $max = $current + 2;
    $min = 2018;
    $years = array();
    for ($i = $max; $i >= $min; $i--) {
        $years[$i] = $i;
    }

    return $years;
}

function lmsdata_get_mons($mo = "") {
    for ($m = 1; $m <= 12; $m++) {
        if (strlen($m) == 1) {
            $m = "0" . $m;
        }
        if ($m == $mo) {
            $date_month .= "<option value='$m' selected>$m</option>\n";
        } else {
            $date_month .= "<option value='$m'>$m</option>\n";
        }
    }
    return $date_month;
}

function lmsdata_get_days($da) {
    for ($d = 1; $d <= 31; $d++) {
        if (strlen($d) == 1) {
            $d = "0" . $d;
        }
        if ($d == $da) {
            $date_day .= "<option value='$d' selected>$d</option>\n";
        } else {
            $date_day .= "<option value='$d'>$d</option>\n";
        }
    }
    return $date_day;
}

function lmsdata_get_terms() {
//    $currentlang = current_language();
//    if($currentlang != 'ko') {
//        return array(
//            '1' => '1st', // 1st Semester
//            '2' => '2nd' // 2nd Semester
//        );
//    } else {
//    }
    return array(
        //     '0' => '비정규과목',
        '1' => '1학기',
        '2' => '2학기',
        '3' => '여름학기',
        '4' => '겨울학기'
    );
}

/**
 * 수강대상자 불러오기
 * @return array
 */
function lmsdata_get_object() {
//    $currentlang = current_language();
//    if($currentlang != 'ko') {
//        return array(
//            '1' => '1st', // 1st Semester
//            '2' => '2nd' // 2nd Semester
//        );
//    } else {
//    }
    return array(
        //     '0' => '비정규과목',
        '1' => '임직원',
        '2' => '멘토',
        '3' => '교육생',
        '4' => '모두'
    );
}

function lmsdata_get_terms2() {
//    $currentlang = current_language();
//    if($currentlang != 'ko') {
//        return array(
//            '1' => '1st', // 1st Semester
//            '2' => '2nd' // 2nd Semester
//        );
//    } else {
//    }
    return array(
        '01' => '정규1학기',
        '110' => '비정규 과목1학기',
        '111' => '미래융합 비정규 과목 1학기',
        '03' => '정규여름학기',
        '130' => '비정규 과목 여름학기',
        '131' => '미래융합 비정규 과목 여름학기',
        '02' => '정규2학기',
        '120' => '비정규 과목 2학기',
        '121' => '미래융합 비정규 과목 2학기',
        '04' => '정규겨울학기',
        '140' => '비정규 과목 겨울학기',
        '141' => '미래융합 비정규 과목 겨울학기',
    );
}

function lmsdata_get_hyears() {
    return array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4'
    );
}

function lmsdata_get_terms_sync() {
    $terms = array(
        '1' => '1 학기',
        '2' => '2 학기',
        '3' => '여름학기',
        '4' => '겨울학기'
    );

    return $terms;
}

function lmsdata_get_sync_tabs() {
    return array(
        array(
            'class' => 'black_btn',
            'text' => '사용자',
            'page' => 'user'),
        array(
            'class' => 'black_btn',
            'text' => '강의',
            'page' => 'course'),
        array(
            'class' => 'black_btn',
            'text' => '강의참여자',
            'page' => 'participant'),
//        array(
//            'class' => 'black_btn',
//            'text'  => '학사일정',
//            'page'  => 'schedule'),
//        array(
//            'class' => 'black_btn',
//            'text'  => '연구내역',
//            'page'  => 'research'),
        array(
            'class' => 'red_btn',
            'text' => '설정',
            'page' => 'config')
    );
}

/**
 * course에 등록된 userid, roleid 목록 반환
 * @param array $course course array, value = courseid 
 * @return {role_assignments} stdClass
 */
function get_courses_role_assignments($course) {
    global $DB;

    list($sql_in, $assign_params) = $DB->get_in_or_equal($course, SQL_PARAMS_NAMED, 'id');

    $sql_where = " WHERE instanceid " . $sql_in;

    $assign_params['contextlevel'] = CONTEXT_COURSE;

    $sql_select = "SELECT ra.id, ra.roleid, ra.userid, ra.modifierid, ro.shortname, ct.instanceid ";
    $sql_from = " FROM {role_assignments} ra 
                     JOIN {role} ro on ro.id = ra.roleid
                     JOIN (
                            SELECT * FROM {context} " . $sql_where . "  and contextlevel = :contextlevel
                          )ct ON ra.contextid = ct.id ";

    $assign_users = $DB->get_records_sql($sql_select . $sql_from, $assign_params);

    return $assign_users;
}

/**
 * 과정 enrol
 * @global type $CFG
 * @global type $PAGE
 * @global type $DB
 * @param type $course
 * @param type $user
 * @return boolean
 */
function set_assign_user($course, $user) {
    global $CFG, $PAGE, $DB, $USER;

    require_once("$CFG->dirroot/enrol/locallib.php");
    require_once($CFG->dirroot . '/lib/enrollib.php');

    if (!is_object($course)) {
        $course = $DB->get_record('course', array('id' => $course));
    }

    //권한문제로 처리
    $change = false;
    if (!is_siteadmin()) {
        $USER->id = 2;
        $change = true;
    }
    $manager = new course_enrolment_manager($PAGE, $course);
    if ($ues = $manager->get_user_enrolments($user->userid)) {
        $manager->assign_role_to_user($user->roleid, $user->userid);
    } else {
        $enrol = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $course->id));
        $timestart = 0;

        if ($user->shortname == 'student') {
            $timestart = $course->startdate - 604800;
        }

        $timeend = 0;

        $instances = $manager->get_enrolment_instances();
        $plugins = $manager->get_enrolment_plugins();

        $instance = $instances[$enrol->id];
        $plugin = $plugins[$instance->enrol];

        $plugin->enrol_user($instance, $user->userid, $user->roleid, $timestart, $timeend);
    }

    if ($change) {
        $USER->id = $user->userid;
    }
    return true;
}

/**
 * enrol 해제
 * @global type $CFG
 * @global type $PAGE
 * @global type $DB
 * @param type $course
 * @param type $userid
 * @param type $roleid
 * @return boolean
 */
function set_unassign_user($course, $userid, $roleid) {
    global $CFG, $PAGE, $DB, $USER;
    require_once("$CFG->dirroot/enrol/locallib.php");

    if (!is_object($course)) {
        $course = $DB->get_record('course', array('id' => $course));
    }
    //권한문제로 처리
    $change = false;
    if (!is_siteadmin()) {
        $USER->id = 2;
        $change = true;
    }
    $manager = new course_enrolment_manager($PAGE, $course);
    $manager->unassign_role_from_user($userid, $roleid);
    if (!$manager->get_user_roles($userid)) { // 권한이 하나도 없으면 등록을 해지한다.
        $ues = $manager->get_user_enrolments($userid);
        foreach ($ues as $ue) {
            $manager->unenrol_user($ue);
        }
    }
    if ($change) {
        $USER->id = $userid;
    }

    return true;
}

function lmsdata_get_submission_assign_count($courses, $userid = 0) {
    global $DB, $USER;

    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return 0;
    }

    list($coursessql, $params) = $DB->get_in_or_equal(array_keys($courses), SQL_PARAMS_NAMED, 'c0');

    if (!empty($userid)) {
        $params['userid'] = $userid;
    } else {
        $params['userid'] = $USER->id;
    }

    $submitted_sql = " SELECT ai.id as assignid, su.status as submitted  
                       FROM {assign} ai
                       JOIN {course_modules} cm ON ai.id = cm.instance AND ai.course = cm.course AND cm.module = 1
                       LEFT JOIN 
                            (SELECT * FROM {assign_submission} WHERE userid = :userid ) su on ai.id = su.assignment
                       WHERE cm.visible = 1 AND ai.course $coursessql ";

    $submitted_arr = $DB->get_records_sql($submitted_sql, $params);
    if (!empty($submitted_arr)) {
        $assign_count = 0;
        $submitted_count = 0;
        foreach ($submitted_arr as $value) {
            if (!empty($value->assignid)) {
                $assign_count++;
            }
            if (!empty($value->submitted)) {
                $submitted_count++;
            }
        }
        $count = (int) ($assign_count) - (int) ($submitted_count);
    } else {
        $count = 0;
    }
    return $count;
}

function enrol_get_my_courses_by_my_info($total = NULL, $param = array(), $page = 1, $limit = 10, $fields = NULL, $sort = 'visible DESC,sortorder ASC') {
    global $DB, $USER;

    // Guest account does not have any courses
    if (isguestuser() or ! isloggedin()) {
        return(array());
    }

    $basefields = array('id', 'category', 'sortorder',
        'shortname', 'fullname', 'idnumber',
        'startdate', 'visible',
        'groupmode', 'groupmodeforce');

    if (empty($fields)) {
        $fields = $basefields;
    } else if (is_string($fields)) {
        // turn the fields from a string to an array
        $fields = explode(',', $fields);
        $fields = array_map('trim', $fields);
        $fields = array_unique(array_merge($basefields, $fields));
    } else if (is_array($fields)) {
        $fields = array_unique(array_merge($basefields, $fields));
    } else {
        throw new coding_exception('Invalid $fileds parameter in enrol_get_my_courses()');
    }
    if (in_array('*', $fields)) {
        $fields = array('*');
    }

    $orderby = "";
    $sort = trim($sort);
    if (!empty($sort)) {
        $rawsorts = explode(',', $sort);
        $sorts = array();
        foreach ($rawsorts as $rawsort) {
            $rawsort = trim($rawsort);
            if (strpos($rawsort, 'c.') === 0) {
                $rawsort = substr($rawsort, 2);
            }
            $sorts[] = trim($rawsort);
        }
        $sort = 'c.' . implode(',c.', $sorts);
        $orderby = "ORDER BY $sort";
    }

    $wheres = array("c.id <> :siteid");
    $params = array('siteid' => SITEID);

    if (isset($USER->loginascontext) and $USER->loginascontext->contextlevel == CONTEXT_COURSE) {
        // list _only_ this course - anything else is asking for trouble...
        $wheres[] = "courseid = :loginas";
        $params['loginas'] = $USER->loginascontext->instanceid;
    }

    $coursefields = 'c.' . join(',c.', $fields);
    list($ccselect, $ccjoin) = context_instance_preload_sql('c.id', CONTEXT_COURSE, 'ctx');

    if (!empty($param['year'])) {
        $params['year'] = $param['year'];
        $wheres = array(" ci.shyy = :year ");
    }
    if (!empty($param['term'])) {
        $params['term'] = $param['term'];
        $wheres = array(" ci.shtm_dcd = :term ");
    }
    if (!empty($param['searchval'])) {
        $params['searchval'] = '%' . $param['searchval'] . '%';
        $sql_like = $DB->sql_like('c.fullname', ':searchval', false);
        $wheres = array($sql_like);
    }

    $wheres = implode(" AND ", $wheres);
    //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
    $sql = "SELECT $coursefields $ccselect 
              FROM {course} c
              JOIN (SELECT DISTINCT e.courseid
                      FROM {enrol} e
                      JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                     WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                   ) en ON (en.courseid = c.id)
           $ccjoin
             WHERE $wheres
          $orderby";
    $params['userid'] = $USER->id;
    $params['active'] = ENROL_USER_ACTIVE;
    $params['enabled'] = ENROL_INSTANCE_ENABLED;
    $params['now1'] = round(time(), -2); // improves db caching
    $params['now2'] = $params['now1'];
    if ($total == 'all') {
        $courses = $DB->get_records_sql($sql, $params);
    } else if ($total == null) {
        $courses = $DB->get_records_sql($sql, $params, jino_offset_lmsdata($page, $limit), $limit);
    }
    // preload contexts and check visibility
    foreach ($courses as $id => $course) {
        context_instance_preload($course);
        if (!$course->visible) {
            if (!$context = get_context_instance(CONTEXT_COURSE, $id)) {
                unset($courses[$id]);
                continue;
            }
            if (!has_capability('moodle/course:viewhiddencourses', $context)) {
                unset($courses[$id]);
                continue;
            }
        }
        $courses[$id] = $course;
    }

    return $courses;
}

function jino_offset_lmsdata($page, $limit) {
    return $limit * ($page - 1);
}

/**
 * @param int $standard courseid
 * @param int $subject_list courseid
 * @param int $type 몰아넣기(0), 되돌리기(1), 분반몰아넣기(2), 교수자 강의삭제(3)
 * @param array $invisible subject_list의 course를 학생들에게 보여줄것인가? 보임(0), 숨김(1))
 * 
 * @return int lmsdata_class_drive_log table id
 */
function add_class_drive_log($standard, $subject_id, $type, $invisible = 0) {
    global $DB, $USER;

    $class = new stdClass();

    $class->standard_id = $standard;
    $class->subject_id = $subject_id;
    $class->user_id = $USER->id;
    $class->invisible = $invisible;
    $class->type = $type;
    $class->timecreated = time();
    $class->timerestore = time();

    return $DB->insert_record('lmsdata_class_drive_log', $class);
}

/**
 * lmsdata_course->hyear 에 있는 연속적인 학년값을 /로 구분하여 반환
 * @param string $hyear
 * 
 * @return string $hyear_str
 */
function get_hyear_str($hyear) {
    $hyear_len = strlen($hyear);
    $temp = array();
    for ($i = 0; $i < $hyear_len; $i++) {
        $temp[] = substr($hyear, $i, 1);
    }

    $hyear_str = implode('/', $temp);
    return $hyear_str;
}

/**
 * lmsdata_course->lectype 에 있는 강의타입을 한글 string으로 반환
 * @param string $lectype
 * 
 * @return string $lectype_str
 */
function get_lectype_str($lectype) {
    $lectype_str;
    switch ($lectype) {
        case 1: // 학정번호
            $lectype_str = '강의';
            break;
        case 2: // 과정명
            $lectype_str = '실습';
            break;
        default:
            $lectype_str = '기타';
            break;
    }

    return $lectype_str;
}

/**
 * lmsdata_course->required_str 에 있는 종필을 한글 string으로 반환
 * @param string $required
 * 
 * @return string $required_str
 */
function get_required_str($required) {
    $required_str;
    switch ($required) {
        case 1: // 학정번호
            $required_str = '전필';
            break;
        case 2: // 과정명
            $required_str = '전선';
            break;
        case 3:
            $required_str = '기타';
            break;
    }

    return $required_str;
}

/**
 * 브라우저로 메시지를 출력한다.
 * 메시지 끝에 '<br/>'을 붙여서 줄바꿈흘 한다.
 * @param string $message
 */
function siteadmin_println($message) {
    echo $message . '<br/>' . "\n";

    siteadmin_flushdata();
}

/**
 * 출력 버퍼에 있는 내용을 브라우저로 보낸다.
 */
function siteadmin_flushdata() {
    while (ob_get_level() > 0) {
        ob_end_flush();
    }

    flush();

    ob_start();
}

function siteadmin_scroll_down() {
    echo '<script type="text/javascript">
    window.scrollTo(0, document.body.scrollHeight);
</script>';

    siteadmin_flushdata();
}

function get_weeknum($time) {
    $week = date('w', mktime(0, 0, 0, date('n', $time), 1, date('Y', $time)));
    return ceil(($week + date('j', $time) - 1) / 7);
}

function get_code_view($id) {
    global $DB;
    $sql = "select * from {lmsdata_code} where id=" . $id . "";
    return $DB->get_record_sql($sql);
}

function get_code_category() {
    global $DB;
    $sql = "select distinct id,code_category from {lmsdata_code} where depth=1";
    return $DB->get_records_sql($sql);
}

function get_code_category2($id) {
    global $DB;
    $sql = "select code_category from {lmsdata_code} where id=$id and depth=1";
    return $DB->get_field_sql($sql);
}

function get_parentid($category) {
    global $DB;
    $sql = "select id from {lmsdata_code} where code_category=" . $category . "";
    return $DB->get_records_sql($sql);
}

function code_category_check($code_category) {
    global $DB;
    return $DB->record_exists("lmsdata_code", array("code_category" => $code_category));
}

function code_check($code, $code_category) {
    global $DB;

    return $DB->record_exists("lmsdata_code", array("code" => $code, "depth" => 2, "code_category" => $code_category));
}

//function depth_check($id){
//    global $DB;
//    
//    $sql = "select depth from {lmsdata_code} where id=$id";
//
//    return $DB->get_field_sql($sql);
//}
function get_parent_tag($id) {
    global $DB;

    $sql = "select parentid from {lmsdata_code} where id=$id";

    return $DB->get_field_sql($sql);
}

/**
 * 기수생성 과정템플릿 복사
 * @global type $CFG
 * @global type $DB
 * @global type $USER
 * @param type $courseid
 * @param type $templateid
 * @param type $userid
 * @return boolean
 */
function edu_import_template($courseid, $templateid, $userid = 0) {
    global $CFG, $DB, $USER;

    if (!$userid) {
        $userid = $USER->id;
    }

    $restoretarget = backup::TARGET_CURRENT_DELETING;

    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $templatecourse = $DB->get_record('course', array('id' => $templateid), '*', MUST_EXIST);

    $bc = new backup_controller(backup::TYPE_1COURSE, $templatecourse->id, backup::FORMAT_MOODLE, backup::INTERACTIVE_YES, backup::MODE_IMPORT, $userid);
    $bc->get_plan()->get_setting('users')->set_status(backup_setting::LOCKED_BY_CONFIG);

    $params = array('importid' => $templatecourse->id, 'target' => $restoretarget);
    template_import_ui::skip_current_stage(false);
    $backup = new template_import_ui($bc, $params, backup_ui::STAGE_FINAL);

    $progress = new \core\progress\none();
    $progress->start_progress('', 2);
    $backup->get_controller()->set_progress($progress);

    $backupid = $backup->get_backupid();
    $tempdestination = $CFG->tempdir . '/backup/' . $backupid;

    // First execute the backup
    $backup->execute();
    $backup->destroy();
    unset($backup);

    $progress->progress(1);

//    siteadmin_println($backupid.'-'.$course->id.'-'.backup::INTERACTIVE_NO.'-'.backup::MODE_IMPORT.'-'.$userid.'-'.$restoretarget);

    $rc = new restore_controller($backupid, $course->id, backup::INTERACTIVE_NO, backup::MODE_IMPORT, $userid, $restoretarget);
//    siteadmin_println($rc->get_executiontime());
    $progress->start_progress('Restore process', 2);
    $rc->set_progress($progress);

    if ($rc->get_status() == backup::STATUS_REQUIRE_CONV) {
        $rc->convert();
    }

    // Execute prechecks
    $warnings = false;
    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults)) {
            if (!empty($precheckresults['errors'])) { // If errors are found, terminate the import.
                fulldelete($tempdestination);
                die();
            }
            if (!empty($precheckresults['warnings'])) { // If warnings are found, go ahead but display warnings later.
                $warnings = $precheckresults['warnings'];
            }
        }
    }
    if ($restoretarget == backup::TARGET_CURRENT_DELETING || $restoretarget == backup::TARGET_EXISTING_DELETING) {
        restore_dbops::delete_course_content($course->id);
    }
    // Execute the restore.
    $rc->execute_plan();
    $rc->destroy();

    // Delete the temp directory now
    fulldelete($tempdestination);

    // End restore section of progress tracking (restore/precheck).
    $progress->end_progress();

    // All progress complete. Hide progress area.
    $progress->end_progress();

    // Course Format은 안겨져와서 수동으로 바꿔줌.
    $course->format = $templatecourse->format;
    $formatoptions = $DB->get_records('course_format_options', array('courseid' => $templatecourse->id));
    foreach ($formatoptions as $formatoption) {
        $course->{$formatoption->name} = $formatoption->value;
    }
    course_get_format($course->id)->update_course_format_options($course);
    enrol_course_updated(true, $course, null);
    unset($rc);
    unset($formatoptions);
    unset($course);
    unset($progress);
    unset($restoretarget);
    unset($bc);

    return TRUE;
}

/**
 * 기수 생성시 게시글 복사
 * @global type $DB
 * @global type $CFG
 * @param type $courseid
 * @param type $templateid
 * @param type $learningstart
 */
function edu_import_board($courseid, $templateid, $learningstart) {
    global $DB, $CFG;
    require_once($CFG->libdir . '/filestorage/file_storage.php');
    //템플릿 게시판글 복사    
    if ($boards = $DB->get_records_sql("select jc.* from {jinotechboard_contents} jc join {jinotechboard} j on j.id = jc.board where jc.course = $templateid and j.type = 1 ")) {
        if ($jinotechboard = $DB->get_record('jinotechboard', array('course' => $courseid, 'type' => 1))) {
            foreach ($boards as $board) {
                $boardid = $board->board;
                $board->course = $courseid;
                $board->board = $jinotechboard->id;
                if (empty($learningstart)) {
                    $board->timecreated = $board->timecreated;
                    $board->timemodified = $board->timemodified;
                } else {
                    $board->timecreated = $learningstart;
                    $board->timemodified = $learningstart;
                }
                $jinotechboardid = $DB->insert_record('jinotechboard_contents', $board);
                //기존 게시물의 cm을 가져옴
                $templatecm = get_coursemodule_from_instance("jinotechboard", $boardid, $templateid);
                $context = context_module::instance($templatecm->id);
                $fs = get_file_storage();
                $files = $fs->get_area_files($context->id, 'mod_jinotechboard', "attachment", $board->id);
                //새로운 게시물의 cm을 가져옴
                $cm = get_coursemodule_from_instance("jinotechboard", $jinotechboard->id, $courseid);
                $context = context_module::instance($cm->id);

                //새로운 코스
                foreach ($files as $file) {
                    if ($file->get_filesize() > 0) {
                        $filename = $file->get_filename();
                        $content = $file->get_content();
                        $fileinfo = array(
                            'contextid' => $context->id, // lcms context id
                            'component' => 'mod_jinotechboard',
                            'filearea' => 'attachment',
                            'itemid' => $jinotechboardid,
                            'filepath' => '/',
                            'filename' => $filename
                        );
                        $fs->create_file_from_string($fileinfo, $content);
                    }
                }
            }
        }
    }


    if ($boards = $DB->get_records_sql("select jc.* from {jinotechboard_contents} jc join {jinotechboard} j on j.id = jc.board where jc.course = $templateid and j.type = 6 ")) {
        if ($jinotechboard = $DB->get_record('jinotechboard', array('course' => $courseid, 'type' => 6))) {
            foreach ($boards as $board) {
                $boardid = $board->board;
                $board->course = $courseid;
                $board->board = $jinotechboard->id;
                $jinotechboardid = $DB->insert_record('jinotechboard_contents', $board);
                //기존 게시물의 cm을 가져옴
                $templatecm = get_coursemodule_from_instance("jinotechboard", $boardid, $templateid);
                $context = context_module::instance($templatecm->id);
                $fs = get_file_storage();
                $files = $fs->get_area_files($context->id, 'mod_jinotechboard', "attachment", $board->id);
                //새로운 게시물의 cm을 가져옴
                $cm = get_coursemodule_from_instance("jinotechboard", $jinotechboard->id, $courseid);
                $context = context_module::instance($cm->id);

                //새로운 코스
                foreach ($files as $file) {
                    if ($file->get_filesize() > 0) {
                        $filename = $file->get_filename();
                        $content = $file->get_content();
                        $fileinfo = array(
                            'contextid' => $context->id, // lcms context id
                            'component' => 'mod_jinotechboard',
                            'filearea' => 'attachment',
                            'itemid' => $jinotechboardid,
                            'filepath' => '/',
                            'filename' => $filename
                        );
                        $fs->create_file_from_string($fileinfo, $content);
                    }
                }
            }
        }
    }
}

/**
 * 기수 생성시 블럭 세팅
 * @global type $CFG
 * @global type $DB
 * @param type $course
 */
function edu_blocks_add_default_course_blocks($course) {
    global $CFG, $DB;

    if (!empty($CFG->defaultblocks_override)) {
        $blocknames = blocks_parse_default_blocks_list($CFG->defaultblocks_override);
    } else if ($course->id == SITEID) {
        $blocknames = blocks_get_default_site_course_blocks();
    } else if (!empty($CFG->{'defaultblocks_' . $course->format})) {
        $blocknames = blocks_parse_default_blocks_list($CFG->{'defaultblocks_' . $course->format});
    } else {
        require_once($CFG->dirroot . '/course/lib.php');
        $blocknames = course_get_format($course)->get_default_blocks();
    }

    $page = new moodle_page();
    $page->set_course($course);

    if ($course->id == SITEID) {
        $pagetypepattern = 'site-index';
    } else {
        $pagetypepattern = '*';
    }

    $parentcontextid = $page->context->id;
    $showinsubcontexts = 1;

    foreach ($blocknames as $region => $regionblocks) {
        foreach ($regionblocks as $offset => $blockname) {
            if ($block = $DB->get_record('block_instances', array('blockname' => $blockname, 'parentcontextid' => $parentcontextid))) {
                $block->pagetypepattern = $pagetypepattern;
                $block->showinsubcontexts = 1;
                $DB->update_record('block_instances', $block);
            }
        }
    }

    // $page->blocks->add_blocks($blocknames, $pagetypepattern, null, true);
}

/**
 * 연수일정 파일 업로드 함수
 * @param type $file_del
 * @param type $contextid
 * @param type $filearea
 * @param type $id
 * @param type $itemid
 */
function edu_courselist_file_upload($file_del, $contextid, $filearea, $id, $itemid = 0) {

    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($id && (!($_FILES["$filearea"]['name'] == null || $_FILES["$filearea"]['name'] == '') || $file_del == 1)) {
        $overlap_files = $fs->get_area_files($contextid, 'local_courselist', "$filearea", $itemid, 'id');
        foreach ($overlap_files as $file) {
            if ($file->get_filesize() > 0) {
                if ($file_del) {
                    $filename = $file->get_filename();
                    $file->delete();
                }
            }
        }
    }

    if (!empty($_FILES["$filearea"]['tmp_name'])) {

        $file_record = array(
            'contextid' => $contextid,
            'component' => 'local_courselist',
            'filearea' => "$filearea",
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $_FILES["$filearea"]['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $storage_id = $fs->create_file_from_pathname($file_record, $_FILES["$filearea"]['tmp_name']);
    }
}

/**
 * 연수일정 파일 가져오고 삭제하는 함수
 * @global type $CFG
 * @global type $OUTPUT
 * @param type $contextid
 * @param type $filearea
 * @param type $itemid
 * @param type $type
 * @return string
 */
function edu_courselist_get_file($contextid, $filearea, $itemid = 0, $type) {
    global $CFG, $OUTPUT;

    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_courselist', "$filearea", $itemid, 'id');
    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_courselist/' . $filearea . '/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            if ($type == 1) {
                $fileobj = "<span id ='file' name='file_link_1'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_1' value='삭제' onclick='remove_file_1()'/></span>";
            } else if ($type == 2) {
                $fileobj = "<span id ='file' name='file_link_2'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_2' value='삭제' onclick='remove_file_2()'/></span>";
            } else {
                $fileobj = "<span id ='file' name='file_link'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button' value='삭제' onclick='remove_file()'/></span>";
            }
        }
    }
    return $fileobj;
}

function edu_courselist_get_file_img($contextid, $filearea, $itemid = 0) {
    global $CFG, $OUTPUT;

    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_courselist', "$filearea", $itemid, 'id');
    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_courselist/' . $filearea . '/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            $filepath .= $path;
        }
    }
    return $filepath;
}

/**
 * 강사 enrol 시키는 함수
 * @global type $CFG
 * @global type $DB
 * @global type $USER
 * @param type $courseid
 * @param type $templateid
 * @return boolean
 */
function edu_import_teacher($courseid, $templateid) {
    global $CFG, $DB, $USER;

    $templateteacher = $DB->get_records('lmsdata_course_teacher', array('courseid' => $templateid));
    $course = $DB->get_record('course', array('id' => $courseid));
    $role = $DB->get_record('role', array('shortname' => 'teacher'), 'id, shortname');
    foreach ($templateteacher as $templateteachers) {
        $user = $DB->get_record('lmsdata_user', array('id' => $templateteachers->lmsuserid));
        $user->roleid = $role->id;
        set_assign_user($course, $user);
    }
    return TRUE;
}

function edu_control_admincategory() {
    global $DB, $USER, $CFG;
    $configadmin = $DB->get_record('config', array('name' => siteadmins));
    $adminlist = explode(',', $configadmin->value);

    $super = $adminlist[0];
    unset($adminlist[0]);
    if ($USER->id == $super) {
        $auth = 'super';
    } else if (in_array($USER->id, $adminlist)) {
        $auth = 'sub';
    } else {
        $auth = 'user';
    }
    $pages = ['/chamktu/users/', '/chamktu/support/', '/chamktu/stats/', '/chamktu/board/'];
    $request_uri = $_SERVER['REQUEST_URI'];
    foreach ($pages as $page) {
        if ($auth != 'super' && (strpos($request_uri, $page) !== false)) {
            redirect($CFG->wwwroot);
        }
    }
    return $auth;
}

/**
 * qna게시판 추가
 * not used
 * @global type $DB
 * @param type $newcourse
 * @param type $board
 * @return type
 */
function create_jinotechboard_course_modules($newcourse, $board) {
    global $DB, $CFG;

//    require_once($CFG->dirroot . '/lib/accesslib.php');
//    require_once($CFG->dirroot . '/course/modlib.php');
//    require_once($CFG->dirroot . '/course/modlib.php');


    //질문답변(qna)게시판 추가
//        $data->allownotice = 0;
//        $data->allowreply = 1;
//        $data->allowcomment = 0;
//        $data->allowsecret = 1;
//        $data->allowrecommend = 0;
//        $data->allowgigan = 0;
//        $data->name = '과정Q&A';
//        $data->intro = '<p>질문 답변게시판입니다.</p>';
//        $data->type = 2;
//        $data->course = $co$newcourseurse->id;
//         create_module($data);

//    $modules = $DB->get_record('modules', array('name' => 'jinotechboard'), 'id');
//
//    $course_modules = new stdClass();
//    $course_modules->course = $newcourse->id;
//    $course_modules->instance = $board;
//    $course_modules->module = $modules->id;
//    $course_modules->modulename = 'jinotechboard';
//    $course_modules->section = 1;
//    $course_modules->added = time();
//    $course_modules->score = 0;
//    $course_modules->indent = 0;
//    $course_modules->visible = 1;
//    $course_modules->visibleoncoursepage = 1;
//    $course_modules->groupmode = 0;
//    $course_modules->groupingid = 0;
//    $course_modules->completion = 0;
//    $course_modules->deletioninprogress = 0;
//
//    $course_modules->completionview = 0;
//    $course_modules->completionexpected = 0;
//    $course_modules->showdescription = 0;
////    $moduleinfo = add_moduleinfo($moduleinfo, $course, null);
////    $course_module_getid = create_module($course_modules);
//    $course_module_getid = add_moduleinfo($course_modules, $newcourse, null);
//
////    $course_module_id = $DB->insert_record('course_modules', $course_modules);
////    
////    $section = $DB->get_record('course_sections', array('section'=>0, 'course'=>$newcourse->id));
////    $update_section = new stdClass();
////    $update_section->id = $section->id;
////    $update_section->sequence = $section->sequence.','.$course_module_id;
////    $result_section = $DB->update_record('course_sections', $update_section);
//
//    context_module::instance($course_module_getid->id);
}


/**
 * 관리 페이지에 접근 권한이 있는지 체크
 * 사용자가 시스템 관리 그룹에 속해있거나, 접근할 수 있는 메뉴가 있으면 true를 반환한다.
 * 
 * @global object $CFG
 * @global object $DB
 * @global object $USER
 * @param int $userid
 * @return boolean
 */
function local_management_has_role($userid = 0) {
    global $CFG, $DB, $USER;
    
    if(!$userid) {
        $userid = $USER->id;
    }
    
    require_once ($CFG->dirroot . '/local/management/role/lib.php');
    
    // 시스템 관리 그룹에 속해 있으면 
    if(local_management_role_is_group_member(LOCAL_MANAGEMENT_GROUP_SYSADMIN, $userid)) {
        return true;
    }
    
    // 사용자가 속한 그룹과 사용자에게 지정된 메뉴
    $menus = $DB->count_records_sql("SELECT COUNT(m.id)
        FROM {management_menu} m
        JOIN {management_menu} p ON m.lft BETWEEN p.lft AND p.rgt
        WHERE p.id IN (SELECT mu.menuid
                       FROM {management_menu_user} mu 
                       WHERE mu.userid = :userid1
                       UNION
                       SELECT mg.menuid
                       FROM {management_menu_group} mg
                       JOIN {management_group_member} gm ON gm.groupid = mg.groupid
                       WHERE gm.userid = :userid2)
          AND m.rgt = m.lft + 1", array('userid1' => $userid, 'userid2' => $userid));
    
    return $menus > 0;
}

/**
 * 메뉴에 사용자가 접근 권한이 있는지 체크
 * 
 * @global object $CFG
 * @global object $DB
 * @global object $USER
 * @param string $menu 메뉴의 Identification(shortname)
 * @param int $userid
 * @return boolean
 */
function local_management_has_role_menu($menu, $userid = 0) {
    global $CFG, $DB, $USER;
    
    if(!$userid) {
        $userid = $USER->id;
    }
    
    require_once ($CFG->dirroot . '/local/management/role/lib.php');
    
    // 시스템 관리 그룹에 속해 있으면 
    if(local_management_role_is_group_member(LOCAL_MANAGEMENT_GROUP_SYSADMIN, $userid)) {
        return true;
    }
    
    // 사용자가 지정된 메뉴에 접근 권한이 있는지 체크
    return $DB->record_exists_sql("SELECT 1
        FROM {management_menu} m
        JOIN {management_menu} p ON m.lft BETWEEN p.lft AND p.rgt
        WHERE p.id IN (SELECT mu.menuid
                       FROM {management_menu_user} mu 
                       WHERE mu.userid = :userid1
                       UNION
                       SELECT mg.menuid
                       FROM {management_menu_group} mg
                       JOIN {management_group_member} gm ON gm.groupid = mg.groupid
                       WHERE gm.userid = :userid2)
          AND m.shortname = :shortname", 
            array('userid1' => $userid, 'userid2' => $userid, 'shortname' => $menu));
}

/**
 * 메뉴의 최하위 노드를 리턴한다.
 * 
 * @param \local_management\menu $menu
 * @return \local_management\menu
 */
function local_management_leaf_child_menu(\local_management\menu $menu) {
    if($menu->has_children()) {
        return local_management_leaf_child_menu($menu->children[0]);
    }
    
    return $menu;
}

/**
 * $url 이 $CFG->wwwroot 로 시작하지 않는 경우 $CFG->wwwroot 를 붙여준다.
 * 
 * @global object $CFG
 * @param string $url
 * @return string
 */
function local_management_make_absolute($url) {
    global $CFG;
    
    // Return $CFG->wwwroot if no url
    if(!$url) {
        return $CFG->wwwroot;
    }

    // Return if already absolute URL
    if(parse_url($url, PHP_URL_SCHEME) != '') {
        return $url;
    }
    
    // Url only containing query or anchor
    if($url[0] == '#' || $url[0] == '?') {
        return $CFG->wwwroot . $url;
    }
    
    if($url[0] != '/') {
        $url = '/' . $url;
    }
    
    return $CFG->wwwroot . $url;
}

/**
 *  js 디렉토리에 있는 모든 .js 파일들
 * 
 * @global object $CFG
 * @return array
 */
function local_management_load_js() {
    global $CFG;
    
    $js = array();
    
    $dir = getcwd();
    
    $dirmanagement = $CFG->dirroot.'/local/management/';
    
    $files = local_management_load_files($dirmanagement.'js', 'js');
    if($files) {
        $js = array_merge($js, $files);
    }
    
    $subdirs = explode('/', substr($dir, strlen($dirmanagement)));
    foreach($subdirs as $subdir) {
        if(!empty($subdir)) {
            $dirmanagement .= $subdir . '/';
            $files = local_management_load_files($dirmanagement.'js', 'js');
            if($files) {
                $js = array_merge($js, $files);
            }
        }
    }
    
    array_walk($js, 'local_management_trim_dirroot');
    
    return $js;
}

/**
 * css 디렉토리에 있는 모든 .css 파일들
 * 
 * @global object $CFG
 * @return array
 */
function local_management_load_css() {
    global $CFG;
    
    $css = array();
    
    $dir = getcwd();
    
    $dirmanagement = $CFG->dirroot.'/local/management/';
    
    $files = local_management_load_files($dirmanagement.'css', 'css');
    if($files) {
        $css = array_merge($css, $files);
    }
    
    $subdirs = explode('/', substr($dir, strlen($dirmanagement)));
    foreach($subdirs as $subdir) {
        if(!empty($subdir)) {
            $dirmanagement .= $subdir . '/';
            $files = local_management_load_files($dirmanagement.'css', 'css');
            if($files) {
                $css = array_merge($css, $files);
            }
        }
    }
    
    array_walk($css, 'local_management_trim_dirroot');
    
    return $css;
}

/**
 * 문자 앞의 $CFG->dirroot를 제거한다.
 * 
 * @global object $CFG
 * @param string $str
 */
function local_management_trim_dirroot(&$str) {
    global $CFG;
    
    $str = substr($str, strlen($CFG->dirroot));
}

/**
 * 디렉토리에 있는 파일 목록을 배열로 반환한다.
 * 
 * @param string $dir 디렉토리
 * @param string $ext 파일 확장자
 * @return array|boolean
 */
function local_management_load_files($dir, $ext) {
    if(file_exists($dir) && is_dir($dir)) {
        return glob("$dir/*.$ext");
    }
    
    return false;
}
//
///**
// * 연수일정 파일 가져오고 삭제하는 함수
// * @global type $CFG
// * @global type $OUTPUT
// * @param type $contextid
// * @param type $filearea
// * @param type $itemid
// * @param type $type
// * @return string
// */
//function edu_courselist_get_file($contextid, $filearea, $itemid = 0, $type) {
//    global $CFG, $OUTPUT;
//
//    $fs = get_file_storage();
//    $files = $fs->get_area_files($contextid, 'local_courselist', "$filearea", $itemid, 'id');
//    $fileobj = '';
//    foreach ($files as $file) {
//        $filename = $file->get_filename();
//        $mimetype = $file->get_mimetype();
//        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
//
//        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_courselist/' . $filearea . '/' . $itemid . '/' . $filename);
//        if ($file->get_filesize() > 0) {
//            if ($type == 1) {
//                $fileobj = "<span id ='file' name='file_link_1'><a href=\"$path\">$iconimage</a>";
//                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
//                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_1' value='삭제' onclick='remove_file_1()'/></span>";
//            } else if ($type == 2) {
//                $fileobj = "<span id ='file' name='file_link_2'><a href=\"$path\">$iconimage</a>";
//                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
//                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_2' value='삭제' onclick='remove_file_2()'/></span>";
//            } else {
//                $fileobj = "<span id ='file' name='file_link'><a href=\"$path\">$iconimage</a>";
//                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
//                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button' value='삭제' onclick='remove_file()'/></span>";
//            }
//        }
//    }
//    return $fileobj;
//}
function local_management_date_form($date){
    return date("Y.m.d", $date);
}

function local_management_return_user_info($userid, $type = 1){
    global $DB;
    
    $query = "select u.id, u.username,  GROUP_CONCAT(u.firstname , u.lastname) as fullname   
                from {user} u
                join {lmsdata_user} lu ON lu.userid = u.id
                where u.id = :userid";
    $user = $DB->get_record_sql($query, array('userid' => $userid));
    
    if($type == 1){
        return $user->username;
    }else if($type == 2){
        return $user->fullname;
    }
}