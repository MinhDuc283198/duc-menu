<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

$id = optional_param('id', 0, PARAM_INT); // 게시판 타입
$site = optional_param('site', 1, PARAM_INT); // 이러닝 구인구직개인 구인구직기업
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);


$name = get_string('board:clist','local_management');
$boardname = 'board';

$board = $DB->get_record('jinoboard', array('type' => $id));
switch (current_language()){
case 'ko' :
    $name = $board->name;
    break;
case 'en' :
    $name = $board->engname;
    break;
case 'vi' :
    $name = $board->vnname;
    break;
}       
$where ="";
if($id==2||$id==12||$id==13){ // 1:1게시판의경우는 답글빼고 토탈카운트계산해야함
    $where = " AND ref = id";
}
$sql = "select count(id) from {jinoboard_contents} where board = :board $where";
$totalcount = $DB->count_records_sql($sql, array('board' => $board->type));

switch ($site){
    case 1: //공지사항
        $url1 = 1;
        $url2 = 8;
        $url3 = 9;
        $boardname = 'SNot';
        break;
    case 2 : //faq
        $url1 = 3;
        $url2 = 10;
        $url3 = 11;
        $boardname = 'FAQ';
        break;
    case 3 : //1:1 상담
        $url1 = 2;
        $url2 = 12;
        $url3 = 13;
        $boardname = 'COUN';
        break;
    case 4 : // jobs site - 법무 & 세무
        $url1 = 14;
        $url2 = 15;
        $boardname = 'LegalTax';
        break;
}


$pagesettings = array(
    'title' => $name.get_string('boardset','local_management'),
    'heading' => $name.get_string('boardset','local_management'),
    'subheading' => '',
    'menu' => $boardname,
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);

if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('board_list.php?id='.$id);
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);
require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');


?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                    <ul class="mk-c-tab tab-event">
                        <?php if($boardname == 'LegalTax') : ?>
                            <li <?php if($id==14){echo 'class="on"';} ?>><a href="/local/management/board/board_list_notice.php?site=<?php echo $site;?>&id=<?php echo $url1;?>"><?php echo get_string('employer:legalTax_legal', 'local_job'); ?></a></li>
                            <li <?php if($id==15){echo 'class="on"';} ?>><a href="/local/management/board/board_list_notice.php?site=<?php echo $site;?>&id=<?php echo $url2;?>"><?php echo get_string('employer:legalTax_tax', 'local_job'); ?></a></li>
                        <?php else : ?>
                            <li <?php if($id < 4){echo 'class="on"';} ?>><a href="/local/management/board/board_list_notice.php?site=<?php echo $site;?>&id=<?php echo $url1;?>"><?php echo get_string('e-learning','local_management');?></a></li>
                            <li <?php if($id==8||$id==10||$id==12){echo 'class="on"';} ?>><a href="/local/management/board/board_list_notice.php?site=<?php echo $site;?>&id=<?php echo $url2;?>"><?php echo get_string('jobs','local_management');?> - <?php echo get_string('jobs_per','local_management');?></a></li>
                            <li <?php if($id==9||$id==11||$id==13){echo 'class="on"';} ?>><a href="/local/management/board/board_list_notice.php?site=<?php echo $site;?>&id=<?php echo $url3;?>"><?php echo get_string('jobs','local_management');?> - <?php echo get_string('jobs_com','local_management');?></a></li>
                        <?php endif; ?>
                    </ul>    
                <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/local/management/board/board_contents_delete.php'>
                    <input type="hidden" name="type" value="<?php echo $id ?>">
                    <input type="hidden" name="site" value="<?php echo $site ?>">
                    <table>
                        <caption class="hidden-caption"><?php echo $name ;?> <?php echo get_string('boardset', 'local_management'); ?></caption>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="5%">No.</th>
                                <?php 
                                if ($board->allowcategory) {
                                    ?><th scope="row">카테고리</th><?php
                                }
                                ?>
                                <th scope="row" width="30%"><?php echo get_string('title', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('view:cnt', 'local_jinoboard'); ?></th>
                                <th scope="row" width="10%"><?php if($id==2||$id==12||$id==13) {echo get_string('replies', 'local_jinoboard'); } else { echo get_string('comment', 'local_jinoboard'); } ?></th>
                                <th scope="row" width="15%"><?php echo get_string('writer', 'local_jinoboard'); ?></th>
                                <th scope="row" width="15%"><?php echo get_string('date', 'local_jinoboard'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $offset = 0;
                            if ($page != 0) {
                                $offset = ($page - 1) * $perpage;
                            }
                            $list_num = $offset;
                            $num = $totalcount - $offset;
                            $and = '';
                            if($id==2||$id==12||$id==13){
                                 $and = ' and jc.ref = jc.id';
                            }
                           
                            if($totalcount){ 
                                $sql1 = "SELECT jc.id, jc.title, jc.viewcnt, jc.timemodified, jc.commentscount, u.firstname, u.lastname, u.id as userid,jc.category "
                                        . "FROM {jinoboard_contents} jc JOIN {user} u ON u.id = jc.userid WHERE board = :board $and ORDER BY jc.id DESC";
                                $contents = $DB->get_records_sql($sql1, array('board' => $board->type), $offset, $perpage);
                                foreach($contents as $content){
                                    if($id==2||$id==12||$id==13){
                                        $ancount = $DB->count_records_sql("SELECT COUNT(*) FROM {jinoboard_contents} where board =:board and ref !=id and ref = :id",array('board'=>$id,"id"=>$content->id));
                                    }
                                    ?>
                                <tr>
                                    <td class="chkbox">
                                                <input class="chkbox" type="checkbox" title="check" id="chkbox" name="contentid<?php echo $content->id; ?>" value="<?php echo $content->id; ?>" />
                                    </td>
                                    <td class="number">
                                        <?php echo $num; ?>
                                    </td>  
                                     <?php 
                                if ($board->allowcategory) {
                                     switch (current_language()){
                                        case 'ko' :
                                            $content_g = 'name';
                                            break;
                                        case 'en' :
                                            $content_g = 'enname';
                                            break;
                                        case 'vi' :
                                            $content_g = 'vnname';
                                            break;
                                    }
                                    $category = $DB->get_records('jinoboard_category',array('id'=>$content->category));
                                    ?><td><?php foreach($category as $c) { echo $c->$content_g; }?></td><?php
                                }
                                ?>
                                    <td><a href="/local/management/board/board_write<?php if($board->type==2||$board->type==12||$board->type==13){ echo '_qna'; } ?>.php?mod=edit&id=<?php echo $board->type;?>&cid=<?php echo $content->id;?>&site=<?php echo $site;?>&bname=<?php echo $boardname;?>&notice=1"><?php echo $content->title; ?><a></td>
                                    <td><?php echo $content->viewcnt; ?></td>
                                    <td><?php if($id==2||$id==12||$id==13){ echo $ancount; } else { echo $content->commentscount ; } ?></td>
                                    <td><?php echo $content->firstname; ?></td>
                                    <td><?php echo date('Y-m-d',$content->timemodified); ?></td>
                                </tr>    
                                    <?php
                                    $num--;
                                }
                            } else {
                                ?> <tr><td colspan="7"><?php echo get_string('nocontent', 'local_jinoboard'); ?></td></tr>
                                <?php
                            }
                                ?>
                        </tbody>
                    </table>
                        <div id="btn_area">
                                <?php if($boardname != 'LegalTax') : ?>
                                <input type="button" id="blue_btn" class="red_btn" value="<?php echo get_string('board:set', 'local_management'); ?>" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_modify.php?id=<?php echo $board->type;?>&bname=<?php echo $boardname;?>"'  />    
                                <?php endif; ?>
                                <?php if($boardname != 'COUN'){ ?><input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_write.php?site=<?php echo $site; ?>&mod=add&id=<?php echo $board->type;?>&bname=<?php echo $boardname;?>&notice=<?php echo $boardname=='LegalTax'?1:0; ?>"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" /><?php } ?>
                                <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>"  />
                        </div>
                </form>
            <?php print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . '/local/management/board/board_list_notice.php?site='.$site.'&id='.$id); ?>
            </div>
        </div>
    </div>
</section>

<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
 function delboard(type){
     if (confirm("<?php echo get_string('board:alert4', 'local_management'); ?>")) {
            $.ajax({
                url: "board_delete.php",
                type: 'POST',
                dataType: 'json',
                data: {type: type},
                success: function (data) {
                    alert('<?php echo get_string('board:alert5', 'local_management'); ?>'+data.cnt+'<?php echo get_string('board:alert6', 'local_management'); ?>');
                    window.location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }

            });
        }
 }    
 
 $(function () {
    $("#allcheck").click(function () {
        var chk = $("#allcheck").is(":checked");

        if (chk) {
            $(".chkbox input").each(function () {
                this.checked = true;
            });
        } else {
            $(".chkbox input").each(function () {
                this.checked = false;
            });
        }
    });
    $("#delete_button").click(function () {
        if (!$(".chkbox").is(":checked")) {
            alert('<?php echo get_string('board:alert7', 'local_management'); ?>');
            return false;
        }

        if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
            $('#form').submit();
        }
    });
});
</script>