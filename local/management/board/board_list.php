<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

$id = optional_param('id', 0, PARAM_INT); // 게시판 타입
$site = optional_param('site', 0, PARAM_INT); // 사이트 타입 0 : 이러닝 1: 구인구직
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$notice =optional_param('notice', 0, PARAM_INT);

if($notice ==1){ //값이 없으면 고객센터, 값이 있으면 일반게시판
    $where = " WHERE jb.type > 3 and jb.site = 0 ";  
    $name = get_string('board:list','local_management');
    $boardname = 'manage board';
}  else {
    $where = " WHERE jb.type < 4 ";  
    $name = get_string('board:clist','local_management');
    $boardname = 'board';
}

if($id==0){
    //id값이 없으면 전체 게시판 리스트를 출력
    $bsql = "SELECT jb.id,jb.type,jb.name,jb.timemodified,u.firstname,u.lastname FROM {jinoboard} jb LEFT JOIN {user} u ON u.id = jb.userid $where ORDER BY id DESC";
    $board = $DB->get_records_sql($bsql,array());
    $totalcount = $DB->count_records_sql('SELECT COUNT(*) FROM {jinoboard} jb '.$where, array());
} else {
    $board = $DB->get_record('jinoboard', array('type' => $id));
    switch (current_language()){
    case 'ko' :
        $name = $board->name;
        break;
    case 'en' :
        $name = $board->engname;
        break;
    case 'vi' :
        $name = $board->vnname;
        break;
}       
    $and = $id==2 ? ' and ref = id ' : '';
    $sql = "select count(id) from {jinoboard_contents} where board = :board $and";
    $totalcount = $DB->count_records_sql($sql, array('board' => $board->type));
//    $total_pages = jinoboard_get_total_pages($totalcount, $perpage);
}
$pagesettings = array(
    'title' => $name.get_string('boardset','local_management'),
    'heading' => $name.get_string('boardset','local_management'),
    'subheading' => '',
    'menu' => $boardname,
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('board_list.php?id='.$id);
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);
require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');



?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/local/management/board/board_contents_delete.php'>
                    <input type="hidden" name="type" value="<?php echo $id ?>">
                    <table>
                        <caption class="hidden-caption"><?php echo $name ;?> <?php echo get_string('boardset', 'local_management'); ?></caption>
                        <?php if($id){ ?>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="5%">No.</th>
                                <th scope="row" width="35%"><?php echo get_string('title', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('view:cnt', 'local_jinoboard'); ?></th>
                                <th scope="row" width="10%"><?php echo $id !=2 ? get_string('comment', 'local_jinoboard') : get_string('replies', 'local_jinoboard'); ?></th>
                                <th scope="row" width="15%"><?php echo get_string('writer', 'local_jinoboard'); ?></th>
                                <th scope="row" width="15%"><?php echo get_string('date', 'local_jinoboard'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $offset = 0;
                            if ($page != 0) {
                                $offset = ($page - 1) * $perpage;
                            }
                            $list_num = $offset;
                            $num = $totalcount - $offset;
                            $and = $id==2 ? ' and jc.ref = jc.id' : '';
                            if($totalcount){
                                $sql1 = "SELECT jc.id, jc.title, jc.viewcnt, jc.timemodified, jc.commentscount, u.firstname, u.lastname, u.id as userid "
                                        . "FROM {jinoboard_contents} jc JOIN {user} u ON u.id = jc.userid WHERE board = :board $and ORDER BY jc.id DESC";
                                $contents = $DB->get_records_sql($sql1, array('board' => $board->type), $offset, $perpage);
                                foreach($contents as $content){
                                    if($id==2){
                                        $ancount = $DB->count_records_sql("SELECT COUNT(*) FROM {jinoboard_contents} where board =2 and ref !=id and ref = :id",array("id"=>$content->id));
                                    }
                                    ?>
                                <tr>
                                    <td class="chkbox">
                                                <input class="chkbox" type="checkbox" title="check" id="chkbox" name="contentid<?php echo $content->id; ?>" value="<?php echo $content->id; ?>" />
                                    </td>
                                    <td class="number">
                                        <?php echo $num; ?>
                                    </td>  
                                    <td><a href="/local/management/board/board_write<?php if($board->type==2){ echo '_qna'; } ?>.php?mod=edit&id=<?php echo $board->type;?>&cid=<?php echo $content->id;?>"><?php echo $content->title; ?><a></td>
                                    <td><?php echo $content->viewcnt; ?></td>
                                    <td><?php echo $id !=2 ? $content->commentscount : $ancount ; ?></td>
                                    <td><?php echo $content->firstname.$content->lastname; ?></td>
                                    <td><?php echo date('Y-m-d',$content->timemodified); ?></td>
                                </tr>    
                                    <?php
                                    $num--;
                                }
                            } else {
                                ?> <tr><td colspan="7"><?php echo get_string('nocontent', 'local_jinoboard'); ?></td></tr>
                                <?php
                            }
                                ?>
                        </tbody>
                        <?php } else {

                            ?>
                            <thead>
                            <tr>
                                <th scope="row" width="30%"><?php echo get_string('board:name', 'local_management'); ?></th>
                                <th scope="row" width="20%"><?php echo get_string('board:writer', 'local_management'); ?></th>
                                <th scope="row" width="20%"><?php echo get_string('board:update', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('board:delete', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('board:move', 'local_management'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($board as $b){ ?>
                                <tr>
                                    <td><a href="/local/management/board/board_list.php?notice=<?php echo $notice; ?>&id=<?php echo $b->type;?>"><?php echo $b->name; ?></a></td>
                                    <td><?php echo $b->firstname.$b->lastname; ?></td>
                                    <td><?php echo date('Y-m-d',$b->timemodified); ?></td>
                                    <td><input type="button" onclick="delboard(<?php echo $b->type; ?>)" value='<?php echo get_string('del', 'local_management'); ?>'></td>
                                    <td><input type="button" onclick="window.open('<?php 
                                    if($b->type>3){ 
                                        echo $CFG->wwwroot."/local/jinoboard/board.php?type=".$b->type; 
                                    } else { 
                                        echo $CFG->wwwroot."/local/jinoboard/index.php?type=".$b->type; } ?>')" value='<?php echo get_string('board:move', 'local_management'); ?>'></td>
                                </tr>
                                <?php $num--; 
                                } ?>
                            </tbody>
                            <?php
                        }?>
                    </table>
                        <div id="btn_area">
                            <?php if($id){
                                ?>
                                <input type="button" id="blue_btn" class="red_btn" value="<?php echo get_string('board:set', 'local_management'); ?>" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_modify.php?id=<?php echo $board->type;?>&notice=<?php echo $notice;?>"'  />    
                                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_write.php?mod=add&id=<?php echo $board->type;?>"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                                <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>"  />
                                <?php
                            } else {?>
                                <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_modify.php?notice=<?php echo $notice;?>"' value="<?php echo get_string('board:made', 'local_management'); ?>" />
                            <?php } ?>
                        </div>
                </form>
            <?php print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . '/local/management/board/board_list.php?id='.$id); ?>
            </div>
        </div>
    </div>
</section>

<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
 function delboard(type){
     if (confirm("<?php echo get_string('board:alert4', 'local_management'); ?>")) {
            $.ajax({
                url: "board_delete.php",
                type: 'POST',
                dataType: 'json',
                data: {type: type},
                success: function (data) {
                    alert('<?php echo get_string('board:alert5', 'local_management'); ?>'+data.cnt+'<?php echo get_string('board:alert6', 'local_management'); ?>');
                    window.location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }

            });
        }
 }    
 
 $(function () {
    $("#allcheck").click(function () {
        var chk = $("#allcheck").is(":checked");

        if (chk) {
            $(".chkbox input").each(function () {
                this.checked = true;
            });
        } else {
            $(".chkbox input").each(function () {
                this.checked = false;
            });
        }
    });
    $("#delete_button").click(function () {
        if (!$(".chkbox").is(":checked")) {
            alert('<?php echo get_string('board:alert7', 'local_management'); ?>');
            return false;
        }

        if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
            $('#form').submit();
        }
    });
});
</script>