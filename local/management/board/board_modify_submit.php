<?php
    require_once(__DIR__ . '/../../../config.php');
    require_once $CFG->dirroot . '/local/management/lib/contents_lib.php';
    $id = optional_param('id', 0, PARAM_INT);
    $notice = optional_param('notice', 0, PARAM_INT);
    
    $name = optional_param('name',"",PARAM_TEXT);
    $engname = optional_param('engname',"",PARAM_TEXT);
    $vnname = optional_param('vnname',"",PARAM_TEXT);
    $maxbytes = optional_param('maxbytes', 0, PARAM_INT);
    $maxattachments = optional_param('maxattachments', 0, PARAM_INT);
    $allownew = optional_param('allownew', 0, PARAM_INT);
    $newday = optional_param('newday', 0, PARAM_INT);
    $allownotice = optional_param('allownotice', 0, PARAM_INT);
    $allowreply = optional_param('allowreply', 0, PARAM_INT);  
    $allowcomment = optional_param('allowcomment', 0, PARAM_INT);  
    $allowrental = optional_param('allowrental', 0, PARAM_INT);  
    $allowupload = optional_param('allowupload', 0, PARAM_INT);  
    $allowsecret = optional_param('allowsecret', 0, PARAM_INT);  
    $allowcategory = optional_param('allowcategory', 0, PARAM_INT);  
    $allowperiod = optional_param('allowperiod', 0, PARAM_INT);  
    $modifytime = time();
            
    $data = new stdClass();
    $data->name = $name;
    $data->engname = $engname;
    $data->vnname = $vnname;
    $data->maxbytes = $maxbytes;
    $data->maxattachments = $maxattachments;
    $data->allownew = $allownew;
    $data->newday = $newday;
    $data->allownotice = $allownotice;
    $data->allowreply = $allowreply;
    $data->allowcomment = $allowcomment;
    $data->allowrental = $allowrental;
    $data->allowupload = $allowupload;
    $data->allowsecret = $allowsecret;
    $data->allowcategory = $allowcategory;
    $data->allowperiod = $allowperiod;
    $data->userid = $USER->id;
    $data->timemodified = $modifytime;
    
 
    if(!$id){
        $cnt = $DB->count_records_sql('SELECT COUNT(id) FROM {jinoboard}',array());
        $data->type = $cnt+1;
        $id = $DB->insert_record('jinoboard', $data);
    } else {
        $data->id = $id;
        $id = $DB->update_record('jinoboard',$data);
        
    }
    
        
        redirect('/local/management/board/board_list.php?notice='.$notice);