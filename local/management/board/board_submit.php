<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

$context = context_system::instance();
$id = optional_param('id', 0, PARAM_INT); // 게시판 타입 
$site = optional_param('site', 0, PARAM_INT); //site 값이 있으면 고객센터관리 
$cid = optional_param('cid', 0, PARAM_INT); // 게시글 아이디
$category = optional_param('category', 0, PARAM_INT);
$ref = optional_param('ref', 0, PARAM_INT);
$title = optional_param('title',"",PARAM_TEXT);
$text = optional_param('text',"",PARAM_RAW);
$reger = optional_param('reger', 0, PARAM_INT); // 상단게시
$publisheddate = optional_param('publisheddate', "", PARAM_TEXT); // 상단게시 종료일
$file_id = optional_param('file_id', 0, PARAM_INT);
$file_thumb_id = optional_param('file_id', 0, PARAM_INT);
$file_del = optional_param('file_del', 0, PARAM_INT);      
$file_thumb_del = optional_param('file_del', 0, PARAM_INT);      
//print_object($file_thumb_id);die();
$board = $DB->get_record('jinoboard', array('type' => $id));

$thistime = time();
$newdata = new object();
$newdata->title = $title;
$newdata->contents = $text;
$newdata->userid = $USER->id;
$newdata->board = $id;
$newdata->category = $category;
$newdata->ref = $ref;
$newdata->isnotice = $reger;
if($reger){
    if($publisheddate){
        $newdata->timeend = strtotime($publisheddate) + 86400 - 1;
    }else{
        $newdata->timeend = 0;
    }
}
$newdata->timemodified = time();
if($cid){
    //업데이트
    
    $newdata->id = $cid;
    $DB->update_record('jinoboard_contents', $newdata);

    $itemid = $cid;
    
} else {
    //인서트 
    $newdata->timecreated = time();
   
   $itemid= $DB->insert_record('jinoboard_contents', $newdata);

}

if ($file_id != 0) {
    
    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($cid && !($_FILES['uploadfile']['name']==null || $_FILES['uploadfile']['name']=='')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachments', $itemid, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }
 
$fname = $_FILES['uploadfile']['name'];

if(!empty($fname)){
$fcount = count($fname);
 for($i=0;$i<$fcount;$i++){
    if (!empty($_FILES['uploadfile']['tmp_name'][$i])) {
                $file_record = array(
                    'contextid' => $context->id,
                    'component' => 'local_jinoboard',
                    'filearea' => 'attachments',
                    'itemid' => $itemid,
                    'filepath' => '/',
                    'filename' => $_FILES['uploadfile']['name'][$i],
                    'timecreated' => time(),
                    'timemodified' => time(),
                    'userid' => $USER->id,
                    'author' => fullname($USER),
                    'license' => 'allrightsreserved',
                    'sortorder' => 0
                );
                $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile']['tmp_name'][$i]);
            }}
     
 }

}

if ($file_thumb_id != 0) {
    
    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($cid && !($_FILES['uploadthumbfile']['name']==null || $_FILES['uploadthumbfile']['name']=='')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_jinoboard', 'thumbnail', $itemid, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }
 
$fname = $_FILES['uploadthumbfile']['name'];

if(!empty($fname)){
$fcount = count($fname);
 for($i=0;$i<$fcount;$i++){
    if (!empty($_FILES['uploadthumbfile']['tmp_name'])) {
                $file_record = array(
                    'contextid' => $context->id,
                    'component' => 'local_jinoboard',
                    'filearea' => 'thumbnail',
                    'itemid' => $itemid,
                    'filepath' => '/',
                    'filename' => $_FILES['uploadthumbfile']['name'],
                    'timecreated' => time(),
                    'timemodified' => time(),
                    'userid' => $USER->id,
                    'author' => fullname($USER),
                    'license' => 'allrightsreserved',
                    'sortorder' => 0
                );
                $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadthumbfile']['tmp_name']);
            }}
     
 }

}
if($site==0){
    redirect($SITECFG->wwwroot . '/local/management/board/board_list.php?id='.$board->type); 
} else {
    redirect($SITECFG->wwwroot . '/local/management/board/board_list_notice.php?site='.$site.'&id='.$board->type); 
}