<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
$id = optional_param('id', 0, PARAM_INT); // 게시판 타입
$cid = optional_param('cid', 0, PARAM_INT); // 게시글 아이디
$bname = optional_param('bname', '', PARAM_RAW); // 게시판 타입
$site = optional_param('site', '', PARAM_RAW); // 게시판 타입
$notice = optional_param('notice', 0, PARAM_INT); // 값이 1이면 목록-> board_list_notice로 이동되도록함
$mod = optional_param('mod', 'add', PARAM_INT); // 새글작성 add / 글 수정 edit
$board = $DB->get_record('jinoboard', array('type' => $id));
switch (current_language()) {
    case 'ko' :
        $boardname = $board->name;
        break;
    case 'en' :
        $boardname = $board->engname;
        break;
    case 'vi' :
        $boardname = $board->vnname;
        break;
}
$boardname2 = !empty($bname) ? $bname : 'manage board';
$pagesettings = array(
    'title' => $boardname . get_string('write2', 'local_management'),
    'heading' => $boardname . get_string('write2', 'local_management'),
    'subheading' => '',
    'menu' => $boardname2,
    'js' => array(
        $CFG->wwwroot . '/chamktu/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot . '/chamktu/js/ckfinder-2.4/ckfinder.js'
    ),
    'css' => array(),
    'nav_bar' => array()
);

if ($mod == 'edit') {
    $sql = "SELECT * FROM {jinoboard_contents} WHERE id = $cid";
    $content = $DB->get_record_sql($sql, array());
    $answercontents = $DB->get_records_sql("SELECT * FROM {jinoboard_contents} where board =:board and ref !=id and ref = :id", array('board' => $id, "id" => $content->id));
}

include_once($CFG->dirroot . '/local/management/header.php');

$temp->itemid = $cid;

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $content->id, 'timemodified', false);

function sizeFilter($bytes) {
    $label = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $bytes >= 1024 && $i < ( count($label) - 1 ); $bytes /= 1024, $i++)
        ;
    return( round($bytes, 2) . " " . $label[$i] );
}

$maxsize = sizeFilter($board->maxbytes);
if ($content) {
    $content->contents = file_rewrite_pluginfile_urls($content->contents, 'pluginfile.php', $context->id, 'local_jinoboard', 'contents', $content->id);

    $attachments = "";
    if (count($files) > 0) {
        $type = '';
        $attfile = '';
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_jinoboard/attachment/' . $content->id . '/' . $filename);
            $attfile .= "<li>";

            if ($board->type == $CFG->DATAID) {
                $attfile .= "<a href=\"javascript:alertDistribution('$path');\">$iconimage</a> ";
                $attfile .= "<a href=\"javascript:alertDistribution('$path');\">" . s($filename) . "</a>";
            } else {
                $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
            }
            $attfile .= '</li>';
        }
        $attachments .= $attfile;
    }
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <table cellpadding="0" cellspacing="0" class="detail">    
                    <tbody>
                        <tr>
                            <td class="field_title"><?php echo get_string('title', 'local_management'); ?></td> 
                            <td class="field_value"  colspan="3">
                                <?php echo $content ? $content->title : ''; ?>
                        </tr>
                        <tr>
                            <td class="field_title"><?php echo get_string('boardcontent', 'local_management'); ?></td> 
                            <td class="field_value"  colspan="3">
                                <?php echo $content ? $content->contents : ""; ?>
                            </td>
                        </tr>
                        <?php if ($attachments) {
                            ?>
                            <tr>
                                <td class="field_title"><?php echo get_string('attachments', 'local_management'); ?></td>
                                <td class="field_value"  colspan="3"><?php echo $attachments; ?></td>
                            </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                <?php
                if ($answercontents) {
                    foreach ($answercontents as $answercontent) {
                        ?>
                        <table cellpadding="0" cellspacing="0" class="detail">    
                            <tbody>
                                <tr>
                                    <td class="field_title"><?php echo get_string('title', 'local_management'); ?></td> 
                                    <td class="field_value"  colspan="3">
        <?php echo $answercontent->title; ?>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('boardcontent', 'local_management'); ?></td> 
                                    <td class="field_value"  colspan="3">
        <?php echo $answercontent->contents; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                    <?php }
                ?>
                <h3 class="sub-tit"><?php echo get_string('board:writeanswer', 'local_management'); ?></h3>
                <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./board_submit.php" method="POST">
                    <input type='hidden' name='id' value='<?php echo $board->type; ?>'>
                    <input type='hidden' name='ref' value='<?php echo $content ? $content->id : ''; ?>'>
                    <input type='hidden' name='site' value='<?php echo $site; ?>'>
                    <input type='hidden' name='category' value='0'>
                    <table cellpadding="0" cellspacing="0" class="detail">    
                        <tbody>
                            <tr>
                                <td class="field_title"><?php echo get_string('title', 'local_management'); ?></td> 
                                <td class="field_value"  colspan="3">
                                    <input type="text" name ="title" size="140" value="<?php echo 'RE : ' . $content->title; ?>">
                            </tr>
                            <tr>
                                <td class="field_title"><?php echo get_string('boardcontent', 'local_management'); ?></td> 
                                <td class="field_value"  colspan="3">
                                    <textarea class="ckeditor" rows="7" name="text"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="btn_area">
                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_management'); ?>" style="float: right;"  onclick="return submit_check()" />
                        <input type="button" id="admin_list" class="normal_btn" value="<?php echo get_string('list', 'local_management'); ?>" style="float: left;" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_list<?php if ($notice) {
                    echo '_notice';
                } ?>.php?id=<?php echo $board->type; ?>&site=<?php echo $site; ?>"'/>
                    </div> <!-- Bottom Button Area -->
                </form>

            </div>
        </div>
    </div>        

</section>

<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
    $(function () {
        $('input[name=reger]').click(function () {
            if ($('input[name=reger]').is(':checked')) {
                $('input[name=publisheddate]').removeAttr('disabled');
            } else {
                $('input[name=publisheddate]').attr('disabled', 'disabled');
            }

        });
        document.getElementById("uploadfile").onchange = function () {
            var chksize = <?php echo $board->maxbytes == 0 ? '1000000000000' : $board->maxbytes; ?>;
            var File_Size = document.getElementById("uploadfile").files[0].size;
            if (File_Size > chksize) {
                alert('<?php echo get_string('boardcontent', 'local_management', $maxsize); ?>');
                document.getElementById("uploadfile").value = "";
            }
        };
    });

    function submit_check() {
        var title = $('input[name=title]').val();
        if(title==''){
            alert('<?php echo get_string('entertitle','local_management'); ?>');
            return false;
        }
        var num = <?php echo $board->maxattachments; ?>;
        if ($("input[type='file']").val() != null) {
            var filearylen = document.getElementById('uploadfile').files.length;
            if (filearylen >= (num + 1)) {
                alert('<?php echo get_string('board:alert2', 'local_management'); ?>' + num + '<?php echo get_string('board:alert3', 'local_management'); ?>');
                document.getElementById("uploadfile").value = "";
                return false;
            } else {
                return true;
            }
        }
    }

    $("#timedue").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timeavailable").datepicker("option", "maxDate", selectedDate);
        }
    });

</script>