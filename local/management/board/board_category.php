<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
?>
<link rel="stylesheet" type="text/css" href="./styles/common.css" />
<?php
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('board_category.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);


$id = optional_param("id", 0, PARAM_INT);
$notice = optional_param("notice", 0, PARAM_INT);
if($notice ==1){ //값이 없으면 고객센터, 값이 있으면 일반게시판
    $boardname = 'manage board';
}  else {
    $boardname = 'board';
}
$board = $DB->get_record('jinoboard', array('type' => $id));

if (!$id){
    $pagesettings = array(
        'title' => get_string('board:made', 'local_management'),
        'heading' => get_string('board:made', 'local_management'),
        'subheading' => '',
        'menu' => $boardname,
        'js' => array(),
        'css' => array(),
        'nav_bar' => array()
    );
} else {
    $pagesettings = array(
        'title' => $board->name,
        'heading' => $board->name,
        'subheading' => '',
        'menu' => $boardname,
        'js' => array(),
        'css' => array(),
        'nav_bar' => array()
    );
}

$board = $DB->get_record('jinoboard', array('id' => $id));
?>
<?php include_once($CFG->dirroot.'/local/management/header.php'); ?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
        <div class="siteadmin_tabs">
            <a href="<?php echo $CFG->wwwroot; ?>/local/management/board/board_modify.php?id=<?php echo $id; ?>"><p class="black_btn "><?php echo get_string('board_boardInformation', 'local_lmsdata'); ?></p></a>
            <?php  if($board->allowcategory){ ?>
                <a href="<?php echo $CFG->wwwroot; ?>/local/management/board/board_category.php?id=<?php echo $id; ?>"><p class="black_btn black_btn_selected "><?php echo get_string('board_category', 'local_lmsdata'); ?></p></a>
            <?php } ?>
        </div>
        <div>
        </div>
        <div id="category_content_body">
            <?php 
                $categories = $DB->get_records('jinoboard_category',array('board'=>$id),'sortorder asc');
                $cnt = 1;
                foreach($categories as $category){
                    
                $usertypes = explode('/', $category->usertype); 
            ?>
            <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./board_category_modify.php?id=<?php echo $id; ?>" method="POST">
                <input type="hidden" value="<?php echo $category->id; ?>" name="cid">
            <div id="category_form">
                <fieldset style="clear: both;">
                    <legend><h2><?php echo get_string('board_category', 'local_lmsdata'); ?> <?php echo $cnt++; ?></h2></legend>
                    <div>
                        <table cellpadding="0" cellspacing="0" class="detail">
                            <tbody>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categorynameko', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_300" name ="name" value="<?php echo $category->name; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categorynameen', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_300" name ="engname" value="<?php echo $category->engname; ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categorynamevn', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_300" name ="vnname" value="<?php echo $category->vnname; ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('siteadmin_act', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <select name="isused" class="w_70">
                                            <?php $selected = ($category->isused == 2)?'selected':''; ?>
                                            <option value="1"><?php echo get_string('siteadmin_act', 'local_lmsdata'); ?></option>
                                            <option  <?php echo $selected; ?> value="2"><?php echo get_string('siteadmin_noact', 'local_lmsdata'); ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categoryorder', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_150" name ="sortorder" value="<?php echo $category->sortorder; ?>" />
                                    </td>
                                </tr>
                        </table>
                    </div>
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('updates', 'local_lmsdata'); ?>" style="float: right;" />

                </fieldset>
            </div>
        </form>
            <?php } ?>
        </div>
        <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./board_category_add.php?id=<?php echo $id; ?>" method="POST">
            <div id="category_form">
                <fieldset style="clear: both;">
                    <legend><h2><?php echo get_string('board_categoryregist', 'local_lmsdata'); ?></h2></legend>
                    <div> 
                        <table cellpadding="0" cellspacing="0" class="detail">
                            <tbody>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categorynameko', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_300" name ="name" value=""/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categorynameen', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_300" name ="engname" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categorynamevn', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_300" name ="vnname" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('siteadmin_act', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <select name="isused" class="w_70">
                                            <option value="1"><?php echo get_string('siteadmin_act', 'local_lmsdata'); ?></option>
                                            <option value="2"><?php echo get_string('siteadmin_noact', 'local_lmsdata'); ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('board_categoryorder', 'local_lmsdata'); ?></td>
                                    <td class="field_value">
                                        <input type="text" class="w_150" name ="sortorder" value="" />
                                        <span style="color:#666;">ex ) 1</span>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </fieldset>
            </div>
            <div></div>
            <div class="btn_area">
                <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('board_categoryregist', 'local_lmsdata'); ?>"  />
                <input type="button" id="notice_list" class="normal_btn" value="<?php echo get_string('board_list', 'local_lmsdata'); ?>"  onclick="location.href = 'board_list.php'" />
            </div> <!-- Bottom Button Area -->

        </form>
    </div>
        </div>
</div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>
<script type="text/javascript">
    $(function () {
        $("input:radio[name=noticescore]").each(function () {
            $(this).click(function () {
                noticescore_changed($(this).val());
                objection_changed($("input:radio[name=objection]:checked").val());
            });
        });
        $("input:radio[name=objection]").each(function () {
            $(this).click(function () {
                objection_changed($(this).val());
            });
        });
    });
</script>
