<?php
require_once(__DIR__ . '/../../../config.php');
$type = optional_param('type', 0, PARAM_INT);

if($type){ //게시판을 삭제
    $del = $DB->delete_records('jinoboard',array('type'=>$type));
}

$cnt = 0;
if($del){ //해당 게시판에 등록된 게시글들도 삭제 
    $DB->delete_records('jinoboard_contents',array('board'=>$type));
    $cnt++;
}
$result->del = $del;
$result->cnt = $cnt;
echo(json_encode($result, JSON_UNESCAPED_UNICODE));