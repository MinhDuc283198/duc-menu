<?php
require_once(__DIR__ . '/../../../config.php');
$PAGE->set_url('/local/management/board/view.php');


$sql1 = "SELECT jc.id,jc.contents, jc.title, jc.viewcnt, jc.timemodified, jc.commentscount, u.firstname, u.lastname, u.id as userid,jc.category FROM {jinoboard_contents} jc JOIN {user} u ON u.id = jc.userid WHERE board = 3 ORDER BY jc.id DESC";
$contents = $DB->get_records_sql($sql1);

echo $OUTPUT->header();

?>

<!DOCTYPE html>
<html  dir="ltr" lang="ko" xml:lang="ko">
<head>
    <title>Masterkorean FAQ</title>
    <link rel="shortcut icon" href="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .collapse-vs {
            border-top: 1px solid #eaeaea;

            margin-top: 10px;
        }
        .card-header {
            cursor: pointer;
        }
        .accordion {
            border-bottom: 1px solid #eaeaea;
            padding: 10px;
            margin-bottom: 0px;
        }
        .accordion:nth-of-type(1) {
            border-top: 1px solid #eaeaea;
        }
        .mb-0 {
            font-size: 20px !important;
            margin-bottom: 0px !important;
            margin-bottom: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .active {
            background: #f5f7ff;
        }
        .card-body {
            padding-top: 10px;
        }
    </style>
</head>
<body>

    <?php
    $index = 0;
    foreach($contents as $key => $content){
        $index++;
        ?>

        <div class="accordion" id="accordion<?php echo $content->id ?>">
            <div class="card">
                <div class="card-header" data-id="<?php echo $content->id ?>">
                    <h2 class="mb-0">
                        <?php echo $index ?>. <?php echo $content->title ?>
                    </h2>
                </div>

                <div id="collapse<?php echo $content->id ?>" class="collapse-vs" style="display: none">
                    <div class="card-body">
                        <?php echo $content->contents ?>
                    </div>
                </div>
            </div>

        </div>
        <?php
    }
    ?>


    <script>
        $('.card-header').click(function(){
            let id = $(this).data('id');
            $('.collapse-vs').hide(2);
            $('#collapse' + id).show('2')
            $(".accordion").removeClass('active');
            $("#accordion" + id).addClass('active')
        })
    </script>

</body>
<?php
echo $OUTPUT->footer();
?>
