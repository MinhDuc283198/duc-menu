<?php
require_once(__DIR__ . '/../../../config.php');
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/form/editor.php';
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
$id = optional_param('id', 0, PARAM_INT); // 게시판 타입
$site = optional_param('site', 0, PARAM_INT); // 게시판 타입
$notice = optional_param('notice', 0, PARAM_INT); // 값이 1이면 목록-> board_list_notice로 이동되도록함
$bname = optional_param('bname', '', PARAM_RAW); // 게시판 타입
$cid = optional_param('cid', 0, PARAM_INT); // 게시글 아이디
$mod = optional_param('mod', 'add', PARAM_INT); // 새글작성 add / 글 수정 edit
$board = $DB->get_record('jinoboard', array('type' => $id));
switch (current_language()) {
    case 'ko' :
        $boardname = $board->name;
        break;
    case 'en' :
        $boardname = $board->engname;
        break;
    case 'vi' :
        $boardname = $board->vnname;
        break;
}
$boardname2 = !empty($bname) ? $bname : 'manage board' ;
$pagesettings = array(
    'title' => $boardname . get_string('write2', 'local_management'),
    'heading' => $boardname . get_string('write2', 'local_management'),
    'subheading' => '',
    'menu' => $boardname2,
    'js' => array(
        $CFG->wwwroot . '/local/management/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot . '/local/management/js/ckfinder-2.4/ckfinder.js'
    ),
    'css' => array(),
    'nav_bar' => array()
);

if ($mod == 'edit') {
    $sql = "SELECT * FROM {jinoboard_contents} WHERE id = $cid";
    $content = $DB->get_record_sql($sql, array());
}

include_once($CFG->dirroot . '/local/management/header.php');

$temp->itemid = $cid;

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachments', $temp->itemid, "", false);
$thumbfiles = $fs->get_area_files($context->id, 'local_jinoboard', 'thumbnail', $temp->itemid, "", false);
//print_object($files);print_object($thumbfiles);die();
function sizeFilter($bytes) {
    $label = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $bytes >= 1024 && $i < ( count($label) - 1 ); $bytes /= 1024, $i++)
        ;
    return( round($bytes, 2) . " " . $label[$i] );
}

$maxsize = sizeFilter($board->maxbytes);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">

                <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="./board_submit.php" method="POST">
                    <input type='hidden' name='id' value='<?php echo $board->type; ?>'>
                    <input type='hidden' name='site' value='<?php echo $site; ?>'>
                    <input type='hidden' name='cid' value='<?php echo $content ? $content->id : ''; ?>'>
                    <input type='hidden' name='category' value='0'>
                    <table cellpadding="0" cellspacing="0" class="detail">    
                        <tbody>
                            <tr>
                                <td class="field_title"><?php echo get_string('title', 'local_management'); ?><span class="red">*</span></td> 
                                <td class="field_value"  colspan="3">
                                    <input type="text" name ="title" size="140" value="<?php echo $content ? $content->title : ''; ?>">
                            </tr>
                            <?php
                            if ($board->allowcategory) {
                                $categorys = $DB->get_records('jinoboard_category', array('isused' => 1,'board'=>$id));
                                ?>
                                <tr>
                                    <td class="field_title"><?php echo get_string('category', 'local_management'); ?></td> 
                                    <td class="field_value"  colspan="3"><select name='category' id='publisher' class="w_160">
                                            <?php
                                            foreach ($categorys as $ca) {
                                                $select = '';
                                                if ($ca->id == $content->category) {
                                                    $select = 'selected';
                                                }
                                                echo '<option value=' . $ca->id . ' ' . $select . ' >' . $ca->name . '</option>';
                                            }
                                            ?></select>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <?php
                            if ($board->allownotice) {
                                ?>
                                <tr>
                                    <td class="field_title"><?php echo get_string('notice', 'local_jinoboard'); ?></td> 
                                    <td class="field_value"  colspan="3"><input class="chkbox" type="checkbox" value="1" name='reger' <?php echo $content->isnotice ? 'checked' : ''; ?>/><?php echo get_string('board:noticeset', 'local_management'); ?></td>
                                </tr>
                                <tr>
                                    <td class="field_title"><?php echo get_string('timeend', 'local_jinoboard'); ?></td> 
                                    <td class="field_value"  colspan="3">
                                        <input type="text" id='timedue' class="w_200 date" name ="publisheddate" value="<?php echo $content->timeend ? date('Y-m-d',$content->timeend) : date('Y-m-d', time()); ?> " <?php echo !$content->timeend ? 'disabled="disabled"' : ''?>  />
                                        <input class="chkbox" type="checkbox" value="1" name='reger2' <?php echo ($content->isnotice && $content->timeend != 0) ? 'checked' : ''; ?> <?php echo !$content->isnotice? 'disabled=true' : ''?>/>기간설정
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td class="field_title"><?php echo get_string('boardcontent', 'local_management'); ?><span class="red">*</span></td> 
                                <td class="field_value"  colspan="3">
                                    <textarea class="ckeditor" id='editor' rows="7" name="text"><?php echo $content ? $content->contents : ""; ?></textarea>
                                </td>
                            </tr>
                            <tr> 
                                    <td class="field_title" style="background-color: #F2F2F2"><?php echo get_string('thumbnail', 'local_management'); ?></td>
                                    <td class="field_value number" colspan="3">                                
                                        <?php
                                        if ($temp->itemid) {
                                            $output = '';
                                            $cnt = 0;
                                            foreach ($thumbfiles as $tfile) {
                                                $tfilename = $tfile->get_filename();
                                                $tmimetype = $tfile->get_mimetype();
                                                $ticonimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($tmimetype)) . '" class="icon" alt="' . $tmimetype . '" />';

                                                $tpath = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_jinoboard/thumbnail/' . $cid . '/' . $tfilename);
                                                if ($tfile->get_filesize() > 0) {
                                                    $tfileobj[$cnt] .= "<div id ='file' class='file-bx'><a href=\"$tpath\">$ticonimage</a>";
                                                    $tfileobj[$cnt] .= format_text("<a href=\"$tpath\">" . s($tfilename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
                                                    $tfileobj[$cnt] .= '<div class="ex_img"><input type="hidden" name="file_thumb_del" value="0"/><img src="' . $tpath . '"></div>';
                                                    $cnt++;
                                                }
                                            }
                                        }
                                        for ($n = 0; $n <= 0; $n++) {
                                            ?>
                                        <div class="fileBox"><input type="file" name="uploadthumbfile" id="uploadfile" accept=".gif, .jpg, .png"/> <span>(160*138px)</span>

                                                <?php
                                                if (isset($tfileobj[$n])) {
                                                    echo $tfileobj[$n];
                                                }
                                                echo "</div>";
                                            }
                                            ?>
                                            <input type="hidden" class="" name="file_thumb_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                                    </td>
                                </tr>    
                            <?php
                            if ($board->allowupload) { // --- 첨부파일 기능 사용할경우
                                ?>
                                <tr>
                                    <td class="field_title" style="background-color: #F2F2F2"><?php echo get_string('attachment', 'local_jinoboard'); ?></td>
                                    <td class="field_value number" colspan="3">                                
                                        <?php
                                        if ($temp->itemid) {
                                            $output = '';
                                            $cnt = 0;
                                            foreach ($files as $file) {
                                                $filename = $file->get_filename();
                                                $mimetype = $file->get_mimetype();
                                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_jinoboard/attachments/' . $cid . '/' . $filename);
                                                if ($file->get_filesize() > 0) {
                                                    $fileobj[$cnt] .= "<div id ='file' class='file-bx'><a href=\"$path\">$iconimage</a>";
                                                    $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
                                                    $fileobj[$cnt] .= '<div class="ex_img"><input type="hidden" name="file_del" value="0"/><img src="' . $path . '" ></div>';
                                                    $cnt++;
                                                }
                                            }
                                        }
                                        for ($n = 0; $n <= 0; $n++) {
                                            ?>
                                            <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile[]" id="uploadfile" style=" float:left;" accept=".gif, .jpg, .png, .zip, .docx, .pptx, .pdf" multiple/> 

                                                <?php
                                                if (isset($fileobj[$n])) {
                                                    echo $fileobj[$n];
                                                }
                                                echo "</div>";
                                            }
                                            ?>
                                            <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                                    </td>
                                </tr>    

                                <?php
                            }
                            ?>
                        </tbody>

                    </table>
                    <div id="btn_area">
                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_management'); ?>" style="float: right;"  onclick="return submit_check()" />
                        <input type="button" id="admin_list" class="normal_btn" value="<?php echo get_string('list', 'local_management'); ?>" style="float: left;" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/board/board_list<?php if($notice){ echo '_notice'; } ?>.php?id=<?php echo $board->type; ?>&site=<?php echo $site; ?>"'/>
                    </div> <!-- Bottom Button Area -->
                </form>
            </div>
        </div>
    </div>
</section>

<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
    // ckeditor 툴바 설정
   

    var editor = CKEDITOR.replace('editor', {
        language: '<?php if(current_language()!='vi'){  echo current_language(); } else { echo 'vn'; } ?>',
        filebrowserBrowseUrl: '../js/ckfinder-2.4/ckfinder.html',
        filebrowserImageBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Flash',
        filebrowserUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Flash',
        enterMode: CKEDITOR.ENTER_BR,
        allowedContent: true
    });
    
    CKFinder.setupCKEditor(editor, '../');
    
    
    $(function () {
        $('input[name=reger]').click(function () {
            if ($('input[name=reger]').is(':checked')) {
                //$('input[name=publisheddate]').removeAttr('disabled');
                $('input[name=reger2]').removeAttr('disabled');
            } else {
                $('input[name=publisheddate]').attr('disabled', 'disabled');
                $('input[name=reger2]').attr('disabled','disabled');
                $("input[name=reger2]").prop("checked", false);
            }

        });
        
        $('input[name=reger2]').click(function () {
            if ($('input[name=reger2]').is(':checked')) {
                $('input[name=publisheddate]').removeAttr('disabled');
            } else {
                $('input[name=publisheddate]').attr('disabled', 'disabled');
            }
        });
        
        
        document.getElementById("uploadfile").onchange = function () {
            var chksize = <?php echo $board->maxbytes == 0 ? '1000000000000' : $board->maxbytes; ?>;
            var File_Size = document.getElementById("uploadfile").files[0].size;
            if (File_Size > chksize) {
                alert('<?php echo get_string('boardcontent', 'local_management', $maxsize); ?>');
                document.getElementById("uploadfile").value = "";
            }
        };
    });

    function submit_check() {
        var title = $('input[name=title]').val();
        if(title==''){
            alert('<?php echo get_string('entertitle','local_management'); ?>');
            return false;
        }
        var num = <?php echo $board->maxattachments; ?>;
        if(num==0){
            num = 100;
        }
        if ($("input[type='file']").val() != null) {
            var filearylen = document.getElementById('uploadfile').files.length;
            if (filearylen >= (num + 1)) {
                alert('<?php echo get_string('board:alert2', 'local_management'); ?>' + num + '<?php echo get_string('board:alert3', 'local_management'); ?>');
                document.getElementById("uploadfile").value = "";
                return false;
            } else {
                return true;
            }
        }
    }

    $("#timedue").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timeavailable").datepicker("option", "maxDate", selectedDate);
        }
    });

</script>