<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('bannermanagement','local_management'),
    'heading' => get_string('bannermanagement','local_management'),
    'subheading' => '',
    'menu' => 'bannerM',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner_edit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerM/lib.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$banner = lmsdata_banner_get_banner_content($id);

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
$fileobj = lmsdata_banner_get_file($context->id, 'banner', $id, 1);
$temp->itemid = $banner->id;

include_once($CFG->dirroot.'/local/management/header.php');
?>
<section class="content">
<div id="contents">
    <div id="content">
        <form action="banner_submit.php" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="mod" value="edit"/>
            <table class="detail">
                <colgroup>
                    <col width="200px" />
                    <col width="/" />
                </colgroup>
                <tr>
                    <td class="field_title"><?php echo get_string('bannername','local_management'); ?></td>
                    <td class="field_value">
                        <input type="text" name="name" value="<?php echo $banner-> name;?>"
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('attachments','local_management'); ?></td>
                    <td class="field_value">
                        <?php echo $fileobj ?>
                        <?php echo $banner-> filename;?>
                        <input type="file" name="uploadfile" id="uploadfile"/> 
                        <input type="hidden" class="" name="file_id" value="<?php
                        if ($temp->itemid > 0) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                        ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('link','local_management'); ?></td>
                    <td class="field_value">
                        <input type="text" name="linkurl" value="<?php echo $banner-> linkurl;?>"
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('order','local_management'); ?></td>
                    <td class="field_value"><input type="text" name="target" value="<?php echo $banner-> target;?>"></td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('isused','local_management'); ?></td>
                    <td class="field_value">
                        <?php if ($banner->isused == 1) { ?>
                        <input type="radio" name="isused" value="1" checked="checked"><?php echo get_string('used','local_management'); ?>&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0"><?php echo get_string('notused','local_management'); ?></td>
                            <?php 
                            } else {?>
                        <input type="radio" name="isused" value="1" ><?php echo get_string('used','local_management'); ?>&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0" checked="checked"><?php echo get_string('notused','local_management'); ?></td>
                   </td>
                </tr>
                            <?php }?>            
            </table>
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <div id="btn_area">
                        <input type="submit" id="add_user" class="blue_btn btn-area-right" value="<?php echo get_string('save','local_management'); ?>" />
                        <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
            </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->  
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#banner_list').click(function () {
            location.href = "./banner.php";
        });
    });
</script>