<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('communitymanagement','local_management'),
    'heading' => get_string('communitymanagement','local_management'),
    'subheading' => '',
    'menu' => 'facebook',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('facebook_write.php');
    redirect(get_login_url());
}


$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);


if ($id && $mod == 'edit') {
    $data = $DB->get_record('lmsdata_facebook_group', array('id' => $id));
}

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'facebook_group', $data->id, "", false);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form action="facebook_submit.php" id="banner_submit" enctype="multipart/form-data" method="POST">
                    <table>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('groupname','local_management'); ?></td>
                            <td style="text-align:left;" colspan="3"><input type="text" name="name" style="width:70%;" value="<?php echo $data->name; ?>"></td>
                        </tr>
                        <tr>
                            <td class="field_title" style="background-color: #F2F2F2"><?php echo get_string('thumbnail','local_management'); ?><br>(jpg, png)</td>
                            <td class="field_value number" colspan="3">                                
                                <?php
                                if ($files) {
                                    $output = '';
                                    $cnt = 0;
                                    foreach ($files as $file) {
                                        $filename = $file->get_filename();
                                        $mimetype = $file->get_mimetype();
                                        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/facebook_group/' . $id . '/' . $filename);
                                        if ($file->get_filesize() > 0) {
                                            
                                            $fileobj[$cnt] .= "<div class='file-bx' id ='file'><a href=\"$path\">$iconimage</a>";
                                            $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
                                            $fileobj[$cnt] .= '<div class="ex_img"><input type="hidden" name="file_del" value="0"/><img src="' . $path . '"></div>';
                                            $cnt++;
                                        }
                                    }
                                }
                                for ($n = 0; $n <= 0; $n++) {
                                    ?>
                                <div class="fileBox"><input type="file" name="uploadfile" id="uploadfile"/><span>(300*174px)</span></div>

                                        <?php
                                        if (isset($fileobj[$n])) {
                                            echo $fileobj[$n];
                                        }
                                        echo "</div>";
                                    }
                                    ?>
                                    <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                            </td>
                        </tr>

                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('link','local_management'); ?></td>
                            <td colspan="3" style="text-align:left;"><input type="text" name="linkurl" style="width:70%;" value="<?php echo $data->linkurl; ?>"></td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('order','local_management'); ?></td>
                            <td colspan="3" style="text-align:left;"><input type="text" name="target" value="<?php echo $data->order; ?>"></td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('isused','local_management'); ?></td>
                            <td colspan="3" style="text-align:left;">
                                <input type="radio" name="isused" value="1" <?php echo $data->isused == 1 ? 'checked="checked"' : '';?>><?php echo get_string('used','local_management'); ?>&nbsp;&nbsp;&nbsp; 
                                <input type="radio" name="isused" value="0" <?php echo $data->isused == 0 ? 'checked="checked"' : '';?>><?php echo get_string('notused','local_management'); ?>
                            </td>
                        </tr>
                        <input type="hidden" name="mod" value="<?php echo $mod ?>">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                    </table>
                    <div clas="btn_area">
                        <a href="<?php echo $CFG->wwwroot . '/local/management/facebook/facebook_list.php'; ?>"><input type="button" id="notice_list" class="normal_btn f-l" value="<?php echo get_string('list','local_management'); ?>" /></a>
                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save','local_management'); ?>" style="float: right; margin: 0 10px 0 0" />
                    </div> <!-- Bottom Button Area -->
                </form>
            </div> 
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    $(function() {    
        $("input[name=uploadfile]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["jpg", "jpeg", "png"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2','local_management')?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });
    });
</script>