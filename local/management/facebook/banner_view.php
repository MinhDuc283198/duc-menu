<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('bannermanagement','local_management'),
    'heading' => get_string('bannermanagement','local_management'),
    'subheading' => '',
    'menu' => 'bannerM',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/banner_view.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
include_once($CFG->dirroot.'/local/management/header.php');

$id = optional_param('id', 0, PARAM_INT);

$banner = $DB->get_record('lmsdata_banner', array('id' => $id));

if ($banner->id) {
        $fileobj = lmsdata_banner_get_file($context->id, 'banner', $id, 1);
    }
?>
<section class="content">
<div id="contents">
    <div id="content">
        <div class="frm_popup">
            <table cellspadding="0" cellspacing="0" class="detail">
                <tr>
                    <td class="field_title"><?php echo get_string('bannername','local_management'); ?></td>
                    <td class="field_value">
                        <?php echo $banner-> name; ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('attachments','local_management'); ?></td>
                    <td class="field_value">
                            <?php echo $fileobj; ?>
                            <input type="hidden" name="file_del" value="0"/>
                        </td>
                </tr>
                
                <tr>
                    <td class="field_title"><?php echo get_string('link','local_management'); ?></td>
                    <td class="field_value">
                        <?php echo $banner-> linkurl;?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('order','local_management'); ?></td>
                    <td class="field_value">
                        <?php echo $banner->target; ?></td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('isused','local_management'); ?></td>
                    <td class="field_value">
                        <?php if ($banner->isused == 1) {
                            echo get_string("used",'local_managerment');
                        } else {
                            echo get_string("notused",'local_management');
                        } ?></td>
                </tr>
            </table>
            <div id="btn_area">
                <input type="button" id="banner_write"  class="blue_btn" value="<?php echo get_string('edit', 'local_lmsdata'); ?>" style="float:right" />
                <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
            </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->
</section>

<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>

<script type='text/javascript'>
    $(document).ready(function(){
        $('#banner_list').click(function(){
            location.href = "./banner.php";
        });
    $('#banner_write').click(function(){
        location.href = '<?php echo($CFG->wwwroot); ?>/local/management/bannerM/banner_edit.php?id=<?php echo($banner->id); ?>';
        });    
    });
</script>   