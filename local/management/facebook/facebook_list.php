<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('communitymanagement','local_management'),
    'heading' => get_string('communitymanagement','local_management'),
    'subheading' => '',
    'menu' => 'facebook',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('facebook_list.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

include_once($CFG->dirroot . '/local/management/header.php');
include_once($CFG->dirroot . '/local/management/lib/paging.php');

$currpage = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$id = optional_param('id', 0, PARAM_INT);


$totalcount = $DB->count_records_select("lmsdata_facebook_group", array());
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/local/management/facebook/facebook_delete.php'>
                    <table>
                        <caption class="hidden-caption"><?php echo get_string('communitymanagement','local_management'); ?></caption>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="5%"><?php echo get_string('number','local_management'); ?></th>
                                <th scope="row" width="45%"><?php echo get_string('groupname','local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('thumbnail','local_management'); ?></th>
                                <th scope="row" width="15%"><?php echo get_string('link','local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('isused','local_management'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $offset = 0;
                            if ($currpage != 0) {
                                $offset = ($currpage - 1) * $perpage;
                            }
                            $num = $totalcount - $offset;
                            $banners = $DB->get_records_sql('select * from {lmsdata_facebook_group} order by id desc', array(),$offset,$perpage);
                            if ($totalcount) {
                                foreach ($banners as $banner) {
                                    $fs = get_file_storage();
                                    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'facebook_group', $banner->id, "", false);
                                    ?>
                                    <tr>
                                        <td class="chkbox">
                                            <input class="chkbox" type="checkbox" title="check" id="chkbox" name="bannerid<?php echo $banner->id; ?>" value="<?php echo $banner->id; ?>" />
                                        </td>
                                        <td class="number">
                                            <?php echo $num; ?>
                                        </td>   
                                        <td>
                                            <a href='<?php echo $CFG->wwwroot ?>/local/management/facebook/facebook_write.php?mod=edit&id=<?php echo $banner->id ?>'><?php echo $banner->name ?></a>
                                        </td>   
                                        <?php
                                        if ($banner->id) {
                                                    $output = '';
                                                    if (!empty($files)) {
                                                        foreach ($files as $file) {
                                                            $filename = $file->get_filename();
                                                            $mimetype = $file->get_mimetype();
                                                            $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                                            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/facebook_group/' . $banner->id . '/' . $filename);
                                                            if ($file->get_filesize() > 0) {
                                                                $fileobj = '<img class=\'small\' src="' . $path . '">';
                                                            }
                                                        }
                                                    } else {
                                                        $path = $CFG->wwwroot . '/theme/oklassedu/pix/images/default_commu.png';
                                                        $fileobj = '<img class=\'small\' src="' . $path . '">';
                                                    }
                                                }
                                                ?>
                                        <td>
                                            <?php
                                            if ($banner->id) {
                                                echo $fileobj;
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $banner->linkurl ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (($banner->isused) == 1) {
                                                echo get_string('used','local_management');
                                            } else {
                                                echo get_string('notused','local_management');
                                            }
                                            ?>         
                                        </td>
                                    </tr>
                                    <?php
                                    $num--;
                                }
                            } else if (!$totalcount) {
                                ?>
                                <tr><td colspan="6"><span><?php echo get_string('noenrollmentcontents','local_management'); ?></span></td></tr>
<?php } ?>
                        </tbody>
                    </table>
                    <div id="btn_area">
                        <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/facebook/facebook_write.php?mod=add"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                        <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>"  />
                    </div>
                </form>
<?php print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . '/local/management/facebook/facebook_list.php'); ?>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    // $(function() {
//        $("#allcheck").click(function() {
//            var chk = $("#allcheck").is(":checked");
//
//            if (chk) {
//                $(".chkbox input").each(function() {
//                    this.checked = true;
//                });
//            } else {
//                $(".chkbox input").each(function() {
//                    this.checked = false;
//                });
//            }
//        });
    $(function () {
        $("#allcheck").click(function () {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function () {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function () {
                    this.checked = false;
                });
            }
        });
        $("#delete_button").click(function () {
            if (!$(".chkbox").is(":checked")) {
                alert(get_string('board:alert7','local_management'));
                return false;
            }

            if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
                $('#form').submit();
            }
        });
    });


</script>

