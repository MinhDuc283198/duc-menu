function UserEditor(idSelector) {
    var $ul = $("#" + idSelector);
    
    function ajax(action, data) {
        var rvalue = false;

        $.ajax({
            type: 'POST',
            url: 'user.ajax.php',
            dataType: 'JSON',
            async: false,
            data: {action: action, data: data},
            success: function(result) {
                rvalue = result;
            },
            error: function(xhr, status, error) {
            }
        });

        return rvalue;
    }
    
    function get_users($ul) {
        $.ajax({
            type: 'POST',
            url: 'user.ajax.php',
            dataType: 'JSON',
            data: {action: 'users'},
            success: function(data) {
                if(data.status == 'success') {
                    $ul.empty();
                    $.each(data.users, function (k, v) {
                        var $li = render_user(v);
                        $ul.append($li);
                    });
                } else {
                    alert(data.error);
                }
            },
            error: function(xhr, status, error) {}
        });
    }
    
    function render_user(user) {
        var pictureItem = $('<img>').attr('src', user.picture).addClass('user-image');
        var textItem = $('<span>').addClass('txt').text(user.fullname);
        var btnGroup = $('<div>').addClass('btn-group pull-right');
        var $btnInfo = $("<a>").addClass('btn btn-primary btn-sm btnInfo clickable').attr("href", "#").html('<i class="fa fa-info-circle clickable"></i>');
        btnGroup.append($btnInfo);
        $($btnInfo).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            show_user_info($li);
            get_groups($li);
            get_menus($li);
        });

        var $btnRemove = $("<a>").addClass('btn btn-danger btn-sm btnRemove clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
        btnGroup.append($btnRemove);
        $($btnRemove).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            remove_user($li);
        });

        var div = $('<div>').css({"overflow": "auto"}).append(pictureItem).append("&nbsp;").append(textItem).append(btnGroup);
        var $li = $("<li>").data(user);
        $li.addClass('list-group-item pr-0').append(div);
        
        return $li;
    }
    
    function show_user_info($item) {
        $('.user-info').find('.profile-user-img').attr('src', $item.data('picture'));
        $('.user-info').find('.profile-username').html($item.data('fullname'));
        $('.user-info').find('#email').html($item.data('email'));
        $('.user-info').find('#company').html($item.data('company'));
        $('.user-info').find('#position').html($item.data('position'));
    }
    
    function remove_user($item) {
        if(confirm("사용자를 삭제하시겠습니까?")) {
            var result = ajax('remove', {'userid': $item.data('id')});
            if(result.status == 'success') {
                $item.remove();
                reset();
            } else {
                alert(result.error);
            }
        }
    }
    
    function get_groups($item) {
        $.ajax({
            type: 'POST',
            url: 'user.ajax.php',
            dataType: 'JSON',
            data: {action: 'groups', data: {userid: $item.data('id')}},
            success: function(data) {
                if(data.status == 'success') {
                    $ulG = $('#groupEditor');
                    $ulG.empty();
                    $.each(data.groups, function (k, v) {
                        var $li = render_group(v);
                        $ulG.append($li);
                    });
                } else {
                    alert(data.error);
                }
            },
            error: function(xhr, status, error) {}
        });
    }
    
    function render_group(group) {
        var $li = $('<li>').addClass('list-group-item pr-0');
        $li.data(group);
        var $div = $('<div>').css('overflow', 'auto');
        var $i = $('<i>').addClass('fa fa-group');
        var $span = $('<span>').addClass('txt').append(group.name).css('margin-right', '5px');
        
        var $divbtn = $('<div>').addClass('btn-group pull-right');
        var $btnRemove = $("<a>").addClass('btn btn-danger btn-sm btnRemove clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
        $divbtn.append($btnRemove);
        $($btnRemove).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            remove_group($li);
        });
        
        $div.append($i).append("&nbsp;").append($span).append($divbtn);
        $li.append($div);
        
        return $li;
    }
    
    function remove_group($item) {
        if(confirm("사용자를 그룹에서 삭제하시겠습니까?")) {
            var result = ajax('remove_group', {'groupid': $item.data('id'),'userid': $item.data('userid')});
            if(result.status == 'success') {
                $item.remove();
            } else {
                alert(result.error);
            }
        }
    }
    
    function get_menus($item) {
        $.ajax({
            type: 'POST',
            url: 'user.ajax.php',
            dataType: 'JSON',
            data: {action: 'menus', data: {userid: $item.data('id')}},
            success: function(data) {
                if(data.status == 'success') {
                    $ulM = $('#menuEditor');
                    $ulM.empty();
                    $.each(data.menus, function (k, v) {
                        var $li = render_menu(v);
                        $ulM.append($li);
                    });
                } else {
                    alert(data.error);
                }
            },
            error: function(xhr, status, error) {}
        });
    }
    
    function render_menu(menu) {
        var iconItem = $('<i>').addClass(menu.icon);
        var textItem = $('<span>').addClass('txt').text(menu.text);
        var btnGroup = $('<div>').addClass('btn-group pull-right');
//        var $btnInfo = $("<a>").addClass('btn btn-primary btn-sm btnInfoMenu clickable').attr("href", "#").html('<i class="fa fa-info-circle clickable"></i>');
//        $btnInfo.attr('data-toggle', 'modal').attr('data-target', '#modal-member-info');
//        btnGroup.append($btnInfo);
//        $($btnInfo).on('click', function (e) {
//            e.preventDefault();
//            var $li = $(this).closest('li');
//            show_menu_info($li);
//        });

        var $btnRemove = $("<a>").addClass('btn btn-danger btn-sm btnRemoveMenu clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
        btnGroup.append($btnRemove);
        $($btnRemove).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            remove_menu($li);
        });

        var div = $('<div>').css({"overflow": "auto"}).append(iconItem).append("&nbsp;").append(textItem).append(btnGroup);
        var $li = $("<li>").data(menu);
        $li.addClass('list-group-item pr-0').append(div);
        
        return $li;
    }
    
    function remove_menu($item) {
        if(confirm("메뉴를 삭제하시겠습니까?")) {
            var result = ajax('remove_menu', {'menuid': $item.data('id'),'userid': $item.data('userid')});
            if(result.status == 'success') {
                $item.remove();
            } else {
                alert(result.error);
            }
        }
    }
    
    function reset() {
        $('.user-info').find('.profile-user-img').attr('src', './img/blank-profile.png');
        $('.user-info').find('.profile-username').text('');
        $('.user-info').find('#email').text('');
        $('.user-info').find('#company').text('');
        $('.user-info').find('#position').text('');
        
        $('#groupEditor').empty();
        $('#menuEditor').empty();
    }
    
    return {
        init: function() {
            get_users($ul);
        }
    }
}