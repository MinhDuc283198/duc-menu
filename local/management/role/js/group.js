function GroupEditor(idSelector) {
    var $ul = $("#" + idSelector);
    var $addButton = $('#btnAddGroup');
    var $updateButton = $('#btnUpdateGroup');
    var $addMemberButton = $('#btnAddMember');
    var $uploadExcelButton = $('#btnUploadExcel');
    var $searchUserButton = $('#btnSearchUser');
    var $form = $('#frmEdit');
    var $itemEditing = null;

    function ajax(action, data) {
        var rvalue = false;

        $.ajax({
            type: 'POST',
            url: 'group.ajax.php',
            dataType: 'JSON',
            async: false,
            data: {action: action, data: data},
            success: function(result) {
                rvalue = result;
            },
            error: function(xhr, status, error) {
            }
        });

        return rvalue;
    }

    function get_groups($ul) {
        $.ajax({
            type: 'POST',
            url: 'group.ajax.php',
            dataType: 'JSON',
            data: {action: 'groups'},
            success: function(data) {
                if(data.status == 'success') {
                    $ul.empty();
                    $.each(data.groups, function (k, v) {
                        var $li = $('<li>').addClass('list-group-item pr-0');
                        $li.data(v);
                        var $div = $('<div>').css('overflow', 'auto');
                        var $i = $('<i>').addClass('fa fa-group');
                        var $span = $('<span>').addClass('txt').append(v.name).css('margin-right', '5px');
                        var $divbtn =  buttons($li.data().ismain);
                        $div.append($i).append("&nbsp;").append($span).append($divbtn);
                        $li.append($div);
                        $ul.append($li);
                    });
                } else {
                    alert(data.error);
                }
            },
            error: function(xhr, status, error) {}
        });
    }

    function buttons(ismain) {
        var $divbtn = $('<div>').addClass('btn-group pull-right');
        var $btnEdit = $("<a>").addClass('btn btn-primary btn-sm btnEdit clickable').attr("href", "#").html('<i class="fa fa-edit clickable"></i>');
        $divbtn.append($btnEdit);
        $($btnEdit).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            edit($li);
        });

        if(ismain != '1') {
            var $btnRemv = $("<a>").addClass('btn btn-danger btn-sm btnRemove clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
            $divbtn.append($btnRemv);
            $($btnRemv).on('click', function (e) {
                e.preventDefault();
                var $li = $(this).closest('li');
                remove($li);
            });
        }
        return $divbtn;
    }

    function edit($item) {
        $itemEditing = $item;
        
        // 편집 창 셋팅
        var data = $item.data();
        $.each(data, function (p, v) {
            $form.find("[name=" + p + "]").val(v);
        });
        
        // 멤버 불러오기
        get_members(data.id);
        
        get_menus(data.id);
        
        $('small#group-name').text(data.name);
        
        $updateButton.removeAttr('disabled');
        $addMemberButton.removeAttr('disabled');
        $uploadExcelButton.removeAttr('disabled');
    }

    function validate() {
        var data = {};

        $form.find('input[type=hidden]').each(function(){
            if($(this).attr('name') != undefined) {
                data[$(this).attr('name')] = $(this).val().trim();
            }
        });

        $form.find('.item-menu').each(function(){
            if($(this).attr('name') != undefined) {
                data[$(this).attr('name')] = $(this).val().trim();
            }
        });

        if(data.name.length == 0) {
            alert("Text를 입력하세요.");
            return null;
        }
        if(data.shortname.length == 0) {
            alert("Identification을 입력하세요.");
            return null;
        }

        return data;
    }

    function add() {
        var data = validate();
        if(data != null) {
            var result = ajax('add', data);
            if(result.status == 'success') {
                var btnGroup = buttons('0');
                var textItem = $('<span>').addClass('txt').text(result.group.name);
                var iconItem = $('<i>').addClass('fa fa-group');
                var div = $('<div>').css({"overflow": "auto"}).append(iconItem).append("&nbsp;").append(textItem).append(btnGroup);
                var $li = $("<li>").data(result.group);
                $li.addClass('list-group-item pr-0').append(div);
                $ul.append($li);
                reset_form();
            } else {
                alert(result.error);
            }
        }
    }

    function update() {
        var data = validate();
        if(data != null) {
            var result = ajax('update', data);
            if(result.status == 'success') {
                $form.find('.item-menu').each(function(){
                    $itemEditing.data($(this).attr('name'), $(this).val());
                });
                $itemEditing.find('span.txt').first().text($itemEditing.data('name'));
                reset_form();
            } else {
                alert(result.error);
            }
        }
    }

    function remove($item) {
        if(confirm("그룹을 삭제하시겠습니까?")) {
            var data = {id: $item.data('id')};
            var result = ajax('remove', data);
            if(result.status == 'success') {
                $item.remove();
                reset_form();
            } else {
                alert(result.error);
            }
        }
    }

    function get_members(gid) {
        var result = ajax('members', {id: gid});
        if(result.status = 'success') {
            var $ulMember = $('#memberEditor');
            $ulMember.empty();
            $.each(result.members, function() {
                var $li = render_member(this);
                
                $ulMember.append($li);
            });
        }
    }
    
    function render_member(member) {
        var pictureItem = $('<img>').attr('src', member.picture).addClass('user-image');
        var textItem = $('<span>').addClass('txt').text(member.fullname);
        var btnGroup = $('<div>').addClass('btn-group pull-right');
        var $btnInfo = $("<a>").addClass('btn btn-primary btn-sm btnInfo clickable').attr("href", "#").html('<i class="fa fa-info-circle clickable"></i>');
        $btnInfo.attr('data-toggle', 'modal').attr('data-target', '#modal-member-info');
        btnGroup.append($btnInfo);
        $($btnInfo).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            show_member_info($li);
        });

        var $btnRemove = $("<a>").addClass('btn btn-danger btn-sm btnRemove clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
        btnGroup.append($btnRemove);
        $($btnRemove).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            remove_member($li);
        });

        var div = $('<div>').css({"overflow": "auto"}).append(pictureItem).append("&nbsp;").append(textItem).append(btnGroup);
        var $li = $("<li>").data(member);
        $li.addClass('list-group-item pr-0').append(div);
        
        return $li;
    }

    function add_member(member) {
        var gid = $itemEditing.data('id');
        var uid = member.id;
        var result = ajax('add_member', {'groupid': gid, 'userid': uid});
        if(result.status == 'success') {
            member.memberid = result.memberid;
            $li = render_member(member);
            var $ulMember = $('#memberEditor');
            $ulMember.append($li);
        } else {
            alert(result.error);
        }
    }

    function remove_member($item) {
        if(confirm("그룹 멤버를 삭제하시겠습니까?")) {
            var result = ajax('remove_member', {id: $item.data('memberid')});
            if(result.status = 'success') {
                $item.remove();
            } else {
                alert(result.error);
            }
        }
    }
    
    function show_member_info($item) {
        $('#modal-member-info .modal-body').find('.profile-user-img').attr('src', $item.data('picture'));
        $('#modal-member-info .modal-body').find('.profile-username').text($item.data('fullname'));
        $('#modal-member-info .modal-body').find('#email').text($item.data('email'));
        $('#modal-member-info .modal-body').find('#company').text($item.data('company'));
        $('#modal-member-info .modal-body').find('#position').text($item.data('position'));
    }
    
    function search_user() {
        if($itemEditing == null) {
            return false;
        }
        
        var id = $itemEditing.data('id');
        var search = $('form#searchUser input[name=search]').val();
        
        if(search.trim().length == 0) {
            alert("검색어를 입력하세요");
            return false;
        }
        
        var result = ajax('search_user', {id: id, search: search});
        if(result.status == 'success') {
            $tbody = $('#modal-add-member #user-list table tbody');
            $tbody.empty();
            if(result.users.length > 0) {
                $.each(result.users, function() {
                    $tr = $('<tr>');
                    $tr.data(this);
                    
                    var $tdFullName =$('<td>');
                    var $picture = $('<img>').attr('src', this.picture).addClass('user-image');
                    var $fullname = $('<span>').addClass('txt').text(this.fullname);
                    $tdFullName.append($picture).append("&nbsp;").append($fullname);
                    
                    var $tdEmail =$('<td>').text(this.email);
                    var $tdCompany =$('<td>').text(this.company);
                    
                    var $tdButtons =$('<td>');;
                    var $divbtn = $('<div>').addClass('btn-group pull-right');
                    var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                    $divbtn.append($btnAdd);
                    $($btnAdd).on('click', function (e) {
                        e.preventDefault();
                        var $tr = $(this).closest('tr');
                        add_member($tr.data());
                        $('#modal-add-member').modal('hide');
                    });
                    $tdButtons.append($divbtn);
                    
                    $tr.append($tdFullName).append($tdEmail).append($tdCompany).append($tdButtons);
                    $tbody.append($tr);
                });
            } else {
                $tbody.append('<tr><td colspan="4">데이터가 없습니다</td></tr>');
            }
        } else {
            alert(result.error);
        }
    }
    
    function get_menus(groupid) {
        var result = ajax('menus', {id: groupid});
        if(result.status = 'success') {
            var $ul = $('#menuEditor');
            $ul.empty();
            $.each(result.menus, function() {
                var $li = render_menu(this);
                $ul.append($li);
            });
        } else {
            alert(result.error);
        }
    }
    
    function render_menu(menu) {
        var iconItem = $('<i>').addClass(menu.icon);
        var textItem = $('<span>').addClass('txt').text(menu.text);
        var btnGroup = $('<div>').addClass('btn-group pull-right');
        var $btnRemove = $("<a>").addClass('btn btn-danger btn-sm btnRemoveMenu clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
        btnGroup.append($btnRemove);
        $($btnRemove).on('click', function (e) {
            e.preventDefault();
            var $li = $(this).closest('li');
            remove_menu($li);
        });

        var div = $('<div>').css({"overflow": "auto"}).append(iconItem).append("&nbsp;").append(textItem).append(btnGroup);
        var $li = $("<li>").data(menu);
        $li.addClass('list-group-item pr-0').append(div);
        
        return $li;
    }
    
    function remove_menu($item) {
        if(confirm("그룹 메뉴를 삭제하시겠습니까?")) {
            var result = ajax('remove_menu', {groupid: $item.data('groupid'), menuid: $item.data('id')});
            if(result.status = 'success') {
                $item.remove();
            } else {
                alert(result.error);
            }
        }
    }
    
    function reset_form() {
        $form[0].reset();
        $updateButton.attr('disabled', true);
        $addMemberButton.attr('disabled', true);
        $uploadExcelButton.attr('disabled', true);
        $itemEditing = null;
        $('#memberEditor').empty();
        $('small#group-name').html('');
    }
    
    return {
        init: function() {
            $addButton.on('click', add);
            $updateButton.on('click', update);
            $searchUserButton.on('click', search_user);
            $("#searchUser input[type=text][name=search]").keypress(function(e) {
                if (e.keyCode == 13){
                    e.preventDefault();
                    search_user();
                }
            });
            
            $('#modal-add-member').on('show.bs.modal', function (event) {
                $('#modal-add-member input[name=search]').val('');
                $tbody = $('#modal-add-member #user-list table tbody');
                $tbody.empty();
                $tbody.append('<tr><td colspan="4">데이터가 없습니다</td></tr>');
            });
            
            get_groups($ul);
        }
    }
}


