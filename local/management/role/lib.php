<?php

define('LOCAL_MANAGEMENT_GROUP_SYSADMIN', 1);

/**
 * 그룹을 추가한다.
 * 
 * @global object $DB
 * @global object $USER
 * @param object $group
 * @return int
 */
function local_management_role_add_group($group) {
    global $DB, $USER;
    
    //shortname 중복되는지 체크
    if($DB->record_exists('management_group', array('shortname' => $group->shortname))) {
        print_error('duplicatedshortname', 'local_management');
    }
    
    $group->ismain = 0;
    $group->timecreated = time();
    $group->creator = $USER->id;
    
    return $DB->insert_record('management_group', $group);
}

/**
 * 그룹 정보를 업데이트 한다.
 * 
 * @global object $DB
 * @global object $USER
 * @param object $group
 * @return int
 */
function local_management_role_update_group($group) {
    global $DB, $USER;
    
    $groupold = $DB->get_record('management_group', array('id' => $group->id));
    
    //shortname 중복되는지 체크
    if($groupold->shortname != $group->shortname) {
        if($DB->record_exists('management_group', array('shortname' => $group->shortname))) {
            print_error('duplicatedshortname', 'local_management');
        }
    }
    
    return $DB->update_record('management_group', $group);
}

/**
 * 그룹을 삭제한다.
 * 그룹 멤버들도 삭제된다
 * 
 * @global object $DB
 * @param int $id
 * @return int
 */
function local_management_role_delete_group($id) {
    global $DB;
    
    $rvalue = false;
    
    $group = $DB->get_record('management_group', array('id' => $id));
    if($group) {
        if($menu->ismain == 1) {
            print_error('cannotbedeleted', 'local_management');
        }
        
        // 그룹 멤버 삭제
        $DB->delete_records('management_group_member', array('groupid' => $id));
        
        $rvalue = $DB->delete_records('management_group', array('id' => $id));
    }
    
    return $rvalue;
}

/**
 * 그룹에 추가할 사용자를 검색한다.
 * $groupid가 지정되는 경우 해당 그룹에 소속된 사용자는 검색에서 제외시킨다.
 * 
 * @global object $DB
 * @global object $PAGE
 * @param int $groupid
 * @param string $search
 * @return array
 */
function local_management_role_search_user($groupid = 0, $search = '') {
    global $DB, $PAGE;
    
    $allnames = get_all_user_name_fields(true, 'u');
    $picturefields = user_picture::fields('u');
    
    $userfields = array_merge(explode(',', $allnames), explode(',', $picturefields));
    $userfields = array_unique($userfields);
    
    if (($key = array_search('u.id', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    if (($key = array_search('u.firstname', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    
    $struserfields = implode(', ', $userfields);
    
    $sql = "SELECT u.id, u.username, u.firstname, $struserfields
            FROM {user} u ";
    
    $conditions = array();
    $params = array();
    
    if($groupid) {
        $sql .= " LEFT JOIN {management_group_member} m ON m.groupid = :groupid AND m.userid = u.id";
        $params['groupid'] = $groupid;
        $conditions[] = 'm.userid IS NULL';
    }
    
    if(!empty($search)) {
        $conditionsearch = array();
        
        // 이름
        $conditionsearch[] = $DB->sql_like("u.firstname", ":firstname", false);
        $params['firstname'] = "%$search%";
        
        // 이메일
        $conditionsearch[] = $DB->sql_like("u.email", ":email", false);
        $params['email'] = "%$search%";
        
        // 직군
        //$conditionsearch[] = $DB->sql_like("job.name", ":job", false);
        //$params['job'] = "%$search%";
        
        // 회사
        //$conditionsearch[] = $DB->sql_like("comp.cust_if_nm", ":company", false);
        //$params['company'] = "%$search%";
        
        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }
    
    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = ' WHERE ' . implode(' AND ', $conditions);
    }
    
    $sqlsort = "ORDER BY u.firstname, u.email";
    
    $members = $DB->get_records_sql("$sql $sqlwhere $sqlsort",
                $params);
    
    foreach ($members as $member) {
        $userpicture = new user_picture($member);
        $userpicture->size = 64;
        $member->picture = $userpicture->get_url($PAGE)->out(false);
        $member->fullname = fullname($member);
    }
    
    return $members;
}

/**
 * 그룹 멤버를 구한다.
 * $groupid가 지정되지 않는 경우 전체 그룹의 멤버를 리턴한다.
 * 
 * @global object $DB
 * @global object $PAGE
 * @param int $groupid
 * @param string $search
 * @return array
 */
function local_management_role_get_group_members($groupid = 0, $search = '') {
    global $DB, $PAGE;
    
    $allnames = get_all_user_name_fields(true, 'u');
    $picturefields = user_picture::fields('u');
    
    $userfields = array_merge(explode(',', $allnames), explode(',', $picturefields));
    $userfields = array_unique($userfields);
    
    if (($key = array_search('u.id', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    if (($key = array_search('u.firstname', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    
    $struserfields = implode(', ', $userfields);
    
    $conditions = array();
    $params = array();
    
    $sql = "SELECT DISTINCT u.id, u.username, u.firstname, $struserfields
                , m.id AS memberid
            FROM {management_group_member} m
            JOIN {user} u ON u.id = m.userid
            ";
    
    if($groupid) {
        $conditions[] = 'm.groupid = :groupid';
        $params['groupid'] = $groupid;
    }
    
    if(!empty($search)) {
        $conditionsearch = array();
        
        // 이름
        $conditionsearch[] = $DB->sql_like("u.firstname", ":firstname", false);
        $params['firstname'] = "%$search%";
        
        // 이메일
        $conditionsearch[] = $DB->sql_like("u.email", ":email", false);
        $params['email'] = "%$search%";
        
        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }
    
    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = " WHERE " . implode(' AND ', $conditions);
    }
    
    $sqlsort = "ORDER BY u.firstname, u.email";
    
    $members = $DB->get_records_sql("$sql $sqlwhere $sqlsort",
                $params);
    
    foreach ($members as $member) {
        $userpicture = new user_picture($member);
        $userpicture->size = 64;
        $member->picture = $userpicture->get_url($PAGE)->out(false);
        $member->fullname = fullname($member);
    }
    
    return $members;
}

/**
 * 그룹에 등록된 메뉴 목록을 반환한다.
 * 
 * @global object $DB
 * @param int $groupid
 * @param string $search
 * @return array
 */
function local_management_role_get_group_memus($groupid = 0, $search = '') {
    global $DB;
    
    $conditions = array();
    $params = array();
    
    $sql = "SELECT m.*
            , g.groupid
        FROM {management_menu} m
        JOIN {management_menu_group} g ON g.menuid = m.id";
    
    if($groupid) {
        $conditions[] = 'g.groupid = :groupid';
        $params['groupid'] = $groupid;
    }
    
    if(!empty($search)) {
        $conditionsearch = array();
        
        $conditionsearch[] = $DB->sql_like("m.text", ":text", false);
        $params['text'] = "%$search%";
        
        $conditionsearch[] = $DB->sql_like("m.shortname", ":shortname", false);
        $params['shortname'] = "%$search%";
        
        $conditionsearch[] = $DB->sql_like("m.href", ":href", false);
        $params['href'] = "%$search%";
        
        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }
    
    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = " WHERE " . implode(' AND ', $conditions);
    }
    
    $sqlsort = "ORDER BY m.text, m.shortname";
    
    $menus = $DB->get_records_sql("$sql $sqlwhere $sqlsort",
                $params);
    
    return $menus;
}

/**
 * 메뉴를 삭제한다.
 * 
 * @global object $DB
 * @param int $groupid
 * @param int $menuid
 * @return int
 */
function local_management_role_delete_menu($groupid, $menuid) {
    global $DB;
    
    return $DB->delete_records('management_menu_group', array('groupid' => $groupid, 'menuid' => $menuid));
}

/**
 * 그룹 멤버와 메뉴에 등록된 사용자를 구한다.
 * 
 * @global object $DB
 * @global object $PAGE
 * @param string $search
 * @return array
 */
function local_management_role_get_members($search = '') {
    global $DB, $PAGE;
    
    $allnames = get_all_user_name_fields(true, 'u');
    $picturefields = user_picture::fields('u');
    
    $userfields = array_merge(explode(',', $allnames), explode(',', $picturefields));
    $userfields = array_unique($userfields);
    
    if (($key = array_search('u.id', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    if (($key = array_search('u.firstname', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    
    $struserfields = implode(', ', $userfields);
    
    $conditions = array();
    $params = array();
    
    $sql = "SELECT u.id, u.username, u.firstname, $struserfields

            FROM (SELECT userid FROM {management_group_member}
                  UNION
                  SELECT userid FROM {management_menu_user}) m
            JOIN {user} u ON u.id = m.userid

            ";
    
    if(!empty($search)) {
        $conditionsearch = array();
        
        // 이름
        $conditionsearch[] = $DB->sql_like("u.firstname", ":firstname", false);
        $params['firstname'] = "%$search%";
        
        // 이메일
        $conditionsearch[] = $DB->sql_like("u.email", ":email", false);
        $params['email'] = "%$search%";
        
        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }
    
    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = " WHERE " . implode(' AND ', $conditions);
    }
    
    $sqlsort = "ORDER BY u.firstname, u.email";

    $members = $DB->get_records_sql("$sql $sqlwhere $sqlsort",
                $params);
    
    foreach ($members as $member) {
        $userpicture = new user_picture($member);
        $userpicture->size = 64;
        $member->picture = $userpicture->get_url($PAGE)->out(false);
        $member->fullname = fullname($member);
    }
    
    return $members;
}

/**
 * 사용자가 속한 그룹 목록을 반환한다.
 * 
 * @global object $DB
 * @param int $userid
 * @return array
 */
function local_management_role_get_user_groups($userid) {
    global $DB;
    
    $groups = $DB->get_records_sql("SELECT g.id, g.name, g.shortname, g.description, g.ismain
            , m.userid
        FROM {management_group} g 
        JOIN {management_group_member} m ON m.groupid = g.id
        WHERE m.userid = :userid", array('userid' => $userid));
    
    return $groups;
}

/**
 * 사용자를 그룹의 멤버로 등록한다.
 * 
 * @global object $DB
 * @global object $USER
 * @param int $groupid
 * @param int $userid
 * @return int|boolean
 */
function local_management_role_add_group_member($groupid, $userid) {
    global $DB, $USER;
    
    if(!$DB->record_exists('management_group_member', array('groupid' => $groupid, 'userid' => $userid))) {
        $member = new stdClass();
        $member->groupid = $groupid;
        $member->userid = $userid;
        $member->creator = $USER->id;
        $member->timecreated = time();
        return $DB->insert_record('management_group_member', $member);
    }
    
    return false;
}

function local_management_role_delete_group_member($id) {
    global $DB;
    
    return $DB->delete_records('management_group_member', array('id' => $id));
}

/**
 * 사용자를 삭제한다.
 * 사용자가 속한 그룹, 사용자 메뉴를 모두 삭제한다.
 * 
 * @global object $DB
 * @param int $userid
 */
function local_management_role_delete_member($userid) {
    global $DB;
    
    // 소속된 그룹에서 삭제
    $DB->delete_records('management_group_member', array('userid' => $userid));
    
    // 메뉴 삭제
    $DB->delete_records('management_menu_user', array('userid' => $userid));
}

/**
 * 사용자의 메뉴 목록을 반환한다.
 * 
 * @global object $DB
 * @param int $userid
 * @param int $search
 * @return array
 */
function local_management_role_get_user_menus($userid = 0, $search = '') {
    global $DB;
    
    $conditions = array();
    $params = array();
    
    $sql = "SELECT m.*
            , u.userid
        FROM {management_menu} m
        JOIN {management_menu_user} u ON u.menuid = m.id";
    
    if($userid) {
        $conditions[] = 'u.userid = :userid';
        $params['userid'] = $userid;
    }
    
    if(!empty($search)) {
        $conditionsearch = array();
        
        $conditionsearch[] = $DB->sql_like("m.text", ":text", false);
        $params['text'] = "%$search%";
        
        $conditionsearch[] = $DB->sql_like("m.shortname", ":shortname", false);
        $params['shortname'] = "%$search%";
        
        $conditionsearch[] = $DB->sql_like("m.href", ":href", false);
        $params['href'] = "%$search%";
        
        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }
    
    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = " WHERE " . implode(' AND ', $conditions);
    }
    
    $sqlsort = "ORDER BY m.text, m.shortname";
    
    $menus = $DB->get_records_sql("$sql $sqlwhere $sqlsort",
                $params);
    
    return $menus;
}

/**
 * 사용자의 메뉴를 삭제한다.
 * 
 * @global object $DB
 * @param int $userid
 * @param int $menuid
 * @return int
 */
function local_management_role_delete_user_menu($userid, $menuid) {
    global $DB;
    
    return $DB->delete_records('management_menu_user', array('menuid' => $menuid, 'userid' => $userid));
}

/**
 * 사용자가 그룹의 멤버이면 true 반환
 * 
 * @global object $DB
 * @param int $groupid
 * @param int $userid
 * @return bool
 */
function local_management_role_is_group_member($groupid, $userid = 0) {
    global $DB, $USER;
    
    if(!$userid) {
        $userid = $USER->id;
    }
    
    return $DB->record_exists_sql("SELECT 1
        FROM {management_group_member} gm
        JOIN {management_group} g ON g.id = gm.groupid
        WHERE gm.userid = :userid 
          AND g.id = :id", array('userid' => $userid, 'id' => $groupid));
}

/**
 * 사용자가 그룹의 멤버이면 true 반환
 * 
 * @global object $DB
 * @param string $sortname
 * @param int $userid
 * @return bool
 */
function local_management_role_is_group_member_by_identification($sortname, $userid = 0) {
    global $DB, $USER;
    
    if(!$userid) {
        $userid = $USER->id;
    }
    
    return $DB->record_exists_sql("SELECT 1
        FROM {management_group_member} gm
        JOIN {management_group} g ON g.id = gm.groupid
        WHERE gm.userid = :userid 
          AND g.shortname = :shortname", array('userid' => $userid, 'shortname' => $sortname));
}