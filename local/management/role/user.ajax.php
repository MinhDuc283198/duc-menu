<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/role/lib.php');

$action = required_param('action', PARAM_ALPHAEXT);

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('user');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}

$rdata = new stdClass();

switch($action) {
    case 'users':
        $users = local_management_role_get_members();
        $rdata->status = 'success';
        $rdata->users = array_values($users);
        break;
    case 'groups':
        $data = (object) required_param_array('data', PARAM_RAW);
        $groups = local_management_role_get_user_groups($data->userid);
        $rdata->status = 'success';
        $rdata->groups = array_values($groups);
        break;
    case 'remove':
        $data = (object) required_param_array('data', PARAM_RAW);
        local_management_role_delete_member($data->userid);
        $rdata->status = 'success';
        break;
    case 'remove_group':
        $data = (object) required_param_array('data', PARAM_RAW);
        $member = $DB->get_record('management_group_member', array('groupid' => $data->groupid, 'userid' => $data->userid));
        if($member) {
            local_management_role_delete_group_member($member->id);
        } else {
            print_error('invalidarguments');
        }
        $rdata->status = 'success';
        break;
    case 'menus':
        $data = (object) required_param_array('data', PARAM_RAW);
        $menus = local_management_role_get_user_menus($data->userid);
        $rdata->status = 'success';
        $rdata->menus = array_values($menus);
        break;
    case 'remove_menu':
        $data = (object) required_param_array('data', PARAM_RAW);
        local_management_role_delete_user_menu($data->userid, $data->menuid);
        $rdata->status = 'success';
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

echo $OUTPUT->header();
echo json_encode($rdata);
echo $OUTPUT->footer();