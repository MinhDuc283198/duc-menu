<?php

/**
 * 그룹 목록, 추가, 수정, 삭제
 * 그룹에 사용자 추가/삭제
 */


require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '그룹 관리',
    'heading' => '그룹 관리',
    'subheading' => '',
    'menu' => 'authM',
    'js' => array('/local/management/role/group.js'),
    'css' => array(),
    'nav_bar'=> array()
);

include_once('../header.php');

?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">그룹</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="groupEditor" class="list-group">
                    </ul>
                </div>
            </div>
            
            <div class="box box-profile">
                <div class="box-header with-border">
                    <h3 class="box-title">그룹 멤버 <small id="group-name"></small></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="memberEditor" class="list-group">
                    </ul>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnAddMember" class="btn btn-danger" data-toggle="modal" data-target="#modal-add-member" disabled="disabled"><i class="fa fa-plus"></i> 추가</button>
                    <!-- button type="button" id="btnUploadExcel" class="btn btn-danger" data-toggle="modal" data-target="#modal-upload-excel" disabled="disabled"><i class="fa fa-file-excel-o"></i> 엑셀 업로드</button -->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info box-menu">
                <div class="box-header with-border">
                    <h3 class="box-title">편집</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form id="frmEdit" role="form">
                        <input type="hidden" name="id" value="0" />
                        <div class="form-group">
                            <label for="href">Name</label>
                            <input type="text" class="form-control item-menu" id="name" name="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="href">Identification</label>
                            <input type="text" class="form-control item-menu" id="shortname" name="shortname" placeholder="Identification">
                        </div>
                        <div class="form-group">
                            <label for="target">Description</label>
                            <textarea class="form-control item-menu" rows="3" name="description" placeholder="Description"></textarea>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnUpdateGroup" class="btn btn-primary" disabled="disabled"><i class="fa fa-refresh"></i> 수정</button>
                    <button type="button" id="btnAddGroup" class="btn btn-success"><i class="fa fa-plus"></i> 추가</button>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">그룹 메뉴 <small id="group-name"></small></h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="menuEditor" class="list-group">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once('../footer.php');
?>
<div class="modal modal-default fade modal-dialog-centered" id="modal-member-info">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">멤버 정보</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-profile col-md-3">
                    <img class="profile-user-img img-responsive img-circle"/>
                    <h3 class="profile-username text-center"></h3>
                </div>
                <div class="col-md-9">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>이메일</b> <span class="pull-right" id="email"></span>
                        </li>
                        <li class="list-group-item">
                            <b>회사</b> <span class="pull-right" id="company"></span>
                        </li>
                        <li class="list-group-item">
                            <b>직위</b> <span class="pull-right" id="position"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
        </div>
      </div>
    </div>
</div>
<div class="modal modal-default fade modal-dialog-centered" id="modal-add-member">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">멤버 추가</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-danger col-md-12">
                    <form action="#" method="post" id="searchUser">
                        <input type="hidden" name="id" value="0" />
                        <div class="input-group">
                            <input type="text" name="search" placeholder="이름, 이메일로 검색하세요" class="form-control"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-flat" id="btnSearchUser">검색</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-12" id="user-list">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>이름</th>
                                <th>이메일</th>
                                <th>회사</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="4">데이터가 없습니다</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
        </div>
      </div>
    </div>
</div>

<div class="modal modal-default fade modal-dialog-centered" id="modal-upload-excel">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">멤버 추가</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-danger col-md-12">
                    <form action="#" method="post" id="searchUser">
                        <input type="hidden" name="id" value="0" />
                        <div class="input-group">
                            <input type="text" name="search" placeholder="이름, 이메일로 검색하세요" class="form-control"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-flat" id="btnSearchUser">검색</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-12" id="user-list">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>이름</th>
                                <th>이메일</th>
                                <th>회사</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="4">데이터가 없습니다</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
          <button type="button" class="btn btn-primary">추가</button>
        </div>
      </div>
    </div>
</div>
