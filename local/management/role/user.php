<?php

/** 
 * 권한이 부여된 수용자 목록, 사용자별 메뉴 목록, 소속된 그룹
 */

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '사용자 관리',
    'heading' => '사용자 관리',
    'subheading' => '',
    'menu' => 'usersM',
    'js' => array('/local/management/role/user.js'),
    'css' => array(),
    'nav_bar'=> array()
);

include_once('../header.php');
?>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">사용자</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="userEditor" class="list-group">
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box box-profile user-info">
                <div class="box-header with-border">
                    <h4 class="box-title">사용자 정보</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="box-profile col-md-3">
                            <img src="./img/blank-profile.png" class="profile-user-img img-responsive img-circle"/>
                            <h3 class="profile-username text-center"></h3>
                        </div>
                        <div class="col-md-9">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>이메일</b> <span class="pull-right" id="email"></span>
                                </li>
                                <li class="list-group-item">
                                    <b>회사</b> <span class="pull-right" id="company"></span>
                                </li>
                                <li class="list-group-item">
                                    <b>직위</b> <span class="pull-right" id="position"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">사용자 그룹</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="groupEditor" class="list-group">
                    </ul>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">사용자 메뉴</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="menuEditor" class="list-group">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once('../footer.php');