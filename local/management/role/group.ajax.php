<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/role/lib.php');

$action = required_param('action', PARAM_ALPHAEXT);

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('group');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}


$rdata = new stdClass();

switch ($action) {
    case 'groups':
        $groups = $DB->get_records('management_group');
        
        $rdata->status = 'success';
        $rdata->groups = array_values($groups);
        break;
    case 'add':
        $data = (object) required_param_array('data', PARAM_RAW);
        unset($data->id);
        
        $data->id = local_management_role_add_group($data);
        $rdata->status = 'success';
        $rdata->group = $data;
        break;
    case 'update':
        $data = (object) required_param_array('data', PARAM_RAW);
        local_management_role_update_group($data);
        
        $rdata->status = 'success';
        $rdata->group = $data;
        break;
    case 'remove':
        $data = (object) required_param_array('data', PARAM_RAW);
        local_management_role_delete_group($data->id);
        
        $rdata->status = 'success';
        break;
    case 'members':
        $data = (object) required_param_array('data', PARAM_RAW);
        $members = local_management_role_get_group_members($data->id);
        
        $rdata->status = 'success';
        $rdata->members = array_values($members);
        break;
    case 'add_member':
        $data = (object) required_param_array('data', PARAM_RAW);
        unset($data->id);
        
        $meberid = local_management_role_add_group_member($data->groupid, $data->userid);
        $rdata->status = 'success';
        $rdata->meberid = $meberid;
        break;
    case 'remove_member':
        $data = (object) required_param_array('data', PARAM_RAW);
        local_management_role_delete_group_member($data->id);
        
        $rdata->status = 'success';
        break;
    case 'search_user':
        $data = (object) required_param_array('data', PARAM_RAW);
        $users = local_management_role_search_user($data->id, $data->search);
        
        $rdata->status = 'success';
        $rdata->users = array_values($users);
        break;
    case 'menus':
        $data = (object) required_param_array('data', PARAM_RAW);
        $memus = local_management_role_get_group_memus($data->id);
        
        $rdata->status = 'success';
        $rdata->menus = array_values($memus);
        break;
    case 'remove_menu':
        $data = (object) required_param_array('data', PARAM_RAW);
        local_management_role_delete_menu($data->groupid, $data->menuid);
        $rdata->status = 'success';
        break;
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

echo $OUTPUT->header();
echo json_encode($rdata);
echo $OUTPUT->footer();