<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/courses/lib.php');
require_once($CFG->dirroot . '/local/management/lib/paging.php');

$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'samplecontent':
        $data = required_param_array('data', PARAM_RAW);
        $list = searchSampleContentList($data["search"], $data['samplecontent'],$data['page']);
        $i = 0;
        foreach($list->list2 as $key=>$val){
            unset($list->list2[$key]);
            $new_key = $i;
            $list->list[$new_key] = $val;
            $i++;
        }

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->paging = pop_print_paging_navbar_script($list->total_count, $data['page'], 10, 'javascript:local_management_search_sample_content(:page);');
        echo json_encode($rvalue);
        break;
    case 'add_content':
        $data = required_param_array('data', PARAM_RAW);
        $content = searchSampleContent($data["contentid"]);
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $content;
        echo json_encode($rvalue);
        break;
    case 'teacher':
        $data = required_param_array('data', PARAM_RAW);
        $list = searchTeacherList($data["search"], $data['teacher']);
        $i = 0;
        foreach($list->list2 as $key=>$val){
            unset($list->list2[$key]);
            $new_key = $i;
            $list->list[$new_key] = $val;
            $i++;
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->paging = pop_print_paging_navbar_script($list->total_count, $data['page'], 10, 'javascript:local_management_search_teacher(:page);');
        echo json_encode($rvalue);
        break;
    case 'add_teacher':
        $data = required_param_array('data', PARAM_RAW);
        $user = searchTeacher($data["userid"]);
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $user;
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();