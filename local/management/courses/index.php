<?php
/**
 * 과정관리 리스트 페이지
 */

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/courses/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
require_once("$CFG->libdir/excellib.class.php");

$pagesettings = array(
    'title' => get_string('course:title','local_management'),
    'heading' => get_string('course:title','local_management'),
    'subheading' => '',
    'menu' => 'course',
    'js' => array($CFG->wwwroot . '/local/management/courses/course_list.js'),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

if(optional_param('resizeallbanner', 0, PARAM_INT) == 1) {
    require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
    global $DB;
    $_courses = $DB->get_records('lmsdata_course');
    foreach ($_courses as $_course) {
        $_context = context_course::instance($_course->courseid);
        lmsdata_banner_resize(540, $_context->id, 'local_courselist', 'thumbnailimg', $_course->courseid);
    }
}

$periodtype = optional_param('periodtype', 0, PARAM_INT);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$cata1 = optional_param('cata1', 0, PARAM_INT); //대분류
$cata2 = optional_param('cata2', 0, PARAM_INT); //중분류
$copyright = optional_param('copyright', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$isused = optional_param_array('isused',array(), PARAM_RAW);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$excel = optional_param('excel', 0, PARAM_INT);
$searchtext = trim($searchtext);
if($learningstart_str){
    $learningstart = str_replace(".", "-", $learningstart_str); 
    $learningstart = strtotime($learningstart);
}
if($learningend_str){
    $learningend = str_replace(".", "-", $learningend_str); 
    $learningend = strtotime($learningend) + 86399;
}

if($learningstart && $periodtype == 1){
    $sql_where[] = " lc.timecreated >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if($learningend && $periodtype == 1){
    $sql_where[] = " lc.timecreated <= :learningend ";
    $params['learningend'] = $learningend;
}

//검색용 파라미터
if ($cata2 > 0) {
    $sql_where[] = " ca.id = :category1 ";
    $params['category1'] = $cata2;
} else if ($cata1 > 0) {
    $sql_where[] = " (ca.id= :category2 or ca2.id = :category3)";
    $params['category2'] = $cata1;
    $params['category3'] = $cata1;
}

if($copyright){
    $sql_where[] = " lc.copyright = :copyright ";
    $params['copyright'] = $copyright;
}
if($isused){
    foreach($isused as $iskey => $isval){
        $sql_where_isused[] = " lc.isused = :isused".$iskey;
        $params['isused'.$iskey] = $isval;
    }
    if (!empty($sql_where_isused)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($searchtext)) {
    $sql_where_search[] = $DB->sql_like('lc.coursename', ':coursename');
    $params['coursename'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('lc.vi_coursename', ':vi_coursename');
    $params['vi_coursename'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('lc.en_coursename', ':en_coursename');
    $params['en_coursename'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('lc.coursecd', ':coursecode');
    $params['coursecode'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('u.firstname', ':firstname');
    $params['firstname'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('u.lastname', ':lastname');
    $params['lastname'] = '%' . $searchtext . '%';
    if (!empty($sql_where_search)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_search) . ')';
    } else {
        $sql_where[] = '';
    }
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}
$orderby = ' group by mc.id order by lc.timecreated DESC, lc.id DESC ';
$sql_select = 'SELECT 
                mc.id, mc.category, ca.name ca1name, ca2.name ca2name, mc.fullname, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber,
                lc.id lcid, lc.mainexposure, lc.exposuresort, lc.classobject, lc.coursename, lc.coursetype, lc.coursecd, lc.classtype, lc.price, lc.timecreated, lc.isused, lc.lecturecnt,  
                lc.vi_coursename, lc.timecreated, lc.teacher, (select title from {lmsdata_copyright} where id=copyright)  as kocopyright,(select titleen from {lmsdata_copyright} where id=copyright)  as encopyright,(select titlevn from {lmsdata_copyright} where id=copyright)  as vncopyright
                , u.firstname, u.lastname, lu.usergroup, ccl.en_name cc1en_name, ccl.vi_name cclvi_name, ccl2.en_name cc2en_name, ccl2.vi_name cc2vi_name, ca2.id ca2id, ca.id caid  ';
                
$sql_from = " FROM {course} mc 
            JOIN {lmsdata_course} lc ON lc.courseid = mc.id 
            JOIN {course_categories} ca ON ca.id = mc.category
            LEFT JOIN {course_categories_lang} ccl ON ca.id = ccl.categoryid 
            LEFT JOIN {lmsdata_course_professor} lcp ON lcp.courseid = lc.id 
            LEFT JOIN {lmsdata_teacher} lt ON lcp.teacherid = lt.userid 
            LEFT JOIN {user} u ON u.id = lt.userid 
            LEFT JOIN {lmsdata_user} lu ON lu.userid = u.id AND lu.usergroup = 'pr' 
            LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id 
            LEFT JOIN {course_categories_lang} ccl2 ON ca2.id = ccl2.categoryid ";
$count_courses = $DB->count_records_sql("SELECT count(DISTINCT mc.id) " . $sql_from . $sql_where, $params);
if(!$excel){
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $params, ($currpage - 1) * $perpage, $perpage);
$perpage_array = [10,20,30,50];
$isused_array = array(get_string('used', 'local_management'),get_string('notused', 'local_management'));
?>

<?php 

include_once($CFG->dirroot.'/local/management/header.php'); ?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
        <form name="course_search" id="course_search" class="search_area" action="index.php" method="get">
            <input type="hidden" name="page" value="1" />
            <input type="hidden" name="perpage" value="<?php echo $perpage?>" />
            <input type="hidden" value="0" name="excel" >
            <div>
                <label><?php echo get_string('category','local_management')?></label>
                 <select title="category01" name="cata1" id="course_search_cata1" onchange="cata_changed_c(this);"  class="w_160">
                    <option value="0"><?php echo get_string('bigcategory', 'local_management') ?></option>
                    <?php
                    $query = "select cc.*, ccl.en_name, ccl.vi_name from {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid  WHERE cc.depth=1 ORDER BY cc.sortorder  ASC, cc.id DESC, cc.idnumber DESC, cc.name DESC";
                    $catagories = $DB->get_records('course_categories', array('visible' => 1, 'depth' => 1), 'sortorder', 'id, idnumber, name');
                    $catagories = $DB->get_records_sql($query);
                    if ($cata1) {
                        foreach ($catagories as $catagory) {
                            switch (current_language()){
                                case 'ko' :
                                    $category_name = $catagory->name;
                                    break;
                                case 'en' :
                                    $category_name = $catagory->en_name;
                                    break;
                                case 'vi' :
                                    $category_name = $catagory->vi_name;
                                    break;
                            }
                                        
                            $selected = '';
                            if ($catagory->id == $cata1) {
                                $selected = ' selected';
                            }
                            echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $category_name . '</option>';
                        }

                    } else {
                        foreach ($catagories as $catagory) {
                            switch (current_language()){
                                case 'ko' :
                                    $category_name = $catagory->name;
                                    break;
                                case 'en' :
                                    $category_name = $catagory->en_name;
                                    break;
                                case 'vi' :
                                    $category_name = $catagory->vi_name;
                                    break;
                            }
                            echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"> ' . $category_name . '</option>';
                        }
                    }
                    ?>
                </select>
     
                <select title="category02" name="cata2" id="course_search_cata2" class="w_160" <?php if($course && $course->coursearea == $course->coursetype){?> style='display:none;' <?php }?> >
                    <option value="0"><?php echo get_string('middlecategory', 'local_management')?></option>
                    <?php
                    $tmpcategory = $DB->get_record('course_categories', array('id' => $cata1), '*');
                    if($tmpcategory){
                    $catagories = $DB->get_records('course_categories', array('depth' => 2, 'parent' => $cata1), 'sortorder', 'id, idnumber, name');

                    foreach ($catagories as $catagory) {
                        $selected = '';
                        if ($catagory->id == $cata2) {
                            $selected = ' selected';
                        }
                        echo '<option id="' . $course->ca1idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                    }
                    }
                    ?>
                </select>
            </div>
            <div>
                <label><?php echo get_string('copyright', 'local_management')?></label>
                <select name='copyright' id='copyright' class="w_160">
                    <option value="0"><?php echo get_string('copyright0', 'local_management')?></option>
                    <?php
                    $copyright_array = searchCopyrightList(0);
                    foreach($copyright_array as $ca){
                        $select = '';
                        if($ca->id == $copyright){
                            $select = 'selected';
                        }
                        echo '<option value='.$ca->id.' '.$select.' >'.$ca->title.'</option>';
                    }

                    ?>
                </select>
            </div>
            <div>
                <label style="display: none;"><?php echo get_string('timecreated','local_management')?></label>
                <label>
                    <select name="periodtype">
                        <option value="">- 조건 -</option>
                        <option value="1" <?php echo ($periodtype == 1 ? 'selected':'') ?> >강좌등록일</option>
                        <option value="2" <?php echo ($periodtype == 2 ? 'selected':'') ?> >결제/입금일</option>
                    </select>
                </label>
                <input type="text" title="<?php echo get_string('time','local_management')?>" name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str?>" placeholder="<?php echo get_string('click', 'local_management'); ?>  " >
                <span class="dash">~</span> 
                <input type="text" title="<?php echo get_string('time','local_management')?>" name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str?>" placeholder="<?php echo get_string('click', 'local_management'); ?>  " > 
            </div>
            <div>
                <label><?php echo get_string('isused','local_management')?></label>
                <?php foreach($isused_array as $iakey => $iaval){?>
                <label class="mg-bt10"><input type="checkbox" name="isused[]" value="<?php echo $iakey?>" <?php foreach($isused as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                <?php }?>
            </div>
            
            <div>
                <label><?php echo get_string('search','local_management')?></label>
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('input_course', 'local_management'); ?>"  class="search-text w100"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_management'); ?>"/>     
                <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>"/>   
            </div>
        </form><!--Search Area2 End-->
        <span><?php echo get_string('total','local_management',$count_courses)?></span>
        <select name="perpage2" class="perpage2">
            <?php 
            foreach($perpage_array as $pa){
                $selected = '';
                if($pa == $perpage){
                    $selected = 'selected';
                }
            ?>
            <option value="<?php echo $pa?>" <?php echo $selected?>><?php echo $pa?><?php echo get_string('viewmore','local_management')?>
            <?php
            }
            ?>
        </select>
        <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="excel_download();"/> 
        <div style="float:right;">            
            <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('courseregist', 'local_management') ?>" onclick="javascript:location.href = 'form.php';"/> 
        </div>
        <table>
            <thead>
                <tr>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('number', 'local_management') ?></th>
                    <th rowspan="2" scope="row" width="38%"><?php echo get_string('processname', 'local_management')?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('processcode', 'local_management')?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('category', 'local_management')?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('lecturecnt', 'local_management')?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('professor', 'local_management') ?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('price', 'local_management') .'('. get_string('regularprice', 'local_management').')'; ?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('gross_sales', 'local_management') ?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('copyright', 'local_management')?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('totalenrollment', 'local_management')?></th>
                    <th colspan="2" scope="row" width="6%"><?php echo get_string('completion', 'local_management')?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('timecreated', 'local_management') ?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('isused', 'local_management') ?></th>
                    <th rowspan="2" scope="row" width="6%"><?php echo get_string('classroomentry', 'local_management')?></th>
                </tr>
                <tr>
                    <th scope="row"><?php echo get_string('user:normal', 'local_management'); ?></th>
                    <th scope="row"><?php echo get_string('user:bop', 'local_management'); ?></th>
                </tr>
                <?php if ($count_courses) :
                    $total_sales = 0;
                    $total_lectures = 0;
                    $total_enrollments = 0;
                    $total_normal_completion = 0;
                    $total_bop_completion = 0;
                    foreach ($courses as $course) {
                        $getstudentsql = "
                        FROM m_lmsdata_payment lp
                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                        JOIN m_course  c ON c.id = ctx.instanceid 
                        JOIN m_user u ON lp.userid = u.id
                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                        WHERE  lc.parentcourseid = :courseid";
                         
                        $getcompletesql = "
                        FROM m_lmsdata_payment lp
                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                        JOIN m_course  c ON c.id = ctx.instanceid 
                        JOIN m_user u ON lp.userid = u.id
                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                        JOIN {course_completions} cc ON cc.course = lc.courseid AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                        WHERE  lc.parentcourseid = :courseid AND lu.usergroup = :usergroup";
                        $getstudent = $DB->count_records_sql("SELECT COUNT(*) " .$getstudentsql, array('courseid' => $course->lcid));
                        
                        $getcomplete_rs = $DB->count_records_sql("SELECT COUNT(*) ".$getcompletesql, array('courseid' => $course->lcid, 'usergroup' => 'rs'));
                        $getcomplete_bp = $DB->count_records_sql("SELECT COUNT(*) ".$getcompletesql, array('courseid' => $course->lcid, 'usergroup' => 'bp'));

                        $period_total_sale_sql = '';
                        if ($learningstart && $periodtype == 2) {
                            $period_total_sale_sql = "lp.timecreated >= {$learningstart} AND ";
                        }
                        if ($learningend && $periodtype == 2) {
                            $period_total_sale_sql = "lp.timecreated <= {$learningend} AND ";
                        }
                        $total_sale_sql = "SELECT sum(lc.price) as total
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE {$period_total_sale_sql} lc.courseid = '{$course->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
                        
                        $total_sales += $DB->get_record_sql($total_sale_sql)->total;

                        $total_lectures += $course->lecturecnt;
                        $total_enrollments += $getstudent;
                        $total_normal_completion += $getcomplete_rs;
                        $total_bop_completion += $getcomplete_bp;
                    }
                ?>
                    <tr>
                        <th><?php echo get_string('total_sum', 'local_management'); ?></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><?php echo $total_lectures; ?></th>
                        <th></th>
                        <th></th>
                        <th><?php echo number_format($total_sales); ?></th>
                        <th></th>
                        <th><?php echo $total_enrollments; ?></th>
                        <th><?php echo $total_normal_completion; ?></th>
                        <th><?php echo $total_bop_completion; ?></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                <?php endif; ?>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="15"><?php echo get_string('empty_course', 'local_management'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    ?>
                    <tr data-id="<?php echo $course->id?>">
                        <td><?php echo $startnum--; ?></td>
                        <td class="left"><a href="<?php echo './form.php?id=' . $course->id; ?>"><?php echo $course->vi_coursename; ?><br><?php echo $course->coursename; ?></a></td>
                        <td><?php echo empty($course->coursecd) ? '-' : $course->coursecd; ?></td>
                        <td>
                            <?php
                            switch (current_language()){
                                case 'ko' :
                                    if($course->ca1name){
                                        if($course->ca2name) { $category_name = $course->ca2name. ' > ' .$course->ca1name; }else{ $category_name = $course->ca1name;}
                                    }else{
                                        $category_name =  $course->ca2name;
                                    }
                                    break;
                                case 'en' :
                                    if($course->ca1name){
                                        if($course->cc2en_name) { $category_name = $course->cc2en_name. ' > ' .$course->cc1en_name; }else{ $category_name = $course->cc1en_name;}
                                    }else{
                                        $category_name =  $course->cc2en_name;
                                    }
                                    break;
                                case 'vi' :
                                    if($course->ca1name){
                                        if($course->cc2vi_name) { $category_name = $course->cc2vi_name. ' > ' .$course->cc1vi_name; }else{ $category_name = $course->cc1vi_name;}
                                    }else{
                                        $category_name =  $course->cc2vi_name;
                                    }
                                    break;
                            }
                            echo $category_name;
                            ?>
                        </td>
                        <td><?php echo $course->lecturecnt?></td>
                        <td>
                            <?php
                            $teacher_array_query = "select lu.userid as id,u.firstname, u.lastname, lu.usergroup 
                                    from {lmsdata_course_professor} lcp JOIN {lmsdata_user} lu ON lcp.teacherid=lu.userid AND lu.usergroup = 'pr'  
                                    JOIN {user} u ON u.id=lu.userid 
                                    JOIN {lmsdata_teacher} lt ON lu.userid = lt.userid  
                                    WHERE lcp.courseid = :courseid";
                            $teacher_array = $DB->get_records_sql($teacher_array_query,array('courseid'=>$course->lcid));
                            $teacher = array();
                            foreach($teacher_array as $ta){
                                $teacher[]= $ta->firstname;
                            }
                            $teacher_text = implode(',', $teacher);
                            echo $teacher_text;
                            ?>
                            
                        </td>
                        <td><?php echo number_format($course->price)?></td>
                        <td>
                            <?php 
                                $period_total_sale_sql = '';
                                if ($learningstart && $periodtype == 2) {
                                    $period_total_sale_sql = "lp.timecreated >= {$learningstart} AND ";
                                }
                                if ($learningend && $periodtype == 2) {
                                    $period_total_sale_sql = "lp.timecreated <= {$learningend} AND ";
                                }
                                $total_sale_sql = "SELECT sum(lc.price) as total
                                    FROM {lmsdata_payment} lp
                                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                                    WHERE {$period_total_sale_sql} lc.courseid = '{$course->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
                            
                                echo number_format($DB->get_record_sql($total_sale_sql)->total);
                            ?>
                        </td>
                        <?php
                        switch (current_language()){
                            case 'ko' :
                                $copyright_name = $course->kocopyright;
                                break;
                            case 'en' :
                                $copyright_name = $course->encopyright;
                                break;
                            case 'vi' :
                                $copyright_name = $course->vicopyright;
                                break;
                        }
                        ?>
                        <td><?php echo $copyright_name?></td>
                        <?php
                        $getstudentsql = "
                        FROM m_lmsdata_payment lp
                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                        JOIN m_course  c ON c.id = ctx.instanceid 
                        JOIN m_user u ON lp.userid = u.id
                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                        WHERE  lc.parentcourseid = :courseid";
                         
                        $getcompletesql = "
                        FROM m_lmsdata_payment lp
                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                        JOIN m_course  c ON c.id = ctx.instanceid 
                        JOIN m_user u ON lp.userid = u.id
                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                        JOIN {course_completions} cc ON cc.course = lc.courseid AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                        WHERE  lc.parentcourseid = :courseid AND lu.usergroup = :usergroup";
                        $getstudent = $DB->count_records_sql("SELECT COUNT(*) " .$getstudentsql, array('courseid' => $course->lcid));
                        
                        $getcomplete_rs = $DB->count_records_sql("SELECT COUNT(*) ".$getcompletesql, array('courseid' => $course->lcid, 'usergroup' => 'rs'));
                        $getcomplete_bp = $DB->count_records_sql("SELECT COUNT(*) ".$getcompletesql, array('courseid' => $course->lcid, 'usergroup' => 'bp'));
                            
                        ?>
                        <td><?php echo number_format($getstudent)?></td>
                        <td><?php echo number_format($getcomplete_rs)?></td>
                        <td><?php echo number_format($getcomplete_bp)?></td>
                        <td><?php echo date('Y.m.d',$course->timecreated)?></td>
                        <td><?php  echo ($course->isused == 0) ? get_string('used', 'local_management') : get_string('notused', 'local_management');?></td>

                        <td>
                            <input type="button" class="normal_btn" value="<?php echo get_string('classroomentry', 'local_management')?>" onclick="window.open('<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->id; ?>')"/>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->

        <div id="btn_area">
            <div style="float:right;">            
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('courseregist', 'local_management') ?>" onclick="javascript:location.href = 'form.php';"/> 
            </div>
        </div>
        <?php
        if (($count_courses / $perpage) > 1) {
            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        }

        $query_string = '';
        if (!empty($excel_params)) {
            $query_array = array();
            foreach ($excel_params as $key => $value) {
                $query_array[] = urlencode($key) . '=' . urlencode($value);
            }
            $query_string = '?' . implode('&', $query_array);
        }
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->
</div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>
<?php }else{
    $courses_excel = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $params);
//    $chkdatas = get_contents($userid, $category2, $search, $page_num, $perpage);
//    $datas = get_contents($userid, $category2, $search, $page_num, $chkdatas->total_count);
    //$datas = $courses_excel;

    $fields = array(
        get_string('number', 'local_management'),
        get_string('processname', 'local_management'),
        get_string('processcode', 'local_management'),
        get_string('category', 'local_management'),
        get_string('lecturecnt', 'local_management'),
        get_string('professor', 'local_management'),
        get_string('price', 'local_management') .'('. get_string('regularprice', 'local_management').')',
        get_string('gross_sales', 'local_management'),
        get_string('copyright', 'local_management'),
        get_string('totalenrollment', 'local_management'),
        get_string('completion', 'local_management') . ' - ' . get_string('user:normal', 'local_management'),
        get_string('completion', 'local_management') . ' - ' . get_string('user:bop', 'local_management'),
        get_string('timecreated', 'local_management'),
        get_string('isused', 'local_management')
    );

    $date = date('Y-m-d', time());
    $filename = '강좌리스트_' . $date. '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    foreach ($courses_excel as $file) {
        $teacher_array_query = "select lu.userid as id,u.firstname, u.lastname from {lmsdata_course_professor} lcp JOIN {lmsdata_user} lu ON lcp.teacherid=lu.userid JOIN {user} u ON u.id=lu.userid JOIN {lmsdata_teacher} lt ON lu.userid = lt.userid  WHERE lcp.courseid = :courseid";
        $teacher_array = $DB->get_records_sql($teacher_array_query,array('courseid'=>$file->lcid));
        $teacher = array();
        foreach($teacher_array as $tak => $ta){
            $teacher[]= $ta->firstname;
        }
        
        $getstudentsql = "
        FROM m_lmsdata_payment lp
        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
        JOIN m_course  c ON c.id = ctx.instanceid 
        JOIN m_user u ON lp.userid = u.id
        JOIN m_lmsdata_user lu ON u.id = lu.userid 
        WHERE  lc.parentcourseid = :courseid";

        $getcompletesql = "
        FROM m_lmsdata_payment lp
        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
        JOIN m_course  c ON c.id = ctx.instanceid 
        JOIN m_user u ON lp.userid = u.id
        JOIN m_lmsdata_user lu ON u.id = lu.userid 
        JOIN {course_completions} cc ON cc.course = lc.courseid AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
        WHERE  lc.parentcourseid = :courseid AND lu.usergroup = :usergroup";
        $getstudent = $DB->count_records_sql("SELECT COUNT(*) " .$getstudentsql, array('courseid' => $file->lcid));
        
        $getcomplete_rs = $DB->count_records_sql("SELECT COUNT(*) ".$getcompletesql, array('courseid' => $file->lcid, 'usergroup' => 'rs'));
        $getcomplete_bp = $DB->count_records_sql("SELECT COUNT(*) ".$getcompletesql, array('courseid' => $file->lcid, 'usergroup' => 'bp'));
        
        switch (current_language()){
            case 'ko' :
                if($file->ca1name){
                    if($file->ca2name) { $category_name = $file->ca2name. ' > ' .$file->ca1name; }else{ $category_name = $file->ca1name;}
                }else{
                    $category_name =  $file->ca2name;
                }
                break;
            case 'en' :
                if($file->ca1name){
                    if($file->cc2en_name) { $category_name = $file->cc2en_name. ' > ' .$file->cc1en_name; }else{ $category_name = $file->cc1en_name;}
                }else{
                    $category_name =  $file->cc2en_name;
                }
                break;
            case 'vi' :
                if($file->ca1name){
                    if($file->cc2vi_name) { $category_name = $file->cc2vi_name. ' > ' .$file->cc1vi_name; }else{ $category_name = $file->cc1vi_name;}
                }else{
                    $category_name =  $file->cc2vi_name;
                }
                break;
        }
        
        switch (current_language()){
            case 'ko' :
                $copyright_name = $file->kocopyright;
                break;
            case 'en' :
                $copyright_name = $file->encopyright;
                break;
            case 'vi' :
                $copyright_name = $file->vicopyright;
                break;
        }
        
        $teacher_text = implode(',', $teacher);
        $col = 0;

        $period_total_sale_sql = '';
        if ($learningstart && $periodtype == 2) {
            $period_total_sale_sql = "lp.timecreated >= {$learningstart} AND ";
        }
        if ($learningend && $periodtype == 2) {
            $period_total_sale_sql = "lp.timecreated <= {$learningend} AND ";
        }
        $total_sale_sql = "SELECT count(lc.price)
            FROM {lmsdata_payment} lp
            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
            WHERE {$period_total_sale_sql} lc.courseid = '{$course->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
    
        $total_sale = $DB->count_records_sql($total_sale_sql);

        $worksheet[0]->write($row, $col++, $count_courses--);
        $worksheet[0]->write($row, $col++, $file->vi_coursename.PHP_EOL.$file->coursename);
        $worksheet[0]->write($row, $col++, $file->coursecd);
        $worksheet[0]->write($row, $col++, $category_name);
        $worksheet[0]->write($row, $col++, $file->lecturecnt);
        $worksheet[0]->write($row, $col++, $teacher_text);
        $worksheet[0]->write($row, $col++, number_format($file->price));
        $worksheet[0]->write($row, $col++, number_format($total_sale));
        $worksheet[0]->write($row, $col++, $copyright_name);
        $worksheet[0]->write($row, $col++, number_format($getstudent));
        $worksheet[0]->write($row, $col++, number_format($getcomplete_rs));
        $worksheet[0]->write($row, $col++, number_format($getcomplete_bp));
        $worksheet[0]->write($row, $col++, date('Y.m.d',$file->timecreated));
        $worksheet[0]->write($row, $col++, $file->isused == 0  ? get_string('used', 'local_management') : get_string('notused', 'local_management') );
        

        $row++;
    }

    $workbook->close();
    die;

}?>
<script>
$(document).ready(function () {
    $(".perpage2").change(function () {
        var perpage = $('select[name=perpage2]').val();
        $('input[name=perpage]').val(perpage);
        $("form[id=course_search]").submit();
    });
    $('#reset').on('click',function(){
        $('#course_search_cata1').val('0');
        $('#course_search_cata2').val('0');
        $('#copyright').val('0');
        $('.s_date').val('');
        $('.e_date').val('');
        $("input:checkbox[name='isused[]']").attr("checked", false);
        $('input[name=searchtext]').val('');
        $("form[id=course_search]").submit();
    });
});
    start_datepicker_c('', '');
    end_datepicker_c('', '');
    
    function excel_download() {
        $("input[name='excel']").val(1);
        $('#course_search').submit();
        $("input[name='excel']").val(0);
    }
</script>    