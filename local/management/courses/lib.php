<?php

function searchSampleContentList($search = "", $samplecontent = '', $page = 1){
    global $DB; 
    
    $like = '';
    if (!empty($search)) {
        $like .= " and " . $DB->sql_like('con.con_name', ':search');
    }
    
    if($samplecontent){
        $like .= ' and rep.id NOT IN ( '.$samplecontent.' ) ';
    }
    
    $sql = "select "
                . "rep.id , rep.referencecnt, "
                . "con.id as conid , con.share_yn, con.con_type, con.con_name, con.reg_dt, con.update_dt, con.category, ca.parent "
                . "from {lcms_repository} as rep "
                . "join {lcms_contents} as con on con.id = rep.lcmsid "
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type and con.con_type!=:type2 and con.con_type!=:type3  $like  order by con.id desc";

    $count_sql = "select "
            . "count(rep.id) "
            . "from {lcms_repository} rep "
            . "join {lcms_contents} con on con.id = rep.lcmsid "
            . "join {lmsdata_category} as ca on ca.id = con.category "
            . " where con.con_type!=:type and con.con_type!=:type2 and con.con_type!=:type3  $like";
    
    $return = new stdClass();
    $return->total_count = $DB->count_records_sql($count_sql, array('search' => '%' . $search . '%', 'type' => 'ref', 'type2' => 'tplan' , 'type3' => 'visangurl'));
    //$return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }
    //$currpage = 1;
    //$return->list = $DB->get_records_sql($sql, array('search' => '%' . $search . '%', 'type' => 'ref', 'type2' => 'tplan'), $offset, $perpage);
    $return->list2 = $DB->get_records_sql($sql, array('search' => '%' . $search . '%', 'type' => 'ref', 'type2' => 'tplan' , 'type3' => 'visangurl'), ($page - 1) * 10, 10);
    
    return $return;
}

function searchSampleContent($id){
    global $DB; 
    
    $like = '';
    if (!empty($search)) {
        $like .= " and " . $DB->sql_like('con.con_name', ':search', false);
    }
    
    $sql = "select "
                . "rep.id , rep.referencecnt, "
                . "con.id as conid , con.share_yn, con.con_type, con.con_name, con.reg_dt, con.update_dt, con.category, ca.parent "
                . "from {lcms_repository} as rep "
                . "join {lcms_contents} as con on con.id = rep.lcmsid "
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type  and rep.id=:id order by con.id desc";


    $list = $DB->get_record_sql($sql, array('search' => '%' . $search . '%', 'type' => 'ref', 'id' => $id));


    return $list;
}

function searchTeacherList($search = "",$teacher='', $page = 1, $perpage = 10){
    global $DB; 
    
    $like = '';
    if (!empty($search)) {
        $like .= " and " . $DB->sql_like('con.con_name', ':search', false);
    }
    
    $sql = "SELECT u.id, lit.id as litid, lit.profile, lit.comment, u.username, u.firstname as fullname, lu.usergroup, lit.isused    
            FROM {user} u 
            JOIN {lmsdata_user} lu ON u.id = lu.userid
            JOIN {lmsdata_teacher} lit ON u.id = lit.userid
            WHERE lu.usergroup='pr' AND lit.isused = 1  ";
    
    if (!empty($search)) {
         $conditionsearch = array();
        
        // firstname
        $conditionsearch[] = $DB->sql_like("u.firstname", ":firstname", false);
        $params['firstname'] = "%$search%";
        // lastname
        $conditionsearch[] = $DB->sql_like("u.lastname", ":lastname", false);
        $params['lastname'] = "%$search%";
        // username
        $conditionsearch[] = $DB->sql_like("u.username", ":username", false);
        $params['username'] = "%$search%";
        
        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }
    
     $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = " AND " . implode(' OR ', $conditions);
    }
    if(!empty($teacher)){
        $sqlwhere .= ' AND lit.userid NOT IN ('.$teacher.')';
    }
    $sqlsort = "ORDER BY u.firstname DESC, u.email DESC, u.id DESC";

    $count_sql = "SELECT count(u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON u.id=lu.userid 
            JOIN {lmsdata_teacher} lit ON u.id = lit.userid
            WHERE lu.usergroup='pr'  AND lit.isused = 1  ";
    
    $return = new stdClass();
    $return->total_count = $DB->count_records_sql("$count_sql $sqlwhere $sqlsort", $params);
    //$return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }

    $return->list = $DB->get_records_sql("$sql $sqlwhere $sqlsort",$params);
    $return->num = $return->total_count - (($page - 1) * $perpage);


    return $return;
}

function searchTeacher($id){
    global $DB; 
    

    $sql = "SELECT u.id, lit.id as litid, lit.profile, lit.comment, u.username, GROUP_CONCAT(u.firstname ,' ', u.lastname) as fullname  
            FROM {user} u 
            JOIN {lmsdata_teacher} lit ON u.id = lit.userid
            WHERE u.id = :id";


    $list = $DB->get_record_sql($sql, array('id' => $id));


    return $list;
}

//출판사 or 저작권
function searchCopyrightList($type,$id){
    global $DB; 
    $q_select = 'SELECT lc.id,lc.title,lc.titleen,lc.titlevn, lc.timecreated,u.lastname AS username,lc.type,cs.id as csid  ';
    $q_from = ' FROM {lmsdata_copyright} lc JOIN {user} u on lc.userid = u.id LEFT JOIN (SELECT id,copyid FROM {lmsdata_copyright_set} cs WHERE cs.dataid = :id ) cs on cs.copyid = lc.id';
    $q_where = ' WHERE lc.type=:type'; 
    $q_order = ' ORDER BY id DESC';
    return $list = $DB->get_records_sql($q_select . $q_from . $q_where . $q_order, array('type' => $type,'id'=>$id));
}

/**
 * 무들에 사용자 등록
 * @global type $CFG, $DB, $USER
 * @param type $course
 * @param type $user
 * @return boolean
 */
function siteadmin_set_assign_user($course, $user){
    global $CFG, $PAGE, $DB;
    
    require_once("$CFG->dirroot/enrol/locallib.php");
    
    if(!is_object($course)) {
        $course = $DB->get_record('course', array('id'=>$course));
    }
    $manager = new course_enrolment_manager($PAGE, $course);
    if($ues = $manager->get_user_enrolments($user->id)) { 
        $manager->assign_role_to_user($user->roleid, $user->id);
    } else { 
        $enrol = $DB->get_record('enrol', array('enrol'=>'manual', 'courseid'=>$course->id));
        $timestart = 0;

        if($user->shortname == 'student'){
            $timestart = $course->startdate - 604800;
        } 

        $timeend = 0;

        $instances = $manager->get_enrolment_instances();
        $plugins = $manager->get_enrolment_plugins();
        
        $instance = $instances[$enrol->id];
        $plugin = $plugins[$instance->enrol];

        $plugin->enrol_user($instance, $user->id, $user->roleid, $timestart, $timeend);

    }
    
    return true;
}
