<?php

/**
 * 과정 submit 파일
 */

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
//require_once($CFG->dirroot . '/local/management/lib/lib.php');
require_once($CFG->dirroot . '/mod/jinotechboard/lib.php');
require_once($CFG->dirroot . '/mod/quiz/lib.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/courses/lib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');

$courseid = optional_param('id', 0, PARAM_INT);


$lmsdata = new stdClass();
$copy = new stdClass();
$copy->userid = $USER->id;
$copy->timecreated = time(); 
foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}

$lmsdata->roughprocess = $cata1;
$lmsdata->coursearea = $cata1;

if($cata2 && $cata2 != 0){
    $lmsdata->coursetype = $cata2;
    $lmsdata->detailprocess = $cata2;
}else{
    $lmsdata->coursetype = $cata1;
    $lmsdata->detailprocess = $cata1;
}

if($lmsdata->samplecontent){
    $lmsdata->samplecontent = implode(",", $lmsdata->samplecontent);
}else{
    $lmsdata->samplecontent = '';
}
$lmsdata->price = str_replace(',','',$lmsdata->price);

$courseconfig = get_config('moodlecourse');

if (!$courseid) {
    //코드 가져오기
    $ordernum = $DB->get_record('lmsdata_course', array('year'=>date('y',time())),'max(ordernum) as max');
    $lmsdata->ordernum = $ordernum->max+1;
    $lmsdata->year = date('y',time());

    //코드 중복체크
    $lmsdata->coursecd = $lmsdata->year.'C'.sprintf("%04d",$lmsdata->ordernum);
    $overlap_cd1 = $DB->get_record('lmsdata_course', array('coursecd'=>$lmsdata->coursecd));
    $overlap_cd2 = $DB->get_record('lmsdata_class', array('code'=>$lmsdata->coursecd));
    $overlap_cd3 = $DB->get_record('lmsdata_textbook', array('code'=>$lmsdata->coursecd));
    if($overlap_cd1 || $overlap_cd2 || $overlap_cd3){
        echo "코드가 중복되었습니다.";
        exit;
    }

    //입력
    $data = new stdClass();
    $data->fullname = $lmsdata->coursename;
    $data->shortname = $lmsdata->coursename . time();
    $data->startdate = time();
    $data->summary = $intro;
    $data->summaryformat = 1;
    if($cata2 && $cata2 != 0){
        $data->category = $cata2;
    }else{
        $data->category = $cata1;
    }

    $data->format = 'lguplus';
    $data->newsitems = $courseconfig->newsitems;
    $data->showgrades = $courseconfig->showgrades;
    $data->showreports = $courseconfig->showreports;
    $data->maxbytes = $courseconfig->maxbytes;
    $data->groupmode = $courseconfig->groupmode;
    $data->groupmodeforce = $courseconfig->groupmodeforce;
    $data->visible = $courseconfig->visible;
    $data->lang = $courseconfig->lang;
    $data->enablecompletion = $courseconfig->enablecompletion;


    $course = create_course($data);
    $courseid = $course->id;
    
    if($lmsdata->lecturecnt){
        for($i=0; $i<$lmsdata->lecturecnt; $i++){
            course_create_section($courseid);
        }
    }

    //기본블럭설정
    edu_blocks_add_default_course_blocks($course);

    // 과정 qna 추가
    $newcourse = $DB->get_record('course', array('id' => $courseid));
    new_course_create_activity_jinotechboard($newcourse, 1);
//    new_course_create_activity_quiz($newcourse);
    new_course_create_activity_jinotechboard($newcourse, 3);
    new_course_create_activity_jinotechboard($newcourse, 2);
    new_course_create_activity_jinotechboard($newcourse, 4);

    $lmsdata->courseid = $course->id;
    $lmsdata->userid = $USER->id;
    $lmsdata->timecreated = time();
    $lmsdatacourse = $DB->insert_record('lmsdata_course', $lmsdata);
    
    if($lmsdatacourse){
        $copy->dataid = $lmsdatacourse;
        $copy->category = 'course';
        $copy->copyid = $lmsdata->copyright;
        $newcopy =  $DB->insert_record('lmsdata_copyright_set', $copy);
        
        $teacher = new stdClass();
        $teacher->courseid = $lmsdatacourse;
        $teacher->timecreated = $lmsdata->timecreated;
        foreach($lmsdata->teacher as $lt){
            $teacher->teacherid = $lt;
            $DB->insert_record('lmsdata_course_professor', $teacher);
            //역할부여
            $role = $DB->get_record('role', array('shortname' => 'editingteacher'), 'id, shortname');
            $manager = $DB->get_record('user', array('id' => $lt));
            if($manager){
                $manager->roleid = $role->id;
                siteadmin_set_assign_user($courseid, $manager);
            }
        }
    }
} else {
//    //코드 중복체크
//    $overlap_cd1 = $DB->get_record('lmsdata_course', array('coursecd'=>$lmsdata->coursecd));
//    $overlap_cd2 = $DB->get_record('lmsdata_class', array('code'=>$lmsdata->coursecd));
//    $overlap_cd3 = $DB->get_record('lmsdata_textbook', array('code'=>$lmsdata->coursecd));
//    if($overlap_cd1 || $overlap_cd2 || $overlap_cd3){
//        if($overlap_cd1->courseid != $courseid){
//            echo "코드가 중복되었습니다.";
//            exit;
//        }
//    }
//    
    //수정
    $data = $DB->get_record('course', array('id' => $courseid));
    $fullname = $data->fullname;
    $shortname = $data->shortname;
    $shname = substr($shortname, strlen($fullname));

    if ($data->fullname != $lmsdata->coursename) {
        $data->fullname = $lmsdata->coursename;
        $data->shortname = $lmsdata->coursename . $shname;
    }

    if (!empty($intro)) {
        $data->summary = $intro;
    }
    
    $data->category = empty($cata2) ? $cata1 : $cata2;

    $data_lms = $DB->get_record('lmsdata_course', array('courseid' => $courseid));
    $lmsdata->id = $data_lms->id;
    $data_lmsclass = $DB->get_records('lmsdata_class', array('parentcourseid' => $data_lms->id));

    if ($data_lmsclass) {
        foreach ($data_lmsclass as $clas) {
            $coursedata = $DB->get_record('course', array('id' => $clas->courseid));
            $coursedata->category = $data->category;
            $coursedata->fullname = $lmsdata->coursename;
            $coursedata->shortname = $lmsdata->coursename . time();

            if (!empty($intro)) {
                $coursedata->summary = $intro;
            }

            if (!empty($coursedata) && !empty($coursedata->id)) {
                $DB->update_record('course', $coursedata);
            }
        }
    }

    update_course($data);
    $lmsdata->timeupdated = time();
    $update_yn = $DB->update_record('lmsdata_course', $lmsdata);
    
    $teacher = new stdClass();
    $teacher->courseid = $data_lms->id;
    $teacher->timecreated = time();
    
    //기존
    $alist = $DB->get_records('lmsdata_course_professor',array('courseid'=>$data_lms->id));
    foreach($alist as $al){
        if (!in_array($al->teacherid, $lmsdata->teacher)) {
            $DB->delete_records('lmsdata_course_professor',array('courseid'=>$data_lms->id, 'teacherid'=>$al->teacherid));
            //$manager = $DB->get_record('user', array('id' => $al->teacherid));
            //enrol_user_delete($manager);
            
            
            
            
            
            $lmsdatacourse = $DB->get_record('lmsdata_course', array('courseid'=>$courseid));
            if($lmsdatacourse){
                $lmsdataclass = $DB->get_records('lmsdata_class', array('parentcourseid'=>$lmsdatacourse->id));
                if($lmsdataclass){
                    foreach($lmsdataclass as $lc){
                        //siteadmin_set_assign_user($lc->courseid, $manager);
                        
                        $testDB = $DB->get_record('enrol',array('courseid'=>$courseid, 'enrol'=>'manual'));
                        $ueid = $testDB->id;
                        $ue = $DB->get_record('user_enrolments', array('id' => $ueid));
                        $instance = $DB->get_record('enrol', array('id'=>$ue->enrolid));
                        
                        
                        if($instance){
                            $plugin = enrol_get_plugin($instance->enrol);
                            $plugin->unenrol_user($instance, $ue->userid);
                        }
                        
                    }
                }
            }
            
            
            
            
       }
    }

    foreach($lmsdata->teacher as $lt){
        $teacher->teacherid = $lt;
        $tlist = $DB->get_record('lmsdata_course_professor',array('courseid'=>$data_lms->id, 'teacherid'=>$lt));
        if(!$tlist){
            $DB->insert_record('lmsdata_course_professor', $teacher);
            //역할부여
            $role = $DB->get_record('role', array('shortname' => 'editingteacher'), 'id, shortname');
            $manager = $DB->get_record('user', array('id' => $lt));
            if($manager){
                $manager->roleid = $role->id;
                if(siteadmin_set_assign_user($courseid, $manager)){
                    $lmsdatacourse = $DB->get_record('lmsdata_course', array('courseid'=>$courseid));
                    if($lmsdatacourse){
                        $lmsdataclass = $DB->get_records('lmsdata_class', array('parentcourseid'=>$lmsdatacourse->id));
                        if($lmsdataclass){
                            foreach($lmsdataclass as $lc){
                                siteadmin_set_assign_user($lc->courseid, $manager);
                            }
                        }
                    }
                }
            }
        }
    }

    if ($lmsdata->copy_setid){
        $copy->id = $lmsdata->copy_setid;
        $copy->copyid = $lmsdata->copyright;
        $DB->update_record('lmsdata_copyright_set', $copy);
    }
    
    //로그
    $logdata = new stdClass();
    $logdata->productid = $courseid;
    $logdata->userid = $USER->id;
    $logdata->timecreated = time();
    $logdata->type = 0;

    $DB->insert_record('lmsdata_product_log', $logdata);
}
$context = context_course::instance($courseid);
edu_courselist_file_upload($file_del, $context->id, 'thumbnailimg', $id,$courseid);

require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
lmsdata_banner_resize(540, $context->id, 'local_courselist', 'thumbnailimg', $courseid);
    

redirect($CFG->wwwroot . '/local/management/courses/index.php');
