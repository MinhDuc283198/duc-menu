function call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}
//select box 초기화
function remove_file() {
    $("span[name='file_link']").remove();
    $("input[name='remove_button']").remove();
    $("input[name='file_del']").val(1);
}

//select box 변경시 이벤트
function cata_changed_c(sel) {
    var selCata2 = $('#course_search_cata2');
    cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = cata_get_child_cata_c($(sel).val());
    if (categories !== null) {
        $('#course_search_cata2').show();
        $('#cata2_end').val(0);
        cata_add_select_detail_options(selCata2, categories);
    }else{
        $('#course_search_cata2').hide();
        $('#cata2_end').val(1);
    }
}

//select box 변경시 이벤트
function cata_get_child_cata_c(pid) {
    var categories = null;
    $.ajax({
        url: '/local/management/courses/child_detailcata.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                categories = data.categories;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return categories;
}

function cata_add_select_detail_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.cname
        }));
    });
}
//select box 초기화
function cata_clean_select(sel) {
    $(sel[0].options).each(function () {
        if ($(this).val() != 0) {
            $(this).remove();
        }
        ;
    });
}
function cata_page(page) {
    $('[name=page]').val(page);
    $('#course_search').submit();
}

function li_del(txt,id){
    $('li#'+txt+id).remove();
}

$(document).ready(function () {
//    $('.close').on('click', function() {
//        var id = $(this).data('id');
//        console.log(id);
//        $(this).parents('li#'+id).remove();
//    });
    //맛보기 영상
    $('#btnSampleSearch').on('click', function() {
        local_management_search_sample_content(1);
    });
    $('#p-modal-add-sample').on('click', function() {
        local_management_search_sample_content(1);
    });
    //담당강사
     $('#btnTeacherSearch').on('click', function() {
        local_management_search_teacher(1);
    });
    $('#p-modal-add-teacher').on('click', function() {
        local_management_search_teacher(1);
    });

//    $('#free').on('click', function() {
//        if ($("input:checkbox[id='free']").is(":checked") == true){
//            $('input[name=price]').val(0);
//            $("input[name=price]").attr("readonly",true).attr("disabled",false);
//        }else{
//            $("input[name=price]").attr("readonly",false).attr("disabled",false);
//        }
//    });
});


//값이 없는지 체크
function local_management_value_null_check(select){
    if( $(select).val() == null || $(select).val() == 0 || $(select).val() == undefined || $(select).val() == ''){
        return false;
    }else{
        return true;
    }
}

function local_management_add_sample_content(content) {
    var contentid = content.id;

    var result = call_ajax('add_content', {contentid: contentid}, false);
    if(result.status == 'success') {
        //$li = local_management_menu_render_user(user);
        $li = "<li id='contents"+result.list.id+"'>"+result.list.con_name+"<a class='close' data-id='"+result.list.id+"' href='#' onclick='li_del(\"contents\","+result.list.id+")'>close</a><input type='hidden' name='samplecontent[]' value='"+result.list.id+"'></li>";
        var $ul = $('#content_result');
        $ul.html($li);
    } else {
        alert(result.error);
    }
}




function local_management_add_teacher(teacher) {
    var userid = teacher.id;
    var result = call_ajax('add_teacher', {userid: userid}, false);
    if(result.status == 'success') {
        //$li = local_management_menu_render_user(user);
        $li = "<li id='teacher"+result.list.id+"'>"+result.list.fullname+"("+result.list.username+")"+"<a class='close' data-id='"+result.list.id+"' href='#' onclick='li_del(\"teacher\","+result.list.id+")'>close</a><input type='hidden' name='teacher[]' value='"+result.list.id+"'></li>";
        var $ul = $('#teacher_result');
        $ul.append($li);
    } else {
        alert(result.error);
    }
}
