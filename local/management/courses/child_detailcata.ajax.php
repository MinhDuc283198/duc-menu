<?php
require_once(__DIR__ . '/../../../config.php');

$pid = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$query = 'select cc.*, ccl.en_name, ccl.vi_name '
        . 'from {course_categories} cc '
        . 'LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid '
        . 'WHERE cc.visible=1 AND cc.parent=:parent '
        . 'ORDER BY cc.sortorder ASC, cc.id DESC, cc.idnumber DESC, cc.name DESC';
$catagories = $DB->get_records_sql($query,array('parent'=>$pid));
if($catagories) {
    $i = 0;
    foreach($catagories as $key => $category){
        switch (current_language()){
            case 'ko' :
                $categorycname = $category->name;
                break;
            case 'en' :
                $categorycname = $category->en_name;
                break;
            case 'vi' :
                $categorycname = $category->vi_name;
                break;
        }
        $catagories[$key]->cname = $categorycname;
        
        unset($catagories[$key]);
        $new_key = $i;
        $catagories2[$new_key] = $category;
        $i++;
        
    }

    $returnvalue->status = 'success';
    $returnvalue->categories = $catagories2;
} else {
    $returnvalue->status = 'error';
    $returnvalue->message = get_string('empty_case','local_lmsdata');
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);