<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib.php';
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once $CFG->dirroot . '/lib/form/editor.php';
require_once($CFG->dirroot . '/local/management/courses/lib.php');

$pagesettings = array(
    'title' => get_string('course:title','local_management'),
    'heading' => get_string('course:title','local_management'),
    'subheading' => '',
    'menu' => 'course',
    'js' => array(
            $CFG->wwwroot .'/chamktu/js/ckeditor-4.3/ckeditor.js',
            $CFG->wwwroot .'/chamktu/js/ckfinder-2.4/ckfinder.js',
            $CFG->wwwroot . '/local/management/courses/course_list.js'
            ),
    'css' => array(),
    'nav_bar' => array()
);


require_login(0, false);
// Check for valid admin user - no guest autologin
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('form.php');
    redirect(get_login_url());
}
$context = context_system::instance();

//require_capability('moodle/site:config', $context);

$courseid = optional_param('id', 0, PARAM_INT);

if (!empty($courseid)) {
    $course_sql = " SELECT co.id,co.category, lc.id as lcid, co.fullname, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, ca.id caid, ca2.id ca2id, ca.name ca1name, ca2.name ca2name, lc.*                        
                    FROM {course} co
                    JOIN {lmsdata_course} lc ON co.id = lc.courseid 
                    JOIN {course_categories} ca on ca.id = co.category 
                    left JOIN {course_categories} ca2 ON ca.parent = ca2.id 
                    WHERE co.id = :courseid ";
    $params = array('courseid' => $courseid);
    $course = $DB->get_record_sql($course_sql, $params);

    $context = context_course::instance($course->courseid);
    $fileobj = edu_courselist_get_file($context->id, 'thumbnailimg',$course->courseid);

    $mainexposures = explode(',', $course->mainexposure);
    
    $categories = $DB->get_record('course_categories', array('id'=>$course->category));
    if($categories->parent != 0){
        $cata2 = $categories->id;
        $parentcategories = $DB->get_record('course_categories', array('id'=>$categories->parent));
        $cata1 = $parentcategories->id;
    }else{
        $cata1 = $categories->id;
    }
}

$reviewperiod_array = array(10,20,30);
$courseperiod_array = array(30,50,70,100);

$copyright_array = searchCopyrightList(0,$course->id);
?>

<?php include_once($CFG->dirroot.'/local/management/header.php'); ?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
        <form name="" id="course_search" action="submit.php?id=<?php echo $courseid; ?>" method="post" enctype="multipart/form-data">
            <?php
            if (!empty($course)) {
                echo '<input type="hidden" name="courseid" value="' . $courseid . '" />';
            }
            ?>
            <table cellpadding="0" cellspacing="0" class="detail">
                <input type="hidden" readonly="" name="trainingarea" value="0"/> 
                <input type="hidden" readonly="" name="classobject" value="0"/> 
                <input type="hidden" readonly="" id="cata2_end" name="cata2_end" value="<?php echo ($course->coursearea == $course->coursetype) ? '1':'0'?>"/> 
                <tbody>
                     <tr>
                         <td class="field_title" colspan="2"><?php echo get_string('category', 'local_management') ?> <span class="red">*</span></td> 
                        <td class="field_value" colspan="3">
                            <select title="category01" name="cata1" id="course_search_cata1" onchange="cata_changed_c(this);"  class="w_160">
                                <option value="0"><?php echo get_string('bigcategory', 'local_management') ?></option>
                                <?php
                                //$catagories = $DB->get_records('course_categories', array('visible' => 1, 'depth' => 1), 'sortorder', 'id, idnumber, name');
                                $query = 'select cc.*, ccl.en_name, ccl.vi_name from {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid WHERE cc.visible=1 AND cc.depth=1 ORDER BY cc.sortorder ASC, cc.id DESC, cc.idnumber DESC, cc.name DESC';
                                $catagories = $DB->get_records_sql($query);
        
                                if ($course) {
                                    foreach ($catagories as $catagory) {
                                        switch (current_language()){
                                            case 'ko' :
                                                $category_name = $catagory->name;
                                                break;
                                            case 'en' :
                                                $category_name = $catagory->en_name;
                                                break;
                                            case 'vi' :
                                                $category_name = $catagory->vi_name;
                                                break;
                                        }
                                
                                        $selected = '';
                                        if ($catagory->id == $cata1) {
                                            $selected = ' selected';
                                        }
                                        echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $category_name. '</option>';
                                    }
                                    
                                } else {
                                    foreach ($catagories as $catagory) {
                                        switch (current_language()){
                                            case 'ko' :
                                                $category_name = $catagory->name;
                                                break;
                                            case 'en' :
                                                $category_name = $catagory->en_name;
                                                break;
                                            case 'vi' :
                                                $category_name = $catagory->vi_name;
                                                break;
                                        }
                                        
                                        echo '<option id="' . $catagory->idnumber . '" value="' . $catagory->id . '"> ' . $category_name. '</option>';
                                    }
                                }
                                ?>
                            </select>
     
                            <select title="category02" name="cata2" id="course_search_cata2" class="w_160" <?php if(!$cata2){?> style='display:none;' <?php }?> >
                                <option value="0"><?php echo get_string('middlecategory', 'local_management')?></option>
                                <?php
                                //$catagories = $DB->get_records('course_categories', array('visible' => 1, 'depth' => 2, 'parent' => $cata1), 'sortorder', 'id, idnumber, name');
                                $query = 'select cc.*, ccl.en_name, ccl.vi_name from {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid WHERE cc.visible=1 AND cc.depth=2 AND cc.parent=:parent ORDER BY cc.sortorder ASC, cc.id DESC, cc.idnumber DESC, cc.name DESC';
                                $catagories = $DB->get_records_sql($query, array('parent'=>$cata1));

                                foreach ($catagories as $catagory) {
                                    
                                     switch (current_language()){
                                            case 'ko' :
                                                $category_name = $catagory->name;
                                                break;
                                            case 'en' :
                                                $category_name = $catagory->en_name;
                                                break;
                                            case 'vi' :
                                                $category_name = $catagory->vi_name;
                                                break;
                                        }
                                        
                                    $selected = '';
                                    if ($catagory->id == $cata2) {
                                        $selected = ' selected';
                                    }
                                    echo '<option id="' . $course->ca1idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $category_name . '</option>';
                                }
                                ?>
                            </select>

<!--                            <select title="category03" name="cata3" id="course_search_cata3" class="w_160">
                                <option value="0"><?php echo get_string('small', 'local_management') . get_string('class', 'local_management')?></option>
                            <?php
                            $catagories = $DB->get_records('course_categories', array('visible' => 1, 'parent' => $mid_category->id), 'sortorder', 'id, idnumber, name');
                            if($catagories || $courseid == 0){
                                foreach ($catagories as $catagory) {
                                    $selected = '';
                                    if ($catagory->id == $course->coursetype) {
                                        $selected = ' selected';
                                    }
                                    echo '<option id="' . $course->ca1idnumber . '" value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                                }
                                }
                                ?>
                            </select>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" rowspan="3"><?php echo get_string('coursename', 'local_management') ?> <span class="red">*</span></td>
                        <td class="field_title"><?php echo get_string('vi', 'local_management') ?></td>
                        <td class="field_value"  colspan="3">
                            <input type="text" title="<?php echo get_string('processname', 'local_management') ?>" name="vi_coursename" placeholder="" class="w100" value="<?php echo $course->vi_coursename; ?>"/>
                        </td>
                        
                    </tr>
                    <tr>
                          <td class="field_title"><?php echo get_string('ko', 'local_management') ?></td>
                          <td class="field_value"  colspan="3">
                          <input type="text" title="<?php echo get_string('processname', 'local_management') ?>" name="coursename" placeholder="" class="w100" value="<?php echo $course->fullname; ?>"/>
                          </td>
                    </tr>
                    <tr>
                          <td class="field_title"><?php echo get_string('en', 'local_management') ?></td>
                          <td class="field_value"  colspan="3">
                            <input type="text" title="<?php echo get_string('processname', 'local_management') ?>" name="en_coursename" placeholder="" class="w100" value="<?php echo $course->en_coursename; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('processcode', 'local_management') ?> <span class="red">*</span></td>
                        <td class="field_value">
                            <span><?php echo ($course->coursecd) ? $course->coursecd : '' ?>※ <?php echo get_string('code_text', 'local_management') ?></span>
                        </td>
                        <td class="field_title"><?php echo get_string('price', 'local_management') .'('. get_string('regularprice', 'local_management').')'; ?> <span class="red">*</span></td>
                        <td class="field_value">
                            <div class='input-txt'>
                                 <input type="text"  style = "text-align:right;" class='pricenumber' title="<?php echo get_string('price', 'local_management') .'('. get_string('regularprice', 'local_management').')'; ?>" name="price" placeholder="" size="10"  value="<?php echo $course->price; ?>" onkeyup="inputNumberFormat(this)" <?php if($course->price == 0){?>readonly="readonly"<?php }?>/> VND
                            </div>
<!--                            <input type="checkbox" id='free' name='free' <?php echo ($course->price == 0) ? 'checked' : ''?>/> <label for ='free'><?php echo get_string('free', 'local_management')?></label>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('lecturenum', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_value">
                            <input type="text"  style = "text-align:center;" title="<?php echo get_string('lecturenum', 'local_management')?>" name="lecturecnt" placeholder="" size="10" value="<?php echo $course->lecturecnt; ?>"   onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/>
                            <span><?php echo get_string('lecture', 'local_management') ?></span>
                        </td>
                        <td class="field_title"><?php echo get_string('copyright', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_value">
                            <select name='copyright' id='copyright' class="w_160">
                                <?php
                                foreach($copyright_array as $ca){
                                    $select = '';
                                    if($ca->id == $course->copyright){
                                        $select = 'selected';
                                    }
                                    switch (current_language()){
                                        case 'ko' :
                                            $capyright_lang = $ca->title;
                                            break;
                                        case 'en' :
                                            $capyright_lang = $ca->titleen;
                                            break;
                                        case 'vi' :
                                            $capyright_lang = $ca->titlevn;
                                            break;
                                    }
                        
                                    echo '<option value='.$ca->id.' '.$select.' >'.$capyright_lang.'</option>';
                                    if($ca->csid){  $set =  '<input type="hidden" readonly=""  name="copy_setid" value="'.$ca->csid.'"/> '; }
                                }
                                echo $set;
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('Instructor', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_value" colspan="3">
                            <input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-teacher" id="p-modal-add-teacher">
                            <ul id="teacher_result" class="list-group close-box">
                                <?php
                                $teacher_array_query = "select lu.userid as id,u.firstname, u.username, lu.usergroup from {lmsdata_course_professor} lcp JOIN {lmsdata_user} lu ON lcp.teacherid=lu.userid AND lu.usergroup = 'pr'  JOIN {user} u ON u.id=lu.userid WHERE lcp.courseid = :courseid";
                                $teacher_array = $DB->get_records_sql($teacher_array_query,array('courseid'=>$course->lcid));
                                foreach($teacher_array as $tak => $ta){
                                    $teacher = searchTeacher($ta->id);
                                ?>
                                <li id='teacher<?php echo $ta->id?>'><?php echo $teacher->fullname?>(<?php echo $teacher->username?>) <a class='close' data-id='<?php echo $ta->id?>' href='#' onclick="li_del('teacher',<?php echo $ta->id?>)">close</a><input type='hidden' name='teacher[]' value='<?php echo $ta->id?>'></li>
                                <?php } ?>
                            </ul>
                        </td>    
                    </tr>
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('samplevideo', 'local_management') ?></td> 
                        <td class="field_value" colspan="3">
                            <input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-sample" id="p-modal-add-sample">
                            <ul id="content_result" class="list-group close-box">
                                <?php
                                if($course->samplecontent != ""){
                                $content_array =explode(',' , $course->samplecontent);
                                foreach($content_array as $ca){
                                    $samplecontent = searchSampleContent($ca);
                                ?>
                                <li id='contents<?php echo $ca?>'><?php echo $samplecontent->con_name?><a class='close' data-id='<?php echo $ca?>' href='#' onclick="li_del('contents',<?php echo $ca?>)">close</a><input type='hidden' name='samplecontent[]' value='<?php echo $ca?>'></li>
                                <?php }} ?>
                            </ul>
                        </td>    
                    </tr>
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('processimg', 'local_management')?> <span class="red">*</span><br>(gif, jpg, jpeg, png)</td>
                        <td class="field_value" colspan="3">
                            <div class="input-file"> 
                                <input type="text" readonly="readonly" class="file-name form-inline" /> 
                                <label for="upload01" class="file-label"><?php echo get_string('file_upload', 'local_management')?></label> 
                                <input type="file" id="upload01" title="<?php echo get_string('thumbnail', 'local_management');?>" name="thumbnailimg" placeholder="" size="40" value=""/>
                            </div>(540*305px)
                            <?php echo $fileobj; ?>
                            <input type="hidden" name="file_del" value="0"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" rowspan="3"><?php echo get_string('courseonelineintroduce', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_title" ><?php echo get_string('vi', 'local_management') ?></td>
                        <td class="field_value" colspan="3">
                            <input type='text' name='vi_onelineintro' class="w100" size='150' value='<?php echo $course->vi_onelineintro?>' />
                        </td>
                    </tr>
                     <tr>
                        <td class="field_title" ><?php echo get_string('ko', 'local_management') ?></td>
                        <td class="field_value" colspan="3">
                            <input type='text' name='onelineintro' class="w100" size='150' value='<?php echo $course->onelineintro?>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" ><?php echo get_string('en', 'local_management') ?></td>
                        <td class="field_value" colspan="3">
                            <input type='text' name='en_onelineintro' class="100" size='150' value='<?php echo $course->en_onelineintro?>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" rowspan="3"><?php echo get_string('courseintro', 'local_management')?> <span class="red">*</span></td>
                         <td class="field_title" ><?php echo get_string('vi', 'local_management') ?></td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="vi_intro" title="<?php echo get_string('courseintro', 'local_management')?>" class="ckeditor" id="vi_intro" rows="5" ><?php echo!empty($course->vi_intro) ? $course->vi_intro : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" ><?php echo get_string('ko', 'local_management') ?></td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="intro" title="<?php echo get_string('courseintro', 'local_management')?>" class="ckeditor" id="intro" rows="5" ><?php echo!empty($course->intro) ? $course->intro : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title" ><?php echo get_string('en', 'local_management') ?></td>
                        <td class="field_value" colspan="3"> 
                            <textarea name="en_intro" title="<?php echo get_string('courseintro', 'local_management')?>" class="ckeditor" id="en_intro" rows="5" ><?php echo!empty($course->en_intro) ? $course->en_intro : ''; ?></textarea>
                        </td>
                    </tr>
<!--                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('courseperiod', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_value" > 
                            <select name="courseperiod" id='courseperiod'>
                                <?php
                                    foreach($courseperiod_array as $ca){
                                        $selected = "";
                                        if($ca == $course->courseperiod){
                                            $selected = "selected";
                                        }
                                        echo "<option value=".$ca." $selected >".$ca.get_string('day', 'local_management')."</option>";
                                    }
                                ?>
                            </select>
                        </td>
                        <td class="field_title"><?php echo get_string('reviewperiod', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_value" > 
                            <select name="reviewperiod" id='reviewperiod'>
                                <?php
                                    foreach($reviewperiod_array as $ra){
                                        $selected = "";
                                        if($ra == $course->reviewperiod){
                                            $selected = "selected";
                                        }
                                        echo "<option value=".$ra." $selected >".$ra.get_string('day', 'local_management')."</option>";
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
-->
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('isused', 'local_management')?> <span class="red">*</span></td>
                        <td class="field_value" colspan="3"> 
                            <input type="radio" name="isused" value="0" <?php echo ($course->isused == 0) ? "checked":"" ?> ><?php echo get_string('used', 'local_management')?>
                            <input type="radio" name="isused" value="1"<?php echo ($course->isused == 1) ? "checked":"" ?>><?php echo get_string('notused', 'local_management')?>
                        </td>
                    </tr>
                    </tr>
                    <?php
                    if($courseid){
                        $modi_query = "select * from {lmsdata_product_log} where type=0 and productid=:id order by timecreated desc limit 1";
                        $modi_sql = $DB->get_record_sql($modi_query, array("id"=>$courseid));
                    ?>
                    <tr>
                        <td class="field_title" colspan="2"><?php echo get_string('insert', 'local_management')?>/<?php echo get_string('modify', 'local_management') .get_string('day', 'local_management') ?></td>
                        <td class="field_value" colspan="3"> 
                            <?php echo date('Y.m.d',$course->timecreated) ?>  (<?php echo local_management_return_user_info($course->userid)?>)
                            <?php
                            if($modi_sql){
                                echo get_string('modify', 'local_management').' :' . date('Y.m.d',$modi_sql->timecreated) .'('.local_management_return_user_info($modi_sql->userid).')';
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                    
                </tbody> 
            </table>
            <div id="btn_area">
                <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_management'); ?>" onclick="return course_create_submit_check()"/>
                <?php if (!empty($courseid)) { ?>
                    <!--<input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_management'); ?>" onclick="course_delete('<?php echo $courseid; ?>')"/>-->
                <?php } ?>
                <input type="button" style="float:left; margin-right: 10px;" value="<?php echo get_string('list', 'local_management'); ?>" onclick="javascript:location.href = 'index.php';"/>
            </div>
        </form><!--Search Area2 End-->
        <?php
                $product_sql = "select lc.*, lco.price as originprice
                from {lmsdata_class} lc
                JOIN {course} mc ON lc.courseid = mc.id
                JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                JOIN {course} mc2 ON lco.courseid = mc2.id
                JOIN {course_categories} ca ON ca.id = mc2.category
                left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                WHERE (lc.type = 1 or lc.type=3) and lc.parentcourseid = :id";

                $product = $DB->get_records_sql($product_sql  . $orderby, array("id"=> $course->lcid));
                if($product){
                ?>
        <table cellpadding="0" cellspacing="0" class="detail">    
            <tr>
                <td class="field_title"><?php echo get_string('productstatus', 'local_management')?></td>
                <td class="field_value">
                    <?php
                    foreach($product as $pr){
                        echo '<a href="/local/management/goods/form.php?id='.$pr->id.'">'.$pr->title . '|' . $pr->code .'|'. $pr->price."</a><br>";
                    }
                    ?>
                </td>
            </tr>
        </table>

        <?php
                }
        ?>
    </div><!--Content End-->

</div> <!--Contents End-->
</div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>

<div class="modal modal-default fade scroll_pop" id="modal-add-sample">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo get_string('samplevideo', 'local_management') ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-danger col-md-12">
                    <form action="#" method="post" id="searchSampleContent">
                        <input type="hidden" name="id" value="0" />
                        <div class="input-group">
                            <input type="text" name="search" placeholder="<?php echo get_string('course:samplesearchtext','local_management')?>" class="form-control"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-flat" id="btnSampleSearch"><?php echo get_string('search', 'local_management'); ?></button>
<!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-12" id="contents-list">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?php echo get_string('number','local_management')?></th>
                                <th><?php echo get_string('title', 'local_management'); ?></th>
                                <th><?php echo get_string('course:kinds', 'local_management'); ?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management')?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="popup_paging">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management'); ?></button>
        </div>
      </div>
    </div>
</div>




<div class="modal modal-default fade scroll_pop" id="modal-add-teacher">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close','local_management')?>">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo get_string('Instructor', 'local_management')?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-danger col-md-12">
                    <form action="#" method="post" id="searchTeacher">
                        <input type="hidden" name="id" value="0" />
                        <div class="input-group">
                            <input type="text" name="search" placeholder="<?php echo get_string('course:teachersearchtext','local_management')?>" class="form-control"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-flat" id="btnTeacherSearch"><?php echo get_string('search','local_management')?></button>
<!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col-md-12" id="teacher-list">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?php echo get_string('number','local_management')?></th>
                                <th><?php echo get_string('name','local_management')?></th>
                                <th><?php echo get_string('course:id','local_management')?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="4"><?php echo get_string('course:no_data','local_management')?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="popup_paging">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close','local_management')?></button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
    
    var thumbnailimg;

    $(document).ready(function () {
        <?php if(empty($courseid)){?>
        $("#free").attr("checked", false);   
        $("input[name=price]").attr("readonly",false).attr("disabled",false);
        <?php }?>
        if ($("span[name='file_link']").attr('id')) {
            thumbnailimg = 'exists';
        }

        /**
         * 썸네일 등록시 이미지 파일 체크
         * @type type
         */
        $("input[name=thumbnailimg]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["gif", "jpg", "jpeg", "png", "webp"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype','local_management')?>");
                    $(this).val("");
                    $('.file-name').val('');
                    return;
                }else{
                    $('input[name=file_del]').val(1);
                }
                thumbnailimg = $(this).val();
            }
        });
        
        

    });

    /**
     * 숫자키만 입력가능하게
     * @param {type} event
     * @returns {undefined|Boolean}
     */
    function onlyNumber(event) {
        event = event || window.event;
        var keyID = (event.which) ? event.which : event.keyCode;
        if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39) {
            return;
        } else {
            return false;
        }
    }
    /**
     * 문자열은 삭제
     * @param {type} event
     * @returns {undefined}
     */
    function removeChar(event) {
        event = event || window.event; 
        var keyID = (event.which) ? event.which : event.keyCode;

        if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39) {
            return;
        } else {
            event.target.value = event.target.value.replace(/[^0-9]/g, "");
        }
    }
    
    function course_delete(courseid) {
        if (confirm("강좌와 연결된 기수와 관련 정보들 모두 삭제하시겠습니까?") == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/management/courses/edu_course_delete.php" ?>',
                method: 'POST',
                data: {
                    courseid: courseid
                },
                success: function (data) {
                    if (data.status == 'success') {
                        alert('삭제하였습니다.');
                        document.location.href = "<?php echo $CFG->wwwroot . "/local/management/courses/index.php"; ?>";
                    } else {
                        alert('삭제하지 못했습니다.');
                        document.location.href = "<?php echo $CFG->wwwroot . "/local/management/courses/index.php"; ?>";
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                    alert(jqXHR.responseText);
                }
            });
        }
    }
    
    
$("#modal-add-teacher").keydown(function (event){
    if (event.keyCode == '13') {
        if (window.event) {
            event.preventDefault();
            local_management_search_teacher(1);
            return;
        }
    }
 });
 
 $("#modal-add-sample").keydown(function (event){
    if (event.keyCode == '13') {
        if (window.event) {
            event.preventDefault();
            local_management_search_sample_content(1);
            return;
        }
    }
 });

 //값 체크
function course_create_submit_check(){
    //카테고리
    if($('#cata2_end').val() == 1){
        if(local_management_value_null_check('#course_search_cata1') == false){
            alert('<?php echo get_string('goods:valcategory','local_management')?>');
            return false;
        }
    }else{
        if(local_management_value_null_check('#course_search_cata2') == false){
            alert('<?php echo get_string('goods:valcategory','local_management')?>');
            return false;
        }
    }
    //과정명
    if(local_management_value_null_check('input[name=coursename]') == false){
        alert('<?php echo get_string('goods:valcoursename','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=vi_coursename]') == false){
        alert('<?php echo get_string('goods:valcoursename','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=en_coursename]') == false){
        alert('<?php echo get_string('goods:valcoursename','local_management')?>');
        return false;
    }

    //금액
    if($("input:checkbox[id='free']").is(":checked") == false && local_management_value_null_check('input[name=price]') == false){
        alert('<?php echo get_string('goods:valprice','local_management')?>');
        return false;
    }
    //강의수
    if(local_management_value_null_check('input[name=lecturecnt]') == false){
        alert('<?php echo get_string('goods:valnum','local_management')?>');
        return false;
    }
    //저작권
    if(local_management_value_null_check('#copyright') == false){
        alert('<?php echo get_string('goods:valcopy','local_management')?>');
        return false;
    }
    //담당강사
    if(local_management_value_null_check("input[name='teacher[]']") == false){
        alert('<?php echo get_string('goods:valteacher','local_management')?>');
        return false;
    }

    //과정이미지
    if(local_management_value_null_check('input[name=thumbnailimg]') == false && $('#file').length == false){
        alert('<?php echo get_string('goods:valimg','local_management')?>');
        return false;
    }
    
    //강좌 한줄 소개
    if(local_management_value_null_check('input[name=onelineintro]') == false){
        alert('<?php echo get_string('goods:valoneline','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=vi_onelineintro]') == false){
        alert('<?php echo get_string('goods:valoneline','local_management')?>');
        return false;
    }
    if(local_management_value_null_check('input[name=en_onelineintro]') == false){
        alert('<?php echo get_string('goods:valoneline','local_management')?>');
        return false;
    }
    //소개
    if(CKEDITOR.instances.intro.getData() == ''){
        alert('<?php echo get_string('goods:valintro','local_management')?>');
        return false;
    }
    if(CKEDITOR.instances.vi_intro.getData() == ''){
        alert('<?php echo get_string('goods:valintro','local_management')?>');
        return false;
    }
    if(CKEDITOR.instances.en_intro.getData() == ''){
        alert('<?php echo get_string('goods:valintro','local_management')?>');
        return false;
    }
    //사용여부
    if($('input:radio[name=isused]').is(':checked') == false){
        alert('<?php echo get_string('goods:valisused','local_management')?>');
        return false;
    }
    return true;
}

//담당강사
function local_management_search_teacher(page) {
    var search = $('form#searchTeacher input[name=search]').val();
    search = $.trim(search);
    var teacher = document.getElementsByName('teacher[]');
    var teacher_txt = '';

    for(i = 0; i<teacher.length; i++){
        if(i == teacher.length - 1){
            teacher_txt += teacher[i].value;
        }else{
            teacher_txt += teacher[i].value + ',';
        }
    }
//    if(search != ''){
//        if(search.trim().length == 0) {
//            alert("검색어를 입력하세요");
//            return false;
//        }
//    }
    var result = call_ajax('teacher', {search: search, teacher:teacher_txt, page:page}, false);
    if(result.status == "success") {
        $tbody = $('#modal-add-teacher #teacher-list table tbody');
        $tbody.empty();
        $paging = $('#modal-add-teacher .popup_paging');
        $paging.empty();
        if(result.list.total_count > 0) {
            var $i = result.list.total_count - ((page-1) * 10);
            $.each(result.list.list, function() {
                $tr = $('<tr>');
                $tr.data(this);
                var $num = $('<td>').text($i);
                var $tdFullName =$('<td>');
                var $fullname = $('<span>').addClass('txt').text(this.fullname);
                $tdFullName.append($fullname);

                var $tdEmail =$('<td>').text(this.username);

                var $tdButtons =$('<td>');
                var $divbtn = $('<div>').addClass('btn-group pull-right');
                var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                $divbtn.append($btnAdd);
                $($btnAdd).on('click', function (e) {
                    e.preventDefault();
                    var $tr = $(this).closest('tr');
                    local_management_add_teacher($tr.data());
                    $tr.remove();
                    $('#modal-add-teacher').modal('hide');
                });
                $tdButtons.append($divbtn);

                $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                $tbody.append($tr);
                $i--;
            });
            $paging.append(result.paging);
        } else {
            $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data','local_management')?></td></tr>');
            $paging.append(result.paging);
        }
    } else {
        alert(result.error);
    }
}    

//맛보기영상
function local_management_search_sample_content(page) {
    var search = $('form#searchSampleContent input[name=search]').val();
    search = $.trim(search);
    var samplecontent = document.getElementsByName('samplecontent[]');
    var samplecontent_txt = '';

    for(i = 0; i<samplecontent.length; i++){
        if(i == samplecontent.length - 1){
            samplecontent_txt += samplecontent[i].value;
        }else{
            samplecontent_txt += samplecontent[i].value + ',';
        }
    }
//    if(search != ''){
//        if(search.trim().length == 0) {
//            alert("검색어를 입력하세요");
//            return false;
//        }
//    }
    var result = call_ajax('samplecontent', {search: search,samplecontent:samplecontent_txt,page:page}, false);
    if(result.status == "success") {
        $tbody = $('#modal-add-sample #contents-list table tbody');
        $tbody.empty();
        $paging = $('#modal-add-sample .popup_paging');
        $paging.empty();
        if(result.list.total_count > 0) {
            //$startnum = $count_courses - (($currpage - 1) * $perpage);
            var $i = result.list.total_count - ((page-1) * 10);
            $.each(result.list.list, function() {
                $tr = $('<tr>');
                $tr.data(this);
                var $num = $('<td>').text($i);
                var $tdFullName =$('<td>');
                var $fullname = $('<span>').addClass('txt').text(this.con_name);
                $tdFullName.append($fullname);

                var $tdEmail =$('<td>').text(this.con_type);

                var $tdButtons =$('<td>');
                var $divbtn = $('<div>').addClass('btn-group pull-right');
                var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                $divbtn.append($btnAdd);
                $($btnAdd).on('click', function (e) {
                    e.preventDefault();
                    var $tr = $(this).closest('tr');
                    local_management_add_sample_content($tr.data());
                    $('#modal-add-sample').modal('hide');
                });
                $tdButtons.append($divbtn);

                $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                $tbody.append($tr);
                $i--;
            });
            $pagediv = '<div class="pagination">&nbsp;<strong>1</strong>&nbsp;<a href="javascript:local_management_search_sample_content(2);">2</a>&nbsp;<a href="javascript:local_management_search_sample_content(3);">3</a>&nbsp;<a class="prev" href="javascript:local_management_search_sample_content(2);"><img alt="prev" src="http://open.jinotech.com:13080/chamktu/img/pagination_right.png"/></a></div>';
            $paging.append(result.paging);
        } else {
            $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data','local_management')?></td></tr>');
            $paging.append(result.paging);
        }
    } else {
        alert(result.error);
    }
}
</script>
