<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');


$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);
$stext = optional_param('stext', '', PARAM_RAW);
$stext = trim($stext);
if ($type == 1) {
    $Identification = 'recommend';
    $menutext = get_string('recommendcourse', 'local_management');
} else if ($type == 2) {
    $Identification = 'popular';
    $menutext = get_string('popularcourse', 'local_management');
} else {
    $Identification = 'package';
    $menutext = get_string('package', 'local_management');
}
$pagesettings = array(
    'title' => $menutext,
    'heading' => $menutext,
    'subheading' => '',
    'menu' => $Identification,
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');



$perpages = [10, 30, 50, 100];

//$contents = management_contnets_get_contents($type, $page, $perpage, $stype, $scate, $stext,$ordertype,$orderitem);
//$totalcount = management_contnets_get_total($type, $stype, $scate, $stext);

$query = "select lmc.*, lc.id as lcid, lc.title, lc.vi_title, lc.en_title, lc.code, lc.price, lc.type, lc.isused, lc.learningstart,lc.learningend  from {lmsdata_main_course} lmc JOIN {lmsdata_class} lc ON lmc.courseid = lc.id 
          WHERE lmc.type=:type";
$params = array();
$params['type'] = $type;
if (!empty($stext)) {
    $sql_where_search = array();
    $sql_where = '';
    $sql_where_search[] = $DB->sql_like('lc.title', ':coursename');
    $params['coursename'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.vi_title', ':vi_coursename');
    $params['vi_coursename'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.en_title', ':en_coursename');
    $params['en_coursename'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.code', ':coursecode');
    $params['coursecode'] = '%' . $stext . '%';
    if (!empty($sql_where_search)) {
        $sql_where = ' AND ( ' . implode(' or ', $sql_where_search) . ')';
    } else {
        $sql_where = '';
    }
}

$contents = $DB->get_records_sql($query . $sql_where, $params);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">


                <div id = "data1" class = "tab-pane fade in active">
                    <div>

                        <form action="index.php" method="get" name='search_form'>
                            <input type='hidden' name='type' value="<?php echo $type ?>">
                            <input type="hidden" name="orderitem" value="<?php echo $orderitem ?>"/>
                            <input type="hidden" name="ordertype" value="<?php echo $ordertype ?>"/>
                            <input type="text" name="stext" value="<?php echo $stext != '' ? $stext : null ?>" placeholder="<?php echo get_string('goods:placeholder', 'local_management') ?>">
                            <input type="submit" value="검색">
<!--                                <select class="f-r" name="perpage">
                            <?php foreach ($perpages as $perp) { ?>
                                                <option value="<?php echo $perp ?>" <?php echo $perpage == $perp ? 'selected' : '' ?>><?php echo $perp ?></option>
                            <?php } ?>
                            </select>-->
                        </form>

                    </div>
                    <table>
                        <thead class = "thead-light">
                            <tr>
                                <th>
                                    <input type = "checkbox" class = "checkall" />
                                </th>
                                <th><?php echo get_string('number', 'local_management') ?></th>
                                <th><?php echo get_string('goodsname', 'local_management') ?></th>
                                <th><?php echo get_string('goodstype', 'local_management') ?></th>
                                <th><?php echo get_string('goodscode', 'local_management') ?></th>
                                <th><?php echo get_string('price', 'local_management') ?></th>
                                <?php if ($type == 3) { ?>
                                    <th><?php echo get_string('color', 'local_management') ?></th>
                                    <th><?php echo get_string('best', 'local_management') ?></th>
                                    <th><?php echo get_string('modify', 'local_management') ?></th>
                                <?php } ?>
                                <th><?php echo get_string('isused', 'local_management') ?></th>
                                <th><?php echo get_string('applicationperiod', 'local_management') ?></th>
                                <th><?php echo get_string('timecreated', 'local_management') ?></th>
                                <?php if($type ==1){ ?>
                                <th><?php echo get_string('course:recommended', 'local_management') ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($contents) {
                                $startnum = (($page - 1) * $perpage) + 1;
                                foreach ($contents as $content) {
                                    ?>
                                    <tr data-id="<?php echo $content->id ?>" data-code="<?php echo $content->code ?>">
                                        <td>
                                            <input type = "checkbox" class="conslist"  conid="<?php echo $content->id ?>"/>
                                        </td>
                                        <td><?php echo $startnum++ ?></td>
                                        <td><a href="<?php echo $CFG->wwwroot ?>/local/management/goods/form.php?id=<?php echo $content->lcid ?>">
                                                <?php
                                                switch (current_language()) {
                                                    case 'ko' :
                                                        $category_name = $content->title;
                                                        break;
                                                    case 'en' :
                                                        $category_name = $content->en_title;
                                                        break;
                                                    case 'vi' :
                                                        $category_name = $content->vi_title;
                                                        break;
                                                }
                                                ?>
                                                <?php echo $category_name ?>
                                            </a> </td>
                                        <?php
                                        if ($content->type == 1) {
                                            $typetxt = get_string('course', 'local_management');
                                        } else if ($content->type == 3) {
                                            $typetxt = get_string('course', 'local_management') . '+' . get_string('book', 'local_management');
                                        }
                                        ?>
                                        <td><?php echo $typetxt ?> </td>
                                        <td><?php echo $content->code ?> </td>
                                        <td><?php echo number_format($content->price) ?></td>
                                        <?php if ($type == 3) { ?>
                                            <td class="colorcode">
                                                <span style="display:inline-block;width: 15px;height: 15px;background: #<?php echo $content->colorcode ?>;" id="colorspan<?php echo $content->id ?>"></span>
                                                <input type="text" value="<?php echo $content->colorcode ?>" name="colorcode" id="colorcode<?php echo $content->id ?>" onchange="colorchange(this)">
                                            </td>
                                            <td><input type="checkbox" name="best<?php echo $content->id ?>" id="best<?php echo $content->id ?>" <?php echo ($content->best == 1) ? "checked" : "" ?>></td>
                                            <td><button class='edit'><?php echo get_string('modify', 'local_management') ?></button></td>
                                        <?php } ?>
                                        <td>
                                            <?php
                                            if ($content->isused == 0) {
                                                echo get_string('used', 'local_management');
                                            } else {
                                                echo get_string('notused', 'local_management');
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo date("Y.m.d", $content->learningstart) . '~' . date("Y.m.d", $content->learningend) ?></td>
                                        <td><?php echo date("Y.m.d", $content->timecreated) ?> </td>
                                         <?php if($type ==1){ ?>
                                        <td>
                                            <input type = "checkbox" class="recommended" <?php echo $content->recommended ? 'checked':''?>  conid="<?php echo $content->id ?>"/>
                                        </td>
                                         <?php } ?>
                                    </tr>
                                    <?php
                                }
                            } else {
                                $empty_num = 9;
                                if ($type == 3) {
                                    $empty_num = 12;
                                }
                                ?>
                            <td colspan="<?php echo $empty_num ?>"><?php echo get_string('goods:nodata', 'local_management') ?></td>
                        <?php } ?> 
                        </tbody>
                    </table>
                    <?php if (!empty($paging)) { ?>
                        <nav aria-label = "Page navigation" class = "text-center">
                            <ul class = "pagination">
                                <?php echo $paging ?>
                            </ul>
                        </nav>
                    <?php } ?>
                </div>

                <div class = "text-right mg-bt20">
                    <!--                <button class="btn btn-success pull-right xlsdown">엑셀다운로드</button> 
                                    <button class = "btn btn-danger pull-right addpop">엑셀업로드</button>-->
                    <button class = "red_btn " onclick="removecontents();"><?php echo get_string('del', 'local_management') ?></button>
                </div>
                <!--/.box -->

                <div class = "search-form">
                    <form onsubmit="consearch(); return false">
                        <!--                        <label for = "consearchid">상품명, 상품code</label>-->
                        <input type = "text" class = "form-control" id = "consearchid" placeholder="<?php echo get_string('goods:placeholder', 'local_management') ?>" />
                        <input type = "button" class = "btn btn-default" onclick="consearch()" value = "<?php echo get_string('search', 'local_management') ?>" />
                    </form>
                </div>
                <table id="append_contents">
                    <thead class = "thead-light">
                        <tr>
                            <th><?php echo get_string('number', 'local_management') ?></th>
                            <th><?php echo get_string('goodsname', 'local_management') ?></th>
                            <th><?php echo get_string('goodstype', 'local_management') ?></th>
                            <th><?php echo get_string('code', 'local_management') ?></th>
                            <th><?php echo get_string('price', 'local_management') ?></th>
                            <?php if ($type == 3) { ?>
                                <th><?php echo get_string('color', 'local_management') ?></th>
                                <th><?php echo get_string('best', 'local_management') ?></th>
                            <?php } ?>
                            <th><?php echo get_string('insert', 'local_management') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                        </tr>
                    </tbody>
                </table>
<!--                <p class = "t-info">개별 등록은 콘텐츠 아이디를 입력 하신 후, 정보 확인 후 개별 등록을 하실 수 있습니다.<br/>여러개의 콘텐츠 등록을 원하시면, 엑셀 업로드 기능을 이용해 등록 해주세요. </p>-->
<!--                <div class = "btn_area">
                    <button class = "blue_btn" onclick="searchadd()"><?php echo get_string('insert', 'local_management') ?></button>
                </div>-->

            </div>
            <!--/.col -->
        </div>
    </div>

</section>
<!--/.content -->


<?php
include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">
    $(function () {
        $(".recommended").click(function(){
            var  id = $(this).attr("conid");
            var  checked = $(this).is(":checked") ? 1 :0;
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'recommended',data:{id:id,checked:checked}},
                success: function (data, status, xhr) {
                },
            });
            
        });
        $('.colorcode').on('click', function (e) {
            e.preventDefault();
            var tr = $(this).closest("tr");
            var id = tr.data('id');
            $('#colorcode' + id).colorpicker();
        });

    });
    function colorchange(object) {
        var tr = $(object).closest("tr");
        var id = tr.data('id');
        var nwcolor = $('#colorcode' + id).val();
        $('#colorspan' + id).css('background', '#' + nwcolor);
    }

    //수정
    $('.edit').on('click', function (e) {
        var tr = $(this).closest("tr");
        var id = tr.data('id');
        var code = tr.data('code');
        var colorcode = $('#colorcode' + id).val();
        //var best = $('#best'+id).val();
        var best = $('#best' + id).is(":checked");
        searchadd("edit", id, code, colorcode, best);
//        e.preventDefault();
//        var colorcode = $('#colorcode'+$btnAdd.data('id')).val();
//        var best =$("#best"+$btnAdd.data('id')).is(":checked")
//        searchadd($btnAdd.data('id'),$btnAdd.data('code'), colorcode,best);
    });

    function removecontents() {
        if (confirm('<?php echo get_string('job:delalert', 'local_management') ?>')) {
            var type = "<?php echo $type ?>";
            var conarr = new Array();
            $(".conslist:checked").each(function (i, v) {
                conarr.push($(v).attr("conid"));
            });
            var result = call_ajax('delete', {conid: conarr, type: type}, false);
            if (result.status == 'success') {
                alert('<?php echo get_string('goods:deletedo', 'local_management') ?>');
                location.reload();
            } else {
                alert(result.error);
            }
//            $.ajax({
//                type: 'POST',
//                url: 'remove_reco.php',
//                data: {conid: conarr, type: type},
//                success: function (data, status, xhr) {
//
//                    location.reload();
//                },
//            });
        }
    }



    function call_ajax(action, data, async) {
        var rvalue = false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            async: async,
            data: {action: action, data: data},
            success: function (result) {
                rvalue = result;
            },
            error: function (xhr, status, error) {
            }
        });

        return rvalue;
    }

    function consearch() {
        var conid = $("#consearchid").val();
        var type = $("input[name=type]").val();
        conid = $.trim(conid);
        if (conid == '') {
            alert("<?php echo get_string('goods:searchtext', 'local_management') ?>");
        } else {
            var result = call_ajax('search', {code: conid, type: type}, false);
            if (result.status == 'success') {
                $tbody = $('#append_contents tbody');
                $tbody.empty();
                if (result.total == 0) {
                    alert('<?php echo get_string('goods:no_data', 'local_management') ?>');
                } else {
                    var $i = result.total;
                    $.each(result.list, function () {
                        $tr = $('<tr>');
                        $tr.data(this);
                        var text = '';
                        if (this.type == 1) {
                            text = '<?php echo get_string('course', 'local_management') ?>';
                        } else {
                            text = '<?php echo get_string('course', 'local_management') ?>' + '+' + '<?php echo get_string('book', 'local_management') ?>';
                        }
                        // onclick="colorpicker()"
                        var txt = '<td><input type="text" id="colorcode' + this.id + '" name="colorcode" value="" class="colorcode2"></td>';

                        var $num = $('<td>').text($i);
                        var $title = $('<td>').text(this.title);
                        var $text = $('<td>').text(text);
                        var $code = $('<td>').text(this.code);
                        var $price = $('<td>').text(comma(this.price));
                        if (type == 3) {
                            var $txt = txt;
                            $('.colorcode2').on('click', function (e) {
                                e.preventDefault();
                                $('.colorcode2').colorpicker();
                            });
                            var bt = '<td><input type="checkbox" name="best" id="best' + this.id + '"></td>';
                            var $best = bt;
                            var $type3 = $txt + $best;
                        }
                        var $btnAddo = $("<a data-id='" + this.id + "' data-code='" + this.code + "'>").addClass('btn btn-danger btn-sm clickable').html('<i class="fa fa-plus clickable"></i>');
                        var $btnAdd = $('<td>').html($btnAddo);
                        $($btnAddo).on('click', function (e) {
                            e.preventDefault();
                            var colorcode = $('#colorcode' + $btnAddo.data('id')).val();
                            var best = $("#best" + $btnAddo.data('id')).is(":checked")
                            searchadd('add', $btnAddo.data('id'), $btnAddo.data('code'), colorcode, best);
                        });

                        $tr.append($num).append($title).append($text).append($code).append($price).append($type3).append($btnAdd);
                        $tbody.append($tr);
                        $i--;
                    });
                }
            } else {
                alert(result.error);
            }
        }
    }

    function searchadd(mod, id, code, colorcode, best) {
        var type = $("input[name=type]").val();
        if (best == true) {
            best = 1;
        } else {
            best = 0;
        }
        var text = '';
        var mod_txt = '';
        var confirm_txt = '';
        if (type == 1) {
            text = "<?php echo get_string('recommendcourse', 'local_management') ?>"
        } else if (type == 2) {
            text = "<?php echo get_string('popularcourse', 'local_management') ?>"
        } else {
            text = "<?php echo get_string('package', 'local_management') ?>";
        }
        if (mod == 'add') {
            mod_txt = "<?php echo get_string('goods:insert', 'local_management') ?>";
            confirm_txt = '<?php echo get_string('goodscode', 'local_management') ?>' + code + ' <?php echo get_string('goods:to', 'local_management') ?> [' + text + '] ' + '<?php echo get_string('goods:item', 'local_management') ?>' + mod_txt + '<?php echo get_string('goods:do', 'local_management') ?>';
        } else if (mod == "edit") {
            mod_txt = "<?php echo get_string('modify', 'local_management') ?>";
            confirm_txt = '[' + code + ']' + '<?php echo get_string('goods:confirmtxt', 'local_management') ?>';
        }
        if (code == '') {
            alert('<?php echo get_string('goods:information', 'local_management') ?>');
        } else {
            //if (confirm(("상품코드 : " + code + " 를 [ "+text+" ] 항목에 "+mod_txt+"하시겠습니까?"))) {
            if (confirm(confirm_txt)) {
                var result = call_ajax(mod, {code: code, id: id, type: type, colorcode: colorcode, best: best}, false);
                if (result.status == 'success') {
                    location.reload();
                } else {
                    alert(result.error);
                }

            }
        }
    }
</script>
