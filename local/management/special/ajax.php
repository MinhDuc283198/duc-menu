<?php
define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');

$action = optional_param('action','',PARAM_RAW);
switch ($action) {
    case 'search' :
        $data = required_param_array('data', PARAM_RAW);
        $now = time();
        $cnt_sql = "select count(lc.id) ";
        $sql = "select lc.* ";
        $sql_from = "
                from m_lmsdata_class lc
                JOIN m_course mc ON lc.courseid = mc.id
                JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
                JOIN m_course mc2 ON lco.courseid = mc2.id
                JOIN m_course_categories ca ON ca.id = mc2.category
                left JOIN m_course_categories ca2 ON ca.parent = ca2.id
                WHERE ( lc.type = 1 or lc.type = 3) and (lc.code like :code or lc.title like :title) AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0";
        $orderby = ' ORDER BY lc.id DESC';
        
        //중복
        $overlap = $DB->get_fieldset_sql("select courseid from {lmsdata_main_course} where type = ? and courseid is not null", array($data['type']));
        if($overlap){ 
           foreach($overlap as  $ol){
                $sql_where[] = " $ol ";
            }
            if($sql_where){
                $sql_from .= ' AND lc.id NOT IN ( ' . implode(' , ', $sql_where) . ')';
            }
        }
        
        $params['code'] = '%'.trim($data['code']).'%';
        $params['title'] = '%'.trim($data['code']).'%';
        $list = $DB->get_records_sql($sql.$sql_from  . $orderby, $params);
        $total = $DB->count_records_sql($cnt_sql.$sql_from, $params);

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->total = $total;

        echo json_encode($rvalue);
        break;
    case 'add' :
        $data = required_param_array('data', PARAM_RAW);
        $lmsdata = new stdClass();
        $lmsdata->timecreated = time();
        $lmsdata->type = $data['type'];
        $lmsdata->courseid = $data['id'];
        if(empty($data['colorcode'])){
            $data['colorcode'] = "ffffff";
        }
        $lmsdata->best = $data['best'];
        $lmsdata->colorcode = $data['colorcode'];
        $DB->insert_record('lmsdata_main_course', $lmsdata);

        $rvalue = new stdClass();
        $rvalue->status = 'success';

        echo json_encode($rvalue);
        break;
    case 'edit' :
        $data = required_param_array('data', PARAM_RAW);
        $lmsdata = new stdClass();
        $lmsdata->timemodified = time();
        $lmsdata->id = $data['id'];
        if(empty($data['colorcode'])){
            $data['colorcode'] = "ffffff";
        }
        $lmsdata->best = $data['best'];
        $lmsdata->colorcode = $data['colorcode'];
        $DB->update_record('lmsdata_main_course', $lmsdata);
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';

        echo json_encode($rvalue);
        break;
    case 'delete':
        $data = array();
        $data['type'] = $_POST['data']['type'];
        $data['conids'] = $_POST['data']['conid'];
        //$data = required_param_array('data', PARAM_RAW);
        
        if($data['conids']){
            foreach($data['conids'] as $conid){
                $DB->delete_records('lmsdata_main_course',array('id'=>$conid, 'type'=>$data['type']));
            }
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';

        echo json_encode($rvalue);
        break;
    case 'recommended' :
        $data = required_param_array('data', PARAM_RAW);
        $rt = $DB->get_record("lmsdata_main_course",array('id'=>$data['id']));
        $rt -> recommended = $data['checked'];
        $DB->update_record("lmsdata_main_course",$rt);
        echo json_encode($data);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}
