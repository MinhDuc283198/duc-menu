<?php

$string['pluginname'] = '사이트 관리';
$string['pluginnameplural'] = '사이트 관리';

$string['cannotbedeleted'] = '삭제할 수 없습니다.';
$string['cannotdeletemainmenu'] = '메인 메뉴는 삭제할 수 없습니다.';
$string['duplicatedshortname'] = '중복된 Identification 입니다.';

$string['nopermissions'] = '권한이 없습니다';



//  결제 관리 언어팩
$string['pay'] ='결제';
$string['number'] ='번호';
$string['item'] ='품목';
$string['productname'] ='결제 상품명';
$string['user'] ='사용자';
$string['amount'] ='결제금액(VND)';
$string['paymentdate'] ='결제일';
$string['paymentmethod'] ='결제 수단';
$string['paymentstatus'] ='결제상태';
$string['receipt'] ='영수증';
$string['book'] ='교재';
$string['course'] ='강좌';
$string['cardpay'] ='카드결제';
$string['acountpay'] ='계좌입금';
$string['beforepay'] ='결제전';
$string['successpay'] ='결제완료';
$string['failpay'] ='결제실패';
$string['beforecancel'] ='환불요청';
$string['printing'] ='인쇄';
$string['manageterms'] ='비상 약관 관리';
$string['proceeding'] = '진행중';
$string['refundcompleted'] = '환불완료';
$string['paydate'] = '결제/입금일';
$string['refunddate'] = '환불(승인취소)일';
$string['changestatus'] = '무통장 입금 상태 변경';
$string['waiting'] = '입금대기';
$string['confirmpayment'] = '입금확인';
$string['refundstatus'] ='환불상태';
$string['paymentstatus2'] ='결제(입금) 상태';
$string['paycondate'] ='결제일/입금확인일';
$string['procomplete'] = '처리완료';
$string['normal'] = '정상승인';
$string['approval'] = '승인취소';
$string['refunding'] = '환불진행';
$string['refundcon'] = '환불완료';
$string['bankbookwaiting'] = '무통장입금대기';
$string['remark'] = '비고';
$string['shippinginfo'] = '배송정보';
$string['cell'] = '휴대폰 번호';
$string['shippingaddress'] = '배송주소';
$string['refundprocessing'] = '환불처리';
$string['paymentinfo'] = '결제정보';
$string['refundinfo'] = '환불정보';
$string['refunddate'] = '환불접수일';
$string['refundreason'] = '환불사유';
$string['refundreasontxt'] = '환불 사유를 입력해주세요';
$string['refunmethod'] = '환불방식';
$string['selectrefunmethod'] = '환불방식선택';
$string['bankbook'] = '무통장 입금';
$string['refundaccount'] = '환불계좌';
$string['accounttxt'] = "계좌번호 입력(‘-’없이 입력 )";
$string['refundamount'] = '환불금액';
$string['refundcancel'] = '환불취소';
$string['refundcanceltxt'] = '승인취소요청 및 환불처리 후에는 환불 취소가 불가합니다.';
$string['refundtxt'] = '모모페이, 신용카드 환불은 승인취소, 계좌 환불은 무통장 입금을 선택하세요.';
$string['paydatecon'] = '입금확인일';
$string['refundmethod'] ='환불 수단';

//강좌 등록 관리 언어팩
$string['course:title'] = '강좌등록 관리';
$string['processname'] = '강좌명(베트남어/한국어)';
$string['processcode'] = '강좌코드';
$string['processclass'] = '과정 분류';
$string['lecturenum'] = '강의수';
$string['professor'] = '교수';
$string['price'] = '금액';
$string['regularprice'] = '정가';
$string['copyright'] = '저작권';
$string['totalenrollment'] = '총 수강인원';
$string['completion'] = '수료인원';
$string['timecreated'] = '등록일';
$string['isused'] = '사용여부';
$string['classroomentry'] = '강의실 입장';
$string['used'] = '사용';
$string['notused'] = '사용안함';
$string['free'] = '무료수강';
$string['category'] = '카테고리';
$string['lecture'] = '강';
$string['Instructor'] = '담당강사';
$string['search'] = '검색';
$string['samplevideo'] = '맛보기 영상';
$string['processimg'] = '과정 이미지';
$string['courseonelineintroduce'] = '강좌 한줄 소개';
$string['courseintro'] = '강좌 소개';
$string['courseperiod'] = '수강기간';
$string['reviewperiod'] = '복습기간';
$string['day'] = '일';
$string['insert'] = '등록';
$string['modify'] = '수정';
$string['productstatus'] = '상품등록 현황';
$string['save'] = '저장';
$string['list'] = '목록';
$string['bigcategory'] = '대분류 전체';
$string['middlecategory'] = '중분류 전체';
$string['courseregist'] = '강좌 등록';
$string['reset'] = '초기화';
$string['click'] = '클릭하세요';
$string['empty_course'] = '등록된 강좌가 없습니다.';
$string['coursename'] = '강좌명';
$string['ko'] = '한국어';
$string['vi'] = '베트남어';
$string['en'] = '영어';
$string['code_text'] = '등록 시, 자동 부여됩니다.';
$string['file_upload'] = '파일 업로드';
$string['empty_data'] = '데이터가 없습니다.';
$string['input_course'] = '강좌명, 강좌코드, 교수명 검색';
$string['input_book'] = '교재명, 교재코드, 저자명 검색';
$string['empty_book'] = '등록된 교재가 없습니다.';
$string['copyright0'] = '저작권 전체';
$string['bookname2'] = '교재명(베트남어/한국어)';
$string['productstats'] = '상품등록현황';
$string['time'] = '시간';
$string['viewmore'] = '개씩 더보기';
$string['searchtext'] = '강의명을 입력하세요.';
$string['originname'] = '원본명';
$string['origincode'] = '원본코드';
$string['sale'] = '수강(판매)';
$string['completion2'] = '수료';
$string['notcompletion'] = '미수료';
$string['studentregistration'] = '수강생 등록';
$string['goods:copy'] = '복사';
$string['studentexcel'] = '수강생 엑셀등록';
$string['origincourse'] = '강좌원본정보';
$string['originbook'] = '교재원본정보';
$string['discountrat'] = '할인율';
$string['none'] = '없음';
$string['name'] = '이름';
$string['close'] = '닫기';
$string['origincoursegoods'] = '강좌원본상품';
$string['originbookgoods'] = '교재원본상품';
$string['total'] = '총 {$a}개';
$string['course:teachersearchtext'] = '이름, ID로 검색하세요';
$string['course:id'] = "ID";
$string['course:no_data'] = "검색결과가 없습니다.";
$string['course:kinds'] = "종류";
$string['course:samplesearchtext'] = "제목으로 검색하세요";
$string['course:recommended'] = "추천사용";


//추천검색어 언어팩
$string['insert-stext'] = '추천검색어로 등록하시겠습니까? (추천검색어는 최대 5개까지 등록 가능합니다.)';
$string['delete-stext'] = "추천검색어를 해제하시겠습니까?";
$string['alert-stext'] = "추천검색어는 최대 5개까지 등록가능합니다.";
$string['nodata-stext'] = "등록된 추천검색어가 없습니다.";
$string['stext'] = "검색어";
$string['stext-manage'] = "추천검색어 관리";
$string['e-learning'] = "이러닝";
$string['jobs'] = "구인구직";
$string['jobs_per'] = "개인";
$string['jobs_com'] = "기업";
$string['division'] = "구분";

//교재 등록 관리 언어팩
$string['book:title'] = '교재 소개';
$string['bookname'] = '교재명';
$string['bookcode'] = '교재코드';
$string['bookclass'] = '교재 분류';
$string['publisher'] = '출판사';
$string['author'] = '저자';
$string['publisheddate'] = '발행일';
$string['bookimg'] = '교재 이미지';
$string['bookintro'] = '교재 소개';
$string['bookregist'] = '교재 등록';
$string['book:valtitle'] = '교재명을 입력해주세요';
$string['book:valpublisher'] = '출판사를 입력해주세요';
$string['book:valauthor'] = '저자를 입력해주세요';
$string['book:valdate'] = '발행일을 입력해주세요';
$string['book:valimg'] = '교재이미지를 입력해주세요';

//상품관리 언어팩
$string['goods:title'] = '상품관리';
$string['goodsname'] ='상품명';
$string['goodscode'] ='상품코드';
$string['goodstype'] ='상품타입';
$string['goodsprice'] = '상품가격';
$string['price2'] ='판매가(VND)';
$string['applicationperiod'] ='강좌등록일';
$string['goodsregist'] = '상품 등록';
$string['rcoursegoods'] = '관련 강좌 상품';
$string['rbookgoods'] = '관련 교재 상품';
$string['color'] = '색상';
$string['best'] = 'best';
$string['goods:nodata'] = '검색 결과가 없습니다.';
$string['goods:placeholder'] = '상품코드,상품명 검색';
$string['goods:delalert'] = '선택한 항목을 삭제하시겠습니까?';
$string['goods:searchtext'] = '검색어를 입력해주세요';
$string['goods:no_data'] = '없는상품입니다.';
$string['goods:insert'] = '추가';
$string['goods:information'] = '상품 정보를 확인해 주세요';
$string['goods:to'] = '를';
$string['goods:item'] = '항목에';
$string['goods:do'] = '하시겠습니까?';
$string['recommendcourse'] = '추천강좌';
$string['popularcourse'] = '인기강좌';
$string['package'] = '한국어 추천 패키지';
$string['goods:deletedo'] = '삭제 완료되었습니다.';
$string['goods:confirmtxt'] = ' 상품의 색상/best항목 수정사항을 저장하시겠습니까?';
$string['goods:type'] = '상품유형';
$string['goods:valcategory'] = '카테고리를 입력해주세요';
$string['goods:valcoursename'] = '과정명을 입력해주세요';
$string['goods:valprice'] = '금액(정가)를 입력해주세요';
$string['goods:valnum'] = '강의수를 입력해주세요';
$string['goods:valcopy'] = '저작권을 입력해주세요';
$string['goods:valteacher'] = '담당강사를 입력해주세요';
$string['goods:valimg'] = '과정이미지를 입력해주세요';
$string['goods:valoneline'] = '강좌 한줄 소개를 입력해주세요';
$string['goods:valintro'] = '소개를 입력해주세요';
$string['goods:valisused'] = '사용여부를 입력해주세요';
$string['goods:valimgtype'] = 'gif, jpg, jpeg, png 파일만 업로드 해주세요';
$string['goods:valimgtype2'] = 'jpg,png 파일만 업로드 해주세요';
$string['goods:usercomplete'] = '등록이 완료되었습니다';
$string['goods:overlap'] = '중복';
$string['goods:failure'] = '실패';
$string['goods:file'] = '파일을 등록해야 합니다';
$string['goods:form'] = '양식에 맞춰서 사용자를 추가해주세요';
$string['goods:valdivision'] = '구분을 입력해주세요';
$string['goods:valgoodsname'] = '상품명을 입력해주세요';
$string['goods:valprice2'] = '판매가를 입력해주세요';
$string['goods:valperiod'] = '적용기간을 입력해주세요';
$string['goods:valperiod2'] = '수강기간을 입력해주세요';
$string['goods:valorigincourse'] = '강좌 원본정보를 입력해주세요';
$string['goods:valoriginbook'] = '교재 원본정보를 입력해주세요';
$string['goods:searchtext2'] = '상품명, 상품코드 검색';
$string['goods:ocoursesearch'] = '강좌명, 강좌코드로 검색하세요';
$string['goods:obooksearch'] = '교재명, 교재코드로 검색하세요';
$string['goods:goodssearch'] = '상품명, 상품코드로 검색하세요';
$string['goods:tag'] = '태그';
$string['goods:input'] = '입력';
$string['goods:taginput'] = '태그 입력(#입력 제외)';
$string['goods:taqalert'] = '태그를 입력해주세요.';
$string['goods:tagalert2'] = '#은 입력할 수 없습니다.';

//배송관리 언어팩
$string['ordernumber'] ='주문번호';
$string['code'] = '코드';
$string['orderer'] = '주문자';
$string['address'] = '주소';
$string['contact'] = '연락처';
$string['deliverystatus'] = '배송상태';



$string['title'] = '제목';
$string['boardcontent'] = '내용';
$string['service1'] = '개인정보보호방침';
$string['service2'] = '이용약관';
$string['term'] = '약관';
$string['service3'] = '서비스운영규정';
$string['del'] = '삭제';
$string['close'] = '닫기';
$string['write'] = '게시글 작성';
$string['correct'] = '수정';


$string['job:manage'] = '인기 채용정보 관리';
$string['job:manage2'] = '추천 채용정보 관리';
$string['job:comname'] = '회사명';
$string['job:name'] = '공고제목';
$string['job:id'] = '공고ID';
$string['job:date'] = '공고등록일';
$string['job:enddate'] = '공고마감일';
$string['job:maindate'] = '메인등록일';
$string['job:nodata'] = '검색 결과가 없습니다.';
$string['job:delalert'] = '선택한 항목을 삭제하시겠습니까?';


$string['search'] = '검색';
$string['job:alert1'] = '콘텐츠 정보 확인을 해주세요';
$string['job:alert2'] = '해당 공고를 인기 채용정보에 추가하시겠습니까?';
$string['job:alert3'] = '공고 아이디를 입력해주세요';
$string['job:alert4'] = '없는 아이디 입니다.';

//---보드
$string['write2'] = '글 작성';
$string['board:alert'] = '해당 게시판은 최대 {$a}크기의 파일만 업로드 가능합니다.';
$string['board:alert2'] = '파일은 최대';
$string['board:alert3'] = '개 첨부 가능합니다';
$string['category'] = '카테고리';
$string['board:noticeset'] = '상단게시설정';
$string['board:notice'] = '공지사항';
$string['board:qna'] = '1:1상담';
$string['board:manage'] = '게시판관리';
$string['board:list'] = '게시판 리스트';
$string['board:clist'] = '고객센터 리스트';
$string['boardset'] = '관리';
$string['board:writeanswer'] = '답글 작성';
$string['board:name'] = '게시판 이름';
$string['board:set'] = '게시판 설정';
$string['board:made'] = '게시판 만들기';
$string['board:writer'] = '개설자';
$string['board:update'] = '최근 업데이트';
$string['board:delete'] = '게시판 삭제';
$string['board:move'] = '이동';
$string['board:alert4'] = '게시판을 삭제하면 해당 게시판에 작성된 게시글들도 전부 삭제됩니다. 삭제하시겠습니까?';
$string['board:alert5'] = '해당 게시판과';
$string['board:alert6'] = '개의 게시글이 삭제되었습니다.';
$string['board:alert7'] = '삭제할 번호의 체크박스를 선택하세요.';
$string['successcancel'] ='취소 성공';

$string['bannermanagement'] = '배너관리';
$string['bannername'] = '배너명';
$string['attachments'] = '첨부파일';
$string['link'] = '링크';
$string['order'] = '순서';
$string['communitymanagement'] = '커뮤니티관리';
$string['groupname'] = '그룹명';
$string['thumbnail'] = '썸네일';
$string['noenrollmentcontents'] = '등록된 내용이 없습니다.';
$string['logoutnotsavegologinpage'] = '로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.';
$string['popupmanagement'] = '팝업관리';
$string['entertitle'] = '제목을 입력하세요.';
$string['mainpage'] = '메인페이지';
$string['contentsinputpls'] = '내용을 입력해 주세요.';
//출판사
$string['copyset'] = '저작권/출판사 관리';
$string['copy:writer'] = '등록자';
$string['copy'] = '저작권';
$string['publishinghouse'] = '출판사';
$string['copy:nodata'] = '등록된 저작권/출판사가 없습니다.';
$string['copy:alert'] = '빈칸 없이 입력해주세요.';


$string['job:numalert'] = '공고 ID는 숫자만 입력 가능합니다.';
$string['banner:alert'] = 'PC용 이미지와 Mobile용 이미지를 전부 업로드해주세요.';
$string['member'] = '회원';

//배송관리
$string['delivery:title'] = '배송관리';
$string['delivery:all'] = '전체';
$string['delivery:preparing'] = '준비중';
$string['delivery:shipping'] = '배송중';
$string['delivery:completed'] = '배송완료';
$string['delivery:searchtext'] = '상품명, 주문번호, 주문자 입력';
$string['delivery:searchtext2'] = '결제일 검색';
$string['delivery:change'] = '배송상태 변경';
$string['delivery:no_data'] = '검색 결과가 없습니다.';
$string['delivery:text'] = '선택하신 사용자의 배송상태를 변경 하시겠습니까?';
$string['delivery:status'] = '배송상태';
$string['delivery:processingdate'] = '처리일';
$string['delivery:manager'] = '담당자';
$string['delivery:histroy'] = '교재배송이력';
$string['delivery:total'] = '총';
$string['delivery:text2'] = '명의 배송 상태가 변경 되었습니다.';
$string['delivery:text3'] = '배송 상태를 변경할 사용자가 선택 되지 않았습니다. \n사용자를 선택 후 상태 변경을 해주세요.';

//회원관리
$string['user:title1'] = '개인회원 관리';
$string['user:title2'] = '강사 관리';
$string['user:title3'] = '관리자 관리';
$string['user:title4'] = '기업회원 관리';
$string['user:jointype'] = '가입유형';
$string['user:email'] = 'email';
$string['user:facebook'] = 'facebook';
$string['user:google'] = 'google';
$string['user:zalo'] = 'zalo';
$string['user:membertype'] = '회원유형';
$string['user:normal'] = '일반';
$string['user:bop'] = 'BOP';
$string['user:timecreated'] = '가입일';
$string['user:id'] = '사용자 ID';
$string['user:name'] = '이름';
$string['user:birth'] = '생년월일';
$string['user:phone'] = '전화번호';
$string['user:site'] = '가입사이트';
$string['user:last'] = '최근 접속일';
$string['user:auth'] = '이메일 인증 여부';
$string['user:sharing'] = '정보공유 동의 여부';
$string['user:activity'] = '활동여부';
$string['user:company'] = '회사';
$string['user:company_name'] = '기업명';
$string['user:approval'] = '관리자 승인';
$string['user:approval_pending'] = '대기';
$string['user:approval_accepted'] = '승인';
$string['user:approval_notaccepted'] = '반려';
$string['user:registrationcertificate'] = '사업자 등록증';
$string['user:business_registration_number'] = '사업자등록번호';
$string['user:request_date'] = '승인요청일';
$string['user:approval_date'] = '승인일';
$string['user:member_type'] = '회원유형';
$string['user:com_department'] = '담당자 부서/직급';
$string['user:com_contact_name'] = '담당자 이름';
$string['user:com_contact_number'] = '담당자 연락처';
$string['user:com_contact_email'] = '담당자 이메일';
$string['user:com_country'] = '국가';
$string['user:employers_company_type'] = '산업군';
$string['user:com_address'] = '주소';
$string['user:download'] = '다운로드';
$string['user:login'] = '로그인';
$string['user:noconnection'] = '접속안함';
$string['user:elearning'] = '이러닝';
$string['user:activity1'] = '활동';
$string['user:inactivity'] = '미활동';
$string['user:nodata'] = '데이터가 없습니다';
$string['user:registration'] = '사용자 등록';
$string['user:excell'] = '사용자 엑셀 등록';
$string['user:authority'] = '권한';
$string['user:duplicate'] = '중복체크';
$string['user:img'] = '프로필 이미지';
$string['user:pw'] = '비밀번호';
$string['user:pwchange'] = '비밀번호 변경';
$string['user:profile'] = '프로필';
$string['user:aword'] = '제자들에게 한 마디';
$string['user:goodsname'] = '강좌(상품)명';
$string['user:goodscode'] = '강좌(상품)코드';
$string['user:searchtext'] = 'ID, 이름, 이메일, 전화번호 검색';
$string['user:ad'] = '관리자';
$string['user:pr'] = '강사';
$string['user:bp'] = 'BOP회원';
$string['user:rs'] = '일반';
$string['user:emailtext1'] = '이메일을 입력해 주세요';
$string['user:emailtext2'] = '중복된 이메일입니다.';
$string['user:emailtext3'] = '사용가능한 이메일입니다.';
$string['user:idtext1'] = 'ID를 입력해 주세요';
$string['user:idtext2'] = '중복된 ID입니다.';
$string['user:idtext3'] = '사용가능한 ID입니다.';
$string['user:val1'] = '권한을 설정해 주세요';
$string['user:val2'] = '비밀번호를 입력해 주세요';
$string['user:val3'] = '이름을 입력해 주세요';
$string['user:val4'] = '이메일 중복체크를 해주세요';
$string['user:val5'] = 'ID 중복체크를 해주세요';
$string['user:val6'] = '전화번호를 입력해 주세요';
$string['user:delete'] = '정말 삭제하시겠습니까?';
$string['user:timestart'] = '수강시작일';
$string['user:timeend'] = '수강종료일';
$string['user:timemodify'] = '수강종료일을 수정하시겠습니까?';
$string['user:timesuccess'] = '수정이 완료되었습니다.';
$string['user:oneline'] = '강사 한줄 소개';
$string['user:applycourse'] = '강좌에 적용';
$string['user:applyintro'] = '강사소개 메뉴에 적용';
$string['user:searchtext2'] = '사용자 ID / 이름 검색';

//카테고리
$string['category:title'] = '강좌카테고리';
$string['category:top'] = '상위범주';
$string['category:sel'] = '선택';
$string['category:name'] = '범주명';
$string['category:Identification'] = '범주 식별번호';
$string['category:explan'] = '설명';

//콘텐츠 등록관리
$string['contents:title'] = '콘텐츠 등록 관리';
$string['contents:tplan'] = '교안';
$string['contents:type'] = '유형';
$string['contents:searchtext'] = '콘텐츠명, 콘텐츠 코드 검색';
$string['contents:name'] = '콘텐츠명';
$string['contents:code'] = '콘텐츠 코드';
$string['contents:category'] = '분류';
$string['contents:visang'] = '비상 URL';

$string['order:search'] = '주문번호,상품명,상품코드,주문자 검색';
$string['order:date'] = '주문일';
$string['notaperiod'] = '대기';

//배지 관리.
$string['badge'] = '배지';
$string['typeselect'] = '유형선택';
$string['badgemanagement'] = '배지 관리';
$string['badgetypemanagement'] = '배지 유형 관리';
$string['nobadge'] = ' 등록된 배지가 없습니다.';
$string['badgeimage'] = '배지 이미지';
$string['badgename'] = '이름';
$string['badgetype'] = '유형';
$string['badgecode'] = '코드';
$string['badgeusing'] = '사용여부';
$string['badgeexplanation'] = '설명';
$string['searchholder'] = '배지 이름,코드 검색';
$string['usingtype'] = '는(은) 사용중인 유형입니다.';
$string['nonetype'] = '유형 전체';
$string['all'] = '모두';
$string['png'] = 'png 파일만 업로드 해주세요';

//수강후기 관리
$string['review:title'] = '수강후기 관리';
$string['review:title2'] = '수강후기';
$string['isopened'] = '공개여부';
$string['opened'] = '공개';
$string['notopened'] = '비공개';
$string['courseproduct'] = '강좌 상품';
$string['courseproductview'] = '강좌 상품별 보기';
$string['allview'] = '전체보기';
$string['review:searchtext'] = '강좌 상품명, 상품코드 검색';
$string['review:searchtext2'] = '내용, 작성자명 검색';
$string['review:timecreated'] = '작성일';
$string['review:timemodified'] = '수정일';
$string['review:course'] = '강좌 상품명';
$string['review:grade'] = '평점';
$string['review:go'] = '강의실 수강후기로 이동';
$string['review:reviewcontents'] = '수강후기 내용';
$string['review:scope'] = '별점';
$string['review:writer'] = '작성자';
$string['review:text'] = '수강후기 공개여부를 변경하시겠습니까?';

$string['sitename:mk'] = 'Master Korean - 대한민국 최고의 교육 기관인 Visang에서';
$string['sitename:mk_description'] = "Master Korean! Master Your Job! 초보자, 초보자, 중급, 토픽 준비 및 비즈니스 커뮤니케이션을위한 다양한 온라인 한국어 코스를 제공합니다.";
$string['sitename:mk_jobs'] = 'Master Korean jobs';

$string['okmedia:play_confirm'] = '이전 학습구간 시점부터 이어보시겠습니까?';
$string['okmedia:play_progress'] = '진도율';
$string['okmedia:play_stop'] = '학습 종료';

$string['lecturecnt'] = '강의수';

$string['gross_sales'] = '총매출액(VND)';
$string['total_sum'] = '합계';
$string['paymentprice'] = '총매출액(VND)';

$string['certificate:title'] = '이수증 관리';
$string['certificate:confirm'] = '이수증 확인';
$string['certificate:submit'] = '이수증 재발급';
$string['certificate:count'] = '이수증 발급인원';
$string['certificate:usercount'] = '이수증 발급 회원수';
$string['certificate:rp'] = '일반 회원';
$string['certificate:bp'] = 'BOP 회원';
//coupon
$string['coupon_manage']='Coupon management';
$string['coupon_list']='List of coupon';
$string['error_list']='Error Reports';
$string['cp_no']='No';
$string['cp_title']='Title';
$string['cp_img']='Image';
$string['cp_course']='Course';
$string['cp_dr']='Discount Rate';
$string['cp_dis']='Description';
$string['cp_renderqr']='Render QR Code';
$string['cp_qty']='Quantity';
$string['cp_crdate']='Created Date';
$string['cp_sent']='Sent';
$string['cp_sentdate']='Sent Date';
$string['cp_expried']='Expired';
$string['cp_exprieddate']='Expired Date';
$string['pagi_prev']='PREV';
$string['pagi_next']='NEXT';
$string['rc_manage']='쿠폰 받는 사람 ';
$string['rc_email']='Email';
$string['rc_rcdate']='Received Date';
$string['rc_code']='Subscription Code';
$string['rc_dowloadpdf']='Dowload PDF';
$string['rc_used']='Used';
$string['rc_useddate']='Used Date';
$string['err_coupon']='Coupon';
$string['err_coupontype']='Type';
$string['err_couponerror']='Error';
$string['err_coupondate']='Date';
$string['err_couponaction']='Action';
$string['create_coupon']='Generate Coupon';
$string['send_coupon']='Send Coupon by email';
$string['receivers']='RECEIVERS';
$string['notmember']='Not Members';
$string['member']='Members';
$string['save']='Save';
$string['cancel']='Cancel';
$string['next']='Next';
$string['send']='Send';
$string['emailbody']='Email body';
$string['select-date']='Select a date';
$string['choose-file']='Choose file';
$string['qryes']='YES';
$string['qrno']='NO';
$string['startdate']='Start Date';
$string['expiredate']='Expire Date';
$string['cp_actions']='Actions';
$string['cp_nodata']='Don\'t have data';
$string['confirmdelete']='이 할인 코드를 삭제하시겠습니까?';
$string['succdel']='쿠폰 1개 삭제됨';
$string['faildel']='이 쿠폰은 삭제할 수 없습니다';
$string['confirmdelete1']='이 오류를 삭제하시겠습니까?';
$string['succdel1']='삭제된 오류 1개';
$string['faildel1']='이 오류는 삭제할 수 없습니다';

$string['user:gender'] = '성별';
$string['user:male'] = '남성';
$string['user:female'] = '여자';
$string['user:login_number'] = '로그인 횟수';