<?php
require_once(__DIR__ . '/../../../config.php');
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/local/popup/lib.php';
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
$pagesettings = array(
    'title' => get_string('copyset', 'local_management'),
    'heading' =>  get_string('copyset', 'local_management'),
    'subheading' => '',
    'menu' => 'copyright',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('copyright_list');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT); // $id 있으면 상세페이지
$type = optional_param('type', 0, PARAM_INT); 

$PAGE->set_context($context);

$totalcount = $DB->count_records_select('lmsdata_copyright',array());


include_once($CFG->dirroot . '/local/management/header.php');
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                    <table cellpadding="0" cellspacing="0" class="normal" width="100%">
                        <caption class="hidden-caption"><?php echo get_string('copyset', 'local_management'); ?></caption>
                        <?php if(!$id){ ?>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="5%">No.</th>
                                <th scope="row" width="10%"><?php  echo get_string('division', 'local_management'); ?></th>
                                <th scope="row" width="35%"><?php echo get_string('name', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('copy:writer', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('timecreated', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('list', 'local_management'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $startnum = $totalcount - (($page - 1) * $perpage);
                            $offset = 0;
                            if ($page != 0) {
                                $offset = ($page - 1) * $perpage;
                            }
                            $sql = "SELECT lc.id,lc.title,lc.titleen,lc.titlevn, lc.timecreated,u.firstname AS username,lc.type FROM {lmsdata_copyright} lc JOIN {user} u on lc.userid = u.id ORDER BY id DESC";
                            $copyrights = $DB->get_records_sql($sql,array(),$offset,$perpage);
                            if($copyrights){
                                foreach($copyrights as $text){ ?>
                                    <tr>
                                        <td class="chkbox"><input type="checkbox" title="check"  name="delchk"/><input class="idval" type="hidden" value="<?php echo $text->id ?>"></td>
                                        <td class="number"><?php echo $startnum; ?></td>
                                        <td><?php echo $text->type ==0 ? get_string('copy', 'local_management') : get_string('publishinghouse', 'local_management'); ?></td>
                                        <td class="title"><a href="./copyright_write.php?id=<?php echo $text->id; ?>"><?php echo $text->title.' / '.$text->titleen.' / '.$text->titlevn ?></a></td>
                                        <td><?php echo $text->username; ?></td> 
                                        <td><?php echo date("Y-m-d", $text->timecreated); ?></td>
                                        <td><input type="button" onclick="document.location.href='/local/management/copyright/copyright_list.php?id=<?php echo $text->id; ?>&type=<?php echo $text->type; ?>'" value='<?php echo get_string('board:move', 'local_management'); ?>'></td>
                                    </tr>
                                <?php
                                $startnum--;
                                }
                            } else {
                                ?>
                                    <tr>
                                        <td colspan = "7"> <?php  echo get_string('copy:nodata', 'local_management'); ?> </td>
                                    </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <?php } else if ($id) { ?>
                            <thead>
                            <tr>
                                <th scope="row" width="5%">No.</th>
                                <th scope="row" width="10%"><?php  echo get_string('division', 'local_management'); ?></th>
                                <th scope="row" width="35%"><?php echo get_string('course', 'local_management'); ?>/<?php echo get_string('book', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('timecreated', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('board:move', 'local_management'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if($type==0){
                                $union ="UNION SELECT cs.id,cs.userid, cs.category,cs.timecreated,lc.title, lc.titleen,lc.titlevn,lc.type, tc.coursename as datatitle, tc.courseid as dataid
                                        FROM {lmsdata_copyright} lc
                                        JOIN {lmsdata_copyright_set} cs on lc.id = cs.copyid
                                        JOIN {lmsdata_course} tc on tc.id =cs.dataid
                                        WHERE lc.id = $id  and cs.category = 'course'";
                            }
                            $sql = "SELECT cs.id,cs.userid, cs.category, cs.timecreated,lc.title, lc.titleen,lc.titlevn,lc.type, tt.title as datatitle,tt.id as dataid
                                    FROM {lmsdata_copyright} lc
                                    JOIN {lmsdata_copyright_set} cs on lc.id = cs.copyid
                                    JOIN {lmsdata_textbook} tt on  tt.id = cs.dataid 
                                    WHERE lc.id = $id and cs.category = 'book'
                                    $union
                                    ORDER BY timecreated DESC";
                            $offset = 0;
                            if ($page != 0) {
                                $offset = ($page - 1) * $perpage;
                            }
                            $copyrights = $DB->get_records_sql($sql,array(),$offset,$perpage);
                            $totalcount = count($copyrights);
                            $startnum = $totalcount - (($page - 1) * $perpage);
                            if($copyrights){
                                foreach($copyrights as $text){ 
                                    $url = $text->category == 'book' ? '/local/management/textbook/form.php?mod=edit&id='.$text->dataid : '/local/management/courses/form.php?id='.$text->dataid;
                                    ?>
                                    <tr>
                                        <td class="number"><?php echo $startnum; ?></td>
                                        <td><?php echo $text->type ==0 ? get_string('copy', 'local_management') : get_string('publishinghouse', 'local_management'); ?></td>
                                        <td class="title"><?php echo $text->datatitle; ?></td>
                                        <td><?php echo date("Y-m-d", $text->timecreated); ?></td>
                                        <td><input type="button" onclick="document.location.href='<?php echo $url; ?>'" value='<?php echo get_string('board:move', 'local_management'); ?>'></td>
                                    </tr>
                                <?php
                                $startnum--;
                                }
                            } else {
                                ?>
                                    <tr>
                                        <td colspan = "5">  <?php  echo get_string('copy:nodata', 'local_management'); ?> </td>
                                    </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <?php } ?>
                    </table>
                     <div id="btn_area">
                         <?php if(!$id){
                             ?>
                            <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/copyright/copyright_write.php"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                            <input type="button" id="copyright_delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left;"/>
                             <?php
                         } else if($id) {?>
                            <input type="button"onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/copyright/copyright_list.php"' class="red_btn" value="목록" style="float: left;"/>
                         <?php } ?>
                    </div>
            <div class="pagination">
            <?php
            print_paging_navbar($totalcount, $page, $perpage, $CFG->wwwroot . '/local/management/copyright/copyright_list.php', array('id'=>$id,'type'=>$type));
            ?>
            </div><!-- Pagination End -->
            </div>
                
        </div>  
    </div>
 </section>


<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
    
     function muldel() {
          var ids = [];
            $("input[name=delchk]:checked").each(function (index) {
                ids[index] = $(this).siblings(".idval").val();
            });
                    
         $.ajax({
            url: "copyright_delete.php",
            type: 'POST',
            dataType: 'json',
            data: {ids: ids},
            success: function (data) {
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });

    }
    $(function () {
        $("#allcheck").click(function () {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function () {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function () {
                    this.checked = false;
                });
            }
        });

        $("#copyright_delete_button").click(function () {
            if (confirm("<?php echo get_string('job:delalert', 'local_management'); ?>")) {
                muldel();
            }
        });
        
    });
</script>