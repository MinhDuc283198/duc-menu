<?php
 require_once(__DIR__ . '/../../../config.php');
    
    
    require_login(0, false);
    if (isguestuser()) {
        $SESSION->wantsurl = (string)new moodle_url('popup_submit.php');
        redirect(get_login_url());
    }
    $context = context_system::instance();
    //require_capability('moodle/site:config', $context);
    
    $id = optional_param('id', 0, PARAM_INT);
    $title_ko = optional_param('title_ko', "", PARAM_TEXT);
    $title_en = optional_param('title_en', "", PARAM_TEXT);
    $title_vn = optional_param('title_bn', "", PARAM_TEXT);
    $divide = optional_param('divide', 0, PARAM_INT);
    
    $values = new stdClass();
    $values->title = $title_ko;
    $values->titleen = $title_en;
    $values->titlevn = $title_vn;
    $values->type = $divide;
    
    if(!$id){
        $values->timecreated = time();
        $values->userid = $USER->id;
        $DB->insert_record('lmsdata_copyright',$values);
    } else {
        $values->id = $id;
        $DB->update_record('lmsdata_copyright',$values);
    }
    
    
    
    
?>
<script>
    window.onload = function(){
        location.href = '<?php echo $CFG->wwwroot;?>/local/management/copyright/copyright_list.php'
    }
</script>