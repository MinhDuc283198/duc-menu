<?php
/**
 * 과정관리 리스트 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('certificate:title', 'local_management'),
    'heading' => get_string('certificate:title', 'local_management'),
    'subheading' => '',
    'menu' => 'certificateNo',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$certino = $DB->get_record_sql("select code from m_lmsdata_certificateno");
$certinolist = explode("-", $certino->code);
$certinocount = count($certinolist);
?>

<?php include_once($CFG->dirroot . '/local/management/header.php'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <span>사용중인 이수증 번호</span>
                <input type="text" title="certificateNn" value="<?php echo $certino->code;?>-<?php echo date(Ymd)?>-B1234" class="search-text w100 text-center" readonly>
                <input type="button" class="blue_btn" style="margin-right: 10px;" value="새로고침" onclick="location.reload();" /> 
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="이수증예제" onclick="window.open('/local/mypage/coursecompletemopopup.php?id=2');"/> 
                <form id="certificate_form" action="certificate_submit.php">
                <table id="myTable">
                    <thead>
                        <tr>
                            <th scope="row" width="80px"><?php echo get_string('number', 'local_management') ?></th>
                            <th><?php echo get_string('code', 'local_management') ?></th>             
                        </tr>
                    </thead>
                    
                    <tbody id="tbody">   
                    <?php
                        $startnum = 0;
                        foreach ($certinolist as $code) {
                            $startnum++;                                                 
                    ?>                     
                        <tr>
                            <td><?php echo $startnum;?></td>
                            <td><input type="text" title="Code" name="code[]" value="<?php echo $code;?>" class="search-text w100 text-center"></td>                                        
                        </tr>
                    <?php } ?>  
                    </tbody>
                </table>
                </form><!--Table End-->
                <input type="hidden" value="<?php echo $startnum?>" id="startnum">
                <div id="btn_area">
                    <div style="float:right;"> 
                        <button type="button" class="blue_btn" id="btn-add-row">구분추가</button>
                        <button type="button" class="blue_btn" id="btn-delete-row">구분삭제</button>
                        <input type="submit" class="blue_btn" id="submit" style="margin-right: 10px;" value="<?php echo get_string('save', 'local_management'); ?>"/> 
                    </div>
                </div>             
            </div><!--Content End-->    
        </div> <!--Contents End-->
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script type="text/javascript">
    $(document).ready(function () {   
        $('#submit').click(function () {
            $("form[id=certificate_form]").submit();
        });        
    });

    $('#btn-add-row').click(function() {
        var startnum = $('#startnum').val();
        startnum++;
        var insertTr = '';  
        insertTr += '<tr>';
        insertTr += '<td>'+startnum+'</td>';       
        insertTr += '<td><input type="text" title="Code" name="code[]" value="" class="search-text w100 text-center"></td>';       
        insertTr += '</tr>';
        $('#startnum').val(startnum);
        $("#myTable > tbody:last").append(insertTr);
    });

    $('#btn-delete-row').click(function() {
        var startnum = $('#startnum').val();        
        startnum--;
        $("#myTable > tbody:last > tr:last").remove();   
        $('#startnum').val(startnum);
    });

</script>    
