<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");

$id = optional_param("id", 0, PARAM_INT);
$courseid = optional_param("courseid", 0, PARAM_INT);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);

if ($searchtext) {
    $sql_where= " and (mu.username like '%" . $searchtext . "%' or mu.firstname like '%". $searchtext . "%')";
    $params['searchtext'] = $searchtext;
}

$sql_select = 'select mu.username, mu.firstname, mu.phone1, mu.phone2, lp.userid';
$sql = " from m_lmsdata_payment lp
        JOIN m_user mu ON mu.id = lp.userid
        JOIN m_lmsdata_class lc ON lc.id = lp.ref     
        WHERE lp.ref = $id and lp.status = 1 order by lp.id desc" ;

$courses = $DB->get_records_sql($sql_select . $sql , $params);
$total = $DB->count_records_sql('select count(*)' . $sql, $params);

$date = date('Y-m-d', time());
$filename = '이수증관리_' . $date . '.xls';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);

$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');
$col = 0;
foreach ($fields as $fieldname) {
    $worksheet[0]->write(0, $col, $fieldname);
    $col++;
}

$row = 1;
$num = 1;
foreach ($courses as $course) {
    $col = 0;

    $getprogress = $DB->get_record('course_completions', array('course' => $courseid, 'userid'=>$course->userid)); 
    $certificate = $DB->get_record('lmsdata_certificate', array('courseid' => $courseid, 'userid'=>$course->userid)); 
    if($getprogress->progress >= 100) { 
        $getprogress->progress = "수료" ;
    }else{
        $getprogress->progress = number_format($getprogress->progress)."%" ;
    }

    $worksheet[0]->write($row, $col++, $certificate->certificatenumber);
    $worksheet[0]->write($row, $col++, $course->username);
    $worksheet[0]->write($row, $col++, $course->firstname);
    $worksheet[0]->write($row, $col++, $course->phone2);
    $worksheet[0]->write($row, $col++, $course->phone1);
    $worksheet[0]->write($row, $col++, $getprogress->progress);

    $row++;
}

$workbook->close();
die;
