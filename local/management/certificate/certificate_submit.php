<?php
/**
 *  수정 submit
 */
require_once(__DIR__ . '/../../../config.php');
$context = context_system::instance();

require_login();

$code = required_param('code', PARAM_RAW);

$certificate = new stdClass();

foreach ($code as $codeno) {
    if($codeno){
        $certificateno = $certificateno."-".$codeno ;
    }
}
$certificate->code = substr($certificateno, 1);
$certificate->id = 1;

$DB->update_record('lmsdata_certificateno', $certificate);

redirect($CFG->wwwroot.'/local/management/certificate/index.php',get_string('cetificatenomodify','local_mypage'));

