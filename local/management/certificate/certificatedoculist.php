<?php
/**
 * 이수증 관리 리스트 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('certificate:title', 'local_management'),
    'heading' => get_string('certificate:title', 'local_management'),
    'subheading' => '',
    'menu' => 'certificateLi',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
$context = context_system::instance();

$id = optional_param("id", 0, PARAM_INT);
$courseid = optional_param("courseid", 0, PARAM_INT);
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);

if ($searchtext) {
    $sql_where= " and (mu.username like '%" . $searchtext . "%' or mu.firstname like '%". $searchtext . "%')";
    $params['searchtext'] = $searchtext;
}

$orderby = ' order by lp.id desc ';

$sql_select = 'select mu.username, mu.firstname, mu.phone1, mu.phone2, lp.userid';
$sql = " from m_lmsdata_payment lp
        JOIN m_user mu ON mu.id = lp.userid
        JOIN m_lmsdata_class lc ON lc.id = lp.ref     
        WHERE lp.ref = $id and lp.status = 1 $sql_where" ;

$courses = $DB->get_records_sql($sql_select . $sql . $orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql('select count(*)' . $sql, $params);
$perpage_array = [10, 20, 30, 50];

$lclass = $DB->get_record('lmsdata_class', array('courseid' => $courseid));

$getstudent = $DB->count_records_sql("SELECT count(*) FROM m_lmsdata_payment WHERE ref = :id and status = 1", array('id' => $id));
$getcerficate = $DB->count_records_sql("SELECT count(*) FROM m_lmsdata_certificate WHERE courseid = :courseid", array('courseid' => $courseid));

if ($lclass->type == 1) {
    $division = get_string('course', 'local_management');
} else if ($lclass->type == 2) {
    $division = get_string('book', 'local_management');
} else {
    $division = get_string('course', 'local_management') . '+' . get_string('book', 'local_management');
}

$corigincode = '';
if ($lclass->type == 1) {
    $corigincode = $lclass->lcocode;
} else if ($lclass->type == 2) {
    $corigincode = $lclass->ltcode;
} else if ($lclass->type == 3) {
    $corigincode = $lclass->lcocode . '<br>' . $lclass->ltcode;
}

?>

<?php include_once($CFG->dirroot . '/local/management/header.php'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">

            <table>
                    <thead>
                        <tr>
                            <th scope="row" width="110px"><?php echo get_string('division', 'local_management') ?></th> 
                            <th scope="row" ><?php echo get_string('goodsname', 'local_management') ?></th>                                                 
                            <th scope="row" width="10%"><?php echo get_string('goodscode', 'local_management') ?></th>
                            <th scope="row" width="10%"><?php echo get_string('origincode', 'local_management') ?></th>
                            <th scope="row" width="20%"><?php echo get_string('applicationperiod', 'local_management') ?></th>
                            <th width="15%"><?php echo get_string('certificate:count', 'local_management') ?></th>                     
                        </tr>
                    </thead>
                        <tr>
                            <td><?php echo $division;?></td>
                            <td><?php echo $lclass->vi_title . '<br>' . $lclass->title ?></td>
                            <td><?php echo $lclass->code ?></td>
                            <td><?php echo $corigincode ?></td>
                            <td><?php echo date('Y.m.d', $lclass->learningstart) ?> ~ <?php echo date('Y.m.d', $lclass->learningend) ?></td>
                            <td><?php echo $getcerficate;?> / <?php echo $getstudent;?></td>
                        </tr>
                </table>
                <form name="course_search" id="course_search" action="?" method="get">
                <select name="perpage2" class="perpage2">
                    <?php
                    foreach ($perpage_array as $pa) {
                        $selected = '';
                        if ($pa == $perpage) {
                            $selected = 'selected';
                        }
                        ?>
                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                        <?php
                        }
                    ?>
                </select>
                <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="excel_download();"/> 
                <input type="hidden" name="page" value="1" />
                <input type="hidden" name="id" value="<?php echo $id ?>" />
                <input type="hidden" name="courseid" value="<?php echo $courseid ?>" />                  
                <div style="float:right;">            
                    <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('user:searchtext2', 'local_management') ?> "  class="search-text w100"/> 
                    <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>  
                </div>
                </form>
                <table>
                    <thead>
                        <tr>
                            <th width="100">이수증 발급일자</th>                               
                            <th scope="row" >이수증 번호</th>                           
                            <th width="10%"><?php echo get_string('course:id', 'local_management') ?></th>                            
                            <th scope="row" width="12%"><?php echo get_string('name', 'local_management') ?></th>   
                            <th scope="row" width="12%"><?php echo get_string('user:birth', 'local_management') ?></th>                                                 
                            <th scope="row" width="12%"><?php echo get_string('user:phone', 'local_management') ?></th>
                            <th scope="row" width="110">수료여부</th>                        
                            <th scope="row" width="110"><?php echo get_string('certificate:confirm', 'local_management') ?></th>
                            <th scope="row" width="110"><?php echo get_string('certificate:submit', 'local_management') ?></th>                     
                        </tr>
                    </thead>
                    <?php if ($count_courses === 0) { ?>
                        <tr>
                            <td colspan="16"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                        </tr>
                    <?php } else {
                        $startnum = $count_courses - (($currpage - 1) * $perpage);
                        foreach ($courses as $course) {
                        $getprogress = $DB->get_record('course_completions', array('course' => $courseid, 'userid'=>$course->userid)); 
                        $certificate = $DB->get_record('lmsdata_certificate', array('courseid' => $courseid, 'userid'=>$course->userid)); 
                        if($getprogress->progress >= 100) { 
                            $getprogress->progress = "수료" ;
                        }else{
                            $getprogress->progress = number_format($getprogress->progress)."%" ;
                        }
                    ?>
                        <tr>
                            <td><?php if($course->timemodified) { echo date('Y-m-d', $course->timemodified); } else { echo "-"; }?></td>                      
                            <td><?php if(!$certificate->certificatenumber) { echo "미발행";}?><?php echo $certificate->certificatenumber;?><?php if($certificate->reconfirm>0) { echo "<br>[재발행: ".$certificate->reconfirm."]";}?></td>
                            <td><?php echo $course->username;?></td>
                            <td><?php echo $course->firstname;?></td>    
                            <td><?php echo $course->phone2;?></td>   
                            <td><?php echo $course->phone1;?></td>                                                                                               
                            <td><?php echo $getprogress->progress;?></td>                               
                            <td><input type="button" class="normal_btn" value="<?php echo get_string('certificate:confirm', 'local_management') ?>" onclick="window.open('<?php echo $CFG->wwwroot . '/local/mypage/coursecompletepopup.php?id=' . $courseid; ?>')"/></td>
                            <td><input type="button" class="normal_btn" value="<?php echo get_string('certificate:submit', 'local_management') ?>" onclick="window.open('<?php echo $CFG->wwwroot . '/local/mypage/coursecompletemopopup.php?ty=mo&id=' . $courseid; ?>')"/></td>
                        </tr>
                    <?php }
                    }?>    
                </table><!--Table End-->

                <?php
                if (($count_courses / $perpage) > 1) {
                    print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
                }

                $query_string = '';
                if (!empty($excel_params)) {
                    $query_array = array();
                    foreach ($excel_params as $key => $value) {
                        $query_array[] = urlencode($key) . '=' . urlencode($value);
                    }
                    $query_string = '?' . implode('&', $query_array);
                }
                ?>            
            </div><!--Content End-->    
        </div> <!--Contents End-->
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script type="text/javascript">
    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    function excel_download() {
        var form = $('#course_search');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
        return false;
    }
</script>    