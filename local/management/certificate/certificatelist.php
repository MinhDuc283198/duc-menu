<?php
/**
 * 이수증 관리 리스트 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('certificate:title', 'local_management'),
    'heading' => get_string('certificate:title', 'local_management'),
    'subheading' => '',
    'menu' => 'certificateLi',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$type = optional_param_array('type', array(), PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$isused = optional_param_array('isused', array(), PARAM_RAW);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$searchtext = trim($searchtext);


if ($learningstart_str) {
    $learningstart = str_replace(".", "-", $learningstart_str);
    $learningstart = strtotime($learningstart);
}
if ($learningend_str) {
    $learningend = str_replace(".", "-", $learningend_str);
    $learningend = strtotime($learningend);
}
if ($learningstart) {
    $sql_where[] = " a.learningstart <= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if ($learningend) {
    $sql_where[] = " a.learningend >= :learningend ";
    $params['learningend'] = $learningend;
}

if ($type) {
    foreach ($type as $tkey => $tval) {
        $sql_where_type[] = " a.type = :type" . $tkey;
        $params['type' . $tkey] = $tval;
    }
    if (!empty($sql_where_type)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_type) . ')';
    } else {
        $sql_where[] = '';
    }
}
if ($isused) {
    foreach ($isused as $iskey => $isval) {
        $sql_where_isused[] = " a.isused = :isused" . $iskey;
        $params['isused' . $iskey] = $isval;
    }
    if (!empty($sql_where_isused)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($searchtext)) {
    $sql_where_search[] = $DB->sql_like('a.title', ':title');
    $params['title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('a.vi_title', ':vi_title');
    $params['vi_title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('a.en_title', ':en_title');
    $params['en_title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('a.code', ':code');
    $params['code'] = '%' . $searchtext . '%';
    $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
}

if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$orderby = ' order by id desc ';

$sql_select = 'select *';
$sql = " from (select lc.*, lco.coursename as lconame,lco.en_coursename as en_lconame,lco.vi_coursename as vi_lconame, lco.coursecd as lcocode, '' as lttitle,'' as en_lttitle,'' as vi_lttitle, '' as ltcode, mc.id as mcid   

from m_lmsdata_class lc
JOIN m_course mc ON lc.courseid = mc.id
JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
JOIN m_course mc2 ON lco.courseid = mc2.id
JOIN m_course_categories ca ON ca.id = mc2.category
join m_enrol e on e.courseid = lco.courseid
join m_user_enrolments ue on ue.enrolid = e.id
left JOIN m_course_categories ca2 ON ca.parent = ca2.id
WHERE lc.type = 1 
UNION
select lc.* , '' as lconame,'' as en_lconame, '' as vi_lconame, '' as lcocode, lt.title as lttitle,lt.en_title as en_lttitle,lt.vi_title as vi_lttitle, lt.code as ltcode, '' as mcid   
from m_lmsdata_class lc

JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
WHERE lc.type = 2 
UNION
select lc.* , lco.coursename as lconame,lco.en_coursename as en_lconame,lco.vi_coursename as vi_lconame, lco.coursecd as lcocode, lt.title as lttitle,lt.en_title as en_lttitle,lt.vi_title as vi_lttitle, lt.code as ltcode, mc.id as mcid   

from m_lmsdata_class lc
JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
JOIN m_course mc ON lc.courseid = mc.id
JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
JOIN m_course mc2 ON lco.courseid = mc2.id
JOIN m_course_categories ca ON ca.id = mc2.category
join m_enrol e on e.courseid = lco.courseid
join m_user_enrolments ue on ue.enrolid = e.id 
left JOIN m_course_categories ca2 ON ca.parent = ca2.id
WHERE lc.type = 3) a $sql_where";

$courses = $DB->get_records_sql($sql_select . $sql . $orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql('select count(*)' . $sql, $params);
$perpage_array = [10, 20, 30, 50];
$isused_array = array(get_string('used', 'local_management'),get_string('notused', 'local_management'));
$type_array = array('1'=>get_string('course', 'local_management'), '2'=>get_string('book', 'local_management'), '3'=>get_string('course', 'local_management').'+'.get_string('book', 'local_management'));
$get_array =explode('.php?' , $_SERVER['REQUEST_URI']);
?>

<?php include_once($CFG->dirroot . '/local/management/header.php'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">

                <form name="course_search" id="course_search" class="search_area" action="?" method="get">
                    <input type="hidden" name="page" value="1" />
                    <input type="hidden" name="perpage" value="<?php echo $perpage ?>" />
                    <input type="hidden" name="progress" value="<?php echo $progress ?>" />                    
                    <div>
                        <label><?php echo get_string('goods:type', 'local_management') ?></label>
                        <?php foreach($type_array as $tkey => $tval){?>
                        <label class="mg-bt10"><input type="checkbox" name="type[]" value="<?php echo $tkey?>" <?php foreach($type as $ischeck){if($ischeck == $tkey){echo "checked";}}?>><?php echo $tval ?></label>  
                        <?php }?>
                    </div>
                    <div>
                        <label><?php echo get_string('applicationperiod', 'local_management') ?></label>
                        <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> " > 
                        <span class="dash">~</span>
                        <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> " > 
                    </div>
                    <div>
                        <label><?php echo get_string('isused', 'local_management') ?> </label>
                        <?php foreach($isused_array as $iakey => $iaval){?>
                        <label class="mg-bt10"><input type="checkbox" name="isused[]" value="<?php echo $iakey?>" <?php foreach($isused as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                        <?php }?>
                    </div>

                    <div>
                        <label><?php echo get_string('search', 'local_management') ?> </label>
                        <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('goods:searchtext2', 'local_management') ?> "  class="search-text w100"/>
                        <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>     
                        <input type="button" class="" value="<?php echo get_string('reset', 'local_management') ?> " id="reset"/>   
                    </div>
                </form><!--Search Area2 End-->

                <span><?php echo get_string('total', 'local_management', $count_courses) ?></span>
                <select name="perpage2" class="perpage2">
                    <?php
                    foreach ($perpage_array as $pa) {
                        $selected = '';
                        if ($pa == $perpage) {
                            $selected = 'selected';
                        }
                        ?>
                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                            <?php
                        }
                    ?>
                </select>

                <!--<input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="textbook_excel_down();" />-->
                <input type="button" class="" value="강좌"> <input type="button" class="" value="상품">
                <table>
                    <thead>
                        <tr>
                            <th scope="row" width="70px" rowspan="2"><?php echo get_string('number', 'local_management') ?></th>
                            <th scope="row" width="8%" rowspan="2"><?php echo get_string('division', 'local_management') ?></th>   
                            <th scope="row" width="25%" rowspan="2"><?php echo get_string('goodsname', 'local_management') ?></th>                                                 
                            <th scope="row" width="10%" rowspan="2"><?php echo get_string('goodscode', 'local_management') ?></th>
                            <th scope="row" width="10%" rowspan="2"><?php echo get_string('origincode', 'local_management') ?></th>
                            <th scope="row" width="20%" rowspan="2">과정기간<br>(과정운영기간)</th>
                            <th rowspan="2"><?php echo get_string('certificate:count', 'local_management') ?></th>   
                            <th scope="row" colspan="2" ><?php echo get_string('certificate:usercount', 'local_management') ?></th>                                            
                            <th width="120px" rowspan="2"><?php echo get_string('classroomentry', 'local_management') ?></th>
                        </tr>
                        <tr>                                            
                            <th scope="row" width="10%">일반 회원</th>
                            <th scope="row" width="10%">BOP 회원</th>
                        </tr>
                                               
                    </thead>
                    <?php if ($count_courses === 0) { ?>
                        <tr>
                            <td colspan="16"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                        </tr>
                        <?php
                    } else {
                        $startnum = $count_courses - (($currpage - 1) * $perpage);
                        foreach ($courses as $course) {
                            ?>
                            <tr>
                                <td><?php echo $startnum--; ?></td>
                                <td>
                                <?php
                                if ($course->type == 1) {
                                    echo get_string('course', 'local_management');
                                } else if ($course->type == 2) {
                                    echo get_string('book', 'local_management');
                                } else {
                                    echo get_string('course', 'local_management') . '+' . get_string('book', 'local_management');
                                }
                                ?>
                                </td>
                                <td><a href="certificatedoculist.php?id=<?php echo $course->id ?>&courseid=<?php echo $course->courseid ?>"><?php echo $course->vi_title . '<br>' . $course->title ?></a></td>
                                <td><?php echo $course->code ?></td>
                                <?php
                                $origincode = '';
                                if ($course->type == 1) {
                                    $origincode = $course->lcocode;
                                } else if ($course->type == 2) {
                                    $origincode = $course->ltcode;
                                } else if ($course->type == 3) {
                                    //$originname = $course->lconame.'<br>'.$course->lttitle;
                                    $origincode = $course->lcocode . '<br>' . $course->ltcode;
                                }
                                ?>
                                <td><?php echo $origincode ?></td>
                                <td><?php echo date('Y.m.d', $course->learningstart) ?> <br/> ~ <?php echo date('Y.m.d', $course->learningend) ?></td>
                                <?php                    
                                    $getstudentsql = "SELECT count(*) FROM m_lmsdata_payment WHERE ref = :id and status = 1";
                                    $getstudent = $DB->count_records_sql($getstudentsql, array('id' => $course->id));
                                    $getcerficatesql = "SELECT count(*) FROM m_lmsdata_certificate WHERE courseid = :courseid";
                                    $getcerficate = $DB->count_records_sql($getcerficatesql, array('courseid' => $course->courseid));
                                    $getcerficaterssql = "SELECT count(*) FROM m_lmsdata_certificate WHERE courseid = :courseid and usergroup = 'rs'";                                    
                                    $getstudentrs = $DB->count_records_sql($getcerficaterssql, array('courseid' => $course->courseid));
                                    $getcerficatebpsql = "SELECT count(*) FROM m_lmsdata_certificate WHERE courseid = :courseid and usergroup = 'bp'";                                    
                                    $getstudentbp = $DB->count_records_sql($getcerficatebpsql, array('courseid' => $course->courseid));                                    
                                ?>
                                <td><a href="certificatedoculist.php?id=<?php echo $course->id ?>&courseid=<?php echo $course->courseid ?>"><?php echo $getcerficate;?></a >/ <?php echo $getstudent;?></td>
                                <td><a href="certificatedoculist.php?id=<?php echo $course->id ?>&courseid=<?php echo $course->courseid ?>"><?php echo $getstudentrs;?></a></td>
                                <td><a href="certificatedoculist.php?id=<?php echo $course->id ?>&courseid=<?php echo $course->courseid ?>"><?php echo $getstudentbp;?></a></td>
                              <td><?php if (($course->type != 2)) { ?><input type="button" class="normal_btn" value="<?php echo get_string('classroomentry', 'local_management') ?>" onclick="window.open('<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->courseid; ?>')"/><?php } else {
                            echo '-';
                        } ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>    
                </table><!--Table End-->

                <?php
                if (($count_courses / $perpage) > 1) {
                    print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
                }

                $query_string = '';
                if (!empty($excel_params)) {
                    $query_array = array();
                    foreach ($excel_params as $key => $value) {
                        $query_array[] = urlencode($key) . '=' . urlencode($value);
                    }
                    $query_string = '?' . implode('&', $query_array);
                }
                ?>            
            </div><!--Content End-->    
        </div> <!--Contents End-->
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script type="text/javascript">
    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    $(document).ready(function () {
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=course_search]").submit();
        });        
        $('#reset').click(function () {
            $('input[name=learningstart]').val('');
            $('input[name=learningend]').val('');
            $('input[name=searchtext]').val('');
            $("input:checkbox[name='type[]']").attr("checked", false);
            $("input:checkbox[name='isused[]']").attr("checked", false);
            $("form[id=course_search]").submit();
        });
        $(".progres").change(function () {
            var progress = $('select[name=progress]').val();
            $('input[name=progress]').val(progress);
            $("form[id=course_search]").submit();
        });          
    });

</script>    
