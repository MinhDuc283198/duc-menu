<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Scheduled task admin pages.
 *
 * @package    tool_task
 * @copyright  2013 Damyon Wiese <damyon@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$PAGE->set_url('/local/management/task/edit_task_send_list_job.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$strheading = get_string('scheduledtasks', 'tool_task');
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);
$pagesettings = array(
    'title' => $strheading,
    'heading' => $strheading,
    'subheading' => '',
    'menu' => 'sendjobmenu',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);
require_login();

require_capability('moodle/site:config', context_system::instance());

$renderer = $PAGE->get_renderer('tool_task');

$action = optional_param('action', 'edit', PARAM_ALPHAEXT);
$taskname = "local_job\\task\\cron_task";
$task = null;
$mform = null;

if ($taskname) {
    $task = \core\task\manager::get_scheduled_task($taskname);
    if (!$task) {
        print_error('invaliddata');
    }
    $taskrate=$DB->get_record('vi_rate_send_email_job', array('id'=>1));
}

if ($action == 'edit') {
    $PAGE->navbar->add(get_string('edittaskschedule', 'tool_task', $task->get_name()));
}

if ($task) {
    $mform = new local_management_edit_task_form(null, $task);
}

if ($mform && ($mform->is_cancelled() || !empty($CFG->preventscheduledtaskchanges))) {
    redirect(new moodle_url('/local/management/task/edit_task_send_list_job.php'));
} else if ($action == 'edit' && empty($CFG->preventscheduledtaskchanges)) {

    if ($data = $mform->get_data()) {


        if ($data->resettodefaults) {
            $defaulttask = \core\task\manager::get_default_scheduled_task($taskname);
            $task->set_minute($defaulttask->get_minute());
            $task->set_hour($defaulttask->get_hour());
            $task->set_month($defaulttask->get_month());
            $task->set_day_of_week($defaulttask->get_day_of_week());
            $task->set_day($defaulttask->get_day());
            $task->set_disabled($defaulttask->get_disabled());
            $task->set_customised(false);
            $taskrate->matchingrate=60;
            $DB->update_record('vi_rate_send_email_job',$taskrate);
        } else {
            $task->set_minute($data->minute);
            $task->set_hour($data->hour);
            $task->set_month($data->month);
            $task->set_day_of_week($data->dayofweek);
            $task->set_day($data->day);
            $task->set_disabled($data->disabled);
            $task->set_customised(true);
            $taskrate->matchingrate=$data->matchingrate;
            $DB->update_record('vi_rate_send_email_job',$taskrate);
        }

        try {
            \core\task\manager::configure_scheduled_task($task);
            redirect($PAGE->url, get_string('changessaved'), null, \core\output\notification::NOTIFY_SUCCESS);
        } catch (Exception $e) {
            redirect($PAGE->url, $e->getMessage(), null, \core\output\notification::NOTIFY_ERROR);
        }
    } else {
        include_once($CFG->dirroot.'/local/management/header.php');
        echo $OUTPUT->heading(get_string('edittaskschedule', 'tool_task', $task->get_name()));
        $mform->display();
        include_once($CFG->dirroot.'/local/management/footer.php');
    }

}
