<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '동기화 설정',
    'heading' => '동기화 설정',
    'subheading' => '',
    'menu' => 'sync_setting',
    'js' => array('/local/management/sync/setting.js'),
    'css' => array(),
    'nav_bar' => array()
);

$confirm = optional_param('confirm', 0, PARAM_INT);
if($confirm) {
    confirm_sesskey();
    
    $php = optional_param('php', '', PARAM_RAW);
    set_config('php', $php, 'local_sync');
    $username = optional_param('username', '', PARAM_RAW);
    set_config('username', $username, 'local_sync');
    $password = optional_param('password', '', PARAM_RAW);
    set_config('password', $password, 'local_sync');
}

$config = get_config('local_sync');

include_once($CFG->dirroot . '/local/management/header.php');
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">설정</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?php if($confirm) { ?>
                    <div class="alert alert-info alert-dismissible show" role="alert">
                        저장되었습니다.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php } ?>
                    <form id="frmSetting" role="form" method="POST">
                        <input type="hidden" name="confirm" value="1" />
                        <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>" />
                        <div class="form-group">
                            <label for="php" class="control-label">PHP 경로</label>
                            <input type="text" class="form-control" name="php" value="<?php echo $config->php; ?>" placeholder="/usr/bin/php">
                        </div>
                        <div class="form-group">
                            <label for="username" class="control-label">계정</label>
                            <input type="text" class="form-control" name="username" value="<?php echo $config->username; ?>" placeholder="admin">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">비밀번호</label>
                            <div>
                            <input type="password" class="form-control" name="password" value="<?php echo $config->password; ?>" placeholder="1234">
                            <input type="checkbox" value="1" id="toggle_password"/> 비밀번호 보기
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnSave" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
                </div>
            </div>
        </div>
    
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">curl을 사용한 동기화 방법</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    명령줄에서 다음과 같이 실행한다.
                    
                    <div class="cmd">
                        curl --data "sync=<span class="param">{sync}</span>&crsn=<span class="param">{crsn}</span>&async=<span class="param">{async}</span>" -X POST --digest --user <span class="param">{username}</span>:<span class="param">{password}</span> <?php echo $CFG->wwwroot; ?>/local/sync/exec.php
                    </div>
                    
                    <div class="parameters">
                        <dl>
                            <dt>{sync}</dt>
                            <dd>동기화 대상, <span class="required">필수 사항</span>
                                <ul>
                                    <li>user : 사용자</li>
                                    <li>course : 과정/차수</li>
                                    <li>prof : PO/강사</li>
                                    <li>enrol : 수강생</li>
                                    <li>changeenrol : 수강 취소/변경</li>
                                    <li>group : 반/팀</li>
                                    <li>lecture : 과목</li>
                                </ul>
                            </dd>
                            <dt>{username}</dt>
                            <dd>
                                계정, <span class="required">필수 사항</span>
                            </dd>
                            <dt>{password}</dt>
                            <dd>
                                비밀번호, <span class="required">필수 사항</span>
                            </dd>
                            <dt>{crsn}</dt>
                            <dd>
                                과정 운영 일정 번호(LGAPORTAL.dbo.T_CRS_MGM_SCH_BSE_M -> CRS_MGM_SCH_N)<br/>
                                지정하지 않을 경우 전체를 동기화<br/>
                                사용자(user), 과정/차수(course)를 동기화 할 경우 이 값을 지정하면 안됨
                            </dd>
                            <dt>{async}</dt>
                            <dd>
                                비동기 여부<br/>
                                비동기방식으로 실행할 경우 true로 설정<br/>
                                비동기방식은 에러 코드를 받을 수 없음
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include_once($CFG->dirroot . '/local/management/footer.php');