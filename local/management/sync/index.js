function locam_management_sync_alert(msg) {
    $('#alertModal .modal-body').html(msg);
    $('#alertModal').modal('show');
}

function locam_management_sync_exec() {
    loading.show();
    
    var username = $('input[name=username]').val();
    if($.trim(username).length == 0) {
        locam_management_sync_alert("계정을 입력하세요.");
        loading.hide();
        return false;
    }

    var password = $('input[name=password]').val();
    if($.trim(password).length == 0) {
        locam_management_sync_alert("비밀번호를 입력하세요.");
        loading.hide();
        return false;
    }

    var sync = $("select[name=sync] option:selected").val();
    if(sync == 0) {
        locam_management_sync_alert("동기화를 선택하세요.");
        loading.hide();
        return false;
    }

    var data = {};
    data.sync = sync;
    if(sync != 'user' && sync != 'course') {
        data.crsn =  $('input[name=crsn]').val();
    }
    data.async = $('input:checkbox[name=async]').is(':checked');

    var url = $('form#frmExec').attr('action');
    DigestAjax.postDigest(url, {
        username: username,
        password: password,
        data: data
    }).done(function(data, textStatus, jqXHR) {
        if(data.status == 0) {
            locam_management_sync_alert("동기화를 완료했습니다.");
            loading.hide();
        } else {
            locam_management_sync_alert('오류! ' + data.output);
            loading.hide();
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        locam_management_sync_alert('오류! ' + jqXHR.responseJSON.output);
        loading.hide();
    });
}

$(document).ready(function() {
    $('form#frmExec').submit(function(e) {
        e.preventDefault();
        locam_management_sync_exec();
        return false;
    });
    $('#btnExec').click(function() {
        $('form#frmExec').submit();
    });
});
