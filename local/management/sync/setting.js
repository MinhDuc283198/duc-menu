function local_management_sync_toggle_password() {
    var x = $('input[name=password]')[0];
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

$(document).ready(function() {
    $('#btnSave').click(function() {
        $('form#frmSetting').submit();
    });
    
    $('#toggle_password').change(function() {
        local_management_sync_toggle_password();
    });
});
