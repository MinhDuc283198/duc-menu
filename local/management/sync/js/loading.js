(function ($) {
    loading = {
        show: function () {
            if ($("#loading").length == 0) {
                $("body").append("<div id='loading'></div>");
            }
        },
        hide: function () {
            if ($("#loading").length != 0) {
                $("#loading").remove();
            }
        }
    }
})(jQuery);