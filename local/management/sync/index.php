<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '동기화 수동 실행',
    'heading' => '동기화 수동 실행',
    'subheading' => '',
    'menu' => 'sync_index',
    'js' => array(
        '/local/management/sync/digest-ajax.min.js',
        '/local/management/sync/index.js'),
    'css' => array(),
    'nav_bar' => array()
);

include_once($CFG->dirroot . '/local/management/header.php');
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">실행</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?php if($confirm) { ?>
                    <div class="alert alert-info alert-dismissible"><span>저장되었습니다.</span></div>
                    <?php } ?>
                    <form id="frmExec" role="form" method="POST" action="<?php echo $CFG->wwwroot . '/local/sync/exec.php'; ?>">
                        <div class="form-group">
                            <label for="username" class="control-label">계정 <span class="required">*</span></label>
                            <input type="text" class="form-control" name="username" value="" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">비밀번호 <span class="required">*</span></label>
                            <input type="password" class="form-control" name="password" value="" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label for="sync" class="control-label">동기화 <span class="required">*</span></label>
                            <select class="form-control" name="sync" >
                                <option value="0">선택</option>
                                <option value="user">사용자</option>
                                <option value="course">과정/차수</option>
                                <option value="prof">PO/강사</option>
                                <option value="enrol">수강생</option>
                                <option value="changeenrol">수강 취소/변경</option>
                                <option value="group">반/팀</option>
                                <option value="lecture">과목</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="crsn" class="control-label">과정 운영 일정 번호(CRS_MGM_SCH_N)</label>
                            <input type="text" class="form-control" name="crsn" value="" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                <input type="checkbox" name="async" value="1" /> 비동기
                            </label>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnExec" class="btn btn-primary"><i class="fa fa-execute"></i> 실행</button>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title">&nbsp;</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<?php
include_once($CFG->dirroot . '/local/management/footer.php');