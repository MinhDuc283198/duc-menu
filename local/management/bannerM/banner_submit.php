<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once dirname(__FILE__) .'/lib.php';

if (!isloggedin()) {
    echo '<script type="text/javascript">
            alert("로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.");
            document.location.href="' . $SITECFG->wwwroot . '/main/user/login.php";
        </script>';
}

$id = optional_param('id', 0, PARAM_INT);
$name = required_param('name', PARAM_TEXT);
$mod = optional_param('mod', "", PARAM_TEXT);
$linkurl = optional_param('linkurl',"",PARAM_TEXT);
$file_id = optional_param('file_id', 0, PARAM_INT);
$file_del = optional_param('file_del', 0, PARAM_INT);
$file_id_mo = optional_param('file_id_mo', 0, PARAM_INT);
$file_del_mo = optional_param('file_del_mo', 0, PARAM_INT);
$isused = optional_param('isused', 0, PARAM_INT);
$order = optional_param('order', 0, PARAM_INT);
$site = optional_param('site', 0, PARAM_INT);
$search = optional_param('search', 0, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);
$reger = optional_param('reger', 0, PARAM_INT); // 예약글 여부 . 1 = 예약글
$startdate = optional_param('startdate', "", PARAM_TEXT);
$enddate = optional_param('enddate', "", PARAM_TEXT);
$colorcode = optional_param('colorcode', "000000", PARAM_TEXT);

$thistime = time();
$newdata = new object();
$newdata->id = $id;
$newdata->name = $name;
$newdata->linkurl =$linkurl;
$newdata->category = $category;
$newdata->isused = $isused;
$newdata->site = $site;
$newdata->orderby = $order;
$newdata->search = $search;
if(empty($colorcode)){
    $colorcode = "ffffff";
}

$newdata->colorcode = $colorcode;
$newdata->target = 0;
$message = "";
if($category==1&&$isused==1){ //하단배너&사용함으로 설정한경우
    if($reger){ // 예약게시기간 있는경우
         $chk = $DB->get_records_sql('SELECT * FROM {lmsdata_banner} WHERE isused = 1  AND category = 1 AND id != :id AND site = :site',array('id'=>$id,'site'=>$site));
         $test = strtotime($startdate);
         $test1 = strtotime($enddate);
         if($chk){
             foreach($chk as $c){
                 if($c->startdate <= $test && $test <= $c->enddate){ // 현재 시작일이 기존 시작일보다 크고  & 기존 종료일보다 작다 
                     $newdata->isused = 0;
                     $newdata->startdate = 0;
                     $newdata->enddate = 0;
                     $reger = 0;
                     $message = "해당 기간에 예약(사용)중인 하단배너가 있습니다. 하단배너는 최대 1개까지 사용 할 수 있습니다. 리스트에서 확인해주세요.";
                 } else if ($c->startdate <= $test1 && $test1 <= $c->enddate){ //현재 종료일이 기존 시작일보다 크고  & 기존 종료일보다 작다 
                     $newdata->isused = 0;
                     $newdata->startdate = 0;
                     $newdata->enddate = 0;
                     $reger = 0;
                     $message = "해당 기간에 예약(사용)중인 하단배너가 있습니다. 하단배너는 최대 1개까지 사용 할 수 있습니다. 리스트에서 확인해주세요.";
                 }
             }
         }
        
    } else { //예약게시기간 없는경우 -> 바로게시 
        $chk = $DB->get_records_sql('SELECT * FROM {lmsdata_banner} WHERE isused = 1  AND category = 1 AND site = :site  AND id != :id AND ((startdate=0)OR(startdate < :thistime ) and ( :thistime2 < enddate))',array('thistime'=>$thistime,'thistime2'=>$thistime,'id'=>$id,'site'=>$site));
        if($chk){
            $newdata->isused = 0;
            $message = "이미 사용중인 하단배너가 있습니다. 하단배너는 최대 1개까지 사용 할 수 있습니다. 리스트에서 확인해주세요.";
        }
    }
}
if($reger){
    $newdata->startdate = strtotime($startdate);
    $newdata->enddate = strtotime("tomorrow", strtotime($enddate)) - 1;
} else {
    $newdata->startdate = 0;
    $newdata->enddate = 0;
}
$context = context_system::instance();
    if ($mod == "edit") {

    $newdata->id = $id;
    $DB->update_record('lmsdata_banner', $newdata);

    $itemid = $id;
} else if ($mod == "add") {

   $temp = new stdClass();
   $newdata->created_at = time();
   $temp= $DB->insert_record('lmsdata_banner', $newdata);

    $id = $temp;
}

if ($file_id != 0) {
    
    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($mod == 'edit' && !($_FILES['uploadfile']['name']==null || $_FILES['uploadfile']['name']=='')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $itemid, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }
 

if (!empty($_FILES['uploadfile']['tmp_name'])) {
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_lmsdata',
                'filearea' => 'banner',
                'itemid' => $id,
                'filepath' => '/',
                'filename' => $_FILES['uploadfile']['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
            );
            $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile']['tmp_name']);
            lmsdata_banner_resize(1600, $context->id, 'local_lmsdata', 'banner', $id);
}}

//-----모바일
if ($file_id_mo != 0) {
    
    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($mod == 'edit' && !($_FILES['uploadfile_mo']['name']==null || $_FILES['uploadfile_mo']['name']=='')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'bannermobi', $itemid, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {           
            if ($file->get_filesize() > 0) {
                if ($file_del) { 
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }
 

if (!empty($_FILES['uploadfile_mo']['tmp_name'])) {
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_lmsdata',
                'filearea' => 'bannermobi',
                'itemid' => $id,
                'filepath' => '/',
                'filename' => $_FILES['uploadfile_mo']['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
            );
            $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile_mo']['tmp_name']);
            lmsdata_banner_resize(750, $context->id, 'local_lmsdata', 'bannermobi', $id);
}}

    if(!empty($message)){
         echo '<script type="text/javascript">
            alert("'.$message.'");
        </script>';

    }
redirect($SITECFG->wwwroot . '/local/management/bannerM/banner.php');