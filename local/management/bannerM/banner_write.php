<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage','local_lmsdata'),
    'heading' => get_string('banner_manage','local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerM',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner_write.php');
    redirect(get_login_url());
}


$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');

$mod = optional_param('mod', "", PARAM_TEXT);
$temp->itemid = $banner->id;



$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form action="banner_submit.php" id="banner_submit" enctype="multipart/form-data" method="POST" onsubmit="return chkfile()">
                    <table>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:name','local_lmsdata');?></td>
                            <td style="text-align:left;" colspan="3"><input type="text" name="name" style="width:70%;"></td>
                        </tr>
                        <tr>
                            <td class="field_title" style="background-color: #F2F2F2"><?php echo get_string('contents_attachments','local_lmsdata');?>(PC)<br />(jpg, png)</td>
                            <td class="field_value number" colspan="3">                                
<?php
if ($temp->itemid) {
    $output = '';
    $cnt = 0;
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $id . '/' . $filename);
        if ($file->get_filesize() > 0) {
            $fileobj[$cnt] = '<input type="hidden" name="file_del" value="0"/><img src="' . $path . '">';
            $fileobj[$cnt] .= "<div style='float:left;' id ='file'><a href=\"$path\">$iconimage</a>";
            $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
            $cnt++;
        }
    }
}
for ($n = 0; $n <= 0; $n++) {
    ?>
                                <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile" id="uploadfile" style=" float:left;" accept=".jpg, .png"/> <span><?php echo get_string('banner:top','local_lmsdata')?> : (1600*379px)<br><?php echo get_string('banner:bot','local_lmsdata')?> : (1600*100px)</span>

    <?php
    if (isset($fileobj[$n])) {
        echo $fileobj[$n];
    }
    echo "</div>";
}
?>
                                    <input type="hidden" class="" name="file_id" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>

                            </td>
                        </tr>
                        <tr>
                            <td class="field_title" style="background-color: #F2F2F2"><?php echo get_string('contents_attachments','local_lmsdata');?>(Mobile)<br />(jpg, png)</td>
                            <td class="field_value number" colspan="3">                       
<?php
if ($temp->itemid) {
    $output = '';
    $cnt = 0;
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/attachment/' . $id . '/' . $filename);
        if ($file->get_filesize() > 0) {
            $fileobj[$cnt] = '<input type="hidden" name="file_del_mo" value="0"/><img src="' . $path . '">';
            $fileobj[$cnt] .= "<div style='float:left;' id ='file'><a href=\"$path\">$iconimage</a>";
            $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context)) . '</div>';
            $cnt++;
        }
    }
}
for ($n = 0; $n <= 0; $n++) {
    ?>
                                    <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile_mo" id="uploadfile_mo" style=" float:left;" accept=".jpg, .png"/><span><?php echo get_string('banner:top','local_lmsdata')?> : (750*580px)<br><?php echo get_string('banner:bot','local_lmsdata')?> : (750*100px)</span>

    <?php
    if (isset($fileobj[$n])) {
        echo $fileobj[$n];
    }
    echo "</div>";
}
?>
                                    <input type="hidden" class="" name="file_id_mo" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/> 
                            </td>
                        </tr>
                         <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:site','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;">
                                <input type="checkbox" name="site" value="0" checked="checked"><?php echo get_string('e-learning','local_lmsdata');?>
                                <input type="checkbox" name="site" value="1"><?php echo get_string('job','local_lmsdata');?>
                            </td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:link','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;"><input type="text" name="linkurl" value="" style="width:70%;"></td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:color','local_lmsdata');?></td>
                             <td colspan="3" style="text-align:left;"><input type="text" id="color" name="colorcode" value=""></td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:num','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;"><input type="text" name="order" value=""></td>
                        </tr>
                         <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:loca','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;">
                                <input type="checkbox" name="category" value="0" checked="checked"><?php echo get_string('banner:top','local_lmsdata');?>&nbsp;&nbsp;&nbsp; 
                                <input type="checkbox" name="category" value="1"><?php echo get_string('banner:bot','local_lmsdata');?>
                            </td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:search','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;">
                                <input type="checkbox" name="search" value="0" checked="checked"><?php echo get_string('unuse','local_lmsdata');?>
                                <input type="checkbox" name="search" value="1"><?php echo get_string('use','local_lmsdata');?>
                            </td>
                        </tr>
                         <tr>
                            <td class="field_title" style="background-color: #F2F2F2"><font color="#F00A0D"  size="3px;"></font><?php echo get_string('post_period', 'local_lmsdata'); ?></td>
                            <td class="field_value" style="text-align:left;">
                                <input type="checkbox" name="reger" value="1" ><?php echo get_string('banner:set','local_lmsdata');?>
                                <br>
                                <input type="text" id="timeavailable" title="time" name="startdate" value="<?php
                                echo date("Y-m-d", time());
                                ?>" disabled/>
                                <span> ~ </span><input type="text" id="timedue" title="time" name="enddate" value="<?php
                                echo date("Y-m-d", time() + 604800);
                                ?>" disabled/></td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:use','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;">
                                <input type="checkbox" name="isused" value="1" checked="checked"><?php echo get_string('use','local_lmsdata');?>&nbsp;&nbsp;&nbsp; 
                                <input type="checkbox" name="isused" value="0"><?php echo get_string('unuse','local_lmsdata');?>
                            </td>
                        </tr>
                        <input type="hidden" name="mod" value="<?php echo $mod ?>">
                    </table>
                    <div clas="btn_area">
                        <a href="<?php echo $CFG->wwwroot . '/local/management/bannerM/banner.php'; ?>"><input type="button" id="notice_list" class="blue_btn" value="<?php echo get_string('banner:list','local_lmsdata');?>" style="float: left; border: 1px solid #999" /></a>

                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save','local_lmsdata');?>" style="float: right; margin: 0 10px 0 0" />

                    </div> <!-- Bottom Button Area -->
                </form>
            </div> 
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    $(function() {    
        $("input[name=uploadfile]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["jpg", "jpeg", "png"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2','local_management')?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });
        
        $("input[name=uploadfile_mo]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["jpg", "jpeg", "png"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2','local_management')?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });
        
        $('#color').colorpicker();
        
        $('input[name=reger]').click(function () {
            if($('input[name=reger]').is(':checked')){
                 $('input[name=startdate]').removeAttr('disabled');
                 $('input[name=enddate]').removeAttr('disabled');
            } else {
                $('input[name=startdate]').attr('disabled','disabled');
                $('input[name=enddate]').attr('disabled','disabled');
            }
               
        });
        $('input[name=category]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=category]').prop('checked', false);
                $(this).prop('checked', true);
                if($(this).val() > 0){
                     $('input[name=target]').attr('disabled',true);
                     $('input[name=search]').attr('disabled',true);
                } else {
                    $('input[name=target]').attr('disabled',false);
                    $('input[name=search]').attr('disabled',false);
                }
            }
       }));
       $('input[name=search]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=search]').prop('checked', false);
                $(this).prop('checked', true);
            }
       }));
       $('input[name=site]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=site]').prop('checked', false);
                $(this).prop('checked', true);
            }
       }));
       $('input[name=isused]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=isused]').prop('checked', false);
                $(this).prop('checked', true);
            }
       }));
    });
 
    $("#timeavailable").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timedue").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#timedue").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timeavailable").datepicker("option", "maxDate", selectedDate);
        }
    });
    function chkfile(){
        var fileCheck = document.getElementById("uploadfile").value;
        var fileCheck2 = document.getElementById("uploadfile_mo").value;
        if(!fileCheck || !fileCheck2){
            alert("<?php echo get_string('banner:alert','local_management');?>");
            return false;
        }
    }
    </script>