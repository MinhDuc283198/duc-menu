<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage', 'local_lmsdata'),
    'heading' => get_string('banner_manage', 'local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerM',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');

$currpage = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$id = optional_param('id', 0, PARAM_INT);

$site = optional_param('site', -1, PARAM_INT);
$category = optional_param('category', -1, PARAM_INT);
$isused = optional_param('isused', -1, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$sortcol = optional_param('sortcol', '', PARAM_RAW);
$isasc = optional_param('isasc', -1, PARAM_INT);

$totalcount = lmsdata_banner_get_banner_count();

if(optional_param('resizeallbanner', 0, PARAM_INT) == 1) {
    global $DB;
    $_banners = $DB->get_records('lmsdata_banner');
    foreach ($_banners as $_banner) {
        lmsdata_banner_resize(1600, $context->id, 'local_lmsdata', 'banner', $_banner->id);
        lmsdata_banner_resize(750, $context->id, 'local_lmsdata', 'bannermobi', $_banner->id);
    }
}

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form id="frm_search" class="search_area" action="banner.php" method="get">
                    <input type="hidden" name="page" value="<?php echo $page; ?>">
                    <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                    <input type="hidden" name="sortcol">
                    <input type="hidden" name="isasc">

                    <div>
                        <label><?php echo get_string('banner:site', 'local_lmsdata'); ?></label>
                        <label class="mg-bt10"><input type="radio" name="site" value="-1" <?php echo $site == -1 ? 'checked' : ''; ?>><?php echo get_string('all', 'local_management'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="site" value="0" <?php echo $site == 0 ? 'checked' : ''; ?>><?php echo get_string('e-learning', 'local_lmsdata'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="site" value="1" <?php echo $site == 1 ? 'checked' : ''; ?>><?php echo get_string('job', 'local_lmsdata'); ?></label>     
                    </div>
                    <div>
                        <label><?php echo get_string('banner:loca', 'local_lmsdata'); ?></label>
                        <label class="mg-bt10"><input type="radio" name="category" value="-1" <?php echo $category == -1 ? 'checked' : ''; ?>><?php echo get_string('all', 'local_management'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="category" value="0" <?php echo $category == 0 ? 'checked' : ''; ?>><?php echo get_string('banner:top', 'local_lmsdata'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="category" value="1" <?php echo $category == 1 ? 'checked' : ''; ?>><?php echo get_string('banner:bot', 'local_lmsdata'); ?></label>     
                    </div>
                    <div>
                        <label><?php echo get_string('banner:use', 'local_lmsdata') ?></label>
                        <label class="mg-bt10"><input type="radio" name="isused" value="-1" <?php echo $isused == -1 ? 'checked' : ''; ?>><?php echo get_string('all', 'local_management'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="isused" value="1" <?php echo $isused == 1 ? 'checked' : ''; ?>><?php echo get_string('use', 'local_lmsdata'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="isused" value="0" <?php echo $isused == 0 ? 'checked' : ''; ?>><?php echo get_string('unuse', 'local_lmsdata'); ?></label>     
                    </div>

                    <div>
                        <label><?php echo get_string('search', 'local_management') ?></label>
                        <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('banner:name', 'local_lmsdata'); ?>"  class="search-text w100"/>
                        <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_management'); ?>"/>     
                        <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>"/>   
                    </div>
                </form>

                <span><?php echo get_string('total', 'local_management', $totalcount) ?></span>
                <select class="perpage2" onchange="$('input[name=perpage]').val(this.value);$('#frm_search').submit();">
                    <?php
                        $perpage_array = [10, 20, 30, 50];
                        foreach ($perpage_array as $pa) {
                        $selected = '';
                        if ($pa == $perpage) {
                            $selected = 'selected';
                        }
                    ?>
                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                    <?php
                    }
                    ?>
                </select>

                <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/local/management/bannerM/banner_delete.php'>
                    <table>
                        <caption class="hidden-caption"><?php echo get_string('banner_manage', 'local_lmsdata'); ?></caption>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="5%">No.</th>
                                <th scope="row" width="20%" onclick="sortcol('name', <?php echo ($sortcol != 'name' ? 1:($isasc ? 0:1)); ?>)"><?php echo get_string('banner:name', 'local_lmsdata').' '.($sortcol != 'name' ? '↕':($isasc ? '↑':'↓')); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('banner:loca', 'local_lmsdata'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('banner:site', 'local_lmsdata'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('contents_attachments', 'local_lmsdata') . ' (PC)'; ?></th>
                                <th scope="row" width="10%"><?php echo get_string('contents_attachments', 'local_lmsdata') . ' (Mobile)'; ?></th>
                                <th scope="row" width="15%"><?php echo get_string('banner:link', 'local_lmsdata'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('banner:use', 'local_lmsdata'); ?></th>
                                <th scope="row" width="15%" onclick="sortcol('startdate', <?php echo ($sortcol != 'startdate' ? 1:($isasc ? 0:1)); ?>)"><?php echo get_string('banner:date', 'local_lmsdata').' '.($sortcol != 'startdate' ? '↕':($isasc ? '↑':'↓')); ?></th>
                                <th scope="row" width="5%" onclick="sortcol('orderby', <?php echo ($sortcol != 'orderby' ? 1:($isasc ? 0:1)); ?>)"><?php echo get_string('banner:display_orderby', 'local_lmsdata').' '.($sortcol != 'orderby' ? '↕':($isasc ? '↑':'↓')); ?></th>
                                <th scope="row" width="15%" onclick="sortcol('created_at', <?php echo ($sortcol != 'created_at' ? 1:($isasc ? 0:1)); ?>)"><?php echo get_string('banner:created_at', 'local_lmsdata').' '.($sortcol != 'created_at' ? '↕':($isasc ? '↑':'↓')); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $offset = 0;
                            if ($currpage != 0) {
                                $offset = ($currpage - 1) * $perpage;
                            }
                            $num = $totalcount - $offset;
                            $banners = lmsdata_banner($offset, $perpage);
                            if ($totalcount) {
                                foreach ($banners as $banner) {
                                    $fs = get_file_storage();
                                    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
                            ?>
                                    <tr>
                                        <td class="chkbox">
                                            <input class="chkbox" type="checkbox" title="check" id="chkbox" name="bannerid<?php echo $banner->id; ?>" value="<?php echo $banner->id; ?>" />
                                        </td>
                                        <td class="number">
                                            <?php echo $num; ?>
                                        </td>
                                        <td>
                                            <a href='<?php echo $CFG->wwwroot ?>/local/management/bannerM/banner_view.php?mod=edit&id=<?php echo $banner->id ?>'><?php echo $banner->name ?></a>
                                        </td>
                                        <td>
                                            <?php echo ($banner->category == 0) ? get_string('banner:top', 'local_lmsdata') : get_string('banner:bot', 'local_lmsdata') ?>
                                        </td>
                                        <td>
                                            <?php echo ($banner->site > 0) ? get_string('job', 'local_lmsdata') : get_string('e-learning', 'local_lmsdata'); ?>
                                        </td>
                                        <?php
                                        if ($banner->id) {
                                            $output = '';
                                            foreach ($files as $file) {
                                                $filename = $file->get_filename();
                                                $mimetype = $file->get_mimetype();
                                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/banner/' . $banner->itemid . '/' . $filename);
                                                if ($file->get_filesize() > 0) {
                                                    $fileobj = '<img class=\'small\' src="' . $path . '">';
                                                }
                                            }

                                            $files = $fs->get_area_files($context->id, 'local_lmsdata', 'bannermobi', $banner->itemid, "", false);
                                            foreach ($files as $file) {
                                                $filename = $file->get_filename();
                                                $mimetype = $file->get_mimetype();
                                                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/bannermobi/' . $banner->itemid . '/' . $filename);
                                                if ($file->get_filesize() > 0) {
                                                    $fileobj_mo = '<img class=\'small\' src="' . $path . '">';
                                                }
                                            }
                                        }
                                        ?>
                                        <td>
                                            <?php
                                            if ($banner->id) {
                                                echo $fileobj;
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($banner->id) {
                                                echo $fileobj_mo;
                                                $fileobj_mo = '';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $banner->linkurl ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (($banner->isused) == 1) {
                                                echo get_string('use', 'local_lmsdata');
                                            } else {
                                                echo get_string('unuse', 'local_lmsdata');
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo (!empty($banner->startdate)) ? date('Y-m-d', $banner->startdate) . '<br>~' . date('Y-m-d', $banner->enddate) : '-'; ?>
                                        </td>
                                        <td><?php echo $banner->orderby == 0 ? '-' : $banner->orderby; ?></td>
                                        <td><?php echo (!empty($banner->created_at)) ? date('Y-m-d', $banner->created_at):'-'; ?></td>
                                    <?php
                                    $num--;
                                }
                            } else if (!$totalcount) {
                                    ?>
                                    <tr>
                                        <td colspan="12"><span><?php echo get_string('banner:nodata', 'local_lmsdata'); ?></span></td>
                                    </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                    <div id="btn_area">
                        <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/bannerM/banner_write.php?mod=add"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                        <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" />
                    </div>
                </form>
                <?php print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . "/local/management/bannerM/banner.php?perpage=$perpage&sortcol=$sortcol&isasc=$isasc&site=$site&category=$category&isused=$isused&searchtext=$searchtext"); ?>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    // $(function() {
    //        $("#allcheck").click(function() {
    //            var chk = $("#allcheck").is(":checked");
    //
    //            if (chk) {
    //                $(".chkbox input").each(function() {
    //                    this.checked = true;
    //                });
    //            } else {
    //                $(".chkbox input").each(function() {
    //                    this.checked = false;
    //                });
    //            }
    //        });
    $(function() {
        $("#allcheck").click(function() {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function() {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function() {
                    this.checked = false;
                });
            }
        });
        $("#delete_button").click(function() {
            if (!$(".chkbox").is(":checked")) {
                alert("<?php echo get_string('delalert', 'local_lmsdata'); ?>");
                return false;
            }

            if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
                $('#form').submit();
            }
        });

        $('#reset').on('click', function(){
            window.location.href = "<?php echo $CFG->wwwroot; ?>/local/management/bannerM/banner.php";
        })
    });

    function sortcol(sortcol, isasc) {
        $('input[name=sortcol]').val(sortcol);
        $('input[name=isasc]').val(isasc);
        $('#frm_search').submit();
    }
</script>