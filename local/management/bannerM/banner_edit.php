<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage', 'local_lmsdata'),
    'heading' => get_string('banner_manage', 'local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerM',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner_edit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerM/lib.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$banner = lmsdata_banner_get_banner_content($id);

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
$fileobj = lmsdata_banner_get_file($context->id, 'banner', $id, 1);
$mfiles = $fs->get_area_files($context->id, 'local_lmsdata', 'bannermobi', $banner->itemid, "", false);
$mfileobj = lmsdata_banner_get_file($context->id, 'bannermobi', $id, 1);
$temp->itemid = $banner->id;

include_once($CFG->dirroot . '/local/management/header.php');
?>
<section class="content">
    <div class="box box-body">
        <div>
            <form action="banner_submit.php" enctype="multipart/form-data" method="POST" onsubmit="return chkfile()">
                <input type="hidden" name="mod" value="edit"/>
                <table class="detail">
                    <tr>
                        <td class="field_title" width="20%"><?php echo get_string('banner:name', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="text" name="name" value="<?php echo $banner->name; ?>"
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('contents_attachments', 'local_lmsdata'); ?>(PC)</td>
                        <td class="field_value">
                            <?php
                            for ($n = 0; $n <= 0; $n++) {
                                ?>
                           
                                <?php echo $banner->filename; ?>
                                <input type="file" name="uploadfile" id="uploadfile"/> <span><?php echo get_string('banner:top','local_lmsdata')?> : (1600*379px)<br><?php echo get_string('banner:bot','local_lmsdata')?> : (1600*100px)</span>
                                <input type="hidden" class="" name="file_id" value="<?php
                                if ($temp->itemid > 0) {
                                    echo 1;
                                } else {
                                    echo 0;
                                }
                                ?>"/>
                                
                                <div class="mg-tp10">
                                    <?php
                                    if (isset($fileobj[$n])) {
                                        echo $fileobj[$n];
                                    }
                                    echo "</div>";
                                }
                                ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('contents_attachments', 'local_lmsdata'); ?>(Mobile)</td>
                        <td class="field_value">
                            <?php
                            for ($n = 0; $n <= 0; $n++) {
                                ?>
                            
                                <?php echo $banner->filename; ?>
                                <input type="file" name="uploadfile_mo" id="uploadfile_mo"/> <span><?php echo get_string('banner:top','local_lmsdata')?> : (750*580px)<br><?php echo get_string('banner:bot','local_lmsdata')?> : (750*100px)</span>
                                <input type="hidden" class="" name="file_id_mo" value="<?php
                                if ($temp->itemid > 0) {
                                    echo 1;
                                } else {
                                    echo 0;
                                }
                                ?>"/>
                                <div class="mg-tp10 ex-img"> 

                                    <?php
                                    if (isset($mfileobj[$n])) {
                                        echo $mfileobj[$n];
                                    }
                                    echo "</div>";
                                }
                                ?>
                        </td>
                    </tr>
                    <tr>
                        <td  class="field_title"><?php echo get_string('banner:site', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="checkbox" name="site" value="0" <?php echo $banner->site == 0 ? ' checked="checked"' : ''; ?>><?php echo get_string('e-learning', 'local_lmsdata'); ?>
                            <input type="checkbox" name="site" value="1" <?php echo $banner->site == 1 ? ' checked="checked"' : ''; ?>><?php echo get_string('job', 'local_lmsdata'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:link', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="text" name="linkurl" value="<?php echo $banner->linkurl; ?>"
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:num', 'local_lmsdata'); ?></td>
                        <td class="field_value"><input type="text" name="order" value="<?php echo $banner->orderby; ?>" <?php if ($banner->category == 1) {
                                           echo 'disabled="disabled"';
                                       }; ?>></td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:color', 'local_lmsdata'); ?></td>
                        <td class="field_value"><input type="text" id="color" name="colorcode" value="<?php echo $banner->colorcode; ?>"></td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:loca', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="checkbox" name="category" value="0" <?php if ($banner->category == 0) {
                                           echo 'checked="checked"';
                                       }; ?>><?php echo get_string('banner:top', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp; 
                            <input type="checkbox" name="category" value="1" <?php if ($banner->category == 1) {
                                           echo 'checked="checked"';
                                       }; ?>><?php echo get_string('banner:bot', 'local_lmsdata'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:search', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="checkbox" name="search" value="0" <?php if ($banner->search == 0) {
                                           echo 'checked="checked"';
                                       };
                                       if ($banner->category == 1) {
                                           echo 'disabled="disabled"';
                                       }; ?>><?php echo get_string('unuse', 'local_lmsdata'); ?>
                            <input type="checkbox" name="search" value="1"<?php if ($banner->search == 1) {
                                echo 'checked="checked"';
                            };
                            if ($banner->category == 1) {
                                echo 'disabled="disabled"';
                            }; ?>><?php echo get_string('use', 'local_lmsdata'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:date', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="checkbox" name="reger" value="1" ><?php echo get_string('banner:set', 'local_lmsdata'); ?>
                            <br>
                            <input type="text" id="timeavailable" title="time" name="startdate" value="<?php echo $banner->startdate != 0 ? date("Y-m-d", $banner->startdate):''; ?>" disabled/>
                            <span> ~ </span><input type="text" id="timedue" title="time" name="enddate" value="<?php echo $banner->enddate !=0 ? date("Y-m-d", $banner->enddate):''; ?>" disabled/></td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:use', 'local_lmsdata'); ?></td>
                        <td class="field_value">
<?php if ($banner->isused == 1) { ?>
                                <input type="radio" name="isused" value="1" checked="checked"><?php echo get_string('use', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0"><?php echo get_string('unuse', 'local_lmsdata'); ?></td>
    <?php } else {
    ?>
                        <input type="radio" name="isused" value="1" ><?php echo get_string('use', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0" checked="checked"><?php echo get_string('unuse', 'local_lmsdata'); ?></td>
                        </td>
                        </tr>
<?php } ?>            
                </table>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div id="btn_area">
                    <input type="submit" id="add_user" class="blue_btn btn-area-right" value="<?php echo get_string('save', 'local_lmsdata'); ?>" />
                    <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->  
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script type="text/javascript">
    $(function () {
        $('#color').colorpicker();

        if($('input[name=startdate]').val() != '' || $('input[name=enddate]').val() != '') {
            $('input[name=reger]').prop('checked', true);    
            $('input[name=startdate]').prop('disabled', false);    
            $('input[name=enddate]').prop('disabled', false);    
        }
        $('input[name=reger]').click(function () {
            if ($('input[name=reger]').is(':checked')) {
                $('input[name=startdate]').removeAttr('disabled');
                $('input[name=enddate]').removeAttr('disabled');
            } else {
                $('input[name=startdate]').val('');
                $('input[name=startdate]').attr('disabled', 'disabled');
                $('input[name=enddate]').val('');
                $('input[name=enddate]').attr('disabled', 'disabled');
            }

        });
        $('input[name=category]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=category]').prop('checked', false);
                $(this).prop('checked', true);
            }
            if ($(this).val() > 0) {
                $('input[name=order]').attr('disabled', true);
                $('input[name=search]').attr('disabled', true);
            } else {
                $('input[name=order]').attr('disabled', false);
                $('input[name=search]').attr('disabled', false);
            }
        }));
        $('input[name=site]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=site]').prop('checked', false);
                $(this).prop('checked', true);
            }
        }));
        $('input[name=search]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=search]').prop('checked', false);
                $(this).prop('checked', true);
            }
        }));
        $('input[name=isused]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=isused]').prop('checked', false);
                $(this).prop('checked', true);
            }
        }));
    });
    $(document).ready(function () {
        $('#banner_list').click(function () {
            location.href = "./banner.php";
        });
    });
    $("#timeavailable").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timedue").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#timedue").datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function (selectedDate) {
            $("#timeavailable").datepicker("option", "maxDate", selectedDate);
        }
    });
    function chkfile() {
        var file_id = document.getElementByName("file_id").value;
        var file_id_mo = document.getElementByName("file_id_mo").value;
        var fileCheck = document.getElementById("uploadfile").value;
        var fileCheck2 = document.getElementById("uploadfile_mo").value;
        if ((file_id != 1 && !fileCheck) || (file_id_mo != 1 && !fileCheck2)) {
            alert("<?php echo get_string('banner:alert', 'local_management'); ?>");
            return false;
        }
    }
</script>