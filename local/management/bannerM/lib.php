<?php

function lmsdata_banner($page, $perpage)
{
    global $DB;

    $site = optional_param('site', -1, PARAM_INT);
    $category = optional_param('category', -1, PARAM_INT);
    $isused = optional_param('isused', -1, PARAM_INT);
    $searchtext = optional_param('searchtext', '', PARAM_RAW);
    $sortcol = optional_param('sortcol', '', PARAM_RAW);
    $isasc = optional_param('isasc', -1, PARAM_INT);

    // $sort = "lb.orderby <= 0, lb.orderby ASC, lb.startdate <= 0, lb.startdate ASC, lb.enddate ASC, lb.id DESC";
    $sort = "lb.id DESC";
    $where = array();
    $params = array();

    if ($site != -1) {
        $where[] = "lb.site=$site";
    }
    if ($category != -1) {
        $where[] = "lb.category=$category";
    }
    if ($isused != -1) {
        $where[] = "lb.isused=$isused";
    }

    if ($searchtext != '') {
        $where[] = $DB->sql_like('lb.name', ':lbname', false);
        $params['lbname'] = '%' . $searchtext . '%';
    }

    if ($sortcol != '') {
        switch ($sortcol) {
            case 'orderby':
                $sort = "lb.orderby <= 0, lb.orderby " . ($isasc ? 'ASC' : 'DESC');
                break;
            case 'startdate':
                $sort = "lb.startdate <= 0, lb.startdate " . ($isasc ? 'ASC' : 'DESC');
                break;
            case 'created_at':
                $sort = "lb.created_at <= 0, lb.created_at " . ($isasc ? 'ASC' : 'DESC') . ", lb.id " . ($isasc ? 'ASC' : 'DESC');
                break;
            default:
                $sort = "lb.$sortcol " . ($isasc ? 'ASC' : 'DESC');
                break;
        }
    }

    $where_sql = "";
    if (count($where)) {
        $where_sql = "where " . implode(" AND ", $where);
    }

    $sql = "select * from {lmsdata_banner} lb
          join (SELECT itemid FROM {files} WHERE component = 'local_lmsdata' AND filesize >0 and filearea='banner') f 
          ON lb.id = f.itemid
          $where_sql 
          ORDER BY $sort";

    return $DB->get_records_sql($sql, $params, $page, $perpage);
}

function lmsdata_banner_get_banner_count()
{
    global $DB;

    $site = optional_param('site', -1, PARAM_INT);
    $category = optional_param('category', -1, PARAM_INT);
    $isused = optional_param('isused', -1, PARAM_INT);
    $searchtext = optional_param('searchtext', '', PARAM_RAW);
    $sortcol = optional_param('sortcol', '', PARAM_RAW);
    $isasc = optional_param('isasc', -1, PARAM_INT);

    $where = array();
    $params = array();

    if ($site != -1) {
        $where[] = "lb.site=$site";
    }
    if ($category != -1) {
        $where[] = "lb.category=$category";
    }
    if ($isused != -1) {
        $where[] = "lb.isused=$isused";
    }

    if ($searchtext != '') {
        $where[] = $DB->sql_like('lb.name', ':lbname', false);
        $params['lbname'] = '%' . $searchtext . '%';
    }

    $where_sql = "";
    if (count($where)) {
        $where_sql = "where " . implode(" AND ", $where);
    }

    $sql = "select count(*) from {lmsdata_banner} lb
        left join (SELECT itemid FROM {files} WHERE component = 'local_lmsdata' AND filesize >0 AND filearea='banner') f 
        ON lb.id = f.itemid
        $where_sql";

    return $DB->count_records_sql($sql, $params);
}
//
//function print_paging_navbar($totalcount, $page, $perpage, $baseurl, $params = null, $maxdisplay = 18) {
//    global $CFG;
//    $pagelinks = array();
//   
//    $lastpage = 1;
//    if($totalcount > 0) {
//        $lastpage = ceil($totalcount / $perpage);
//    }
//   
//    if($page > $lastpage) {
//        $page = $lastpage;
//    }
//           
//    if ($page > round(($maxdisplay/3)*2)) {
//        $currpage = $page - round($maxdisplay/2);
//        if($currpage > ($lastpage - $maxdisplay)) {
//            if(($lastpage - $maxdisplay) > 0){
//                $currpage = $lastpage - $maxdisplay;
//            }
//        }
//    } else {
//        $currpage = 1;
//    }
//   
//   
//   
//    if($params == null) {
//        $params = array();
//    }
//   
//    $prevlink = '';
//    if ($page > 1) {
//        $params['page'] = $page - 1;
//        $prevlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/>', array('class'=>'next'));
//    } else {
//        $prevlink = '<a href="#" class="next"><img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/></a>';
//    }
//   
//    $nextlink = '';
//     if ($page < $lastpage) {
//        $params['page'] = $page + 1;
//        $nextlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/>', array('class'=>'prev'));
//    } else {
//        $nextlink = '<a href="#" class="prev"><img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/></a>';
//    }
//   
//   
//    echo '<div class="pagination">';
//   
//    $pagelinks[] = $prevlink;
//   
//    if ($currpage > 1) {
//        $params['page'] = 1;
//        $firstlink = html_writer::link(new moodle_url($baseurl, $params), 1);
//       
//        $pagelinks[] = $firstlink;
//        if($currpage > 2) {
//            $pagelinks[] = '...';
//        }
//    }
//   
//    $displaycount = 0;
//    while ($displaycount <= $maxdisplay and $currpage <= $lastpage) {
//        if ($page == $currpage) {
//            $pagelinks[] = '<strong>'.$currpage.'</strong>';
//        } else {
//            $params['page'] = $currpage;
//            $pagelink = html_writer::link(new moodle_url($baseurl, $params), $currpage);
//            $pagelinks[] = $pagelink;
//        }
//       
//        $displaycount++;
//        $currpage++;
//    }
//   
//    if ($currpage - 1 < $lastpage) {
//        $params['page'] = $lastpage;
//        $lastlink = html_writer::link(new moodle_url($baseurl, $params), $lastpage);
//       
//        if($currpage != $lastpage) {
//            $pagelinks[] = '...';
//        }
//        $pagelinks[] = $lastlink;
//    }
//   
//    $pagelinks[] = $nextlink;
//   
//   
//    echo implode('&nbsp;', $pagelinks);
//   
//    echo '</div>';
//}

function lmsdata_banner_get_banner_content($id)
{
    global $DB;

    return $DB->get_record('lmsdata_banner', array('id' => $id));
}

function lmsdata_banner_get_file($contextid, $filearea, $itemid = 0, $type, $on = '')
{
    global $CFG, $OUTPUT;

    $fs = get_file_storage();
    $files = $fs->get_area_files($contextid, 'local_lmsdata', $filearea, $itemid, "", false);

    $fileobj = '';
    $cnt = 0;
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_lmsdata/' . $filearea . '/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            $fileobj[$cnt] = '<input type="hidden" name="file_del" value="0"/><img src="' . $path . '">';
            $fileobj[$cnt] .= "<div style='float:left;' id ='file'><a href=\"$path\">$iconimage</a>";
            $fileobj[$cnt] .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $contextid)) . '</div>';
            $cnt++;
        }
    }
    return $fileobj;
}

/*
 * 받은 파라미터로 studytype을 리턴한다
 * classtype : lmsdata_course classtype을 파라미터로 받음 
 * return : $studytype
 */
function studytype($classtype)
{

    $classtypes = explode(',', $classtype);
    $studytype = '';

    foreach ($classtypes as $classtype) {
        if ($classtype == 1) {
            $studytype .= ', 영재원';
        } else if ($classtype == 2) {
            $studytype .= ', 일반인';
        } else if ($classtype == 3) {
            $studytype .= ', 영재센터';
        }
    }
    $studytype = trim(substr($studytype, 1));

    return $studytype;
}

function lmsdata_banner_resize($max_width, $contextid, $component, $filearea, $itemid)
{
    global $CFG, $OUTPUT, $USER;
    $fs = get_file_storage();
    $overlap_files = $fs->get_area_files($contextid, $component, $filearea, $itemid, 'id');

    foreach ($overlap_files as $file) {
        if ($file->get_filesize() > 0) {
            $filename = $file->get_filename();
            $source = imagecreatefromstring($file->get_content());
            $width = imagesx($source);

            if ($max_width < $width) {
                $percent = $max_width / $width;
                $thumb = imagescale($source, $width * $percent);

                $file_record = array(
                    'contextid' => $contextid,
                    'component' => $component,
                    'filearea' => $filearea,
                    'itemid' => $itemid,
                    'filepath' => '/',
                    'filename' => $filename,
                    'timecreated' => time(),
                    'timemodified' => time(),
                    'userid' => $USER->id,
                    'author' => fullname($USER),
                    'license' => 'allrightsreserved',
                    'sortorder' => 0
                );
                ob_start();
                imagepng($thumb);
                $content = ob_get_clean();

                $file->delete();
                $storage_id = $fs->create_file_from_string($file_record, $content);
            }
        }
    }
}
