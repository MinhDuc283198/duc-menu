  </div>
  <!-- /.content-wrapper -->
  
  <footer class="main-footer">
    Copyright © VISANG Education Group Vietnam Company
  </footer>
</div>
<!-- ./wrapper -->



<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/local/management/js/jquery-ui.min.js"></script>

<script src="<?php echo $CFG->wwwroot; ?>/local/management/colorpicker-master/jquery.colorpicker.js"></script>
<?php
$scripts = local_management_load_js();

foreach($scripts as $js) {
    echo '<script src="'.local_management_make_absolute($js).'"></script>';
}

// 추가 선언된 자바스크립트
if(isset($pagesettings['js'])) {
    foreach($pagesettings['js'] as $js) {
        echo '<script src="'.local_management_make_absolute($js).'"></script>';
    }
}
?>
</body>
</html>
