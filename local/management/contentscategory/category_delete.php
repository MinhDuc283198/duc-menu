<?php

require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot . '/chamktu/lib.php';

$delete = required_param('delete', PARAM_INT);

$category = $DB->get_record('lmsdata_category', array('id' => $delete));

$DB->delete_records('lmsdata_category_apply', array('categoryid' => $delete));
$DB->delete_records('lmsdata_category_name', array('categoryid' => $delete));
$DB->delete_records('lmsdata_category', array('id' => $delete));

if ($category->type == 1) {
    $subcategorys = $DB->get_records('lmsdata_category', array('parent' => $delete));
    foreach ($subcategorys as $subcategory) {
        $DB->delete_records('lmsdata_category_apply', array('categoryid' => $subcategory->id));
        $DB->delete_records('lmsdata_category_name', array('categoryid' => $subcategory->id));
        $DB->delete_records('lmsdata_category', array('id' => $subcategory->id));
    }
}
if ($category->depth == 2) {
    $DB->execute("update {lmsdata_category} set step = step - 1 where parent = :parent and depth = :depth and step > :step", array('parent' => $category->parent, 'depth' => $category->depth, 'step' => $category->step));
} else if ($category->depth == 1) {
    $DB->execute("update {lmsdata_category} set step = step - 1 where depth = :depth and step > :step", array('depth' => $category->depth, 'step' => $category->step));
}


redirect('category.php');
