<?php

require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot . '/chamktu/lib.php';
// Check for valid admin user - no guest autologin

require_login(0, false);

$names = required_param_array('name', PARAM_RAW);
//$required = required_param('required', PARAM_INT);
$type = required_param('type', PARAM_INT);
$step = optional_param('step', '', PARAM_RAW);
$isused = required_param('isused', PARAM_INT);
//$url = optional_param('url', '', PARAM_RAW);
$parent = optional_param('parent', 0, PARAM_INT);
//$icon = optional_param('icon', '', PARAM_RAW);
//$usergroups = required_param_array('usergroup', PARAM_RAW);
$current_step = optional_param('current_step', 0, PARAM_INT);
$edit = optional_param('edit', 0, PARAM_INT);
$ispopup = optional_param('ispopup', 1, PARAM_INT);
$sub_parent = optional_param('sub_parent', 0, PARAM_INT);

/* To Do Edit 
 * 1.step rule define. 
 * 2. make edit
 * 3. test 
 */

$data = new stdClass();

if (!$edit) {
    if ($sub_parent != 0) {
        $data->depth = '3';
        $data->parent = $parent;
        $data->sub_parent = $sub_parent;
    } else if ($type != 2) {
        $data->depth = '1';
    } else {
        $data->depth = '2';
        $data->parent = $parent;
    }
    $data->type = $type;
    if ($step == -1) {  // 스텝 0 = 맨 앞
        if ($type == 2) {
            $max = $DB->get_field_sql('select max(step) from {lmsdata_category} where parent = :parent and depth = :depth', array('depth' => $data->depth, 'parent' => $parent));
        } else {
            $max = $DB->get_field_sql('select max(step) from {lmsdata_category} where depth = :depth', array('depth' => $data->depth));
        }
        $data->step = $max + 1;
    } else {
        $data->step = $step;
        if ($type == 2) {
            $DB->execute("update {lmsdata_category} set step = step + 1 where parent = ? and depth = ? and step >= ?", array($parent, $data->depth, $step));
        } else {
            $DB->execute("update {lmsdata_category} set step = step + 1 where depth = ? and step >= ?", array($data->depth, $step));
        }
    }


//    $data->icon = $icon;
//    $data->required = $required; 
//    $data->url = $url;
    $data->userid = $USER->id;
    $data->isused = $isused;
    $data->ispopup = $ispopup;
    $data->timecreated = time();

    $new_category = $DB->insert_record('lmsdata_category', $data);
    if ($type != 2) {
        $DB->set_field('lmsdata_category', 'parent', $new_category, array('id' => $new_category));
    }

    foreach ($names as $lang => $name) {
        $name_languge = new stdClass();
        $name_languge->categoryid = $new_category;
        $name_languge->lang = $lang;
        $name_languge->name = $name;
        $name_languge->timemodified = time();
        $name_by_languge = $DB->insert_record('lmsdata_category_name', $name_languge);
    }
    $usergroups = array('0' => 'rs', '1' => 'pr');
    foreach ($usergroups as $usergroup => $val) {
        $category_usergroup = new stdClass();
        $category_usergroup->categoryid = $new_category;
        $category_usergroup->usergroup = $val;
        $category_usergroup->timecreated = time();
        $category_usergroup->timemodified = time();
        $name_by_languge = $DB->insert_record('lmsdata_category_apply', $category_usergroup);
    }

    redirect('category.php');
} else {
    $category = $DB->get_record('lmsdata_category', array('id' => $edit));
    $data->id = $edit;

    if ($sub_parent != 0) {
        $data->depth = '3';
        $data->parent = $parent;
    } else if ($type != 2) {
        $data->depth = '1';
    } else {
        $data->depth = '2';
        $data->parent = $parent;
    }

    $data->sub_parent = $sub_parent;

    $current_groups = $DB->get_records('lmsdata_category_apply', array('categoryid' => $edit));



//    $ug = array(); // make new apply and change key,value
//    foreach ($usergroups as $k => $v) {
//        $ug[$v] = $k;
//        if ($group_apply = $DB->get_record('category_apply', array('categoryid' => $edit, 'usergroup' => $v))) {
//            continue;
//        } else {
//            $category_usergroup = new stdClass();
//            $category_usergroup->categoryid = $edit;
//            $category_usergroup->usergroup = $v;
//            $category_usergroup->timecreated = time();
//            $category_usergroup->timemodified = time();
//            $name_by_languge = $DB->insert_record('category_apply', $category_usergroup);
//        }
//    }
//    /* Delete Undefine Usergroups */
//    foreach ($current_groups as $k => $v) {
//        if (!isset($ug[$v->usergroup])) {
//            $DB->delete_records('category_apply', array('categoryid' => $edit, 'usergroup' => $v->usergroup));
//        }
//    }

    if ($step != '') {
        if ($category->type == 2) {
            $current_step = $category->step;
            $max = $DB->get_field_sql('select max(step) from {lmsdata_category} where parent = :parent and depth = :depth and sub_parent = :sub_parent', array('depth' => $data->depth, 'parent' => $parent, 'sub_parent' => $sub_parent));

            $cntmax = $DB->count_records_sql('select count(id) from {lmsdata_category} where parent = :parent and depth = :depth', array('depth' => $data->depth, 'parent' => $parent));
            if ($step == -1) {  // 스텝 0 = 맨 앞   -1 맨뒤
                $DB->execute("update {lmsdata_category} set step = step - 1 where parent = :parent and depth = :depth and step > :step and step < :step1", array('parent' => $parent, 'depth' => $category->depth, 'step' => $current_step, 'step1' => $cntmax));
                $data->step = $cntmax - 1;
            } else {
                if ($step < $current_step) {
                    $DB->execute("update {lmsdata_category} set step = step + 1 where parent = :parent and depth = :depth and step >= :step and step < :step1", array('parent' => $parent, 'depth' => $category->depth, 'step' => $step, 'step1' => $current_step));
                    $data->parent = $parent;
                    $data->step = $step;
                } else if ($step > $current_step) {
                    $DB->execute("update {lmsdata_category} set step = step - 1 where parent = :parent and depth = :depth and step <= :step and step > :step1", array('parent' => $parent, 'depth' => $category->depth, 'step' => $step, 'step1' => $current_step));
                    $data->parent = $parent;
                    $data->step = $step;
                }
            }
        } else {
            $current_step = $category->step;
            $cntmax = $DB->count_records_sql('select count(id) from {lmsdata_category} where depth = :depth', array('depth' => $data->depth));
//            $DB->execute("update {lmsdata_category} set step = step - 1 where depth = ? and step >= ?", array($category->parent, $category->depth, $category->step));
            if ($step == -1) {
//                $max = $DB->get_field_sql('select max(step) from {lmsdata_category} where depth = :depth', array('depth' => $category->depth));
//                $data->step = ($max) ? $max + 1 : 0;
                $DB->execute("update {lmsdata_category} set step = step - 1 where depth = :depth and step > :step and step < :step1", array('depth' => $category->depth, 'step' => $current_step, 'step1' => $cntmax));
                $data->step = $cntmax - 1;
            } else {
                if ($step < $current_step) {
                    $DB->execute("update {lmsdata_category} set step = step + 1 where depth = :depth and step >= :step and step < :step1", array('depth' => $category->depth, 'step' => $step, 'step1' => $current_step));
                    $data->step = $step;
                } else if ($step > $current_step) {
                    $DB->execute("update {lmsdata_category} set step = step - 1 where  depth = :depth and step <= :step and step > :step1", array('depth' => $category->depth, 'step' => $step, 'step1' => $current_step));
                    $data->step = $step;
                }
            }
        }
    }


//    $data->icon = $icon;
//    $data->required = $required;
//    $data->url = $url;
    $data->type = $type;
    $data->userid = $USER->id;
    $data->ispopup = $ispopup;
    $data->isused = $isused;
    $data->timemodified = time();
    $DB->update_record('lmsdata_category', $data);

    
    foreach ($names as $lang => $name) {
        if (!$name) {
            $DB->delete_records('lmsdata_category_name', array('categoryid' => $edit, 'lang' => $lang));
        } else if (!$lang_list = $DB->get_record('lmsdata_category_name', array('categoryid' => $edit, 'lang' => $lang))) {
            $name_languge = new stdClass();
            $name_languge->categoryid = $edit;
            $name_languge->lang = $lang;
            $name_languge->name = $name;
            $name_languge->timemodified = time();
            $name_by_languge = $DB->insert_record('lmsdata_category_name', $name_languge);
        } else {
            $name_languge = new stdClass();
            $name_languge->id = $lang_list->id;
            $name_languge->name = $name;
            $name_languge->timemodified = time();
            $name_by_languge = $DB->update_record('lmsdata_category_name', $name_languge);
        }
    }


    redirect('category_add.php?id=' . $edit);
}