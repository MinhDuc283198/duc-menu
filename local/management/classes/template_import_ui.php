<?php
require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/moodle2/backup_plan_builder.class.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot . '/backup/util/ui/import_extensions.php');

class template_import_ui extends import_ui {
    public function __construct($controller, array $params = null, $stage = null) {
        $this->controller = $controller;
        $this->progress = self::PROGRESS_INTIAL;
        $this->stage = $this->initialise_stage($stage, $params);
        // Process UI event before to be safe.
        $this->controller->process_ui_event();
    }
    
    protected function initialise_stage($stage = null, array $params = null) {
        if ($stage == null) {
            $stage = optional_param('stage', self::STAGE_INITIAL, PARAM_INT);
        }
        if (self::$skipcurrentstage) {
            $stage *= 2;
        }
        switch ($stage) {
            case backup_ui::STAGE_INITIAL:
                $stage = new import_ui_stage_inital($this, $params);
                break;
            case backup_ui::STAGE_SCHEMA:
                $stage = new import_ui_stage_schema($this, $params);
                break;
            case backup_ui::STAGE_CONFIRMATION:
                $stage = new import_ui_stage_confirmation($this, $params);
                break;
            case backup_ui::STAGE_FINAL:
                $stage = new import_ui_stage_final($this, $params);
                break;
            default:
                $stage = false;
                break;
        }
        
        return $stage;
    }
}
