<?php

// Standard GPL and phpdocs
namespace local_management\output;

use block_rss_client\output\item;
use renderable;
use renderer_base;
use templatable;
use stdClass;

class coupon_management_page implements renderable, templatable
{
    /** @var string $sometext Some text to show how to pass data to a template. */
    var $datapage = null;

    public function __construct($datapage)
    {
        $this->datapage = $datapage;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output)
    {
        global $CFG;
        $data = new stdClass();
        $couponid = $this->datapage->couponid;
        $coupons = $this->get_coupons($this->datapage->searchcoursetext);
        if (count($coupons) <= 0) {
            $coupondisplay = '';
            $coupondisshow = get_string('cp_nodata', 'local_management');
        }
        if ($couponid == null) {
            $displayrecei = 'none';
        } else {
            $displayrecei = 'block';
            $coupontitle = get_coupon_title_by_id($couponid);
            $receivers = $this->get_detail_receivers($couponid);
        }
        if (count($receivers) <= 0) {
            $receidisplay = '';
            $receidisshow = get_string('cp_nodata', 'local_management');
        }
        $errors = $this->get_errors();
        if (count($errors) <= 0) {
            $errordisplay = '';
            $errordisshow = get_string('cp_nodata', 'local_management');
        }

        return array(
            'coupons' => $coupons,
            'errors' => $errors,
            'url' => $this->get_page_infor(),
            'receivers' => $receivers,
            'couponid' => $couponid,
            'displayrecei' => $displayrecei,
            'coupontitle' => $coupontitle->title,
            'errordisplay' => $errordisplay,
            'errordisshow' => $errordisshow,
            'receidisplay' => $receidisplay,
            'receidisshow' => $receidisshow,
            'coupondisplay' => $coupondisplay,
            'coupondisshow' => $coupondisshow,
        );
    }

    public function get_detail_receivers($couponid)
    {
        $searchrecei = $this->datapage->searchrecei;
        $receivers = array();
        if ($searchrecei !== "" && $couponid !== null) {
            $receieds = get_coupons_receivers_search($searchrecei, $couponid);
        } else {
            $receieds = get_coupon_receivers($couponid);
        }
        foreach ($receieds as $receied) {
            $iterm = new stdClass();
//                $iterm->no = $noerror;
            $coupon = get_coupon_by_id($receied->coupon_id);
            $iterm->email = $receied->email;
            $iterm->received_time = date_format_string($receied->received_time, '%Y/%m/%d');
            $iterm->key_code = $coupon->key_code;
            if ($receied->is_used == 0) {
                $iterm->is_used = 'NO';
                $iterm->used_date = '-';
            } else if ($receied->is_used == 1) {
                $iterm->is_used = 'YES';
                $iterm->used_date = date_format_string($receied->used_time, '%Y/%m/%d');
            }
            array_push($receivers, $iterm);
        }
        return $receivers;
    }

    public function get_coupons($searchtext = '')
    {
        global $CFG;
        $coupons = null;
        if(empty($searchtext)) {
            $coupons = array_reverse(get_coupons_list());
        } else {
            $coupons = array_reverse(get_searching_coupons($searchtext));
        }
        $coupon_infor = array();
        $nocoupon = 1;
        foreach ($coupons as $coupon) {
            $iterm = new stdClass();
            $iterm->no = $nocoupon;
            $iterm->idcoupon = $coupon->id;
            $iterm->editlink = $CFG->wwwroot . '/local/management/coupon/coupongenerate.php?id=' . $coupon->id;
            $iterm->detaillink = $CFG->wwwroot . '/local/management/coupon/index.php?id=' . $coupon->id;
            $iterm->title = $coupon->title;
            $iterm->img = $coupon->filename;
            $coupon_course = get_course_coupon_by_id($coupon->id);
            $course_infor = get_course_by_id($coupon_course->course_id);
            $iterm->course = $course_infor->vi_coursename;
            $iterm->disrate = $coupon->discount_rate;
            $iterm->quantity = $coupon->count;
            $iterm->create_time = date_format_string($coupon->create_time, '%Y/%m/%d');
            if ($coupon->is_send == 1) {
                $iterm->is_send = 'YES';
                $iterm->send_date = date_format_string($coupon->send_date, '%Y/%m/%d');
            } else if ($coupon->is_send == 0) {
                $iterm->is_send = 'NO';
                $iterm->send_date = '-';
            }
            $today = time();
            if ($coupon->expire_date - $today <= 0) {
                $iterm->is_expire = "YES";
            } else {
                $iterm->is_expire = "NO";
            }
            $iterm->expire_date = date_format_string($coupon->expire_date, '%Y/%m/%d');
            array_push($coupon_infor, $iterm);
            $nocoupon++;
        }

        return $coupon_infor;
    }

    public function get_errors()
    {
        $errors_infor = array();
        $searcherror = $this->datapage->searcherror;
        if ($searcherror == "") {
            $errors = get_error_list();
        } else {
            $errors = get_error_list_search($searcherror);
        }

        foreach ($errors as $error) {
            $iterm = new stdClass();
            $iterm->iderror = $error->id;
            $coupon = get_coupon_by_id($error->coupon_id);
            $iterm->coupon = $coupon->title;
            $iterm->error_type = $error->error_type;
            $iterm->error_message = $error->error_message;
            $iterm->created_time = date_format_string($error->created_time, '%Y/%m/%d');
            array_push($errors_infor, $iterm);
//                $noerror++;
        }
        return $errors_infor;
    }

    public function get_page_infor()
    {
        global $CFG;
        return array(
            'urladd' => $CFG->wwwroot . '/local/management/coupon/coupongenerate.php',
            'basurl' => $this->datapage->baseurl,
        );
    }


}