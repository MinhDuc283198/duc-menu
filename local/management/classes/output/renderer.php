<?php
// Standard GPL and phpdocs
namespace local_management\output;

defined('MOODLE_INTERNAL') || die;

use plugin_renderer_base;

class renderer extends plugin_renderer_base {
    /**
     * Defer to template.
     *
     * @param local_management $page
     *
     * @return string html for the page
     */
    public function render_coupon_generate_page($page) {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_management/coupon_generate_page', $data);
    }
    public function render_coupon_management_page($page)
    {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_management/coupon_management_page', $data);
    }
}