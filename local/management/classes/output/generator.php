<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Coupon code generator
 *
 * File         generator.php
 * Encoding     UTF-8
 *
 * @package     block_coupon
 *
 * @copyright   Sebsoft.nl
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * */

namespace local_management\output;
defined('MOODLE_INTERNAL') || die();

use local_management\output\codegenerator;
use local_management\output\icoupongenerator;

/**
 * block_coupon\coupon\generator
 *
 * @package     block_coupon
 *
 * @copyright   Sebsoft.nl
 * @author      R.J. van Dongen <rogier@sebsoft.nl>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class generator implements icoupongenerator
{
    /**
     * Courses (each element has id, fullname)
     * @var array
     */
    protected $course;
    /**
     * Groups (each element has id, name)
     * @var array
     */
    protected $groups;
    /**
     * Cohorts (each element has id, name)
     * @var array
     */
    protected $cohorts;

    /**
     * list of errors messages
     * @var array
     */
    protected $errors;
    /**
     * list of generated coupon ids
     * @var array
     */
    protected $generatorids;
    /**
     * list of generated coupon codes
     * @var array
     */
    /**
     * Get errors
     * @return array
     */
    public function get_errors()
    {
        return $this->errors;
    }

    /**
     * get the generated coupon IDs
     * @return array
     */
    public function get_generated_couponids()
    {
        return $this->generatorids;
    }

    /**
     * get the generated coupon codes
     * @return array
     */
    public function get_generated_couponcodes()
    {
        return $this->generatorcodes;
    }

    /**
     * Generate a batch of coupons
     * @param \block_coupon\test\coupon\generatoroptions $options
     * @return bool
     */
    public function generate_coupons(generatoroptions $options)
    {
        $this->generatorids = array();
        $this->generatorcodes = array();
        // First, correct options.
//        $this->fix_options($options);
        // Validate options.
//        $this->validate_options($options);
        // And generate.
        return $this->generate($options);
    }
    /**
     * update a batch of coupons
     * @param \block_coupon\test\coupon\generatoroptions $options
     * @return bool
     */
    public function update_coupons(generatoroptions $options)
    {
        $this->generatorids = array();
        $this->generatorcodes = array();
        // First, correct options.
//        $this->fix_options($options);
        // Validate options.
//        $this->validate_options($options);
        // And generate.
        return $this->update($options);
    }

    /**
     * Fix generator options
     * @param \block_coupon\test\coupon\generatoroptions $options
     */
    protected function fix_options(generatoroptions $options)
    {
        global $USER;
        // If we have recipients, amount is the number of recipients.
        if (!empty($options->recipients)) {
            $options->amount = count($options->recipients);
        }
        // If the owner id hasn't been set, the user id will be the owner.
        if (empty($options->ownerid)) {
            $options->ownerid = $USER->id;
        }
    }

    /**
     * Validate generator options
     * @param \block_coupon\test\coupon\generatoroptions $options
     * @throws \moodle_exception
     */
    protected function validate_options(generatoroptions $options)
    {
        switch ($options->type) {
            case generatoroptions::COURSE:
            case generatoroptions::ENROLEXTENSION:
                if (empty($options->courses)) {
                    throw new \moodle_exception('err:no-courses', 'block_coupon');
                }
                // Validate courses.
                $this->validate_courses($options->courses, $options->groups);
                break;

            case generatoroptions::COHORT:
                if (empty($options->cohorts)) {
                    throw new \moodle_exception('err:no-cohorts', 'block_coupon');
                }
                // Validate cohorts.
                $this->validate_cohorts($options->cohorts);
                break;

            default:
                throw new \moodle_exception('err:no-courses', 'block_coupon');
                break; // Never reached.
        }
        // If we have recipients, we should also have an emailbody.
        if (!empty($options->recipients) && empty($options->emailbody)) {
            throw new \moodle_exception('error:no-emailbody', 'block_coupon');
        }
    }

    /**
     * Validate the configured courses (and optional coursegroups)
     *
     * @param array $courseids
     * @param array|null $groupids
     * @throws \moodle_exception
     */
    protected function validate_courses($courseids, $groupids = null)
    {
        global $DB;
        // Load courses.
        $this->courses = $DB->get_records_list('course', 'id', $courseids, 'id ASC', 'id, fullname');
        $errors = array();
        foreach ($courseids as $courseid) {
            if (!isset($this->courses[$courseid])) {
                $errors[] = get_string('error:course-not-found', 'block_coupon') . ' (id = ' . $courseid . ')';
            }
        }
        // Groups.
        if (!empty($groupids)) {
            $this->groups = $DB->get_records_list('groups', 'id', $groupids, 'id ASC', 'id, name');
            $errors = array();
            foreach ($groupids as $groupid) {
                if (!isset($this->groups[$groupid])) {
                    $errors[] = get_string('error:group-not-found', 'block_coupon') . ' (id = ' . $groupid . ')';
                }
            }
        }
        // Do we have errors?
        if (!empty($errors)) {
            throw new \block_coupon\exception('error:validate-courses', '', implode('<br/>', $errors));
        }
    }

    /**
     * Validate the configured cohorts
     *
     * @param array $cohortids
     * @throws \moodle_exception
     */
    protected function validate_cohorts($cohortids)
    {
        global $DB;
        // Load courses.
        $this->cohorts = $DB->get_records_list('cohort', 'id', $cohortids, 'id ASC', 'id, name');
        $errors = array();
        foreach ($cohortids as $cohortid) {
            if (!isset($this->cohorts[$cohortid])) {
                $errors[] = get_string('error:cohort-not-found', 'block_coupon') . ' (id = ' . $cohortid . ')';
            }
        }
        // Do we have errors?
        if (!empty($errors)) {
            throw new \moodle_exception('error:validate-cohorts', 'block_coupon', implode('<br/>', $errors));
        }
    }

    /**
     * Internally generate the coupons
     *
     * @param \block_coupon\test\coupon\generatoroptions $options
     * @return boolean
     * @throws \moodle_exception
     */
    protected function generate(generatoroptions $options)
    {
        global $DB;
        raise_memory_limit(MEMORY_HUGE);
        $errors = array();

        $objcoupon = new \stdClass();
        $objcoupon->title = $options->title;
        $objcoupon->description = $options->description;
        $objcoupon->filename = $options->image;
        $objcoupon->start_date = $options->startdate;
        $objcoupon->expire_date = $options->expiredate;
        $objcoupon->discount_rate = $options->discountrate;
        $objcoupon->key_code = $options->couponcode;
        $objcoupon->count = $options->quantity;
        $objcoupon->send_date = (!empty($options->senddate)) ? $options->senddate : null;
        $objcoupon->is_send = null;
        $objcoupon->redirect_url = (!empty($options->redirecturl)) ? $options->redirecturl : null;
        $objcoupon->email_body = null;
        $objcoupon->logo_id = (int)$options->logoid;
        $objcoupon->type_code = $options->type;
        $objcoupon->render_qrcode = ($options->qrcode) ? 1 : 0;
        $objcoupon->create_time = time();
        $objcoupon->modified_time = $options->modifiedtime;
        $objcoupon->expired_time = $options->expiredtime;

        // Insert coupon so we've got an id.
        if (!$objcoupon->id = $DB->insert_record('vi_coupons', $objcoupon)) {
            $errors[] = 'Failed to create general coupon object in database.';
        }

        if ($objcoupon->id) {
            $record = (object)array(
                'coupon_id' => $objcoupon->id,
                'course_id' => $options->courseid
            );

            // And insert in db.
            if (!$DB->insert_record('vi_coupons_courses', $record)) {
                $errors[] = 'Failed to create course link ' . $options->courseid. ' record for coupon id ' . $objcoupon->id . '.';
            }
        }

        if ($objcoupon->id && $objcoupon->render_qrcode === 1 && $options->qrcodeurl !== null) {

            $record = (object)array(
                'coupon_id' => $objcoupon->id,
                'qrcodeurl' => $options->qrcodeurl
            );

            if (!$DB->insert_record('vi_coupon_qrcode', $record)) {
                $errors[] = 'Failed to create course link ' . $options->qrcodeurl. ' record for coupon id ' . $objcoupon->id . '.';
            }
        }

        if (!empty($errors)) {
            throw new \moodle_exception('error:coupon:generator', 'local_management', implode('<br/>', $errors));
        }
        return $objcoupon->id;
    }
    /**
     * Internally generate the coupons
     *
     * @param \block_coupon\test\coupon\generatoroptions $options
     * @return boolean
     * @throws \moodle_exception
     */
    protected function update(generatoroptions $options)
    {
        global $DB;
        raise_memory_limit(MEMORY_HUGE);
        $errors = array();

        $objcoupon = new \stdClass();
        $objcoupon->id = $options->id;
        $objcoupon->title = $options->title;
        $objcoupon->description = $options->description;
        $objcoupon->filename = $options->image;
        $objcoupon->start_date = $options->startdate;
        $objcoupon->expire_date = $options->expiredate;
        $objcoupon->discount_rate = $options->discountrate;
        $objcoupon->key_code = $options->couponcode;
        $objcoupon->count = $options->quantity;
        $objcoupon->send_date = (!empty($options->senddate)) ? $options->senddate : null;
        $objcoupon->is_send = null;
        $objcoupon->redirect_url = (!empty($options->redirecturl)) ? $options->redirecturl : null;
        $objcoupon->email_body = null;
        $objcoupon->logo_id = (int)$options->logoid;
        $objcoupon->type_code = $options->type;
        $objcoupon->render_qrcode = ($options->qrcode) ? 1 : 0;
        $objcoupon->create_time = time();
        $objcoupon->modified_time = $options->modifiedtime;
        $objcoupon->expired_time = $options->expiredtime;
        // old coupon
        $oldcouponcourse=$DB->get_record('vi_coupons_courses',array('coupon_id'=>$options->id));
        // Insert coupon so we've got an id.
        if (!$objcoupon->id = $DB->update_record('vi_coupons', $objcoupon)) {
            $errors[] = 'Failed to update coupon object in database.';
        }

        if ($objcoupon->id) {
            $record = (object)array(
                'id'=>$oldcouponcourse->id,
                'coupon_id' => $options->id,
                'course_id' => $options->courseid
            );

            // And insert in db.
            if (!$DB->update_record('vi_coupons_courses', $record)) {
                $errors[] = 'Failed to update course link ' . $options->courseid. ' record for coupon id ' . $objcoupon->id . '.';
            }
        }
        //old coupon qrcode
        $oldcouponqrcode=$DB->get_record('vi_coupon_qrcode',array('coupon_id'=>$options->id));

        if ($oldcouponqrcode->id && $objcoupon->render_qrcode === 1 && $options->qrcodeurl !== null) {

            $record = (object)array(
                'id'=>$oldcouponqrcode->id,
                'coupon_id' => $options->id,
                'qrcodeurl' => $options->qrcodeurl
            );

                if (!$DB->update_record('vi_coupon_qrcode', $record)) {
                $errors[] = 'Failed to update course link ' . $options->qrcodeurl. ' record for coupon id ' . $objcoupon->id . '.';
            }
        }
        if($oldcouponqrcode && $objcoupon->render_qrcode === 0){
            $aaaaaaa=$DB->delete_records('vi_coupon_qrcode',array('coupon_id'=>$oldcouponqrcode->coupon_id));
        }
        if ($oldcouponqrcode===false && $objcoupon->render_qrcode === 1 && $options->qrcodeurl !== null) {

            $record = (object)array(
                'coupon_id' => $options->id,
                'qrcodeurl' => $options->qrcodeurl
            );

            if (!$DB->insert_record('vi_coupon_qrcode', $record)) {
                $errors[] = 'Failed to create course link ' . $options->qrcodeurl. ' record for coupon id ' . $objcoupon->id . '.';
            }
        }


        if (!empty($errors)) {
            throw new \moodle_exception('error:coupon:generator', 'local_management', implode('<br/>', $errors));
        }
        return $options->id;
    }

    /**
     * Generate the personalized email
     * @param string $template
     * @param \stdClass $coupon coupon record
     * @return string
     */
    protected function generate_email($template, $coupon)
    {
        global $SITE;
        $gendertxt = (!is_null($coupon->for_user_gender)) ? $coupon->for_user_gender : '';
        $extensionperiod = '';
        if ($coupon->typ == generatoroptions::ENROLEXTENSION) {
            if (empty($coupon->enrolperiod)) {
                $extensionperiod = get_string('enrolperiod:indefinite', 'block_coupon');
            } else {
                $extensionperiod = get_string('enrolperiod:extension', 'block_coupon', format_time($coupon->enrolperiod));
            }
        }

        // Replace some strings in the email body.
        $arrreplace = array(
            '##to_name##',
            '##site_name##',
            '##to_gender##',
            '##extensionperiod##',
        );
        $arrwith = array(
            $coupon->for_user_name,
            $SITE->fullname,
            $gendertxt,
            $extensionperiod
        );

        // Check if we're generating based on course, in which case we enter the course name too.
        if (isset($this->courses) && !empty($this->courses)) {

            $coursenames = array();
            foreach ($this->courses as $course) {
                $coursenames[] = $course->fullname;
            }

            $arrreplace[] = '##course_fullnames##';
            $arrwith[] = implode('<br/>', $coursenames);
        }

        return str_replace($arrreplace, $arrwith, $template);
    }

    /**
     * Insert coupon links for courses and optional coursegroups
     *
     * @param \stdClass $coupon coupon record
     * @param array $errors
     * @return bool true if valid, false if there's errors
     */
    protected function insert_coupon_courses($coupon, &$errors)
    {
        global $DB;
        $errors = array();
        foreach ($this->courses as $course) {
            // An object for each added course.
            $record = (object)array(
                'coupon_id' => $coupon->id,
                'course_id' => $course->id
            );
            // And insert in db.
            if (!$DB->insert_record('vi_coupons_courses', $record)) {
                $errors[] = 'Failed to create course link ' . $course->id . ' record for coupon id ' . $coupon->id . '.';
            }
        }
        if (!empty($this->groups)) {
            foreach ($this->groups as $group) {
                // An object for each added cohort.
                $record = (object)array(
                    'couponid' => $coupon->id,
                    'groupid' => $group->id
                );
                // And insert in db.
                if (!$DB->insert_record('vi_coupon_groups', $record)) {
                    $errors[] = 'Failed to create group link ' . $group->id . ' record for coupon id ' . $coupon->id . '.';
                }
            }
        }
        return !empty($errors);
    }

    /**
     * Insert coupon links for cohorts
     *
     * @param \stdClass $coupon coupon record
     * @param array $errors
     * @return bool true if valid, false if there's errors
     */
    protected function insert_coupon_cohorts($coupon, &$errors)
    {
        global $DB;
        $errors = array();
        foreach ($this->cohorts as $cohort) {
            // An object for each added cohort.
            $record = (object)array(
                'couponid' => $coupon->id,
                'cohortid' => $cohort->id
            );
            // And insert in db.
            if (!$DB->insert_record('block_coupon_cohorts', $record)) {
                $errors[] = 'Failed to create cohort link ' . $cohort->id . ' record for coupon id ' . $coupon->id . '.';
            }
        }
        return !empty($errors);
    }

}