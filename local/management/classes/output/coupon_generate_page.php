<?php
// Standard GPL and phpdocs
namespace local_management\output;

use renderable;
use renderer_base;
use templatable;
use stdClass;

class coupon_generate_page implements renderable, templatable {
    /** @var string $sometext Some text to show how to pass data to a template. */
    var $couponcode = null;
    var $savecouponurl = null;
    var $renderqrcodeurl = null;
    var $couponindexurl = null;
    var $user_emails = null;
    var $couponid=null;
    var $saved=null;

    public function __construct($sometext) {
        $this->couponcode = codegenerator::generate_unique_code(20);
        $this->savecouponurl = $sometext->savecouponurl;
        $this->renderqrcodeurl = $sometext->renderqrcodeurl;
        $this->couponindexurl = $sometext->couponindexurl;
        $this->user_emails = $sometext->user_emails;
        $this->couponid=$sometext->couponid;
        $this->saved=$sometext->saved;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
        global $DB,$CFG;
        $data = new stdClass();
        $data->couponcode = $this->couponcode;
        $data->savecouponurl = $this->savecouponurl;
        $data->couponindexurl = $this->couponindexurl;
        $data->user_emails = $this->user_emails;
        $data->saved = $this->saved;

        $coupon_infor=null;
        $course_infor=null;
        $data->coupon_infor= new stdClass();
        $data->course_infor= new stdClass();
        if($this->couponid){
            $coupon_infor=$DB->get_record('vi_coupons',array('id'=>$this->couponid));
            $data->couponcode=$coupon_infor->key_code;
            $coupon_infor->start_date=date_format_string($coupon_infor->start_date,'%Y-%m-%d');
            $coupon_infor->expire_date=date_format_string($coupon_infor->expire_date,'%Y-%m-%d');
            $data->coupon_infor=$coupon_infor;
            $courseid=$DB->get_record('vi_coupons_courses',array('coupon_id'=>$this->couponid),'course_id');
            $course_infor=local_course_class_id($courseid->course_id);
            $data->course_infor=$course_infor;
            $coupon_infor->imgurl=$CFG->wwwroot."/pluginfile.php/".$coupon_infor->filename;
            $coupon_infor->required='';

            if($coupon_infor->render_qrcode==1){
                $qrcode=$DB->get_record('vi_coupon_qrcode',array('coupon_id'=>$this->couponid));
                $cpqrcode = $CFG->wwwroot . '/pluginfile.php/'.$qrcode->qrcodeurl;
                $coupon_infor->render_qrcodeyes='checked';
                $coupon_infor->render_qrcodeno='';
                $coupon_infor->cpqrcode=$cpqrcode;
                $coupon_infor->cpqrcodestyle='display:block;';
            }else{
                $coupon_infor->render_qrcodeyes='';
                $coupon_infor->render_qrcodeno='checked';
                $coupon_infor->cpqrcodestyle='display:none;';
            }
        }else{
            $coupon_infor->discount_rate=40;
            $coupon_infor->count=40;
            $data->coupon_infor=$coupon_infor;
            $course_infor->id=-1;
            $course_infor->vi_coursename='Courses';
            $coupon_infor->imgurl='/local/management/coupon/asset/img/defaultimagepreview.png';
            $data->course_infor=$course_infor;
            $coupon_infor->required='rules=required';
            $coupon_infor->render_qrcode='checked';
            $coupon_infor->cpqrcodestyle='display:none;';
        }
        return $data;
    }
}