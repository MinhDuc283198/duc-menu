<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' =>  get_string('delivery:title','local_management'),
    'heading' => get_string('delivery:title','local_management'),
    'subheading' => '',
    'menu' => 'productdelivery',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');

$type = optional_param('type', 'main', PARAM_RAW);
$stype = optional_param('stype', '', PARAM_RAW);
$scate = optional_param('scate', '', PARAM_RAW);
$stext = optional_param('stext', '', PARAM_RAW);    
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);

$delivery = optional_param('delivery', 'all', PARAM_RAW);
$searctext = optional_param('searctext', '', PARAM_RAW);
$paymentdate_str = optional_param('paymentdate', '', PARAM_RAW);
$searctext = trim($searctext);
$where_array = array();
$wehre = '';

if ($delivery == 'all') {
    $wehre .= '';
}else if($delivery == '3'){
    $wehre .= " AND ( lp.delivery = 0 OR lp.delivery is NULL OR lp.delivery ='' )";
}else {
    $wehre .= " AND lp.delivery = $delivery ";
}

if($paymentdate_str){
    $learningstart = str_replace(".", "-", $paymentdate_str); 
    $learningstart = strtotime($learningstart);
    $learningend = $learningstart + 86400 - 1;
}

if($learningstart){
    $wehre .=  " AND lpy.timemodified  >= $learningstart AND lpy.timemodified <= $learningend";
}

if ($searctext) {
    $wehre .= " AND (u.firstname like '%$searctext%'  OR lc.title like '%$searctext%' OR lc.en_title like '%$searctext%' OR lc.vi_title like '%$searctext%' OR lpy.paycode = '$searctext') ";
}

$perpages = [10, 20, 30, 50];

$select_cnt = " select count(lp.id) ";
$select = " select lp.id,lpy.paycode paycode, lp.payid, lp.address, lp.cell, lp.timemodified as lptimemodified, lc.title, lc.vi_title, lc.en_title,u.id as userid , u.firstname, u.lastname, lc.code, lp.delivery , lpy.timemodified as lpytimemodified, lpy.id as lpyid"
        . ", tb.title as booktitle, tb.vi_title as vi_booktitle, tb.en_title as en_booktitle, tb.code as bookcode ";
$query = "
from {lmsdata_productdelivery} lp
JOIN {lmsdata_class} lc ON lc.id = lp.bookid 
JOIN {lmsdata_payment} lpy ON lp.payid = lpy.id 
JOIN {user} u ON u.id = lpy.userid 
JOIN {lmsdata_user} lu ON u.id = lu.userid
LEFT JOIN {lmsdata_textbook} tb ON tb.id = lc.bookid
WHERE lpy.status = 1  $wehre
";
$orderby = " ORDER BY lpy.timemodified DESC, lp.id DESC";
$contents = $DB->get_records_sql($select . $query . $orderby,array() ,($page - 1) * $perpage, $perpage);

$total = $DB->count_records_sql($select_cnt . $query);

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <div>
                    <form id="search_form" action="./index.php">
                        <input type="hidden" name="excel" value ="0">
                        <input type="hidden" name="page" value="1" />
                        <input type="hidden" name="perpage" value="<?php echo $perpage?>" />
                        <ul class="mk-c-tab tab-event" id="delivery_status">
                            <li style="width:20%" <?php if ($delivery == 'all') { ?>class="on"<?php } ?>><a href="#" data-type="all"><?php echo get_string('delivery:all','local_management')?></a></li>
                            <li style="width:20%" <?php if ($delivery == '3') { ?>class="on"<?php } ?>><a href="#" data-type='3'><?php echo get_string('delivery:preparing','local_management')?></a></li>
                            <li style="width:20%" <?php if ($delivery == '2') { ?>class="on"<?php } ?>><a href="#" data-type='2'><?php echo get_string('delivery:shipping','local_management')?></a></li>
                            <li style="width:20%" <?php if ($delivery == '1') { ?>class="on"<?php } ?>><a href="#" data-type='1'><?php echo get_string('delivery:completed','local_management')?></a></li>
                        </ul>
                        <div>
                            <input type="hidden" name="delivery" value="<?php echo $delivery ?>">
                            <input type="text" name="searctext" placeholder="<?php echo get_string('delivery:searchtext','local_management')?>" value="<?php echo $searctext ?>">
                            <input type="text" name="paymentdate" placeholder="<?php echo get_string('delivery:searchtext2','local_management')?>" class="w_120 date s_date" value="<?php echo $paymentdate_str?>">
                            <input type="submit" value="<?php echo get_string('search','local_management')?>" >
                            <input type="button" value="<?php echo get_string('reset','local_management')?>" id="reset">
                        </div>
                    </form>
                </div>
                <div>
                    <span class='left'>
                        <span><?php echo get_string('total','local_management',$total)?></span>
                        <span>
                            <select name="perpage2" class="perpage2">
                                <?php foreach ($perpages as $perp) { ?>
                                    <option value="<?php echo $perp ?>" <?php echo $perpage == $perp ? 'selected' : '' ?>><?php echo $perp ?><?php echo get_string('viewmore','local_management')?></option>
                                <?php } ?>
                            </select>
                        </span>
                    </span>
                    <span class="btn_area">
                        <span><?php echo get_string('delivery:change','local_management')?></span>
                        <input type="button" value="<?php echo get_string('delivery:preparing','local_management')?>" onclick="delivery_stats(0)">
                        <input type="button" value="<?php echo get_string('delivery:shipping','local_management')?>" onclick="delivery_stats(2)">
                        <input type="button" value="<?php echo get_string('delivery:completed','local_management')?>" onclick="delivery_stats(1)">
                        
                        <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="excel_download();"/> 
                    </span>
                </div>
                <div class = "tab-content">

                    <div id = "data1" class = "tab-pane fade in active">
                        <table>
                            <thead class = "thead-light">
                                <tr>
                                    <th width="50px">
                                        <input type = "checkbox" class = "checkall" />
                                    </th>
                                    <th width="6%"><?php echo get_string('number', 'local_management') ?></th>
                                    <th width="6%"><?php echo get_string('ordernumber', 'local_management') ?></th>
                                    <th><?php echo get_string('pay', 'local_management').get_string('goodsname', 'local_management') ?></th>
                                    <th><?php echo get_string('pay', 'local_management').get_string('goodscode', 'local_management') ?></th>
                                    <th><?php echo get_string('bookname', 'local_management') ?></th>
                                    <th><?php echo get_string('book', 'local_management').get_string('goodscode', 'local_management') ?></th>
                                    <th><?php echo get_string('paymentdate', 'local_management') ?></th>
                                    <th><?php echo get_string('orderer', 'local_management') ?></th>
                                    <th><?php echo get_string('address', 'local_management') ?></th>
                                    <th><?php echo get_string('contact', 'local_management') ?></th>
                                    <th><?php echo get_string('deliverystatus', 'local_management') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($contents) {
                                    $startnum = $total - (($page - 1) * $perpage);
                                    foreach ($contents as $content) {
                                        ?>
                                        <tr>
                                            <td>
                                                <input type = "checkbox" class="conslist" name="conslist"  conid="<?php echo $content->id ?>"/>
                                            </td>
                                            <td><?php echo $startnum-- ?></td>
                                            <td><?php echo $content->paycode ?></td>
                                            <?php
                                            switch (current_language()){
                                                case 'ko' :
                                                    $category_name = $content->title;
                                                    break;
                                                case 'en' :
                                                    $category_name = $content->en_title;
                                                    break;
                                                case 'vi' :
                                                    $category_name = $content->vi_title;
                                                    break;
                                            }
                                            ?>
                                            <td><?php echo $category_name ?></td>
                                            <td><?php echo $content->code ?></td>
                                            <td><?php
                                            switch (current_language()){
                                                case 'ko' :
                                                    $bookname = $content->booktitle;
                                                    break;
                                                case 'en' :
                                                    $bookname = $content->en_booktitle;
                                                    break;
                                                case 'vi' :
                                                    $bookname = $content->vi_booktitle;
                                                    break;
                                            }
                                            echo $bookname; ?></td>
                                            <td><?php echo $content->bookcode ?></td>
                                            <td><?php echo date('Y.m.d',$content->lpytimemodified) ?> </td>
                                            <td><?php echo $content->firstname . ' ' . $content->lastname ?> </td>
                                            <td><?php echo $content->address ?></td>
                                            <td><?php echo $content->cell ?></td>
                                            <td>
                                                <?php
                                                switch ($content->delivery) {
                                                    case 0 :
                                                        $delivery_txt = get_string('delivery:preparing','local_management');
                                                        break;
                                                    case 1:
                                                        $delivery_txt = get_string('delivery:completed','local_management');
                                                        break;
                                                    case 2:
                                                        $delivery_txt = get_string('delivery:shipping','local_management');
                                                        break;
                                                }
                                                ?>
                                                <a data-toggle="modal" data-target="#modal-add-course-origin" data-id="<?php echo $content->id ?>" data-userid="<?php echo $content->userid ?>"><?php echo $delivery_txt ?></a>
                                                <br>
                                                <?php echo date('Y.m.d',$content->lptimemodified); ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    ?>
                                <td colspan="12"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                            <?php } ?> 
                            </tbody>
                        </table>
                        <nav aria-label = "Page navigation" class = "text-center">
                            <ul class = "pagination">
                                <?php echo $paging ?>
                            </ul>
                        </nav>
                    </div>
                </div>
                <?php
                if (($total / $perpage) > 1) {
                    print_paging_navbar_script($total, $page, $perpage, 'javascript:cata_page(:page);');
                }

                $query_string = '';
                if (!empty($excel_params)) {
                    $query_array = array();
                    foreach ($excel_params as $key => $value) {
                        $query_array[] = urlencode($key) . '=' . urlencode($value);
                    }
                    $query_string = '?' . implode('&', $query_array);
                }
            ?>  
            </div>
            
        </div>
    </div>

</section>
<!--/.content -->


<?php
include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">
    function excel_download() {
        var form = $('#search_form');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
         return false;
    }
    function cata_page(page) {
        $('[name=page]').val(page);
        $('#search_form').submit();
    }
    start_datepicker_c('', '');
    function delivery_stats(stats) {
        var length = $('input[name=conslist]:checked').length;
        if (length > 0) {
            if (confirm('<?php echo get_string('delivery:text','local_management')?>')) {
                var ids = new Array();
                $('input[name=conslist]:checked').each(function (i, v) {
                    ids.push($(v).attr("conid"));
                });

                $.ajax({
                    type: 'POST',
                    url: 'delivery.ajax.php',
                    data: {ids: ids, type: stats},
                    success: function (data, status, xhr) {
                        alert("<?php echo get_string('delivery:total','local_management')?> " + data + "<?php echo get_string('delivery:text2','local_management')?>");
                        location.reload();
                    },
                });
            }
        } else {
            alert("<?php echo get_string('delivery:text3','local_management')?>");
        }
    }
    $(document).ready(function () {
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=search_form]").submit();
        });
    
        $('#reset').click(function () {
            $('input[name=delivery]').val('all');
            $('input[name=searctext]').val('');
            $('input[name=paymentdate]').val('');
            $("form[id=search_form]").submit();
        });

        $('#delivery_status li a').click(function () {
            var delivery = $(this).data('type');
            $('input[name=delivery]').val(delivery);
            $('#search_form').submit();
        })
        $('#modal-add-course-origin').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var deleteUrl = button.data('id');
            var userid = button.data('userid');

            var result = call_ajax('list', {id: deleteUrl, userid: userid}, false);
            if (result.status == 'success') {
                if (result.contents) {
                    var nowlang = '<?php echo current_language()?>';
                    
                    $('#p_orderid').text(result.contents.payid);
                    
                    switch (nowlang){
                        case 'ko' :
                            $title = result.contents.title;
                            break;
                        case 'en' :
                            $title = result.contents.en_title;
                            break;
                        case 'vi' :
                            $title = result.contents.vi_title;
                            break;
                    }
                                                                
                    $('#p_goodsname').text($title + '(' + result.contents.code + ')');
                    $('#p_orderuser').text(result.contents.firstname + '(' + result.contents.username + ')');
                    $('#p_address').text(result.contents.address);
                    $('#p_cell').text(result.contents.cell);
                }

                $tbody = $('#modal-add-course-origin #course-origin-list table tbody');
                $tbody.empty();
                if (result.total_cnt > 0) {
                    var $i = result.total_cnt;
                    $.each(result.list, function () {
                        $tr = $('<tr>');
                        $tr.data(this);
                        var $num = $('<td>').text($i);
                        var $tdFullName = $('<td>');
                        var $fullname = $('<span>').addClass('txt').text(this.delivery_txt);
                        $tdFullName.append($fullname);

                        var $tdEmail = $('<td>').text(this.timecreated_txt);
                        var $tdUser = $('<td>').text(this.firstname + '(' + this.username + ')');

                        $tr.append($num).append($tdFullName).append($tdEmail).append($tdUser);
                        $tbody.append($tr);
                        $i--;
                    });
                } else {
                    $tbody.append('<tr><td colspan="4"><?php echo get_string('delivery:no_data','local_management')?></td></tr>');
                }
            }
        })
    });

    function call_ajax(action, data, async) {
        var rvalue = false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            async: async,
            data: {action: action, data: data},
            success: function (result) {
                rvalue = result;
            },
            error: function (xhr, status, error) {
            }
        });

        return rvalue;
    }

</script>



<div class="modal modal-default fade" id="modal-add-course-origin">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
                    <span aria-hidden="true">&times;</span></button>
                <input type="hidden" name="modal_id">
                <h4 class="modal-title"><?php echo get_string('delivery:histroy','local_management')?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <table class="table table-hover">
                            <tr><th><?php echo get_string('ordernumber', 'local_management') ?></th><td id="p_orderid"></td></tr>
                            <tr><th><?php echo get_string('goodsname', 'local_management') ?>(<?php echo get_string('code', 'local_management') ?>)</th><td id="p_goodsname"></td></tr>
                            <tr><th><?php echo get_string('orderer', 'local_management') ?></th><td id="p_orderuser"></td></tr>
                            <tr><th><?php echo get_string('address', 'local_management') ?></th><td id="p_address"></td></tr>
                            <tr><th><?php echo get_string('contact', 'local_management') ?></th><td id="p_cell"></td></tr>
                        </table>
                    </div>
                    <div class="col-md-12" id="course-origin-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('delivery:status', 'local_management')?></th>
                                    <th><?php echo get_string('delivery:processingdate', 'local_management')?></th>
                                    <th><?php echo get_string('delivery:manager', 'local_management')?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management') ?></button>
            </div>
        </div>
    </div>
</div>