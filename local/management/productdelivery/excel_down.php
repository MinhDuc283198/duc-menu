<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/local/management/textbook/lib.php');

$type = optional_param('type', 'main', PARAM_RAW);
$stype = optional_param('stype', '', PARAM_RAW);
$scate = optional_param('scate', '', PARAM_RAW);
$stext = optional_param('stext', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);

$delivery = optional_param('delivery', 'all', PARAM_RAW);
$searctext = optional_param('searctext', '', PARAM_RAW);
$paymentdate_str = optional_param('paymentdate', '', PARAM_RAW);

$where_array = array();
$wehre = '';
if ($delivery) {
    if ($delivery == 'all') {
        $wehre .= '';
    } else {
        $wehre .= " AND lp.delivery = $delivery ";
    }
}

if($paymentdate_str){
    $learningstart = str_replace(".", "-", $paymentdate_str); 
    $learningstart = strtotime($learningstart);
    $learningend = $learningstart + 86400 - 1;
}

if($learningstart){
    $wehre .=  " AND lpy.timemodified  >= $learningstart AND lpy.timemodified <= $learningend";
}

if ($searctext) {
    $wehre .= " AND (u.firstname like '%$searctext%' OR lp.payid like '%$searctext%' OR lc.title like '%$searctext%' OR lc.en_title like '%$searctext%' OR lc.vi_title like '%$searctext%') ";
}

$select_cnt = " select count(lp.id) ";
$select = " select lp.id, lp.payid, lp.address, lp.cell, lp.timemodified as lptimemodified, lc.title, lc.vi_title, lc.en_title,u.id as userid , u.firstname, u.lastname, lc.code, lp.delivery , lpy.timemodified as lpytimemodified ";
$query = "
from m_lmsdata_productdelivery lp
JOIN m_lmsdata_class lc ON lc.id = lp.bookid 
JOIN m_lmsdata_payment lpy ON lp.payid = lpy.id 	
JOIN m_user u ON u.id = lpy.userid 
JOIN m_lmsdata_user lu ON u.id = lu.userid
WHERE lpy.status = 1  $wehre
";
$orderby = " ORDER BY lpy.timemodified DESC, lp.id DESC";

$books = $DB->get_records_sql($select . $query . $orderby,array());
$total = $DB->count_records_sql($select_cnt . $query);

    $fields = array(
        get_string('number', 'local_management'),
        get_string('ordernumber', 'local_management'),
        get_string('goodsname', 'local_management') .'('. get_string('code', 'local_management').')',
        get_string('paymentdate', 'local_management'),
        get_string('orderer', 'local_management'),
        get_string('address', 'local_management'),
        get_string('contact', 'local_management'),
        get_string('deliverystatus', 'local_management')
    );

    $date = date('Y-m-d', time());
    $filename = '배송관리_' . $date. '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    $num = 1;
    foreach ($books as $book) {
        $col = 0;

        $worksheet[0]->write($row, $col++, $num++);
        switch (current_language()){
            case 'ko' :
                $category_name = $book->title;
                break;
            case 'en' :
                $category_name = $book->en_title;
                break;
            case 'vi' :
                $category_name = $book->vi_title;
                break;
        }
        $worksheet[0]->write($row, $col++, $book->payid);
        $worksheet[0]->write($row, $col++, $category_name .'(' .$book->code.')');
        $worksheet[0]->write($row, $col++, date('Y.m.d',$book->lpytimemodified));
        $worksheet[0]->write($row, $col++, $book->firstname );
        $worksheet[0]->write($row, $col++, $book->address);
        $worksheet[0]->write($row, $col++, $book->cell);
        switch ($book->delivery) {
            case 0 :
                $delivery_txt = get_string('delivery:preparing','local_management');
                break;
            case 1:
                $delivery_txt = get_string('delivery:completed','local_management');
                break;
            case 2:
                $delivery_txt = get_string('delivery:shipping','local_management');
                break;
        }
        $worksheet[0]->write($row, $col++, $delivery_txt.'<br>'.date('Y.m.d',$book->lptimemodified));
        
        $row++;
    }

    $workbook->close();
    die;

    