<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/courses/lib.php');


$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'list':
        $data = required_param_array('data', PARAM_RAW);

        $select = " select lp.id, lp.payid, lpy.userid, lp.address, lp.cell, lp.timemodified as lptimemodified, lc.title, lc.vi_title, lc.en_title,  u.firstname, u.lastname,u.username, lc.code, lp.delivery , lpy.timemodified as lpytimemodified ";
        $query = "
        from m_lmsdata_productdelivery lp
        JOIN m_lmsdata_class lc ON lc.id = lp.bookid 
        JOIN m_lmsdata_payment lpy ON lp.payid = lpy.id 	
        JOIN m_user u ON u.id = lpy.userid 
        JOIN m_lmsdata_user lu ON u.id = lu.userid
        WHERE lpy.status = 1 AND lpy.userid = {$data['userid']}  AND lp.id = {$data['id']} 
        ";
        $orderby = " ORDER BY lptimemodified DESC";
        $contents = $DB->get_record_sql($select.$query.$orderby);

        
        $l_select = " select lpl.*,u.firstname, u.username   ";
        $l_select_cnt = " select count(lpl.id) ";
        $l_query = " FROM {lmsdata_productdelivery_log} lpl 
                     JOIN {lmsdata_productdelivery} lp ON lpl.pdid = lp.id
                     JOIN m_lmsdata_payment lpy ON lp.payid = lpy.id 
                     JOIN m_user u ON u.id = lpl.userid 
                     JOIN m_lmsdata_user lu ON u.id = lu.userid
                     WHERE lpy.status = 1 AND lpy.userid = {$data['userid']} AND lp.id = {$data['id']}";
        $l_orderby = " ORDER BY lpl.timecreated DESC, lpl.id DESC ";
        $list = $DB->get_records_sql($l_select.$l_query.$l_orderby);
        $total_cnt = $DB->count_records_sql($l_select_cnt.$l_query);
        
        if($total_cnt>0){
            foreach($list as $key => $val){
                switch ($val->delivery) {
                    case 0 :
                        $list[$key]->delivery_txt = get_string('delivery:preparing','local_management');
                        break;
                    case 1 :
                        $list[$key]->delivery_txt = get_string('delivery:completed','local_management');
                        break;
                    case 2 :
                        $list[$key]->delivery_txt = get_string('delivery:shipping','local_management');
                        break;
                }
                $list[$key]->timecreated_txt = date('Y.m.d', $val->timecreated);
            }
        }
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->contents = $contents;
        $rvalue->list = $list;
        $rvalue->total_cnt = $total_cnt;
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();