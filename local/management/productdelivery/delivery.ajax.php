<?php
require_once(__DIR__ . '/../../../config.php');

$type = optional_param('type', 0,PARAM_INT);      //0:준비, 1:완료
$ids = optional_param_array('ids', '', PARAM_RAW);

$btype = 0;
if($type == 0){
    $btype = 1;
}else{
    $btype = 0;
}

$i = 0;
foreach ($ids as $id) {
//    $sql = "SELECT * FROM {lmsdata_productdelivery} WHERE id=:id and delivery=:btype";
//    $temp = $DB->get_record_sql($sql, array("id"=>$id, "btype"=>$btype));
    $sql = "SELECT * FROM {lmsdata_productdelivery} WHERE id=:id";
    $temp = $DB->get_record_sql($sql, array("id"=>$id));
    if($temp->delivery != $type){
        $newdata->delivery = $type;
        $newdata->id = $id;
        $newdata->timemodified = time();
        $DB->update_record('lmsdata_productdelivery', $newdata);
        
        $logdata->userid = $USER->id;
        $logdata->pdid = $id;
        $logdata->delivery = $type;
        $logdata->timecreated = time();
        $DB->insert_record('lmsdata_productdelivery_log', $logdata);
        $i++;
    }
}

echo $i;