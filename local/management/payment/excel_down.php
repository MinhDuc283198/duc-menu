<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");

$stext = optional_param('stext', '', PARAM_RAW);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);
$type = optional_param_array('type', array(), PARAM_RAW);
$paymethod = optional_param_array('paymethod', array(), PARAM_RAW);
$refundtype = optional_param('refundtype', 'all', PARAM_RAW);
$status = optional_param_array('status', array(), PARAM_RAW);
$status1 = optional_param('status1', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$order_date = optional_param('order_date', '', PARAM_RAW);
$stext = trim($stext);

if ($learningstart_str) {
    $learningstart = str_replace(".", "-", $learningstart_str);
    $learningstart = strtotime($learningstart);
}
if ($learningend_str) {
    $learningend = str_replace(".", "-", $learningend_str);
    $learningend = strtotime($learningend);
}
if ($learningstart) {
    $sql_where[] = " $order_date >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if ($learningend) {
    $sql_where[] = " $order_date <=:learningend ";
    $params['learningend'] = $learningend + 86400 - 1;
}
if ($refundtype) {
    if ($refundtype == "ing") {
        $sql_where[] = " lr.refundtype = :refundtype ";
        $params['refundtype'] = 0;
    } else if ($refundtype == "com") {
        $sql_where[] = " lr.refundtype = :refundtype ";
        $params['refundtype'] = 1;
    }
}

if ($type) {
    foreach ($type as $tkey => $tval) {
        if ($tval == 3) {
            $sql_where_type[] = " ( lc.type = :type" . $tkey . " AND lpd.delivery is not null )";
            $params['type' . $tkey] = $tval;
        } else if ($tval == 1) {
            $sql_where_type[] = " ( lc.type = :type" . $tkey . " AND lpd.delivery is null )";
            $params['type' . $tkey] = $tval;
        } else {
            $sql_where_type[] = " lc.type = :type" . $tkey;
            $params['type' . $tkey] = $tval;
        }
    }
    if (!empty($sql_where_type)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_type) . ')';
    } else {
        $sql_where[] = '';
    }
}
if ($status1) {
    $sql_where[] = " lp.status = :status  AND lp.paymethod = 'directdeposit' ";
    $params['status'] = 0;
}
if ($status) {
    foreach ($status as $tkey => $tval) {
        if ($tval == 'normal') {
            //정상승인 //무통X
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod != 'directdeposit' )";
            $params['status' . $tkey] = 1;
        } else if ($tval == 'waiting') {
            //입금대기
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod = 'directdeposit' )";
            $params['status' . $tkey] = 0;
        } else if ($tval == 'comfirm') {
            //입금확인
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod = 'directdeposit' )";
            $params['status' . $tkey] = 0;
        } else if ($tval == 'cancel') {
            //승인취소  //무통X
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod = 'directdeposit' AND lr.refundtype = 1 )";
            $params['status' . $tkey] = 1;
        }
    }
    if (!empty($sql_where_status)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_status) . ')';
    } else {
        $sql_where[] = '';
    }
}

if ($paymethod) {
    foreach ($paymethod as $tkey => $tval) {
        $sql_where_paymethod[] = " lp.paymethod = :paymethod" . $tkey;
        $params['paymethod' . $tkey] = $tval;
    }
    if (!empty($sql_where_paymethod)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_paymethod) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($stext)) {
    //주문번호
    $sql_where_search[] = $DB->sql_like('lp.paycode', ':id');
    $params['id'] = '%' . $stext . '%';
    //상품명
    $sql_where_search[] = $DB->sql_like('lc.title', ':title');
    $params['title'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.en_title', ':en_title');
    $params['en_title'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.vi_title', ':vi_title');
    $params['vi_title'] = '%' . $stext . '%';
    //상품코드
    $sql_where_search[] = $DB->sql_like('lc.code', ':code');
    $params['code'] = '%' . $stext . '%';
    //주문자
    $sql_where_search[] = $DB->sql_like('u.firstname', ':firstname');
    $params['firstname'] = '%' . $stext . '%';
    $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}


$payselect = "select lp.*,u.firstname,u.lastname, lc.title, lc.vi_title, lc.en_title, lc.price, lc.code, lpd.delivery, tb.id  tbid, lc.type lctype, lc.courseid, lr.id lrid, lr.completedate, lr.refundtype   ";
$payfrom = " from {lmsdata_payment} lp
                join {user} u on lp.userid = u.id
                join {lmsdata_class} lc on lc.id = lp.ref
                left join {lmsdata_productdelivery} lpd on lpd.payid = lp.id  
                LEFT JOIN {lmsdata_textbook} tb ON tb.id = lc.bookid
                LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id ";
$payorder = " ORDER BY lp.timecreated DESC, lp.id DESC";

$patlists = $DB->get_records_sql($payselect . $payfrom . $sql_where . $payorder, $params);
$total = $DB->count_records_sql("SELECT COUNT(lp.id) $payfrom $sql_where", $params);

$paystatus = [
    0 => get_string('beforepay', 'local_management'),
    1 => get_string('successpay', 'local_management'),
    2 => get_string('failpay', 'local_management'),
    3 => get_string('beforecancel', 'local_management'),
    4 => get_string('successcancel', 'local_management')
];
$paymethod_array = [
    "momo" => "MoMo AIO Payment",
    "domestic" => "Domestic card (ATM Card)",
    "international" => "International card (Visa, Master, Amex, JCB)",
    "directdeposit" => "무통장 입금"
];
$status_array = [
    "normal" => get_string('normal', 'local_management'),
    "waiting" => get_string('waiting', 'local_management'),
    "comfirm" => get_string('confirmpayment', 'local_management'),
    "cancel" => get_string('approval', 'local_management'),
];

$type_array = array('1' => get_string('course', 'local_management'), '2' => get_string('book', 'local_management'), '3' => get_string('course', 'local_management') . '+' . get_string('book', 'local_management'));

// =======================

$fields = array(
    get_string('number', 'local_management'),
    get_string('goods:type', 'local_management'),
    get_string('ordernumber', 'local_management'),
    get_string('goodsname', 'local_management'),
    get_string('goodscode', 'local_management'),
    get_string('orderer', 'local_management'),
    get_string('amount', 'local_management'),
    get_string('order:date', 'local_management'),
    get_string('paycondate', 'local_management'),
    get_string('paymentprice', 'local_management'),
    get_string('paymentmethod', 'local_management'),
    get_string('refundstatus', 'local_management'),
    get_string('refunddate', 'local_management')
);

$date = date('Y-m-d', time());
$filename = '결제관리_' . $date . '.xls';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);

$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');
$col = 0;
foreach ($fields as $fieldname) {
    $worksheet[0]->write(0, $col, $fieldname);
    $col++;
}

$row = 1;
$num = 1;
foreach ($patlists as $patlist) {
    $col = 0;
    switch (current_language()) {
        case 'ko':
            $paytitle = $patlist->title;
            break;
        case 'en':
            $paytitle = $patlist->en_title;
            break;
        case 'vi':
            $paytitle = $patlist->vi_title;
            break;
    }

    $status_txt = "";
    if ($patlist->lrid) {
        switch ($patlist->refundtype) {
            case 0:
                $status_txt = get_string('proceeding', 'local_management');
                break;
            case 1:
                $status_txt = get_string('procomplete', 'local_management');
                break;
        }
    }

    $type_txt = '';

    if ($patlist->lctype == 1) {
        $type_txt = get_string('course', 'local_management');
    } else if ($patlist->lctype == 3) {
        $type_txt = get_string('course', 'local_management') . '+' . get_string('book', 'local_management');
    } else {
        $type_txt = get_string('book', 'local_management');
    }
    $price_txt = number_format($patlist->price);
    if (($patlist->forced == 1)) {
        $price_txt = get_string('free', 'local_management');
    }

    $worksheet[0]->write($row, $col++, $num++);
    $worksheet[0]->write($row, $col++, $type_txt);
    $worksheet[0]->write($row, $col++, $patlist->paycode);
    $worksheet[0]->write($row, $col++, $paytitle);
    $worksheet[0]->write($row, $col++, $patlist->code);
    $worksheet[0]->write($row, $col++, $patlist->firstname." ".$patlist->lastname);
    $worksheet[0]->write($row, $col++, $price_txt);
    $worksheet[0]->write($row, $col++, date('Y.m.d',$patlist->timecreated));
    $worksheet[0]->write($row, $col++, ($patlist->status != 0) ? date('Y.m.d',$patlist->timemodified) : '-');

    // $period_total_sale_sql = '';
    // if ($learningstart) {
    //     $period_total_sale_sql = "{$order_date} >= {$learningstart} AND ";
    // }
    // if ($learningend) {
    //     $period_total_sale_sql = "{$order_date} <= {$learningend} AND ";
    // }
    // $total_sale_sql = "SELECT count(lc.price)
    //     FROM {lmsdata_payment} lp
    //     LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
    //     LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
    //     LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
    //     WHERE {$period_total_sale_sql} lc.courseid = '{$patlist->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
    
    // $worksheet[0]->write($row, $col++, number_format($DB->count_records_sql($total_sale_sql)));
    if ($patlist->forced == 1) {
        $worksheet[0]->write($row, $col++, 0);
    } else {
        $worksheet[0]->write($row, $col++, $price_txt);
    }
    
    $worksheet[0]->write($row, $col++, $patlist->paymethod);
    $worksheet[0]->write($row, $col++, ($status_txt) ? $status_txt : '-');
    $worksheet[0]->write($row, $col++, ($patlist->completedate) ? date('Y.m.d',$patlist->completedate) : '-');

    $row++;
}

$workbook->close();
die;
