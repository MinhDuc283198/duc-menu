<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');


$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'refund_save':
        $data = required_param_array('data', PARAM_RAW);
        
        $data['refunddate'] = strtotime(str_replace('.','-',$data['refunddate']));


        $lmsdata = new stdClass();
        $lmsdata->payid = $data['payid'];
        $lmsdata->refunddate = $data['refunddate'];
        $lmsdata->userid = $USER->id;
        $lmsdata->text = $data['text'];
        $lmsdata->type = $data['lrtype'];
        $lmsdata->price = $data['price'];
        $lmsdata->refundtype = 0;
        // 무통장 입금일경우, 환불계좌 정보 추가
        if($lmsdata->type == 1){
            $lmsdata->bank = $data['bank'];
            $lmsdata->refundbanknr = $data['refundbanknr'];
        }

        if($data['id']){
            $lmsdata->id = $data['id'];
            $lmsdata->timemodified = time();
            $DB->update_record('lmsdata_refund', $lmsdata);
        }else{
            $lmsdata->timecreated = time();
            $DB->insert_record('lmsdata_refund', $lmsdata);
        }
        
        $rvalue = new stdClass();
        $rvalue->status = 'success';

        echo json_encode($rvalue);
        break;
    case 'delete' :
        $data = required_param_array('data', PARAM_RAW);
        
        if($data['id']){
            $DB->delete_records('lmsdata_refund', array('id'=>$data['id']));
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        break; 
    case 'refund_go':
        $data = required_param_array('data', PARAM_RAW);
        $list = $DB->get_record('lmsdata_refund', array('id' => $data['id']));
        
        $lmsdata = new stdClass();
        $lmsdata->id = $data['id'];
        $lmsdata->completedate = time();
        $lmsdata->timemodified = time();
            
        if($list->type == 1){
            $lmsdata->refundtype = 1;
        }else{
            $lmsdata->refundtype = 4;
        }
        $DB->update_record('lmsdata_refund', $lmsdata);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->id = $lmsdata;
        echo json_encode($rvalue);
        break; 
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();