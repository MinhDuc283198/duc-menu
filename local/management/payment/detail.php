<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '주문/결제 상세내역',
    'heading' => '주문/결제 상세내역',
    'subheading' => '',
    'menu' => 'payment',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');

$id = optional_param('id', 0, PARAM_INT);
$payselect = "select lp.*,u.firstname,u.lastname,u.username, lc.title, lc.vi_title, lc.en_title, lc.price, lc.code, lpd.delivery, lpd.address, lpd.cell, tb.id  tbid, lc.type lctype, lr.id lrid, lr.refunddate, lr.text lrtext, lr.type lrtype, lr.price lrprice, lr.bank, lr.refundbanknr, lr.completedate ";
$payfrom = " from {lmsdata_payment} lp
                join {user} u on lp.userid = u.id
                join {lmsdata_class} lc on lc.id = lp.ref
                left join {lmsdata_productdelivery} lpd on lpd.payid = lp.id  
                LEFT JOIN {lmsdata_textbook} tb ON tb.id = lc.bookid
                LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                WHERE lp.id = :id ";
$content = $DB->get_record_sql($payselect.$payfrom, array("id"=>$id));

$type_txt = '';
if($content->lctype == 1){
    $type_txt = get_string('course', 'local_management');
}else if($content->lctype == 3){
        $type_txt = get_string('course', 'local_management') .'+' .get_string('book', 'local_management');
}else{
    $type_txt = get_string('book', 'local_management');
}
$paytitle = '';
switch (current_language()){
    case 'ko' :
        $paytitle = $content->title;
        break;
    case 'en' :
        $paytitle = $content->en_title;
        break;
    case 'vi' :
        $paytitle = $content->vi_title;
        break;
}
$price_txt = number_format($content->price).' VND';
if(($content->forced == 1)){
    $price_txt = get_string('free', 'local_management');
}
$deliverry = get_string('waiting','local_management');

if($content->status != 0){
    switch ($content->delivery) {
        case 0 :
            $deliverry = get_string('delivery:preparing','local_management');
            break;
        case 1:
            $deliverry = get_string('delivery:completed','local_management');
            break;
        case 2:
            $deliverry = get_string('delivery:shipping','local_management');
            break;
    }
}

$paystatus = [ 0 => get_string('waiting','local_management'),
               1 => get_string('successpay', 'local_management'),
               2 => get_string('failpay', 'local_management'),
               3 => get_string('beforecancel', 'local_management'),
               4 => get_string('successcancel', 'local_management')];

$banks['003'] = "기업은행";
$banks['004'] = "국민은행";
$banks['011'] = "농협중앙회";
$banks['012'] = "단위농협";
$banks['020'] = "우리은행";
$banks['031'] = "대구은행";
$banks['005'] = "외환은행";
$banks['023'] = "SC제일은행";
$banks['032'] = "부산은행";
$banks['045'] = "새마을금고";
$banks['027'] = "한국씨티은행";
$banks['034'] = "광주은행";
$banks['039'] = "경남은행";
$banks['007'] = "수협";
$banks['048'] = "신협";
$banks['037'] = "전북은행";
$banks['035'] = "제주은행";
$banks['064'] = "산림조합";
$banks['071'] = "우체국";
$banks['081'] = "하나은행";
$banks['088'] = "신한은행";
$banks['209'] = "동양종금증권";
$banks['243'] = "한국투자증권";
$banks['240'] = "삼성증권";
$banks['230'] = "미래에셋";
$banks['247'] = "우리투자증권";
$banks['218'] = "현대증권";
$banks['266'] = "SK증권";
$banks['278'] = "신한금융투자";
$banks['262'] = "하이증권";
$banks['263'] = "HMC증권";
$banks['267'] = "대신증권";
$banks['270'] = "하나대투증권";
$banks['279'] = "동부증권";
$banks['280'] = "유진증권";
$banks['287'] = "메리츠증권";
$banks['291'] = "신영증권";
$banks['238'] = "대우증권";
 
function getBankCode($name,$code=""){
  global $banks;
  if( 0 < count($banks) ){
    $str = "<select name='".$name."'>";
    $str .= '<option value=0>은행 선택</option>';
    foreach( $banks as $i => $v ){
      $str .= "<option value='".$v."' ".($code > 0 && $v==$code?"selected":"").">";
      $str .= "[".$v."] ".$v;
      $str .= "</option>";
    }
    $str .= "</select>";
 
    return $str;
  }
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <h4><?php echo get_string('paymentinfo','local_management')?></h4>
                <table>
                    <tr>
                        <th><?php echo get_string('orderer','local_management')?>ID</th>
                        <td><?php echo $content->firstname." ".$content->lastname . '('.$content->username.')' ?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('ordernumber','local_management')?></th>
                        <td><?php echo $content->paycode ?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('goods:type','local_management')?></th>
                        <td><?php echo $type_txt ?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('goodsname','local_management')?></th>
                        <td><?php echo $paytitle?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('goodscode','local_management')?></th>
                        <td><?php echo $content->code?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('amount','local_management')?></th>
                        <td><?php echo $price_txt?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('order:date','local_management')?></th>
                        <td><?php echo date('Y.m.d H:i',$content->timecreated)?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('paycondate','local_management')?></th>
                        <td><?php echo ($content->status != 0) ? date('Y.m.d',$content->timemodified) : '-'?> </td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('paymentmethod','local_management')?></th>
                        <td><?php echo $content->paymethod ?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('paymentstatus2','local_management')?></th>
                        <td><?php echo $paystatus[$content->status];?></td>
                    </tr>
                </table>
                <?php if($content->tbid){ ?> 
                <h4><?php echo get_string('shippinginfo','local_management')?></h4>
                <table>
                    <tr>
                        <th><?php echo get_string('cell','local_management')?></th>
                        <td><?php echo $content->cell?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('shippingaddress','local_management')?></th>
                        <td><?php echo $content->address?></td>
                    </tr>
                    <tr>
                        <th><?php echo get_string('deliverystatus','local_management')?></th>
                        <td><?php echo $deliverry?></td>
                    </tr>
                </table>
                <?php }?>
                <div class="unrefund_process">
                <input type="button" value="<?php echo get_string('refundprocessing', 'local_management'); ?>" id="refund_process">
                <input type="button" value="<?php echo get_string('list', 'local_management'); ?>" onclick="javascript:location.href = './index.php';">
                </div>
                <br>
                
                <div class="refund_process" style="display:none;">
                    <h4><?php echo get_string('refundinfo','local_management')?></h4>
                    <table>
                        <tr>
                            <th><?php echo get_string('refunddate','local_management')?></th>
                            <td>
                                <input type="text" title="<?php echo  get_string('time', 'local_management') ?>" name="refunddate" class="w_120 date s_date" value="<?php echo ($content->refunddate) ? date("Y.m.d",$content->refunddate) : ''?>" placeholder="<?php echo get_string('click', 'local_management')?>  ">
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo get_string('refundreason','local_management')?></th>
                            <td><input type="text" name="text" value="<?php echo $content->lrtext?>" placeholder="<?php echo get_string('refundreasontxt','local_management')?>"></td>
                        </tr>
                        <tr>
                            <th><?php echo get_string('refunmethod','local_management')?></th>
                            <td>
                                <select name="lrtype">
                                    <option value="all"><?php echo get_string('selectrefunmethod','local_management')?></option>
                                    <?php if($content->paymethod != "directdeposit"){ ?>
                                    <option value="0" <?php echo ($content->lrtype == 0) ? "selected" : "";?>><?php echo  get_string('approval', 'local_management') ?></option>
                                    <?php }?>
                                    <option value="1" <?php echo ($content->lrtype == 1) ? "selected" : "";?>><?php echo  get_string('bankbook', 'local_management') ?></option>
                                </select>
                                ※ <?php echo  get_string('refundtxt', 'local_management') ?>
                            </td>
                        </tr>
                        <tr class="lrtype1">
                            <th><?php echo get_string('refundaccount','local_management')?></th>
                            <td>
                                <?php echo getBankCode('bank',$content->bank)?>
                                <input type="text" name="refundbanknr" value="<?php echo $content->refundbanknr?>" placeholder="<?php echo get_string('accounttxt','local_management')?>">
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo get_string('refundamount','local_management')?></th>
                            <td>
                                <?php
                                //switch($content->)
                                ?>
                                <input type="text" name="price" value="<?php echo $content->lrprice?>"> VND <input type="button" value="<?php echo  get_string('refundprocessing', 'local_management') ?>" id="refund_go">
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo  get_string('refunddate', 'local_management') ?></th>
                            <td><?php echo ($content->completedate) ? date("Y.m.d H:i", $content->completedate) : '-'?></td>
                        </tr>
                    </table>
                    <input type="button" value="<?php echo get_string('refundcancel','local_management')?>" id="refund_cancel"> ※ <?php echo get_string('refundcanceltxt','local_management')?>
                    <input type="button" value="<?php echo get_string('save','local_management')?>" id="refund_save">
                    <input type="button" value="<?php echo get_string('list', 'local_management'); ?>" onclick="javascript:location.href = './index.php';">
                </div>
            </div>
            
        </div>
    </div>
</section>
<?php
include_once($CFG->dirroot . '/local/management/footer.php');
?>
<script type="text/javascript">
function call_ajax(action, data, async) {
    var rvalue = false;

    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function (result) {
            rvalue = result;
        },
        error: function (xhr, status, error) {
        }
    });

    return rvalue;
}
$(document).ready(function () {
    $('.refund_process').hide();
    <?php if($content->lrid){?>
        $('.refund_process').show();
        $('.unrefund_process').hide();
    <?php }?>
        
    <?php if(!$content->lrid || $content->lrtype == 0){?>
            $('.lrtype1').hide();
    <?php } ?>
    
    $("#refund_go").on('click',function(e){
        var refunddate = $('input[name=refunddate]').val();
        var text = $('input[name=text]').val();
        var lrtype = $('select[name=lrtype]').val();
        var price = $('input[name=price]').val();
        var payid = '<?php echo $id?>';
        var bank = $('select[name=bank]').val();
        var refundbanknr = $('input[name=refundbanknr]').val();
        
        if(!refunddate){
            alert("환불접수일을 입력해주세요");
            return false;
        }
        if(!text){
            alert("환불사유를 입력해주세요");
            return false;
        }
        if(!lrtype){
            alert("환불방식 입력해주세요");
            return false;
        }
        if(!price){
            alert("환불금액 입력해주세요");
            return false;
        }
        if(lrtype == 1){
            if(!bank){
                alert("은행을 입력해주세요");
                return false;
            }
            if(!refundbanknr){
                alert("계좌번호를 입력해주세요");
                return false;
            }
        }
        
        var result = call_ajax('refund_save', {id: '<?php echo $content->lrid?>', refunddate:refunddate,text:text,lrtype:lrtype,price:price,payid:payid,bank:bank,refundbanknr:refundbanknr}, false);
        if (result.status == 'success') {
            var result = call_ajax('refund_go', {id: '<?php echo $content->lrid?>', type:$('select[name=lrtype]').val() }, false);
            if(result.status == 'success') {
                location.reload();
            }
        }

    });
    
    $('#refund_cancel').on('click',function(e){
        if (confirm("입력된 환불정보를 삭제하시겠습니까? ") == true) {
            var result = call_ajax('delete', {id: '<?php echo $content->lrid?>'}, false);
            if (result.status == 'success') {
                location.reload();
            }
        }
    });
   
    $('#refund_process').on('click',function(e){
        <?php if($content->status != 0){ ?>
            $('.refund_process').show();
            $('.unrefund_process').hide();
        <?php }else{?>
            alert("결제전입니다.");
        <?php }?>
    });
            
    $('select[name=lrtype]').on('change',function (e) {
        if($('select[name=lrtype]').val() == 0){
            $('.lrtype1').hide();
        }else if($('select[name=lrtype]').val() == 1){
            $('.lrtype1').show();
        }else{
            $('.lrtype1').hide();
        }
    });
    $('#refund_save').on('click', function (event) {
        var refunddate = $('input[name=refunddate]').val();
        var text = $('input[name=text]').val();
        var lrtype = $('select[name=lrtype]').val();
        var price = $('input[name=price]').val();
        var payid = '<?php echo $id?>';
        var bank = $('select[name=bank]').val();
        var refundbanknr = $('input[name=refundbanknr]').val();
        
        if(!refunddate){
            alert("환불접수일을 입력해주세요");
            return false;
        }
        if(!text){
            alert("환불사유를 입력해주세요");
            return false;
        }
        if(!lrtype){
            alert("환불방식 입력해주세요");
            return false;
        }
        if(!price){
            alert("환불금액 입력해주세요");
            return false;
        }
        
        if(lrtype == 1){
            if(!bank || bank == 0){
                alert("은행을 입력해주세요");
                return false;
            }
            if(!refundbanknr){
                alert("계좌번호를 입력해주세요");
                return false;
            }
        }
        
        var result = call_ajax('refund_save', {id: '<?php echo $content->lrid?>', refunddate:refunddate,text:text,lrtype:lrtype,price:price,payid:payid,bank:bank,refundbanknr:refundbanknr}, false);
        if (result.status == 'success') {
            location.reload();
        }
    });
        
    start_datepicker_c('', '');
});
</script>