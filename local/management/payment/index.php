<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '결제관리',
    'heading' => '결제관리',
    'subheading' => '',
    'menu' => 'payment',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');

$stext = optional_param('stext', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);
$type = optional_param_array('type', array(), PARAM_RAW);
$paymethod = optional_param_array('paymethod', array(), PARAM_RAW);
$refundtype = optional_param('refundtype', 'all', PARAM_RAW);
$status = optional_param_array('status', array(), PARAM_RAW);
$status1 = optional_param('status1', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$order_date = optional_param('order_date', '', PARAM_RAW);
$stext = trim($stext);

if ($learningstart_str) {
    $learningstart = str_replace(".", "-", $learningstart_str);
    $learningstart = strtotime($learningstart);
}
if ($learningend_str) {
    $learningend = str_replace(".", "-", $learningend_str);
    $learningend = strtotime($learningend);
}
if ($learningstart) {
    $sql_where[] = " $order_date >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if ($learningend) {
    $sql_where[] = " $order_date <=:learningend ";
    $params['learningend'] = $learningend+ 86400 - 1;
}
if($refundtype){
    if($refundtype == "ing"){
        $sql_where[] = " lr.refundtype = :refundtype ";
        $params['refundtype'] = 0;
    }else if($refundtype == "com"){
        $sql_where[] = " lr.refundtype = :refundtype ";
        $params['refundtype'] = 1;
    }
}

if ($type) {
    foreach ($type as $tkey => $tval) {
        if($tval == 3){
            $sql_where_type[] = " ( lc.type = :type" . $tkey . " AND lpd.delivery is not null )";
            $params['type' . $tkey] = $tval;
        }else if($tval == 1){
            $sql_where_type[] = " ( lc.type = :type" . $tkey . " AND lpd.delivery is null )";
            $params['type' . $tkey] = $tval;
        }else{
            $sql_where_type[] = " lc.type = :type" . $tkey;
            $params['type' . $tkey] = $tval;
        }
    }
    if (!empty($sql_where_type)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_type) . ')';
    } else {
        $sql_where[] = '';
    }
}
if($status1){
    $sql_where[] = " lp.status = :status  AND lp.paymethod = 'directdeposit' ";
    $params['status'] = 0;
}
if ($status) {
    foreach ($status as $tkey => $tval) {
        if($tval == 'normal'){
            //정상승인 //무통X
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod != 'directdeposit' )";
            $params['status' . $tkey] = 1;
        }else if($tval == 'waiting'){
            //입금대기
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod = 'directdeposit' )";
            $params['status' . $tkey] = 0;
        }else if($tval == 'comfirm'){
            //입금확인
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod = 'directdeposit' )";
            $params['status' . $tkey] = 0;
        }else if($tval == 'cancel'){
            //승인취소  //무통X
            $sql_where_status[] = " ( lp.status = :status" . $tkey . " AND lp.paymethod = 'directdeposit' AND lr.refundtype = 1 )";
            $params['status' . $tkey] = 1;
        }
    }
    if (!empty($sql_where_status)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_status) . ')';
    } else {
        $sql_where[] = '';
    }
}

if ($paymethod) {
    foreach ($paymethod as $tkey => $tval) {
        $sql_where_paymethod[] = " lp.paymethod = :paymethod" . $tkey;
        $params['paymethod' . $tkey] = $tval;
    }
    if (!empty($sql_where_paymethod)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_paymethod) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($stext)) {
    //주문번호
    $sql_where_search[] = $DB->sql_like('lp.paycode', ':id');
    $params['id'] = '%' . $stext . '%';
    //상품명
    $sql_where_search[] = $DB->sql_like('lc.title', ':title');
    $params['title'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.en_title', ':en_title');
    $params['en_title'] = '%' . $stext . '%';
    $sql_where_search[] = $DB->sql_like('lc.vi_title', ':vi_title');
    $params['vi_title'] = '%' . $stext . '%';
    //상품코드
    $sql_where_search[] = $DB->sql_like('lc.code', ':code');
    $params['code'] = '%' . $stext . '%';
    //주문자
    $sql_where_search[] = $DB->sql_like('u.firstname', ':firstname');
    $params['firstname'] = '%' . $stext . '%';
    $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}


$payselect = "select lp.*,u.firstname,u.lastname, lc.title, lc.vi_title, lc.en_title, lc.price, lc.code, lpd.delivery, tb.id  tbid, lc.type lctype, lc.courseid, lr.id lrid, lr.completedate, lr.refundtype   ";
$payfrom = " from {lmsdata_payment} lp
                join {user} u on lp.userid = u.id
                join {lmsdata_class} lc on lc.id = lp.ref
                left join {lmsdata_productdelivery} lpd on lpd.payid = lp.id  
                LEFT JOIN {lmsdata_textbook} tb ON tb.id = lc.bookid
                LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id ";
$payorder = " ORDER BY lp.timecreated DESC, lp.id DESC";

$patlists = $DB->get_records_sql($payselect.$payfrom.$sql_where.$payorder, $params, ($page - 1) * $perpage, $perpage);
$total = $DB->count_records_sql("SELECT COUNT(lp.id) $payfrom $sql_where",$params);

$paystatus = [ 0 => get_string('beforepay', 'local_management'),
               1 => get_string('successpay', 'local_management'),
               2 => get_string('failpay', 'local_management'),
               3 => get_string('beforecancel', 'local_management'),
               4 => get_string('successcancel', 'local_management')];
$paymethod_array = [
    "momo" => "MoMo AIO Payment",
    "domestic" => "Domestic card (ATM Card)",
    "international" => "International card (Visa, Master, Amex, JCB)",
    "directdeposit" => "무통장 입금"
];
$status_array = [
    "normal" => get_string('normal','local_management'),
    "waiting" => get_string('waiting','local_management'),
    "comfirm" => get_string('confirmpayment','local_management'),
    "cancel" => get_string('approval','local_management'),
];

$perpage_array = [10, 20, 30, 50];
$type_array = array('1'=>get_string('course', 'local_management'), '2'=>get_string('book', 'local_management'), '3'=>get_string('course', 'local_management').'+'.get_string('book', 'local_management'));
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <?php if(!empty($tabs)){?>
                    <ul class="nav nav-tabs">
                        <?php
                        foreach ($tabs as $key => $tab) {
                            $active = $key == $type ? 'active' : '';
                            ?>
                            <li class = "nav-item  <?php echo $active ?> ">
                                <a class = "nav-link" data-toggle = "" href="javascript:movepage('<?php echo $key ?>');"><?php echo $tab ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>

                <div class = "tab-content">

                    <div id = "data1" class = "tab-pane fade in active">
                        <div>

                            <form action="index.php" method="get" name='search_form' id='search_form' class="search_area" >
                                <input type='hidden' name='page' value='1'>
                                <input type='hidden' name='perpage' value='<?php echo $perpage?>'>
                                <input type='hidden' name='type' value="<?php echo $type ?>">
                                <input type="hidden" name="orderitem" value="<?php echo $orderitem?>"/>
                                <input type="hidden" name="ordertype" value="<?php echo $ordertype?>"/>
                                <div>
                                    <label><?php echo  get_string('goods:type', 'local_management') ?></label>
                                    <?php foreach($type_array as $tkey => $tval){?>
                                    <label class="mg-bt10"><input type="checkbox" name="type[]" value="<?php echo $tkey?>" <?php foreach($type as $ischeck){if($ischeck == $tkey){echo "checked";}}?>><?php echo $tval ?></label>
                                    <?php }?>
                                </div>
                                <div>
                                    <label><?php echo  get_string('paymentmethod', 'local_management') ?></label>
                                    <?php foreach($paymethod_array as $tkey => $tval){?>
                                    <label class="mg-bt10">
                                        <input type="checkbox" name="paymethod[]" value="<?php echo $tkey?>"
                                        <?php foreach($paymethod as $ischeck){if($ischeck == $tkey){echo "checked";}}?>><?php echo $tval ?>
                                    </label>
                                    <?php }?>
                                </div>
                                <div>
                                    <label><?php echo get_string('paymentstatus2','local_management')?></label>
                                    <?php foreach($status_array as $tkey => $tval){?>
                                    <label class="mg-bt10">
                                        <input type="checkbox" name="status[]" value="<?php echo $tkey?>"
                                        <?php foreach($status as $ischeck){if($ischeck == $tkey){echo "checked";} }?>><?php echo $tval ?>
                                    </label>
                                    <?php }?>
                                </div>
                                <div>
                                    <label><?php echo get_string('refundstatus','local_management')?></label>
                                    <label class="mg-bt10"><input type="radio" name="refundtype" value="all" <?php echo ($refundtype == "all") ? "checked" : "" ?>><?php echo get_string('delivery:all','local_management')?></label>
                                    <label class="mg-bt10"><input type="radio" name="refundtype" value="ing" <?php echo ($refundtype == "ing") ? "checked" : "" ?>><?php echo get_string('proceeding','local_management')?></label>
                                    <label class="mg-bt10"><input type="radio" name="refundtype" value="com" <?php echo ($refundtype == "com") ? "checked" : "" ?>><?php echo get_string('refundcompleted','local_management')?></label>
                                </div>
                                <div>
                                    <label>
                                        <select name="order_date">
                                            <option value="lp.timecreated" <?php echo ($order_date == 'lp.timecreated') ? " selected" : ""?>><?php echo  get_string('order:date', 'local_management') ?></option>
                                            <option value="lp.timemodified" <?php echo ($order_date == 'lp.timemodified') ? " selected" : ""?>><?php echo  get_string('paydate', 'local_management') ?></option>
                                            <option value="lr.completedate" <?php echo ($order_date == 'lr.completedate') ? " selected" : ""?>><?php echo  get_string('refunddate', 'local_management') ?></option>
                                        </select>
                                    </label>
                                    <input type="text" title="<?php echo  get_string('time', 'local_management') ?>" name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str?>" placeholder="<?php echo get_string('click', 'local_management')?>  " id="dp1578535626143">
                                    <span class="dash">~</span>
                                    <input type="text" title="<?php echo  get_string('time', 'local_management') ?>" name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str?>" placeholder="<?php echo get_string('click', 'local_management')?>  " id="dp1578535626144">
                                </div>
                                <div>
                                    <label><?php echo get_string('search', 'local_management')?></label>
                                    <input type="text" name="stext" value="<?php echo $stext != '' ? $stext : null ?>" placeholder="<?php echo get_string('order:search', 'local_management')?>">
                                    <input type="submit" value="<?php echo get_string('search', 'local_management')?>">
                                    <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management')?>">
                                </div>
                            </form>
                            <table>
                                <tr>
                                    <td><?php echo get_string('bankbookwaiting','local_management')?></td>
                                    <td><?php echo $DB->count_records_sql("select count(*)" . $payfrom . " WHERE lp.status = 0 AND lp.paymethod = 'directdeposit'");?></td>
                                    <td><?php echo get_string('refunding','local_management')?></td>
                                    <td><?php echo $DB->count_records_sql("select count(*)" . $payfrom . " WHERE lr.refundtype = 0");?></td>
                                    <td><?php echo get_string('refundcon','local_management')?></td>
                                    <td><?php echo $DB->count_records_sql("select count(*)" . $payfrom . " WHERE lr.refundtype = 1");?></td>
                                </tr>
                            </table>
                            
                            <div style="margin-top: 10px;">
                                <span><?php echo get_string('changestatus', 'local_management') ?></span>
                                <input type="button" id="all_waiting" value="<?php echo get_string('waiting', 'local_management') ?>">
                                <input type="button" id="all_confirm" value="<?php echo get_string('confirmpayment', 'local_management') ?>">
    <!--                            <select class="f-r" name="perpage">
                                    <?php foreach ($perpage_array as $perp) { ?>
                                        <option value="<?php echo $perp ?>" <?php echo $perpage == $perp ? 'selected' : '' ?>><?php echo $perp ?></option>
                                    <?php } ?>
                                </select>-->
                                <input type="button" id="all_delete" style="border-color: #ec8b8b; color: #ec8b8b;" value="Delete">
                            </div>
                            <div style="margin-top: 10px;">
                                <span><?php echo get_string('total', 'local_management', $total) ?></span>
                                <select name="perpage2" class="perpage2">
                                    <?php
                                    foreach ($perpage_array as $pa) {
                                        $selected = '';
                                        if ($pa == $perpage) {
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                                            <?php
                                        }
                                        ?>
                                </select>
                                <input type="button" class="blue_btn" style="margin-right: 10px;" value="Download Excel" onclick="excel_down();">
                            </div>
                        </div>
                        <table>
                            <thead class = "thead-light">
                                <tr>
                                    <th width="5%"><input type = "checkbox" class = "checkall" /></th>
                                    <th width="5%"><?php echo  get_string('number', 'local_management') ?></th>
                                    <th width="7%"><?php echo  get_string('goods:type', 'local_management') ?></th>
                                    <th><?php echo  get_string('ordernumber', 'local_management') ?></th>
                                    <th width="23%"><?php echo  get_string('goodsname', 'local_management') ?></th>
                                    <th><?php echo  get_string('goodscode', 'local_management') ?></th>
                                    <th><?php echo  get_string('orderer', 'local_management') ?></th>
                                    <th><?php echo  get_string('amount', 'local_management') ?></th>
                                    <th><?php echo get_string('order:date', 'local_management')?></th>
                                    <th><?php echo get_string('paycondate','local_management')?></th>
                                    <th><?php echo get_string('paymentprice','local_management')?></th>
                                    <th><?php echo get_string('paymentmethod','local_management')?></th>
                                    <th><?php echo get_string('paymentstatus2','local_management')?></th>
                                    <th><?php echo get_string('refundstatus','local_management')?></th>
                                    <th><?php echo get_string('refunddate','local_management')?></th>
                                </tr>
                                <?php if ($patlists) :
                                    $total_sales = $DB->get_record_sql("SELECT sum(lc.price) as total $payfrom $sql_where and lp.forced = 0",$params)->total;
                                    // foreach($patlists as $patlist) {
                                    //     // $period_total_sale_sql = '';
                                    //     // if ($learningstart) {
                                    //     //     $period_total_sale_sql = "{$order_date} >= {$learningstart} AND ";
                                    //     // }
                                    //     // if ($learningend) {
                                    //     //     $period_total_sale_sql = "{$order_date} <= {$learningend} AND ";
                                    //     // }
                                    //     // $total_sale_sql = "SELECT sum(lc.price) as total
                                    //     //     FROM {lmsdata_payment} lp
                                    //     //     LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                                    //     //     LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                                    //     //     LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                                    //     //     WHERE {$period_total_sale_sql} lc.courseid = '{$patlist->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
                                        
                                    //     // $total_sales += $DB->get_record_sql($total_sale_sql)->total;
                                    //     if($patlist->forced != 1) {
                                    //         $total_sales += $patlist->price;
                                    //     }
                                    // }

                                ?>
                                <tr>
                                    <th><?php echo get_string('total_sum', 'local_management'); ?></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?php echo number_format($total_sales); ?></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <?php endif; ?>
                            </thead>
                            <tbody>
                                <?php
                                if ($patlists) {
                                    $startnum = $total - (($page - 1) * $perpage);
                                    foreach ($patlists as $patlist) {
                                         switch (current_language()){
                                            case 'ko' :
                                                $paytitle = $patlist->title;
                                                break;
                                            case 'en' :
                                                $paytitle = $patlist->en_title;
                                                break;
                                            case 'vi' :
                                                $paytitle = $patlist->vi_title;
                                                break;
                                        }

                                        $status_txt = "";
                                        if($patlist->lrid){
                                            switch($patlist->refundtype){
                                                case 0 :
                                                    $status_txt = get_string('proceeding','local_management');
                                                    break;
                                                case 1 :
                                                    $status_txt = get_string('procomplete','local_management');
                                                    break;
                                            }
                                        }

                                        $type_txt = '';

                                        if($patlist->lctype == 1){
                                            $type_txt = get_string('course', 'local_management');
                                        }else if($patlist->lctype == 3){
                                                $type_txt = get_string('course', 'local_management') .'+' .get_string('book', 'local_management');
                                        }else{
                                            $type_txt = get_string('book', 'local_management');
                                        }
                                        $price_txt = number_format($patlist->price);
                                        if(($patlist->forced == 1)){
                                            $price_txt = get_string('free', 'local_management');
                                        }
                                        if($patlist->paytype == 4){
                                            $patlist_credit=$DB->get_record('vi_credit_menu',['id' =>$patlist->ref]);
                                            $type_txt='Credit';
                                            $paytitle=$patlist_credit->credit_name;
                                            $patlist->code=$patlist_credit->id;
                                            $price_txt=number_format($patlist_credit->price);
                                            $patlist_employer=$DB->get_record('vi_employers',['id' =>$patlist->userid]);
                                            $patlist->firstname=$patlist_employer->company_name;
                                            $patlist->lastname ='';
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <input type = "checkbox" class="conslist" name="conslist"  conid="<?php echo $patlist->id ?>" userid="<?php echo $patlist->userid?>"/>
                                            </td>
                                            <td><?php echo $startnum-- ?></td>
                                            <td><?php echo $type_txt ?> </td>
                                            <td><a href="./detail.php?id=<?php echo $patlist->id?>"><?php echo $patlist->paycode ?></a></td>
                                            <td><?php echo $paytitle?></td>
                                            <td><?php echo $patlist->code?></td>
                                            <td><?php echo $patlist->firstname." ".$patlist->lastname ?> </td>
                                            <td><?php echo $price_txt;?> </td>
                                            <td><?php echo date('Y.m.d',$patlist->timecreated)?> </td>
                                            <td><?php echo ($patlist->status != 0) ? date('Y.m.d',$patlist->timemodified) : '-'?> </td>
                                            <td>
                                                <?php 
                                                    if(($patlist->forced == 1)){
                                                        // Free
                                                        echo 0;
                                                    } else {
                                                        // $period_total_sale_sql = '';
                                                        // if ($learningstart) {
                                                        //     $period_total_sale_sql = "{$order_date} >= {$learningstart} AND ";
                                                        // }
                                                        // if ($learningend) {
                                                        //     $period_total_sale_sql = "{$order_date} <= {$learningend} AND ";
                                                        // }
                                                        // $total_sale_sql = "SELECT sum(lc.price) as total
                                                        //     FROM {lmsdata_payment} lp
                                                        //     LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                                                        //     LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                                                        //     LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                                                        //     WHERE {$period_total_sale_sql} lc.courseid = '{$patlist->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
                                                        
                                                        // echo number_format($DB->get_record_sql($total_sale_sql)->total);
                                                        echo $price_txt;
                                                    }
                                                ?>
                                            </td>
                                            <td> <?php echo $patlist->paymethod ?> </td>
                                            <td>
                                                <?php echo $patlist->status == 0 ?'<input type="button" onclick="paycheck('.$patlist->id.','.$patlist->userid.')" value="'.$paystatus[$patlist->status].'">':$paystatus[$patlist->status];?>
                                            </td>
                                            <td><?php echo ($status_txt) ? $status_txt : '-'?></td>
                                            <td><?php echo ($patlist->completedate) ? date('Y.m.d',$patlist->completedate) : '-' ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                <td colspan="15"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                            <?php } ?>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation" class="text-center">
                            <?php
        //                        $pageurl = $CFG->wwwroot.'/local/management/users/index.php?search=' . $searchtext;
        //                        echo local_management_users_paing($page, $perpage, $totalusers, $pageurl,$searchtext,$orderitem,$ordertype);
                            if (($total / $perpage) > 1) {
                                print_paging_navbar_script($total, $page, $perpage, 'javascript:cata_page(:page);');
                                //print_paging_navbar($total, $page, $perpage, $CFG->wwwroot . '/local/management/payment/index.php', array("search" => $search));
                            }
                            ?>
                        </nav>
                    </div>
                </div>
            </div>
<!--            <div class = "btns clearfix">
                <button class="btn btn-success pull-right xlsdown">엑셀다운로드</button>
                <button class = "btn btn-danger pull-right addpop">엑셀업로드</button>
                <button class = "btn btn-primary  pull-right" onclick="removecontents();">삭제</button>
            </div>

            <div class = "btns clearfix">
                <button class = "btn btn-primary btn-danger  pull-right searchadd">등록</button>
            </div>-->

        </div>
        <!--/.col -->
    </div>

</section>
<!--/.content -->


<?php

include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">
    function all_watingconfirm(txt, callback){
        var length = $(".conslist:checked").length;
        var text = "";
        if(txt == "waiting"){
            text = "<?php echo get_string('waiting','local_management')?>";
        }else if(txt == "confirm"){
            text = "<?php echo get_string('confirmpayment','local_management')?>";
        } else if(txt == "delete") {
            text = "Delete";
        }

        if(length > 0){
            var conarr = new Array();
            $(".conslist:checked").each(function (i, v) {
                conarr.push($(v).attr("conid"));
            });
            var userid = new Array();
            $(".conslist:checked").each(function (i, v) {
                userid.push($(v).attr("userid"));
            });

            if(confirm("선택한 "+length+"건을 "+text+" 처리하시겠습니까?")){
                $.ajax({
                    type: 'POST',
                    url: 'paycheckall_ajax.php',
                    data: {conarr: conarr, userid: userid, txt:txt},
                    success: function (data, status, xhr) {
                        //location.reload();
                        if(callback) callback(data);
                    },
                });
            }
        }
    }
    $(document).ready(function () {
        $('#all_waiting').click(function() {
            all_watingconfirm("waiting", function() {
                location.reload();
            });
        });
        $('#all_confirm').click(function() {
            all_watingconfirm("confirm", function() {
                location.reload();
            });
        });
        $('#all_delete').click(function() {
            all_watingconfirm("delete", function() {
                location.reload();
            });
        });
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=search_form]").submit();
        });

        $('#reset').click(function () {
            $('input[name=learningstart]').val('');
            $('input[name=learningend]').val('');
            $('input[name=stext]').val('');
            $("input:checkbox[name='type[]']").attr("checked", false);
            $("form[id=search_form]").submit();
        });
    })
    start_datepicker_c('', '');
    end_datepicker_c('', '');
    function paycheck(id,userid){
        if(confirm("<?php echo get_string('check_pay', 'local_payment'); ?>")){
            $.ajax({
                type: 'POST',
                url: 'paycheck_ajax.php',
                data: {id: id, userid: userid},
                success: function (data, status, xhr) {
                    location.reload();
                },
            });
        }
    }
    function cata_page(page) {
        $('[name=page]').val(page);
        $('#search_form').submit();
    }
    function removecontents() {
        if (confirm('선택한 항목을 삭제하시겠습니까?')) {
            var type = "<?php echo $type ?>";
            var conarr = new Array();
            $(".conslist:checked").each(function (i, v) {
                conarr.push($(v).attr("conid"));
            });
            $.ajax({
                type: 'POST',
                url: 'remove_reco.php',
                data: {conid: conarr, type: type},
                success: function (data, status, xhr) {

                    location.reload();
                },
            });
        }
    }
    function searchadd() {
        var addid = $(".sid").text();
        var type = '<?php echo $type ?>';
        var jobgroup = $("#jobgroup").val();
        var bl_co_cd = $("#bl_co_cd").val();
        if (addid == '') {
            alert('콘텐츠 정보 확인을 해주세요');
        } else {
            if (type == "comppop" && bl_co_cd == '') {
                alert('회사를 검색해주세요');
            } else {
                if (confirm(("콘텐츠 ID : " + addid + " 를 [ <?php echo $tabs[$type] ?> ] 항목에 추가하시겠습니까?"))) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: 'add_search.php',
                        data: {conid: addid, type: type, jobgroup: jobgroup, bl_co_cd: bl_co_cd},
                        success: function (data, status, xhr) {
                            if (data.status == 'false') {
                                alert(data.text);
                            } else {
                                location.reload();
                            }
                        },
                    });
                }
            }
        }
    }

    function excel_down() {
        var form = $('#search_form');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
        return false;
    }

</script>
