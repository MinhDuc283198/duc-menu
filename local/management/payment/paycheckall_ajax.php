<?php

require_once(__DIR__ . '/../../../config.php');

$conarr = optional_param_array('conarr', array(), PARAM_RAW);
$userid = optional_param_array('userid', array(), PARAM_RAW);
$txt = optional_param('txt', "", PARAM_RAW);
if($txt == "waiting"){
    $status = 0;
}else if($txt == "confirm"){
    $status = 1;
}
if($conarr) {
    foreach ($conarr as $ca) {
        if ($txt == "delete") {
            $DB->delete_records("lmsdata_payment", array("id" => $ca));
        } else {
            $payment = $DB->get_record('lmsdata_payment', array('id' => $ca));
            $payment->status = $status;
            $DB->update_record('lmsdata_payment', $payment);

            if ($status == 0) {
                $courseid = $DB->get_field("lmsdata_class", 'courseid', array("id" => $payment->ref));
                $enrolid = $DB->get_field("enrol", 'id', array("courseid" => $courseid, "sortorder" => 0));
                $DB->delete_records("user_enrolments", array("enrolid" => $enrolid, "userid" => $payment->userid));
                if ($payment->paytype == 4) {
                    foreach ($userid as $employer_id) {
                        $credit_id = $payment->ref;
//                        $employer_id = $userid;
                        $query = "SELECT 
                            cm.id,
                            cm.credit_name,
                            cm.points,
                            cm.price,
                            cm.banner_image_url,
                            cm.expired_date
                            FROM {vi_credit_menu} cm
                            WHERE cm.id=$credit_id
                            ";
                        $credit_menu = $DB->get_record_sql($query);
                        $sql = "SELECT 
                        *
                        FROM {vi_credits} cre
                        WHERE cre.employer_id=$employer_id
                        ";
                        $credit = $DB->get_record_sql($sql);
                        if($credit||($credit->credit_point>$credit_menu->points)){
                            $credit->credit_point -= $credit_menu->points;
                            $credit->timemodified = time();
                            $DB->update_record('vi_credits', $credit);
                        }

                    }
                }
            }

            if ($status == 1) {
                if ($payment->paytype != 4) {

                    $datatype = $DB->get_field_sql("select type from {lmsdata_class} where id = ? ", array($payment->ref));

                    if ($datatype != 2) {
                        require_once $CFG->dirroot . '/local/lmsdata/lib.php';
                        require_once $CFG->dirroot . '/local/course/lib.php';
                        $courseid = $DB->get_field("lmsdata_class", 'courseid', array("id" => $payment->ref));
                        $enrolresult = local_course_whether_enrol($courseid, $payment->userid, 'student');
                        if ($enrolresult == false) {
                            $result = local_lmsdata_enrol_user($courseid, $payment->userid, 'student');
                            $returnvalues = new stdClass();
                            $returnvalues->status = $result;
                            if ($result) {

                            } else {
                                $aa = local_lmsdata_enrol_edit_user($courseid, $payment->userid, 'student');
                            }
                        }
                    }
                } else {
                    if ($userid) {
                        foreach ($userid as $employer_id){

                            $credit_id = $payment->ref;
//                        $employer_id = $userid;
                        $query = "SELECT 
                            cm.id,
                            cm.credit_name,
                            cm.points,
                            cm.price,
                            cm.banner_image_url,
                            cm.expired_date
                            FROM {vi_credit_menu} cm
                            WHERE cm.id=$credit_id
                            ";
                        $credit_menu = $DB->get_record_sql($query);
                        $sql = "SELECT 
                        *
                        FROM {vi_credits} cre
                        WHERE cre.employer_id=$employer_id
                        ";
                        $credit = $DB->get_record_sql($sql);


                        if (!$credit) {
                            $data = new stdClass();
                            $data->employer_id = $employer_id;
                            $data->credit_point = $credit_menu->points;
                            $data->expire_date = $credit_menu->expired_date;
                            $data->timecreated = time();
                            $DB->insert_record('vi_credits', $data);


                        } else {
                            $credit->credit_point += $credit_menu->points;
                            $credit->timemodified = time();
                            $DB->update_record('vi_credits', $credit);
                        }

                        $data = new stdClass();
                        $data->member_credit_id = $credit->id;
                        $data->transaction_code = '1001';
                        $data->pg_code = '3003';
                        $data->price = $credit_menu->price;
                        $data->point = $credit_menu->points;
                        $data->timelogged = time();
                        $DB->insert_record('vi_credits_logs', $data);
                    }
                }

                }
            }
        }
    }
}


    