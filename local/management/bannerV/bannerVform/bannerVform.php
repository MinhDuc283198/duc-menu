<?php
defined('MOODLE_INTERNAL') || die();
global $VISANG;
require_once($VISANG->dirroot . '/classes/formslib.php');
class bannerV_form extends moodleform{
    public $renderer;
    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;
        $mform= $this->_form;
        $this->renderer= new \local_visang\form_renderer($mform);
        $action = $this->_customdata['action'];
        $returnurl = $this->_customdata['returnurl'];
        $returnto = $this->_customdata['returnto'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);

        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'name', get_string('name'), array('required', 'class' => 'form-control'));
        $mform->addRule('name', null, 'required', null, 'server');
        $mform->setType('name', PARAM_RAW);

        $mform->addElement('advfile', 'filename', 'filename', null, array('maxfiles' => 1, 'accepted_types' => 'video/*;capture=camera'));

        $mform->addElement('advcheckbox','isused','isused');
        $mform->setDefault('isused',0);

        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }
    function save($data)
    {
        global $DB;
        $logo = $this->_form->getElement('filename')->save_file($data->user_avatar_url);

        $content = new stdClass();
        $content->name = $data->name;
        $content->filename=$data->filename;
        $content->isused=$data->isused;
        $content->timemodified = time();
        // echo '<pre>';
        // var_dump($logo);
        // echo '</pre>';
        // die();


        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                $DB->insert_record('banner_video', $content);
                break;
            case 'edit':
                $DB->update_record('banner_video', $content);
                break;
        }
    }
}