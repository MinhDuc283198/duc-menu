<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage','local_lmsdata'),
    'heading' => get_string('banner_manage','local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerV',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/banner_view.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerM/lib.php');
include_once($CFG->dirroot.'/local/management/header.php');

$id = optional_param('id', 0, PARAM_INT);

$banner = $DB->get_record('banner_video', array('id' => $id));


?>
<section class="content">
<div id="contents">
    <div class="box box-body">
        <div class="frm_popup">
            <table cellspadding="0" cellspacing="0" class="detail img-tb">
                <tr>
                    <td class="field_title"><?php echo get_string('banner:name','local_lmsdata');?></td>
                    <td class="field_value">
                        <?php echo $banner-> name; ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('contents_attachments','local_lmsdata');?>(PC)</td>
                    <td class="field_value">
                                                   <?php
                        for ($n = 0; $n <= 0; $n++) {
                                            ?>
                                            <div style="width:100%; clear: both; float: left;  padding: 5px;">

                                                <?php
                                                if (isset($fileobj[$n])) {
                                                    echo $fileobj[$n];
                                                }
                                                echo "</div>";
                                            }
                                            ?>
                            <input type="hidden" name="file_del" value="0"/>
                        </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('banner:use','local_lmsdata');?></td>
                    <td class="field_value">
                        <?php if ($banner->isused == 1) {
                            echo get_string('use','local_lmsdata') ;
                        } else {
                            echo get_string('unuse','local_lmsdata');
                        } ?></td>
                </tr>
            </table>
            <div id="btn_area">
                <input type="button" id="banner_write"  class="blue_btn" value="<?php echo get_string('modify', 'local_management'); ?>" style="float:right" />
                <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list', 'local_management'); ?>" style="float:left;" />
            </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->
</section>

<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>

<script type='text/javascript'>
    $(document).ready(function(){
        $('#banner_list').click(function(){
            location.href = "./banner.php";
        });
    $('#banner_write').click(function(){
        location.href = '<?php echo($CFG->wwwroot); ?>/local/management/bannerV/banner_edit.php?id=<?php echo($banner->id); ?>';
        });    
    });
</script>   