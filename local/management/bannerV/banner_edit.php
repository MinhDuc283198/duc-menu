<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage', 'local_lmsdata'),
    'heading' => get_string('banner_manage', 'local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerV',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner_edit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerV/lib.php');

$id = optional_param('id', 0, PARAM_INT);
$mod = optional_param('mod', "", PARAM_TEXT);
$banner = video_banner_get_banner_content($id);

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
$pathvs = video_banner_get_file($context->id, 'banner', $id, 1);
$temp->itemid = $banner->id;

include_once($CFG->dirroot . '/local/management/header.php');
?>
<section class="content">
    <div class="box box-body">
        <div>
            <form action="banner_submit.php" enctype="multipart/form-data" method="POST" onsubmit="return chkfile()">
                <input type="hidden" name="mod" value="edit"/>
                <table class="detail">
                    <tr>
                        <td class="field_title" width="20%"><?php echo get_string('banner:name', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="text" name="name" value="<?php echo $banner->name; ?>"
                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('contents_attachments', 'local_lmsdata'); ?></td>
                        <td class="field_value">
                            <input type="file" name="uploadfile" id="uploadfile" style=" float:left;" accept=".mp4"/>
                                <input type="hidden" class="" name="filename" value="<?php
                                if ($temp->itemid > 0) {
                                    echo 1;
                                } else {
                                    echo 0;
                                }
                                ?>"/>

                                <div class="mg-tp10">
                                    <video muted width="200px" height="100px">
                                        <source src="<?php echo $pathvs;?>" type="video/mp4" alt="">
                                    </video>
                                    </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="field_title"><?php echo get_string('banner:use', 'local_lmsdata'); ?></td>
                        <td class="field_value">
<?php if ($banner->isused == 1) { ?>
                                <input type="radio" name="isused" value="1" checked="checked"><?php echo get_string('use', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0"><?php echo get_string('unuse', 'local_lmsdata'); ?></td>
    <?php } else {
    ?>
                        <input type="radio" name="isused" value="1" ><?php echo get_string('use', 'local_lmsdata'); ?>&nbsp;&nbsp;&nbsp; <input type="radio" name="isused" value="0" checked="checked"><?php echo get_string('unuse', 'local_lmsdata'); ?></td>
                        </td>
                        </tr>
<?php } ?>            
                </table>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div id="btn_area">
                    <input type="submit" id="add_user" class="blue_btn btn-area-right" value="<?php echo get_string('save', 'local_lmsdata'); ?>" />
                    <input type="button" id="banner_list" class="normal_btn" value="<?php echo get_string('list2', 'local_lmsdata'); ?>" style="float:left;" />
                </div><!--Btn Area End-->
            </form>
        </div><!--Form Popup End-->
    </div><!--Content End-->
</div> <!--Contents End-->  
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script type="text/javascript">
    $(function () {
        $("input[name=uploadfile]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["mp4"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2','local_management')?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });
        // $('#color').colorpicker();
        //
        // if($('input[name=startdate]').val() != '' || $('input[name=enddate]').val() != '') {
        //     $('input[name=reger]').prop('checked', true);
        //     $('input[name=startdate]').prop('disabled', false);
        //     $('input[name=enddate]').prop('disabled', false);
        // }
        // $('input[name=reger]').click(function () {
        //     if ($('input[name=reger]').is(':checked')) {
        //         $('input[name=startdate]').removeAttr('disabled');
        //         $('input[name=enddate]').removeAttr('disabled');
        //     } else {
        //         $('input[name=startdate]').val('');
        //         $('input[name=startdate]').attr('disabled', 'disabled');
        //         $('input[name=enddate]').val('');
        //         $('input[name=enddate]').attr('disabled', 'disabled');
        //     }
        //
        // });
        // $('input[name=category]').click((function (event) {
        //     if ($(this).prop('checked')) {
        //         $('input[name=category]').prop('checked', false);
        //         $(this).prop('checked', true);
        //     }
        //     if ($(this).val() > 0) {
        //         $('input[name=order]').attr('disabled', true);
        //         $('input[name=search]').attr('disabled', true);
        //     } else {
        //         $('input[name=order]').attr('disabled', false);
        //         $('input[name=search]').attr('disabled', false);
        //     }
        // }));
        // $('input[name=site]').click((function (event) {
        //     if ($(this).prop('checked')) {
        //         $('input[name=site]').prop('checked', false);
        //         $(this).prop('checked', true);
        //     }
        // }));
        // $('input[name=search]').click((function (event) {
        //     if ($(this).prop('checked')) {
        //         $('input[name=search]').prop('checked', false);
        //         $(this).prop('checked', true);
        //     }
        // }));
        $('input[name=isused]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=isused]').prop('checked', false);
                $(this).prop('checked', true);
            }
        }));
    });
    $(document).ready(function () {
        $('#banner_list').click(function () {
            location.href = "./banner.php";
        });
    });
    // $("#timeavailable").datepicker({
    //     dateFormat: "yy-mm-dd",
    //     onClose: function (selectedDate) {
    //         $("#timedue").datepicker("option", "minDate", selectedDate);
    //     }
    // });
    // $("#timedue").datepicker({
    //     dateFormat: "yy-mm-dd",
    //     onClose: function (selectedDate) {
    //         $("#timeavailable").datepicker("option", "maxDate", selectedDate);
    //     }
    // });
    function chkfile() {
        var file_id = document.getElementByName("file_id").value;
        var fileCheck = document.getElementById("uploadfile").value;
        if ((file_id != 1 && !fileCheck)) {
            alert("<?php echo get_string('banner:alert', 'local_management'); ?>");
            return false;
        }
    }
</script>