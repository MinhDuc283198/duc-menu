<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
global $DB;
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once dirname(__FILE__) . '/lib.php';

if (!isloggedin()) {
    echo '<script type="text/javascript">
            alert("로그아웃 되어 저장 되지 않았습니다. 로그인 페이지로 이동합니다.");
            document.location.href="' . $SITECFG->wwwroot . '/main/user/login.php";
        </script>';
}

$id = optional_param('id', 0, PARAM_INT);
$name = required_param('name', PARAM_TEXT);
$mod = optional_param('mod', "", PARAM_TEXT);
$filename = optional_param('filename', 0, PARAM_INT);
$isused = optional_param('isused', 0, PARAM_INT);
$thistime = time();
$newdata = new object();
$newdata->id = $id;
$newdata->name = $name;
$newdata->isused = $isused;
$newdata->filename = $filename;
$message = "";
if ($isused == 1) { //하단배너&사용함으로 설정한경우
    $chk = $DB->get_records_sql('SELECT * FROM {banner_video} WHERE isused = 1  AND id != :id ', array('id' => $id));
}
$context = context_system::instance();
if ($mod == "edit") {
    if($isused==1){
        foreach($chk as $ch){
            $ch->isused=0;
            $DB->update_record('banner_video',$ch);
        }
    }
    $newdata->id = $id;
    $newdata->timemodified = time();
    $DB->update_record('banner_video', $newdata);

    $itemid = $id;
} else if ($mod == "add") {
    if($isused==1){
        foreach($chk as $ch){
            $ch->isused=0;
            $DB->update_record('banner_video',$ch);
        }
    }
    $temp = new stdClass();
    $newdata->timecreated = time();
    $newdata->timemodified = time();
    $temp = $DB->insert_record('banner_video', $newdata);

    $id = $temp;
}

if ($filename != 0) {

    $fs = get_file_storage();

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if ($mod == 'edit' && !($_FILES['uploadfile']['name'] == null || $_FILES['uploadfile']['name'] == '')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $itemid, 'id');
        $cnt = 0;
        foreach ($overlap_files as $file) {
            if ($file->get_filesize() > 0) {
                if ($file_del) {
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }
    }


    if (!empty($_FILES['uploadfile']['tmp_name'])) {
        $file_record = array(
            'contextid' => $context->id,
            'component' => 'local_lmsdata',
            'filearea' => 'banner',
            'itemid' => $id,
            'filepath' => '/',
            'filename' => $_FILES['uploadfile']['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile']['tmp_name']);
        video_banner_resize(1600, $context->id, 'banner_video', 'banner', $id);
    }
}

//-----모바일
if (!empty($message)) {
    echo '<script type="text/javascript">
            alert("' . $message . '");
        </script>';

}
redirect($SITECFG->wwwroot . '/local/management/bannerV/banner.php');