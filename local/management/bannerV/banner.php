<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage', 'local_lmsdata'),
    'heading' => get_string('banner_manage', 'local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerV',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/local/management/bannerV/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');

$currpage = optional_param('page', 1, PARAM_INT);     // which page to show
$perpage = optional_param('perpage', 10, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange = optional_param('pagerange', 10, PARAM_INT);  //하단에 리스트에 보이는 페이지수
$id = optional_param('id', 0, PARAM_INT);

$isused = optional_param('isused', -1, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$sortcol = optional_param('sortcol', '', PARAM_RAW);
$isasc = optional_param('isasc', -1, PARAM_INT);

$totalcount = video_banner_get_banner_count();

//if(optional_param('resizeallbanner', 0, PARAM_INT) == 1) {
//    global $DB;
//    $_banners = $DB->get_records('banner_video');
//    foreach ($_banners as $_banner) {
//        video_banner_resize(1600, $context->id, 'local_lmsdata', 'banner', $_banner->id);
//    }
//}

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <span><?php echo get_string('total', 'local_management', $totalcount) ?></span>

                <form method='post' id='form' action='<?php echo $CFG->wwwroot ?>/local/management/bannerV/banner_delete.php'>
                    <table>
                        <caption class="hidden-caption"><?php echo get_string('banner_manage', 'local_lmsdata'); ?></caption>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="10%">No.</th>
                                <th scope="row" width="25%" onclick="sortcol('name', <?php echo ($sortcol != 'name' ? 1:($isasc ? 0:1)); ?>)"><?php echo get_string('banner:name', 'local_lmsdata').' '.($sortcol != 'name' ? '↕':($isasc ? '↑':'↓')); ?></th>
                                <th scope="row" width="25%"><?php echo get_string('contents_attachments', 'local_lmsdata'); ?></th>
                                <th scope="row" width="15%"><?php echo get_string('banner:use', 'local_lmsdata'); ?></th>
                                <th scope="row" width="20%" onclick="sortcol('created_at', <?php echo ($sortcol != 'created_at' ? 1:($isasc ? 0:1)); ?>)"><?php echo get_string('banner:created_at', 'local_lmsdata').' '.($sortcol != 'created_at' ? '↕':($isasc ? '↑':'↓')); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $offset = 0;
                            if ($currpage != 0) {
                                $offset = ($currpage - 1) * $perpage;
                            }
                            $num = $totalcount - $offset;
                            $banners = video_banner($offset, $perpage);
                            if ($totalcount) {
                                foreach ($banners as $banner) {
                                    $fs = get_file_storage();
                                    $files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
                            ?>
                                    <tr>
                                        <td class="chkbox">
                                            <input class="chkbox" type="checkbox" title="check" id="chkbox" name="bannerid<?php echo $banner->id; ?>" value="<?php echo $banner->id; ?>" />
                                        </td>
                                        <td class="number">
                                            <?php echo $num; ?>
                                        </td>
                                        <td>
                                            <a href='<?php echo $CFG->wwwroot ?>/local/management/bannerV/banner_view.php?mod=edit&id=<?php echo $banner->id ?>'><?php echo $banner->name ?></a>
                                        </td>
                                        <?php
                                        if($banner->id){
                                            $output='';
                                            foreach($files as $file){
                                                $filename1=$file->get_filename();
                                                $mimetype = $file->get_mimetype();
                                                if($mimetype=='video/mp4'){
                                                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/local_lmsdata/banner/' . $banner->itemid . '/' . $filename1);
                                                }
                                            }
                                        }
                                    ?>

                                        <td>
                                            <video muted width="100px" height="100px">
                                                <source src="<?php echo $path;?>" type="video/mp4" alt="">
                                            </video>
                                        </td>
                                        <td>
                                            <?php
                                            if (($banner->isused) == 1) {
                                                echo get_string('use', 'local_lmsdata');
                                            } else {
                                                echo get_string('unuse', 'local_lmsdata');
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo (!empty($banner->timecreated)) ? date('Y-m-d', $banner->timecreated):'-'; ?></td>
                                    <?php
                                    $num--;
                                }
                            } else if (!$totalcount) {
                                    ?>
                                    <tr>
                                        <td colspan="12"><span><?php echo get_string('banner:nodata', 'local_lmsdata'); ?></span></td>
                                    </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                    <div id="btn_area">
                        <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/bannerV/banner_write.php?mod=add"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" />
                        <input type="button" id="delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" />
                    </div>
                </form>
                <?php print_paging_navbar($totalcount, $currpage, $perpage, $CFG->wwwroot . "/local/management/bannerM/banner.php?perpage=$perpage&sortcol=$sortcol&isasc=$isasc&isused=$isused&searchtext=$searchtext"); ?>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    // $(function() {
    //        $("#allcheck").click(function() {
    //            var chk = $("#allcheck").is(":checked");
    //
    //            if (chk) {
    //                $(".chkbox input").each(function() {
    //                    this.checked = true;
    //                });
    //            } else {
    //                $(".chkbox input").each(function() {
    //                    this.checked = false;
    //                });
    //            }
    //        });
    $(function() {
        $("#allcheck").click(function() {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function() {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function() {
                    this.checked = false;
                });
            }
        });
        $("#delete_button").click(function() {
            if (!$(".chkbox").is(":checked")) {
                alert("<?php echo get_string('delalert', 'local_lmsdata'); ?>");
                return false;
            }

            if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
                $('#form').submit();
            }
        });

        $('#reset').on('click', function(){
            window.location.href = "<?php echo $CFG->wwwroot; ?>/local/management/bannerV/banner.php";
        })
    });

    function sortcol(sortcol, isasc) {
        $('input[name=sortcol]').val(sortcol);
        $('input[name=isasc]').val(isasc);
        $('#frm_search').submit();
    }
</script>