<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('banner_manage','local_lmsdata'),
    'heading' => get_string('banner_manage','local_lmsdata'),
    'subheading' => '',
    'menu' => 'bannerV',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('banner_write.php');
    redirect(get_login_url());
}


$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once($CFG->dirroot . '/chamktu/board/lib.php');
include_once($CFG->dirroot . '/local/management/header.php');

$mod = optional_param('mod', "", PARAM_TEXT);
$temp->itemid = $banner->id;



$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'local_lmsdata', 'banner', $banner->itemid, "", false);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form action="banner_submit.php" id="banner_submit" enctype="multipart/form-data" method="POST" onsubmit="return chkfile()">
                    <table>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:name','local_lmsdata');?></td>
                            <td style="text-align:left;" colspan="3"><input type="text" name="name" style="width:70%;"></td>
                        </tr>
                        <tr>
                            <td class="field_title" style="background-color: #F2F2F2"><?php echo get_string('contents_attachments','local_lmsdata');?><br />(mp4)</td>
                            <td class="field_value number" colspan="3">
                                <div style="width:100%; clear: both; float: left;  padding: 5px;"><input type="file" name="uploadfile" id="uploadfile" style=" float:left;" accept=".mp4"/>
                                    <input type="hidden" class="" name="filename" value="<?php echo $temp->itemid ? $temp->itemid : -1 ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td  style="background-color: #F2F2F2"><?php echo get_string('banner:use','local_lmsdata');?></td>
                            <td colspan="3" style="text-align:left;">
                                <input type="checkbox" name="isused" value="1" checked="checked"><?php echo get_string('use','local_lmsdata');?>&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" name="isused" value="0"><?php echo get_string('unuse','local_lmsdata');?>
                            </td>
                        </tr>
                        <input type="hidden" name="mod" value="<?php echo $mod ?>">
                    </table>
                    <div clas="btn_area">
                        <a href="<?php echo $CFG->wwwroot . '/local/management/bannerV/banner.php'; ?>"><input type="button" id="notice_list" class="blue_btn" value="<?php echo get_string('banner:list','local_lmsdata');?>" style="float: left; border: 1px solid #999" /></a>

                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save','local_lmsdata');?>" style="float: right; margin: 0 10px 0 0" />

                    </div> <!-- Bottom Button Area -->
                </form>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    $(function() {
        $("input[name=uploadfile]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["mp4"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2','local_management')?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });

        //$("input[name=uploadfile_mo]").change(function () {
        //    if ($(this).val() != "") {
        //        var ext = $(this).val().split(".").pop().toLowerCase();
        //        if ($.inArray(ext, ["mp4"]) == -1) {
        //            alert("<?php //echo get_string('goods:valimgtype2','local_management')?>//");
        //            $(this).val("");
        //            //$('.file-name').val('');
        //            return;
        //        }
        //        thumbnailimg = $(this).val();
        //    }
        //});

        // $('#color').colorpicker();

        // $('input[name=reger]').click(function () {
        //     if($('input[name=reger]').is(':checked')){
        //          $('input[name=startdate]').removeAttr('disabled');
        //          $('input[name=enddate]').removeAttr('disabled');
        //     } else {
        //         $('input[name=startdate]').attr('disabled','disabled');
        //         $('input[name=enddate]').attr('disabled','disabled');
        //     }
        //
        // });
       //  $('input[name=category]').click((function (event) {
       //      if ($(this).prop('checked')) {
       //          $('input[name=category]').prop('checked', false);
       //          $(this).prop('checked', true);
       //          if($(this).val() > 0){
       //               $('input[name=target]').attr('disabled',true);
       //               $('input[name=search]').attr('disabled',true);
       //          } else {
       //              $('input[name=target]').attr('disabled',false);
       //              $('input[name=search]').attr('disabled',false);
       //          }
       //      }
       // }));
       // $('input[name=search]').click((function (event) {
       //      if ($(this).prop('checked')) {
       //          $('input[name=search]').prop('checked', false);
       //          $(this).prop('checked', true);
       //      }
       // }));
       // $('input[name=site]').click((function (event) {
       //      if ($(this).prop('checked')) {
       //          $('input[name=site]').prop('checked', false);
       //          $(this).prop('checked', true);
       //      }
       // }));
       $('input[name=isused]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=isused]').prop('checked', false);
                $(this).prop('checked', true);
            }
       }));
    });

    // $("#timeavailable").datepicker({
    //     dateFormat: "yy-mm-dd",
    //     onClose: function (selectedDate) {
    //         $("#timedue").datepicker("option", "minDate", selectedDate);
    //     }
    // });
    // $("#timedue").datepicker({
    //     dateFormat: "yy-mm-dd",
    //     onClose: function (selectedDate) {
    //         $("#timeavailable").datepicker("option", "maxDate", selectedDate);
    //     }
    // });
    function chkfile(){
        var fileCheck = document.getElementById("uploadfile").value;
        if(!fileCheck){
            alert("<?php echo get_string('banner:alert','local_management');?>");
            return false;
        }
    }
    </script>