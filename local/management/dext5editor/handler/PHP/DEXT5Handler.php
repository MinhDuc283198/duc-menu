<?php

	class DEXT5Handler
	{
		
		// 기본 옵션 정보
		private static $defaultOptions = Array(
			'_fontName' => './PHP/ARIAL.TTF',
			'_fontSize' => 50,
			'_fontColor' => 0x1e90ff
		);
		
		//에러 메세지
		private static $LastErrorMessage = Array(
			'LastErrorMessage' => ''
		);
		
		public static function LastErrorMessage(){
			return 	DEXT5Handler::$LastErrorMessage['LastErrorMessage'];
		}
		
		/**
		 * 동일 폴더에 사용자가 지정한 크기로 썸네일 파일을 생성합니다.
		 * 원본보다 크게 설정하는 경우, 원본의 크기로 생성이 됩니다.
		 * 
		 * @param	String	$strSourceFile	원본 파일이 있는 경로를 지정한다.
		 * @param	Array	$strSuffix	 	원본 파일에서 파일명과 확장자 사이에 삽입할 문자열
		 * @param	Number	$nNewWidth		섬네일 넓이. 하나가 null 인 경우 다른 값(width 또는 height)을 기준으로 이미지의 비율에 맞게 크기가 설정된다.
		 * @param	Number	$nNewHeight		섬네일 높이.
		 * 
		 * @return	int		0이면 성공, 0 이외의 값이면 실패
		 */
		public static function imageThumbnail($strSourceFile, $strSuffix, $nNewWidth, $nNewHeight)
		{
			$rtn_value = -1;
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
	
			$type = getimagesize($strSourceFile);
			// 1 = GIF, 2 = JPEG, 3 = PNG
			switch ($type[2])
			{
				case 2: $strConvertFormat = 'jpg'; break;
				case 3: $strConvertFormat = 'png'; break;
				default: $strConvertFormat = 'jpg'; break;
			}
			
			$_size = DEXT5Handler::GetImageNewSize($strSourceFile, $nNewWidth, $nNewHeight, FALSE);
			
			preg_match('@^(.+/)?([^/]+)\.([^.]+)?$@', $strSourceFile, $m);
			$strNewFileName = $m[1].$m[2].$strSuffix.'.'.$strConvertFormat;
			
			if(DEXT5Handler::DEXTImageConvertResize($strSourceFile, $strNewFileName, $_size, $strConvertFormat) == TRUE){
				$rtn_value = 0;
			};
				
			return $rtn_value;
		}// END: function imageThumbnail();
		
		/**
		 * 사용자가 지정한 경로,파일명,크기로 썸네일 파일을 생성합니다.
		 * 원본보다 크게 설정 가능 합니다.
		 * 
		 * @param	String	$strSourceFile		    원본 파일이 있는 경로를 지정한다.
		 * @param	Array	$strNewFileName	    대상 파일 경로 및 파일명
		 * @param	Number	$nNewWidth		    섬네일 넓이. 하나가 null 인 경우 다른 값(width 또는 height)을 기준으로 이미지의 비율에 맞게 크기가 설정된다.
		 * @param	Number	$nNewHeight	    섬네일 높이.
		 * @param	Number	$nDeleteOldFile	    원본 파일 삭제 여부 (0: 원본 보존 / 1: 원본 파일 삭제)
		 *
		 * @return	int		0이면 성공, 0 이외의 값이면 실패
		 */
		public static function GetImageThumbOrNewEx($strSourceFile, $strNewFileName, $nNewWidth, $nNewHeight, $nDeleteOldFile)
		{
			$rtn_value = -1;
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
	
			$type = getimagesize($strSourceFile);
			// 1 = GIF, 2 = JPEG, 3 = PNG
			switch ($type[2])
			{
				case 2: $strConvertFormat = 'jpg'; break;
				case 3: $strConvertFormat = 'png'; break;
				default: $strConvertFormat = 'jpg'; break;
			}
			
            $_size = DEXT5Handler::GetImageNewSize($strSourceFile, $nNewWidth, $nNewHeight, TRUE);
			
			if(DEXT5Handler::DEXTImageConvertResize($strSourceFile, $strNewFileName, $_size, $strConvertFormat) == TRUE){
				$rtn_value = 0;
			};
			
			if($nDeleteOldFile == 1 ){
				if(is_file($strSourceFile) == TRUE){
					unlink($strSourceFile);
				}
			}
				
			return $rtn_value;
		}// END: function GetImageThumbOrNewEx();
	
		/**
		 * 
		 * @param	String	$strSourceFile		    원본파일.
		 * @param	String	$strNewFileName	    생성될 경로 및 파일명.
		 * @param	Array	$newSize			        생성될 사이즈.
		 * @param	String	$strConvertFormat	    생성될 파일 확장자.
		 * 
 	 	 *	- 설정된 키워드
		 *		%PATH%			    원본 이미지 경로
		 *		%FILENAME%		원본 이미지 파일명 (확장자 제외)
		 *		%EXT%			    섬네일 출력 형식 확장자 
		 *		%THUMB_WIDTH	섬네일 넓이
		 *		%THUMB_HEIGHT	섬네일 높이
		 *		%IMAGE_WIDTH	원본 이미지 넓이
		 *		%IMAGE_HEIGHT	원본 이미지 높이 
		 * 
		 * @return	Boolean		TRUE이면 성공, FALSE이외의 값이면 실패
		 */
		public static function DEXTImageConvertResize($strSourceFile, $strNewFileName, $newSize, $strConvertFormat)
		{
			$rtn_value = FALSE;
            
			$type = getimagesize($strSourceFile);
			// 1 = GIF, 2 = JPEG, 3 = PNG
			switch ($type[2])
			{
				case 2: $image = imagecreatefromjpeg($strSourceFile); break;
				case 3: $image = imagecreatefrompng($strSourceFile); break;
				default: $image = imagecreatefromjpeg($strSourceFile); break;
			}
	
			// AntiAlias
			if (function_exists('imageantialias'))
				imageantialias($image, TRUE);
	
			$image_attr = getimagesize($strSourceFile);
			$image_width = $image_attr[0];
			$image_height = $image_attr[1];
			
			$thumb_width = $newSize['newWidth'];
			$thumb_height = $newSize['newHeight'];
			
			$thumbnail = imagecreatetruecolor($thumb_width, $thumb_height);
			@imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width, $image_height);
	
			preg_match('@^(.+/)?([^/]+)\.([^.]+)?$@', $strNewFileName, $m);
			
			$savepath = str_replace(Array('%PATH%', '%FILENAME%', '%EXT%', '%THUMB_WIDTH%', '%THUMB_HEIGHT%', '%IMAGE_WIDTH%', '%IMAGE_HEIGHT%'), Array($m[1], $m[2], $strConvertFormat, $nNewWidth, $nNewHeight, $image_width, $image_height), $m[1].$m[2].".".$strConvertFormat);
			DEXT5Handler::validatePath($savepath);
	
			switch ($strConvertFormat)
			{
				case png: if ( imagepng($thumbnail, $savepath,100)) $rtn_value = TRUE; break;
				case jpg:
				default: if ( imagejpeg($thumbnail, $savepath,100)) $rtn_value = TRUE; break;
			}
			
			return $rtn_value;
		}
		
		/**
		 * 썸네일 사이즈 
		 * 
		 * @param	String 	$strSourceFile	원본파일.
		 * @param	Number	$nNewWidth		섬네일 넓이.
		 * @param	Number	$nNewHeight	섬네일 높이.
		 * @param	Boolean	$isSizeOver		원본보다 큰이미지 혀용  TRUE,FALSE.
		 * 
		 * @return	Array	$_size	
		 */
		public static function GetImageNewSize($strSourceFile, $nNewWidth, $nNewHeight, $isSizeOver)
		{
			
			$image_attr = getimagesize($strSourceFile);
			$image_width = $image_attr[0];
			$image_height = $image_attr[1];
			
			$_size = array(	'newWidth' => $nNewWidth,
							'newHeight' => $nNewHeight );
			
			if($nNewWidth + $nNewHeight == 0){
				$_size['newWidth'] = $image_width;
				$_size['newHeight'] = $image_height;	
				return $_size;	
			}
			
			if($isSizeOver){
				
				if ($nNewWidth == 0)
				{
					$nNewWidth = intval($image_width * ($nNewHeight / ($image_height * 1.0)));
				}
				if ($nNewHeight == 0)
				{
					$nNewHeight = intval($image_height * ($nNewWidth / ($image_width * 1.0)));
				}
					
			}else {
				if ($nNewWidth == 0 || $nNewWidth > $image_width)
				{
					if ($nNewWidth == 0 && $nNewHeight > 0 && $nNewHeight < $image_width)
					{
						$nNewWidth = intval($image_width * ($nNewHeight / ($image_height * 1.0)));
					}
					else { $nNewWidth = $image_width; }
				}
	
				if ($nNewHeight == 0 || $nNewHeight > $image_height)
				{
					if ($nNewHeight == 0 && $nNewWidth > 0 && $nNewWidth < $image_width)
					{
						$nNewHeight = intval($image_height * ($nNewWidth / ($image_width * 1.0)));
					}
					else { $nNewHeight = $image_height; }
				}
			}
					
			$_size['newWidth'] = $nNewWidth;
			$_size['newHeight'] = $nNewHeight;
			
			return $_size;
		}
		
		/**
		 * 새로운 파일 이름의 문자열을 생성합니다
		 * 
		 * @param	String 	$sExtension		확장자.
		 * @param	String 	$sType			파일명 생성 타입을 설정합니다. “GUID”, “TIME” 중 하나를 사용합니다..
		 * 
		 * @return	int		    $rtn_value	
		 */
		public static function GetNewFileNameEx($sExtension, $sType)
		{
			$rtn_value = "";
			if (strcmp(strtolower($sType),strtolower('TIME')) == 0)
			{
				$rtn_value = date('YmdGis');                      
			}
			else
			{
			   if (function_exists('com_create_guid')){
			       $rtn_value = com_create_guid();
			   }else{
			       mt_srand((double)microtime()*10000);
			       $rtn_value = strtoupper(md5(uniqid(rand(), true)));
			   }
			}
			$rtn_value = str_replace('{','',$rtn_value);
			$rtn_value = str_replace('}','',$rtn_value);
			$rtn_value = str_replace('-','',$rtn_value);
			
			if (strlen($sExtension) > 0)
			{
				$rtn_value = $rtn_value.'.'.$sExtension;
			}
	
			return $rtn_value;
		}
				
		/**
		 * 원본 파일을 새로운 이미지 포맷으로 변경합니다
		 * 
		 * @param	String 	$strSourceFile		    원본 파일.
		 * @param	String 	$sNewFormat			jpg 또는 png만 지원합니다.
		 * @param	String 	$nSkipSameFormat    	동일한 포멧인 경우 변환하지 않고 Skip합니다.(1 : Skip 처리).
		 * 
		 * @return	int		$rtn_value	
		 */		
		public static function ImageConvertFormat($strSourceFile, $sNewFormat, $nSkipSameFormat)
		{
			$rtn_value = -1;
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			// 이미지 타입
			// 1 = GIF, 2 = JPEG, 3 = PNG
			$type = getimagesize($strSourceFile);
			switch ($type[2])
			{
				case 2: $strConvertFormat = 'jpg'; break;
				case 3: $strConvertFormat = 'png'; break;
				default: $strConvertFormat = 'jpg'; break;
			}
			if($nSkipSameFormat == 1 && strcmp(strtolower($sNewFormat), $strConvertFormat) == 0){
				return $rtn_value = 0;
			}
			
			switch (strtolower($sNewFormat)) {
				case 'jpg':	break;
				case 'png':	break;
				default: $sNewFormat = 'jpg'; break;
			}
			
			$_size = DEXT5Handler::GetImageNewSize($strSourceFile, $nNewWidth, $nNewHeight, FALSE);
			
			preg_match('@^(.+/)?([^/]+)\.([^.]+)?$@', $strSourceFile, $m);
			$strNewFileName = $m[1].$m[2].'_new.'.$sNewFormat;
            
			if(DEXT5Handler::DEXTImageConvertResize($strSourceFile, $strNewFileName, $_size, $sNewFormat) == TRUE){
				$rtn_value = 0;
			};
		
			return $rtn_value;
		}
	
		/**
		 * 원본 이미지를 지정한 크기로 변환합니다.
		 * 가로 또는 세로 크기에서 하나의 값만 0보다 크게 설정하면, 다른 부분의 크기는 자동으로 계산되어 설정됩니다.
		 * 
		 * @param	String 	$strSourceFile	원본 파일.
		 * @param	String 	$nNewWidth		가로크기.
		 * @param	String 	$nNewHeight		세로크기.
		 * 
		 * @return	int		$rtn_value	
		 */	
		public static function ImageConvertSize($strSourceFile, $nNewWidth, $nNewHeight){
			$rtn_value = -1;
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			$type = getimagesize($strSourceFile);
			switch ($type[2])
			{
				case 2: $strConvertFormat = 'jpg'; break;
				case 3: $strConvertFormat = 'png'; break;
				default: $strConvertFormat = 'jpg'; break;
			}
			if($nSkipSameFormat == 1 && strcmp(strtolower($sNewFormat), $strConvertFormat) == 0){
				return $rtn_value = 0;
			}
			
			switch (strtolower($sNewFormat)) {
				case 'jpg':	break;
				case 'png':	break;
				default: $sNewFormat = 'jpg'; break;
			}
			
			$_size = DEXT5Handler::GetImageNewSize($strSourceFile, $nNewWidth, $nNewHeight, TRUE);
			
			if(DEXT5Handler::DEXTImageConvertResize($strSourceFile, $strSourceFile, $_size, $strConvertFormat) == TRUE){
				$rtn_value = 0;
			};
		
			return $rtn_value;
		}
		
		
		/**
		 * 대상(원본) 이미지를 지정된 퍼센트로 축소하거나 늘립니다.
		 * nPercent 에 50을 입력하면 이미지가 절반으로 축소되고, 200을 입력한 경우 2배로 확대됩니다
		 * 
		 * @param	String 	$strSourceFile	원본 파일.
		 * @param	String 	$nPercent		변환비율.
		 * 
		 * @return	int		    $rtn_value	
		 */	
		public static function ImageConvertSizeByPercent($strSourceFile, $nPercent){
			$rtn_value = -1;
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			$type = getimagesize($strSourceFile);
			switch ($type[2])
			{
				case 2: $strConvertFormat = 'jpg'; break;
				case 3: $strConvertFormat = 'png'; break;
				default: $strConvertFormat = 'jpg'; break;
			}
			if($nSkipSameFormat == 1 && strcmp(strtolower($sNewFormat), $strConvertFormat) == 0){
				return $rtn_value = 0;
			}
			
			switch (strtolower($sNewFormat)) {
				case 'jpg':	break;
				case 'png':	break;
				default: $sNewFormat = 'jpg'; break;
			}
			
			$_size = DEXT5Handler::GetImageSizeByPercent($strSourceFile, $nPercent);
			
			if(DEXT5Handler::DEXTImageConvertResize($strSourceFile, $strSourceFile, $_size, $strConvertFormat) == TRUE){
				$rtn_value = 0;
			};
		
			return $rtn_value;
		}

		
		/**
		 * 대상이미지 변환될 이미지 사이즈 비율 반환
		 * 
		 * @param	String 	$strSourceFile	원본 파일.
		 * @param	String 	$nPercent		변환비율.
		 * 
		 * @return	Array	    $_size	
		 */	
		public static function GetImageSizeByPercent($strSourceFile, $nPercent){
			
			$_size = array(	'newWidth' => 0,
							'newHeight' => 0 );
			
			$image_attr = getimagesize($strSourceFile);
			$image_width = $image_attr[0];
			$image_height = $image_attr[1];
			
			$_size['newWidth'] = intval($image_width * $nPercent / 100);
			$_size['newHeight'] = intval($image_height * $nPercent / 100);
			
			return $_size;
		}
		
		/**
		  * 원본 이미지를 회전한 이미지로 변환합니다.(반 시계 방향)
		  * 
		  * @param	String	    $strSourceFile		원본 파일.
		  * @param	int 	    $nAngle				회전각도 (0, 90, 180, 270, 360 값 중 하나 선택).
		  * 
		  * @return	int		    $rtn_value			0이외의 값 실패	
		  */
		public static function ImageRotate($strSourceFile, $nAngle)
		{
			
			$rtn_value = -1;
		 	if ( ! file_exists($strSourceFile)){
		 		DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			$check = FALSE;
			switch($nAngle){
				case 0: break;
				case 90: break;
				case 180: break;
				case 270: break;
				case 360: break;
				default: DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'Check Angle Value'; return $rtn_value; break;
			}
			
			$type = getimagesize($strSourceFile);
			switch ($type[2])
			{
				case 2: $image= imagecreatefromjpeg($strSourceFile); break;
				case 3: $image= imagecreatefrompng($strSourceFile); break;
				default: $image= imagecreatefromjpeg($strSourceFile); break;
			}
			
			$rotate = imagerotate($image, $nAngle, 0);
		 
			switch ($type[2])
			{
				case 2: header('Content-type: image/jpeg'); if(imagejpeg($rotate, $strSourceFile,100)) $rtn_value = 0; break;
				case 3: header('Content-type: image/png'); if(imagepng($rotate, $strSourceFile,100)) $rtn_value = 0; break;
				default: header('Content-type: image/jpeg'); if(imagejpeg($rotate, $strSourceFile,100)) $rtn_value = 0; break;
			}
				
			return $rtn_value;
		}
		 
		
		/**
		 * 대상(원본) 이미지를 지정된 워터마크 이미지로 워터마크 처리를 합니다.
		 * 이미지 파일에 회사의 로고나 출처를 알릴 때 유용할 수 있습니다.
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * @param	String 	$strWaterMarkFile	워터마크로 사용할 이미지 파일.
		 * @param	String 	$strVAlignFrom		세로 정렬(TOP, MIDDLE, BOTTOM 값 중 하나를 사용).
		 * @param	int 	$nVMargin			픽셀값, strVAlignFrom 값으로부터의 여백, "MIDDLE" 인 경우 무시됨.
		 * @param	String 	$strHAlignFrom		가로 정렬 (LEFT, CENTER, RIGHT 값 중 하나를 사용).
		 * @param	int 	$nHMargin			픽셀값, strHAlignFrom 값으로부터의 여백, "CENTER" 인 경우 무시됨.
		 * @param	int 	$nTransparency		워터마크 투명도 (100 이면 완전투명이고 0이면 완전불투명).
		 * 
		 * @return	int		$rtn_value			0이외의 값 실패
		 */	
		public static function ImageWaterMark($strSourceFile, $strWaterMarkFile, $strVAlignFrom, $nVMargin, $strHAlignFrom, $nHMargin, $nTransparency)
		{
			$rtn_value = -1;
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			$sourceImage = getimagesize($strSourceFile);
			switch ($sourceImage[2])
			{
				case 2: $image = imagecreatefromjpeg($strSourceFile); header("Content-Type: image/jpeg"); break;
				case 3: $image = imagecreatefrompng($strSourceFile); header("Content-Type: image/png"); break;
				default: $image = imagecreatefromjpeg($strSourceFile); header("Content-Type: image/jpeg"); break;
			}
			
			$watermarkImage = getimagesize($strWaterMarkFile);
			switch ($watermarkImage[2])
			{
				case 2: $watermark = imagecreatefromjpeg($strWaterMarkFile); break;
				case 3: $watermark = imagecreatefrompng($strWaterMarkFile); break;
				default: $watermark = imagecreatefromjpeg($strWaterMarkFile); break;
			}
			
			$imgWidth = imagesx($image); 
			$imgHeight = imagesy($image); 
			
			$watermarkWidth = imagesx($watermark); 
			$watermarkHeight = imagesy($watermark); 
			
			if ($strVAlignFrom == 'BOTTOM')
			{
				$nVMargin = intval($imgHeight - $nVMargin - $watermarkHeight);
			}
			else if ($strVAlignFrom == 'MIDDLE')
			{
				$nVMargin = intval(($imgHeight / 2) - ($watermarkHeight / 2));
			}
			
			if ($strHAlignFrom == 'RIGHT')
			{
				$nHMargin = intval($imgWidth - $nHMargin - $watermarkWidth);
			}
			else if ($strHAlignFrom == 'CENTER')
			{
				$nHMargin = intval(($imgWidth / 2) - ($watermarkWidth / 2));
			}
			
			imagecopymerge($image, $watermark, $nHMargin, $nVMargin, 0, 0, imagesx($watermark), imagesy($watermark), 90);
			
			switch ($sourceImage[2])
			{
				case 2: if(imagejpeg($image, $strSourceFile,100)) $rtn_value = 0; break;
				case 3: if(imagepng($image, $strSourceFile,100)) $rtn_value = 0; break;
				default: if(imagejpeg($image, $strSourceFile,100)) $rtn_value = 0; break;
			}
			
			imagedestroy($image);
			
			return $rtn_value;
		}
		
		/**
		 * 대상(원본) 이미지를 지정된 텍스트로 워터마크 처리를 합니다.
		 * 이미지 파일에 회사의 로고나 출처를 알릴 때 유용할 수 있습니다.
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * @param	String 	$strTextDesc		    워터마크 텍스트.
		 * @param	String 	$strVAlignFrom		세로 정렬(TOP, MIDDLE, BOTTOM 값 중 하나를 사용).
		 * @param	int 	    $nVMargin			픽셀값, strVAlignFrom 값으로부터의 여백, "MIDDLE" 인 경우 무시됨.
		 * @param	String 	$strHAlignFrom		가로 정렬 (LEFT, CENTER, RIGHT 값 중 하나를 사용).
		 * @param	int 	    $nHMargin			픽셀값, strHAlignFrom 값으로부터의 여백, "CENTER" 인 경우 무시됨.
		 * @param	int 	    $nTransparency		워터마크 투명도 (100 이면 완전투명이고 0이면 완전불투명).
		 * @param	int 	    $nAngle				텍스트 회전 각도(0 보다 큰 경우, AlignFrom과 Margin값은 무시되고 중앙에 표시가 됩니다.).
		 * 
		 * @return	int		    $rtn_value			0이외의 값 실패s
		 */		
		public static function TextWaterMark($strSourceFile, $strTextDesc, $strVAlignFrom, $nVMargin, $strHAlignFrom, $nHMargin, $nAngle)
		{	
			$rtn_value = -1;

			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			$sourceImage = getimagesize($strSourceFile);
			switch ($sourceImage[2])
			{
				case 2: $image = imagecreatefromjpeg($strSourceFile); break;
				case 3: $image = imagecreatefrompng($strSourceFile); break;
				default: $image = imagecreatefromjpeg($strSourceFile); break;
			}
			
			$imgWidth = imagesx($image); 
			$imgHeight = imagesy($image); 
			
			$fontDir = DEXT5Handler::$defaultOptions['_fontName'];
			$fontSize = DEXT5Handler::$defaultOptions['_fontSize'];
			$fontColor = DEXT5Handler::$defaultOptions['_fontColor'];
			
			$fontln = strlen($strTextDesc);
			
			if ($fontSize <= 0) $fontSize = 20;

			if ($strVAlignFrom == 'BOTTOM')
			{
				$nVMargin = intval($imgHeight - $nVMargin);
			}
			else if ($strVAlignFrom == 'MIDDLE')
			{
				$nVMargin = intval(($imgHeight / 2));
			}
			
			if ($strHAlignFrom == 'RIGHT')
			{
				$nHMargin = intval($imgWidth - $nHMargin);
			}
			else if ($strHAlignFrom == 'CENTER')
			{
				$nHMargin = intval(($imgWidth / 2));
			}

			imagettftext($image, $fontSize, $nAngle, $nHMargin, $nVMargin, $fontColor, $fontDir, $strTextDesc);
			
			switch ($sourceImage[2])
			{
				case 2: if(imagejpeg($image, $strSourceFile,100)) $rtn_value = 0; break;
				case 3: if(imagepng($image, $strSourceFile,100)) $rtn_value = 0; break;
				default: if(imagejpeg($image, $strSourceFile,100)) $rtn_value = 0; break;
			}
			
			imagedestroy($image);
			
			return $rtn_value;			
		}
		
		/**
		 * 대상 이미지의 Width값을 반환합니다.
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * 
		 * @return	int		    $rtn_value			Width.
		 */	
		public static function GetImageWidth($strSourceFile)
		{
			$rtn_value = '';
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
	
			$sourceImage = getimagesize($strSourceFile);
			switch ($sourceImage[2])
			{
				case 2: $image = imagecreatefromjpeg($strSourceFile); break;
				case 3: $image = imagecreatefrompng($strSourceFile); break;
				default: $image = imagecreatefromjpeg($strSourceFile); break;
			}
			
			$rtn_value = imagesx($image); 
			
			return $rtn_value;
		}
		
		/**
		 * 대상 이미지의 Height값을 반환합니다.
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * 
		 * @return	int		    $rtn_value			Height.
		 */
		public static function GetImageHeight($strSourceFile)
		{
			$rtn_value = '';
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
	
			$sourceImage = getimagesize($strSourceFile);
			switch ($sourceImage[2])
			{
				case 2: $image = imagecreatefromjpeg($strSourceFile); break;
				case 3: $image = imagecreatefrompng($strSourceFile); break;
				default: $image = imagecreatefromjpeg($strSourceFile); break;
			}
			
			$rtn_value = imagesy($image); 
			
			return $rtn_value;
		}
		
		/**
		 * 대상 이미지의 포멧형식을 반환합니다.
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * 
		 * @return	String	    $rtn_value			파일포멧형식.
		 */
		public static function GetImageFormat($strSourceFile)
		{
			$rtn_value = '';
			
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
	
			$sourceImage = getimagesize($strSourceFile);
			$rtn_value = $sourceImage[mime]; 
			
			return $rtn_value;
		}
		
		/**
		 * 대상 이미지의 파일사이즈를 반환합니다.
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * 
		 * @return	int		    $rtn_value			파일사이즈 (byte).
		 */
		public static function GetImageFileSize($strSourceFile)
		{
			$rtn_value = '';
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			$rtn_value = filesize($strSourceFile);
			return $rtn_value;
		}
		
		/**
		 * 대상 이미지를 삭제합니다..
		 * 
		 * @param	String 	$strSourceFile		원본 파일.
		 * 
		 * @return	int		    $rtn_value			0이외의 값 실패.
		 */
		public static function DeleteFile($strSourceFile)
		{
			$rtn_value = -1;
			if ( ! file_exists($strSourceFile)){
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'File Not Found!';
				return $rtn_value;
			}
			
			if(is_file($strSourceFile) == TRUE){
				unlink($strSourceFile);
				$rtn_value = 0;
			}else {
				DEXT5Handler::$LastErrorMessage['LastErrorMessage'] = 'Failed to delete file';
			}
		 	flush();
			 
			return $rtn_value;
		}
		
		/**
		 * TextWatermark에서 사용할 폰트를 설정합니다
		 * 
		 * @param	String 	$fontName	폰트명.
		 * @param	int 	    $fontSize	    폰트크기.
		 * @param	String 	$fontColor	폰트 색상 (RGB형식의 Hex).
		 * 
		 * @return	void
		 */	
		public static function SetFontInfo($fontName, $fontSize, $fontColor)
		{
			DEXT5Handler::$defaultOptions['_fontName'] = $fontName;				
			DEXT5Handler::$defaultOptions['_fontSize'] = $fontSize;				
			DEXT5Handler::$defaultOptions['_fontColor'] = $fontColor;				
		}
		
		/**
		 * 경로가 존재하는지 체크하고 없다면 폴더를 생성
		 * @param	String	    $path			체크할 경로
		 * @return	Boolean	
		 */
		public static function validatePath($path)
		{
			$a = explode('/', dirname($path));
			$p = '';
			foreach ($a as $v)
			{
				$p.= $v .'/';
				if ( ! is_dir($p))
					mkdir($p, 0757);
			}
	
			return true;
		}// END: function validatePath();
	
	}// END: class DEXT5Handler
	

?>
