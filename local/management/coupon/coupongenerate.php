<?php
// Standard GPL and phpdocs
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/management/job/lib.php');
require_once(__DIR__ . '/couponlib.php');

// Set up the page.
$title = get_string('coupon_manage', 'local_management');

$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'coupongenerate';
$VISANG->page->addCSS($VISANG->wwwroot. '/classes/dist/css/coupon.css');
$VISANG->page->addJS($CFG->wwwroot. '/local/management/coupon/asset/js/validator.js');
$VISANG->page->addCSS('https://pro.fontawesome.com/releases/v5.10.0/css/all.css');
$VISANG->page->addJS($VISANG->wwwroot. '/classes/dist/js/script.js');
$couponid=optional_param('couponid','',PARAM_INT);
$saved=optional_param('saved','',PARAM_RAW);
$pagetitle = $title;
$url = new moodle_url("/local/management/coupon/coupongenerate.php");
$PAGE->set_url($url);
$PAGE->set_pagelayout('adminmanagement');

//$user_emails = mkj_get_user_emails();

$templatedata = (object)[
    'savecouponurl' => $CFG->wwwroot. '/local/management/coupon/savecoupon.php',
    'renderqrcodeurl' => $CFG->wwwroot. '/local/management/coupon/renderqrcodeurl.php',
    'couponindexurl' => $CFG->wwwroot. '/local/management/coupon/index.php',
    'couponid'=>$couponid,
    'saved'=>$saved,
];

$output = $PAGE->get_renderer('local_management');

echo $output->header();

$renderable = new \local_management\output\coupon_generate_page($templatedata);
echo $output->render($renderable);

echo $output->footer();