<?php
global $DB, $CFG;
require_once(__DIR__ . '/../../../../config.php');
require_once(__DIR__ . '/lipemail.php');
require_once($CFG->dirroot . '/local/management/coupon/couponlib.php');

defined('MOODLE_INTERNAL') || die();

$email_users = array();
if (isset($_POST)) {
    $memberEmails = $_POST["memberEmail"];
    $notmemberEmails = $_POST["notMemberEmail"];
    $couponid =$_POST["couponId"];
    $cpImage =$_POST["cpImage"];
    $cpqrcode =$_POST["cpQrCode"];
}

$coupon = $DB->get_record('vi_coupons', array('id' => $couponid));
$couponcourse=$DB->get_record('vi_coupons_courses', array('coupon_id' => $couponid));
$discount_rate=$coupon->discount_rate;
$key_code=$coupon->key_code;
$expire_date=date_format_string($coupon->expire_date, '%Y/%m/%d');
$course_infor=local_course_class_id($couponcourse->course_id);
$courseurl=$CFG->wwwroot .'/local/course/detail.php?id='.$couponcourse->course_id;
$description=$coupon->description;
$coursetitle=$course_infor->en_coursename;
$dataheader="<!DOCTYPE html>
<html lang='ko'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=Edge' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>
</head>

<body>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese'rel='stylesheet' />
        <table width='500' border='0' cellpadding='0' cellspacing='0' align='center' class='full' style='border-radius: 6px;'>
            <tbody><tr>
                <td width='100%' style='border-radius: 6px; box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 6px 0px; background-color: rgb(255, 255, 255);' bgcolor='#ffffff' c-style='not3Body'>
                    <!-- Start Top -->
                    <table width='500' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#00d2c2' c-style='not2GreenBG' object='drag-module-small' style='border-top-right-radius: 6px; border-top-left-radius: 6px; background-color: #f4f4f4;'>
                        <tbody><tr>
                            <td width='100%' valign='middle' align='center'>
                                <!-- Header Text --> 
                                <table width='300' border='0' cellpadding='0' cellspacing='0' align='center' style='text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>
                                    <tbody><tr>
                                        <td width='100%' height='35'></td>
                                    </tr>
                                    <tr>
                                        <td valign='middle' width='100%' style='text-align: center;    font-size: 27px; color: #ffffff; line-height: 35px; font-weight: 400;' class='fullCenter' t-style='not3WhiteText' mc:edit='18' object='text-editable'>
                                            <a href='https://www.masterkorean.vn/' target='_blank'><img src='https://www.masterkorean.vn/theme/oklassedu/pix/images/logo_not_beta.png'></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100%' height='35' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table><div style='display: none' id='element_05156994036510831'><p></p></div>";

$data="<table width='400' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#ffffff' c-style='not3Body' object='drag-module-small' style='background-color: rgb(255, 255, 255);'>
                        <tbody><tr>
                            <td width='100%' valign='middle' align='center'>
                                <!-- Header Text --> 
                                <table width='350' border='0' cellpadding='0' cellspacing='0' align='center' style='text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>
                                    <tbody><tr>
                                        <td width='100%' height='40'></td>
                                    </tr>
                                    <tr>
                                        <td valign='middle' width='100%' style='text-align: center;    font-size: 40px; color: #3f4345; line-height: 42px; font-weight: 700;' class='fullCenter' t-style='not3Headlines'  object='text-editable'>
                                            <singleline>EXTRA ". $discount_rate ." % OFF</singleline>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100%' height='8' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign='middle' width='100%' style='text-align: center;    font-size: 14px; color: #8e9197; line-height: 24px; font-weight: 400;' class='fullCenter' t-style='not3Text' mc:edit='20' object='text-editable'>
                                            <singleline>Here’s your Couponcode. Hurry! ENDS ".$expire_date."</singleline>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100%' height='35' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table><!-- End Second -->
                    <table width='400' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#ffffff' c-style='not3Body' object='drag-module-small' style='background-color: rgb(255, 255, 255);'>
                        <tbody><tr>
                            <td width='352' valign='middle' align='center'>
                                <table width='265' border='0' cellpadding='0' cellspacing='0' align='center' style='text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>
                                    <tbody><tr>
                                        <td valign='middle' width='100%' class='icon18' align='center'>
                                            <!-- Input 2 -->
                                            <table width='220' border='0' cellpadding='0' cellspacing='0' align='center' style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 1px solid #ebeef5;' class='fullCenter'>
                                                <tbody><tr>
                                                    <td width='49' height='44' style='text-align: center;' object='image-editable'><img editable='true' src='http://rocketway.net/themebuilder/products/notifications/templates/notify2/images/image_49px_1.jpg' width='49' alt='' border='0' mc:edit='21' align='left' class='icon49'></td>
                                                    <td width='10' height='44' class='erase'></td>
                                                    <td width='151' height='44' style='text-align: center;    font-size: 15px; color: #485BC6; line-height: 22px; font-weight: 700;padding: 0 10px;' mc:edit='22' object='text-editable'><singleline>". $key_code ." </singleline></td>
                                                    <td width='10' height='44' class='erase'></td>
                                                </tr>
                                            </tbody></table><!-- End Space -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100%' height='15' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                    <table width='400' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#ffffff' c-style='not3Body' object='drag-module-small' style='background-color: rgb(255, 255, 255);'>
                        <tbody><tr>
                            <td width='100%' valign='middle' align='center'>
                                <!-- Text --> 
                                <table width='330' border='0' cellpadding='0' cellspacing='0' align='center' style='text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>
                                    <tbody><tr>
                                        <td width='100%' height='15' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign='middle' width='100%' style='text-align: justify;    font-size: 14px; color: #8e9197; line-height: 24px; font-weight: 400;' class='fullCenter' t-style='not3Text' mc:edit='23' object='text-editable'>
                                            <span>". $description ." </span>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                    <table width='400' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#ffffff' c-style='not3Body' object='drag-module-small' style='background-color: rgb(255, 255, 255);'>
                        <tbody><tr>
                            <td width='100%' valign='middle' align='center'>
                                <table width='330' border='0' cellpadding='0' cellspacing='0' align='center' style='text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>
                                    <tbody><tr>
                                        <td width='100%' height='30' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width='100%' align='center'>
                                            <!-- SORTABLE -->
                                            <div class='sortable_inner ui-sortable'>
                                            <table border='0' cellpadding='0' cellspacing='0' align='center' class='buttonScale'>
                                                <tbody><tr>
                                                    <td align='center' height='36' bgcolor='#3f4345' c-style='not3Button' style='border-radius: 5px; padding-left: 10px; padding-right: 10px; color: rgb(255, 255, 255); font-size: 13px; font-weight: 700; line-height: 1px; background-color: #485BC6;' t-style='not3WhiteText' mc:edit='24'>
                                                        <a href=". $courseurl ."  object='link-editable' style='color: #ffffff; text-decoration: none; width: 100%;' t-style='not3WhiteText'><multiline>". $coursetitle ." </multiline></a>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody></table> 
                            </td>
                        </tr>
                    </tbody></table>
                    ";
$datafooter="<table width='400' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#ffffff' c-style='not3Body' object='drag-module-small' style='background-color: rgb(255, 255, 255);'>
                        <tbody><tr>
                            <td width='100%' valign='middle' align='center'>
                            
                                <table width='330' border='0' cellpadding='0' cellspacing='0' align='center' style='text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>
                                    <tbody><tr>
                                        <td width='100%' height='25' style='font-size: 1px; line-height: 1px;'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign='middle' width='100%' style='text-align: center;    font-size: 13px; color: #8e9197; line-height: 24px; font-weight: 400;' class='fullCenter' t-style='not3Text' object='text-editable' mc:edit='25'>
                                            <!-- <multiline>or do something else <a href='#' style='color: #8e9197;' t-style='not3Text' object='link-editable'>here</a></multiline> -->
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    <table width='500' border='0' cellpadding='0' cellspacing='0' align='center' class='mobile' bgcolor='#ffffff' c-style='not3Body' object='drag-module-small' style='border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; background-color: #f4f4f4;'>
                        <tbody><tr>
                            <td width='50%' valign='middle' align='center' style='font-family: Noto Sans KR, Noto Sans SC,맑은고딕, sans-serif;width: auto; margin: 10px 0; padding: 20px 30px;box-sizing: border-box;border-top: 1px solid #ddd;margin: 30px -30px 0;text-align: center;'>
                                    <p style='font-family: inherit;font-size: 13px;margin: 0 0 3px;color: #777;margin-top: 7px;'>Copyright © VISANG Education Group Vietnam Company </p>                            
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
        </tbody></table>
</body>
</html>";

$messagehtml = $dataheader.$data.$datafooter;
$subject = $coupon->title;
$supportuser = \core_user::get_support_user();

// send coupon for email member
foreach ($memberEmails as $me) {
    $userid = $me['userId'];
    $email_user = $DB->get_record('user', array('id' => $userid));
        $checkresult=email_to_user($email_user, $supportuser, $subject, '', $messagehtml);
        if($checkresult){
            $emailreceivers = new stdClass();
            $emailreceivers->coupon_id = $couponid;
            $emailreceivers->user_id = $userid;
            $emailreceivers->email = $email_user->email;
            $emailreceivers->is_member = 1;
            $emailreceivers->coupon_key_code = $coupon->key_code;
            $emailreceivers->is_used = 0;
            $emailreceivers->received_time = time();
            $DB->insert_record('vi_coupons_receivers', $emailreceivers);
        }
        else{
            $error= new stdClass();
            $error ->coupon_id=$couponid;
            $error ->error_type='Email';
            $error ->error_message='Sending '.$email_user->email.' failed! Reason: failed';
            $error ->created_time=time();
            $DB->insert_record('vi_coupons_errors',$error);
        }

}
// send coupon for email not member
foreach ($notmemberEmails as $notme) {
        $checkresult = email_to_not_user($notme, $supportuser, $subject,'', $messagehtml);
        if($checkresult){
            $emailreceivers = new stdClass();
            $emailreceivers->coupon_id = $couponid;
            $emailreceivers->user_id = null;
            $emailreceivers->email = $notme;
            $emailreceivers->is_member = 0;
            $emailreceivers->coupon_key_code = $coupon->key_code;
            $emailreceivers->is_used = 0;
            $emailreceivers->received_time = time();
            $DB->insert_record('vi_coupons_receivers', $emailreceivers);
        }else{
            $error= new stdClass();
            $error ->coupon_id=$couponid;
            $error ->error_type='Email';
            $error ->error_message='Sending '.$notme.' failed! Reason: failed';
            $error ->created_time=time();
            $DB->insert_record('vi_coupons_errors',$error);
        }
}
echo $checkresult;