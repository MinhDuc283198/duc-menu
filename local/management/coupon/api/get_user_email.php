<?php

require_once(__DIR__ . '/../../../../config.php');

defined('MOODLE_INTERNAL') || die();
global $DB;

require_login();
$users = $DB->get_records_sql("SELECT u.id, u.email FROM {user} u WHERE u.confirmed = 1 AND  u.deleted = 0");

$responsedata = array();

foreach($users as $user) {
    array_push($responsedata, $user);
}

header('Content-type: application/json');
echo json_encode( $responsedata );