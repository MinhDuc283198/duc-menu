<?php


require_once(__DIR__ . '/../../../../config.php');
require_once($CFG->dirroot . '/local/management/coupon/couponlib.php');

defined('MOODLE_INTERNAL') || die();
global $DB;
require_login();
$courses = local_course_class_list();
$language = current_language();
$responsedata = array();

foreach ($courses as $course) {
    $courseitem = new stdClass();
    $courseitem->id = $course->id;
    if($language=='vi'){
        $courseitem->name = $course->vi_coursename;
    }
    if($language=='ko'){
        $courseitem->name = $course->coursename;
    }
    if($language=='en'){
        $courseitem->name = $course->en_coursename;
    }

    array_push($responsedata, $courseitem);
}

header('Content-type: application/json');
echo json_encode($responsedata);