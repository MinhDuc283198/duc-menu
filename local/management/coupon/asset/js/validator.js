// function validator(formSelector) {
let formRules = [];
let validatorRules = {
    required: function (value) {
        return value ? undefined : 'Trường này bắt là bắt buộc';
    },
    min: function (min) {
        return function (value) {
            return (value.length >= min) ? undefined : `Tối thiểu ${min} ký tự`;
        }
    },
    countmin: function (countmin) {
        return function (value) {
            return (value >= countmin) ? undefined : `Tối thiểu là ${countmin}`;
        }
    },
    countmax: function (countmax) {
        return function (value) {
            return (value - countmax < 0) ? undefined : `Tối đa là ${countmax}`;
        }
    }
};

const formElement = document.querySelector('#cg-form');

function handleValidate(e) {
    let rules = formRules[e.target.name]
    let errorMessage;

    rules.find(function (rule) {
        errorMessage = rule(e.target.value);
        return errorMessage;
    });

    if (errorMessage) {
        const formGroup = e.target.closest('.form-group');
        if (formGroup) {
            const formMessage = formGroup.querySelector('.form-mes');
            if (formMessage) {
                formMessage.innerText = errorMessage;
            }
        }
    }

    return !errorMessage;
}

function handleClearError(e) {
    const formGroup = e.target.closest('.form-group');
    if (formGroup) {
        const formMessage = formGroup.querySelector('.form-mes');
        if (formMessage) {
            formMessage.innerText = '';
        }
    }
}

if (formElement) {
    const inputs = formElement.querySelectorAll('[name][rules]');

    for (let input of inputs) {
        const rules = input.getAttribute('rules').split('|');
        for (let rule of rules) {
            const isRuleHasValue = rule.includes(':');
            let ruleInfo;
            let ruleVal;

            if (isRuleHasValue) {
                ruleInfo = rule.split(':');
                rule = ruleInfo[0]
                ruleVal = ruleInfo[1]
            }
            let ruleFunc = validatorRules[rule];

            if (isRuleHasValue) {
                ruleFunc = ruleFunc(ruleVal);
            }

            if (Array.isArray(formRules[input.name])) {
                formRules[input.name].push(ruleFunc);
            } else {
                formRules[input.name] = [ruleFunc];
            }
        }

        //add event
        input.onblur = handleValidate;
        input.oninput = handleClearError;
        if (input.name === 'course') {
            input.onchange = handleValidate;
        }
    }
}


// }
//
// validator('#cg-form');