<?php

require_once(__DIR__ . '/../../../config.php');
global $USER, $CFG,$DB;
require_once($CFG->dirroot . '/local/management/coupon/thirdparty/QrCode/src/QrCode.php');

require_once(__DIR__ . '/couponlib.php');

use local_management\output\generatoroptions;
use local_management\output\generator;

$context = context_system::instance();


$title = optional_param('title', '', PARAM_RAW);
$image = optional_param('image', '', PARAM_RAW);
$course = optional_param('course', '', PARAM_INT);
$discountrate = optional_param('discount-rate', '', PARAM_INT);
$description = optional_param('description', '', PARAM_RAW);
$qrcode = optional_param('qrcode', '', PARAM_INT);
$quantity = optional_param('quantity', '', PARAM_INT);
$couponcode = optional_param('couponcode', '', PARAM_RAW);
$startdate = optional_param('start-date', '', PARAM_RAW);
$expiredate = optional_param('expire-date', '', PARAM_RAW);
$expiredtime = optional_param('expired-time', '', PARAM_RAW);
$submittype = optional_param('submitType', '', PARAM_RAW);
$couponid = optional_param('couponid', '', PARAM_INT);

$couponimage = $_FILES['image'];

$courseurl = $CFG->wwwroot . '/local/course/detail.php?id=' . $course;

$renderqrcode = new \Endroid\QrCode\QrCode();
$renderqrcode->setText($courseurl);
$renderqrcode->setSize(300);

$qrcodeimg = $renderqrcode->get();

$qrfile = 'qrcode1.png';

file_put_contents($qrfile, $qrcodeimg);
if ($qrcode === 1) {
    $qrcodeimagepath = mkj_save_coupon_qrcode_image($context->id, $USER->id, $qrfile);
} else {
    $qrcodeimagepath = null;
}
$time = time();
$generateoptions = new generatoroptions();
$nameimg=$couponimage['name'];
if($nameimg!=''){
    $imagepath = mkj_save_coupon_image($context->id, $USER->id);
    $generateoptions->image = $imagepath;
}else{
    $oldcoupon=$DB->get_record('vi_coupons',array('id'=>$couponid));
    $generateoptions->image = $oldcoupon->filename;
}
$generateoptions->title = $title;
$generateoptions->courseid = $course;
$generateoptions->discountrate = $discountrate;
$generateoptions->description = $description;
$generateoptions->qrcode = $qrcode;
$generateoptions->quantity = $quantity;
$generateoptions->couponcode = $couponcode;
$generateoptions->startdate = strtotime($startdate);
$generateoptions->expiredate = strtotime($expiredate);
$generateoptions->expiredtime = strtotime($expiredate);
$generateoptions->modifiedtime = $time;
$generateoptions->qrcodeurl = $qrcodeimagepath;

$generateoptions->type = 5001;

$generator = new generator();

if($couponid ){
    $generateoptions->id=$couponid;
    $couponid = $generator->update_coupons($generateoptions);
}else{
    $couponid = $generator->generate_coupons($generateoptions);
}


if ($submittype === 'save') {
    $responsedata = (object)[
        'resType' => 'url',
        'url' => $CFG->wwwroot . '/local/management/coupon/coupongenerate.php?action=edit&couponid='.$couponid.'&saved=true',
    ];
    header('Content-type: application/json');
    echo json_encode($responsedata);
}
if ($submittype === 'next') {
    $responsedata = (object)[
        'resType' => 'couponId',
        'id' => $couponid,
        'title' => $title,
        'description' => $description,
        'cpImage' => $imagepath,
        'cpQrCode' => $qrcodeimagepath,
        'discount' => $discountrate,
        'saved'=>true,
    ];
    header('Content-type: application/json');
    echo json_encode($responsedata);
}





