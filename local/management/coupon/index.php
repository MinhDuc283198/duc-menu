<?php
global $DB, $CFG,$SESSION,$VISANG,$PAGE;
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/management/job/lib.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/coupon/couponlib.php');

//$pagesettings = array(
//    'title' => get_string('coupon_manage', 'local_management'),
//    'heading' => get_string('coupon_manage', 'local_management'),
//    'subheading' => '',
//    'menu' => 'menuM',
//    'js' => array(),
//    'css' => array('style'),
//    'nav_bar' => array()
//);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$page2 = optional_param('page2', 1, PARAM_INT);
$perpage2 = optional_param('perpage2', 10, PARAM_INT);

$page3 = optional_param('page3', 1, PARAM_INT);
$perpage3 = optional_param('perpage3', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'company_name', PARAM_RAW);
$couponid=optional_param('couponid', '', PARAM_INT);
$errorid=optional_param('errorid', '', PARAM_INT);
$action=optional_param('action', '', PARAM_RAW);

require_login(0, false);


$offset = ($page - 1) * $perpage;
$offset2 = ($page2 - 1) * $perpage2;
$offset3 = ($page3 - 1) * $perpage3;
if($couponid!=''&&$action=='delete'){
    $DB->delete_records("vi_coupons_courses", array("coupon_id" => $couponid));
    $DB->delete_records("vi_coupons_receivers", array("coupon_id"=>$couponid));
    $DB->delete_records("vi_coupons_errors", array("coupon_id"=>$couponid));
    $DB->delete_records("vi_coupons", array("id"=>$couponid));
}
if($errorid!=''&&$action=='delete'){
    $DB->delete_records("vi_coupons_errors", array("id"=>$errorid));
}
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}// Set up the page.
$title = get_string('coupon_manage', 'local_management');
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->subheading = '';
$VISANG->page->menu = 'couponmenu';
$VISANG->page->js = array();
$VISANG->page->css = array();
$VISANG->page->nav_bar = array();
//$VISANG->page->addCSS('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
$couponiddetail= optional_param('iddetail', '', PARAM_INT);
$searcherror=optional_param(searcherror,'',PARAM_RAW_TRIMMED);
$searchcoupon=optional_param(searchcoupon,'',PARAM_RAW_TRIMMED);
$searchrecei=optional_param(searchrecei,'',PARAM_RAW_TRIMMED);
$searchcoursetext=optional_param(searchcoursetext,'',PARAM_RAW_TRIMMED);
$couponid=optional_param(id,'',PARAM_INT);
if($searchcoupon!=''){
    $totalcount = count_search_coupon($searchcoupon);
}else{
    $sqlcount='SELECT COUNT(*) FROM {vi_coupons}';
    $totalcount = $DB->count_records_sql($sqlcount);
}
if($searcherror!=''){
    if($couponiddetail!=null) {
        $totalcount_error = count_error_list_search($searcherror,$couponiddetail);
    }
}else{
    if($couponiddetail!=null) {
        $sqlcouponseeor = 'SELECT COUNT(*) FROM {vi_coupons_errors}';
        $totalcount_error = $DB->count_records_sql($sqlcouponseeor,array('coupon_id'=>$couponiddetail));
    }
}
if($searchrecei!=''){
    if($couponiddetail!=null) {
        $totalrecei = count_coupons_receivers_search($searchrecei,$couponiddetail);
    }
}else{
    if($couponiddetail!=null){
        $sqlrecei="SELECT COUNT(*) FROM {vi_coupons_receivers}  WHERE  coupon_id=$couponiddetail";
        $totalrecei=$DB->count_records_sql($sqlrecei);
    }
}
$url = new moodle_url("/local/management/coupon/index.php");
$PAGE->set_url($url);
$PAGE->set_pagelayout('adminmanagement');
$filterurl = new moodle_url($url, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage,
));
$filterurl2 = new moodle_url($url, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page2' => $page2,
    'perpage2' => $perpage2,
));
$filterurl3 = new moodle_url($url, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page3' => $page3,
    'perpage3' => $perpage3
));
$VISANG->page->header();
$tablecoupon= new html_table();
$tablecoupon->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$tablecoupon->id = 'coupon_list';
$tablecoupon->attributes['class'] = 'table table-bordered dataTable';
$tablecoupon->head = array(
    get_string('cp_no','local_management'),
    get_string('cp_title','local_management'),
    get_string('cp_course','local_management'),
    get_string('cp_dr','local_management'),
    get_string('cp_qty','local_management'),
    get_string('cp_crdate','local_management'),
    get_string('cp_sent','local_management'),
    get_string('cp_sentdate','local_management'),
    get_string('cp_expried','local_management'),
    get_string('cp_exprieddate','local_management'),
    get_string('err_couponaction','local_management'),
);
$tablecoupon->data= array();
$couponslist= get_coupons($offset,$perpage,$searchcoupon);
$editlink=$CFG->wwwroot . '/local/management/coupon/coupongenerate.php';
foreach($couponslist as $item){
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($editlink, array('action' => 'edit', 'couponid' => $item->idcoupon)),
        'fa fa-pencil-square-o'

    );
    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'couponid' => $item->idcoupon))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';
    $row= array(
        $item->no,
        html_writer::link(
            new moodle_url($url, array( 'iddetail' => $item->idcoupon)),
            $item->title
        ),
        $item->course,
        $item->disrate,
        $item->quantity,
        $item->create_time,
        $item->is_send,
        $item->send_date,
        $item->is_expire,
        $item->expire_date,
        html_writer::div($actions, 'd-flex'),
    );
    $tablecoupon->data[] = $row;
}
if (count($tablecoupon->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($tablecoupon->head);
    $tablecoupon->data[] = $emptyrow;
}
//receiver table
if($couponiddetail!=null){
    $tablereceiver= new html_table();
    $tablereceiver->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
    $tablereceiver->id = 'coupon_list';
    $tablereceiver->attributes['class'] = 'table table-bordered dataTable';
    $tablereceiver->head = array(
        get_string('rc_email','local_management'),
        get_string('rc_rcdate','local_management'),
        get_string('rc_code','local_management'),
        get_string('rc_used','local_management'),
        get_string('rc_useddate','local_management'),
    );
    $tablereceiver->data= array();

    $receiverslist= get_detail_receivers($offset3,$perpage3,$couponiddetail,$searchrecei);
    foreach($receiverslist as $item){
        $row= array(
            $item->email,
            $item->received_time,
            $item->key_code,
            $item->is_used,
            $item->used_date,
        );
        $tablereceiver->data[] = $row;
    }
    if (count($tablereceiver->data) == 0) {
        $emptyrow = new html_table_row();
        $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
        $emptyrow->cells[0]->colspan = count($tablereceiver->head);
        $tablereceiver->data[] = $emptyrow;
    }
    //error table
    $tablerror= new html_table();
    $tablerror->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
    $tablerror->id = 'coupon_list';
    $tablerror->attributes['class'] = 'table table-bordered dataTable';
    $tablerror->head = array(
        get_string('err_coupon','local_management'),
        get_string('err_coupontype','local_management'),
        get_string('err_couponerror','local_management'),
        get_string('err_coupondate','local_management'),
        get_string('err_couponaction','local_management'),
    );
    $tablerror->data= array();
    $errorsslist= get_errors($offset2,$perpage2,$searcherror,$couponiddetail);
    foreach($errorsslist as $item){
        $actions = '';
        // delete
        $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'errorid' => $item->iderror))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';
        $row= array(
            $item->coupon,
            $item->error_type,
            $item->error_message,
            $item->created_time,
            html_writer::div($actions, 'd-flex'),
        );
        $tablerror->data[] = $row;
    }
    if (count($tablerror->data) == 0) {
        $emptyrow = new html_table_row();
        $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
        $emptyrow->cells[0]->colspan = count($tablerror->head);
        $tablerror->data[] = $emptyrow;
    }
}
?>
    <section class="content">
        <h2><?php echo get_string('coupon_list','local_management')?></h2>
        <div class="box table-management">
            <div class="box-body">
                <div class="form-group">
                    <div class="pull-right">
                        <form method="get" class="form-inline">
                            <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                            <div class="input-group">
                                <input class="form-control pull-right" type="search" name="searchcoupon" value="<?php echo $searchcoupon; ?>" placeholder="<?php echo get_string('search'); ?>">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <div class="pull-left">
                        <?php
                        echo html_writer::link(
                            new moodle_url($editlink),
                            "Add new",
                            array('class' => 'btn btn-primary')
                        );
                        ?>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="form-group " style="text-align: center">
                    <?php
                    echo html_writer::table($tablecoupon);
                    ?>
                </div>
                <div>
                    <div class="pull-left">
                        <?php
                        $_row = new stdClass();
                        $_row->from = $offset;
                        $_row->to = min($offset + $perpage, $totalcount);
                        $_row->total = $totalcount;
                        $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                        ?>
                    </div>
                    <div class="pull-right">
                        <?php
                        $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php if($couponiddetail!=null) : ?>
            <?php
            $title=get_coupon_title_by_id($couponiddetail);
            ?>
            <h2><?php echo get_string('rc_manage','local_management')?> <?php echo $title->title;?></h2>
            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <div class="pull-right">
                            <form method="get" class="form-inline">
                                <input type="hidden" name="perpage3" value="<?php echo $perpage3; ?>">
                                <input type="hidden" name="iddetail" value="<?php echo $couponiddetail; ?>">
                                <div class="input-group">
                                    <input class="form-control pull-right" type="search" name="searchrecei" value="<?php echo $searchrecei; ?>" placeholder="<?php echo get_string('search'); ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" >
                        <?php
                        echo html_writer::table($tablereceiver);
                        ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset3;
                            $_row->to = min($offset3 + $perpage3, $totalrecei);
                            $_row->total = $totalrecei;
                            $VISANG->page->table_perpage($perpage3, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            table_pagination3($filterurl3, null, ceil($totalrecei / $perpage3), $page3,$couponiddetail);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php if (count($errorsslist) != 0) : ?>
            <h2><?php echo get_string('error_list','local_management')?></h2>
            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <div class="pull-right">
                            <form method="get" class="form-inline">
                                <input type="hidden" name="perpage2" value="<?php echo $perpage2; ?>">
                                <div class="input-group">
                                    <input class="form-control pull-right" type="search" name="searcherror" value="<?php echo $searcherror; ?>" placeholder="<?php echo get_string('search'); ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" >
                        <?php
                        echo html_writer::table($tablerror);
                        ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset2;
                            $_row->to = min($offset2 + $perpage2, $totalcount_error);
                            $_row->total = $totalcount_error;
                            $VISANG->page->table_perpage($perpage2, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            table_pagination2($filterurl2, null, ceil($totalcount_error / $perpage2), $page2);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php endif;?>
        <?php endif; ?>
    </section>


    <style>
        .table-management .c3{
            text-align: right;
        }
        .table-management .c0,.c1,.c2,.c4,.c5{
            text-align: center;
        }
        .table-management .media-left {
            padding: 0 0 0 25px;
        }
    </style>
<?php
$VISANG->page->footer();

