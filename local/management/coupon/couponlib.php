<?php

function get_coupons_list($offset,$perpage)
{
    global $DB, $USER;
    $sql = "SELECT vc.* FROM {vi_coupons} vc ORDER BY vc.create_time DESC LIMIT $perpage OFFSET $offset";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_searching_coupons($offset,$perpage,$searchtext)
{
    global $DB, $USER;
    $params = array();
    $query=array();
    $query[]= $DB->sql_like('lc.vi_coursename', ':vi_coursename' , false);
    $params['vi_coursename'] = '%'.$searchtext.'%';
    $query[] = $DB->sql_like('vc.title', ':title' , false);
    $params['title'] = '%'.$searchtext.'%';
    $sql_str = implode(" OR ", $query);
    $sql = "SELECT vc.* FROM {vi_coupons} vc
            LEFT JOIN {vi_coupons_courses} cc ON cc.coupon_id = vc.id
            LEFT JOIN {lmsdata_course} lc ON lc.id = cc.course_id ";
    $result = $DB->get_records_sql("$sql WHERE $sql_str ORDER BY vc.create_time DESC LIMIT $perpage OFFSET $offset", $params);
    return $result;
}

function count_search_coupon($searchtext)
{
    global $DB, $USER;
    $params = array();
    $query=array();
    $query[]= $DB->sql_like('lc.vi_coursename', ':vi_coursename' , false);
    $params['vi_coursename'] = '%'.$searchtext.'%';
    $query[] = $DB->sql_like('vc.title', ':title' , false);
    $params['title'] = '%'.$searchtext.'%';
    $sql_str = implode(" OR ", $query);
    $sql = "SELECT COUNT(*) FROM {vi_coupons} vc
            LEFT JOIN {vi_coupons_courses} cc ON cc.coupon_id = vc.id
            LEFT JOIN {lmsdata_course} lc ON lc.id = cc.course_id ";
    $result = $DB->count_records_sql("$sql WHERE $sql_str", $params);
    return $result;
}

function get_error_list($offset,$perpage,$couponid)
{
    global $DB, $USER;
    $sql = "SELECT vce.* FROM {vi_coupons_errors} vce WHERE vce.coupon_id=$couponid ORDER BY vce.created_time DESC LIMIT $perpage OFFSET $offset";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_coupons_send()
{
    global $DB, $USER;
    $sql = "SELECT vc.* FROM {vi_coupons} vc WHERE vc.is_send=1";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_coupon_receivers($offset,$perpage,$couponid)
{
    global $DB, $USER;
    $sql = "SELECT vcr.* FROM {vi_coupons_receivers} vcr WHERE vcr.coupon_id=$couponid ORDER BY vcr.received_time DESC LIMIT $perpage OFFSET $offset";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_coupon_by_id($couponid)
{
    global $DB, $USER;
    $sql = "SELECT vc.* FROM {vi_coupons} vc  WHERE vc.id= $couponid";
    $result = $DB->get_record_sql($sql);
    return $result;
}

function get_coupon_title_by_id($couponid)
{
    global $DB, $USER;
    $sql = "SELECT vc.title FROM {vi_coupons} vc  WHERE vc.id= $couponid";
    $result = $DB->get_record_sql($sql);
    return $result;
}

function get_course_by_id($id)
{
    global $DB, $USER;
    $sql = "SELECT lc.en_coursename FROM {lmsdata_course} lc  WHERE lc.id= $id";
    $result = $DB->get_record_sql($sql);
    return $result;
}

function get_course_coupon_by_id($id)
{
    global $DB, $USER;
    $sql = "SELECT vcc.* FROM {vi_coupons_courses} vcc  WHERE vcc.coupon_id= $id";
    $result = $DB->get_record_sql($sql);
    return $result;
}

function get_error_list_search($offset,$perpage,$searchtext,$couponid)
{
    global $DB, $USER;
    $sql_select = "FROM {vi_coupons_errors} vce
                    LEFT JOIN {vi_coupons} vc ON vce.coupon_id = vc.id 
                    ";
    $sql = array();
    $params = array();
    $sql[] = $DB->sql_like('vc.title', ':title', false);
    $params['title'] = '%' . $searchtext . '%';
    $sql[] = $DB->sql_like('vce.error_type', ':error_type', false);
    $params['error_type'] = '%' . $searchtext . '%';
    $sql[] = $DB->sql_like('vce.error_message', ':error_message', false);
    $params['error_message'] = '%' . $searchtext . '%';
    $sql_str = implode(" OR ", $sql);
    $errors = $DB->get_records_sql("SELECT vce.* $sql_select WHERE vce.coupon_id=$couponid AND $sql_str ORDER BY vce.create_time DESC LIMIT $perpage OFFSET $offset", $params);

    return $errors;
}
function count_error_list_search($searchtext,$couponid)
{
    global $DB, $USER;
    $sql_select = "FROM {vi_coupons_errors} vce
                    LEFT JOIN {vi_coupons} vc ON vce.coupon_id = vc.id 
                    ";
    $sql = array();
    $params = array();
    $sql[] = $DB->sql_like('vc.title', ':title', false);
    $params['title'] = '%' . $searchtext . '%';
    $sql[] = $DB->sql_like('vce.error_type', ':error_type', false);
    $params['error_type'] = '%' . $searchtext . '%';
    $sql[] = $DB->sql_like('vce.error_message', ':error_message', false);
    $params['error_message'] = '%' . $searchtext . '%';
    $sql_str = implode(" OR ", $sql);
    $errors = $DB->count_records_sql("SELECT  COUNT(*) $sql_select WHERE vce.coupon_id=$couponid AND $sql_str", $params);

    return $errors;
}

function get_coupons_receivers_search($offset,$perpage,$searchtext, $couponid)
{
    global $DB, $USER;
    $sql_select = "FROM {vi_coupons_receivers} vcr
                    LEFT JOIN {vi_coupons} vc ON vcr.coupon_id = vc.id 
                    ";
    $sql = array();
    $params = array();
    $sql[] = $DB->sql_like('vcr.email', ':email', false);
    $params['email'] = '%' . $searchtext . '%';
    $sql[] = $DB->sql_like('vc.key_code', ':key_code', false);
    $params['key_code'] = '%' . $searchtext . '%';
    $sql_str = implode(" OR ", $sql);
    $receis = $DB->get_records_sql("SELECT vcr.* $sql_select WHERE vcr.coupon_id=$couponid AND $sql_str ORDER BY vcr.received_time DESC LIMIT $perpage OFFSET $offset", $params);

    return $receis;
}
function count_coupons_receivers_search($searchtext, $couponid)
{
    global $DB, $USER;
    $sql_select = "FROM {vi_coupons_receivers} vcr
                    LEFT JOIN {vi_coupons} vc ON vcr.coupon_id = vc.id 
                    ";
    $sql = array();
    $params = array();
    $sql[] = $DB->sql_like('vcr.email', ':email', false);
    $params['email'] = '%' . $searchtext . '%';
    $sql[] = $DB->sql_like('vc.key_code', ':key_code', false);
    $params['key_code'] = '%' . $searchtext . '%';
    $sql_str = implode(" OR ", $sql);
    $receis = $DB->count_records_sql("SELECT COUNT(*) $sql_select WHERE vcr.coupon_id=$couponid AND $sql_str", $params);

    return $receis;
}
function remove_mycoupon($id)
{
    global $DB, $USER;
    $DB->delete_records("vi_coupons_receivers", array("id" => $id));
}

function mkj_save_coupon_image($id, $userid)
{
    $cpimage = $_FILES['image'];
    $avatar_path = '';
    if ($cpimage['name'] != '') {
        $fs = get_file_storage();
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $cpimage['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $userid
        );
        $fs->create_file_from_pathname($file_record, $cpimage['tmp_name']);

        $avatar_path = implode('/', array($id, 'local_visang', 'images', $itemid, $cpimage['name']));
        return $avatar_path;
    }
}

function mkj_save_coupon_qrcode_image($id, $userid, $file)
{
//    $cpimage = $_FILES['image'];
    $avatar_path = '';
    if ($file != '') {
        $fs = get_file_storage();
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $file,
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $userid
        );
        $fs->create_file_from_pathname($file_record, $file);

        $avatar_path = implode('/', array($id, 'local_visang', 'images', $itemid, $file));
        return $avatar_path;
    }
}

function mkj_get_user_emails()
{
    global $DB;
//    $useremails = $DB->get_records_select('user', '', array('confirmed' => 1, 'deleted' => 0), '', 'id, email,confirmed,deleted' );
    $users = $DB->get_records_sql("SELECT u.id, u.email FROM {user} u WHERE u.confirmed = 1 AND  u.deleted = 0");
    $useremails = array();
    foreach ($users as $user) {
        $user = array('email' => $user->email, 'id' => $user->id);
        array_push($useremails, $user);
    }
    return $useremails;
}

function get_coupons($offset,$perpage,$searchtext = '')
{
    global $CFG;
    $coupons = null;
    if(empty($searchtext)) {
        $coupons = get_coupons_list($offset,$perpage);
    } else {
        $coupons = get_searching_coupons($offset,$perpage,$searchtext);
    }
    $coupon_infor = array();
    $nocoupon = $offset+1;
    foreach ($coupons as $coupon) {
        $iterm = new stdClass();
        $iterm->no = $nocoupon;
        $iterm->idcoupon = $coupon->id;
        $iterm->editlink = $CFG->wwwroot . '/local/management/coupon/coupongenerate.php?id=' . $coupon->id;
        $iterm->detaillink = $CFG->wwwroot . '/local/management/coupon/index.php?id=' . $coupon->id;
        $iterm->title = $coupon->title;
        $iterm->img = $coupon->filename;
        $coupon_course = get_course_coupon_by_id($coupon->id);
        $course_infor = get_course_by_id($coupon_course->course_id);
        $iterm->course = $course_infor->vi_coursename;
        $iterm->disrate = $coupon->discount_rate;
        $iterm->quantity = $coupon->count;
        $iterm->create_time = date_format_string($coupon->create_time, '%Y/%m/%d');
        if ($coupon->is_send == 1) {
            $iterm->is_send = 'YES';
            $iterm->send_date = date_format_string($coupon->send_date, '%Y/%m/%d');
        } else if ($coupon->is_send == 0) {
            $iterm->is_send = 'NO';
            $iterm->send_date = '-';
        }
        $today = time();
        if ($coupon->expire_date - $today <= 0) {
            $iterm->is_expire = "YES";
        } else {
            $iterm->is_expire = "NO";
        }
        $iterm->expire_date = date_format_string($coupon->expire_date, '%Y/%m/%d');
        array_push($coupon_infor, $iterm);
        $nocoupon++;
    }

    return $coupon_infor;
}
function get_errors($offset,$perpage,$searcherror='',$couponid)
{
    $errors_infor = array();
    if ($searcherror == "") {
        $errors = get_error_list($offset,$perpage,$couponid);
    } else {
        $errors = get_error_list_search($offset,$perpage,$searcherror,$couponid);
    }

    foreach ($errors as $error) {
        $iterm = new stdClass();
        $iterm->iderror = $error->id;
        $coupon = get_coupon_by_id($error->coupon_id);
        $iterm->coupon = $coupon->title;
        $iterm->error_type = $error->error_type;
        $iterm->error_message = $error->error_message;
        $iterm->created_time = date_format_string($error->created_time, '%Y/%m/%d');
        array_push($errors_infor, $iterm);
//                $noerror++;
    }
    return $errors_infor;
}
function get_detail_receivers($offset,$perpage,$couponid,$searchrecei='')
{

    $receivers = array();
    if ($searchrecei !== "" && $couponid !== null) {
        $receieds = get_coupons_receivers_search($offset,$perpage,$searchrecei, $couponid);
    } else {
        $receieds = get_coupon_receivers($offset,$perpage,$couponid);
    }
    foreach ($receieds as $receied) {
        $iterm = new stdClass();
//                $iterm->no = $noerror;
        $coupon = get_coupon_by_id($receied->coupon_id);
        $iterm->email = $receied->email;
        $iterm->received_time = date_format_string($receied->received_time, '%Y/%m/%d');
        $iterm->key_code = $coupon->key_code;
        if ($receied->is_used == 0) {
            $iterm->is_used = 'NO';
            $iterm->used_date = '-';
        } else if ($receied->is_used == 1) {
            $iterm->is_used = 'YES';
            $iterm->used_date = date_format_string($receied->used_time, '%Y/%m/%d');
        }
        array_push($receivers, $iterm);
    }
    return $receivers;
}
function table_pagination2($url, $params = array(), $total_pages = 1, $current_page = 1, $max_nav = 10)
{
    $padding = floor($max_nav / 2);
    $page_start = max(1, $current_page - $padding);
    $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

    echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
    if ($current_page > 1) {
        $p = $params;
        $p['page2'] = $current_page - 1;
        echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&laquo;</span></a></li>';
    } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="active"><span>' . $i . '</span></li>';
        } else {
            $p = $params;
            $p['page2'] = $i;
            echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '">' . $i . '</a></li>';
        }
    }
    if ($current_page < $total_pages) {
        $p = $params;
        $p['page2'] = $current_page + 1;
        echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&raquo;</span></a></li>';
    } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
    echo \html_writer::end_tag('ul');
}
function table_pagination3($url, $params = array(), $total_pages = 1, $current_page = 1,$couponiddetail, $max_nav = 10)
{
    $padding = floor($max_nav / 2);
    $page_start = max(1, $current_page - $padding);
    $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

    echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
    if ($current_page > 1) {
        $p = $params;
        $p['page3'] = $current_page - 1;
        $tempurl=(new \moodle_url($url, $p))->out().'&iddetail='.$couponiddetail;
        echo '<li><a href="' . $tempurl . '"><span aria-hidden="true">&laquo;</span></a></li>';
    } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="active"><span>' . $i . '</span></li>';
        } else {
            $p = $params;
            $p['page3'] = $i;
            $tempurl=(new \moodle_url($url, $p))->out().'&iddetail='.$couponiddetail;
            echo '<li><a href="'.$tempurl.'">' . $i . '</a></li>';
        }
    }
    if ($current_page < $total_pages) {
        $p = $params;
        $p['page3'] = $current_page + 1;
        $tempurl=(new \moodle_url($url, $p))->out().'&iddetail='.$couponiddetail;
        echo '<li><a href="' . $tempurl . '"><span aria-hidden="true">&raquo;</span></a></li>';
    } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
    echo \html_writer::end_tag('ul');
}
function get_image_coupon($couponid){
    global $DB;
    return $DB->get_record('vi_coupons',array('id'=>$couponid));
}
function local_course_class_list() {
    global $DB, $USER, $CFG;

    $now = time();
    $select_cnt = "select count(a.id) ";
    $lc_select = 'lc.id, lc.parentcourseid, lc.courseid, lc.learningstart, lc.learningend, lc.timecreated, lc.type, lc.code, lc.price, 
                  lc.courseperiod, lc.reviewperiod, lc.isused, lc.bookid, lc.discount,lco.coursecd as lcocode, lco.price as lcoprice, mc.id as mcid , mc.category,   
                  lco.courseid as lcocousreid, lco.lecturecnt, lco.samplecontent, lc.vi_title, lc.en_title, lc.title, lco.intro, lco.vi_intro, lco.en_intro, lco.coursename, lco.vi_coursename, lco.en_coursename,
                  lc.keywords, lc.keywords_en, lc.keywords_vi ';

    $t_select1 = " '' as ltid, '' as lttitle, '' as vi_lttitle, '' as en_lttitle, '' as ltprice, '' as text, '' as vi_text, '' as en_text,'' as author, '' as vi_author,  '' as en_author, '' as publisher, '' as vi_publisher, '' as en_publisher ";

    $t_select2 = " lt.id as ltid, lt.title as lttitle, lt.vi_title as vi_lttitle, lt.en_title as en_lttitle, lt.price as ltprice, lt.text, lt.vi_text, lt.en_text, lt.author, lt.vi_author, lt.en_author, 
                  (select title from {lmsdata_copyright} where id=lt.publisher)  as publisher, 
                  (select titlevn from {lmsdata_copyright} where id=lt.publisher)  as vi_publisher,
                  (select titleen from {lmsdata_copyright} where id=lt.publisher)  as en_publisher ";
    $select = "SELECT * ";
    $from = " FROM  
            (select $lc_select, $t_select1  
            from {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 1 AND lco.isused = 0 
            UNION
            select $lc_select, $t_select2 
            from {lmsdata_class} lc
            JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 3 AND lco.isused = 0 AND lt.isused = 0 ) a ";
    $where = " WHERE a.learningstart <= $now AND $now <= a.learningend AND a.isused = 0 ";
    $order = " ORDER BY a.id DESC, a.timecreated DESC";


    $return = $DB->get_records_sql($select . $from . $where . $order);

    if ($return) {
        //강사소개

        switch (current_language()) {
            case 'ko' :
                $return->lang_title = $return->title;
                $return->lang_coursename = $return->coursename;
                $return->lang_textbookname = $return->lttitle;
                $return->lang_onelineintro = $return->onelineintro;
                $return->lang_courseintro = $return->intro;
                $return->lang_booktext = $return->text;
                $return->lang_publisher = $return->publisher;
                $return->lang_author = $return->author;
                $return->lang_keywords = $return->keywords;
                break;
            case 'en' :
                $return->lang_title = $return->en_title;
                $return->lang_coursename = $return->en_coursename;
                $return->lang_textbookname = $return->en_lttitle;
                $return->lang_onelineintro = $return->en_onelineintro;
                $return->lang_intro = $return->en_lttitle;
                $return->lang_courseintro = $return->en_intro;
                $return->lang_booktext = $return->en_text;
                $return->lang_publisher = $return->en_publisher;
                $return->lang_author = $return->en_author;
                $return->lang_keywords = $return->keywords_en;
                break;
            case 'vi' :
                $return->lang_title = $return->vi_title;
                $return->lang_coursename = $return->vi_coursename;
                $return->lang_textbookname = $return->vi_lttitle;
                $return->lang_onelineintro = $return->vi_onelineintro;
                $return->lang_intro = $return->vi_lttitle;
                $return->lang_courseintro = $return->vi_intro;
                $return->lang_booktext = $return->vi_text;
                $return->lang_publisher = $return->vi_publisher;
                $return->lang_author = $return->vi_author;
                $return->lang_keywords = $return->keywords_vi;
                break;
        }
    }

    return $return;
}
function local_course_class_id($id) {
    global $DB, $USER, $CFG;

    $now = time();
    $select_cnt = "select count(a.id) ";
    $lc_select = 'lc.id, lc.parentcourseid, lc.courseid, lc.learningstart, lc.learningend, lc.timecreated, lc.type, lc.code, lc.price, 
                  lc.courseperiod, lc.reviewperiod, lc.isused, lc.bookid, lc.discount,lco.coursecd as lcocode, lco.price as lcoprice, mc.id as mcid , mc.category,   
                  lco.courseid as lcocousreid, lco.lecturecnt, lco.samplecontent, lc.vi_title, lc.en_title, lc.title, lco.intro, lco.vi_intro, lco.en_intro, lco.coursename, lco.vi_coursename, lco.en_coursename,
                  lc.keywords, lc.keywords_en, lc.keywords_vi ';

    $t_select1 = " '' as ltid, '' as lttitle, '' as vi_lttitle, '' as en_lttitle, '' as ltprice, '' as text, '' as vi_text, '' as en_text,'' as author, '' as vi_author,  '' as en_author, '' as publisher, '' as vi_publisher, '' as en_publisher ";

    $t_select2 = " lt.id as ltid, lt.title as lttitle, lt.vi_title as vi_lttitle, lt.en_title as en_lttitle, lt.price as ltprice, lt.text, lt.vi_text, lt.en_text, lt.author, lt.vi_author, lt.en_author, 
                  (select title from {lmsdata_copyright} where id=lt.publisher)  as publisher, 
                  (select titlevn from {lmsdata_copyright} where id=lt.publisher)  as vi_publisher,
                  (select titleen from {lmsdata_copyright} where id=lt.publisher)  as en_publisher ";
    $select = "SELECT * ";
    $from = " FROM  
            (select $lc_select, $t_select1  
            from {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 1 AND lco.isused = 0 
            UNION
            select $lc_select, $t_select2 
            from {lmsdata_class} lc
            JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            WHERE lc.type = 3 AND lco.isused = 0 AND lt.isused = 0 ) a ";
    $where = " WHERE a.id=:id AND a.learningstart <= $now AND $now <= a.learningend AND a.isused = 0 ";
    $order = " ORDER BY a.id DESC, a.timecreated DESC";
    $params = array('id' => $id);

    $return = $DB->get_record_sql($select . $from . $where . $order,$params);

    if ($return) {
        //강사소개

        switch (current_language()) {
            case 'ko' :
                $return->lang_title = $return->title;
                $return->lang_coursename = $return->coursename;
                $return->lang_textbookname = $return->lttitle;
                $return->lang_onelineintro = $return->onelineintro;
                $return->lang_courseintro = $return->intro;
                $return->lang_booktext = $return->text;
                $return->lang_publisher = $return->publisher;
                $return->lang_author = $return->author;
                $return->lang_keywords = $return->keywords;
                break;
            case 'en' :
                $return->lang_title = $return->en_title;
                $return->lang_coursename = $return->en_coursename;
                $return->lang_textbookname = $return->en_lttitle;
                $return->lang_onelineintro = $return->en_onelineintro;
                $return->lang_intro = $return->en_lttitle;
                $return->lang_courseintro = $return->en_intro;
                $return->lang_booktext = $return->en_text;
                $return->lang_publisher = $return->en_publisher;
                $return->lang_author = $return->en_author;
                $return->lang_keywords = $return->keywords_en;
                break;
            case 'vi' :
                $return->lang_title = $return->vi_title;
                $return->lang_coursename = $return->vi_coursename;
                $return->lang_textbookname = $return->vi_lttitle;
                $return->lang_onelineintro = $return->vi_onelineintro;
                $return->lang_intro = $return->vi_lttitle;
                $return->lang_courseintro = $return->vi_intro;
                $return->lang_booktext = $return->vi_text;
                $return->lang_publisher = $return->vi_publisher;
                $return->lang_author = $return->vi_author;
                $return->lang_keywords = $return->keywords_vi;
                break;
        }
    }

    return $return;
}
