
<?php
require_once(__DIR__ . '/../../../config.php');
require_once(dirname(__FILE__) . '/couponlib.php');


global $VISANG, $CFG, $PAGE,$DB;

if(isset($_POST['id'])){
    $id=  $_POST['id'];
    try {
        $DB->delete_records("vi_coupons_courses", array("coupon_id" => $id));
        $DB->delete_records("vi_coupons_receivers", array("coupon_id"=>$id));
        $DB->delete_records("vi_coupons_errors", array("coupon_id"=>$id));
        $DB->delete_records("vi_coupons", array("id"=>$id));
    } catch (dml_exception $e) {
    }

}

