<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('coupon_manage', 'local_management'),
    'heading' => get_string('coupon_manage', 'local_management'),
    'subheading' => '',
    'menu' => 'menuM',
    'js' => array(),
    'css' => array('style'),
    'nav_bar' => array()
);
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('coupon-add.php');
    redirect(get_login_url());
}
?>

<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
      integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
<?php

include_once($CFG->dirroot . '/local/management/header.php');
?>

<link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/coupon/asset/css/style.css"/>

<div class="container-fluid">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <input type="radio" name="switch-table" id="addcoupon" class="rbswitch" checked/>
                <input type="radio" name="switch-table" id="sendemail" class="rbswitch" />
                <header>
                    <div class="switch-table">
                        <label for="addcoupon" class="addcoupon1" id="">
                            <p><?php echo get_string('create_coupon', 'local_management') ?></p>
                        </label>
                        <label for="sendemail" class="sendemail1" id="">
                            <p><?php echo get_string('send_coupon', 'local_management') ?></p>
                        </label>
                    </div>
                </header>
            </div>
        </div>
        <div class="cards-area">
            <div class="cards">
                <div class="row add-step" id="add-coupon" style="">
                    <div class="col-md-6 add-coupon-left">
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_title','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="title" name="title" class="input-width"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_img','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <input type="file" id="filename" name="filename" class="input-width choise-file"
                                       hidden/>
                                <label for="filename" class="input-width choise-file"><i class="fas fa-upload"></i><?php echo get_string('choose-file','local_management')?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_course','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <select name="course" id="course" class="select-course input-width" width="80%">
                                    <option value="1">course 1</option>
                                    <option value="2">course 2</option>
                                    <option value="3">course 3</option>
                                    <option value="4">course 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_dr','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <input type="number" id="discount-rate" name="discount-rate"
                                       class="discount-rate input-width" min="0" max="100" step="1" value="0"/>%
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_dis','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <textarea id="description" name="description" class="area-des" rows="4" cols="33"
                                          class="input-width">
                                </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_renderqr','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <input type="radio" name="qrcode" id="yesqrcode" class="rbqrcode" checked/>
                                <label for="yesqrcode"><?php echo get_string('qryes','local_management')?></label>
                                <input type="radio" name="qrcode" id="noqrcode" class="rbqrcode" />
                                <label for="noqrcode"><?php echo get_string('qrno','local_management')?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('cp_qty','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <input type="number" id="quantity" name="quantity" min="0" step="1" value="0"
                                       class="input-width"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <span><?php echo get_string('rc_code','local_management')?></span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="couponcode" name="couponcode" class="input-width"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-8">
                                <a class="btn input-width btn-dowload"><?php echo get_string('rc_dowloadpdf','local_management')?> <i
                                            class="fas fa-arrow-alt-to-bottom"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 add-coupon-right">
                        <div class="row">
                            <div class="col-md 12">
                                <div class="preview-coupon">

                                </div>
                            </div>
                        </div>
                        <div class="row row-date">
                            <div class="col-md-2"></div>
                            <div class="col-md-3">
                                <span>
                                    <?php echo get_string('startdate','local_management')?>
                                </span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="input-width input-date" name="startdate" id="startdate"
                                       autocomplete="off" placeholder="<?php echo get_string('select-date','local_management')?>"/>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-3">
                                <span>
                                    <?php echo get_string('expiredate','local_management')?>
                                </span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="input-width input-date" name="expiredate" id="expiredate"
                                       autocomplete="off" placeholder="<?php echo get_string('select-date','local_management')?>"/>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <a class="btn btn-cancel"><?php echo get_string('cancel','local_management') ?></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-save"><?php echo get_string('save','local_management') ?></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-next"><?php echo get_string('next','local_management') ?></a>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
                <div class="row add-step" id="send-email" style="display: none">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 send-mail-tit">
                                <span><?php echo get_string('receivers','local_management')?></span>
                            </div>
                        </div>
                        <div class="row add-email">
                            <div class="col-md-8 ">
                                <div class="email-member">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <span class="member-tit">
                                            <?php echo get_string('member','local_management') ?>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="searchbox">
                                                <input type="text" class="search-input1"/>
                                                <button class="btn-search1"><i class="far fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="list-emailmem">

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="list-emailsend">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 ">
                                <div class="email-notmember">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <span class="notmember-tit">
                                            <?php echo get_string('notmember','local_management') ?>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="searchbox">
                                                <input type="text" class="search-input1"/>
                                                <button class="btn-search1"><i class="far fa-search"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="list-emailmem">
                                                a
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row emailbody">
                            <div class="col-md-12">
                                <div class="row">
                                    <span class="email-body-tit">
                                        <?php echo get_string('emailbody','local_management') ?>
                                    </span>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="ck-emailbody">
                                            <textarea name="emailbody">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="col-md-2">
                                        <div class="email-btn">
                                            <a class="btn btn-send"> <?php echo get_string('send','local_management') ?></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="email-btn">
                                            <a class="btn btn-cancel"> <?php echo get_string('cancel','local_management') ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace( 'emailbody' );
</script>
<script>
    $(document).ready(function () {
        $('input[name="switch-table"]').change(function () {
            if ($('#addcoupon').prop('checked')) {
                document.getElementById("add-coupon").style.display = "";
                document.getElementById("send-email").style.display = "none";
            }
            if ($('#sendemail').prop('checked')) {
                document.getElementById("add-coupon").style.display = "none";
                document.getElementById("send-email").style.display = "";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#startdate").datepicker({
            showOn: this,
            buttonText: 'Show Date',
            buttonImageOnly: true,
            buttonImage: '',
            dateFormat: 'yy-mm-dd'
        });

        $("#startdate").click(function () {
            $("#startdate").datepicker("show");
        });

        $("#expiredate").datepicker({
            showOn: this,
            buttonText: 'Show Date',
            buttonImageOnly: true,
            buttonImage: '',
            dateFormat: 'yy-mm-dd'
        });

        $("#expiredate").click(function () {
            $("#expiredate").datepicker("show");
        });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
