<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/local/management/users/lib.php');

$type = optional_param('type', 1, PARAM_INT);
$classid = optional_param('classid', 0, PARAM_INT);

$sql_where = array();
$where = '';


if($type == 1){
    $isused = optional_param_array('isused', array(), PARAM_RAW);
    $learningstart_str = optional_param('learningstart', '', PARAM_RAW);
    $learningend_str = optional_param('learningend', '', PARAM_RAW);
    $searchtext = optional_param('searchtext', '', PARAM_RAW);
    $searchtext = trim($searchtext);

    if ($learningstart_str) {
        $learningstart = str_replace(".", "-", $learningstart_str);
        $learningstart = strtotime($learningstart);
    }
    if ($learningend_str) {
        $learningend = str_replace(".", "-", $learningend_str);
        $learningend = strtotime($learningend);
    }
    if ($learningstart) {
        $sql_where[] = " lc.learningstart <= :learningstart ";
        $params['learningstart'] = $learningstart;
    }
    if ($learningend) {
        $sql_where[] = " lc.learningend >= :learningend ";
        $params['learningend'] = $learningend;
    }


    if ($isused) {
        foreach ($isused as $iskey => $isval) {
            $sql_where_isused[] = " lc.isused = :isused" . $iskey;
            $params['isused' . $iskey] = $isval;
        }
        if (!empty($sql_where_isused)) {
            $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
        } else {
            $sql_where[] = '';
        }
    }

    if (!empty($searchtext)) {
        $sql_where_search[] = $DB->sql_like('lc.title', ':title');
        $params['title'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('lc.vi_title', ':vi_title');
        $params['vi_title'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('lc.en_title', ':en_title');
        $params['en_title'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('lc.code', ':code');
        $params['code'] = '%' . $searchtext . '%';
        $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
    }

    if (!empty($sql_where)) {
        $sql_where_list = ' WHERE ' . implode(' and ', $sql_where) . $where;
    } else {
        $sql_where_list = '';
    }

    $select_cnt = " select count(lc.id) ";
    $select = " select lc.*, lo.id loid, mc.id mcid, mc2.id mc2id, ca.id caid, mc2.category, ca2.id ca2id , (select count(*) from m_lmsdata_course_like where moodlecourseid = mc.id) as reviewcnt, (select AVG(score) avg from {lmsdata_course_like} where moodlecourseid = mc.id) as reviewavg";
    $query = "
    from {lmsdata_class} lc
    JOIN {lmsdata_course} lo ON lc.parentcourseid = lo.id 
    JOIN {course} mc ON mc.id =  lc.courseid
    JOIN {course} mc2 ON lo.courseid = mc2.id
    JOIN {course_categories} ca ON ca.id = mc2.category 
    left JOIN {course_categories} ca2 ON ca.parent = ca2.id
    $sql_where_list
    ";
    $orderby = " ORDER BY lc.timecreated DESC, lc.id DESC";
    $contents = $DB->get_records_sql($select . $query . $orderby,$params ,($page - 1) * $perpage, $perpage);
    $total = $DB->count_records_sql($select_cnt . $query,$params);
    
    $isused_array = array(get_string('used', 'local_management'),get_string('notused', 'local_management'));
}else{
    if($classid){
        $sql_where[] = " mc.id = :mcid ";
        $params['mcid'] = $classid;
    }

    $isopend = optional_param_array('isopend', array(), PARAM_RAW);
    $timecreatedstart_str = optional_param('timecreatedstart', '', PARAM_RAW);
    $timecreatedend_str = optional_param('timecreatedend', '', PARAM_RAW);
    $searchtext = optional_param('searchtext2', '', PARAM_RAW);
    $searchtext = trim($searchtext);

    if ($isopend) {
        foreach ($isopend as $iskey => $isval) {
            $sql_where_isused[] = " lcl.isopend = :isused" . $iskey;
            $params['isused' . $iskey] = $isval;
        }
        if (!empty($sql_where_isused)) {
            $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
        } else {
            $sql_where[] = '';
        }
    }

    if (!empty($searchtext)) {
        $sql_where_search[] = $DB->sql_like('lcl.contents', ':contents');
        $params['contents'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('u.firstname', ':firstname');
        $params['firstname'] = '%' . $searchtext . '%';
        $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
    }
    
    
    if ($timecreatedstart_str) {
        $learningstart = str_replace(".", "-", $timecreatedstart_str);
        $learningstart = strtotime($learningstart);
    }
    if ($timecreatedend_str) {
        $learningend = str_replace(".", "-", $timecreatedend_str);
        $learningend = strtotime($learningend);
        $learningend = strtotime("+1 days",$learningend) - 1;
    }
    if ($learningstart) {
        $sql_where[] = " lcl.timecreated >= :learningstart ";
        $params['learningstart'] = $learningstart;
    }
    if ($learningend) {
        $sql_where[] = " lcl.timecreated <= :learningend ";
        $params['learningend'] = $learningend;
    }
    

    if (!empty($sql_where)) {
        $sql_where_list = ' WHERE ' . implode(' and ', $sql_where) . $where;
    } else {
        $sql_where_list = '';
    }


    $select_cnt = " select count(lcl.id) ";
    $select = "SELECT lcl.* , lc.id lcid, lc.vi_title, lc.en_title, lc.title, lc.code, u.firstname ";
    $query = "
            from {lmsdata_course_like} lcl
            JOIN {course} mc ON mc.id =  lcl.moodlecourseid 
            JOIN {lmsdata_class} lc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lo ON lc.parentcourseid = lo.id 
            JOIN {course} mc2 ON lo.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category 
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            JOIN {user} u ON u.id = lcl.userid 
            JOIN {lmsdata_user} lu ON lu.userid = u.id
            $sql_where_list
            ";
    $orderby = " ORDER BY lcl.timecreated DESC, lcl.id DESC";
    $contents = $DB->get_records_sql($select . $query . $orderby,$params ,($page - 1) * $perpage, $perpage);
    $total = $DB->count_records_sql($select_cnt . $query,$params);
    
    $isopened_array = array(get_string('opened','local_management'),get_string('notopened','local_management'));
    
    $class_query = "SELECT mc.id, lc.title, lc.vi_title, lc.en_title, lc.timecreated lctimecreated 
                    from {lmsdata_class} lc
                    JOIN {course} mc ON lc.courseid = mc.id
                    JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                    JOIN {course} mc2 ON lco.courseid = mc2.id
                    JOIN {course_categories} ca ON ca.id = mc2.category
                    left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                    ORDER BY lc.timecreated ASC, mc.id ASC ";
    $lmsdata_class = $DB->get_records_sql($class_query);

    $class_array = array();
    foreach($lmsdata_class as  $class ){
        switch (current_language()) {
            case 'ko' :
                $class_title = $class->title;
                break;
            case 'en' :
                $class_title = $class->vi_title;
                break;
            case 'vi' :
                $class_title = $class->en_title;
                break;
        }
        $class_array[$class->id] = $class_title;
    }
}
    if($type == 1){
        $fields = array(
            get_string('number', 'local_management'),
            get_string('review:course', 'local_management'),
            get_string('goodscode', 'local_management'),
            get_string('applicationperiod', 'local_management'),
            get_string('isused', 'local_management'),
            get_string('review:title2', 'local_management'),
            get_string('review:grade', 'local_management')
        );
    }else{                   
        $fields = array(
            get_string('number', 'local_management'),
            get_string('review:course', 'local_management'),
            get_string('goodscode', 'local_management'),
            get_string('review:reviewcontents', 'local_management'),
            get_string('review:scope', 'local_management'),
            get_string('review:writer', 'local_management'),
            get_string('review:timecreated', 'local_management'),
            get_string('review:timemodified', 'local_management'),
            get_string('isopened', 'local_management')
        );
    }

    $date = date('Y-m-d', time());
    $filename = '수강후기_' . $date. '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    $num = 1;
    foreach ($contents as $content) {
        $col = 0;
        $worksheet[0]->write($row, $col++, $num++);
        if($type == 1){
            $worksheet[0]->write($row, $col++, $content->vi_title."<br>".$content->title);
            $worksheet[0]->write($row, $col++, $content->code);
            $worksheet[0]->write($row, $col++, date("Y.m.d",$content->learningstart) .' ~ '.date("Y.m.d",$content->learningend) );
            $isused = ($content->isused == 0) ? get_string('used', 'local_management') : get_string('notused', 'local_management');
            $worksheet[0]->write($row, $col++, $isused);
            $worksheet[0]->write($row, $col++, number_format($content->reviewcnt).get_string('count','local_course'));
            $worksheet[0]->write($row, $col++, round($content->reviewavg) .get_string('score','local_course'));
        }else{
            $worksheet[0]->write($row, $col++, $content->vi_title."<br>".$content->title);
            $worksheet[0]->write($row, $col++, $content->code);
            $worksheet[0]->write($row, $col++, $content->contents);
            $worksheet[0]->write($row, $col++, $content->score);
            $worksheet[0]->write($row, $col++, $content->firstname);
            $worksheet[0]->write($row, $col++, date("Y.m.d", $content->timecreated));
            $timemodified =  ($content->timemodified != 0) ? date("Y.m.d", $content->timemodified) : '-';
            $worksheet[0]->write($row, $col++, $timemodified);
            $isopened = ($content->isopend == 0) ? get_string('opened','local_management'): get_string('notopened','local_management');
            $worksheet[0]->write($row, $col++, $isopened);
        }
                                
        $row++;
    }

    $workbook->close();
    die;

    