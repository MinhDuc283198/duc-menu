<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/goods/lib.php');
require_once($CFG->dirroot . '/local/management/lib/paging.php');

$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'isopend':
        $data = required_param_array('data', PARAM_RAW);
        
        if($data['opend'] == 0){
            $data['opend'] = 1;
        }else{
            $data['opend'] = 0;
        }

        $change = new stdClass();
        $change->id = $data['id'];
        $change->isopend = $data['opend'];
        $aa = $DB->update_record('lmsdata_course_like', $change);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();