<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' =>  get_string('review:title','local_management'),
    'heading' => get_string('review:title','local_management'),
    'subheading' => '',
    'menu' => 'review',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
//$orderitem = optional_param('orderitem', '', PARAM_RAW);
//$ordertype = optional_param('ordertype', '', PARAM_RAW);
$type = optional_param('type', 1, PARAM_INT);
$classid = optional_param('classid', 0, PARAM_INT);

$sql_where = array();
$where = '';


if($type == 1){
    $isused = optional_param_array('isused', array(), PARAM_RAW);
    $learningstart_str = optional_param('learningstart', '', PARAM_RAW);
    $learningend_str = optional_param('learningend', '', PARAM_RAW);
    $searchtext = optional_param('searchtext', '', PARAM_RAW);
    $searchtext = trim($searchtext);

    if ($learningstart_str) {
        $learningstart = str_replace(".", "-", $learningstart_str);
        $learningstart = strtotime($learningstart);
    }
    if ($learningend_str) {
        $learningend = str_replace(".", "-", $learningend_str);
        $learningend = strtotime($learningend);
    }
    if ($learningstart) {
        $sql_where[] = " lc.learningstart <= :learningstart ";
        $params['learningstart'] = $learningstart;
    }
    if ($learningend) {
        $sql_where[] = " lc.learningend >= :learningend ";
        $params['learningend'] = $learningend;
    }


    if ($isused) {
        foreach ($isused as $iskey => $isval) {
            $sql_where_isused[] = " lc.isused = :isused" . $iskey;
            $params['isused' . $iskey] = $isval;
        }
        if (!empty($sql_where_isused)) {
            $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
        } else {
            $sql_where[] = '';
        }
    }

    if (!empty($searchtext)) {
        $sql_where_search[] = $DB->sql_like('lc.title', ':title');
        $params['title'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('lc.vi_title', ':vi_title');
        $params['vi_title'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('lc.en_title', ':en_title');
        $params['en_title'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('lc.code', ':code');
        $params['code'] = '%' . $searchtext . '%';
        $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
    }

    if (!empty($sql_where)) {
        $sql_where_list = ' WHERE ' . implode(' and ', $sql_where) . $where;
    } else {
        $sql_where_list = '';
    }

    $select_cnt = " select count(lc.id) ";
    $select = " select lc.*, lo.id loid, mc.id mcid, mc2.id mc2id, ca.id caid, mc2.category, ca2.id ca2id , (select count(*) from m_lmsdata_course_like where moodlecourseid = mc.id) as reviewcnt, (select AVG(score) avg from {lmsdata_course_like} where moodlecourseid = mc.id) as reviewavg";
    $query = "
    from {lmsdata_class} lc
    JOIN {lmsdata_course} lo ON lc.parentcourseid = lo.id 
    JOIN {course} mc ON mc.id =  lc.courseid
    JOIN {course} mc2 ON lo.courseid = mc2.id
    JOIN {course_categories} ca ON ca.id = mc2.category 
    left JOIN {course_categories} ca2 ON ca.parent = ca2.id
    $sql_where_list
    ";
    $orderby = " ORDER BY lc.timecreated DESC, lc.id DESC";
    $contents = $DB->get_records_sql($select . $query . $orderby,$params ,($page - 1) * $perpage, $perpage);
    $total = $DB->count_records_sql($select_cnt . $query,$params);
    
    $isused_array = array(get_string('used', 'local_management'),get_string('notused', 'local_management'));
}else{
    if($classid){
        $sql_where[] = " mc.id = :mcid ";
        $params['mcid'] = $classid;
    }

    $isopend = optional_param_array('isopend', array(), PARAM_RAW);
    $timecreatedstart_str = optional_param('timecreatedstart', '', PARAM_RAW);
    $timecreatedend_str = optional_param('timecreatedend', '', PARAM_RAW);
    $searchtext = optional_param('searchtext2', '', PARAM_RAW);
    $searchtext = trim($searchtext);

    if ($isopend) {
        foreach ($isopend as $iskey => $isval) {
            $sql_where_isused[] = " lcl.isopend = :isused" . $iskey;
            $params['isused' . $iskey] = $isval;
        }
        if (!empty($sql_where_isused)) {
            $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
        } else {
            $sql_where[] = '';
        }
    }

    if (!empty($searchtext)) {
        $sql_where_search[] = $DB->sql_like('lcl.contents', ':contents');
        $params['contents'] = '%' . $searchtext . '%';
        $sql_where_search[] = $DB->sql_like('u.firstname', ':firstname');
        $params['firstname'] = '%' . $searchtext . '%';
        $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
    }
    
    
    if ($timecreatedstart_str) {
        $learningstart = str_replace(".", "-", $timecreatedstart_str);
        $learningstart = strtotime($learningstart);
    }
    if ($timecreatedend_str) {
        $learningend = str_replace(".", "-", $timecreatedend_str);
        $learningend = strtotime($learningend);
        $learningend = strtotime("+1 days",$learningend) - 1;
    }
    if ($learningstart) {
        $sql_where[] = " lcl.timecreated >= :learningstart ";
        $params['learningstart'] = $learningstart;
    }
    if ($learningend) {
        $sql_where[] = " lcl.timecreated <= :learningend ";
        $params['learningend'] = $learningend;
    }
    

    if (!empty($sql_where)) {
        $sql_where_list = ' WHERE ' . implode(' and ', $sql_where) . $where;
    } else {
        $sql_where_list = '';
    }


    $select_cnt = " select count(lcl.id) ";
    $select = "SELECT lcl.* , lc.id lcid, lc.vi_title, lc.en_title, lc.title, lc.code, u.firstname ";
    $query = "
            from {lmsdata_course_like} lcl
            JOIN {course} mc ON mc.id =  lcl.moodlecourseid 
            JOIN {lmsdata_class} lc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lo ON lc.parentcourseid = lo.id 
            JOIN {course} mc2 ON lo.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category 
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            JOIN {user} u ON u.id = lcl.userid 
            JOIN {lmsdata_user} lu ON lu.userid = u.id
            $sql_where_list
            ";
    $orderby = " ORDER BY lcl.timecreated DESC, lcl.id DESC";
    $contents = $DB->get_records_sql($select . $query . $orderby,$params ,($page - 1) * $perpage, $perpage);
    $total = $DB->count_records_sql($select_cnt . $query,$params);
    
    $isopened_array = array(get_string('opened','local_management'),get_string('notopened','local_management'));
    
    $class_query = "SELECT mc.id, lc.title, lc.vi_title, lc.en_title, lc.timecreated lctimecreated 
                    from {lmsdata_class} lc
                    JOIN {course} mc ON lc.courseid = mc.id
                    JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                    JOIN {course} mc2 ON lco.courseid = mc2.id
                    JOIN {course_categories} ca ON ca.id = mc2.category
                    left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                    ORDER BY lc.timecreated ASC, mc.id ASC ";
    $lmsdata_class = $DB->get_records_sql($class_query);

    $class_array = array();
    foreach($lmsdata_class as  $class ){
        switch (current_language()) {
            case 'ko' :
                $class_title = $class->title;
                break;
            case 'en' :
                $class_title = $class->vi_title;
                break;
            case 'vi' :
                $class_title = $class->en_title;
                break;
        }
        $class_array[$class->id] = $class_title;
    }
}

$perpages = [10, 20, 30, 50];

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <div>
                    <ul class="mk-c-tab tab-event" id="delivery_status">
                        <li style="width:20%" <?php if ($type == 1) { ?>class="on"<?php } ?>><a href="#" data-type="1"><?php echo get_string('courseproductview','local_management')?></a></li>
                        <li style="width:20%" <?php if ($type == 2) { ?>class="on"<?php } ?>><a href="#" data-type='2'><?php echo get_string('allview','local_management')?></a></li>
                    </ul>
                    
                    <form class="search_area" id="search_form" action="./index.php">
                        <input type="hidden" name="excel" value ="0">
                        <input type="hidden" name="page" value="1" />
                        <input type="hidden" name="perpage" value="<?php echo $perpage?>" />
                        <input type="hidden" name="type" value="<?php echo $type?>" />
                        <input type="hidden" name="classid" value="<?php echo $classid?>" />
                        <div>
                            <?php if($type == 1){?>
                            <div>
                                <label><?php echo get_string('applicationperiod', 'local_management') ?></label>
                                <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> " > 
                                <span class="dash">~</span>
                                <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> " > 
                            </div>
                            <div>
                                <label><?php echo get_string('isused', 'local_management') ?> </label>
                                <?php foreach($isused_array as $iakey => $iaval){?>
                                <label class="mg-bt10"><input type="checkbox" name="isused[]" value="<?php echo $iakey?>" <?php foreach($isused as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                                <?php }?>
                            </div>
                            <div>
                                <label><?php echo get_string('search', 'local_management') ?></label>
                                <input type="text" name="searchtext" placeholder="<?php echo get_string('review:searchtext', 'local_management')?>" value="<?php echo $searchtext?>">
                                <input type="submit" value="<?php echo get_string('search','local_management')?>" >
                                <input type="button" value="<?php echo get_string('reset','local_management')?>" id="reset">
                            </div>
                            <?php }else{?>
                            <div>
                                <label><?php echo get_string('courseproduct', 'local_management') ?></label>
                                <select name="classid">
                                    <option value="0"><?php echo get_string('delivery:all','local_management')?></option>
                                    <?php 
                                    if($class_array){
                                        foreach($class_array as $key => $val){
                                    ?>
                                    <option value="<?php echo $key?>"  <?php if($classid == $key){echo "selected";}?> ><?php echo $val?></option>
                                    <?php }}?>
                                </select>
                            </div>
                            <div>
                                <label><?php echo get_string('review:timecreated','local_management')?></label>
                                <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="timecreatedstart" class="w_120 date s_date" value="<?php echo $timecreatedstart_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> " > 
                                <span class="dash">~</span>
                                <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="timecreatedend" class="w_120 date e_date" value="<?php echo $timecreatedend_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> " > 
                            </div>
                            <div>
                                <label><?php echo get_string('isopened','local_management')?></label>
                                <?php foreach($isopened_array as $iakey => $iaval){?>
                                <label class="mg-bt10"><input type="checkbox" name="isopend[]" value="<?php echo $iakey?>" <?php foreach($isopend as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                                <?php }?>
                            </div>
                            <div>
                                <label><?php echo get_string('search', 'local_management') ?></label>
                                <input type="text" name="searchtext2" placeholder="<?php echo get_string('review:searchtext2', 'local_management')?>" value="<?php echo $searchtext?>">
                                <input type="submit" value="<?php echo get_string('search','local_management')?>" >
                                <input type="button" value="<?php echo get_string('reset','local_management')?>" id="reset">
                            </div>
                            <?php }?>
                        </div>
                    </form>
                </div>
                <div>
                    <span class='left'>
                        <span><?php echo get_string('total','local_management',$total)?></span>
                        <span>
                            <select name="perpage2" class="perpage2">
                                <?php foreach ($perpages as $perp) { ?>
                                    <option value="<?php echo $perp ?>" <?php echo $perpage == $perp ? 'selected' : '' ?>><?php echo $perp ?><?php echo get_string('viewmore','local_management')?></option>
                                <?php } ?>
                            </select>
                        </span>
                    </span>
                    <span class="btn_area">
                        <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="excel_download();"/> 
                    </span>
                </div>
                <div class = "tab-content">

                    <div id = "data1" class = "tab-pane fade in active">
                        <table>
                            <?php if($type == 1){?>
                            <thead class = "thead-light">
                                <th width="6%"><?php echo get_string('number', 'local_management') ?></th>
                                <th width="30%"><?php echo get_string('review:course','local_management')?></th>
                                <th><?php echo get_string('goodscode','local_management')?></th>
                                <th width="15%"><?php echo get_string('applicationperiod','local_management')?></th>
                                <th><?php echo get_string('isused', 'local_management') ?></th>
                                <th><?php echo get_string('review:title2', 'local_management') ?></th>
                                <th><?php echo get_string('review:grade', 'local_management') ?></th>
                                <th><?php echo get_string('review:go', 'local_management') ?></th>
                            </thead>
                            <tbody>
                                <?php
                                if ($contents) {
                                    $startnum = $total - (($page - 1) * $perpage);
                                    foreach ($contents as $content) {
                                        ?>
                                        <tr>
                                            <td><?php echo $startnum-- ?></td>
                                            <td><a href="<?php echo $CFG->wwwroot?>/local/management/goods/form.php?id=<?php echo $content->id?>"><?php echo $content->vi_title; ?><br><?php echo $content->title; ?></a></td>
                                            <td><?php echo $content->code; ?></td>
                                            <td><?php echo date("Y.m.d",$content->learningstart)?> ~ <?php echo date("Y.m.d",$content->learningend)?></td>
                                            <td><?php echo ($content->isused == 0) ? get_string('used', 'local_management') : get_string('notused', 'local_management'); ?></td>
                                            <td><a href="<?php echo $CFG->wwwroot?>/local/management/review/index.php?type=2&classid=<?php echo $content->mcid?>"><?php echo number_format($content->reviewcnt)?><?php echo get_string('count','local_course')?></a></td>
                                            <td><?php echo round($content->reviewavg) ?><?php echo get_string('score','local_course')?></td>
                                            <td><input type="button" class="normal_btn" value="<?php echo get_string('classroomentry', 'local_management') ?>" onclick="window.open('<?php echo $CFG->wwwroot . '/local/course/coursereview.php?id=' . $content->courseid; ?>')"/></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    ?>
                                <td colspan="8"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                            <?php } ?> 
                            </tbody>
                            <?php }else{?>
                            <thead class = "thead-light">
                                <th width="6%"><?php echo get_string('number', 'local_management') ?></th>
                                <th width="30%"><?php echo get_string('review:course','local_management')?></th>
                                <th><?php echo get_string('goodscode','local_management')?></th>
                                <th width="15%"><?php echo get_string('review:reviewcontents','local_management')?></th>
                                <th><?php echo get_string('review:scope','local_management')?></th>
                                <th><?php echo get_string('review:writer','local_management')?></th>
                                <th><?php echo get_string('review:timecreated','local_management')?></th>
                                <th><?php echo get_string('review:timemodified','local_management')?></th>
                                <th><?php echo get_string('isopened','local_management')?></th>
                            </thead>
                            <tbody>
                                <?php
                                if ($contents) {
                                    $startnum = $total - (($page - 1) * $perpage);
                                    foreach ($contents as $content) {
                                        ?>
                                        <tr>
                                            <td><?php echo $startnum-- ?></td>
                                            <td><a href="<?php echo $CFG->wwwroot?>/local/management/goods/form.php?id=<?php echo $content->id?>"><?php echo $content->vi_title; ?><br><?php echo $content->title; ?></a></td>
                                            <td><?php echo $content->code; ?></td>
                                            <td><?php echo $content->contents?></td>
                                            <td><?php echo $content->score?></td>
                                            <td><?php echo $content->firstname?></td>
                                            <td><?php echo date("Y.m.d", $content->timecreated)?></td>
                                            <td><?php echo ($content->timemodified != 0) ? date("Y.m.d", $content->timemodified) : '-'?></td>
                                            <td><input type="button" id="use_text" onclick="isopend(<?php echo $content->id ?>,<?php echo $content->isopend ?>)" value="<?php echo ($content->isopend == 0) ? get_string('opened','local_management'): get_string('notopened','local_management'); ?>"></td>
                                            
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    ?>
                                <td colspan="8"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                            <?php } ?> 
                            </tbody>
                            <?php }?>
                        </table>
                    </div>
                </div>
                <?php
                if (($total / $perpage) > 1) {
                    print_paging_navbar_script($total, $page, $perpage, 'javascript:cata_page(:page);');
                }

                $query_string = '';
                if (!empty($excel_params)) {
                    $query_array = array();
                    foreach ($excel_params as $key => $value) {
                        $query_array[] = urlencode($key) . '=' . urlencode($value);
                    }
                    $query_string = '?' . implode('&', $query_array);
                }
            ?>  
            </div>
            
        </div>
    </div>

</section>
<!--/.content -->


<?php
include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">
    function excel_download() {
        var form = $('#search_form');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
         return false;
    }
    function cata_page(page) {
        $('[name=page]').val(page);
        $('#search_form').submit();
    }
    start_datepicker_c('', '');
    end_datepicker_c('', '');

    $(document).ready(function () {
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=search_form]").submit();
        });
    
        $('#reset').click(function () {
            $('.date').val('');
            $("input:checkbox[name='isused[]']").attr("checked", false);
            $("input:checkbox[name='isopend[]']").attr("checked", false);
            $('input[name=searchtext]').val('');
            $('input[name=searchtext2]').val('');
            $('select[name=classid]').val('0');
            $("form[id=search_form]").submit();
        });

        $('#delivery_status li a').click(function () {
            var delivery = $(this).data('type');
            $('input[name=type]').val(delivery);
            $('#search_form').submit();
        })
    });

    function call_ajax(action, data, async) {
        var rvalue = false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            async: async,
            data: {action: action, data: data},
            success: function (result) {
                rvalue = result;
            },
            error: function (xhr, status, error) {
            }
        });

        return rvalue;
    }
    function isopend(id,opend){
        if (confirm("<?php echo get_string('review:text', 'local_management')?>")) {
            var result = call_ajax('isopend', {id: id, opend:opend}, false);
            if(result.status == "success") {
                location.reload();
            }
        }
    }

</script>


