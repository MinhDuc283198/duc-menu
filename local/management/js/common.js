$(document).ready(function () {
    //커스텀 파일 업로드
    var uploadFile = $('.input-file input[type=file]');
    uploadFile.on('change', function () {
        if (window.FileReader) {
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.file-name').val(filename);
    });
    //탭 이벤트
    $("[data-toggle='tab'] a").click(function () {
        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");
        var target = $(this).attr("href");
        $(target).removeClass("hide").addClass("show").addClass("active");
        $(target).siblings().removeClass("show").removeClass("active").addClass("hide");
        return false;
    });
    //탬 넓이 조절
    var num = $(".mk-c-tab > li").length;
    var tab_w = 100 / num;
    $(".mk-c-tab > li").width(tab_w + "%");

    //checkbox 전체선택
    $("input.checkall").click(function () {
        var check = $(this).prop("checked");
        $(this).closest("table").find(".conslist").prop("checked", check);
    });
    $(".table input[type=checkbox]").not(".checkall").click(function () {
        if (!$(this).prop("checked")) {
            $("input.checkall").prop("checked", false);
        }
    });

    //테이블 스크롤 적용
    $("table").wrap(function () {
        if (!$(this).hasClass("ui-dialog-content") && $(this).parents(".table-scroll").length == 0) {
            return "<div class='table-scroll'></div>"
        }
    });
    
    $("table td[style='text-align:left']").removeAttr("style").addClass("left");
   
    var price = $('.pricenumber').val();
    price = comma(price);
    $('.pricenumber').val(price);


//    var cellPrice = $('.pricenumber').html();   
//    cellPrice = comma(cellPrice);
//    $('.pricenumber').html(cellPrice);
});

function inputNumberFormat(obj) {
    obj.value = comma(uncomma(obj.value));
}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
}


function onlyNumber(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
        return;
    else
        return false;
}
function removeChar(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
        return;
    else
        event.target.value = event.target.value.replace(/[^0-9]/g, "");
}

function start_datepicker_c(mindate, maxdate) {
    $(".s_date").datepicker({
        dateFormat: "yy.mm.dd",
        minDate: mindate,
        maxDate: maxdate,
        onSelect: function (selectedDate) {
            // 시작일 선택시 연결된 종료일 datepicker의 최소값 설정
            $(this).siblings(".e_date").datepicker("option", "minDate", selectedDate);
        }
    });
}

function end_datepicker_c(mindate, maxdate) {
    $(".e_date").datepicker({
        dateFormat: "yy.mm.dd",
        minDate: mindate,
        maxDate: maxdate,
        onSelect: function (selectedDate) {
            // 종료일 선택시 연결된 시작일 datepicker의 최대값 설정
            $(this).siblings(".s_date").datepicker("option", "maxDate", selectedDate);
        }
    });
}