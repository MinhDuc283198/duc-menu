/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
            config.plugins =
                'about,' +
                'a11yhelp,' +
                'basicstyles,' +
                'bidi,' +
                'blockquote,' +
                'clipboard,' +
                'colorbutton,' +
                'colordialog,' +
                'contextmenu,' +
                'dialogadvtab,' +
                'div,' +
                'elementspath,' +
                'enterkey,' +
                'entities,' +
                'filebrowser,'+
                'find,' +
                'flash,' +
                'floatingspace,' +
                'font,' +
                'format,' +
                'forms,' +
                'horizontalrule,' +
                'htmlwriter,' +
                'image,' +
                'iframe,' +
                'indentlist,' +
                'indentblock,' +
                'justify,' +
                'link,' +
                'list,' +
                'liststyle,' +
                'magicline,' +
                'maximize,' +
                'newpage,' +
                'pagebreak,' +
                'pastefromword,' +
                'pastetext,' +
                'preview,' +
                'print,' +
                'removeformat,' +
                'resize,' +
                'selectall,' +
                'showblocks,' +
                'showborders,' +
                'smiley,' +
                'sourcearea,' +
                'specialchar,' +
                'stylescombo,' +
                'tab,' +
                'table,' +
                'tabletools,' +
                'templates,' +
                'toolbar,' +
                'undo,' +
                'wysiwygarea';
        config.image_previewText = ' ';
        config.allowedContent = true;
        config.filebrowserUploadUrl = '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Files';
        config.filebrowserImageUploadUrl = '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Images';
};