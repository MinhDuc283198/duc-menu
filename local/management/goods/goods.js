function call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}

function li_del(txt,id){
    $('li#'+txt+id).remove();
}

$(document).ready(function () {
    if($('input[name=discount_chk]:checked').val() == 'on'){
        //$("input[name=discount]").attr("disabled",false).attr("readonly",false);
    }else{
        //$("input[name=discount").attr("disabled",true).attr("readonly",false);
    }
    $('input[name=discount_chk]').on('click', function(){
        if($('input[name=discount_chk]:checked').val() == 'on'){
            var ocprice = Number($('input[name=ocprice]').val());
            var obprice = Number($('input[name=obprice]').val());
            var sum = Number(ocprice+obprice);
            var price = $('input[name=price]').val();
            price = Number(price.replace(/,/gi,''));
            var discount = (1 - (price / sum)) * 100;
            discount = Math.round(discount);
            $('input[name=discount]').val(discount);
            //$("input[name=discount]").attr("disabled",false).attr("readonly",false);
        }else{
            $('input[name=discount]').val('0');
            //$("input[name=discount").attr("disabled",true).attr("readonly",false);
        }
    })
    
    //초기셋팅
    if($('input[name=type]:checked').val() == 1){
        $(".type_textbook").hide();
        $(".type_course").show();
    }else if($('input[name=type]:checked').val() == 2){
        $(".type_textbook").show();
        $(".type_course").hide();
    }else if($('input[name=type]:checked').val() == 3){
        $(".type_textbook").show();
        $(".type_course").show();
    }
    //강좌 / 교재 선택 시
    $('input[name=type]').on('change', function() {
        if($('input[name=type]:checked').val() == 1){
            $(".type_textbook").hide();
            $(".type_course").show();
        }else if($('input[name=type]:checked').val() == 2){
            $(".type_textbook").show();
            $(".type_course").hide();
        }else if($('input[name=type]:checked').val() == 3){
            $(".type_textbook").show();
            $(".type_course").show();
        }
    });
    
//    $(".date").datepicker({
//        dateFormat: "yy-mm-dd",
//        onClose: function (selectedDate) {
//            $("#learningend").datepicker("option", "minDate", selectedDate);
//        }
//    });

    
    //강좌 원본 상품
    $('#btnCourseOriginSearch').on('click', function() {
        local_management_search_course_origin(1);
    });
    $('#p-modal-add-course-origin').on('click', function() {
        local_management_search_course_origin(1);
    });
    
    //교재 원본 상품
    $('#btnBookOriginSearch').on('click', function() {
        local_management_search_book_origin(1);
    });
    $('#p-modal-add-book-origin').on('click', function() {
        local_management_search_book_origin(1);
    });
    
    //관련 교재 상품
    $('#btnRtextbookSearch').on('click', function() {
        local_management_search_rtextbook(1);
    });
    $('#p-modal-add-rtextbook').on('click', function() {
        local_management_search_rtextbook(1);
    });

    //관련 강좌 상품
    $('#btnRcourseSearch').on('click', function() {
        local_management_search_rcourse(1);
    });
     $('#p-modal-add-rcourse').on('click', function() {
        local_management_search_rcourse(1);
    });
     $('#p-modal-add-badge').on('click', function() {
        local_management_search_badge(1);
    });
    
});





//값이 없는지 체크
function local_management_value_null_check(select){
    if( $(select).val() == null || $(select).val() == 0 || $(select).val() == undefined || $(select).val() == ''){
        return false;
    }else{
        return true;
    }
}

//function comma(num){
//    var len, point, str;  
//       
//    num = num + "";  
//    point = num.length % 3 ;
//    len = num.length;  
//   
//    str = num.substring(0, point);  
//    while (point < len) {  
//        if (str != "") str += ",";  
//        str += num.substring(point, point + 3);  
//        point += 3;  
//    }  
//     
//    return str;
// 
//}