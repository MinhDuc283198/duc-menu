<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/local/management/classes/template_import_ui.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/courses/lib.php');

$lmsdata = new stdClass();
$id = optional_param('id', 0, PARAM_INT); // lmsdata_class->id
$mod = optional_param('mod', '', PARAM_RAW);
foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $lmsdata->$key = $val;
}

//die;

$lmsdata->learningstart = str_replace(".", "-", $lmsdata->learningstart); 
$lmsdata->learningend = str_replace(".", "-", $lmsdata->learningend); 
$lmsdata->learningstart = strtotime($lmsdata->learningstart);
$lmsdata->learningend = strtotime($lmsdata->learningend) + 86399;
$lmsdata->deleted= 0;
$lmsdata->price = str_replace(',','',$lmsdata->price);
//if($lmsdata->relationcourse){
//    $lmsdata->relationcourse = implode(",", $lmsdata->relationcourse);
//}
//if($lmsdata->relationtextbook){
//    $lmsdata->relationtextbook = implode(",", $lmsdata->relationtextbook);
//}

$lmsdata->scheduleid = empty($scheduleid) ? '1' : $scheduleid; // 임의로 설정함
$lmsdata->cardinalnum = 1;

if($lmsdata->discount_chk != 'on'){
    $lmsdata->discount = 0;
}

if($mod != 'edit'){
    $lmsdata->timecreated = time();
    $lmsdata->userid = $USER->id;
    //코드 가져오기
    $ordernum = $DB->get_record('lmsdata_class', array('year'=>date('y',time())),'max(ordernum) as max');
    $lmsdata->ordernum = $ordernum->max+1;
    $lmsdata->year = date('y',time());
    //코드 중복체크
    $lmsdata->code = $lmsdata->year.'P'.sprintf("%04d",$lmsdata->ordernum);
    $overlap_cd1 = $DB->get_record('lmsdata_course', array('coursecd'=>$lmsdata->code));
    $overlap_cd2 = $DB->get_record('lmsdata_class', array('code'=>$lmsdata->code));
    $overlap_cd3 = $DB->get_record('lmsdata_textbook', array('code'=>$lmsdata->code));
    if($overlap_cd1 || $overlap_cd2 || $overlap_cd3){
        echo "코드가 중복되었습니다.";
        exit;
    }
    if($lmsdata->courseid){
        //강좌
        if($mod == 'copy'){
            $lmsdata_course = $DB->get_record('lmsdata_class', array('id' => $lmsdata->id));
            $copy_courseid = $lmsdata_course->courseid;
            $lmsdata_course = $DB->get_record('lmsdata_course', array('id' => $lmsdata_course->parentcourseid));
        }else{
            $lmsdata_course = $DB->get_record('lmsdata_course', array('courseid' => $lmsdata->courseid));
        }

        $coursething = $DB->get_record('course', array('id' => $lmsdata_course->courseid));
        $data = new stdClass();
        $data = $DB->get_record('course', array('id' => $lmsdata_course->courseid));
        $data->summary = $coursething->summary;
        $data->fullname = empty($fullname) ? $data->fullname : $fullname;

        $data->shortname = '[class_' . $data->fullname . ']' . time();
        $data->category = $coursething->category;
        $data->scheduleid = empty($scheduleid) ? '1' : $scheduleid;
        $data->courseid = empty($coid) ? $data->courseid : $coid;
        
        $course = create_course($data);

        $lmsdata->parentcourseid = $lmsdata_course->id;
        $lmsdata->courseid = $course->id;
        $lmsdata->classnum = $classnum;
        
        $lmsdata->templateyn = 'N';
        $lmsdata->coursename = $fullname;

        $classid = $aa = $DB->insert_record('lmsdata_class', $lmsdata);
        $skills = explode(',', $lmsdata->skills);
        foreach ($skills as $key => $value) {
            $skilldata = new stdClass();
            $skilldata->classid = $classid;
            $skilldata->skillid = $value;
            $skilldata->timecreated = time();
            $skilldata->timemodified = time();
            $DB->insert_record('lmsdata_class_skills', $skilldata);
        }
        if($aa){
            $DB->delete_records('lmsdata_class_relation', array('classid'=>$aa));
            $recourse = new stdClass();
            $rebook = new stdClass();
            //
            if ($lmsdata->relationbadge) {
                $recourse->classid = $aa;
                $recourse->type = 3;
                foreach ($lmsdata->relationbadge as $lr) {
                    $recourse->dataid = $lr;
                    $DB->insert_record('lmsdata_class_relation', $recourse);
                }
                $lmsdata->relationbadge = '';
            }
            //관련 강좌 / 교재
            if($lmsdata->relationcourse){
                $recourse->classid = $aa;
                $recourse->type = 1;
                foreach($lmsdata->relationcourse as $lr){
                    $recourse->dataid = $lr;
                    $DB->insert_record('lmsdata_class_relation', $recourse);
                }
                $lmsdata->relationcourse = '';
            }
            if($lmsdata->relationtextbook){
                $rebook->classid = $aa;
                $rebook->type = 2;
                foreach($lmsdata->relationtextbook as $lr){
                    $rebook->dataid = $lr;
                    $DB->insert_record('lmsdata_class_relation', $rebook);
                }
                $lmsdata->relationtextbook = '';
            }
        }
        if (edu_import_template($course->id, $lmsdata_course->courseid)) {

            if ($lmsdata->evaluationstart && $lmsdata->evaluationend) {
                //퀴즈 데이터 날짜 변경       
                $quizs = $DB->get_records('quiz', array('course' => $course->id));
                foreach ($quizs as $quiz) {
                    $quiz->timeopen = $lmsdata->learningstart;
                    $quiz->timeclose = $lmsdata->learningend;
                    $DB->update_record('quiz', $quiz);
                }
                //okmedia 데이터 날짜 변경       
                $okmedias = $DB->get_records('okmedia', array('course' => $course->id));
                foreach ($okmedias as $okmedia) {
                    $okmedia->timestart = $lmsdata->learningstart;
                    $okmedia->timeend = $lmsdata->learningend;
                    $DB->update_record('okmedia', $okmedia);
                }
            }
            //게시글 복사
            edu_import_board($course->id, $lmsdata_course->courseid);

            //템플릿상태값 변경
            $lmsdata->id = $classid;
            $lmsdata->templateyn = 'Y';
            $DB->update_record('lmsdata_class', $lmsdata);
        }
        //담당교수 가져오기
        if($mod != 'copy'){
            $teacher_list = $DB->get_records('lmsdata_course_professor', array('courseid'=>$lmsdata_course->id));
            if($teacher_list){
                $role = $DB->get_record('role', array('shortname' => 'editingteacher'), 'id, shortname');
                foreach($teacher_list as $tl){
                    //역할부여
                    $manager = $DB->get_record('user', array('id' => $tl->teacherid));
                    if($manager){
                        $manager->roleid = $role->id;
                        siteadmin_set_assign_user($course->id, $manager);
                    }
                }
            }
        }else{
            //복사할때 선생님 복사 하는거 하고있었음
            $coursecontext = context_course::instance($copy_courseid);
//            $contextids = $coursecontext->get_parent_context_ids();
//            $contextids[] = $coursecontext->id;
//            list($contextids, $params) = $DB->get_in_or_equal($contextids, SQL_PARAMS_QM);
            $sql = "SELECT ra.*, r.name, r.shortname
                    FROM {role_assignments} ra, {role} r, {context} c
                    WHERE ra.roleid = r.id
                    AND ra.contextid = c.id
                    AND ra.contextid = :contextid
                    AND r.id = 3";
            $test =  $DB->get_records_sql($sql ,array('contextid'=>$coursecontext->id));
            $role = $DB->get_record('role', array('shortname' => 'editingteacher'), 'id, shortname');
            foreach($test as $ct){
                //역할부여
                $manager = $DB->get_record('user', array('id' => $ct->userid));
                if($manager){
                    $manager->roleid = $role->id;
                    siteadmin_set_assign_user($course->id, $manager);
                }
            }
        }
    }else{
        $aa = $DB->insert_record('lmsdata_class', $lmsdata);
        if($aa){
            $DB->delete_records('lmsdata_class_relation', array('classid'=>$aa));
            $recourse = new stdClass();
            $rebook = new stdClass();
            //
            if ($lmsdata->relationbadge) {
                $recourse->classid = $aa;
                $recourse->type = 3;
                foreach ($lmsdata->relationbadge as $lr) {
                    $recourse->dataid = $lr;
                    $DB->insert_record('lmsdata_class_relation', $recourse);
                }
                $lmsdata->relationbadge = '';
            }
            //관련 강좌 / 교재
            if($lmsdata->relationcourse){
                $recourse->classid = $aa;
                $recourse->type = 1;
                foreach($lmsdata->relationcourse as $lr){
                    $recourse->dataid = $lr;
                    $DB->insert_record('lmsdata_class_relation', $recourse);
                }
                $lmsdata->relationcourse = '';
            }
            if($lmsdata->relationtextbook){
                $rebook->classid = $aa;
                $rebook->type = 2;
                foreach($lmsdata->relationtextbook as $lr){
                    $rebook->dataid = $lr;
                    $DB->insert_record('lmsdata_class_relation', $rebook);
                }
                $lmsdata->relationtextbook = '';
            }
        }
    }

}else{
//    //코드 중복체크
//    $overlap_cd1 = $DB->get_record('lmsdata_course', array('coursecd'=>$lmsdata->code));
//    $overlap_cd2 = $DB->get_record('lmsdata_class', array('code'=>$lmsdata->code));
//    $overlap_cd3 = $DB->get_record('lmsdata_textbook', array('code'=>$lmsdata->code));
//    if($overlap_cd1 || $overlap_cd2 || $overlap_cd3){
//        if($overlap_cd2->id != $lmsdata->id){
//            echo "코드가 중복되었습니다.";
//            exit;
//        }
//    }
    //수정
    $lmsdata->timemodified = time();
    $aa = $DB->update_record('lmsdata_class', $lmsdata);
    
    $DB->delete_records('lmsdata_class_skills',array('classid'=>$lmsdata->id));
    $skills = explode(',', $lmsdata->skills);
        foreach ($skills as $key => $value) {
            $skilldata = new stdClass();
            $skilldata->classid = $lmsdata->id;
            $skilldata->skillid = $value;
            $skilldata->timecreated = time();
            $skilldata->timemodified = time();
            $DB->insert_record('lmsdata_class_skills', $skilldata);
        }
    if($aa){
        $DB->delete_records('lmsdata_class_relation', array('classid'=>$lmsdata->id));
        $recourse = new stdClass();
        $rebook = new stdClass();
        //
        if ($lmsdata->relationbadge) {
            $recourse->classid = $lmsdata->id;
            $recourse->type = 3;
            foreach ($lmsdata->relationbadge as $lr) {
                $recourse->dataid = $lr;
                $DB->insert_record('lmsdata_class_relation', $recourse);
            }
            $lmsdata->relationbadge = '';
        }
        //관련 강좌 / 교재
        if ($lmsdata->relationcourse) {
            $recourse->classid = $lmsdata->id;
            $recourse->type = 1;
            foreach($lmsdata->relationcourse as $lr){
                $recourse->dataid = $lr;
                $DB->insert_record('lmsdata_class_relation', $recourse);
            }
            $lmsdata->relationcourse = '';
        }
        if($lmsdata->relationtextbook){
            $rebook->classid = $lmsdata->id;
            $rebook->type = 2;
            foreach($lmsdata->relationtextbook as $lr){
                $rebook->dataid = $lr;
                $DB->insert_record('lmsdata_class_relation', $rebook);
            }
            $lmsdata->relationtextbook = '';
        }
        
        $enrollist = $DB->get_records('enrol',array('courseid'=>$lmsdata->courseid));
        $course = $DB->get_record('course', array('id'=>$lmsdata->courseid));
        $edit_data = new stdClass();


        if($enrollist){
            foreach($enrollist as $el){
                $manager = new course_enrolment_manager($PAGE, $course, $filter);
                
                $edit_ue_query = "SELECT ue.*, u.id as uid, lu.userid, lu.usergroup FROM {user_enrolments} ue JOIN {user} u ON u.id = ue.userid JOIN {lmsdata_user} lu ON lu.userid = u.id WHERE ue.enrolid = :enrolid AND usergroup != 'pr' ";
                $edit_ue_list = $DB->get_records_sql($edit_ue_query, array('enrolid' => $el->id) );
                //$edit_ue_list = $DB->get_records('user_enrolments', array('enrolid' => $el->id));

                if($edit_ue_list){
                    //print_object($edit_ue_list);    //박강사(4) 있음
                    foreach($edit_ue_list as $eul){
                        $user = $DB->get_record('user', array('id'=>$eul->userid), '*', MUST_EXIST);
                        //print_object($user);
                        $edit_data->ue = $el->id;
                        $edit_ue = $DB->get_record('user_enrolments', array('id' => $eul->id), '*', MUST_EXIST);
                        $edit_data->timestart = $edit_ue->timestart;
                        $edit_data->timeend = $edit_data->timestart + ( ($lmsdata->courseperiod+$lmsdata->reviewperiod) * 86400);
                        $manager->edit_enrolment($edit_ue, $edit_data);
                    }
                }
                
            }
        }
    }
    
    $logdata = new stdClass();
    $logdata->productid = $lmsdata->id;
    $logdata->userid = $USER->id;
    $logdata->timecreated = time();
    $logdata->type = 1;

    $DB->insert_record('lmsdata_product_log', $logdata);
}

redirect($CFG->wwwroot . '/local/management/goods/index.php?'.$lmsdata->array);
