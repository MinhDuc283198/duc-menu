<?php
/**
 * 기수 생성 및 수정 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/goods/lib.php');
require_once $CFG->dirroot . '/lib/coursecatlib.php';
require_once $CFG->dirroot . '/local/popup/lib.php';
require_once $CFG->dirroot . '/lib/form/editor.php';

// Check for valid admin user - no guest autologin

$pagesettings = array(
    'title' => get_string('goods:title', 'local_management'),
    'heading' => get_string('goods:title', 'local_management'),
    'subheading' => '',
    'menu' => 'class',
    'js' => array(
        $CFG->wwwroot . '/chamktu/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot . '/chamktu/js/ckfinder-2.4/ckfinder.js',
        $CFG->wwwroot . '/local/management/goods/goods.js'
    ),
    'css' => array(),
    'nav_bar' => array()
);


require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('edu_class_add.php');
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$id = optional_param('id', 0, PARAM_INT); // lmsdata_class->id
$mod = optional_param('mod', '', PARAM_RAW);
$search_srt = json_decode($_GET['array']);
if ($mod != 'copy' && $id != 0) {
    $mod = 'edit';
}
// lmsdata가 없어서 임시로
//if (!empty($content_lmsdata)) {
if (!empty($id)) {
    $temp_query = 'select * from {lmsdata_class} where id=:id';
    $temp = $DB->get_record_sql($temp_query, array('id' => $id));
    if ($temp->type == 1) {
        $course_sql = "select lc.*, lco.price as lcoprice, '' as ltprice,  lco.coursename as lconame, lco.coursecd as lcocode, '' as lttitle, '' as ltcode 
                    from m_lmsdata_class lc
                    JOIN m_course mc ON lc.courseid = mc.id
                    JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
                    JOIN m_course mc2 ON lco.courseid = mc2.id
                    JOIN m_course_categories ca ON ca.id = mc2.category
                    left JOIN m_course_categories ca2 ON ca.parent = ca2.id
                    WHERE lc.type = 1 and lc.id=:id";
    } else if ($temp->type == 2) {
        $course_sql = "select lc.* , '' as lcoprice ,lt.price as ltprice, '' as lconame, '' as lcocode, lt.title as lttitle, lt.code as ltcode 
                    from m_lmsdata_class lc
                    JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
                    WHERE lc.type = 2  and lc.id=:id";
    } else {
        $course_sql = "select lc.* , lco.price as lcoprice ,lt.price as ltprice, lco.coursename as lconame, lco.coursecd as lcocode, lt.title as lttitle, lt.code as ltcode
                    from m_lmsdata_class lc
                    JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
                    JOIN m_course mc ON lc.courseid = mc.id
                    JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
                    JOIN m_course mc2 ON lco.courseid = mc2.id
                    JOIN m_course_categories ca ON ca.id = mc2.category
                    left JOIN m_course_categories ca2 ON ca.parent = ca2.id
                    WHERE lc.type = 3  and lc.id=:id";
    }
    $course = $DB->get_record_sql($course_sql, array('id' => $id));
}


$reviewperiod_array = array(0, 10, 20, 30);
$courseperiod_array = array(30, 50, 70, 100);
?>

<?php include_once($CFG->dirroot . '/local/management/header.php'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form name="" id="course_search" action="submit.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <input type="hidden" name="mod" value="<?php echo $mod ?>">
                    <input type="hidden" name="array" value="<?php echo $search_srt ?>">
                    <table cellpadding="0" cellspacing="0" class="detail">
                        <colgroup>
                            <col width="12%" />
                            <col width="12%" />
                            <col width="32%" />
                            <col width="12%" />
                            <col width="32%" />
                        </colgroup>
                        <tbody>

                            <tr id="table_class">
                                <td class="field_title" colspan="2"><?php echo get_string('division', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value" colspan="3">
                                    <input type="radio" name="type" value="1" <?php echo ($course->type == "" || $course->type == 1) ? "checked" : "" ?> <?php echo ($id) ? 'onclick="return(false);"' : '' ?>><?php echo get_string('course', 'local_management') ?>  
                                    <input type="radio" name="type" value="2" <?php echo ($course->type == 2) ? "checked" : "" ?>  <?php echo ($id) ? 'onclick="return(false);"' : '' ?>><?php echo get_string('book', 'local_management') ?> 
                                    <input type="radio" name="type" value="3" <?php echo ($course->type == 3) ? "checked" : "" ?>  <?php echo ($id) ? 'onclick="return(false);"' : '' ?>><?php echo get_string('course', 'local_management') ?>   + <?php echo get_string('book', 'local_management') ?> 
                                </td>
                            </tr>
                            <tr id="table_class">
                                <td class="field_title" rowspan="3"><?php echo get_string('goodsname', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_title" ><?php echo get_string('vi', 'local_management') ?> </td>
                                <td class="field_value" colspan="3">
                                    <input type="text" class="w100" name="vi_title" value="<?php echo $course->vi_title ?>">
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('ko', 'local_management') ?></td>
                                <td class="field_value" colspan="3"> 
                                    <input type="text" class="w100" name="title" value="<?php echo $course->title ?>">
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('en', 'local_management') ?>  </td>
                                <td class="field_value" colspan="3"> 
                                    <input type="text" class="w100" name="en_title" value="<?php echo $course->en_title ?>">
                                </td>
                            </tr>


                            <tr id="table_class">
                                <td class="field_title" colspan="2"><?php echo get_string('goodscode', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value" colspan="3">
                                    <span id="code_name"><?php echo $course->code ?>※ <?php echo get_string('code_text', 'local_management') ?></span>
                                </td>
                            </tr>
                            <?php
                            //print_object($course);exit;
                            ?>
                            <!-- 강좌 원본 정보-->
                            <tr id="table_class"  class="type_course">
                                <td class="field_title" colspan="2"><?php echo get_string('origincourse', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value" colspan="3">
                                    <?php if (!$id) { ?><input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-course-origin" id="p-modal-add-course-origin"><?php } ?>

                                    <ul id="course_origin_result" class="list-group close-box">
                                        <?php if ($course->type != 2 && $course->courseid) { ?>
                                            <li id="ocourse<?php echo $course->courseid ?>"><?php echo $course->lconame ?>(<?php echo $course->lcocode ?>) | <?php echo number_format($course->lcoprice) ?> VND<input type="hidden" name="courseid" value="<?php echo $course->courseid ?>" <?php echo ($course->courseid) ? 'readonly' : '' ?>></li>
                                        <?php } ?>
                                    </ul>

                                </td>
                            </tr>
                            <!-- 교재 원본 정보-->
                            <tr id="table_class"  class="type_textbook">
                                <td class="field_title" colspan="2"><?php echo get_string('originbook', 'local_management') ?>  <span class="red">*</span></td>
                                <td class="field_value" colspan="3">
                                    <?php if (!$id) { ?><input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-book-origin" id="p-modal-add-book-origin"><?php } ?>

                                    <ul id="book_origin_result" class="list-group close-box">
                                        <?php if ($course->type != 1 && $course->bookid) { ?>
                                            <li id="obook<?php echo $course->bookid ?>"><?php echo $course->lttitle ?>(<?php echo $course->ltcode ?>) | <?php echo number_format($course->ltprice) ?> VND<input type="hidden" name="bookid" value="<?php echo $course->bookid ?>" <?php echo ($course->bookid) ? 'readonly' : '' ?>></li>
                                        <?php } ?>
                                    </ul>

                                </td>
                            </tr>
                        <input type="hidden" name="ocprice" value="<?php echo $course->lcoprice ?>">
                        <input type="hidden" name="obprice" value="<?php echo $course->ltprice ?>">
                        <tr id="table_class">
                            <td class="field_title" colspan="2"><?php echo get_string('price2', 'local_management') ?> <span class="red">*</span></td>
                            <td class="field_value" colspan="3">
                                <div class='input-txt'>
                                    <input type="text"  class='pricenumber'  onkeyup="inputNumberFormat(this)" name="price" value="<?php echo number_format($course->price) ?>"> VND
                                </div>
                                <input type="checkbox" id='discount_chk' name='discount_chk' <?php echo ($course->discount != 0) ? 'checked' : '' ?>/> <label for ='discount_chk'><?php echo get_string('discountrat', 'local_management') ?></label>
                                <div class='input-txt'>
                                    <input type="text" name="discount" value="<?php echo $course->discount ?>" readonly> %
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title" id="courseptitle" colspan="2"><?php echo get_string('applicationperiod', 'local_management') ?> <span class="red">*</span></td>
                            <td class="field_value" id="coursepvalue">

                                <input type="text" title="<?php echo get_string('time', 'local_management') ?>" name="learningstart" class="w_120 date s_date" value="<?php echo ($course->learningstart) ? date('Y.m.d', $course->learningstart) : '' ?>" placeholder="<?php echo get_string('click', 'local_management') ?>"/> ~ 
                                <input type="text" title="<?php echo get_string('time', 'local_management') ?>" name="learningend"  class="w_120 date e_date" value="<?php echo ($course->learningend) ? date('Y.m.d', $course->learningend) : '' ?>" placeholder="<?php echo get_string('click', 'local_management') ?>"/> 
                            </td>
                        </tr>
                        <tr class="type_course">
                            <td class="field_title" colspan="2"><?php echo get_string('courseperiod', 'local_management') ?> <span class="red">*</span></td>
                            <td class="field_value" > 
                                <input type="text" name="courseperiod" id="courseperiod" onkeyup="daysnum()" value="<?php echo ($course->courseperiod) ? $course->courseperiod : '' ?>"><?php echo get_string('day', 'local_management');?>
                            </td>
                            <td class="field_title"><?php echo get_string('reviewperiod', 'local_management') ?> <span class="red">*</span></td>
                            <td class="field_value" > 
                                <select name="reviewperiod" id="reviewperiod">
                                    <?php
                                    foreach ($reviewperiod_array as $ra) {
                                        $selected = "";
                                        if ($ra == $course->reviewperiod) {
                                            $selected = "selected";
                                        }
                                        if ($ra == 0) {
                                            $ra_txt = get_string('none', 'local_management');
                                        } else {
                                            $ra_txt = $ra . get_string('day', 'local_management');
                                        }
                                        echo "<option value=" . $ra . " $selected >" . $ra_txt . "</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr id="table_class" class="type_course">
                            <td class="field_title" colspan="2"><?php echo get_string('jobs:job_skills', 'local_visang') ?> </td>
                            <td class="field_value" colspan="3">
                                <div class="autocomplete-bx">
                                    <input type="text"  class='' id =""   value="" />
                                    <div class="autocomplete-list">
                                        <ul class="skill_list_ul">
                                        </ul>
                                    </div>
                                </div>
                                <ul class="close-box skill_select_ul">
                                    <?php if($course){
                                        $skillslists = $DB->get_records_sql("select skillid,name
                                                                            from {lmsdata_class_skills} lcs
                                                                            join {vi_skills} vs on lcs.skillid = vs.id  where lcs.classid = ?",array($course->id));
                                        foreach ($skillslists as $skillslist) {  $skillids[] = $skillslist->skillid;?>
                                            
                                             <li class="skillsadd" id="skills<?php echo $skillslist->skillid ?>" data-id="<?php echo $skillslist->skillid ?>"><span><?php echo $skillslist->name ?></span><a class="close"></a></li>
                                    <?php }}?>
                                </ul>
                            </td>
                        <input type="hidden" name="skills" value="<?php echo $course ? implode(',',$skillids):'';?>">
                        </tr>
                        <tr id="table_class" class="type_course">
                            <td class="field_title" colspan="2"><?php echo get_string('jobs:korean_level', 'local_visang') ?> </td>
                            <td class="field_value" colspan="3">
                                <select name="klevel" id="reviewperiod">
                                    <?php
                                    $klevels = $DB->get_records("vi_korean_levels");
                                    foreach ($klevels as $klevel) {
                                        $selected = "";
                                        if ($klevel->id == $course->klevel) {
                                            $selected = "selected";
                                        }
                                        echo "<option value=" . $klevel->id . " $selected >" . $klevel->name . "</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr id="table_class">
                            <td class="field_title" colspan="2"><?php echo get_string('badge', 'local_management') ?> </td>
                            <td class="field_value" colspan="3">
                                <input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-badge" id="p-modal-add-badge">
                                <ul id="badge_result" class="list-group close-box">

                                    <?php
                                    $rebadge_list = $DB->get_records('lmsdata_class_relation', array('type' => 3, 'classid' => $id));
                                    if ($rebadge_list) {
                                        foreach ($rebadge_list as $ra) {
                                            $rcourse = getBadge($ra->dataid);
                                            ?>
                                            <li id='badge<?php echo $ra->dataid ?>'><?php echo $rcourse->name ?>(<?php echo $rcourse->code ?>)<a class='close' data-id='<?php echo $ra->dataid ?>' href='#'   onclick="li_del('badge',<?php echo $ra->dataid ?>)">close</a><input type='hidden' name='badge[]' value='<?php echo $ra->dataid ?>'></li>
                                        <?php }
                                    }
                                    ?>


                                </ul>
                            </td>
                        </tr>
                        <tr id="table_class">
                            <td class="field_title" colspan="2"><?php echo get_string('rcoursegoods', 'local_management') ?> </td>
                            <td class="field_value" colspan="3">
                                <input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-rcourse" id="p-modal-add-rcourse">
                                <ul id="relationcourse_result" class="list-group close-box">

                                    <?php
                                    $recourse_list = $DB->get_records('lmsdata_class_relation', array('type' => 1, 'classid' => $id));
                                    if ($recourse_list) {
                                        foreach ($recourse_list as $ra) {
                                            $rcourse = searchClass($ra->dataid);
                                            ?>
                                            <li id='rcourse<?php echo $ra->dataid ?>'><?php echo $rcourse->title ?>(<?php echo $rcourse->code ?>)<a class='close' data-id='<?php echo $ra->dataid ?>' href='#'   onclick="li_del('rcourse',<?php echo $ra->dataid ?>)">close</a><input type='hidden' name='relationcourse[]' value='<?php echo $ra->dataid ?>'></li>
                                        <?php }
                                    }
                                    ?>


                                </ul>
                            </td>
                        </tr>
                        <tr id="table_class">
                            <td class="field_title" colspan="2"><?php echo get_string('rbookgoods', 'local_management') ?> </td>
                            <td class="field_value" colspan="3">
                                <input type="button" value="<?php echo get_string('search', 'local_management') ?>"  data-toggle="modal" data-target="#modal-add-rtextbook" id="p-modal-add-rtextbook">
                                <ul id="relationtextbook_result" class="list-group close-box">

                                    <?php
                                    $rebook_list = $DB->get_records('lmsdata_class_relation', array('type' => 2, 'classid' => $id));
                                    if ($rebook_list) {
                                        foreach ($rebook_list as $ra) {
                                            $rbook = searchClass($ra->dataid);
                                            ?>
                                            <li id='rbook<?php echo $ra->dataid ?>'><?php echo $rbook->title ?>(<?php echo $rbook->code ?>)<a class='close' data-id='<?php echo $ra->dataid ?>' href='#' onclick="li_del('rbook',<?php echo $ra->dataid ?>)">close</a><input type='hidden' name='relationtextbook[]' value='<?php echo $ra->dataid ?>'></li>
    <?php }
}
?>

                                </ul>
                            </td>
                        </tr>

<!--                    <tr id="table_class" class="type_textbook" style="display:none">
    <td class="field_title">관련 강좌 상품 매핑 현황 <span class="red">*</span></td>
    <td class="field_value" colspan="3">
                        <?php
                        $rquery = "select * from {lmsdata_class} where type=1 and relationtextbook like '%$id%'";
                        $rcourse = $DB->get_records_sql($rquery);
                        if ($rcourse) {
                            foreach ($rcourse as $rc) {
                                echo '<a href="/local/management/goods/form.php?id=' . $rc->id . '">' . $rc->title . '(' . $rc->code . ')</a><br>';
                            }
                        }
                        ?>
    </td>
</tr>-->
                        <tr>
                            <td class="field_title" colspan="2"><?php echo get_string('isused', 'local_management') ?> <span class="red">*</span></td>
                            <td class="field_value" colspan="3"> 
                                <input type="radio" name="isused" value="0" <?php echo ($course->isused == 0) ? "checked" : "" ?> ><?php echo get_string('used', 'local_management') ?>
                                <input type="radio" name="isused" value="1"<?php echo ($course->isused == 1) ? "checked" : "" ?>><?php echo get_string('notused', 'local_management') ?>    
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title" rowspan="3"><?php echo get_string('goods:tag', 'local_management') ?></td>
                            <td class="field_title" ><?php echo get_string('vi', 'local_management') ?></td>
                            <td class="field_value" colspan="3">
                                <div class="detail-search" id="detail-search_vi">
                                    <input type="text" name="tmp_keywords_vi"  placeholder="<?php echo get_string('goods:taginput', 'local_management') ?>"/>
                                    <input type="button" value="<?php echo get_string('goods:input', 'local_management') ?>" id="detailsearch_vi_btn" class="btn">
                                </div>
                                <div class="hash-area">
                                    <?php
                                    if ($course->keywords_vi) {
                                        $keywords = explode("#", $course->keywords_vi);
                                        for ($i = 1; $i < count($keywords); $i++) {
                                            ?>
                                            <span class="vi" id="hash_vi<?php echo $i ?>" ><?php echo $keywords[$i] ?><a class="close" href="#">close</a>
                                                <input type="hidden" id="hash_vi_value<?php echo $i ?>" name="hash_vi_value<?php echo $i ?>" value="<?php echo $keywords[$i] ?>"> 
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <input type="hidden" name="keywords_vi" value="<?php echo $course->keywords_vi ?>"/>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="field_title" ><?php echo get_string('ko', 'local_management') ?></td>
                            <td class="field_value" colspan="3">
                                <div class="detail-search"  id="detail-search">
                                    <input type="text" name="tmp_keywords"  placeholder="<?php echo get_string('goods:taginput', 'local_management') ?>"/>
                                    <input type="button" value="<?php echo get_string('goods:input', 'local_management') ?>" id="detailsearch_btn" class="btn">
                                </div>
                                <div class="hash-area">
                                    <?php
                                    if ($course->keywords) {
                                        $keywords = explode("#", $course->keywords);
                                        for ($i = 1; $i < count($keywords); $i++) {
                                            ?>
                                            <span class="ko" id="hash<?php echo $i ?>" ><?php echo $keywords[$i] ?><a class="close" href="#">close</a>
                                                <input type="hidden" id="hash_value<?php echo $i ?>" name="hash_value<?php echo $i ?>" value="<?php echo $keywords[$i] ?>"> 
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <input type="hidden" name="keywords" value="<?php echo $course->keywords ?>"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title" ><?php echo get_string('en', 'local_management') ?>  </td>
                            <td class="field_value" colspan="3">
                                <div class="detail-search" id="detail-search_en">
                                    <input type="text" name="tmp_keywords_en"  placeholder="<?php echo get_string('goods:taginput', 'local_management') ?>"/>
                                    <input type="button" value="<?php echo get_string('goods:input', 'local_management') ?>" id="detailsearch_en_btn" class="btn">
                                </div>
                                <div class="hash-area">
                                    <?php
                                    if ($course->keywords_en) {
                                        $keywords = explode("#", $course->keywords_en);
                                        for ($i = 1; $i < count($keywords); $i++) {
                                            ?>
                                            <span class="en" id="hash_en<?php echo $i ?>" ><?php echo $keywords[$i] ?><a class="close" href="#">close</a>
                                                <input type="hidden" id="hash_en_value<?php echo $i ?>" name="hash_en_value<?php echo $i ?>" value="<?php echo $keywords[$i] ?>"> 
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <input type="hidden" name="keywords_en" value="<?php echo $course->keywords_en ?>"/>
                                </div>
                            </td>
                        </tr>
                        <?php
                        if ($id) {
                            $modi_query = "select * from {lmsdata_product_log} where type=1 and productid=:id order by timecreated desc limit 1";
                            $modi_sql = $DB->get_record_sql($modi_query, array("id" => $id));
                            ?>
                            <tr>
                                <td class="field_title" colspan="2"><?php echo get_string('insert', 'local_management') ?>/<?php echo get_string('modify', 'local_management') . get_string('day', 'local_management') ?></td>
                                <td class="field_value" colspan="3"> 
                                    <?php echo date('Y.m.d', $course->timecreated) ?>  (<?php echo local_management_return_user_info($course->userid) ?>)

                                    <?php
                                    if ($modi_sql) {
                                        echo get_string('modify', 'local_management') . ' :' . date('Y.m.d', $modi_sql->timecreated) . '(' . local_management_return_user_info($modi_sql->userid) . ')';
                                    }
                                    ?>
                                </td>
                            </tr>
    <?php
}
?>

                        </tbody>
                    </table>
                    <div id="btn_area">
                        <input type="submit" class="blue_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('save', 'local_management'); ?>"  onclick="return goods_create_submit_check()"/>
<?php if (!empty($id)) { ?>
                                    <!--<input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="course_delete('<?php echo $id; ?>')"/>-->
<?php } ?>
                        <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('list', 'local_management'); ?>" onclick="javascript:location.href = 'index.php?<?php echo $search_srt ?>';"/>
                    </div>

                    <div id="btnidarea"></div>

                </form><!--Search Area2 End-->

            </div><!--Content End-->

        </div> <!--Contents End-->
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script>
    $(document).ready(function () {
        var id = '<?php echo $id ?>';
        var type = '<?php echo $course->type ?>';
        if (id) {
            if (type == 1) {
                $(".type_textbook").hide();
                $(".type_course").show();
            } else if (type == 2) {
                $(".type_textbook").show();
                $(".type_course").hide();
            } else if (type == 3) {
                $(".type_textbook").show();
                $(".type_course").show();
            }
        }
        start_datepicker_c('', '');
        end_datepicker_c('', '');
    })

</script>
<!-- 강좌 원본상품 -->
<div class="modal modal-default fade" id="modal-add-course-origin">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo get_string('origincoursegoods', 'local_management') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchCourseOrigin">
                            <div class="input-group">
                                <input type="text" name="search" placeholder="<?php echo get_string('goods:ocoursesearch', 'local_management') ?>" class="form-control"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-flat" id="btnCourseOriginSearch"><?php echo get_string('search', 'local_management') ?></button>
    <!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="course-origin-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('name', 'local_management') ?></th>
                                    <th><?php echo get_string('code', 'local_management') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="popup_paging">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management') ?></button>
            </div>
        </div>
    </div>
</div>


<!-- 교재 원본상품 -->
<div class="modal modal-default fade" id="modal-add-book-origin">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo get_string('originbookgoods', 'local_management') ?> </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchBookOrigin">
                            <div class="input-group">
                                <input type="text" name="search" placeholder="<?php echo get_string('goods:obooksearch', 'local_management') ?>" class="form-control"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-flat" id="btnBookOriginSearch"><?php echo get_string('search', 'local_management') ?></button>
    <!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="book-origin-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('name', 'local_management') ?></th>
                                    <th><?php echo get_string('code', 'local_management') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="popup_paging">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- 관련 강좌 상품 -->
<div class="modal modal-default fade" id="modal-add-rcourse">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo get_string('rcoursegoods', 'local_management') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchRcourse">
                            <input type="hidden" name="origin_type" value="1" />
                            <div class="input-group">
                                <input type="text" name="search" placeholder="<?php echo get_string('goods:goodssearch', 'local_management') ?>" class="form-control"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-flat" id="btnRcourseSearch"><?php echo get_string('search', 'local_management') ?></button>
    <!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="rcourse-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('name', 'local_management') ?></th>
                                    <th><?php echo get_string('code', 'local_management') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="popup_paging">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management') ?></button>
            </div>
        </div>
    </div>
</div>


<!-- 관련 교재 상품 -->
<div class="modal modal-default fade" id="modal-add-rtextbook">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo get_string('rbookgoods', 'local_management') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchRtextbook">
                            <input type="hidden" name="origin_type" value="1" />
                            <div class="input-group">
                                <input type="text" name="search" placeholder="<?php echo get_string('goods:goodssearch', 'local_management') ?>" class="form-control"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-flat" id="btnRtextbookSearch"><?php echo get_string('search', 'local_management') ?></button>
    <!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="rtextbook-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('name', 'local_management') ?></th>
                                    <th><?php echo get_string('code', 'local_management') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="popup_paging">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-default fade" id="modal-add-badge">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo get_string('close', 'local_management') ?>">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo get_string('badge', 'local_management') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchbadge">
                            <input type="hidden" name="origin_type" value="1" />
                            <div class="input-group">
                                <input type="text" name="search" placeholder="<?php echo get_string('searchholder', 'local_management') ?>" class="form-control"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-flat" id="btnRtextbookSearch"><?php echo get_string('search', 'local_management') ?></button>
    <!--                                <input type="submit"  class="btn btn-danger btn-flat" id="btnSearchGroup" value="검색">-->
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="badge-list">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('name', 'local_management') ?></th>
                                    <th><?php echo get_string('code', 'local_management') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="popup_paging">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo get_string('close', 'local_management') ?></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //자동완성 이벤트
    function html_autocomplete(skillsdata,type){
        var html = "";
        $.each(skillsdata,function(k,v){
           html += '<li data-id="'+v.id+'" data-text="'+v.name+'" class="skillitem" ><a href="#">'+v.name+'</a></li>';
        });
        return html;
    }
    function make_skillval(){
       var skillid =  new Array();
        $.each($(".skillsadd"),function(){
           var id = $(this).attr("data-id");
           skillid.push(id);   
       });
       $('input[name=skills]').val(skillid.join(","));
    }
    $(".autocomplete-bx input[type=text]").keyup(function(){
        var txt = $.trim($(this).val());
        if(txt !== ""){
            var skillsdata = call_ajax('get_skill',{id:0,txt:txt},false);
            var html = html_autocomplete(skillsdata.info);
            $(".skill_list_ul").empty().append(html);
            $(this).siblings(".autocomplete-list").show(); 
        }else{
            $(this).siblings(".autocomplete-list").hide(); 
        }
    });
        $("input[name=courseperiod]").on("keyup", function() {
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        });
    $(".autocomplete-bx input[type=text]").keyup(function(){
        var txt = $.trim($(this).val());
        if(txt !== ""){
            var skillsdata = call_ajax('get_skill',{id:0,txt:txt},false);
            var html = html_autocomplete(skillsdata.info);
            $(".skill_list_ul").empty().append(html);
            $(this).siblings(".autocomplete-list").show(); 
        }else{
            $(this).siblings(".autocomplete-list").hide(); 
        }
    });
    $(".autocomplete-bx input[type=text]").focus(function(){
        var txt = $(this).val();
        if(txt !== ""){
            $(this).siblings(".autocomplete-list").show(); 
        }
    });
    $(document).on('click','.skillsadd a',function(){
        $(this).parent().remove();
        make_skillval();
    });
    $(document).on('click','.skillitem',function(){
        console.log($('#skills'+ $(this).attr("data-id")).attr('data-id') );
        if($('#skills'+ $(this).attr("data-id")).attr('data-id') == null){
        var html = '<li class="skillsadd" id="skills'+ $(this).attr("data-id")+'" data-id="'+ $(this).attr("data-id")+'"><span>'+ $(this).attr("data-text")+'</span><a class="close"></a></li>';
            $(".skill_select_ul").append(html);
            make_skillval();
        }
       $(".autocomplete-list").hide(); 
    });
//    $(".autocomplete-bx input[type=text]").blur(function(){
//        $(this).siblings(".autocomplete-list").hide(); 
//    });
    
    $("#detailsearch_btn").on("click", function (e) {
        keywords_insert("");
        return false;
    });
    function keywords_insert(lang) {
        var tag_num = 0;
        var lang_txt = "";
        if (lang != '') {
            lang_txt = "_" + lang;
        }

        var hash_tag = $('input[name=tmp_keywords' + lang_txt + ']').val();
        if (hash_tag == "") {
            alert("<?php echo get_string('goods:taqalert', 'local_management') ?>");
            return false;
        } else {
            if (hash_tag.match('#')) {
                alert('<?php echo get_string('goods:tagalert2', 'local_management') ?>');
                return false;
            }
        }

        var pre = $('input[name=keywords' + lang_txt + ']').val();
        $('input[name=keywords' + lang_txt + ']').val(pre + '#' + hash_tag);

        var hash_area = $('#detail-search' + lang_txt).closest("td").find(".hash-area");
        var tag = ' <span id="hash' + tag_num + '" >' + $(".detail-search input[name=tmp_keywords" + lang_txt + "]").val()
                + '<a class="close" href="#">close</a><input type="hidden" name="hash"' + lang_txt + '"_value" id="hash"' + lang_txt + '"_value' + tag_num + '" value="' + $(".detail-search input[type=text]").val() + '"> </span>';
        hash_area.append(tag);
        $(".detail-search input[name=tmp_keywords" + lang_txt + "]").val("");
        tag_num++;
    }

    $("#detailsearch_vi_btn").on("click", function (e) {
        keywords_insert("vi");
        return false;
    });
    $("#detailsearch_en_btn").on("click", function (e) {
        keywords_insert("en");
        return false;
    });

    $(document).on("click", ".hash-area > span .close", function () {
        var lang = '_' + $(this).parent("span").attr('class');
        $(this).parent().remove();
        if (lang == '_ko') {
            lang = '';
        }
        var test = "";
        for (var i = 1; i < 500; i++) {
            if ($('#hash' + lang + '_value' + i).val()) {
                test += '#' + $('#hash' + lang + '_value' + i).val();
            }
            $('input[name=keywords' + lang + ']').val(test);
        }
        return false;
    });

    $("#modal-add-course-origin").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                local_management_search_course_origin(1);
                return;
            }
        }
    });
    $("#modal-add-book-origin").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                local_management_search_book_origin(1);
                return;
            }
        }
    });

    $("input[name=tmp_keywords]").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                keywords_insert();
                return;
            }
        }
    });
    $("#modal-add-badge").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                local_management_search_badge(1);
                return;
            }
        }
    });
    $("#modal-add-rcourse").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                local_management_search_rcourse(1);
                return;
            }
        }
    });

    $("#modal-add-rtextbook").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                local_management_search_rtextbook(1);
                return;
            }
        }
    });

    $("input[name=tmp_keywords_vi]").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                keywords_insert("vi");
                return false;
            }
        }
    });

    $("input[name=tmp_keywords]").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                keywords_insert("");
                return false;
            }
        }
    });

    $("input[name=tmp_keywords_en]").keydown(function (event) {
        if (event.keyCode == '13') {
            if (window.event) {
                event.preventDefault();
                keywords_insert("en");
                return false;
            }
        }
    });

    $(function () {
<?php if ($id != 0) { ?>
            $('input[name=type]').attr('readonly', 'true');
            //$('#code_name').append('※ '+"<?php echo get_string('code_text', 'local_management') ?>");
<?php } ?>
    })
    
    //값 체크
    function goods_create_submit_check() {
        //상품 type
        var type = $('input[name="type"]:checked').val();
        //구분
        if (local_management_value_null_check('input[name="type"]:checked') == false) {
            alert('<?php echo get_string('goods:valdivision', 'local_management') ?>.');
            return false;
        }
        //상품명
        if (local_management_value_null_check('input[name=title]') == false) {
            alert('<?php echo get_string('goods:valgoodsname', 'local_management') ?>');
            return false;
        }
        //상품코드
//    if(local_management_value_null_check('input[name=code]') == false){
//        alert('상품코드를 입력해주세요.');
//        return false;
//    }
        //금액
//    if(local_management_value_null_check('input[name=price]') == false){
//        alert('<?php echo get_string('goods:valprice2', 'local_management') ?>');
//        return false;
//    }
        //적용기간
        if (local_management_value_null_check('input[name=learningstart]') == false || local_management_value_null_check('input[name=learningend]') == false) {
            alert('<?php echo get_string('goods:valperiod', 'local_management') ?>');
            return false;
        }
        //사용여부
        if ($('input:radio[name=isused]').is(':checked') == false) {
            alert('<?php echo get_string('goods:valisused', 'local_management') ?>');
            return false;
        }

        if (type == 1 || type == 3) {
            //강좌
            //수강기간
            if (local_management_value_null_check('#courseperiod') == false) {
                alert('<?php echo get_string('goods:valperiod2', 'local_management') ?>');
                return false;
            }
            //복습기간 -> 복습기간이 0일인 강좌가 있을 수 있어
//        if(local_management_value_null_check('#reviewperiod') == false){
//            alert('복습기간을 입력해주세요.');
//            return false;
//        }
            //원본정보
            if (local_management_value_null_check('input[name=courseid]') == false) {
                alert('<?php echo get_string('goods:valorigincourse', 'local_management') ?>');
                return false;
            }
        } else if (type == 2 || type == 3) {
            //교재  
            //원본정보
            if (local_management_value_null_check('input[name=bookid]') == false) {
                alert('<?php echo get_string('goods:valoriginbook', 'local_management') ?>');
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

//강좌 원본상품 검색
    function local_management_search_course_origin(page) {
        var search = $('form#searchCourseOrigin input[name=search]').val();
        search = $.trim(search);
//    if(search != ''){
//        if(search.trim().length == 0) {
//            alert("검색어를 입력하세요");
//            return false;
//        }
//    }
        $('#searchCourseOrigin').keydown(function (e) {
            if (e.keyCode == 13) {
                event.preventDefault();
                //$('#searchCourseOrigin').submit();
            }
        });
        var result = call_ajax('coruseoriginlist', {search: search, page: page}, false);
        if (result.status == "success") {
            $tbody = $('#modal-add-course-origin #course-origin-list table tbody');
            $tbody.empty();
            $paging = $('#modal-add-course-origin .popup_paging');
            $paging.empty();
            if (result.list.total_count > 0) {
                var $i = result.list.total_count - ((page - 1) * 10);
                $.each(result.list.list, function () {
                    $tr = $('<tr>');
                    $tr.data(this);
                    var $num = $('<td>').text($i);
                    var $tdFullName = $('<td>');

                    switch ('<?php echo current_language() ?>') {
                        case 'ko' :
                            $fullnametxt = this.coursename;
                            break;
                        case 'en' :
                            $fullnametxt = this.en_coursename;
                            break;
                        case 'vi' :
                            $fullnametxt = this.vi_coursename;
                            break;
                    }

                    var $fullname = $('<span>').addClass('txt').text($fullnametxt);
                    $tdFullName.append($fullname);

                    var $tdEmail = $('<td>').text(this.coursecd);
                    var $tdButtons = $('<td>');
                    ;
                    var $divbtn = $('<div>').addClass('btn-group pull-right');
                    var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                    $divbtn.append($btnAdd);
                    $($btnAdd).on('click', function (e) {
                        e.preventDefault();
                        var $tr = $(this).closest('tr');
                        local_management_add_course_origin($tr.data());
                        $('#modal-add-course-origin').modal('hide');
                    });
                    $tdButtons.append($divbtn);

                    $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                    $tbody.append($tr);
                    $i--;
                });
                $paging.append(result.paging);
            } else {
                $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>');
                $paging.append(result.paging);
            }
        } else {
            alert(result.error);
        }
    }

    function local_management_add_course_origin(origin) {
        var originid = origin.id;

        var result = call_ajax('add_course_origin', {originid: originid}, false);
        if (result.status == 'success') {
            switch ('<?php echo current_language() ?>') {
                case 'ko' :
                    $fullname = result.list.coursename;
                    break;
                case 'en' :
                    $fullname = result.list.en_coursename;
                    break;
                case 'vi' :
                    $fullname = result.list.vi_coursename;
                    break;
            }
            $li = "<li id='obook" + result.list.id + "'>" + $fullname + '(' + result.list.coursecd + ') | ' + comma(result.list.price) + 'VND' + "<a class='close' href='#'  onclick='li_del(\"obook\"," + result.list.id + ")'>close</a><input type='hidden' name='courseid' value='" + result.list.id + "'></li>";
            var $ul = $('#course_origin_result');
            $ul.html($li);
            $('input[name=ocprice]').val(result.list.price);

        } else {
            alert(result.error);
        }
    }

//교재 원본상품 검색
    function local_management_search_book_origin(page) {
        var search = $('form#searchBookOrigin input[name=search]').val();
        search = $.trim(search);
//    if(search != ''){
//        if(search.trim().length == 0) {
//            alert("검색어를 입력하세요");
//            return false;
//        }
//    }
        var result = call_ajax('bookoriginlist', {search: search, page: page}, false);
        if (result.status == "success") {
            $tbody = $('#modal-add-book-origin #book-origin-list table tbody');
            $tbody.empty();
            $paging = $('#modal-add-book-origin .popup_paging');
            $paging.empty();
            if (result.list.total_count > 0) {
                var $i = result.list.total_count - ((page - 1) * 10);
                $.each(result.list.list, function () {
                    $tr = $('<tr>');
                    $tr.data(this);
                    var $num = $('<td>').text($i);
                    var $tdFullName = $('<td>');
                    switch ('<?php echo current_language() ?>') {
                        case 'ko' :
                            $fullnametxt = this.title;
                            break;
                        case 'en' :
                            $fullnametxt = this.en_title;
                            break;
                        case 'vi' :
                            $fullnametxt = this.vi_title;
                            break;
                    }
                    var $fullname = $('<span>').addClass('txt').text($fullnametxt);
                    $tdFullName.append("&nbsp;").append($fullname);

                    var $tdEmail = $('<td>').text(this.code);


                    var $tdButtons = $('<td>');
                    ;
                    var $divbtn = $('<div>').addClass('btn-group pull-right');
                    var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                    $divbtn.append($btnAdd);
                    $($btnAdd).on('click', function (e) {
                        e.preventDefault();
                        var $tr = $(this).closest('tr');
                        local_management_add_book_origin($tr.data());
                        $('#modal-add-book-origin').modal('hide');
                    });
                    $tdButtons.append($divbtn);

                    $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                    $tbody.append($tr);
                    $i--;
                });
                $paging.append(result.paging);
            } else {
                $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>');
                $paging.append(result.paging);
            }
        } else {
            alert(result.error);
        }
    }

    function local_management_add_book_origin(origin) {
        var originid = origin.id;
        var result = call_ajax('add_book_origin', {originid: originid}, false);
        if (result.status == 'success') {
            switch ('<?php echo current_language() ?>') {
                case 'ko' :
                    $fullname = result.list.title;
                    break;
                case 'en' :
                    $fullname = result.list.en_title;
                    break;
                case 'vi' :
                    $fullname = result.list.vi_title;
                    break;
            }
            $li = "<li id='ocourse" + result.list.id + "'>" + $fullname + '(' + result.list.code + ') | ' + comma(result.list.price) + 'VND' + "<a class='close' href='#'   onclick='li_del(\"ocourse\"," + result.list.id + ")' >close</a><input type='hidden' name='bookid' value='" + result.list.id + "'></li>";
            var $ul = $('#book_origin_result');
            $ul.html($li);
            $('input[name=obprice]').val(result.list.price);
        } else {
            alert(result.error);
        }
    }


    function local_management_search_badge(page) {
        var search = $('form#searchbadge input[name=search]').val();

        search = $.trim(search);
        var result = call_ajax('badgelist', {search: search, page: page}, false);
        if (result.status == "success") {
            console.log(search);
            $tbody = $('#modal-add-badge #badge-list table tbody');
            $tbody.empty();
            $paging = $('#modal-add-badge .popup_paging');
            $paging.empty();
            if (result.list.total_count > 0) {
                var $i = result.list.total_count - ((page - 1) * 10);
                $.each(result.list.list, function () {
                    var badgedata = this;
                    $tr = $('<tr>');
                    $tr.data(this);
                    var $num = $('<td>').text($i);
                    var $tdFullName = $('<td>');
                    var $fullname = $('<span>').addClass('txt').text(this.name);
                    $tdFullName.append($fullname);
                    var $tdEmail = $('<td>').text(this.code);
                    var $tdButtons = $('<td>');
                    var $divbtn = $('<div>').addClass('btn-group pull-right');
                    var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                    $divbtn.append($btnAdd);
                    $($btnAdd).on('click', function (e) {
                        e.preventDefault();
                        var $tr = $(this).closest('tr');
                        local_management_add_badge(badgedata);
                        $tr.remove();
                        $('#modal-add-badge').modal('hide');
                    });
                    $tdButtons.append($divbtn);

                    $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                    $tbody.append($tr);
                    $i--;
                });
                $paging.append(result.paging);
            } else {
                $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>');
                $paging.append(result.paging);
            }
        } else {
            alert(result.error);
        }
    }
    function local_management_add_badge(badge) {
        $fullname = badge.name;
        //$li = local_management_menu_render_user(user);
        $li = "<li id='badge" + badge.id + "'>" + $fullname + '(' + badge.code + ')' + "<a class='close' href='#'  onclick='li_del(\"badge\"," + badge.id + ")'>close</a><input type='hidden' name='relationbadge[]' value='" + badge.id + "'></li>";
        var $ul = $('#badge_result');
        $ul.append($li);
    }
//관련 강좌 상품
    function local_management_search_rcourse(page) {
        var search = $('form#searchRcourse input[name=search]').val();
        var id = $('form#course_search input[name=id]').val();
        search = $.trim(search);
//    if(search != ''){
//        if(search.trim().length == 0) {
//            alert("검색어를 입력하세요");
//            return false;
//        }`
//    }
        var result = call_ajax('rcourselist', {search: search, id: id, page: page}, false);
        if (result.status == "success") {
            $tbody = $('#modal-add-rcourse #rcourse-list table tbody');
            $tbody.empty();
            $paging = $('#modal-add-rcourse .popup_paging');
            $paging.empty();
            if (result.list.total_count > 0) {
                var $i = result.list.total_count - ((page - 1) * 10);
                $.each(result.list.list, function () {
                    $tr = $('<tr>');
                    $tr.data(this);
                    var $num = $('<td>').text($i);
                    var $tdFullName = $('<td>');
                    var $fullname = $('<span>').addClass('txt').text(this.lang_title);
                    $tdFullName.append($fullname);

                    var $tdEmail = $('<td>').text(this.code);

                    var $tdButtons = $('<td>');
                    var $divbtn = $('<div>').addClass('btn-group pull-right');
                    var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                    $divbtn.append($btnAdd);
                    $($btnAdd).on('click', function (e) {
                        e.preventDefault();
                        var $tr = $(this).closest('tr');
                        local_management_add_rcourse($tr.data());
                        $tr.remove();
                        $('#modal-add-rcourse').modal('hide');
                    });
                    $tdButtons.append($divbtn);

                    $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                    $tbody.append($tr);
                    $i--;
                });
                $paging.append(result.paging);
            } else {
                $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>');
                $paging.append(result.paging);
            }
        } else {
            alert(result.error);
        }
    }

    function local_management_add_rcourse(course) {
        var courseid = course.id;

        var result = call_ajax('add_rcourse', {courseid: courseid}, false);
        if (result.status == 'success') {
            switch ('<?php echo current_language() ?>') {
                case 'ko' :
                    $fullname = result.list.title;
                    break;
                case 'en' :
                    $fullname = result.list.en_title;
                    break;
                case 'vi' :
                    $fullname = result.list.vi_title;
                    break;
            }
            //$li = local_management_menu_render_user(user);
            $li = "<li id='rcourse" + result.list.id + "'>" + $fullname + '(' + result.list.code + ')' + "<a class='close' href='#'  onclick='li_del(\"rcourse\"," + result.list.id + ")'>close</a><input type='hidden' name='relationcourse[]' value='" + result.list.id + "'></li>";
            var $ul = $('#relationcourse_result');
            $ul.append($li);
        } else {
            alert(result.error);
        }
    }

//관련 교재 상품
    function local_management_search_rtextbook(page) {
        var search = $('form#searchRtextbook input[name=search]').val();
        var id = $('form#course_search input[name=id]').val();
        search = $.trim(search);
//    if(search != ''){
//        if(search.trim().length == 0) {
//            alert("검색어를 입력하세요");
//            return false;
//        }
//    }
        var result = call_ajax('rtextbooklist', {search: search, id: id, page: page}, false);
        if (result.status == "success") {
            $tbody = $('#modal-add-rtextbook #rtextbook-list table tbody');
            $tbody.empty();
            $paging = $('#modal-add-rtextbook .popup_paging');
            $paging.empty();
            if (result.list.total_count > 0) {
                var $i = result.list.total_count - ((page - 1) * 10);
                $.each(result.list.list, function () {
                    $tr = $('<tr>');
                    $tr.data(this);
                    var $num = $('<td>').text($i);
                    var $tdFullName = $('<td>');
                    var $fullname = $('<span>').addClass('txt').text(this.lang_title);
                    $tdFullName.append($fullname);

                    var $tdEmail = $('<td>').text(this.code);

                    var $tdButtons = $('<td>');
                    var $divbtn = $('<div>').addClass('btn-group pull-right');
                    var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                    $divbtn.append($btnAdd);
                    $($btnAdd).on('click', function (e) {
                        e.preventDefault();
                        var $tr = $(this).closest('tr');
                        local_management_add_rtextbook($tr.data());
                        $tr.remove();
                        $('#modal-add-rtextbook').modal('hide');
                    });
                    $tdButtons.append($divbtn);

                    $tr.append($num).append($tdFullName).append($tdEmail).append($tdButtons);
                    $tbody.append($tr);
                    $i--;
                });
                $paging.append(result.paging);
            } else {
                $tbody.append('<tr><td colspan="4"><?php echo get_string('empty_data', 'local_management') ?></td></tr>');
                $paging.append(result.paging);
            }
        } else {
            alert(result.error);
        }
    }

    function local_management_add_rtextbook(textbook) {
        var textbookid = textbook.id;

        var result = call_ajax('add_rtextbook', {textbookid: textbookid}, false);
        if (result.status == 'success') {
            //$li = local_management_menu_render_user(user);
            switch ('<?php echo current_language() ?>') {
                case 'ko' :
                    $fullname = result.list.title;
                    break;
                case 'en' :
                    $fullname = result.list.en_title;
                    break;
                case 'vi' :
                    $fullname = result.list.vi_title;
                    break;
            }
            $li = "<li id='rbook" + result.list.id + "'>" + $fullname + '(' + result.list.code + ')' + "<a class='close' href='#' onclick='li_del(\"rbook\"," + result.list.id + ")'>>close</a><input type='hidden' name='relationtextbook[]' value='" + result.list.id + "'></li>";
            var $ul = $('#relationtextbook_result');
            $ul.append($li);
        } else {
            alert(result.error);
        }
    }
</script>