<?php

/**
 * 과정관리 리스트 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('goods:title', 'local_management'),
    'heading' => get_string('goods:title', 'local_management'),
    'subheading' => '',
    'menu' => 'class',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$type = optional_param_array('type', array(), PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$isused = optional_param_array('isused', array(), PARAM_RAW);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$searchtext = trim($searchtext);

$periodtype = optional_param('periodtype', 0, PARAM_INT);

if ($learningstart_str) {
    $learningstart = str_replace(".", "-", $learningstart_str);
    $learningstart = strtotime($learningstart);
}
if ($learningend_str) {
    $learningend = str_replace(".", "-", $learningend_str);
    $learningend = strtotime($learningend);
}
if ($learningstart && $periodtype == 1) {
    $sql_where[] = " a.learningstart >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if ($learningend && $periodtype == 1) {
    $sql_where[] = " a.learningend <= :learningend ";
    $params['learningend'] = $learningend;
}

if ($type) {
    foreach ($type as $tkey => $tval) {
        $sql_where_type[] = " a.type = :type" . $tkey;
        $params['type' . $tkey] = $tval;
    }
    if (!empty($sql_where_type)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_type) . ')';
    } else {
        $sql_where[] = '';
    }
}
if ($isused) {
    foreach ($isused as $iskey => $isval) {
        $sql_where_isused[] = " a.isused = :isused" . $iskey;
        $params['isused' . $iskey] = $isval;
    }
    if (!empty($sql_where_isused)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($searchtext)) {
    $sql_where_search[] = $DB->sql_like('a.title', ':title');
    $params['title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('a.vi_title', ':vi_title');
    $params['vi_title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('a.en_title', ':en_title');
    $params['en_title'] = '%' . $searchtext . '%';
    $sql_where_search[] = $DB->sql_like('a.code', ':code');
    $params['code'] = '%' . $searchtext . '%';
    $sql_where[] = '(' . implode(' OR ', $sql_where_search) . ')';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$orderby = ' order by id desc ';

$sql_select = 'select *';
$sql = " from (select lc.*, lco.coursename as lconame,lco.en_coursename as en_lconame,lco.vi_coursename as vi_lconame, lco.coursecd as lcocode, '' as lttitle,'' as en_lttitle,'' as vi_lttitle, '' as ltcode, mc.id as mcid, '' as lcolecturecnt   
from m_lmsdata_class lc
JOIN m_course mc ON lc.courseid = mc.id
JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
JOIN m_course mc2 ON lco.courseid = mc2.id
JOIN m_course_categories ca ON ca.id = mc2.category
left JOIN m_course_categories ca2 ON ca.parent = ca2.id
WHERE lc.type = 1 
UNION
select lc.* , '' as lconame,'' as en_lconame, '' as vi_lconame, '' as lcocode, lt.title as lttitle,lt.en_title as en_lttitle,lt.vi_title as vi_lttitle, lt.code as ltcode, '' as mcid, '' as lcolecturecnt   
from m_lmsdata_class lc
JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
WHERE lc.type = 2 
UNION
select lc.* , lco.coursename as lconame,lco.en_coursename as en_lconame,lco.vi_coursename as vi_lconame, lco.coursecd as lcocode, lt.title as lttitle,lt.en_title as en_lttitle,lt.vi_title as vi_lttitle, lt.code as ltcode, mc.id as mcid, lco.lecturecnt as lcolecturecnt   
from m_lmsdata_class lc
JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
JOIN m_course mc ON lc.courseid = mc.id
JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
JOIN m_course mc2 ON lco.courseid = mc2.id
JOIN m_course_categories ca ON ca.id = mc2.category
left JOIN m_course_categories ca2 ON ca.parent = ca2.id
WHERE lc.type = 3) a $sql_where";

$courses = $DB->get_records_sql($sql_select . $sql . $orderby, $params, ($currpage - 1) * $perpage, $perpage);
$count_courses = $DB->count_records_sql('select count(*)' . $sql, $params);
$perpage_array = [10, 20, 30, 50];
$isused_array = array(get_string('used', 'local_management'), get_string('notused', 'local_management'));
$type_array = array('1' => get_string('course', 'local_management'), '2' => get_string('book', 'local_management'), '3' => get_string('course', 'local_management') . '+' . get_string('book', 'local_management'));
$get_array = explode('.php?', $_SERVER['REQUEST_URI']);
?>

<?php include_once($CFG->dirroot . '/local/management/header.php'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">

                <form name="course_search" id="course_search" class="search_area" action="index.php" method="get">
                    <input type="hidden" name="page" value="1" />
                    <input type="hidden" name="perpage" value="<?php echo $perpage ?>" />
                    <div>
                        <label><?php echo get_string('goods:type', 'local_management') ?></label>
                        <?php foreach ($type_array as $tkey => $tval) { ?>
                            <label class="mg-bt10"><input type="checkbox" name="type[]" value="<?php echo $tkey ?>" <?php foreach ($type as $ischeck) {
                                                                                                                        if ($ischeck == $tkey) {
                                                                                                                            echo "checked";
                                                                                                                        }
                                                                                                                    } ?>><?php echo $tval ?></label>
                        <?php } ?>
                    </div>
                    <div>
                        <label style="display: none;"><?php echo get_string('applicationperiod', 'local_management') ?></label>
                        <label>
                            <select name="periodtype">
                                <option value="">- 조건 -</option>
                                <option value="1" <?php echo ($periodtype == 1 ? 'selected':'') ?> >강좌등록일</option>
                                <option value="2" <?php echo ($periodtype == 2 ? 'selected':'') ?> >결제/입금일</option>
                            </select>
                        </label>
                        <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> ">
                        <span class="dash">~</span>
                        <input type="text" title="<?php echo get_string('time', 'local_management') ?> " name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?> ">
                    </div>
                    <div>
                        <label><?php echo get_string('isused', 'local_management') ?> </label>
                        <?php foreach ($isused_array as $iakey => $iaval) { ?>
                            <label class="mg-bt10"><input type="checkbox" name="isused[]" value="<?php echo $iakey ?>" <?php foreach ($isused as $ischeck) {
                                                                                                                            if ($ischeck == $iakey) {
                                                                                                                                echo "checked";
                                                                                                                            }
                                                                                                                        } ?>><?php echo $iaval ?></label>
                        <?php } ?>
                    </div>

                    <div>
                        <label><?php echo get_string('search', 'local_management') ?> </label>
                        <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('goods:searchtext2', 'local_management') ?> " class="search-text w100" />
                        <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>" />
                        <input type="button" class="" value="<?php echo get_string('reset', 'local_management') ?> " id="reset" />
                    </div>
                </form>
                <!--Search Area2 End-->


                <span><?php echo get_string('total', 'local_management', $count_courses) ?></span>
                <select name="perpage2" class="perpage2">
                    <?php
                    foreach ($perpage_array as $pa) {
                        $selected = '';
                        if ($pa == $perpage) {
                            $selected = 'selected';
                        }
                    ?>
                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                        <?php
                    }
                        ?>
                </select>
                <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="textbook_excel_down();" />
                <div style="float:right;">
                    <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('goodsregist', 'local_management'); ?>" onclick="javascript:location.href = 'form.php';" />
                </div>
                <table>
                    <thead>
                        <tr>
                            <th rowspan="2" scope="row" width="50px"><?php echo get_string('number', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="5%"><?php echo get_string('division', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="10%"><?php echo get_string('goodsname', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="7%"><?php echo get_string('goodscode', 'local_management') ?></th>
                            <th rowspan="2" width="10%"><?php echo get_string('originname', 'local_management') ?></th>
                            <th rowspan="2"><?php echo get_string('origincode', 'local_management') ?></th>
                            <th rowspan="2"><?php echo get_string('lecturecnt', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="6%"><?php echo get_string('price2', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="6%"><?php echo get_string('gross_sales', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="8%"><?php echo get_string('applicationperiod', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="6%"><?php echo get_string('timecreated', 'local_management') ?></th>
                            <th rowspan="2" scope="row" width="6%"><?php echo get_string('isused', 'local_management') ?></th>
                            <th rowspan="2"><?php echo get_string('sale', 'local_management') ?></th>
                            <th colspan="2"><?php echo get_string('completion2', 'local_management') ?></th>
                            <th rowspan="2"><?php echo get_string('notcompletion', 'local_management') ?></th>
                            <th rowspan="2" width="100px"><?php echo get_string('classroomentry', 'local_management') ?></th>
                            <th rowspan="2" width="100px"><?php echo get_string('studentregistration', 'local_management') ?></th>
                            <th rowspan="2" width="80px"><?php echo get_string('goods:copy', 'local_management') ?></th>
                        </tr>
                        <tr>
                            <th scope="row"><?php echo get_string('user:normal', 'local_management'); ?></th>
                            <th scope="row"><?php echo get_string('user:bop', 'local_management'); ?></th>
                        </tr>
                        <?php if ($count_courses) :
                            $total_sales = 0;
                            $total_course_sales = 0;
                            $total_normal_completion = 0;
                            $total_bop_completion = 0;
                            $total_not_completion = 0;
                            foreach ($courses as $course) {
                                $getstudentsql = "SELECT count(*) 
                                    FROM m_lmsdata_payment lp
                                    JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                                    JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                                    JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                                    JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                                    JOIN m_course  c ON c.id = ctx.instanceid 
                                    JOIN m_user u ON lp.userid = u.id
                                    JOIN m_lmsdata_user lu ON u.id = lu.userid 
                                    WHERE  c.id = :courseid2 and u.id not in (SELECT teacherid FROM {lmsdata_course_professor}  ) ";

                                $parentcourseid = $DB->get_field_sql('select lc.parentcourseid FROM {lmsdata_class} lc
                                                                        JOIN {course} mc ON lc.courseid = mc.id where mc.id = :id', array('id' => $course->courseid));

                                $ucount = "SELECT COUNT(*) ";

                                $usql = "FROM {user_enrolments} ue
                                    JOIN {enrol} e ON (e.id = ue.enrolid)
                                    JOIN {user} u ON ue.userid = u.id
                                    LEFT JOIN ( SELECT lp.userid,lc.courseid,lp.forced, lp.timecreated, lp.paytype, lc.reviewperiod FROM {lmsdata_class} lc 
                                                JOIN {lmsdata_payment} lp ON lc.id = lp.ref WHERE lc.courseid = $course->courseid group by lp.userid ) lcp ON lcp.userid = u.id
                                    LEFT JOIN {course_completions} cc ON cc.userid = u.id AND cc.course = e.courseid
                                    WHERE e.courseid = $course->courseid and u.id not in (SELECT teacherid FROM {lmsdata_course_professor} ) ";


                                if ($course->courseid) {
                                    $userlist_cnt = $DB->count_records_sql($ucount . $usql);
                                } else {
                                    $userlist_cnt = 0;
                                }


                                $getcompletesql = "SELECT count(*) 
                                    FROM m_lmsdata_payment lp
                                    JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                                    JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                                    JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                                    JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                                    JOIN m_course  c ON c.id = ctx.instanceid 
                                    JOIN m_user u ON lp.userid = u.id
                                    JOIN m_lmsdata_user lu ON u.id = lu.userid 
                                    JOIN {course_completions} cc ON cc.course = lc.courseid AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                    WHERE  c.id = :courseid2 AND lu.usergroup = :usergroup";

                                $getstudent = $DB->count_records_sql($getstudentsql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel1' => 50, 'archetype' => 'student'));

                                $getcomplete_rs = $DB->count_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => 50, 'archetype' => 'student', 'usergroup' => 'rs'));
                                $getcomplete_bp = $DB->count_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => 50, 'archetype' => 'student', 'usergroup' => 'bp'));

                                $period_total_sale_sql = '';
                                if ($learningstart && $periodtype == 2) {
                                    $period_total_sale_sql = "lp.timecreated >= {$learningstart} AND ";
                                }
                                if ($learningend && $periodtype == 2) {
                                    $period_total_sale_sql = "lp.timecreated <= {$learningend} AND ";
                                }
                                $total_sale_sql = "SELECT sum(lc.price) as total
                                    FROM {lmsdata_payment} lp
                                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                                    WHERE {$period_total_sale_sql} lc.courseid = '{$course->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
                                
                                $total_sales += $DB->get_record_sql($total_sale_sql)->total;

                                $total_course_sales += $userlist_cnt;
                                $total_normal_completion += $getcomplete_rs;
                                $total_bop_completion += $getcomplete_bp;
                                $total_not_completion += $userlist_cnt - ($getcomplete_rs + $getcomplete_bp);
                            }
                        ?>
                            <tr>
                                <th><?php echo get_string('total_sum', 'local_management'); ?></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><?php echo number_format($total_sales); ?></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><?php echo $total_course_sales; ?></th>
                                <th><?php echo $total_normal_completion; ?></th>
                                <th><?php echo $total_bop_completion; ?></th>
                                <th><?php echo $total_not_completion; ?></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        <?php endif; ?>
                    </thead>
                    <?php if ($count_courses === 0) { ?>
                        <tr>
                            <td colspan="19"><?php echo get_string('delivery:no_data', 'local_management') ?></td>
                        </tr>
                        <?php
                    } else {
                        $startnum = $count_courses - (($currpage - 1) * $perpage);
                        foreach ($courses as $course) {
                        ?>
                            <tr>
                                <td><?php echo $startnum--; ?></td>
                                <td>
                                    <?php
                                    if ($course->type == 1) {
                                        echo get_string('course', 'local_management');
                                    } else if ($course->type == 2) {
                                        echo get_string('book', 'local_management');
                                    } else {
                                        echo get_string('course', 'local_management') . '+' . get_string('book', 'local_management');
                                    }
                                    ?>
                                </td>
                                <td><a href='./form.php?id=<?php echo $course->id ?>&array=<?php echo urlencode(json_encode($get_array[1])) ?>'><?php echo $course->vi_title . '<br>' . $course->title ?></a></td>
                                <td><?php echo $course->code ?></td>
                                <?php
                                $originname = '';
                                $origincode = '';
                                if ($course->type == 1) {
                                    $origincode = $course->lcocode;
                                    switch (current_language()) {
                                        case 'ko':
                                            $originname = $course->lconame;
                                            break;
                                        case 'en':
                                            $originname = $course->en_lconame;
                                            break;
                                        case 'vi':
                                            $originname = $course->vi_lconame;
                                            break;
                                    }
                                } else if ($course->type == 2) {
                                    $origincode = $course->ltcode;
                                    switch (current_language()) {
                                        case 'ko':
                                            $originname = $course->lttitle;
                                            break;
                                        case 'en':
                                            $originname = $course->en_lttitle;
                                            break;
                                        case 'vi':
                                            $originname = $course->vi_lttitle;
                                            break;
                                    }
                                } else if ($course->type == 3) {
                                    //$originname = $course->lconame.'<br>'.$course->lttitle;
                                    $origincode = $course->lcocode . '<br>' . $course->ltcode;
                                    switch (current_language()) {
                                        case 'ko':
                                            $originname = $course->lconame . '<br>' . $course->lttitle;
                                            break;
                                        case 'en':
                                            $originname = $course->en_lconame . '<br>' . $course->en_lttitle;
                                            break;
                                        case 'vi':
                                            $originname = $course->vi_lconame . '<br>' . $course->vi_lttitle;
                                            break;
                                    }
                                }
                                ?>
                                <td><?php echo $originname ?></td>
                                <td><?php echo $origincode ?></td>
                                <td>
                                    <?php
                                    //강의수
                                    // echo $DB->count_records_sql("select count(*) from {course_sections} where course=:mcid and section != 0 ORDER BY section ASC", array('mcid' => $course->courseid));
                                    echo $course->lcolecturecnt ? $course->lcolecturecnt  : '-';
                                    ?>
                                </td>
                                <td><?php echo number_format($course->price) ?></td>
                                <td>
                                    <?php 
                                        $period_total_sale_sql = '';
                                        if ($learningstart && $periodtype == 2) {
                                            $period_total_sale_sql = "lp.timecreated >= {$learningstart} AND ";
                                        }
                                        if ($learningend && $periodtype == 2) {
                                            $period_total_sale_sql = "lp.timecreated <= {$learningend} AND ";
                                        }
                                        $total_sale_sql = "SELECT sum(lc.price) as total
                                            FROM {lmsdata_payment} lp
                                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                                            WHERE {$period_total_sale_sql} lc.courseid = '{$course->courseid}' AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))";
                                        
                                        echo number_format($DB->get_record_sql($total_sale_sql)->total);
                                    ?>
                                </td>
                                <td><?php echo date('Y.m.d', $course->learningstart) ?> <br /> ~ <?php echo date('Y.m.d', $course->learningend) ?></td>
                                <td><?php echo date('Y.m.d', $course->timecreated) ?></td>
                                <td><?php echo ($course->isused == 0) ? get_string('used', 'local_management') : get_string('notused', 'local_management'); ?></td>
                                <?php
                                $getstudentsql = "SELECT count(*) 
                                        FROM m_lmsdata_payment lp
                                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                                        JOIN m_course  c ON c.id = ctx.instanceid 
                                        JOIN m_user u ON lp.userid = u.id
                                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                                        WHERE  c.id = :courseid2 and u.id not in (SELECT teacherid FROM {lmsdata_course_professor}  ) ";

                                $parentcourseid = $DB->get_field_sql('select lc.parentcourseid FROM {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id where mc.id = :id', array('id' => $course->courseid));

                                $ucount = "SELECT COUNT(*) ";

                                $usql = "FROM {user_enrolments} ue
                JOIN {enrol} e ON (e.id = ue.enrolid)
                JOIN {user} u ON ue.userid = u.id
                LEFT JOIN ( SELECT lp.userid,lc.courseid,lp.forced, lp.timecreated, lp.paytype, lc.reviewperiod FROM {lmsdata_class} lc 
                            JOIN {lmsdata_payment} lp ON lc.id = lp.ref WHERE lc.courseid = $course->courseid group by lp.userid ) lcp ON lcp.userid = u.id
                LEFT JOIN {course_completions} cc ON cc.userid = u.id AND cc.course = e.courseid
                WHERE e.courseid = $course->courseid and u.id not in (SELECT teacherid FROM {lmsdata_course_professor} ) ";


                                if ($course->courseid) {
                                    $userlist_cnt = $DB->count_records_sql($ucount . $usql);
                                } else {
                                    $userlist_cnt = 0;
                                }


                                $getcompletesql = "SELECT count(*) 
                                        FROM m_lmsdata_payment lp
                                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                                        JOIN m_course  c ON c.id = ctx.instanceid 
                                        JOIN m_user u ON lp.userid = u.id
                                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                                        JOIN {course_completions} cc ON cc.course = lc.courseid AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                        WHERE  c.id = :courseid2 AND lu.usergroup = :usergroup";

                                $getstudent = $DB->count_records_sql($getstudentsql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel1' => 50, 'archetype' => 'student'));

                                $getcomplete_rs = $DB->count_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => 50, 'archetype' => 'student', 'usergroup' => 'rs'));
                                $getcomplete_bp = $DB->count_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => 50, 'archetype' => 'student', 'usergroup' => 'bp'));
                                ?>
                                <td><?php echo ($course->type != 2) ? number_format($userlist_cnt) : '-'; ?> </td>
                                <td><?php echo ($course->type != 2) ? number_format($getcomplete_rs) : '-' ?></td>
                                <td><?php echo ($course->type != 2) ? number_format($getcomplete_bp) : '-' ?></td>
                                <td><?php echo ($course->type != 2) ? number_format($userlist_cnt - ($getcomplete_rs + $getcomplete_bp)) : '-' ?></td>


                                <td><?php if (($course->type != 2)) { ?><input type="button" class="normal_btn" value="<?php echo get_string('classroomentry', 'local_management') ?>" onclick="window.open('<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->courseid; ?>')" /><?php } else {
                                                                                                                                                                                                                                                                                            echo '-';
                                                                                                                                                                                                                                                                                        } ?></td>
                                <td><?php if (($course->type != 2)) { ?><input type="button" class="normal_btn" data-course="<?php echo $course->courseid; ?>" value="<?php echo get_string('studentregistration', 'local_management') ?>" onclick="enrol_user(this);" /><?php } else {
                                                                                                                                                                                                                                                                            echo '-';
                                                                                                                                                                                                                                                                        } ?></td>
                                <td><?php if (($course->type != 2)) { ?><input type="button" class="normal_btn" value="<?php echo get_string('goods:copy', 'local_management') ?>" onclick="javascript:location.href = 'form.php?id=<?php echo $course->id; ?>&mod=copy';" /><?php } else {
                                                                                                                                                                                                                                                                                echo '-';
                                                                                                                                                                                                                                                                            } ?></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </table>
                <!--Table End-->

                <div id="btn_area">
                    <div style="float:right;">
                        <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('goodsregist', 'local_management'); ?>" onclick="javascript:location.href = 'form.php';" />
                    </div>
                </div>
                <?php
                if (($count_courses / $perpage) > 1) {
                    print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
                }

                $query_string = '';
                if (!empty($excel_params)) {
                    $query_array = array();
                    foreach ($excel_params as $key => $value) {
                        $query_array[] = urlencode($key) . '=' . urlencode($value);
                    }
                    $query_string = '?' . implode('&', $query_array);
                }
                ?>
            </div>
            <!--Content End-->
        </div>
        <!--Contents End-->
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script type="text/javascript">
    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    $(document).ready(function() {
        $(".perpage2").change(function() {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=course_search]").submit();
        });
        $('#reset').click(function() {
            $('input[name=learningstart]').val('');
            $('input[name=learningend]').val('');
            $('input[name=searchtext]').val('');
            $("input:checkbox[name='type[]']").attr("checked", false);
            $("input:checkbox[name='isused[]']").attr("checked", false);
            $("form[id=course_search]").submit();
        });
    });

    /**
     * 수강생 엑셀 등록 팝업
     * @param element button
     * @returns      
     * */
    function enrol_user(button) {
        var courseid = $(button).data('course');
        var tag = $("<div id='course_enrol_popup'></div>");
        $.ajax({
            url: '<?php echo $CFG->wwwroot . '/local/management/goods/enrol_course_excel.php'; ?>',
            method: 'POST',
            data: {
                id: courseid
            },
            success: function(data) {
                tag.html(data).dialog({
                    title: '<?php echo get_string('studentexcel', 'local_management') ?>',
                    modal: true,
                    width: 600,
                    resizable: false,
                    buttons: [{
                        id: 'close',
                        text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                        disable: true,
                        click: function() {
                            $(this).dialog("close");
                        }
                    }],
                    open: function() {
                        $(".ui-dialog-buttonset").prepend('<input type="button" class="blue_btn" value="<?php echo get_string('add3', 'local_lmsdata'); ?>" onclick="enrol_course_excel_submit()"/>')
                    },
                    close: function() {
                        $('#frm_enrol_excel').remove();
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }


    start_datepicker_c('', '');
    end_datepicker_c('', '');

    function textbook_excel_down() {
        var form = $('#course_search');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
        return false;
    }
</script>