<?php
require_once(__DIR__ . '/../../../config.php');
require_once dirname(dirname (__FILE__)).'/lib.php';
require_once $CFG->dirroot.'/lib/coursecatlib.php';

$courseid = required_param('id',  PARAM_INT);
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

?>
<div id="popup_content">
    <form name="frm_enrol_excel" id="frm_enrol_excel" class="search_area" method="post" enctype="multipart/form-data">
        <input type="file" name="enrol_excel" size="50"/>
        <p><strong><a style="color:red;" href="./download.php" alt="Sample" alt="Sample">[Sample]</a></strong><?php echo get_string('goods:form','local_management')?>.</p>
    </form>
</div><!--Content End-->
<script type="text/javascript">
    function enrol_course_excel_submit() {
        if($.trim($("input[name='enrol_excel']").val()) != '') {
             var filename = $.trim($("input[name='enrol_excel']").val());
             var extension = filename.replace(/^.*\./, ''); 
             if(extension == filename) {
                 extension = "";
             } else {
                 extension = extension.toLowerCase();
             }
             
             if($.inArray( extension, [ "xls", "xlsx" ] ) == -1) {
                 alert("<?php echo get_string('onlyexcell','local_lmsdata'); ?>");
                 return false;
             } else {
                var form = $('#frm_enrol_excel');
                //FormData
                var formData = new FormData(form[0]);
                formData.append('courseid', <?php echo $courseid;?>);
                $.ajax({
                    url: '<?php echo $CFG->wwwroot.'/local/management/goods/enrol_course_excel.execute.php'; ?>',
                    processData: false,
                    contentType: false,
                    data: formData,
                    dataType: 'json',
                    type: 'POST',
                    success: function(result){
                        console.log(result);
                        $('#frm_enrol_excel').remove();
                        $('#course_enrol_popup').dialog('destroy').remove();
                        alert('<?php echo get_string('goods:usercomplete','local_management')?>.(<?php echo get_string('insert','local_management')?>:' + result.success + ', <?php echo get_string('goods:overlap','local_management')?>:' + result.begin + ', <?php echo get_string('goods:failure','local_management')?>:' + (result.empty + result.notobject) + ')');
                        location.reload();
                    }
                });
             }
        } else {
            alert('<?php echo get_string('goods:file','local_management')?>');
        }
    }
    
</script>    