<?php

function searchCourseList($search = "", $page=1){
    global $DB;
    $sql_where_query = "  WHERE lc.isused = 0  ";
    //검색용 파라미터
    if (!empty($search)) {
        $sql_where_search[] = $DB->sql_like('lc.coursename', ':coursename');
        $params['coursename'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('lc.vi_coursename', ':vi_coursename');
        $params['vi_coursename'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('lc.en_coursename', ':en_coursename');
        $params['en_coursename'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('lc.coursecd', ':coursecd');
        $params['coursecd'] = '%' . $search . '%';
        if($sql_where_search){
            $sql_where[] = '('.implode(' or ', $sql_where_search).')';
        }
    }
    if (!empty($sql_where)) {
        $sql_where_query .= ' AND ' . implode(' and ', $sql_where);
    } 

    $orderby = ' order by lc.timecreated DESC, lc.id DESC ';
    $sql_select = 'SELECT 
                    mc.id, mc.category, ca.name ca1name, ca2.name ca2name, mc.fullname, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber,
                    lc.id lcid, lc.mainexposure, lc.exposuresort, lc.classobject, lc.coursename,lc.vi_coursename, lc.en_coursename, lc.coursecd, lc.classtype, lc.price, lc.timecreated, lc.isused, lc.coursecd ';
    $count_sql = 'SELECT count(mc.id) ';

    $sql_from = ' FROM {course} mc 
                        JOIN {lmsdata_course} lc ON lc.courseid = mc.id 
                        JOIN {course_categories} ca ON ca.id = mc.category 
                        left JOIN {course_categories} ca2 ON ca.parent = ca2.id';

    $return = new stdClass();
    $return->total_count = $DB->count_records_sql($count_sql . $sql_from . $sql_where_query, $params);
    //$return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }

    $return->list2 = $DB->get_records_sql($sql_select . $sql_from . $sql_where_query . $orderby, $params, ($page - 1) * 10, 10);
    $return->num = $return->total_count - (($page - 1) * $perpage);
    
    return $return;
}

function searchCourse($id){
    global $DB;

    $sql_select = 'SELECT 
                            mc.id, mc.category, ca.name ca1name, ca2.name ca2name, mc.fullname, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber,
                            lc.id lcid, lc.mainexposure, lc.exposuresort, lc.classobject, lc.coursename,lc.en_coursename, lc.vi_coursename, lc.coursecd, lc.classtype, lc.price, lc.timecreated, lc.isused, lc.courseperiod, lc.reviewperiod ';

    $sql_from = ' FROM {course} mc 
                        JOIN {lmsdata_course} lc ON lc.courseid = mc.id 
                        JOIN {course_categories} ca ON ca.id = mc.category 
                        left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                    WHERE mc.id = :id';

    $courses = $DB->get_record_sql($sql_select . $sql_from , array("id"=>$id));

    return $courses;
}

function searchTextbookList($search = "", $page=1){
    global $DB;
    $sql_where_query = ' WHERE isused = 0 ';
     //검색용 파라미터
    if (!empty($search)) {
        $sql_where_search[] = $DB->sql_like('title', ':title');
        $params['title'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('vi_title', ':vi_title');
        $params['vi_title'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('en_title', ':en_title');
        $params['en_title'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('code', ':code');
        $params['code'] = '%' . $search . '%';
        if($sql_where_search){
            $sql_where[] = '('.implode(' or ', $sql_where_search).')';
        }
    }
    
    if (!empty($sql_where)) {
        $sql_where_query .= ' AND ' . implode(' and ', $sql_where);
    }
    $orderby = ' order by timecreated desc , id desc ';
    $sql_select = 'select * ';
    $count_sql = 'SELECT count(id) ';

    $sql_from = ' from {lmsdata_textbook}';

    $return = new stdClass();
    $return->total_count = $DB->count_records_sql($count_sql . $sql_from . $sql_where_query, $params);
    //$return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }

    $return->list2 = $DB->get_records_sql($sql_select . $sql_from . $sql_where_query . $orderby, $params, ($page - 1) * 10, 10);
    $return->num = $return->total_count - (($page - 1) * $perpage);
    
    return $return;
}

function searchTextbook($id){
    global $DB;

    $sql_select = 'select * ';
    $sql_from = ' from {lmsdata_textbook} WHERE id=:id';

    $courses = $DB->get_record_sql($sql_select . $sql_from , array("id"=>$id));
    
   
    return $courses;
}

function searchClassList($search = "",$type=1,$id='', $page = 1, $perpage = 10){
    global $DB;
    $now = time();
     //검색용 파라미터
    if (!empty($search)) {
        $sql_where_search[] = $DB->sql_like('lc.title', ':title');
        $params['title'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('lc.vi_title', ':vi_title');
        $params['vi_title'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('lc.en_title', ':en_title');
        $params['en_title'] = '%' . $search . '%';
        $sql_where_search[] = $DB->sql_like('lc.code', ':code');
        $params['code'] = '%' . $search . '%';
        if($sql_where_search){
            $sql_where_array[] = '('.implode(' or ', $sql_where_search).')';
        }
    }

    $sql_where_array[] = 'lc.deleted = :deleted';
    $params['deleted'] = 0;
    $sql_where_array[] = 'lc.isused = :isused';
    $params['isused'] = 0;
    $sql_where = " WHERE lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0 ";    
    
    if (!empty($sql_where_array)) {
        $sql_where .=  ' AND ' .implode(' and ', $sql_where_array);
    } else {
        $sql_where .= '';
    }
    if($type == 1){
        $sql_where .= " AND (lc.type=:type or lc.type=:type2)";
        $params['type'] = 1;
        $params['type2'] = 3;
        $sql_from = ' from {lmsdata_class} lc   
                      JOIN {course} mc ON lc.courseid = mc.id
                      JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                      JOIN {course} mc2 ON lco.courseid = mc2.id
                      JOIN {course_categories} ca ON ca.id = mc2.category
                      left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                    ';
    }else{
        $sql_where .= " AND lc.type=:type ";
        $params['type'] = 2;
        $sql_from = ' from {lmsdata_class} lc   
                    JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id ';
    }
    if(!empty($id)){
        $sql_where .= " AND lc.id NOT IN ($id)";
    }
    
    $orderby = ' order by lc.timecreated desc , lc.id desc ';
    $sql_select = 'select lc.* ';
    $count_sql = 'SELECT count(lc.id) ';

    $return = new stdClass();
    $return->total_count = $DB->count_records_sql($count_sql . $sql_from . $sql_where, $params);
    //$return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }
    
    $return->list2 = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $params, ($page - 1) * 10, 10);
    
    foreach ($return->list2 as $key => $val) {
        switch (current_language()) {
            case 'ko' :
                $return->list2[$key]->lang_title = $val->title;
                break;
            case 'en' :
                $return->list2[$key]->lang_title = $val->en_title;
                break;
            case 'vi' :
                $return->list2[$key]->lang_title = $val->vi_title;
                break;
        }
    }
    
    $return->num = $return->total_count - (($page - 1) * $perpage);
    
    return $return;
}

function searchClass($id){
     global $DB;

    $sql_select = 'select * ';
    $sql_from = ' from {lmsdata_class} WHERE id=:id';

    $courses = $DB->get_record_sql($sql_select . $sql_from , array("id"=>$id));
    
   
    return $courses;
}

function searchBadge($search = "", $page = 1, $perpage = 10) {
    global $DB;
    $lang = current_language();
    $sql = "SELECT lb.id 
           ,lb.title_$lang name
           ,lb.timecreated
           ,lb.used
           ,lb.code
           ,lbc.title_$lang cname ";
    $from = " FROM {lmsdata_badge} lb 
     JOIN {lmsdata_badge_category} lbc on lb.category = lbc.id  
     ";
    $where = '';
    $param = array();
    if ($search) {
        $addwhere[] = "( lb.title_vi LIKE :searchtext1  or lb.title_ko LIKE :searchtext2  or lb.title_en LIKE :searchtext3 or lb.code LIKE :searchtext4 ) ";
        $param['searchtext1'] = "%$search%";
        $param['searchtext2'] = "%$search%";
        $param['searchtext3'] = "%$search%";
        $param['searchtext4'] = "%$search%";
    }
    if (!empty($addwhere)) {
        $where = " WHERE " . implode(' AND ', $addwhere);
    }
    $order = "ORDER BY lb.id DESC ";
    $return =new stdClass();
    $return->list = $DB->get_records_sql($sql . $from . $where . $order, $param ,($page - 1) * 10, 10);
    $return->total_count = $DB->count_records_sql("SELECT count(lb.id)" . $from . $where, $param);

    return $return;
}
function getBadge($id) {
    global $DB;
    $lang = current_language();
    $sql = "SELECT lb.id 
           ,lb.title_$lang name
           ,lb.timecreated
           ,lb.used
           ,lb.code";
    $from = " FROM {lmsdata_badge} lb  where id = :id ";
    $param = array();
    $param['id'] = $id;

    $return= $DB->get_record_sql($sql . $from , $param );


    return $return;
}

function get_skill($id = 0,$text =''){
    global $DB,$USER;
    
    if($id == 0){
    $sql = " select id,name  from {vi_skills} ";
    $where = " where name like ? ";
    $params['name'] = "%$text%";
    }
    $skills = $DB->get_records_sql($sql.$where,$params);
    return $skills;
    
}