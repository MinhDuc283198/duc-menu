<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');

global $DB, $PAGE;

$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$courseid = required_param('courseid', PARAM_INT);
$filename = $_FILES['enrol_excel']['name'];
$filepath = $_FILES['enrol_excel']['tmp_name'];

$role_arr = $DB->get_records('role', null, '', 'shortname, id, name');
$role_type = array('t' => 'editingteacher', 's' => 'student');

$course = $DB->get_record('course', array('id' => $courseid));
$lmsdataClass = $DB->get_record('lmsdata_class', array('courseid' => $courseid));

// 강의의 usertypecode 확인(classobject)
$lmsdataCoursesql = 'SELECT lco.*, ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber 
                                    FROM {lmsdata_course} lco
                                    JOIN {course_categories} ca ON ca.id = lco.detailprocess 
                                    LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id
                                    WHERE lco.id = :id';
$lmsdataCourse = $DB->get_record_sql($lmsdataCoursesql, array('id' => $lmsdataClass->parentcourseid));

$enrol = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $course->id));
$manager = new course_enrolment_manager($PAGE, $course);

$instances = $manager->get_enrolment_instances();
$instance = $instances[$enrol->id];

$plugins = $manager->get_enrolment_plugins();
$plugin = $plugins[$instance->enrol];

$context = context_course::instance($course->id, MUST_EXIST);

//엑셀파일 read
$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
}

$maxRow = $objWorksheet->getHighestRow();


$returnvalue = new stdClass();
$returnvalue->success = 0;
$returnvalue->begin = 0;
$returnvalue->empty = 0;
$returnvalue->notobject = 0;
error_log('[START:]' . date('Y-m-d H:i:s') . "]\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
$role_code = 's';
for ($i = 2; $i <= $maxRow; $i++) {
//    $no = trim($objWorksheet->getCell('A' . $i)->getValue());      //no - 의미없는 값이어서 제외
    //$fullname = $objWorksheet->getCell('B' . $i)->getValue();                     //이름
    $username = trim($objWorksheet->getCell('A' . $i)->getValue());               //위탁코드 - user테이블의 username
    //$role_code = trim(strtolower($objWorksheet->getCell('D' . $i)->getValue()));   //역할(t:editingteacher, s:student)
    //엑셀값이 비어있나 체크
    if (!empty($username) && $username != '') {

        //username으로 사용자가 생성되어있나 체크
        $user = $DB->get_record('user', array('username' => $username));
        if (!empty($user)) {
            $role_shortname = $role_type[$role_code];
            $roleid = $role_arr[$role_shortname]->id;
            $available = 1;

            // 대상이 아닌 경우 등록되지 않도록 설정
            if ($available > 0) {
                //등록되는 roleid 체크
                if (!empty($roleid)) {
                    //첫수강
                    if(!$ues = $manager->get_user_enrolments($user->id)){
                        if ($role_shortname != 'student') {
                            $startdate = 0;
                            $enddate = 0;
                        } else {
                            ///학습시작일/종료일을 기수관리 교육기간에서 가져옴  
                            $startdate = $start = strtotime(date("Y-m-d")." 00:00:00");
                            $enddate = $end = $start + ( ($lmsdataClass->courseperiod+$lmsdataClass->reviewperiod) * 86400) - 1;
                        }
                        $plugin->enrol_user($instance, $user->id, $roleid, $startdate, $enddate, null);
                        
                        
                        //등록 로그 남김 - NEW:코스id:사용자이름:사용자ID:role이름
                        error_log("\t" . '-NEW:' . $course->id . ':' . $user->lastname . ':' . $user->username . ':' . $role_shortname . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
                        $returnvalue->success++;
                    } else {
                        
                        //재수강
                        //$enrol = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $course->id));
                        $startdate = 0;
                        $enddate = 0;
                        
                        if ($role_shortname == 'student') {
                            $startdate = $start = strtotime(date("Y-m-d")." 00:00:00");
                            $enddate = $end = $start + ( ($lmsdataClass->courseperiod+$lmsdataClass->reviewperiod) * 86400) - 1;
                            //time() + ( ($lmsdataClass->courseperiod+$lmsdataClass->reviewperiod) * 86400);
                        }
                        
                        foreach($ues as $ue){
                            $ue2 = $DB->get_record('user_enrolments', array('id' => $ue->id), '*', MUST_EXIST);
                        }
                        $course_modules = $DB->get_records('course_modules', array('course' => $courseid));
                        
                        if($course_modules){
                            foreach($course_modules as $cm){
                                $DB->delete_records('course_modules_completion', array('coursemoduleid' => $cm->id, 'userid' => $user->id));
                            }
                        }
                        
                        //quiz
                        $quizlist = $DB->get_records('quiz', array('course'=>$courseid));
                        if($quizlist){
                            foreach($quizlist as $ql){
                                $DB->delete_records('quiz_attempts', array('quiz' => $ql->id, 'userid' => $user->id));
                            }
                        }

                        //okmedia
                        $okmedialist = $DB->get_records('okmedia', array('course'=>$courseid));
                        if($okmedialist){
                            foreach($okmedialist as $ol){
                                $DB->delete_records('okmedia_track', array('okmediaid' => $ol->id, 'userid' => $user->id));
                                $DB->delete_records('okmedia_playtime', array('okmediaid' => $ol->id, 'userid' => $user->id));
                            }
                        }

                        //수강완료
                        $DB->delete_records('course_completions', array('course' => $courseid, 'userid' => $user->id));
                        $DB->delete_records('course_completion_crit_compl', array('course' => $courseid, 'userid' => $user->id));
                        cache::make('core', 'completion')->purge();
                        
                        $edit_data = new stdClass();
                        $edit_data->status = $ue2->status;
                        $edit_data->timestart = $startdate;
                        $edit_data->timeend = $enddate;
                        $edit_data->ue = $ue2->id;
                        $manager->edit_enrolment($ue2, $edit_data);
                        error_log("\t" . $available.'-BEING:' . $course->id . ':' . $user->lastname . ':' . $user->username . ':' . $role_shortname . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
                        $returnvalue->begin++;
                    }
                    //결제테이블 추가
                    $lmsdata = new stdClass();
                    $lmsdata->userid = $user->id;
                    $lmsdata->status = 1;
                    $lmsdata->type = 1;
                    $lmsdata->ref = $lmsdataClass->id;
                    $lmsdata->paytype = 1;
                    $lmsdata->timecreated = time();
                    $lmsdata->timemodified = time();
                    $lmsdata->forced = 1;
                    $payid = $DB->insert_record('lmsdata_payment', $lmsdata);
                    
                    if($payid){
                        //수강기간테이블 추가
                        $lmsdata3 = new stdClass();
                        $lmsdata3->payid = $payid;
                        $lmsdata3->userid = $user->id;
                        $lmsdata3->reviewperiod = $lmsdataClass->reviewperiod;
                        $lmsdata3->starttime = $startdate;
                        $lmsdata3->endtime = $enddate;
                        $lmsdata3->timecreated = time();
                        $lmsdata3->timemodified = time();
                        $DB->insert_record('lmsdata_course_period', $lmsdata3);

                        if($lmsdataClass->type == 3) {
                            $lmsdatauser = $DB->get_record('lmsdata_user', array('userid'=>$user->id));
                            $lmsdata2 = new stdClass();
                            $lmsdata2->payid = $payid;
                            $lmsdata2->bookid = $lmsdataClass->id;
                            $lmsdata2->address = $lmsdatauser->address.' '.$lmsdatauser->address_detail;
                            $lmsdata2->cell = $user->phone1;
                            $lmsdata2->timecreated = time();
                            $lmsdata2->timemodified = time();
                            $DB->insert_record('lmsdata_productdelivery', $lmsdata2);
                        }
                    }
                }
                
            } else {
                //수강대상이 아님 - NOTOBJECT:코스id:사용자이름:사용자ID:role이름
                error_log("\t" . '-NOTOBJECT:' . $course->id . ':' . $user->lastname . ':' . $user->username . ':' . $role_shortname . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
                $returnvalue->notobject++;
            }
        } else {
            //username이 맞는 사용자가 없음 - EMPTY:코스id:사용자ID
            error_log("\t" . '-EMPTY:' . $course->id . ':' . $username . "\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');
            $returnvalue->empty++;
        }
    }
}
error_log('[END:]' . date('Y-m-d H:i:s') . "]\n\n\n", 3, $CFG->dirroot . '/chamktu/manage/enrol_excel.log');

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
?>

