<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/goods/lib.php');
require_once($CFG->dirroot . '/local/management/lib/paging.php');

$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'coruseoriginlist':
        $data = required_param_array('data', PARAM_RAW);
        //강좌
        $list = searchCourseList($data["search"],$data['page']);
        $i = 0;
        foreach($list->list2 as $key=>$val){
            unset($list->list2[$key]);
            $new_key = $i;
            $list->list[$new_key] = $val;
            $i++;
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->paging = pop_print_paging_navbar_script($list->total_count, $data['page'], 10, 'javascript:local_management_search_course_origin(:page);');
        echo json_encode($rvalue);
        break;
    case 'bookoriginlist' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchTextbookList($data["search"], $data['page']);
        $i = 0;
        foreach($list->list2 as $key=>$val){
            unset($list->list2[$key]);
            $new_key = $i;
            $list->list[$new_key] = $val;
            $i++;
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->paging = pop_print_paging_navbar_script($list->total_count, $data['page'], 10, 'javascript:local_management_search_book_origin(:page);');
        echo json_encode($rvalue);
        break;
    case 'add_course_origin':
        $data = required_param_array('data', PARAM_RAW);
        $list = searchCourse($data["originid"]);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'add_book_origin' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchTextbook($data["originid"]);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'rtextbooklist' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchClassList($data["search"],2,$data['id'],$data['page']);
        $i = 0;
        foreach($list->list2 as $key=>$val){
            unset($list->list2[$key]);
            $new_key = $i;
            $list->list[$new_key] = $val;
            $i++;
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->paging = pop_print_paging_navbar_script($list->total_count, $data['page'], 10, 'javascript:local_management_search_rcourse(:page);');
        echo json_encode($rvalue);
        break;
    case 'add_rtextbook' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchClass($data["textbookid"]);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'rcourselist' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchClassList($data["search"],1,$data['id'],$data['page']);
        $i = 0;
        foreach($list->list2 as $key=>$val){
            unset($list->list2[$key]);
            $new_key = $i;
            $list->list[$new_key] = $val;
            $i++;
        }
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->paging = pop_print_paging_navbar_script($list->total_count, $data['page'], 10, 'javascript:local_management_search_rcourse(:page);');
        echo json_encode($rvalue);
        break;
    case 'add_rcourse' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchClass($data["courseid"]);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'badgelist' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchBadge($data["search"],$data['page']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'add_badge' :
        $data = required_param_array('data', PARAM_RAW);
        $list = searchBadge($data["search"],$data['page']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        echo json_encode($rvalue);
        break;
    case 'get_skill' :
        $data = required_param_array('data', PARAM_RAW);
        $list = get_skill($data["id"],$data['txt']);
        $rvalue = new stdClass();
        $rvalue->info =$list;
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();