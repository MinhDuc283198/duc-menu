<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/local/management/textbook/lib.php');


$type = optional_param_array('type',array(), PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);
$isused = optional_param_array('isused',array(), PARAM_RAW);
$searchtext = optional_param('searchtext', '', PARAM_RAW);


if($learningstart_str){
    $learningstart = str_replace(".", "-", $learningstart_str); 
    $learningstart = strtotime($learningstart);
}
if($learningend_str){
    $learningend = str_replace(".", "-", $learningend_str); 
    $learningend = strtotime($learningend);
}
if($learningstart){
    $sql_where[] = " a.learningstart <= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if($learningend){
    $sql_where[] = " a.learningend >= :learningend ";
    $params['learningend'] = $learningend;
}

if($type){
    $type1 = '';
    $type2 = '';
    $type3 ='';
    foreach($type as $tkey => $tval){
        if($tval == 1){
            $type1 = ' checked';
        }else if($tval == 2){
            $type2 = ' checked';
        }else if($tval == 3){
            $type3 = ' checked';
        }
        $sql_where_type[] = " a.type = :type".$tkey;
        $params['type'.$tkey] = $tval;
    }
    if (!empty($sql_where_type)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_type) . ')';
    } else {
        $sql_where[] = '';
    }
}
if($isused){
    $isused0 = '';
    $isused1 = '';
    foreach($isused as $iskey => $isval){
        if($isval == 0){
            $isused0 = ' checked';
        }else if($isval == 1){
            $isused1 = ' checked';
        }
        $sql_where_isused[] = " a.isused = :isused".$iskey;
        $params['isused'.$iskey] = $isval;
    }
    if (!empty($sql_where_isused)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
    } else {
        $sql_where[] = '';
    }
}

if (!empty($searchtext)) {
    $sql_where[] = $DB->sql_like('a.title', ':title');
    $params['title'] = '%' . $searchtext . '%';
    $sql_where[] = $DB->sql_like('a.vi_title', ':vi_title');
    $params['vi_title'] = '%' . $searchtext . '%';
    $sql_where[] = $DB->sql_like('a.en_title', ':en_title');
    $params['en_title'] = '%' . $searchtext . '%';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$orderby = ' order by id desc ';

$sql_select = 'select *';
$sql = " from (select lc.*, lco.coursename as lconame,lco.en_coursename as en_lconame,lco.vi_coursename as vi_lconame, lco.coursecd as lcocode, '' as lttitle,'' as en_lttitle,'' as vi_lttitle, '' as ltcode, mc.id as mcid, '' as lcolecturecnt   
from m_lmsdata_class lc
JOIN m_course mc ON lc.courseid = mc.id
JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
JOIN m_course mc2 ON lco.courseid = mc2.id
JOIN m_course_categories ca ON ca.id = mc2.category
left JOIN m_course_categories ca2 ON ca.parent = ca2.id
WHERE lc.type = 1 
UNION
select lc.* , '' as lconame,'' as en_lconame, '' as vi_lconame, '' as lcocode, lt.title as lttitle,lt.en_title as en_lttitle,lt.vi_title as vi_lttitle, lt.code as ltcode, '' as mcid, '' as lcolecturecnt   
from m_lmsdata_class lc
JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
WHERE lc.type = 2 
UNION
select lc.* , lco.coursename as lconame,lco.en_coursename as en_lconame,lco.vi_coursename as vi_lconame, lco.coursecd as lcocode, lt.title as lttitle,lt.en_title as en_lttitle,lt.vi_title as vi_lttitle, lt.code as ltcode, mc.id as mcid, lco.lecturecnt as lcolecturecnt   
from m_lmsdata_class lc
JOIN m_lmsdata_textbook lt ON lc.bookid = lt.id
JOIN m_course mc ON lc.courseid = mc.id
JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id
JOIN m_course mc2 ON lco.courseid = mc2.id
JOIN m_course_categories ca ON ca.id = mc2.category
left JOIN m_course_categories ca2 ON ca.parent = ca2.id
WHERE lc.type = 3) a $sql_where";

$courses = $DB->get_records_sql($sql_select.$sql  . $orderby, $params);


    $fields = array(
        get_string('number', 'local_management'),
        get_string('division', 'local_management'),
        get_string('goodsname', 'local_management'),
        get_string('goodscode', 'local_management'),
        get_string('originname','local_management'),
        get_string('origincode','local_management'),
        get_string('lecturecnt','local_management'),
        get_string('price2', 'local_management'),
        get_string('applicationperiod', 'local_management'),
        get_string('timecreated', 'local_management'),
        get_string('isused', 'local_management'),
        get_string('sale','local_management'),
        get_string('completion2','local_management') . ' - ' . get_string('user:normal', 'local_management'),
        get_string('completion2','local_management') . ' - ' . get_string('user:bop', 'local_management'),
        get_string('notcompletion','local_management')
    );

    $date = date('Y-m-d', time());
    $filename = '상품리스트_' . $date. '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;
    $num = 1;
    foreach ($courses as $course) {
        $col = 0;

        $worksheet[0]->write($row, $col++, $num++);
        if($course->type == 1){
            $langcoursetype = get_string('course','local_management');
        }else if($course->type == 2){
            $langcoursetype =  get_string('book','local_management');
        }else{
            $langcoursetype =  get_string('course','local_management') .'+'.get_string('book','local_management');
        }
        $worksheet[0]->write($row, $col++, $langcoursetype);
        $worksheet[0]->write($row, $col++, $course->vi_title.'<br>'.$course->title);
        $worksheet[0]->write($row, $col++, $course->code);
        $originname = '';
        $origincode = '';
        if($course->type == 1){
            $origincode = $course->lcocode;
            switch (current_language()){
                case 'ko' :
                    $originname = $course->lconame;
                    break;
                case 'en' :
                    $originname = $course->en_lconame;
                    break;
                case 'vi' :
                    $originname = $course->vi_lconame;
                    break;
            }

        }else if($course->type == 2){
            $origincode = $course->ltcode;
            switch (current_language()){
                case 'ko' :
                    $originname = $course->lttitle;
                    break;
                case 'en' :
                    $originname = $course->en_lttitle;
                    break;
                case 'vi' :
                    $originname = $course->vi_lttitle;
                    break;
            }
        }else if($course->type == 3){
            //$originname = $course->lconame.'<br>'.$course->lttitle;
            $origincode = $course->lcocode.'<br>'.$course->ltcode;
            switch (current_language()){
                case 'ko' :
                    $originname = $course->lconame.'<br>'.$course->lttitle;
                    break;
                case 'en' :
                    $originname = $course->en_lconame.'<br>'.$course->en_lttitle;
                    break;
                case 'vi' :
                    $originname = $course->vi_lconame.'<br>'.$course->vi_lttitle;
                    break;
            }
        }
        $worksheet[0]->write($row, $col++, $originname );
        $worksheet[0]->write($row, $col++, $origincode );
        $worksheet[0]->write($row, $col++, $course->lcolecturecnt ? $course->lcolecturecnt  : '-' );
        $worksheet[0]->write($row, $col++, number_format($course->price) );
        $worksheet[0]->write($row, $col++, date('Y.m.d',$course->learningstart).' ~ '.date('Y.m.d',$course->learningend) );
        $worksheet[0]->write($row, $col++, date('Y.m.d',$course->timecreated) );
        $worksheet[0]->write($row, $col++, $course->isused == 0  ? get_string('used', 'local_management') : get_string('notused', 'local_management'));
        
        $getstudentsql = "SELECT count(*) 
                        FROM m_lmsdata_payment lp
                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                        JOIN m_course  c ON c.id = ctx.instanceid 
                        JOIN m_user u ON lp.userid = u.id
                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                        WHERE  c.id = :courseid2";

        $getcompletesql = "SELECT count(*) 
                        FROM m_lmsdata_payment lp
                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                        JOIN m_course  c ON c.id = ctx.instanceid 
                        JOIN m_user u ON lp.userid = u.id
                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                        JOIN {course_completions} cc ON cc.course = lc.courseid AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                        WHERE  c.id = :courseid2 AND lu.usergroup = :usergroup";

        $getstudent = $DB->count_records_sql($getstudentsql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel1' => 50, 'archetype' => 'student'));
        
        $getcomplete_rs = $DB->count_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => 50, 'archetype' => 'student', 'usergroup' => 'rs'));
        $getcomplete_bp = $DB->count_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => 50, 'archetype' => 'student', 'usergroup' => 'bp'));
        
        $worksheet[0]->write($row, $col++, ($course->type != 2) ? number_format($getstudent) : '-' );
        $worksheet[0]->write($row, $col++, ($course->type != 2) ? number_format($getcomplete_rs) : '-'  );
        $worksheet[0]->write($row, $col++, ($course->type != 2) ? number_format($getcomplete_bp) : '-'  );
        $worksheet[0]->write($row, $col++, ($course->type != 2) ? number_format($getstudent - ($getcomplete_rs + $getcomplete_bp)) : '-'  );
        $row++;
    }

    $workbook->close();
    die;

    