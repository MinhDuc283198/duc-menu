$(document).ready(function () {
    $(".uperpage").change(function () {
        $("form[id=form-search]").submit();
    });
    $(".tb-arrow").click(function () {
        $('input[name=orderitem]').val($(this).attr("id"));
        var type = ' ';
        if ($(this).hasClass("dash")) {
            type = 'up';
        } else if ($(this).hasClass("up")) {
            type = '';
        } else {
            type = 'dash';
        }
        $('input[name=ordertype]').val(type);
        $("#course_search").submit();
    });
});

function call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}