<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once $CFG->dirroot . '/local/signup/lib.php';

require_login();

$email = optional_param('email', null, PARAM_RAW);

$email = strtolower($email);
$returndata = [];

if ( $email != trim($email) || strpos($email,' ')) {

    $returndata['status'] = 0;
    $returndata['text'] = get_string('blankcheck','local_signup');
} else {
    $user = $DB->get_record_sql('select * from {user} where  email = ? or username = ? ',array('email'=>$email,'username'=>$email));

    if ($user) {
        $returndata['status'] = 0;
        $returndata['text'] = get_string('notavailable', 'local_signup',array("type"=>$typelang));
    }
}

if (! empty($returndata)) {
    response_success('success', $returndata);
    die;
}

$usercontext = context_user::instance($USER->id);

$user = $USER;
$user->timemodified = time();
$user->email = $email;
$user->username = $email;
$user->confirmed = 0;
// update lmsdata_user table
$lmsdata_user = $DB->get_record_sql("select * from m_lmsdata_user where userid = $USER->id");
$lmsdata_user->email2 =  $email;

$DB->update_record('lmsdata_user', $lmsdata_user);

// update user
user_update_user($user, false, false);
// send mail confirm
$emailauth = new local_auth_plugin_email();

$emailauth->send_confirmation_email($user);
// logout
redirect('/login/logout.php?sesskey=' . $USER->sesskey . '&logout=1');