<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->dirroot . '/user/profile/lib.php');
require_once $CFG->dirroot . '/local/signup/lib.php';
require_once($CFG->libdir . '/gdlib.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/user/editadvanced_form.php');
require_once($CFG->dirroot . '/user/editlib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->libdir . '/password_compat/lib/password.php');
require_once($CFG->dirroot . '/local/management/users/lib.php');

global $DB, $PAGE;

$context = context_system::instance();

$filepath = $_FILES['enrol_excel']['tmp_name'];

//엑셀파일 read
$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();

$dataUser = $objWorksheet->toArray();

$returnvalue = new stdClass();
$returnvalue->success = 0;
$returnvalue->begin = 0;
$returnvalue->empty = 0;
$returnvalue->notobject = 0;
error_log('[START:]' . date('Y-m-d H:i:s') . "]\n", 3, $CFG->dirroot . '/local/management/users/excel_user.log');

$keys = array_flip($dataUser[0]);


for($i = 1; $i < count($dataUser); $i++ ) {

    $fullname = $dataUser[$i][$keys['fullname']];
    $email = $username = trim($dataUser[$i][$keys['email']]);
    $usergroup = trim($dataUser[$i][$keys['type']]);
    $phone1 = trim($dataUser[$i][$keys['phone']]);
    $joinstatus = trim($dataUser[$i][$keys['join']]);

    $username = preg_replace("/\s+/", "", strtolower($username));
    $email = preg_replace("/\s+/", "", $email);

    if (!empty($username) && $username != '' && !empty($fullname) && $fullname && !empty($usergroup) && $usergroup && !empty($email) && $email) {

        $sql = "select * from m_user where username = '$username' or email = '$email'";
        $user = $DB->get_record_sql($sql);

        if (empty($user)) {

            $usernew = new stdClass();

            $user->calendartype = $CFG->calendartype;
            $user->username = $username;
            $user->phone1 = $phone1;

            $user->firstname = $fullname;
            $user->lastname = '&nbsp;';
            $user->lang = current_language();
            $user->firstaccess = 0;
            $user->timecreated = time();
            $user->mnethostid = $CFG->mnet_localhost_id;
            $user->secret = random_string(15);
            $user->email = $email;
            $user->password = hash_internal_user_password($email, TRUE);
            $user->confirmed = 1;   //이메일 인증 했는지 안했는지
            $user->usergroup = $usergroup;

            $user->id = $usernew->id = user_create_user($user, false, false);

            if ($user->id) {
                $lmsdatauser = new stdClass();
                $lmsdatauser->userid = $user->id;
                $lmsdatauser->phone1 = $user->phone1;
                $lmsdatauser->joinstatus = $joinstatus;
                $lmsdatauser->usergroup = $usergroup;
                $lmsdatauser->email = $email;

                $DB->insert_record('lmsdata_user', $lmsdatauser);

                if ($usergroup == "ad") {
                    local_user_addpageadmin("manager", "add", $user->id);
                }
                // success
                $returnvalue->success++;
            } else {
                //값이비었음
                $returnvalue->empty++;
            }

        } else {
            //중복
            $returnvalue->begin++;
        }
    }
}

error_log('[END:]' . date('Y-m-d H:i:s') . "]\n\n\n", 3, $CFG->dirroot . '/local/management/users/excel_user.log');

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
