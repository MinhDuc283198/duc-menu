<?php
    $stat_total_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND e.employer_id IS NULL 
                AND (lu.usergroup = 'rs' OR lu.usergroup = 'bp')");

    $stat_total_free_rs_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND NOT EXISTS ( 
                    SELECT null 
                    FROM {lmsdata_payment} lp 
                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref 
                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid 
                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                    WHERE lp.userid = u.id 
                        AND lp.forced = 0 
                        AND lc.price > 0 
                        AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) 
                ) 
                AND e.employer_id IS NULL 
                AND ( lu.usergroup = 'rs' )");

    $stat_total_free_bp_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND NOT EXISTS ( 
                    SELECT null 
                    FROM {lmsdata_payment} lp 
                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref 
                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid 
                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                    WHERE lp.userid = u.id 
                        AND lp.forced = 0 
                        AND lc.price > 0 
                        AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) 
                ) 
                AND e.employer_id IS NULL 
                AND ( lu.usergroup = 'bp' )");

    $stat_total_paid_rs_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND u.timecreated <= '{$thistime}' 
                AND EXISTS ( 
                    SELECT null 
                    FROM {lmsdata_payment} lp 
                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref 
                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid 
                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                    WHERE lp.userid = u.id 
                        AND lp.forced = 0 
                        AND lc.price > 0 
                        AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) 
                ) 
                AND e.employer_id IS NULL 
                AND ( lu.usergroup = 'rs' )");

    $stat_total_paid_bp_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND u.timecreated <= '{$thistime}' 
                AND EXISTS ( 
                    SELECT null 
                    FROM {lmsdata_payment} lp 
                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref 
                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid 
                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                    WHERE lp.userid = u.id 
                        AND lp.forced = 0 
                        AND lc.price > 0 
                        AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) 
                ) 
                AND e.employer_id IS NULL 
                AND ( lu.usergroup = 'bp' )");

    $stat_total_active_paid_rs_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND u.lastaccess > 0
                AND EXISTS (
                    SELECT null
                    FROM (
                        SELECT 
                            (ue.timeend - (86400*lc.reviewperiod)) as enddate, 
                            ue.userid as userid
                        from {lmsdata_class} lc
                        join {course} co on co.id = lc.courseid 
                        join {enrol} e on e.courseid = co.id
                        join {user_enrolments} ue on ue.enrolid = e.id
                        where ue.status = 0
                    ) z 
                    WHERE $thistime < z.enddate and z.userid = u.id
                )
                AND EXISTS ( 
                    SELECT null 
                    FROM {lmsdata_payment} lp 
                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref 
                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid 
                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                    WHERE lp.userid = u.id 
                        AND lp.forced = 0 
                        AND lc.price > 0 
                        AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                ) 
                AND e.employer_id IS NULL 
                AND ( lu.usergroup = 'rs' )");

    $stat_total_active_paid_bp_members = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) 
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id 
            LEFT JOIN {vi_employers} em ON e.employer_id = em.id 
            WHERE u.deleted = 0 
                AND u.lastaccess > 0
                AND EXISTS (
                    SELECT null
                    FROM (
                        SELECT 
                            (ue.timeend - (86400*lc.reviewperiod)) as enddate, 
                            ue.userid as userid
                        from {lmsdata_class} lc
                        join {course} co on co.id = lc.courseid 
                        join {enrol} e on e.courseid = co.id
                        join {user_enrolments} ue on ue.enrolid = e.id
                        where ue.status = 0
                    ) z 
                    WHERE $thistime < z.enddate and z.userid = u.id
                )
                AND EXISTS ( 
                    SELECT null 
                    FROM {lmsdata_payment} lp 
                    LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref 
                    LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid 
                    LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                    WHERE lp.userid = u.id 
                        AND lp.forced = 0 
                        AND lc.price > 0 
                        AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                ) 
                AND e.employer_id IS NULL 
                AND ( lu.usergroup = 'bp' )");

