<?php
$file = './user_sample_v4.xlsx'; // 파일의 전체 경로
$file_name = 'user_sample_v4.xlsx'; // 저장될 파일 이름

if($_GET['type'] == 4) {
    $file = './user_sample_v5.xlsx';
    $file_name = 'user_sample_v5.xlsx';
}

header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . $file_name . '"');
header('Content-Transfer-Encoding: binary');
header('Content-length: ' . filesize($file));
header('Expires: 0');
header("Pragma: public");

$fp = fopen($file, 'rb');
fpassthru($fp);
fclose($fp);
