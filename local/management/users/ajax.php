<?php

define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');


$action = optional_param('action','',PARAM_RAW);


switch ($action) {
    case 'email_chk':
        $data = required_param_array('data', PARAM_RAW);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $data['email'] = preg_replace("/\s+/","",strtolower($data['email'])); 
        $rvalue->list = $DB->get_record('user',array('email'=>$data['email']));
        echo json_encode($rvalue);
        break;
    case 'username_chk':
        $data = required_param_array('data', PARAM_RAW);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $data['username'] = preg_replace("/\s+/","",strtolower($data['username'])); 
        $rvalue->list = $DB->get_record('user',array('username'=>$data['username']));
        echo json_encode($rvalue);
        break;
    case 'modifyendtime' :
        require_once("$CFG->dirroot/enrol/locallib.php"); // Required for the course enrolment manager.
        $data = required_param_array('data', PARAM_RAW);
        $course = $DB->get_record('course', array('id'=>$data['courseid']), '*', MUST_EXIST);
        $ue = $DB->get_record('user_enrolments', array('id' => $data['ueid']), '*', MUST_EXIST);
        
        $data['time'] = str_replace(".", "-", $data['time']); 
        $data['time'] = strtotime($data['time']);
        //복습기간은 맨 처음! 결제했을때 그 기간 따라가는걸로 하기로 함
        $period = $DB->get_record('lmsdata_course_period', array('payid'=>$data['id']));
        $data['time'] = strtotime('+1 day',$data['time']) - 1 + ($period->reviewperiod * 86400);

        $manager = new course_enrolment_manager($PAGE, $course, $filter);
        $edit_data = new stdClass();
        $edit_data->status = $ue->status;
        $edit_data->timestart = $ue->timestart;
        $edit_data->timeend = $data['time'];
        $edit_data->ue = $data['ueid'];
        $manager->edit_enrolment($ue, $edit_data);
        
        $lmsdata = new stdClass();
        $lmsdata->id = $data['lcpid'];
        $lmsdata->endtime = $data['time'];
        $lmsdata->timemodified = time();

        $DB->update_record('lmsdata_course_period', $lmsdata);

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();