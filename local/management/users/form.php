<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/users/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');


$id = optional_param("id", 0, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);

$title = '';
$menu_txt = '';
if ($type == 1) {
    //rs, bp
    $title = get_string('user:title1', 'local_management');
    $menu_txt = 'normal';
    $usergroup = array('lu.usergroup = "rs"', 'lu.usergroup = "bp"');
} else if ($type == 2) {
    //pr
    $title = get_string('user:title2', 'local_management');
    $menu_txt = 'teacher';
    $usergroup = array('lu.usergroup = "pr"');
} else if ($type == 4) {
    // company users
    $title = get_string('user:title4', 'local_management');
    $menu_txt = 'companyusers';
    $usergroup = array('lu.usergroup = "rs"', 'lu.usergroup = "bp"', 'lu.usergroup = "ad"');
} else {
    //ad
    $title = get_string('user:title3', 'local_management');
    $menu_txt = 'admin';
    $usergroup = array('lu.usergroup = "ad"');
}


$pagesettings = array(
    'title' => $title,
    'heading' => $title,
    'subheading' => '',
    'menu' => $menu_txt,
    'js' => array(
        $CFG->wwwroot . '/chamktu/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot . '/chamktu/js/ckfinder-2.4/ckfinder.js'
    ),
    'css' => array(),
    'nav_bar' => array()
);
$temp = new stdclass();

$fullname = '';
if (!empty($id)) {
    $sql = "SELECT u.id, lu.id as luid, u.username, u.email, u.suspended,u.firstname, u.lastname, u.department, lu.usergroup, lu.usergroup, lu.address, lu.address_detail,u.phone1,u.phone2, u.auth, u.lastaccess, u.timecreated, f.itemid, lu.sns     
            FROM {user} u 
            JOIN {lmsdata_user} lu ON lu.userid = u.id 
            LEFT JOIN (SELECT userid as fuserid, itemid FROM {files} WHERE component = 'user' AND filesize >0 AND mimetype LIKE '%image%' AND source is null) f ON u.id = fuserid
            WHERE u.id = " . $id;
    $temp = $DB->get_record_sql($sql);

    if ($temp->usergroup == 'pr') {
        $t_sql = 'SELECT * FROM {lmsdata_teacher} WHERE userid = :id';
        $t_temp = $DB->get_record_sql($t_sql, array('id' => $id));
    }

    $file_obj = $DB->get_record('files', array('itemid' => $temp->itemid, 'license' => 'allrightsreserved'));

    $fullname = fullname($temp);

}
$auth_array = local_management_users_auth_array();
?>
<?php include_once($CFG->dirroot . '/local/management/header.php'); ?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="<?php echo './submit.php'; ?>" method="POST">
                    <input type="hidden" name="id" value="<?php echo $temp->id ?>">
                    <input type="hidden" name="luid" value="<?php echo $temp->luid ?>">
                    <input type="hidden" name="type" value="<?php echo $type; ?>">
                    <table cellpadding="0" cellspacing="0" class="detail">
                        <tbody>
                            <?php if ($type == 4) : ?>
                                <?php
                                $lastaccess = get_string('user:noconnection', 'local_management');
                                if ($temp->lastaccess) {
                                    $lastaccess = userdate($temp->lastaccess, '%Y.%m.%0d %H:%M');
                                }

                                $employer = $DB->get_record_sql(
                                    "SELECT e.* 
                                    FROM {vi_employers} e
                                    JOIN {vi_admin_employers} ae ON ae.employer_id = e.id
                                    WHERE ae.user_id=:userid",
                                    array('userid' => $temp->id)
                                );
                                ?>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:member_type', 'local_management') ?></td>
                                    <td class="field_value" colspan="3">
                                        <?php echo get_string("user:company", 'local_management'); ?>
                                        <select class="w_300" title="group" name="usergroup" style="display: none;">
                                            <?php
                                            if ($id) {
                                                $auth_type = $temp->usergroup;
                                            } else {
                                                switch ($type) {
                                                    case 3:
                                                        $auth_type = 'ad';
                                                        break;
                                                    case 2:
                                                        $auth_type = 'pr';
                                                        break;
                                                    default:
                                                        $auth_type = 'rs';
                                                        break;
                                                }
                                            }
                                            foreach ($auth_array as $key => $val) {
                                                $selected = ($auth_type == $key) ? "selected" : '';
                                            ?>
                                                <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo get_string("user:$key", 'local_management'); ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:id', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <?php if (!empty($id)) { ?>
                                            <input type="text" class="w_300" title="username" name="username" readonly="true" value="<?php echo $temp->username; ?>" autocomplete="off" />
                                        <?php } else { ?>
                                            <input type="text" class="w_300" title="username" data-overlap="0" name="username" value="" autocomplete="off" />
                                            <input type="button" name="username_chk" value="<?php echo get_string('user:duplicate', 'local_management') ?>" class="gray_btn" />
                                            <input type="hidden" name="username_overlap">
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:company_name', 'local_management'); ?> <span class="red">*</span> </td>
                                    <td class="field_value" colspan="3">
                                        <input type="text" title="company_name" required class="w_300" name="company_name" value="<?php echo $employer->company_name; ?>" autocomplete="off" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:pw', 'local_management') ?> <span class="red">*</span> </td>
                                    <td class="field_value" colspan="3">
                                        <input type="password" title="password" class="w_300" <?php if ($id) { ?> disabled="true" <?php } ?> name="newpassword" value="" autocomplete="off" />
                                        <?php if ($id) { ?>
                                            <?php echo get_string('user:pwchange', 'local_management') ?> <input type="checkbox" name="pw_edit">
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:business_registration_number', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <input type="text" title="phone2" class="w_300" required name="phone2" value="<?php echo $temp->phone2 ?>" autocomplete="off" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:registrationcertificate', 'local_management'); ?></td>
                                    <td class="field_value" colspan="3">
                                        <p class="mg-bt10"><input type="file" name="uploadfile_certificate"></p>
                                        <?php if ($employer->company_cover != '') : ?>
                                            <a target="_blank" href="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $employer->company_cover; ?>"><?php echo get_string('user:download', 'local_management'); ?></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:com_country', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <select class="w_300" title="group" name="country_id" required>
                                            <?php
                                            $countries = $DB->get_records('vi_countries', null, '{vi_countries}.name ASC');
                                            foreach ($countries as $country) {
                                                $country_name = $country->name_ko;
                                                switch (current_language()) {
                                                    case 'en':
                                                        $country_name = $country->name_en;
                                                        break;
                                                    case 'vi':
                                                        $country_name = $country->name_vi;
                                                        break;
                                                }
                                                $selected = ($employer->country_id == $country->id) ? "selected" : '';
                                            ?>
                                                <option value="<?php echo $country->id; ?>" <?php echo $selected ?>><?php echo $country_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:employers_company_type', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <select class="w_300" title="group" name="company_type_id" required>
                                            <?php
                                            $company_types = $DB->get_records('vi_company_types', null, '{vi_company_types}.name ASC');
                                            foreach ($company_types as $company_type) {
                                                $selected = ($employer->company_type_id == $company_type->id) ? "selected" : '';
                                            ?>
                                                <option value="<?php echo $company_type->id; ?>" <?php echo $selected ?>><?php echo $company_type->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:com_address', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <textarea required name="company_address" id="" class="w_300" rows="3"><?php echo $employer->company_address; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:com_contact_name', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <input type="text" title="name" class="w_300" required name="firstname" value="<?php echo $temp->firstname ?>" autocomplete="off" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:com_department', 'local_management'); ?></td>
                                    <td class="field_value" colspan="3">
                                        <input type="text" title="department" class="w_300" name="department" value="<?php echo $temp->department; ?>" autocomplete="off" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:com_contact_number', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <input type="text" title="phone" required name="phone1" value="<?php echo (!empty($temp->phone1)) ? $temp->phone1 : ""; ?>" maxlength="13" placeholder='' autocomplete="off" /> <span><?php echo get_string('phone_txt', 'local_my') ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:com_contact_email', 'local_management'); ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <input type="email" style="padding: 7px 5px;" class="w_300" required title="email" data-overlap="0" name="email" value="<?php echo $temp->email; ?>" autocomplete="off" />
                                    </td>
                                </tr>
                                <?php if (!empty($id)) : ?>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:request_date', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <?php echo date('Y.m.d', $temp->timecreated); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:approval_date', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <?php echo $employer->approval_date ? date('Y.m.d', $employer->approval_date) : '-'; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:last', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <?php echo $lastaccess; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:approval', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <select name="approval" class="w_300">
                                                <option value="0" <?php echo ($employer->confirmed == 0 ? ' selected' : ''); ?>><?php echo get_string('user:approval_pending', 'local_management'); ?></option>
                                                <option value="1" <?php echo ($employer->confirmed == 1 ? 'selected' : ''); ?>><?php echo get_string('user:approval_accepted', 'local_management'); ?></option>
                                                <option value="-1" <?php echo ($employer->confirmed == -1 ? ' selected' : ''); ?>><?php echo get_string('user:approval_notaccepted', 'local_management'); ?></option>
                                            </select>
                                        </td>
                                    </tr>
                                <?php else : ?>
                                    <input type="hidden" name="approval" value="1">
                                <?php endif; ?>

                            <?php else : ?>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:authority', 'local_management') ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <select class="w_160" title="group" name="usergroup">
                                            <?php
                                            if ($id) {
                                                $auth_type = $temp->usergroup;
                                            } else {
                                                switch ($type) {
                                                    case 3:
                                                        $auth_type = 'ad';
                                                        break;
                                                    case 2:
                                                        $auth_type = 'pr';
                                                        break;
                                                    default:
                                                        $auth_type = 'rs';
                                                        break;
                                                }
                                            }
                                            foreach ($auth_array as $key => $val) {
                                                $selected = ($auth_type == $key) ? "selected" : '';
                                            ?>
                                                <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo get_string("user:$key", 'local_management') ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <?php if ($id) { ?>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:jointype', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <?php 
                                            if ($temp->sns == '' or $temp->sns == 'email') {
                                                $temp->sns = get_string('user:email', 'local_management');
                                            }
                                            echo $temp->sns;
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php ?>
                                <tr>
                                    <td class="field_title" colspan="2">ID <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <?php if (!empty($id)) { ?>
                                            <input type="text" class="w_300" title="username" name="username" readonly="true" value="<?php echo $temp->username; ?>" autocomplete="off" />
                                        <?php } else { ?>
                                            <input type="text" class="w_300" title="username" data-overlap="0" name="username" value="" autocomplete="off" />
                                            <input type="button" name="username_chk" value="<?php echo get_string('user:duplicate', 'local_management') ?>" class="gray_btn" />
                                            <input type="hidden" name="username_overlap">
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:name', 'local_management') ?> <span class="red">*</span> </td>
                                    <td class="field_value" colspan="3">
                                        <input type="text" title="name" class="w_200" name="firstname" value="<?php echo $temp->firstname ?>" autocomplete="off" />
                                    </td>
                                </tr>
                                <tr class="pr">
                                    <input type="hidden" name="itemid" value="<?php echo $temp->itemid ?>">
                                    <td class="field_title" rowspan="2"><?php echo get_string('user:img', 'local_management') ?><br>(jpg,png)</td>
                                    <td class="field_title"><?php echo get_string('user:applycourse', 'local_management') ?></td>
                                    <td class="field_value" colspan="2">
                                        <?php
                                        if ($id) {
                                            $user = $DB->get_record('user', array('id' => $id));
                                            $teacherimgpath = local_course_teacher_get_imgpath($user);
                                        }
                                        ?>

                                        <p class="mg-bt10"><input type="file" name="uploadfile"> <span>(512*512px)</span></p>
                                        <img src="<?php echo $teacherimgpath ?>" width="200" />
                                        <?php //}
                                        ?>
                                    </td>
                                </tr>

                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('user:applyintro', 'local_management') ?></td>
                                    <?php
                                    if ($id) {
                                        $usercontext = context_user::instance($id);
                                        $fs = get_file_storage();
                                        $files = $fs->get_area_files($usercontext->id, 'local_lmsdata', 'teacher', $id, "", false);
                                        foreach ($files as $file) {
                                            $filename = $file->get_filename();
                                            $mimetype = $file->get_mimetype();
                                            $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

                                            $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $usercontext->id . '/local_lmsdata/teacher/' . $id . '/' . $filename);
                                            if ($file->get_filesize() > 0) {
                                                $fileobj = '<input type="hidden" name="file_del" value="0"/><img src="' . $path . '" width="200">';
                                                //$fileobj .= "<div style='float:left;' id ='file'><a href=\"$path\">$iconimage</a>";
                                                //$fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $usercontext)) . '</div>';
                                            }
                                        }
                                    }
                                    ?>
                                    <td class="field_value" colspan="3">
                                        <p class="mg-bt10"><input type="file" name="uploadfile_2"> <span>(540*305px)</span></p>
                                        <?php echo $fileobj ?>
                                        <?php //}
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:pw', 'local_management') ?> <span class="red">*</span> </td>
                                    <td class="field_value" colspan="3">
                                        <input type="password" title="password" class="w_300" <?php if ($id) { ?> disabled="true" <?php } ?> name="newpassword" value="" autocomplete="off" />
                                        <?php if ($id) { ?>
                                            <?php echo get_string('user:pwchange', 'local_management') ?> <input type="checkbox" name="pw_edit">
                                        <?php } ?>
                                    </td>
                                </tr>
                                <!--
                            <?php if ($id) { ?>
                            <tr>
                                <td class="field_title">비밀번호 확인</td>
                                <td class="field_value">
                                    <input type="password" title="password" class="w_300" <?php if (!empty($id)) echo "disabled='true'"; ?> name ="repassword" value=""/>
                                </td>
                            </tr>
                            <?php } ?>
                            -->

                                <!--                            <tr>
                                <td class="field_title">주소 </td>
                                <td class="field_value">
                                    <input type="text" class="w_300" name="address" value="<?php echo $temp->address ?>"><br>
                                    <input type="text" class="w_300" name="address_detail" value="<?php echo $temp->address_detail ?>">
                                </td>
                            </tr>-->
                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:birth', 'local_management') ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <?php
                                        $birth_array = explode('-', $temp->phone2);
                                        ?>
                                        <select name="birth_y">
                                            <?php
                                            foreach (range(date('Y'), 1960) as $year) {
                                                if ($birth_array[0] == $year) {
                                                    echo '<option value="' . $year . '" selected>' . $year . '</option>';
                                                } else {
                                                    echo '<option value="' . $year . '">' . $year . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <select name="birth_m">
                                            <?php
                                            for ($m = 1; $m <= 12; $m++) {
                                                if (strlen($m) == 1)
                                                    $m = "0" . $m;
                                                if ($m == $birth_array[1]) {
                                                    $date_month .= "<option value='$m' selected>$m</option>\n";
                                                } else {
                                                    $date_month .= "<option value='$m'>$m</option>\n";
                                                }
                                            }
                                            echo $date_month;
                                            ?>
                                        </select>
                                        <select name="birth_d">
                                            <?php
                                            for ($d = 1; $d <= 31; $d++) {
                                                if (strlen($d) == 1)
                                                    $d = "0" . $d;
                                                if ($d == $birth_array[2]) {
                                                    $date_day .= "<option value='$d' selected>$d</option>\n";
                                                } else {
                                                    $date_day .= "<option value='$d'>$d</option>\n";
                                                }
                                            }
                                            echo $date_day;
                                            ?>
                                        </select>

                                    </td>
                                </tr>

                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:email', 'local_management') ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <?php
                                        $lmsdata_user_query = 'select u.id, lu.id as lu_id, lu.address, lu.address_detail, lu.email2, lu.zipcode from {user} u LEFT JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
                                        $lmsdata_user = $DB->get_record_sql($lmsdata_user_query, array('id' => $temp->id));
                                        ?>
                                        <input type="email" style="padding: 7px 5px;" class="w_300" title="email" data-overlap="0" name="email2" value="<?php echo $lmsdata_user->email2; ?>" autocomplete="off" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="field_title" colspan="2"><?php echo get_string('user:phone', 'local_management') ?> <span class="red">*</span></td>
                                    <td class="field_value" colspan="3">
                                        <?php
                                        $phone_array = explode('-', $temp->phone1);
                                        ?>
                                        <!--                                    <input type="text" title="phone" name ="phone_1" value="<?php echo (!empty($temp->phone1)) ? $phone_array[0] : ""; ?>" maxlength="6"/>
                                    <input type="text" title="phone" name ="phone_2" value="<?php echo (!empty($temp->phone1)) ? $phone_array[1] : ""; ?>" maxlength="6"/>
                                    <input type="text" title="phone" name ="phone_3" value="<?php echo (!empty($temp->phone1)) ? $phone_array[2] : ""; ?>" maxlength="6"/>-->
                                        <input type="text" title="phone" name="phone1" value="<?php echo (!empty($temp->phone1)) ? $temp->phone1 : ""; ?>" maxlength="13" placeholder='' autocomplete="off" /> <span><?php echo get_string('phone_txt', 'local_my') ?></span>
                                    </td>
                                </tr>
                                <?php if ($id) { ?>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:timecreated', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <?php echo date('Y.m.d', $temp->timecreated) ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $lastaccess = get_string('user:noconnection', 'local_management');
                                    if ($user->lastaccess) {
                                        $lastaccess = userdate($temp->lastaccess, '%Y.%m.%0d %H:%M');
                                    }
                                    ?>
                                    <tr>
                                        <td class="field_title" colspan="2"><?php echo get_string('user:last', 'local_management') ?></td>
                                        <td class="field_value" colspan="3">
                                            <?php echo $lastaccess ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <!--                            <tr>
                                <td class="field_title">상태 <span class="red">*</span></td>
                                <td class="field_value" >
                                    <select title="status" class="w_160" name="suspended">
                                        <option <?php
                                                if (isset($temp->suspended) && $temp->suspended == '0') {
                                                    echo (!empty($temp->suspended)) ? "selected='selected'" : "";
                                                }
                                                ?> value="0"><?php echo get_string('siteadmin_act', 'local_lmsdata'); ?></option>
                                        <option <?php
                                                if (isset($temp->suspended) && $temp->suspended == '1') {
                                                    echo (!empty($temp->suspended)) ? "selected='selected'" : "";
                                                }
                                                ?> value="1"><?php echo get_string('siteadmin_noact', 'local_lmsdata'); ?></option>
                                    </select>
                                </td>
                            </tr>-->

                                <tr class="pr">
                                    <td class="field_title" colspan="2"><?php echo get_string('user:activity', 'local_management') ?></td>
                                    <td class="field_value" colspan="3">
                                        <input type="radio" name="isused" value="1" readonly="readonly" <?php if ($t_temp->isused == 1 || $id == 0) {
                                                                                                            echo 'checked';
                                                                                                        } else {
                                                                                                            echo '';
                                                                                                        } ?>><?php echo get_string('user:activity1', 'local_management') ?>
                                        <input type="radio" name="isused" value="0" readonly="readonly" <?php if ($t_temp->isused == 0 && !empty($id)) {
                                                                                                            echo 'checked';
                                                                                                        } else {
                                                                                                            echo '';
                                                                                                        } ?>><?php echo get_string('user:inactivity', 'local_management') ?>
                                    </td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title" rowspan="3"><?php echo get_string('user:oneline', 'local_management') ?></td>
                                    <td class="field_title"><?php echo get_string('vi', 'local_management') ?> </td>
                                    <td class="field_value" colspan="3"><input type="text" class="w100" size="150" name="oneline_vi" value="<?php echo $t_temp->oneline_vi ?>"></td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('ko', 'local_management') ?></td>
                                    <td class="field_value" colspan="3"><input type="text" class="w100" size="150" name="oneline" value="<?php echo $t_temp->oneline ?>"></td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('en', 'local_management') ?></td>
                                    <td class="field_value" colspan="3"><input type="text" class="w100" size="150" name="oneline_en" value="<?php echo $t_temp->oneline_en ?>"></td>
                                </tr>

                                <tr class="pr">
                                    <td class="field_title" rowspan="3"><?php echo get_string('user:profile', 'local_management') ?> </td>
                                    <td class="field_title"><?php echo get_string('vi', 'local_management') ?> </td>
                                    <td class="field_value" colspan="3">
                                        <textarea class="ckeditor" rows="7" name="profile_vi"><?php echo (!empty($t_temp->profile_vi)) ? $t_temp->profile_vi : ""; ?></textarea>
                                    </td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('ko', 'local_management') ?></td>
                                    <td class="field_value" colspan="3">
                                        <textarea class="ckeditor" rows="7" name="profile"><?php echo (!empty($t_temp->profile)) ? $t_temp->profile : ""; ?></textarea>
                                    </td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('en', 'local_management') ?></td>
                                    <td class="field_value" colspan="3">
                                        <textarea class="ckeditor" rows="7" name="profile_en"><?php echo (!empty($t_temp->profile_en)) ? $t_temp->profile_en : ""; ?></textarea>
                                    </td>
                                </tr>

                                <tr class="pr">
                                    <td class="field_title" rowspan="3"><?php echo get_string('user:aword', 'local_management') ?> </td>
                                    <td class="field_title"><?php echo get_string('vi', 'local_management') ?> </td>
                                    <td class="field_value" colspan="3">
                                        <textarea class="ckeditor" rows="7" name="comment_vi"><?php echo (!empty($t_temp->comment_vi)) ? $t_temp->comment_vi : ""; ?></textarea>
                                    </td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('ko', 'local_management') ?> </td>
                                    <td class="field_value" colspan="3">
                                        <textarea class="ckeditor" rows="7" name="comment"><?php echo (!empty($t_temp->comment)) ? $t_temp->comment : ""; ?></textarea>
                                    </td>
                                </tr>
                                <tr class="pr">
                                    <td class="field_title"><?php echo get_string('en', 'local_management') ?> </td>
                                    <td class="field_value" colspan="3">
                                        <textarea class="ckeditor" rows="7" name="comment_en"><?php echo (!empty($t_temp->comment_en)) ? $t_temp->comment_en : ""; ?></textarea>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <?php
                    $select = 'select lp.id, lp.timemodified, lc.title,lc.code, lc.vi_title, lc.en_title,lc.id as lcid, 
                                lc.courseperiod, lcp.reviewperiod, ue.timestart as time_payment_start, ue.timeend as time_payment_end, mc.id as mcid, ue.id as ueid, lcp.starttime, lcp.endtime, lcp.id lcpid 
                                , case when (select max(lp2.id) max
                                            from m_lmsdata_course_period lcp2 JOIN m_lmsdata_payment lp2 ON lcp2.payid = lp2.id 
                                            WHERE  lp2.userid = lp.userid AND  lp2.ref = lc.id 
                                            GROUP BY lp2.ref) = lp.id then 1 else 0  end visibly

                                ';
                    $cnt_select = 'select count(lp.id) ';
                    $query = 'from m_lmsdata_payment   lp
                            JOIN {lmsdata_class} lc ON lp.ref = lc.id 
                            JOIN {course} mc ON lc.courseid = mc.id
                            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                            join {enrol} e on e.courseid = mc.id
                            join {user_enrolments} ue on ue.enrolid = e.id AND ue.status = 0 
                            JOIN {context} ctx ON ctx.instanceid = mc.id AND contextlevel = 50
                            join {role_assignments} ra on ra.contextid = ctx.id and ra.userid = ue.userid AND ue.userid = :userid 
                            JOIN {role} ro ON ra.roleid = ro.id and ro.id = 5 
                            LEFT JOIN {lmsdata_course_period} lcp ON lcp.payid = lp.id AND lcp.userid = lp.userid 
                            where lp.type = 1 AND lp.userid = :userid2 
                            ORDER BY lp.timemodified DESC, lp.id DESC';
                    $list = $DB->get_records_sql($select . $query, array('userid' => $temp->id, 'userid2' => $temp->id));

                    $total = $DB->count_records_sql($cnt_select . $query, array('userid' => $temp->id, 'userid2' => $temp->id));
                    ?>
                    <?php if ($list && $type == 1) {
                        $i =  $total; ?>
                        <table>
                            <thead>
                                <th><?php echo get_string('number', 'local_management') ?></th>
                                <th><?php echo get_string('goodsname', 'local_management') ?></th>
                                <th><?php echo get_string('goodscode', 'local_management') ?></th>
                                <th><?php echo get_string('paymentdate', 'local_management') ?></th>
                                <th><?php echo get_string('user:timestart', 'local_management') ?></th>
                                <th><?php echo get_string('user:timeend', 'local_management') ?></th>
                                <th><?php echo get_string('modify', 'local_management') ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($list as $key => $li) { ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><a href="/local/management/goods/form.php?id=<?php echo $li->lcid ?>"><?php echo $li->title ?></a></td>
                                        <td><a href="/local/management/goods/form.php?id=<?php echo $li->lcid ?>"><?php echo $li->code ?></a></td>
                                        <td><?php echo date('Y.m.d', $li->timemodified) ?></td>
                                        <?php
                                        $end_time = $li->time_payment_end + ($li->reviewperiod * 86400);
                                        ?>
                                        <td>
                                            <?php echo date('Y.m.d', $li->time_payment_start); ?>
                                        </td>
                                        <?php if ($li->visibly == 1) { ?>
                                            <td>
                                                <input type="text" title="<?php echo get_string('time', 'local_management') ?>"
                                                       id="endtime_<?php echo $li->id ?>" name="learningstart" class="w_120 date s_date"
                                                       value="<?php echo date('Y.m.d', $end_time) ?>"
                                                       placeholder="<?php echo get_string('click', 'local_management') ?>">
                                            </td>
                                            <td>
                                                <input type="button" value="<?php echo get_string('modify', 'local_management') ?>"
                                                       onclick="modifyendtime(' <?php echo $li->mcid ?>','<?php echo $li->ueid ?>','<?php echo $li->id ?>','<?php echo $li->reviewperiod ?>','<?php echo $li->lcpid ?>')">
                                            </td>
                                        <?php } else { ?>
                                            <td><?php echo date('Y.m.d', $end_time) ?></td>
                                        <?php } ?>
                                    </tr>
                                <?php $i--;
                                } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <?php if ($type == 2 && $id) {
                        $subcourse_query_select = "select lc.* ";
                        $subcourse_query = "
                                        from m_lmsdata_class  lc
                                        join m_lmsdata_course  lco on lc.parentcourseid = lco.id
                                        join m_course  co on co.id = lc.courseid
                                        join m_enrol  e on e.courseid = co.id
                                        join m_user_enrolments  ue on ue.enrolid = e.id
                                        JOIN m_context  ctx ON ctx.instanceid = co.id AND contextlevel = 50
                                        join m_role_assignments  ra on ra.contextid = ctx.id and ra.userid = ue.userid AND ra.roleid = 3 AND ra.userid = $id 
                                        JOIN m_role  ro ON ra.roleid = ro.id";

                        $subcourse = $DB->get_records_sql($subcourse_query_select . $subcourse_query);

                        $subcourse_cnt = $DB->count_records_sql("select count(lc.id) " . $subcourse_query);
                        if ($subcourse_cnt > 0) {
                    ?>
                            <table>
                                <thead>
                                    <th><?php echo get_string('number', 'local_management') ?></th>
                                    <th><?php echo get_string('goodsname', 'local_management') ?></th>
                                    <th><?php echo get_string('goodscode', 'local_management') ?></th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($subcourse as $tl) {
                                        $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><a href="/local/management/goods/form.php?id=<?php echo $tl->id ?>"><?php echo $tl->title ?></a></td>
                                            <td><a href="/local/management/goods/form.php?id=<?php echo $tl->id ?>"><?php echo $tl->code ?></a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                    <?php }
                    } ?>
                    <div id="btn_area">
                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_lmsdata'); ?>" style="float: right;" onclick="return val_check()" />
                        <!--
                        <?php if ($id != 0) { ?>
                                <input type="button" id="add_delete" class="red_btn" onclick="delete_temp_user('<?php echo $id; ?>');" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: right; margin-left: 15px;" />
                        <?php } ?>
                        -->
                        <input type="button" id="temp_list" class="normal_btn" value="<?php echo get_string('list', 'local_management'); ?>" style="float: left;" />

                    </div> <!-- Bottom Button Area -->
                </form>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>


<script type="text/javascript">
    $(document).ready(function() {
        $("input[name=uploadfile]").change(function() {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["jpg", "jpeg", "png"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2', 'local_management') ?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });

        $("input[name=uploadfile_2]").change(function() {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["jpg", "jpeg", "png"]) == -1) {
                    alert("<?php echo get_string('goods:valimgtype2', 'local_management') ?>");
                    $(this).val("");
                    //$('.file-name').val('');
                    return;
                }
                thumbnailimg = $(this).val();
            }
        });

        $("input[name=phone1]").on("keyup", function() {
            $(this).val($(this).val().replace(/[^0-9]/g, ""));
        });
        //비밀번호 변경 클릭시
        $('.field_value input[name=pw_edit]').click(function() {
            if ($('.field_value input[name=pw_edit]').prop('checked') == true) {
                $(".field_value input[name=newpassword]").prop('disabled', false);
                $(".field_value input[name=repassword]").prop('disabled', false);
            } else {
                $(".field_value input[name=newpassword]").prop('disabled', true);
                $(".field_value input[name=repassword]").prop('disabled', true);
            }
        });
        //목록 버튼 클릭시
        $('#temp_list').click(function() {
            location.href = "./index.php?type=<?php echo $type ?>";
        });

        //권한이 강사라면
        pr_display();
        $("select[name=usergroup]").change(function() {
            pr_display();
        });

        //중복체크 클릭
        $('input[name=email_chk]').click(function() {
            var email = $('input[name=email]').val();
            if (!email) {
                alert('<?php echo get_string('user:emailtext1', 'local_management') ?>');
            } else {
                var result = call_ajax('email_chk', {
                    email: email
                }, false);
                if (result.status == 'success') {
                    if (result.list) {
                        alert('<?php echo get_string('user:emailtext2', 'local_management') ?>');
                        $('input[name=email_overlap]').val(0);
                    } else {
                        alert('<?php echo get_string('user:emailtext3', 'local_management') ?>');
                        $('input[name=email_overlap]').val(1);
                    }
                }
            }
        });


        $('input[name=username_chk]').click(function() {
            var username = $('input[name=username]').val();
            if (!username) {
                alert('<?php echo get_string('user:idtext1', 'local_management') ?>');
            } else {
                var result = call_ajax('username_chk', {
                    username: username
                }, false);
                if (result.status == 'success') {
                    if (result.list) {
                        alert('<?php echo get_string('user:idtext2', 'local_management') ?>');
                        $('input[name=username_overlap]').val(0);
                    } else {
                        alert('<?php echo get_string('user:idtext3', 'local_management') ?>');
                        $('input[name=username_overlap]').val(1);
                    }
                }
            }
        });
    });
    // 값 체크
    function val_check() {
        var usergroup = $('select[name=usergroup]').val();
        var email = $('input[name=email2]').val();
        var password = $('input[name=newpassword]').val();
        var name = $('input[name=firstname]').val();
        var email_check = $('input[name=email_overlap]').val();
        var username_chk = $('input[name=username_overlap]').val();
        var phone1 = $('input[name=phone1]').val();
        var username = $('input[name=username]').val();

        if (!usergroup) {
            alert('<?php echo get_string('user:val1', 'local_management') ?>');
            return false;
        }
        if (!username) {
            alert('<?php echo get_string('user:idtext1', 'local_management') ?>');
            return false;
        }
        if ($('input[name=email2]').length && !email) {
            alert('<?php echo get_string('user:emailtext1', 'local_management') ?>');
            return false;
        }
        <?php if (!$id) { ?>
            if (!password) {
                alert('<?php echo get_string('user:val2', 'local_management') ?>');
                return false;
            }
        <?php } ?>
        if (!name) {
            alert('<?php echo get_string('user:val3', 'local_management') ?>');
            return false;
        }

        //        if(!status){
        //            alert('상태를 선택해 주세요');
        //            return false;
        //        }
        if (email_check == 0) {
            alert('<?php echo get_string('user:val4', 'local_management') ?>');
            return false;
        }
        if (username_chk == 0) {
            alert('<?php echo get_string('user:val5', 'local_management') ?>');
            return false;
        }
        if (!phone1) {
            alert('<?php echo get_string('user:val6', 'local_management') ?>');
            return false;
        }

        return true;
    }
    //권한이 강사일때와 아닐때
    function pr_display() {
        var auth = $("select[name=usergroup]").val();
        if (auth == 'pr') {
            $(".pr").css("display", "");
        } else {
            $(".pr").css("display", "none");
        }
    }

    function remove_file() {
        $("a[name='file_link']").remove();
        $("input[name='remove_button']").remove();
        $("input[name='file_del']").val(1);
    }

    function delete_temp_user(did) {
        if (confirm("정말 삭제하시겠습니까??") == true) {
            location.href = '<?php echo 'inftemp_submit.php?id='; ?>' + did + '<?php echo '&mod=delete'; ?>';
        } else {
            return false;
        }
    }
    start_datepicker_c('', '');

    function modifyendtime(courseid, ueid, id, reviewperiod, lcpid) {
        var time = $('#endtime_' + id).val();
        if (confirm("<?php echo get_string('user:timemodify', 'local_management') ?>") == true) {
            var result = call_ajax('modifyendtime', {
                id: id,
                time: time,
                courseid: courseid,
                ueid: ueid,
                reviewperiod: reviewperiod,
                lcpid: lcpid
            }, false);
            if (result.status == 'success') {
                alert('<?php echo get_string('user:timesuccess', 'local_management') ?>');
                location.reload();
            }
        }
    }
</script>