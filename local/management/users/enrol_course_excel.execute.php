<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->dirroot . '/user/profile/lib.php');
require_once $CFG->dirroot . '/local/signup/lib.php';
require_once($CFG->libdir . '/gdlib.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/user/editadvanced_form.php');
require_once($CFG->dirroot . '/user/editlib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->libdir . '/password_compat/lib/password.php');
require_once($CFG->dirroot . '/local/management/users/lib.php');

global $DB, $PAGE;

$context = context_system::instance();
//require_capability('moodle/site:config', $context);

//$type = required_param('type', PARAM_RAW);
$filename = $_FILES['enrol_excel']['name'];
$filepath = $_FILES['enrol_excel']['tmp_name'];

$type = optional_param('type', 1, PARAM_INT);

/*
$role_arr = $DB->get_records('role', null, '', 'shortname, id, name');
$role_type = array('t' => 'editingteacher', 's' => 'student');

$course = $DB->get_record('course', array('id' => $courseid));
$lmsdataClass = $DB->get_record('lmsdata_class', array('courseid' => $courseid));

// 강의의 usertypecode 확인(classobject)
$lmsdataCoursesql = 'SELECT lco.*, ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber 
                                    FROM {lmsdata_course} lco
                                    JOIN {course_categories} ca ON ca.id = lco.detailprocess 
                                    LEFT JOIN {course_categories} ca2 ON ca.parent = ca2.id
                                    WHERE lco.id = :id';
$lmsdataCourse = $DB->get_record_sql($lmsdataCoursesql, array('id' => $lmsdataClass->parentcourseid));

$enrol = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $course->id));
$manager = new course_enrolment_manager($PAGE, $course);

$instances = $manager->get_enrolment_instances();
$instance = $instances[$enrol->id];

$plugins = $manager->get_enrolment_plugins();
$plugin = $plugins[$instance->enrol];

$context = context_course::instance($course->id, MUST_EXIST);
*/
//엑셀파일 read
$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
}

$maxRow = $objWorksheet->getHighestRow();


$returnvalue = new stdClass();
$returnvalue->success = 0;
$returnvalue->begin = 0;
$returnvalue->empty = 0;
$returnvalue->notobject = 0;
error_log('[START:]' . date('Y-m-d H:i:s') . "]\n", 3, $CFG->dirroot . '/local/management/users/excel_user.log');

for ($i = 2; $i <= $maxRow; $i++) {
    if ($type == 4) {
        $fullname = $company_name = trim($objWorksheet->getCell('B' . $i)->getValue());
        $username = trim($objWorksheet->getCell('C' . $i)->getValue());
        $password = trim($objWorksheet->getCell('D' . $i)->getValue());
        $phone2 = trim($objWorksheet->getCell('E' . $i)->getValue());
        // $company_cover = trim($objWorksheet->getCell('F' . $i)->getValue());
        $country_name = trim($objWorksheet->getCell('F' . $i)->getValue());
        $company_type_name = trim($objWorksheet->getCell('G' . $i)->getValue());

        $country = $DB->get_record_select('vi_countries', 'name=:name OR name_ko=:nameko OR name_en=:nameen OR name_vi=:namevi', array('name' => $country_name, 'nameko' => $country_name, 'nameen' => $country_name, 'namevi' => $country_name));
        $country_id = $country->id;

        $company_type = $DB->get_record_select('vi_company_types', 'name=:name', array('name' => $company_type_name));
        $company_type_id = $company_type->id;

        $company_address = trim($objWorksheet->getCell('H' . $i)->getValue());
        $firstname = trim($objWorksheet->getCell('I' . $i)->getValue());
        $department = trim($objWorksheet->getCell('J' . $i)->getValue());
        $phone1 = trim($objWorksheet->getCell('K' . $i)->getValue());
        $email = trim($objWorksheet->getCell('L' . $i)->getValue());
        $approval = trim($objWorksheet->getCell('M' . $i)->getValue());

        $usergroup = 'rs';
    } else {
        //    $no = trim($objWorksheet->getCell('A' . $i)->getValue());      //no - 의미없는 값이어서 제외
        $fullname = $objWorksheet->getCell('B' . $i)->getValue();                     //이름
        $username = trim($objWorksheet->getCell('C' . $i)->getValue());               //아이디/이메일주소

        /*
    * 회원유형
        rs - 일반회원
        bp - BOP 회원
        pr - 강사
        ad - 관리자
    */
        $usergroup = trim(strtolower($objWorksheet->getCell('D' . $i)->getValue()));
        $phone1 = trim($objWorksheet->getCell('E' . $i)->getValue());             //휴대폰 번호
        $email = trim($objWorksheet->getCell('F' . $i)->getValue());             //이메일 주소
        $joinstatus = trim($objWorksheet->getCell('G' . $i)->getValue());

    }

    $username = preg_replace("/\s+/", "", strtolower($username));
    $email = preg_replace("/\s+/", "", $email);

    //엑셀값이 비어있나 체크
    if (!empty($username) && $username != '' && !empty($fullname) && $fullname && !empty($usergroup) && $usergroup && !empty($email) && $email) {
        //username으로 사용자가 생성되어있나 체크
        //있으면 중복응로 생성 불가능, 없으면 생성 가능
        $user = $DB->get_record('user', array('username' => $username));

        $user_email = $DB->get_record('user', array('email' => $email));
        if (empty($user) && empty($user_email)) {
            $usernew = new stdClass();
            //신규
            if (empty($user->calendartype)) {
                $user->calendartype = $CFG->calendartype;
            }

            $user->username = $username;
            $user->phone1 = $phone1;
            //$user->phone2 = $usernew->phone2;
            if ($type == 4) {
                $user->phone2 = $phone2;
                $user->department = $department;
            }
            $user->firstname = $fullname;
            $user->lastname = '&nbsp;';
            $user->lang = current_language();
            $user->firstaccess = 0;
            $user->timecreated = time();
            $user->mnethostid = $CFG->mnet_localhost_id;
            $user->secret = random_string(15);
            $user->email = $email;
            $user->password = $email;
            $user->confirmed = 1;   //이메일 인증 했는지 안했는지
            $user->usergroup = $usergroup;
            if ($usergroup == "ad" || $usergroup == "pr") {
                $user->auth = 'manual';
                $plainpassword = $email;
                $user->password = hash_internal_user_password($email, TRUE);

                $user->id = $usernew->id = user_create_user($user, false, false);

                user_add_password_history($user->id, $plainpassword);

                if ($user->id) {
                    $returnvalue->success++;

                    $lmsdatauser = new stdClass();
                    $lmsdatauser->userid = $user->id;
                    $lmsdatauser->phone1 = $user->phone1;
                    $lmsdatauser->joinstatus = 'e';
                    $DB->insert_record('lmsdata_user', $lmsdatauser);

                    if ($usergroup == "ad") {
                        local_user_addpageadmin("manager", "add", $user->id);
                    }
                } else {
                    //값이비었음
                    $returnvalue->empty++;
                }
            } else {

                $user->auth = 'email';
                $user->joinstatus = $joinstatus;

                if ($type == 4) {

                    $user->secret = "CACC";
                    $user->password = $password;
                }

                $emailauth = new local_auth_plugin_email();

                $user->id = $usernew->id = $emailauth->user_signup($user);

                if ($user->id) {
                    $returnvalue->success++;
                } else {
                    //값이비었음
                    $returnvalue->empty++;
                }
            }

            if ($type == 4) {
                $emp = new stdClass();
                $emp->company_name = $company_name;
                // $emp->company_cover = $company_cover;
                $emp->company_address = $company_address;
                $emp->company_type_id = $company_type_id;
                $emp->country_id = $country_id;
                $emp->confirmed = $approval == 'Y' ? 1 : ($approval == 'N' ? -1 : 0);
                $emp->published = $approval == 'Y' ? 1 : 0;
                $emp->approval_date = $approval == 'Y' || $approval == 'N' ? time() : 0;
                $emp->timecreated = time();
                $emp->timemodified = time();
                $emp_id = $DB->insert_record('vi_employers', $emp);

                $admin_emp = new stdClass();
                $admin_emp->employer_id = $emp_id;
                $admin_emp->user_id = $user->id;
                $DB->insert_record('vi_admin_employers', $admin_emp);
            }
        } else {
            //중복
            $returnvalue->begin++;
        }
    }
}
error_log('[END:]' . date('Y-m-d H:i:s') . "]\n\n\n", 3, $CFG->dirroot . '/local/management/users/excel_user.log');

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
