<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/gdlib.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/user/editadvanced_form.php');
require_once($CFG->dirroot . '/user/editlib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->dirroot . '/local/management/users/lib.php');
require_once($CFG->libdir . '/password_compat/lib/password.php');
require_once $CFG->dirroot . '/local/signup/lib.php';
$usernew = new stdClass();
foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    $usernew->$key = $val;
}

$usernew->email = $usernew->email ? $usernew->email:$usernew->email2;

$usernew->username = preg_replace("/\s+/", "", strtolower($usernew->username));
$usernew->newpassword = preg_replace("/\s+/", "", $usernew->newpassword);
$usernew->email = preg_replace("/\s+/", "", $usernew->email);
$usernew->email2 = preg_replace("/\s+/", "", $usernew->email2);
$usernew->phone1 = preg_replace("/\s+/", "", $usernew->phone1);
$file_id = optional_param("file_id", 0, PARAM_INT);
$usernew->lastname = '&nbsp;';
//$usernew->phone1 = $usernew->phone_1.'-'.$usernew->phone_2.'-'.$usernew->phone_3;
$id = $usernew->id;
//$usernew->id = $usernew->luid;
$usernew->phone2 = $usernew->birth_y . '-' . $usernew->birth_m . '-' . $usernew->birth_d;

//사진
$filename = $_FILES['uploadfile']['name'];
$filepath = $_FILES['uploadfile']['tmp_name'];

$type = optional_param('type', 1, PARAM_INT);

$fs = get_file_storage();
if ($usernew->id) {
    $user = $DB->get_record('user', array('id' => $usernew->id), '*', MUST_EXIST);
    $usercontext = context_user::instance($user->id);
    $usernew->timemodified = time();

    if ($usernew->pw_edit == 'on') {
        $usernew->password = trim($usernew->newpassword);
        if (!empty($usernew->password)) {
            $usernew->password = hash_internal_user_password($usernew->password);
        }
    }
    user_update_user($usernew, false, false);

    $t_query = 'select id from {lmsdata_teacher} where userid=:id';
    $teacher = $DB->get_record_sql($t_query, array('id' => $id));
    if ($usernew->usergroup == 'pr') {
        $teachernew = new stdClass();
        $teachernew->timemodified = $usernew->timemodified;
        $teachernew->profile = $usernew->profile;
        $teachernew->profile_vi = $usernew->profile_vi;
        $teachernew->profile_en = $usernew->profile_en;
        $teachernew->comment = $usernew->comment;
        $teachernew->comment_vi = $usernew->comment_vi;
        $teachernew->comment_en = $usernew->comment_en;
        $teachernew->oneline = $usernew->oneline;
        $teachernew->oneline_vi = $usernew->oneline_vi;
        $teachernew->oneline_en = $usernew->oneline_en;
        //기존값 있는지
        if ($teacher) {
            //만약 기존값이 있으면 update
            $teachernew->id = $teacher->id;
            $teachernew->isused = $usernew->isused;
            $DB->update_record('lmsdata_teacher', $teachernew);
        } else {
            //신규값이라면 isnert
            $teachernew->timecreated = $usernew->timemodified;
            $teachernew->userid = $id;
            $teachernew->isused = $usernew->isused;
            $DB->insert_record('lmsdata_teacher', $teachernew);
        }
    } else if ($usernew->usergroup != "pr") {
        //pr이 아니라면 lmsdata_teacher 삭제
        if ($teacher) {
            $DB->delete_records('lmsdata_teacher', array('id' => $teacher->id));
        }


        if ($usernew->usergroup == "ad") {
            local_user_addpageadmin("manager", "add", $usernew->id);
        } else if ($usernew->usergroup != "ad") {
            local_user_addpageadmin("manager", "delete", $id);
        }
    }
    //user_update_user($usernew, false, false);

    $lmsdata_user = $DB->get_record('lmsdata_user', array('userid' => $usernew->id), '*', MUST_EXIST);
    $lmsdatauser = new stdClass();
    $lmsdatauser->id = $lmsdata_user->id;
    $lmsdatauser->usergroup = $usernew->usergroup;
    $lmsdatauser->email2 = $usernew->email2;
    $DB->update_record('lmsdata_user', $lmsdatauser);

    //사진
    // Load user preferences.
    useredit_load_preferences($user);

    // Load custom profile fields data.
    profile_load_data($user);

    // Update mail bounces.
    useredit_update_bounces($user, $usernew);

    // Update forum track preference.
    useredit_update_trackforums($user, $usernew);

    if (!empty($filename)) {
        $context = context_system::instance();

        $fs = get_file_storage();

        if (!empty($_FILES['uploadfile']['tmp_name'])) {
            $file_del = 1;
        }

        //if ($mod == 'edit' && !empty($id) && $file_del == 1) {

        //}

        if (!empty($_FILES['uploadfile']['tmp_name'])) {
            $overlap_files = $DB->get_records('files', array('itemid' => $file_id));
            foreach ($overlap_files as $file) {
                $fs->get_file_instance($file)->delete();
                $icon_files = $DB->get_records('files', array('contextid' => $file->contextid, 'component' => 'user', 'filearea' => 'icon'));
                foreach ($icon_files as $ifile) {
                    $fs->get_file_instance($ifile)->delete();
                }
            }

            //save user picture
            $filename = $_FILES['uploadfile']['name'];
            $filepath = $_FILES['uploadfile']['tmp_name'];

            $draftitemid = file_get_unused_draft_itemid();

            $filerecord = array(
                'contextid' => $usercontext->id,
                'component' => 'user',
                'filearea' => 'draft',
                'itemid' => $draftitemid,
                'filepath' => '/',
                'filename' => $filename,
                'userid' => $usernew->id,
                'license' => 'allrightsreserved'
            );
            $fs = get_file_storage();
            $file = $fs->create_file_from_pathname($filerecord, $filepath);
            $newrev = process_new_icon($usercontext, 'user', 'icon', 0, $filepath);
            $DB->set_field('user', 'picture', $newrev, array('id' => $usernew->id));
        }
    }

    if (!empty($_FILES['uploadfile_2']['tmp_name'])) {
        $overlap_files = $DB->get_records('files', array('contextid' => $usercontext->id, 'component' => 'local_lmsdata', 'filearea' => 'teacher', 'itemid' => $user->id));
        foreach ($overlap_files as $file) {
            $fs->get_file_instance($file)->delete();
            $icon_files = $DB->get_records('files', array('contextid' => $file->contextid, 'component' => 'user', 'filearea' => 'icon'));
        }

        $file_record = array(
            'contextid'   => $usercontext->id,
            'component'   => 'local_lmsdata',
            'filearea'    => 'teacher',
            'itemid'      => $user->id,
            'filepath'    => '/',
            'filename'    => $_FILES['uploadfile_2']['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid'      => $USER->id,
            'author'      => fullname($USER),
            'license'     => 'allrightsreserved',
            'sortorder'   => 0
        );
        //print_object($_FILES['script']['tmp_name']);exit;
        $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile_2']['tmp_name']);
    }
    // Preload custom fields.
    profile_load_custom_fields($USER);

    if ($type == 4) {
        $_u = new stdClass();
        $_u->id = $usernew->id;
        $_u->phone2 = optional_param('phone2', '', PARAM_RAW);
        $_u->department = optional_param('department', '', PARAM_RAW);
        $DB->update_record('user', $_u);

        $employer = $DB->get_record_sql(
            "SELECT e.* 
            FROM {vi_employers} e
            JOIN {vi_admin_employers} ae ON ae.employer_id = e.id
            WHERE ae.user_id=:userid",
            array('userid' => $usernew->id)
        );

        $certificate_path = '';
        $certificate = $_FILES['uploadfile_certificate'];
        if (!empty($certificate['name'])) {
            $context = context_system::instance();
            $fs = get_file_storage();
            if ($employer->company_cover != '') {
                $fileinfo = explode('/', $employer->company_cover);
                $file = $fs->get_file($fileinfo[0], $fileinfo[1], $fileinfo[2], $fileinfo[3], '/', $fileinfo[4]);
                if ($file) {
                    $file->delete();
                }
            }

            $itemid = file_get_unused_draft_itemid();
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_visang',
                'filearea' => 'docs',
                'itemid' => $itemid,
                'filepath' => '/',
                'filename' => $certificate['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id
            );
            $fs->create_file_from_pathname($file_record, $certificate['tmp_name']);

            $certificate_path = implode('/', array($context->id, 'local_visang', 'docs', $itemid, $certificate['name']));
        }

        $emp = new stdClass();
        $emp->id = $employer->id;
        $emp->company_name = $usernew->company_name;
        if ($certificate_path != '') $emp->company_cover = $certificate_path;
        $emp->company_address = required_param('company_address', PARAM_RAW);
        $emp->company_type_id = required_param('company_type_id', PARAM_INT);
        $emp->country_id = required_param('country_id', PARAM_INT);
        $emp->confirmed = $usernew->approval;
        $emp->published = $usernew->approval == 1 ? 1 : 0;
        if($employer->confirmed != $usernew->approval) $emp->approval_date = time();
        $DB->update_record('vi_employers', $emp);
    } else {
        switch ($usernew->usergroup) {
            case 'ad':
                $type = 3;
                break;
            case 'pr':
                $type = 2;
                break;
            default:
                $type = 1;
                break;
        }
    }

    echo '<script type="text/javascript">document.location.href="./form.php?id=' . $id . '&type=' . $type . '"</script>';
} else {
    //신규
    require_once($CFG->dirroot . '/user/profile/lib.php');
    require_once($CFG->dirroot . '/user/lib.php');
    require_once $CFG->dirroot . '/local/signup/lib.php';

    if (empty($user->calendartype)) {
        $user->calendartype = $CFG->calendartype;
    }

    $user->username = $usernew->username;
    $user->email = $usernew->email;
    $user->phone1 = $usernew->phone1;
    $user->phone2 = $usernew->phone2;
    $user->firstname = $usernew->firstname;
    $user->lastname = '&nbsp;';
    $user->lang = current_language();
    // auto confirmed
    $user->confirmed = 1;

    $user->firstaccess = 0;
    $user->timecreated = time();
    $user->mnethostid = $CFG->mnet_localhost_id;
    $user->secret = random_string(15);
    $plainpassword = $usernew->newpassword;
    $user->password = $usernew->newpassword;

    if ($usernew->usergroup == "ad" || $usernew->usergroup == "pr") {
        $user->auth = 'manual';
        $user->password = hash_internal_user_password($usernew->newpassword);
        $user->id = $usernew->id = user_create_user($user, false, false);
        user_add_password_history($user->id, $plainpassword);
        if ($user->id) {
            $lmsdatauser = new stdClass();
            $lmsdatauser->userid = $user->id;
            $lmsdatauser->usergroup = $usernew->usergroup;
            $lmsdatauser->birthday = $user->phone2;
            $lmsdatauser->phone1 = $user->phone1;
            $lmsdatauser->joinstatus = 'e';
            $lmsdatauser->email2 = $usernew->email2;
            $DB->insert_record('lmsdata_user', $lmsdatauser);
            if ($usernew->usergroup == 'pr') {
                $teachernew = new stdClass();
                $teachernew->timemodified = $usernew->timemodified;
                $teachernew->profile = $usernew->profile;
                $teachernew->profile_vi = $usernew->profile_vi;
                $teachernew->profile_en = $usernew->profile_en;
                $teachernew->comment = $usernew->comment;
                $teachernew->comment_vi = $usernew->comment_vi;
                $teachernew->comment_en = $usernew->comment_en;
                $teachernew->timecreated = $usernew->timemodified;
                $teachernew->userid = $user->id;
                $teachernew->isused = $usernew->isused;
                $teachernew->oneline = $usernew->oneline;
                $teachernew->oneline_vi = $usernew->oneline_vi;
                $teachernew->oneline_en = $usernew->oneline_en;

                $DB->insert_record('lmsdata_teacher', $teachernew);
            } else if ($usernew->usergroup == "ad") {
                local_user_addpageadmin("manager", "add", $usernew->id);
            }

            $usercontext = context_user::instance($usernew->id);

            // Update preferences.
            useredit_update_user_preference($usernew);

            // Save custom profile fields data.
            profile_save_data($usernew);

            //save user picture
            if (!empty($_FILES['uploadfile']['tmp_name'])) {
                $draftitemid = file_get_unused_draft_itemid();
                $filerecord = array(
                    'contextid' => $usercontext->id,
                    'component' => 'user',
                    'filearea' => 'draft',
                    'itemid' => $draftitemid,
                    'filepath' => '/',
                    'filename' => $filename,
                    'userid' => $usernew->id,
                    'license' => 'allrightsreserved'
                );

                $fs = get_file_storage();
                $file = $fs->create_file_from_pathname($filerecord, $filepath);

                $newrev = process_new_icon($usercontext, 'user', 'icon', 0, $filepath);

                $DB->set_field('user', 'picture', $newrev, array('id' => $usernew->id));
            }
            // Trigger update/create event, after all fields are stored.
            \core\event\user_created::create_from_userid($usernew->id)->trigger();

            if (!empty($_FILES['uploadfile_2']['tmp_name'])) {
                $file_record = array(
                    'contextid'   => $usercontext->id,
                    'component'   => 'local_lmsdata',
                    'filearea'    => 'teacher',
                    'itemid'      => $user->id,
                    'filepath'    => '/',
                    'filename'    => $_FILES['uploadfile_2']['name'],
                    'timecreated' => time(),
                    'timemodified' => time(),
                    'userid'      => $USER->id,
                    'author'      => fullname($USER),
                    'license'     => 'allrightsreserved',
                    'sortorder'   => 0
                );
                //print_object($_FILES['script']['tmp_name']);exit;
                $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['uploadfile_2']['tmp_name']);
            }
        }
    } else {
        $user->auth = 'email';
        if ($type == 4) {
            $user->confirmed = 1;
            $user->secret = "CACC";
        }
        $emailauth = new local_auth_plugin_email();
        $user->id = $usernew->id = $emailauth->user_signup($user);
    }

    if ($type == 4) {
        $_u = new stdClass();
        $_u->id = $usernew->id;
        $_u->phone2 = optional_param('phone2', '', PARAM_RAW);
        $_u->department = optional_param('department', '', PARAM_RAW);
        $DB->update_record('user', $_u);

        $certificate_path = '';
        $certificate = $_FILES['uploadfile_certificate'];
        if (!empty($certificate['name'])) {
            $context = context_system::instance();
            $fs = get_file_storage();

            $itemid = file_get_unused_draft_itemid();
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_visang',
                'filearea' => 'docs',
                'itemid' => $itemid,
                'filepath' => '/',
                'filename' => $certificate['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $usernew->id
            );
            $fs->create_file_from_pathname($file_record, $certificate['tmp_name']);

            $certificate_path = implode('/', array($context->id, 'local_visang', 'docs', $itemid, $certificate['name']));
        }

        $emp = new stdClass();
        $emp->company_name = $usernew->company_name;
        if ($certificate_path != '') $emp->company_cover = $certificate_path;
        $emp->company_address = required_param('company_address', PARAM_RAW);
        $emp->company_type_id = required_param('company_type_id', PARAM_INT);
        $emp->country_id = required_param('country_id', PARAM_INT);
        $emp->confirmed = $usernew->approval;
        $emp->published = $usernew->approval == 1 ? 1 : 0;
        $emp->approval_date = time();
        $emp->timecreated = time();
        $emp->timemodified = time();
        $emp_id = $DB->insert_record('vi_employers', $emp);

        $admin_emp = new stdClass();
        $admin_emp->employer_id = $emp_id;
        $admin_emp->user_id = $usernew->id;
        $DB->insert_record('vi_admin_employers', $admin_emp);
    } else {
        switch ($usernew->usergroup) {
            case 'ad':
                $type = 3;
                break;
            case 'pr':
                $type = 2;
                break;
            default:
                $type = 1;
                break;
        }
    }

    echo '<script type="text/javascript">document.location.href="./index.php?type=' . $type . '"</script>';
}
