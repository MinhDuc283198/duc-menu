<?php

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/local/management/users/lib.php');

$current_language = current_language();
$thistime = time();

$page = optional_param('page', 1, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);

$tabview = optional_param('tabview', 1, PARAM_INT);

$auth2 = optional_param_array('auth', null, PARAM_RAW);
$joinstatus = optional_param_array('joinstatus', null, PARAM_RAW);
$usergroup2 = optional_param_array('usergroup', null, PARAM_RAW);
$searchtext = trim($searchtext);
$title = '';
$menu_txt = '';
if ($type == 1) {
    //rs, bp
    $title = get_string('user:title1', 'local_management');

    switch($tabview) {
        case 1: 
            $title .= " (Total Member)";
            break;
        case 2: 
            $title .= " (Free Member(General))";
            $usergroup2 = ['rs'];
            break;
        case 3: 
            $title .= " (Free Member(BOP))";
            $usergroup2 = ['bp'];
            break;
        case 4: 
            $title .= " (Paid Member(General))";
            $usergroup2 = ['rs'];
            break;
        case 5: 
            $title .= " (Paid Member(BOP))";
            $usergroup2 = ['bp'];
            break;
        case 6: 
            $title .= " (Active Paid Member(General))";
            $usergroup2 = ['rs'];
            break;
        case 7: 
            $title .= " (Active Paid Member(BOP))";
            $usergroup2 = ['bp'];
            break;
    }

    $menu_txt = 'normal';
    if ($usergroup2) {
        foreach ($usergroup2 as $key => $ug2) {
            $usergroup[$key] = ' lu.usergroup = "' . $ug2 . '" ';
        }
    } else {
        $usergroup = array('lu.usergroup = "rs"', 'lu.usergroup = "bp"');
    }
} else if ($type == 2) {
    //pr
    $title = get_string('user:title2', 'local_management');
    $menu_txt = 'teacher';
    $usergroup = array('lu.usergroup = "pr"');
} else if ($type == 4) {
    // company users
    $title = get_string('user:title4', 'local_management');
    $menu_txt = 'companyusers';
    // $usergroup = array('lu.usergroup IS NULL', 'lu.usergroup = "rs"', 'lu.usergroup = "bp"', 'lu.usergroup = "ad"');
    $usergroup = array('1=1');
} else {
    //ad
    $title = get_string('user:title3', 'local_management');
    $menu_txt = 'admin';
    $usergroup = array('lu.usergroup = "ad"');
}

$pagesettings = array(
    'title' => $title,
    'heading' => $title,
    'subheading' => '',
    'menu' => $menu_txt,
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

if ($auth2) {
    foreach ($auth2 as $key => $au) {
        if ($au == 'email') {
            $auth[$key] = ' ( lu.sns = "" OR lu.sns = "email" )';
        } else {
            $auth[$key] = ' lu.sns = "' . $au . '" ';
        }
        // if ($au == 'email') {
        //     $auth[$key] = ' ( u.auth = "' . $au . '" OR u.auth = "manual" )';
        // } else {
        //     $auth[$key] = ' u.auth = "' . $au . '" ';
        // }
    }
}

$sql_select = "SELECT IF(em.id IS NULL, CONCAT('user_', u.id), CONCAT('employer_', em.id)) AS key_id, u.id, u.auth, u.confirmed, u.username, u.firstname, u.email, u.phone1, u.phone2, u.timecreated, u.lastaccess, lu.usergroup, lu.userid,lu.statuscode,lu.joinstatus, lu.email2, lu.sns, em.id AS employer_id ";
$sql_select_count = "SELECT COUNT(DISTINCT u.id) ";

$sql_from = "FROM {user} u
JOIN {lmsdata_user} lu ON lu.userid = u.id
LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id
LEFT JOIN {vi_employers} em ON e.employer_id = em.id";

$conditions = array("u.deleted = 0");
$params = array();

if($type == 1) {
    switch($tabview) {
        case 2: 
            $conditions[] = "NOT EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                        )";
            break;
        case 3: 
            $conditions[] = "NOT EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                        )";
            break;
        case 4: 
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) AND lc.learningend < '{$thistime}' 
                        )";
            break;
        case 5: 
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) AND lc.learningend < '{$thistime}' 
                        )";
            break;
        case 6: 
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) AND lc.learningend >= '{$thistime}'
                        )";
            break;
        case 7: 
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) AND lc.learningend >= '{$thistime}'
                        )";
            break;
    }
}

if ($type == 4) {
    // company users
    // $conditions[] = 'e.employer_id IS NOT NULL';
    $sql_from = "FROM {vi_employers} em
                LEFT JOIN {vi_admin_employers} e ON e.employer_id = em.id
                LEFT JOIN {user} u ON u.id = e.user_id
                LEFT JOIN {lmsdata_user} lu ON lu.userid = u.id";
    // $conditions = array("(u.id IS NULL OR u.deleted = 0)");
    $conditions = array("1=1");
    $sql_select_count = "SELECT COUNT(DISTINCT em.id) ";
} else {
    $conditions[] = 'e.employer_id IS NULL';
}

$sql_where = ' WHERE ' . implode(' AND ', $conditions);

if ($learningstart_str) {
    $learningstart = str_replace(".", "-", $learningstart_str);
    $learningstart = strtotime($learningstart);
}
if ($learningend_str) {
    $learningend = str_replace(".", "-", $learningend_str);
    $learningend = strtotime($learningend) + 86399;
}

if ($learningstart) {
    $sql_where_array[] = " u.timecreated >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if ($learningend) {
    $sql_where_array[] = " u.timecreated <= :learningend ";
    $params['learningend'] = $learningend;
}

if (!empty($sql_where_array)) {
    $sql_where .= ' AND ' . implode(' and ', $sql_where_array);
} else {
    $sql_where .= '';
}

if ($usergroup) {
    $aa = ' AND (' . implode(' OR ', $usergroup);
    $aa .= ')';
    $sql_where .= $aa;
}
if ($auth) {
    $aa = ' AND (' . implode(' OR ', $auth);
    $aa .= ')';
    $sql_where .= $aa;
}

// joinstatus
if(is_array($joinstatus)) {
    $_site = [];
    if(in_array('elearning', $joinstatus)) {
        array_push($_site, 'lu.joinstatus = "e"');
        array_push($_site, 'lu.joinstatus = ""');
    }

    if(in_array('job', $joinstatus)) {
        array_push($_site, 'lu.joinstatus = "j"');
    }

    if(count($_site)) {
        $sql_where .= ' AND ('.implode(' OR ', $_site).') ';
    }
}

if ($searchtext) {
    $aa = ' AND (u.username like :username OR u.firstname like :firstname OR u.email like :email OR u.phone1 like :phone1 OR em.company_name like :company_name)';
    $params['username'] = '%' . $searchtext . '%';
    $params['firstname'] = '%' . $searchtext . '%';
    $params['email'] = '%' . $searchtext . '%';
    $params['phone1'] = '%' . $searchtext . '%';
    $params['company_name'] = '%' . $searchtext . '%';
    $sql_where .= $aa;
}
//정렬기능 추가 
$sql_sort = "ORDER BY u.timecreated DESC";
$orderarr = array("usernameorder" => "u.username", "nameoreder" => "u.firstname ", "birthoreder" => "u.phone2", "timecreatedoreder" => "u.timecreated", "lastoreder" => "u.lastaccess", "emailoreder" => "u.email", "comoreder" => "lu.usergroup");
//print_object($orderarr);exit;
if (empty($ordertype) && !empty($orderitem)) {
    $sql_sort = "ORDER BY $orderarr[$orderitem] DESC";
    if ($orderarr[$orderitem] == 'lu.usergroup') {
        $sql_sort = "ORDER BY FIELD(lu.usergroup,'ad','pr','bp','rs')";
    }
} else if ($ordertype == 'up') {
    $sql_sort = "ORDER BY $orderarr[$orderitem] ASC";
    if ($orderarr[$orderitem] == 'lu.usergroup') {
        $sql_sort = "ORDER BY FIELD(lu.usergroup,'rs','bp','pr','ad')";
    }
}

$users = $DB->get_records_sql("$sql_select $sql_from $sql_where $sql_sort", $params);
$totalusers = $DB->count_records_sql("$sql_select_count $sql_from $sql_where", $params);

if ($type == 4) {
    $fields = array(
        get_string('number', 'local_management'),
        get_string('user:member_type', 'local_management'),
        get_string('user:id', 'local_management'),
        get_string('user:company_name', 'local_management'),
        get_string('user:business_registration_number', 'local_management'),
        // get_string('user:registrationcertificate', 'local_management'),
        '한국어 활용수준',
        get_string('user:com_country', 'local_management'),
        get_string('user:employers_company_type', 'local_management'),
        get_string('user:com_address', 'local_management'),
        get_string('user:com_contact_name', 'local_management'),
        get_string('user:com_department', 'local_management'),
        get_string('user:com_contact_number', 'local_management'),
        get_string('user:com_contact_email', 'local_management'),
        get_string('user:request_date', 'local_management'),
        get_string('user:approval_date', 'local_management'),
        get_string('user:last', 'local_management'),
        get_string('user:approval', 'local_management'),
    );
} else {
    $fields = array(
        get_string('number', 'local_management'),
        get_string('user:jointype', 'local_management'),
        get_string('user:membertype', 'local_management'),
        get_string('user:id', 'local_management'),
        get_string('user:name', 'local_management'),
        get_string('user:birth', 'local_management'),
        get_string('user:email', 'local_management'),
        get_string('user:phone', 'local_management'),
        $type == 1 && in_array('job', $joinstatus) ? '한국어 활용수준':'',
        get_string('user:timecreated', 'local_management'),
        get_string('user:site', 'local_management'),
        get_string('user:last', 'local_management'),
        get_string('user:auth', 'local_management'),
        get_string('user:sharing', 'local_management'),
        ($type == 2) ? get_string('user:activity', 'local_management') : '',
    );
}

$date = date('Y-m-d', time());
$filename = '회원관리_' . $date . '.xls';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);

$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');
$col = 0;
foreach ($fields as $fieldname) {
    if($filename != '') {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }
}

$row = 1;
$num = 1;
foreach ($users as $user) {
    $col = 0;
    $worksheet[0]->write($row, $col++, $num++);

    if ($type == 4) {
        $employer = $DB->get_record_sql(
            "SELECT e.* 
            FROM {vi_employers} e
            WHERE e.id=:employerid",
            array('employerid' => $user->employer_id)
        );

        $worksheet[0]->write($row, $col++, get_string("user:company", 'local_management'));
        $worksheet[0]->write($row, $col++, $user->username);
        $worksheet[0]->write($row, $col++, $employer->company_name);
        $worksheet[0]->write($row, $col++, $user->phone2);

        // 한국어 활용 수준
        $job_need_korean_level = $DB->get_record_sql(
            "SELECT j.* 
            FROM {vi_jobs} j
            WHERE j.employer_id=:employerid AND j.korean_level_id > 1
            ORDER BY j.enddate DESC",
            array('employerid' => $employer->id)
        );
        $korean_level = '-';
        if($job_need_korean_level){
            $_p = $DB->get_record('vi_korean_levels', array('id' => $job_need_korean_level->korean_level_id));
            if($_p) {
                switch ($current_language) {
                    case 'ko':
                        $korean_level = $_p->name_ko;
                        break;
                    case 'en':
                        $korean_level = $_p->name_en;
                        break;
                    
                    default:
                        $korean_level = $_p->name_vi;
                        break;
                }
            }
        }
        $worksheet[0]->write($row, $col++, $korean_level);
        // 한국어 활용 수준

        // $worksheet[0]->write($row, $col++, $employer->company_cover);
        $country = $DB->get_record('vi_countries', array('id' => $employer->country_id), 'name_' . current_language() . ' AS name');
        $worksheet[0]->write($row, $col++, $country->name);

        $company_type = $DB->get_record('vi_company_types', array('id' => $employer->company_type_id), 'name');
        $worksheet[0]->write($row, $col++, $company_type->name);

        $worksheet[0]->write($row, $col++, $employer->company_address);
        $worksheet[0]->write($row, $col++, $user->firstname);
        $worksheet[0]->write($row, $col++, $user->department);
        $worksheet[0]->write($row, $col++, $user->phone1);
        $worksheet[0]->write($row, $col++, $user->email);
        $worksheet[0]->write($row, $col++, $user->timecreated ? date('Y.m.d', $user->timecreated) : '');
        $worksheet[0]->write($row, $col++, $employer->approval_date ? date('Y.m.d', $employer->approval_date) : '');
        $lastaccess = get_string('user:noconnection', 'local_management');
        if ($user->lastaccess) {
            $lastaccess = userdate($user->lastaccess, '%Y.%m.%0d %H:%M');
        }
        $worksheet[0]->write($row, $col++, $lastaccess);
        $approval = '';
        switch ($employer->confirmed) {
            case 0:
                // $approval = get_string('user:approval_pending', 'local_management');
                $approval = '';
                break;
            case 1:
                // $approval = get_string('user:approval_accepted', 'local_management');
                $approval = 'Y';
                break;
            case -1:
                // $approval = get_string('user:approval_notaccepted', 'local_management');
                $approval = 'N';
                break;
        }
        $worksheet[0]->write($row, $col++, $approval);
    } else {
        // if ($user->auth == 'manual' or $user->auth == 'email') {
        //     $user->auth = get_string('user:email', 'local_management');
        // }
        // $worksheet[0]->write($row, $col++, $user->auth);
        if ($user->sns == '' or $user->sns == 'email') {
            $user->sns = get_string('user:email', 'local_management');
        }
        $worksheet[0]->write($row, $col++, $user->sns);
        $auth_array = local_management_users_auth_array();
        foreach ($auth_array as $key => $val) {
            if ($key == $user->usergroup) {
                $authname = get_string("user:$key", 'local_management');
            }
        }
        $worksheet[0]->write($row, $col++, $authname);
        $worksheet[0]->write($row, $col++, $user->username);
        $worksheet[0]->write($row, $col++, $user->firstname);
        $worksheet[0]->write($row, $col++, $user->phone2);
        $worksheet[0]->write($row, $col++, $user->email);
        $worksheet[0]->write($row, $col++, $user->phone1."\r");

        //한국어 활용 수준
        if($type == 1 && in_array('job', $joinstatus)) {
            $job_need_korean_level = $DB->get_record_sql(
                "SELECT l.* 
                FROM {vi_user_korean_levels} l
                WHERE l.user_id=:userid AND l.korean_level_id > 1",
                array('userid' => $user->id)
            );
            $korean_level = '-';
            if($job_need_korean_level){
                $_p = $DB->get_record('vi_korean_levels', array('id' => $job_need_korean_level->korean_level_id));
                if($_p) {
                    switch ($current_language) {
                        case 'ko':
                            $korean_level = $_p->name_ko;
                            break;
                        case 'en':
                            $korean_level = $_p->name_en;
                            break;
                        
                        default:
                            $korean_level = $_p->name_vi;
                            break;
                    }
                }
            }
            
            $worksheet[0]->write($row, $col++, $korean_level);
        }
        //한국어 활용 수준

        $worksheet[0]->write($row, $col++, date('Y.m.d', $user->timecreated));
        if ($user->joinstatus == "e" || $user->joinstatus == "") {
            $joinstatus_txt = get_string('user:elearning', 'local_management');
        } else {
            $joinstatus_txt = get_string('jobs', 'local_management');
        }
        $worksheet[0]->write($row, $col++, $joinstatus_txt);
        $lastaccess = get_string('user:noconnection', 'local_management');
        if ($user->lastaccess) {
            $lastaccess = userdate($user->lastaccess, '%Y.%m.%0d %H:%M');
        }
        $worksheet[0]->write($row, $col++, $lastaccess);
        if ($user->confirmed == 1) {
            $worksheet[0]->write($row, $col++, 'Y');
        } else {
            $worksheet[0]->write($row, $col++, 'N');
        }
        if ($user->statuscode == "1") {
            $statuscode_txt = 'Y';
        } else {
            $statuscode_txt = 'N';
        }
        $worksheet[0]->write($row, $col++, $statuscode_txt);

        if ($type == 2) {
            $teacher = $DB->get_record('lmsdata_teacher', array('userid' => $user->id));
            switch ($teacher->isused) {
                case 0:
                    $isused_txt = get_string('user:inactivity', 'local_management');
                    break;
                case 1:
                    $isused_txt = get_string('user:activity1', 'local_management');
                    break;
            }
        }
        $worksheet[0]->write($row, $col++, $isused_txt);
    }

    $row++;
}

$workbook->close();
die;
