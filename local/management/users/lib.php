<?php

function local_management_users_paing($page, $perpage, $tcount, $url,$search, $orderitem , $ordertype, $block = 10 ) {
    $pagedata = '';
    
    $totalcount = $tcount;
    if ($totalcount > 0) {
        $pageNum = ceil($totalcount / $perpage);
        $blockNum = ceil($pageNum / $block);
        $nowBlock = ceil($page / $block);
        $s_page = ($nowBlock * $block) - ($block - 1);
        if ($s_page <= 1) {
            $s_page = 1;
        }
        $e_page = $nowBlock * $block;
        if ($pageNum <= $e_page) {
            $e_page = $pageNum;
        }
        
        $pagedata = '<ul class="pagination">';
        
        if ($nowBlock != 1) {
            $pagedata .= '<li class = "page-item prev"><a class="last" href="' . $url . '&page=' . ($s_page - 1 ) .'&perpage=' . ($perpage) .'&search=' . ($search ) .'&orderitem=' . ($orderitem ) .'&ordertype=' . ($ordertype) . '" page=' . ($s_page - 1 ) . '>«</a></li>';
        }
        for ($p = $s_page; $p <= $e_page; $p++) {
            if ($p == $page) {
                $pagedata .= ' <li class = "page-item active"><a class = "page-link">' . $p . '</a></li>';
            } else {
                $pagedata .= '<li class = "page-item"><a class = "page-link" page=' . $p . ' href = "' . $url .'&page=' . ($p ) .  '&perpage=' . $perpage  . '&search=' . ($search ) .'&orderitem=' . ($orderitem ) .'&ordertype=' . ($ordertype) .'">' . $p . '</a></font>';
            }
        }
        if ($nowBlock != $blockNum) {
            $pagedata .= '<li class = "page-item next"><a class="last" href="' . $url . '&page=' . ($e_page + 1) . '&perpage=' . ($perpage) .'&search=' . ($search ).'&orderitem=' . ($orderitem ) .'&ordertype=' . ($ordertype) .'" page=' . ($e_page + 1) . '>»</a></li>';
        }
        
        $pagedata .= '</ul>';
    }
    return $pagedata;
}


function local_management_users_auth_array(){
    return array(
        'ad' => '관리자',
        'pr' => '강사',
        'bp' => 'BOP회원',
        'rs' => '일반'
    );
}
function local_user_addpageadmin($type, $action, $userid) {
    global $DB;
    $roleid = $DB->get_field('role', 'id', array('shortname' => $type));
    if ($roleid) {
        if ($action == 'add') {
            $return = role_assign($roleid, $userid, 1);
        } else if ($action == 'delete') {
            $return = role_unassign($roleid, $userid, 1);
        }
    }
}