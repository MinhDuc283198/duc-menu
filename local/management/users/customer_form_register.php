<?php
/**
 * 콘텐츠 관리 리스트 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');


$pagesettings = array(
    'title' => l("form.register_form_admin_title"),
    'heading' => l("form.register_form_admin_title"),
    'subheading' => '',
    'menu' => 'contentsI',
    'css' => array(),
    'nav_bar' => array()
);


// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
require_login();

$context = context_system::instance();

require_once ($CFG->dirroot .  '/local/management/lib/contents_lib.php');

$context = context_system::instance();

$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);

$start = $page * $perpage;

$operator = "";

$flag = false;

if($_GET['search']) {

    $flag = true;

    $operator .= " WHERE (m_customer_register.email LIKE '%". $_GET['search'] . "%'";
    $operator .= " OR m_customer_register.name LIKE '%". $_GET['search'] . "%'";
    $operator .= " OR m_customer_register.phone LIKE '%". $_GET['search'] . "%')";
}

if($_GET['time_start']) {
    
    $dtime = DateTime::createFromFormat("Y.m.d", $_GET['time_start']);
    $timestamp = $dtime->getTimestamp();
    
    if($flag) {
        $operator .=  " AND m_customer_register.created_at > $timestamp";
    } else {
        $operator .=  " WHERE m_customer_register.created_at > $timestamp";
    }

    $flag = true;

}

if($_GET['end_til']) {
    $dtime = DateTime::createFromFormat("Y.m.d", $_GET['end_til']);
    $timestamp = $dtime->getTimestamp();

    if($flag) {
        $operator .=  " AND m_customer_register.created_at < $timestamp";
    } else {
        $operator .=  " WHERE m_customer_register.created_at < $timestamp";
    }
    $flag = true;

}

$count = $DB->count_records_sql("SELECT COUNT(*) FROM m_customer_register LEFT JOIN m_lmsdata_class ON m_customer_register.classid = m_lmsdata_class.id" . $operator);

if ($start > $count) {
    $page = 0;
    $start = 0;
}

$sql = "select m_customer_register.*, m_lmsdata_class.en_title from m_customer_register LEFT JOIN m_lmsdata_class ON m_customer_register.classid = m_lmsdata_class.id " . $operator . " ORDER BY m_customer_register.id DESC";

$customer_info = $DB->get_records_sql($sql, null , $start, $perpage);

$PAGE->set_context($context);

$query_string = $_SERVER['QUERY_STRING'];

parse_str($query_string, $query_string);

$paginate  = '';

for($p = 0; $p <= $count / $perpage; $p++) {

    unset($query_string['page']);
    $query_string['page'] = $p;

    $output = implode('&', array_map(
        function ($v, $k) { return sprintf("%s=%s", $k, $v); },
        $query_string,
        array_keys($query_string)
    ));
    $paginate .= "<a href='?". $output ."'>$p</a>";
}

include_once($CFG->dirroot.'/local/management/header.php');
?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
            <form id="frm_content_search" action="" class="search_area">
                <div>
                    <label><?php echo get_string('timecreated','local_management')?></label>
                    <input type="text" title="<?php echo get_string('time','local_management')?>" name="time_start" class="w_120 date s_date" value="<?php echo $_GET['time_start']?>" placeholder="<?php echo get_string('click', 'local_management'); ?>  " >
                    <span class="dash">~</span> 
                    <input type="text" title="<?php echo get_string('time','local_management')?>" name="end_til" class="w_120 date e_date" value="<?php echo $_GET['end_til'] ?>" placeholder="<?php echo get_string('click', 'local_management'); ?>  " > 
                </div>
                <label><?php echo get_string('search','local_management')?></label>
                <input type="text" title="search" name="search"  id="search" value="<?php echo $_GET['search'] ?>" class="search-text">
                <input type="submit" class="search_btn" id="searchbtn" value="<?php echo get_string('search', 'local_jinoboard'); ?>">
                <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>"/>   
            </form>
            <span><?php echo get_string('total','local_management',$count)?></span>
            
            <span><input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/></span>      
           <table>
                <tr>
                    <th><?php echo l('form.register_form_index'); ?></th>
                    <th><?php echo l('form.register_form_name'); ?></th>
                    <th><?php echo l('form.register_form_email'); ?></th>
                    <th><?php echo l('form.register_form_phone'); ?></th>
                    <th>Created at</th>
                    <th>Course Name</th>
                </tr>
                <?php if(empty($customer_info)) { ?>
                <h3>No Record</h3>
                <?php }else { ?>
                <?php foreach($customer_info as $key => $info) { ?>
                <tr>
                    <td><?php echo $key ?></td>
                    <td><?php echo $info->name ?></td>
                    <td><?php echo $info->email ?></td>
                    <td><?php echo $info->phone ?></td>
                    <td><?php echo date('Y/m/d H:i:s', $info->created_at) ?></td>
                    <td><?php echo $info->en_title ?></td>
                </tr>
                <?php } ?>
                <?php } ?>
           </table>
           <div class="pagination">
                <?php echo $paginate ?>
           </div>
        </div>
    </div>
    </div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>
<script>

    $(document).ready(function () {
        $('#reset').on('click',function(){
            $('#content_cata1').val('0');
            $('#content_cata2').val('0');
            $('#copyright').val('0');
            $('.s_date').val('');
            $('.e_date').val('');
            $("input:checkbox[name='share_yn[]']").attr("checked", false);
            $("input:checkbox[name='con_type[]']").attr("checked", false);
            $('input[name=search]').val('');
            location.reload();
        });
        
    });
    
    start_datepicker_c('', '');
    end_datepicker_c('', '');
</script>

