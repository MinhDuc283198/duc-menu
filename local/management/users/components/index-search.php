<form name="course_search" id="course_search" class="search_area" action="index.php" method="get">
    <input type="hidden" name="page" value="1">
    <input type="hidden" name="perpage" value="<?php echo $perpage ?>">
    <input type="hidden" name="type" value="<?php echo $type ?>">
    <input type="hidden" name="orderitem" value="<?php echo $orderitem ?>" />
    <input type="hidden" name="ordertype" value="<?php echo $ordertype ?>" />
    <input type="hidden" name="tabview" value="<?php echo $tabview ?>" />
    <?php if ($type == 1) { ?>
        <div>
            <label><?php echo get_string('user:jointype', 'local_management') ?></label>
            <?php foreach ($auth_array as $iakey => $iaval) { ?>
                <label class="mg-bt10"><input type="checkbox" name="auth[]" value="<?php echo $iakey ?>" <?php foreach ($auth2 as $ischeck) {
                        if ($ischeck == $iakey) {
                            echo "checked";
                        }
                    } ?>><?php echo $iaval ?></label>
            <?php } ?>
        </div>
        <div>
            <label>가입구분:</label>
            <label class="mg-bt10"><input type="checkbox" name="joinstatus[]" value="elearning" <?php echo in_array('elearning', $joinstatus) ? 'checked' : '' ?>>
                <?php echo get_string('user:elearning', 'local_management'); ?>
            </label>
            <label class="mg-bt10"><input type="checkbox" name="joinstatus[]" value="job" <?php echo in_array('job', $joinstatus) ? 'checked' : '' ?>>
                <?php echo get_string('jobs', 'local_management') ?>
            </label>
        </div>
        <div>
            <label><?php echo get_string('user:membertype', 'local_management') ?></label>
            <?php foreach ($usergroup_array as $iakey => $iaval) { ?>
                <label class="mg-bt10"><input type="checkbox" name="usergroup[]" value="<?php echo $iakey ?>" <?php foreach ($usergroup2 as $ischeck) {
                        if ($ischeck == $iakey) {
                            echo "checked";
                        }
                    } ?>><?php echo $iaval ?></label>
            <?php } ?>
        </div>

        <div>
            <label><?php echo get_string('user:timecreated', 'local_management') ?></label>
            <input type="text" title="<?php echo get_string('time', 'local_management') ?>" name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?>  ">
            <span class="dash">~</span>
            <input type="text" title="<?php echo get_string('time', 'local_management') ?>" name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str ?>" placeholder="<?php echo get_string('click', 'local_management') ?>  ">
        </div>
    <?php } ?>
    <div>
        <label><?php echo get_string('search', 'local_management') ?></label>
        <input type="text" title="search" name="searchtext" value="<?php echo $searchtext ?>" placeholder="<?php echo get_string('user:searchtext', 'local_management') ?>" class="search-text w100">
        <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_management') ?>">
        <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management') ?>">
    </div>
</form>