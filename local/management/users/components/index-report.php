<div>
    <a class="stat-box bg-aqua" href="index.php?type=<?php echo $type; ?>&tabview=1">
        <h3>
            <?php
            echo number_format($stat_total_members);
            ?>
        </h3>
        Total Member<br />&nbsp;
    </a>
    <a class="stat-box bg-green" href="index.php?type=<?php echo $type; ?>&tabview=2">
        <h3>
            <?php
            echo number_format($stat_total_free_rs_members);
            ?>
        </h3>
        Free Member<br />(General)
    </a>
    <a class="stat-box bg-green" href="index.php?type=<?php echo $type; ?>&tabview=3">
        <h3>
            <?php
            echo number_format($stat_total_free_bp_members);
            ?>
        </h3>
        Free Member<br />(BOP)
    </a>
    <a class="stat-box bg-yellow" href="index.php?type=<?php echo $type; ?>&tabview=4">
        <h3>
            <?php
            echo number_format($stat_total_paid_rs_members);
            ?>
        </h3>
        Paid Member<br />(General)
    </a>
    <a class="stat-box bg-yellow" href="index.php?type=<?php echo $type; ?>&tabview=5">
        <h3>
            <?php
            echo number_format($stat_total_paid_bp_members);
            ?>
        </h3>
        Paid Member<br />(BOP)
    </a>
    <a class="stat-box bg-red" href="index.php?type=<?php echo $type; ?>&tabview=6">
        <h3>
            <?php
            echo number_format($stat_total_active_paid_rs_members);
            ?>
        </h3>
        Active Paid Member<br />(General)
    </a>
    <a class="stat-box bg-red" href="index.php?type=<?php echo $type; ?>&tabview=7">
        <h3>
            <?php
            echo number_format($stat_total_active_paid_bp_members);
            ?>
        </h3>
        Active Paid Member<br />(BOP)
    </a>
</div>