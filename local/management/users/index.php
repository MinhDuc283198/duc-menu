<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once($CFG->dirroot . '/local/management/users/lib.php');

$current_language = current_language();
$thistime = strtotime(date('Y-m-d'));

$page = optional_param('page', 1, PARAM_INT);
$type = optional_param('type', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);

$tabview = optional_param('tabview', 1, PARAM_INT);

$employerid = optional_param('employerid', 0, PARAM_INT);
$approval = optional_param('approval', 0, PARAM_INT);
if ($employerid != 0) {
    $_employer = $DB->get_record('vi_employers', array('id' => $employerid));

    $emp = new stdClass();
    $emp->id = $employerid;
    $emp->confirmed = $approval;
    $emp->published = $approval == 1 ? 1 : 0;
    if ($_employer->confirmed != $approval) $emp->approval_date = time();
    $DB->update_record('vi_employers', $emp);
    redirect(new moodle_url($CFG->wwwroot . '/local/management/users/index.php', array(
        'type' => 4,
        'page' => $page,
        'perpage' => $perpage,
        'orderitem' => $orderitem,
        'ordertype' => $ordertype,
        'searchtext' => $searchtext,
    )));
}

$auth2 = optional_param_array('auth', null, PARAM_RAW);
$joinstatus = optional_param_array('joinstatus', null, PARAM_RAW);
$usergroup2 = optional_param_array('usergroup', null, PARAM_RAW);
$searchtext = trim($searchtext);
$title = '';
$menu_txt = '';
if ($type == 1) {
    //rs, bp
    $title = get_string('user:title1', 'local_management');

    switch($tabview) {
        case 1: 
            $title .= " (Total Member)";
            break;
        case 2: 
            $title .= " (Free Member(General))";
            $usergroup2 = ['rs'];
            break;
        case 3: 
            $title .= " (Free Member(BOP))";
            $usergroup2 = ['bp'];
            break;
        case 4: 
            $title .= " (Paid Member(General))";
            $usergroup2 = ['rs'];
            break;
        case 5: 
            $title .= " (Paid Member(BOP))";
            $usergroup2 = ['bp'];
            break;
        case 6: 
            $title .= " (Active Paid Member(General))";
            $usergroup2 = ['rs'];
            break;
        case 7: 
            $title .= " (Active Paid Member(BOP))";
            $usergroup2 = ['bp'];
            break;
    }

    $menu_txt = 'normal';
    if ($usergroup2) {
        foreach ($usergroup2 as $key => $ug2) {
            $usergroup[$key] = ' lu.usergroup = "' . $ug2 . '" ';
        }
    } else {
        $usergroup = array('lu.usergroup = "rs"', 'lu.usergroup = "bp"');
    }
} else if ($type == 2) {
    //pr
    $title = get_string('user:title2', 'local_management');
    $menu_txt = 'teacher';
    $usergroup = array('lu.usergroup = "pr"');
} else if ($type == 4) {
    // company users
    $title = get_string('user:title4', 'local_management');
    $menu_txt = 'companyusers';
    // $usergroup = array('lu.usergroup IS NULL', 'lu.usergroup = "rs"', 'lu.usergroup = "bp"', 'lu.usergroup = "ad"');
    $usergroup = array('1=1');
} else {
    //ad
    $title = get_string('user:title3', 'local_management');
    $menu_txt = 'admin';
    $usergroup = array('lu.usergroup = "ad"');
}

$pagesettings = array(
    'title' => $title,
    'heading' => $title,
    'subheading' => '',
    'menu' => $menu_txt,
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

if ($auth2) {
    foreach ($auth2 as $key => $au) {
        if ($au == 'email') {
            $auth[$key] = ' ( lu.sns = "" OR lu.sns = "email" )';
        } else {
            $auth[$key] = ' lu.sns = "' . $au . '" ';
        }
    }
}

if ($type != 4) {
    $sql_select = "SELECT IF(em.id IS NULL, CONCAT('user_', u.id), CONCAT('employer_', em.id)) AS key_id, 
                COUNT(la.id) as login_number,
                u.id, u.auth, u.confirmed, u.username, u.firstname, 
                u.email, u.phone1, u.phone2, u.timecreated, u.lastaccess, lu.usergroup, lu.sex, lu.userid,lu.statuscode,lu.joinstatus, lu.email2, lu.sns, 
                em.id AS employer_id ";
    $sql_select_count = "SELECT COUNT(DISTINCT u.id) ";

    $sql_from = "FROM {user} u
    JOIN {lmsdata_user} lu ON lu.userid = u.id
    LEFT JOIN {user_lastaccess} la ON la.userid = u.id
    LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id
    LEFT JOIN {vi_employers} em ON e.employer_id = em.id";
} else {
    $sql_select = "SELECT IF(em.id IS NULL, CONCAT('user_', u.id), CONCAT('employer_', em.id)) AS key_id, 
                u.id, u.auth, u.confirmed, u.username, u.firstname, 
                u.email, u.phone1, u.phone2, u.timecreated, u.lastaccess, lu.usergroup, lu.sex, lu.userid,lu.statuscode,lu.joinstatus, lu.email2, lu.sns, 
                em.id AS employer_id ";
    $sql_select_count = "SELECT COUNT(DISTINCT u.id) ";

    $sql_from = "FROM {user} u
    JOIN {lmsdata_user} lu ON lu.userid = u.id
    LEFT JOIN {vi_admin_employers} e ON e.user_id = u.id
    LEFT JOIN {vi_employers} em ON e.employer_id = em.id";
}



$conditions = array("u.deleted = 0");
$params = array();

if($type == 1) {
    switch($tabview) {
        case 2: 
            $conditions[] = "NOT EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                        )";
            break;
        case 3: 
            $conditions[] = "NOT EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                        )";
            break;
        case 4: 
            $conditions[] = "u.timecreated <= '{$thistime}'";
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) 
                        )";
            break;
        case 5: 
            $conditions[] = "u.timecreated <= '{$thistime}'";
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1)) 
                        )";
            break;
        case 6: 
            $conditions[] = "u.lastaccess > 0";
            $conditions[] = "EXISTS (
                SELECT null
                FROM (
                    SELECT 
                        (ue.timeend - (86400*lc.reviewperiod)) as enddate, 
                        ue.userid as userid
                    from {lmsdata_class} lc
                    join {course} co on co.id = lc.courseid 
                    join {enrol} e on e.courseid = co.id
                    join {user_enrolments} ue on ue.enrolid = e.id
                    where ue.status = 0
                ) z 
                WHERE $thistime < z.enddate and z.userid = u.id
            )";
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                        )";
            break;
        case 7: 
            $conditions[] = "u.lastaccess > 0";
            $conditions[] = "EXISTS (
                SELECT null
                FROM (
                    SELECT 
                        (ue.timeend - (86400*lc.reviewperiod)) as enddate, 
                        ue.userid as userid
                    from {lmsdata_class} lc
                    join {course} co on co.id = lc.courseid 
                    join {enrol} e on e.courseid = co.id
                    join {user_enrolments} ue on ue.enrolid = e.id
                    where ue.status = 0
                ) z 
                WHERE $thistime < z.enddate and z.userid = u.id
            )";
            $conditions[] = "EXISTS
                        (
                            SELECT null 
                            FROM {lmsdata_payment} lp
                            LEFT JOIN {lmsdata_class} lc on lc.id = lp.ref
                            LEFT JOIN {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                            LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                            WHERE lp.userid = u.id AND lp.forced = 0  AND lc.price > 0 AND (lr.id IS NULL OR (lr.id IS NOT NULL AND lr.refundtype != 1))
                        )";
            break;
    }
}

if ($type == 4) {
    // company users
    // $conditions[] = 'e.employer_id IS NOT NULL';

    $sql_from = "FROM {vi_employers} em
                LEFT JOIN {vi_admin_employers} e ON e.employer_id = em.id
                LEFT JOIN {user} u ON u.id = e.user_id
                LEFT JOIN {lmsdata_user} lu ON lu.userid = u.id";
    // $conditions = array("(u.id IS NULL OR u.deleted = 0)");
    $conditions = array("1=1");
    $sql_select_count = "SELECT COUNT(DISTINCT em.id) ";
} else {
    $conditions[] = 'e.employer_id IS NULL';
}

$sql_where = ' WHERE ' . implode(' AND ', $conditions);

if ($learningstart_str) {
    $learningstart = str_replace(".", "-", $learningstart_str);
    $learningstart = strtotime($learningstart);
}
if ($learningend_str) {
    $learningend = str_replace(".", "-", $learningend_str);
    $learningend = strtotime($learningend) + 86399;
}

if ($learningstart) {
    $sql_where_array[] = " u.timecreated >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if ($learningend) {
    $sql_where_array[] = " u.timecreated <= :learningend ";
    $params['learningend'] = $learningend;
}

if (!empty($sql_where_array)) {
    $sql_where .= ' AND ' . implode(' and ', $sql_where_array);
} else {
    $sql_where .= '';
}

if ($usergroup) {
    $aa = ' AND (' . implode(' OR ', $usergroup);
    $aa .= ')';
    $sql_where .= $aa;
}
if ($auth) {
    $aa = ' AND (' . implode(' OR ', $auth);
    $aa .= ')';
    $sql_where .= $aa;
}
// joinstatus
if (is_array($joinstatus)) {
    $_site = [];
    if (in_array('elearning', $joinstatus)) {
        array_push($_site, 'lu.joinstatus = "e"');
        array_push($_site, 'lu.joinstatus = ""');
    }

    if (in_array('job', $joinstatus)) {
        array_push($_site, 'lu.joinstatus = "j"');
    }

    if (count($_site)) {
        $sql_where .= ' AND (' . implode(' OR ', $_site) . ') ';
    }
}
if ($searchtext) {
    $aa = ' AND (u.username like :username OR u.firstname like :firstname OR u.email like :email OR u.phone1 like :phone1 OR em.company_name like :company_name)';
    $params['username'] = '%' . $searchtext . '%';
    $params['firstname'] = '%' . $searchtext . '%';
    $params['email'] = '%' . $searchtext . '%';
    $params['phone1'] = '%' . $searchtext . '%';
    $params['company_name'] = '%' . $searchtext . '%';
    $sql_where .= $aa;
}
//정렬기능 추가 
$sql_sort = "ORDER BY u.timecreated DESC";
$orderarr = array("usernameorder" => "u.username", "nameoreder" => "u.firstname ", "birthoreder" => "u.phone2", "timecreatedoreder" => "u.timecreated", "lastoreder" => "u.lastaccess", "emailoreder" => "u.email", "comoreder" => "lu.usergroup");
//print_object($orderarr);exit;
if (empty($ordertype) && !empty($orderitem)) {
    $sql_sort = "ORDER BY $orderarr[$orderitem] DESC";
    if ($orderarr[$orderitem] == 'lu.usergroup') {
        $sql_sort = "ORDER BY FIELD(lu.usergroup,'ad','pr','bp','rs')";
    }
} else if ($ordertype == 'up') {
    $sql_sort = "ORDER BY $orderarr[$orderitem] ASC";
    if ($orderarr[$orderitem] == 'lu.usergroup') {
        $sql_sort = "ORDER BY FIELD(lu.usergroup,'rs','bp','pr','ad')";
    }
}

if ($type == 4) {
    $users = $DB->get_records_sql("$sql_select $sql_from $sql_where $sql_sort ", $params, ($page - 1) * $perpage, $perpage);
} else {
    $users = $DB->get_records_sql("$sql_select $sql_from $sql_where group by la.userid $sql_sort ", $params, ($page - 1) * $perpage, $perpage);

}

$totalusers = $DB->count_records_sql("$sql_select_count $sql_from $sql_where", $params);
$perpage_array = [10, 20, 30, 50];

$usergroup_array = array('rs' => get_string('user:normal', 'local_management'), 'bp' => get_string('user:bop', 'local_management'));
$auth_array = array('email' => get_string('user:email', 'local_management'), 'facebook' => get_string('user:facebook', 'local_management'), 'google' => get_string('user:google', 'local_management'));
include_once($CFG->dirroot . '/local/management/header.php');
?>
<style>
    .stat-box {
        display: inline-block;
        padding: 15px;
        text-align: center;
        margin: 3px auto;
        min-width: 150px;
        cursor: pointer;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

    .stat-box:hover {
        box-shadow: 0 0 0 3px #b5b9bd;
    }

    .stat-box h3 {
        font-size: 34px;
        font-weight: bold;
        margin: 0px;
    }
</style>

<section class="content">
    <?php if ($type == 1) : ?>
        <?php include('repo/index-repo.php'); ?>
        <?php include('components/index-report.php'); ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-body">
                <?php include('components/index-search.php'); ?>
                <span><?php echo get_string('total', 'local_management', $totalusers) ?></span>
                <select name="perpage2" class="perpage2">
                    <?php
                    foreach ($perpage_array as $pa) {
                        $selected = '';
                        if ($pa == $perpage) {
                            $selected = 'selected';
                        }
                    ?>
                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                        <?php
                    }
                        ?>
                </select>
                <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="excel_download();" />
                <div style="float:right;">
                    <button onclick="location.href = './form.php?type=<?php echo $type ?>'"><?php echo get_string('user:registration', 'local_management') ?></button>
                    <button onclick="excel_user(this);" /><?php echo get_string('user:excell', 'local_management') ?></button>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th width="50px"><?php echo get_string('number', 'local_management') ?></th>
                            <?php if ($type == 4) : ?>
                                <th width="6%"><?php echo get_string('user:member_type', 'local_management') ?></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'usernameorder' ? $ordertype : 'dash'; ?>" id="usernameorder"><?php echo get_string('user:id', 'local_management') ?></span></th>
                                <th><?php echo get_string('user:company_name', 'local_management') ?></th>
                                <th><?php echo get_string('user:business_registration_number', 'local_management') ?></th>
                                <th><?php echo get_string('user:registrationcertificate', 'local_management') ?></th>
                                <th>한국어 활용 수준</th>
                                <th><?php echo get_string('user:request_date', 'local_management') ?></th>
                                <th><?php echo get_string('user:approval_date', 'local_management') ?></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'lastoreder' ? $ordertype : 'dash'; ?>" id="lastoreder"><?php echo get_string('user:last', 'local_management') ?></span></th>
                                <th><?php echo get_string('user:approval', 'local_management') ?></th>
                            <?php else : ?>
                                <th width="6%"><?php echo get_string('user:jointype', 'local_management') ?></th>
                                <th width="6%"><?php echo get_string('user:membertype', 'local_management') ?></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'usernameorder' ? $ordertype : 'dash'; ?>" id="usernameorder"><?php echo get_string('user:id', 'local_management') ?></span></th>
                                <th width="6%"><?php echo get_string('user:gender', 'local_management') ?></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'nameoreder' ? $ordertype : 'dash'; ?>" id="nameoreder"><?php echo get_string('user:name', 'local_management') ?></span></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'birthoreder' ? $ordertype : 'dash'; ?>" id="birthoreder"><?php echo get_string('user:birth', 'local_management') ?></span></th>
                                <th><?php echo get_string('user:email', 'local_management') ?></th>
                                <th><?php echo get_string('user:phone', 'local_management') ?></th>

                                <?php if ($type == 1 && in_array('job', $joinstatus)) : ?>
                                    <th>한국어 활용 수준</th>
                                <?php endif; ?>
                                <th width="6%"><?php echo get_string('user:login_number', 'local_management') ?></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'timecreatedoreder' ? $ordertype : 'dash'; ?>" id="timecreatedoreder"><?php echo get_string('user:timecreated', 'local_management') ?></span></th>
                                <th><?php echo get_string('user:site', 'local_management') ?></th>
                                <th><span class="tb-arrow <?php echo $orderitem == 'lastoreder' ? $ordertype : 'dash'; ?>" id="lastoreder"><?php echo get_string('user:last', 'local_management') ?></span></th>
                                <th><?php echo get_string('user:auth', 'local_management') ?></th>
                                <th><?php echo get_string('user:sharing', 'local_management') ?></th>
                                <?php if ($type == 2) { ?>
                                    <th><?php echo get_string('user:activity', 'local_management') ?></th>
                                <?php } ?>
                            <?php endif; ?>
                            <?php if ($type != 3) { ?>
                                <th><?php echo get_string('user:login', 'local_management') ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($totalusers > 0) {
                            $startnum = $totalusers - (($page - 1) * $perpage);
                            foreach ($users as $user) {
                                $lastaccess = get_string('user:noconnection', 'local_management');
                                if ($user->lastaccess) {
                                    $lastaccess = userdate($user->lastaccess, '%Y.%m.%0d %H:%M');
                                }
                                echo '<tr>';
                                echo '<td>' . $startnum-- . '</td>';
                                if ($type == 4) {

                                    $employer = $DB->get_record_sql(
                                        "SELECT e.* 
                                        FROM {vi_employers} e
                                        WHERE e.id=:employerid",
                                        array('employerid' => $user->employer_id)
                                    );

                                    echo '<td>' . get_string("user:company", 'local_management') . '</td>';
                                    echo is_null($user->username) ? '<td><a href="' . ($CFG->wwwroot) . '/local/management/job/employers/managers.php?employerid=' . $employer->id . '"><small><i>(Add manager)</i></small></a></td>' : '<td><a href="./form.php?id=' . $user->userid . '&type=' . $type . '">' . $user->username . '</a></td>';
                                    echo '<td>' . $employer->company_name . '</td>';
                                    echo '<td>' . $user->phone2 . '</td>';

                                    echo '<td>';
                                    if ($employer->company_cover != '') echo '<a target="_blank" href="' . ($CFG->wwwroot . '/pluginfile.php/' . $employer->company_cover) . '">' . get_string('user:download', 'local_management') . '</a>';
                                    echo '</td>';

                                    // 한국어 활용 수준
                                    $job_need_korean_level = $DB->get_record_sql(
                                        "SELECT j.* 
                                        FROM {vi_jobs} j
                                        WHERE j.employer_id=:employerid AND j.korean_level_id > 1
                                        ORDER BY j.enddate DESC",
                                        array('employerid' => $employer->id)
                                    );
                                    $korean_level = '-';
                                    if ($job_need_korean_level) {
                                        $_p = $DB->get_record('vi_korean_levels', array('id' => $job_need_korean_level->korean_level_id));
                                        if ($_p) {
                                            switch ($current_language) {
                                                case 'ko':
                                                    $korean_level = $_p->name_ko;
                                                    break;
                                                case 'en':
                                                    $korean_level = $_p->name_en;
                                                    break;

                                                default:
                                                    $korean_level = $_p->name_vi;
                                                    break;
                                            }
                                        }
                                    }
                                    echo '<td>' . $korean_level . '</td>';


                                    echo '<td>' . (!is_null($user->timecreated) ? date('Y.m.d', $user->timecreated) : '-') . '</td>';
                                    echo '<td>' . ($employer->approval_date ? date('Y.m.d', $employer->approval_date) : '-') . '</td>';
                                    echo '<td>' . $lastaccess . '</td>';

                                    echo '<td>
                                            <form method="get">
                                                <input type="hidden" name="employerid" value="' . $employer->id . '">
                                                <select onchange="this.form.submit();" name="approval">
                                                    <option value="0" ' . ($employer->confirmed == 0 ? 'selected' : '') . '>' . get_string('user:approval_pending', 'local_management') . '</option>
                                                    <option value="1" ' . ($employer->confirmed == 1 ? 'selected' : '') . '>' . get_string('user:approval_accepted', 'local_management') . '</option>
                                                    <option value="-1" ' . ($employer->confirmed == -1 ? 'selected' : '') . '>' . get_string('user:approval_notaccepted', 'local_management') . '</option>
                                                </select>
                                            </form>
                                        </td>';
                                } else {
                                    if ($user->sns == '' or $user->sns == 'email') {
                                        $user->sns = get_string('user:email', 'local_management');
                                    }
                                    echo '<td>' . $user->sns . '</td>';
                                    $auth_array = local_management_users_auth_array();
                                    foreach ($auth_array as $key => $val) {
                                        if ($key == $user->usergroup) {
                                            $authname = get_string("user:$key", 'local_management');
                                        }
                                    }
                                    echo '<td>' . $authname . '</td>';
                                    echo '<td><a href="./form.php?id=' . $user->userid . '&type=' . $type . '">' . $user->username . '</a></td>';

                                    if ($user->sex == 1) {
                                        $gender =  get_string('user:male');
                                    } elseif ($user->sex == 2) {
                                        $gender = get_string('user:female');
                                    } else {
                                        $gender = null;
                                    }

                                    echo '<td>' . $gender . '</td>';

                                    echo '<td>' . $user->firstname . '</td>';
                                    echo '<td>' . $user->phone2 . '</td>';
                                    echo $user->email2 ? '<td>' . $user->email2 . '</td>' : '<td>' . $user->email . '</td>';
                                    echo '<td>' . $user->phone1 . '</td>';

                                    //한국어 활용 수준
                                    if ($type == 1 && in_array('job', $joinstatus)) {
                                        $job_need_korean_level = $DB->get_record_sql(
                                            "SELECT l.* 
                                            FROM {vi_user_korean_levels} l
                                            WHERE l.user_id=:userid AND l.korean_level_id > 1",
                                            array('userid' => $user->id)
                                        );
                                        $korean_level = '-';
                                        if ($job_need_korean_level) {
                                            $_p = $DB->get_record('vi_korean_levels', array('id' => $job_need_korean_level->korean_level_id));
                                            if ($_p) {
                                                switch ($current_language) {
                                                    case 'ko':
                                                        $korean_level = $_p->name_ko;
                                                        break;
                                                    case 'en':
                                                        $korean_level = $_p->name_en;
                                                        break;

                                                    default:
                                                        $korean_level = $_p->name_vi;
                                                        break;
                                                }
                                            }
                                        }
                                        echo '<td>' . $korean_level . '</td>';
                                    }

                                    echo '<td>' . $user->login_number . '</td>';

                                    echo '<td>' . date('Y.m.d', $user->timecreated) . '</td>';
                                    //lu.statuscode,lu.joinstatus
                                    if ($user->joinstatus == "e" || $user->joinstatus == "") {
                                        $joinstatus_txt = get_string('user:elearning', 'local_management');
                                    } else {
                                        $joinstatus_txt = get_string('jobs', 'local_management');
                                    }
                                    echo '<td>' . $joinstatus_txt . '</td>';
                                    echo '<td>' . $lastaccess . '</td>';
                                    if ($user->confirmed == 1) {
                                        echo '<td>Y</td>';
                                    } else {
                                        echo '<td>N</td>';
                                    }

                                    if ($user->statuscode == "1") {
                                        $statuscode_txt = 'Y';
                                    } else {
                                        $statuscode_txt = 'N';
                                    }
                                    echo '<td>' . $statuscode_txt . '</td>';
                                    if ($type == 2) {

                                        $teacher = $DB->get_record('lmsdata_teacher', array('userid' => $user->id));
                                        switch ($teacher->isused) {
                                            case 0:
                                                $isused_txt = get_string('user:inactivity', 'local_management');
                                                break;
                                            case 1:
                                                $isused_txt = get_string('user:activity1', 'local_management');
                                                break;
                                        }

                                        echo '<td>' . $isused_txt . '</td>';
                                    }
                                }

                                if ($type != 3) {
                                    if ($type == 4) {
                                        echo is_null($user->userid) ? '<td></td>' : '<td><a onclick="mkjob_loginas(' . $user->userid . ')" class="btn btn-default">' . get_string('user:login', 'local_management') . '</a></td>';
                                    } else {
                                        $force_login_url = new moodle_url('/course/loginas.php', array('user' => $user->userid, 'sesskey' => sesskey()));
                                        echo '<td><a href="' . $force_login_url . '" class="btn btn-default">' . get_string('user:login', 'local_management') . '</a></td>';
                                    }
                                }
                                echo '</tr>';
                            }
                        } else {
                            if ($type == 1) {
                                $colspan = 14;
                                //한국어 활용 수준
                                if (in_array('job', $joinstatus)) {
                                    $colspan++;
                                }
                            } else if ($type == 3) {
                                $colspan = 13;
                            } else if ($type == 4) {
                                $colspan = 12;
                            } else {
                                $colspan = 15;
                            }
                        ?>
                            <tr>
                                <td colspan='<?php echo $colspan ?>'><?php echo get_string('user:nodata', 'local_management') ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>


                <div id="btn_area">
                    <div style="float:right;">
                        <?php if ($type != 4) : ?>
                            <button onclick="location.href = './form.php?type=<?php echo $type ?>'"><?php echo get_string('user:registration', 'local_management') ?></button>
                            <button onclick="excel_user(this);" /><?php echo get_string('user:excell', 'local_management') ?></button>
                        <?php endif; ?>
                    </div>
                </div>
                <nav aria-label="Page navigation" class="text-center">
                    <?php
                    //                        $pageurl = $CFG->wwwroot.'/local/management/users/index.php?search=' . $searchtext;
                    //                        echo local_management_users_paing($page, $perpage, $totalusers, $pageurl,$searchtext,$orderitem,$ordertype);
                    if (($totalusers / $perpage) > 1) {
                        print_paging_navbar_script($totalusers, $page, $perpage, 'javascript:cata_page(:page);');
                    }
                    ?>
                </nav>
            </div>
        </div>
    </div>
</section>

<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
    function excel_download() {
        var form = $('#course_search');
        var action = form.attr('action');
        form.attr('action', 'excel_down.php');
        form.submit();
        form.attr('action', action);
        return false;
    }

    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }
    $(document).ready(function() {
        $(".perpage2").change(function() {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=course_search]").submit();
        });
        $('#reset').on('click', function() {
            //$('#course_search_cata1').val('0');
            //$('#course_search_cata2').val('0');
            //$('#copyright').val('0');
            $('.s_date').val('');
            $('.e_date').val('');
            $("input:checkbox[name='auth[]']").attr("checked", false);
            $("input:checkbox[name='usergroup[]']").attr("checked", false);
            $('input[name=searchtext]').val('');
            $("form[id=course_search]").submit();
        });

    });
    start_datepicker_c('', '');
    end_datepicker_c('', '');


    function excel_user(button) {
        var type = '<?php echo $type ?>';
        var tag = $("<div id='course_enrol_popup'></div>");
        <?php
            $importApi = $type !== 1 ? $CFG->wwwroot . '/local/management/users/enrol_course_excel.php' : $CFG->wwwroot . '/local/management/users/user_import.php';
        ?>
        $.ajax({
            url: '<?php echo $importApi; ?>',
            method: 'POST',
            data: {
                type: type
            },
            success: function(data) {
                tag.html(data).dialog({
                    title: '<?php echo get_string('user:excell', 'local_management') ?>',
                    modal: true,
                    width: 600,
                    resizable: false,
                    buttons: [{
                        id: 'close',
                        text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                        disable: true,
                        click: function() {
                            $(this).dialog("close");
                        }
                    }],
                    close: function() {
                        $('#frm_enrol_excel').remove();
                        $(this).dialog('destroy').remove()
                    },
                    open: function() {
                        $(".ui-dialog-buttonset").prepend('<input type="button" class="blue_btn" value="<?php echo get_string('add3', 'local_lmsdata'); ?>" onclick="enrol_course_excel_submit()"/>')
                    }
                }).dialog('open');
            }
        });
    }

    function mkjob_loginas(userid) {
        $.post("<?php echo $CFG->wwwroot . '/course/loginas.php'; ?>", {
            user: userid,
            sesskey: "<?php echo sesskey(); ?>"
        }, function() {
            window.location.href = "<?php echo $CFG->wwwroot; ?>/local/job/";
        });
    }
</script>