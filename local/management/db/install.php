<?php

function xmldb_local_management_install() {
    global $CFG, $DB, $USER;
    
    require_once($CFG->dirroot . '/local/management/menu/lib.php');
    
    $time = time();
    
    $menu_root = new stdClass();
    $menu_root->text = 'Home';
    $menu_root->shortname = 'root';
    $menu_root->icon = 'fa fa-home';
    $menu_root->description = '';
    $menu_root->href = '/management/index.php';
    $menu_root->target = '_self';
    $menu_root->title = '';
    $menu_root->lft = 1;
    $menu_root->rgt = 2;
    $menu_root->ismain = 1;
    $menu_root->creator = $USER->id;
    $menu_root->parentid = 0;
    $menu_root->timecreated = $time;
    
    $rootid = $DB->insert_record('management_menu', $menu_root);
    
    $menus = array(
        array('text' => '메뉴 관리', 'shortname' => 'menu', 'icon' => 'fa fa-list-ul',  'description' => '', 'href' => '/local/management/menu/index.php', 'target' => '_self', 'title' => '메뉴관리', 'ismain' => 1),
        array('text' => '권한 관리', 'shortname' => 'role', 'icon' => 'fa fa-key', 'description' => '', 'href' => '#', 'target' => '_self', 'title' => '권한관리', 'ismain' => 1,
            'children' => array(
                array('text' => '그룹관리', 'shortname' => 'group', 'icon' => 'fa fa-group', 'description' => '', 'href' => '/local/management/role/group.php', 'target' => '_self', 'title' => '그룹관리', 'ismain' => 1),
                array('text' => '사용자관리', 'shortname' => 'user', 'icon' => 'fa fa-user', 'description' => '', 'href' => '/local/management/role/user.php', 'target' => '_self', 'title' => '사용자관리', 'ismain' => 1),
            )),
        array('text' => '콘텐츠관리', 'shortname' => 'M_CON1', 'icon' => 'fa fa-address-card', 'description' => '', 'href' => '#', 'target' => '_self', 'title' => '콘텐츠관리', 'ismain' => 0,
            'children' => array(
                array('text' => '추천 콘텐츠 관리', 'shortname' => 'contents', 'icon' => 'fa fa-fonticons', 'description' => '', 'href' => '/local/management/contents/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
                array('text' => '메인 콘텐츠 관리', 'shortname' => 'banner', 'icon' => 'fa fa-connectdevelop', 'description' => '', 'href' => '/local/management/banner/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
            )),
        array('text' => '학습 독려', 'shortname' => 'encourage', 'icon' => 'fa fa-envelope-o', 'description' => '', 'href' => '#', 'target' => '_self', 'title' => '권한관리', 'ismain' => 0,
            'children' => array(
                array('text' => '발송 템블릿', 'shortname' => 'encourage_template', 'icon' => 'fa fa-file-text-o', 'description' => '', 'href' => '/local/management/encourage/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
                array('text' => '발송 스케쥴', 'shortname' => 'encourage_schedule', 'icon' => 'fa fa-calendar', 'description' => '', 'href' => '/local/management/encourage/schedule.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
            )),
        array('text' => 'DLP 약관 관리', 'shortname' => 'term', 'icon' => 'fa fa-align-left', 'description' => '', 'href' => '/local/management/term/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
        array('text' => '통계 다운로드', 'shortname' => 'ST_01', 'icon' => 'fa fa-area-chart', 'description' => '', 'href' => '/local/management/statistics/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0,
            'children' => array(
                array('text' => '엑셀 다운로드', 'shortname' => 'statistics', 'icon' => 'fa fa-arrow-circle-down', 'description' => '', 'href' => '/local/management/statistics/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
            )),
        array('text' => '전체 학습방', 'shortname' => 'courses', 'icon' => 'fa fa-align-justify', 'description' => '', 'href' => '/local/management/courses/index.php', 'target' => '_self', 'title' => '', 'ismain' => 0),
    );
    
    foreach ($menus as $menu) {
        xmldb_local_management_install_menu($menu, $rootid);
    }
    
    // 시스템 관리 그룹 추가
    $groupsysadmin = new stdClass();
    $groupsysadmin->shortname = 'sysadmin';
    $groupsysadmin->name = '시스템 관리';
    $groupsysadmin->description = '';
    $groupsysadmin->ismain = 1;
    $groupsysadmin->creator = $USER->id;
    $groupsysadmin->timecreated = $time;
    $groupsysadmin->id = $DB->insert_record('management_group', $groupsysadmin);
    
    // 사이트 관리자를 시스템 관리 그룹에 등록
    foreach (explode(',', $CFG->siteadmins) as $admin) {
        $admin = (int)$admin;
        if ($admin) {
            $member = new stdClass();
            $member->groupid = $groupsysadmin->id;
            $member->userid = $admin;
            $member->creator = $USER->id;
            $member->timecreated = $time;
            $DB->insert_record('management_group_member', $member);
        }
    }
}

function xmldb_local_management_install_menu($menu, $parentid) {
    $menuobj = new stdClass();
    $menuobj->text = $menu['text'];
    $menuobj->shortname = $menu['shortname'];
    $menuobj->icon = $menu['icon'];
    $menuobj->description = $menu['description'];
    $menuobj->href = $menu['href'];
    $menuobj->target = $menu['target'];
    $menuobj->title = $menu['title'];
    $menuobj->ismain = $menu['ismain'];
    
    $menuid = local_management_menu_add($menuobj, $parentid);
    
    if(isset($menu['children'])) {
        foreach($menu['children'] as $child) {
            xmldb_local_management_install_menu($child, $menuid);
        }
    }
}