<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_management_upgrade($oldversion)
{
    global $CFG, $DB, $OUTPUT;
    $dbman = $DB->get_manager();
    if ($oldversion < 2021061700) {
        $table = new xmldb_table('vi_job_feedback');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('username', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('user_avatar_url', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('title', XMLDB_TYPE_TEXT, '1000', null, null, null, null);
        $table->add_field('company_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('job_position', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('content', XMLDB_TYPE_TEXT, '', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

//    if ($oldversion < 2021072803) {
//        $table = new xmldb_table('vi_common_code');
//        $field = new xmldb_field('point', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
//        if (!$dbman->field_exists($table, $field)) {
//            $dbman->add_field($table, $field);
//        }
//    }

//    if ($oldversion < 2021072801) {
//        $table = new xmldb_table('vi_credits_logs');
//        $field = new xmldb_field('object_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
//        if (!$dbman->field_exists($table, $field)) {
//            $dbman->add_field($table, $field);
//        }
//    }

    //create table avatar_user
    if ($oldversion < 2021080500) {
        $table = new xmldb_table('avatar_user');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('url', XMLDB_TYPE_TEXT, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('created_at', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('foreign', XMLDB_KEY_FOREIGN_UNIQUE, array('user_id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    //create table banner_video
    if ($oldversion < 2021080500) {
        $table = new xmldb_table('banner_video');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('filename', XMLDB_TYPE_TEXT, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('isused', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2021080501) {
        $table = new xmldb_table('vi_coupon_qrcode');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('coupon_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('qrcodeurl', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('coupon_fk', XMLDB_KEY_FOREIGN, array('coupon_id'), 'vi_coupons', array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2021091503) {
        $table = new xmldb_table('vi_rate_send_email_job');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('taskname', XMLDB_TYPE_TEXT, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('matchingrate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        $row = new stdClass();
        $row->id=111;
        $row->taskname ="\\local_job\\task\\cron_task";
        $row->matchingrate = 60;
        $row->timecreated = time();
        $row->timemodified = time();

        if (!($DB->get_records('vi_rate_send_email_job',array('id' => $row->id)))) {
            $DB->insert_record('vi_rate_send_email_job', $row);
        }
        $row2 = new stdClass();
        $row2->id=112;
        $row2->taskname ="\\local_job\\task\\employer_task";
        $row2->matchingrate = 60;
        $row2->timecreated = time();
        $row2->timemodified = time();

        if (!($DB->get_records('vi_rate_send_email_job',array('id' =>  $row2->id)))) {
            $DB->insert_record('vi_rate_send_email_job', $row2);
        }
    }

    if ($oldversion < 2021091506) {

        $table = new xmldb_table('vi_credits');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('employer_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('credit_point', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('expire_date', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('foreign', XMLDB_KEY_FOREIGN, array('employer_id'),'vi_employers','id');
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('vi_credits_logs');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('member_credit_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('transaction_code', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('withdraw_code', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('pg_code', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('point', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timelogged', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('object_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('foreign', XMLDB_KEY_FOREIGN, array('member_credit_id'),'vi_credits','id');
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('vi_credit_menu');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('credit_name', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('banner_image_url', XMLDB_TYPE_TEXT, '10', null, null, null, null);
        $table->add_field('points', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('expired_date', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('vi_common_code_category');
        $table->add_field('category_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name_vi', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('name_ko', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('name_en', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('category_id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('vi_common_code');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('code', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('category_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name_vi', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name_ko', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('name_en', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('is_used', XMLDB_TYPE_CHAR, '10', null, XMLDB_NOTNULL,null, 'Y');
        $table->add_field('point', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('foreign', XMLDB_KEY_FOREIGN, array('category_id'),'vi_common_code_category','category_id');
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table = new xmldb_table('vi_credit_menu_logs');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('credit_menu_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('task_code', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('banner_image_url', XMLDB_TYPE_TEXT, '', null, null, null, null);
        $table->add_field('points', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('price', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('expired_date', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timelogged', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('foreign', XMLDB_KEY_FOREIGN, array('credit_menu_id'),'vi_credit_menu','id');
        $table->add_key('foreign_common_code', XMLDB_KEY_FOREIGN, array('task_code'),'vi_common_code','code');

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $row = new stdClass();
        $row->category_id = 100;
        $row->name_vi ='Giao dịch';
        $row->name_ko = '거래';
        $row->name_en = 'Transaction';
        $row->timecreated = time();

        if (!($DB->get_records('vi_common_code_category',array('category_id' => $row->category_id)))) {
            $DB->insert_record('vi_common_code_category', $row);
        }

        $row = new stdClass();
        $row->category_id = 200;
        $row->name_vi ='Sử dụng';
        $row->name_ko = '지불';
        $row->name_en = 'Withdraw';
        $row->timecreated = time();
        if (!($DB->get_records('vi_common_code_category',array('category_id' => $row->category_id)))) {
            $DB->insert_record('vi_common_code_category', $row);
        }

        $row = new stdClass();
        $row->category_id = 300;
        $row->name_vi ='CỔNG THANH TOÁN';
        $row->name_ko = '결제 게이트웨이';
        $row->name_en = 'PAYMENT_GATEWAY';
        $row->timecreated = time();
        if (!($DB->get_records('vi_common_code_category',array('category_id' => $row->category_id)))) {
            $DB->insert_record('vi_common_code_category', $row);
        }

        $row = new stdClass();
        $row->category_id = 400;
        $row->name_vi ='MENU NHIỆM VỤ';
        $row->name_ko = '메뉴 작업';
        $row->name_en = 'MENU_TASK';
        $row->timecreated = time();
        if (!($DB->get_records('vi_common_code_category',array('category_id' => $row->category_id)))) {
            $DB->insert_record('vi_common_code_category', $row);
        }

        $row = new stdClass();
        $row->code = 1001;
        $row->category_id = 100;
        $row->name_vi = 'ĐẶT TRƯỚC';
        $row->name_ko = '크레딧 구매';
        $row->name_en = 'DEPOSIT';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 1002;
        $row->category_id = 100;
        $row->name_vi = 'RÚT';
        $row->name_ko = '크레딧 지불';
        $row->name_en = 'WITHDRAW';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 2001;
        $row->category_id = 200;
        $row->name_vi = 'ĐĂNG CÔNG VIỆC';
        $row->name_ko = '잡포스트';
        $row->name_en = 'JOBPOST';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 2002;
        $row->category_id = 200;
        $row->name_vi = 'XEM CV';
        $row->name_ko = '이력서 보기';
        $row->name_en = 'CVVIEW';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 2003;
        $row->category_id = 200;
        $row->name_vi = 'TẢI CV';
        $row->name_ko = '이력서 다운로드';
        $row->name_en = 'CVDOWN';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 3001;
        $row->category_id = 300;
        $row->name_vi = 'Ví điện tử MOMO';
        $row->name_ko = '모모 전자지갑';
        $row->name_en = 'MOMO E-Wallet';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 3002;
        $row->category_id = 300;
        $row->name_vi = 'ATM / VISA / Mastercard';
        $row->name_ko = 'ATM/비자/마스터카드';
        $row->name_en = 'ATM/VISA/Master Card';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 3003;
        $row->category_id = 300;
        $row->name_vi = 'Tiền gửi trực tiếp';
        $row->name_ko = '계좌 입금';
        $row->name_en = 'Direct deposit';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 4001;
        $row->category_id = 400;
        $row->name_vi = 'THÊM MỚI';
        $row->name_ko = '추가';
        $row->name_en = 'ADD';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 4002;
        $row->category_id = 400;
        $row->name_vi = 'CHỈNH SỬA';
        $row->name_ko = '편집';
        $row->name_en = 'EDIT';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

        $row = new stdClass();
        $row->code = 4003;
        $row->category_id = 400;
        $row->name_vi = 'XÓA BỎ';
        $row->name_ko = '삭제';
        $row->name_en = 'DELETE';
        $row->is_used = 'Y';
        if (!($DB->get_records('vi_common_code',array('code' => $row->code)))) {
            $DB->insert_record('vi_common_code', $row);
        }

    }

    if ($oldversion < 2021091505) {
        //create table vi_coupons
        $table = new xmldb_table('vi_coupons');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('description', XMLDB_TYPE_TEXT, '255', null, false, null, null);
        $table->add_field('filename', XMLDB_TYPE_CHAR, '255', null, false, null, null);
        $table->add_field('start_date', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);
        $table->add_field('expire_date', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);
        $table->add_field('discount_rate', XMLDB_TYPE_INTEGER, '3', null, XMLDB_NOTNULL, null, null);
        $table->add_field('key_code', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('count', XMLDB_TYPE_INTEGER, '10', null, false, null, null);
        $table->add_field('send_date', XMLDB_TYPE_INTEGER, '11', null, false, null, null);
        $table->add_field('is_send', XMLDB_TYPE_INTEGER, '1', null, false, null, null);
        $table->add_field('redirect_url', XMLDB_TYPE_CHAR, '255', null, false, null, null);
        $table->add_field('email_body', XMLDB_TYPE_TEXT, '255', null, false, null, null);
        $table->add_field('logo_id', XMLDB_TYPE_INTEGER, '11', null, false, null, null);
        $table->add_field('type_code', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('render_qrcode', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('create_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);
        $table->add_field('modified_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);
        $table->add_field('expired_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //create table vi_coupons_courses
        $table = new xmldb_table('vi_coupons_courses');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('coupon_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('course_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('coupon_fk', XMLDB_KEY_FOREIGN, array('coupon_id'), 'vi_coupons', array('id'));
        $table->add_key('course_fk', XMLDB_KEY_FOREIGN, array('course_id'), 'vi_course', array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //create table vi_common_coupon_code
        $table = new xmldb_table('vi_common_coupon_code');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('code_category_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('code_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('code_name_vi', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('code_name_en', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('code_name_ko', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('use', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('code_category_id_fk', XMLDB_KEY_FOREIGN, array('code_category_id'), 'vi_coupon_code_category', array('code_category_id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //create table vi_coupon_code_category
        $table = new xmldb_table('vi_coupon_code_category');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('coupon_code_category', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('coupon_code_category_name_vi', XMLDB_TYPE_CHAR, '50', null, false, null, null);
        $table->add_field('coupon_code_category_name_en', XMLDB_TYPE_CHAR, '50', null, false, null, null);
        $table->add_field('coupon_code_category_name_ko', XMLDB_TYPE_CHAR, '50', null, false, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('code_unix', XMLDB_KEY_UNIQUE, array('coupon_code_category'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //create table vi_coupons_errors
        $table = new xmldb_table('vi_coupons_errors');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('coupon_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('error_type', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('error_message', XMLDB_TYPE_TEXT, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('created_time', XMLDB_TYPE_INTEGER, '11', null, XMLDB_NOTNULL, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('coupon_fk', XMLDB_KEY_FOREIGN, array('coupon_id'), 'vi_coupons', array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //create table vi_coupons_receivers
        $table = new xmldb_table('vi_coupons_receivers');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('coupon_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, false, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '50', null, false, null, null);
        $table->add_field('is_member', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('coupon_key_code', XMLDB_TYPE_CHAR, '50', null, false, null, null);
        $table->add_field('is_used', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('received_time', XMLDB_TYPE_INTEGER, '11', null, false, null, null);
        $table->add_field('used_time', XMLDB_TYPE_INTEGER, '11', null, false, null, null);


        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('coupon_fk', XMLDB_KEY_FOREIGN, array('coupon_id'), 'vi_coupons', array('id'));
        $table->add_key('user_fk', XMLDB_KEY_FOREIGN, array('user_id'), 'vi_user', array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    if($oldversion<2021091507){
        $table = new xmldb_table('vi_coupon_qrcode');
        $field = new xmldb_field('dataimg', XMLDB_TYPE_CHAR, '1333', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2021091507) {
        $table = new xmldb_table('management_menu');
        $field = new xmldb_field('groupid', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    return true;
}