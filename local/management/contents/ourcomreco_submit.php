<?php

require_once(__DIR__ . '/../../../config.php');
require_once ($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once ($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once ($CFG->libdir . '/excellib.class.php');


$errjob = [];
$arrcon = [];
$arrcom = [];
$tcount = 0;
$jbcount = 0;
$arcount = 0;
$comcount = 0;

if (isset($_FILES['excell_contents'])) {
    $filename = $_FILES['excell_contents']['name'];
    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    if (!in_array($ext, array('xlsx', 'xls'))) {
        $returnparam['errmsg'] = "확장자는 엑셀파일 xlsx, xls 만 업로드 가능합니다";
        $returnparam['success'] = 0;
    } else {
        $filepath = $_FILES['excell_contents']['tmp_name'];
        $objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
        $objReader->setReadDataOnly(true);
        $objExcel = $objReader->load($filepath);

        $objExcel->setActiveSheetIndex(0);
        $objWorksheet = $objExcel->getActiveSheet();
        $rowIterator = $objWorksheet->getRowIterator();

        foreach ($rowIterator as $row) {   // 모든 행에 대해서
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
        }
        $maxRow = $objWorksheet->getHighestRow();   // 가장 큰 행의 값
        $tname = "ourcomreco";

        for ($i = 2; $i <= $maxRow; $i++) {

            $recodata = new stdClass();
            $librarydata = $DB->get_record('library', array('id' => $objWorksheet->getCell('A' . $i)->getValue()));
            $cust_cd = $objWorksheet->getCell('B' . $i)->getValue();
            $areadysql = "select * from {dlp_reco_$tname} where libraryid=:libraryid and bl_co_cd =:bl_co_cd";
            $aready = $DB->get_record_sql($areadysql, array('libraryid' => $objWorksheet->getCell('A' . $i)->getValue(), 'bl_co_cd' => $cust_cd));
            $sql = " select * from  LGAPORTAL.dbo.T_SLS_CUST_M_damo  where CUST_CD = ? ";
            $comdata = $DB->get_record_sql($sql, array($cust_cd));
            if ($librarydata && !$aready) {
                $recodata->libraryid = $librarydata->id;
                $recodata->timecreated = time();
                $recodata->timemodified = time();
                $recodata->startdate = time();
                $recodata->userid = $USER->id;
                $recodata->sort = 0;
                $recodata->type = 0;
                $recodata->enddate = time();
                $recodata->category1 = $librarydata->category1;
                $recodata->s_score = 0;
                $recodata->temp_categoryid = $temp_categoryid;
                $logdata->userid = $USER->id;
                $logdata->libraryid = $librarydata->id;
                $logdata->tablename = "$tname";
                $logdata->tablename_ko = $tabs2[$tabs[$type]];
                $logdata->event = 'I';
                $logdata->eventtime = time();
                if ($comdata) {
                    $recodata->bl_co_cd = $cust_cd;
                    $DB->insert_record("dlp_reco_$tname", $recodata);
                    $DB->insert_record("dlp_reco_log", $logdata);
                } else {
                    $arrcom[] = $objWorksheet->getCell('A' . $i)->getValue();
                    $comcount++;
                }
            } else {
                $arrcon[] = $objWorksheet->getCell('A' . $i)->getValue();
                $arcount++;
            }
            $tcount++;
        }
        $returnparam['errmsg'] = '업로드 완료';
        $returnparam['arrcon'] = implode(',', $arrcon);
        $returnparam['errjob'] = implode(',', $errjob);
        $returnparam['arrcom'] = implode(',', $arrcom);
        $returnparam['comcount'] = $comcount;
        $returnparam['jbcount'] = $jbcount;
        $returnparam['arcount'] = $arcount;
        $returnparam['tcount'] = $tcount++;
        $returnparam['success'] = 1;
    }
}



echo json_encode($returnparam);

