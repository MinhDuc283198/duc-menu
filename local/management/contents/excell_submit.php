<?php

require_once(__DIR__ . '/../../../config.php');
require_once ($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once ($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once ($CFG->libdir . '/excellib.class.php');

$tabs = ['1' => 'main','2' => 'leader', '3' => 'pop', '4' => 'favorite', '5' => 'companion', '6' => 'comppop'];
$tabs2 = ['main' => 'LG 리더십아카데미 추천', 'leader' => '경제연구원', 'pop' => '실시간 Hot&new', 'favorite' => '관심분야', 'companion' => '동료들이 많이본 컨텐츠','comppop' => '우리회사 인기'];


$errjob = [];
$arrcon = [];
$arrcom = [];
$tcount = 0;
$jbcount = 0;
$arcount = 0;
$comcount = 0;

if (isset($_FILES['excell_contents'])) {
    $filename = $_FILES['excell_contents']['name'];
    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    if (!in_array($ext, array('xlsx', 'xls'))) {
        $returnparam['errmsg'] = "확장자는 엑셀파일 xlsx, xls 만 업로드 가능합니다";
        $returnparam['success'] = 0;
    } else {
        $filepath = $_FILES['excell_contents']['tmp_name'];
        $objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
        $objReader->setReadDataOnly(true);
        $objExcel = $objReader->load($filepath);

        $objExcel->setActiveSheetIndex(0);
        $objWorksheet = $objExcel->getActiveSheet();
        $rowIterator = $objWorksheet->getRowIterator();

        foreach ($rowIterator as $row) {   // 모든 행에 대해서
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
        }
        $maxRow = $objWorksheet->getHighestRow();   // 가장 큰 행의 값
        $type = $objWorksheet->getCell('A' . 9)->getValue();
        $tname = '';
        if ($tabs[$type] == 'main') {
            $tname = 'dlp_main_content';
        } else {
            $tname = "dlp_reco_$tabs[$type]";
        }
        for ($i = 11; $i <= $maxRow; $i++) {

            $recodata = new stdClass();
            $librarydata = $DB->get_record('library', array('id' => $objWorksheet->getCell('A' . $i)->getValue()));
            $jobgroup = $objWorksheet->getCell('B' . $i)->getValue();
            $cust_cd = $objWorksheet->getCell('C' . $i)->getValue();
            $temp_categoryid = $objWorksheet->getCell('D' . $i)->getValue() == null ? null :$objWorksheet->getCell('D' . $i)->getValue();
            if($type == 6){
                $areadysql = "select * from {dlp_reco_$tabs[$type]} where libraryid=:libraryid and bl_co_cd =:bl_co_cd";
                $aready = $DB->get_record_sql($areadysql , array('libraryid' => $objWorksheet->getCell('A' . $i)->getValue(),'bl_co_cd'=>$cust_cd));
            }else  if($type == 4 && $temp_categoryid == null){
                $areadysql = "select * from {dlp_reco_$tabs[$type]} where libraryid=:libraryid and temp_categoryid is null ";
                $aready = $DB->get_record_sql($areadysql , array('libraryid' => $objWorksheet->getCell('A' . $i)->getValue(),'temp_categoryid'=>$temp_categoryid));
            }else  if($type == 4 && $temp_categoryid != null){
                $areadysql = "select * from {dlp_reco_$tabs[$type]} where libraryid=:libraryid and temp_categoryid =:temp_categoryid";
                $aready = $DB->get_record_sql($areadysql , array('libraryid' => $objWorksheet->getCell('A' . $i)->getValue(),'temp_categoryid'=>$temp_categoryid));
            }else{
                $aready = $DB->get_record("$tname", array('libraryid' => $objWorksheet->getCell('A' . $i)->getValue()));
            }
            
            if ($type == 5) {
                $jobgroupdata = $DB->get_records('dlp_jobgroup', array('jobgroupid' => $jobgroup));
            }elseif($type == 6){
                $sql = " select * from  LGAPORTAL.dbo.T_SLS_CUST_M_damo  where CUST_CD = ? ";
                $comdata = $DB->get_record_sql($sql, array($cust_cd));
            }
            if ($librarydata && !$aready) {
                $recodata->libraryid = $librarydata->id;
                $recodata->timecreated = time();
                $recodata->timemodified = time();
                $recodata->startdate = time();
                $recodata->userid = $USER->id;
                $recodata->sort = 0;
                $recodata->type = 0;
                $recodata->enddate = time();
                $recodata->category1 = $librarydata->category1;
                $recodata->s_score = 0;
                $recodata->temp_categoryid = $temp_categoryid;
                $logdata->userid = $USER->id;
                $logdata->libraryid = $librarydata->id;
                $logdata->tablename = "$tname";
                $logdata->tablename_ko = $tabs2[$tabs[$type]];
                $logdata->event = 'I';
                $logdata->eventtime = time();
                if ($type == 5) {
                    if ($jobgroupdata) {
                        $recodata->jobgroup = $jobgroup;
                        $DB->insert_record("$tname", $recodata);
                        $DB->insert_record("dlp_reco_log", $logdata);
                    } else {
                        $errjob[] = $objWorksheet->getCell('A' . $i)->getValue();
                        $jbcount++;
                    }
                }elseif($type == 6){ 
                     if ($comdata) {
                        $recodata->bl_co_cd = $cust_cd;
                        $DB->insert_record("$tname", $recodata);
                        $DB->insert_record("dlp_reco_log", $logdata);
                    } else {
                        $arrcom[] = $objWorksheet->getCell('A' . $i)->getValue();
                        $comcount++;
                    }
                }else {
                    $DB->insert_record("$tname", $recodata);
                    $DB->insert_record("dlp_reco_log", $logdata);
                }
            } else {
                $arrcon[] = $objWorksheet->getCell('A' . $i)->getValue();
                $arcount++;
            }
            $tcount++;
        }
        $returnparam['errmsg'] = '업로드 완료';
        $returnparam['arrcon'] = implode(',', $arrcon);
        $returnparam['errjob'] = implode(',', $errjob);
        $returnparam['arrcom'] = implode(',', $arrcom);
        $returnparam['comcount'] = $comcount;
        $returnparam['jbcount'] = $jbcount;
        $returnparam['arcount'] = $arcount;
        $returnparam['tcount'] = $tcount++;
        $returnparam['success'] = 1;
    }
}



echo json_encode($returnparam);

