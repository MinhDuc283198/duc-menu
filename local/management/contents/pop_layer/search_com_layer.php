
<div class = "modal" id="searchcom" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <h5 class = "modal-title">회사 검색</h5>
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
                    <span aria-hidden = "true">&times;
                    </span>
                </button>
            </div>
            <form onsubmit="searchcom(); return false;">
                <div class = "modal-body">
                    <div class = "custom-file">
                        <input type = "text" class = "form-control comcode"  placeholder="회사코드를 입력해주세요"/>
                        <button class = "btn btn-default comsearch">검색</button>
                    </div>
                </div>
            </form>
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th>회사코드</th>
                        <th>회사명</th>
                        <th>선택</th>
                    </tr>
                </thead>
                <tbody class="appendcom">
                    <tr>
                        <td colspan="3">회사를 검색해 주세요</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>