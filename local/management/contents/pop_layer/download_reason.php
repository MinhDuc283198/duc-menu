<div class = "modal" id="xlsdownlayer" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <h5 class = "modal-title">다운로드 사유</h5>
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
                    <span aria-hidden = "true">&times;
                    </span>
                </button>
            </div>
            <div class = "modal-body">
                <div class = "custom-file">
                    <div class="inp-form-wp">
                        <h3> 다운로드 사유를 입력하세요.</h3>
                        <div class="inp-cont">
                            <p><textarea class="form-control" name="download_reasoninput" style="resize: none; height: 150px;" placeholder="ex) 교육참가자 숙소안내용"></textarea></p>
                        </div>
                    </div>
                </div>
                <div class="t-center">
                    <button type="button" class="btn btn-primary submit_reason">확인</button>
                    <button type="button" class="btn cancle_reason" >취소</button>
                </div>
            </div>

        </div>
    </div>
</div>