<!--엑셀업로드 팝업 -->
<div class = "modal" id="modal" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <h5 class = "modal-title">엑셀 업로드</h5>
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
                    <span aria-hidden = "true">&times;
                    </span>
                </button>
            </div>
            <div class = "modal-body">
                <form id="excell_form" action="excell_submit.do" method="post" enctype="multipart/form-data" class = "file-form" onsubmit="return false">
                    <div class = "custom-file">
                        <input type = "text" class = "form-control" />
                        <label for = "fd" class = "btn btn-default">파일찾기</label>
                        <input type = "file" id = "fd" name="excell_contents" />
                    </div>
                    <input type = "button" value = "등록" class = "btn btn-primary btn-danger addexcell" />
                </form>
                <input type = "button" value = "양식다운로드" class = "btn btn-primary m-auto exdown" />
                <p class = "t-info">엑셀 업로드 시, 시스템에서 엑셀 양식을 다운로드 하신 엑셀 화일을 이용해 주세요.</p>
            </div>

        </div>
    </div>
</div>
