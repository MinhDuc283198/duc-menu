<?php

require('../../../config.php');
require_once('lib.php');

$type = required_param('type', PARAM_RAW);
$conids = optional_param_array('conid', '', PARAM_RAW);

$tname = '';
if ($type == 'main') {
    $tname = 'dlp_main_content';
} else {
    $tname = "dlp_reco_$type";
}

$tabs = [ 'main' => 'LG 리더십아카데미 추천','leader' => '경제연구원', 'pop' => '실시간 Hot&new', 'favorite' => '관심분야', 'companion' => '동료들이 많이본 컨텐츠'];

foreach ($conids as $conid) {
    $libraryid = $DB->get_field("$tname",'libraryid', array('id' => $conid));
    $DB->delete_records("$tname", array('id' => $conid));
    $logdata->userid = $USER->id;
    $logdata->libraryid = $libraryid;
    $logdata->tablename = "dlp_reco_$type";
    $logdata->tablename_ko = $tabs[$type];
    $logdata->event = 'D';
    $logdata->eventtime = time();
    $DB->insert_record("dlp_reco_log", $logdata);
}

