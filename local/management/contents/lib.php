<?php

function management_contnets_get_contents($type, $page = 0, $perpage = 0, $stype, $scate, $stext, $ordertype, $orderitem) {
    global $DB;
    $tname = '';
    if ($type == 'main') {
        $tname = 'dlp_main_content';
    } else {
        $tname = "dlp_reco_$type";
    }
    $whereelements = [];
    $where = '';
    if ($stext != '') {
        if ($stype == 0) {
            $whereelements[] = "( drl.libraryid like :stext or l.subject like :stext2 )";
            $params['stext'] = "%$stext%";
            $params['stext2'] = "%$stext%";
        } elseif ($stype == 1) {
            $whereelements[] = "( drl.libraryid like :stext )";
            $params['stext'] = "%$stext%";
        } elseif ($stype == 2) {
            $whereelements[] = "( l.subject like :stext )";
            $params['stext'] = "%$stext%";
        } elseif ($stype == 3 && ( $type == 'comppop' || $type =='ourcomreco' )) {
            $whereelements[] = "( tsc.cust_cd like :stext or tsc.cust_if_nm like :stext2 )";
            $params['stext'] = "%$stext%";
            $params['stext2'] = "%$stext%";
        }
    }

    if ($scate != 0) {
        $whereelements[] = " l.category1 =  :scate ";
        $params['scate'] = $scate;
    }

    if (!empty($whereelements)) {
        $where = " WHERE  " . implode(' AND ', $whereelements);
    }

    $params['lang'] = current_language();
    $addjoin = ' ';
    $addselect = '  ';
    $order_str = '';
    if ($type == 'comppop' || $type =='ourcomreco') {
        $addselect = " ,tsc.cust_cd,tsc.cust_if_nm ";
        $addjoin = " JOIN LGAPORTAL.dbo.T_SLS_CUST_M_damo tsc ON drl.bl_co_cd =  tsc.cust_cd ";
        $orderarr = array("comname" => "tsc.cust_if_nm  COLLATE Korean_Wansung_BIN ", "comcode" => "tsc.cust_cd");
        if (empty($ordertype) && !empty($orderitem)) {
            $order_str = "ORDER BY $orderarr[$orderitem] DESC";
        } else if ($ordertype == 'up') {
            $order_str = "ORDER BY $orderarr[$orderitem] ASC";
        } else if ($ordertype == 'dash') {
            $order_str = "ORDER BY drl.timecreated desc";
        }
    } else {
        $order_str = ' ORDER BY drl.timecreated desc';
    }

    $content_sql = "SELECT drl.id drlid ,l.*, drl.*, IsNULL( dcl.name, 'categoryname' ) AS catename, drl.timecreated  $addselect
                    FROM {" . $tname . "} drl
                    JOIN {library} l ON l.id = drl.libraryid AND openscope != 'n'
                    $addjoin
                    LEFT JOIN {dlp_category_lang} dcl ON dcl.categoryid = l.category1 AND dcl.lang = :lang ";

    if ($perpage == 0) {
        $content = $DB->get_records_sql($content_sql . $where . $order_str, $params);
    } else {
        $content = $DB->get_records_sql($content_sql . $where . $order_str, $params, ($page - 1) * $perpage, $perpage);
    }
//    print_object($content);
    return $content;
}

function management_contents_category() {
    global $DB;
    $sql = "select * from {dlp_category} dc join {dlp_category_lang} dcl on dcl.categoryid = dc.categoryid where dc.depth = 1  AND dcl.lang = ? ";
    $data = $DB->get_records_sql($sql, array(current_language()));
    return $data;
}

function management_contnets_get_total($type, $stype, $scate, $stext) {
    global $DB;
    $tname = '';
    if ($type == 'main') {
        $tname = 'dlp_main_content';
    } else {
        $tname = "dlp_reco_$type";
    }

    $whereelements = [];
    if ($stext != '') {
        if ($stype == 0) {
            $whereelements[] = "( drl.libraryid like :stext or l.subject like :stext2 )";
            $params['stext'] = "%$stext%";
            $params['stext2'] = "%$stext%";
        } elseif ($stype == 1) {
            $whereelements[] = "( drl.libraryid like :stext )";
            $params['stext'] = "%$stext%";
        } elseif ($stype == 2) {
            $whereelements[] = "( l.subject like :stext )";
            $params['stext'] = "%$stext%";
        } elseif ($stype == 3 && ( $type == 'comppop' || $type =='ourcomreco' )) {
            $whereelements[] = "( tsc.cust_cd like :stext or tsc.cust_if_nm like :stext2 )";
            $params['stext'] = "%$stext%";
            $params['stext2'] = "%$stext%";
        }
    }

    if ($scate != 0) {
        $whereelements[] = " l.category1 =  :scate ";
        $params['scate'] = $scate;
    }

    if (!empty($whereelements)) {
        $where = " WHERE  " . implode(' AND ', $whereelements);
    }

    $params['lang'] = current_language();
    $content_sql = "SELECT count(*)
                    FROM {" . $tname . "} drl
                    JOIN {library} l ON l.id = drl.libraryid AND openscope != 'n'
                    LEFT JOIN {dlp_category_lang} dcl ON dcl.categoryid = l.category1 AND dcl.lang = :lang ";

    $count = $DB->count_records_sql($content_sql . $where, $params);

    return $count;
}

;

function management_contnets_paing($page, $perpage, $tcount, $type, $url, $block = 10) {
    global $DB;
    $totalcount = $tcount;
    if ($totalcount != 0) {
        $pageNum = ceil($totalcount / $perpage);
        $blockNum = ceil($pageNum / $block);
        $nowBlock = ceil($page / $block);
        $s_page = ($nowBlock * $block) - ($block - 1);
        if ($s_page <= 1) {
            $s_page = 1;
        }
        $e_page = $nowBlock * $block;
        if ($pageNum <= $e_page) {
            $e_page = $pageNum;
        }
        if ($nowBlock != 1) {
            $pagedata .= '<li class = "page-item prev"><a class="last" href="' . $url . '&type=' . $type . '&page=' . ($s_page - 1 ) . '" page=' . ($s_page - 1 ) . '>«</a></li>';
        }
        for ($p = $s_page; $p <= $e_page; $p++) {
            if ($p == $page) {
                $pagedata .= ' <li class = "page-item active"><a class = "page-link">' . $p . '</a></li>';
            } else {
                $pagedata .= '<li class = "page-item"><a class = "page-link" page=' . $p . ' href = "' . $url . '&type=' . $type . '&page=' . $p . '">' . $p . '</a></font>';
            }
        }
        if ($nowBlock != $blockNum) {
            $pagedata .= '<li class = "page-item next"><a class="last" href="' . $url . '&type=' . $type . '&page=' . ($e_page + 1) . '" page=' . ($e_page + 1) . '>»</a></li>';
        }
    }
    return $pagedata;
}
