<?php

header('Set-Cookie: fileDownload=true; path=/');
require_once(__DIR__ . '/../../../config.php');
require_once('lib.php');
require_once("$CFG->libdir/excellib.class.php");

$type = optional_param('type', 'main', PARAM_RAW);
$stype = optional_param('stype', '', PARAM_RAW);
$scate = optional_param('scate', '', PARAM_RAW);
$stext = optional_param('stext', '', PARAM_RAW);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);

$tabs = ['main' => 'LG 리더십아카데미 추천', 'leader' => '경제연구원', 'pop' => '실시간 Hot&new', 'favorite' => '관심분야', 'companion' => '동료들이 많이본 컨텐츠', 'comppop' => '우리회사 인기', 'ourcomreco' => '우리회사 추천'];
$downloadfilename = clean_filename("$tabs[$type] " . date('Y-m-d', time()) . ".xls");
$workbook = new MoodleExcelWorkbook("-");
$workbook->send($downloadfilename);
$sheet = $workbook->add_worksheet("");

$xlscols = array(
    'head' => array('콘텐츠 ID', ' 콘텐츠 타입 ', ' 카테고리 ', ' 콘텐츠명 ', '등록날짜 ')
);

if ($type == 'comppop' || $type == 'ourcomreco') {
    $sql = " select cust_cd,cust_if_nm from  LGAPORTAL.dbo.T_SLS_CUST_M_damo  ";
    $custs = $DB->get_records_sql($sql);
    $xlscols['head'][] = '회사';
}
if ($type == 'companion') {
    $sql = " select jobgroupid ,jobgroup from {dlp_jobgroup}  ";
    $jobgroups = $DB->get_records_sql($sql);
    $xlscols['head'][] = '직무';
}
if ($type == 'comppop' || $type == 'ourcomreco') {
$xlscols['head'][] = '회사코드';
}else if ($type == 'favorite') {
$xlscols['head'][] = '카테고리코드';
}

$contents = management_contnets_get_contents($type, 0, 0, $stype, $scate, $stext, $ordertype, $orderitem);


$xlsformat = array(
    'head' => array('border' => 1, 'align' => 'center', 'v_align' => 'center', 'bg_color' => '#d0d0d0', 'text_wrap' => true),
    'default' => array('border' => 1, 'v_align' => 'center', 'bg_color' => 'white'),
);


$col = 0;
$row = 0;
foreach ($xlscols['head'] as $head) {
    $sheet->write_string($row, $col, $head, $xlsformat['head']);
    $col += 1;
}
$row += 1;
foreach ($contents as $content) {
    $col = 0;
    $sheet->write($row, $col++,  $content->libraryid , $xlsformat['default']);
    $sheet->write($row, $col++, $content->filetype, $xlsformat['default']);
    $sheet->write($row, $col++, $content->catename, $xlsformat['default']);
    $sheet->write($row, $col++, $content->subject, $xlsformat['default']);
    $sheet->write($row, $col++, date("Y-m-d", $content->timecreated), $xlsformat['default']);
    if ($type == 'comppop' || $type == 'ourcomreco') {
       $sheet->write($row, $col++, $custs[$content->bl_co_cd]->cust_if_nm, $xlsformat['default']);
       $sheet->write($row, $col++, $content->bl_co_cd, $xlsformat['default']);
    }
    if ($type == 'favorite') { 
        $sheet->write($row, $col++,$content->temp_categoryid == null ? $content->category1 : $content->temp_categoryid , $xlsformat['default']);
    }
    if ($type == 'companion') {
       $sheet->write($row, $col++, $jobgroups[$content->jobgroup]->jobgroup, $xlsformat['default']);
    }
    $row += 1;
}

$workbook->close();

die();
