<?php

require('../../../config.php');

$conid = required_param('conid',PARAM_INT);
$sql = " SELECT l.*, IsNULL( dcl.name, 'categoryname' ) AS catename
         from {library} l 
         LEFT JOIN {dlp_category_lang} dcl ON dcl.categoryid = l.category1 AND dcl.lang = :lang where l.id =:id ";
$data = $DB->get_record_sql($sql,array('id' => $conid,'lang' => current_language()));

$datajson['id']= $data->id;
$datajson['subject']= $data->subject;
$datajson['filetype']= $data->filetype;
$datajson['catename']= $data->catename;
$datajson['status']= $data->id == true ? true : false;
        
echo json_encode($datajson);