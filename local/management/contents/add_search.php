<?php

require('../../../config.php');

$conid = required_param('conid', PARAM_INT);
$type = required_param('type', PARAM_RAW);
$jobgroup = optional_param('jobgroup', 0, PARAM_RAW);
$bl_co_cd = optional_param('bl_co_cd', 0, PARAM_RAW);
$tname = '';

if ($type == 'main') {
    $tname = 'dlp_main_content';
} else {
    $tname = "dlp_reco_$type";
}
$librarydata = $DB->get_record('library', array('id' => $conid));
if($type == 'comppop' || $type == 'ourcomreco'){
$aready = $DB->get_record("$tname", array('libraryid' => $conid,'bl_co_cd'=>$bl_co_cd));
}else{
$aready = $DB->get_record("$tname", array('libraryid' => $conid));
}
$tabs = ['main' => 'LG 리더십아카데미 추천', 'leader' => '경제연구원', 'pop' => '실시간 Hot&new', 'favorite' => '관심분야', 'companion' => '동료들이 많이본 컨텐츠','comppop' => '우리회사 인기','ourcomreco' => '우리회사 추천'];


$returndata = array();
if (!$aready) {
    $recodata->libraryid = $librarydata->id;
    $recodata->timecreated = time();
    $recodata->timemodified = time();
    $recodata->startdate = time();
    $recodata->bl_co_cd = $bl_co_cd;
    $recodata->enddate = time();
    $recodata->userid = $USER->id;
    $recodata->sort = 0;
    $recodata->type = 0;
    $recodata->category1 = $librarydata->category1;
    $recodata->s_score = 0;
    $recodata->jobgroup = $jobgroup;
    $DB->insert_record("$tname", $recodata);
    $logdata->userid = $USER->id;
    $logdata->libraryid = $librarydata->id;
    $logdata->tablename = $tname;
    $logdata->tablename_ko = $tabs[$type];
    $logdata->event = 'I';
    $logdata->eventtime = time();
    $DB->insert_record("dlp_reco_log", $logdata);
    $returndata['status'] = 'success';
} else {
    $returndata['status'] = 'false';
    $returndata['text'] = '이미 추천 항목에 존재하는 콘텐츠 입니다';
}


echo json_encode($returndata);
