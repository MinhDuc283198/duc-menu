$(document).ready(function () {

    $("select[name=perpage]").change(function () {
        $("form[name=search_form]").submit();
    });
    $(".comsearch").click(function () {
        searchcom();
    });
    //checkbox 전체선택
    $("input.checkall").click(function () {
        var check = $(this).prop("checked");
        $(this).closest("table").find("input[type=checkbox]").prop("checked", check);
    });
    $(".table input[type=checkbox]").not(".checkall").click(function () {
        if (!$(this).prop("checked")) {
            $("input.checkall").prop("checked", false);
        }
    });

    //custom file 이벤트
    var fileTarget = $('.custom-file input[type=file]');
    fileTarget.on('change', function () {
        if (window.FileReader) {
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.form-control').val(filename);
    });


    //엑셀업로드 팝업 이벤트
    $(".addpop").click(function () {
        $("#modal").show();
    });
    $("#modal .close").click(function () {
        $(this).closest("#modal").hide();
    });
    //회사검색
    $(".searchcom").click(function () {
        $(".comcode").val('');
        $("#searchcom").show();
    });
    $("#searchcom .close").click(function () {
        $(this).closest("#searchcom").hide();
    });
    //다운로드 사유
    $(".xlsdown").click(function () {
        $("#xlsdownlayer").show();
    });
    $("#xlsdownlayer .close").click(function () {
        $(this).closest("#xlsdownlayer").hide();
    });

    $(".searchadd").click(function () {
        searchadd();
    });
    $(".exdown").click(function () {
        if ($("input[name=type]").val() == 'ourcomreco') {
            location.href = "ourcomreco.xlsx";
        } else {
            location.href = "recommendedcontents.xlsx";
        }
    });
    $(".addexcell").click(function () {
        addexcell();
    });

    $(document).on('click', '.cancle_reason', function () {
        $("#xlsdownlayer").hide();
    });
    $(document).on('click', '.submit_reason', function () {

        var courseid = 0;
        var xlsurl = 'contents_xls.php';
        var button = $(this);
        button.hide();
        var reason = $('textarea[name=download_reasoninput]').val();
        var url_split = window.location.hostname;
        var url_split2 = window.location.href;
        var url = url_split2.split(url_split);
        var formData = new FormData();
        formData.append("reason", '[ 추천컨텐츠 다운로드 ]' + reason);
        formData.append("url", url[1]);
        formData.append("courseid", courseid);
        $.ajax({
            url: "/local/dlp/download_reason.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                if (data) {
                    var formData = $("form[name='search_form']").serialize();
                    console.log(formData);
                    $.fileDownload(xlsurl, {httpMethod: "POST", data: formData}).done(function () {
                    }).fail(function () {
                        alert('다운로드에 실패하였습니다. 관리자에게 문의 주세요.');
                    });
                    $("#xlsdownlayer").hide();
                } else {
                    alert('사유 등록에 실패하였습니다. 다시 시도해주세요.');
                    button.show();
                }

            }
        });
    });
    $(".tb-arrow").click(function () {
        $('input[name=orderitem]').val($(this).attr("id"));
        var type = ' ';
        if ($(this).hasClass("dash")) {
            type = 'up';
        } else if ($(this).hasClass("up")) {
            type = '';
        } else {
            type = 'dash';
        }
        $('input[name=ordertype]').val(type);
        $("form[name=search_form]").submit();
    });
});
function selectcom(code, name) {
    $('.comname').empty().text(name + '[' + code + ']');
    $("#bl_co_cd").val(code);
    $("#searchcom").hide();
}
function movepage(type) {
    location.href = "index.php?type=" + type;
}
function consearch() {
    var conid = $("#consearchid").val();
    if (conid == '') {
        alert('콘텐츠 아이디를 입력해주세요');
    } else {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'contents_search.php',
            data: {conid: conid},
            success: function (data, status, xhr) {
                if (data.status == false) {
                    alert('없는 콘텐츠 ID 입니다.');
                } else {
                    $(".sid").empty().text(data.id);
                    $(".stype").empty().text(data.filetype);
                    $(".scate").empty().text(data.catename);
                    $(".sname").empty().append("<a href='javascript:opensearch(" + data.id + ")'>" + data.subject + "</a>");
                }
            },
        });
    }
}

function addexcell() {
    var form = $('#excell_form')[0];
    var formData = new FormData(form);
    var url = '';
    if ($("input[name=type]").val() == 'ourcomreco') {
        url = 'ourcomreco_submit.php';
    } else {
        url = 'excell_submit.php';
    }
    $.ajax({
        url: url,
        processData: false,
        contentType: false,
        data: formData,
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            if (result.success == 0) {
                alert(result.errmsg);
            } else {
                alert("업로드 완료  총 " + result.tcount + " 중 \n 이미 존재 하는 콘텐츠 " + result.arcount + " 개 ID : " + result.arrcon + " \n 직무코드 오류 " + +result.jbcount + " 개 ID " + result.errjob + " \n 회사코드 오류 " + +result.comcount + " 개 ID " + result.arrcom);
                location.reload();
            }
        }
    });
}
function opensearch(libid) {
    var url = '/local/library/detail.php?id=' + libid;
    var popup = null;
    var width = 1560;
    var height = screen.height - 140;
    if (screen.width < 1560) {
        width = screen.width - 100;
    }
    var left = (screen.width - width) / 2;
    var top = 0;
    settings = 'height=' + height + ',width=' + width + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes,status=no';
    if (sessionStorage.getItem("app") == 'Y') {
        var hosturl = window.location.href.substr(0, window.location.href.length - 1);
        utils.openPopup(location.protocol + "//" + location.host + url);
    } else {
        popup = window.open(url, 'library', settings);
        popup.window.focus();
    }
}

function searchcom() {
    if ($(".comcode").val() == '') {
        alert('코드를 입력해주세요');
    } else {
        $.ajax({
            type: 'POST',
            url: 'search_company.php',
            data: {comcode: $(".comcode").val()},
            success: function (data, status, xhr) {
                $(".appendcom").empty().append(data);
            },
        });
    }
    return false;
}