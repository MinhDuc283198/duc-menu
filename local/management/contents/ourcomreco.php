<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once('lib.php');
$pagesettings = array(
    'title' => '우리회사 추천',
    'heading' => '우리회사 추천',
    'subheading' => '',
    'menu' => 'comrecommend',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');

$type = optional_param('type', 'ourcomreco', PARAM_RAW);
$stype = optional_param('stype', '', PARAM_RAW);
$scate = optional_param('scate', '', PARAM_RAW);
$stext = optional_param('stext', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$orderitem = optional_param('orderitem', '', PARAM_RAW);
$ordertype = optional_param('ordertype', '', PARAM_RAW);

$perpages = [10, 30, 50, 100];
$tabs = ['ourcomreco' => '우리회사추천'];

$contents = management_contnets_get_contents($type, $page, $perpage, $stype, $scate, $stext,$ordertype,$orderitem);
//print_object($contents);
$totalcount = management_contnets_get_total($type, $stype, $scate, $stext);
$addpagingparam = '';
if ($type == 'comppop') {
    $addpagingparam = "&orderitem=$orderitem&ordertype=$ordertype";
    $sql = " select cust_cd,cust_if_nm from  LGAPORTAL.dbo.T_SLS_CUST_M_damo  ";
    $custs = $DB->get_records_sql($sql);
}
$paging = management_contnets_paing($page, $perpage, $totalcount, $type, "index.php?stype=$stype&scate=$scate&stext=$stext&perpage=$perpage$addpagingparam");

if ($type == 'companion') {
    $sql = " select jobgroupid ,jobgroup from {dlp_jobgroup}  ";
    $jobgroups = $DB->get_records_sql($sql);
}

$catelists = management_contents_category();
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <ul class="nav nav-tabs">
                    <?php
                    foreach ($tabs as $key => $tab) {
                        $active = $key == $type ? 'active' : '';
                        ?>
                        <li class = "nav-item  <?php echo $active ?> ">
                            <a class = "nav-link" data-toggle = "" href="javascript:movepage('<?php echo $key ?>');"><?php echo $tab ?></a>
                        </li>
                    <?php } ?>
                </ul>

                <div class = "tab-content">

                    <div id = "data1" class = "tab-pane fade in active">
                        <div>

                            <form action="ourcomreco.php" method="get" name='search_form'>
                                <input type='hidden' name='type' value="<?php echo $type ?>">
                                <input type="hidden" name="orderitem" value="<?php echo $orderitem?>"/>
                                <input type="hidden" name="ordertype" value="<?php echo $ordertype?>"/>
                                <select name="stype">
                                    <option value="0" <?php echo $stype == 0 ? 'selected' : '' ?>>검색타입</option>
                                    <option value="1" <?php echo $stype == 1 ? 'selected' : '' ?>>콘텐츠ID</option>
                                    <option value="2" <?php echo $stype == 2 ? 'selected' : '' ?>>콘텐츠명</option>
                                    <option value="3" <?php echo $stype == 3 ? 'selected' : '' ?>>회사코드/회사명</option>
                                </select>
                                <select  name="scate">
                                    <option value="0">카테고리</option>
                                    <?php foreach ($catelists as $catelist) { ?>
                                        <option value="<?php echo $catelist->categoryid ?>" <?php echo $scate == $catelist->categoryid ? 'selected' : '' ?>><?php echo $catelist->name ?></option>
                                    <?php } ?>
                                </select>
                                <input type="text" name="stext" value="<?php echo $stext != '' ? $stext : null ?>">
                                <input type="submit" value="검색">
                                <select class="f-r" name="perpage">
                                    <?php foreach ($perpages as $perp) { ?>
                                        <option value="<?php echo $perp ?>" <?php echo $perpage == $perp ? 'selected' : '' ?>><?php echo $perp ?></option>
                                    <?php } ?>
                                </select>
                            </form>

                        </div>
                        <table class = "table table-bordered">
                            <thead class = "thead-light">
                                <tr>
                                    <th>
                                        <input type = "checkbox" class = "checkall" />
                                    </th>
                                    <th>NO</th>
                                    <th>콘텐츠 ID</th>
                                    <th>콘텐츠 타입</th>
                                    <th>카테고리</th>
                                    <th>콘텐츠명</th>
                                    <th>등록날짜</th>
                                    <th class="w-10"><span class="tb-arrow <?php echo $orderitem == 'comname' ? $ordertype : 'dash' ;?>" id="comname">회사</span></th> 
                                    <th class="w-10"><span class="tb-arrow <?php echo $orderitem == 'comcode' ? $ordertype : 'dash' ;?>" id="comcode">회사코드</span></th> 
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($contents) {
                                    $startnum = (($page - 1) * $perpage) + 1;
                                    foreach ($contents as $content) {
                                        ?>
                                        <tr>
                                            <td>
                                                <input type = "checkbox" class="conslist"  conid="<?php echo $content->drlid ?>"/>
                                            </td>
                                            <td><?php echo $startnum++ ?></td>
                                            <td><?php echo $content->libraryid ?> </td>
                                            <td><?php echo $content->filetype ?> </td>
                                            <td><?php echo $content->catename ?> </td>
                                            <td class = "text-left"><a href="javascript:opensearch(<?php echo $content->libraryid ?>)"><?php echo $content->subject ?> </a></td>
                                            <td><?php echo date("Y-m-d", $content->timecreated) ?> </td>
                                                <td class="w-10"><?php echo $content->cust_if_nm ?></td> 
                                                <td class="w-10"><?php echo $content->bl_co_cd ?></td> 
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                <td colspan="9">검색 결과가 없습니다.</td>
                            <?php } ?> 
                            </tbody>
                        </table>
                        <nav aria-label = "Page navigation" class = "text-center">
                            <ul class = "pagination">
                                <?php echo $paging ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class = "btns clearfix">
                <button class="btn btn-success pull-right xlsdown">엑셀다운로드</button> 
                <button class = "btn btn-danger pull-right addpop">엑셀업로드</button>
                <button class = "btn btn-primary  pull-right" onclick="removecontents();">삭제</button>
            </div>
            <!--/.box -->

            <div class = "box box-body">
                <div class = "search-form">
                    <form onsubmit="consearch(); return false">
                        <label for = "consearchid">콘텐츠 ID</label>
                        <input type = "text" class = "form-control" id = "consearchid" />
                        <input type = "button" class = "btn btn-default" onclick="consearch()" value = "정보확인" />
                    </form>
                </div>
                <table class = "table table-bordered">
                    <thead class = "thead-light">
                        <tr>
                            <th>콘텐츠 ID</th>
                            <th>콘텐츠 타입</th>
                            <th>카테고리</th>
                            <th>콘텐츠명</th>
                            <th class="w-10">회사</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="sid"></td>
                            <td class="stype"></td>
                            <td class="scate"></td>
                            <td class = "text-left sname" ></td>
                            <td>
                                <span class="comname"></span>
                                <button class = "btn btn-default searchcom">검색</button>
                                <input type="hidden" id="bl_co_cd">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class = "t-info">개별 등록은 콘텐츠 아이디를 입력 하신 후, 정보 확인 후 개별 등록을 하실 수 있습니다.<br/>여러개의 콘텐츠 등록을 원하시면, 엑셀 업로드 기능을 이용해 등록 해주세요. </p>
            </div>
            <div class = "btns clearfix">
                <button class = "btn btn-primary btn-danger  pull-right searchadd">등록</button>
            </div>

        </div>
        <!--/.col -->
    </div>

</section>
<!--/.content -->


<?php
//레이어
include_once($CFG->dirroot . '/local/management/contents/pop_layer/excell_down_layer.php');
include_once($CFG->dirroot . '/local/management/contents/pop_layer/search_com_layer.php');
include_once($CFG->dirroot . '/local/management/contents/pop_layer/download_reason.php');

include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">
    function removecontents() {
        if (confirm('선택한 항목을 삭제하시겠습니까?')) {
            var type = "<?php echo $type ?>";
            var conarr = new Array();
            $(".conslist:checked").each(function (i, v) {
                conarr.push($(v).attr("conid"));
            });
            $.ajax({
                type: 'POST',
                url: 'remove_reco.php',
                data: {conid: conarr, type: type},
                success: function (data, status, xhr) {

                    location.reload();
                },
            });
        }
    }
    function searchadd() {
        var addid = $(".sid").text();
        var type = '<?php echo $type ?>';
        var jobgroup = $("#jobgroup").val();
        var bl_co_cd = $("#bl_co_cd").val();
        if (addid == '') {
            alert('콘텐츠 정보 확인을 해주세요');
        } else {
            if (type == "comppop" && bl_co_cd == '') {
                alert('회사를 검색해주세요');
            } else {
                if (confirm(("콘텐츠 ID : " + addid + " 를 [ <?php echo $tabs[$type] ?> ] 항목에 추가하시겠습니까?"))) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: 'add_search.php',
                        data: {conid: addid, type: type, jobgroup: jobgroup, bl_co_cd: bl_co_cd},
                        success: function (data, status, xhr) {
                            if (data.status == 'false') {
                                alert(data.text);
                            } else {
                                location.reload();
                            }
                        },
                    });
                }
            }
        }
    }

</script>
