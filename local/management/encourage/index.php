<?php 
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/encourage/lib.php';

$pagesettings = array(
    'title' => '학습 독려 발송 템블릿',
    'heading' => '학습 독려 발송 템블릿',
    'subheading' => '',
    'menu' => 'encourage_template',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);


$categoryid = optional_param('category', 0, PARAM_INT);
$used = optional_param('used', 1, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$sort = optional_param('sort', 2, PARAM_INT); // 1:이름, 2:발송시간, 0:등록일
$asc = optional_param('asc', 1, PARAM_INT);

$limitfrom = ($page - 1) * $perpage;

$conditions = array('et.isdeleted = :isdeleted');
$params = array('isdeleted' => 0);
if($categoryid) {
    $conditions[] = 'et.categoryid = :categoryid';
    $params['categoryid'] = $categoryid;
}

if($used >= 0) {
    $conditions[] = 'et.isused = :isused';
    $params['isused'] = $used;
}

if(!empty($search)) {
    $conditionsearch = array();

    // 이름
    $conditionsearch[] = $DB->sql_like("et.name", ":name", false);
    $params['name'] = "%$search%";

    // 설명
    $conditionsearch[] = $DB->sql_like("et.description", ":description", false);
    $params['description'] = "%$search%";

    // 제목
    $conditionsearch[] = $DB->sql_like("et.title", ":title", false);
    $params['title'] = "%$search%";

    // 내용
    $conditionsearch[] = $DB->sql_like("et.message", ":message", false);
    $params['message'] = "%$search%";

    $conditions[] = '('.implode(' OR ', $conditionsearch).')';
}

$sortreftime = local_encourage_sortby_ref_times('ett1');
// 템플릿에 발송시점이 여러 개 있을 수 있기 때문에 "CROSS APPLY"를 이용해서 
// 가장 빠른 발송시점 하나만 JOIN 한다.
$sql_from = "FROM {encourage_template} et 
    JOIN {encourage_category} ec ON ec.id = et.categoryid
    CROSS APPLY (
        SELECT  TOP 1 ett1.reftime, ett1.day, ett1.hour, ett1.minute
        FROM    {encourage_template_time} ett1
        WHERE   ett1.templateid = et.id
        ORDER BY $sortreftime
    ) ett ";
$sql_where = "WHERE ".implode(' AND ', $conditions);

$sql_orderby = "";
$strasc = "ASC";

if($asc == 0) {
    $strasc = "DESC";
}

if($sort == 0) {
    $sql_orderby = "ORDER BY et.timecreated $strasc";
} else if($sort == 1) {
    $sql_orderby = "ORDER BY et.name $strasc";
} else if($sort == 2) {
    $sql_orderby = "ORDER BY " . local_encourage_sortby_ref_times('ett')
        . " $strasc, ett.day $strasc, ett.hour $strasc, ett.minute $strasc";
}


$templates = $DB->get_records_sql("SELECT et.*, ec.name AS categoryname $sql_from $sql_where $sql_orderby", $params, $limitfrom, $perpage);
$counttemplates = $DB->count_records_sql("SELECT COUNT(*) $sql_from $sql_where", $params);

$methods = local_encourage_get_methods();
$targets = local_encourage_get_targets();
$types = local_encourage_get_types();


include_once('../header.php');

?> 

<!---------- content start ---------->
<section class="content">
    <div class="contents_box encourage-templates">
        <div class="search-area box-header">
            <form id="form-search" action="index.php" class="form-horizontal">
                <input type="hidden" name="page" value="1"/>
                <input type="hidden" name="sort" value="<?php echo $sort; ?>"/>
                <input type="hidden" name="asc" value="<?php echo $asc; ?>"/>
                <div class="row">
                <div class="col-md-6">
                        <label for="categoryid" class="col-sm-1 control-label">분류</label>
                        <div class="col-sm-3">
                        <select id="categoryid" name="category" class="form-control">
                        <option value="0">전체</option>
                        <?php
                        $categories = $DB->get_records('encourage_category', array('isdeleted' => 0), 'sortorder ASC');
                        foreach($categories as $category) {
                            $selected = '';
                            if($categoryid == $category->id) {
                                $selected = ' selected';
                            }
                            echo '<option value="'.$category->id.'"'.$selected.'>'.$category->name.'</option>';
                        }
                        ?>
                        </select>
                        </div>
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" value="<?php echo $search; ?>"/>
                            <div class="input-group-btn">
                                <input type="submit" value="검색" class="btn btn-primary"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="isused" class="col-sm-9 control-label">사용여부</label>
                        <div class="col-sm-3">
                            <select id="isused" name="used" class="form-control w_100">
                                <option value="1"<?php if($used == 1) { ?> selected<?php } ?>>사용</option>
                                <option value="0"<?php if($used == 0) { ?> selected<?php } ?>>미사용</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="id_all" value="0" title="전체 선택/해제"/></th>
                                    <th>분류</th>
                                    <th>방법</th>
                                    <th>대상</th>
                                    <th>과정구분</th>
                                    <th class="sorting<?php if($sort == 1) { echo strtolower("_$strasc"); } ?>" data-sort="1">이름<br/>(제목)</th>
                                    <th class="sorting<?php if($sort == 2) { echo strtolower("_$strasc"); } ?>" data-sort="2">발송시간</th>
                                    <th class="sorting<?php if($sort == 0) { echo strtolower("_$strasc"); } ?>" data-sort="0">등록일</th>
                                    <th>사용여부</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($counttemplates > 0) {
                                //$num = ($page - 1) * $perpage + 1;
                                foreach ($templates as $template) {
                                    $method = $methods[$template->method];
                                    $target = $targets[$template->target];
                                    $isused = ($template->isused == 1) ? '사용' : '사용안함';
                                    $type = $types[$template->type];

                                    $timestrings = array();
                                    $sort = local_encourage_sortby_ref_times().', day, hour, minute';
                                    $times = $DB->get_records('encourage_template_time', array('templateid' => $template->id), $sort);
                                    foreach($times as $time) {
                                        $timestring = '';
                                        switch($time->reftime) {
                                            case 1:
                                                $timestring .= '과정 시작일로부터 ';
                                                break;
                                            case 2:
                                                $timestring .= '과정 종료일로부터 ';
                                                break;
                                            case 3:
                                                $timestring .= '사전과정 시작일로부터 ';
                                                break;
                                            case 4:
                                                $timestring .= '사후과정 시작일로부터 ';
                                                break;
                                            case 5:
                                                $timestring .= '사전과정 종료일로부터 ';
                                                break;
                                            case 6:
                                                $timestring .= '사후과정 종료일로부터 ';
                                                break;
                                            default:
                                                break;
                                        }
                                        $timestring .= $time->day.'일 '.$time->hour.'시 '.$time->minute.'분';
                                        if($time->isrepeat) {
                                            $timestring .= '(반복)';
                                        }
                                        $timestrings[] = $timestring;
                                    }

                                    echo '<tr>';
                                    echo '<td><input type="checkbox" name="id[]" value="'.$template->id.'"></td>';
                                    echo '<td>'.$template->categoryname.'</td>';
                                    echo '<td>'.$method.'</td>';
                                    echo '<td>'.$target.'</td>';
                                    echo '<td>'.$type.'</td>';
                                    echo '<td class="text-left"><a href="'.$CFG->wwwroot.'/local/management/encourage/write.php?id='.$template->id.'">'.$template->name.'<br/>';
                                    echo '('.$template->title.')</a></td>';
                                    echo '<td>'.implode('<br/>', $timestrings).'</td>';
                                    echo '<td>'.userdate($template->timecreated, '%Y-%m-%0d').'</td>';
                                    echo '<td>'.$isused.'</td>';
                                    echo '</tr>';

                                    //$num += 1;
                                }
                            } else {
                                echo '<tr>';
                                echo '<td colspan="8'
                                . ''
                                        . ''
                                        . '">데이터가 없습니다.</td>';
                                echo '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="pull-left">
                            <?php
                            if($used) {
                                echo '<input id="change-used" type="button" class="btn btn-primary" value="미사용" data-used="0" />';
                            } else {
                                echo '<input id="change-used" type="button" class="btn btn-primary" value="사용" data-used="1" />';
                            }
                            ?>
                            <input id="add-template" type="button" class="btn btn-danger" value="템플릿 등록" />
                        </div>
                        <ul class="pagination pagination-sm no-margin pull-right">
            <?php
                        $lastpage = ceil($counttemplates / $perpage);
                        $pageprev = ($page > 1) ? $page - 1 : 1;
                        $pagenext = ($page < $lastpage) ? $page + 1 : $lastpage;

                        echo "<li class='paginate_button'><a href='$CFG->wwwroot/local/management/encourage/index.php?page=$pageprev&category=$categoryid' class='next' data-page='$pageprev'>";
                        echo "«";
                        echo "</a></li>";

                        for($i = 1; $i <= $lastpage; ++$i) {
                            if($i == $page) {
                                echo "<li class='paginate_button active'><a href='$CFG->wwwroot/local/management/encourage/index.php?page=$i&category=$categoryid' data-page='$i'>$i</a></li>";
                            } else {
                                echo "<li class='paginate_button'><a href='$CFG->wwwroot/local/management/encourage/index.php?page=$i&category=$categoryid' data-page='$i'>$i</a></li>";
                            }
                        }

                        echo "<li class='paginate_button'><a href='$CFG->wwwroot/local/management/encourage/index.php?page=$pagenext&category=$categoryid' class='prev' data-page='$pagenext'>";
                        echo "»";
                        echo "</a></li>";
            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!---------- content end ---------->
<?php include_once '../footer.php'; ?> 

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type=button]#change-used').click(function(e) {
            change_used($(e.target).attr('data-used'));
        });
        
        $('input[type=button]#add-template').click(function() {
            document.location.href = 'write.php?category=<?php echo $categoryid; ?>';
        });
        
        $('input[type=button]#goto-schedule').click(function() {
            document.location.href = 'schedule.php';
        });
        
        $('select[name=category]').change(function() {
            go_to_page(1);
        });
        
        $('select[name=used]').change(function() {
            go_to_page(1);
        });
        
        $('input[name=id_all]').change(function(e) {
            if($(e.target).is(":checked")) {
                $('input[name="id[]"]').prop('checked', true);
            } else {
                $('input[name="id[]"]').prop('checked', false);
            }
        });
        
        $('.pagination a').click(function(e) {
            e.preventDefault();
            
            var page = $(this).attr('data-page');
            go_to_page(page);
        });
        
        $('th.sorting,th.sorting_asc,th.sorting_desc').click(function(e) {
            e.preventDefault();
            
            var sort = $(e.target).attr('data-sort');
            var asc = 1;
            
            var classes = $(e.target).attr('class').split(' ');
            for(var i = 0; i < classes.length; i++) {
                if(classes[i] == 'sorting_asc') {
                    asc = 0;
                    break;
                }
            }
            
            $("#form-search input[name=sort]").val(sort);
            $("#form-search input[name=asc]").val(asc);
            
            go_to_page(1);
        });
    });
    
    function change_used(used) {
        var ids = [];
        $('input[name="id[]"]:checked').each(function() {
            ids.push($(this).val());
        });
        
        if(ids.length == 0) {
            alert("템플릿을 선택하세요.");
            return false;
        }
        
        var confirmMsg = "선택된 템플릿을 " + $('input[type=button]#change-used').val() + "으로 변경하시겠습니까?";
        if(confirm(confirmMsg)) {
            $.ajax({
                url: 'change_used.ajax.php',
                async: false,
                data: {used: used, ids: ids},
                dataType: 'json',
                success: function(data, textStatus, jqXHR) {
                    location.replace('index.php?category=<?php echo $categoryid; ?>&used=<?php echo $used; ?>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        }
    }
    
    function go_to_page(page) {
        $("#form-search input[name=page]").val(page);
        $("#form-search").submit();
    }
</script>
