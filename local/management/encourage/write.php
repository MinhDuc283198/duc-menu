<?php 
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot.'/local/encourage/lib.php';

$id = optional_param('id', 0, PARAM_INT);
$categoryid = optional_param('category', 0, PARAM_INT);

$pagetitle = '템블릿 등록';
if($id) {
    $pagetitle = '템블릿 수정';
}

$pagesettings = array(
    'title' => '학습 독려 발송 템블릿',
    'heading' => '학습 독려 발송 템블릿',
    'subheading' => $pagetitle,
    'menu' => 'encourage_template',
    'js' => array('/local/management/dext5editor/js/dext5editor.js'),
    'css' => array(),
    'nav_bar'=> array(array('url' => '#', 'text' => $pagetitle, 'active' => true))
);


$template = false;
if($id) {
    $template = $DB->get_record('encourage_template', array('id'=>$id));
    $sort = local_encourage_sortby_ref_times().', day, hour, minute';
    $times = $DB->get_records('encourage_template_time', array('templateid' => $template->id), $sort);
    $template->times = $times;
} else {
    $template = new stdClass();
    $template->name = '';
    $template->description = '';
    $template->categoryid = $categoryid;
    $template->title = '';
    $template->message = '';
    $template->method = 1;
    $template->target = 1;
    $template->type = 1;
    $template->isused = 1;
    $template->times = null;
}

$methods = local_encourage_get_methods();
$targets = local_encourage_get_targets();
$types = local_encourage_get_types();

include_once('../header.php');
?> 
<!---------- content start ---------->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4><?php echo $pagetitle; ?></h4>
                </div>
                <div class="box-body">
                    <form id="form_encourage" class="form-horizontal" action="write_submit.php" method="post" enctype="multipart/form-data" onsubmit="return encourage_check_form();">
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">이름</label>
                            <div class="col-sm-10">
                                <input name="name" type="text" title="이름" class="form-control" value="<?php echo $template->name; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">설명</label>
                            <div class="col-sm-10">
                                <textarea name="description" title="설명" class="form-control" rows="5"><?php echo $template->description;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">분류</label>
                            <div class="col-sm-10">
                                <select name="categoryid" class="form-control">
                                    <option value="0">선택</option>
                                    <?php
                                    $categories = $DB->get_records('encourage_category', array('isdeleted' => 0), 'sortorder ASC');
                                    foreach ($categories as $category) {
                                        $selected = '';
                                        if($category->id == $template->categoryid) {
                                            $selected = ' selected';
                                        }
                                        echo "<option value='$category->id'$selected>$category->name</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">제목</label>
                            <div class="col-sm-10">
                                <input name="title" type="text" title="제목" class="form-control" value="<?php echo $template->title; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">방법</label>
                            <div class="col-sm-10">
                            <?php
                            foreach($methods as $value=>$name) {
                                $checked = '';
                                if($template->method == $value) {
                                    $checked = ' checked';
                                }
                                echo '<div class="radio">';
                                echo '<label for="method_'.$value.'">';
                                echo '<input type="radio" name="method" id="method_'.$value.'" value="'.$value.'"'.$checked.'/>';
                                echo $name;
                                echo '</label>';
                                echo '</div>';
                            }
                            ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">대상</label>
                            <div class="col-sm-10">
                            <?php
                            foreach($targets as $value=>$name) {
                                $checked = '';
                                if($template->target == $value) {
                                    $checked = ' checked';
                                }
                                echo '<div class="radio">';
                                echo '<label for="target_'.$value.'">';
                                echo '<input type="radio" name="target" id="target_'.$value.'" value="'.$value.'"'.$checked.'/>';
                                echo $name;
                                echo '</label>';
                                echo '</div>';
                            }
                            ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">과정구분</label>
                            <div class="col-sm-10">
                            <?php
                            foreach($types as $value=>$name) {
                                $checked = '';
                                if($template->type == $value) {
                                    $checked = ' checked';
                                }
                                echo '<div class="radio">';
                                echo '<label for="target_'.$value.'">';
                                echo '<input type="radio" name="type" id="type_'.$value.'" value="'.$value.'"'.$checked.'/>';
                                echo $name;
                                echo '</label>';
                                echo '</div>';
                            }
                            ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">발송 시점</label>
                            <div class="col-sm-10">
                                <div>&#8251; 발송 시점을 기준일 이전으로 설정하려면 일에 음수 값을 입력하세요(-30을 입력할 경우 기준일 30일 전에 발송함)</div>
                                <ul id="list-times">
                                    <?php
                                    $options = local_encourage_get_ref_times();

                                    foreach($template->times as $time) {
                                        $day = floor($time->repeatinterval / 86400);
                                        $day_remain = $time->repeatinterval % 86400;
                                        $hour = floor($time->repeatinterval / 3600);
                                        $hour_remain = $time->repeatinterval % 3600;
                                        $min = floor($time->repeatinterval / 60);

                                        if($day > 0 && $day_remain == 0) {
                                            $time->repeatintervaltype = 'd';
                                            $time->repeatinterval = $day;
                                        } else if($hour > 0 && $hour_remain == 0) {
                                            $time->repeatintervaltype = 'h';
                                            $time->repeatinterval = $hour;
                                        } else {
                                            $time->repeatintervaltype = 'm';
                                            $time->repeatinterval = $min;
                                        }

                                        $li = '<li>'
                                                . '<div class="sending-time" style="display: inline-block; vertical-align: middle;">'
                                                . '<select name="times['.$time->id.'][reftime]" id="reftime">';
                                        foreach($options as $key => $value) {
                                            $selected = '';
                                            if($key == $time->reftime) {
                                                $selected = ' selected';
                                            }
                                           $li .= '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
                                        }
                                        $li .= '</select> '
                                                . '<input type="text" name="times['.$time->id.'][day]" class="w_50" value="'.$time->day.'"/> 일'
                                                . ' <input type="text" name="times['.$time->id.'][hour]" class="w_50" value="'.$time->hour.'"/> 시'
                                                . ' <input type="text" name="times['.$time->id.'][minute]" class="w_50" value="'.$time->minute.'"/> 분'
                                                . ' <input id="change-repeat" type="checkbox" name="times['.$time->id.'][isrepeat]" value="1"';
                                        if($time->isrepeat) {
                                            $li .= ' checked';
                                        }
                                        $li .= '/> 반복'
                                                . '<div class="sending-time-repeat" id="sending-time-repeat"';
                                        if(!$time->isrepeat) {
                                            $li .= ' style="display:none;"';
                                        }
                                        $li .= '>'
                                                . '반복 주기 <input type="text" name="times['.$time->id.'][repeatinterval]" class="w_50" value="'.$time->repeatinterval.'"/>'
                                                . '<select id="repeatinterval" name="times['.$time->id.'][repeatintervaltype]">'
                                                . '<option value="d"';
                                        if($time->repeatintervaltype == 'h') {
                                            $li .= ' selected';
                                        }
                                        $li .= '>일</option>'
                                                . '<option value="h"';
                                        if($time->repeatintervaltype == 'h') {
                                            $li .= ' selected';
                                        }
                                        $li .= '>시간</option>'
                                                . '<option value="m"';
                                        if($time->repeatintervaltype == 'm') {
                                            $li .= ' selected';
                                        }
                                        $li .= '>분</option>'
                                                . '</select>'
                                                . ' 반복 종료 '
                                                . '<select id="repeatend" name="times['.$time->id.'][repeatend]">'
                                                . '<option value="-1"';
                                        if($time->repeatend == -1) {
                                            $li .= ' selected';
                                        }
                                        $li .= '>과정시작일</option>'
                                                . '<option value="-2"';
                                        if($time->repeatend == -2) {
                                            $li .= ' selected';
                                        }
                                        $li .= '>과정종료일</option>'
                                                . '<option value="-3"';
                                        if($time->repeatend == -3) {
                                            $li .= ' selected';
                                        }
                                        $li .= '>사전과정종료일</option>'
                                                . '<option value="-4"';
                                        if($time->repeatend == -4) {
                                            $li .= ' selected';
                                        }
                                        $li .= '>사후과정종료일</option>'
                                                . '</select>';
                                        $li .= ' <input type="checkbox" name="times['.$time->id.'][repeatonce]" value="1"';
                                        if($time->repeatonce == 1) {
                                            $li .= ' checked';
                                        }
                                        $li .= '/> 하루에 한번만 발송'
                                                . '</div>'
                                                . '</div>'
                                                . ' <input type="button" value="삭제" id="delete-time" class="btn btn-danger"/>'
                                                . '</li>';

                                        echo $li;
                                    }
                                    ?>
                                </ul>
                                <div>
                                <input type="button" value="추가" id="add-time" class="btn btn-primary"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">사용여부</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label for="isused01">
                                        <input type="radio" name="isused" id="isused01" value="1"<?php if($template->isused == 1) { ?> checked<?php } ?>/>
                                        사용
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="isused02">
                                        <input type="radio" name="isused" id="isused02" value="0"<?php if($template->isused == 0) { ?> checked<?php } ?>/>
                                        사용안함
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">내용</label>
                            <div class="col-sm-10">
                                <div id="message_lms">
                                    <textarea id="message_lms_textarea" name="message" title="내용" class="form-control" rows="5"></textarea>
                                    <div id="message_limit"><?php echo get_string('messagelimit', 'local_encourage'); ?></div>
                                </div>
                                <div id="message_email"></div>
                            </div>
                        </div>
                        <div class="form-group" id="attachments">
                            <label for="" class="col-sm-2 control-label">첨부 파일</label>
                            <div class="col-sm-10">
                                <?php
                                $attachs = $DB->get_records('encourage_attachment', array('tid' => $id, 'ecid' => 0));
                                foreach($attachs as $attach) {
                                    echo '<div id="encourage-attach" data-id="'.$attach->id.'">';
                                    echo '<input type="hidden" name="attachids[]" value="'.$attach->id.'"/>';
                                    echo '<span id="filename">'.$attach->filename.'</span> <input type="button" value="삭제" id="delete-attach" class="btn btn-danger">';
                                    echo '</div>';
                                }
                                ?>
                                <div class="encourage-template-attach-input">
                                    <input type="text" id="fileName" class="form-control" readonly="readonly" >
                                    <label for="file_input_hidden" class="select-file">파일 선택</label>
                                    <input type="file" name="attach" id="file_input_hidden" style="display:none;" onchange="encourage_change_attach(this);"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <input id="submit_encourage" type="button" class="btn btn-primary w_80" value="저장" />
                    <input id="goto-list" type="button" class="btn btn-default w_80" value="취소" />
                    <?php if($id) { ?>
                    <input id="delete_encourage" type="button" class="btn btn-default w_80 pull-right" value="삭제" />
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h4>변수</h4>
                </div>
                <div class="box-body">
                    <ul class="list-item">
                        <li>$$crs_nm$$ : 과정 이름</li>
                        <li>$$crs_nmt$$ : 과정 차수</li>
                        <li>$$crs_st_d$$ : 과정 시작일</li>
                        <li>$$crs_end_d$$ : 과정 종료일</li>
                        <li>$$pre_crs_st_d$$ : 사전 과정 시작일</li>
                        <li>$$pre_crs_end_d$$ : 사전 과정 종료일</li>
                        <li>$$post_crs_st_d$$ : 사후 과정 시작일</li>
                        <li>$$post_crs_end_d$$ : 사후 과정 종료일</li>
                        <li>$$crs_lrn_rslt$$ : 학습 내용 및 결과</li>
                        <li>$$user_nm$$ : 학습자명</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="encourage_message_value" style="display: none;"><?php echo $template->message;?></div>
<!---------- content end ---------->
<?php include_once '../footer.php'; ?> 

<script type="text/javascript">
    var editorName = 'editor_message';
    
    $(document).ready(function() {
        $('input[type=button]#goto-list').click(function() {
            document.location.href = 'index.php';
        });
        
        $('input[type=button]#add-time').click(function() {
            encourage_add_time();
        });
        
        $('#list-times li').find('input[type=checkbox]#change-repeat').change(function() {
            var index = encourage_extract_index($(this).attr('name'));
            encourage_changed_repeat(index);
        });
        
        $('input[type=button]#delete-time').click(function() {
            var index = $(this).closest("li").index();
            encourage_delete_time(index);
        });
        
        $('input[type=button]#submit_encourage').click(function() {
            $('#form_encourage').submit();
        });
        
        $('input[type=button]#delete_encourage').click(function() {
            if(confirm('탬블릿을 삭제하시겠습니까?')) {
                document.location.href = 'delete.php?id=<?php echo $id; ?>&sesskey=<?php echo sesskey();?>';
            }
        });
        
        $('input[type=button]#delete-attach').click(function() {
            if(confirm('첨부 파일을 삭제하시겠습니까?')) {
                var attachid = $(this).closest("div#encourage-attach").attr('data-id');
                encourage_delete_attach(attachid);
            }
        });
        
        $('#list-times li').css('display', 'block');
        
        if($("#list-times li").length == 0) {
            encourage_add_time();
        }
        
        $("input:radio[name=method]").change(function() {
            encourage_changed_method($(this).val());
        });
        
        $("input:radio[name=target]").change(function() {
            encourage_changed_target($(this).val());
        });
        
        if($("div#encourage-attach").length > 0) {
            $("div.encourage-template-attach-input").css('display', 'none');
        }
        
        DEXT5.config.EditorHolder = "message_email";
        DEXT5.config.Width = "100%";
        new Dext5editor(editorName);
    });
    
    <?php
    // LMS 템블릿을 저장했다가 편집할 때
    // 이메일로 변경하면 editor가 안보이는 문제 때문에
    // edotor가 로드된 후에 editor를 숨기도록 이벤트 처리.
    // textarea와 editor가 잠깐동안 동시에 보이는 문제가 있음.
    ?>
    function dext_editor_loaded_event(editor) {
        var method = $("input:radio[name=method]:checked").val();
        encourage_changed_method(method);
        
        var target = $("input:radio[name=target]:checked").val();
        encourage_changed_target(target);
        
        if(method == 1) {// Email
            DEXT5.setHtmlValue($("#encourage_message_value").html(), editorName);
        } else {// LMS
            $("textarea#message_lms_textarea").val($("#encourage_message_value").html());
        }
    }
    
    function encourage_check_form() {
        // 이름
        var name = $('input[name=name]').val();
        if(name.trim().length == 0) {
            alert("이름을 입력하세요.");
            return false;
        }
        
        // 분류
        var categoryid = $("select[name=categoryid]").val();
        if(categoryid == 0) {
            alert("분류를 선택하세요.");
            return false;
        }
        
        // 제목
        var title = $('input[name=title]').val();
        if(title.trim().length == 0) {
            alert("제목을 입력하세요.");
            return false;
        }
        
        // 내용
        var isEmptyMessage = false;
        var method = $("input:radio[name=method]:checked").val();
        if(method == 1) {// Email
            if(DEXT5.isEmpty(editorName)) {
                isEmptyMessage = true;
            }
        } else {
            var message = $('textarea[name=message]').val();
            if(message.trim().length == 0) {
                isEmptyMessage = true;
            }
        }
        if(isEmptyMessage) {
            alert("내용을 입력하세요.");
            return false;
        }
        
        if(method == 1) { // Email일 경우
            $('textarea[name=message]').val(DEXT5.getBodyValue(editorName));
        }
        
        // 발송시점
        var isCompleted = true;
        $('input[name^=times]').each(function() {
            var name = encourage_extract_name($(this).attr('name'));
            var value = $(this).val().trim();
            switch(name) {
                case 'day':
                    if(value.length == 0) {
                        alert("발송 시점의 일을 입력하세요.");
                        isCompleted = false;
                        break;
                    }
                    if(!encourage_check_integer(value)) {
                        alert("발송 시점의 일은 정수를 입력하세요.");
                        isCompleted = false;
                    }
                    break;
                case 'hour':
                    if(value.length == 0) {
                        alert("발송 시점의 시를 입력하세요.");
                        isCompleted = false;
                        break;
                    }
                    if(!encourage_check_integer(value) || value < 0 || value > 23) {
                        alert("발송 시점의 시는 0 ~ 23 사이의 정수를 입력하세요.");
                        isCompleted = false;
                    }
                    break;
                case 'minute':
                    if(value.length == 0) {
                        alert("발송 시점의 분을 입력하세요.");
                        isCompleted = false;
                        break;
                    }
                    if(!encourage_check_integer(value) || value < 0 || value > 59) {
                        alert("발송 시점의 분은 0 ~ 59 사이의 정수를 입력하세요.");
                        isCompleted = false;
                        break;
                    }
                    if((value % 10) > 0) {
                        alert("발송 시점의 분은 10 분 단위로 입력하세요.");
                        isCompleted = false;
                    }
                    break;
                case 'isrepeat':
                    if($(this).is(':checked')) {
                        var index = encourage_extract_index($(this).attr('name'));
                        var intervaltype = $('select[name="times[' + index + '][repeatintervaltype]"]').val();
                        var interval = $('input[name="times[' + index + '][repeatinterval]"]').val().trim();
                        if(interval.length == 0) {
                            alert("발송 시점의 반복 주기를 입력하세요.");
                            isCompleted = false;
                            break;
                        }
                        if(!encourage_check_integer(interval) || interval <= 0) {
                            alert("발송 시점의 반복 주기는 0 보다 큰 정수를 입력하세요.");
                            isCompleted = false;
                            break;
                        }
                        if(intervaltype == 'm' && (interval % 10) > 0) {
                            alert("발송 시점의 반복 주기는 10 분 단위로 입력하세요.");
                            isCompleted = false;
                        }
                    }
                    break;
                default:
                    break;
            }
            
            if(!isCompleted) {
                return false;
            }
        });
        
        return isCompleted;
    }
    
    function encourage_check_integer(num) {
        return /^-{0,1}\d+$/.test(num);
    }
    
    function encourage_extract_index(str) {
        var match =  str.match(/(\w+)/ig);
        if(match.length == 3) {
            return match[1];
        }
        
        return null;
    }
    
    function encourage_extract_name(str) {
        var match =  str.match(/(\w+)/ig);
        if(match.length == 3) {
            return match[2];
        }
        
        return null;
    }
    
    function encourage_get_times_index() {
        var prefix = 'idx_';
        var maxIndex = 0;
        $('input[name^=times]').each(function() {
            var name = encourage_extract_name($(this).attr('name'));
            if(name == 'day') {
                var match =  $(this).attr('name').match(/(\w+)/ig);
                if(match.length == 3) {
                    var index = match[1];
                    if(index.startsWith(prefix)) {
                        index = index.substring(prefix.length, index.length);
                        maxIndex = Math.max(maxIndex, parseInt(index)) + 1;
                    }
                }
            }
        });
        return prefix + maxIndex;
    }
    
    function encourage_add_time() {
        var index = encourage_get_times_index();
        
        var liElement = '<li>'
                + '<div class="sending-time" style="display: inline-block; vertical-align: middle;">'
                + '<select name="times[' + index + '][reftime]">'
                + '<option value="3" selected>사전과정 시작일로부터</option>'
                + '<option value="5">사전과정 종료일로부터</option>'
                + '<option value="1">과정 시작일로부터</option>'
                + '<option value="2">과정 종료일로부터</option>'
                + '<option value="4">사후과정 시작일로부터</option>'
                + '<option value="6">사후과정 종료일로부터</option>'
                + '</select> '
                + '<input type="text" name="times[' + index + '][day]" class="w_50" value=""/> 일'
                + ' <input type="text" name="times[' + index + '][hour]" class="w_50" value=""/> 시'
                + ' <input type="text" name="times[' + index + '][minute]" class="w_50" value=""/> 분'
                + ' <input id="change-repeat" type="checkbox" name="times[' + index + '][isrepeat]" value="1"/> 반복'
                + '<div class="sending-time-repeat" id="sending-time-repeat" style="display:none;">'
                + '반복 주기 <input type="text" name="times[' + index + '][repeatinterval]" class="w_50" value="30"/>'
                + '<select id="repeatinterval" name="times[' + index + '][repeatintervaltype]">'
                + '<option value="d">일</option>'
                + '<option value="h">시간</option>'
                + '<option value="m" selected>분</option>'
                + '</select>'
                + ' 반복 종료 '
                + '<select id="repeatend" name="times[' + index + '][repeatend]">'
                + '<option value="-1">과정시작일</option>'
                + '<option value="-2">과정종료일</option>'
                + '<option value="-3" selected>사전과정종료일</option>'
                + '<option value="-4">사후과정종료일</option>'
                + '</select>'
                //+ '<input type="text" name="times[' + index + '][repeatenddays]" class="w_50" value=""/>'
                + ' <input type="checkbox" name="times[' + index + '][repeatonce]" value="1" checked /> 하루에 한번만 발송'
                + '</div>'
                + '</div>'
                + ' <input type="button" value="삭제" id="delete-time" class="btn btn-danger"/>'
                + '</li>';
        
        $("#list-times").append(liElement);
        
        $('#list-times li:last-child').css('display', 'block');
        
        var btnDelete = $('#list-times li:last-child').find("input[type=button]#delete-time");
        btnDelete.click(function() {
            var index = $(this).closest("li").index();
            encourage_delete_time(index);
        });
        
        var chkRepeat = $('#list-times li:last-child').find("input:checkbox[name='times[" + index + "][isrepeat]']");
        chkRepeat.change(function() {
            var index = encourage_extract_index($(this).attr('name'));
            encourage_changed_repeat(index);
        });
    }
    
    function encourage_delete_time(index) {
        if(confirm("삭제하시겠습니까?")) {
            $("#list-times").find("li:eq(" + index + ")").remove();
        }
    }
    
    function encourage_delete_attach(attachid) {
        $("div[data-id=" + attachid + "]#encourage-attach").remove();
        
        if($("div#encourage-attach").length == 0) {
            $("div.encourage-template-attach-input").css('display', 'block');
        }
    }
    
    function encourage_changed_method(method) {
        if(method == 1) {// Email
            $('div#message_lms').hide();
            $('div#message_email').show();
            $('div#attachments').show();
            $('input:radio[name=target][value=4]').attr('disabled', false);
        } else {// LMS
            $('div#message_lms').show();
            $('div#message_email').hide();
            $('div#attachments').hide();
            $('input[type=text]#fileName').val('');
            $('input[type=file]#file_input_hidden').val('');
            $('input:radio[name=target][value=4]').attr('disabled', true);
        }
    }
    
    function encourage_changed_target(target) {
        if(target == 4) {
            $('input:radio[name=method][value=2]').attr('disabled', true);
        } else {
            $('input:radio[name=method][value=2]').attr('disabled', false);
        }
    }
    
    function encourage_changed_repeat(index) {
        var checkbox = $("#list-times li").find('input:checkbox[name="times[' + index + '][isrepeat]"]')[0];
        var divRepead = $(checkbox).closest('li').find('div#sending-time-repeat');
        if($(checkbox).is(":checked")) {
            $(divRepead[0]).show();
        } else {
            $(divRepead[0]).hide();
        }
    }
    
    function encourage_change_attach(obj) {
        if(0 == obj.files[0].size){
            alert("파일 크기가 0 byte인 파일은 추가할 수 없습니다.");
            return false;
        }

        if(obj.files[0].name != undefined){
            $("#fileName").val(obj.files[0].name);
        }
    }
    
    function encourage_convert_readable_filesize(bytes) {
        var thresh = 1024;
        
        if(Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        
        var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
        var u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while(Math.abs(bytes) >= thresh && u < units.length - 1);
        
        return bytes.toFixed(1)+' '+units[u];
    }
</script>
