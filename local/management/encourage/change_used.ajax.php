<?php

require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot.'/local/encourage/lib.php';

$context = context_system::instance();
require_login($context);

$used = required_param('used', PARAM_INT);
$ids = required_param_array('ids', PARAM_INT);

foreach($ids as $id) {
    $DB->set_field('encourage_template', 'isused', $used, array('id' => $id));
}

echo json_encode($ids);