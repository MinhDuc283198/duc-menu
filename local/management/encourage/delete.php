<?php 

require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot.'/local/encourage/lib.php';

$context = context_system::instance();
require_login($context);

$id = required_param('id', PARAM_INT);

if (!confirm_sesskey()) {
    echo '<script type="text/javascript">'
        . 'alert("잘못된 접근입니다.");'
        . 'document.location.replace("'.$CFG->wwwroot.'");'
        . '</script>';
    die();
}

$template = $DB->get_record('encourage_template', array('id'=>$id));
if($template) {
    $template->isdeleted = 1;
    $DB->update_record('encourage_template', $template);
}

header("Location: $CFG->wwwroot/local/management/encourage/index.php");
die();