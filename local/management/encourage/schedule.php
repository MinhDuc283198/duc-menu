<?php 
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/encourage/lib.php';

$pagesettings = array(
    'title' => '학습 독려 발송 스케쥴',
    'heading' => '학습 독려 발송 스케쥴',
    'subheading' => '',
    'menu' => 'encourage_schedule',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);


$categoryid = optional_param('category', 0, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$timeschedule = optional_param('ts', '', PARAM_RAW);
$timeschedule2 = optional_param('ts2', '', PARAM_RAW);

if(empty($timeschedule)) {
    $monday = strtotime('monday this week');
    $timeschedule = strftime('%Y-%m-%d', $monday);
}
if(empty($timeschedule2)) {
    $friday = strtotime('friday this week');
    $timeschedule2 = strftime('%Y-%m-%d', $friday);
}
$time = strtotime($timeschedule);
$time2 = strtotime($timeschedule2);



$sql = "SELECT * FROM (
SELECT ect.id, ect.isrepeat, ect.timeschedule, ect.hour, ect.minute, ect.repeatend, ect.repeatinterval, ect.repeatonce, ect.reftime
     , ec.id AS ecid, ec.title, ec.message, ec.name, ec.method, ec.target, ec.type, ec.categoryid, ec.courseid
     , ca.name AS categoryname
     , dc.crs_nm, dc.crs_nmt
     , CASE WHEN ect.isrepeat = 1 THEN CASE WHEN ect.repeatend = -1 THEN dc.crs_st_d
                                            WHEN ect.repeatend = -2 THEN dc.crs_end_d
                                            WHEN ect.repeatend = -3 THEN etir.pre_end
                                            WHEN ect.repeatend = -4 THEN etir.post_end
                                            ELSE 0 END
     ELSE 0 END AS timeend
FROM {encourage_course} ec 
JOIN {encourage_course_time} ect ON ect.ecid = ec.id
JOIN {encourage_category} ca ON ca.id = ec.categoryid
JOIN {course} co ON co.id = ec.courseid
JOIN {dlp_class} dc ON dc.courseid = co.id
JOIN {encourage_course_time_ref} etir ON etir.courseid = co.id
WHERE ec.active = 1
) a
WHERE (   (a.isrepeat = 0 AND ($time <= a.timeschedule AND a.timeschedule <= $time2))
       OR (a.isrepeat = 1 AND (   ($time <= a.timeschedule AND a.timeschedule <= $time2 )
                               OR ($time <= a.timeend AND a.timeend <= $time2)
                               OR (a.timeschedule <= $time AND $time <= a.timeend)
                               OR (a.timeschedule <= $time2 AND $time2 <= a.timeend) )))
";

$sql_orderby = " ORDER BY a.timeschedule ASC, a.hour ASC, a.minute ASC";

$conditions = array();
$params = array();
if($categoryid) {
    $conditions[] = 'a.categoryid = :categoryid';
    $params['categoryid'] = $categoryid;
}

if(!empty($search)) {
    $conditionsearch = array();

    // 이름
    $conditionsearch[] = $DB->sql_like("a.name", ":name", false);
    $params['name'] = "%$search%";

    // 설명
    //$conditionsearch[] = $DB->sql_like("a.description", ":description", false);
    //$params['description'] = "%$search%";

    // 제목
    $conditionsearch[] = $DB->sql_like("a.title", ":title", false);
    $params['title'] = "%$search%";

    // 내용
    $conditionsearch[] = $DB->sql_like("a.message", ":message", false);
    $params['message'] = "%$search%";
    
    // 강의 이름
    $conditionsearch[] = $DB->sql_like("a.crs_nm", ":crs_nm", false);
    $params['crs_nm'] = "%$search%";

    $conditions[] = '('.implode(' OR ', $conditionsearch).')';
}

if(count($conditions) > 0) {
    $sql .= ' AND '. implode(' AND ', $conditions);
}
$schedules = $DB->get_records_sql($sql . $sql_orderby, $params);

$methods = local_encourage_get_methods();
$targets = local_encourage_get_targets();
$types = local_encourage_get_types();

include_once('../header.php');
?>

<section class="content">
    <div class="contents_box encourage-schedules">
        <div class="search-area box-header">
            <form id="form-search-schedules" action="schedule.php" class="form-horizontal">
                <div class="row">
                    <div class="col-md-6">
                        <label for="categoryid" class="col-sm-1 control-label">분류</label>
                        <div class="col-sm-5">
                        <select id="categoryid" name="category" class="form-control">
                        <option value="0">전체</option>
                        <?php
                        $categories = $DB->get_records('encourage_category', array('isdeleted' => 0), 'sortorder ASC');
                        foreach($categories as $category) {
                            $selected = '';
                            if($categoryid == $category->id) {
                                $selected = ' selected';
                            }
                            echo '<option value="'.$category->id.'"'.$selected.'>'.$category->name.'</option>';
                        }
                        ?>
                        </select>
                        </div>
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" value="<?php echo $search; ?>"/>
                            <div class="input-group-btn">
                                <input type="submit" value="검색" class="btn btn-primary"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <label for="timeschedule" class="col-sm-2 control-label">발송일</label>
                            <div class="col-sm-4">
                                <input type="text" name="ts" class="form-control time-schedule" value=""/>
                            </div>
                            <div class="col-sm-1">~</div>
                            <div class="col-sm-4">
                                <input type="text" name="ts2" class="form-control time-schedule" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <div class="row">
        <div class="col-xs-12">
        <div class="box">
        <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>분류</th>
                    <th>방법</th>
                    <th>대상</th>
                    <th>과정구분</th>
                    <th>과정</th>
                    <th>이름<br/>(제목)</th>
                    <th>발송시간</th>
                    <th>발송건수</th>
                </tr>
            </thead>
            <?php
            if(count($schedules) > 0) {
                //$num = ($page - 1) * $perpage + 1;
                foreach ($schedules as $schedule) {
                    $method = $methods[$schedule->method];
                    $target = $targets[$schedule->target];
                    $type = $types[$schedule->type];
                    
                    $timescheduled = $schedule->timeschedule + $schedule->hour * 3600 + $schedule->minute * 60;
                    $conditions = array('el.ecid = :ecid', 'el.ectid = :ectid');
                    $params = array('ecid' => $schedule->ecid, 'ectid' => $schedule->id);
                    if($schedule->isrepeat) {
                        $conditions[] = '(el.timescheduled >= :timefrom AND el.timescheduled <= :timeto)';
                        $params['timefrom'] = $timescheduled;
                        $params['timeto'] = time();
                    } else {
                        $conditions[] = 'timescheduled = :timescheduled';
                        $params['timescheduled'] = $timescheduled;
                    }
                    $countsend = $DB->count_records_sql("SELECT COUNT(u.username) 
                        FROM {encourage_log} el
                        JOIN {encourage_course} ec ON ec.id = el.ecid
                        JOIN {encourage_category} ca ON ca.id = ec.categoryid
                        JOIN {user} u ON u.id = el.userid
                        JOIN {dlp_user} du ON du.userid = u.id
                        JOIN {dlp_class} dc On dc.courseid = ec.courseid
                        JOIN LGAPORTAL.dbo.T_STDT_STTS_M tm ON tm.STDT_CD = du.mem_cd 
                                                           AND tm.CRS_MGM_SCH_N = dc.crs_mgm_sch_n
                                                           AND tm.at_lec_stts_c_cd IN ('100101', '100103', '100105')
                        LEFT JOIN LGAPORTAL.DBO.T_SLS_CUST_M_damo comp ON comp.cust_cd = du.bl_co_cd
                        WHERE ". implode(" AND ", $conditions), $params);
                    
                    $timestring = strftime('%Y-%m-%d %A', $schedule->timeschedule)
                            . ' '
                            . str_pad($schedule->hour, 2, '0', STR_PAD_LEFT)
                            . ':'
                            . str_pad($schedule->minute, 2, '0', STR_PAD_LEFT);
                    
                    if($schedule->isrepeat) {
                        $timestring .= ' (반복)';
                    }
                    
                    $urlmonitoring = $CFG->wwwroot.'/local/encourage/monitoring.php?id='.$schedule->courseid;
                    $urlreport = $CFG->wwwroot.'/local/encourage/report/index.php?id='.$schedule->courseid.'&ecid='.$schedule->ecid.'&ts='.$timescheduled;
                    echo '<tr>';
                    echo '<td>'.$schedule->categoryname.'</td>';
                    echo '<td>'.$method.'</td>';
                    echo '<td>'.$target.'</td>';
                    echo '<td>'.$type.'</td>';
                    echo '<td><a href="'.$urlmonitoring.'">'.$schedule->crs_nm.' '. $schedule->crs_nmt .'차</a></td>';
                    echo '<td class="text-left"><a href="'.$urlreport.'">'.$schedule->name.'<br/>';
                    echo '('.$schedule->title.')</a></td>';
                    echo '<td><a href="'.$urlreport.'">'.$timestring.'</a></td>';
                    echo '<td>'.$countsend.'</td>';
                    echo '</tr>';
                    
                    //$num += 1;
                }
            } else {
                echo '<tr>';
                echo '<td colspan="8'
                . ''
                        . ''
                        . '">데이터가 없습니다.</td>';
                echo '</tr>';
            }
            ?>
        </table>
        </div>
        </div>
        </div>
        </div>
    </div>
</section>
<?php
include_once '../footer.php';
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[name=ts]').datepicker({
            format: 'yyyy-mm-dd',
            endDate: '<?php echo $timeschedule2; ?>',
            todayHighlight: true,
            language: 'ko',
            todayBtn: true
        }).datepicker('setDate', '<?php echo $timeschedule; ?>')
        .on('changeDate', function () {
                $('#form-search-schedules').submit();
            });
        
        $('input[name=ts2]').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '<?php echo $timeschedule; ?>',
            todayHighlight: true,
            language: 'ko',
            todayBtn: true
        }).datepicker('setDate', '<?php echo $timeschedule2; ?>')
        .on('changeDate', function () {
                $('#form-search-schedules').submit();
            });;
        
        $('input[type=button]#goto-template').click(function() {
            document.location.href = 'index.php';
        });
    });
</script>

