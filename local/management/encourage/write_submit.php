<?php 
require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot.'/local/encourage/lib.php';

$context = context_system::instance();
require_login($context);

$id = required_param('id', PARAM_INT);
$method = required_param('method', PARAM_INT);
$target = required_param('target', PARAM_INT);
$type = required_param('type', PARAM_INT);
$isused = required_param('isused', PARAM_INT);
$times = $_POST['times'];//required_param_array('time', PARAM_INT);
$categoryid = required_param('categoryid', PARAM_INT);
$title = required_param('title', PARAM_RAW);
$message = required_param('message', PARAM_RAW);
$name = required_param('name', PARAM_RAW);
$description = optional_param('description', '', PARAM_RAW);
$attach = $_FILES["attach"]; // 새로 추가된 첨부파일
$attachids = $_POST['attachids']; // 이전 첨부 파일, 여기에 전달되지 않는 것은 삭제한다

$repeatintervaltypes = array(
    'd' => 86400,
    'h' => 3600,
    'm' => 60
);

if($id) { // Update
    $template = $DB->get_record('encourage_template', array('id' => $id));
    $template->name = $name;
    $template->description = $description;
    $template->categoryid = $categoryid;
    $template->title = $title;
    $template->message = $message;
    $template->method = $method;
    $template->target = $target;
    $template->type = $type;
    $template->isused = $isused;
    $template->timemodified = time();
    $DB->update_record('encourage_template', $template);
    
    $timeids = array();
    foreach($times as $key=>$time) {
        $time = (object) $time;
        if(!isset($time->repeatonce)) {
            $time->repeatonce = 0;
        }
        if(isset($time->isrepeat)) {
            if($time->isrepeat == 1) {
                $time->repeatinterval = $time->repeatinterval * $repeatintervaltypes[$time->repeatintervaltype];
            }
        } else {
            $time->isrepeat = 0;
            $time->repeatinterval = 1800; // 0을 넣을 경우 발송 시간을 찾는 쿼리에서 에러가 나서 기본값을 30분으로 넣는다.
            $time->repeatend = 0;
            $time->repeatonce = 0;
        }
        $timeid = false;
        if(is_int($key)) { // Update
            $time->id = $key;
            $DB->update_record('encourage_template_time', $time);
            $timeid = $key;
        } else { // Insert
            $time->templateid = $id;
            $timeid = $DB->insert_record('encourage_template_time', $time);
        }
        if($timeid) {
            $timeids[] = $timeid;
        }
    }
    
    // 지워진 time 삭제..
    $params = array('templateid' => $id);
    $sql = "SELECT * "
        . "FROM {encourage_template_time} "
        . "WHERE templateid = :templateid ";
    if(count($timeids) > 0) {
        $incondition = '';
        list($incondition, $inparams) = $DB->get_in_or_equal($timeids, SQL_PARAMS_NAMED, 'param', false);
        $params = array_merge($params, $inparams);

        if(!empty($incondition)) {
            $sql .= "  AND id ".$incondition;
        }
    }
    $deletedtimes = $DB->get_records_sql($sql, $params);
    foreach($deletedtimes as $deletedtime) {
        $DB->delete_records('encourage_template_time', array('id' => $deletedtime->id));
    }
} else { // Insert
    $template = new stdClass();
    $template->name = $name;
    $template->description = $description;
    $template->categoryid = $categoryid;
    $template->title = $title;
    $template->message = $message;
    $template->method = $method;
    $template->target = $target;
    $template->type = $type;
    $template->isused = $isused;
    $template->timecreated = time();
    $template->timemodified = 0;
    $template->userid = $USER->id;
    $template->isdeleted = 0;
    
    $id = $DB->insert_record('encourage_template', $template);
    if($id) {
        foreach($times as $time) {
            $time = (object) $time;
            $time->templateid = $id;
            if(!isset($time->repeatonce)) {
                $time->repeatonce = 0;
            }
            if(isset($time->isrepeat)) {
                if($time->isrepeat == 1) {
                    $time->repeatinterval = $time->repeatinterval * $repeatintervaltypes[$time->repeatintervaltype];
                }
            } else {
                $time->isrepeat = 0;
                $time->repeatinterval = 1800;
                $time->repeatend = 0;
                $time->repeatonce = 0;
            }
            $DB->insert_record('encourage_template_time', $time);
        }
    }
}


$params = array('tid' => $id, 'ecid' => 0);
$incondition = '';
if(!empty($attachids)) {
    $inparams = array();
    list($incondition, $inparams) = $DB->get_in_or_equal($attachids, SQL_PARAMS_NAMED, 'id_', false);
    $params = array_merge($params, $inparams);
}

$sql = "SELECT * 
FROM {encourage_attachment} 
WHERE tid = :tid AND ecid = :ecid";
if($incondition != '') {
    $sql .= " AND id $incondition";
}
$deletedattachs = $DB->get_records_sql($sql, $params);
foreach($deletedattachs as $deletedattach) {
    // 과정에 추가된 탬플릿의 첨부파일은 삭제하지 않는다
    $added = $DB->record_exists_select('encourage_attachment', 'tid = :tid AND ecid != :ecid AND filename = :filename', 
            array('tid' => $deletedattach->tid, 
                'ecid' => $deletedattach->ecid, 
                'filename' => $deletedattach->filename));
    local_encourage_delete_attachment($deletedattach->id, !$added);
}

// 첨부파일 처리
local_encourage_save_attachment($attach, $id, 0);

header("Location: $CFG->wwwroot/local/management/encourage/index.php");
die();