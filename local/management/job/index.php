<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');
require_once($VISANG->dirroot . '/lib/skills.php');

//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
require_login(0, false);
$current_language = current_language();

$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 0, PARAM_INT);
if ($page == 0) $page = 1;
if ($perpage == 0) $perpage = 10;
$offset = ($page - 1) * $perpage;
// ===================================================================================================
// 
$sql = "SELECT j.*, e.company_name
        FROM {vi_jobs} j
        JOIN {vi_employers} e ON e.id = j.employer_id
        WHERE j.deleted = 0
        ORDER BY j.timecreated DESC
        LIMIT $perpage OFFSET $offset";
$latest_jobs = $DB->get_records_sql($sql, array());

$sql_count = "SELECT COUNT(distinct j.id)
        FROM {vi_jobs} j
        JOIN {vi_employers} e ON e.id = j.employer_id
        WHERE j.deleted = 0";
$totalcount = $DB->count_records_sql($sql_count, array());
// ===================================================================================================
// renders
$title = get_string('visang:dashboard', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->menu = 'dsb';
$VISANG->page->heading = $title;

$VISANG->page->header();

$thistime = time();
?>
    <style>
        .stat-box {
            display: inline-block;
            padding: 15px;
            text-align: center;
            margin: 3px auto;
            min-width: 150px;
        }

        .stat-box h3 {
            font-size: 34px;
            font-weight: bold;
            margin: 0px;
        }

        @media (min-width: 1200px) {
            .col-lg-12-5 {
                width: 20% !important;
            }
        }
    </style>

    <section class="content">
        <div>
            <a href="<?php echo $CFG->wwwroot . '/local/management/job/employers/employers.php'; ?>" class="stat-box bg-aqua">
                <h3><?php echo $DB->count_records('vi_employers'); ?></h3>
                &nbsp;<br />Employers
            </a>
            <div class="stat-box bg-green">
                <h3><?php echo $totalcount; ?></h3>
                &nbsp;<br />Total Jobs
            </div>
            <div class="stat-box bg-green">
                <h3>
                    <?php
                    echo $DB->count_records_sql("SELECT COUNT(DISTINCT j.id) FROM {vi_jobs} j WHERE j.published = 1 AND j.deleted = 0 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))");
                    ?>
                </h3>
                &nbsp;<br />Published Jobs
            </div>
            <div class="stat-box bg-aqua">
                <h3>
                    <?php
                    echo $DB->count_records_sql("SELECT COUNT(DISTINCT e.id) FROM {vi_employers} e LEFT JOIN {vi_jobs} j ON e.id = j.employer_id  WHERE j.korean_level_id > 1");
                    ?>
                </h3>
                Korean-related<br />Employers
            </div>
            <div class="stat-box bg-green">
                <h3>
                    <?php
                    echo $DB->count_records_sql("SELECT COUNT(DISTINCT j.id) FROM {vi_jobs} j WHERE j.korean_level_id > 1");
                    ?>
                </h3>
                Korean-related<br />Total jobs
            </div>
            <div class="stat-box bg-green">
                <h3>
                    <?php
                    echo $DB->count_records_sql("SELECT COUNT(DISTINCT j.id) FROM {vi_jobs} j WHERE j.korean_level_id > 1 AND j.published = 1 AND ((j.startdate=0)OR(j.startdate < $thistime AND $thistime < j.enddate))");
                    ?>
                </h3>
                Korean-related<br />Published Jobs
            </div>
            <div class="stat-box bg-yellow">
                <h3><?php echo $DB->count_records('vi_job_applications'); ?></h3>
                Applications<br />Total Members
            </div>
            <div class="stat-box bg-yellow">
                <h3>
                    <?php
                    $bop_application_sql_count = "SELECT COUNT(distinct j.id)
                    FROM {vi_job_applications} j
                    LEFT JOIN {lmsdata_user} lu ON lu.userid = j.user_id
                    WHERE lu.usergroup = :usergroup";
                    $bop_application_count = $DB->count_records_sql($bop_application_sql_count, array('usergroup' => 'bp'));
                    echo $bop_application_count;
                    ?>
                </h3>
                Applications<br />BOP Members
            </div>
            <div class="stat-box bg-red">
                <h3><?php echo $DB->count_records('vi_reviews'); ?></h3>
                &nbsp;<br />Reviews
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Latest Jobs
                            <form action="latest_jobs_excel_down.php" method="get" style="display: inline-block; margin-left: 15px;">
                                <button type="submit" class="btn btn-primary">Download excel file</button>
                            </form>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Job</th>
                                    <th>Employer</th>
                                    <th style="width: 130px;">한국어 활용 수준</th>
                                    <th>일반회원</th>
                                    <th>BOP</th>
                                    <th>Applications</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($latest_jobs as $job) {
                                    $salary = $DB->get_record('vi_job_salary_categories', array('id' => $job->job_salary_category_id));

                                    $_skills = '';
                                    $skill_list = visang_get_job_skills($job->id);
                                    foreach ($skill_list as $skill) {
                                        $_skills .= html_writer::div($skill->name, 'vi-tag');
                                    }

                                    $korean_level = '-';
                                    if ($job->korean_level_id > 1) {
                                        $_p = $DB->get_record('vi_korean_levels', array('id' => $job->korean_level_id));
                                        if ($_p) {
                                            switch ($current_language) {
                                                case 'ko':
                                                    $korean_level = $_p->name_ko;
                                                    break;
                                                case 'en':
                                                    $korean_level = $_p->name_en;
                                                    break;

                                                default:
                                                    $korean_level = $_p->name_vi;
                                                    break;
                                            }
                                        }
                                    }

                                    // $total_application_count = $DB->count_records('vi_job_applications', array('job_id' => $job->id));
                                    // usergroup = rs
                                    $application_count = $DB->count_records_sql("SELECT COUNT(DISTINCT a.id) 
                                        FROM {vi_job_applications} a
                                        JOIN {lmsdata_user} lu ON lu.userid = a.user_id
                                        WHERE a.job_id = :jobid AND lu.usergroup <> 'bp'", [
                                        'jobid' => $job->id
                                    ]);
                                    $bop_application_count = $DB->count_records_sql("SELECT COUNT(DISTINCT a.id) 
                                        FROM {vi_job_applications} a
                                        JOIN {lmsdata_user} lu ON lu.userid = a.user_id
                                        WHERE a.job_id = :jobid AND lu.usergroup = 'bp'", [
                                        'jobid' => $job->id
                                    ]);

                                    $status = '<div style="width: 100px;">';
                                    $status .= $job->hot_job ? html_writer::tag('span', 'Hot job', array('class' => 'label label-warning d-inline-block')) : '';
                                    $status .= $job->confirmed ? html_writer::tag('span', 'Confirmed', array('class' => 'label label-success d-inline-block')) : '';
                                    $status .= $job->published ? html_writer::tag('span', 'Published', array('class' => 'label label-success d-inline-block')) : '';
                                    $status .= $job->deleted ? html_writer::tag('span', 'Deleted', array('class' => 'label label-danger d-inline-block')) : '';
                                    $status .= '</div>';
                                    echo '<tr>
                                            <td>' . userdate($job->timecreated, get_string('strftimedatetimeshort')) . '</td>
                                            <td>
                                                <a class="h4 text-primary" href="' . $VISANG->wwwroot . '/employers/jobs.php?employerid=' . $job->employer_id . '">' . $job->title . '</a>
                                                <div class="text-success"><i class="fa fa-usd" aria-hidden="true"></i> ' . $salary->name . '</div>
                                                <div>' . $job->short_description . '</div>
                                                <div>' . $_skills . '</div>
                                            </td>
                                            <td><a href="' . $VISANG->wwwroot . '/employers/view.php?employerid=' . $job->employer_id . '">' . $job->company_name . '</a></td>
                                            <td>' . $korean_level . '</td>
                                            <td style="width: 80px;">' . $application_count . '</td>
                                            <td>' . $bop_application_count . '</td>
                                            <td>' . ($application_count + $bop_application_count) . '</td>
                                            <td>' . $status . '</td>
                                        </tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->

                        <div>
                            <form id="mform1" action="index.php" method="get" style="display: none;">
                                <input type="hidden" name="page" value="1">
                                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                            </form>
                            <div class="pull-left form-inline">
                                <?php
                                $_row = new stdClass();
                                $_row->from = min($offset, $totalcount);
                                $_row->to = min($offset + $perpage, $totalcount);
                                $_row->total = $totalcount;
                                // $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                                $nums = array(10, 20, 30, 50);
                                ?>
                                <select onchange="$('input[name=perpage]').val(this.value); $('#mform1').submit();" class="form-control">
                                    <?php
                                    foreach ($nums as $num) {
                                        $opts = array('value' => $num);
                                        if ($num == $perpage) {
                                            $opts['selected'] = 'selected';
                                        }
                                        echo \html_writer::tag('option', get_string('showperpage', 'local_visang', $num), $opts);
                                    }
                                    ?>
                                </select>
                                <span class="text-muted" style="margin-left: 10px;">Rows <?php echo $_row->from; ?>-<?php echo $_row->to; ?> of <?php echo $totalcount; ?></span>
                            </div>
                            <div class="pull-right">
                                <?php
                                // $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                                $total_pages = ceil($totalcount / $perpage);
                                $current_page = $page;
                                $max_nav = 10;
                                $padding = floor($max_nav / 2);
                                $page_start = max(1, $current_page - $padding);
                                $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

                                echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
                                if ($current_page > 1) {
                                    echo '<li><a class="cursor-pointer" onclick="$(\'input[name=page]\').val(' . ($current_page - 1) . ');$(\'#mform1\').submit();"><span aria-hidden="true">&laquo;</span></a></li>';
                                } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
                                for ($i = $page_start; $i <= $page_end; $i++) {
                                    if ($i == $current_page) {
                                        echo '<li class="active"><span>' . $i . '</span></li>';
                                    } else {
                                        echo '<li><a class="cursor-pointer" onclick="$(\'input[name=page]\').val(' . $i . ');$(\'#mform1\').submit();">' . $i . '</a></li>';
                                    }
                                }
                                if ($current_page < $total_pages) {
                                    echo '<li><a class="cursor-pointer" onclick="$(\'input[name=page]\').val(' . ($current_page + 1) . ');$(\'#mform1\').submit();"><span aria-hidden="true">&raquo;</span></a></li>';
                                } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
                                echo \html_writer::end_tag('ul');
                                ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
$VISANG->page->footer();
