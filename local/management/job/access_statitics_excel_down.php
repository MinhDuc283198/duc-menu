<?php

require_once(dirname(__FILE__) . '/lib.php');
require_once("$CFG->libdir/excellib.class.php");
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);

$current_language = current_language();
$now = new DateTime("now", core_date::get_server_timezone_object());
// ===================================================================================================
$from_year = optional_param('fyear', 0, PARAM_INT);
$from_mon = optional_param('fmon', 0, PARAM_INT);
$to_year = optional_param('tyear', 0, PARAM_INT);
$to_mon = optional_param('tmon', 0, PARAM_INT);
$view_mode = optional_param('v', 'month', PARAM_RAW);

if ($from_year == 0) $from_year = $now->format('Y');
if ($from_mon == 0) $from_mon = $now->format('m');
if ($to_year == 0) $to_year = $now->format('Y');
if ($to_mon == 0) $to_mon = $now->format('m');

$time_format = '%Y-%m';
if ($view_mode == 'day') {
    $time_format = '%Y-%m-%d';
}

$sql = "SELECT FROM_UNIXTIME(timecreated, '{$time_format}') AS mformat, COUNT(*) AS cnt
    FROM {logstore_standard_log} l
    LEFT JOIN {lmsdata_user} lu ON lu.userid = l.userid
    WHERE l.eventname = :eventname
        AND l.timecreated >= :starttime
        AND l.timecreated < :endtime
        [joinstatus]
    GROUP BY FROM_UNIXTIME(timecreated, '{$time_format}')
    ORDER BY FROM_UNIXTIME(timecreated, '{$time_format}') ASC;";

$params = [
    'eventname' => '\core\event\user_loggedin',
    'starttime' => date('U', strtotime("{$from_year}-{$from_mon}-01 00:00:00")),
    'endtime' => date('U', strtotime("{$to_year}-{$to_mon}-01 00:00:00 +1 month")),
    'joinstatus' => 'j'
];

if ($view_mode == 'month') {
    $fields = array(
        get_string('statitics:month', 'local_visang'),
        get_string('statitics:access_count', 'local_visang'),
        get_string('statitics:access_count', 'local_visang'),
        get_string('statitics:total', 'local_visang')
    );
} else if ($view_mode == 'week') {
    $fields = array(
        get_string('statitics:month', 'local_visang'),
        get_string('statitics:week', 'local_visang'),
        get_string('statitics:access_count', 'local_visang') . '(masterkorean)',
        get_string('statitics:access_count', 'local_visang') . '(jobs)',
        get_string('statitics:total', 'local_visang')
    );
} else if ($view_mode == 'day') {
    $fields = array(
        get_string('statitics:dayofweek', 'local_visang'),
        get_string('statitics:access_count', 'local_visang') . '(masterkorean)',
        get_string('statitics:access_count', 'local_visang') . '(jobs)',
        get_string('statitics:total', 'local_visang')
    );
}

$date = date('Y-m-d', time());
$filename = get_string('statitics:access', 'local_visang') . '_' . $date . '.xls';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);

$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');
$col = 0;
foreach ($fields as $fieldname) {
    if ($filename != '') {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }
}

if ($view_mode == 'month') {
    $statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);
    $joinstatus = 'AND lu.joinstatus = :joinstatus';
    $job_statitics = $DB->get_records_sql(str_replace('[joinstatus]', $joinstatus, $sql), $params);
    $year = '';

    $row = 1;
    foreach ($statitics as $mon => $stat) {
        $col = 0;
        $month = explode('-', $mon);

        $_month = '';
        switch ($current_language) {
            case 'en':
                $_month .= date('M', strtotime($mon . '-01'));
                break;
            case 'vi':
                $_month .= get_string('statitics:month', 'local_visang') . ' ' . intval($month[1]);
                break;
            case 'ko':
                $_month .= intval($month[1]) . get_string('statitics:month', 'local_visang');
                break;
        }

        if ($year != $month[0]) {
            $_month .= ' (' . $month[0] . ')';
            $year = $month[0];
        }

        $ecnt = $stat->cnt - $job_statitics[$mon]->cnt;
        $jcnt = $job_statitics[$mon]->cnt;

        $worksheet[0]->write($row, $col++, $_month);
        $worksheet[0]->write($row, $col++, ($ecnt > 0 ? $ecnt : '-'));
        $worksheet[0]->write($row, $col++, ($jcnt > 0 ? $jcnt : '-'));
        $worksheet[0]->write($row, $col++, ($stat->cnt > 0 ? $stat->cnt : '-'));

        $row++;
    }
} else if ($view_mode == 'week') {
    $statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);

    $row = 1;
    foreach ($statitics as $mon => $stat) {
        $col = 0;

        $data = [];
        $month = explode('-', $mon);
        $start = "{$mon}-01";
        $end = date('Y-m-d', strtotime("{$start} +1 month"));
        $week = 1;
        while ($start < $end) {
            $params['starttime'] = date('U', strtotime($start));
            if (strtotime("{$start} +7 days") < strtotime($end)) {
                $params['endtime'] = date('U', strtotime("{$start} +7 days"));
            } else {
                $params['endtime'] = date('U', strtotime($end));
            }
            $_statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);
            $_job_statitics = $DB->get_records_sql(str_replace('[joinstatus]', 'AND lu.joinstatus = :joinstatus', $sql), $params);

            $ecnt = '-';
            $jcnt = '-';
            $cnt = '-';
            if (count($_statitics)) {
                $_t = array_pop($_statitics);
                $_j = array_pop($_job_statitics);
                $ecnt = $_t->cnt - $_j->cnt;
                if ($_j) $jcnt = $_j->cnt;
                $cnt = $_t->cnt;
            }

            $data[$week] = [
                'e' => $ecnt,
                'j' => $jcnt,
                'total' => $cnt,
            ];

            $start = date('Y-m-d', strtotime("{$start} +7 days"));
            $week++;
        }

        $_month = '';
        switch ($current_language) {
            case 'en':
                $_month .= date('M', strtotime($month[0] . '-' . $month[1] . '-01'));
                break;
            case 'vi':
                $_month .= get_string('statitics:month', 'local_visang') . ' ' . intval($month[1]);
                break;
            case 'ko':
                $_month .= intval($month[1]) . get_string('statitics:month', 'local_visang');
                break;
        }
        if ($year != $month[0]) {
            $_month .= ' (' . $month[0] . ')';
            $year = $month[0];
        }

        $worksheet[0]->write($row, $col++, $_month);

        $_week = '';
        switch ($current_language) {
            case 'en':
                $_week .= get_string('statitics:week', 'local_visang') . ' 1';
                break;
            case 'vi':
                $_week .= get_string('statitics:week', 'local_visang') . ' 1';
                break;
            case 'ko':
                $_week .= '1' . get_string('statitics:week', 'local_visang');
                break;
        }
        $worksheet[0]->write($row, $col++, $_week);

        $worksheet[0]->write($row, $col++, $data[1]['e']);
        $worksheet[0]->write($row, $col++, $data[1]['j']);
        $worksheet[0]->write($row, $col++, $data[1]['total']);

        $row++;

        for ($i = 2; $i <= count($data); $i++) {
            $col = 0;
            $worksheet[0]->write($row, $col++, '');
            $_week = '';
            switch ($current_language) {
                case 'en':
                    $_week .= get_string('statitics:week', 'local_visang') . ' ' . $i;
                    break;
                case 'vi':
                    $_week .= get_string('statitics:week', 'local_visang') . ' ' . $i;
                    break;
                case 'ko':
                    $_week .= $i . get_string('statitics:week', 'local_visang');
                    break;
            }
            $worksheet[0]->write($row, $col++, $_week);

            $worksheet[0]->write($row, $col++, $data[$i]['e']);
            $worksheet[0]->write($row, $col++, $data[$i]['j']);
            $worksheet[0]->write($row, $col++, $data[$i]['total']);

            $row++;
        }

        $row++;
    }
} else if ($view_mode == 'day') {
    $statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);
    $job_statitics = $DB->get_records_sql(str_replace('[joinstatus]', 'AND lu.joinstatus = :joinstatus', $sql), $params);
    $year = '';

    $row = 1;
    foreach ($statitics as $mon => $stat) {
        $col = 0;

        $month = explode('-', $mon);

        $worksheet[0]->write($row, $col++, get_string('statitics:' . date('D', strtotime($mon)), 'local_visang') . " " . date('d/m/Y', strtotime($mon)));

        if ($year != $month[0]) {
            $year = $month[0];
        }

        $ecnt = $stat->cnt - $job_statitics[$mon]->cnt;
        $jcnt = $job_statitics[$mon]->cnt;

        $worksheet[0]->write($row, $col++, ($ecnt > 0 ? $ecnt : '-'));
        $worksheet[0]->write($row, $col++, ($jcnt > 0 ? $jcnt : '-'));
        $worksheet[0]->write($row, $col++, ($stat->cnt > 0 ? $stat->cnt : '-'));

        $row++;
    }
}

$workbook->close();
die;
