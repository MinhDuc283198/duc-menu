<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

function visang_search_salaries()
{
    global $DB;
    $employerid = required_param('employerid', PARAM_INT);
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();

    $sql .= 'employer_id=:employerid';
    $params['employerid'] = $employerid;

    if ($search != '') {
        $sql .= ' AND ' . $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params[$searchfield] = '%' . $search . '%';
    }
    $offset = ($page - 1) * $perpage;
    $skills = $DB->get_records_select('vi_job_salary_categories', $sql, $params, '{vi_job_salary_categories}.name ASC', $fields, $offset, $perpage);

    return $skills;
}

function visang_search_reasons()
{
    global $DB;
    $employerid = required_param('employerid', PARAM_INT);
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();

    $sql .= 'employer_id=:employerid';
    $params['employerid'] = $employerid;

    if ($search != '') {
        $sql .= ' AND ' . $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params[$searchfield] = '%' . $search . '%';
    }
    $offset = ($page - 1) * $perpage;
    $skills = $DB->get_records_select('vi_reason_categories', $sql, $params, '{vi_reason_categories}.name ASC', $fields, $offset, $perpage);

    return $skills;
}

function visang_findOrCreate_salaryID($name, $employerid)
{
    global $DB;
    $salary = $DB->get_record_select('vi_job_salary_categories', 'name=:name AND employer_id=:employerid', array('name' => $name, 'employerid' => $employerid));
    if ($salary == null) {
        $salary = new stdClass();
        $salary->name = $name;
        $salary->employer_id = $employerid;
        return $DB->insert_record('vi_job_salary_categories', $salary);
    } else return $salary->id;
}

function visang_findOrCreate_reasonCaregoryID($name, $employerid)
{
    global $DB;
    $reason = $DB->get_record_select('vi_reason_categories', 'name=:name AND employer_id=:employerid', array('name' => $name, 'employerid' => $employerid));
    if ($reason == null) {
        $reason = new stdClass();
        $reason->name = $name;
        $reason->employer_id = $employerid;
        return $DB->insert_record('vi_reason_categories', $reason);
    } else return $reason->id;
}

function visang_get_employer_reasons($employerid)
{
    global $DB;
    $sql = "SELECT cat.*
    FROM {vi_reason_categories} cat
    JOIN {vi_employer_reasons} e ON cat.id = e.reason_category_id
    WHERE e.employer_id = :employerid
    ORDER BY cat.name ASC";
    return $DB->get_records_sql($sql, array('employerid' => $employerid));
}

function visang_get_job_reasons($jobid)
{
    global $DB;
    $sql = "SELECT cat.*
    FROM {vi_reason_categories} cat
    JOIN {vi_job_reasons} j ON cat.id = j.reason_category_id
    WHERE j.job_id = :jobid
    ORDER BY cat.name ASC";
    return $DB->get_records_sql($sql, array('jobid' => $jobid));
}

function visang_search_jobs()
{
    global $DB, $CFG;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 20, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW_TRIMMED));
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = array();
    $params = array();
    $searchfields = array('title');

    $offset = ($page - 1) * $perpage;

    foreach ($searchfields as $searchfield) {
        $sql[] = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params[$searchfield] = '%' . $search . '%';
    }

    $sql_str = "FROM {vi_jobs} j
                JOIN {vi_employers} e ON j.employer_id = e.id
                WHERE " . implode(" AND ", $sql) . "
                ORDER BY j.timecreated DESC, j.title ASC";
    $jobs = $DB->get_records_sql("SELECT j.id, j.title, e.company_name " . $sql_str, $params, $offset, $perpage);
    return $jobs;
}

function visang_search_employers()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 20, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'company_name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();

    if ($search != '') {
        $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params[$searchfield] = '%' . $search . '%';
    }
    $offset = ($page - 1) * $perpage;
    $employers = $DB->get_records_select('vi_employers', $sql, $params, '{vi_employers}.company_name ASC', $fields, $offset, $perpage);

    return $employers;
}
