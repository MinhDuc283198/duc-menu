<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

function visang_search_users()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    // $searchfield = optional_param('searchfield', 'username', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $searchfields = array('username', 'firstname');

    $sql = array();
    $params = array();
    if ($search != '') {
        foreach ($searchfields as $searchfield) {
            $sql[] = $DB->sql_like($searchfield, ':' . $searchfield, false);
            $params[$searchfield] = '%' . $search . '%';
        }
    }
    $offset = ($page - 1) * $perpage;
    $users = $DB->get_records_select('user', implode(" OR ", $sql), $params, '{user}.username ASC', $fields, $offset, $perpage);

    return $users;
}

function visang_get_admin_employers($employerid)
{
    global $DB;
    $sql = "SELECT u.*
    FROM {user} u
    JOIN {vi_admin_employers} e ON u.id = e.user_id
    WHERE e.employer_id = :employerid
    ORDER BY u.username ASC";
    return $DB->get_records_sql($sql, array('employerid' => $employerid));
}
