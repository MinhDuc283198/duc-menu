<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

function visang_get_rating_html($num, $length = 5)
{
    $html = '';
    for ($i = 1; $i <= $length; $i++) {
        if ($num < $i) {
            if ($num > $i - 1) {
                $html .= ' <i class="fa fa-star-half-o" aria-hidden="true"></i>';
            } else {
                $html .= ' <i class="fa fa-star-o" aria-hidden="true"></i>';
            }
        } else {
            $html .= ' <i class="fa fa-star" aria-hidden="true"></i>';
        }
    }
    return $html;
}

// function visang_get_employer_ratings($employerid)
// {
//     global $DB;
//     $sql = "SELECT cat.*, r.rate
//         FROM {vi_ratings} cat
//         LEFT JOIN {vi_employer_review_ratings} r ON cat.id = r.rating_id
//         WHERE r.employer_id = :employerid
//         ORDER BY cat.sortorder ASC";
//     return $DB->get_records_sql($sql, array('employerid' => $employerid));
// }

function visang_get_employer_ratings($employerid, $userid = 0)
{
    global $DB;
    $ratings = array();
    $lang = current_language();
    $where = "rv.employer_id=$employerid";
    if($userid != 0) $where .= " AND rv.user_id=$userid";
    $query = "SELECT vr.id, vr.title_$lang as title, AVG(rvr.point) as rate
                FROM {vi_ratings} vr
                LEFT OUTER JOIN {vi_review_ratings} rvr ON rvr.rating_id = vr.id
                LEFT OUTER JOIN {vi_reviews} rv ON rvr.review_id = rv.id
                WHERE $where
                GROUP BY vr.id
                ORDER BY vr.sortorder";
    $ratings = $DB->get_records_sql($query);
    $rate = 0.00;
    if (count($ratings)) {
        foreach ($ratings as $rating) {
            $rate += floatval($rating->rate);
        }
        $rate = $rate / count($ratings);
    } else {
        $ratings = $DB->get_records_sql("SELECT vr.id, vr.title_$lang as title, COALESCE(0) as rate
                                        FROM {vi_ratings} vr
                                        ORDER BY vr.sortorder");
    }
    return array('ratings' => $ratings, 'rate' => $rate);
}
