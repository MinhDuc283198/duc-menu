<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

function visang_search_districts()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();
    if ($search != '') {
        $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params = array($searchfield => '%' . $search . '%');
    }
    $offset = ($page - 1) * $perpage;
    $skills = $DB->get_records_select('vi_districts', $sql, $params, '{vi_districts}.name ASC', $fields, $offset, $perpage);

    return $skills;
}

function visang_findOrCreate_districtID($name, $countryid)
{
    global $DB;
    $district = $DB->get_record_select('vi_districts', 'name=:name AND country_id=:countryid', array('name' => $name, 'countryid' => $countryid));
    if ($district == null) {
        $district = new stdClass();
        $district->name = $name;
        $district->name_ko = $name;
        $district->name_en = $name;
        $district->name_vi = $name;
        $district->country_id = $countryid;
        return $DB->insert_record('vi_districts', $district);
    } else return $district->id;
}

function visang_search_countries()
{
    global $DB;
    $page = optional_param('page', 1, PARAM_INT);
    $perpage = optional_param('perpage', 10, PARAM_INT);
    $search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
    $searchfield = optional_param('searchfield', 'name', PARAM_RAW);
    $fields = optional_param('fields', '*', PARAM_RAW);

    $sql = '';
    $params = array();
    if ($search != '') {
        $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
        $params = array($searchfield => '%' . $search . '%');
    }
    $offset = ($page - 1) * $perpage;
    $skills = $DB->get_records_select('vi_countries', $sql, $params, '{vi_countries}.name ASC', $fields, $offset, $perpage);

    return $skills;
}

function visang_findOrCreate_countryID($name)
{
    global $DB;
    $country = $DB->get_record_select('vi_countries', 'name=:name', array('name' => $name));
    if ($country == null) {
        $country = new stdClass();
        $country->name = $name;
        return $DB->insert_record('vi_countries', $country);
    } else return $country->id;
}
