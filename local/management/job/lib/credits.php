<?php


/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
defined('MOODLE_INTERNAL') || die();

function table_pagination2($url, $params = array(), $total_pages = 1, $current_page = 1, $max_nav = 10)
{
    $padding = floor($max_nav / 2);
    $page_start = max(1, $current_page - $padding);
    $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

    echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
    if ($current_page > 1) {
        $p = $params;
        $p['page2'] = $current_page - 1;
        echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&laquo;</span></a></li>';
    } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="active"><span>' . $i . '</span></li>';
        } else {
            $p = $params;
            $p['page2'] = $i;
            echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '">' . $i . '</a></li>';
        }
    }
    if ($current_page < $total_pages) {
        $p = $params;
        $p['page2'] = $current_page + 1;
        echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&raquo;</span></a></li>';
    } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
    echo \html_writer::end_tag('ul');
}