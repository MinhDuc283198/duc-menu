<?php
/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once(dirname(__FILE__) . '/../lib.php');

global $CFG, $VISANG, $DB, $USER;

$context = context_system::instance();

$PAGE->set_context($context);


require_sesskey();

//$cvfiles = $_FILES['file'];

$files = array_filter($_FILES['file']['name']); //Use something similar before processing files.
$total_count = count($_FILES['file']['name']);

for( $i=0 ; $i < $total_count ; $i++ ) {

    $tmpFilePath = $_FILES['file']['tmp_name'][$i];

    if ($tmpFilePath) {
        $context = context_system::instance();
        $fs = get_file_storage();
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $context->id,
            'component' => 'local_visang',
            'filearea' => 'docs',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $_FILES['file']['name'][$i],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id
        );

        $fs->create_file_from_pathname($file_record, $_FILES['file']['tmp_name'][$i]);

        $resume = new stdClass();
        $resume->title = $_FILES['file']['name'][$i];
        $resume->user_id = $USER->id;
        $resume->type = 'cvfile';
        $resume->attachment_url = implode('/', array($context->id, 'local_visang', 'docs', $itemid, $_FILES['file']['name'][$i]));
        $resume->published = 1;
        $resume->timecreated = time();
        $resume->timemodified = time();
        $DB->insert_record('vi_resumes', $resume);
    }
}
redirect('./index.php');