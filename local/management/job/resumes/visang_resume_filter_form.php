<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');

class visang_resume_filter_form extends moodleform
{
  public $renderer;

  function definition()
  {
    global $CFG, $DB, $USER, $VISANG;

    $mform = $this->_form;
    $this->renderer = new \local_visang\form_renderer($mform);

    $baseurl = $this->_customdata['baseurl'];
    $action = 'filters';

    $mform->addElement('hidden', 'action');
    $mform->setType('action', PARAM_RAW);
    $mform->setDefault('action', $action);

    $mform->addElement('hidden', 'page');
    $mform->setType('page', PARAM_INT);
    $mform->setDefault('page', 1);

    $mform->addElement('hidden', 'perpage');
    $mform->setType('perpage', PARAM_INT);
    $mform->setDefault('perpage', 10);

    // ==========================================================================================
    $mform->addElement('header', 'filters', 'Filters');

    $mform->addElement('advautocomplete', 'employers', 'Employers', array(), array(
      'action' => 'search_employers',
      'fields' => 'id,company_name',
      'template' => '<div style="margin:0px; display:inline-block;">
                      <b>{{ item.company_name }}</b>
                    </div>',
      'canaddnew' => false
    ));

    $mform->addElement('advautocomplete', 'jobs', 'Jobs', array(), array(
      'action' => 'search_jobs',
      'template' => '<div style="margin:0px; display:inline-block;">
                      <b>{{ item.title }}</b>
                      <i class="text-muted"> - {{ item.company_name }}</i>
                    </div>',
      'canaddnew' => false
    ));

    $mform->addElement('advautocomplete', 'users', 'Users', array(), array(
      'action' => 'search_users',
      'fields' => 'id,firstname,email',
      'template' => '<div style="margin:0px; display:inline-block;">
                      <b>{{ item.firstname }}</b>
                      <i class="text-muted">({{ item.email }})</i>
                    </div>',
      'canaddnew' => false
    ));

    $types = array(
      'cv' => 'Resume',
      'file' => 'Attachments',
      'video' => 'Video profile',
    );
    $mform->addElement('checklist', 'types', 'Type', $types, array('class' => 'margin'));

    $mform->addElement('input', 'search', 'Search by title', array('class' => 'form-control min-w'));
    $mform->setType('search', PARAM_RAW);
    // ==========================================================================================
    $actions = array();
    $actions[] = &$mform->createElement('xbutton', 'submitbutton', 'Find CV', array('class' => 'btn btn-default margin', 'value' => 'submit'));
    $actions[] = &$mform->createElement('html', html_writer::link($baseurl, 'Clear filters', array('class' => 'btn btn-default margin')));
    $mform->addElement('formactions', 'form_actions', '', $actions);
  }

  function save($data)
  {
    //
  }
}
