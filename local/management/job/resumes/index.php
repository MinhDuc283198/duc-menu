<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/visang_resume_filter_form.php');

$action = optional_param('action', '', PARAM_ALPHA);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));

$baseurl = $VISANG->wwwroot . '/resumes/index.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
if ($action == 'export') {
    $d_id = required_param('id', PARAM_INT);
    $sesskey = required_param('sesskey', PARAM_ALPHA);
    $dir = dirname(__FILE__) . '/export/' . $sesskey;

    define('AJAX_SCRIPT', true);
    $d_resume = $DB->get_record('vi_resumes', array('id' => $d_id));
    if ($d_resume) {
        $fs = get_file_storage();

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if ($d_resume->type == 'file' || $d_resume->type == 'cvfile' || $d_resume->type == 'resume') {
            $fileinfo = explode("/", $d_resume->attachment_url);
            $file = $fs->get_file($fileinfo[0], $fileinfo[1], $fileinfo[2], $fileinfo[3], '/', $fileinfo[4]);
            if ($file) {
                file_put_contents($dir . '/' . $d_resume->id . '_' . $d_resume->title . '_' . $fileinfo[4], $file->get_content());
            }
        }

        if ($d_resume->type == 'video') {
            echo json_encode(array('status' => 0));
            die();
        }

        echo json_encode(array('status' => 1));
        die();
    }
    echo json_encode(array('status' => 0));
    die();
}

if ($action == 'zip') {
    $sesskey = required_param('sesskey', PARAM_ALPHA);
    $dir = dirname(__FILE__) . '/export/' . $sesskey;

    define('AJAX_SCRIPT', true);

    $filename = date('CV_Y-m-d', time()) . '.zip';

    $zip = new ZipArchive();
    $zip->open($dir . '/' . $filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file) {
        if (!$file->isDir()) {
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($dir) + 1);
            $zip->addFile($filePath, $relativePath);
        }
    }
    $zip->close();

    echo json_encode(array('status' => 1, 'download_link' => $VISANG->wwwroot . '/resumes/export/' . $sesskey . '/' . $filename));
    die();
}

if ($action == 'closeexport') {
    $sesskey = required_param('sesskey', PARAM_ALPHA);
    $dir = dirname(__FILE__) . '/export/' . $sesskey;

    define('AJAX_SCRIPT', true);

    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file) {
        if (!$file->isDir()) {
            unlink($file->getRealPath());
        }
    }
    rmdir($dir);

    echo json_encode(array('status' => 1));
    die();
}

// only for dev mode
if ($action == 'resumedevcache') {
    define('AJAX_SCRIPT', true);
    $fs = get_file_storage();
    $offset = ($page - 1) * $perpage;
    $resumes = $DB->get_records('vi_resumes', array('type' => 'resume'), '', '*', $offset, $perpage);
    foreach ($resumes as $resume) {
        if ($resume->attachment_url != '') {
            $fileinfo = explode("/", $resume->attachment_url);
            $file = $fs->get_file($fileinfo[0], $fileinfo[1], $fileinfo[2], $fileinfo[3], '/', $fileinfo[4]);
            if ($file) {
                $file->delete();
            }
        }
        $itemid = file_get_unused_draft_itemid();
        $file_record = array(
            'contextid' => $context->id,
            'component' => 'local_visang',
            'filearea' => 'docs',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $resume->title . '.html',
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id
        );
        $d_url = $CFG->wwwroot . '/local/job/resumes/pop_resume_detail.php?resumeid=' . $resume->id . '&sesskey=' . sesskey() . '&download=1&lang=' . current_language();
        $fs->create_file_from_string($file_record, file_get_contents($d_url));
        $attachment_url = implode('/', array($context->id, 'local_visang', 'docs', $itemid, $resume->title . '.html'));

        $res = new stdClass();
        $res->id = $resume->id;
        $res->attachment_url = $attachment_url;
        $DB->update_record('vi_resumes', $res);
    }
    echo json_encode(array('status' => 1, 'page' => $page, 'perpage' => $perpage));
    die();
}

$mform = new visang_resume_filter_form($baseurl, array('baseurl' => $baseurl));
$formdata = $mform->get_submitted_data();

// filters
$sql = "FROM {vi_resumes} cv";
$where = array();
$params = array();

if ($formdata) {
    $page = intval($formdata->page);
    $perpage = intval($formdata->perpage);

    if ($formdata->search != '') {
        $where[] = $DB->sql_like('cv.title', ':' . 'title', false);
        $params['title'] = '%' . $formdata->search . '%';
    }

    if (count($formdata->employers)) {
        $sql .= " LEFT OUTER JOIN {vi_job_applications} apply ON apply.candidate_cv = cv.id
                LEFT OUTER JOIN {vi_jobs} j ON apply.job_id = j.id";
        $employer_ids = array();
        foreach ($formdata->employers as $employer) {
            $employer = json_decode($employer);
            array_push($employer_ids, $employer->id);
        }
        $where[] = "j.employer_id IN (" . implode(",", $employer_ids) . ")";
    }

    if (count($formdata->jobs)) {
        if (!count($formdata->employers)) $sql .= " LEFT OUTER JOIN {vi_job_applications} apply ON apply.candidate_cv = cv.id";
        $job_ids = array();
        foreach ($formdata->jobs as $job) {
            $job = json_decode($job);
            array_push($job_ids, $job->id);
        }
        $where[] = "apply.job_id IN (" . implode(",", $job_ids) . ")";
    }

    if (count($formdata->users)) {
        $user_ids = array();
        foreach ($formdata->users as $user) {
            $user = json_decode($user);
            array_push($user_ids, $user->id);
        }
        $where[] = "cv.user_id IN (" . implode(",", $user_ids) . ")";
    }

    if (count($formdata->types)) {
        $types = array();
        foreach ($formdata->types as $type) {
            if ($type == 'cv') {
                $types[] = "cv.type=:resume";
                $types[] = "cv.type=:cvfile";
                $params['resume'] = 'resume';
                $params['cvfile'] = 'cvfile';
            } else {
                $types[] = "cv.type=:" . $type;
                $params[$type] = $type;
            }
        }
        $where[] = "(" . implode(" OR ", $types) . ")";
    }
}

$offset = ($page - 1) * $perpage;
if (count($where)) $sql .= " WHERE " . implode(" AND ", $where);
$sql .= " ORDER BY cv.timecreated DESC, cv.title ASC";
$resumes = $DB->get_records_sql("SELECT cv.* " . $sql . " LIMIT $perpage OFFSET $offset", $params);
$totalcount = $DB->count_records_sql("SELECT COUNT(*) " . $sql, $params);

$mform->renderer->_form->getElement('page')->_attributes['value'] = $page;

if ($action == 'getallresumes') {
    define('AJAX_SCRIPT', true);
    $ids = array();
    $cvs = $DB->get_records_sql("SELECT cv.id " . $sql, $params);
    foreach ($cvs as $cv) {
        array_push($ids, $cv->id);
    }
    echo json_encode(array('ids' => $ids));
    die();
}
// ===================================================================================================
// renders
$title = 'All CV';
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'resumes';
$VISANG->page->navbar[] = 'CV';

$VISANG->page->header();

$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table->id = 'resumes';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    '<input type="checkbox" title="allcheck" id="allcheck" @change="allcheck" class="chkbox">',
    'Title',
    'Type',
    'User',
    'Created at',
    ''
);

$table->data = array();

foreach ($resumes as $resume) {
    $actions = '';

    $type = '';
    $resume_url = '';
    switch ($resume->type) {
        case 'resume':
            $type = 'Resume';
            $resume_url = $CFG->job_wwwroot . '/local/job/resumes/view.php?resumeid=' . $resume->id;
            break;
        case 'cvfile':
            $type = 'Resume';
            $resume_url = $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url;
            break;
        case 'file':
            $type = 'Attachments';
            $resume_url = $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url;
            break;
        case 'video':
            $type = 'Video profile';
            $resume_url = $resume->video_url;
            break;
    }

    $actions .= '<div class="btn btn-default" role="button" @click="visang_view_cv(\'' . $resume_url . '\', \'' . $resume->type . '\')"><i class="fa fa-eye" aria-hidden="true"></i> View CV</div>';

    $row = array(
        '<input type="checkbox" name="ids[]" value="' . $resume->id . '" @change="chkboxItem" class="chkbox chkbox-item">',
        $resume->title,
        $type,
        $DB->get_record('user', array('id' => $resume->user_id))->firstname,
        userdate($resume->timecreated, get_string('strftimedatetimeshort')),
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<style>
    #resumes th:first-child,
    #resumes td:first-child {
        width: 40px !important;
        min-width: 40px !important;
        text-align: center;
    }
</style>
<section class="content">
    <?php if (!is_writable(dirname(__FILE__) . '/export/')) : ?>
        <div class="form-group">
            <div class="callout callout-warning">
                <h4>Warning!</h4>
                <p>The directory <?php echo dirname(__FILE__) . '/export/'; ?> has no write permissions.</p>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <?php $mform->renderer->display(); ?>
    </div>
    <div>
        <p>
            <b>이력서 총 개수: <?php echo $totalcount; ?>개</b>
        </p>
    </div>
    <div class="box">
        <div class="box-body" id="tbl_resumes">
            <div class="form-group">
                <button class="btn btn-primary" @click="downloads('selected')">Download selected <span v-if="ids.length">({{ ids.length }})</span></button>
                <button class="btn btn-warning" @click="downloads('all')">Download all (<?php echo $totalcount; ?>)</button>
                <button class="btn btn-success" @click="uploadCv()">Upload CV</button>
            </div>
            <div class="form-group">
                <?php
                echo html_writer::table($table);
                ?>
                <div>
                    <div class="pull-left form-inline">
                        <?php
                        $_row = new stdClass();
                        $_row->from = min($offset, $totalcount);
                        $_row->to = min($offset + $perpage, $totalcount);
                        $_row->total = $totalcount;
                        // $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                        $nums = array(10, 20, 30, 50);
                        ?>
                        <select onchange="$('input[name=perpage]').val(this.value); $('#mform1').submit();" class="form-control">
                            <?php 
                            foreach ($nums as $num) {
                                $opts = array('value' => $num);
                                if ($num == $perpage) {
                                    $opts['selected'] = 'selected';
                                }
                                echo \html_writer::tag('option', get_string('showperpage', 'local_visang', $num), $opts);
                            }
                            ?>
                        </select>
                        <span class="text-muted" style="margin-left: 10px;">Rows <?php echo $_row->from; ?>-<?php echo $_row->to; ?> of <?php echo $totalcount; ?></span>
                    </div>
                    <div class="pull-right">
                        <?php
                        // $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                        $total_pages = ceil($totalcount / $perpage);
                        $current_page = $page;
                        $max_nav = 10;
                        $padding = floor($max_nav / 2);
                        $page_start = max(1, $current_page - $padding);
                        $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

                        echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
                        if ($current_page > 1) {
                            echo '<li><a class="cursor-pointer" onclick="$(\'input[name=page]\').val('.($current_page - 1).');$(\'#mform1\').submit();"><span aria-hidden="true">&laquo;</span></a></li>';
                        } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
                        for ($i = $page_start; $i <= $page_end; $i++) {
                            if ($i == $current_page) {
                                echo '<li class="active"><span>' . $i . '</span></li>';
                            } else {
                                echo '<li><a class="cursor-pointer" onclick="$(\'input[name=page]\').val('.$i.');$(\'#mform1\').submit();">' . $i . '</a></li>';
                            }
                        }
                        if ($current_page < $total_pages) {
                            echo '<li><a class="cursor-pointer" onclick="$(\'input[name=page]\').val('.($current_page + 1).');$(\'#mform1\').submit();"><span aria-hidden="true">&raquo;</span></a></li>';
                        } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
                        echo \html_writer::end_tag('ul');
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="downloadModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h3>Export ({{ progress_idx }}/{{ ids.length }})</h3>
                            <p>Successful: {{ successful }}</p>
                            <p>Failed: {{ failed }}</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bd-toggle-animated-progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" :style="{width: progress+'%'}">
                                    <span v-if="progress">{{ progress }}%</span>
                                    <span v-else style="color: #333;">Pending...</span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <button @click="closeModal" type="button" class="btn btn-warning">Close</button>
                            <a v-if="download_link" :href="download_link" target="_blank" class="btn btn-primary">Download file</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Large modal -->
            <!-- Modal -->
            <div class="modal fade" id="upload-cv-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
                <form method="post" action="cv_upload.php" enctype="multipart/form-data">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>">
                            <div class="modal-body">
                                <h3>Choice Cv</h3>
                                <div class="form-group">
                                    <label for="cv_ip">CV title <span class="text-danger">(*)</span></label>
                                    <input class="form-control" id="cv_ip"  type="file" name="file[]" accept=".doc,.docx,.pdf" readonly multiple>
                                </div>

                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <button data-dismiss="modal" type="button" class="btn btn-danger">Close</button>
                                <button type="submit" class="btn btn-success">Upload</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Large modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="vi-cv-modal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <iframe id="vi-cv-iframe" :src="view_cv" frameborder="0" style="width: 100%; height: 95vh;" scrolling="yes"></iframe>
                    </div>
                </div>
            </div>
        </div>
</section>

<script>
    $.fn.serializeFormJSON = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $(function() {
        new Vue({
            el: '#tbl_resumes',
            data: {
                progress_status: false,
                ids: [],
                progress_idx: 0,
                successful: 0,
                failed: 0,
                download_link: '',
                view_cv: ''
            },
            computed: {
                progress: function() {
                    return Math.round((this.progress_idx / this.ids.length) * 100);
                }
            },
            methods: {
                allcheck: function() {
                    var checked = $('#allcheck').is(':checked');
                    $('.chkbox-item').each(function(idx, el) {
                        $(el).prop('checked', checked);
                    });
                    this.chkboxItem();
                },
                chkboxItem: function() {
                    var ids = [];
                    $('.chkbox-item:checked').each(function(idx, el) {
                        ids.push($(el).val());
                    });
                    this.ids = ids;
                },
                downloads: function(action) {
                    if (action == 'selected' && !this.ids.length) return;
                    if (action == 'all') {
                        var params = $('#mform1').serializeFormJSON();
                        params.action = 'getallresumes';
                        $.post("<?php echo $baseurl; ?>", params, function(data) {
                            data = JSON.parse(data);
                            this.ids = data.ids;
                            $('#downloadModal').modal('show');
                            this.downloadAction();
                        }.bind(this))
                    } else {
                        $('#downloadModal').modal('show');
                        this.downloadAction();
                    }
                },
                uploadCv: function() {
                        $('#upload-cv-modal').modal('show');
                },
                downloadAction() {
                    this.progress_idx = 0;
                    this.successful = 0;
                    this.failed = 0;
                    this.progress_status = true;
                    this.download_link = '';
                    for (var i = 0; i < this.ids.length && this.progress_status; i++) {
                        var id = this.ids[i];
                        $.post("<?php echo $baseurl; ?>", {
                                action: "export",
                                sesskey: "<?php echo sesskey(); ?>",
                                id: id,
                            }, function(data) {
                                data = JSON.parse(data);
                                if (data.status) {
                                    this.successful++;
                                } else this.failed++;
                            }.bind(this))
                            .fail(function() {
                                this.failed++;
                            }.bind(this))
                            .always(function() {
                                this.progress_idx++;
                                if (this.progress_idx == this.ids.length && this.failed < this.ids.length) {
                                    this.zipfile();
                                }
                            }.bind(this));
                    }
                },
                zipfile: function() {
                    $.post("<?php echo $baseurl; ?>", {
                        action: "zip",
                        sesskey: "<?php echo sesskey(); ?>",
                    }, function(data) {
                        data = JSON.parse(data);
                        this.download_link = data.download_link;
                    }.bind(this));
                },
                closeModal: function() {
                    this.progress_status = false;
                    $.post("<?php echo $baseurl; ?>", {
                        action: "closeexport",
                        sesskey: "<?php echo sesskey(); ?>",
                    });
                    $('#downloadModal').modal('hide');
                },
                visang_view_cv: function(url, type) {
                    this.view_cv = '';
                    if (type == 'cvfile' || type == 'file') {
                        setTimeout(function() {
                            // this.view_cv = 'https://docs.google.com/gview?url=' + url + '&embedded=true';
                            this.view_cv = url;
                            $('#vi-cv-modal').modal('show');
                        }.bind(this), 200);
                    } else {
                        window.open(url, '_blank');
                    }
                }
            }
        });
    });
</script>
<?php
$VISANG->page->footer();
