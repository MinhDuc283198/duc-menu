<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');

class visang_rating_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $ratingid = $this->_customdata['ratingid'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'ratingid');
        $mform->setType('ratingid', PARAM_INT);
        $mform->setDefault('ratingid', $ratingid);

        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'title', get_string('name'), array('required', 'class' => 'form-control min-w'));
        $mform->addRule('title', null, 'required', null, 'server');
        $mform->setType('title', PARAM_RAW);

        $mform->addElement('input', 'title_ko', get_string('name') . '[KO]', array('required', 'class' => 'form-control min-w'));
        $mform->addRule('title_ko', null, 'required', null, 'server');
        $mform->setType('title_ko', PARAM_RAW);

        $mform->addElement('input', 'title_en', get_string('name') . '[EN]', array('required', 'class' => 'form-control min-w'));
        $mform->addRule('title_en', null, 'required', null, 'server');
        $mform->setType('title_en', PARAM_RAW);

        $mform->addElement('input', 'title_vi', get_string('name') . '[VI]', array('required', 'class' => 'form-control min-w'));
        $mform->addRule('title_vi', null, 'required', null, 'server');
        $mform->setType('title_vi', PARAM_RAW);

        $mform->addElement('input', 'short_name', get_string('shortname'), array('class' => 'form-control min-w'));
        $mform->setType('short_name', PARAM_RAW);

        $mform->addElement('input', 'sortorder', get_string('order'), array('class' => 'form-control min-w', 'type' => 'number', 'min' => 0));
        $mform->setType('sortorder', PARAM_INT);

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    function save($data)
    {
        global $DB;

        $content = new stdClass();
        $content->title = $data->title;
        $content->title_ko = $data->title_ko;
        $content->title_en = $data->title_en;
        $content->title_vi = $data->title_vi;
        $content->short_name = $data->short_name;
        $content->sortorder = max(0, intval($data->sortorder));
        $content->timemodified = time();

        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                $DB->insert_record('vi_ratings', $content);
                break;
            case 'edit':
                $content->id = $data->ratingid;
                $DB->update_record('vi_ratings', $content);
                break;
        }
    }
}
