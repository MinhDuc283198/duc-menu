<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');

class visang_setting_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        // ==========================================================================================
        $mform->addElement('header', 'apis', 'APIs & Services');
        $mform->addElement('input', 'google_map_api', get_string('settings:google_map_api', 'local_visang'), array('class' => 'form-control', 'desc' => 'Google Map API key.'));
        
        $mform->addElement('header', 'payment', 'momo payment system key');
        $mform->addElement('input', 'partnerCode', get_string('settings:partnerCode', 'local_visang'), array('class' => 'form-control', 'desc' => 'Momopay partnerCode'));
        $mform->addElement('input', 'accessKey', get_string('settings:accessKey', 'local_visang'), array('class' => 'form-control', 'desc' => 'Momopay accessKey'));
        $mform->addElement('input', 'secretKey', get_string('settings:secretKey', 'local_visang'), array('class' => 'form-control', 'desc' => 'Momopay secretKey'));
        
        $mform->addElement('header', 'login', 'oauth2 key [mk]');
        $mform->addElement('input', 'facebookclientid', get_string('settings:facebooklogin', 'local_visang'), array('class' => 'form-control', 'desc' => 'facebookclientid'));
        $mform->addElement('input', 'facebookclientsecret', get_string('settings:facebooksecret', 'local_visang'), array('class' => 'form-control', 'desc' => 'facebookclientsecret'));
        $mform->addElement('input', 'googleclientid', get_string('settings:googlelogin', 'local_visang'), array('class' => 'form-control', 'desc' => 'googleclientid'));
        $mform->addElement('input', 'googleclientsecret', get_string('settings:googlesecret', 'local_visang'), array('class' => 'form-control', 'desc' => 'googleclientsecret'));
        
        $mform->addElement('header', 'login', 'oauth2 key [mk jobs]');
        $mform->addElement('input', 'facebookclientid2', get_string('settings:facebooklogin', 'local_visang'), array('class' => 'form-control', 'desc' => 'facebookclientid'));
        $mform->addElement('input', 'facebookclientsecret2', get_string('settings:facebooksecret', 'local_visang'), array('class' => 'form-control', 'desc' => 'facebookclientsecret'));
        $mform->addElement('input', 'googleclientid2', get_string('settings:googlelogin', 'local_visang'), array('class' => 'form-control', 'desc' => 'googleclientid'));
        $mform->addElement('input', 'googleclientsecret2', get_string('settings:googlesecret', 'local_visang'), array('class' => 'form-control', 'desc' => 'googleclientsecret'));
        
        $mform->addElement('header', 'login', 'visang cdn data');
        $mform->addElement('input', 'cdnbaseurl', get_string('settings:cdnbaseurl', 'local_visang'), array('class' => 'form-control', 'desc' => 'cdnbaseurl'));
        $mform->addElement('input', 'cdnjwttoken', get_string('settings:cdnjwttoken', 'local_visang'), array('class' => 'form-control', 'desc' => 'cdnjwttoken'));
                
        $mform->addElement('header', 'login', 'SMS infomation');
        $mform->addElement('input', 'brandname', get_string('settings:brandname', 'local_visang'), array('class' => 'form-control', 'desc' => 'brandname'));
        $mform->addElement('input', 'clientid', get_string('settings:clientid', 'local_visang'), array('class' => 'form-control', 'desc' => 'clientid'));
        $mform->addElement('input', 'clientsecret', get_string('settings:clientsecret', 'local_visang'), array('class' => 'form-control', 'desc' => 'clientsecret'));

        $mform->addElement('header', 'email', 'Email feedback');
        $mform->addElement('input', 'emailfeedback', 'Email feedback', array('class' => 'form-control', 'desc' => 'Emailfeedback'));
        // ==========================================================================================
        // $mform->addElement('header', 'admin', 'Visang administrators');
        // $mform->addElement('advautocomplete', 'users', get_string('admin'), array('desc' => 'Please enter the user.', 'class' => 'vi-tag-wrap-list rounded-0'), array(
        //     'action' => 'search_users',
        //     'fields' => 'id,username,firstname,lastname,email',
        //     'template' => '<div class="user-block" style="margin:0px; display:inline-block;">
        //                         <img class="img-circle" :src="\'' . $CFG->wwwroot . '/user/pix.php/\'+item.id+\'/f2.jpg\'">
        //                         <span class="username">
        //                             <a>{{ item.firstname }} {{ item.lastname }}</a>
        //                         </span>
        //                         <span class="description">{{ item.email }}</span>
        //                     </div>',
        //     'canaddnew' => false
        // ));

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    function save($data)
    {
        global $DB,$CFG;
        foreach ($data as $key => $value) {
            if(!$DB->record_exists('vi_settings',array('name'=>$key))){
                $nfield = new stdClass();
                $nfield->name = $key;
                $nfield->value = $value;
                $DB->insert_record('vi_settings',$nfield);
            }else{
                $DB->set_field_select('vi_settings', 'value', $value, 'name=:name', array('name' => $key));
                if(strpos($key,'facebookclient')!==false||strpos($key,'googleclient')!==false){
                    //mk oauth2 client id, secret key는 config에 저장 O , 
                    //mk jobs aouth2 는 저장 X
                    if(strpos($key,'2')===false){
                        $DB->set_field_select('config_plugins', 'value', $value, 'name=:name', array('name' => $key));
                        set_config($key, $value, 'auth/googleoauth2');
                    }
                }
            }
        }
    }
}
