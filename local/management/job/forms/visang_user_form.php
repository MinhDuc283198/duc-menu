<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');
require_once($CFG->dirroot . '/user/lib.php');

class visang_user_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $userid = $this->_customdata['userid'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);
        $mform->setDefault('userid', $userid);

        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'username', get_string('username'), array('required', 'class' => 'form-control min-w'));
        $mform->addRule('username', null, 'required', null, 'server');
        $mform->setType('username', PARAM_RAW);

        $pass_desc = '';
        if ($action == 'edit') $pass_desc = 'Enter the password if any changes.';
        $mform->addElement('input', 'password', get_string('password'), array('class' => 'form-control min-w', 'type' => 'password', 'desc' => $pass_desc));
        $mform->setType('password', PARAM_RAW);

        $mform->addElement('input', 'firstname', get_string('firstname'), array('required', 'class' => 'form-control min-w'));
        $mform->addRule('firstname', null, 'required', null, 'server');
        $mform->setType('firstname', PARAM_RAW);

        $mform->addElement('input', 'lastname', get_string('lastname'), array('required', 'class' => 'form-control min-w'));
        $mform->addRule('lastname', null, 'required', null, 'server');
        $mform->setType('lastname', PARAM_RAW);

        $mform->addElement('input', 'email', get_string('email'), array('required', 'class' => 'form-control min-w', 'type' => 'email'));
        $mform->addRule('email', null, 'required', null, 'server');
        $mform->setType('email', PARAM_RAW);

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    public function validation($data, $files)
    {
        global $DB;
        $errors = array();
        if (isset($data['username']) && $DB->count_records_select('user', 'id <> ? AND username = ?', array($data['userid'], $data['username']))) {
            $errors['username'] = 'username already exists';
        }
        if ($data['action'] == 'add' && trim($data['password']) == '') {
            $errors['password'] = get_string('err_required', 'form');
        }
        return $errors;
    }

    function save($data)
    {
        global $DB, $CFG;

        $data->password = trim($data->password);

        $content = new stdClass();
        $content->mnethostid = $CFG->mnet_localhost_id;
        $content->confirmed = 1;
        $content->username = $data->username;
        if ($data->password != '') $content->password = hash_internal_user_password($data->password);
        $content->firstname = $data->firstname;
        $content->lastname = $data->lastname;
        $content->email = $data->email;

        switch ($data->action) {
            case 'add':
                user_create_user($content, false, false);
                break;
            case 'edit':
                $content->id = $data->userid;
                user_update_user($content, false, false);
                break;
        }
    }
}
