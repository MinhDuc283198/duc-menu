<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/CreditManagement/forms/visang_action_point_form.php');


$action = optional_param('action', 'edit', PARAM_ALPHA);
$code_point = optional_param('code_point', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/frm_action_point.php', array('action' => $action, 'code_point' => $code_point, 'returnto' => $returnto));
$returnurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/index.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
// $PAGE->set_url(new moodle_url($baseurl));
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
// $mform = new visang_jmsf_form($baseurl, array('action' => $action, 'skillid' => $skillid, 'returnurl' => $returnurl),'post',array('enctype' => 'multipart/form-data'));
$mform = new visang_action_point_form($baseurl, array('action' => $action, 'code_point' => $code_point, 'returnurl' => $returnurl, 'returnto' => $returnto),'post', '', array('enctype' => 'multipart/form-data'));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_submitted_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

$sql_point_code="
    SELECT *
    FROM {vi_common_code} cc 
    where cc.code= $code_point
";



if ($action === 'add') {
    $title = 'Credit point Management';
} else {
    $skill = $DB->get_record_sql($sql_point_code, array('code' => $code_point));
    if ($skill == null) {
        throw new moodle_exception('idnotfound');
    } else {
        // $skill->expired_date = date("Y-m-d", $skill->expired_date);
        $mform->set_data($skill);
    }
    $title = 'Edit credit point' . ': ' . $skill->name_vi;
}

// ===================================================================================================
// renders
$VISANG->page->title = 'Edit Action point ';
$VISANG->page->heading = $title;
$VISANG->page->menu = ' Edit Action point';
$VISANG->page->navbar[] = 'Edit Action point';
// $VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');
?>






<?php

$VISANG->page->footer();




