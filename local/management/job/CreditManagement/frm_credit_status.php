<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/CreditManagement/forms/visang_creditsts_form.php');


$action = optional_param('action', 'edit', PARAM_ALPHA);
$creditid = optional_param('creditsts_id', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/frm_credit_status.php', array('action' => $action, 'creditsts_id' => $creditid, 'returnto' => $returnto));
$returnurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/index.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
// $PAGE->set_url(new moodle_url($baseurl));
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
// $mform = new visang_jmsf_form($baseurl, array('action' => $action, 'skillid' => $skillid, 'returnurl' => $returnurl),'post',array('enctype' => 'multipart/form-data'));
$mform = new visang_creditsts_form($baseurl, array('action' => $action, 'creditsts_id' => $creditid, 'returnurl' => $returnurl, 'returnto' => $returnto),'post', '', array('enctype' => 'multipart/form-data'));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_submitted_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

$sql_credit_sts="SELECT cre.*,emp.company_name
            FROM {vi_credits}  cre
            JOIN {vi_employers} emp on emp.id=cre.employer_id 
            ";
// $credit_sts=$DB->get_record_sql($sql_credit_sts);
    // var_dump($credit_sts);
    // die();

if ($action === 'add') {
    $title = 'CreditManagement';
} else {
    $skill = $DB->get_record_sql($sql_credit_sts, array('id' => $creditid));
    // var_dump($skill);
    // die();
    if ($skill == null) {
        throw new moodle_exception('idnotfound');
    } else {
        // $skill->expired_date = date("Y-m-d", $skill->expired_date);
        $mform->set_data($skill);
    }
    $title = 'Edit CreditManagement' . ': ' . $skill->company_name;
}

// ===================================================================================================
// renders
$VISANG->page->title = 'Edit CreditManagement';
$VISANG->page->heading = $title;
$VISANG->page->menu = ' CreditManagement';
$VISANG->page->navbar[] = 'CreditManagement';
// $VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');
?>






<?php

$VISANG->page->footer();




