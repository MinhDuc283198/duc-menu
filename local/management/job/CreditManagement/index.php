<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */


require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../lib/credits.php');
$action = optional_param('action', '', PARAM_ALPHA);
$creditid = optional_param('creditid', 0, PARAM_INT);
$creditsts_id = optional_param('creditsts_id', 0, PARAM_INT);
$code_point=optional_param('code_point', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$page2 = optional_param('page2', 1, PARAM_INT);
$perpage2 = optional_param('perpage2', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));

$searchfield = optional_param('searchfield', 'company_name', PARAM_RAW);


$sesskey=sesskey();


$baseurl = $VISANG->wwwroot . '/CreditManagement/index.php';
$formurl_credit_log_employer = $VISANG->wwwroot . '/CreditManagement/detail_employer_credit.php';

$formurl = $VISANG->wwwroot . '/CreditManagement/frmcredit.php';
$formurl_credit_status = $VISANG->wwwroot . '/CreditManagement/frm_credit_status.php';
$formurl_credit_action_point=$VISANG->wwwroot . '/CreditManagement/frm_action_point.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage,
        'page2' => $page2,
    'perpage2' => $perpage2
));
require_login(0, false);
//$context=context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
if ($action == 'delete' && $creditid != 0 && confirm_sesskey()) {
    if (!$DB->record_exists('vi_credit_menu', array('id' => $creditid))) {
        throw new moodle_exception('idnotfound');
    }else {
        $DB->delete_records('vi_credit_menu', array('id' => $creditid));
        redirect($filterurl);
    }
}

// filters
//$filters[] = 'emp.employer_id=:employerid';
$filters = array();
$params = array();
if ($search != '') {
    $filters[] = $DB->sql_like('emp.'.$searchfield, ':' . $searchfield, false);
//    $params = array($searchfield => '%' . $search . '%');
    $params[$searchfield] = '%' . $search . '%';


}
//if ($search != '') {
//    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
//    $params = array($searchfield => '%' . $search . '%');
////    $params[$searchfield] = '%' . $search . '%';
//
//
//}

$offset = ($page - 1) * $perpage;
$offset2 = ($page2 - 1) * $perpage2;
$sql_credit_sts="SELECT cre.*,emp.company_name
            FROM {vi_credits}  cre
            JOIN {vi_employers} emp on emp.id=cre.employer_id 
            " . (count($filters) ? " WHERE " . implode(' AND ', $filters) : "") . "
            ORDER BY cre.timemodified DESC
            LIMIT $perpage2
            OFFSET $offset2
            ";
$credit_sts=$DB->get_records_sql($sql_credit_sts,$params);
$sql_point_code="
    SELECT *
    FROM {vi_common_code} cc 
    where cc.category_id=200 
";
$point_code=$DB->get_records_sql($sql_point_code);


$creditid = $DB->get_records_select('vi_credit_menu', $sql, $params, ' {vi_credit_menu}.timecreated DESC', '*', $offset, $perpage);


$count_sts="
            SELECT
            COUNT(*)
            FROM {vi_credits}  cre
            JOIN {vi_employers} emp on emp.id=cre.employer_id

            ";
$totalcount = $DB->count_records_select('vi_credit_menu', $sql, $params, 'COUNT(*)');
$totalcount_sts=$DB->count_records_sql($count_sts,$params);

// ===================================================================================================
// renders
$title = "Credit management";
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'Credit management';
$VISANG->page->navbar[] = 'Credit management';

$VISANG->page->header();
//col table credit manager
$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table->id = 'credit_list';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    'credit name',
    'Banner Image',
    'Point',
    'Price',
    'Expired At',
);

$table->data = array();
//col table credit status
$table_credit_sts = new html_table();
$table_credit_sts->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table_credit_sts->id = 'credit_list';
$table_credit_sts->attributes['class'] = 'table table-bordered dataTable';
$table_credit_sts->head = array(
    'employer',
    'credit points',
    'Last modified',
//    'Expired at',
    'Create At',

);

$table_manager_point = new html_table();
$table_manager_point->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table_manager_point->id = 'credit_list';
$table_manager_point->attributes['class'] = 'table table-bordered dataTable';
$table_manager_point->head = array(
    'Action credit',
    'Credit points',
);

$table_manager_point->data = array();
foreach ($creditid as $skill) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl, array('action' => 'edit', 'creditid' => $skill->id)),
        'fa fa-pencil-square-o'

    );

    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'creditid' => $skill->id))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';
    $logo = $skill->banner_image_url == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/default_emp.png' : $CFG->wwwroot . '/pluginfile.php/' . $skill->banner_image_url;
    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'creditid' => $skill->id)),
            $skill->credit_name
        ),
       
        '<div class="media" style="text-align: center">
        ' . html_writer::div(html_writer::empty_tag('img', array('src' => $logo, 'class' => 'media-object', 'width' => '60')), 'media-left') . '
        </div>',
        $skill->points,
        $price_format = number_format($skill->price, 0, '', '.').' VND',

//        $skill->price,
//        userdate($skill->expired_date, get_string('strftimedatetimeshort')),
        date("Y-m-d", $skill->expired_date),
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}


foreach ($credit_sts as $skill) {
    $actions = '';
    // edit
     $actions .= $VISANG->page->table_action_button(
         new moodle_url($formurl_credit_log_employer, array('action' => 'view', 'creditid' => $skill->id)),
         'fa fa-eye'

     );
//     $actions .= $VISANG->page->table_action_button(
//        new moodle_url($formurl_credit_status, array('action' => 'edit', 'creditsts_id' => $skill->id)),
//        'fa fa-pencil-square-o'
//
//    );

//     $actions .= '<div class="btn-group">
//       <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//         <i class="fa fa-cog" aria-hidden="true"></i>
//       </button>
//     </div>';
    $row = array(
        html_writer::link(
            new moodle_url($formurl_credit_log_employer, array('action' => 'edit', 'creditid' => $skill->id)),
            $skill->company_name
        ),


//        $skill->company_name,
        $skill->credit_point,
        userdate($skill->timemodified, get_string('strftimedatetimeshort')),
//        userdate($skill->expired_date, get_string('strftimedatetimeshort')),
        userdate($skill->timecreated, get_string('strftimedatetimeshort')),

        html_writer::div($actions, 'd-flex'),
    );

    $table_credit_sts->data[] = $row;
}
/////////////////////////////table point action
$lang = current_language();
if ($lang == 'vi') {
    $code_name_field = 'name_vi';
} elseif ($lang == 'ko') {
    $code_name_field = 'name_ko';
} else {
    $code_name_field = 'name_en';
}
foreach ($point_code as $skill) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl_credit_action_point, array('action' => 'edit', 'code_point' => $skill->code)),
        'fa fa-pencil-square-o'

    );


    $row = array(
        html_writer::link(
            new moodle_url($formurl_credit_action_point, array('action' => 'edit', 'code_point' => $skill->code)),
            $skill->$code_name_field
        ),

        $skill->point,

        html_writer::div($actions, 'd-flex'),
    );

    $table_manager_point->data[] = $row;
}

if (count($table_manager_point->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table_manager_point->head);
    $table_manager_point->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="box table-management">
        <div class="box-body">
            <div class="form-group">
                <div class="pull-left">
                    <?php
                    echo html_writer::link(
                        new moodle_url($formurl, array('action' => 'add')),
                        "Add",
                        array('class' => 'btn btn-primary')
                    );
                    ?>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="form-group " style="text-align: center">
                <?php
                echo html_writer::table($table);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset;
                    $_row->to = min($offset + $perpage, $totalcount);
                    $_row->total = $totalcount;
                    $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <h2>Credit status</h2>
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <div class="pull-right">
                    <form method="get" class="form-inline">
                        <input type="hidden" name="perpage2" value="<?php echo $perpage2; ?>">
<!--                        <input type="hidden" name="company_name" value="company_name">-->
                        <select name="searchfield" class="form-control">
                            <option value="company_name" <?php if ($searchfield == 'company_name') { ?> selected="selected" <?php } ?>>Company name</option></select>

                        <div class="input-group">
                            <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group" >
                <?php
                echo html_writer::table($table_credit_sts);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset2;
                    $_row->to = min($offset2 + $perpage2, $totalcount_sts);
                    $_row->total = $totalcount_sts;
                    $VISANG->page->table_perpage($perpage2, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount_sts / $perpage2), $page2);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <h2>Point credit management</h2>
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <div class="clearfix"></div>
            </div>
            <div class="form-group" >
                <?php
                echo html_writer::table($table_manager_point);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset2;
                    $_row->to = min($offset2 + $perpage2, $totalcount_sts);
                    $_row->total = $totalcount_sts;
                    $VISANG->page->table_perpage($perpage2, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    table_pagination2($filterurl, null, ceil($totalcount_sts / $perpage2), $page2);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>


<style>
    .table-management .c3{
        text-align: right;
    }
    .table-management .c0,.c1,.c2,.c4,.c5{
        text-align: center;
    }
    .table-management .media-left {
        padding: 0 0 0 25px;
    }
</style>
<?php


$VISANG->page->footer();
