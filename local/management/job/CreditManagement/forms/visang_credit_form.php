<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');

// require_once($VISANG->dirroot . './local/management/job/classes/formslib.php');
// require_once($CFG->libdir . './lib/formslib.php');


class visang_credit_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $creditid = $this->_customdata['creditid'];
        $returnurl = $this->_customdata['returnurl'];
        $returnto = $this->_customdata['returnto'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'creditid');
        $mform->setType('creditid', PARAM_INT);
        $mform->setDefault('creditid', $creditid);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);
        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'credit_name', get_string('name'), array('required', 'class' => 'form-control'));
        $mform->addRule('credit_name', null, 'required', null, 'server');
        $mform->setType('credit_name', PARAM_RAW);

        $mform->addElement('input', 'points', 'point', array( 'min' => 1, 'required','value'=>'0'));
        $mform->setType('points', PARAM_RAW);

        $mform->addElement('input', 'unit_price', 'unit price', array( 'required','value'=>'0'));

      
        $mform->addElement('input', 'price', 'price', array( 'required','onkeyup'=>'inputNumberFormat(this)'));
        $mform->setType('credit_name', PARAM_RAW);


        // $age_group = array();
        // $age_group[] = $mform->createElement('input', 'price', 'price', array('type' => 'number', 'required','readonly'));
        // $age_group[] =  &$mform->createElement('xbutton', 'submitbutton','price', array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        // $mform->addGroup($age_group, 'price_group', 'price', array(' '), false);

        $mform->addElement('input', 'expired_date', 'expired_date', array('required', 'type' => 'date', 'class' => 'form-control expired_date'));
        $mform->addRule('expired_date', null, 'required', null, 'server');
        $mform->setType('expired_date', PARAM_RAW);
        
        $mform->addElement('advfile', 'banner_image_url', 'banner_image_url', null, array('maxfiles' => 1, 'accepted_types' => 'image/*;capture=camera'));
        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);


    }

    function save($data)
    {
        global $DB;
        $logo = $this->_form->getElement('banner_image_url')->save_file($data->banner_image_url);

        $content = new stdClass();
        $content->credit_name = $data->credit_name;
        $content->banner_image_url = count($logo) ? $logo[0] : '';
        $content->points=$data->points;
        $content->price=str_replace(',', '', $data->price);
        $content->expired_date = strtotime($data->expired_date);

        $content->timemodified = time();
        // echo '<pre>';
        // var_dump($content);
        // echo '</pre>';
        // die();


        switch ($data->action) {
            case 'add':

                $content->timecreated = time();
                $DB->insert_record('vi_credit_menu', $content);
                break;
            case 'edit':
                $content->id = $data->creditid;
                $DB->update_record('vi_credit_menu', $content);
                break;
        }
    }
}