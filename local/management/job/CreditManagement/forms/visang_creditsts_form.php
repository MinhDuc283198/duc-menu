<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');

// require_once($VISANG->dirroot . './local/management/job/classes/formslib.php');
// require_once($CFG->libdir . './lib/formslib.php');


class visang_creditsts_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $creditid = $this->_customdata['creditsts_id'];
        $returnurl = $this->_customdata['returnurl'];
        $returnto = $this->_customdata['returnto'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'creditid');
        $mform->setType('creditsts_id', PARAM_INT);
        $mform->setDefault('creditid', $creditid);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);
        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));
        $mform->addElement('input', 'credit_point', 'Credit points', array('class' => 'form-control min-w', 'type' => 'number', 'min' => 0));
        $mform->setType('credit_point', PARAM_INT);
        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);


    }

    function save($data)
    {
        global $DB;

        $content = new stdClass();

        $content->credit_point=$data->credit_point;

        // echo '<pre>';
        // var_dump($content);
        // echo '</pre>';
        // die();


        switch ($data->action) {
            // case 'add':

            //     $content->timecreated = time();
            //     $DB->insert_record('vi_credit_menu', $content);
            //     break;
            case 'edit':
                $content->id = $data->creditid;
                $DB->update_record('vi_credits', $content);
                break;
        }
    }
}