<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');

// require_once($VISANG->dirroot . './local/management/job/classes/formslib.php');
// require_once($CFG->libdir . './lib/formslib.php');


class visang_action_point_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $code_point = $this->_customdata['code_point'];
        $returnurl = $this->_customdata['returnurl'];
        $returnto = $this->_customdata['returnto'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'code_point');
        $mform->setType('code_point', PARAM_INT);
        $mform->setDefault('code_point', $code_point);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);
        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));
        $mform->addElement('input', 'point', 'Point', array('class' => 'form-control min-w', 'type' => 'number', 'min' => 0));
        $mform->setType('point', PARAM_INT);
        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);


    }

    function save($data)
    {
        global $DB;

        $content = new stdClass();

        $content->point=$data->point;

        // echo '<pre>';
        // var_dump($content);
        // echo '</pre>';
        // die();


        switch ($data->action) {

            case 'edit':
                $content->code = $data->code_point;
                $sql="UPDATE {vi_common_code} com
                        SET com.point = $data->point
                        WHERE com.code=$data->code_point";
                $DB->execute($sql);
                break;
        }
    }
}