<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$action = optional_param('action', '', PARAM_ALPHA);
$creditid = optional_param('creditid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
//$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
//$searchfield = optional_param('searchfield', 'username', PARAM_RAW);
$returnto = optional_param('returnto', '', PARAM_ALPHA);



$sesskey = sesskey();
$returnurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/index.php');


// $formurl = $VISANG->wwwroot . '/jmsf/frmjmsf.php';
$baseurl = $VISANG->wwwroot . '/CreditManagement/detail_employer_credit.php';
// $formurl = $VISANG->wwwroot . '/skills/frmskill.php';


$filterurl = new moodle_url($baseurl, array(
//    'search' => $search,
//    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
//if ($action == 'delete' && $skillid != 0 && confirm_sesskey()) {
//    if (!$DB->record_exists('vi_job_feedback', array('id' => $skillid))) {
//        throw new moodle_exception('idnotfound');
//    } else {
//        $DB->delete_records('vi_job_feedback', array('id' => $skillid));
//        redirect($filterurl);
//    }
//}

// filters
$sql = '';
$params = array();
//if ($search != '') {
//    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
//    $params = array($searchfield => '%' . $search . '%');
//
//}
$offset = ($page - 1) * $perpage;
//$sql_cre="
//            SELECT
//            *
//            FROM {vi_credits_logs}  crl
//
//            WHERE crl.member_credit_id=$creditid
//            ORDER BY crl.timelogged DESC
//            ";
//$employer_credit=$DB->get_records_sql($sql_cre,$params);
$employer_credit = $DB->get_records_select('vi_credits_logs', $sql, $params, ' {vi_credits_logs}.timelogged DESC', '*', $offset, $perpage);
//$count="
//            SELECT
//            COUNT(*)
//            FROM {vi_credits_logs}  crl
//            WHERE  crl.member_credit_id=$creditid
//
//            ";
//$totalcount=$DB->count_records_sql($count,$params);
$totalcount = $DB->count_records_select('vi_credits_logs', $sql, $params, 'COUNT(*)');
// ===================================================================================================
// renders
$title = 'Employer credit';
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'Credit managerment';
$VISANG->page->navbar[] = 'Credit managerment';

$VISANG->page->header();

$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table->id = 'skills_list';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    'transaction type',
    'withdraw type',
    'Payment type',
    'point',
    'price',
    'Transaction Date',
);
$lang = current_language();
if ($lang == 'vi') {
    $code_name_field = 'name_vi';
} elseif ($lang == 'ko') {
    $code_name_field = 'name_ko';
} else {
    $code_name_field = 'name_en';
}
$table->data = array();

foreach ($employer_credit as $skill) {
//    $actions = '';
    // edit
//    $actions .= $VISANG->page->table_action_button(
//        new moodle_url($formurl, array('action' => 'edit', 'skillid' => $skill->id)),
//        'fa fa-pencil-square-o'
//
//    );

//    $actions .= '<div class="btn-group">
//      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//        <i class="fa fa-cog" aria-hidden="true"></i>
//      </button>
//      <ul class="dropdown-menu dropdown-menu-right">
//        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'skillid' => $skill->id))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
//      </ul>
//    </div>';
//    $logo = $skill->user_avatar_url == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/default_emp.png' : $CFG->wwwroot . '/pluginfile.php/' . $skill->user_avatar_url . '?preview=thumb';
    $row = array(
//        html_writer::link(
//            new moodle_url($formurl, array('action' => 'edit', 'skillid' => $skill->id)),
//            $skill->username
//        ),
        $skill->code_name_transaction = $DB->get_field('vi_common_code', $code_name_field, array('code' => $skill->transaction_code)),
        $skill->code_name_withdraw = $DB->get_field('vi_common_code', $code_name_field, array('code' => $skill->withdraw_code)),
        $skill->code_name_payment = $DB->get_field('vi_common_code', $code_name_field, array('code' => $skill->pg_code)),
        $skill->point,
        $price_format = number_format($skill->price, 0, '', '.').' VND',
        userdate($skill->timelogged, get_string('strftimedatetimeshort')),
//        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <div class="pull-left">


                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <?php
                    echo html_writer::table($table);
                    ?>
                </div>
                <div>
                    <div class="pull-left">
                        <?php
                        $_row = new stdClass();
                        $_row->from = $offset;
                        $_row->to = min($offset + $perpage, $totalcount);
                        $_row->total = $totalcount;
                        $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                        ?>
                    </div>
                    <div class="pull-right">
                        <?php
                        $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div style="text-align: center">
                        <?php
                        echo html_writer::link(
                            new moodle_url($returnurl),
                            "Back",
                            array('class' => 'btn btn-primary')
                        );
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php
$VISANG->page->footer();
