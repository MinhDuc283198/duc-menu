<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/CreditManagement/forms/visang_credit_form.php');


$action = optional_param('action', 'add', PARAM_ALPHA);
$creditid = optional_param('creditid', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}
require_login(0, false);
$baseurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/frmcredit.php', array('action' => $action, 'creditid' => $creditid, 'returnto' => $returnto));
$returnurl = new moodle_url($VISANG->wwwroot . '/CreditManagement/index.php');
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
// $PAGE->set_url(new moodle_url($baseurl));
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
// $mform = new visang_jmsf_form($baseurl, array('action' => $action, 'skillid' => $skillid, 'returnurl' => $returnurl),'post',array('enctype' => 'multipart/form-data'));
$mform = new visang_credit_form($baseurl, array('action' => $action, 'creditid' => $creditid, 'returnurl' => $returnurl, 'returnto' => $returnto),'post', '', array('enctype' => 'multipart/form-data'));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_submitted_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = 'CreditManagement';
} else {
    $skill = $DB->get_record('vi_credit_menu', array('id' => $creditid));
    if ($skill == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $skill->expired_date = date("Y-m-d", $skill->expired_date);
        $mform->set_data($skill);
    }
    $title = 'Edit CreditManagement' . ': ' . $skill->credit_name;
}

// ===================================================================================================
// renders
$VISANG->page->title = 'Edit CreditManagement';
$VISANG->page->heading = $title;
$VISANG->page->menu = ' CreditManagement';
$VISANG->page->navbar[] = 'CreditManagement';
// $VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');
?>
<style>
    input.form-control.expired_date {
        width: 50%;
    }
</style>

<script>
  $( 'input[name=points]').keyup(function() {
      $(function(){
            var  a= $('input[name=points]').val();
        var b=$('input[name=unit_price]').val();
             
        var c= a*b;
        $('input[name=price]').val(c);
    });
    });
    
    $( 'input[name=unit_price]').keyup(function() {
      $(function(){
            var  a= $('input[name=points]').val();
        var b=$('input[name=unit_price]').val();
             
        var c= a*b;
          $( 'input[name=price]').val(c);

    

    });
    });
      function inputNumberFormat(obj) {
          obj.value = comma(uncomma(obj.value));
      }

      function comma(str) {
          str = String(str);
          return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
      }

      function uncomma(str) {
          str = String(str);
          return str.replace(/[^\d]+/g, '');
      }

</script>

<?php

$VISANG->page->footer();




