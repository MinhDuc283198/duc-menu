<?php

namespace local_visang;

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */
require_once($CFG->libdir . '/formslib.php');
require_once("HTML/QuickForm.php");
require_once("HTML/QuickForm/text.php");
require_once("HTML/QuickForm/group.php");
// require_once("HTML/QuickForm/file.php");

class VisangQuickForm_formactions extends \HTML_QuickForm_group
{
    public function __construct($elementName = null, $elementLabel = null, $elements = null, $separator = null, $appendName = true)
    {
        parent::__construct($elementName, $elementLabel);
        $this->_type = 'formactions';
        if (isset($elements) && is_array($elements)) {
            $this->setElements($elements);
        }
        if (isset($separator)) {
            $this->_separator = $separator;
        }
        if (isset($appendName)) {
            $this->_appendName = $appendName;
        }
    }

    public function toHtml()
    {
        $html = '';
        foreach ($this->_elements as $element) {
            $html .= $element->toHtml();
        }
        return \html_writer::div($html, 'text-center');
    }
}

class VisangQuickForm_input extends \HTML_QuickForm_input
{
    public function __construct($elementName = null, $elementLabel = null, $attributes = null)
    {
        parent::__construct($elementName, $elementLabel, $attributes);
        $attributes = (array) $attributes;
        $type = array_key_exists('type', $attributes) ? $attributes['type'] : 'text';
        $this->setType($type);
    }
}

class VisangQuickForm_checklist extends \HTML_QuickForm_input
{
    protected $items;   // key => value
    protected $item_value;
    protected $item_type;

    public function __construct($elementName = null, $elementLabel = null, $items = null, $attributes = null)
    {

        $this->_type = 'checklist';
        $this->items = (array) $items;
        $attributes = (array) $attributes;
        $this->item_type = array_key_exists('type', $attributes) && $attributes['type'] == 'radio' ? $attributes['type'] : 'checkbox';
        if ($this->item_type == 'checkbox') $this->item_value = array();
        else $this->item_value = '';
        parent::__construct($elementName, $elementLabel, $attributes);
    }

    public function getValue()
    {
        if ($this->item_type == 'checkbox') {
            return optional_param_array($this->getName(), array(), PARAM_RAW);
        } else return optional_param($this->getName(), '', PARAM_RAW);
    }

    public function setValue($value)
    {
        if ($this->item_type == 'checkbox') $this->item_value = (array) $value;
        else $this->item_value = $value;
    }

    public function toHtml()
    {
        $name = $this->getName();
        $container_attrs = array('id' => 'checklist_' . $name, 'name' => 'checklist_' . $name);
        if (isset($this->_attributes['class'])) {
            $container_attrs['class'] = $this->_attributes['class'];
        }
        $html = \html_writer::start_tag('div', $container_attrs);
        if ($this->item_type == 'checkbox') {
            $name .= '[]';
        }
        if (count($this->items)) {
            foreach ($this->items as $key => $value) {
                $attrs = array('type' => $this->item_type, 'name' => $name, 'value' => $key);
                if (($this->item_type == 'checkbox' && in_array($key, (array) $this->item_value)) || $key == $this->item_value) {
                    $attrs['checked'] = 'checked';
                }
                $html .= '<div>
                            <div class="' . $this->item_type . '">
                                <label>' . \html_writer::empty_tag('input', $attrs) . ' ' . $value . '</label>
                            </div>
                        </div>';
            }
        } else {
            $html .= \html_writer::tag('i', '(' . get_string('empty:data', 'local_visang') . '..)', array('class' => 'text-muted'));
        }
        $html .= \html_writer::end_tag('div');
        return $html;
    }
}

class VisangQuickForm_adveditor extends \HTML_QuickForm_input
{
    protected $editor_type;

    public function __construct($elementName = null, $elementLabel = null, $editor_type = 'wysihtml5', $attributes = null)
    {
        global $CFG, $VISANG;

        $this->_type = 'adveditor';
        $this->editor_type = in_array($editor_type, array('wysihtml5')) ? $editor_type : 'wysihtml5';
        parent::__construct($elementName, $elementLabel, $attributes);

        switch ($this->editor_type) {
            default:
                $VISANG->page->addCSS($CFG->wwwroot . '/local/management/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
                $VISANG->page->addJS($CFG->wwwroot . '/local/management/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
                break;
        }
    }

    private function getElementId()
    {
        return '__' . $this->editor_type . '_' . $this->getName();
    }

    public function toHtml()
    {
        global $VISANG;

        $html = '';
        switch ($this->editor_type) {
            default:
                $html .= \html_writer::tag('textarea', $this->getValue(), array('id' => $this->getElementId(), 'name' => $this->getName(), 'class' => 'form-control'));

                $VISANG->page->addScript('$(function () {
                    $("#' . $this->getElementId() . '").wysihtml5(); 
                });');
                break;
        }
        return $html;
    }
}

class VisangQuickForm_advfile extends \HTML_QuickForm_input
{
    protected $_filedata = array('itemid' => 0, 'files' => array(), 'upload' => array(), 'del' => array());
    protected $_options    = array('maxbytes' => -1, 'maxfiles' => -1, 'accepted_types' => '*');

    public function __construct($elementName = null, $elementLabel = null, $attributes = null, $options = null)
    {
        global $PAGE, $CFG, $VISANG;

        $options = (array) $options;
        foreach ($options as $name => $value) {
            if (array_key_exists($name, $this->_options)) {
                $this->_options[$name] = $value;
            }
        }
        if (!isset($options['maxbytes'])) {
            $this->_options['maxbytes'] = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        }
        $this->_type = 'advfile';
        parent::__construct($elementName, $elementLabel, $attributes);

        $VISANG->page->addJS($VISANG->wwwroot . '/js/vue.min.js');
    }

    private function getElementId()
    {
        return '__advfile_' . $this->getName();
    }

    public function filesizeToText($bytes, $decimals = 0)
    {
        // $sz = 'BKMGTP';
        $sz = ' KMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor] . 'B';
    }

    function setValue($data)
    {
        if (is_array($data) || $data != '') {
            $this->_filedata['files'] = (array) $data;
            if (count($this->_filedata['files'])) {
                $this->_filedata['itemid'] = explode('/', $this->_filedata['files'][0])[3];
            }
        }
    }

    function exportValue(&$submitValues, $assoc = false)
    {
        $value = array(
            'itemid' => optional_param($this->getElementId() . '_itemid', file_get_unused_draft_itemid(), PARAM_INT),
            'upload' => $this->get_submitted_files(),
            'files' => optional_param_array($this->getElementId() . '_files', array(), PARAM_RAW),
            'del' => optional_param_array($this->getElementId() . '_del', array(), PARAM_RAW)
        );

        $this->_filedata = $value;
        return $this->_prepareValue($value, $assoc);
    }

    private function get_submitted_files()
    {
        $files = array();
        $elementName = $this->getElementId() . '_upload';
        if (!empty($_FILES) && isset($_FILES[$elementName])) {
            foreach ($_FILES[$elementName]['name'] as $idx => $filename) {
                $size = $_FILES[$elementName]['size'][$idx];
                if ($_FILES[$elementName]['error'][$idx] == 0 && $filename != '' && $size > 0) {
                    if ($this->_options['maxbytes'] != -1 && $size > $this->_options['maxbytes']) {
                        continue;
                    }
                    $file = new \stdClass();
                    $file->name = $filename;
                    $file->type = $_FILES[$elementName]['type'][$idx];
                    $file->tmp_name = $_FILES[$elementName]['tmp_name'][$idx];
                    $file->size = $size;

                    $files[] = $file;
                }
            }

            unset($_FILES[$elementName]);
        }

        if (count($files)) {
            return $files;
        } else {
            return null;
        }
    }

    public function save_file($data, $component = 'local_visang', $filearea = 'images')
    {
        global $DB, $USER;
        $context = \context_system::instance();
        $fs = get_file_storage();

        $itemid = $data['itemid'];

        if (!is_null($data['upload'])) {
            if ($this->_options['maxfiles'] == 1) {
                $files = $fs->get_area_files($context->id, $component, $filearea, $itemid, 'timemodified', false);
                foreach ($files as $f) {
                    if ($f) {
                        $f->delete();
                    }
                }
            }

            foreach ($data['upload'] as $fileupload) {
                $existfile = $fs->get_file($context->id, $component, $filearea, $itemid, '/', $fileupload->name);
                if (!$existfile) {
                    $file_record = array(
                        'contextid' => $context->id,
                        'component' => $component,
                        'filearea' => $filearea,
                        'itemid' => $itemid,
                        'filepath' => '/',
                        'filename' => $fileupload->name,
                        'timecreated' => time(),
                        'timemodified' => time(),
                        'userid' => $USER->id
                    );
                    $fs->create_file_from_pathname($file_record, $fileupload->tmp_name);
                }
            }
        }

        $file_paths = array();
        $files = $fs->get_area_files($context->id, $component, $filearea, $itemid, 'timemodified', false);
        foreach ($files as $f) {
            if ($f) {
                $path = implode('/', array($f->get_contextid(), $f->get_component(), $f->get_filearea(), $f->get_itemid(), $f->get_filename()));

                if (in_array($path, $data['del'])) {
                    $f->delete();
                } else {
                    $file_paths[] = $path;
                }
            }
        }

        return $file_paths;
    }

    public function toHtml()
    {
        global $VISANG;

        $elid = $this->getElementId();
        $html = '';
        $html .= '<div class="hidden">
                    ' . ($this->_filedata['itemid'] != 0 ? '<input type="hidden" name="' . $elid . '_itemid" value="' . $this->_filedata['itemid'] . '">' : '') . '
                    <input type="hidden" v-for="path in delfiles" :key="path" name="' . $elid . '_del[]" :value="path">
                </div>';
        $attrs = array(
            'type' => 'file',
            'accept' => $this->_options['accepted_types'],
            'name' => $elid . '_upload[]',
            'id' => $elid . '_upload'
        );
        if ($this->_options['maxfiles'] != 1) {
            $attrs['multiple'] = 'multiple';
        }
        $html .= \html_writer::div(
            \html_writer::tag('i', '', array('class' => 'fa fa-paperclip'))
                . ' ' . get_string('attachment', 'local_visang')
                . \html_writer::empty_tag('input', $attrs),
            'btn btn-default btn-file',
            array('style' => 'margin-right: 5px;')
        );

        if ($this->_options['maxbytes'] != -1) {
            $html .= '<p class="help-block text-black">Max. ' . $this->filesizeToText($this->_options['maxbytes']) . '</p>';
        }

        $html .= '<div><span class="vi-tag" v-for="file in inputfiles" :key="file.name">{{ file.name }}</span></div>';

        $list = '';

        foreach ($this->_filedata['files'] as $key => $path) {
            $f = explode('/', $path);
            $url = new \moodle_url('/pluginfile.php/' . $path);
            $list .= '<li id="' . ($elid . '_file_' . $key) . '" data-path="' . $path . '">
                        <span class="mailbox-attachment-icon ' . ($f[2] == 'images' ? 'has-img' : '') . '">
                        ' . ($f[2] == 'images' ? '<img style="height: 150px;" src="' . $url . '">' : '<i class="fa fa-file-o"></i>') . '
                        </span>

                        <div class="mailbox-attachment-info vi-text-truncate">
                            <a target="_blank" href="' . $url . '" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> ' . $f[4] . '</a>
                            <div class="mailbox-attachment-size">
                                &nbsp;
                                <div class="btn btn-danger btn-xs pull-right" @click="delfile(\'' . $elid . '_file_' . $key . '\')"><i class="fa fa-trash-o"></i> ' . get_string('delete') . '</div>
                            </div>
                        </div>
                    </li>';
        }
        $html .= \html_writer::tag('ul', $list, array('class' => 'mailbox-attachments clearfix'));
        // $html = \html_writer::div($html, $elid, array('id' => $elid));
        $html = '<div class="' . $elid . '" id="' . $elid . '" v-cloak>' . $html . '</div>';


        $VISANG->page->addScript('function VisangQuickForm_advfile_mixin() {
            return {
                data: {
                    elid: "_VisangQuickForm_advfile",
                    inputfiles: [],
                    delfiles: [],
                    limit: -1,
                },
                methods: {
                    delfile: function(itemid) {
                        var path = $("#" + itemid).data("path");
                        if (path != undefined && this.delfiles.indexOf(path) == -1) {
                            this.delfiles.push(path);
                            $("#" + itemid).remove();
                        }
                    }
                },
                mounted: function() {
                    var self = this;
                    $("#"+self.elid+"_upload").change(function() {
                        var files = this.files;
                        self.inputfiles = [];
                        if(self.limit != -1 && files.length > self.limit) {
                            alert("You are only allowed to upload a maximum: " + self.limit);
                            $(this).val("");
                        } else {
                            for (var i = 0; i < files.length; i++) {
                                self.inputfiles.push({
                                    name: files[i].name
                                });
                            }
                        } 
                    });
                }
            };
        }');
        $VISANG->page->addScript('$(function() {
            new Vue({
                el: "#' . $elid . '",
                mixins: [VisangQuickForm_advfile_mixin()],
                data: {
                    elid: "' . $elid . '",
                    limit: parseInt(' . $this->_options['maxfiles'] . '),
                }
            });
        });');

        return $html;
    }
}

class VisangQuickForm_advautocomplete extends \HTML_QuickForm_input
{
    protected $_data = array();
    protected $_options = array(
        'url' => '',
        'action' => '',
        'fields' => '*',
        'template' => '{{ item }}',
        'maxitems' => -1,
        'canaddnew' => true
    );

    public function __construct($elementName = null, $elementLabel = null, $attributes = null, $options = null)
    {
        global $VISANG, $CFG;

        $options = (array) $options;
        $this->_options['url'] = $VISANG->wwwroot . '/ajax.php';
        foreach ($options as $name => $value) {
            if (array_key_exists($name, $this->_options)) {
                $this->_options[$name] = $value;
            }
        }

        $this->_type = 'advautocomplete';
        parent::__construct($elementName, $elementLabel, $attributes);
        $VISANG->page->addJS($VISANG->wwwroot . '/js/vue.min.js');
    }

    private function getElementId()
    {
        return '__advautocomplete_' . $this->getName();
    }

    function setValue($data)
    {
        $this->_data = (array) $data;
    }

    function exportValue(&$submitValues, $assoc = false)
    {
        $value = optional_param_array($this->getElementId() . '_items', array(), PARAM_RAW);

        $this->_data = $value;
        return $this->_prepareValue($value, $assoc);
    }

    public function toHtml()
    {
        global $VISANG;

        $elid = $this->getElementId();
        $html = '<div id="' . $elid . '" v-cloak>
                    <div style="position:relative" v-bind:class="{\'open\':openSuggestion}">
                        <input class="form-control min-w" type="text" v-model="inputval" @keydown.enter.prevent="enter" @keydown.down="down" @keydown.up="up" @input="change" />
                        <ul class="dropdown-menu min-w" style="width:100%">
                            <li v-for="(item, index) in matches" v-bind:class="{\'active\': isActive(index)}" @click="suggestionClick(index)">
                                <div>' . $this->_options['template'] . '</div>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div v-if="maxitems >= 0">(Limit: {{maxitems}})</div>
                        <div class="vi-tag-wrap" v-for="(item,i) in items" :key="i">
                            <div class="vi-tag"><div>' . $this->_options['template'] . '</div> <div><i class="fa fa-times-circle close-btn" @click="removeItem(i)" aria-hidden="true"></i></div></div>
                        </div>
                    </div>
                    <div class="hidden">
                        <input type="hidden" v-for="(item,i) in items" :key="i" name="' . $elid . '_items[]" :value="JSON.stringify(item)">
                    </div>
                </div>';

        $VISANG->page->addScript('function VisangQuickForm_advautocomplete_mixin() {
            return {
                data: {
                    elid: "_VisangQuickForm_advautocomplete",
            
                    inputval: "",
                    open: false,
                    current: 0,
                    items: [],
                    matches: [],
                    onfetch: false,
                    maxitems: -1,
                    canaddnew: true,
                    
                    url: "",
                    action: "",
                    sesskey: "",
                    fields: "*"
                },
                computed: {
                    openSuggestion: function() {
                        return this.inputval !== "" &&
                            this.matches.length != 0 &&
                            this.open === true;
                    }
                },
                methods: {
                    fetch: function() {
                        if (this.inputval != "" && !this.onfetch) {
                            this.onfetch = true;
                            $.post(this.url, {
                                action: this.action,
                                sesskey: this.sesskey,
                                search: this.inputval,
                                fields: this.fields
                            }, function(data) {
                                this.onfetch = false;
                                if (data.data) {
                                    this.matches = data.data;
                                    this.current = 0;
                                }
                            }.bind(this));
                        }
                    },
                    addItem: function(item) {
                        if(this.maxitems >= 0 && this.items.length == this.maxitems) return;
                        if (item == undefined && this.canaddnew) {
                            item = {
                                id: "",
                                name: this.inputval.trim()
                            };
                        }
                        var isexist = this.items.filter(_item => {
                            return JSON.stringify(_item) == JSON.stringify(item);
                        });
                        if (item != undefined && isexist.length == 0) {
                            this.items.push(item);
                        }
                        this.inputval = "";
                        this.open = false;
                    },
                    removeItem: function(index) {
                        this.items.splice(index, 1);
                    },
                    enter: function() {
                        if (this.inputval.trim() != "") this.addItem(this.matches[this.current]);
                    },
                    up: function() {
                        this.current = Math.max(0, this.current - 1);
                    },
                    down: function() {
                        this.current = Math.max(0, Math.min(this.matches.length - 1, this.current + 1));
                    },
                    isActive: function(index) {
                        return index === this.current;
                    },
                    change: function() {
                        this.fetch();
                        if (this.open == false) {
                            this.open = true;
                            this.current = 0;
                        }
                    },
                    suggestionClick: function(index) {
                        this.addItem(this.matches[index]);
                    },
                }
            };
        }');
        $VISANG->page->addScript('$(function() {
            new Vue({
                el: "#' . $elid . '",
                mixins: [VisangQuickForm_advautocomplete_mixin()],
                data: {
                    elid: "' . $elid . '",
                    items: [' . implode(',', $this->_data) . '],
                    maxitems: parseInt(' . intval($this->_options['maxitems']) . '),
                    canaddnew: ' . ($this->_options['canaddnew'] ? 'true' : 'false') . ',
                    
                    url: "' . $this->_options['url'] . '",
                    action: "' . $this->_options['action'] . '",
                    sesskey: "' . sesskey() . '",
                    fields: "' . $this->_options['fields'] . '"
                }
            });
        });');
        return \html_writer::div($html, isset($this->_attributes['class']) ? $this->_attributes['class'] : '');
    }
}

class VisangQuickForm_customfield extends \HTML_QuickForm_input
{
    protected $_template;

    public function __construct($elementName = null, $elementLabel = null, $attributes = null, $template = null)
    {
        $this->_template = $template;
        $this->_type = 'customfield';
        parent::__construct($elementName, $elementLabel, $attributes);
    }

    public function setTemplate($template)
    {
        $this->_template = $template;
    }

    public function getTemplate()
    {
        return $this->_template;
    }

    public function toHtml()
    {
        return $this->_template;
    }
}

\MoodleQuickForm::registerElementType('formactions', __FILE__, '\local_visang\VisangQuickForm_formactions');
\MoodleQuickForm::registerElementType('input', __FILE__, '\local_visang\VisangQuickForm_input');
\MoodleQuickForm::registerElementType('checklist', __FILE__, '\local_visang\VisangQuickForm_checklist');
\MoodleQuickForm::registerElementType('adveditor', __FILE__, '\local_visang\VisangQuickForm_adveditor');
\MoodleQuickForm::registerElementType('advfile', __FILE__, '\local_visang\VisangQuickForm_advfile');
\MoodleQuickForm::registerElementType('advautocomplete', __FILE__, '\local_visang\VisangQuickForm_advautocomplete');
\MoodleQuickForm::registerElementType('customfield', __FILE__, '\local_visang\VisangQuickForm_customfield');

// =========================================================================================================================
class form_renderer
{
    // protected $_form;
    public $_form;

    public function __construct(&$mform)
    {
        $this->_form = &$mform;
    }

    public function toArray()
    {
        $hiddens = array();
        $groups = array();
        $current_group = &$groups;
        foreach ($this->_form->_elements as $element) {
            if ($element->_type == 'hidden') {
                $hiddens[] = $element;
            } else if ($element->_type == 'header') {
                $header = $element;
                $header->_elements = array();
                $current_group = &$header->_elements;
                $groups[] = $header;
            } else if ($element->_type == 'formactions') {
                $groups[] = $element;
                $current_group = &$groups;
            } else {
                $current_group[] = $element;
            }
        }
        foreach ($hiddens as $hidden) {
            $groups[] = $hidden;
        }
        return $groups;
    }

    public function has_error($element)
    {
        return isset($this->_form->_errors[$element]);
    }

    public function display()
    {
        echo $this->render();
    }

    public function render()
    {
        return $this->start_form()
            . $this->render_elements($this->toArray())
            . $this->end_form();
    }

    public function start_form()
    {
        return \html_writer::start_tag('form', $this->_form->_attributes);
    }

    public function end_form()
    {
        return \html_writer::end_tag('form');
    }

    public function render_elements($elements = array())
    {
        $html = '';
        foreach ($elements as $element) {
            switch ($element->_type) {
                case 'header':
                    $html .= $this->render_header($element);
                    break;
                case 'hidden':
                    $html .= $element->toHtml();
                    break;
                case 'formactions':
                    $html .= \html_writer::div($this->_form->_requiredNote, 'form-group');
                    $html .= $element->toHtml();
                    break;
                default:
                    $el = $element->toHtml();
                    if ($this->has_error($element->getName())) {
                        $el = \html_writer::div(
                            $el . \html_writer::div($this->_form->_errors[$element->getName()], 'help-block'),
                            'form-group has-error no-margin'
                        );
                    }
                    $html .= $this->render_fitem($this->get_label($element), $el);
                    break;
            }
            $html .= ' ';
        }
        return $html;
    }

    public function get_label($element)
    {
        $label = \html_writer::tag('label', $element->getLabel());
        if (isset($element->_attributes['required'])) {
            $label .= \html_writer::tag('span', '*', array('class' => 'text-red'));
        }
        if (isset($element->_attributes['desc'])) {
            $label .= \html_writer::div($element->_attributes['desc'], 'fdesc text-muted');
        }
        return $label;
    }

    public function render_fitem($fitemtitle = '', $felement = '')
    {
        return '<div class="vi-fitem">
                    <div class="vi-fitemtitle">' . $fitemtitle . '</div>
                    <div class="vi-felement">' . $felement . '</div>
                </div>';
    }

    public function render_header($element)
    {
        $body = '';
        if (isset($element->_elements)) {
            $body .= $this->render_elements($element->_elements);
        }

        $html = '<div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">' . $element->_text . '</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <div class="vi-form">' . $body . '</div>
                    </div>
                </div>';
        return $html;
    }
}
