<?php

namespace local_visang;

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

class page
{
    public $title = '';
    public $heading = '';
    public $subheading = '';
    public $menu = '';
    public $css = array();
    public $js = array();
    public $styles = array();
    public $scripts = array();
    public $navbar = array();

    function addJS($js)
    {
        if (!in_array($js, $this->js)) $this->js[] = $js;
    }

    function addCSS($css)
    {
        if (!in_array($css, $this->css)) $this->css[] = $css;
    }

    function addStyle($style)
    {
        if (!in_array($style, $this->styles)) $this->styles[] = $style;
    }

    function addScript($script)
    {
        if (!in_array($script, $this->scripts)) $this->scripts[] = $script;
    }

    function addNavbar($navbar)
    {
        if (!in_array($navbar, $this->navbar)) $this->navbar[] = $navbar;
    }

    function header()
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/header.php');
    }

    function footer()
    {
        global $VISANG, $USER;
        include_once($VISANG->dirroot . '/footer.php');
    }

    function table_action_button($url, $icon, $text, $class = 'btn btn-xs btn-default')
    {
        return \html_writer::link(
            $url,
            \html_writer::tag('i', '', array('class' => $icon, 'aria-hidden' => 'true')) . ' ' . $text,
            array('class' => $class, 'role' => 'button')
        );
    }

    function table_perpage($perpage, $label, $params = array())
    {
        echo \html_writer::start_tag('form', array('class' => 'form-inline', 'method' => 'get'));
        foreach ($params as $key => $value) {
            echo \html_writer::empty_tag('input', array('type' => 'hidden', 'name' => $key, 'value' => $value));
        }
        $nums = array(10, 20, 30, 50);
        echo \html_writer::start_tag('select', array('name' => 'perpage', 'onchange' => 'this.form.submit();', 'class' => 'form-control'));
        foreach ($nums as $num) {
            $opts = array('value' => $num);
            if ($num == $perpage) {
                $opts['selected'] = 'selected';
            }
            echo \html_writer::tag('option', get_string('showperpage', 'local_visang', $num), $opts);
        }
        echo \html_writer::end_tag('select');
        echo \html_writer::tag('span', $label, array('class' => 'text-muted', 'style' => 'margin-left:10px;'));
        echo \html_writer::end_tag('form');
    }

    function table_pagination($url, $params = array(), $total_pages = 1, $current_page = 1, $max_nav = 10)
    {
        $padding = floor($max_nav / 2);
        $page_start = max(1, $current_page - $padding);
        $page_end = min(max($current_page + $padding - 1, $page_start + $max_nav - 1), $total_pages);

        echo \html_writer::start_tag('ul', array('class' => 'pagination', 'style' => 'margin: 0px 0px;'));
        if ($current_page > 1) {
            $p = $params;
            $p['page'] = $current_page - 1;
            echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&laquo;</span></a></li>';
        } else echo '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
        for ($i = $page_start; $i <= $page_end; $i++) {
            if ($i == $current_page) {
                echo '<li class="active"><span>' . $i . '</span></li>';
            } else {
                $p = $params;
                $p['page'] = $i;
                echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '">' . $i . '</a></li>';
            }
        }
        if ($current_page < $total_pages) {
            $p = $params;
            $p['page'] = $current_page + 1;
            echo '<li><a href="' . (new \moodle_url($url, $p))->out() . '"><span aria-hidden="true">&raquo;</span></a></li>';
        } else echo '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
        echo \html_writer::end_tag('ul');
    }
}
