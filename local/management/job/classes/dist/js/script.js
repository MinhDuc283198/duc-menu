const titleInput = document.querySelector('#cg-upload-title');
const descriptionInput = document.querySelector('#description');
const imageInput = document.querySelector('#cg-upload-image');
const discountrateInput = document.querySelector('#discountrate');
const cgstartdateinput = document.querySelector('#cg-start-date-input');
const cgexpiredateinput = document.querySelector('#cg-expire-date-input');
const yesqrcode = document.querySelector('#yesqrcode');
const noqrcode = document.querySelector('#noqrcode');

cgstartdateinput.addEventListener('change', (e) => {
    document.getElementById("savedcoupon").value="false";
});
cgexpiredateinput.addEventListener('change', (e) => {
    document.getElementById("savedcoupon").value="false";
});
yesqrcode.addEventListener('change', (e) => {
    document.getElementById("savedcoupon").value="false";
});
noqrcode.addEventListener('change', (e) => {
    document.getElementById("savedcoupon").value="false";
});
titleInput.addEventListener('keyup', (e) => {
    document.getElementById("savedcoupon").value="false";
    const previewTitle = document.querySelector('p.cg-cp-title');
    previewTitle.innerHTML = e.target.value;
});
discountrateInput.addEventListener('change', (e) => {
    document.getElementById("savedcoupon").value="false";
    const previewDiscount = document.querySelector('.cg-cp_discount p span');
    previewDiscount.innerHTML = e.target.value;
});
discountrateInput.addEventListener('keyup', (e) => {
    document.getElementById("savedcoupon").value="false";
    const previewDiscount = document.querySelector('.cg-cp_discount p span');
    previewDiscount.innerHTML = e.target.value;
});

descriptionInput.addEventListener('keyup', (e)=> {
    document.getElementById("savedcoupon").value="false";
    const previewDescription = document.querySelector('.cg-cp-description p');
    previewDescription.innerHTML = e.target.value;

})

imageInput.addEventListener('change', previewFile)

function previewFile(e) {
    const preview = document.querySelector('.coupon-preview_image img');
    const file = e.target.files[0];
    const reader = new FileReader();
    document.getElementById("savedcoupon").value="false";
    reader.addEventListener("load", function () {
        // convert image file to base64 string
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}
