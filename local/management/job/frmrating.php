<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__) . '/forms/visang_rating_form.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$ratingid = optional_param('ratingid', 0, PARAM_INT);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/frmrating.php', array('action' => $action, 'ratingid' => $ratingid));
$returnurl = new moodle_url($VISANG->wwwroot . '/ratings.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($baseurl));
// ===================================================================================================
// handles
$mform = new visang_rating_form($baseurl, array('action' => $action, 'ratingid' => $ratingid, 'returnurl' => $returnurl));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('ratings:add_rating', 'local_visang');
} else {
    $rating = $DB->get_record('vi_ratings', array('id' => $ratingid));
    if ($rating == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $mform->set_data($rating);
    }
    $title = get_string('ratings:edit_rating', 'local_visang') . ': ' . $rating->title;
}

// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'ratings';
$VISANG->page->navbar[] = get_string('ratings', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');

$VISANG->page->footer();
