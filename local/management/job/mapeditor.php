<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');

$type = optional_param('type', 'marker', PARAM_RAW);
$fieldID = optional_param('fieldID', '', PARAM_RAW);
$searchFieldID = optional_param('searchFieldID', '', PARAM_RAW);

$settings = visang_settings();
$apikey = $settings->google_map_api;

//$context = context_system::instance();
require_login(0, false);
require_sesskey();
//require_visang_capability($context);
//$PAGE->set_context($context);

// ===================================================================================================
// handles
if (!in_array($type, array('marker', 'polygon'))) {
    throw new moodle_exception('invalidarguments');
}

?>

<!DOCTYPE html>
<html lang="<?php echo $USER->lang; ?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>

    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $VISANG->wwwroot; ?>/styles.css">

    <script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=<?php echo $apikey; ?>&v=3&libraries=drawing,places&language=<?php echo $USER->lang; ?>"></script>
</head>

<body class="visang-app vi-map">
    <input id="pac-input" class="controls" type="text" placeholder="Search">
    <div id="mapview" style="position: absolute; left: 0px; top: 0px; width:100%; height: 100vh; background: #E4E4E4;"></div>

    <div id="toolbar">
        <div class="btn-group-vertical" role="group">
            <button type="button" class="btn btn-default" id="delete-all-button">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                <div>Delete all</div>
            </button>
            <button type="button" class="btn btn-default" id="delete-button" style="display:none">
                <i class="fa fa-trash" aria-hidden="true"></i>
                <div>Delete</div>
            </button>

            <button type="button" class="btn btn-warning" id="close-button">
                <i class="fa fa-times" aria-hidden="true"></i>
                <div>Close</div>
            </button>
            <button type="button" class="btn btn-success" id="save-button">
                <i class="fa fa-check" aria-hidden="true"></i>
                <div>Save</div>
            </button>
        </div>
    </div>

    <script>
        function MapEditor(elementID, type, saveCallback) {
            this.element = document.getElementById(elementID);
            this.map = null;
            this.type = type || 'marker';
            this.cfg = {
                defaultZoom: 17,
                defaultCenter: new google.maps.LatLng(10.0298914, 105.7710552),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                polyOptions: {
                    strokeWeight: 2,
                    strokeColor: '#FF0000',
                    fillColor: '#FF0000',
                    fillOpacity: 0.2,
                    editable: true
                },
                markerOptions: {
                    draggable: true
                }
            };

            this.drawingManager;
            this.all_overlays = [];
            this.selectedShape;

            this.saveCallback = saveCallback;
        }

        MapEditor.prototype.initialize = function() {
            this.map = new google.maps.Map(this.element, {
                zoom: this.cfg.defaultZoom,
                center: this.cfg.defaultCenter,
                mapTypeId: this.cfg.mapTypeId,
                zoomControl: true
            });

            // this.map.cfg = this.cfg;

            this.initSearchBox();

            if (this.type == 'marker') {
                this.drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.MARKER,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.BOTTOM_CENTER,
                        drawingModes: [
                            google.maps.drawing.OverlayType.MARKER,
                        ]
                    },
                    markerOptions: this.cfg.markerOptions,
                    map: this.map
                });
            } else if (this.type = 'polygon') {
                this.drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.POLYGON,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.BOTTOM_CENTER,
                        drawingModes: [
                            google.maps.drawing.OverlayType.POLYGON,
                        ]
                    },
                    polygonOptions: this.cfg.polyOptions,
                    map: this.map
                });
            }

            this.registerListeners();
        };

        MapEditor.prototype.registerListeners = function() {
            google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function(e) {
                if (e.type != google.maps.drawing.OverlayType.MARKER) {
                    this.drawingManager.setDrawingMode(null);
                    var newShape = e.overlay;
                    newShape.type = e.type;
                    google.maps.event.addListener(newShape, 'click', function() {
                        this.setSelection(newShape);
                    }.bind(this));
                    this.setSelection(newShape);
                } else {
                    this.deleteAllShape();
                }
                this.all_overlays.push(e);
            }.bind(this));
            google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', this.clearSelection.bind(this));
            google.maps.event.addListener(this.map, 'click', this.clearSelection.bind(this));

            if (this.type == 'polygon') {
                document.getElementById('delete-button').style.display = '';
                google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', this.deleteSelectedShape.bind(this));
            }
            google.maps.event.addDomListener(document.getElementById('delete-all-button'), 'click', this.deleteAllShape.bind(this));

            google.maps.event.addDomListener(document.getElementById('close-button'), 'click', this.close.bind(this));
            google.maps.event.addDomListener(document.getElementById('save-button'), 'click', this.save.bind(this));
        }

        MapEditor.prototype.initSearchBox = function() {
            // search box
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            this.map.addListener('bounds_changed', function() {
                searchBox.setBounds(this.map.getBounds());
            }.bind(this));

            var search_result_markers = [];
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }

                search_result_markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                search_result_markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    search_result_markers.push(new google.maps.Marker({
                        map: this.map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                this.map.fitBounds(bounds);
            }.bind(this));
            // end search box
        }

        MapEditor.prototype.clearSelection = function() {
            if (this.selectedShape) {
                this.selectedShape.setEditable(false);
                this.selectedShape = null;
            }
        }

        MapEditor.prototype.setSelection = function(shape) {
            this.clearSelection();
            this.selectedShape = shape;
            shape.setEditable(true);
        }

        MapEditor.prototype.deleteSelectedShape = function() {
            if (this.selectedShape) {
                for (var i = 0; i < this.all_overlays.length; i++) {
                    if (this.selectedShape === this.all_overlays[i].overlay) {
                        this.all_overlays.splice(i, 1);
                        break;
                    }
                }
                this.selectedShape.setMap(null);
            }
        }

        MapEditor.prototype.deleteAllShape = function() {
            for (var i = 0; i < this.all_overlays.length; i++) {
                this.all_overlays[i].overlay.setMap(null);
            }
            this.all_overlays = [];
        }

        MapEditor.prototype.getData = function() {
            var data = '';
            if (this.type == 'polygon') {
                var areas = '[';
                for (var i = 0; i < this.all_overlays.length; i++) {
                    if (i != 0) areas += ',';
                    areas += JSON.stringify(this.all_overlays[i].overlay.getPath().getArray());
                }
                areas += ']';
                if (areas != '[]') data = areas;
            } else if (this.type == 'marker' && this.all_overlays.length) {
                data = JSON.stringify(this.all_overlays[0].overlay.position);
            }

            return data;
        }

        MapEditor.prototype.setPolygon = function(data) {
            var bounds = new google.maps.LatLngBounds();
            var options = this.cfg.polyOptions;
            $.each(data, function(index, value) {
                var opts = options;
                opts.paths = value;
                var pol = new google.maps.Polygon(opts);
                this.all_overlays.push({
                    type: google.maps.drawing.OverlayType.POLYGON,
                    overlay: pol
                });
                pol.setMap(this.map);
                google.maps.event.addListener(pol, 'click', function() {
                    this.setSelection(pol);
                }.bind(this));
                this.setSelection(pol);

                $.each(value, function(idx, val) {
                    var point = new google.maps.LatLng(val.lat, val.lng);
                    bounds.extend(point);
                });
            }.bind(this));
            this.map.fitBounds(bounds);
        }

        MapEditor.prototype.setMarker = function(data) {
            var mar = new google.maps.Marker({
                map: this.map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: data
            });
            this.all_overlays.push({
                type: google.maps.drawing.OverlayType.MARKER,
                overlay: mar
            });

            this.map.setCenter(mar.getPosition());
        }

        MapEditor.prototype.close = function() {
            window.close();
        }

        MapEditor.prototype.save = function() {
            if (typeof this.saveCallback === 'function') {
                this.saveCallback(this.getData());
            }
        }

        google.maps.event.addDomListener(window, 'load', function() {
            var map_editor;
            var field_input = $('#<?php echo $fieldID; ?>', window.opener.document);
            var search_val = $('#<?php echo $searchFieldID; ?>', window.opener.document).val();
            var on_save = function(data) {
                field_input.val(data);
                map_editor.close();
            };
            map_editor = new MapEditor('mapview', '<?php echo $type; ?>', on_save);
            map_editor.initialize();
            if (search_val != '') {
                $('#pac-input').val(search_val);
            }

            var field_val = field_input.val();
            if (field_val != '') {
                map_editor.type == 'marker' ? map_editor.setMarker(JSON.parse(field_val)) : map_editor.setPolygon(JSON.parse(field_val));
            }
        });
    </script>
</body>

</html>