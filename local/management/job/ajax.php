<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

define('AJAX_SCRIPT', true);

require_once(dirname(__FILE__) . '/lib.php');

$action = required_param('action', PARAM_RAW);

//$context = context_system::instance();
require_login(0, false);
require_sesskey();
// require_visang_capability($context);
//$PAGE->set_context($context);

switch ($action) {
    case 'search_users':
        include_once($VISANG->dirroot . '/lib/users.php');
        echo json_encode(array('data' => array_values(visang_search_users())));
        die();
        break;
    case 'search_skills':
        include_once($VISANG->dirroot . '/lib/skills.php');
        echo json_encode(array('data' => array_values(visang_search_skills())));
        die();
        break;
    case 'search_skill_groups':
        include_once($VISANG->dirroot . '/lib/skills.php');
        echo json_encode(array('data' => array_values(visang_search_skill_groups())));
        die();
        break;
    case 'search_districts':
        include_once($VISANG->dirroot . '/lib/locations.php');
        echo json_encode(array('data' => array_values(visang_search_districts())));
        die();
        break;
    case 'search_countries':
        include_once($VISANG->dirroot . '/lib/locations.php');
        echo json_encode(array('data' => array_values(visang_search_countries())));
        die();
        break;
    case 'search_salaries':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        echo json_encode(array('data' => array_values(visang_search_salaries())));
        die();
        break;
    case 'search_reasons':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        echo json_encode(array('data' => array_values(visang_search_reasons())));
        die();
        break;
    case 'search_jobs':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        echo json_encode(array('data' => array_values(visang_search_jobs())));
        die();
        break;
    case 'search_employers':
        include_once($VISANG->dirroot . '/lib/jobs.php');
        echo json_encode(array('data' => array_values(visang_search_employers())));
        die();
        break;
    default:
        throw new moodle_exception('invalidarguments');
        break;
}
