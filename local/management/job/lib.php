<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../../visang/lib.php');
require_once(dirname(__FILE__) . '/classes/page.php');

$VISANG = new stdClass();
$VISANG->wwwroot = $CFG->wwwroot . '/local/management/job';
$VISANG->dirroot = $CFG->dirroot . '/local/management/job';
$VISANG->page = new \local_visang\page();

function require_visang_capability($context)
{
    global $VISANG;
    require_login();
    if (!(has_capability('local/visang:admin', $context))) {
        throw new required_capability_exception($context, 'visang/capability', 'nopermissions', '');
    }
}

function visang_settings()
{
    global $DB;
    return (object) $DB->get_records_select_menu('vi_settings', '', array(), '', 'name, value');
}


