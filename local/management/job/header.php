<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $PAGE, $USER, $VISANG, $context;

require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => $VISANG->page->title,
    'heading' => $VISANG->page->heading,
    'subheading' => '',
    'menu' => $VISANG->page->menu,
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);

// 사용자 메뉴 가져오기
$menus_arr = local_management_menu_get_user_menus();
$menus = local_management_menu_convert_class($menus_arr);
// /local/management/로 들어온 경우
// 첫번째 메뉴 페이지로 리다이렉트 한다.
if(empty($pagesettings['menu']) || $pagesettings['menu'] == 'home') {
    if(!empty($menus)) {
        $menu = local_management_leaf_child_menu($menus[0]);
        $href = local_management_make_absolute($menu->href);
        header('Location: '.$href);
        exit();
    }
}

// 메뉴 하이라이트
$menu = local_management_menu_set_active($menus, $pagesettings['menu']);

// DLP 홈으로 리다이렉트 되지 않도록 하기 위해 여기서 "DLP 홈 바로가기" 메뉴를 추가한다.
// $menu_home = new \local_management\menu(
//     array(
//         'text' => '비상교육 홈 바로가기',
//         'shortname' => 'dlphome',
//         'icon' => 'fa fa-home',
//         'href' => $CFG->wwwroot,
//         'target' => '_blank'));
// array_unshift($menus, $menu_home);
$menu_home = new \local_management\menu(
    array(
        'txt_vi' => 'Trang chủ mk job',
        'txt_ko' => 'mk jobs 바로가기',
        'txt_en' => 'mk jobs homepage',
        'text' => 'mk jobs 바로가기',
        'shortname' => 'mkjobs',
        'icon' => 'fa fa-home',
        'href' => $CFG->job_url.'/local/job',
        'target' => '_blank'));
array_unshift($menus, $menu_home);
$menu_home = new \local_management\menu(
    array(
        'txt_vi' => 'Trang chủ mk',
        'txt_ko' => 'mk 홈 바로가기',
        'txt_en' => 'mk homepage',
        'text' => 'mk 홈 바로가기',
        'shortname' => 'mkhome',
        'icon' => 'fa fa-home',
        'href' => $CFG->wwwroot,
        'target' => '_blank'));
array_unshift($menus, $menu_home);
$userpicture = new user_picture($USER);
$url = $userpicture->get_url($PAGE);
$userpicture = $url->out(false);
$userpicture = str_replace("/f2?rev", "/f1?rev", $userpicture);

$fullname = fullname($USER);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $VISANG->page->title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/dist/css/skins/_all-skins.min.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="<?php echo $VISANG->wwwroot; ?>/styles.css">

    <!-- jQuery 3 -->
    <script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>

    <?php
    foreach ($VISANG->page->css as $css) {
        echo '<link rel="stylesheet" href="' . $css . '">';
    }
    foreach ($VISANG->page->styles as $style) {
        echo html_writer::tag('style', $style);
    }
    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini fixed visang-app">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="<?php echo $CFG->wwwroot; ?>/local/management/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <!--<span class="logo-mini"><b>LG</b>A</span>-->
            <!-- logo for regular state and mobile devices -->
            <!--<span class="logo-lg"><b>LG</b>Academy</span>-->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $userpicture; ?>" class="user-image" alt="<?php echo $fullname; ?>">
                            <span class="hidden-xs"><?php echo $fullname; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                <div class="text-center">
                                    <a href="<?php echo $CFG->wwwroot.'/local/mypage/index.php'; ?>">개인정보</a>
                                </div>
                                </div>
                            </li>
                                <?php
                                $langs = array('vn' => 'Vietnamese ‎(vn)', 'en' => 'English ‎(en)‎', 'ko' => '한국어 ‎(ko)');
                                foreach ($langs as $lang => $val) {
                                    $currlang = current_language();
                                    $on = '';
                                    if ($lang == 'vn') {
                                        $lang_txt = 'vi';
                                    } else {
                                        $lang_txt = $lang;
                                    }
                                    ?>
                                    <li class="user-body <?php echo $currlang == $lang_txt ?  'active' : '' ?>">
                                        <div class="row">
                                            <div class="text-center">
                                                <a href="<?php echo $langurl . "?lang=$lang_txt"; ?>"><?php echo $on ?><?php echo strtoupper($lang) ?></a>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            <li class="user-body">
                                <div class="row">
                                <div class="text-center">
                                    <a href="<?php echo $CFG->wwwroot.'/login/logout.php?sesskey='. sesskey(); ?>">로그아웃</a>
                                </div>
                                </div>
                            </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <?php
                    foreach($menus as $menu) {
                        echo $menu->print_menu();
                    }
                    ?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <?php
                echo html_writer::tag('h1', $VISANG->page->heading . html_writer::tag('small', $VISANG->page->subheading));

                echo html_writer::start_tag('ol', array('class' => 'breadcrumb'));
                echo '<li><i class="fa fa-home"></i> ' . get_string('visang:dashboard', 'local_visang') . '</li>';
                foreach ($VISANG->page->navbar as $navbar) {
                    echo html_writer::tag('li', $navbar);
                }
                echo html_writer::end_tag('ol');
                ?>
            </section>