<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
global $CFG, $VISANG;

?>
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    Copyright &copy; VISANG Education Group Vietnam Company
</footer>
</div>
<!-- ./wrapper -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/dist/js/adminlte.min.js"></script>

<script>
    function open_popup(url) {
        var w = 880;
        var h = 570;
        var l = Math.floor((screen.width - w) / 2);
        var t = Math.floor((screen.height - h) / 2);
        var win = window.open(url, '', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
    }
</script>

<?php
foreach ($VISANG->page->js as $js) {
    echo '<script src="' . $js . '"></script>';
}
foreach ($VISANG->page->scripts as $script) {
    echo html_writer::tag('script', $script);
}
?>

</body>

</html>