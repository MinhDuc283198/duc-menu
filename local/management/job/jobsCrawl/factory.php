<?php

//echo 1;die;

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($VISANG->dirroot . '/lib/skills.php');

$context = context_system::instance();
$PAGE->set_context($context);

$data = $_POST;

if(empty($data)) {
    return 0;
}


$data = json_decode(json_encode($data));
$employer = saveCompany($data);

$data->employerid = $employer;

echo saveJob($data);

function saveCompany($data)
{
    global $DB;

    $content = new stdClass();
    $content->company_name = $data->company_name;
    $content->company_logo =  '';

    $content->company_website = "";
    $content->company_facebook = "";

    $content->company_short_description = $data->meta_description;
    $content->company_description = $data->job_content;
    $content->company_address = $data->address;

    $company_type = $DB->get_records_list('vi_company_types', 'name', $data->career);

    if($company_type){
        $data->company_type_id = $company_type->id;
    }

    $content->company_type_id = $data->company_type_id;

    $content->company_size_id = 6;

    $content->country_id = 1;

    $content->reason_description = $data->job_requirement;
    $content->timemodified = time();

    $content->url = uniqid();
    $content->timecreated = time();
    if ($employer_id = $DB->insert_record('vi_employers', $content)) {
        update_reason_lists($employer_id, $data->job_skills);
        return $employer_id;
    }
    return false;
}

function update_reason_lists($employerid, $reasons)
{
    global $DB, $VISANG;

    $DB->delete_records('vi_employer_reasons', array('employer_id' => $employerid));
    foreach ($reasons as $reason) {
        $reasonid = visang_findOrCreate_reasonCaregoryID($reason, $employerid);

        $row = new stdClass();
        $row->employer_id = $employerid;
        $row->reason_category_id = $reasonid;
        $DB->insert_record('vi_employer_reasons', $row);
    }
}


function saveJob($data)
{
    global $DB, $VISANG;

    $job_salary_category_id = visang_findOrCreate_salaryID($data->salary, $data->employerid) ;

    $skill_group_id = visang_findOrCreate_skillGroupID($data->career[0]);

    $content = new stdClass();
    $content->title = $data->job_name;
    // $content->short_description = $data->short_description;
    $content->description = $data->job_content;
    $content->other_request_info = $data->job_requirement;
//    $content->other_lang_info = $data->other_info;

    $content->employer_id = $data->employerid;
    $content->job_salary_category_id = $job_salary_category_id;
    $content->skill_group_id = $skill_group_id;

    $exp = (int)$data->exp;
    switch($exp) {
        case $exp < 1:
            $data->work_experience_id = 1;
            break;
        case $exp < 3:
            $data->work_experience_id = 2;
            break;
        case $exp < 6:
            $data->work_experience_id = 3;
            break;
        default:
            $data->work_experience_id = 4;
            break;
    }

    $content->work_experience_id = $data->work_experience_id;

    $content->korean_level_id = 1;
    $content->published = 1;

    $content->startdate = time();
    $content->enddate = strtotime($data->expired);
    $content->timemodified = time();

    $content->special_info = 5;
    $content->final_education_id = 2;
    $content->age_from = null;
    $content->age_to = null;

    $content->timecreated = time();
    if ($jobid = $DB->insert_record('vi_jobs', $content)) {

        update_skill_lists($jobid, $data->job_skills);
        update_work_types($jobid, $data->workingForm);
        update_benefit_types($jobid, $data->benefit);
        return $jobid;
    }

    return false;
}
function update_work_types($jobid, $work_types)
{
    global $DB;

    $careBuilderWorkinhForm = [
        1 => 'Nhân viên chính thức',
        2 => 'Thời vụ - Nghề tự do',
        3 => 'Bán thời gian',
        4 => 'Thực tập',
        5 => 'Thời vụ - Nghề tự do'
    ];

    $DB->delete_records('vi_job_work_types', array('job_id' => $jobid));

    foreach ($careBuilderWorkinhForm as $key => $value) {

        if ($work_types == $value) {
            $row = new stdClass();
            $row->job_id = $jobid;
            $row->work_type_id = $key;
        }

        $DB->insert_record('vi_job_work_types', $row);
    }
}

function update_benefit_types($jobid, $benefit)
{
    global $DB;

    $careBuilderBenefit  = [
        1 => "Phụ cấp",
        3 => "Đào tạo",
        4 => "Chăm sóc sức khỏe",
        5 => "Chế độ thưởng",
        6 => "Chế độ bảo hiểm",
        7 => "Tăng lương",
        8 => "Du Lịch"
    ];

    foreach ($careBuilderBenefit as $benefit_type_id => $name) {
        if (in_array($name, $benefit)) {
            $row = new stdClass();
            $row->job_id = $jobid;
            $row->benefit_type_id = $benefit_type_id;
            $row->timecreated = time();
            $row->timemodified = time();
            $DB->insert_record('vi_job_benefit_types', $row);
        }
    }
}

function update_skill_lists($jobid, $skills)
{
    global $DB, $VISANG;

    $DB->delete_records('vi_job_skills', array('job_id' => $jobid));
    foreach ($skills as $skill) {
        $skillid = visang_findOrCreate_skillID($skill);

        $row = new stdClass();
        $row->job_id = $jobid;
        $row->skill_id = $skillid;
        $DB->insert_record('vi_job_skills', $row);
    }
}