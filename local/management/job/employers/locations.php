<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$employerid = optional_param('employerid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_ALPHA);
$locationid = optional_param('locationid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'full_address', PARAM_RAW);

$sesskey = sesskey();

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/locations.php', array('employerid' => $employerid));
$formurl = new moodle_url($VISANG->wwwroot . '/employers/frmlocation.php', array('employerid' => $employerid));

$filterurl = new moodle_url($baseurl, array(
    'employerid' => $employerid,
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));

//$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

//$is_admin = has_capability('local/visang:admin', $context);

// ===================================================================================================
// handles
if ($employer == null || ($employer->deleted == 1 && !$is_admin)) {
    throw new moodle_exception('invalidaccess');
}

if ($action == 'delete' && $locationid != 0 && confirm_sesskey()) {
    if (!$DB->record_exists('vi_locations', array('id' => $locationid))) {
        throw new moodle_exception('idnotfound');
    } else {
        $DB->delete_records('vi_locations', array('id' => $locationid));
        redirect($filterurl);
    }
}

// filters
$sql = '';
$params = array();
$sql .= 'employer_id=:employerid';
$params['employerid'] = $employerid;
if ($search != '') {
    $sql .= ' AND ' . $DB->sql_like($searchfield, ':' . $searchfield, false);
    // $params = array($searchfield => '%' . $search . '%');
    $params[$searchfield] = '%' . $search . '%';
}
$offset = ($page - 1) * $perpage;
$locations = $DB->get_records_select('vi_locations', $sql, $params, '{vi_locations}.full_address ASC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_locations', $sql, $params, 'COUNT(*)');

// ===================================================================================================
// renders
$title = get_string('locations', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign', 'leftalign');
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    get_string('locations:full_address', 'local_visang'),
    get_string('locations:district', 'local_visang'),
    get_string('locations:country', 'local_visang'),
    get_string('locations:view_on_map', 'local_visang'),
    get_string('action')
);
$table->data = array();

foreach ($locations as $location) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl, array('action' => 'edit', 'employerid' => $employerid, 'locationid' => $location->id)),
        'fa fa-pencil-square-o',
        get_string('edit')
    );

    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'locationid' => $location->id))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';

    $district = $DB->get_record('vi_districts', array('id' => $location->district_id));
    $country = $DB->get_record('vi_countries', array('id' => $district->country_id));

    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'employerid' => $employerid, 'locationid' => $location->id)),
            '<i class="fa fa-map-marker" aria-hidden="true"></i> ' . $location->full_address
        ),
        $district->name,
        $country->name,
        html_writer::tag(
            'span',
            '<i class="fa fa-external-link" aria-hidden="true"></i> ' . get_string('view'),
            array(
                'class' => "text-muted cursor-pointer",
                'onclick' => 'javascript:open_popup(\'' . ($VISANG->wwwroot . '/mapview.php?sesskey=' . sesskey() . '&locationid=' . $location->id) . '\')'
            )
        ),
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>

<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'locations';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="pull-left">
                            <?php
                            echo html_writer::link(
                                new moodle_url($formurl, array('action' => 'add')),
                                get_string('locations:add_location', 'local_visang'),
                                array('class' => 'btn btn-primary')
                            );
                            ?>
                        </div>
                        <div class="pull-right">
                            <form method="get" class="form-inline">
                                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                                <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
                                <select name="searchfield" class="form-control">
                                    <option value="full_address" <?php if ($searchfield == 'full_address') { ?> selected="selected" <?php } ?>> <?php echo get_string('locations:full_address', 'local_visang'); ?> </option>
                                </select>

                                <div class="input-group">
                                    <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <?php
                        echo html_writer::table($table);
                        ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset;
                            $_row->to = min($offset + $perpage, $totalcount);
                            $_row->total = $totalcount;
                            $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search, 'employerid' => $employerid));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
