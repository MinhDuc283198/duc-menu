<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/forms/visang_job_form.php');
require_once($VISANG->dirroot . '/lib/skills.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$jobid = optional_param('jobid', 0, PARAM_INT);
$employerid = optional_param('employerid', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

if (!in_array($action, array('add', 'edit')) || $employer == null) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/frmjob.php', array('action' => $action, 'employerid' => $employerid, 'jobid' => $jobid));
$returnurl = new moodle_url($VISANG->wwwroot . '/employers/jobs.php', array('employerid' => $employerid));

//$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
$mform = new visang_job_form($baseurl, array('action' => $action, 'employerid' => $employerid, 'jobid' => $jobid, 'returnto' => $returnto, 'returnurl' => $returnurl));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('jobs:add_job', 'local_visang');
} else {
    $job = $DB->get_record('vi_jobs', array('id' => $jobid));
    if ($job == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $skills = visang_get_job_skills($job->id);
        $job_skills = array();
        foreach ($skills as $skill) {
            $job_skills[] = json_encode(array('id' => $skill->id, 'name' => $skill->name));
        }
        $job->job_skills = $job_skills;

        $salary = $DB->get_record('vi_job_salary_categories', array('id' => $job->job_salary_category_id));
        $job->job_salary = array(
            json_encode(array('id' => intval($salary->id), 'name' => $salary->name))
        );

        $skill_group = $DB->get_record('vi_skill_groups', array('id' => $job->skill_group_id));
        $job->skill_group = array(
            json_encode(array('id' => intval($skill_group->id), 'name' => $skill_group->name))
        );

        $job->startdate = date("Y-m-d", $job->startdate);
        $job->enddate = date("Y-m-d", $job->enddate);

        $job->location_ids = array_values($DB->get_records_menu('vi_job_locations', array('job_id' => $job->id), '', 'id, location_id'));

        $job->work_type_ids = array_values($DB->get_records_menu('vi_job_work_types', array('job_id' => $job->id), '', 'id, work_type_id'));
        $job->benefit_type_ids = array_values($DB->get_records_menu('vi_job_benefit_types', array('job_id' => $job->id), '', 'id, benefit_type_id'));

        $reasons = visang_get_job_reasons($job->id);
        $job_reasons = array();
        foreach ($reasons as $reason) {
            $job_reasons[] = json_encode(array('id' => $reason->id, 'name' => $reason->name));
        }
        $job->job_reasons = $job_reasons;

        $mform->set_data($job);
    }
    $title = get_string('jobs:edit_job', 'local_visang') . ': ' . $job->title;
}

// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = get_string('jobs', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'jobs';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <?php
                    echo html_writer::tag('h3', $title);
                    echo html_writer::start_div('vi-form');
                    $mform->renderer->display();
                    echo html_writer::end_div();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
