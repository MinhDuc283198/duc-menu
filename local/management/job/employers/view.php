<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/lib/skills.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$employerid = optional_param('employerid', 0, PARAM_INT);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/view.php', array('employerid' => $employerid));

//$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

//$is_admin = has_capability('local/visang:admin', $context);

// ===================================================================================================
// handles
if ($employer == null || ($employer->deleted == 1 && !$is_admin)) {
    throw new moodle_exception('invalidaccess');
}

// ===================================================================================================
// renders
$VISANG->page->title = $employer->company_name;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->header();

$employer->skill_groups = visang_get_employer_skill_groups($employer->id);
$employer->top_reasons = visang_get_employer_reasons($employer->id);

?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'info';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <h3>Description</h3>
                    <div class="post">
                        <?php echo $employer->company_description; ?>
                        <p>
                            <?php
                            foreach ($employer->skill_groups as $skill) {
                                echo '<span class="vi-tag">' . $skill->name . '</span>';
                            }
                            ?>
                        </p>
                    </div>

                    <h3><?php echo get_string('employers:employers_reasons', 'local_visang'); ?></h3>
                    <div class="post">
                        <p>
                            <ol class="h4">
                                <?php
                                foreach ($employer->top_reasons as $reason) {
                                    echo '<li><p>' . $reason->name . '</p></li>';
                                }
                                ?>
                            </ol>
                        </p>
                        <?php echo $employer->reason_description; ?>
                    </div>
                    <?php if (false) : ?>
                        <h3>Photos</h3>
                        <div class="post">
                            <ul class="mailbox-attachments clearfix">
                                <?php
                                $photos = $DB->get_records('vi_employer_photos', array('employer_id' => $employer->id));

                                foreach ($photos as $photo) {
                                    $f = explode('/', $photo->path);
                                    echo '<li>
                                        <span class="mailbox-attachment-icon has-img"><img src="' . $CFG->wwwroot . '/pluginfile.php/' . $photo->path . '" style="height: 150px;"></span>
                                        <div class="mailbox-attachment-info vi-text-truncate">
                                            <a href="' . $CFG->wwwroot . '/pluginfile.php/' . $photo->path . '" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> ' . $f[4] . '</a>
                                        </div>
                                    </li>';
                                }
                                ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
