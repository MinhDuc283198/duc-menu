<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/employers/forms/visang_companysize_form.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$comsizeid = optional_param('comsizeid', 0, PARAM_INT);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/frmcompanysize.php', array('action' => $action, 'comsizeid' => $comsizeid));
$returnurl = new moodle_url($VISANG->wwwroot . '/employers/companysizes.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
$mform = new visang_companysize_form($baseurl, array('action' => $action, 'comsizeid' => $comsizeid, 'returnurl' => $returnurl));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('employers:companysizes_add', 'local_visang');
} else {
    $com_size = $DB->get_record('vi_company_sizes', array('id' => $comsizeid));
    if ($com_size == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $mform->set_data($com_size);
    }
    $title = get_string('employers:companysizes_edit', 'local_visang') . ': ' . $com_size->name;
}

// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = get_string('employers:companysizes', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');

$VISANG->page->footer();
