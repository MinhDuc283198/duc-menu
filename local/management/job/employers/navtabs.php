<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $VISANG, $CFG, $is_admin, $employer;

echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/employers.php'), get_string('employers', 'local_visang')), array('class' => $VISANG->theme->navtab == 'employers' ? 'active':''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/companysizes.php'), get_string('employers:companysizes', 'local_visang')), array('class' => $VISANG->theme->navtab == 'companysizes' ? 'active':''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/companytypes.php'), get_string('employers:companytypes', 'local_visang')), array('class' => $VISANG->theme->navtab == 'companytypes' ? 'active':''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/overtimecategories.php'), get_string('employers:overtimecategories', 'local_visang')), array('class' => $VISANG->theme->navtab == 'overtimecategories' ? 'active':''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/workingdatecategories.php'), get_string('employers:workingdatecategories', 'local_visang')), array('class' => $VISANG->theme->navtab == 'workingdatecategories' ? 'active':''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/countries.php'), get_string('employers:countries', 'local_visang')), array('class' => $VISANG->theme->navtab == 'countries' ? 'active':''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/districts.php'), get_string('employers:districts', 'local_visang')), array('class' => $VISANG->theme->navtab == 'districts' ? 'active':''));