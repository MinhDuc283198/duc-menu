<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$employerid = optional_param('employerid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'title', PARAM_RAW);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/followers.php', array('employerid' => $employerid));

$filterurl = new moodle_url($baseurl, array(
    'employerid' => $employerid,
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_login();
//require_visang_capability($context);
$PAGE->set_context($context);
$PAGE->set_url($baseurl);

//$is_admin = has_capability('local/visang:admin', $context);

// ===================================================================================================
// handles
if ($employer == null || ($employer->deleted == 1 && !$is_admin)) {
    throw new moodle_exception('invalidaccess');
}

// filters
$offset = ($page - 1) * $perpage;

$filters = array();
$params = array();

$filters[] = 'f.employer_id=:employerid';
$params['employerid'] = $employerid;

if ($search != '') {
    $filters[] = $DB->sql_like('u.' . $searchfield, ':' . $searchfield, false);
    $params[$searchfield] = '%' . $search . '%';
}

$sql = "FROM {user} u
        JOIN {vi_follow_employers} f ON f.user_id = u.id
        " . (count($filters) ? " WHERE " . implode(' AND ', $filters) : "") . "
        ORDER BY f.timecreated DESC";

$followers = $DB->get_records_sql("SELECT u.*,f.timecreated as follow_timecreated " . $sql . " LIMIT " . $perpage . " OFFSET " . $offset, $params);
$totalcount = $DB->count_records_sql("SELECT COUNT(u.id) " . $sql, $params);

// ===================================================================================================
// renders
$title = get_string('followers', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = $title;
$VISANG->page->header();
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'followers';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="pull-left">
                            <h3><i class="fa fa-users" aria-hidden="true"></i> <? echo $title; ?></h3>
                        </div>
                        <div class="pull-right">
                            <form method="get" class="form-inline">
                                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                                <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
                                <select name="searchfield" class="form-control">
                                    <option value="username" <?php if ($searchfield == 'username') { ?> selected="selected" <?php } ?>> <?php echo get_string('username'); ?> </option>
                                    <option value="firstname" <?php if ($searchfield == 'firstname') { ?> selected="selected" <?php } ?>> <?php echo get_string('firstname'); ?> </option>
                                    <option value="lastname" <?php if ($searchfield == 'lastname') { ?> selected="selected" <?php } ?>> <?php echo get_string('lastname'); ?> </option>
                                    <option value="email" <?php if ($searchfield == 'email') { ?> selected="selected" <?php } ?>> <?php echo get_string('email'); ?> </option>

                                </select>

                                <div class="input-group">
                                    <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <?php if (count($followers)) : ?>
                            <ul class="users-list clearfix">
                                <?php foreach ($followers as $follower) : ?>
                                    <li style="width: auto;">
                                        <img src="<?php echo (new moodle_url('/user/pix.php/' . $follower->id . '/f1.jpg')); ?>" alt="User Image">
                                        <a class="users-list-name"><?php echo $follower->firstname . ' ' . $follower->lastname; ?></a>
                                        <span class="users-list-date">
                                            <?php echo userdate($follower->follow_timecreated, get_string('strftimedatetimeshort')); ?>
                                        </span>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php else : ?>
                            <div class="text-center"><?php echo get_string('empty:data', 'local_visang'); ?></div>
                        <?php endif; ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset;
                            $_row->to = min($offset + $perpage, $totalcount);
                            $_row->total = $totalcount;
                            $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search, 'employerid' => $employerid));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
