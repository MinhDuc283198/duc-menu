<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$action = optional_param('action', '', PARAM_ALPHA);
$employerid = optional_param('employerid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'name', PARAM_RAW);

$notConfirmed = optional_param('notConfirmed', 0, PARAM_INT);
$notPublished = optional_param('notPublished', 0, PARAM_INT);

$sesskey = sesskey();

$baseurl = $VISANG->wwwroot . '/employers/employers.php';
$formurl = $VISANG->wwwroot . '/employers/frmemployer.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($baseurl));

// ===================================================================================================
// handles
if ($action != '' && $employerid != 0 && confirm_sesskey()) {
    $_employer = $DB->get_record('vi_employers', array('id' => $employerid));
    if ($_employer == null) {
        throw new moodle_exception('idnotfound');
    } else {
        switch ($action) {
                // case 'delete':
                //     $row = new stdClass();
                //     $row->id = $employerid;
                //     $row->deleted = $_employer->deleted == '0' ? '1' : '0';
                //     $DB->update_record('vi_employers', $row);
                //     break;
            // case 'confirmed':
            //     $row = new stdClass();
            //     $row->id = $employerid;
            //     $row->confirmed = $_employer->confirmed == '0' ? '1' : '0';
            //     $DB->update_record('vi_employers', $row);
            //     break;
            case 'published':
                $row = new stdClass();
                $row->id = $employerid;
                $row->published = $_employer->published == '0' ? '1' : '0';
                $DB->update_record('vi_employers', $row);
                break;
        }

        redirect($filterurl);
    }
}

// filters
$sql = array();
$params = array();
if ($search != '') {
    $sql[] = $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params[$searchfield] = '%' . $search . '%';
}
// if ($notConfirmed) {
//     $sql[] = 'confirmed=:confirmed';
//     $params['confirmed'] = 0;
// }
if ($notPublished) {
    $sql[] = 'published=:published';
    $params['published'] = 0;
}

$offset = ($page - 1) * $perpage;
$employers = $DB->get_records_select('vi_employers', implode(" AND ", $sql), $params, '{vi_employers}.timecreated DESC, {vi_employers}.company_name ASC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_employers', implode(" AND ", $sql), $params, 'COUNT(*)');

$totalcount_by_jobs = $DB->count_records_sql(
    "SELECT COUNT(distinct e.id)
    FROM {vi_employers} e
    JOIN {vi_jobs} j ON e.id = j.employer_id " . (count($sql) ? "WHERE " . implode(" AND ", $sql) : ''),
    $params
);
// ===================================================================================================
// renders
$title = get_string('employers', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');

$VISANG->page->header();

$table = new html_table();
$table->id = 'employers';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    get_string('name'),
    'Jobs',
    'Followers',
    'Views',
    'Reviews',
    'Approval',
    'Published',
    'Date created',
    get_string('actions'),
);
$table->data = array();

foreach ($employers as $employer) {
    $logo = $employer->company_logo == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/default_emp.png' : $CFG->wwwroot . '/pluginfile.php/' . $employer->company_logo . '?preview=thumb';

    $actions = '';
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($VISANG->wwwroot . '/employers/view.php', array('employerid' => $employer->id)),
        'fa fa-eye',
        'View',
        'btn btn-default'
    );

    // more actions
    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($formurl, array('action' => 'edit', 'employerid' => $employer->id))) . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="' . (new moodle_url($baseurl, array('action' => 'published', 'employerid' => $employer->id, 'sesskey' => sesskey()))) . '"><i class="fa ' . ($employer->published ? 'fa-check-square-o' : 'fa-square-o') . '" aria-hidden="true"></i> Published</a></li>
      </ul>
    </div>';

    $job_count = $DB->count_records('vi_jobs', array('employer_id' => $employer->id));
    $follow_count = $DB->count_records('vi_follow_employers', array('employer_id' => $employer->id));

    $row = array(
        '<div class="media">
            ' . html_writer::div(html_writer::empty_tag('img', array('src' => $logo, 'class' => 'media-object', 'width' => '60')), 'media-left') . '
            <div class="media-body">
                <h4 class="media-heading">' . html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/view.php', array('employerid' => $employer->id)), $employer->company_name) . '</h4>
                <div>' . $employer->company_address . '</div>
            </div>
        </div>',
        $job_count,
        $follow_count,
        $employer->view_count,
        $DB->count_records('vi_reviews', array('employer_id' => $employer->id)),
        html_writer::tag('span', $employer->confirmed == 1 ? 'Accepted' : ($employer->confirmed == 0 ? 'Pending' : 'Not accepted'), array('class' => $employer->confirmed == 1 ? 'label label-success' : ($employer->confirmed == 0 ? 'label label-default' : 'label label-danger'))),
        html_writer::tag('span', $employer->published ? 'Published' : 'Unpublished', array('class' => $employer->published ? 'label label-success' : 'label label-default')),
        userdate($employer->timecreated, get_string('strftimedatetimeshort')),
        html_writer::div($actions, 'd-flex')
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <?php
            $VISANG->theme->navtab = 'employers';
            include('./navtabs.php');
            ?>
        </ul>
        <div class="box-body">
            <div class="form-group">
                <div class="pull-left">
                    <?php
                    echo html_writer::link(
                        new moodle_url($formurl, array('action' => 'add')),
                        get_string('employers:employers_add', 'local_visang'),
                        array('class' => 'btn btn-primary')
                    );
                    ?>
                </div>
                <div class="pull-right">
                    <div style="display: inline-block; margin-right: 30px;">
                        Total (Jobs > 0): <b><?php echo $totalcount_by_jobs; ?></b>
                    </div>

                    <form method="get" class="form-inline" style="display: inline-block;">
                        <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                        <div style="display: none;">
                            <label>Filters: </label>
                            <div class="checkbox d-inline-block" style="margin-left: 20px;">
                                <label>
                                    <input type="checkbox" name="notConfirmed" value="1" onChange="this.form.submit()" <?php echo $notConfirmed ? 'checked' : '' ?>> Not Confirmed
                                </label>
                            </div>
                            <div class="checkbox d-inline-block" style="margin-left: 20px;">
                                <label>
                                    <input type="checkbox" name="notPublished" value="1" onChange="this.form.submit()" <?php echo $notPublished ? 'checked' : '' ?>> Not Published
                                </label>
                            </div>
                        </div>
                        <select name="searchfield" class="form-control">
                            <option value="company_name" <?php if ($searchfield == 'company_name') { ?> selected="selected" <?php } ?>> <?php echo get_string('employers:employers_company_name', 'local_visang'); ?> </option>
                        </select>

                        <div class="input-group">
                            <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <?php
                echo html_writer::table($table);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset;
                    $_row->to = min($offset + $perpage, $totalcount);
                    $_row->total = $totalcount;
                    $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<?php
$VISANG->page->footer();
