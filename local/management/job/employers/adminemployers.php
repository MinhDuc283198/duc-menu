<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($CFG->libdir . '/filelib.php'); // for user picture

$employerid = optional_param('employerid', 0, PARAM_INT);
$employer = $DB->get_record('vi_employers', array('id' => $employerid));
if ($employer == null) {
    throw new moodle_exception('invalidaccess');
}

$action = optional_param('action', '', PARAM_ALPHA);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'name', PARAM_RAW);

$baseurl = $VISANG->wwwroot . '/employers/adminemployers.php';

$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
//$PAGE->set_url(new moodle_url($baseurl));
$PAGE->set_pagelayout('base');

// ===================================================================================================
// handles
// if ($action == 'delete' && $countryid != 0 && confirm_sesskey()) {
//     if (!$DB->record_exists('vi_countries', array('id' => $countryid))) {
//         throw new moodle_exception('idnotfound');
//     } else {
//         $DB->delete_records('vi_countries', array('id' => $countryid));
//         redirect(new moodle_url($baseurl, array(
//             'search' => $search,
//             'searchfield' => $searchfield,
//             'page' => $page,
//             'perpage' => $perpage
//         )));
//     }
// }

//
$sql = '';
$params = array();
if ($search != '') {
    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params = array($searchfield => '%' . $search . '%');
}
$offset = ($page - 1) * $perpage;
$users = $DB->get_records_select('user', $sql, $params, '{user}.firstname ASC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('user', $sql, $params, 'COUNT(*)');
// ===================================================================================================
// renders
$title = get_string('employers:employers_admin', 'local_visang');
$heading = get_string('pluginname', 'local_visang');

$PAGE->set_title($title);
echo $OUTPUT->header();

echo html_writer::start_tag('div', array('class' => 'container')); // main container
echo $OUTPUT->heading($heading);
$VISANG->active_navbar = $VISANG->wwwroot . '/employers/employers.php';
visang_get_navbar();
require_once(dirname(__FILE__) . '/com/com_employer_header.php');
echo html_writer::tag('h3', $title);

$actionparams = array(
    'employerid' => $employerid,
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage,
    'sesskey' => sesskey()
);

$admintable = new html_table();
$admintable->colclasses = array('leftalign', 'leftalign');
$admintable->id = 'usertable';
$admintable->attributes['class'] = 'table';
$admintable->data = array();
if (count($admintable->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = 2;
    $admintable->data[] = $emptyrow;
}

$usertable = new html_table();
$usertable->colclasses = array('leftalign', 'leftalign');
$usertable->id = 'usertable';
$usertable->attributes['class'] = 'table';
$usertable->data = array();
foreach ($users as $user) {
    $row = array(
        $user->firstname . ' ' . $user->lastname . ' '
            . html_writer::tag('span', '(' . $user->username . '/' . $user->email . ')', array('class' => 'muted')),
        '',
    );
    // adduser
    $adduser = $actionparams;
    $adduser['action'] = 'adduser';
    $adduser['userid'] = $user->id;
    $row[1] .= html_writer::tag('a', 'Add', array('href' => (new moodle_url($baseurl, $adduser))));

    $usertable->data[] = $row;
}
if (count($usertable->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = 2;
    $usertable->data[] = $emptyrow;
}
?>
<div class="row-fluid vi_board_cards">
    <div class="span6">
        <section class="vi_board_card">
            <h5>Current managers</h5>
            <?php
            echo html_writer::table($admintable);
            ?>
        </section>
    </div>
    <div class="span6">
        <section class="vi_board_card">
            <h5>Users</h5>
            <form class="table-search-option">
                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
                <select name="searchfield">
                    <option value="username" <?php if ($searchfield == 'username') { ?> selected="selected" <?php } ?>> <?php echo get_string('username'); ?> </option>
                    <option value="firstname" <?php if ($searchfield == 'firstname') { ?> selected="selected" <?php } ?>> <?php echo get_string('firstname'); ?> </option>
                    <option value="lastname" <?php if ($searchfield == 'lastname') { ?> selected="selected" <?php } ?>><?php echo get_string('lastname'); ?></option>
                    <option value="email" <?php if ($searchfield == 'email') { ?> selected="selected" <?php } ?>><?php echo get_string('email'); ?></option>
                </select>
                <input type="text" src="javascript:alert('XSS');" name="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('search'); ?>">
                <input type="submit" value="<?php echo get_string('search'); ?>" />
                <br>
            </form>
            <?php
            echo html_writer::table($usertable);
            // ======
            echo html_writer::start_tag('div', array('class' => 'clearfix'));

            echo html_writer::start_tag('div', array('class' => 'pull-left'));
            visang_get_table_perpage($perpage, '(' . $offset . '-' . min($offset + $perpage, $totalcount) . ' ' . get_string('oftotalrows', 'local_visang') . ' ' . $totalcount . ')', array('searchfield' => $searchfield, 'search' => $search, 'employerid' => $employerid));
            echo html_writer::end_tag('div');

            echo html_writer::start_tag('div', array('class' => 'pull-right'));
            visang_get_paging_bar($baseurl, array('search' => $search, 'searchfield' => $searchfield, 'perpage' => $perpage, 'employerid' => $employerid), ceil($totalcount / $perpage), $page);
            echo html_writer::end_tag('div');

            echo html_writer::end_tag('div');
            // ======
            ?>
        </section>
    </div>
</div>
<?php

// ======

// ======

echo html_writer::end_tag('div'); // end of main container
echo $OUTPUT->footer();
