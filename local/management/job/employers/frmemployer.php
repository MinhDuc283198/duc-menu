<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/employers/forms/visang_employer_form.php');
require_once($VISANG->dirroot . '/lib/skills.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$employerid = optional_param('employerid', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/frmemployer.php', array('action' => $action, 'employerid' => $employerid, 'returnto' => $returnto));
$returnurl = new moodle_url($VISANG->wwwroot . '/employers/employers.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
switch ($returnto) {
    case 'view':
        $returnurl = new moodle_url($VISANG->wwwroot . '/employers/view.php', array('employerid' => $employerid));
        break;
}

$mform = new visang_employer_form($baseurl, array('action' => $action, 'employerid' => $employerid, 'returnurl' => $returnurl, 'returnto' => $returnto), 'post', '', array('enctype' => 'multipart/form-data'));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_submitted_data();
    if (!is_null($formdata) && $mform->is_validated() && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('employers:employers_add', 'local_visang');
} else {
    $employer = $DB->get_record('vi_employers', array('id' => $employerid));
    if ($employer == null) {
        throw new moodle_exception('idnotfound');
    } else {
        if($employer->url == '') $employer->url = uniqid();

        $skills = visang_get_employer_skill_groups($employer->id);
        $skill_ids = array();
        foreach ($skills as $skill) {
            $skill_ids[] = json_encode(array('id' => $skill->id, 'name' => $skill->name));
        }
        $employer->skill_groups = $skill_ids;

        $reasons = visang_get_employer_reasons($employer->id);
        $top_reasons = array();
        foreach ($reasons as $reason) {
            $top_reasons[] = json_encode(array('id' => $reason->id, 'name' => $reason->name));
        }
        $employer->top_reasons = $top_reasons;

        $employer->company_photos = array_values($DB->get_records_menu('vi_employer_photos', array('employer_id' => $employer->id), '', 'id, path'));

        $mform->set_data($employer);
    }
    $title = get_string('employers:employers_edit', 'local_visang') . ': ' . $employer->company_name;
}

// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');
?>
<script>
    $(function(){
        $('input[name=company_website]').on('change', function(){
            var url = $(this).val();
            if(url.startsWith('http://')) {
                $(this).val(url.split('http://',2)[1]);
            } else if(url.startsWith('https://')) {
                $(this).val(url.split('https://',2)[1]);
            }
        });
        $('input[name=company_facebook]').on('change', function(){
            var url = $(this).val();
            if(url.startsWith('http://')) {
                $(this).val(url.split('http://',2)[1]);
            } else if(url.startsWith('https://')) {
                $(this).val(url.split('https://',2)[1]);
            }
        });
    });
</script>
<?php
$VISANG->page->footer();
