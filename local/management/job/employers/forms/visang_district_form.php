<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');

class visang_district_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $districtid = $this->_customdata['districtid'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'districtid');
        $mform->setType('districtid', PARAM_INT);
        $mform->setDefault('districtid', $districtid);

        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'name', get_string('name'), array('required', 'class' => 'form-control min-w'));
        $mform->addRule('name', null, 'required', null, 'server');
        $mform->setType('name', PARAM_RAW);

        // $mform->addElement('input', 'name_ko', get_string('name') . ' [KO]', array('class' => 'form-control min-w'));
        // $mform->setType('name_ko', PARAM_RAW);

        // $mform->addElement('input', 'name_en', get_string('name') . ' [EN]', array('class' => 'form-control min-w'));
        // $mform->setType('name_en', PARAM_RAW);

        // $mform->addElement('input', 'name_vi', get_string('name') . ' [VI]', array('class' => 'form-control min-w'));
        // $mform->setType('name_vi', PARAM_RAW);

        $mform->addElement('input', 'short_name', get_string('shortname'), array('class' => 'form-control min-w'));
        $mform->setType('short_name', PARAM_RAW);

        $countries = $DB->get_records_menu('vi_countries', null, '{vi_countries}.name ASC', 'id,name');
        $mform->addElement('select', 'country_id', get_string('employers:employers_country', 'local_visang'), $countries, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('country_id', null, 'required', null, 'server');
        $mform->setType('country_id', PARAM_INT);

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    function save($data)
    {
        global $DB;

        $content = new stdClass();
        $content->name = $data->name;
        // $content->name_ko = $data->name_ko;
        // $content->name_en = $data->name_en;
        // $content->name_vi = $data->name_vi;
        $content->name_ko = $data->name;
        $content->name_en = $data->name;
        $content->name_vi = $data->name;
        $content->short_name = $data->short_name;
        $content->country_id = $data->country_id;
        $content->timemodified = time();

        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                $DB->insert_record('vi_districts', $content);
                break;
            case 'edit':
                $content->id = $data->districtid;
                $DB->update_record('vi_districts', $content);
                break;
        }
    }
}
