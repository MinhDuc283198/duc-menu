<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');
require_once($VISANG->dirroot . '/lib/jobs.php');
require_once($VISANG->dirroot . '/lib/skills.php');

class visang_job_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $employerid = $this->_customdata['employerid'];
        $jobid = $this->_customdata['jobid'];
        $returnto = $this->_customdata['returnto'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'employerid');
        $mform->setType('employerid', PARAM_INT);
        $mform->setDefault('employerid', $employerid);

        $mform->addElement('hidden', 'jobid');
        $mform->setType('jobid', PARAM_INT);
        $mform->setDefault('jobid', $jobid);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);

        // ==========================================================================================
        $mform->addElement('input', 'title', get_string('jobs:title', 'local_visang'), array('required', 'class' => 'form-control'));
        $mform->addRule('title', null, 'required', null, 'server');
        $mform->setType('title', PARAM_RAW);

        // $mform->addElement('textarea', 'short_description', get_string('jobs:short_description', 'local_visang'), 'rows="3" required="required" class="form-control"');
        // $mform->addRule('short_description', null, 'required', null, 'server');
        // $mform->setType('short_description', PARAM_RAW);

        $mform->addElement('advautocomplete', 'skill_group', 'Group category', array('required'), array(
            'action' => 'search_skill_groups',
            'fields' => 'id,name',
            'template' => '{{ item.name }}',
            'maxitems' => 1,
        ));

        $mform->addElement('advautocomplete', 'job_skills', get_string('jobs:job_skills', 'local_visang'), array('required', 'desc' => 'Please enter the skills.'), array(
            'action' => 'search_skills',
            'fields' => 'id,name',
            'template' => '{{ item.name }}'
        ));

        $work_experiences = $DB->get_records_menu('vi_work_experiences', array(), '', 'id,name');
        $mform->addElement('checklist', 'work_experience_id', get_string('jobs:work_experience', 'local_visang'), $work_experiences, array('class' => 'margin', 'type' => 'radio'));

        $work_types = $DB->get_records_menu('vi_work_types', array(), '', 'id,name');
        $mform->addElement('checklist', 'work_type_ids', get_string('jobs:work_type', 'local_visang'), $work_types, array('class' => 'margin'));

        $benefit_types = $DB->get_records_menu('vi_benefit_types', array(), '', 'id,name');
        $mform->addElement('checklist', 'benefit_type_ids',  get_string('benefits', 'local_job'), $benefit_types, array('class' => 'margin'));

        $working_date_categories = $DB->get_records_menu('vi_working_date_categories', null, '{vi_working_date_categories}.sortorder ASC, {vi_working_date_categories}.name ASC', 'id,name');
        $mform->addElement('select', 'working_date_category_id', get_string('employers:employers_working_date_category', 'local_visang'), $working_date_categories, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('working_date_category_id', null, 'required', null, 'server');
        $mform->setType('working_date_category_id', PARAM_INT);

        $mform->addElement('advautocomplete', 'job_salary', get_string('jobs:job_salary', 'local_visang'), array('required', 'desc' => 'Please enter the salary.'), array(
            'action' => 'search_salaries',
            'fields' => 'id,name',
            'template' => '{{ item.name }}',
            'maxitems' => 1,
            'url' => $VISANG->wwwroot . '/ajax.php?employerid=' . $employerid
        ));

        $locations = $DB->get_records_menu('vi_locations', array('employer_id' => $employerid), '{vi_locations}.full_address ASC', 'id,full_address');
        $mform->addElement('checklist', 'location_ids', get_string('jobs:location', 'local_visang'), $locations, array('class' => 'margin'));

        // $mform->addElement('advautocomplete', 'job_reasons', get_string('jobs:job_reasons', 'local_visang'), array('required', 'desc' => 'Please enter the top reasons.', 'class' => 'vi-tag-wrap-list'), array(
        //     'action' => 'search_reasons',
        //     'fields' => 'id,name',
        //     'template' => '{{ item.name }}',
        //     'maxitems' => 3,
        //     'url' => $VISANG->wwwroot . '/ajax.php?employerid=' . $employerid
        // ));

        $mform->addElement('adveditor', 'description', get_string('jobs:description', 'local_visang'));
        $mform->setType('description', PARAM_RAW);

        $final_educations = $DB->get_records_menu('vi_final_educations', null, '{vi_final_educations}.sortorder ASC, {vi_final_educations}.name ASC', 'id,name_' . current_language());
        $mform->addElement('select', 'final_education_id', get_string('c_FinalEducation', 'local_job'), $final_educations, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('final_education_id', null, 'required', null, 'server');
        $mform->setType('final_education_id', PARAM_INT);

        $age_group = array();
        $age_group[] = &$mform->createElement('input', 'age_from', '', array('type' => 'number', 'min' => 1, 'required'));
        $age_group[] = &$mform->createElement('input', 'age_to', '', array('type' => 'number', 'min' => 1, 'required'));
        $mform->addGroup($age_group, 'age_group', get_string('c_age', 'local_job'), array(' '), false);


        $mform->addElement('adveditor', 'other_request_info', get_string('jobs:skills_experience', 'local_visang'));
        $mform->setType('other_request_info', PARAM_RAW);

        $korean_levels = $DB->get_records_menu('vi_korean_levels', null, '{vi_korean_levels}.sortorder ASC, {vi_korean_levels}.name ASC', 'id,name');
        $mform->addElement('select', 'korean_level_id', get_string('jobs:korean_level', 'local_visang'), $korean_levels, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('korean_level_id', null, 'required', null, 'server');
        $mform->setType('korean_level_id', PARAM_INT);

        $mform->addElement('adveditor', 'other_lang_info', get_string('jobs:culture_description', 'local_visang'));
        $mform->setType('other_lang_info', PARAM_RAW);

        $selectyesno = array(
            '1' => get_string('yes'),
            '0' => get_string('no'),
        );
        // $mform->addElement('checklist', 'hot_job', get_string('jobs:hot_job', 'local_visang'), $selectyesno, array('class' => 'margin', 'type' => 'radio'));
        // $mform->setDefault('hot_job', 0);
        // $mform->addElement('checklist', 'confirmed', get_string('jobs:confirmed', 'local_visang'), $selectyesno, array('class' => 'margin', 'type' => 'radio'));
        // $mform->setDefault('confirmed', 0);
        $mform->addElement('checklist', 'published', get_string('jobs:published', 'local_visang'), $selectyesno, array('class' => 'margin', 'type' => 'radio'));
        $mform->setDefault('published', 0);
        // $mform->addElement('checklist', 'deleted', get_string('jobs:deleted', 'local_visang'), $selectyesno, array('class' => 'margin', 'type' => 'radio'));
        // $mform->setDefault('deleted', 0);

        $mform->addElement('input', 'startdate', 'Start date', array('required', 'type' => 'date', 'class' => 'form-control'));
        $mform->addRule('startdate', null, 'required', null, 'server');
        $mform->setType('startdate', PARAM_RAW);

        $mform->addElement('input', 'enddate', 'End date', array('required', 'type' => 'date', 'class' => 'form-control'));
        $mform->addRule('enddate', null, 'required', null, 'server');
        $mform->setType('enddate', PARAM_RAW);

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    public function validation($data, $files)
    {
        global $DB;
        $errors = array();
        if (count($data['location_ids']) == 0) {
            $errors['location_ids'] = get_string('err_required', 'form');
        }
        if (count($data['job_skills']) == 0) {
            $errors['job_skills'] = get_string('err_required', 'form');
        }
        if (count($data['job_salary']) == 0) {
            $errors['job_salary'] = get_string('err_required', 'form');
        }
        // if (count($data['job_reasons']) == 0) {
        //     $errors['job_reasons'] = get_string('err_required', 'form');
        // }
        if (count($data['skill_group']) == 0) {
            $errors['skill_group'] = get_string('err_required', 'form');
        }

        if (strtotime($data['enddate']) < strtotime($data['startdate'])) {
            $errors['enddate'] = 'invalid value.';
        }

        if (intval($data['age_to']) < intval($data['age_from'])) {
            $errors['age_group'] = 'invalid value.';
        }
        return $errors;
    }

    function save($data)
    {
        global $DB, $VISANG;

        $salary = json_decode($data->job_salary[0]);
        $job_salary_category_id = $salary->id == '' ? visang_findOrCreate_salaryID($salary->name, $data->employerid) : $salary->id;

        $skill_group = json_decode($data->skill_group[0]);
        $skill_group_id = $skill_group->id == '' ? visang_findOrCreate_skillGroupID($skill_group->name) : $skill_group->id;

        $content = new stdClass();
        $content->title = $data->title;
        // $content->short_description = $data->short_description;
        $content->description = $data->description;
        $content->other_request_info = $data->other_request_info;
        $content->other_lang_info = $data->other_lang_info;
        $content->employer_id = $data->employerid;
        $content->job_salary_category_id = $job_salary_category_id;
        $content->skill_group_id = $skill_group_id;
        $content->work_experience_id = $data->work_experience_id;
        $content->korean_level_id = $data->korean_level_id;
        // $content->hot_job = $data->hot_job;
        // $content->confirmed = $data->confirmed;
        $content->published = $data->published;
        // $content->deleted = $data->deleted;
        $content->startdate = strtotime($data->startdate);
        $content->enddate = strtotime($data->enddate);
        $content->timemodified = time();

        $content->special_info = $data->working_date_category_id;
        $content->final_education_id = $data->final_education_id;
        $content->age_from = $data->age_from;
        $content->age_to = $data->age_to;

        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                if ($jobid = $DB->insert_record('vi_jobs', $content)) {
                    $this->update_location_lists($jobid, $data->location_ids);
                    $this->update_skill_lists($jobid, $data->job_skills);
                    // $this->update_reason_lists($data->employerid, $jobid, $data->job_reasons);
                    $this->update_work_types($jobid, $data->work_type_ids);
                    $this->update_benefit_types($jobid, $data->benefit_type_ids);
                }
                break;
            case 'edit':
                $content->id = $data->jobid;
                $DB->update_record('vi_jobs', $content);
                $this->update_location_lists($data->jobid, $data->location_ids);
                $this->update_skill_lists($data->jobid, $data->job_skills);
                // $this->update_reason_lists($data->employerid, $data->jobid, $data->job_reasons);
                $this->update_work_types($data->jobid, $data->work_type_ids);
                $this->update_benefit_types($data->jobid, $data->benefit_type_ids);
                break;
        }
    }

    function update_location_lists($jobid, $location_ids)
    {
        global $DB;

        $DB->delete_records('vi_job_locations', array('job_id' => $jobid));
        foreach ($location_ids as $location_id) {
            $row = new stdClass();
            $row->job_id = $jobid;
            $row->location_id = $location_id;
            $DB->insert_record('vi_job_locations', $row);
        }
    }

    function update_work_types($jobid, $work_type_ids)
    {
        global $DB;

        $DB->delete_records('vi_job_work_types', array('job_id' => $jobid));
        foreach ($work_type_ids as $work_type_id) {
            $row = new stdClass();
            $row->job_id = $jobid;
            $row->work_type_id = $work_type_id;
            $DB->insert_record('vi_job_work_types', $row);
        }
    }

    function update_benefit_types($jobid, $benefit_type_ids)
    {
        global $DB;

        $DB->delete_records('vi_job_benefit_types', array('job_id' => $jobid));
        foreach ($benefit_type_ids as $benefit_type_id) {
            $row = new stdClass();
            $row->job_id = $jobid;
            $row->benefit_type_id = $benefit_type_id;
            $row->timecreated=time();
            $row->timemodified=time();
            $DB->insert_record('vi_job_benefit_types', $row);
        }
    }

    public function update_skill_lists($jobid, $skills)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_job_skills', array('job_id' => $jobid));
        foreach ($skills as $skill) {
            $skill = json_decode($skill);
            $skillid = $skill->id == '' ? visang_findOrCreate_skillID($skill->name) : $skill->id;

            $row = new stdClass();
            $row->job_id = $jobid;
            $row->skill_id = $skillid;
            $DB->insert_record('vi_job_skills', $row);
        }
    }

    public function update_reason_lists($employerid, $jobid, $reasons)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_job_reasons', array('job_id' => $jobid));
        foreach ($reasons as $reason) {
            $reason = json_decode($reason);
            $reasonid = $reason->id == '' ? visang_findOrCreate_reasonCaregoryID($reason->name, $employerid) : $reason->id;

            $row = new stdClass();
            $row->job_id = $jobid;
            $row->reason_category_id = $reasonid;
            $DB->insert_record('vi_job_reasons', $row);
        }
    }
}
