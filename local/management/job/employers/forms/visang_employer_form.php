<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');
require_once($VISANG->dirroot . '/lib/skills.php');
require_once($VISANG->dirroot . '/lib/jobs.php');

class visang_employer_form extends moodleform
{

    public $renderer;

    function definition()
    {
        global $PAGE, $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $employerid = $this->_customdata['employerid'];
        $returnurl = $this->_customdata['returnurl'];
        $returnto = $this->_customdata['returnto'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'employerid');
        $mform->setType('employerid', PARAM_INT);
        $mform->setDefault('employerid', $employerid);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);

        // ===============================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'company_name', get_string('employers:employers_company_name', 'local_visang'), array('required', 'class' => 'form-control'));
        $mform->addRule('company_name', null, 'required', null, 'server');
        $mform->setType('company_name', PARAM_RAW);

        $mform->addElement('input', 'company_address', get_string('employers:employers_company_address', 'local_visang'), array('required', 'class' => 'form-control'));
        $mform->addRule('company_address', null, 'required', null, 'server');
        $mform->setType('company_address', PARAM_RAW);

        $company_types = $DB->get_records_menu('vi_company_types', null, '{vi_company_types}.sortorder ASC, {vi_company_types}.name ASC', 'id,name');
        $mform->addElement('select', 'company_type_id', get_string('employers:employers_company_type', 'local_visang'), $company_types, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('company_type_id', null, 'required', null, 'server');
        $mform->setType('company_type_id', PARAM_INT);

        $company_sizes = $DB->get_records_menu('vi_company_sizes', null, '{vi_company_sizes}.sortorder ASC, {vi_company_sizes}.name ASC', 'id,name');
        $mform->addElement('select', 'company_size_id', get_string('employers:employers_company_size', 'local_visang'), $company_sizes, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('company_size_id', null, 'required', null, 'server');
        $mform->setType('company_size_id', PARAM_INT);

        $countries = $DB->get_records_menu('vi_countries', null, '{vi_countries}.name ASC', 'id,name');
        $mform->addElement('select', 'country_id', get_string('employers:employers_country', 'local_visang'), $countries, array('required', 'class' => 'form-control min-w'));
        $mform->addRule('country_id', null, 'required', null, 'server');
        $mform->setType('country_id', PARAM_INT);

        // $working_date_categories = $DB->get_records_menu('vi_working_date_categories', null, '{vi_working_date_categories}.sortorder ASC, {vi_working_date_categories}.name ASC', 'id,name');
        // $mform->addElement('select', 'working_date_category_id', get_string('employers:employers_working_date_category', 'local_visang'), $working_date_categories, array('required', 'class' => 'form-control min-w'));
        // $mform->addRule('working_date_category_id', null, 'required', null, 'server');
        // $mform->setType('working_date_category_id', PARAM_INT);

        // $overtime_categories = $DB->get_records_menu('vi_overtime_categories', null, '{vi_overtime_categories}.sortorder ASC, {vi_overtime_categories}.name ASC', 'id,name');
        // $mform->addElement('select', 'overtime_category_id', get_string('employers:employers_overtime_category', 'local_visang'), $overtime_categories, array('required', 'class' => 'form-control min-w'));
        // $mform->addRule('overtime_category_id', null, 'required', null, 'server');
        // $mform->setType('overtime_category_id', PARAM_INT);

        // ===============================================================================================
        $mform->addElement('header', 'description', get_string('description'));

        $mform->addElement('textarea', 'company_short_description', get_string('employers:employers_company_short_description', 'local_visang'), 'rows="3" class="form-control"');
        $mform->setType('company_short_description', PARAM_RAW);

        $mform->addElement('adveditor', 'company_description', get_string('employers:employers_company_description', 'local_visang'));
        $mform->setType('company_description', PARAM_RAW);

        // ===============================================================================================
        // $mform->addElement('header', 'skill_list', get_string('employers:employers_skills', 'local_visang'));

        // $mform->addElement('advautocomplete', 'skills', get_string('skills', 'local_visang'), array('required', 'desc' => 'Please enter the skills.'), array(
        //     'action' => 'search_skills',
        //     'fields' => 'id,name',
        //     'template' => '{{ item.name }}'
        // ));
        // $mform->addElement('advautocomplete', 'skill_groups', get_string('skills:group', 'local_visang'), array('required', 'desc' => 'Please enter the group of skills.'), array(
        //     'action' => 'search_skill_groups',
        //     'fields' => 'id,name',
        //     'template' => '{{ item.name }}'
        // ));

        // ===============================================================================================
        $mform->addElement('header', 'reasons', get_string('employers:employers_reasons', 'local_visang'));

        $mform->addElement('advautocomplete', 'top_reasons', get_string('employers:employers_top_reasons', 'local_visang'), array('required', 'desc' => 'Please enter the top reasons.', 'class' => 'vi-tag-wrap-list'), array(
            'action' => 'search_reasons',
            'fields' => 'id,name',
            'template' => '{{ item.name }}',
            'url' => $VISANG->wwwroot . '/ajax.php?employerid=' . $employerid
        ));

        $mform->addElement('adveditor', 'reason_description', get_string('employers:employers_reason_description', 'local_visang'));
        $mform->setType('reason_description', PARAM_RAW);

        // ===============================================================================================
        $mform->addElement('header', 'photos', get_string('employers:employers_photos', 'local_visang'));
        $mform->addElement('advfile', 'company_logo', get_string('employers:employers_company_logo', 'local_visang'), null, array('maxfiles' => 1, 'accepted_types' => 'image/*;capture=camera'));
        // $mform->addElement('advfile', 'company_cover', get_string('employers:employers_company_cover', 'local_visang'), null, array('maxfiles' => 1, 'accepted_types' => 'image/*;capture=camera'));
        // $mform->addElement('advfile', 'company_photos', get_string('employers:employers_company_photos', 'local_visang'), null, array('maxfiles' => 5, 'accepted_types' => 'image/*;capture=camera'));

        // ===============================================================================================
        $mform->addElement('header', 'links', get_string('employers:employers_links', 'local_visang'));
        if ($action == 'edit') {
            $mform->addElement('input', 'url', get_string('employers:employers_url', 'local_visang'), array('required', 'class' => 'form-control'));
            $mform->addRule('url', null, 'required', null, 'server');
            $mform->setType('url', PARAM_RAW);
        }

        $mform->addElement('input', 'company_website', get_string('employers:employers_company_website', 'local_visang'), array('class' => 'form-control', 'placeholder'=>'job.masterkorean.vn'));
        $mform->setType('company_website', PARAM_RAW);

        $mform->addElement('input', 'company_facebook', get_string('employers:employers_company_facebook', 'local_visang'), array('class' => 'form-control','placeholder'=>'facebook.com/masterkoreanvietnam'));
        $mform->setType('company_facebook', PARAM_RAW);

        // $mform->addElement('input', 'company_twitter', get_string('employers:employers_company_twitter', 'local_visang'), array('class' => 'form-control'));
        // $mform->setType('company_twitter', PARAM_RAW);

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    public function validation($data, $files)
    {
        global $DB;
        $errors = array();
        if (isset($data['url']) && $DB->count_records_select('vi_employers', 'id <> ? AND url = ?', array($data['employerid'], $data['url']))) {
            $errors['url'] = 'URL already exists';
        }
        // if (count($data['skills']) == 0) {
        //     $errors['skills'] = get_string('err_required', 'form');
        // }
        // if (count($data['skill_groups']) == 0) {
        //     $errors['skill_groups'] = get_string('err_required', 'form');
        // }
        return $errors;
    }

    function save($data)
    {
        global $DB;

        $logo = $this->_form->getElement('company_logo')->save_file($data->company_logo);
        // $cover = $this->_form->getElement('company_cover')->save_file($data->company_cover);
        // $photos = $this->_form->getElement('company_photos')->save_file($data->company_photos);

        $content = new stdClass();
        $content->company_name = $data->company_name;
        $content->company_logo = count($logo) ? $logo[0] : '';
        // $content->company_cover = count($cover) ? $cover[0] : '';
        $content->company_website = $data->company_website;
        $content->company_facebook = $data->company_facebook;
        // $content->company_twitter = $data->company_twitter;
        $content->company_short_description = $data->company_short_description;
        $content->company_description = $data->company_description;
        $content->company_address = $data->company_address;
        $content->company_type_id = $data->company_type_id;
        $content->company_size_id = $data->company_size_id;
        $content->country_id = $data->country_id;
        // $content->working_date_category_id = $data->working_date_category_id;
        // $content->overtime_category_id = $data->overtime_category_id;
        $content->reason_description = $data->reason_description;
        $content->timemodified = time();

        switch ($data->action) {
            case 'add':
                $content->url = uniqid();
                $content->timecreated = time();
                if ($employer_id = $DB->insert_record('vi_employers', $content)) {
                    // $this->update_skill_lists($employer_id, $data->skills);
                    // $this->update_skill_groups($employer_id, $data->skill_groups);
                    $this->update_reason_lists($employer_id, $data->top_reasons);
                    // $this->update_slideshow($employer_id, $photos);
                }
                break;
            case 'edit':
                $content->id = $data->employerid;
                $content->url = $data->url;
                $DB->update_record('vi_employers', $content);
                // $this->update_skill_lists($data->employerid, $data->skills);
                // $this->update_skill_groups($data->employerid, $data->skill_groups);
                $this->update_reason_lists($data->employerid, $data->top_reasons);
                // $this->update_slideshow($data->employerid, $photos);
                break;
        }
    }

    function update_skill_lists($employerid, $skills)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_employer_skills', array('employer_id' => $employerid));
        foreach ($skills as $skill) {
            $skill = json_decode($skill);
            $skillid = $skill->id == '' ? visang_findOrCreate_skillID($skill->name) : $skill->id;

            $row = new stdClass();
            $row->employer_id = $employerid;
            $row->skill_id = $skillid;
            $DB->insert_record('vi_employer_skills', $row);
        }
    }

    function update_skill_groups($employerid, $skill_groups)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_employer_skill_groups', array('employer_id' => $employerid));
        foreach ($skill_groups as $skill) {
            $skill = json_decode($skill);
            $skillid = $skill->id == '' ? visang_findOrCreate_skillGroupID($skill->name) : $skill->id;

            $row = new stdClass();
            $row->employer_id = $employerid;
            $row->skill_group_id = $skillid;
            $DB->insert_record('vi_employer_skill_groups', $row);
        }
    }

    function update_reason_lists($employerid, $reasons)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_employer_reasons', array('employer_id' => $employerid));
        foreach ($reasons as $reason) {
            $reason = json_decode($reason);
            $reasonid = $reason->id == '' ? visang_findOrCreate_reasonCaregoryID($reason->name, $employerid) : $reason->id;

            $row = new stdClass();
            $row->employer_id = $employerid;
            $row->reason_category_id = $reasonid;
            $DB->insert_record('vi_employer_reasons', $row);
        }
    }

    function update_slideshow($employerid, $photos)
    {
        global $DB;

        $DB->delete_records('vi_employer_photos', array('employer_id' => $employerid));
        foreach ($photos as $path) {
            $photo = new stdClass();
            $photo->employer_id = $employerid;
            $photo->path = $path;
            $photo->timemodified = time();
            $photo->timecreated = time();
            $DB->insert_record('vi_employer_photos', $photo);
        }
    }
}
