<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');

class visang_companysize_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $comsizeid = $this->_customdata['comsizeid'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'comsizeid');
        $mform->setType('comsizeid', PARAM_INT);
        $mform->setDefault('comsizeid', $comsizeid);

        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'name', get_string('name'), array('required', 'class' => 'form-control'));
        $mform->addRule('name', null, 'required', null, 'server');
        $mform->setType('name', PARAM_RAW);

        $mform->addElement('input', 'short_name', get_string('shortname'), array('class' => 'form-control min-w'));
        $mform->setType('short_name', PARAM_RAW);

        $mform->addElement('input', 'sortorder', get_string('order'), array('class' => 'form-control min-w', 'type' => 'number', 'min' => 0));
        $mform->setType('sortorder', PARAM_INT);

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    function save($data)
    {
        global $DB;

        $content = new stdClass();
        $content->name = $data->name;
        $content->short_name = $data->short_name;
        $content->sortorder = max(0, intval($data->sortorder));
        $content->timemodified = time();

        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                $DB->insert_record('vi_company_sizes', $content);
                break;
            case 'edit':
                $content->id = $data->comsizeid;
                $DB->update_record('vi_company_sizes', $content);
                break;
        }
    }
}
