<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');
require_once($VISANG->dirroot . '/lib/locations.php');

class visang_location_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $employerid = $this->_customdata['employerid'];
        $locationid = $this->_customdata['locationid'];
        $returnto = $this->_customdata['returnto'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'employerid');
        $mform->setType('employerid', PARAM_INT);
        $mform->setDefault('employerid', $employerid);

        $mform->addElement('hidden', 'locationid');
        $mform->setType('locationid', PARAM_INT);
        $mform->setDefault('locationid', $locationid);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);

        // ==========================================================================================
        // $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'full_address', get_string('locations:full_address', 'local_visang'), array('required', 'class' => 'form-control', 'id' => 'full_address'));
        $mform->addRule('full_address', null, 'required', null, 'server');
        $mform->setType('full_address', PARAM_RAW);

        $mform->addElement('customfield', 'map_location', get_string('locations:location_on_map', 'local_visang'), array('desc' => 'Please select a location on the map.'));
        $mform->addRule('map_location', null, 'required', null, 'server');
        $mform->setType('map_location', PARAM_RAW);
        $this->set_map_location_field();

        $mform->addElement('advautocomplete', 'district', get_string('locations:district', 'local_visang'), array('required', 'desc' => 'Please enter the district.'), array(
            'action' => 'search_districts',
            'fields' => 'id,name',
            'template' => '{{ item.name }}',
            'maxitems' => 1
        ));

        $mform->addElement('advautocomplete', 'country', get_string('locations:country', 'local_visang'), array('required', 'desc' => 'Please enter the country.'), array(
            'action' => 'search_countries',
            'fields' => 'id,name',
            'template' => '{{ item.name }}',
            'maxitems' => 1
        ));

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    public function set_map_location_field()
    {
        global $VISANG;
        $el = &$this->_form->getElement('map_location');
        $fname = $el->getName();
        $el->setTemplate('<div class="input-group">
                            <input type="text" id="' . $fname . '" name="' . $fname . '" readonly value="' . $el->getValue() . '" class="form-control">
                            <span class="input-group-btn">
                                <div onclick="javascript:open_popup(\'' . $VISANG->wwwroot . '/mapeditor.php?sesskey=' . sesskey() . '&type=marker&fieldID=' . $fname . '&searchFieldID=full_address\')" class="btn btn-default btn-flat">' . get_string('locations:edit_on_map', 'local_visang') . '</div>
                            </span>
                        </div>');
    }

    public function validation($data, $files)
    {
        global $DB;
        $errors = array();
        if (count($data['district']) == 0) {
            $errors['district'] = get_string('err_required', 'form');
        }
        if (count($data['country']) == 0) {
            $errors['country'] = get_string('err_required', 'form');
        }
        return $errors;
    }

    public function set_data($data)
    {
        parent::set_data($data);
        $this->set_map_location_field();
    }

    function save($data)
    {
        global $DB, $VISANG;

        $map = json_decode($data->map_location);
        $country = json_decode($data->country[0]);
        $district = json_decode($data->district[0]);
        $countryid = $country->id == '' ? visang_findOrCreate_countryID($country->name) : $country->id;
        $districtid = $district->id == '' ? visang_findOrCreate_districtID($district->name, $countryid) : $district->id;

        $content = new stdClass();
        $content->employer_id = $data->employerid;
        $content->full_address = $data->full_address;
        $content->lat = $map->lat;
        $content->lng = $map->lng;
        $content->lng = $map->lng;
        $content->district_id = $districtid;
        $content->timemodified = time();

        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                $DB->insert_record('vi_locations', $content);
                break;
            case 'edit':
                $content->id = $data->locationid;
                $DB->update_record('vi_locations', $content);
                break;
        }
    }
}
