<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');

class visang_employer_manager_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $employerid = $this->_customdata['employerid'];

        $mform->addElement('hidden', 'employerid');
        $mform->setType('employerid', PARAM_INT);
        $mform->setDefault('employerid', $employerid);
        // ==========================================================================================
        $mform->addElement('advautocomplete', 'users', get_string('employers:employers_admin', 'local_visang'), array('required', 'desc' => 'Please enter the user.', 'class' => 'vi-tag-wrap-list rounded-0'), array(
            'action' => 'search_users',
            'fields' => 'id,username,firstname,lastname,email',
            'template' => '<div class="user-block" style="margin:0px; display:inline-block;">
                                <img class="img-circle" :src="\'' . $CFG->wwwroot . '/user/pix.php/\'+item.id+\'/f2.jpg\'">
                                <span class="username">
                                    <a>{{ item.firstname }} {{ item.lastname }}</a>
                                </span>
                                <span class="description">{{ item.email }}</span>
                            </div>',
            'canaddnew' => false
        ));
        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    function save($data)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_admin_employers', array('employer_id' => $data->employerid));
        foreach ($data->users as $user) {
            $user = json_decode($user);
            $row = new stdClass();
            $row->user_id = intval($user->id);
            $row->employer_id = $data->employerid;
            $DB->insert_record('vi_admin_employers', $row);
        }
    }
}
