<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/lib/ratings.php');

$employerid = optional_param('employerid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'title', PARAM_RAW);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/reviews.php', array('employerid' => $employerid));

$filterurl = new moodle_url($baseurl, array(
    'employerid' => $employerid,
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));

//$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

//$is_admin = has_capability('local/visang:admin', $context);

// ===================================================================================================
// handles
if ($employer == null || ($employer->deleted == 1 && !$is_admin)) {
    throw new moodle_exception('invalidaccess');
}

// filters
$sql = '';
$params = array();
$sql .= 'employer_id=:employerid';
$params['employerid'] = $employerid;
if ($search != '') {
    $sql .= ' AND ' . $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params[$searchfield] = '%' . $search . '%';
}
$offset = ($page - 1) * $perpage;
$reviews = $DB->get_records_select('vi_reviews', $sql, $params, '{vi_reviews}.timecreated DESC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_reviews', $sql, $params, 'COUNT(*)');

// ===================================================================================================
// renders
$title = get_string('reviews', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

?>
<style>
    .btn-grey {
        background-color: #D8D8D8;
        color: #FFF;
    }

    .rating-block {
        background-color: #FAFAFA;
        border: 1px solid #EFEFEF;
        padding: 15px 15px 20px 15px;
        border-radius: 3px;
    }

    .bold {
        font-weight: 700;
    }

    .padding-bottom-7 {
        padding-bottom: 7px;
    }

    .review-block {
        background-color: #FAFAFA;
        border: 1px solid #EFEFEF;
        padding: 15px;
        border-radius: 3px;
        margin-bottom: 15px;
    }

    .review-block-name {
        font-size: 12px;
        margin: 10px 0;
    }

    .review-block-date {
        font-size: 12px;
    }

    .review-block-rate {
        font-size: 13px;
        margin-bottom: 15px;
    }

    .review-block-title {
        font-size: 15px;
        font-weight: 700;
        margin-bottom: 10px;
    }

    .review-block-description {
        font-size: 13px;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'reviews';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                $ratings = visang_get_employer_ratings($employer->id);
                ?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="rating-block">
                                    <h4>Rating</h4>
                                    <h2 class="bold padding-bottom-7"><?php echo round($ratings['rate'], 2); ?> <small>/ 5</small></h2>
                                    <h3 class="text-yellow"><?php echo visang_get_rating_html(round($ratings['rate'], 2)); ?></h3>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <h4>Ratings for specific areas</h4>
                                <table class="table">
                                    <?php foreach ($ratings['ratings'] as $rating) : ?>
                                        <tr>
                                            <td><?php echo $rating->title; ?></td>
                                            <td>
                                                <div class="text-yellow"><?php echo visang_get_rating_html(round($rating->rate, 2)); ?> <b class="text-black"><?php echo round($rating->rate, 2); ?></b></div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div class="pull-left">
                                <h4><i class="fa fa-star" aria-hidden="true"></i> Reviews</h4>
                            </div>
                            <div class="pull-right" style="display: none">
                                <form method="get" class="form-inline">
                                    <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                                    <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
                                    <select name="searchfield" class="form-control">
                                        <option value="title" <?php if ($searchfield == 'title') { ?> selected="selected" <?php } ?>> <?php echo get_string('reviews:title', 'local_visang'); ?> </option>
                                        <option value="review_content" <?php if ($searchfield == 'review_content') { ?> selected="selected" <?php } ?>> <?php echo get_string('reviews:review_content', 'local_visang'); ?> </option>
                                        <option value="feedback_content" <?php if ($searchfield == 'feedback_content') { ?> selected="selected" <?php } ?>> <?php echo get_string('reviews:feedback_content', 'local_visang'); ?> </option>
                                    </select>

                                    <div class="input-group">
                                        <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php if (count($reviews)) : ?>
                            <?php foreach ($reviews as $review) : ?>
                                <div class="review-block">
                                    <?php
                                    $_user = $DB->get_record('user', array('id' => $review->user_id));
                                    $review_ratings = visang_get_employer_ratings($employerid, $review->user_id);
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            <img src="<?php echo (new moodle_url('/user/pix.php/' . $_user->id . '/f2.jpg')); ?>" class="img-circle">
                                            <div class="review-block-name"><a><?php echo $_user->firstname . ' ' . $_user->lastname; ?></a></div>
                                            <div class="review-block-date"><?php echo userdate($review->timecreated, get_string('strftimedatetimeshort')); ?></div>
                                            <h3 style="display: none">
                                                <?php echo $review->recommend ? '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' : '<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>' ?>
                                            </h3>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="review-block-rate text-yellow">
                                                <?php echo visang_get_rating_html(round($review_ratings['rate'], 2)); ?>
                                                <b class="text-black"><?php echo round($review_ratings['rate'], 2); ?></b>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                        <span class="fa fa-caret-down"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <table class="table" style="font-size: .8rem; color: #333; width: 300px;">
                                                            <?php foreach ($review_ratings['ratings'] as $r) : ?>
                                                                <tr>
                                                                    <td><?php echo $r->title; ?></td>
                                                                    <td>
                                                                        <div class="text-yellow"><?php echo visang_get_rating_html(round($r->rate, 2)); ?> <b class="text-black"><?php echo round($r->rate, 2); ?></b></div>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </table>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="review-block-title"><?php echo $review->title; ?></div>
                                            <div class="review-block-description">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th>What I liked</th>
                                                            <th>Suggestions for improvement</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php echo $review->review_content; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $review->feedback_content; ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <div class="text-center review-block"><?php echo get_string('empty:data', 'local_visang'); ?></div>
                        <?php endif; ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset;
                            $_row->to = min($offset + $perpage, $totalcount);
                            $_row->total = $totalcount;
                            $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search, 'employerid' => $employerid));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
