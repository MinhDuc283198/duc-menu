<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $VISANG, $CFG, $is_admin, $employer, $_employer_tab;

echo html_writer::start_tag('ul', array('class' => 'nav nav-tabs'));

echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/view.php', array('employerid' => $employer->id)), 'Infomation'), array('class' => $_employer_tab == 'info' ? 'active' : ''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/jobs.php', array('employerid' => $employer->id)), 'Jobs'), array('class' => $_employer_tab == 'jobs' ? 'active' : ''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/applications.php', array('employerid' => $employer->id)), 'Applications'), array('class' => $_employer_tab == 'applications' ? 'active' : ''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/reviews.php', array('employerid' => $employer->id)), 'Reviews'), array('class' => $_employer_tab == 'reviews' ? 'active' : ''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/locations.php', array('employerid' => $employer->id)), 'Locations'), array('class' => $_employer_tab == 'locations' ? 'active' : ''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/followers.php', array('employerid' => $employer->id)), 'Followers'), array('class' => $_employer_tab == 'followers' ? 'active' : ''));
echo html_writer::tag('li', html_writer::link(new moodle_url($VISANG->wwwroot . '/employers/managers.php', array('employerid' => $employer->id)), 'Managers'), array('class' => $_employer_tab == 'managers' ? 'active' : ''));

echo html_writer::end_tag('ul');
