<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

global $VISANG, $CFG, $is_admin, $employer;

require_once($VISANG->dirroot . '/lib/skills.php');

$employer->company_type = $DB->get_record('vi_company_types', array('id' => $employer->company_type_id), 'id,name,short_name');
$employer->company_size = $DB->get_record('vi_company_sizes', array('id' => $employer->company_size_id), 'id,name,short_name');
$employer->country = $DB->get_record('vi_countries', array('id' => $employer->country_id), 'id,name,short_name');
$employer->working_date_category = $DB->get_record('vi_working_date_categories', array('id' => $employer->working_date_category_id), 'id,name,short_name');
$employer->overtime_category = $DB->get_record('vi_overtime_categories', array('id' => $employer->overtime_category_id), 'id,name,short_name');

?>

<div class="box box-primary">
    <div class="box-body box-profile">
        <?php $logo = $employer->company_logo == '' ? $VISANG->wwwroot . '/pix/default-50x50.gif' : $CFG->wwwroot . '/pluginfile.php/' . $employer->company_logo; ?>
        <img class="profile-user-img img-responsive" src="<?php echo $logo; ?>" style="width: 200px;">

        <h3 class="profile-username text-center"><?php echo $employer->company_name; ?></h3>
        <p class="text-muted text-center"><?php echo $employer->company_short_description; ?></p>
        <p class="text-muted text-center"><i class="fa fa-external-link" aria-hidden="true"></i> <a target="_blank" href="#"><?php echo $employer->url; ?></a></p>

        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b><?php get_string("reviews","local_visang");?></b>
                <div class="pull-right"><?php echo $DB->count_records('vi_reviews', array('employer_id' => $employer->id)); ?></div>
            </li>
            <li class="list-group-item">
                <b><?php get_string("views","local_job");?></b>
                <div class="pull-right"><?php echo $employer->view_count; ?></div>
            </li>
            <li class="list-group-item">
                <b><?php get_string("course:app","local_oklearning");?></b>
                <div class="pull-right"><?php echo html_writer::tag('span', $employer->confirmed == 1 ? 'Accepted' : ($employer->confirmed == 0 ? 'Pending' : 'Not accepted'), array('class' => $employer->confirmed == 1 ? 'label label-success' : ($employer->confirmed == 0 ? 'label label-default' : 'label label-danger'))); ?></div>
            </li>
            <li class="list-group-item">
                <b><?php get_string("jobs:published","local_visang");?></b>
                <div class="pull-right"><?php echo html_writer::tag('span', $employer->published ? 'Published' : 'Unpublished', array('class' => $employer->published ? 'label label-success' : 'label label-default')); ?></div>
            </li>
            <?php
            if ($is_admin && $employer->deleted == 1) {
                echo '<li class="list-group-item">
                        <b>Deleted</b>
                        <div class="pull-right"><span class="label label-danger">Deleted</span></div>
                    </li>';
            }
            ?>
        </ul>
        <a href="<?php echo (new moodle_url($VISANG->wwwroot . '/employers/frmemployer.php', array('action' => 'edit', 'employerid' => $employer->id, 'returnto' => 'view'))); ?>" class="btn btn-primary btn-block"><b><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php get_string("edit","local_oklearning");?></b></a>
    </div>
    <!-- /.box-body -->
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php get_string("employers:employers","local_visang");?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <strong><?php get_string("employers:full_address","local_visang");?></strong>
        <p class="text-muted"><?php echo $employer->company_address; ?></p>

        <strong><?php get_string("employers:employers_company_type","local_visang");?></strong>
        <p class="text-muted"><?php echo $employer->company_type->name; ?></p>

        <strong><?php get_string("employers:employers_company_size","local_visang");?></strong>
        <p class="text-muted"><?php echo $employer->company_size->name; ?></p>

        <strong><?php get_string("employers:country","local_visang");?></strong>
        <p class="text-muted"><?php echo $employer->country->name; ?></p>

        <strong><?php get_string("employers:employers_working_date_category","local_visang");?></strong>
        <p class="text-muted"><?php echo $employer->working_date_category->name; ?></p>

        <strong><?php get_string("employers:employers_overtime_category","local_visang");?></strong>
        <p class="text-muted"><?php echo $employer->overtime_category->name; ?></p>
    </div>
    <!-- /.box-body -->
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php get_string("employers:employers_links","local_visang");?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <strong><?php get_string("employers:employers_company_website","local_visang");?></strong>
        <p class="text-muted"><a target="_blank" href="<?php echo $employer->company_website; ?>"><?php echo $employer->company_website; ?></a></p>
        <strong><?php get_string("employers:employers_company_facebook","local_visang");?></strong>
        <p class="text-muted"><a target="_blank" href="<?php echo $employer->company_facebook; ?>"><?php echo $employer->company_facebook; ?></a></p>
        <?php if (false) : ?>
            <strong><?php get_string("employers:employers_company_twitter","local_visang");?></strong>
            <p class="text-muted"><a target="_blank" href="<?php echo $employer->company_twitter; ?>"><?php echo $employer->company_twitter; ?></a></p>
        <?php endif; ?>
    </div>
    <!-- /.box-body -->
</div>