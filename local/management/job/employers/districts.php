<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$action = optional_param('action', '', PARAM_ALPHA);
$districtid = optional_param('districtid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'name', PARAM_RAW);

$sesskey = sesskey();

$baseurl = $VISANG->wwwroot . '/employers/districts.php';
$formurl = $VISANG->wwwroot . '/employers/frmdistrict.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($baseurl));

// ===================================================================================================
// handles
if ($action == 'delete' && $districtid != 0 && confirm_sesskey()) {
    if (!$DB->record_exists('vi_districts', array('id' => $districtid))) {
        throw new moodle_exception('idnotfound');
    } else {
        $DB->delete_records('vi_districts', array('id' => $districtid));
        redirect($filterurl);
    }
}

// filters
$sql = '';
$params = array();
if ($search != '') {
    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params = array($searchfield => '%' . $search . '%');
}
$offset = ($page - 1) * $perpage;
$districts = $DB->get_records_select('vi_districts', $sql, $params, '{vi_districts}.name ASC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_districts', $sql, $params, 'COUNT(*)');
// ===================================================================================================
// renders
$title = get_string('employers:districts', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('districts', 'local_visang');
$VISANG->page->navbar[] = $title;

$VISANG->page->header();

$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign');
$table->id = 'districts';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    get_string('name'),
    get_string('shortname'),
    get_string('actions')
);
$table->data = array();

foreach ($districts as $district) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl, array('action' => 'edit', 'districtid' => $district->id)),
        'fa fa-pencil-square-o',
        get_string('edit')
    );
    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'districtid' => $district->id))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';

    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'districtid' => $district->id)),
            $district->name
        ),
        $district->short_name,
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <?php
            $VISANG->theme->navtab = 'districts';
            include('./navtabs.php');
            ?>
        </ul>
        <div class="box-body">
            <div class="form-group">
                <div class="pull-left">
                    <?php
                    echo html_writer::link(
                        new moodle_url($formurl, array('action' => 'add')),
                        get_string('employers:districts_add', 'local_visang'),
                        array('class' => 'btn btn-primary')
                    );
                    ?>
                </div>
                <div class="pull-right">
                    <form method="get" class="form-inline">
                        <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                        <select name="searchfield" class="form-control">
                            <option value="name" <?php if ($searchfield == 'name') { ?> selected="selected" <?php } ?>> <?php echo get_string('name'); ?> </option>
                            <option value="short_name" <?php if ($searchfield == 'short_name') { ?> selected="selected" <?php } ?>><?php echo get_string('shortname'); ?></option>
                        </select>

                        <div class="input-group">
                            <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <?php
                echo html_writer::table($table);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset;
                    $_row->to = min($offset + $perpage, $totalcount);
                    $_row->total = $totalcount;
                    $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<?php
$VISANG->page->footer();
