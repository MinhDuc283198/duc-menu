<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

require_once($VISANG->dirroot . '/lib/jobs.php');

$employerid = optional_param('employerid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$jobid = optional_param('jobid', 0, PARAM_INT);
$applyid = optional_param('applyid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'candidate_fullname', PARAM_RAW);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));
$job = $DB->get_record('vi_jobs', array('id' => $jobid));

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/applications.php', array('employerid' => $employerid, 'jobid' => $jobid));

$filterurl = new moodle_url($baseurl, array(
    'employerid' => $employerid,
    'jobid' => $jobid,
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));

//$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

//$is_admin = has_capability('local/visang:admin', $context);

// ===================================================================================================
// handles
if ($employer == null || ($employer->deleted == 1 && !$is_admin)) {
    throw new moodle_exception('invalidaccess');
}

if ($action != '' && $applyid != 0 && confirm_sesskey()) {
    $__apply = $DB->get_record('vi_job_applications', array('id' => $applyid));
    if ($__apply == null) {
        throw new moodle_exception('idnotfound');
    } else {
        switch ($action) {
            case 'confirmed':
                $row = new stdClass();
                $row->id = $applyid;
                $row->confirmed = $__apply->confirmed == '0' ? '1' : '0';
                $DB->update_record('vi_job_applications', $row);
                break;
        }

        redirect($filterurl);
    }
}

// filters
$offset = ($page - 1) * $perpage;

$filters = array();
$params = array();

$filters[] = 'j.employer_id=:employerid';
$params['employerid'] = $employerid;

if ($jobid != 0) {
    $filters[] = 'app.job_id=:jobid';
    $params['jobid'] = $jobid;
}

$sql = "FROM {vi_job_applications} app
        JOIN {vi_jobs} j ON app.job_id = j.id
        " . (count($filters) ? " WHERE " . implode(' AND ', $filters) : "") . "
        ORDER BY app.timecreated DESC";
$applications = $DB->get_records_sql("SELECT app.* " . $sql . " LIMIT " . $perpage . " OFFSET " . $offset, $params);
$totalcount = $DB->count_records_sql("SELECT COUNT(app.id) " . $sql, $params);

// ===================================================================================================
// renders
$title = get_string('jobs:applications', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = get_string('jobs', 'local_visang');
$VISANG->page->navbar[] = $job->title;
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

$table = new html_table();
// $table->colclasses = array('leftalign');
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    'Date',
    get_string('jobs:title', 'local_visang'),
    get_string('jobs:applications_candidate_fullname', 'local_visang'),
    'Note',
    get_string('user'),
    'Status',
    get_string('action')
);

if ($job != null) {
    unset($table->head[1]);
}

$table->data = array();

foreach ($applications as $apply) {
    $actions = '';

    $resume = $DB->get_record('vi_resumes', array('id' => $apply->candidate_cv));
    $resume_url = '';
    switch ($resume->type) {
        case 'resume':
            $resume_url = $CFG->wwwroot . '/local/job/resumes/view.php?resumeid=' . $resume->id;
            break;
        case 'cvfile':
            $resume_url = $CFG->wwwroot . '/pluginfile.php/' . $resume->attachment_url;
            break;
        case 'video':
            $resume_url = $resume->video_url;
            break;
    }
    $actions .= '<div class="btn btn-default" role="button" onclick="visang_view_cv(\'' . $resume_url . '\', \'' . $resume->type . '\')"><i class="fa fa-eye" aria-hidden="true"></i> View CV</div>';
    // $actions .= '<div class="btn btn-default" role="button" onclick="visang_view_cv(\'' . ($CFG->wwwroot . '/pluginfile.php/' . $apply->candidate_cv) . '\')"><i class="fa fa-eye" aria-hidden="true"></i> View CV</div>';

    // more actions
    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($baseurl, array('action' => 'confirmed', 'applyid' => $apply->id, 'sesskey' => sesskey()))) . '"><i class="fa ' . ($apply->confirmed ? 'fa-check-square-o' : 'fa-square-o') . '" aria-hidden="true"></i> Confirmed</a></li>
      </ul>
    </div>';

    $status = '<div style="width: 100px;">';
    $status .= $apply->confirmed ? html_writer::tag('span', 'Confirmed', array('class' => 'label label-success d-inline-block')) : '';
    $status .= '</div>';

    $_user = $DB->get_record('user', array('id' => $apply->user_id));

    $row = array(
        userdate($apply->timecreated, get_string('strftimedatetimeshort')),
        '',
        $apply->candidate_fullname,
        $apply->candidate_advantages,
        '<div class="post">
            <div class="user-block" style="margin:0px;">
                <img class="img-circle" src="' . (new moodle_url('/user/pix.php/' . $_user->id . '/f2.jpg')) . '">
                <span class="username">
                    <a>' . $_user->firstname . ' ' . $_user->lastname . '</a>
                </span>
                <span class="description">' . $_user->email . '</span>
            </div>
        </div>',
        $status,
        html_writer::div($actions, 'd-flex'),
    );

    if ($job != null) {
        unset($row[1]);
    } else {
        $_job = $DB->get_record('vi_jobs', array('id' => $apply->job_id));
        $row[1] = html_writer::link(new moodle_url($baseurl, array('employerid' => $employerid, 'jobid' => $_job->id)), $_job->title);
    }

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'applications';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="pull-left">
                            <h4>
                                <i class="fa fa-suitcase" aria-hidden="true"></i> <?php echo $job == null ? get_string('jobs:applications_all', 'local_visang') : '[Job] ' . $job->title; ?>
                            </h4>
                        </div>
                        <div class="pull-right">
                            <form method="get" class="form-inline">
                                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                                <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
                                <input type="hidden" name="jobid" value="<?php echo $jobid; ?>">
                                <select name="searchfield" class="form-control">
                                    <option value="candidate_fullname" <?php if ($searchfield == 'candidate_fullname') { ?> selected="selected" <?php } ?>> <?php echo get_string('jobs:applications_candidate_fullname', 'local_visang'); ?> </option>
                                </select>

                                <div class="input-group">
                                    <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <?php
                        echo html_writer::table($table);
                        ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset;
                            $_row->to = min($offset + $perpage, $totalcount);
                            $_row->total = $totalcount;
                            $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search, 'employerid' => $employerid, 'jobid' => $jobid));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Large modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="vi-cv-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <iframe id="vi-cv-iframe" frameborder="0" style="width: 100%; height: 95vh;" scrolling="yes"></iframe>
        </div>
    </div>
</div>

<script>
    function visang_view_cv(url, type) {
        if (type == 'cvfile') {
            $('#vi-cv-iframe').attr('src', 'https://docs.google.com/gview?url=' + url + '&embedded=true');
            $('#vi-cv-iframe').change();
            $('#vi-cv-modal').modal('show');
        } else {
            window.open(url, '_blank');
        }
    }
</script>

<?php

$VISANG->page->footer();
