<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/forms/visang_location_form.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$locationid = optional_param('locationid', 0, PARAM_INT);
$employerid = optional_param('employerid', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

if (!in_array($action, array('add', 'edit')) || $employer == null) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/frmlocation.php', array('action' => $action, 'employerid' => $employerid, 'locationid' => $locationid));
$returnurl = new moodle_url($VISANG->wwwroot . '/employers/locations.php', array('employerid' => $employerid));

$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
//$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
$mform = new visang_location_form($baseurl, array('action' => $action, 'employerid' => $employerid, 'locationid' => $locationid, 'returnto' => $returnto, 'returnurl' => $returnurl));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('locations:add_location', 'local_visang');
} else {
    $location = $DB->get_record('vi_locations', array('id' => $locationid));
    if ($location == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $map = new stdClass();
        $map->lat = floatval($location->lat);
        $map->lng = floatval($location->lng);
        $location->map_location = htmlspecialchars(json_encode($map), ENT_QUOTES, 'UTF-8');

        $district = $DB->get_record('vi_districts', array('id' => $location->district_id));
        $country = $DB->get_record('vi_countries', array('id' => $district->country_id));

        $location->district = array(
            json_encode(array('id' => intval($district->id), 'name' => $district->name))
        );
        $location->country = array(
            json_encode(array('id' => intval($country->id), 'name' => $country->name))
        );

        $mform->set_data($location);
    }
    $title = get_string('locations:edit_location', 'local_visang');
}

// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = get_string('locations', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'locations';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <?php
                    echo html_writer::tag('h3', $title);
                    echo html_writer::start_div('vi-form');
                    $mform->renderer->display();
                    echo html_writer::end_div();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
