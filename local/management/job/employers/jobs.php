<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/lib/skills.php');

$employerid = optional_param('employerid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$jobid = optional_param('jobid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'title', PARAM_RAW);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/jobs.php', array('employerid' => $employerid));
$formurl = new moodle_url($VISANG->wwwroot . '/employers/frmjob.php', array('employerid' => $employerid));

$filterurl = new moodle_url($baseurl, array(
    'employerid' => $employerid,
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_login();
//require_visang_capability($context);
$PAGE->set_context($context);
$PAGE->set_url($baseurl);

//$is_admin = has_capability('local/visang:admin', $context);

// ===================================================================================================
// handles
if ($employer == null || ($employer->deleted == 1 && !$is_admin)) {
    throw new moodle_exception('invalidaccess');
}

if ($action != '' && $jobid != 0 && confirm_sesskey()) {
    $action_job = $DB->get_record('vi_jobs', array('id' => $jobid));
    if ($action_job == null) {
        throw new moodle_exception('idnotfound');
    } else {
        switch ($action) {
                 case 'delete':
                     $row = new stdClass();
                     $row->id = $jobid;
                     $row->deleted = $action_job->deleted == '0' ? '1' : '0';
                     $DB->update_record('vi_jobs', $row);
                     break;
                // case 'hot_job':
                //     $row = new stdClass();
                //     $row->id = $jobid;
                //     $row->hot_job = $action_job->hot_job == '0' ? '1' : '0';
                //     $DB->update_record('vi_jobs', $row);
                //     break;
                // case 'confirmed':
                //     $row = new stdClass();
                //     $row->id = $jobid;
                //     $row->confirmed = $action_job->confirmed == '0' ? '1' : '0';
                //     $DB->update_record('vi_jobs', $row);
                //     break;
            case 'published':
                $row = new stdClass();
                $row->id = $jobid;
                $row->published = $action_job->published == '0' ? '1' : '0';
                $DB->update_record('vi_jobs', $row);
                break;
        }

        redirect($filterurl);
    }
}

// filters
$sql = '';
$params = array();
$sql .= 'employer_id=:employerid and deleted = 0';
$params['employerid'] = $employerid;
if ($search != '') {
    $sql .= ' AND ' . $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params[$searchfield] = '%' . $search . '%';
}
$offset = ($page - 1) * $perpage;
$jobs = $DB->get_records_select('vi_jobs', $sql, $params, '{vi_jobs}.timecreated DESC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_jobs', $sql, $params, 'COUNT(*)');

// ===================================================================================================
// renders
$title = get_string('jobs', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

$table = new html_table();
// $table->colclasses = array('leftalign');
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    get_string('jobs:description', 'local_visang'),
    'Applications',
    'Publish date',
    'Status',
    get_string('action')
);
$table->data = array();

foreach ($jobs as $job) {
    $actions = '';

    $actions .= $VISANG->page->table_action_button(
        new moodle_url($VISANG->wwwroot . '/employers/applications.php', array('employerid' => $employerid, 'jobid' => $job->id)),
        'fa fa-eye',
        'Applications',
        'btn btn-default'
    );

    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
             <li>
                <a onclick="javascript:return confirm(\'Are you sure you want to delete this job?\')"  href="' . (new moodle_url($baseurl, array('action' => 'delete', 'employerid' => $employerid, 'jobid' => $job->id, 'sesskey' => sesskey()))) . '">
                <i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
             </li>
            <li role="separator" class="divider"></li>
            <li><a href="' . (new moodle_url($formurl, array('action' => 'edit', 'employerid' => $employerid, 'jobid' => $job->id))) . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="' . (new moodle_url($baseurl, array('action' => 'published', 'employerid' => $employerid, 'jobid' => $job->id, 'sesskey' => sesskey()))) . '"><i class="fa ' . ($job->published ? 'fa-check-square-o' : 'fa-square-o') . '" aria-hidden="true"></i> Published</a></li>
      </ul>
    </div>';

    $salary = $DB->get_record('vi_job_salary_categories', array('id' => $job->job_salary_category_id));

    $_skills = '';
    $skill_list = visang_get_job_skills($job->id);
    foreach ($skill_list as $skill) {
        $_skills .= html_writer::div($skill->name, 'vi-tag');
    }

    $status = '<div style="width: 100px;">';
    // $status .= $job->hot_job ? html_writer::tag('span', 'Hot job', array('class' => 'label label-warning d-inline-block')) : '';
    // $status .= $job->confirmed ? html_writer::tag('span', 'Confirmed', array('class' => 'label label-success d-inline-block')) : '';
    $status .= $job->published ? html_writer::tag('span', 'Published', array('class' => 'label label-success d-inline-block')) : '';
    // $status .= $job->deleted ? html_writer::tag('span', 'Deleted', array('class' => 'label label-danger d-inline-block')) : '';
    $status .= '</div>';

    $_locations = '';
    // $locations = $DB->get_records_menu('vi_job_locations', array('job_id' => $job->id), '', 'id, location_id')

    $application_count = $DB->count_records_select('vi_job_applications', 'job_id=:jobid', array('jobid' => $job->id), 'COUNT(*)');

    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'employerid' => $employerid, 'jobid' => $job->id)),
            $job->title,
            array('class' => 'h4 text-primary')
        )
            . html_writer::tag('div', '<i class="fa fa-usd" aria-hidden="true"></i> ' . $salary->name, array('class' => 'text-success'))
            . html_writer::div($job->short_description)
            . $_locations
            . html_writer::div($_skills),
        $application_count,
        // userdate($job->timecreated, get_string('strftimedatetimeshort')),
        userdate($job->startdate, get_string('strftimedatefullshort')) . ' ~ ' . userdate($job->enddate, get_string('strftimedatefullshort')),
        $status,
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'jobs';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="pull-left">
                            <?php
                            echo html_writer::link(
                                new moodle_url($formurl, array('action' => 'add')),
                                get_string('jobs:add_job', 'local_visang'),
                                array('class' => 'btn btn-primary')
                            );
                            ?>
                        </div>
                        <div class="pull-right">
                            <form method="get" class="form-inline">
                                <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                                <input type="hidden" name="employerid" value="<?php echo $employerid; ?>">
                                <select name="searchfield" class="form-control">
                                    <option value="title" <?php if ($searchfield == 'title') { ?> selected="selected" <?php } ?>> <?php echo get_string('jobs:title', 'local_visang'); ?> </option>
                                    <option value="short_description" <?php if ($searchfield == 'short_description') { ?> selected="selected" <?php } ?>> <?php echo get_string('jobs:short_description', 'local_visang'); ?> </option>
                                </select>

                                <div class="input-group">
                                    <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <?php
                        echo html_writer::table($table);
                        ?>
                    </div>
                    <div>
                        <div class="pull-left">
                            <?php
                            $_row = new stdClass();
                            $_row->from = $offset;
                            $_row->to = min($offset + $perpage, $totalcount);
                            $_row->total = $totalcount;
                            $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search, 'employerid' => $employerid));
                            ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
