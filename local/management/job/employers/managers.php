<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/forms/visang_employer_manager_form.php');
require_once($VISANG->dirroot . '/lib/users.php');

$employerid = optional_param('employerid', 0, PARAM_INT);

$employer = $DB->get_record('vi_employers', array('id' => $employerid));

if ($employer == null) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/employers/managers.php', array('employerid' => $employerid));

//$context = context_system::instance();
require_login(0, false);
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
$mform = new visang_employer_manager_form($baseurl, array('employerid' => $employerid));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($baseurl);
    }
}

$mdata = new stdClass();
$managers = visang_get_admin_employers($employerid);
$users = array();
foreach ($managers as $manager) {
    $users[] = json_encode(array(
        'id' => $manager->id,
        'username' => $manager->username,
        'firstname' => $manager->firstname,
        'lastname' => $manager->lastname,
        'email' => $manager->email
    ));
}
$mdata->users = $users;

$mform->set_data($mdata);

// ===================================================================================================
// renders
$title = get_string('employers:employers_admin', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = get_string('employers', 'local_visang');
$VISANG->page->menu = 'employers';
$VISANG->page->navbar[] = get_string('employers', 'local_visang');
$VISANG->page->navbar[] = $employer->company_name;
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <?php include_once($VISANG->dirroot . '/employers/com/com_employer_info.php'); ?>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <?php
                $_employer_tab = 'managers';
                include_once($VISANG->dirroot . '/employers/com/com_employer_tabs.php');
                ?>
                <div class="box-body">
                    <?php
                    echo html_writer::tag('h3', $title);
                    echo html_writer::start_div('vi-form');
                    $mform->renderer->display();
                    echo html_writer::end_div();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

$VISANG->page->footer();
