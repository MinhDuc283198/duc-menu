<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');

$locationid = required_param('locationid', PARAM_INT);

$settings = visang_settings();
$apikey = $settings->google_map_api;

//$context = context_system::instance();
require_login(0, false);
require_sesskey();
// require_visang_capability($context);
//$PAGE->set_context($context);

// ===================================================================================================
// handles

$location = $DB->get_record('vi_locations', array('id' => $locationid));
if ($location == null) {
    throw new moodle_exception('invalidarguments');
}
?>

<!DOCTYPE html>
<html lang="<?php echo $USER->lang; ?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>

    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $VISANG->wwwroot; ?>/styles.css">

    <script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=<?php echo $apikey; ?>&v=3&libraries=drawing,places&language=<?php echo $USER->lang; ?>"></script>
</head>

<body class="visang-app vi-map">
    <div id="mapview" style="position: absolute; left: 0px; top: 0px; width:100%; height: 100vh; background: #E4E4E4;"></div>

    <script>
        function MapEditor(elementID, type, saveCallback) {
            this.element = document.getElementById(elementID);
            this.map = null;
            this.type = type || 'marker';
            this.infowindow = null;
            this.cfg = {
                defaultZoom: 17,
                defaultCenter: new google.maps.LatLng(10.0298914, 105.7710552),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                polyOptions: {
                    strokeWeight: 2,
                    strokeColor: '#FF0000',
                    fillColor: '#FF0000',
                    fillOpacity: 0.2,
                    editable: false
                },
                markerOptions: {
                    draggable: false
                }
            };
        }

        MapEditor.prototype.initialize = function() {
            this.map = new google.maps.Map(this.element, {
                zoom: this.cfg.defaultZoom,
                center: this.cfg.defaultCenter,
                mapTypeId: this.cfg.mapTypeId,
                zoomControl: true
            });

            this.registerListeners();
        };

        MapEditor.prototype.registerListeners = function() {
            //
        }

        MapEditor.prototype.getInfoWindowHTML = function(title) {
            var html = '<div class="vimap-infowindow">' + title + '</div>';
            return html;
        };

        MapEditor.prototype.setMarker = function(data, title) {
            var mar = new google.maps.Marker({
                map: this.map,
                draggable: false,
                animation: google.maps.Animation.DROP,
                position: data
            });

            mar.addListener('click', function(e) {
                if (this.infowindow) {
                    this.infowindow.close();
                }
                this.infowindow = new google.maps.InfoWindow({
                    content: this.getInfoWindowHTML(title)
                });
                google.maps.event.addListener(this.infowindow, 'closeclick', function() {
                    this.infowindow = null;
                }.bind(this));
                this.infowindow.open(this.map, mar);
            }.bind(this));

            this.map.setCenter(mar.getPosition());

            //
            this.infowindow = new google.maps.InfoWindow({
                content: this.getInfoWindowHTML(title)
            });
            google.maps.event.addListener(this.infowindow, 'closeclick', function() {
                this.infowindow = null;
            }.bind(this));
            this.infowindow.open(this.map, mar);
        }

        MapEditor.prototype.close = function() {
            window.close();
        }

        google.maps.event.addDomListener(window, 'load', function() {
            var map_editor;
            map_editor = new MapEditor('mapview');
            map_editor.initialize();

            <?php if ($location != null) : ?>
                map_editor.setMarker({
                        "lat": parseFloat(<?php echo floatval($location->lat); ?>),
                        "lng": parseFloat(<?php echo floatval($location->lng); ?>)
                    },
                    "<?php echo $location->full_address; ?>"
                );
            <?php endif; ?>
        });
    </script>
</body>

</html>