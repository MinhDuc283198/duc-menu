<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__) . '/forms/visang_setting_form.php');

$baseurl = new moodle_url($VISANG->wwwroot . '/visang_settings.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($baseurl));
// ===================================================================================================
// handles
$mform = new visang_setting_form($baseurl);

if ($mform->is_submitted()) {
    $formdata = $mform->get_data();
    if (!is_null($formdata)) {
        $mform->save($formdata);
//        redirect($baseurl);
    }
} else {
    $mform->set_data(visang_settings());
}

// ===================================================================================================
// renders
$title = get_string('settings:settings', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'settings';
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');

$VISANG->page->footer();
