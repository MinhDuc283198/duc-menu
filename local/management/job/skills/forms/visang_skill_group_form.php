<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();

require_once($VISANG->dirroot . '/classes/formslib.php');
require_once($VISANG->dirroot . '/lib/skills.php');

class visang_skill_group_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $groupid = $this->_customdata['groupid'];
        $returnurl = $this->_customdata['returnurl'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'groupid');
        $mform->setType('groupid', PARAM_INT);
        $mform->setDefault('groupid', $groupid);

        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'name', get_string('name'), array('required', 'class' => 'form-control'));
        $mform->addRule('name', null, 'required', null, 'server');
        $mform->setType('name', PARAM_RAW);

        $mform->addElement('input', 'short_name', get_string('shortname'), array('class' => 'form-control min-w'));
        $mform->setType('short_name', PARAM_RAW);

        $mform->addElement('input', 'sortorder', get_string('order'), array('class' => 'form-control min-w', 'type' => 'number', 'min' => 0));
        $mform->setType('sortorder', PARAM_INT);

        // ==========================================================================================
        $mform->addElement('header', '_skills', get_string('skills:list', 'local_visang'));

        $mform->addElement('advautocomplete', 'skills', get_string('skills', 'local_visang'), array('required', 'desc' => 'Please enter the skills.'), array(
            'action' => 'search_skills',
            'fields' => 'id,name',
            'template' => '{{ item.name }}'
        ));

        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);
    }

    public function validation($data, $files)
    {
        global $DB;
        $errors = array();
        if (count($data['skills']) == 0) {
            $errors['skills'] = get_string('err_required', 'form');
        }
        return $errors;
    }

    function save($data)
    {
        global $DB;

        $content = new stdClass();
        $content->name = $data->name;
        $content->short_name = $data->short_name;
        $content->sortorder = max(0, intval($data->sortorder));
        $content->timemodified = time();

        switch ($data->action) {
            case 'add':
                $content->timecreated = time();
                if ($skill_group_id = $DB->insert_record('vi_skill_groups', $content)) {
                    $this->update_group_lists($skill_group_id, $data->skills);
                }
                break;
            case 'edit':
                $content->id = $data->groupid;
                $DB->update_record('vi_skill_groups', $content);
                $this->update_group_lists($data->groupid, $data->skills);
                break;
        }
    }

    function update_group_lists($groupid, $skills)
    {
        global $DB, $VISANG;

        $DB->delete_records('vi_skill_group_lists', array('skill_group_id' => $groupid));
        foreach ($skills as $skill) {
            $skill = json_decode($skill);
            $skillid = $skill->id == '' ? visang_findOrCreate_skillID($skill->name) : $skill->id;

            $skill_group = new stdClass();
            $skill_group->skill_id = $skillid;
            $skill_group->skill_group_id = $groupid;
            $DB->insert_record('vi_skill_group_lists', $skill_group);
        }
    }
}
