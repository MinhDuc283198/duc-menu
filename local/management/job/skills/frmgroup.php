<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/forms/visang_skill_group_form.php');
require_once($VISANG->dirroot . '/lib/skills.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$groupid = optional_param('groupid', 0, PARAM_INT);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/skills/frmgroup.php', array('action' => $action, 'groupid' => $groupid));
$returnurl = new moodle_url($VISANG->wwwroot . '/skills/group.php');

$context = context_system::instance();
require_visang_capability($context);
$PAGE->set_context($context);
$PAGE->set_url($baseurl);

// ===================================================================================================
// handles
$mform = new visang_skill_group_form($baseurl, array('action' => $action, 'groupid' => $groupid, 'returnurl' => $returnurl));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('skills:add_group', 'local_visang');
} else {
    $skill_group = $DB->get_record('vi_skill_groups', array('id' => $groupid));
    if ($skill_group == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $skill_lists = visang_get_group_skills($groupid);
        $skills = array();
        foreach ($skill_lists as $skill) {
            $skills[] = json_encode(array('id' => $skill->id, 'name' => $skill->name));
        }
        $skill_group->skills = $skills;

        $mform->set_data($skill_group);
    }
    $title = get_string('skills:edit_group', 'local_visang') . ': ' . $skill_group->name;
}
// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'skill_groups';
$VISANG->page->navbar[] = get_string('skills', 'local_visang');
$VISANG->page->navbar[] = get_string('skills:group', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');

$VISANG->page->footer();
