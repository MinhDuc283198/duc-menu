<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$action = optional_param('action', '', PARAM_ALPHA);
$groupid = optional_param('groupid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'name', PARAM_RAW);

$sesskey = sesskey();

$baseurl = $VISANG->wwwroot . '/skills/group.php';
$formurl = $VISANG->wwwroot . '/skills/frmgroup.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($baseurl));

// ===================================================================================================
// handles
if ($action == 'delete' && $groupid != 0 && confirm_sesskey()) {
    if (!$DB->get_record('vi_skill_groups', array('id' => $groupid))) {
        throw new moodle_exception('idnotfound');
    } else {
        $DB->delete_records('vi_skill_group_lists', array('skill_group_id' => $groupid));
        $DB->delete_records('vi_skill_groups', array('id' => $groupid));
        redirect($filterurl);
    }
}

// filters
$sql = '';
$params = array();
if ($search != '') {
    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params = array($searchfield => '%' . $search . '%');
}
$offset = ($page - 1) * $perpage;
$skill_groups = $DB->get_records_select('vi_skill_groups', $sql, $params, '{vi_skill_groups}.sortorder ASC, {vi_skill_groups}.name ASC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('vi_skill_groups', $sql, $params, 'COUNT(*)');
// ===================================================================================================
// renders
$title = get_string('skills:group', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'skill_groups';
$VISANG->page->navbar[] = get_string('skills', 'local_visang');
$VISANG->page->navbar[] = $title;

$VISANG->page->header();

$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table->id = 'skills_groups';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    get_string('name'),
    get_string('shortname'),
    get_string('order'),
    get_string('actions')
);
$table->data = array();

foreach ($skill_groups as $skill_group) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl, array('action' => 'edit', 'groupid' => $skill_group->id)),
        'fa fa-pencil-square-o',
        get_string('edit')
    );

    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'groupid' => $skill_group->id))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';

    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'groupid' => $skill_group->id)),
            $skill_group->name
        ),
        $skill_group->short_name,
        $skill_group->sortorder,
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <div class="pull-left">
                    <?php
                    echo html_writer::link(
                        new moodle_url($formurl, array('action' => 'add')),
                        get_string('skills:add_group', 'local_visang'),
                        array('class' => 'btn btn-primary')
                    );
                    ?>
                </div>
                <div class="pull-right">
                    <form method="get" class="form-inline">
                        <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                        <select name="searchfield" class="form-control">
                            <option value="name" <?php if ($searchfield == 'name') { ?> selected="selected" <?php } ?>> <?php echo get_string('name'); ?> </option>
                            <option value="short_name" <?php if ($searchfield == 'short_name') { ?> selected="selected" <?php } ?>><?php echo get_string('shortname'); ?></option>
                        </select>

                        <div class="input-group">
                            <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <?php
                echo html_writer::table($table);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset;
                    $_row->to = min($offset + $perpage, $totalcount);
                    $_row->total = $totalcount;
                    $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<?php
$VISANG->page->footer();
