<?php

require_once(dirname(__FILE__) . '/lib.php');
require_once($VISANG->dirroot . '/lib/skills.php');
require_once($CFG->libdir . '/excellib.class.php');

//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
require_login(0, false);
$current_language = current_language();

$sql = "SELECT j.*, e.company_name
        FROM {vi_jobs} j
        JOIN {vi_employers} e ON e.id = j.employer_id
        ORDER BY j.timecreated DESC";
$latest_jobs = $DB->get_records_sql($sql, array());

$fields = array(
    '#',
    'Date',
    'Job title',
    'Job description',
    'Skills',
    'Salary',
    'Employer',
    '한국어 활용 수준',
    '일반회원',
    'BOP',
    'Applications',
    'Hot job',
    'Confirmed',
    'Published',
    'Deleted'
);

$date = date('Y-m-d', time());
$filename = 'jobs-' . $date . '.xls';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);

$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');
$col = 0;
foreach ($fields as $fieldname) {
    $worksheet[0]->write(0, $col, $fieldname);
    $col++;
}

$row = 1;
$num = 1;

foreach ($latest_jobs as $job) {
    $salary = $DB->get_record('vi_job_salary_categories', array('id' => $job->job_salary_category_id));

    $_skills = [];
    $skill_list = visang_get_job_skills($job->id);
    foreach ($skill_list as $skill) {
        array_push($_skills, $skill->name);
    }

    $korean_level = '-';
    if ($job->korean_level_id > 1) {
        $_p = $DB->get_record('vi_korean_levels', array('id' => $job->korean_level_id));
        if ($_p) {
            switch ($current_language) {
                case 'ko':
                    $korean_level = $_p->name_ko;
                    break;
                case 'en':
                    $korean_level = $_p->name_en;
                    break;

                default:
                    $korean_level = $_p->name_vi;
                    break;
            }
        }
    }

    // $total_application_count = $DB->count_records('vi_job_applications', array('job_id' => $job->id));
    // usergroup = rs
    $application_count = $DB->count_records_sql("SELECT COUNT(DISTINCT a.id) 
        FROM {vi_job_applications} a
        JOIN {lmsdata_user} lu ON lu.userid = a.user_id
        WHERE a.job_id = :jobid AND lu.usergroup <> 'bp'", [
        'jobid' => $job->id
    ]);
    $bop_application_count = $DB->count_records_sql("SELECT COUNT(DISTINCT a.id) 
        FROM {vi_job_applications} a
        JOIN {lmsdata_user} lu ON lu.userid = a.user_id
        WHERE a.job_id = :jobid AND lu.usergroup = 'bp'", [
        'jobid' => $job->id
    ]);

    // ==============

    $col = 0;
    $worksheet[0]->write($row, $col++, $num++);
    $worksheet[0]->write($row, $col++, userdate($job->timecreated, get_string('strftimedatetimeshort')));
    $worksheet[0]->write($row, $col++, $job->title);
    $worksheet[0]->write($row, $col++, $job->short_description);
    $worksheet[0]->write($row, $col++, implode(', ', $$_skills));
    $worksheet[0]->write($row, $col++, $salary->name);
    $worksheet[0]->write($row, $col++, $job->company_name);
    $worksheet[0]->write($row, $col++, $korean_level);
    $worksheet[0]->write($row, $col++, $application_count);
    $worksheet[0]->write($row, $col++, $bop_application_count);
    $worksheet[0]->write($row, $col++, $application_count + $bop_application_count);
    $worksheet[0]->write($row, $col++, $job->hot_job ? 'Hot job' : '-');
    $worksheet[0]->write($row, $col++, $job->confirmed ? 'Confirmed' : '-');
    $worksheet[0]->write($row, $col++, $job->published ? 'Published' : '-');
    $worksheet[0]->write($row, $col++, $job->deleted ? 'Deleted' : '-');

    $row++;
}

$workbook->close();
die;
