<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');

$action = optional_param('action', '', PARAM_ALPHA);
$userid = optional_param('userid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'username', PARAM_RAW);

$baseurl = $VISANG->wwwroot . '/users.php';
$formurl = $VISANG->wwwroot . '/frmuser.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles

// filters
$sql = '';
$params = array();
if ($search != '') {
    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params = array($searchfield => '%' . $search . '%');
}
$offset = ($page - 1) * $perpage;
$users = $DB->get_records_select('user', $sql, $params, '{user}.username ASC', '*', $offset, $perpage);
$totalcount = $DB->count_records_select('user', $sql, $params, 'COUNT(*)');
// ===================================================================================================
// renders
$title = get_string('users', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'users';
$VISANG->page->navbar[] = $title;

$VISANG->page->header();

$table = new html_table();
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    get_string('username'),
    get_string('name'),
    get_string('email'),
    get_string('lastaccess'),
    get_string('actions')
);

$table->data = array();

foreach ($users as $user) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl, array('action' => 'edit', 'userid' => $user->id)),
        'fa fa-pencil-square-o',
        get_string('edit')
    );

    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'userid' => $user->id)),
            $user->username
        ),
        $user->firstname . ' ' . $user->lastname,
        $user->email,
        userdate($user->lastaccess, get_string('strftimedatetimeshort')),
        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <div class="pull-left">
                    <?php
                    echo html_writer::link(
                        new moodle_url($formurl, array('action' => 'add')),
                        get_string('users:add_user', 'local_visang'),
                        array('class' => 'btn btn-primary')
                    );
                    ?>
                </div>
                <div class="pull-right">
                    <form method="get" class="form-inline">
                        <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                        <select name="searchfield" class="form-control">
                            <option value="username" <?php if ($searchfield == 'username') { ?> selected="selected" <?php } ?>> <?php echo get_string('username'); ?> </option>
                            <option value="firstname" <?php if ($searchfield == 'firstname') { ?> selected="selected" <?php } ?>><?php echo get_string('firstname'); ?></option>
                            <option value="lastname" <?php if ($searchfield == 'lastname') { ?> selected="selected" <?php } ?>><?php echo get_string('lastname'); ?></option>
                            <option value="email" <?php if ($searchfield == 'email') { ?> selected="selected" <?php } ?>><?php echo get_string('email'); ?></option>
                        </select>

                        <div class="input-group">
                            <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <?php
                echo html_writer::table($table);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset;
                    $_row->to = min($offset + $perpage, $totalcount);
                    $_row->total = $totalcount;
                    $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<?php
$VISANG->page->footer();
