<?php

/**
 * Visang
 * This page reuses the simple components of the AdminLTE UI in the local/management package
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2021 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);

$current_language = current_language();
$now = new DateTime("now", core_date::get_server_timezone_object());
// ===================================================================================================
$from_year = optional_param('fyear', 0, PARAM_INT);
$from_mon = optional_param('fmon', 0, PARAM_INT);
$to_year = optional_param('tyear', 0, PARAM_INT);
$to_mon = optional_param('tmon', 0, PARAM_INT);
$view_mode = optional_param('v', 'month', PARAM_RAW);

if ($from_year == 0) $from_year = $now->format('Y');
if ($from_mon == 0) $from_mon = $now->format('m');
if ($to_year == 0) $to_year = $now->format('Y');
if ($to_mon == 0) $to_mon = $now->format('m');

$time_format = '%Y-%m';
if ($view_mode == 'day') {
    $time_format = '%Y-%m-%d';
}

$sql = "SELECT FROM_UNIXTIME(timecreated, '{$time_format}') AS mformat, COUNT(*) AS cnt
    FROM {logstore_standard_log} l
    LEFT JOIN {lmsdata_user} lu ON lu.userid = l.userid
    WHERE l.eventname = :eventname
        AND l.timecreated >= :starttime
        AND l.timecreated < :endtime
        [joinstatus]
    GROUP BY FROM_UNIXTIME(timecreated, '{$time_format}')
    ORDER BY FROM_UNIXTIME(timecreated, '{$time_format}') ASC;";

$params = [
    'eventname' => '\core\event\user_loggedin',
    'starttime' => date('U', strtotime("{$from_year}-{$from_mon}-01 00:00:00")),
    'endtime' => date('U', strtotime("{$to_year}-{$to_mon}-01 00:00:00 +1 month")),
    'joinstatus' => 'j'
];

// ===================================================================================================
// renders
$title = get_string('statitics:access', 'local_visang');
$VISANG->page->title = $title;
$VISANG->page->menu = 'access_statitics';
$VISANG->page->heading = $title;

$VISANG->page->addCSS($CFG->wwwroot . "/local/management/css/style.css");
$VISANG->page->header();
?>
<style>
    button:disabled {
        background-color: #f2f2f2 !important;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-body">
                <form name="frm_search" id="frm_search" class="search_area" action="access_statitics.php" method="get">
                    <input type="hidden" id="vmode" name="v" value="<?php echo $view_mode; ?>">
                    <div>
                        <label><?php echo get_string('statitics:period', 'local_visang') ?></label>
                        <?php if ($current_language != 'ko') : ?>
                            <span style="margin-left: 10px;"><?php echo get_string('statitics:year', 'local_visang') ?></span>
                        <?php endif; ?>
                        <select name="fyear" class="w_160" style="margin-right: 0px !important;">
                            <option value="0"> - <?php echo get_string('contents_now', 'local_lmsdata'); ?> -</option>
                            <?php
                            $years = lmsdata_get_years();
                            foreach ($years as $v => $y) {
                                $selected = '';
                                if ($v == $from_year) {
                                    $selected = ' selected';
                                }
                                echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                            }
                            ?>
                        </select>
                        <?php if ($current_language == 'ko') : ?>
                            <span style="margin-right: 10px;"><?php echo get_string('statitics:year', 'local_visang') ?></span>
                        <?php endif; ?>

                        <?php if ($current_language != 'ko') : ?>
                            <span style="margin-left: 10px;"><?php echo get_string('statitics:month', 'local_visang') ?></span>
                        <?php endif; ?>
                        <select name="fmon" class="w_160" style="margin-right: 0px !important;">
                            <option value="0"> - <?php echo get_string('contents_now', 'local_lmsdata'); ?> -</option>
                            <?php
                            $mons = lmsdata_get_mons($from_mon);
                            echo $mons;
                            ?>
                        </select>
                        <?php if ($current_language == 'ko') : ?>
                            <span style="margin-right: 10px;"><?php echo get_string('statitics:month', 'local_visang') ?></span>
                        <?php endif; ?>

                        <span>~</span>

                        <?php if ($current_language != 'ko') : ?>
                            <span style="margin-left: 10px;"><?php echo get_string('statitics:year', 'local_visang') ?></span>
                        <?php endif; ?>
                        <select name="tyear" class="w_160" style="margin-right: 0px !important;">
                            <option value="0"> - <?php echo get_string('contents_now', 'local_lmsdata'); ?> -</option>
                            <?php
                            $years = lmsdata_get_years();
                            foreach ($years as $v => $y) {
                                $selected = '';
                                if ($v == $to_year) {
                                    $selected = ' selected';
                                }
                                echo '<option value="' . $v . '"' . $selected . '> ' . $y . '</option>';
                            }
                            ?>
                        </select>
                        <?php if ($current_language == 'ko') : ?>
                            <span style="margin-right: 10px;"><?php echo get_string('statitics:year', 'local_visang') ?></span>
                        <?php endif; ?>

                        <?php if ($current_language != 'ko') : ?>
                            <span style="margin-left: 10px;"><?php echo get_string('statitics:month', 'local_visang') ?></span>
                        <?php endif; ?>
                        <select name="tmon" class="w_160" style="margin-right: 0px !important;">
                            <option value="0"> - <?php echo get_string('contents_now', 'local_lmsdata'); ?> -</option>
                            <?php
                            $mons = lmsdata_get_mons($to_mon);
                            echo $mons;
                            ?>
                        </select>
                        <?php if ($current_language == 'ko') : ?>
                            <span style="margin-right: 10px;"><?php echo get_string('statitics:month', 'local_visang') ?></span>
                        <?php endif; ?>

                        <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_management') ?>">
                    </div>
                </form>

                <div>
                    <button <?php echo $view_mode == 'month' ? 'disabled' : 'onclick="vtab(0)"' ?>>
                        <?php echo get_string('statitics:month', 'local_visang') ?>
                    </button>
                    <button <?php echo $view_mode == 'week' ? 'disabled' : 'onclick="vtab(1)"' ?>>
                        <?php echo get_string('statitics:week', 'local_visang') ?>
                    </button>
                    <button <?php echo $view_mode == 'day' ? 'disabled' : 'onclick="vtab(2)"' ?>>
                        <?php echo get_string('statitics:dayofweek', 'local_visang') ?>
                    </button>
                    <button class="blue_btn" onclick="excel_download();">
                        <?php echo get_string('excell_down', 'local_lmsdata'); ?>
                    </button>
                </div>

                <?php if ($view_mode == 'month') : ?>
                    <table>
                        <thead>
                            <tr>
                                <th width="20%"><?php echo get_string('statitics:month', 'local_visang') ?></th>
                                <th width="30%"><?php echo get_string('statitics:access_count', 'local_visang') ?> (masterkorean)</th>
                                <th width="30%"><?php echo get_string('statitics:access_count', 'local_visang') ?> (jobs)</th>
                                <th width="20%"><?php echo get_string('statitics:total', 'local_visang') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);
                            $joinstatus = 'AND lu.joinstatus = :joinstatus';
                            $job_statitics = $DB->get_records_sql(str_replace('[joinstatus]', $joinstatus, $sql), $params);
                            $year = '';

                            foreach ($statitics as $mon => $stat) {
                                $month = explode('-', $mon);
                                echo '<tr>';
                                echo '<td>';
                                switch ($current_language) {
                                    case 'en':
                                        echo date('M', strtotime($mon . '-01'));
                                        break;
                                    case 'vi':
                                        echo get_string('statitics:month', 'local_visang') . ' ' . intval($month[1]);
                                        break;
                                    case 'ko':
                                        echo intval($month[1]) . get_string('statitics:month', 'local_visang');
                                        break;
                                }

                                if ($year != $month[0]) {
                                    echo ' (' . $month[0] . ')';
                                    $year = $month[0];
                                }

                                $ecnt = $stat->cnt - $job_statitics[$mon]->cnt;
                                $jcnt = $job_statitics[$mon]->cnt;
                                echo '</td>';
                                echo '<td>' . ($ecnt > 0 ? $ecnt : '-') . '</td>';
                                echo '<td>' . ($jcnt > 0 ? $jcnt : '-') . '</td>';
                                echo '<td>' . ($stat->cnt > 0 ? $stat->cnt : '-') . '</td>';
                                echo '</tr>';
                            }

                            if (count($statitics) == 0) {
                                echo "<tr><td colspan=\"4\">" . get_string('user:nodata', 'local_management') . "</td></tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                <?php endif; ?>

                <?php if ($view_mode == 'week') : ?>
                    <table>
                        <thead>
                            <tr>
                                <th width="20%"><?php echo get_string('statitics:month', 'local_visang') ?></th>
                                <th width="20%"><?php echo get_string('statitics:week', 'local_visang') ?></th>
                                <th width="20%"><?php echo get_string('statitics:access_count', 'local_visang') ?> (masterkorean)</th>
                                <th width="20%"><?php echo get_string('statitics:access_count', 'local_visang') ?> (jobs)</th>
                                <th width="20%"><?php echo get_string('statitics:total', 'local_visang') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);

                            foreach ($statitics as $mon => $stat) {
                                $data = [];
                                $month = explode('-', $mon);
                                $start = "{$mon}-01";
                                $end = date('Y-m-d', strtotime("{$start} +1 month"));
                                $week = 1;
                                while ($start < $end) {
                                    $params['starttime'] = date('U', strtotime($start));
                                    if (strtotime("{$start} +7 days") < strtotime($end)) {
                                        $params['endtime'] = date('U', strtotime("{$start} +7 days"));
                                    } else {
                                        $params['endtime'] = date('U', strtotime($end));
                                    }
                                    $_statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);
                                    $_job_statitics = $DB->get_records_sql(str_replace('[joinstatus]', 'AND lu.joinstatus = :joinstatus', $sql), $params);

                                    $ecnt = '-';
                                    $jcnt = '-';
                                    $cnt = '-';
                                    if (count($_statitics)) {
                                        $_t = array_pop($_statitics);
                                        $_j = array_pop($_job_statitics);
                                        $ecnt = $_t->cnt - $_j->cnt;
                                        if ($_j) $jcnt = $_j->cnt;
                                        $cnt = $_t->cnt;
                                    }

                                    $data[$week] = [
                                        'e' => $ecnt,
                                        'j' => $jcnt,
                                        'total' => $cnt,
                                    ];

                                    $start = date('Y-m-d', strtotime("{$start} +7 days"));
                                    $week++;
                                }

                                echo "<tr>";

                                echo "<td rowspan=\"" . count($data) . "\">";
                                switch ($current_language) {
                                    case 'en':
                                        echo date('M', strtotime($month[0] . '-' . $month[1] . '-01'));
                                        break;
                                    case 'vi':
                                        echo get_string('statitics:month', 'local_visang') . ' ' . intval($month[1]);
                                        break;
                                    case 'ko':
                                        echo intval($month[1]) . get_string('statitics:month', 'local_visang');
                                        break;
                                }
                                if ($year != $month[0]) {
                                    echo ' (' . $month[0] . ')';
                                    $year = $month[0];
                                }
                                echo "</td>";

                                echo '<td>';
                                switch ($current_language) {
                                    case 'en':
                                        echo get_string('statitics:week', 'local_visang') . ' 1';
                                        break;
                                    case 'vi':
                                        echo get_string('statitics:week', 'local_visang') . ' 1';
                                        break;
                                    case 'ko':
                                        echo '1' . get_string('statitics:week', 'local_visang');
                                        break;
                                }
                                echo '</td>';

                                echo "<td>{$data[1]['e']}</td>
                                    <td>{$data[1]['j']}</td>
                                    <td>{$data[1]['total']}</td>";

                                echo "</tr>";

                                for ($i = 2; $i <= count($data); $i++) {
                                    echo "<tr>";
                                    echo '<td>';
                                    switch ($current_language) {
                                        case 'en':
                                            echo get_string('statitics:week', 'local_visang') . ' ' . $i;
                                            break;
                                        case 'vi':
                                            echo get_string('statitics:week', 'local_visang') . ' ' . $i;
                                            break;
                                        case 'ko':
                                            echo $i . get_string('statitics:week', 'local_visang');
                                            break;
                                    }
                                    echo '</td>';

                                    echo "<td>{$data[$i]['e']}</td>
                                        <td>{$data[$i]['j']}</td>
                                        <td>{$data[$i]['total']}</td>";
                                    echo "</tr>";
                                }
                            }

                            if (count($statitics) == 0) {
                                echo "<tr><td colspan=\"5\">" . get_string('user:nodata', 'local_management') . "</td></tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                <?php endif; ?>

                <?php if ($view_mode == 'day') : ?>
                    <table>
                        <thead>
                            <tr>
                                <th width="20%"><?php echo get_string('statitics:dayofweek', 'local_visang') ?></th>
                                <th width="30%"><?php echo get_string('statitics:access_count', 'local_visang') ?> (masterkorean)</th>
                                <th width="30%"><?php echo get_string('statitics:access_count', 'local_visang') ?> (jobs)</th>
                                <th width="20%"><?php echo get_string('statitics:total', 'local_visang') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $statitics = $DB->get_records_sql(str_replace('[joinstatus]', '', $sql), $params);
                            $job_statitics = $DB->get_records_sql(str_replace('[joinstatus]', 'AND lu.joinstatus = :joinstatus', $sql), $params);
                            $year = '';

                            foreach ($statitics as $mon => $stat) {
                                $month = explode('-', $mon);
                                echo '<tr>';
                                echo '<td>';
                                echo get_string('statitics:' . date('D', strtotime($mon)), 'local_visang') . " " . date('d/m/Y', strtotime($mon));

                                if ($year != $month[0]) {
                                    $year = $month[0];
                                }

                                $ecnt = $stat->cnt - $job_statitics[$mon]->cnt;
                                $jcnt = $job_statitics[$mon]->cnt;
                                echo '</td>';
                                echo '<td>' . ($ecnt > 0 ? $ecnt : '-') . '</td>';
                                echo '<td>' . ($jcnt > 0 ? $jcnt : '-') . '</td>';
                                echo '<td>' . ($stat->cnt > 0 ? $stat->cnt : '-') . '</td>';
                                echo '</tr>';
                            }

                            if (count($statitics) == 0) {
                                echo "<tr><td colspan=\"4\">" . get_string('user:nodata', 'local_management') . "</td></tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<script>
    function vtab(tab) {
        var tabs = ['month', 'week', 'day'];
        $('#frm_search #vmode').val(tabs[tab]);
        $('#frm_search').submit();
    }

    function excel_download() {
        var form = $('#frm_search');
        var action = form.attr('action');
        form.attr('action', 'access_statitics_excel_down.php');
        form.submit();
        form.attr('action', action);
        return false;
    }
</script>
<?php
$VISANG->page->footer();
