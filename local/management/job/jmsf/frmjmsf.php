<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');
require_once($VISANG->dirroot . '/jmsf/forms/visang_jmsf_form.php');


$action = optional_param('action', 'add', PARAM_ALPHA);
$skillid = optional_param('skillid', 0, PARAM_INT);
$returnto = optional_param('returnto', '', PARAM_ALPHA);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/jmsf/frmjmsf.php', array('action' => $action, 'skillid' => $skillid, 'returnto' => $returnto));
$returnurl = new moodle_url($VISANG->wwwroot . '/jmsf/index.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
// $PAGE->set_url(new moodle_url($baseurl));
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
// $mform = new visang_jmsf_form($baseurl, array('action' => $action, 'skillid' => $skillid, 'returnurl' => $returnurl),'post',array('enctype' => 'multipart/form-data'));
$mform = new visang_jmsf_form($baseurl, array('action' => $action, 'skillid' => $skillid, 'returnurl' => $returnurl, 'returnto' => $returnto),'post', '', array('enctype' => 'multipart/form-data'));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_submitted_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = 'Job matching feedback';
} else {
    $skill = $DB->get_record('vi_job_feedback', array('id' => $skillid));
    if ($skill == null) {
        throw new moodle_exception('idnotfound');
    } else {
        $mform->set_data($skill);
    }
    $title = 'Edit feedback' . ': ' . $skill->username;
}

// ===================================================================================================
// renders
$VISANG->page->title = 'Job feedback';
$VISANG->page->heading = $title;
$VISANG->page->menu = 'jmsf';
$VISANG->page->navbar[] = 'Job feedback';
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
?>
<script src="/local/management/job/js/ckeditor/ckeditor.js"></script>
<!--<script>-->
<!--    CKEDITOR.replace( 'content', {height: '500px',extraPlugins: 'imageuploader',} );-->
<!--    -->
<!--</script>-->
<script>
    var editor=CKEDITOR.replace( 'content',{height: '500px',
        extraPlugins : 'filebrowser',
        filebrowserBrowseUrl:'browser.php?type=Images',
        filebrowserUploadMethod:"form",
        filebrowserUploadUrl:"/local/management/job/jmsf/upload.php"
    });
</script>
<?php
echo html_writer::end_tag('section');

$VISANG->page->footer();

?>

