<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/../lib.php');

$action = optional_param('action', '', PARAM_ALPHA);
$skillid = optional_param('skillid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = $DB->sql_like_escape(optional_param('search', '', PARAM_RAW));
$searchfield = optional_param('searchfield', 'username', PARAM_RAW);



$sesskey = sesskey();

// $baseurl = $VISANG->wwwroot . '/skills/list.php';
// $formurl = $VISANG->wwwroot . '/jmsf/frmjmsf.php';
$baseurl = $VISANG->wwwroot . '/jmsf/index.php';
// $formurl = $VISANG->wwwroot . '/skills/frmskill.php';
$formurl = $VISANG->wwwroot . '/jmsf/frmjmsf.php';

$filterurl = new moodle_url($baseurl, array(
    'search' => $search,
    'searchfield' => $searchfield,
    'page' => $page,
    'perpage' => $perpage
));
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url($baseurl);
// ===================================================================================================
// handles
if ($action == 'delete' && $skillid != 0 && confirm_sesskey()) {
    if (!$DB->record_exists('vi_job_feedback', array('id' => $skillid))) {
        throw new moodle_exception('idnotfound');
    } else {
        $DB->delete_records('vi_job_feedback', array('id' => $skillid));
        redirect($filterurl);
    }
}

// filters
$sql = '';
$params = array();
if ($search != '') {
    $sql = $DB->sql_like($searchfield, ':' . $searchfield, false);
    $params = array($searchfield => '%' . $search . '%');

}
$offset = ($page - 1) * $perpage;
$skills = $DB->get_records_select('vi_job_feedback', $sql, $params, ' {vi_job_feedback}.timecreated DESC', '*');
// $jmsf=$DB->get_records_sql($sql, $params_array, $limitfrom, $limitnum);
$totalcount = $DB->count_records_select('vi_job_feedback', $sql, $params, 'COUNT(*)');
// ===================================================================================================
// renders
$title = 'Job feedback';
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'jmsf';
$VISANG->page->navbar[] = 'jmsf';

$VISANG->page->header();

$table = new html_table();
$table->colclasses = array('leftalign', 'leftalign', 'leftalign', 'leftalign');
$table->id = 'skills_list';
$table->attributes['class'] = 'table table-bordered dataTable';
$table->head = array(
    'name',
    'title',
    'user_avatar_url',
    'company',
    'job_position',
    'post date',

);

$table->data = array();

foreach ($skills as $skill) {
    $actions = '';
    // edit
    $actions .= $VISANG->page->table_action_button(
        new moodle_url($formurl, array('action' => 'edit', 'skillid' => $skill->id)),
        'fa fa-pencil-square-o'

    );

    $actions .= '<div class="btn-group">
      <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="' . (new moodle_url($filterurl, array('action' => 'delete', 'sesskey' => sesskey(), 'skillid' => $skill->id))) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> ' . get_string('delete') . '</a></li>
      </ul>
    </div>';
    $logo = $skill->user_avatar_url == '' ? $CFG->wwwroot . '/theme/oklassedu/pix/images/default_emp.png' : $CFG->wwwroot . '/pluginfile.php/' . $skill->user_avatar_url . '?preview=thumb';
    $row = array(
        html_writer::link(
            new moodle_url($formurl, array('action' => 'edit', 'skillid' => $skill->id)),
            $skill->username
        ),
        $skill->title,
        '<div class="media">
        ' . html_writer::div(html_writer::empty_tag('img', array('src' => $logo, 'class' => 'media-object', 'width' => '60')), 'media-left') . '
        </div>',
        $skill->company_name,
        $skill->job_position,
        userdate($skill->timecreated, get_string('strftimedatetimeshort')),

        html_writer::div($actions, 'd-flex'),
    );

    $table->data[] = $row;
}

if (count($table->data) == 0) {
    $emptyrow = new html_table_row();
    $emptyrow->cells[0] = new html_table_cell('<center>' . get_string('empty:data', 'local_visang') . '</center>');
    $emptyrow->cells[0]->colspan = count($table->head);
    $table->data[] = $emptyrow;
}
?>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <div class="pull-left">
                    <?php
                    echo html_writer::link(
                        new moodle_url($formurl, array('action' => 'add')),
                        "Add",
                        array('class' => 'btn btn-primary')
                    );
                    ?>
                </div>
                <div class="pull-right">
                    <form method="get" class="form-inline">
                        <input type="hidden" name="perpage" value="<?php echo $perpage; ?>">
                        <select name="searchfield" class="form-control">
                            <option value="username" <?php if ($searchfield == 'username') { ?> selected="selected" <?php } ?>> <?php echo get_string('name'); ?> </option>
                            <option value="title" <?php if ($searchfield == 'title') { ?> selected="selected" <?php } ?>>Title</option>
                            <option value="company_name" <?php if ($searchfield == 'company_name') { ?> selected="selected" <?php } ?>>Company</option>
                            <option value="job_position" <?php if ($searchfield == 'job_position') { ?> selected="selected" <?php } ?>>Job position</option>
                            <option value="content" <?php if ($searchfield == 'content') { ?> selected="selected" <?php } ?>>Content</option>

                        </select>

                        <div class="input-group">
                            <input class="form-control pull-right" type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo get_string('search'); ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <?php
                echo html_writer::table($table);
                ?>
            </div>
            <div>
                <div class="pull-left">
                    <?php
                    $_row = new stdClass();
                    $_row->from = $offset;
                    $_row->to = min($offset + $perpage, $totalcount);
                    $_row->total = $totalcount;
                    $VISANG->page->table_perpage($perpage, get_string('oftotalrows', 'local_visang', $_row), array('searchfield' => $searchfield, 'search' => $search));
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                    $VISANG->page->table_pagination($filterurl, null, ceil($totalcount / $perpage), $page);
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<?php
$VISANG->page->footer();
