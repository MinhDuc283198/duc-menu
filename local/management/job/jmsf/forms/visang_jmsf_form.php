<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

defined('MOODLE_INTERNAL') || die();
require_once($VISANG->dirroot . '/classes/formslib.php');

// require_once($VISANG->dirroot . './local/management/job/classes/formslib.php');
// require_once($CFG->libdir . './lib/formslib.php');


class visang_jmsf_form extends moodleform
{
    public $renderer;

    function definition()
    {
        global $CFG, $DB, $USER, $VISANG;

        $mform = $this->_form;
        $this->renderer = new \local_visang\form_renderer($mform);

        $action = $this->_customdata['action'];
        $skillid = $this->_customdata['skillid'];
        $returnurl = $this->_customdata['returnurl'];
        $returnto = $this->_customdata['returnto'];

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_RAW);
        $mform->setDefault('action', $action);

        $mform->addElement('hidden', 'skillid');
        $mform->setType('skillid', PARAM_INT);
        $mform->setDefault('skillid', $skillid);

        $mform->addElement('hidden', 'returnto');
        $mform->setType('returnto', PARAM_RAW);
        $mform->setDefault('returnto', $returnto);
        // ==========================================================================================
        $mform->addElement('header', 'general', get_string('general'));

        $mform->addElement('input', 'username', get_string('name'), array('required', 'class' => 'form-control'));
        $mform->addRule('username', null, 'required', null, 'server');
        $mform->setType('username', PARAM_RAW);

        $mform->addElement('text', 'title', 'title', array('required', 'class' => 'form-control'));
        $mform->addRule('title', null, 'required', null, 'server');
        $mform->setType('title', PARAM_RAW);

        $mform->addElement('input', 'company_name', 'company_name', array('required', 'class' => 'form-control'));
        $mform->addRule('company_name', null, 'required', null, 'server');
        $mform->setType('company_name', PARAM_RAW);

        $mform->addElement('input', 'job_position', 'job_position', array('required', 'class' => 'form-control'));
        $mform->addRule('job_position', null, 'required', null, 'server');
        $mform->setType('job_position', PARAM_RAW);


        $mform->addElement('textarea', 'content', 'title');
        $mform->setType('content', PARAM_RAW);
        // if ($action == 'edit') {
        //     $mform->addElement('input', 'url', get_string('employers:employers_url', 'local_visang'), array('required', 'class' => 'form-control'));
        //     $mform->addRule('url', null, 'required', null, 'server');
        //     $mform->setType('url', PARAM_RAW);
        // }
        // $mform->addElement('filepicker', 'userfile', get_string('file'), null,
        // array('maxbytes' => 60, 'accepted_types' => '*'));
        $mform->addElement('advfile', 'user_avatar_url', 'user_avatar_url', null, array('maxfiles' => 1, 'accepted_types' => 'image/*;capture=camera'));
        // $mform ->addElement('advfile' ,  'avatar_url' , get_string ( 'file' ) ,null,
        // array( 'maxbytes'  => 600 ,  'accepted_types' => 'image/*;capture=camera') ) ;

        // $mform->addElement('header', 'links', get_string('employers:employers_links', 'local_visang'));
        // if ($action == 'edit') {
        //     $mform->addElement('input', 'url', get_string('employers:employers_url', 'local_visang'), array('required', 'class' => 'form-control'));
        //     $mform->addRule('url', null, 'required', null, 'server');
        //     $mform->setType('url', PARAM_RAW);
        // }
        // $mform->addElement('advfile', 'user_avatar_url', get_string('file'), null,
        // array('maxbytes' => 600, 'accepted_types' => 'image/*;capture=camera'));





        // $mform->addElement('filemanager', 'attachments', get_string('attachment', 'moodle'), null,
        // array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 50,
        //       'accepted_types' => array('document'), 'return_types'=> FILE_INTERNAL | FILE_EXTERNAL));
        // ==========================================================================================
        $actions = array();
        $actions[] = &$mform->createElement('xbutton', 'submitbutton', get_string('savechanges'), array('class' => 'btn btn-primary btn-lg margin-r-5', 'value' => 'submit'));
        $actions[] = &$mform->createElement('html', html_writer::link($returnurl, get_string('cancel'), array('class' => 'btn btn-default btn-lg')));
        $mform->addElement('formactions', 'form_actions', '', $actions);


    }

    function save($data)
    {
        global $DB;
        $logo = $this->_form->getElement('user_avatar_url')->save_file($data->user_avatar_url);

        $content = new stdClass();
        $content->username = $data->username;
        $content->user_avatar_url = count($logo) ? $logo[0] : '';

        // $content->user_avatar_url=$data->user_avatar_url;

        $content->title=$data->title;
        $content->company_name=$data->company_name;
        $content->job_position=$data->job_position;
        $content->content=$data->content;
        $content->timemodified = time();
        // echo '<pre>';
        // var_dump($logo);
        // echo '</pre>';
        // die();


        switch ($data->action) {
            case 'add':

                $content->timecreated = time();
                $DB->insert_record('vi_job_feedback', $content);
                break;
            case 'edit':
                $content->id = $data->skillid;
                $DB->update_record('vi_job_feedback', $content);
                break;
        }
    }
}
