<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__) . '/forms/visang_user_form.php');

$action = optional_param('action', 'add', PARAM_ALPHA);
$userid = optional_param('userid', 0, PARAM_INT);

if (!in_array($action, array('add', 'edit'))) {
    throw new moodle_exception('invalidaccess');
}

$baseurl = new moodle_url($VISANG->wwwroot . '/frmuser.php', array('action' => $action, 'userid' => $userid));
$returnurl = new moodle_url($VISANG->wwwroot . '/users.php');
require_login(0, false);
//$context = context_system::instance();
//require_visang_capability($context);
//$PAGE->set_context($context);
$PAGE->set_url(new moodle_url($baseurl));
// ===================================================================================================
// handles
$mform = new visang_user_form($baseurl, array('action' => $action, 'userid' => $userid, 'returnurl' => $returnurl));

if ($mform->is_submitted()) {
    $submitbutton = optional_param('submitbutton', 'cancel', PARAM_ALPHA);
    $formdata = $mform->get_data();
    if (!is_null($formdata) && $submitbutton == 'submit') {
        $mform->save($formdata);
        redirect($returnurl);
    } else if ($submitbutton == 'cancel') {
        redirect($returnurl);
    }
}

if ($action === 'add') {
    $title = get_string('users:add_user', 'local_visang');
} else {
    $user = $DB->get_record('user', array('id' => $userid));
    if ($user == null) {
        throw new moodle_exception('idnotfound');
    } else {
        unset($user->password);
        $mform->set_data($user);
    }
    $title = get_string('users:edit_user', 'local_visang') . ': ' . $user->firstname . ' ' . $user->lastname;
}

// ===================================================================================================
// renders
$VISANG->page->title = $title;
$VISANG->page->heading = $title;
$VISANG->page->menu = 'users';
$VISANG->page->navbar[] = get_string('users', 'local_visang');
$VISANG->page->navbar[] = $title;
$VISANG->page->header();

echo html_writer::start_tag('section', array('class' => 'content'));
$mform->renderer->display();
echo html_writer::end_tag('section');

$VISANG->page->footer();
