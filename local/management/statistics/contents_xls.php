<?php

ini_set('memory_limit', '512M');
ini_set('max_execution_time', 0);

require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");

//fileDownload.js 사용을 위한 셋팅
header('Set-Cookie: fileDownload=true; path=/');

$downloadtype = required_param('download_type', PARAM_TEXT);
$startdate = required_param('start_date', PARAM_TEXT);
$enddate = required_param('end_date', PARAM_TEXT);
$filenames = [
        'blended_learning_student_list' => '집체교육_참가자_목록',
        'statistics_library' => '라이브러리_통계',
        'statistics_library_with_progress' => '라이브러리_통계(진도율_포함)',
        'logged_in_student_list' => '로그인_통계',
        'logged_in_blended_learning_student_list' => '로그인_통계(집합교육_대상자)',
        'input_socialdata_student_list' => '추가정보(자기소개,해시태그,학력정보)_입력_통계',
        'input_socialdata_blended_learning_student_list' => '추가정보(자기소개,해시태그,학력정보)_입력_통계(집합교육_대상자)',
        'input_extradata_student_list' => '추가정보(직무,관심분야_전문주제)_입력_통계',
        'input_extradata_blended_learning_student_list' => '추가정보(직무,관심분야_전문주제)_입력_통계(집합교육_대상자)',
        'video_viewed_student_list' => '비디오_시청_통계',
        'video_viewed_blended_learning_student_list' => '비디오_시청_통계(집합교육_대상자)'
];
if ($downloadtype === 'statistics_library_with_progress') {
    $filename = clean_filename($filenames[$downloadtype]) . '.xls';
} else {
    $filename =
            clean_filename($filenames[$downloadtype]) . '_' . clean_filename($startdate) . '-' . clean_filename($enddate) . '.xls';
}

$workbook = new MoodleExcelWorkbook($filename);

$worksheet1 = $workbook->add_worksheet('Data');
$cellformat = [
        'header' => [
                'v_aligh' => 'center',
                'text_wrap' => true,
                'bg_color' => '#e0e0e0'
        ],
        'header-center' => [
                'align' => 'center',
                'v_align' => 'center',
                'text_wrap' => true,
                'bg_color' => '#f0f0f0'
        ],
        'data' => [
                'v_align' => 'center',
                'text_wrap' => true
        ]
];

$xlsheaderscodeinfo = [
        'code' => [
                'blended_learning_student_list' => 'E201',
                'statistics_library_with_progress' => 'C101',
                'statistics_library' => 'C102',
                'logged_in_student_list' => 'L301',
                'logged_in_blended_learning_student_list' => 'L302',
                'input_socialdata_student_list' => 'L311',
                'input_socialdata_blended_learning_student_list' => 'L312',
                'input_extradata_student_list' => 'L313',
                'input_extradata_blended_learning_student_list' => 'L314',
                'video_viewed_student_list' => 'C111',
                'video_viewed_blended_learning_student_list' => 'C112'
        ],
        'name' => [
                'blended_learning_student_list' => '집체교육 참가자 목록',
                'statistics_library_with_progress' => '모든 유형의 라이브러리 통계(조회일시, 조회자, 콘텐츠)',
                'statistics_library' => '모든 유형의 라이브러리 통계(조회일시, 조회자, 콘텐츠, 진도율)',
                'logged_in_student_list' => '기간 내 로그인 한 사용자',
                'logged_in_blended_learning_student_list' => '기간 내 로그인 한 집합교육 대상 사용자',
                'input_socialdata_student_list' => '기간 내 추가정보(자기소개, 해시태그, 학력정보)를 입력한 사용자',
                'input_socialdata_blended_learning_student_list' => '기간 내 추가정보(자기소개, 해시태그, 학력정보)를 입력한 집합교육 대상 사용자',
                'input_extradata_student_list' => '기간 내 추가정보(직무, 관심주제, 전문분야)를 입력한 사용자(중복)',
                'input_extradata_blended_learning_student_list' => '기간 내 추라정보(직무, 관심주제, 전문분야)를 입력한 집합교육 대상 사용자(중복)',
                'video_viewed_student_list' => '기간 내 비디오를 시청한 사용자(시청수 포함)',
                'video_viewed_blended_learning_student_list' => '기간 내 비디오를 시청한 집합교육 대상 사용자(시청수 포함)'
        ]
];
$xlsheaders = [
        'blended_learning_student_list' => [
                '과정명',
                '차수',
                '시작일',
                '종료일',
                '사용자명',
                '이메일',
                '회사명',
                '직위',
                '직무코드',
                '직무',
                '직군',
                '교육구분'
        ],
        'statistics_library_with_progress' => [
                'id',
                '조회일시',
                '조회자',
                '회사명',
                '콘텐츠id',
                '진도율',
                '카테고리1',
                '카테고리2',
                '유형',
                '제목',
                '등록일시'
        ],
        'statistics_library' => [
                '조회일시',
                '조회자',
                '회사명',
                '콘텐츠id',
                '카테고리1',
                '카테고리2',
                '출처',
                '유형',
                '제목',
                '등록일시'
        ],
        'logged_in_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)'
        ],
        'logged_in_blended_learning_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)'
        ],
        'input_socialdata_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)',
                '자기소개입력여부',
                '기간내입력해시태그수',
                '기간내입력학력정보수'
        ],
        'input_socialdata_blended_learning_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)',
                '자기소개입력여부',
                '기간내입력해시태그수',
                '기간내입력학력정보수'
        ],
        'input_extradata_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)',
                '관심주제(상위)',
                '관심주제(하위)',
                '전문분야(상위)',
                '전문분야(하위)'
        ],
        'input_extradata_blended_learning_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)',
                '관심주제(상위)',
                '관심주제(하위)',
                '전문분야(상위)',
                '전문분야(하위)'
        ],
        'video_viewed_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)',
                '기간내비디오시청수'
        ],
        'video_viewed_blended_learning_student_list' => [
                '사용자id',
                '마지막 로그인 일시',
                '사번',
                '이메일',
                '회사명',
                '직위',
                '직무(상위)',
                '직무(하위)',
                '기간내비디오시청수'
        ]
];

if ($startdate === '') {
    $results = $DB->get_records_sql('EXEC {proc_' . $downloadtype . '} :enddate',
            ['enddate' => $enddate.' 23:59:59']);
} else {
    $results = $DB->get_records_sql('EXEC {proc_' . $downloadtype . '} :enddate, :startdate',
            ['enddate' => $enddate.' 23:59:59', 'startdate' => $startdate]);
}

$row = 0;
$col = 0;
$worksheet1->write_string($row, $col++, $xlsheaderscodeinfo['code'][$downloadtype], ['color'=>'#b00035', 'bg_color'=>'#ffff00']);
$worksheet1->write_string($row, $col++, $xlsheaderscodeinfo['name'][$downloadtype], ['bg_color'=>'#ffff00']);
for($i=2; $i<count($xlsheaders[$downloadtype]); $i++) {
    $worksheet1->write_string($row, $col++, '', ['bg_color'=>'#ffff00']);
}
$row++;

if (!empty($results)) {
    foreach ($results as $result) {
        $col = 0;
        $result = get_object_vars($result);
        foreach ($xlsheaders[$downloadtype] as $key) {
            if ($row === 1) {
                $worksheet1->write_string($row, $col, $key, $cellformat['header-center']);
                $worksheet1->write_string($row+1, $col++, (array_key_exists($key, $result) ? $result[$key] : 'NULL'),
                        $cellformat['data']);
            }
            else {
                $worksheet1->write_string($row, $col++, (array_key_exists($key, $result) ? $result[$key] : 'NULL'),
                        $cellformat['data']);
            }
        }
        if ($row === 1) {
            $row = 3;
        } else {
            $row++;
        }
    }
}

$workbook->close();
