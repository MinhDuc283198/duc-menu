function show_loading() {
    if ($("#loading").length == 0) {
        $("body").append("<div id='loading'></div>");
    }
}

function hide_loading() {
    if ($("#loading").length != 0) {
        $("#loading").remove();
    }
}

$(document).ready(function () {
    //다운로드 사유
    var buttoncontext = null;
    $(".xlsdown").click(function () {
        buttoncontext = this;
        $("#xlsdownlayer").show();
        return false;
    });
    $("#xlsdownlayer .close").click(function () {
        buttoncontext = undefined;
        $(this).closest("#xlsdownlayer").hide();
    });

    $(document).on('click', '.cancle_reason', function () {
        buttoncontext = undefined;
        $("#xlsdownlayer").hide();
    });
    $(document).on('click', '.submit_reason', function () {
        show_loading();
        var courseid = 0;
        var xlsurl = 'contents_xls.php';
        var button = $(this);
        button.hide();
        var reason = $('textarea[name=download_reasoninput]').val();
        var url_split = window.location.hostname;
        var url_split2 = window.location.href;
        var url = url_split2.split(url_split);
        var formData = new FormData();
        formData.append("reason", '[ 통계 다운로드 ]' + reason);
        formData.append("url", url[1]);
        formData.append("courseid", courseid);
        $.ajax({
            url: "/local/dlp/download_reason.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                if (data) {
                    var formData = $(buttoncontext).parent().serialize();
                    $.fileDownload(xlsurl, {httpMethod: "POST", data: formData}).done(function () {
                    }).done(function() {
                        hide_loading();
                    }).fail(function () {
                        alert('다운로드에 실패하였습니다. 관리자에게 문의 주세요.');
                        hide_loading();
                    });
                } else {
                    alert('사유 등록에 실패하였습니다. 다시 시도해주세요.');
                    hide_loading();
                }
            },
            complete: function() {
                $('textarea[name=download_reasoninput]').val("");
                button.show();
                $("#xlsdownlayer").hide();
            }
        });
    });
});
