<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
$pagesettings = array(
        'title' => '엑셀 다운로드',
        'heading' => '엑셀 다운로드',
        'subheading' => '',
        'menu' => 'statistics',
        'js' => array(),
        'css' => array(),
        'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');

if (!is_siteadmin()) {
    redirect($CFG->wwwroot);
}

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h5 class="box-title">
                <strong style="color:#b00035">E201</strong> 집체교육 참가자 목록
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="blended_learning_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 집체교육 참가자 목록</li>
                    <li>프로시져명 : [dbo].[m_proc_blended_learning_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_blended_learning_student_list] [종료일] [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>과정명</th>
                        <th>차수</th>
                        <th>시작일</th>
                        <th>종료일</th>
                        <th>사용자명</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무코드</th>
                        <th>직무</th>
                        <th>직군</th>
                        <th>교육구분</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>DLP의과정</td>
                        <td>1</td>
                        <td>2019-03-25<br>00:00:00</td>
                        <td>2019-08-25<br>23:59:59</td>
                        <td>홍길동</td>
                        <td>testdlp101<br>@lgagademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>5500</td>
                        <td>SCM</td>
                        <td>Supply Chain Planning</td>
                        <td>lgacademy</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">C101</strong> 라이브러리 통계
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="statistics_library">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 모든 유형의 라이브러리 통계 (조회일시, 조회자, 콘텐츠)</li>
                    <li>프로시져명 : [dbo].[m_proc_statistics_library]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_statistics_library] [종료일] [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>조회일시</th>
                        <th>조회자</th>
                        <th>회사명</th>
                        <th>콘텐츠ID</th>
                        <th>카테고리1</th>
                        <th>카테고리2</th>
                        <th>출처</th>
                        <th>유형</th>
                        <th>제목</th>
                        <th>등록일시</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>12345</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>홍길동</td>
                        <td>2123</td>
                        <td>2900</td>
                        <td>2902</td>
                        <td>video</td>
                        <td>기상 이변, 트렌드를 바꾸다</td>
                        <td>2019-03-24<br>17:29:59</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">C102</strong> 라이브러리 통계(진도율 포함)
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="statistics_library_with_progress">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 모든 유형의 라이브러리 통계 (조회일시, 조회자, 콘텐츠, 진도율)</li>
                    <li>프로시져명 : [dbo].[m_proc_statistics_library_with_progress]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_statistics_library_with_progress] [종료일] [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>id</th>
                        <th>조회일시</th>
                        <th>조회자</th>
                        <th>회사명</th>
                        <th>콘텐츠ID</th>
                        <th>진도율</th>
                        <th>카테고리1</th>
                        <th>카테고리2</th>
                        <th>유형</th>
                        <th>제목</th>
                        <th>등록일시</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>12345</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>홍길동</td>
                        <td>2123</td>
                        <td>50%</td>
                        <td>2900</td>
                        <td>2902</td>
                        <td>video</td>
                        <td>기상 이변, 트렌드를 바꾸다</td>
                        <td>2019-03-24<br>17:29:59</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">L301</strong> 로그인 통계
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="logged_in_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 로그인 한 사용자</li>
                    <li>프로시져명 : [dbo].[m_proc_logged_in_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_logged_in_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인<br>일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>Sales</td>
                        <td>Sales</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">L302</strong> 로그인 통계(집합)
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="logged_in_blended_learning_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 로그인 한 집합교육 대상 사용자</li>
                    <li>프로시져명 : [dbo].[m_proc_logged_in_blended_learning_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_logged_in_blended_learning_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인<br>일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>Sales</td>
                        <td>Sales</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">L311</strong> 추가정보(자기소개, 해시태그, 학력정보) 입력 통계
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="input_socialdata_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 추가정보(자기소개, 해시태그, 학력정보)를 입력한 사용자</li>
                    <li>프로시져명 : [dbo].[m_proc_input_socialdata_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_input_socialdata_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인<br>일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                        <th>자기소개<br>입력여부</th>
                        <th>기간내 입력된<br>해시태그 수</th>
                        <th>기간내 입력된<br>학력정보 수</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>Sales</td>
                        <td>B2B Sales</td>
                        <td>1</td>
                        <td>3</td>
                        <td>2</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">L312</strong> 추가정보(자기소개, 해시태그, 학력정보) 입력 통계(집합)
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="input_socialdata_blended_learning_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 추가정보(자기소개, 해시태그, 학력정보)를 입력한 집합교육 대상 사용자</li>
                    <li>프로시져명 : [dbo].[m_proc_input_socialdata_blended_learning_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_input_socialdata_blended_learning_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인<br>일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                        <th>자기소개<br>입력여부</th>
                        <th>기간내 입력된<br>해시태그 수</th>
                        <th>기간내 입력된<br>학력정보 수</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>Sales</td>
                        <td>B2B Sales</td>
                        <td>1</td>
                        <td>3</td>
                        <td>2</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">L313</strong> 추가정보(직무, 관심주제, 전문분야) 입력 통계
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="input_extradata_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 추가정보(직무, 관심주제, 전문분야)를 입력한 사용자(중복)</li>
                    <li>프로시져명 : [dbo].[m_proc_input_extradata_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_input_extradata_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인<br>일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                        <th>관심주제<br>(상위)</th>
                        <th>관심주제<br>(하위)</th>
                        <th>전문분야<br>(상위)</th>
                        <th>전문분야<br>(하위)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>Sales</td>
                        <td>B2B Sales</td>
                        <td>비지니스</td>
                        <td>영업</td>
                        <td>인문/교양</td>
                        <td>법정필수</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">L314</strong> 추가정보(직무, 관심주제, 전문분야) 입력 통계(집합)
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="input_extradata_blended_learning_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 추가정보(직무, 관심주제, 전문분야)를 입력한 집합교육 대상 사용자(중복)</li>
                    <li>프로시져명 : [dbo].[m_proc_input_extradata_blended_learning_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_input_extradata_blended_learning_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인<br>일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                        <th>관심주제<br>(상위)</th>
                        <th>관심주제<br>(하위)</th>
                        <th>전문분야<br>(상위)</th>
                        <th>전문분야<br>(하위)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101<br>@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>Sales</td>
                        <td>B2B Sales</td>
                        <td>비지니스</td>
                        <td>영업</td>
                        <td>인문/교양</td>
                        <td>법정필수</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">C111</strong> 비디오 시청 통계
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="video_viewed_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 비디오를 시청한 사용자(시청수 포함)</li>
                    <li>프로시져명 : [dbo].[m_proc_video_viewed_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_video_viewed_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인 일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                        <th>기간내<br>비디오<br>시청수</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>R&D</td>
                        <td>R&D</td>
                        <td>2</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5 class="box-title">
                <strong style="color:#b00035">C112</strong> 비디오 시청 통계(집합)
                <form class="pull-right">
                    <label for="start_date">시작일</label>
                    <input type="date" name="start_date" value="">
                    <label for="end_date">종료일</label>
                    <input type="date" name="end_date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="download_type" value="video_viewed_blended_learning_student_list">
                    <button class="btn btn-danger xlsdown">엑셀다운로드</button>
                </form>
            </h5>
            <div class="box box-body">
                <ul class="dash-list">
                    <li>통계명 : 기간 내 비디오를 시청한 집합교육 대상 사용자(시청수 포함)</li>
                    <li>프로시져명 : [dbo].[m_proc_video_viewed_blended_learning_student_list]</li>
                    <li>사용법 : EXEC [dbo].[m_proc_video_viewed_blended_learning_student_list] [종료일], [시작일]</li>
                </ul>
                <div class="t-info mg">데이터 예시</div>
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th>사용자ID</th>
                        <th>마지막<br>로그인 일시</th>
                        <th>사번</th>
                        <th>이메일</th>
                        <th>회사명</th>
                        <th>직위</th>
                        <th>직무<br>(상위)</th>
                        <th>직무<br>(하위)</th>
                        <th>기간내<br>비디오<br>시청수</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>123456</td>
                        <td>2019-03-25<br>08:30:00</td>
                        <td>123456</td>
                        <td>testdlp101@lgacademy.com</td>
                        <td>LG인화원</td>
                        <td>선임</td>
                        <td>R&D</td>
                        <td>R&D</td>
                        <td>2</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>

<?php
// 레이어
include_once($CFG->dirroot . '/local/management/statistics/pop_layer/download_reason.php');

include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">

</script>
