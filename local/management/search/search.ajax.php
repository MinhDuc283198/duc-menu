<?php
require_once(__DIR__ . '/../../../config.php');
global $DB;
// 사용 OR 미사용 할 검색어 아이디를 받음 
$id = optional_param('id',0, PARAM_INT);
$isused = optional_param('used',0, PARAM_INT);
$divide = optional_param('divide',0, PARAM_INT);

$totalcount = $DB->count_records('lmsdata_manage_search',array('isused'=>1,'divide'=>$divide));

$value = new stdClass();
$value->id = $id;
if($isused){
    $value->isused = 0;
    $value->suc = $DB->update_record('lmsdata_manage_search',$value);
} else {
    if($totalcount>=5){
        $value->suc =-1;
    } else {
        $value->isused = 1 ;
        $value->suc = $DB->update_record('lmsdata_manage_search',$value);
    }
}

ini_set( "display_errors", 1 );

@header('Content-type: application/json; charset=utf-8');
echo json_encode($value);
?>
