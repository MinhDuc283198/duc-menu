<?php
 require_once(__DIR__ . '/../../../config.php');
    
    
    require_login(0, false);
    if (isguestuser()) {
        $SESSION->wantsurl = (string)new moodle_url('popup_submit.php');
        redirect(get_login_url());
    }
    $context = context_system::instance();
    //require_capability('moodle/site:config', $context);
    
    $id = optional_param('id', 0, PARAM_INT);
    $title_ko = optional_param('title_ko', "", PARAM_TEXT);
    $title_en = optional_param('title_en', "", PARAM_TEXT);
    $title_vn = optional_param('title_bn', "", PARAM_TEXT);
    $divide = optional_param('divide', 0, PARAM_INT); //0 = 이러닝 / 1 = 구인구직
    
    $values = new stdClass();
    $values->title = $title_ko;
    $values->titleen = $title_en;
    $values->titlevn = $title_vn;
    $values->divide = $divide;
    
    $totalcount = $DB->count_records('lmsdata_manage_search',array('isused'=>1,'divide'=>$divide));
    if($totalcount >=5){
         $values->isused = 0;
    } else {
        $values->isused = 1; 
    }
    
    if(!$id){
        $values->timecreated = time();
        $values->userid = $USER->id;
        $DB->insert_record('lmsdata_manage_search',$values);
    } else {
        $values->id = $id;
        $DB->update_record('lmsdata_manage_search',$values);
    }
    
    
    
    
?>
<script>
    window.onload = function(){
        location.href = '<?php echo $CFG->wwwroot;?>/local/management/search/search.php'
    }
</script>