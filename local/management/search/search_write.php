<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
$pagesettings = array(
    'title' => get_string('stext-manage', 'local_management'),
    'heading' =>get_string('stext-manage', 'local_management'),
    'subheading' => '',
    'menu' => 'search',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
$context = context_system::instance();
$PAGE->set_context($context);

include_once($CFG->dirroot . '/local/management/header.php');

$id = optional_param('id', 0, PARAM_INT); //id값이 있으면 수정

if($id){
    $value = $DB->get_record('lmsdata_manage_search',array('id'=>$id));
}
?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form method='post' id='searchtext_form' action='./search_submit.php'>
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                <table cellspadding="0" cellspacing="0" class="detail">
                    <tr>
                        <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata')."(KO)"; ?></td>
                        <td class="field_value"><input type="text" title="title" id="title_ko" value="<?php echo (!empty($value->title)) ? $value->title : ""; ?>" name="title_ko" class="w_100"/></td>
                    </tr>
                    <tr>
                        <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata')."(EN)"; ?></td>
                        <td class="field_value"><input type="text" title="title" id="title_en" value="<?php echo (!empty($value->titleen)) ? $value->titleen : ""; ?>" name="title_en" class="w_100"/></td>
                    </tr>
                    <tr>
                        <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata')."(VN)"; ?></td>
                        <td class="field_value"><input type="text" title="title" id="title_vn" value="<?php echo (!empty($value->titlevn)) ? $value->titlevn : ""; ?>" name="title_bn" class="w_100"/></td>
                    </tr>
                    <tr>
                        <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('division', 'local_management'); ?></td>
                        <td class="field_value"><select name="divide" class="w_160">
                            <option value="0" <?php echo ($value->divide==0) ? "selected" : ""; ?>><?php echo get_string('e-learning', 'local_management'); ?></option>
                            <option value="1" <?php echo ($value->divide==1) ? "selected" : ""; ?>><?php echo get_string('jobs', 'local_management'); ?></option>
                        </select>
                        </td>
                    </tr>
                </table>
                     <div id="btn_area">
                        <input type="button" id="searchtext_submit" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/search/search_write.php"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                        <input type="button" id="popup_delete_button" class="red_btn" value="<?php echo get_string('cancle', 'local_lmsdata'); ?>" style="float: left;"/>
                    </div>
                </form>
            </div>
        </div>
</div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>


<script type="text/javascript">

  
    $(document).ready(function () {
        
        $('#searchtext_submit').click(function () {
            $('#searchtext_form').submit();
        });
        $('#searchtext_form').submit(function (event) {
            var title_ko = $("#title_ko").val();
            var title_en = $("#title_en").val();
            var title_vn = $("#title_vn").val();
            if (title_ko.trim() == '' || title_en.trim() == '' || title_vn.trim() == '') {
                alert('<?php echo get_string('search:alert', 'local_lmsdata');?>');
                return false;
            };
            
        });
    });
</script>
