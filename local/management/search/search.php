<?php
require_once(__DIR__ . '/../../../config.php');
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/local/popup/lib.php';
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';
$pagesettings = array(
    'title' => get_string('stext-manage', 'local_management'),
    'heading' => get_string('stext-manage', 'local_management'),
    'subheading' => '',
    'menu' => 'search',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('search');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$PAGE->set_context($context);

$totalcount = $DB->count_records_select('lmsdata_manage_search',array());


include_once($CFG->dirroot . '/local/management/header.php');


// 등록하기 -> 검색어 / 노출여부 - 현재 노출중인 검색어 체크해서 5개까지만 가능하게 -
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                    <table cellpadding="0" cellspacing="0" class="normal" width="100%">
                        <caption class="hidden-caption"><?php echo get_string('stext-manage', 'local_management'); ?></caption>
                        <thead>
                            <tr>
                                <th scope="row" width="5%"><input type="checkbox" title="allcheck" id="allcheck" class="chkbox" /></th>
                                <th scope="row" width="5%">No.</th>
                                <th scope="row" width="10%"><?php echo get_string('division', 'local_management'); ?></th>
                                <th scope="row" width="35%"><?php echo get_string('stext', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('author', 'local_lmsdata'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('datecreated', 'local_lmsdata'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('isused', 'local_lmsdata'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $startnum = $totalcount - (($page - 1) * $perpage);
                            $sql = "SELECT ms.id,ms.title,ms.titleen,ms.titlevn, ms.isused, ms.timecreated,u.firstname AS username,ms.divide FROM {lmsdata_manage_search} ms JOIN {user} u on ms.userid = u.id ORDER BY id DESC";
                            $offset = 0;
                            if ($page != 0) {
                                $offset = ($page - 1) * $perpage;
                            }
                            $searchtext = $DB->get_records_sql($sql,array(),$offset, $perpage);
                            if($searchtext){
                                foreach($searchtext as $text){ ?>
                                    <tr>
                                        <td class="chkbox"><input type="checkbox" title="check"  name="delchk"/><input class="idval" type="hidden" value="<?php echo $text->id ?>"></td>
                                        <td class="number"><?php echo $startnum; ?></td>
                                        <td><?php echo $text->divide ==0 ? get_string('e-learning', 'local_management') : get_string('jobs', 'local_management'); ?></td>
                                        <td class="title"><a href="./search_write.php?id=<?php echo $text->id; ?>"><?php echo $text->title.' / '.$text->titleen.' / '.$text->titlevn ?></a></td>
                                        <td><?php echo $text->username; ?></td> 
                                        <td><?php echo date("Y-m-d", $text->timecreated); ?></td>
                                        <td><input type="button" id="use_text" onclick="isused(<?php echo $text->id ?>,<?php echo $text->isused ?>,<?php echo $text->divide ?>)" value="<?php echo $text->isused ==1 ? get_string('siteadmin_use', 'local_lmsdata') : get_string('siteadmin_nouse', 'local_lmsdata'); ?>"></td>
                                    </tr>
                                <?php
                                $startnum--;
                                }
                            } else {
                                ?>
                                    <tr>
                                        <td colspan = "7"> <?php echo get_string('nodata-stext', 'local_management'); ?> </td>
                                    </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                     <div id="btn_area">
                        <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/search/search_write.php"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                        <input type="button" id="search_delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left;"/>
                    </div>
                <div class="pagination">
                    <?php
                    print_paging_navbar($totalcount, $page, $perpage, $CFG->wwwroot . '/local/management/search/search.php', array());
                    ?>
                </div><!-- Pagination End -->
            </div>
                
        </div>  
    </div>
 </section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
    function isused(id,used,divide){
        if(used===0){
            var text = "<?php echo get_string('insert-stext', 'local_management'); ?>";
        } else { var text = "<?php echo get_string('delete-stext', 'local_management'); ?>";}
            if (confirm(text)) {
                $.ajax({
                    url: "search.ajax.php",
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id, used : used, divide : divide
                        },
                    success: function (data) {
                        if(data.suc < 1){
                            alert('<?php echo get_string('alert-stext', 'local_management'); ?>');
                        }
                        window.location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR.responseText);
                    }
                });
            }
        }
     function muldel() {
          var ids = [];
            $("input[name=delchk]:checked").each(function (index) {
                ids[index] = $(this).siblings(".idval").val();
            });
                    
         $.ajax({
            url: "search_delete.php",
            type: 'POST',
            dataType: 'json',
            data: {ids: ids},
            success: function (data) {
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });

    }
    $(function () {
        $("#allcheck").click(function () {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function () {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function () {
                    this.checked = false;
                });
            }
        });

        $("#search_delete_button").click(function () {
            if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
                muldel();
            }
        });
        
    });
</script>

