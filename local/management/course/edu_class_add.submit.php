<?php

/**
 * 기수관리 submit 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/sessionlib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');
require_once($CFG->dirroot . '/local/jeipoint/lib.php'); // local_jeipoint_check_course, local_jeipoint_restore 함수 호출
require_once($CFG->dirroot . '/local/management/classes/template_import_ui.php');
require_once($CFG->dirroot . '/local/management/lib.php');

$lmsdata = new stdClass();
$mod = optional_param('mod', '', PARAM_RAW);
$id = optional_param('id', '', PARAM_RAW); // lmsdata_class->id


foreach ($_REQUEST as $key => $val) {
    $$key = $val;
    if (strpos($key, 'start')) {
        $lmsdata->$key = strtotime($val);
    } else if (strpos($key, 'end')) {
        $lmsdata->$key = strtotime($val) + 86399;
    } else {
        $lmsdata->$key = $val;
    }
}
$lcclass;

if ($id) {
    $getclasssql = 'SELECT * 
                                FROM {lmsdata_class} lc 
                                JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
                                WHERE lc.id = :lcid';
    $lcclass = $DB->get_record_sql($getclasssql, array('lcid' => $id));
} else {
    $lcclass = $DB->get_record('lmsdata_course', array('id' => $catatotal2));
}
if (empty($lmsdata->enrolmentstart)) {
    $lmsdata->enrolmentstart = '';
    $lmsdata->enrolmentend = '';
}

$cate = $DB->get_record('course_categories', array('idnumber' => 'certification_course'));
if ($cate->id == $cata1 || $cate->id == $detailprocess) {
    $learningstart = $lmsdata->learningstart + $lmsdata->learningStime * 60 * 60 + $lmsdata->learningStimes * 60;
    $learningend = ($lmsdata->learningend - 86399) + $lmsdata->learningEtime * 60 * 60 + $lmsdata->learningEtimes * 60;
    $lmsdata->learningstart = $learningstart;
    $lmsdata->learningend = $learningend;
}
$lmsdata->timecreated = time();
$lmsdata->timeupdated = time();
$lmsdata->certificateimg = $_FILES['certificateimg']['name'];
$courseconfig = get_config('moodlecourse');
if ($mod != 'edit') {
    $lmsdata_course = $DB->get_record('lmsdata_course', array('id' => $catatotal2));
    $coursething = $DB->get_record('course', array('id' => $lmsdata_course->courseid));
    $data = new stdClass();
    $data = $DB->get_record('course', array('id' => $lmsdata_course->courseid));
    $data->summary = $coursething->summary;
    $data->fullname = empty($fullname) ? $data->fullname : $fullname;

    $data->shortname = '[class_' . $data->fullname . ']' . time();
    $data->category = $caid;
    if ($mod == 'copy') { // 상세 카테고리 
        $catevalue = empty($detailprocess) ? $data->category : $detailprocess;
        $data->category = $catevalue;
    } else {
        $data->category = empty($cata1) ? $catatotal2 : $caid; // 상세 카테고리 
    }
    $data->scheduleid = empty($scheduleid) ? '1' : $scheduleid;
    $data->courseid = empty($coid) ? $data->courseid : $coid;

    $course = create_course($data);

    $lmsdata->parentcourseid = $lmsdata_course->id;
    $lmsdata->courseid = $course->id;
    $lmsdata->classnum = $classnum;
    $lmsdata->scheduleid = empty($scheduleid) ? '1' : $scheduleid; // 임의로 설정함
    $lmsdata->templateyn = 'N';
    $lmsdata->coursename = $fullname;
    $lmsdata->cardinalnum = 1;
    
    $classid = $DB->insert_record('lmsdata_class', $lmsdata);
    
    
    if (edu_import_template($course->id, $lmsdata_course->courseid)) {

        if ($lmsdata->evaluationstart && $lmsdata->evaluationend) {
            //퀴즈 데이터 날짜 변경       
            $quizs = $DB->get_records('quiz', array('course' => $course->id));
            foreach ($quizs as $quiz) {
                $quiz->timeopen = $lmsdata->learningstart;
                $quiz->timeclose = $lmsdata->learningend;
                $DB->update_record('quiz', $quiz);
            }
            //okmedia 데이터 날짜 변경       
            $okmedias = $DB->get_records('okmedia', array('course' => $course->id));
            foreach ($okmedias as $okmedia) {
                $okmedia->timestart = $lmsdata->learningstart;
                $okmedia->timeend = $lmsdata->learningend;
                $DB->update_record('okmedia', $okmedia);
            }
        }
        //게시글 복사
        edu_import_board($course->id, $lmsdata_course->courseid);

        //템플릿상태값 변경
        $lmsdata->id = $classid;
        $lmsdata->templateyn = 'Y';
        $DB->update_record('lmsdata_class', $lmsdata);
    }
} else {
    $lmsdata->id = $id; // lmsdata_class->id
    
    //신임/연차별 과정일 경우 평가기간 종료일을 수정 했을때 visible = 0 된 사용자 포인트 복구 시켜줘야함
    if(local_jeipoint_check_course($lmsdata->courseid)) {
        $old_evaluationend = $DB->get_field('lmsdata_class', 'evaluationend', array('id' => $id));
        if($lmsdata->evaluationend > $old_evaluationend) {
            local_jeipoint_restore($lmsdata->courseid);
        }
    }
    
    
    $DB->update_record('lmsdata_class', $lmsdata);
}

$context = context_course::instance($lmsdata->courseid);
$fs = get_file_storage();

/* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
if ($id && (!($_FILES['certificateimg']['name'] == null || $_FILES['certificateimg']['name'] == '')) || $file_del == 1) {
    $overlap_files = $fs->get_area_files($context->id, 'local_courselist', 'certificateimg', 0, 'id');
    $cnt = 0;
    foreach ($overlap_files as $file) {
        if ($file->get_filesize() > 0) {
            if ($file_del) {
                $filename = $file->get_filename();
                $file->delete();
            }
            $cnt++;
        }
    }
}

if (!empty($_FILES['certificateimg']['tmp_name'])) {

    $file_record = array(
        'contextid' => $context->id,
        'component' => 'local_courselist',
        'filearea' => 'certificateimg',
        'itemid' => 0,
        'filepath' => '/',
        'filename' => $_FILES['certificateimg']['name'],
        'timecreated' => time(),
        'timemodified' => time(),
        'userid' => $USER->id,
        'author' => fullname($USER),
        'license' => 'allrightsreserved',
        'sortorder' => 0
    );
    $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['certificateimg']['tmp_name']);
}
redirect($CFG->wwwroot . '/local/management/course/edu_class.php');





