<?php
require_once(__DIR__ . '/../../../config.php');
require_once dirname(dirname (__FILE__)).'/lib.php';
require_once $CFG->dirroot.'/lib/coursecatlib.php';

$courseid = required_param('id',  PARAM_INT);
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

?>
<div id="popup_content">
    <form name="frm_enrol_excel" id="frm_enrol_excel" class="search_area" method="post" enctype="multipart/form-data">
        <input type="file" name="enrol_excel" size="50"/>
        <input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('add3','local_lmsdata'); ?>" onclick="enrol_course_excel_submit()"/>
        <br/>
        <p><strong><a style="color:red;" href="enrol_sample_v1.xlsx" alt="Sample" alt="Sample">[Sample]</a></strong>양식에 맞춰서 사용자를 추가해주세요.</p>
    </form>
</div><!--Content End-->
<script type="text/javascript">
    function enrol_course_excel_submit() {
        if($.trim($("input[name='enrol_excel']").val()) != '') {
             var filename = $.trim($("input[name='enrol_excel']").val());
             var extension = filename.replace(/^.*\./, ''); 
             if(extension == filename) {
                 extension = "";
             } else {
                 extension = extension.toLowerCase();
             }
             
             if($.inArray( extension, [ "xls", "xlsx" ] ) == -1) {
                 alert("<?php echo get_string('onlyexcell','local_lmsdata'); ?>");
                 return false;
             } else {
                var form = $('#frm_enrol_excel');
                //FormData
                var formData = new FormData(form[0]);
                formData.append('courseid', <?php echo $courseid;?>);
                $.ajax({
                    url: '<?php echo $CFG->wwwroot.'/local/management/course/enrol_course_excel.execute.php'; ?>',
                    processData: false,
                    contentType: false,
                    data: formData,
                    dataType: 'json',
                    type: 'POST',
                    success: function(result){
                        console.log(result);
                        $('#frm_enrol_excel').remove();
                        $('#course_enrol_popup').dialog('destroy').remove();
                        alert('등록이 완료되었습니다.(등록:' + result.success + ', 중복:' + result.begin + ', 실패:' + (result.empty + result.notobject) + ')');
                    }
                });
             }
        } else {
            alert('파일을 등록해야 합니다.');
        }
    }
    
</script>    