<?php
/**
 * 과정관리 리스트 페이지
 */

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '과정관리',
    'heading' => '과정관리',
    'subheading' => '',
    'menu' => 'course',
    'js' => array($CFG->wwwroot . '/local/management/course/course_list.js'),
    'css' => array(),
    'nav_bar' => array()
);

require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('edu_course.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$coursetype = optional_param('coursetype', 0, PARAM_INT); //0:교과, 1:비교과, 2:이러닝 
$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$cata1 = optional_param('cata1', 0, PARAM_INT);
$classobject = optional_param('classobject', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);

//검색용 파라미터
if ($cata1) {
    $sql_where[] = " (lc.coursearea = :coursearea or lc.roughprocess = :roughprocess) ";
    $params['coursearea'] = $cata1;
    $params['roughprocess'] = $cata1;
}
if ($classobject) {
    $sql_where[] = " ( lc.classobject = :classobject or lc.classtype = :classtype ) ";
    $params['classobject'] = $classobject;
    $params['classtype'] = $classobject;
}
if (!empty($searchtext)) {
    $sql_where[] = $DB->sql_like('lc.coursename', ':coursename');
    $params['coursename'] = '%' . $searchtext . '%';
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}
$orderby = ' order by id desc ';
$sql_select = 'SELECT 
                        mc.id, mc.category, ca.name ca1name, ca2.name ca2name, mc.fullname, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber,
                        lc.id lcid, lc.mainexposure, lc.exposuresort, lc.classobject, lc.coursename, lc.coursecd, lc.classtype';

$sql_from = ' FROM {course} mc 
                    JOIN {lmsdata_course} lc ON lc.courseid = mc.id 
                    JOIN {course_categories} ca ON ca.id = mc.category 
                    left JOIN {course_categories} ca2 ON ca.parent = ca2.id';

$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $orderby, $params, ($currpage - 1) * $perpage, $perpage);

$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);

?>

<?php include_once($CFG->dirroot.'/local/management/header.php'); ?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
        <form name="course_search" id="course_search" class="search_area" action="edu_course.php" method="get">
            <input type="hidden" name="page" value="1" />
            <input type="hidden" name="coursetype" value="<?php echo $coursetype; ?>" />
            <div>
                <select title="category01" name="cata1" id="course_search_cata1" class="w_160">
                    <option value="0">수강형식</option>
                    <?php
                    $catagoriessql_p = 'SELECT cc.id, cc.name
                                                                        FROM {course_categories} cc
                                                                        JOIN {course_categories} cc1 ON cc1.id = cc.parent
                                                                        WHERE (cc1.idnumber = "oklass_regular") and cc.visible = 1 ';
                    $catagories_p = $DB->get_records_sql($catagoriessql_p, array('visible' => 1, 'idnumber1' => 'oklass_regular', 'idnumber2' => 'required_course'));
                    foreach ($catagories_p as $catagory) {
                        $selected = '';
                        if ($catagory->id == $cata1) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $catagory->id . '"' . $selected . '> ' . $catagory->name . '</option>';
                        $selected = '';
                    }
                    ?>
                </select>
                <select name="classobject" title="classobject" id="course_search_cata3" class="w_160">
                    <option value="0">수강대상</option>
                    <?php
                    $mainarrs = array(
                        1 => '임직원',
                        2 => '멘토',
                        3 => '교육생',
                        4 => '임직원, 멘토',
                        5 => '멘토, 교육생',
                        6 => '임직원, 교육생',
                        7 => '전체'
                    );
                    foreach ($mainarrs as $key => $val) {
                        $selected = '';
                        if (!empty($courses->classobject)) {
                            if ($key == $classobject) {
                                $selected = ' selected';
                            }
                        } else {
                            if ($key == $classobject) {
                                $selected = ' selected';
                            }
                        }
                        echo '<option value="' . $key . '"' . $selected . '> ' . $val . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div>
                <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="강의명을 입력하세요."  class="search-text"/>
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
            </div>
        </form><!--Search Area2 End-->

        <table>
            <thead>
                <tr>
                    <th scope="row" width="6%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%">수강대상</th>
                    <th scope="row" width="10%">수강형식</th>
                    <th scope="row" width="6%">코드명</th>
                    <th scope="row" width="38%"><?php echo get_string('course_name', 'local_lmsdata'); ?></th>
                    <th scope="row" width="10%">포인트</th>
                    <th scope="row" width="6%"><?php echo get_string('class_enter', 'local_lmsdata'); ?></th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="9"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    $studytype = '';
                    $type_arrs = array(
                        1 => '임직원',
                        2 => '멘토',
                        3 => '교육생',
                        4 => '임직원, 멘토',
                        5 => '멘토, 교육생',
                        6 => '임직원, 교육생',
                        7 => '전체'
                    );

                    $classobjects = $course->classobject;
                    $classtypes = $course->classtype;

                    if (empty($classobjects) || $classobjects == 0) {
                        $studytype = $type_arrs[$classtypes];
                    } else {
                        $studytype = $type_arrs[$classobjects];
                    }
                    ?>
                    <tr>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo $studytype ?></td>
                        <td><?php echo ($course->ca2idnumber != 'required_course') ? $course->ca1name : $course->ca2name . '<br/>' . $course->ca1name ?></td>
                        <td><?php echo empty($course->coursecd) ? '-' : $course->coursecd; ?></td>
                        <td class="left"><a href="<?php echo $CFG->wwwroot . '/local/management/course/edu_course_add.php?id=' . $course->id; ?>"><?php echo $course->coursename; ?></a>
                        </td>
                        <?php
                        $sql_pointwhere = "where course = " . $course->id;
                        $sql_point = "SELECT course, SUM(ok.point) lecturepoint "
                                . "FROM {okmedia} ok  "
                                . "join {lmsdata_course} lco on lco.courseid = ok.course "
                                . $sql_pointwhere
                                . " group by course";
                        $get_point = $DB->get_record_sql($sql_point);
                        if ($get_point) {
                            ?>
                            <td><?php echo $get_point->lecturepoint ?> P </td>
                            <?php
                        } else {
                            ?>
                            <td>0 P</td>
                            <?php
                        }
                        ?>

                        <td>
                            <input type="button" class="normal_btn" value="입장" onclick="window.open('<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->id; ?>')"/>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->

        <div id="btn_area">
            <div style="float:right;">            
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('create_course', 'local_lmsdata'); ?>" onclick="javascript:location.href = 'edu_course_add.php';"/> 
            </div>
        </div>
        <?php
        if (($count_courses / $perpage) > 1) {
            print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        }

        $query_string = '';
        if (!empty($excel_params)) {
            $query_array = array();
            foreach ($excel_params as $key => $value) {
                $query_array[] = urlencode($key) . '=' . urlencode($value);
            }
            $query_string = '?' . implode('&', $query_array);
        }
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->
</div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>

<script type="text/javascript">

    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    /**
     * 과정 sort 변경 스크립트
     * @param {type} id
     * @returns {undefined}
     */
    function exposuresort_change(id) {
        var val = $("#exposuresort_change_" + id + " option:selected").val();
        $.ajax({
            url: 'edu_exposuresort_change.ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                exposuresort: val,
                id: id,
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 'success') {
                    alert('변경되었습니다.');
                    location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }
    /**
     * 추천인기과정 변경 스크립트
     * @param {type} type
     * @returns {Boolean}
     */
    function mainexposure_change(type) {
        var val = $("#mainexposure_change option:selected").val();

        var course_list = [];

        if (!$(".courseid").is(":checked")) {
            alert('과정을 선택하세요.');
            return false;
        }
        $("input[name=courseid]:checked").each(function () {
            course_list.push($(this).val());
        });

        $.ajax({
            url: 'edu_mainexposure_change.ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                mainexposure: val,
                courseid: course_list,
                type: type
            },
            success: function (data, textStatus, jqXHR) {
                if (data.status == 'success') {
                    alert('변경되었습니다.');
                    location.reload();
                } else {
                    location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jqXHR.responseText);
            }
        });
    }
</script>    
