//select box 초기화
function remove_file() {
    $("span[name='file_link']").remove();
    $("input[name='remove_button']").remove();
    $("input[name='file_del']").val(1);
}
//select box 변경시 이벤트
function cata1_changed(sel) {
    var selCata2 = $('#classyear');
    cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = cata_get_child_categories($(sel).val());
    if (categories !== null) {
        cata_add_select_options(selCata2, categories);
    }
}
//select box 변경시 이벤트
function cata_changed_c(sel) {
    var selCata2 = $('#course_search_cata2');
    cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = cata_get_child_cata_c($(sel).val());
    if (categories !== null) {
        cata_add_select_detail_options(selCata2, categories);
    }
}

//select box 변경시 이벤트
function cata2_changed(sel) {
    selCata3 = $('#course_search_cata3');
    cata_clean_select(selCata3);

    if ($(sel).val() == 0) {
        return;
    }

    var categories = cata_get_child_categories($(sel).val());
    if (categories !== null) {
        cata_add_select_options(selCata3, categories);
    }
}
//select box 변경시 이벤트
function cata_get_child_categories(pid) {
    var categories = null;
    $.ajax({
        url: '/local/management/course/child_categories.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                categories = data.categories;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return categories;
}
//select box 변경시 이벤트
function cata_get_child_cata_c(pid) {
    var categories = null;
    $.ajax({
        url: '/local/management/course/child_detailcata.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                categories = data.categories;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return categories;
}
//select box 변경시 옵션 리스트 출력
function cata_add_select_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.classyear
        }));
    });
}
function cata_add_select_detail_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.name
        }));
    });
}
//select box 초기화
function cata_clean_select(sel) {
    $(sel[0].options).each(function () {
        if ($(this).val() != 0) {
            $(this).remove();
        }
        ;
    });
}
function cata_page(page) {
    $('[name=page]').val(page);
    $('#course_search').submit();
}

function add_session_courses() {
    var add_list = [];
    $(".courseid").each(function (index, element) {
        if ($(this).is(":checked")) {
            add_list.push($(this).val());
        }
    });

    $.ajax({
        url: "./course_list_add.session.ajax.php",
        type: "post",
        data: {
            data: add_list
        },
        async: false,
        success: function (data) {
            $('#course_search').submit();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
}
function del_session_courses() {

    $.ajax({
        url: "./course_list_del.session.ajax.php",
        type: "post",
        async: false,
        success: function (data) {
            $('#course_search').submit();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
}

function check_course_id(check, checkClass) {
    if ($(check).is(":checked")) {
        $("." + checkClass).each(function () {
            this.checked = true;
        });
    } else {
        $("." + checkClass).each(function () {
            this.checked = false;
        });
    }
}

function create_drive_dialog() {

    var tag = $("<div id='course_drive_popup'></div>");
    var drive_list = [];
    var count = 0;
    var standardId;
    var flag;
    $(".scourseid").each(function (index, element) {
        if ($(this).is(":checked")) {
            drive_list.push($(this).val());
            count++;
        }
    });

    if (count < 2) {
        alert("2개 이상의 강의를 선택해야 합니다.");
        return false;
    }

    $.ajax({
        url: 'course_list_drive.ajax.php',
        method: 'POST',
        data: {
            course: drive_list
        },
        success: function (data) {
            tag.html(data).dialog({
                title: '분반몰아넣기',
                modal: true,
                width: 600,
                resizable: false,
                buttons: [{id: 'close',
                        text: '분반몰아넣기',
                        disable: true,
                        click: function () {
                            if ($("input[name=course_standard]:radio:checked").length == 0) {
                                alert("기준 분반을 선택 하세요");
                                return false;
                            }
                            if ($("input[name=flag]:radio:checked").length == 0) {
                                alert("기준 외 분반 비활성/삭제 를 선택해 주세요");
                                return false;
                            } else {
                                standardId = $("input:radio[name=course_standard]:checked").val();
                                flag = $("input:radio[name=flag]:checked").val();
                                $('#frm_course_standard').remove();
                                $(this).dialog('destroy').remove();
                                course_drive_execute(standardId, drive_list, flag);
                            }
                        }},
                    {id: 'close',
                        text: '취소',
                        disable: true,
                        click: function () {
                            $(this).dialog("close");
                        }}
                ],
                close: function () {
                    $('#frm_course_standard').remove();
                    $(this).dialog('destroy').remove()
                }
            }).dialog('open');
        }
    });
}

function course_drive_execute(standard, list, flag) {
    $.ajax({
        url: 'course_list_drive.execute.php',
        method: 'POST',
        dataType: 'json',
        data: {
            standard: standard,
            list: list
        },
        success: function (data) {
            alert("분반몰아넣기를 완료 하였습니다.");
            document.location.href = "course_list.php";
        }
    });
}

function drive_log_dialog(standardId) {
    var subcourse_list = [];
    var tag = $("<div id='course_drive_log_popup'></div>");

    $.ajax({
        url: 'course_drive_log.ajax.php',
        method: 'POST',
        data: {
            course: standardId
        },
        success: function (data) {
            tag.html(data).dialog({
                title: '분반내역',
                modal: true,
                width: 600,
                resizable: false,
                buttons: [{id: 'close',
                        text: '분반되돌리기',
                        disable: true,
                        click: function () {
                            $(".subcourse").each(function (index, element) {
                                subcourse_list.push($(this).val());
                            });
                            restore_course_execute(standardId, subcourse_list);
                        }},
                    {id: 'close',
                        text: '취소',
                        disable: true,
                        click: function () {
                            $(this).dialog("close");
                        }}
                ],
                close: function () {
                    $('#frm_course_standard').remove();
                    $(this).dialog('destroy').remove()
                }
            }).dialog('open');
        }
    });
}

function edit_course() {
    var count = 0;
    $(".courseid").each(function (index, element) {
        if ($(this).is(":checked")) {
            count += 1;
        }
    });

    if (count == 0) {
        alert("수정하려는 강의를 체크 해주세요");
        return false;
    } else if (count > 1) {
        alert("한개의 강의만 체크 해주세요");
        return false;
    }

    var editId = $(".courseid:checked").val();

    document.location.href = "course_list_add.php?id=" + editId;
}

function create_merge_course() {

    var count = 0;
    $('#content').append('<form method="post" id="merge_course" action="course_list_merge_form.php"></form>');
    $(".scourseid").each(function () {
        if ($(this).is(":checked")) {
            $(this).val();
            $('#merge_course').append('<input type="hidden" name="course[]" value="' + $(this).val() + '" />');
            count++;
        }
    });

    if (count < 2) {
        alert("2개 이상의 강의를 선택해야 합니다.");
        return false;
    }

    $('#merge_course').submit();
}

function restore_course_execute(standardId, subcourse_list) {
    $.ajax({
        url: "./course_list_restore.execute.php",
        type: "post",
        data: {
            sdcourse: standardId,
            subcourse: subcourse_list
        },
        async: false,
        success: function (data) {
            alert(data + " 명의 수강생을 되돌리기 하였습니다.");
            $('#course_search').submit();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
}
function getWindowSize() {
    var myWidth = 0;
    var myHeight = 0;

    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if (document.documentElement &&
            (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    return {"width": myWidth, "height": myHeight}
}

function text_disable(checkbox, target, value) {
    target_node = $("input[name=" + target + "]");
    if (checkbox.checked == value) {
        target_node.attr('disabled', null);
    } else {
        target_node.attr('disabled', 'disabled');
    }
}

function mutiselecte_change(leave, arrive) {
    var arrive_node = $('#' + arrive + ' optgroup');
    $('#' + leave + ' option:selected').each(function (i, selected) {
        arrive_node.append(selected);
    });
}

//년도 selectbox 변경시 이벤트 type 1 - 신청목록 type 2 - 취소목록 type 3 - 기수목록
function year_changed(sel, tablename, type = null) {
    if ($(sel).val() == 0) {
        return;
    }
    var categories = '';
    if (type === 1) {
        var selCata3 = $('#classnum');
        cata_clean_select(selCata3);
        $("input[name='searchtext']").attr('value', '');
        $("input[name='searchtext']").prop('value', '');
        categories = year_get_child_classnum($(sel).val(), 1);
    } else if (type === 2) {
        var selCata3 = $('#classnum');
        cata_clean_select(selCata3);
        $("input[name='searchtext']").attr('value', '');
        $("input[name='searchtext']").prop('value', '');
        categories = year_get_child_classnum($(sel).val(), 2);
    } else if (type === 3) {
        var classobject = $('#course_search_cata1').val();
        var selCata3 = $('#classnum');
        var selCata4 = $('#coursename');
        cata_clean_select(selCata3);
        cata_clean_select(selCata4);
        categories = year_get_child_classnum($(sel).val(), null, classobject);
    }

    if (categories !== null && (type == null || type == 1 || type == 2 || type == 3)) {
        classnum_add_select_options(selCata3, categories);
}
}

//classnum selectbox 변경시 이벤트
function classnum_changed(sel, type) {
    if ($(sel).val() == 0) {
        return;
    }
    if (type == 1) {
        var selcoursename = $('#coursename');
        var selclassobject = $('#course_search_cata1').val();
        var selclassyear = $('#classyear').val();
        cata_clean_select(selcoursename);
        var categories = classnum_get_child_coursename(selclassyear, selclassobject, $(sel).val());
    } else if (type == 2) {
        var selcoursename = $('#coursename');
        var selsearchtext = $("input[name='searchtext']").val();
        var selclassyear = $('#classyear').val();
        cata_clean_select(selcoursename);
        $("input[name='searchtext']").attr('value', '');
        $("input[name='searchtext']").prop('value', '');
        var categories = classnum_get_child_coursename(selclassyear, null, $(sel).val());
    }
    if (categories !== null) {
        coursename_add_select_options(selcoursename, categories);
    }
}
//년도 selectbox 변경시 이벤트 차수 selectbox 값 가져옴
function year_get_child_classnum(pid, type, object = null) {
    var categories = null;
    if (type !== null) {
        $.ajax({
            url: '/local/management/course/edu_child_classnum.ajax.php',
            method: 'POST',
            dataType: 'json',
            async: false,
            data: {
                type: type,
                pid: pid
            },
            success: function (data) {
                if (data.status == 'success') {
                    categories = data.categories;
                } else {
                    //alert(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    } else {
        $.ajax({
            url: '/local/management/course/edu_child_classnum.ajax.php',
            method: 'POST',
            dataType: 'json',
            async: false,
            data: { 
                type: 3,
                year: pid,
                object: object
            },
            success: function (data) {
                if (data.status == 'success') {
                    categories = data.categories;
                } else {
                    //alert(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
    return categories;
}

//classnum selectbox 변경시 이벤트 차수 selectbox 값 가져옴
function classnum_get_child_coursename(classyear, classtype, classnum) {
    var categories = null;

    if (classtype == null) {
        $.ajax({
            url: '/local/management/course/edu_child_coursename.ajax.php',
            method: 'POST',
            dataType: 'json',
            async: false,
            data: {
                classyear: classyear,
                classnum: classnum,
                type: 1
            },
            success: function (data) {
                if (data.status == 'success') {
                    categories = data.categories;
                } else {
                    //alert(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    } else {
        $.ajax({
            url: '/local/management/course/edu_child_coursename.ajax.php',
            method: 'POST',
            dataType: 'json',
            async: false,
            data: {
                type: 2,
                classyear: classyear,
                classtype: classtype,
                classnum: classnum
            },
            success: function (data) {
                if (data.status == 'success') {
                    categories = data.categories;
                } else {
                    //alert(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }

    return categories;
}

//년도 selectbox 변경시 이벤트 차수 selectbox 값 가져옴
function classnum_add_select_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.classnum,
            text: option.classnum + '기'
        }));
    });
}

//년도 selectbox 변경시 이벤트 위탁처 selectbox 값 가져옴
function coursename_add_select_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.coursename
        }));
    });
}



/**
 * 쿠키 설정하기
 * 
 * 
 */
function edu_course_setCookie(cookiename, cookievalue, expiredays) {
    var nowday = new Date();
//    var newexpiredays = expiredays*24*60*60*1000; // days
//    var newexpiredays = expiredays*60*60*1000; // hours
    var newexpiredays = expiredays * 60 * 1000; // minutes
    nowday.setTime(nowday.getTime() + newexpiredays);
    document.cookie = cookiename + "=" + cookievalue + ";" + newexpiredays + ";path=/";
}

/**
 * 쿠키 불러오기
 * 
 * 
 */
function edu_course_getCookie(cookiename) {
    var cname = cookiename + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var cn = decodedCookie.split(';');
    for (var i = 0; i < cn.length; i++) {
        var c = cn[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cname) == 0) {
            return c.substring(cname.length, c.length);
        }
    }
    return "";
}

/**
 * 쿠키 삭제하기
 * 
 * 
 */
function edu_course_deleteCookie(cookiename) {
//    var cvalue = edu_course_getCookie(cookiename);
//    document.cookie = cookiename + "=" + cvalue + ";" + "Thu, 01 Jan 1970 00:00:00 UTC;" + ";path=/";
    edu_course_setCookie(cookiename, '', -1);
    console.log(cookiename + " : " + edu_course_getCookie(cookiename));
}

function enable_yeartype() {
    if($('#course_search_cata2 option:selected').text() == '연차별과정') {
        $('#course_yeartype').show();
    } else {
        $('#course_yeartype').hide();
    }
}