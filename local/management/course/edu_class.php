<?php
/**
 * 기수 관리 리스트 페이지
 */
    
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '기수관리',
    'heading' => '기수관리',
    'subheading' => '',
    'menu' => 'class',
    'js' => array($CFG->wwwroot . '/local/management/course/course_list.js'),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('edu_class.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$classyear = optional_param('classyear', 0, PARAM_INT);
$classobject = optional_param('classobject', 0, PARAM_INT);
$classnum = optional_param('classnum', 0, PARAM_INT);
$coursename = optional_param('coursename', 0, PARAM_INT);
$consignmenttitle = optional_param('consignmenttitle', '', PARAM_RAW);
//$mainexposure = optional_param('mainexposure', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);

//검색용 파라미터
if ($classobject) { // 수강대상
    $sql_where[] = " (lco.classobject = :lcoclassobject or lco.classtype = :lcoclasstype) ";
    $params['lcoclassobject'] = $classobject;
    $params['lcoclasstype'] = $classobject;
}
if ($classyear) { // 연도
    $sql_where[] = " lc.classyear = :classyear ";
    $params['classyear'] = $classyear;
}
if ($classnum) { // 기수
    $sql_where[] = " lc.classnum = :classnum ";
    $params['classnum'] = $classnum;
}
if ($coursename) { // 과정명
    $sql_where[] = " lco.id = :coursename ";
    $params['coursename'] = $coursename;
}
if (!empty($sql_where)) {
    $sql_where = ' WHERE ' . implode(' and ', $sql_where);
} else {
    $sql_where = '';
}

$sql_select = "SELECT lc.*,ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, mc.id mcid, mc.fullname, 
                    mc2.category, lco.coursename, lco.classobject lcoclassobject, lco.classtype lcoclasstype ";

$sql_from = " FROM {course} mc
                 JOIN {lmsdata_class} lc ON lc.courseid = mc.id
                JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                JOIN {course} mc2 ON lco.courseid = mc2.id
                 JOIN {course_categories} ca ON ca.id = mc2.category 
                 left JOIN {course_categories} ca2 ON ca.parent = ca2.id";

$sql_orderby = " order by mc.id desc";
$courses = $DB->get_records_sql($sql_select . $sql_from . $sql_where . $sql_orderby, $params, ($currpage - 1) * $perpage, $perpage);

$count_courses = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from . $sql_where, $params);
?>

<?php include_once($CFG->dirroot.'/local/management/header.php'); ?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
        <form name="course_search" id="course_search" class="search_area" action="edu_class.php" method="get">
            <input type="hidden" name="page" value="1" />
            <div style="float:left;">
                <select title="category01" name="classobject" id="course_search_cata1" onchange="cata1_changed(this);"  class="w_160">
                    <option value="0">수강대상</option>
                    <?php
                    $type_arrs = array(
                        1 => '임직원',
                        2 => '멘토',
                        3 => '교육생',
                        4 => '임직원, 멘토',
                        5 => '멘토, 교육생',
                        6 => '임직원, 교육생',
                        7 => '전체'
                    );
                    foreach ($type_arrs as $key => $val) {
                        $selected = '';
                        if ($key == $classobject) {
                            $selected = ' selected';
                        }
                        echo '<option value="' . $key . '"' . $selected . '> ' . $val . '</option>';
                    }
                    ?>
                </select>
                <select title="year" name="classyear" id="classyear" class="w_160" onchange="year_changed(this, 'lmsdata_class', 3);">
                    <option value="0">년도</option>
                    <?php
                    if($classobject){
                        $max = $DB->get_field_sql("SELECT max(classyear) FROM {lmsdata_class}");
                        $min = $DB->get_field_sql("SELECT min(classyear) FROM {lmsdata_class}");
                    
                        foreach (range($min, $max) as $year) {
                            $selected = '';
                            if ($year == $classyear) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $year . '"' . $selected . '>' . $year . '년</option>';
                        }
                    }
                    ?>
                </select> 
                <select title="class" name="classnum" id="classnum" class="w_160" onchange="classnum_changed(this, 1);">
                    <option value="0">기수</option>
                    <?php
                    if($classobject && $classyear){
                        $classnum_where = '';
                        if ($classobject) {
                            $classnum_where = " and (lco.classobject = '" . $classobject . "' or lco.classtype = '" . $classobject . "')";
                        }
                        if ($classyear) {
                            $classnum_where .= " and lc.classyear = " . $classyear;
                        }
                        $datas = $DB->get_records_sql('select lc.classnum from {lmsdata_class} lc join {lmsdata_course} lco on lco.id = lc.parentcourseid where lco.used != 0 and lco.isused != 1 ' . $classnum_where . ' order by lc.classnum desc');
                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->classnum == $classnum) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->classnum . '"' . $selected . '> ' . $data->classnum . '기</option>';
                        }
                    }
                    ?>
                </select>
                <select title="class" name="coursename" id="coursename" class="w_160">
                    <option value="0">과정명</option>
                    <?php
                    if ($classyear && $classobject && $classnum) {
                        $datassql = 'select lco.id, lco.coursename '
                                . 'from {lmsdata_course} lco '
                                . 'join {lmsdata_class} lc on lco.id = lc.parentcourseid '
                                . 'where isused != 1 and lc.classyear = :lcclassyear and (lco.classtype = :lcoclasstype or lco.classobject = :lcoclassobject) and lc.classnum = :lcclassnum';
                        $datas = $DB->get_records_sql($datassql, array('lcclassyear' => $classyear, 'lcoclasstype' => $classobject, 'lcoclassobject' => $classobject, 'lcclassnum' => $classnum));

                        foreach ($datas as $data) {
                            $selected = '';
                            if ($data->id == $coursename) {
                                $selected = ' selected';
                            }
                            echo '<option value="' . $data->id . '"' . $selected . '> ' . $data->coursename . '</option>';
                            $selected = '';
                        }
                    }
                    ?>
                </select>
            </div>

            <div style="float:left;">
                <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_lmsdata'); ?>"/>          
            </div>
        </form><!--Search Area2 End-->

        <table>
            <thead>
                <tr>
                    <th scope="row" width="3%"><input type="checkbox" onclick="check_course_id(this, 'courseid')"/></th>
                    <th scope="row" width="3%"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                    <th scope="row" width="8%">수강대상</th>
                    <th scope="row" width="8%">년도</th>
                    <th scope="row" width="14%">수강형식</th>
                    <th scope="row" width="5%">기수</th>
                    <th scope="row" width="26%"><?php echo get_string('course_name', 'local_lmsdata'); ?></th>
                    <!--<th scope="row" width="12%">수강신청기간</th>-->
                    <th scope="row" width="18%">교육기간</th>
                    <th scope="row" width="5%">수강</th>
                    <th scope="row" width="5%">이수</th>
                    <th scope="row" width="5%">미이수</th>
                    <th scope="row" width="5%">강의실 입장</th>
                    <th scope="row" width="5%">복사</th>
                    <th scope="row" width="5%">수강생등록</th>
                </tr>
            </thead>
            <?php if ($count_courses === 0) { ?>
                <tr>
                    <td colspan="14"><?php echo get_string('empty_course', 'local_lmsdata'); ?></td>
                </tr>
                <?php
            } else {
                $startnum = $count_courses - (($currpage - 1) * $perpage);
                foreach ($courses as $course) {
                    $getstudentsql = "SELECT ra.id 
                                                                    FROM {role_assignments} ra 
                                                                    JOIN {role} ro ON ra.roleid = ro.id 
                                                                    JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel1 
                                                                    JOIN {lmsdata_course_applications} lca ON lca.courseid = :courseid1 AND status = 'apply' 
                                                                    WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                                                    group by ra.userid";

                    $getcompletesql = "SELECT ra.id 
                                                                        FROM {role_assignments} ra 
                                                                        JOIN {role} ro ON ra.roleid = ro.id 
                                                                        JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel2 
                                                                        JOIN {course_completions} cc ON cc.course = :courseid1 AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                                                        WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                                                        group by ra.userid";

                    $getstudent = $DB->get_records_sql($getstudentsql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel1' => CONTEXT_COURSE, 'archetype' => 'student'));
                    $getcomplete = $DB->get_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => CONTEXT_COURSE, 'archetype' => 'student'));
                    ?>
                    <tr>
                        <td><input type="checkbox" class="courseid" name="courseid" value="<?php echo $course->id; ?>"/></td>
                        <td><?php echo $startnum--; ?></td>
                        <td><?php echo (empty($course->lcoclassobject)) ? $type_arrs[$course->lcoclasstype] : $type_arrs[$course->lcoclassobject]; ?></td> <!--수강대상-->
                        <td><?php echo $course->classyear . '년' ?></td>
                        <td>
                            <?php
                if ($course->ca2idnumber == 'oklass_regular') {
                    echo $course->ca1name;
                } else {
                    echo $course->ca2name . '<br>' . $course->ca1name;
                }
                ?>
                        </td>
                        <td><?php echo $course->classnum ? $course->classnum . '기' : '-' ?></td>
                        <td><a href="<?php echo $CFG->wwwroot . '/local/management/course/edu_class_add.php?id=' . $course->id; ?>&mod=edit"><?php echo $course->fullname; ?></a>
                            <span class="mainexposure<?php echo $course->mainexposure; ?>"><?php echo $mainarrs[$course->mainexposure]; ?></span></td>
                        <!--<td><?php echo ($course->enrolmentstart && $course->enrolmentend) ? date('Y-m-d', $course->enrolmentstart) . ' ~ ' . date('Y-m-d', $course->enrolmentend) : '-' ?></td>-->
                        <td><?php echo ($course->learningstart && $course->learningend) ? date('Y-m-d', $course->learningstart) . ' ~ ' . date('Y-m-d', $course->learningend) : '-' ?></td>
                        <td><?php echo count($getstudent); ?></td>
                        <td><?php echo count($getcomplete); ?></td>
                        <td><?php echo (count($getstudent) - count($getcomplete)) ?></td>
                        <td>
                            <input type="button" class="normal_btn" value="입장" onclick="window.open('<?php echo $CFG->wwwroot . '/course/view.php?id=' . $course->courseid; ?>')"/>
                        </td>
                        <td>
                            <input type="button" class="normal_btn" value="복사" onclick="javascript:location.href = 'edu_class_add.php?id=<?php echo $course->id; ?>&mod=copy';"/>
                        </td>
                        <td>
                            <input type="button" class="normal_btn" data-course="<?php echo $course->courseid; ?>" value="등록" onclick="enrol_user(this);"/>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>    
        </table><!--Table End-->
        <input type="button" class="normal_btn" style="float:left; margin-right: 10px;" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" onclick="course_delete()"/>
        <div id="btn_area">            
            <div style="float:right;">
                <input type="submit" class="blue_btn" style="margin-right: 10px;" value="기수등록" onclick="javascript:location.href = 'edu_class_add.php';"/> 
            </div>
        </div>
        <?php
        if (($count_courses / $perpage) > 1) {
            //print_paging_navbar_script($count_courses, $currpage, $perpage, 'javascript:cata_page(:page);');
        }

        $query_string = '';
        if (!empty($excel_params)) {
            $query_array = array();
            foreach ($excel_params as $key => $value) {
                $query_array[] = urlencode($key) . '=' . urlencode($value);
            }
            $query_string = '?' . implode('&', $query_array);
        }
        ?>            
    </div><!--Content End-->    
</div> <!--Contents End-->
</div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>

<script type="text/javascript">

    function cata_page(page) {
        $('[name=page]').val(page);
        $('#course_search').submit();
    }

    /**
     * 기수 삭제 함수
     * @param {type} id
     * @returns {Boolean}
     */
    function course_delete(id) {
        var course_list = [];
        if (!$(".courseid").is(":checked")) {
            alert('기수를 선택하세요.');
            return false;
        }
        $("input[name=courseid]:checked").each(function () {
            course_list.push($(this).val());
        });

        if (confirm("<?php echo get_string('deletecoursecheck'); ?>") == true) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/management/course/edu_class_delete.php" ?>',
                method: 'POST',
                data: {
                    classids: course_list,
                    type: 1
                },
                success: function (data) {
                    document.location.href = "<?php echo $CFG->wwwroot . "/local/management/course/edu_class.php" ?>";
                }
            });
        }
    }

    /**
     * 수강생 엑셀 등록 팝업
     * @param element button
     * @returns      
     * */
    function enrol_user(button) {
        var courseid = $(button).data('course');
        var tag = $("<div id='course_enrol_popup'></div>");
        $.ajax({
            url: '<?php echo $CFG->wwwroot . '/local/management/course/enrol_course_excel.php'; ?>',
            method: 'POST',
            data: {
                id: courseid
            },
            success: function (data) {
                tag.html(data).dialog({
                    title: '수강생 엑셀 등록',
                    modal: true,
                    width: 600,
                    resizable: false,
                    height: 300,
                    buttons: [{id: 'close',
                            text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                            disable: true,
                            click: function () {
                                $(this).dialog("close");
                            }}],
                    close: function () {
                        $('#frm_enrol_excel').remove();
                        $(this).dialog('destroy').remove()
                    }
                }).dialog('open');
            }
        });
    }
</script>    
