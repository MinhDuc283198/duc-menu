<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/local/management/lib.php');

$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$courseid = required_param('courseid', PARAM_INT);

$returnvalue = new stdClass();
if (is_siteadmin($USER)) {
    $lmsdatacourse = $DB->get_record('lmsdata_course', array('courseid' => $courseid));
    if ($lmsdatacourse) {
        $DB->delete_records('lmsdata_course', array('id' => $lmsdatacourse->id));

        // lmsdata_class에 연결되어있는 course 지우기
        $crscourse = $DB->get_record('course', array('id' => $lmsdatacourse->courseid));
        if ($crscourse) {
            delete_course($crscourse, FALSE);
        }
        $class = $DB->get_records('lmsdata_class', array('parentcourseid' => $lmsdatacourse->id));
        if ($class) {
            foreach ($class as $clas) {
                $DB->delete_records('lmsdata_class', array('id' => $clas->id));

                // lmsdata_course에 연결되어있는 course 지우기
                $clscourse = $DB->get_record('course', array('id' => $clas->courseid));
                if ($clscourse) {
                    delete_course($clscourse, FALSE);
                }
                // 수강신청 내역 지우기
                $applicationclass = $DB->get_records('lmsdata_course_applications', array('courseid' => $clas->courseid));
                if ($applicationclass) {
                    foreach ($applicationclass as $class) {
                        $DB->delete_records('lmsdata_course_applications', array('id' => $class->id));
                    }
                }
                $enrol = $DB->get_record('enrol', array('courseid' => $clas->courseid));
                if ($enrol) {
                    $DB->delete_records('enrol', array('id' => $enrol->id));
                }

                $applicationcancel = $DB->get_records('lmsdata_applications_cancel', array('courseid' => $clas->courseid));
                if ($applicationcancel) {
                    foreach ($applicationcancel as $cancel) {
                        $DB->delete_records('lmsdata_applications_cancel', array('id' => $cancel->id));
                    }
                }
                // 강의평가 내역 지우기
                $evaluation = $DB->get_record('lmsdata_contents_evaluation', array('courseid' => $clas->courseid));
                if ($evaluation) {
                    $DB->delete_records('lmsdata_contents_evaluation', array('id' => $evaluation->id));
                }
            }
        }
        $returnvalue->status = 'success';
    } else {
        $returnvalue->status = 'fail';
    }
} else {
    $returnvalue->status = 'fail';
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
