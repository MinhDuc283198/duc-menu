<?php

/**
 * select 박스 선택시 차수를 가져오는 ajax 파일
 */
require_once(__DIR__ . '/../../../config.php');

$returnvalue = new stdClass();
$type = required_param('type', PARAM_INT);
if ($type == 1) {
    $pid = required_param('pid', PARAM_INT);
    
    $catasql = 'select lc.classnum '
            . 'from {lmsdata_class} lc '
            . 'join {lmsdata_course_applications} lca on lca.courseid = lc.courseid '
            . 'where lc.classnum != 0 and lc.classyear = :lcclassyear ';

    $catagories = $DB->get_records_sql($catasql, array('lcclassyear' => $pid));
    if ($catagories) {
        $returnvalue->status = 'success';
        $returnvalue->categories = $catagories;
    } else {
        $returnvalue->status = 'error';
        $returnvalue->message = get_string('empty_case', 'local_lmsdata');
    }
} else if ($type == 2) {
    $pid = required_param('pid', PARAM_INT);
    
    $catasql = 'select lc.classnum '
            . 'from {lmsdata_class} lc '
            . 'join {lmsdata_course_applications} lca on lca.courseid = lc.courseid '
            . 'join {lmsdata_applications_cancel} lac on lac.applicationid = lca.id '
            . 'where lc.classnum != 0 and lc.classyear = :lcclassyear ';

    $catagories = $DB->get_records_sql($catasql, array('lcclassyear' => $pid));
    if ($catagories) {
        $returnvalue->status = 'success';
        $returnvalue->categories = $catagories;
    } else {
        $returnvalue->status = 'error';
        $returnvalue->message = get_string('empty_case', 'local_lmsdata');
    }
} else {
    $year = required_param('year', PARAM_INT);
    $object = required_param('object', PARAM_INT);

    $returnvalue = new stdClass();

    $catasql = 'select lc.classnum '
            . 'from {lmsdata_class} lc '
            . 'join {lmsdata_course} lco on lco.id = lc.parentcourseid '
            . 'where lc.classyear = :lcclassyear and (lco.classobject = :lcoclassobject or lco.classtype = :lcoclasstype)';
    $catagories = $DB->get_records_sql($catasql, array('lcclassyear' => $year, 'lcoclassobject' => $object, 'lcoclasstype' => $object));
    if ($catagories) {
        $returnvalue->status = 'success';
        $returnvalue->categories = $catagories;
    } else {
        $returnvalue->status = 'error';
        $returnvalue->message = get_string('empty_case', 'local_lmsdata');
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
