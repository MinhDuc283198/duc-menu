<?php

define('TRANS', '');
define('TRANS_THUMB', '');
define('MEDIA', '');
define('STORAGE', '/home/appdata/lcmsdata');
define('STORAGE2', '/home/appdata');

//define('STORAGE','/appdata/lcmsdata');
//define('STORAGE2','/appdata');

$LCFG = new stdClass();
$LCFG->allowexthtml = array('zip', 'html');
$LCFG->allowextword = array('hwp', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf');
$LCFG->allowextref = array('hwp', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf', 'mp4', 'mp3', 'wmv');
$LCFG->notallowfile = array('sh', 'exe', 'js', 'php', 'sql', 'jsp', 'asp', 'cgi', 'php3', 'php4', 'php5', 'unknown');

/**
 * 컨텐츠 가져오기
 * @param type int $userid
 * @param type int $groupid
 * @param type string $search
 * @param type int $page
 * @param type int $perpage 
 * @return stdClass total_count , total_pages , files , num
 */
function get_contents($userid = 0, $category = 0, $search = "", $page = 1, $perpage = 10) {
    global $DB;
    $like = '';
    if (!empty($search)) {
        $like .= " and " . $DB->sql_like('con.con_name', ':search', false);
    }
    $userwhere = "";

    if ($category > 0) {
        $userwhere .= " and con.category = :category ";
    }
    if (is_siteadmin($USER->id)) {
        if ($userid != 0) {
            $userwhere = "and rep.userid = :userid";
        }
        $sql = "select "
                . "rep.id , rep.referencecnt, "
                . "con.id as conid , con.share_yn, con.con_type, con.con_name, con.reg_dt, con.update_dt, con.category, ca.parent "
                . "from {lcms_repository} as rep "
                . "join {lcms_contents} as con on con.id = rep.lcmsid " . $like
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type " . $userwhere . " order by con.id desc";
        $count_sql = "select "
                . "count(rep.id) "
                . "from {lcms_repository} rep "
                . "join {lcms_contents} con on con.id = rep.lcmsid " . $like
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type " . $userwhere;
    } else {
        $sql = "select "
                . "rep.id , rep.referencecnt, "
                . "con.id as conid , con.share_yn, con.con_type, con.con_name, con.reg_dt, con.update_dt, con.category, ca.parent "
                . "from {lcms_repository} as rep "
                . "join {lcms_contents} as con on con.id = rep.lcmsid and con.course_cd = :luid " . $like
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type order by con.id desc";
        $count_sql = "select "
                . "count(rep.id) "
                . "from {lcms_repository} rep "
                . "join {lcms_contents} con on con.id = rep.lcmsid and con.course_cd = :luid " . $like
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type";
    }

    $return = new stdClass();
    $return->total_count = $DB->count_records_sql($count_sql, array('luid' => $userid, 'category' => $category, 'search' => '%' . $search . '%', 'type' => 'ref', 'userid' => $userid));
    $return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }

    $return->files = $DB->get_records_sql($sql, array('luid' => $userid, 'category' => $category, 'search' => '%' . $search . '%', 'type' => 'ref', 'userid' => $userid), $offset, $perpage);
    $return->num = $return->total_count - (($page - 1) * $perpage);

    return $return;
}

/**
 * 컨텐츠 가져오기
 * @param type int $userid
 * @param type int $category1
 * @param type int $category2
 * @param type string $search
 * @param type int $page
 * @param type int $perpage 
 * @return stdClass total_count , total_pages , files , num
 */
function get_contents_list($userid = 0, $category1 = 0, $category2 = 0, $where_array = array(), $params=array() ,$search = "", $page = 0, $perpage = 0) {
    global $DB;
    $like = '';
    if (!empty($search)) {
        $where_search_array[] = $DB->sql_like('con.con_name', ':con_name');
        $params['con_name'] = '%' . $search . '%';
        $where_search_array[] = $DB->sql_like('con.con_name', ':con_name_vi');
        $params['con_name_vi'] = '%' . $search . '%';
        $where_search_array[] = $DB->sql_like('con.con_name', ':con_name_en');
        $params['con_name_en'] = '%' . $search . '%';
        $where_search_array[] = $DB->sql_like('con.code', ':code');
        $params['code'] = '%' . $search . '%';
        $where_array[] = '('.implode(' OR  ', $where_search_array).')';
    }
    $sql_where = '';
    if(!empty($where_array)){
        $sql_where .= ' AND ' . implode(' and ', $where_array);
    }

    $userwhere = "";

    $category;
    if ($category2 > 0) {
        $userwhere .= " and con.category = :category ";
        $category = $category2;
    } else if ($category1 > 0) {
        $userwhere .= " and ca.parent = :category ";
        $category = $category1;
    }
    //if (is_siteadmin($USER->id)) {
        if ($userid != 0) {
            $userwhere = "and rep.userid = :userid";
        }
        $sql = "select "
                . "rep.id , rep.referencecnt, "
                . "con.id as conid , con.share_yn, con.con_type, con.con_name,con.con_name_vi,con.con_name_en, con.code, con.reg_dt, con.update_dt, con.category, ca.parent "
                . "from {lcms_repository} as rep "
                . "join {lcms_contents} as con on con.id = rep.lcmsid " . $like
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type " . $userwhere .$sql_where. " order by con.id desc";
        $count_sql = "select "
                . "count(rep.id) "
                . "from {lcms_repository} rep "
                . "join {lcms_contents} con on con.id = rep.lcmsid " . $like
                . "join {lmsdata_category} as ca on ca.id = con.category "
                . " where con.con_type!=:type " . $userwhere . $sql_where;
    //} else {
//        $sql = "select "
//                . "rep.id , rep.referencecnt, "
//                . "con.id as conid , con.share_yn, con.con_type, con.con_name, con.reg_dt, con.update_dt, con.category, ca.parent "
//                . "from {lcms_repository} as rep "
//                . "join {lcms_contents} as con on con.id = rep.lcmsid and con.course_cd = :luid " . $like
//                . "join {lmsdata_category} as ca on ca.id = con.category "
//                . " where con.con_type!=:type order by con.id desc";
//        $count_sql = "select "
//                . "count(rep.id) "
//                . "from {lcms_repository} rep "
//                . "join {lcms_contents} con on con.id = rep.lcmsid and con.course_cd = :luid " . $like
//                . "join {lmsdata_category} as ca on ca.id = con.category "
//                . " where con.con_type!=:type ";
//    }
    $params = array_merge($params,array('luid' => $userid, 'category' => $category, 'type' => 'ref', 'userid' => $userid));

    $return = new stdClass();
    $return->total_count = $DB->count_records_sql($count_sql, $params);
    //$return->total_pages = repository_get_total_pages_inadmin($return->total_count, $perpage);

    $offset = 0;
    if ($page != 0) {
        $offset = ($page - 1) * $perpage;
    }
    if($page == 0){
        $return->files = $DB->get_records_sql($sql,$params);
        $return->num = $return->total_count;
    }else{
        $return->files = $DB->get_records_sql($sql,$params , $offset, $perpage);
        $return->num = $return->total_count - (($page - 1) * $perpage);
    }
    return $return;
}

/**
 * 토탈 페이지 가져오기
 * @param type $rows
 * @param type $limit
 * @return int
 */
function repository_get_total_pages_inadmin($rows, $limit = 10) {
    if ($rows == 0) {
        return 1;
    }

    $total_pages = (int) ($rows / $limit);

    if (($rows % $limit) > 0) {
        $total_pages += 1;
    }

    return $total_pages;
}

/**
 * 업로드 함수
 * @global stdClass $LCFG
 * @global type $DB
 * @param type $contents
 * @param type $id
 * @return type
 */
function upload_lcms_contents_inadmin($contents, $id = 0) {
    global $LCFG, $DB, $USER;

    $contents = (object) $contents;
    $contents->userid = 2;
    switch ($contents->con_type) {
        case 'video':
            $newconid = lcms_insert_db_inadmin($contents, $id);
            $DB->set_field_select('lcms_contents_file', 'con_seq', $newconid, " id = :fileid ", array('fileid' => $contents->video_file_id));
            $extarr = $LCFG->allowextvideo;
            $n = 0;
            break;
        case 'html':
            if ($contents->file_type == 1) {
                $contents->data_dir = 'lms/html/' . $contents->userid . '/' . date('YmdHis');
                $extarr = $LCFG->allowexthtml;
                $n = 1;
            } else if ($contents->file_type == 2) {
                $contents->emb_type = 'url';
                $contents->emb_code = $contents->file_dir;
                $newconid = lcms_insert_db_inadmin($contents, $id);
            }
            break;
        case 'word':
            $contents->data_dir = 'lms/files/' . $contents->userid . '/' . date('YmdHis');
            $extarr = $LCFG->allowextword;
            $n = 0;
            break;
        case 'ref':
            if ($contents->ref_con_type == 'word') {
                $contents->data_dir = 'lms/ref/' . $contents->userid . '/' . date('YmdHis');
                $extarr = $LCFG->allowextref;
                $n = 0;
            } else if ($contents->ref_con_type == 'embed') {
                $newconid = lcms_insert_db_inadmin($contents, $id);
            }
            break;
        case 'embed':
            $newconid = lcms_insert_db_inadmin($contents, $id);
            $youtubeduration = get_youtube_info($contents);
            if (!empty($youtubeduration)) {
                $youtubefiledatas = new stdClass();
                $youtubefiledatas->con_seq = $newconid;
                $youtubefiledatas->filepath = $contents->emb_code;
                $youtubefiledatas->fileoname = $contents->emb_code;
                $youtubefiledatas->filename = $contents->emb_code;
                $youtubefiledatas->filesize = '0';
                $youtubefiledatas->duration = $youtubeduration;
                $youtubefiledatas->con_type = 'youtube';
                $youtubefiledatas->user_no = $USER->id;
                $DB->insert_record('lcms_contents_file', $youtubefiledatas);
            }
            break;
        case 'visangurl':
            $newconid = lcms_insert_db_inadmin($contents, $id);
            $youtubeduration = get_youtube_info($contents);

            if (!empty($youtubeduration)) {
                $youtubefiledatas = new stdClass();
                $youtubefiledatas->con_seq = $newconid;
                $youtubefiledatas->filepath = $contents->emb_code;
                $youtubefiledatas->fileoname = $contents->emb_code;
                $youtubefiledatas->filename = $contents->emb_code;
                $youtubefiledatas->filesize = '0';
                $youtubefiledatas->duration = $youtubeduration;
                $youtubefiledatas->con_type = '';
                $youtubefiledatas->user_no = $USER->id;
                $DB->insert_record('lcms_contents_file', $youtubefiledatas);
            }
            break;
        case 'tplan' :
            $newconid = lcms_insert_db_inadmin($contents, $id);
            break;
    }
    if ($contents->con_type != "embed" && $contents->con_type != "video" && $contents->ref_con_type != 'embed' && $contents->con_type != "tplan" && $contents->con_type != "visangurl") {
        if ($contents->con_type != 'html' || ($contents->con_type == 'html' && $contents->file_type == 1)) {
            $filecount_cnt = lcms_temp_dir_allow_filecount_inadmin($extarr, $n, $contents->con_type);
            if ($filecount_cnt == 1) {
                $newconid = lcms_insert_db_inadmin($contents, $id);
                lcms_temp_dir_fileupload_inadmin($extarr, $contents->con_type, $newconid, $contents->data_dir);
            } else {
                echo $filecount_cnt;
                exit;
            }
        }
    }
    if ($newconid) {
        return $newconid;
    }
}

/**
 * 업데이트 함수 
 * @global stdClass $LCFG
 * @global type $DB
 * @global type $USER
 * @param type $contents
 * @return type
 */
function update_lcms_contents_inadmin($contents) {
    global $LCFG, $DB, $USER;
    
    $contents = (object) $contents;
    switch ($contents->con_type) {
        case 'video':
            $newconid = lcms_update_db_inadmin($contents, $contents->stay_file);
            if ($contents->stay_file == 1) {
                $DB->delete_records('lcms_contents_file', array('con_seq' => $contents->con_id));
                $DB->set_field_select('lcms_contents_file', 'con_seq', $newconid, " id = :fileid ", array('fileid' => $contents->video_file_id));
            }
            break;
        case 'html':
            $contents->data_dir = 'lms/html/' . $USER->id . '/' . date('YmdHis');
            $extarr = $LCFG->allowexthtml;
            $n = 1;
            break;
        case 'word':
            $contents->data_dir = 'lms/files/' . $USER->id . '/' . date('YmdHis');
            $extarr = $LCFG->allowextword;
            $n = 0;
            break;
        case 'ref':
            if ($contents->ref_con_type == 'word') {
                $contents->data_dir = 'lms/ref/' . $USER->id . '/' . date('YmdHis');
                $extarr = $LCFG->allowextref;
                $n = 0;
            } else if ($contents->ref_con_type == 'embed') {
                $newconid = lcms_update_db_inadmin($contents, $contents->stay_file);
            }
            break;
        case 'embed':
            lcms_update_db_inadmin($contents, $contents->stay_file);
            $youtubeduration = get_youtube_info($contents);
            $youtubefiledatas = $DB->get_record("lcms_contents_file", array('con_seq' => $contents->con_id));
            if (!empty($youtubeduration) && $youtubefiledatas) {
                $youtubefiledatas->filepath = $contents->emb_code;
                $youtubefiledatas->fileoname = $contents->emb_code;
                $youtubefiledatas->filename = $contents->emb_code;
                $youtubefiledatas->duration = $youtubeduration;
                $youtubefiledatas->con_type = 'youtube';
                $youtubefiledatas->user_no = $USER->id;
                $DB->update_record('lcms_contents_file', $youtubefiledatas);
            }
            break;
        case 'visangurl':
             lcms_update_db_inadmin($contents, $contents->stay_file);
            $youtubeduration = get_youtube_info($contents);
            $youtubefiledatas = $DB->get_record("lcms_contents_file", array('con_seq' => $contents->con_id));
            if (!empty($youtubeduration) && $youtubefiledatas) {
                $youtubefiledatas->filepath = $contents->emb_code;
                $youtubefiledatas->fileoname = $contents->emb_code;
                $youtubefiledatas->filename = $contents->emb_code;
                $youtubefiledatas->duration = $youtubeduration;
                $youtubefiledatas->con_type = 'visangurl';
                $youtubefiledatas->user_no = $USER->id;
                $DB->update_record('lcms_contents_file', $youtubefiledatas);
            }
            
            break;    
        case 'tplan' :
            $newconid = lcms_update_db_inadmin($contents, $contents->stay_file);
//            if ($contents->stay_file == 1) {
//                $fs = get_file_storage();
//                $context = context_system::instance();  
//                /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
//                //if (!($_FILES["script"]['name'] == null || $_FILES["script"]['name'] == '')) {
//                    $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', "tplan", $newconid, 'id');
//                    foreach ($overlap_files as $file) {
//                        if ($file->get_filesize() > 0) {
//                            $filename = $file->get_filename();
//                            $file->delete();
//                        }
//                    }
//                //}
//            }
            break;
    }

    if ($contents->con_type != "embed" && $contents->con_type != "video" && $contents->ref_con_type != 'embed' && $contents->con_type != "tplan" && $contents->con_type != "visangurl" ) {
        if ($contents->stay_file == 1) {
            $filecount_cnt = lcms_temp_dir_allow_filecount_inadmin($extarr, $n, $contents->con_type);
        } else if ($contents->stay_file == 0) {
            $filecount_cnt = 1;
        }
        if ($filecount_cnt == 1) {
            if ($contents->stay_file == 1) {
                $filepath = explode('/', $contents->data_dir);
                if ($filepath[0] == 'storage') {
                    $contents->data_dir = $data_dir;
                }
                $conid = lcms_update_db_inadmin($contents);

                if ($filepath[0] != 'storage' && $contents->con_type == 'html') {
                    $path_dir = STORAGE . '/' . $contents->data_dir;
                    $copy_dir_inadmin = STORAGE . '/trash/' . $contents->data_dir;
                    exec("rm -rf {$path_dir}");
                    $DB->delete_records('lcms_contents_file', array('con_seq' => $conid));
                }

                lcms_temp_dir_fileupload_inadmin($extarr, $contents->con_type, $conid, $contents->data_dir);
            } else {
                $conid = lcms_update_db_inadmin($contents, $contents->stay_file);
            }
        } else {
            echo $filecount_cnt;
            exit;
        }
    }
    return $contents->con_id;
}

/**
 * 파일 카운트 가져오기
 * @global type $CFG
 * @global type $USER
 * @param type $extarr
 * @param type $n
 * @return boolean|string
 */
function lcms_temp_dir_allow_filecount_inadmin($extarr, $n = 0, $type) {
    global $CFG, $USER, $LCFG;
    $temp_dir = $CFG->dirroot . '/local/management/contentsI/contents_upload/server/php/files/' . $USER->id;
    $count = 0;
    if (is_dir($temp_dir)) {
        $dirs = dir($temp_dir);
        while (false !== ($entry = $dirs->read())) {
            if (($entry != '') && ($entry != '.') && ($entry != '..')) {
                if (is_dir($temp_dir . '/' . $entry)) {
                    return get_string('error:notallowfolder', 'local_repository');
                } else {
                    //파일정보가져오기
                    $path = pathinfo($temp_dir . $entry);
                    echo $type;
                    if ($type == 'word' || $type == 'ref') {
                        if (in_array(strtolower($path['extension']), $LCFG->notallowfile)) {
                            return get_string('error:notallowextfile', 'local_repository');
                        } else {
                            $count++;
                        }
                    } else {
                        if (!in_array(strtolower($path['extension']), $extarr)) {
                            return get_string('error:notallowextfile', 'local_repository') . '--';
                        } else {
                            $count++;
                        }
                    }
                }
            }
        } // while end
    }
    if ($count == 0) {
        return get_string('error:notaddfile', 'local_repository');
    } else if ($n > 0 && $count > $n) {
        return get_string('error:addfilecount', 'local_repository', $n);
    } else {
        return 1;
    }
}

/**
 * DB 업로드 정보 저장
 * @global type $DB
 * @param type $contents
 * @param type $id
 * @return type
 */
function lcms_insert_db_inadmin($contents, $id = 0) {
    global $DB;

    $con_db = new stdClass();
    
    //코드 가져오기
    $ordernum = $DB->get_record('lcms_contents', array('year'=>date('y',time())),'max(ordernum) as max');
    $con_db->ordernum = $ordernum->max+1;
    $con_db->year = date('y',time());

    //코드 중복체크
    $con_db->code = $con_db->year.'M'.sprintf("%04d",$con_db->ordernum);
    $overlap_cd1 = $DB->get_record('lcms_contents', array('code'=>$con_db->coursecd));
    if($overlap_cd1){
        echo "코드가 중복되었습니다.";
        exit;
    }

    $con_db->area_cd = 1;
    $con_db->major_cd = 1;
    $con_db->course_cd = $contents->userid;

    $user_name = $DB->get_record('user', array('id' => $contents->userid));

    $con_db->teacher = $contents->teacher;

    $con_db->share_yn = $contents->share_yn;
    $con_db->onlymain = $contents->onlymain;
    $con_db->category = $contents->category2;   //ㅣlmsdata_category 

    $con_db->con_name = htmlspecialchars($contents->con_name, ENT_QUOTES);
    $con_db->con_name_en = htmlspecialchars($contents->con_name_en, ENT_QUOTES);
    $con_db->con_name_vi = htmlspecialchars($contents->con_name_vi, ENT_QUOTES);
    $con_db->con_type = $contents->con_type;

    $con_db->con_des = $contents->con_des;

    // $con_db->con_tag = $contents->con_tag;
    if (isset($contents->con_total_time)) {
        $con_db->con_total_time = $contents->con_total_time;
    } else {
        $con_db->con_total_time = 0;
    }
    if (!empty($contents->author)) {
        $con_db->author = $contents->author;
    } else {
        $con_db->author = "";
    }
    if ($contents->con_type != 'ref' && $contents->con_type != 'tplan') {
        $con_db->cc_type = $contents->cc_type;
        if ($contents->cc_type != 3) {
            if (!empty($contents->cc_mark)) {
                $con_db->cc_mark = $contents->cc_mark;
            } else {
                $con_db->cc_mark = '';
            }
        } else {
            $con_db->cc_mark = $contents->cc_text;
        }
    } else {
        $con_db->cc_type = '';
        $con_db->cc_mark = '';
    }

    $con_db->embed_type = "";
    $con_db->embed_code = "";
    if ($contents->con_type == "embed" || $contents->con_type == "html") {
        if (strpos($contents->emb_code, 'youtube.com') !== false) {
            $cutstr = strpos($contents->emb_code, '&list=');
            $removelisttext = $cutstr == 0 ? substr($contents->emb_code, 0) : substr($contents->emb_code, 0, $cutstr);
            $contents->emb_code = $removelisttext;
            $con_db->embed_type = 'youtube';
        } else if (strpos($contents->emb_code, 'vimeo.com') !== false) {
            $con_db->embed_type = 'vimeo';
        } else {
            $con_db->embed_type = $contents->emb_type;
        }
        $con_db->embed_code = $contents->emb_code;
    }
    //참고자료
    if (isset($contents->ref_con_type)) {
        $con_db->ref_con_type = $contents->ref_con_type;

        if (strpos($contents->emb_code, 'youtube.com') !== false || strpos($contents->emb_code, 'youtu.be') !== false) {
            $con_db->embed_type = 'youtube';
        } else if (strpos($contents->emb_code, 'vimeo.com') !== false) {
            $con_db->embed_type = 'vimeo';
        } else {
            $con_db->embed_type = $contents->emb_type;
        }
        $con_db->embed_code = $contents->emb_code;
    }

    if ($contents->con_type == "media") {
        $con_db->embed_type = "mediaid";
        $con_db->embed_code = $contents->mediaid;
    }
    $con_db->data_dir = $contents->data_dir;
    $con_db->user_no = $contents->userid;
    $con_db->con_hit = 0;
    $con_db->reg_dt = time();
    $con_db->update_dt = time();
    print_object($con_db);

    $new_conid = $DB->insert_record('lcms_contents', $con_db);


    if ($id != 0) {
        $ref_con_db = new stdClass();
        $ref_con_db->lcmsid = $new_conid;
        $ref_con_db->repository = $id;
        $ref_con_db->userid = $contents->userid;
        $ref_con_db->groupid = 0;
        $new_ref_conid = $DB->insert_record('lcms_repository_reference', $ref_con_db);
        $rep = $DB->get_record('lcms_repository', array('id' => $id), 'referencecnt');
        $DB->set_field('lcms_repository', 'referencecnt', $rep->referencecnt + 1, array('id' => $id));
    } else {
        $rep_con_db = new stdClass();
        $rep_con_db->lcmsid = $new_conid;
        $rep_con_db->userid = $contents->userid;
        if (empty($contents->groupid))
            $contents->groupid = 0;
        $rep_con_db->groupid = $contents->groupid;
        $rep_con_db->referencecnt = 0;
        if ($con_db->con_type == 'embed' || $con_db->con_type == 'tplan') {
            $rep_con_db->status = 1;
        } else {
            //비상 트랜스코딩 미지원으로 1로 변경 원래의 값 5
            $rep_con_db->status = 1;
        }

        $new_rep_conid = $DB->insert_record('lcms_repository', $rep_con_db);
    }

    return $new_conid;
}

/**
 * DB정보 업데이트
 * @global type $DB
 * @global type $USER
 * @param type $contents
 * @param type $file_change
 * @return type
 */
function lcms_update_db_inadmin($contents, $file_change = 0) {

    global $DB, $USER, $CFG;
    $con_db = new stdClass();
    $con_db->id = $contents->con_id;
    //$con_db->course_cd = $USER->id;
    $con_db->teacher = $contents->teacher;

    $con_db->share_yn = $contents->share_yn;
    $con_db->onlymain = $contents->onlymain;
    $con_db->con_name = htmlspecialchars($contents->con_name, ENT_QUOTES);
    $con_db->con_name_vi = htmlspecialchars($contents->con_name_vi, ENT_QUOTES);
    $con_db->con_name_en = htmlspecialchars($contents->con_name_en, ENT_QUOTES);
    $con_db->con_des = $contents->con_des;
    $con_db->con_tag = $contents->con_tag;
    $con_db->category = $contents->category2;
    
    if ($contents->con_total_time) {
        $con_db->con_total_time = $contents->con_total_time;
    } else {
        $con_db->con_total_time = 0;
    }
       
    if (($contents->con_type == "visangurl"&& $file_change == 1) ||( $contents->con_type == "embed" && $file_change == 1)) {
        if (strpos($contents->emb_code, 'youtube.com') !== false || strpos($contents->emb_code, 'youtu.be') !== false) {
            $con_db->embed_type = 'youtube';
        } else if (strpos($contents->emb_code, 'vimeo.com') !== false) {
            $con_db->embed_type = 'vimeo';
        } else {
            $con_db->embed_type = $contents->emb_type;
        }
        $con_db->embed_code = $contents->emb_code;
    }
    if ($file_change == 1) {
        $con_db->data_dir = $contents->data_dir;
        $con_db->con_type = $contents->con_type;
    }
    //$con_db->user_no = $USER->id;
    $con_db->update_dt = time();

    if (!empty($contents->author)) {
        $con_db->author = $contents->author;
    } else {
        $con_db->author = "";
    }
    if ($contents->cc_type) {
        $con_db->cc_type = $contents->cc_type;
    } else {
        $con_db->cc_type = '';
    }
    if ($contents->cc_type != 3) {
        if (!empty($contents->cc_mark)) {
            $con_db->cc_mark = $contents->cc_mark;
        } else {
            $con_db->cc_mark = '';
        }
    } else {
        $con_db->cc_mark = $contents->cc_text;
    }
    $conid = $DB->update_record('lcms_contents', $con_db);
    if ($contents->con_type != 'ref') {
        $repository = $DB->get_record('lcms_repository', array('lcmsid' => $contents->con_id));
    } else {
        $repository = $DB->get_record('lcms_repository_reference', array('lcmsid' => $contents->con_id));
    }
    $rep_con_db = new stdClass();
    $rep_con_db->id = $repository->id;
    $rep_con_db->userid = $USER->id;
    $rep_con_db->groupid = $contents->groupid;
    if ($contents->con_type == "video" && $file_change == 1) {
        //$rep_con_db->status = 5;
        $rep_con_db->status = 1;
    }
    if ($contents->con_type != 'ref') {
        $new_rep_conid = $DB->update_record('lcms_repository', $rep_con_db);
    } else {
        $new_rep_conid = $DB->update_record('lcms_repository_reference', $rep_con_db);
    }

    //재능교육 특화 - 신임과정, 연차별과정 중 종료된 과정에 포함된 okmedia를 가져옴
    if ($contents->better) {
        require_once $CFG->dirroot . '/local/jeipoint/lib.php';
        $idnumbers = array(
            JEL_COURSE_CATEGORY_TRUST => JEL_COURSE_CATEGORY_TRUST,
            JEL_COURSE_CATEGORY_YEAR => JEL_COURSE_CATEGORY_YEAR
        );
        list($idnumberssql, $params) = $DB->get_in_or_equal(array_keys($idnumbers), SQL_PARAMS_NAMED, 'c0');
        $sql = 'SELECT 
                        ok.* 
                FROM {okmedia} ok 
                JOIN {course} co ON co.id = ok.course
                JOIN {course_categories} cc ON cc.id = co.category
                JOIN {lmsdata_class} lc ON lc.courseid = co.id
                WHERE ok.contents = :contents AND lc.learningend < :learningend AND cc.idnumber ' . $idnumberssql;
        $params['contents'] = $contents->con_id;
        $params['learningend'] = time();
        $okmedias = $DB->get_records_sql($sql, $params);

        if (!empty($okmedias)) {
            require_once $CFG->dirroot . '/mod/okmedia/locallib.php';
            foreach ($okmedias AS $okmedia) {
                local_jeipoint_deduction($okmedia->id);
                $okmedia->timeend = time() + (60 * 60 * 24 * 30);
                $DB->update_record('okmedia', $okmedia);
                fcm_push($okmedia, 'update');
            }
        }
    }



    return $contents->con_id;
}

/**
 * 파일 업로드 함수
 * @global type $CFG
 * @global type $USER
 * @global stdClass $LCFG
 * @param type $extarr
 * @param type $type
 * @param type $con_seq
 * @param type $storage
 * @return type
 */
function lcms_temp_dir_fileupload_inadmin($extarr, $type, $con_seq, $storage) {

    global $CFG, $USER, $LCFG;

    $temp_dir = $CFG->dirroot . '/local/management/contentsI/contents/contents_upload/server/php/files/' . $USER->id;
    $count = 0;

    if (is_dir($temp_dir)) {

        $dirs = dir($temp_dir);

        while (false !== ($entry = $dirs->read())) {
            if (($entry != '') && ($entry != '.') && ($entry != '..')) {
                if (is_dir($temp_dir . '/' . $entry)) {
                    //return '파일 폴더구조가 포함되어 있으면 안됩니다.';
                } else {
                    //임시 파일
                    $file = $temp_dir . '/' . $entry;
                    //파일정보가져오기
                    $path = pathinfo($file);
                    $ext = strtolower($path['extension']);
                    if ($type == 'word' || $type == 'ref') {
                        if (in_array($ext, $LCFG->notallowfile)) {
                            return get_string('error:notallowextfile', 'local_repository') . '--';
                        } else {
                            $filename = $entry;
                            $count++;
                            lcms_register_files_inadmin($type, $con_seq, $storage, $filename, $count);
                        }
                    } else {
                        if (!in_array($ext, $extarr)) {
                            return get_string('error:notallowextfile', 'local_repository');
                        } else {
                            $filename = $entry;
                            $count++;
                            lcms_register_files_inadmin($type, $con_seq, $storage, $filename, $count);
                        }
                    }
                }
            }
        }
    }
    rmdir($temp_dir);
}

/**
 * 파일 정보 저장 함수
 * @global type $CFG
 * @global type $USER
 * @global type $DB
 * @param type $type
 * @param type $con_seq
 * @param type $storage
 * @param type $filename
 * @param type $count
 * @return string
 */
function lcms_register_files_inadmin($type, $con_seq, $storage, $filename, $count) {

    global $CFG, $USER, $DB;

    //임시 폴더를 열어 파일이 있는지 확인한다.
    $temp_dir = $CFG->dirroot . '/local/management/contentsI/contents/contents_upload/server/php/files/' . $USER->id . '/';
    $path_dir = STORAGE . '/' . $storage . '/';

    if (!is_dir($path_dir)) {
        if (!mkdir($path_dir, 0777, true)) {
            return 'create folder fail';
        }
    }
    if (file_exists($temp_dir . $filename)) {
        $file = $temp_dir . $filename;

        //파일정보가져오기
        $path = pathinfo($file);
        $filesize = filesize($file);
        $ext = strtolower($path['extension']);
        $basename = $path['basename'];
        $filenm = $path['filename'];
        $filenms = explode('_', $filenm);
        $k = sizeof($filenms) - 1;
        if ($type == 'html' && $ext != 'zip') {
            $newfilename = $basename;
        } else {
            $newfilename = date('YmdHis') . '_' . $count . '.' . $ext;
        }
        $newfile = $path_dir . $newfilename;

        rename($file, $newfile);

        if ($type == 'html' && $ext == 'zip') {
            //압축파일 해제	
            $zipfile = new PclZip($newfile);
            $extract = $zipfile->extract(PCLZIP_OPT_PATH, $path_dir);
            @unlink($newfile);
            $basename = 'index.html';
            $newfilename = $basename;
        }

        //파일 정보에 등록
        $cfile_db = new stdClass();
        $cfile_db->con_seq = $con_seq;
        $cfile_db->user_no = $USER->id;
        $cfile_db->filepath = $storage;
        $cfile_db->filename = $newfilename;
        $cfile_db->filesize = $filesize;
        $cfile_db->fileoname = $basename;
        $cfile_db->duration = 0;
        $cfile_db->con_type = $type;

        $DB->insert_record('lcms_contents_file', $cfile_db);
    }
}

/**
 * 삭제 버튼 리턴함수
 * @global type $CFG
 * @global type $USER
 * @param type $mode
 * @return type
 */
function lcms_temp_dir_filemode_inadmin($mode) {

    global $CFG, $USER;

    $temp_dir = $CFG->dirroot . '/local/management/contentsI/contents/contents_upload/server/php/files/' . $USER->id;

    if (is_dir($temp_dir)) {

        $dirs = dir($temp_dir);
        $returnvalue = '';

        while (false !== ($entry = $dirs->read())) {
            if (($entry != '') && ($entry != '.') && ($entry != '..')) {
                if (is_dir($temp_dir . '/' . $entry)) {
                    //return '파일 폴더구조가 포함되어 있으면 안됩니다.';
                } else {
                    //임시 파일
                    $file = $temp_dir . '/' . $entry;
                    //파일 삭제하기
                    if ($mode == 'del')
                        @unlink($file);
                    if ($mode == 'li') {
                        $encodefile = rawurlencode($file);
                        $returnvalue .= '<li><button type="button" class="btn btn-delete delete" data-type="DELETE" data-url="' . $encodefile . '">del</button>' . $entry . '</li>';
                    }
                }
            }
        }
    }
    if ($mode == 'del')
        rmdir($temp_dir);
    if ($mode == 'li')
        return (!empty($returnvalue)) ? $returnvalue : "";
}

/**
 * 폴더 이동함수
 * @param type $path
 * @param type $dst
 * @return boolean
 */
function copy_dir_inadmin($path, $dst) {


    if (!is_dir($dst))
        @mkdir($dst, 0777, true);
    $d = @opendir($path);
    while ($entry = @readdir($d)) {
        if ($entry != "." && $entry != "..") {
            if (is_dir($path . '/' . $entry)) {
                copy_dir_inadmin($path . '/' . $entry, $dst . '/' . $entry);
            } else {
                if (is_file($path . '/' . $entry)) {
                    @copy($path . '/' . $entry, $dst . '/' . $entry);
                    @chmod($dst . '/' . $entry, 0666);
                    @unlink($path . '/' . $entry);
                }
            }
        }
    }
    @closedir($d);
    @rmdir($d);
    return true;
}

/**
 * 폴더 삭제 함수
 * @param type $dir
 */
function del_dir_inadmin($dir) {
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? del_dir_inadmin("$dir/$file") : unlink("$dir/$file");
    }
    rmdir($dir);
}

/**
 * 파일 존재 체크
 * @global type $CFG
 * @param type $entrys
 * @param type $type
 * @param string $path
 * @param type $json
 * @return type
 */
function scan_data_inadmin($entrys, $type, $path, $json = 1) {

    global $CFG;

    $entrys['spath'] = $path;
    $path = STORAGE . '/' . $path;

    if (is_dir($path)) {

        $scans = scandir($path);
        $entrys['path'] = $path;

        foreach ($scans as $scan) {
            if (($scan != '.') && ($scan != '..')) {
                $scan_name = iconv("euc-kr", "utf-8", $scan);
                $scan = rawurlencode($scan);
                if (is_dir($path . '/' . $scan_name)) {
                    $entrys['dir'][] = $scan_name;
                } else {
                    $entrys['file'][] = $scan_name;
                }
            }
        }
    }

    //header('Content-type: application/json');
    if ($json == 1)
        return json_encode($entrys);
    else
        return $entrys;
}

/**
 * 페이징바 호출 
 * @param type $url
 * @param type $params
 * @param type $total_pages
 * @param type $current_page
 * @param type $max_nav
 */
function repository_get_paging_bar_inadmin($url, $params, $total_pages, $current_page, $max_nav = 10) {
    $total_nav_pages = repository_get_total_pages_inadmin($total_pages, $max_nav);
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }
    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = "page=";
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . "?page=";
    }
    echo html_writer::start_tag('div', array('class' => 'table-footer-area'));
    echo html_writer::start_tag('div', array('class' => 'board-breadcrumbs'));
    if ($current_nav_page > 1) {
        // echo '<span class="board-nav-prev"><a class="prev" href="'.$url.(($current_nav_page - 2) * $max_nav + 1).'"><</a></span>';
    } else {
        // echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    if ($current_page > 1) {
        echo '<span class="board-nav-prev"><a class="prev" href="' . $url . ($current_page - 1) . '"><</a></span>';
    } else {
        echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    echo '<ul>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="current"><a href="#">' . $i . '</a></li>';
        } else {
            echo '<li><a href="' . $url . '' . $i . '">' . $i . '</a></li>';
        }
    }
    echo '</ul>';
    if ($current_page < $total_pages) {
        echo '<span class="board-nav-next"><a class="next" href="' . $url . ($current_page + 1) . '">></a></span>';
    } else {
        echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
    }
    if ($current_nav_page < $total_nav_pages) {
        //echo '<a class="next_" href="' . $url . ($current_nav_page * $max_nav + 1) . '"></a>';
    } else {
        //echo '<a class="next_" href="#"></a>';
    }
    echo html_writer::end_tag('div');
    echo html_writer::end_tag('div');
}

/**
 * youtube 정보 가져오기
 * @param type $vid
 * @return \StdClass
 */
function youtubeinfo_inadmin($vid) {

    $obj = new StdClass();

    $xmlfile = "http://gdata.youtube.com/feeds/api/videos/" . $vid;
    $entry = simplexml_load_file($xmlfile);

    // get nodes in media: namespace for media information
    $media = $entry->children('http://search.yahoo.com/mrss/');

    // get video thumbnail
    $attrs = $media->group->thumbnail[0]->attributes();
    $obj->thumbnail = $attrs['url'];

    // get <yt:duration> node for video length
    $yt = $media->children('http://gdata.youtube.com/schemas/2007');
    $attrs = $yt->duration->attributes();
    $obj->duration = $attrs['seconds'];

    return $obj;
}

function mediainfo_inadmin($mid) {
    $url = '';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);
    return json_decode($data);
}

/**
 * 비메오 정보 가져오기
 * @param type $vid
 * @return \StdClass
 */
function vimeoinfo($vid) {

    $obj = new StdClass();

    $xmlfile = "http://vimeo.com/api/v2/video/" . $vid . ".xml";
    $entry = simplexml_load_file($xmlfile);

    $obj->thumbnail = $entry->video->thumbnail_medium;
    $obj->duration = $entry->video->duration;

    return $obj;
}

function get_youtube_info($contents) {
    $youtubeapikey = "AIzaSyDOPCdHZwszJIGjAQBVhbk1stkUTFSbnzU";
    if (strpos($contents->emb_code, 'youtu.be')) {
        print_object($contents->emb_code);
        $cutstr = strstr($contents->emb_code, 'be/');
        $vname = substr($cutstr, 3);
    } else {
        $cutstr = strpos($contents->emb_code, '&list=');
        if (empty($cutstr)) {
            $removelisttext = substr($contents->emb_code, 0);
        } else {
            $removelisttext = substr($contents->emb_code, 0, $cutstr);
        }
        print_object($removelisttext);
        print_object(1);
        $vname = substr(strstr($removelisttext, 'watch?v='), 8);
    }
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://www.googleapis.com/youtube/v3/videos?id=$vname&key=$youtubeapikey&part=contentDetails",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    $datas = json_decode($response);
    $duration = covtime($datas->items[0]->contentDetails->duration);

    return $duration;
}

function covtime($youtube_time) {
    $timeposition = strpos($youtube_time, 'H');
    $timepos = $timeposition != null ? preg_replace("/[^0-9]/", "", substr($youtube_time, ($timeposition - 2), 2)) : 0;
    $minuteposition = strpos($youtube_time, 'M');
    $minutepos = $minuteposition != null ? preg_replace("/[^0-9]/", "", substr($youtube_time, ($minuteposition - 2), 2)) : 0;
    $secondposition = strpos($youtube_time, 'S');
    $secondpos = $secondposition != null ? preg_replace("/[^0-9]/", "", substr($youtube_time, ($secondposition - 2), 2)) : 0;
    $secondpos = ($secondpos - 1) <= 0 ? 59 : ($secondpos - 1);
    
    if ($timepos != 0 && $minutepos == 0 && $secondpos == 59) {
        $timepos = $timepos - 1;
        $minutepos = 59;
    }else if($minutepos != 0 && $secondpos == 59){  
        $minutepos -= 1 ;     
    }


    $parts[] = $timepos;
    $parts[] = $minutepos;
    $parts[] = $secondpos;


    $sec_init = $parts[2];
    $seconds = $sec_init % 60;
    $seconds_overflow = floor($sec_init / 60);

    $min_init = $parts[1] + $seconds_overflow;
    $minutes = ($min_init) % 60;
    $minutes_overflow = floor(($min_init) / 60);

    $hours = $parts[0] + $minutes_overflow;
    if ($seconds) {
        
    }
    $totalsecond = ($hours * 3600 ) + ($minutes * 60) + ($seconds);

    return $totalsecond;
}

function is_ie($server_agent) {
	if(!isset($server_agent))return false;
	if(strpos($server_agent, 'MSIE') !== false) return true; // IE8
	if(strpos($server_agent, 'Windows NT 6.1') !== false) return true; // IE11
	if(strpos($server_agent, 'rv:11.0') !== false) return true; // IE11
	return false;
}
