<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
$pagesettings = array(
        'title' => 'LPI 규준편차',
        'heading' => 'LPI 규준편차',
        'subheading' => '',
        'menu' => 'pisetup_lpi',
        'js' => array(),
        'css' => array(),
        'nav_bar' => array()
);

include_once($CFG->dirroot . '/local/management/header.php');

if (!is_siteadmin()) {
    redirect($CFG->wwwroot);
}

$calculation = [];
$result = $DB->get_record('lpi_calculation_parameters', []);
foreach ($result as $key => $value) {
    $explode = explode('_', $key);
    if (count($explode) === 2) {
        switch (substr(strtoupper($explode[1]), 0, 1)) {
            case 'A':
            case 'E':
            case 'O':
            case 'C':
                if ($explode[0] === 'avg') {
                    $calculation['average'][substr(strtoupper($explode[1]), 0, 1)][ucfirst($explode[1])] = $value;
                } else if ($explode[0] === 'stdev') {
                    $calculation['stdev'][substr(strtoupper($explode[1]), 0, 1)][ucfirst($explode[1])] = $value;
                }
                break;
            default:
                if ($explode[0] === 'avg') {
                    $thiskey = 'ETC';
                    if(count($calculation['average'][$thiskey]) == 5) {
                        $thiskey = 'ETC2';
                    }
                    $calculation['average'][$thiskey][ucfirst($explode[1])] = $value;
                } else if ($explode[0] === 'stdev') {
                    $thiskey = 'ETC';
                    if(count($calculation['stdev'][$thiskey]) == 5) {
                        $thiskey = 'ETC2';
                    }
                    $calculation['stdev'][$thiskey][ucfirst($explode[1])] = $value;
                }
        }
    } else if (count($explode) === 3) {
        $calculation['sum'][ucfirst($explode[2])] = $value;
    }
}

?>
<style>
    th {
        text-align: center;
    }
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">내면진단 계산 버전</h3>
                    <select class="f-r" name="version">
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <h4 class="mg-bt10">적용된 규준평균</h4>
                    <table id="example1" class="table table-bordered">
                        <colgroup>
                            <col width="50px"/>
                            <col width="50px"/>
                            <col width="100px"/>
                            <col width="/"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>버전</th>
                            <th>대상인원</th>
                            <th>적용일시</th>
                            <th>값</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>4</td>
                            <td>5550</td>
                            <td>2019-08-26</td>
                            <td>
                                <table id="example1" class="table table-bordered">
                                    <colgroup>
                                        <col width="10%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th colspan="6">평균</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($calculation['average'] as $key => $value): ?>
                                        <tr>
                                            <th rowspan="2"><?php echo $key; ?></th>
                                            <?php foreach ($value as $key2 => $value2): ?>
                                                <th><?php echo $key2; ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        <tr>
                                            <?php foreach ($value as $key2 => $value2): ?>
                                                <td><?php echo $value2; ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <table id="example1" class="table table-bordered">
                                    <colgroup>
                                        <col width="10%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                        <col width="18%"/>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th colspan="6">표준편차</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($calculation['stdev'] as $key => $value): ?>
                                        <tr>
                                            <th rowspan="2"><?php echo $key; ?></th>
                                            <?php foreach ($value as $key2 => $value2): ?>
                                                <th><?php echo $key2; ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        <tr>
                                            <?php foreach ($value as $key2 => $value2): ?>
                                                <td><?php echo $value2; ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php
                        if ($usebanners) {
                            $cnt = 1;
                            foreach ($usebanners as $usebanner) {
                                ?>
                                <tr>
                                    <td><input class="editsort w-100" style="text-align: center" disabled type="text" sty
                                               value="<?php echo $usebanner->sorting ?>" data_id="<?php echo $usebanner->id; ?>">
                                    </td>
                                    <td class="go_edit text-left"
                                        data_id="<?php echo $usebanner->id; ?>"><?php echo $usebanner->show_title; ?></td>
                                    <td><?php echo $usebanner->use_f; ?></td>
                                    <td><?php echo date('Y-m-d', $usebanner->timecreated); ?>
                                        <button data_id="<?php echo $usebanner->id; ?>" class="btn btn-primary  pull-right preview">
                                            미리보기
                                        </button>
                                    </td>
                                </tr>
                                <?php
                            };
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-body">
                    <h4 class="mg-bt10">실시간 계산결과</h4>
                    <table id="example1" class="table table-bordered">
                        <colgroup>
                            <col width="10%"/>
                            <col width="/"/>
                            <col width="100px"/>
                            <col width="15%"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>정렬</th>
                            <th>제목</th>
                            <th>사용여부</th>
                            <th>생성일</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($usebanners) {
                            $cnt = 1;
                            foreach ($usebanners as $usebanner) {
                                ?>
                                <tr>
                                    <td><input class="editsort w-100" style="text-align: center" disabled type="text" sty
                                               value="<?php echo $usebanner->sorting ?>" data_id="<?php echo $usebanner->id; ?>">
                                    </td>
                                    <td class="go_edit text-left"
                                        data_id="<?php echo $usebanner->id; ?>"><?php echo $usebanner->show_title; ?></td>
                                    <td><?php echo $usebanner->use_f; ?></td>
                                    <td><?php echo date('Y-m-d', $usebanner->timecreated); ?>
                                        <button data_id="<?php echo $usebanner->id; ?>" class="btn btn-primary  pull-right preview">
                                            미리보기
                                        </button>
                                    </td>
                                </tr>
                                <?php
                            };
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <button class="btn btn-danger pull-right banner_write">실시간 규준평균 반영</button>
</section>
<!-- /.content -->


<?php
include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script>
    /* 배너목록 클릭 */
    $(document).ready(function() {
        var status = 0;
        var dataarr = {};
        $(".sortingedit").click(function() {
            if (status == 0) {
                status = 1;
                dataarr = {};
                $(".sortingedit").text("정렬저장");
                $(".editsort").each(function(index, item) {
                    $(item).attr("disabled", false);
                    eval('dataarr[' + $(item).attr('data_id') + ']=$(item).val()');
                });
            } else {
                if (confirm("정렬 순서를 저장 하시겠습니까?")) {
                    status = 0;
                    dataarr = {};
                    $(".sortingedit").text("정렬수정");
                    $(".editsort").each(function(index, item) {
                        $(item).attr("disabled", true);
                        eval('dataarr[' + $(item).attr('data_id') + ']=$(item).val()');
                    });
                    $.ajax({
                        type: 'POST',
                        url: 'banner_edit_sort.php',
                        data: {sorting: dataarr},
                        success: function(data, status, xhr) {
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText);
                        }
                    });
                } else {
                    $(".sortingedit").text("정렬수정");
                    $(".editsort").each(function(index, item) {
                        eval('var inum = (dataarr[' + $(item).attr('data_id') + '])');
                        $(item).val(inum);
                        $(item).attr("disabled", true);
                    });
                }
            }
        });
    })

    $('.go_edit').click(function() {
        var tid = $(this).attr('data_id');
        location.href = "banner_write.php?type=edit&tid=" + tid;
    });

    /* 배너생성 버튼 클릭 */
    $('.banner_write').click(function() {
        location.href = "banner_write.php?type=add";
    });

    /* PC미리보기 버튼 클릭 */
    var url = '<?php echo $CFG->wwwroot; ?>';
    $('.pc_preview').click(function() {
        window.open(url, '_blank', 'width=1000,height=800,location=0,menubar=0,resizable=0', false);
    });
    /* 모바일 미리보기 버튼 클릭 */
    $('.mobile_preview').click(function() {
        window.open(url, '_blank', 'width=450,height=800,location=0,menubar=0,resizable=0', false);
    });

    $('.preview').click(function() {
        var tid = $(this).attr('data_id');
        window.open("<?php echo $CFG->wwwroot .
                '/local/management/banner/showbanner.php?id='?>" + tid, '_blank', 'width=450,height=800,location=0,menubar=0,resizable=0', false);
    });
</script>
