<?php
require('../../../config.php');

$id = optional_param('id', 0, PARAM_INT);


$banner = get_banner($id)[0];

function get_banner($id) {
    global $DB, $CFG;

    $context = context_system::instance();
    $component = 'local_dlp';
    $filearea = 'banner_img';
    $m_filearea = 'm_banner_img';
    $this_time = time();

    $banners = $DB->get_records('dlp_banner', ['id' => $id], 'sorting asc');

    foreach ($banners as $banner) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, $component, "banner_img", $banner->id, 'id');
        $m_files = $fs->get_area_files($context->id, $component, "m_banner_img", $banner->id, 'id');
        $path = '';
        foreach ($files as $file) {
            $filename = $file->get_filename();
            $mimetype = $file->get_mimetype();
            if ($file->get_filesize() > 0) {
                $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/' . $component . '/' . $filearea . '/' . $banner->id . '/' . $filename);
            }
        }
        foreach ($m_files as $m_file) {
            $m_filename = $m_file->get_filename();
            $m_mimetype = $m_file->get_mimetype();
            if ($m_file->get_filesize() > 0) {
                $m_path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/' . $component . '/' . $m_filearea . '/' . $banner->id . '/' . $m_filename);
            }
        }
        $banner->path = $path;
        $banner->m_path = $m_path;
        if ($banner->target == 1) {
            $banner->targetpage = '_blank';
        }
    }

    return array_values($banners);
}
?>
<link href="/theme/oklassv3/style/util.css" rel="stylesheet" />
<link href="/theme/oklassv3/style/common.css" rel="stylesheet" />
<link href="/theme/oklassv3/style/style.css" rel="stylesheet" />
<link href="/theme/oklassv3/style/style_new.css" rel="stylesheet" />
<link href="/theme/oklassv3/style/community.css" rel="stylesheet" />
<link href="/theme/oklassv3/style/jt.css" rel="stylesheet" />
<link href="/theme/oklassv3/style/jt2.css" rel="stylesheet" />
<style>
    .index-slide .item, .index-slide .item.m_path{display: block !important;}
    #indexSect .index-slide .btn-primary{text-decoration: none;box-sizing: border-box;}
    @media (min-width: 1200px){
        #indexSect .index-slide .btn-primary{line-height: 1.25;}
    }
</style>
<div id="page-wrapper">
    <div role="main" id="wrap">
        <div class="index">
            <!-- 메인 슬라이드 -->
            <section id="indexSect">
                <div class="index-slide">

                    <div class="m-slider">
                        <div class="item" style="background: url(<?php echo $banner->m_path ?>)) no-repeat 50% 0; background-size:cover;">
                            <div class="container">
                                <h2 class="font-lg"><?php echo $banner->sub_title ?></h2>
                                <pre><p class="font-lg"><?php echo $banner->title ?></p></pre>
                                <a class="btn btn-primary btn-lg" href="<?php echo $banner->link ?>" tabindex="0"><?php echo $banner->btn_text ?><span class="caret-right"></span></a>
                            </div>
                        </div>
                        <div class="item m_path" style="background: url(<?php echo $banner->path ?>) no-repeat 50% 0; background-size:cover;">
                            <div class="container">
                                <h2 class="font-lg"><?php echo $banner->sub_title ?></h2>
                    <pre><p class="font-lg"><?php echo $banner->title ?></p></pre>
                    <a class="btn btn-primary btn-lg" href="<?php echo $banner->link ?>" tabindex="0"><?php echo $banner->btn_text ?><span class="caret-right"></span></a>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</section>
</div>
</div>
</div>

