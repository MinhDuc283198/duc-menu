<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '배너관리',
    'heading' => '배너관리',
    'subheading' => '',
    'menu' => 'banner',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);
include_once($CFG->dirroot.'/local/management/header.php');
$usebanners = $DB->get_records('dlp_banner',array('use_f'=>'Y')," timecreated desc");
$notusebanners = $DB->get_records('dlp_banner',array('use_f'=>'N')," timecreated desc");
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!--            
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                    -->
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4 class="mg-bt10">사용중인 배너</h4>
                        <table id="example1" class="table table-bordered">
                             <colgroup>
                                <col width="10%" />
                                <col width="/" />
                                <col width="100px" />
                                <col width="15%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>정렬</th>
                                    <th>제목</th>
                                    <th>사용여부</th>
                                    <th>생성일</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($usebanners) {
                                    $cnt = 1;
                                    foreach ($usebanners as $usebanner) {
                                        ?>
                                        <tr>
                                            <td><input class="editsort w-100" style="text-align: center" disabled type="text" sty value="<?php echo $usebanner->sorting ?>" data_id="<?php echo $usebanner->id; ?>"></td>
                                            <td class="go_edit text-left" data_id="<?php echo $usebanner->id; ?>"><?php echo $usebanner->show_title; ?></td>
                                            <td><?php echo $usebanner->use_f; ?></td>
                                            <td><?php echo date('Y-m-d', $usebanner->timecreated); ?><button data_id="<?php echo $usebanner->id; ?>" class="btn btn-primary  pull-right preview">미리보기</button></td>
                                        </tr>    
                                        <?php
                                    };
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                     <div class="box-body">
                         <h4 class="mg-bt10">미사용 배너</h4>
                        <table id="example1" class="table table-bordered">
                            <colgroup>
                                <col width="10%" />
                                <col width="/" />
                                <col width="100px" />
                                <col width="15%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>정렬</th>
                                    <th>제목</th>
                                    <th>사용여부</th>
                                    <th>생성일</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($notusebanners) {
                                    $cnt = 1;
                                    foreach ($notusebanners as $banner) {
                                        ?>
                                        <tr>
                                            <td><input class="editsort text-center w-100" disabled type="text" sty value="<?php echo $banner->sorting ?>" data_id="<?php echo $banner->id; ?>"></td>
                                            <td class="go_edit text-left" data_id="<?php echo $banner->id; ?>"><?php echo $banner->show_title; ?></td>
                                            <td><?php echo $banner->use_f; ?></td>
                                            <td><?php echo date('Y-m-d', $banner->timecreated); ?><button data_id="<?php echo $banner->id; ?>" class="btn btn-primary  pull-right preview">미리보기</button></td>
                                        </tr>    
                                        <?php
                                    };
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <button class="btn btn-primary pull-left pc_preview">PC버전 미리보기</button>
        <button class="btn btn-primary pull-left mobile_preview">모바일버전 미리보기</button>
        <button class="btn btn-danger pull-right banner_write">배너생성</button>
        <button class="btn btn-primary  pull-right sortingedit">정렬수정</button>
    </section>
    <!-- /.content -->


<?php
include_once($CFG->dirroot.'/local/management/footer.php');
?>

<script>
    /* 배너목록 클릭 */
    $(document).ready(function () {
        var status = 0;
        var dataarr = {};
        $(".sortingedit").click(function () {
            if (status == 0) {
                status = 1;
                dataarr = {};
                $(".sortingedit").text("정렬저장");
                $(".editsort").each(function (index, item) {
                    $(item).attr("disabled", false);
                    eval('dataarr[' + $(item).attr('data_id') + ']=$(item).val()');
                });
            } else {
                if (confirm("정렬 순서를 저장 하시겠습니까?")) {
                    status = 0;
                    dataarr = {};
                    $(".sortingedit").text("정렬수정");
                    $(".editsort").each(function (index, item) {
                        $(item).attr("disabled", true);
                        eval('dataarr[' + $(item).attr('data_id') + ']=$(item).val()');
                    });
                    $.ajax({
                        type: 'POST',
                        url: 'banner_edit_sort.php',
                        data: {sorting: dataarr},
                        success: function (data, status, xhr) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText);
                        }
                    });
                } else {
                    $(".sortingedit").text("정렬수정"); 
                    $(".editsort").each(function (index, item) {
                        eval('var inum = (dataarr[' + $(item).attr('data_id') + '])');
                        $(item).val(inum);
                        $(item).attr("disabled", true);
                    });
                }
            }
        });
    })

    $('.go_edit').click(function () { 
        var tid = $(this).attr('data_id');
        location.href = "banner_write.php?type=edit&tid=" + tid;
    });

    /* 배너생성 버튼 클릭 */
    $('.banner_write').click(function () {
        location.href = "banner_write.php?type=add";
    });

    /* PC미리보기 버튼 클릭 */
    var url = '<?php echo $CFG->wwwroot; ?>';
    $('.pc_preview').click(function () {
        window.open(url, '_blank', 'width=1000,height=800,location=0,menubar=0,resizable=0', false);
    });
    /* 모바일 미리보기 버튼 클릭 */
    $('.mobile_preview').click(function () {
        window.open(url, '_blank', 'width=450,height=800,location=0,menubar=0,resizable=0', false);
    });
    
    $('.preview').click(function () {
        var tid = $(this).attr('data_id');
        window.open("<?php echo $CFG->wwwroot.'/local/management/banner/showbanner.php?id='?>"+tid, '_blank', 'width=450,height=800,location=0,menubar=0,resizable=0', false);
    });
</script>
