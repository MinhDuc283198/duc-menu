<?php

require('../../../config.php');

$sortings = optional_param_array('sorting', '', PARAM_RAW);


foreach ($sortings as $key => $sorting) {
    if ($sorting != null) {
        $bannerdata = $DB ->get_record('dlp_banner',array('id' => $key));
        if($bannerdata){
            $bannerdata->sorting = $sorting;
            $DB->update_record('dlp_banner',$bannerdata);
        }
    }
}