<?php
require('../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '배너관리',
    'heading' => '배너관리',
    'subheading' => '',
    'menu' => 'banner',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);
include_once($CFG->dirroot.'/local/management/header.php');

$type = optional_param('type', '', PARAM_RAW);
$tid = optional_param('tid', '', PARAM_RAW);

$context = context_system::instance();
$filearea = 'banner_img';
$component = 'local_dlp';
$m_filearea = 'm_banner_img';

if ($tid) {
    $banner = $DB->get_record('dlp_banner', ['id' => $tid]);
}
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"> </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" id="banner_form" enctype="multipart/form-data" action="banner_submit.php" method="POST">
                        <input type="hidden" name="type" value="<?php echo $type; ?>" />
                        <input type="hidden" name="tid" value="<?php echo $tid; ?>" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="show_title" class="col-sm-2 control-label">타이틀</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="show_title" id="sub_title" value="<?php echo $banner->show_title; ?>" placeholder="제목을 입력해주세요">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="sub_title" class="col-sm-2 control-label">본문 내용1</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="sub_title" id="sub_title" value="<?php echo $banner->sub_title; ?>" placeholder="배너 상단에 노출될 문구를 입력 해 주세요.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">본문 내용2</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="title" id="title" placeholder="배너 중앙에 노출될 문구를 입력 해 주세요."><?php echo $banner->title; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="link" class="col-sm-2 control-label">링크생성</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="link" value="<?php echo $banner->link; ?>" id="link" placeholder="배너 링크 url을 입력 해 주세요.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="link" class="col-sm-2 control-label">링크텍스트</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control media" name="btn_text" value="<?php echo $banner->btn_text; ?>" id="btn_text" placeholder="사용자에게 노출될 텍스트를 입력해주세요.">
                                    <p>※ URL 입력시 앞에 http 를 꼭 입력해 주세요.<br>※ URL을 입력하지 않으시면 링크 텍스트가 있더라도 버튼이 생성되지 않습니다.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="banner_img" class="col-sm-2 control-label">PC용 이미지</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="banner_img" id="banner_img" >
                                    <?php
                                    if ($tid != 0) {
                                        if (!empty($banner->fname)) {
                                            $fs = get_file_storage();
                                            $files = $fs->get_area_files($context->id, $component, "banner_img", $tid, 'id');
                                            foreach ($files as $file) {
                                                $filename = $file->get_filename();
                                                $mimetype = $file->get_mimetype();
                                                if ($file->get_filesize() > 0) {
                                                    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/' . $component . '/' . $filearea . '/' . $banner->id . '/' . $banner->fname);
                                                }
                                            }
                                            ?> <a download href="<?php echo $path ?>" target="_blank"> 클릭시 이미지 미리보기</a><?php
                                        } else {
                                            ?>  <p>※ 등록된 이미지가 없습니다.</p><?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="m_banner_img" class="col-sm-2 control-label">모바일용 이미지</label> 
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="m_banner_img" id="m_banner_img" >
                                    <?php
                                    if ($tid != 0) {
                                        if (!empty($banner->m_fname)) {
                                            $fs = get_file_storage();
                                            $m_files = $fs->get_area_files($context->id, $component, "m_banner_img", $banner->id, 'id');
                                            foreach ($m_files as $m_file) {
                                                $m_filename = $m_file->get_filename();
                                                $m_mimetype = $m_file->get_mimetype();
                                                if ($m_file->get_filesize() > 0) {
                                                    $m_path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/' . $component . '/' . $m_filearea . '/' . $tid . '/' . $banner->m_fname);
                                                }
                                            }
                                            ?> <a download href="<?php echo $m_path; ?>" target="_blank"> 클릭시 이미지 미리보기</a><?php
                                        } else {
                                            ?>  <p>※ 등록된 이미지가 없습니다.</p><?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="use_f" class="col-sm-2 control-label">사용여부</label>
                                <div class="col-sm-10">
                                    <input type="radio" name="use_f" id="use_f_y" value="Y" <?php echo ($banner->use_f == 'Y') ? 'checked' : ''; ?> >
                                    <label for="use_f_y">사용</label>
                                    <input type="radio" name="use_f" id="use_f_n" value="N" <?php echo ($banner->use_f == 'N') ? 'checked' : ''; ?> >
                                    <label for="use_f_n">미사용</label>
                                    <p>※ 미사용을 선택하시면 메인 화면에 이미지가 노출 되지 않습니다.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="use_f" class="col-sm-2 control-label">팝업여부</label>
                                <div class="col-sm-10">
                                    <input type="radio" name="popyn" id="popy" value="1" <?php echo (($banner->popyn == 1) || !$banner ) ? 'checked' : ''; ?> >
                                    <label for="popy">사용</label>
                                    <input type="radio" name="popyn" id="popn" value="0" <?php echo ($banner->popyn == 0) && $banner ? 'checked' : ''; ?> >
                                    <label for="popn">미사용</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="button" class="btn btn-info pull-right banner_submit">저장</button>
                            <button type="button" class="btn btn-info pull-right" onclick="location.href='<?php echo $CFG->wwwroot?>/local/management/banner/index.php'" >목록</button>
                            <?php
                            if ($type == 'edit') {
                                ?>
                                <button type="button" class="btn btn-info pull-right banner_del" tid="<?php echo $tid; ?>">삭제</button>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->

<?php

include_once($CFG->dirroot.'/local/management/footer.php');
?>

<script>
    $('.banner_submit').click(function () {
        $('#banner_form').submit();
    });

    $('.banner_del').click(function () {
        var tid = $(this).attr('tid');
        location.href = 'banner_delete.php?tid=' + tid;
    });
</script>