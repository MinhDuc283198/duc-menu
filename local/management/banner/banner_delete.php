<?php
    require('../../../config.php');
    
    $tid = optional_param('tid', '', PARAM_RAW);
    
    $banner = $DB->get_record('dlp_banner', ['id' => $tid]);
    $a = $DB->delete_records('dlp_banner', ['id' => $tid]);
    
    /* 배너 삭제시 이미지 파일 삭제 */
    if($a) {
        $fs = get_file_storage();
        $context = context_system::instance();
        $component = 'local_dlp';

        $overlap_files = $fs->get_area_files($context->id, $component, "banner_img", $tid, 'id');
        foreach ($overlap_files as $file) {
            $file->delete();
        }
    }
    