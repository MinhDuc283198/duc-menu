<?php

require('../../../config.php');

$type = optional_param('type', '', PARAM_RAW);
$thistime = time();

$tid = optional_param('tid', '', PARAM_RAW);
$fname = $_FILES['banner_img']['name'];
$m_fname = $_FILES['m_banner_img']['name'];
$link = optional_param('link', '', PARAM_RAW);
$sub_title = optional_param('sub_title', '', PARAM_RAW);
$popyn = optional_param('popyn', '', PARAM_INT);
$title = optional_param('title', '', PARAM_RAW);
$btn_text = optional_param('btn_text', '', PARAM_RAW);
$show_title = optional_param('show_title', '', PARAM_RAW);
$use_f = optional_param('use_f', '', PARAM_RAW);

if ($type == 'add') {
    $banner_info = new stdClass();
} else {
    $banner_info = $DB->get_record('dlp_banner', ['id' => $tid]);
}
$banner_info->show_title = $show_title;
if (!empty($fname)) {
    $banner_info->fname = $fname;
}
if (!empty($m_fname)) {
    $banner_info->m_fname = $m_fname;
}
$banner_info->link = $link;
$banner_info->popyn = $popyn;
$banner_info->sub_title = $sub_title;
$banner_info->title = $title;
$banner_info->btn_text = $btn_text;
$banner_info->use_f = $use_f;
$banner_info->userid = $USER->id;
$banner_info->timecreated = $thistime;
$banner_info->timemodified = $thistime;

if ($type == 'add') {
    $bannerid = $DB->insert_record('dlp_banner', $banner_info);

    $fs = get_file_storage();
    $context = context_system::instance();
    $component = 'local_dlp';

    if (!empty($_FILES["banner_img"]['tmp_name'])) {

        $file_record = array(
            'contextid' => $context->id,
            'component' => $component,
            'filearea' => "banner_img",
            'itemid' => $bannerid,
            'filepath' => '/',
            'filename' => $_FILES["banner_img"]['name'],
            'timecreated' => $thistime,
            'timemodified' => $thistime,
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $fs->create_file_from_pathname($file_record, $_FILES["banner_img"]['tmp_name']);
    }
    if (!empty($_FILES["m_banner_img"]['tmp_name'])) {

        $file_record = array(
            'contextid' => $context->id,
            'component' => $component,
            'filearea' => "m_banner_img",
            'itemid' => $bannerid,
            'filepath' => '/',
            'filename' => $_FILES["m_banner_img"]['name'],
            'timecreated' => $thistime,
            'timemodified' => $thistime,
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $fs->create_file_from_pathname($file_record, $_FILES["m_banner_img"]['tmp_name']);
    }
} else if ($type == 'edit') {
    $DB->update_record('dlp_banner', $banner_info);

    $fs = get_file_storage();
    $context = context_system::instance();
    $component = 'local_dlp';

    /* 새로 올리는 파일이 있으면 해당 번호(위치의 파일을 삭제) */
    if (!($fname == null || $fname == '')) {
        $overlap_files = $fs->get_area_files($context->id, $component, "banner_img", $banner_info->id, 'id');
        foreach ($overlap_files as $file) {
            if ($file->get_filesize() > 0) {
                $filename = $file->get_filename();
                $file->delete();
            }
        }
    }
    if (!($m_fname == null || $m_fname == '')) {
        $overlap_files = $fs->get_area_files($context->id, $component, "m_banner_img", $banner_info->id, 'id');
        foreach ($overlap_files as $file) {
            if ($file->get_filesize() > 0) {
                $filename = $file->get_filename();
                $file->delete();
            }
        }
    }

    if (!empty($_FILES["m_banner_img"]['tmp_name'])) {

        $file_record = array(
            'contextid' => $context->id,
            'component' => $component,
            'filearea' => "m_banner_img",
            'itemid' => $banner_info->id,
            'filepath' => '/',
            'filename' => $_FILES["m_banner_img"]['name'],
            'timecreated' => $thistime,
            'timemodified' => $thistime,
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $fs->create_file_from_pathname($file_record, $_FILES["m_banner_img"]['tmp_name']);
    }
    if (!empty($_FILES["banner_img"]['tmp_name'])) {

        $file_record = array(
            'contextid' => $context->id,
            'component' => $component,
            'filearea' => "banner_img",
            'itemid' => $banner_info->id,
            'filepath' => '/',
            'filename' => $_FILES["banner_img"]['name'],
            'timecreated' => $thistime,
            'timemodified' => $thistime,
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $fs->create_file_from_pathname($file_record, $_FILES["banner_img"]['tmp_name']);
    }
}

redirect('index.php');
