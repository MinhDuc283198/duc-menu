<?php

namespace local_management;

defined('MOODLE_INTERNAL') || die();

/**
 * @property-read int $id
 * @property-read string $text
 * @property-read string $shortname
 * @property-read string $icon
 * @property-read string $description
 * @property-read string $href
 * @property-read string $target
 * @property-read string $title
 * @property-read int $lft
 * @property-read int $rgt
 * @property-read int $ismain
 * @property-read int $parentid
 * @property-read int $creator
 * @property-read int $timecreated
 * @property-read int $depth
 * @property-read array $children
 * @property-read object $parent
 * @property-read object $groupid
 * @property-read string $txt_vi
 * @property-read string $txt_ko
 * @property-read string $txt_en
 */
class menu {
    private $id;
    private $text;
    private $txt_vi;
    private $txt_en;
    private $txt_ko;
    private $shortname;
    private $icon = 'a-circle-o';
    private $description;
    private $href;
    private $target;
    private $title;
    private $lft;
    private $rgt;
    private $ismain;
    private $parentid;
    private $creator;
    private $timecreated;
    private $depth;
    private $children = array();

    private $active = false;
    private $parent = null;
    private $groupid;

    private static $standardproperties = array(
        'id' => false,
        'text' => false,
        'txt_vi' => false,
        'txt_en' => false,
        'txt_ko' => false,
        'shortname' => false,
        'icon' => false,
        'href' => false,
        'target' => false,
        'title' => false,
        'lft' => false,
        'rgt' => false,
        'ismain' => false,
        'parentid' => false,
        'creator' => false,
        'timecreated' => false,
        'depth' => false,
        'children' => false,
        'parent' => false,
        'groupid' => false
    );

    public function __construct($data) {
        foreach($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * Magic method getter
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        if (isset(self::$standardproperties[$name])) {
            if ($method = self::$standardproperties[$name]) {
                return $this->{$method}();
            } else {
                return $this->{$name};
            }
        } else {
            debugging('Invalid cm_info property accessed: '.$name);
            return null;
        }
    }

    public function __set( string $name , mixed $value ) {
        if (method_exists($this, 'set_'.$name)) {
            $method = 'set_'.$name;
            $this->{$method}($value);
        } else if (property_exists($this, $name)) {
            $this->{$name} = $value;
        } else {
            debugging('Invalid completion property accessed: '.$name);
        }
    }

    public function add_child($child) {
        $this->children[] = $child;
        $child->parent = $this;
    }

    public function set_active($active) {
        $this->active = $active;

        // 부모도 active로 설정
        if($this->parent != null) {
            $this->parent->set_active($active);
        }
    }

    public function is_active() {
        return $this->active;
    }

    public function has_children() {
        return count($this->children) > 0;
    }


    public function print_menu() {
        $cssclass = array();
        if($this->is_active()) {
            $cssclass[] = 'active';
        }

        if($this->has_children()) {
            $cssclass[] = 'treeview';
        }

        $cssclassstr = '';
        if(count($cssclass) > 0) {
            $cssclassstr = ' class="'. implode(' ', $cssclass).'"';
        }
        $lang = current_language();
        $textOut = 'None';
        if($lang == 'en') $textOut = $this->txt_en;
        else if($lang == 'vi') $textOut = $this->txt_vi;
        else $textOut = $this->txt_ko;
//         ($lang == 'en') ? $textOut = $this->txt_en : ($lang == 'vi') ? $textOut = $this->txt_vi : $textOut = $this->txt_ko;
        $html = '<li'.$cssclassstr.' id="menu_'.$this->shortname.'">
          <a href="'.local_management_make_absolute($this->href).'" target="'.$this->target.'" title="'.$this->title.'" groupid="'.$this->groupid.'">
            <i class="'.$this->icon.'"></i> <span>'.$textOut.'</span>';
        if($this->has_children()) {
            $html .='    <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">';
            foreach($this->children as $child) {
                $html .= $child->print_menu();
            }
            $html .= '</ul>';
        } else {
            $html .= '</a>';
        }
        $html .= '</li>';

        return $html;
    }

    function print_json() {
        $lang = current_language();
        $textOut = 'None';
        ($lang == 'en') ? $textOut = $this->txt_en : ($lang == 'vi') ? $textOut = $this->txt_vi : $textOut = $this->txt_ko;
        $json = '{"id":"'.$this->id.'","href":"'.htmlentities($this->href).'","icon":"'.$this->icon
            .'","text":"'.htmlentities($textOut).'", "shortname":"'.$this->shortname
            .'", "target": "'.$this->target.'", "title": "'.$this->title
            .'","parentid":"'.$this->parentid.'","description":"'.htmlentities($this->description)
            .'","ismain":"'.$this->ismain.'","groupid":"'.$this->groupid.'","txt_vi":"'.$this->txt_vi.'","txt_en":"'.$this->txt_en.'","txt_ko":"'.$this->txt_ko.'"';
        if($this->has_children()) {
            $jsonchildren = array();
            foreach($this->children as $child) {
                $jsonchildren[] = $child->print_json();
            }
            $json .= ',"children":['.implode(',', $jsonchildren).']';
        }
        $json .= '}';

        return $json;
    }
}
