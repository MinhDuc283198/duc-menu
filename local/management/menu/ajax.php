<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$action = required_param('action', PARAM_ALPHAEXT);

$context = context_system::instance();

require_login($context);
/*
$hasrole = local_management_has_role_menu('menu');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}
*/
switch ($action) {
    case 'load':
        $menus = local_management_menu_get_menus(1);
        $menus = local_management_menu_convert_class($menus);
        $json = array();
        foreach($menus as $menu) {
            $json[] = $menu->print_json();
        }
        echo '['.implode(',', $json).']';
        die();
        break;
    case 'add':
        $data = required_param_array('data', PARAM_RAW);
        $parentid = $data['parentid'];
        if(empty($parentid)) {
            $parentid = 1;
        }
        
        unset($data['id']);
        unset($data['parentid']);
        $menu = (object) $data;
        $menu->ismain = 0;
        $id = local_management_menu_add($menu, $parentid);
        
        echo '{"status":"success","id":"'.$id.'","parentid":"'.$parentid.'"}';
        die();
        break;
    case 'update':
        $data = required_param_array('data', PARAM_RAW);
        $menu = (object) $data;
        
        local_management_menu_update($menu);
        
        echo '{"status":"success"}';
        die();
        break;
    case 'move':
        $data = required_param_array('data', PARAM_RAW);
        if(!isset($data['parent'])) {
            $data['parent'] = 1;
        }
        
        $menu = $DB->get_record('management_menu', array('id' => $data['id']));
        
        if($menu->parentid != $data['parent']) {
            local_management_menu_move($menu->id, $data['parent']);
        }
        
        if(isset($data['prev'])) {
            local_management_menu_move_after_sibling($menu->id, $data['prev']);
        } else if(isset($data['next'])) {
            local_management_menu_move_before_sibiling($menu->id, $data['next']);
        }
        
        echo '{"status":"success","parentid":"'.$data['parent'].'"}';
        die();
        break;
    case 'delete':
        $data = required_param_array('data', PARAM_RAW);
        
        local_management_menu_delete($data['id']);
        
        echo '{"status":"success"}';
        die();
        break;
    case 'groups':
        $data = required_param_array('data', PARAM_RAW);
        $groups = local_management_menu_get_groups($data['id']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->groups = array_values($groups);
        echo json_encode($rvalue);
        die();
        break;
    case 'search_group':
        $data = required_param_array('data', PARAM_RAW);
        $groups = local_management_menu_search_group($data['id'], $data['search']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->groups = array_values($groups);
        echo json_encode($rvalue);
        die();
        break;
    case 'add_group':
        $data = required_param_array('data', PARAM_RAW);
        $id = local_management_menu_add_group($data['id'], $data['groupid']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->id = $id;
        echo json_encode($rvalue);
        die();
        break;
    case 'delete_group':
        $data = required_param_array('data', PARAM_RAW);
        local_management_menu_delete_group($data['id'], $data['groupid']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        die();
        break;
    case 'users':
        $data = required_param_array('data', PARAM_RAW);
        $users = local_management_menu_get_users($data['id']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->users = array_values($users);
        echo json_encode($rvalue);
        die();
        break;
    case 'search_user':
        $data = required_param_array('data', PARAM_RAW);
        $users = local_management_menu_search_user($data['id'], $data['search']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->users = array_values($users);
        echo json_encode($rvalue);
        die();
        break;
    case 'add_user':
        $data = required_param_array('data', PARAM_RAW);
        $id = local_management_menu_add_user($data['id'], $data['userid']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->id = $id;
        echo json_encode($rvalue);
        die();
        break;
    case 'delete_user':
        $data = required_param_array('data', PARAM_RAW);
        local_management_menu_delete_user($data['id'], $data['userid']);
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        die();
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();