function local_management_menu_call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}

function local_management_menu_add(data) {
    return local_management_menu_call_ajax('add', data, false);
}

function local_management_menu_update(data) {
    return local_management_menu_call_ajax('update', data, false);
}

function local_management_menu_move(data) {
    return local_management_menu_call_ajax('move', data, false);
}

function local_management_menu_delete(id) {
    return local_management_menu_call_ajax('delete', {id: id}, false);
}

function local_management_menu_load(editor) {
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {action: 'load'},
        success: function(data) {
            editor.setData(data);
        },
        error: function(xhr, status, error) {}
    });
}

function local_management_menu_groups(id) {
    var $ul = $('#groupEditor');
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {action: 'groups', data: {id : id}},
        success: function(data) {
            if(data.status == 'success') {
                $ul.empty();
                $.each(data.groups, function() {
                    var $li = local_management_menu_render_group(this);
                    $ul.append($li);
                });
            } else {
                alert(data.error);
            }
        },
        error: function(xhr, status, error) {}
    });
}

function local_management_menu_search_group(editor) {
    $itemEditing = editor.getCurrentItem();
    if($itemEditing == null) {
        return false;
    }

    var id = $itemEditing.data('id');
    var search = $('form#searchGroup input[name=search]').val();
    
    var result = local_management_menu_call_ajax('search_group', {id: id, search: search.trim()}, false);
    if(result.status == 'success') {
        $tbody = $('#modal-add-group #group-list table tbody');
        $tbody.empty();
        if(result.groups.length > 0) {
            $.each(result.groups, function() {
                $tr = $('<tr>');
                $tr.data(this);

                $icon = $('<i>').addClass('fa fa-group');
                $name = $('<span>').addClass('txt').text(this.name);
                $tdName = $('<td>').append($icon).append("&nbsp;").append($name)

                var $tdShortname =$('<td>').text(this.shortname);

                var $tdButtons =$('<td>');;
                var $divbtn = $('<div>').addClass('btn-group pull-right');
                var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                $divbtn.append($btnAdd);
                $($btnAdd).on('click', function (e) {
                    e.preventDefault();
                    var $tr = $(this).closest('tr');
                    local_management_menu_add_group(editor, $tr.data());
                    $('#modal-add-group').modal('hide');
                });
                $tdButtons.append($divbtn);

                $tr.append($tdName).append($tdShortname).append($tdButtons);
                $tbody.append($tr);
            });
        } else {
            $tbody.append('<tr><td colspan="3">데이터가 없습니다</td></tr>');
        }
    } else {
        alert(result.error);
    }
}

function local_management_menu_add_group(editor, group) {
    $itemEditing = editor.getCurrentItem();
    if($itemEditing == null) {
        return false;
    }

    var menuid = $itemEditing.data('id');
    var groupid = group.id;
    
    var result = local_management_menu_call_ajax('add_group', {id: menuid, groupid: groupid}, false);
    if(result.status == 'success') {
        var $ul = $('#groupEditor');
        group.menuid = menuid;
        $li = local_management_menu_render_group(group);
        $ul.append($li);
    } else {
        alert(result.error);
    }
}

function local_management_menu_delete_group($item) {
    if(confirm("그룹을 삭제하시겠습니까?")) {
        var result = local_management_menu_call_ajax('delete_group', {id: $item.data('menuid'), groupid: $item.data('id')}, false);
        if(result.status == 'success') {
            $item.remove();
        } else {
            alert(result.error);
        }
    }
}

function local_management_menu_render_group(group) {
    var $divbtn = $('<div>').addClass('btn-group pull-right');
    var $btnRemv = $("<a>").addClass('btn btn-danger btn-sm btnRemoveGroup clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
    $divbtn.append($btnRemv);
    $($btnRemv).on('click', function (e) {
        e.preventDefault();
        var $li = $(this).closest('li');
        local_management_menu_delete_group($li);
    });
    var textItem = $('<span>').addClass('txt').text(group.name);
    var iconItem = $('<i>').addClass('fa fa-group');
    var div = $('<div>').css({"overflow": "auto"}).append(iconItem).append("&nbsp;").append(textItem).append($divbtn);
    var $li = $("<li>").data(group);
    $li.addClass('list-group-item pr-0').append(div);
    return $li;
}


function local_management_menu_users(id) {
    var $ul = $('#userEditor');
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        data: {action: 'users', data: {id : id}},
        success: function(data) {
            if(data.status == 'success') {
                $ul.empty();
                $.each(data.users, function() {
                    var $li = local_management_menu_render_user(this);
                    $ul.append($li);
                });
            } else {
                alert(data.error);
            }
        },
        error: function(xhr, status, error) {}
    });
}

function local_management_menu_reset() {
    $('#btnAddGroup').attr('disabled', true);
    $('#btnAddUser').attr('disabled', true);
    
    $('#groupEditor').empty();
    $('#userEditor').empty();
    
    $('small#menu-name').text('');
}

function local_management_valide_data(editor) {
    var form = editor.getForm();
    var data = {};
    
    form.find('input[type=hidden]').each(function(){
        if($(this).attr('name') != undefined) {
            data[$(this).attr('name')] = $(this).val().trim();
        }
    });
    
    form.find('.item-menu').each(function(){
        if($(this).attr('name') != undefined) {
            data[$(this).attr('name')] = $(this).val().trim();
        }
    });
    
    if(data.icon.length == 0) {
        alert("아이콘을 선택하세요.");
        return null;
    }
    if(data.text.length == 0) {
        alert("Text를 입력하세요.");
        return null;
    }
    if(data.shortname.length == 0) {
        alert("Identification을 입력하세요.");
        return null;
    }
    if(data.href.length == 0) {
        alert("URL를 입력하세요.");
        return null;
    }
    
    return data;
}


function local_management_menu_search_user(editor) {
    $itemEditing = editor.getCurrentItem();
    if($itemEditing == null) {
        return false;
    }

    var id = $itemEditing.data('id');
    var search = $('form#searchUser input[name=search]').val();
    
    if(search.trim().length == 0) {
        alert("검색어를 입력하세요");
        return false;
    }

    var result = local_management_menu_call_ajax('search_user', {id: id, search: search}, false);
    if(result.status == 'success') {
        $tbody = $('#modal-add-user #user-list table tbody');
        $tbody.empty();
        if(result.users.length > 0) {
            $.each(result.users, function() {
                $tr = $('<tr>');
                $tr.data(this);

                var $tdFullName =$('<td>');
                var $picture = $('<img>').attr('src', this.picture).addClass('user-image');
                var $fullname = $('<span>').addClass('txt').text(this.fullname);
                $tdFullName.append($picture).append("&nbsp;").append($fullname);

                var $tdEmail =$('<td>').text(this.email);
                var $tdCompany =$('<td>').text(this.company);

                var $tdButtons =$('<td>');;
                var $divbtn = $('<div>').addClass('btn-group pull-right');
                var $btnAdd = $("<a>").addClass('btn btn-danger btn-sm clickable').attr("href", "#").html('<i class="fa fa-plus clickable"></i>');
                $divbtn.append($btnAdd);
                $($btnAdd).on('click', function (e) {
                    e.preventDefault();
                    var $tr = $(this).closest('tr');
                    local_management_menu_add_user(editor, $tr.data());
                    $('#modal-add-user').modal('hide');
                });
                $tdButtons.append($divbtn);

                $tr.append($tdFullName).append($tdEmail).append($tdCompany).append($tdButtons);
                $tbody.append($tr);
            });
        } else {
            $tbody.append('<tr><td colspan="3">데이터가 없습니다</td></tr>');
        }
    } else {
        alert(result.error);
    }
}

function local_management_menu_add_user(editor, user) {
    $itemEditing = editor.getCurrentItem();
    if($itemEditing == null) {
        return false;
    }

    var menuid = $itemEditing.data('id');
    var userid = user.id;
    
    var result = local_management_menu_call_ajax('add_user', {id: menuid, userid: userid}, false);
    if(result.status == 'success') {
        user.menuid = menuid;
        $li = local_management_menu_render_user(user);
        var $ul = $('#userEditor');
        $ul.append($li);
    } else {
        alert(result.error);
    }
}

function local_management_menu_remove_user($item) {
    if(confirm("사용자를 삭제하시겠습니까?")) {
        var result = local_management_menu_call_ajax('delete_user', {id: $item.data('menuid'), userid: $item.data('id')}, false);
        if(result.status == 'success') {
            $item.remove();
        } else {
            alert(result.error);
        }
    }
}

function local_management_menu_show_user_info($item) {
    $('#modal-user-info .modal-body').find('.profile-user-img').attr('src', $item.data('picture'));
    $('#modal-user-info .modal-body').find('.profile-username').text($item.data('fullname'));
    $('#modal-user-info .modal-body').find('#email').text($item.data('email'));
    $('#modal-user-info .modal-body').find('#company').text($item.data('company'));
    $('#modal-user-info .modal-body').find('#position').text($item.data('position'));
}

function local_management_menu_render_user(user) {
    var pictureItem = $('<img>').attr('src', user.picture).addClass('user-image');
    var textItem = $('<span>').addClass('txt').text(user.fullname);
    var btnGroup = $('<div>').addClass('btn-group pull-right');
    var $btnInfo = $("<a>").addClass('btn btn-primary btn-sm btnInfo clickable').attr("href", "#").html('<i class="fa fa-info-circle clickable"></i>');
    $btnInfo.attr('data-toggle', 'modal').attr('data-target', '#modal-user-info');
    btnGroup.append($btnInfo);
    $($btnInfo).on('click', function (e) {
        e.preventDefault();
        var $li = $(this).closest('li');
        local_management_menu_show_user_info($li);
    });

    var $btnRemove = $("<a>").addClass('btn btn-danger btn-sm btnRemoveUser clickable').attr("href", "#").html('<i class="fa fa-trash clickable"></i>');
    btnGroup.append($btnRemove);
    $($btnRemove).on('click', function (e) {
        e.preventDefault();
        var $li = $(this).closest('li');
        local_management_menu_remove_user($li);
    });

    var div = $('<div>').css({"overflow": "auto"}).append(pictureItem).append("&nbsp;").append(textItem).append(btnGroup);
    var $li = $("<li>").data(user);
    $li.addClass('list-group-item pr-0').append(div);

    return $li;
}


$(document).ready(function () {
    // icon picker options
    var iconPickerOptions = {searchText: "search...", labelHeader: "{0}/{1}"};
    // sortable list options
    var sortableListOptions = {
        placeholderCss: {'background-color': "#cccccc"},
        onChange: function(el) {
            var data = {};
            
            data['id'] = el.data().id;
            
            var prevEl = el.prev();
            if(prevEl.length > 0) {
                data['prev'] = prevEl.data().id;
            }
            
            var nextEl = el.next();
            if(nextEl.length > 0) {
                data['next'] = nextEl.data().id;
            }
            
            var prentEl = el.parent().closest("li");
            if(prentEl.length > 0) {
                data['parent'] = prentEl.data().id;
            }
            
            var result = local_management_menu_move(data);
            if(result && result.status == 'success') {
                el.data().parentid = result.parentid;
            } else {
                alert(result.error);
            }
            
            return true;
        },
        onDelete: function(id) {
            return true;
        },
        beforeDelete: function(el) {
            if(confirm("메뉴를 삭제하시겠습니까?")) {
                var id = el.data().id;

                var result = local_management_menu_delete(id);
                if(result && result.status == 'success') {
                    return true;
                } else {
                    alert(result.error);
                }
            }
            
            return false;
        },
        onEdit: function(el) {
            $('small#menu-name').text(el.data('text'));
            $('#btnAddGroup').removeAttr('disabled');
            $('#btnAddUser').removeAttr('disabled');
            
            local_management_menu_groups(el.data('id'));
            local_management_menu_users(el.data('id'));
        }
    };

    var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions});
    editor.setForm($('#frmEdit'));
    editor.setUpdateButton($('#btnUpdate'));

    $("#btnUpdate").click(function(){
        var data = local_management_valide_data(editor);
        if(data != null) {
            var result = local_management_menu_update(data);
            if(result && result.status == 'success') {
                editor.update();
                
                local_management_menu_reset();
            } else {
                alert(result.error);
            }
        }
    });

    $('#btnAdd').click(function(){
        var data = local_management_valide_data(editor);
        if(data != null) {
            var result = local_management_menu_add(data);
            if(result && result.status == 'success') {
                var nEl = editor.add();

                nEl.data().id = result.id;
                nEl.data().parentid = result.parentid;
                
                local_management_menu_reset();
            } else {
                alert(result.error);
            }
        }
    });
    
    $('#btnSearchGroup').on('click', function() {
        local_management_menu_search_group(editor);
    });
    $("#searchGroup input[type=text][name=search]").keypress(function(e) {
        if (e.keyCode == 13){
            e.preventDefault();
            local_management_menu_search_user(editor);
        }
    });
    
    $('#btnSearchUser').on('click', function() {
        local_management_menu_search_user(editor);
    });
    $("#searchUser input[type=text][name=search]").keypress(function(e) {
        if (e.keyCode == 13){
            e.preventDefault();
            local_management_menu_search_user(editor);
        }
    });
    
    local_management_menu_load(editor);
    
    $('#modal-add-group').on('show.bs.modal', function (event) {
        $('#modal-add-group input[name=search]').val('');
        $tbody = $('#modal-add-group #group-list table tbody');
        $tbody.empty();
        $tbody.append('<tr><td colspan="3">데이터가 없습니다</td></tr>');
    });
    $('#modal-add-user').on('show.bs.modal', function (event) {
        $('#modal-add-user input[name=search]').val('');
        $tbody = $('#modal-add-user #user-list table tbody');
        $tbody.empty();
        $tbody.append('<tr><td colspan="4">데이터가 없습니다</td></tr>');
    });
});