<?php

require_once($CFG->dirroot . '/local/management/menu/classes/menu.class.php');

/**
 * 사용자가 접근 가능한 메뉴 목록을 반환한다.
 *
 * @global object $DB
 * @global object $USER
 * @param int $userid
 * @return array
 */
function local_management_menu_get_user_menus($userid = 0) {
    global $CFG, $DB, $USER;

    if(!$userid) {
        $userid = $USER->id;
    }

    require_once ($CFG->dirroot . '/local/management/role/lib.php');

    // 시스템 관리 그룹에 속해 있으면 전체 메뉴를 반환한다.
    //if(local_management_role_is_group_member(LOCAL_MANAGEMENT_GROUP_SYSADMIN, $userid)) {
    return local_management_menu_get_menus(1);
    //}

    // 사용자가 속한 그룹과 사용자에게 지정된 메뉴의 최하위 메뉴 목록을 구한다.
    $leafids = $DB->get_fieldset_sql("SELECT m.id
        FROM {management_menu} m
        JOIN {management_menu} p ON m.lft BETWEEN p.lft AND p.rgt
        WHERE p.id IN (SELECT mu.menuid
                       FROM {management_menu_user} mu 
                       WHERE mu.userid = :userid1
                       UNION
                       SELECT mg.menuid
                       FROM {management_menu_group} mg
                       JOIN {management_group_member} gm ON gm.groupid = mg.groupid
                       WHERE gm.userid = :userid2)
          AND m.rgt = m.lft + 1", array('userid1' => $userid, 'userid2' => $userid));
    print_object($userid);
    die;
    if(empty($leafids)) {
        return null;
    }

    // 상위 메뉴까지 경로를 포함한 메뉴 목록을 구한다.
    $params = array();

    $incondition = '';
    $inparams = array();
    list($incondition, $inparams) = $DB->get_in_or_equal($leafids, SQL_PARAMS_NAMED, 'id_', TRUE);

    $params = array_merge($params, $inparams);

    $sql = "SELECT DISTINCT p.*
            FROM m_management_menu m
            JOIN m_management_menu p ON m.lft BETWEEN p.lft AND p.rgt
            WHERE p.parentid != 0";

    if($incondition) {
        $sql .= " AND m.id $incondition";
    }

    $sql .= " ORDER BY p.lft";

    return $DB->get_records_sql($sql, $params);
}

/**
 * 전체 메뉴를 반환한다.
 * $parentid를 지정하는 경우 해당 메뉴의 전체 하위 메뉴를 반환한다.
 *
 * 기본적으로 최상위 메뉴는 제외 시킨다.
 * 포함 시키려면 $withtop 값을 true로 지정한다.
 *
 * @global object $DB
 * @param int $parentid
 * @param boolean $withtop 최상위 메뉴($parentid가 지정된 경우 해당 메뉴)를 포함할지 여부
 * @return array|false
 */
function local_management_menu_get_menus($parentid = 0, $withtop = FALSE) {
    global $DB,$USER;
    $conditions = array();
    $params = array();
    $sql_get_type_group="SELECT *
    FROM {management_group_member}
    WHERE userid=$USER->id";
    $userid=$USER->id;
    $get_type_group=$DB->get_record_sql($sql_get_type_group);
    if($parentid) {
        $parent = $DB->get_record('management_menu', array('id' => $parentid));

        $conditions[] = 'm.lft BETWEEN :lft AND :rgt';
        $params['lft'] = $parent->lft;
        $params['rgt'] = $parent->rgt;
    }
    if($get_type_group->groupid!="1") {
        $sql_join_mgm="JOIN  {management_group_member} mgm ON mgm.groupid=m.groupid AND mgm.userid=$userid ";
        $sql_group = "";
        $sql_select_mgm=',mgm.userid,mgm.groupid';
    }
    $where = "";
    if(count($conditions) > 0) {
        $where = "WHERE " . implode(' AND ', $conditions);
    }

    $menus = $DB->get_records_sql("SELECT m.* $sql_select_mgm
            , (COUNT(p.id) - 1) AS depth
        FROM {management_menu} m
        JOIN {management_menu} p ON m.lft BETWEEN p.lft AND p.rgt
        $sql_join_mgm
        $where 
        $sql_group
        GROUP BY m.id, m.text, m.shortname, m.icon, m.description, m.href, m.target, m.title, m.ismain, m.timecreated, m.creator, m.lft, m.rgt, m.parentid, m.groupid
        ORDER BY m.lft", $params);

    if($menus && !$withtop) {
        unset($menus[$parentid]);
    }
    if($get_type_group->groupid!="1"){
        foreach($menus as $menu) {
            $chil_menus=$DB->get_records_sql("SELECT m.*
                                            FROM {management_menu} m
                                            WHERE m.parentid=$menu->id
"                                           );
            foreach($chil_menus as $chil_menu){
                array_push($menus, $chil_menu);
            }

        }
    }


    return $menus;
}


/**
 * DB에서 select 한 object를 \local_management\menu 클래스로 변환한다.
 *
 * @param array $menus
 * @return array \local_management\menu 클래스 배열
 */
function local_management_menu_convert_class($menus) {
    $rvalue = array();

    $classes = array();
    foreach($menus as $menu) {
        $class = new \local_management\menu($menu);
        if(isset($classes[$class->parentid])) {
            $classes[$class->parentid]->add_child($class);
        } else {
            $rvalue[] = $class;
        }
        $classes[$class->id] = $class;
    }

    return $rvalue;
}

/**
 * 메뉴를 선택된 것으로 설정한다.
 * 메뉴 목록에 선택된 메뉴가 없는 경우 false를 반환한다.
 *
 * @param array $menus \local_management\menu 객체 배열
 * @param string $shortname
 * @return object|false
 */
function local_management_menu_set_active($menus, $shortname) {
    foreach($menus as $menu) {
        if($menu->shortname == $shortname) {
            $menu->set_active(true);
            return $menu;
        }
        if($menu->has_children()) {
            $actived = local_management_menu_set_active($menu->children, $shortname);
            if($actived) {
                return $actived;
            }
        }
    }

    return false;
}

/**
 * 바로 하위 메뉴 목록을 반환한다.
 *
 * 전체 하위 메뉴(하위 메뉴의 하위 메뉴까지)를 포함시키려면
 * local_management_menu_get_menus 함수를 이용한다.
 *
 * @global object $DB
 * @param int $parentid
 * @return array|false
 */
function local_management_menu_get_children($parentid) {
    global $DB;

    $parent = $DB->get_record('management_menu', array('id' => $parentid));

    $children  = $DB->get_records_sql("SELECT DISTINCT m.*
            , COUNT(p.id) AS depth
        FROM {management_menu} as m
        JOIN {management_menu} as p ON p.lft < m.lft AND p.rgt > m.rgt
        GROUP BY m.id, m.text, m.shortname, m.icon, m.description, m.href, m.target, m.title, m.ismain, m.creator, m.parentid, m.timecreated, m.lft, m.rgt
        HAVING max(p.lft) = :lft", array('lft' => $parent->lft));

    return $children;
}

/**
 * 이전 형제 메뉴를 반환한다.
 * 없을 경우 false 반환한다.
 *
 * @global object $DB
 * @param int $id
 * @return object|false
 */
function local_management_menu_get_previous_sibling($id) {
    global $DB;

    return $DB->get_record_sql("SELECT *
        FROM {management_menu} 
        WHERE rgt = (SELECT lft - 1 
            FROM {management_menu} WHERE id = :id)", array('id' => $id));
}

/**
 * 다음 형제 메뉴를 반환한다.
 * 없으면 false를 반환한다.
 *
 * @global object $DB
 * @param int $id
 * @return object|false
 */
function local_management_menu_get_next_sibling($id) {
    global $DB;

    return $DB->get_record_sql("SELECT *
        FROM {management_menu} 
        WHERE lft = (SELECT rgt + 1 
            FROM {management_menu} WHERE id = :id)", array('id' => $id));
}

/**
 * 부모 메뉴를 반환한다.
 *
 * @global object $DB
 * @param int $id
 * @return object|boolean
 */
function local_management_menu_get_parent($id) {
    global $DB;

    $parents = $DB->get_records_sql("SELECT p.*
        FROM {management_menu} m, {management_menu} p
        WHERE p.lft < m.lft
          AND p.rgt > m.rgt
          AND m.id = 4
        ORDER BY rgt ASC", array('id' => $id), 0, 1);

    if($parents) {
        return array_shift($parents);
    } else {
        return false;
    }
}

/**
 * 최상위에서 메뉴까지 경로를 반환한다.
 *
 * @global object $DB
 * @param int $menuid
 * @param boolean $withtop
 * @return array|false
 */
function local_management_menu_get_path($menuid, $withtop = FALSE) {
    global $DB;

    $path = $DB->get_records_sql("SELECT p.*
            , CASE WHEN (p.rgt - p.lft) = 1 THEN 1 ELSE 0 END AS isleaf
        FROM {management_menu} AS m, {management_menu} AS p
        WHERE m.lft BETWEEN p.lft AND p.rgt
          AND m.id = :menuid
        ORDER BY p.lft", array('menuid' => $menuid));

    if($path && !$withtop) {
        unset($path[1]);
    }

    return $path;
}

/**
 * 메뉴에 사용할 수 있는 아이콘을 배열로 반환한다.
 *
 * @global object $CFG
 * @return array
 */
function local_management_menu_get_icons() {
    global $CFG;

    $file = $CFG->dirroot.'/local/management/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css';
    $regexp = '/\.(fa\-[a-z\-0-9]+)\:before/';

    $keys = array();
    preg_match_all($regexp, file_get_contents($file), $keys, PREG_PATTERN_ORDER);

    $icons = array();
    foreach($keys[0] as $key) {
        $matches = array();
        preg_match($regexp, $key, $matches);
        $icons[] = $matches[1];
    }
    $icons = array_unique($icons);
    asort($icons);

    return $icons;
}

/**
 * 메뉴를 추가한다.
 *
 * @global object $DB
 * @global object $USER
 * @param object $menu
 * @param int $parentid
 * @return int
 */
function local_management_menu_add($menu, $parentid) {
    global $DB, $USER;

    // shortname이 중복되는지 체크
    if($DB->record_exists('management_menu', array('shortname' => $menu->shortname))) {
        print_error('duplicatedshortname', 'local_management');
    }

    $parent = $DB->get_record('management_menu', array('id' => $parentid));

    $sql = "UPDATE {management_menu} SET lft = CASE WHEN lft > $parent->rgt THEN lft + 2 ELSE lft END,
            rgt = CASE WHEN rgt >= $parent->rgt THEN rgt + 2 ELSE rgt END
    WHERE rgt >= $parent->rgt";

    $DB->execute($sql);

    $menu->lft = $parent->rgt;
    $menu->rgt = $parent->rgt + 1;
    $menu->parentid = $parentid;
    $menu->creator = $USER->id;
    $menu->timecreated = time();

    return $DB->insert_record('management_menu', $menu);
}

function local_management_menu_update($menu) {
    global $DB, $USER;

    $menuold = $DB->get_record('management_menu', array('id' => $menu->id));


    // shortname을 변경한 경우 중복되는지 체크
    if($menu->shortname != $menuold->shortname) {
        if($DB->record_exists('management_menu', array('shortname' => $menu->shortname))) {
            print_error('duplicatedshortname', 'local_management');
        }
    }

    return $DB->update_record('management_menu', $menu);
}

/**
 * 메뉴를 삭제한다.
 * 하위 메뉴가 있는 경우 하위 메뉴까지 삭제된다.
 *
 * @global object $DB
 * @param int $menuid
 */
function local_management_menu_delete($menuid) {
    global $DB;

    $menu = $DB->get_record('management_menu', array('id' => $menuid));

    // 햐위 메뉴 중에 ismain = 1 인 것이 있는지 체크
    $hasmain = $DB->record_exists_select('management_menu', 'lft BETWEEN :lft AND :rgt AND ismain = :ismain'
        , array('lft' => $menu->lft, 'rgt' => $menu->rgt, 'ismain' => 1));
    if($hasmain) {
        print_error('cannotdeletemainmenu', 'local_management');
    }

    if($menu) {
        if($menu->ismain == 1) {
            print_error('cannotdeletemainmenu', 'local_management');
        }

        $DB->delete_records_select('management_menu', 'lft >= :lft AND lft <= :rgt', array('lft' => $menu->lft, 'rgt' => $menu->rgt));
        $DB->execute("UPDATE {management_menu} SET lft = CASE WHEN lft > $menu->lft THEN lft - ($menu->rgt - $menu->lft + 1) ELSE lft END,
                                    rgt = CASE WHEN rgt > $menu->lft THEN rgt - ($menu->rgt - $menu->lft + 1) ELSE rgt END
        WHERE (lft > $menu->lft OR rgt > $menu->lft)");

        // 권한 맵핑 테이블 삭제
        $DB->delete_records('management_menu_group', array('menuid' => $menuid));
        $DB->delete_records('management_menu_user', array('menuid' => $menuid));
    }
}

/**
 * 메뉴의 위치를 서로 바꾼다.
 *
 * @global object $DB
 * @param int $menuid1
 * @param int $menuid2
 */
function local_management_menu_swap($menuid1, $menuid2) {
    global $DB;

    $menu1 = $DB->get_record('management_menu', array('id' => $menuid1));
    $menu2 = $DB->get_record('management_menu', array('id' => $menuid2));
    $DB->execute("UPDATE {management_menu}
           SET lft = CASE WHEN lft BETWEEN $menu1->lft AND $menu1->rgt
                           THEN $menu2->rgt + lft - $menu1->rgt
                           WHEN lft BETWEEN $menu2->lft AND $menu2->rgt
                           THEN $menu1->lft + lft - $menu2->lft
                           ELSE $menu1->lft + $menu2->rgt + lft - $menu1->rgt - $menu2->lft END,
               rgt = CASE WHEN rgt BETWEEN $menu1->lft AND $menu1->rgt
                           THEN $menu2->rgt + rgt - $menu1->rgt
                           WHEN rgt BETWEEN $menu2->lft AND $menu2->rgt
                           THEN $menu1->lft + rgt - $menu2->lft
                           ELSE $menu1->lft + $menu2->rgt + rgt - $menu1->rgt - $menu2->lft END
        WHERE lft BETWEEN $menu1->lft AND $menu2->rgt
          AND $menu1->lft < $menu1->rgt
          AND $menu1->rgt < $menu2->lft
          AND $menu2->lft < $menu2->rgt");

}

/**
 * 메뉴의 상위 메뉴를 바꾼다.
 *
 * @global object $DB
 * @param int $menuid
 * @param int $newparentid
 */
function local_management_menu_move($menuid, $newparentid) {
    global $DB;

    $menu = $DB->get_record('management_menu', array('id' => $menuid));
    $newparent = $DB->get_record('management_menu', array('id' => $newparentid));

    $DB->execute("UPDATE {management_menu}
           SET lft = lft + CASE WHEN $newparent->rgt < $menu->lft
                                THEN CASE WHEN lft BETWEEN $menu->lft AND $menu->rgt
                                          THEN $newparent->rgt - $menu->lft
                                          WHEN lft BETWEEN $newparent->rgt AND $menu->lft - 1
                                          THEN $menu->rgt - $menu->lft + 1
                                          ELSE 0 END
                                WHEN $newparent->rgt > $menu->rgt
                                THEN CASE WHEN lft BETWEEN $menu->lft AND $menu->rgt
                                          THEN $newparent->rgt - $menu->rgt - 1
                                          WHEN lft BETWEEN $menu->rgt + 1 AND $newparent->rgt - 1
                                          THEN $menu->lft - $menu->rgt - 1
                                          ELSE 0 END
                                ELSE 0 END,
               rgt = rgt + CASE WHEN $newparent->rgt < $menu->lft
                                THEN CASE WHEN rgt BETWEEN $menu->lft AND $menu->rgt
                                          THEN $newparent->rgt - $menu->lft
                                          WHEN rgt BETWEEN $newparent->rgt AND $menu->lft - 1
                                          THEN $menu->rgt - $menu->lft + 1
                                          ELSE 0 END
                                WHEN $newparent->rgt > $menu->rgt
                                THEN CASE WHEN rgt BETWEEN $menu->lft AND $menu->rgt
                                          THEN $newparent->rgt - $menu->rgt - 1
                                          WHEN rgt BETWEEN $menu->rgt + 1 AND $newparent->rgt - 1
                                          THEN $menu->lft - $menu->rgt - 1
                                          ELSE 0 END
                                ELSE 0 END");

    $DB->set_field('management_menu', 'parentid', $newparentid, array('id' => $menuid));
}

/**
 * 메뉴를 형제노드(siblingid) 뒤로 이동한다.
 *
 * @global object $DB
 * @param int $menuid
 * @param int $siblingid
 */
function local_management_menu_move_after_sibling($menuid, $siblingid) {
    global $DB;

    $menu = $DB->get_record('management_menu', array('id' => $menuid));
    $parent = $DB->get_record('management_menu', array('id' => $menu->parentid));
    $sibling = $DB->get_record('management_menu', array('id' => $siblingid));

    $DB->execute("UPDATE {management_menu}
        SET
            lft =
                CASE
                    WHEN $sibling->id = $parent->id THEN
                        CASE
                            WHEN lft BETWEEN $parent->lft + 1 AND $menu->lft - 1 THEN
                                lft + ($menu->rgt - $menu->lft) + 1
                            WHEN lft BETWEEN $menu->lft AND $menu->rgt THEN
                                lft - ($menu->lft - ($parent->lft + 1))
                            ELSE
                                lft
                        END
                    ELSE
                        CASE
                            WHEN $menu->lft > $sibling->lft THEN
                                CASE
                                    WHEN lft BETWEEN $sibling->rgt AND $menu->lft - 1 THEN
                                        lft + ($menu->rgt - $menu->lft) + 1
                                    WHEN lft BETWEEN $menu->lft AND $menu->rgt THEN
                                        lft - ($menu->lft - ($sibling->rgt + 1))
                                    ELSE
                                        lft
                                END
                            ELSE
                                CASE
                                    WHEN lft BETWEEN $menu->rgt + 1 AND $sibling->rgt THEN
                                        lft - (($menu->rgt - $menu->lft) + 1)
                                    WHEN lft BETWEEN $menu->lft AND $menu->rgt THEN
                                        lft + ($sibling->rgt - $menu->rgt)
                                    ELSE
                                        lft
                                END
                        END
                END,
            rgt =
                CASE
                    WHEN $sibling->id = $parent->id THEN
                        CASE
                            WHEN rgt BETWEEN $parent->lft + 1 AND $menu->lft - 1 THEN
                                rgt + ($menu->rgt - $menu->lft) + 1
                            WHEN rgt BETWEEN $menu->lft AND $menu->rgt THEN
                                rgt - ($menu->lft - ($parent->lft + 1))
                            ELSE
                                rgt
                        END
                    ELSE
                        CASE
                            WHEN $menu->rgt > $sibling->lft THEN
                                CASE
                                    WHEN rgt BETWEEN $sibling->rgt + 1 AND $menu->lft - 1 THEN
                                        rgt + ($menu->rgt - $menu->lft) + 1
                                    WHEN rgt BETWEEN $menu->lft AND $menu->rgt THEN
                                        rgt - ($menu->lft - ($sibling->rgt + 1))
                                    ELSE
                                        rgt
                                END
                            ELSE
                                CASE
                                    WHEN rgt BETWEEN $menu->rgt+1 AND $sibling->rgt + 1 THEN
                                        rgt - (($menu->rgt - $menu->lft) + 1)
                                    WHEN rgt BETWEEN $menu->lft AND $menu->rgt THEN
                                        rgt + ($sibling->rgt - $menu->rgt)
                                    ELSE
                                        rgt
                                END
                        END
                END
        WHERE lft BETWEEN $parent->lft + 1 AND $parent->rgt");
}

/**
 * 메뉴를 형제노드(siblingid) 앞으로 이동한다.
 *
 * @global object $DB
 * @param int $menuid
 * @param int $siblingid
 */
function local_management_menu_move_before_sibiling($menuid, $siblingid) {
    global $DB;

    $menu = $DB->get_record('management_menu', array('id' => $menuid));
    $parent = $DB->get_record('management_menu', array('id' => $menu->parentid));
    $sibling = $DB->get_record('management_menu', array('id' => $siblingid));

    $DB->execute("UPDATE {management_menu}
        SET
            lft =
                CASE
                    WHEN $sibling->id = $parent->id THEN
                        CASE
                            WHEN lft BETWEEN $menu->rgt + 1 AND $parent->rgt - 1 THEN
                                lft - ($menu->rgt - $menu->lft) - 1
                            WHEN lft BETWEEN $menu->lft AND $menu->rgt THEN
                                lft + ($parent->rgt - ($menu->rgt + 1))
                            ELSE
                                lft
                        END
                    ELSE
                        CASE
                            WHEN $menu->lft > $sibling->lft THEN
                                CASE
                                    WHEN lft BETWEEN $sibling->lft AND $menu->lft - 1 THEN
                                        lft + (($menu->rgt - $menu->lft) + 1)
                                    WHEN lft BETWEEN $menu->lft AND $menu->rgt THEN
                                        lft - ($menu->lft - $sibling->lft)
                                    ELSE
                                        lft
                                END
                            ELSE
                                CASE
                                    WHEN lft BETWEEN $menu->rgt + 1 AND $sibling->lft - 1 THEN
                                        lft - (($menu->rgt - $menu->lft) + 1)
                                    WHEN lft BETWEEN $menu->lft AND $menu->rgt THEN
                                        lft + ($sibling->lft - ($menu->rgt + 1))
                                    ELSE
                                        lft
                                END
                        END
                END,
            rgt =
                CASE
                    WHEN $sibling->id = $parent->id THEN
                        CASE
                            WHEN rgt BETWEEN $menu->rgt + 1 AND $parent->rgt - 1 THEN
                                rgt - (($menu->rgt - $menu->lft) + 1)
                            WHEN rgt BETWEEN $menu->lft AND $menu->rgt THEN
                                rgt - ($parent->rgt - ($menu->rgt + 1))
                            ELSE
                                rgt
                        END
                    ELSE
                        CASE
                            WHEN $menu->rgt > $sibling->lft THEN
                                CASE
                                    WHEN rgt BETWEEN $sibling->lft AND $menu->lft THEN
                                        rgt + ($menu->rgt - $menu->lft) + 1
                                    WHEN rgt BETWEEN $menu->lft AND $menu->rgt THEN
                                        rgt - ($menu->lft - $sibling->lft)
                                    ELSE
                                        rgt
                                END
                            ELSE
                                CASE
                                    WHEN rgt BETWEEN $menu->rgt + 1 AND $sibling->lft - 1 THEN
                                        rgt - (($menu->rgt - $menu->lft) + 1)
                                    WHEN rgt BETWEEN $menu->lft AND $menu->rgt THEN
                                        rgt + ($sibling->lft - ($menu->rgt + 1))
                                    ELSE
                                        rgt
                                END
                        END
                END
        WHERE lft BETWEEN $parent->lft + 1 AND $parent->rgt");
}

/**
 * 메뉴에 등록된 그룹 목록 반환
 *
 * @global object $DB
 * @param int $id
 * @return array
 */
function local_management_menu_get_groups($id) {
    global $DB;

    return $DB->get_records_sql("SELECT g.id, g.name, g.shortname, g.description, g.ismain
            , m.menuid
        FROM {management_group} g
        JOIN {management_menu_group} m ON m.groupid = g.id
        WHERE m.menuid = :menuid", array('menuid' => $id));
}

/**
 * 메뉴에 추가할 그룹 검색
 * $menuid에 등록된 그룹은 제외됨
 *
 * @global object $DB
 * @param int $menuid
 * @param string $search
 * @return array
 */
function local_management_menu_search_group($menuid = 0, $search = '') {
    global $DB;

    $sql = "SELECT g.*
        FROM {management_group} g";

    $conditions = array();
    $params = array();

    if($menuid) {
        $sql .= " LEFT JOIN {management_menu_group} m ON m.menuid = :menuid AND m.groupid = g.id";
        $params['menuid'] = $menuid;
        $conditions[] = 'm.groupid IS NULL';
    }

    if(!empty($search)) {
        $conditionsearch = array();

        // 이름
        $conditionsearch[] = $DB->sql_like("g.name", ":name", false);
        $params['name'] = "%$search%";

        // shortname
        $conditionsearch[] = $DB->sql_like("g.shortname", ":shortname", false);
        $params['shortname'] = "%$search%";

        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }

    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = ' WHERE ' . implode(' AND ', $conditions);
    }

    $sqlsort = "ORDER BY g.name";

    return $DB->get_records_sql("$sql $sqlwhere $sqlsort", $params);
}

/**
 * 메뉴에 그룹 추가
 *
 * @global object $DB
 * @global object $USER
 * @param int $menuid
 * @param int $groupid
 * @return boolean
 */
function local_management_menu_add_group($menuid, $groupid) {
    global $DB, $USER;

    if(!$DB->record_exists('management_menu_group', array('menuid' => $menuid, 'groupid' => $groupid))) {
        $group = new stdClass();
        $group->menuid = $menuid;
        $group->groupid = $groupid;
        $group->creator = $USER->id;
        $group->timecreated = time();
        return $DB->insert_record('management_menu_group', $group);
    }

    return false;
}

/**
 * 메뉴에서 그룹 삭제
 *
 * @global object $DB
 * @param int $menuid
 * @param int $groupid
 * @return int
 */
function local_management_menu_delete_group($menuid, $groupid) {
    global $DB;

    return $DB->delete_records('management_menu_group', array('menuid' => $menuid, 'groupid' => $groupid));
}

/**
 * 메뉴에 추가된 사용자 목록 반환
 *
 * @global object $DB
 * @global object $PAGE
 * @param int $id
 * @return array
 */
function local_management_menu_get_users($id) {
    global $DB, $PAGE;

    $allnames = get_all_user_name_fields(true, 'u');
    $picturefields = user_picture::fields('u');

    $userfields = array_merge(explode(',', $allnames), explode(',', $picturefields));
    $userfields = array_unique($userfields);

    if (($key = array_search('u.id', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    if (($key = array_search('u.firstname', $userfields)) !== false) {
        unset($userfields[$key]);
    }

    $struserfields = implode(', ', $userfields);

    $sql = "SELECT u.id, u.username, u.firstname, $struserfields
                , m.menuid
            FROM {management_menu_user} m
            JOIN {user} u ON u.id = m.userid
            WHERE m.menuid = :menuid";

    $users = $DB->get_records_sql($sql, array('menuid' => $id));

    foreach ($users as $user) {
        $userpicture = new user_picture($user);
        $userpicture->size = 64;
        $user->picture = $userpicture->get_url($PAGE)->out(false);
        $user->fullname = fullname($user);
    }

    return $users;
}

/**
 * 메뉴에 사용자 추가
 *
 * @global object $DB
 * @global object $USER
 * @param int $menuid
 * @param int $userid
 * @return boolean
 */
function local_management_menu_add_user($menuid, $userid) {
    global $DB, $USER;

    if(!$DB->record_exists('management_menu_user', array('menuid' => $menuid, 'userid' => $userid))) {
        $user = new stdClass();
        $user->menuid = $menuid;
        $user->userid = $userid;
        $user->creator = $USER->id;
        $user->timecreated = time();
        return $DB->insert_record('management_menu_user', $user);
    }

    return false;
}

/**
 * 메뉴 사용자 삭제
 *
 * @global object $DB
 * @param int $menuid
 * @param int $userid
 * @return int
 */
function local_management_menu_delete_user($menuid, $userid) {
    global $DB;

    return $DB->delete_records('management_menu_user', array('menuid' => $menuid, 'userid' => $userid));
}

/**
 * 메뉴에 추가할 사용자를 검색
 * $menuid에 등록된 사용자는 제외됨
 *
 * @global object $DB
 * @global object $PAGE
 * @param int $menuid
 * @param string $search
 * @return array
 */
function local_management_menu_search_user($menuid = 0, $search = '') {
    global $DB, $PAGE;

    $allnames = get_all_user_name_fields(true, 'u');
    $picturefields = user_picture::fields('u');

    $userfields = array_merge(explode(',', $allnames), explode(',', $picturefields));
    $userfields = array_unique($userfields);

    if (($key = array_search('u.id', $userfields)) !== false) {
        unset($userfields[$key]);
    }
    if (($key = array_search('u.firstname', $userfields)) !== false) {
        unset($userfields[$key]);
    }

    $struserfields = implode(', ', $userfields);

    $sql = "SELECT u.id, u.username, u.firstname, $struserfields
            FROM {user} u";

    $conditions = array();
    $params = array();

    if($menuid) {
        $sql .= " LEFT JOIN {management_menu_user} m ON m.menuid = :menuid AND m.userid = u.id";
        $params['menuid'] = $menuid;
        $conditions[] = 'm.userid IS NULL';
    }

    if(!empty($search)) {
        $conditionsearch = array();

        // 이름
        $conditionsearch[] = $DB->sql_like("u.firstname", ":firstname", false);
        $params['firstname'] = "%$search%";

        // 이메일
        $conditionsearch[] = $DB->sql_like("u.email", ":email", false);
        $params['email'] = "%$search%";

        $conditions[] = '('.implode(' OR ', $conditionsearch).')';
    }

    $sqlwhere = '';
    if(!empty($conditions)) {
        $sqlwhere = ' WHERE ' . implode(' AND ', $conditions);
    }

    $sqlsort = "ORDER BY u.firstname, u.email";

    $users = $DB->get_records_sql("$sql $sqlwhere $sqlsort",
        $params);

    foreach ($users as $user) {
        $userpicture = new user_picture($user);
        $userpicture->size = 64;
        $user->picture = $userpicture->get_url($PAGE)->out(false);
        $user->fullname = fullname($user);
    }

    return $users;
}
