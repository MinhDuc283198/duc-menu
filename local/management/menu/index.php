<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => '메뉴 관리',
    'heading' => '메뉴 관리',
    'subheading' => '',
    'menu' => 'menuM',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);
global $CFG, $DB, $OUTPUT;
$group_list=$DB->get_records('management_group');
include_once($CFG->dirroot.'/local/management/header.php');


?>
<section class="content">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">메뉴</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="myEditor" class="sortableLists list-group">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">메뉴 편집</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form id="frmEdit" role="form">
                        <input type="hidden" name="id" value="0" />
                        <input type="hidden" name="parentid" value="0" />
                        <div class="form-group">
                            <label for="text">Text</label>
                            <div class="input-group">
                                <div class="input-group-btn"><button type="button" id="myEditor_icon" class="btn btn-outline-secondary iconpicker dropdown-toggle"><i class="empty"></i><span class="caret"></span></button></div>
                                <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Text">
                            </div>
                            <input type="hidden" name="icon" class="item-menu">
                        </div>
                        <div class="form-group">
                            <label for="href">Identification</label>
                            <input type="text" class="form-control item-menu" id="shortname" name="shortname" placeholder="Identification">
                        </div>
                        <div class="form-group">
                            <label for="href">URL</label>
                            <input type="text" class="form-control item-menu" id="href" name="href" placeholder="URL">
                        </div>
                        <div class="form-group">
                            <label for="target">Target</label>
                            <select name="target" id="target" class="form-control item-menu">
                                <option value="_self">Self</option>
                                <option value="_blank">Blank</option>
                                <option value="_top">Top</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="groupid">Group</label>
                            <select name="groupid" id="groupid" class="form-control item-menu">
                                <?php  foreach ($group_list as $group){ ?>
                                    <option value="<?php echo $group->id ?>"><?php echo $group->shortname ?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Tooltip</label>
                            <input type="text" name="title" class="form-control item-menu" id="title" placeholder="Tooltip">
                        </div>
                        <div class="form-group">
                            <label for="txt_vi">Vietnamese</label>
                            <input type="text" name="txt_vi" class="form-control item-menu" id="txt_vi" placeholder="Vietnames">
                        </div>
                        <div class="form-group">
                            <label for="txt_en">English</label>
                            <input type="text" name="txt_en" class="form-control item-menu" id="txt_en" placeholder="English">
                        </div>
                        <div class="form-group">
                            <label for="txt_ko">Korean</label>
                            <input type="text" name="txt_ko" class="form-control item-menu" id="txt_ko" placeholder="Korean">
                        </div>
                        <div class="form-group">
                            <label for="target">Description</label>
                            <textarea class="form-control item-menu" rows="3" name="description" placeholder="Description"></textarea>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnUpdate" class="btn btn-primary" disabled="disabled"><i class="fa fa-refresh"></i> 수정</button>
                    <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> 추가</button>
                </div>
            </div>
        </div>
    </div>
</section>


<?php

include_once($CFG->dirroot.'/local/management/footer.php');
?>
<div class="modal modal-default fade" id="modal-add-group">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">그룹 추가</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchGroup">
                            <input type="hidden" name="id" value="0" />
                            <div class="input-group">
                                <input type="text" name="search" placeholder="Name, Identification 으로 검색하세요" class="form-control"/>
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-flat" id="btnSearchGroup">검색</button>
                            </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="group-list">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Identification</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td colspan="3">데이터가 없습니다</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="modal-add-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">사용자 추가</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <form action="#" method="post" id="searchUser">
                            <input type="hidden" name="id" value="0" />
                            <div class="input-group">
                                <input type="text" name="search" placeholder="이름, 이메일로 검색하세요" class="form-control"/>
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-flat" id="btnSearchUser">검색</button>
                            </span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="user-list">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>이름</th>
                                <th>이메일</th>
                                <th>회사</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td colspan="4">데이터가 없습니다</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="modal-user-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">사용자 정보</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-profile col-md-3">
                        <img class="profile-user-img img-responsive img-circle"/>
                        <h3 class="profile-username text-center"></h3>
                    </div>
                    <div class="col-md-9">
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>이메일</b> <span class="pull-right" id="email"></span>
                            </li>
                            <li class="list-group-item">
                                <b>회사</b> <span class="pull-right" id="company"></span>
                            </li>
                            <li class="list-group-item">
                                <b>직위</b> <span class="pull-right" id="position"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
            </div>
        </div>
    </div>
</div>
