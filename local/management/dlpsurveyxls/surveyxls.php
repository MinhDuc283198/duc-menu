<?php

header('Set-Cookie: fileDownload=true; path=/');
require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT);
$courseid = $DB -> get_field ('dlpsurvey','course',array('id' =>  $id));
$surveyanswerssql = " select  ROW_NUMBER() OVER(ORDER BY u.firstname,q.sortorder ASC) num 
     ,r.id 
     , du.mem_cd   
     , u.username  
     , u.firstname 
     , DS.name     
     , q.sortorder 
     , q.question  
     , s.score      
     , s.feedback
     , s.nfeedback
     ,gps.groups
     ,gps.teams
from {dlpsurvey_score} s
join {dlpsurvey_question} q on q.id = s.questionid
join {dlpsurvey_response} r on r.id = s.responseid
join {dlpsurvey} DS on DS.id = r.satisid
join {user} u ON u.id = r.userid
join {dlp_user} du ON du.userid = u.id
left join (
select gp.name as groups, g.name teams, gm.userid guser
FROM {groupings} gp 
JOIN {groupings_groups} gg ON gg.groupingid = gp.id 
JOIN {groups} g ON g.id = gg.groupid AND g.idnumber LIKE 'C%' 
JOIN {groups_members} gm on gm.groupid = g.id and gp.courseid = ? ) gps on  gps.guser = du.userid
where DS.id = ? and r.completion = 1";
$surveyanswers = $DB->get_records_sql($surveyanswerssql, array($courseid,$id));
$sql = "    select du.id, dc.courseid,cu.fullname,du.name,  dc.crs_nm , dc.crs_nmt,
            FORMAT(DATEADD(S, dc.crs_st_d, '1970-01-01 09:00:00'), 'yyyy') AS yy,du.timestart,du.qtype ";
$from = "            from {dlpsurvey} du
            join {course} cu  on du.course = cu.id 
            join {dlp_class} dc on cu.id = dc.courseid
            where  du.id = ?";
$survays = $DB->get_record_sql($sql . $from, array($id));

$downloadfilename = clean_filename($survays->name. date('Y-m-d', time()) . ".xls");
$workbook = new MoodleExcelWorkbook("-");
$workbook->send($downloadfilename);
$sheet = $workbook->add_worksheet("");

$xlscols1 = array(
    'head' => array('강좌번호', ' 강좌명 ', ' 설문명 ', '차수', '설문타입', '설문일')
);
$xlscols2 = array(
    'head' => array('참가자명','USERID','반','팀',  'E-Mail', '질문', '답변','좋았던점','보완할점')
);

$xlsformat = array(
    'head' => array('border' => 1, 'align' => 'center', 'v_align' => 'center', 'bg_color' => '#d0d0d0', 'text_wrap' => true),
    'default' => array('border' => 1, 'v_align' => 'center', 'bg_color' => 'white'),
);


$col = 0;
$row = 0;

foreach ($xlscols1['head'] as $head) {
    $sheet->write_string($row, $col, $head, $xlsformat['head']);
    $col += 1;
}
$row ++;

$col = 0;
$sheet->write($row, $col++, $survays->courseid, $xlsformat['default']);
$sheet->write($row, $col++, $survays->fullname, $xlsformat['default']);
$sheet->write($row, $col++, $survays->name, $xlsformat['default']);
$sheet->write($row, $col++, $survays->crs_nmt.' 차수', $xlsformat['default']);
$sheet->write($row, $col++, $survay->qtype == 1 ? '과정만족도' : '과목만족도' , $xlsformat['default']);
$sheet->write($row, $col++,$survay->timestart != 0 ? date('Y-m-d', $survay->timestart) : '미지정', $xlsformat['default']);
$row ++;

$col = 0;
foreach ($xlscols2['head'] as $head) {
    $sheet->write_string($row, $col, $head, $xlsformat['head']);
    $col += 1;
}
$row ++;
foreach ($surveyanswers as $surveyanswer) {
    $col = 0;
    $sheet->write($row, $col++,  $surveyanswer->firstname , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->id , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->groups != '' ? $surveyanswer->groups : '-' , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->teams != '' ? $surveyanswer->teams : '-', $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->username , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->question , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->score , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->feedback , $xlsformat['default']);
    $sheet->write($row, $col++,  $surveyanswer->nfeedback , $xlsformat['default']);
    $row += 1;
}
$workbook->close();

die();
