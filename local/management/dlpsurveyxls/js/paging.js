$(document).ready(function () {
    var xlsdownurl = '/mod/dlpsurvey/survey_excell.php?id=';
 
    $(document).on('click', " .xlsdowntable ", function () {
        xlsdownurl =  '/mod/dlpsurvey/survey_excell.php';
        var sid = $(this).attr('surveyid');
        $("#xlsdownlayer").show();
        $(".submit_reason").attr('data-surveyid', sid);
    });
    $(document).on('click', ".xlsdown  ", function () {
        xlsdownurl =  'surveyxls.php';
        var sid = $(this).attr('surveyid');
        $("#xlsdownlayer").show();
        $(".submit_reason").attr('data-surveyid', sid);
    });
    $("#xlsdownlayer .close,.cancle_reason").click(function () {
        $(this).closest("#xlsdownlayer").hide();
        $(".submit_reason").attr('data-surveyid', '');
    });
    $(".openpop").click(function () {
        $("#pop03").modal();
        $.ajax({
            type: 'post',
            url: '/local/management/dlpsurveyxls/xlsdownload.php',
            data: {
                id: $(this).attr("surveyid"),
            },
            success: function (data) {
                $(".survay").empty().append(data);
            }
        });
    });
    $(document).on('click', '.submit_reason', function () {
        var courseid = 0;
        var xlsurl = xlsdownurl;
        var button = $(this);
        button.hide();
        var reason = $('textarea[name=download_reasoninput]').val();
        var url_split = window.location.hostname;
        var url_split2 = window.location.href;
        var url = url_split2.split(url_split);
        var formData = new FormData();
        formData.append("reason", '[ 추천컨텐츠 다운로드 ]' + reason);
        formData.append("url", url[1]);
        formData.append("courseid", courseid);
        $.ajax({
            url: "/local/dlp/download_reason.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                if (data) {
                    $.fileDownload(xlsurl, {httpMethod: "POST", data: {id: $(".submit_reason").attr('data-surveyid')}}).done(function () {
                    }).fail(function () {
                        alert('다운로드에 실패하였습니다. 관리자에게 문의 주세요.');
                    });
                    $("#xlsdownlayer").hide();
                    $('textarea[name=download_reasoninput]').val('');
                    button.show();
                } else {
                    alert('사유 등록에 실패하였습니다. 다시 시도해주세요.');
                    button.show();
                }

            }
        });
    });
    // submit 할 때 goto_page 함수와 submit 이벤트에서
    // 무한루프에 빠지는 것을 방지하기 위해 사용
    var submitted = false;

    function goto_page(page) {
        // true로 바꿔 무한 루프에 빠지지 않도록 함
        submitted = true;

        $('input[name=page]').val(page);
        $('#allcourse_search').submit();
    }

    // 검색할 때 첫번째 페이지로 이동하기 위해 page 값을 1로 바꿈
    $('#allcourse_search').on('submit', function (e) {
        if (!submitted) {
            e.preventDefault();
            goto_page(1);
        }
    });

    $("ul.pagination li.page-item a").click(function (e) {
        e.preventDefault();
        var page = $(this).closest('li').attr('data-page-number');
        var cpage = $("ul.pagination li.page-item.active").attr('data-page-number');
        if (page != cpage) {
            goto_page(page);
        }
    });
});