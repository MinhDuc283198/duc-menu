<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->dirroot . '/mod/dlpsurvey/locallib.php');

$satisid = optional_param('id', 0, PARAM_INT);    // Course Module ID


if ($satisid != 0) {
    $satis = $DB->get_record('dlpsurvey', array('id' => $satisid), '*', MUST_EXIST);
    $course = $DB->get_record("course", array('id' => $satis->course));
    $cmid = $DB->get_field_sql(" select cm.id 
 from m_course_modules cm  
 join m_modules m on cm.module = m.id and m.name = 'dlpsurvey'
 where cm.instance = :id  ", array('id' => $satisid));
    global $DB, $USER;

//질문 정보 전체 가져오기

    $qtypes = dlpsurvey_get_question_all($satis->qtype);

    if ($satis->qtype == '2' && $satis->refercd != 0) {
        $lecture = $DB->get_record('courseinfo', array('course' => $satis->course, 'refercd' => $satis->refercd));

        // 강사로 등록되어 있는 경우 자신의 반만 나오도록 한다.
        $params = array('courseinfoid' => $lecture->id);
        $isteacher = $DB->record_exists_sql("SELECT 1 
        FROM {role_assignments} ra
        JOIN {context} ctx ON ctx.id = ra.contextid
        JOIN {course} co ON co.id = ctx.instanceid AND ctx.contextlevel = :contextlevel
        JOIN {courseinfo} ci ON ci.course = co.id AND ci.refercd = :refercd
        JOIN {courseinfo_time} ct ON ct.prof_userid = ra.userid AND ct.courseinfoid = ci.id
        JOIN {role} ro ON ro.id = ra.roleid
        WHERE co.id = :courseid AND ra.userid = :userid AND ro.shortname = :shortname"
                , array('contextlevel' => CONTEXT_COURSE, 'courseid' => $satis->course, 'userid' => $USER->id, 'shortname' => 'editingteacher', 'refercd' => $satis->refercd));

        if ($isteacher) {
            $params['prof_userid'] = $USER->id;
        }

        $teams = $DB->get_records('courseinfo_time', $params, 'class_n asc');
    }
    $countteams = $DB->count_records('courseinfo_time', array('courseinfoid' => $lecture->id));
    if ($countteams == 1) {
        unset($teams);
    }
    $output = "";

    $scores = dlpsurvey_get_score($satis->course, $satis->id, $teams);
    $feedbacks = dlpsurvey_get_feedback($satis->course, $satis->id);
    $scores['type_CB'] = $total->npsscore;
    $scores['type_CA'] = $total->satisscore;

    foreach ($qtypes as $qtype) {
        if ($qtype['code'] != 'CC' && $qtype['code'] != 'IC') {
            $output .= html_writer::tag('h3', get_string($qtype['name'], 'dlpsurvey'));
            if ($qtype['code'] == 'CA' || $qtype['code'] == 'IA') {
                $output .= html_writer::start_tag("div", array('class' => 'colorbox'));
                $output .= html_writer::tag("h6", "과목평가");
                $output .= html_writer::tag("p", "*과목평가 점수: 과목별 평가문항 9개의 평균값임.");
                $output .= html_writer::tag("p", "*강사평가 점수: 강사 전문지식, 강사 전달스킬, 강사 태도의 평균값임.");
//            $output .= '<span class="btn btn-primary float-right"><a href="' . $CFG->wwwroot . '/mod/dlpsurvey/survey_excell.php?id=' . $cm->instance . '">엑셀 다운로드</a></span>';
            } elseif ($qtype['code'] == 'CB' || $qtype['code'] == 'IB') {
                $output .= html_writer::start_tag("div", array('class' => 'colorbox'));
                $output .= html_writer::tag("h6", "NPS (Net Promoter Score)");
                $output .= html_writer::tag("p", "과정추천의향에 대해 0~10의 척도로 조사하며, 응답 결과를 Promoter(9,10점)Passives(7, 8점)");
                $output .= html_writer::tag("p", " Detractors(0~6점)로 구분함. NPS는 전체 응답자에 대한 Promoters의 비율(%)에서 Detrators의 비율(%)을 뺀 수치를 의미함.");
            }
            $output .= html_writer::end_tag("div"); //== colorbox end
        }
        if ($qtype['code'] == 'CA' || $qtype['code'] == 'IA') {
            $typescore = $scores['type_' . $qtype['code']];
            $table = new html_table();
            $table->attributes['class'] = 'generaltable satis_table';

            /* 과목만 */
            if ($qtype['code'] == 'IA') {
                if (count($teams) > 0) {
                    $table->head = array('반');
                }
            }
            /* HEAD */
            $table->head[] = get_string('qtype_joinper', 'dlpsurvey');
            $table->head[] = get_string($qtype['name'], 'dlpsurvey');
            if ($qtype['qtype'] == 2) {
                $table->head[] = get_string('qtype_po', 'dlpsurvey');
            }
            $table->align = array('center');
            $table->colclasses = array('satis_table_title', 'satis_table_score');
            foreach ($qtype['questions'] as $question) {
                $shortname = preg_replace('/question_ia/', 'questionname_ia', $question->question);
                $table->head[] = get_string($shortname, 'dlpsurvey');
                $table->align[] = 'center';
            }

            $table->data = array();
            if ($qtype['code'] == 'IA' && count($teams) > 0) {
                foreach ($teams as $team) {
                    $table->data[$team->class_n] = array();
                    $table->data[$team->class_n][] = $team->class_n . '. ' . $team->prof_nm;
                    $table->data[$team->class_n][] = $scores['type_joinper_' . $team->class_n] . '%';
                    $table->data[$team->class_n][] = $scores['type_' . $qtype['code'] . '_' . $team->class_n];
                    if ($qtype['qtype'] == 2) {
                        $table->data[$team->class_n][] = $scores['type_po_' . $team->class_n];
                    }
                    foreach ($qtype['questions'] as $question) {
                        $table->data[$team->class_n][] = $scores['satis_' . $question->id . '_' . $team->class_n];
                    }
                }
            } else {
                $table->data[0][] = $scores['type_joinper'] . '%';
                $total = dlpsurvey_get_total_score($satis->course, $satis->id);
                $table->data[0][] = $total->satisscore;
                if ($qtype['qtype'] == 2) {
                    $table->data[0][] = $scores['type_po'];
                }
                foreach ($qtype['questions'] as $question) {
                    $table->data[0][] = $scores['satis_' . $question->id];
                }
            }


            $output .= html_writer::table($table);
        }

        if ($qtype['code'] == 'CB' || $qtype['code'] == 'IB') {
            //nps 점수 가져오기
            $table = new html_table();
            $table->attributes['class'] = 'generaltable satis_table';

            /* 과목만 */
            if ($qtype['code'] == 'IB') {
                if (count($teams) > 0) {
                    $table->head = array('반');
                }
            }

            $table->head[] = 'NPS';
            $datas = array();
            for ($i = 1; $i <= 11; $i++) {
                $table->head[] = $i - 1;
                $table->align[] = 'center';
                $datas[$i] = get_string('people', 'dlpsurvey', 0);
            }

            $datas = array();
            if ($qtype['code'] == 'IB' && count($teams) > 0) {

                foreach ($teams as $team) {
                    $datas[0] = $team->class_n . '. ' . $team->prof_nm;
                    ksort($datas, SORT_NUMERIC);
                    if ($totalscore = dlpsurvey_get_total_score($satis->course, $satis->id, $team)) {
                        $datas[1] = $totalscore->npsscore;
                    } else {
                        $datas[1] = '-';
                    }
                    if ($npsscores = dlpsurvey_nps_score_table($satis->course, $satis->id, $team)) {
                        for ($i = 0; $i <= 10; $i++) {
                            if ($npsscores[$i]->cnt) {
                                $datas[$i + 2] = get_string('people', 'dlpsurvey', $npsscores[$i]->cnt);
                            } else {
                                $datas[$i + 2] = get_string('people', 'dlpsurvey', 0);
                            }
                        }
                        $table->data[$team->class_n] = $datas;
                    }
                }
            } else {
                if ($totalscore = dlpsurvey_get_total_score($satis->course, $satis->id)) {
                    $datas[0] = $totalscore->npsscore;
                } else {
                    $datas[0] = '-';
                }
                ksort($datas, SORT_NUMERIC);
                if ($npsscores = dlpsurvey_nps_score_table($satis->course, $satis->id)) {
                    for ($i = 0; $i <= 10; $i++) {
                        if ($npsscores[$i]->cnt) {
                            $datas[$i + 1] = get_string('people', 'dlpsurvey', $npsscores[$i]->cnt);
                        } else {
                            $datas[$i + 1] = get_string('people', 'dlpsurvey', 0);
                        }
                    }
                }
                $table->data[] = $datas;
            }

            $output .= html_writer::table($table);
        }
    }
}else{
  $output = '<div class="colorbox"><p>*해당 만족도에 대한 설문 결과가 없습니다.</p></div>';  
}
?>

<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <!-- 내용 -->
            <div id="page-mod-dlpsurvey-view">

                <table class="generaltable mg-bt20">
                    <tbody>
                        <tr>
                            <th>과정명</th>
                            <td><?php echo $course->fullname ?></td>
                            <th>설문명</th>
                            <td><?php echo $satis->name ?></td>
                        </tr>
                    </tbody>
                </table>

                <div>
                    <h3 class="f-l">과목평가점수</h3>
                    <div class="f-r">
                        <button type="type" class="btn btn-success xlsdowntable" surveyid="<?php echo $satis->id ?>" cmid="<?php echo $cmid ?>">엑셀 다운로드</button>
                        <button type="type" class="btn btn-primary xlsdown" surveyid="<?php echo $satis->id ?>">RAW 데이터 다운로드</button>
                    </div>
                </div>
<?php echo $output; ?>
            </div><!-- 내용 -->

        </div>
    </div>
</div><!-- popup 영역 -->
