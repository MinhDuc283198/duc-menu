<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/management/courses/lib.php';

$pagesettings = array(
    'title' => '만족도 설문',
    'heading' => '만족도 설문',
    'subheading' => '',
    'menu' => 'dlpsurveyxls',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);


$currpage = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$years = optional_param('years', '', PARAM_RAW);

$where_str = '';
$sql_where = array();
$params = array();

$sql = "    select du.id, dc.courseid,cu.fullname,du.name,  dc.crs_nm , dc.crs_nmt,
            FORMAT(DATEADD(S, dc.crs_st_d, '1970-01-01 09:00:00'), 'yyyy') AS yy,du.timestart,du.qtype";
$from = "            from {dlpsurvey} du
            join {course} cu  on du.course = cu.id 
            join {dlp_class} dc on cu.id = dc.courseid ";

$params = array();
if ($years != '') {
    $sdate = strtotime($years . '-01-01');
    $edate = strtotime($years . '-12-31') + 86399;
    $sql_where[] = ' (crs_st_d >= :sdate AND crs_st_d <= :edate ) ';
    $params['sdate'] = $sdate;
    $params['edate'] = $edate;
}
if ($searchtext != '') {
    $sql_where[] = " (cu.fullname like :stext1 or dc.courseid like :stext2)";
    $params['stext1'] = "%$searchtext%";
    $params['stext2'] = "%$searchtext%";
}
if ($sql_where) {
    $where_str = ' WHERE ' . implode(' and ', $sql_where);
}
$order = ' order by dc.crs_st_d desc  ';

$survays = $DB->get_records_sql($sql . $from . $where_str . $order, $params, ($currpage - 1) * $perpage, $perpage);
foreach ($survays as $survay) {
    $rownum[$survay->courseid] += 1;
}
$survayscount = $DB->count_records_sql("select count (*) " . $from . $where_str, $params);
$min_yesr = date('Y', $DB->get_field('dlp_class', 'MIN(crs_st_d)', []));
$max_yesr = date('Y', $DB->get_field('dlp_class', 'MAX(crs_st_d)', []));

include_once '../header.php';
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-body">
                    <form id="allcourse_search">
                        <input type='hidden' name='page' value="<?php echo $currpage; ?>" />
                        <div class="form-group input-group-sm col-xs-5">
                            <input type="text" class="form-control" name='searchtext' placeholder="강좌명 또는 강좌 번호를 입력해주세요." value="<?php echo $searchtext; ?>" >
                        </div>

                        <div class="form-group col-xs-3">
                            <select class="form-control" name='years'>
                                <option value="" >년도 선택</option>
                                <?php
                                for ($i = $min_yesr; $i <= $max_yesr; $i++) {
                                    $selstr = '';
                                    if ($years == $i) {
                                        $selstr = 'selected';
                                    }
                                    echo '<option value="' . $i . '" ' . $selstr . ' >' . $i . '년도</option>';
                                }
                                ?>  
                            </select>
                        </div>

                        <div class="form-group col-xs-1">
                            <button type="type" class="btn btn-primary">검색</button>
                        </div>
                    </form>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>강좌 번호</th>
                                <th>강좌명</th>
                                <th>설문명</th>
                                <th>차수</th>
                                <th>설문타입</th>
                                <th>설문일</th>
                                <th>엑셀다운로드</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nowcourseid = 0;
                            if ($survays) {
                                foreach ($survays as $survay) {
                                    if ($survay->courseid == $nowcourseid) {
                                        $insert = 0;
                                    } else {
                                        $insert = 1;
                                        $nowcourseid = $survay->courseid;
                                    }
                                    ?>
                                    <tr>
                                        <?php if ($insert == 1) { ?>
                                            <td rowspan="<?php echo $rownum[$survay->courseid] ?>"><?php echo $survay->courseid; ?></td>
                                            <td rowspan="<?php echo $rownum[$survay->courseid] ?>"><?php echo '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $survay->courseid . '">' . $survay->fullname . '</a>'; ?></td>
                                        <?php } ?>
                                        <td><?php echo $survay->name; ?></td>
                                        <td><?php echo $survay->crs_nmt . ' 차수'; ?></td>
                                        <td><?php echo $survay->qtype == 1 ? '과정만족도' : '과목만족도' ?></td>
                                        <td><?php echo $survay->timestart != 0 ? date('Y-m-d', $survay->timestart) : '미지정'; ?></td>
                                        <td>
                                            <button type="type" class="btn btn-primary openpop" surveyid="<?php echo $survay->id?>">확인</button>
                                            <button type="type" class="btn btn-success xlsdown" surveyid="<?php echo $survay->id ?>">다운로드</button>
                                        </td>
                                    </tr>    
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <div class="text-center">

                <?php
                local_management_courses_paging($survayscount, $currpage, $perpage);
                ?>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<div class = "modal" id="xlsdownlayer" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <h5 class = "modal-title">다운로드 사유</h5>
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
                    <span aria-hidden = "true">&times;
                    </span>
                </button>
            </div>
            <div class = "modal-body">
                <div class = "custom-file">
                    <div class="inp-form-wp">
                        <h3> 다운로드 사유를 입력하세요.</h3>
                        <div class="inp-cont">
                            <p><textarea class="form-control" name="download_reasoninput" style="resize: none; height: 150px;" placeholder="ex) 교육참가자 숙소안내용"></textarea></p>
                        </div>
                    </div>
                </div>
                <div class="t-center">
                    <button type="button" class="btn btn-primary submit_reason">확인</button>
                    <button type="button" class="btn cancle_reason" >취소</button>
                </div>
            </div>

        </div>
    </div>
</div>
<div class = "modal" id="xlsdownlayertable" tabindex = "-1" role = "dialog">
    <div class = "modal-dialog" role = "document">
        <div class = "modal-content">
            <div class = "modal-header">
                <h5 class = "modal-title">다운로드 사유</h5>
                <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
                    <span aria-hidden = "true">&times;
                    </span>
                </button>
            </div>
            <div class = "modal-body">
                <div class = "custom-file">
                    <div class="inp-form-wp">
                        <h3> 다운로드 사유를 입력하세요.</h3>
                        <div class="inp-cont">
                            <p><textarea class="form-control" name="download_reasoninput" style="resize: none; height: 150px;" placeholder="ex) 교육참가자 숙소안내용"></textarea></p>
                        </div>
                    </div>
                </div>
                <div class="t-center">
                    <button type="button" class="btn btn-primary submit_reason">확인</button>
                    <button type="button" class="btn cancle_reason" >취소</button>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- popup 영역 -->
<link rel="stylesheet" href="/theme/oklassv3/style/jt.css" />
<div class="modal fade pop-layer-wp survay" id="pop03" role="dialog" ><!--headr 있는 일반팝업 클래스 pop-layer-wp -->
</div>
<?php include_once '../footer.php'; ?>
