/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    $("[data-toggle='tab'] a").click(function(){
        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");
        var target = $(this).attr("href");
        $(target).removeClass("hide").addClass("show").addClass("active");
        $(target).siblings().removeClass("show").removeClass("active").addClass("hide");
        return false;
    });
    
});