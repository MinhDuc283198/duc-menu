<?php
define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');

$action = optional_param('action','',PARAM_RAW);
switch ($action) {
    case 'search' :
        $data = required_param_array('data', PARAM_RAW);
        $stype = trim($data['stype']);
        $code= trim($data['code']);
        if($stype){
            if ($stype == 1) { 
                $where = $code != null ? "  WHERE j.id = " . $code . " AND j.id not in (SELECT jobid from {lmsdata_main_job} where type =0 ) " : "";
            } else if ($stype == 2) {
                $where = $code != null ? "  WHERE j.title like '%" . $code . "%'" . " AND j.id not in (SELECT jobid from {lmsdata_main_job} where type =0 ) " : "";
            } else if ($stype == 3) {
                $where = $code != null ? "  WHERE e.company_name like '%" . $code . "%'" . " AND j.id not in (SELECT jobid from {lmsdata_main_job} where type =0 ) " : "";
            }      
        } else {
            $where =" WHERE j.id  = $code  ";
        }
        
        $sql = "SELECT j.*, e.company_name
                FROM {vi_jobs} j
                JOIN {vi_employers} e ON e.id = j.employer_id
                $where";
        $cnt_sql = "SELECT count(j.id)
                FROM {vi_jobs} j
                JOIN {vi_employers} e ON e.id = j.employer_id
                $where";
        $orderby = ' ORDER BY j.timecreated DESC';

        $list = $DB->get_records_sql($sql  . $orderby);
        $total = $DB->count_records_sql($cnt_sql);

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        $rvalue->total = $total;

        echo json_encode($rvalue);
        break;
    case 'add' :
        $data = required_param_array('data', PARAM_RAW);
        $lmsdata = new stdClass();
        $lmsdata->timecreated = time();
        $lmsdata->type = 0;
        $lmsdata->jobid = $data['code'];
        $DB->insert_record('lmsdata_main_job', $lmsdata);

        $rvalue = new stdClass();
        $rvalue->status = 'success';

        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}
