<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/curriculum/lib.php';

$crs_cd = required_param('crs_cd', PARAM_INT);

$context = context_system::instance();
$crsinfo = local_curriculum_get_course_info($crs_cd);
$crsinfo->thumbnail = local_curriculum_get_course_thumbnail($context, $crs_cd);
$crsinfo->guides = local_curriculum_get_course_guides($context, $crsinfo->crs_cd);

$pagesettings = array(
    'title' => '과정 정보',
    'heading' => '과정 정보',
    'subheading' => $crsinfo->crs_nm,
    'menu' => 'curriculum',
    'js' => array('/local/management/curriculum/course.js'),
    'css' => array(),
    'nav_bar' => array(array('url' => '/local/management/curriculum/course.php?crs_cd=' . $crs_cd, 'text' => '과정 정보'))
);

include_once('../header.php');
?> 

<!---------- content start ---------->
<section class="content">
    <div class="row">
        <section class="col-lg-6">
            <div class="box box-primary course-info">
                <div class="box-header with-border">
                    <h3 class="box-title">과정 정보</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="txt" style="top: 100%;">
                        <div class="m-scroll">
                            <p>
                                <strong><?php echo get_string('coursename', 'local_curriculum'); ?></strong>
                                <span id="target"><?php echo $crsinfo->crs_nm; ?></span>
                            </p>
                            <p>
                                <strong><?php echo get_string('target', 'local_curriculum'); ?></strong>
                                <span id="target"><?php echo $crsinfo->target; ?></span>
                            </p>
                            <!-- p>
                                <strong><?php echo get_string('objective', 'local_curriculum'); ?></strong>
                                <span id="objective"><?php echo $crsinfo->objective; ?></span>
                            </p -->
                            <p>
                                <strong><?php echo get_string('duration', 'local_curriculum'); ?></strong>
                                <span id="duration"><?php echo $crsinfo->duration; ?></span>
                            </p>
                            <p>
                                <strong><?php echo get_string('price', 'local_curriculum'); ?></strong>
                                <span id="price">
                                    <?php echo get_string('price:affiliate', 'local_curriculum', $crsinfo->crs_ir_co_uprc); ?><br>
                                    <?php echo get_string('price:nonaffiliate', 'local_curriculum', $crsinfo->crs_u_ir_co_uprc); ?>
                                </span>
                            </p>
                            <p>
                                <strong><?php echo get_string('location', 'local_curriculum'); ?></strong>
                                <span id="location"><?php echo $crsinfo->location; ?></span>
                            </p>
                            <p>
                                <strong><?php echo get_string('managers', 'local_curriculum'); ?></strong>
                                <span id="managers"><?php
                                    foreach ($crsinfo->managers as $manager) {
                                        echo '<strong>' . $manager->mem_ko_nm . ' ' . $manager->position . '</strong><br/>';
                                        echo '<strong>E</strong>. ' . $manager->em;
                                        echo '&nbsp;<strong>T</strong>. ' . $manager->ph_n;
                                    }
                                    ?></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnList" class="btn btn-primary pull-right" data-cd="<?php echo $crsinfo->cmn_cd; ?>"><i class="fa fa-list"></i> <?php echo get_string('list', 'local_curriculum'); ?></button>
                </div>
            </div>
            <div class="box box-info course-thumbnail">
                <div class="box-header with-border">
                    <h3 class="box-title">썸네일</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="c-img">
                        <img src="<?php echo $crsinfo->thumbnail; ?>" alt="<?php echo $crsinfo->crs_nm; ?>">
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnThumbnail" class="btn btn-primary" data-toggle="modal" data-target="#modal-upload-thumbnail"><i class="fa fa-upload"></i> <?php echo get_string('upload', 'local_curriculum'); ?></button>
                </div>
            </div>
        </section>
        <section class="col-lg-6">
            <div class="box box-info course-guide">
                <div class="box-header with-border">
                    <h3 class="box-title">과정 안내</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="img-area">
                        <ul><?php
                            foreach ($crsinfo->guides as $guide) {
                                echo '<li style="text-align:center"><img src="' . $guide->url . '"></li>';
                            }
                            ?></ul>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnGuide" class="btn btn-primary" data-toggle="modal" data-target="#modal-upload-guides"><i class="fa fa-upload"></i> <?php echo get_string('upload', 'local_curriculum'); ?></button>
                </div>
            </div>
        </section>
    </div>
</section>


<!---------- content end ---------->
<?php
include_once '../footer.php';
?>
<div class="modal modal-default fade modal-dialog-centered" id="modal-upload-thumbnail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">썸네일 업로드</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <div class="upload-guide">
                            .jpg, .png 파일을 업로드하세요.
                        </div>
                        <form action="upload-thumbnail.php" method="post" enctype="multipart/form-data" id="fileupload_thumbnail">
                            <input type="hidden" name="crs_cd" value="<?php echo $crsinfo->crs_cd; ?>" />
                            <div class="input-group">
                                <input type="text" id="thumbnail_filename" class="form-control" readonly="readonly" />
                                <input type="file" name="attach" accept=".jpg,.png" id="thumbnail_file" style="display:none;" />
                                <div class="input-group-btn">
                                    <label for="thumbnail_file" class="btn btn-primary select-file">파일 선택</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
                <button type="button" class="btn btn-primary" id="submit_thumbnail">업로드</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade modal-dialog-centered" id="modal-upload-guides">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">과정 안내 업로드</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-danger col-md-12">
                        <div class="upload-guide">
                            .jpg, .png, .zip 파일을 업로드하세요.
                        </div>
                        <form action="upload-guides.php" method="post" enctype="multipart/form-data" id="fileupload_guides">
                            <input type="hidden" name="crs_cd" value="<?php echo $crsinfo->crs_cd; ?>" />
                            <div class="input-group">
                                <input type="text" id="guides_filename" class="form-control" readonly="readonly" />
                                <input type="file" name="attach" accept=".jpg,.png,.zip" id="guides_file" style="display:none;" />
                                <div class="input-group-btn">
                                    <label for="guides_file" class="btn btn-primary select-file">파일 선택</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
                <button type="button" class="btn btn-primary" id="submit_guides">업로드</button>
            </div>
        </div>
    </div>
</div>