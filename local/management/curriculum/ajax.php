<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/curriculum/lib.php');

$action = required_param('action', PARAM_ALPHAEXT);

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('curriculum');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}

$rdata = new stdClass();

switch ($action) {
    case 'categories' :
        $categories = local_curriculum_get_categories();
        
        $rdata->status = 'success';
        $rdata->categories = $categories;
        break;
    case 'category' :
        $data = required_param_array('data', PARAM_RAW);
        $cmn_cd = $data['cmn_cd'];
        $cmn_cds = explode(',', $cmn_cd);
        
        $category = local_curriculum_get_category_info(array_shift($cmn_cds));
        
        $rdata->status = 'success';
        $rdata->category = $category;
        break;
    case 'courses' :
        $data = required_param_array('data', PARAM_RAW);
        $cmn_cd = $data['cmn_cd'];
        $isonline = $data['isonline'];
        
        $cmn_cds = explode(',', $cmn_cd);
        $courses = array();
        foreach ($cmn_cds as $cmn_cd) {
            $courses = array_merge($courses, local_curriculum_get_courses($cmn_cd, $isonline));
        }
        foreach($courses as $course) {
            $course->thumbnail = local_curriculum_get_course_thumbnail($context, $course->crs_cd);
        }
        $rdata->status = 'success';
        $rdata->courses = $courses;
        break;
    case 'roadmaps' :
        $data = required_param_array('data', PARAM_RAW);
        $cmn_cd = $data['cmn_cd'];
        
        $cmn_cds = explode(',', $cmn_cd);
        $roadmaps = array();
        foreach ($cmn_cds as $cmn_cd) {
            $roadmaps = array_merge($roadmaps, local_curriculum_get_course_roadmaps($context, $cmn_cd));
        }
        $rdata->status = 'success';
        $rdata->roadmaps = $roadmaps;
        break;
    case 'deleteroadmaps' :
        $data = required_param_array('data', PARAM_RAW);
        $cmn_cd = $data['cmn_cd'];
        
        $fs = get_file_storage();
        $fs->delete_area_files($context->id, 'local_curriculum', 'roadmap', $cmn_cd);
        
        $rdata->status = 'success';
        $rdata->roadmaps = $roadmaps;
        break;
    case 'course' :
        $crs_cd = required_param('cmn_cd', PARAM_INT);
        
        $course = local_curriculum_get_category_info($crs_cd);
        $course->thumbnail = local_curriculum_get_course_thumbnail($context, $course->crs_cd);
        $crsinfo->guides = local_curriculum_get_course_guides($context, $course->crs_cd);
        
        $rdata->status = 'success';
        $rdata->course = $course;
        break;
    default :
        throw new moodle_exception('invalidarguments');
}

echo $OUTPUT->header();
echo json_encode($rdata);
echo $OUTPUT->footer();