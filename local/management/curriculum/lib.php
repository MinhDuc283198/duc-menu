<?php

function local_management_curriculum_get_leaf_categories($categories = null) {
    if(empty($categories)) {
        $categories = local_curriculum_get_categories();
        
        $offline = local_management_curriculum_get_leaf_categories($categories->offline);
        $online = local_management_curriculum_get_leaf_categories($categories->online);
        
        return array_merge($offline, $online);
    }
    
    $leafs = array();
    
    foreach($categories as $category) {
        if($category->has_child) {
            $leafs = array_merge($leafs, local_management_curriculum_get_leaf_categories($category->children));
        } else {
            $leafs[] = $category;
        }
    }
    
    return $leafs;
}
