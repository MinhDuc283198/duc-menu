<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/curriculum/lib.php';
require_once $CFG->dirroot . '/lib/gdlib.php';

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('curriculum');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}

// 경리 대학의 경우 ","로 구분된 값이다.
$cmn_cd_param = required_param('cmn_cd', PARAM_RAW);
// 첫번째 cmn_cd에 등록한다. 모두에 등록할 필요가 있을까??
$cmn_cds = explode(',', $cmn_cd_param);
$cmn_cd = array_shift($cmn_cds);

// 업로드 가능한 이미지 파일
$imgfiles = array('jpg', 'png');

$roadmapfile = $_FILES['attach'];

$ext = pathinfo($roadmapfile['name'], PATHINFO_EXTENSION);

$roadmaps = array();
if(strtolower($ext) === 'zip') {
    $zippacker = get_file_packer('application/zip');
    $pathtoextract = $CFG->dataroot . '/temp/curriculum/roadmaps/' . $cmn_cd;
    
    $files = $zippacker->extract_to_pathname($roadmapfile['tmp_name'], $pathtoextract);
    foreach($files as $filename=>$success) {
        $filepath = $pathtoextract . '/' . $filename;
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if(!is_dir($filepath) && in_array(strtolower($ext), $imgfiles)) {
            $roadmap = new stdClass();
            $roadmap->filename = $filename;
            $roadmap->filepath = $filepath;
            $roadmaps[] = $roadmap;
        }
    }
    unlink($roadmapfile['tmp_name']);
    
    // 파일 이름순으로 정렬
    usort($roadmaps, 'local_curriculum_sort_files');
} else {
    $roadmap = new stdClass();
    $roadmap->filename = $roadmapfile['name'];
    $roadmap->filepath = $roadmapfile['tmp_name'];
    $roadmaps[] = $roadmap;
}

$fs = get_file_storage();
$fs->delete_area_files($context->id, 'local_curriculum', 'guide', $cmn_cd);

$sortorder = 1;
foreach($roadmaps as $roadmap) {
    $filename = $roadmap->filename;
    $filepath = $roadmap->filepath;
    
    if(file_exists($filepath)) {

        $filerecord = array('component' => 'local_curriculum', 'filearea' => 'roadmap',
            'contextid' => $context->id, 'itemid' => $cmn_cd, 'filepath' => '/',
            'filename' => $filename, 'userid' => $USER->id, 'sortorder' => $sortorder);
        $stored_file = $fs->create_file_from_pathname($filerecord, $filepath);

        $sortorder += 1;
        
        unlink($filepath);
    }
}

redirect(new moodle_url('/local/management/curriculum/index.php', array('cmn_cd' => $cmn_cd_param)));