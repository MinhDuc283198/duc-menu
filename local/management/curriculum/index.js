$(document).ready(function () {
    var cmn_cd = null;
    
    var params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) { params[key] = value; });
    
    if('cmn_cd' in params) {
        cmn_cd = params.cmn_cd;
    }
    
    var category = new Category();
    category.init(1024*1024*100, cmn_cd);
});