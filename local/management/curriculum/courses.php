<?php 
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/curriculum/lib.php';
require_once $CFG->dirroot . '/local/management/curriculum/lib.php';


$context = context_system::instance();

/*
$crs_cd = required_param('crs_cd', PARAM_INT);
$crsinfo = local_curriculum_get_course_info($crs_cd);
$crsinfo->thumbnail = local_curriculum_get_course_thumbnail($context, $crs_cd);
$crsinfo->guides = local_curriculum_get_course_guides($context, $crsinfo->crs_cd);
*/

$pagesettings = array(
    'title' => '전체 과정',
    'heading' => '전체 과정',
    'subheading' => $crsinfo->crs_nm,
    'menu' => 'curriculum',
    'js' => array('/local/management/curriculum/course.js'),
    'css' => array(),
    'nav_bar'=> array(array ('url'=>'/local/management/curriculum/courseㄴ.php', 'text'=>'전체 과정'))
);


$maxdepth = 3;

include_once('../header.php');
?> 

<!---------- content start ---------->
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <table class="table table-hover table-bordered dataTable">
                    <thead>
                        <tr>
                            <th>분류 코드</th>
                            <?php
//                            for($i = 0; $i < $maxdepth; $i++) {
//                                echo '<th>분류 '. ($i + 1) .'</th>';
//                            }
                            ?>
                            <th>분류</th>
                            <th>과정 코드</th>
                            <th>과정</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $categories = local_management_curriculum_get_leaf_categories();
                        foreach ($categories as $category) {
                            $cmn_cds = explode(',', $category->cmn_cd);
                            foreach ($cmn_cds as $cmn_cd) {
                                $categoryinfo = local_curriculum_get_category_info($cmn_cd, '>>');
                                $categorypaths = explode('>>', $categoryinfo->cmn_path);
                                $categorypath = ($categoryinfo->isonline == 1) ? '정규 e-learning' : '정규집합';
                                array_unshift($categorypaths, $categorypath);

                                $courses = local_curriculum_get_courses($cmn_cd, $categoryinfo->isonline);
                                if($courses) {
                                    foreach($courses as $course) {
                                        echo '<tr>';
                                        echo '<td>'.$categoryinfo->cmn_cd.'</td>';

//                                        $countpaths = count($categorypaths);
//                                        $count = 0;
//                                        foreach($categorypaths as $index => $categorypath) {
//                                            echo '<td>';
//                                            if($countpaths - $index == 1) {
//                                                echo '<a href="' . $CFG->wwwroot . '/local/management/curriculum/index.php?cmn_cd=' . $category->cmn_cd . '">'. $categorypath . '</a>';
//                                            } else {
//                                                echo $categorypath;
//                                            }
//                                            echo '</td>';
//                                            $count += 1;
//                                        }
//                                        for($i = $count; $i < $maxdepth; $i++) {
//                                            echo '<td>-</td>';
//                                        }
                                        echo '<td><a href="' . $CFG->wwwroot . '/local/management/curriculum/index.php?cmn_cd=' . $category->cmn_cd . '">' . implode(' &#10219; ', $categorypaths) . '</a></td>';

                                        echo '<td><a href="' . $CFG->wwwroot . '/local/management/curriculum/course.php?crs_cd=' . $course->crs_cd . '">' . $course->crs_cd . '</a></td>';
                                        echo '<td><a href="' . $CFG->wwwroot . '/local/management/curriculum/course.php?crs_cd=' . $course->crs_cd . '">' . $course->crs_nm . '</a></td>';
                                        echo '</tr>';
                                    }
                                } else {
                                    echo '<tr>';
                                    echo '<td>'.$categoryinfo->cmn_cd.'</td>';

//                                    $countpaths = count($categorypaths);
//                                    $count = 0;
//                                    foreach($categorypaths as $index => $categorypath) {
//                                        echo '<td>';
//                                        if($countpaths - $index == 1) {
//                                            echo '<a href="' . $CFG->wwwroot . '/local/management/curriculum/index.php?cmn_cd=' . $category->cmn_cd . '">'. $categorypath . '</a>';
//                                        } else {
//                                            echo $categorypath;
//                                        }
//                                        echo '</td>';
//                                        $count += 1;
//                                    }
//                                    for($i = $count; $i < $maxdepth; $i++) {
//                                        echo '<td>-</td>';
//                                    }
                                    echo '<td><a href="' . $CFG->wwwroot . '/local/management/curriculum/index.php?cmn_cd=' . $category->cmn_cd . '">' . implode(' &#10219; ', $categorypaths) . '</a></td>';

                                    echo '<td>-</td>';
                                    echo '<td>-</td>';
                                    echo '</tr>';
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!---------- content end ---------->
<?php 
include_once '../footer.php';