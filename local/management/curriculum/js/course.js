function Course() {
    var maxSize = 0;
    
    function validateFile(obj, allow) {
        if(obj.files.length == 0) {
            alert("파일을 선택하세요.");
            return false;
        }
        
        if(0 == obj.files[0].size){
            alert("파일 크기가 0 byte인 파일은 추가할 수 없습니다.");
            return false;
        }
        
        if(maxSize > 0 && obj.files[0].size > maxSize) {
            alert('업로드 파일 최대 크기는 ' + convertReadableFilesize(maxSize) + ' 입니다.');
            
            return false;
        }
        
        var filename = obj.files[0].name;

        if(filename == undefined) {
            return false;
        }
        
        var ext = filename.match(/\.([^\.]+)$/)[0];
        if($.inArray(ext, allow) == -1) {
            alert(allow.join(', ') + " 파일을 업로드할 수 있습니다.");
            return false;
        }
        
        return true;
    }
    
    function validateThumbnail(obj) {
        var validated = validateFile(obj, ['.jpg', '.png']);

        if(!validated) {
            $("#thumbnail_filename").val('');
            $(obj).val('');

            return false;
        }
            
        $("#thumbnail_filename").val(obj.files[0].name);
        
        return true;
    }
    
    function validateGuides(obj) {
        var validated = validateFile(obj, ['.jpg', '.png', '.zip']);

        if(!validated) {
            $("#guides_filename").val('');
            $(obj).val('');

            return false;
        }
            
        $("#guides_filename").val(obj.files[0].name);
        
        return true;
    }
    
    function convertReadableFilesize(bytes) {
        var thresh = 1024;
        
        if(Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        
        var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
        var u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while(Math.abs(bytes) >= thresh && u < units.length - 1);
        
        return bytes.toFixed(1)+' '+units[u];
    }
    
    return {
        init: function(maxsize) {
            maxSize = maxsize;
            
            $(document).on('click', '#btnList', function (e) {
                e.preventDefault();
                
                document.location.href = 'index.php';
            });
            
            // 썸네일 업로드
            $('#submit_thumbnail').click(function() {
                $('#fileupload_thumbnail').submit();
            });
            $('#fileupload_thumbnail').submit(function() {
                return validateThumbnail($('input[type=file]#thumbnail_file')[0]);
            });
            $('#thumbnail_file').change(function() {
                validateThumbnail(this);
            });
            $('#modal-upload-thumbnail').on('show.bs.modal', function (event) {
                $("#thumbnail_filename").val('');
                $("input[type=file]#thumbnail_file").val('');
            });
            
            // 과정 안내 업로드 
            $('#submit_guides').click(function() {
                $('#fileupload_guides').submit();
            });
            $('#fileupload_guides').submit(function() {
                return validateGuides($('input[type=file]#guides_file')[0]);
            });
            $('#guides_file').change(function() {
                validateGuides(this);
            });
            $('#modal-upload-guides').on('show.bs.modal', function (event) {
                $("#guides_filename").val('');
                $("input[type=file]#guides_file").val('');
            });
        }
    }
}