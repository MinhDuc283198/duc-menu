function Category() {
    var maxSize = 0;
    var $main = $("#category-listing");
    var options = { 
        hintCss: { border: '1px dashed #13981D'}, 
        opener: {
            as: 'html',
            close: '<i class="fa fa-minus"></i>',
            open: '<i class="fa fa-plus"></i>',
            openerCss: {'margin-left': '10px', 'float': 'none'},
            openerClass: 'btn btn-success btn-sm',
        },
        ignoreClass: 'clickable',
        listsClass: "pl-0",
        listsCss: {"padding-top": "10px"}
    };
    
    $.fn.iconOpen = function(setting){
        this.removeClass('sortableListsClosed').addClass('sortableListsOpen');
        this.children('ul').css('display', 'block');
        var opener = this.children('div').children('.sortableListsOpener').first();
        if (setting.opener.as === 'html'){
            opener.html(setting.opener.close);
        } else if (setting.opener.as === 'class') {
            opener.addClass(setting.opener.close).removeClass(setting.opener.open);
        }
    };
    $.fn.iconClose = function(setting) {
        this.removeClass('sortableListsOpen').addClass('sortableListsClosed');
        this.children('ul').css('display', 'none');
        var opener = this.children('div').children('.sortableListsOpener').first();
        if (setting.opener.as === 'html') {
            opener.html(setting.opener.open);
        } else if (setting.opener.as === 'class') {
            opener.addClass(setting.opener.open).removeClass(setting.opener.close);
        }
    };
    
    function TButton(attr) {
        return $("<a>").addClass(attr.classCss).addClass('clickable').attr("href", "#").html(attr.text);
    }
    
    function TButtonGroup() {
        var $divbtn = $('<div>').addClass('btn-group pull-right');
        var $btnInfo = TButton({classCss: 'btn btn-info btn-sm btnInfo', text: '<i class="fa fa-info-circle clickable"></i>'});
        $divbtn.append($btnInfo);
        
        return $divbtn;
    }
    
    function TOpener(li){
        var opener = $('<span>').addClass('sortableListsOpener ' + options.opener.openerClass).css(options.opener.openerCss)
                .on('mousedown touchstart', function (e){
                    var li = $(this).closest('li');
                    if (li.hasClass('sortableListsClosed')) {
                        li.iconOpen(options);
                    } else {
                        li.iconClose(options);
                    }
                    return false; // Prevent default
                });
        opener.appendTo(li.children('div').first());
        li.iconOpen(options);
    }
    
    function createCategory(arrayItem, depth) {
        var level = (typeof (depth) === 'undefined') ? 0 : depth;
        var $elem = (level === 0) ? $main : $('<ul>').addClass('pl-0').css('padding-top', '10px');
        $.each(arrayItem, function (k, v) {
            var isParent = (typeof (v.children) !== "undefined") && ($.isArray(v.children));
            var itemObject = {cmn_cd: "", cmn_cd_nm: "", has_child: 0, isonline: 0};
            var temp = $.extend({}, v);
            if (isParent){ 
                delete temp['children'];
            }
            $.extend(itemObject, temp);
            
            var $li = $('<li>').addClass('list-category-item pr-0');
            $li.data(itemObject);
            var $div = $('<div>').css('overflow', 'auto');
            var $span = $('<span>').addClass('txt').append(v.cmn_cd_nm).css('margin-right', '5px');
            $div.append($span);
            $li.append($div);
            if($li.data().has_child == '0') {
                var $divbtn =  TButtonGroup();
                $div.append($divbtn);
            } else {
                TOpener($li);
            }
            if (isParent) {
                $li.append(createCategory(v.children, level + 1));
            }
            $elem.append($li);
        });
        return $elem;
    }
    
    function createCourse(arrayItem) {
        var $ul = $('ul#course-listing');
        $ul.empty();
        $.each(arrayItem, function (k, v) {
            var itemObject = {crs_cd: "", crs_nm: "", has_child: 0};
            var temp = $.extend({}, v);
            $.extend(itemObject, temp);
            
            var $li = $('<li>').addClass('list-course-item pr-0');
            $li.data(itemObject);
            var $div = $('<div>').css('overflow', 'auto');
            var $thumb = $('<div>').css('overflow', 'auto').addClass('course-image');
            var $thumbimg = $('<img>').attr('src', v.thumbnail);
            $thumb.append($thumbimg);
            var $span = $('<span>').addClass('txt').append(v.crs_nm).css('margin-right', '5px');
            $div.append($thumb).append('&nbsp;').append($span);
            var $divbtn = $('<div>').addClass('btn-group pull-right');
            var $btnEdit = TButton({classCss: 'btn btn-primary btn-sm btnEdit', text: '<i class="fa fa-edit clickable"></i>'});
            $divbtn.append($btnEdit);
            $div.append($divbtn);
            $li.append($div);
            
            $ul.append($li);
        });
        return $ul;
    }
    
    function createCourseLoadmaps(roadmaps) {
        var $ul = $('ul#roadmap-listing');
        $ul.empty();
        if(roadmaps.length > 0) {
            $('.course-roadmap .img-area').css('display', 'block');
            $("#btnLoadmapDelete").removeAttr('disabled');
            
            $.each(roadmaps, function (k, v) {
                var $li = $('<li>').css('text-align', 'center');
                var $img = $('<img>').attr('src', v.url);
                $li.append($img);
                $ul.append($li);
            });
        } else {
            $('.course-roadmap .img-area').css('display', 'none');
            $("#btnLoadmapDelete").attr('disabled', true);
        }
        return $ul;
    }
    
    function ajax(action, data) {
        var rvalue = false;

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            async: false,
            data: {action: action, data: data},
            success: function(result) {
                rvalue = result;
            },
            error: function(xhr, status, error) {
            }
        });

        return rvalue;
    }
    
    function getCategories() {
        var data = ajax('categories', null);
        if(data.status === 'success') {
            var items = [
                {cmn_cd: 0, cmn_cd_nm: '정규집합', has_child : 1, children: data.categories.offline},
                {cmn_cd: 0, cmn_cd_nm: '정규 e-learning', has_child : 1, children: data.categories.online},
            ]
            createCategory(items, 0);
        } else {
            alert(data.error);
        }
    }
    
    function getCategory(cmn_cd) {
        var data = ajax('category', {cmn_cd: cmn_cd});
        if(data.status === 'success') {
            return data.category;
        } else {
            alert(data.error);
        }
    }
    
    function getCourses(cmn_cd, isonline) {
        var data = ajax('courses', {cmn_cd: cmn_cd, isonline: isonline});
        if(data.status === 'success') {
            $("#btnLoadmap").removeAttr('disabled');
            
            getCourseLoadmaps(cmn_cd);
            createCourse(data.courses);
        } else {
            alert(data.error);
        }
    }
    
    function getCourseLoadmaps(cmn_cd) {
        $("#btnLoadmap").attr('data-cd', cmn_cd);
        $("#btnLoadmapDelete").attr('data-cd', cmn_cd);
        
        var data = ajax('roadmaps', {cmn_cd: cmn_cd});
        if(data.status === 'success') {
            createCourseLoadmaps(data.roadmaps);
        } else {
            alert(data.error);
        }
    }
    
    function deleteCourseLoadmap(cmn_cd) {
        if(confirm("학습 로드맵을 삭제하시겠습니까?")) {
            var data = ajax('deleteroadmaps', {cmn_cd: cmn_cd});
            if(data.status === 'success') {
                $('ul#roadmap-listing').empty();
                $('.course-roadmap .img-area').css('display', 'none');
                $("#btnLoadmapDelete").attr('disabled', true);
            } else {
                alert(data.error);
            }
        }
    }
    
    function validateFile(obj, allow) {
        if(obj.files.length == 0) {
            alert("파일을 선택하세요.");
            return false;
        }
        
        if(0 == obj.files[0].size){
            alert("파일 크기가 0 byte인 파일은 추가할 수 없습니다.");
            return false;
        }
        
        if(maxSize > 0 && obj.files[0].size > maxSize) {
            alert('업로드 파일 최대 크기는 ' + convertReadableFilesize(maxSize) + ' 입니다.');
            
            return false;
        }
        
        var filename = obj.files[0].name;

        if(filename == undefined) {
            return false;
        }
        
        var ext = filename.match(/\.([^\.]+)$/)[0];
        if($.inArray(ext, allow) == -1) {
            alert(allow.join(', ') + " 파일을 업로드할 수 있습니다.");
            return false;
        }
        
        return true;
    }
    
    function validateGuides(obj) {
        var validated = validateFile(obj, ['.jpg', '.png', '.zip']);

        if(!validated) {
            $("#guides_filename").val('');
            $(obj).val('');

            return false;
        }
            
        $("#guides_filename").val(obj.files[0].name);
        
        return true;
    }
    
    function validateLoadmaps(obj) {
        var validated = validateFile(obj, ['.jpg', '.png', '.zip']);

        if(!validated) {
            $("#roadmaps_filename").val('');
            $(obj).val('');

            return false;
        }
            
        $("#roadmaps_filename").val(obj.files[0].name);
        
        return true;
    }
    
    function convertReadableFilesize(bytes) {
        var thresh = 1024;
        
        if(Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        
        var units = ['kB','MB','GB','TB','PB','EB','ZB','YB'];
        var u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while(Math.abs(bytes) >= thresh && u < units.length - 1);
        
        return bytes.toFixed(1)+' '+units[u];
    }
    
    return {
        init: function(maxsize, cmn_cd) {
            maxSize = maxsize;
            
            if(cmn_cd == null && typeof(sessionStorage) != 'undefined') {
                cmn_cd = sessionStorage.getItem('cmn_cd');
            }
            
            getCategories();
            if(cmn_cd != null) {
                var category = getCategory(cmn_cd);
                getCourses(cmn_cd, category.isonline);
                
                var result = ajax('category', {cmn_cd: cmn_cd});
                if(result.status == 'success') {
                    $("small#course-name").html(result.category.cmn_cd_nm);
                }
            }
            
            $(document).on('click', '.btnInfo', function (e) {
                e.preventDefault();
                var selected = $(this).closest('li');
                
                if(typeof(sessionStorage) != 'undefined') {
                    sessionStorage.setItem('cmn_cd', selected.data().cmn_cd);
                }
                
                $("small#course-name").html(selected.data().cmn_cd_nm);
                
                getCourses(selected.data().cmn_cd, selected.data().isonline);
            });
            
            $(document).on('click', '.btnEdit', function (e) {
                e.preventDefault();
                var selected = $(this).closest('li');
                document.location.href = 'course.php?crs_cd=' + selected.data().crs_cd;
            });
            
            // 과정 안내 업로드 
            $('#submit_guides').click(function() {
                $('#fileupload_guides').submit();
            });
            $('#fileupload_guides').submit(function() {
                return validateGuides($('input[type=file]#guides_file')[0]);
            });
            $('#guides_file').change(function() {
                validateGuides(this);
            });
            $('#modal-upload-guides').on('show.bs.modal', function (e) {
                $("#guides_filename").val('');
                $("input[type=file]#guides_file").val('');
            });
            
            // 학습 로드맵 업로드 
            $('#submit_roadmaps').click(function() {
                $('#fileupload_roadmaps').submit();
            });
            $('#fileupload_roadmaps').submit(function() {
                return validateLoadmaps($('input[type=file]#roadmaps_file')[0]);
            });
            $('#roadmaps_file').change(function() {
                validateLoadmaps(this);
            });
            $('#modal-upload-roadmaps').on('show.bs.modal', function (e) {
                $("input[type=hidden][name=cmn_cd]").val($("#btnLoadmap").attr('data-cd'));
                $("#roadmaps_filename").val('');
                $("input[type=file]#roadmaps_file").val('');
            });
            
            // 학습 로드맵 삭제 이벤트
            $("#btnLoadmapDelete").click(function() {
                var cmn_cd = $(this).attr('data-cd');
                deleteCourseLoadmap(cmn_cd);
            });
        }
    }
}