<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/curriculum/lib.php';
require_once $CFG->dirroot . '/lib/gdlib.php';

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('curriculum');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}

$crs_cd = required_param('crs_cd', PARAM_INT);

$thumbnail = $_FILES['attach'];


$fs = get_file_storage();
$filename = $thumbnail['name'];
$filepath = $thumbnail['tmp_name'];
if(file_exists($filepath)) {
    $fs->delete_area_files($context->id, 'local_curriculum', 'thumbnail', $crs_cd);
    
    //$newfile = resize_image($filepath, 318, 194, true);
    
    $filerecord = array('component' => 'local_curriculum', 'filearea' => 'thumbnail',
        'contextid' => $context->id, 'itemid' => $crs_cd, 'filepath' => '/',
        'filename' => $filename, 'userid' => $USER->id, 'sortorder' => 1);
    $stored_file = $fs->create_file_from_pathname($filerecord, $filepath);
    //$stored_file = $fs->create_file_from_string($filerecord, $newfile);
    
    unlink($filepath);
}

redirect(new moodle_url('/local/management/curriculum/course.php', array('crs_cd' => $crs_cd)));