<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/curriculum/lib.php';
require_once $CFG->dirroot . '/lib/gdlib.php';

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('curriculum');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}

$crs_cd = required_param('crs_cd', PARAM_INT);

// 업로드 가능한 이미지 파일
$imgfiles = array('jpg', 'png');

$guidefile = $_FILES['attach'];

$ext = pathinfo($guidefile['name'], PATHINFO_EXTENSION);

$guides = array();
if(strtolower($ext) === 'zip') {
    $zippacker = get_file_packer('application/zip');
    $pathtoextract = $CFG->dataroot . '/temp/curriculum/guides/' . $crs_cd;
    
    $files = $zippacker->extract_to_pathname($guidefile['tmp_name'], $pathtoextract);
    foreach($files as $filename=>$success) {
        $filepath = $pathtoextract . '/' . $filename;
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if(!is_dir($filepath) && in_array(strtolower($ext), $imgfiles)) {
            $guide = new stdClass();
            $guide->filename = $filename;
            $guide->filepath = $filepath;
            $guides[] = $guide;
        }
    }
    unlink($guidefile['tmp_name']);
    
    // 파일 이름순으로 정렬
    usort($guides, 'local_curriculum_sort_files');
} else {
    $guide = new stdClass();
    $guide->filename = $guidefile['name'];
    $guide->filepath = $guidefile['tmp_name'];
    $guides[] = $guide;
}

$fs = get_file_storage();
$fs->delete_area_files($context->id, 'local_curriculum', 'guide', $crs_cd);

$sortorder = 1;
foreach($guides as $guide) {
    $filename = $guide->filename;
    $filepath = $guide->filepath;
    
    if(file_exists($filepath)) {

        $filerecord = array('component' => 'local_curriculum', 'filearea' => 'guide',
            'contextid' => $context->id, 'itemid' => $crs_cd, 'filepath' => '/',
            'filename' => $filename, 'userid' => $USER->id, 'sortorder' => $sortorder);
        $stored_file = $fs->create_file_from_pathname($filerecord, $filepath);

        $sortorder += 1;
        
        unlink($filepath);
    }
}

if($crs_cd == 0) {
    redirect(new moodle_url('/local/management/curriculum/index.php'));
} else {
    redirect(new moodle_url('/local/management/curriculum/course.php', array('crs_cd' => $crs_cd)));
}