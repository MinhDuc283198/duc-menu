<?php 
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once $CFG->dirroot . '/local/curriculum/lib.php';

$pagesettings = array(
    'title' => '과정 관리',
    'heading' => '과정관리',
    'subheading' => '',
    'menu' => 'curriculum',
    'js' => array('/local/management/curriculum/index.js'),
    'css' => array(),
    'nav_bar'=> array()
);

$context = context_system::instance();
$guides = local_curriculum_get_course_guides($context, 0);

include_once('../header.php');
?> 

<!---------- content start ---------->
<section class="content">
    <div class="row">
        <section class="col-lg-6">
            <div class="box box-danger course-guide collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">정규 집합과정 안내자료</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="img-area">
                        <ul><?php
                        foreach($guides as $guide) {
                            echo '<li style="text-align:center"><img src="'.$guide->url.'"></li>';
                        }
                        ?></ul>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnGuide" class="btn btn-primary" data-toggle="modal" data-target="#modal-upload-guides"><i class="fa fa-upload"></i> <?php echo get_string('upload', 'local_curriculum'); ?></button>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">과정 분류</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="category-listing" class="list-category">
                    </ul>
                </div>
            </div>
        </section>
        <section class="col-lg-6">
            <div class="box box-info course-roadmap">
                <div class="box-header with-border">
                    <h3 class="box-title">학습 로드맵 <small id="course-name"></small></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="img-area">
                        <ul id="roadmap-listing"></ul>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnLoadmap" class="btn btn-primary" data-toggle="modal" data-target="#modal-upload-roadmaps" data-cd="0" disabled="disabled"><i class="fa fa-upload"></i> <?php echo get_string('upload', 'local_curriculum'); ?></button>
                    <button type="button" id="btnLoadmapDelete" class="btn btn-warning" data-cd="0" disabled="disabled"><i class="fa fa-trash"></i> <?php echo get_string('delete', 'local_curriculum'); ?></button>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">과정 <small id="course-name"></small></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="course-listing" class="list-course">
                    </ul>
                </div>
            </div>
        </section>
    </div>
</section>
<!---------- content end ---------->

<div class="modal modal-default fade modal-dialog-centered" id="modal-upload-guides">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">정규 집합과정 안내자료 업로드</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-danger col-md-12">
                    <div class="upload-guide">
                        .jpg, .png, .zip 파일을 업로드하세요.
                    </div>
                    <form action="upload-guides.php" method="post" enctype="multipart/form-data" id="fileupload_guides">
                        <input type="hidden" name="crs_cd" value="0" />
                        <div class="input-group">
                            <input type="text" id="guides_filename" class="form-control" readonly="readonly" />
                            <input type="file" name="attach" accept=".jpg,.png,.zip" id="guides_file" style="display:none;" />
                            <div class="input-group-btn">
                                <label for="guides_file" class="btn btn-primary select-file">파일 선택</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
          <button type="button" class="btn btn-primary" id="submit_guides">업로드</button>
        </div>
      </div>
    </div>
</div>
<?php 
include_once '../footer.php';
?>
<div class="modal modal-default fade modal-dialog-centered" id="modal-upload-roadmaps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">학습 로드맵</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="box-danger col-md-12">
                    <div class="upload-guide">
                        .jpg, .png, .zip 파일을 업로드하세요.
                    </div>
                    <form action="upload-roadmaps.php" method="post" enctype="multipart/form-data" id="fileupload_roadmaps">
                        <input type="hidden" name="cmn_cd" value="0" />
                        <div class="input-group">
                            <input type="text" id="roadmaps_filename" class="form-control" readonly="readonly" />
                            <input type="file" name="attach" accept=".jpg,.png,.zip" id="roadmaps_file" style="display:none;" />
                            <div class="input-group-btn">
                                <label for="roadmaps_file" class="btn btn-primary select-file">파일 선택</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
          <button type="button" class="btn btn-primary" id="submit_roadmaps">업로드</button>
        </div>
      </div>
    </div>
</div>
