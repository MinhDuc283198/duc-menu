<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => get_string('popupmanagement', 'local_management'),
    'heading' => get_string('popupmanagement', 'local_management'),
    'subheading' => '',
    'menu' => 'popupM',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

$id = optional_param('id', 0, PARAM_INT);

$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$data = $DB->get_record('popup', array('id' => $id));
include_once($CFG->dirroot . '/local/management/header.php');
?>
<script  LANGUAGE="JavaScript">
    function deletepopup(id) {
        if (confirm("<?php echo get_string('suredeleteselectedcontents', 'local_popup'); ?>")) {
            document.deleteform.id.value = id;
            document.deleteform.action = '<?php echo($CFG->wwwroot); ?>/local/popup/delete.php';
            document.deleteform.submit();
        }
    }
</script>
<section class="content">
    <div class="box box-body">
        <div class="frm_popup">
            <table cellspadding="0" cellspacing="0" class="detail">
                <tr>
                    <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php echo $data->title; ?></td>
                </tr>
                <tr>
                    <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('post_period', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php echo date("Y-m-d", $data->timeavailable); ?> ~ <?php echo date("Y-m-d", $data->timedue); ?></td>
                </tr>
                <tr>
                    <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('post_visible', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <?php
                        $now = date("Ymd");
                        if ($now >= date("Ymd", $data->timeavailable) && $now <= date("Ymd", $data->timedue)) {
                            echo get_string('active', 'local_lmsdata');
                        } else if ($now < date("Ymd", $data->timeavailable)) {
                            echo get_string('notaperiod', 'local_management');
                        } else {
                            echo get_string('status3', 'local_lmsdata');
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('contents', 'local_lmsdata'); ?></td>
                    <td class="field_value" ><?php echo $data->description; ?></td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('xsize', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php echo $data->popupwidth; ?> px</td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('ysize', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php echo $data->popupheight; ?> px</td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('xposition', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php echo $data->popupx; ?> px</td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('yposition', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php echo $data->popupy; ?> px</td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('banner:site', 'local_lmsdata'); ?></td>
                    <td class="field_value">
                        <?php
                        if ($data->site == 0) {
                            echo get_string('e-learning', 'local_lmsdata');
                        } else if ($data->site == 1) {
                            echo get_string('job', 'local_lmsdata') . "(" . get_string('jobs_per', 'local_management') . ")";
                        } else if ($data->site == 2) {
                            echo get_string('job', 'local_lmsdata') . "(" . get_string('jobs_com', 'local_management') . ")";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="field_title"><?php echo get_string('viewscroll', 'local_lmsdata'); ?></td>
                    <td class="field_value"><?php
                        if ($data->availablescroll == 1) {
                            echo "표시";
                        } else {
                            echo "표시안함";
                        }
                        ?></td>
                </tr>
            </table>
            <div id="btn_area">
                <input type="button" id="popup_write"  class="blue_btn" value="<?php echo get_string('updates', 'local_lmsdata'); ?>" style="float:right" />
                <input type="button" id="popup_list" class="normal_btn" value="<?php echo get_string('banner:list', 'local_lmsdata'); ?>" style="float:left;" />
            </div><!--Btn Area End-->
        </div><!--Form Popup End-->
    </div> <!--Contents End-->
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#popup_list').click(function () {
            location.href = "./popup.php";
        });
        $('#popup_write').click(function () {
            location.href = '<?php echo($CFG->wwwroot); ?>/local/management/popupM/popup_write.php?mode=edit&id=<?php echo($data->id); ?>';
                    });
                });
</script>
