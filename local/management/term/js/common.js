$(document).ready(function () {
    $("[data-toggle='tab'] a").click(function(){
        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");
        var target = $(this).attr("href");
        $(target).removeClass("hide").addClass("show").addClass("active");
        $(target).siblings().removeClass("show").removeClass("active").addClass("hide");
        return false;
    });
    
});