<?php
require_once(__DIR__ . '/../../../config.php');
$context = context_system::instance();
require_login($context);

$id = optional_param('id', 0, PARAM_INT);
$fbstatus = optional_param('fbstatus', 0, PARAM_INT);
$category = optional_param('category', 0, PARAM_INT);
$title = optional_param('title',"", PARAM_RAW);
$contents = optional_param('contents',"", PARAM_RAW);
$title_en = optional_param('title_en',"", PARAM_RAW);
$contents_en = optional_param('contents_en',"", PARAM_RAW);
$title_vi = optional_param('title_vi',"", PARAM_RAW);
$contents_vi = optional_param('contents_vi',"", PARAM_RAW);
$delete = optional_param('delete',"", PARAM_RAW);
$text = optional_param('text', '', PARAM_RAW);
$template = new stdClass();
$template->userid = $USER->id;
$template->title = $title;
$template->content = $contents;
$template->titleen = $title_en;
$template->contenten = $contents_en;
$template->titlevi = $title_vi;
$template->contentvi = $contents_vi;
$template->category = $category;
$template->fbstatus = $fbstatus; // 0이면 개인정보 처리방침 , 1이면 이용약관


if($id == 0){
    $DB->insert_record('notice_board', $template);
}else{
    if(!empty($delete)){
        $template->delflag = '1';
    }
    $template->id = $id;
    $template->edit_userid = $USER->id;
    $template->timeedited = time();
    $DB -> update_record('notice_board',$template);   
}

redirect("index.php?category=".$category.'&fbstatus='.$fbstatus);