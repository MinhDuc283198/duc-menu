<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
$fbstatus = optional_param('fbstatus', 0, PARAM_INT);
switch ($fbstatus){
    case 0 :
        $head =get_string('service1','local_management');
        break;
    case 1 :
        $head =get_string('service2','local_management');
        break;
    case 2 :
        $head = get_string('service3','local_management');
        break;
}
$pagesettings = array(
    'title' => $head.' '.get_string('boardset','local_management'),
    'heading' => $head.' '.get_string('boardset','local_management'),
    'subheading' => '',
    'menu' => 'term'.$fbstatus,
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');
$category = optional_param('category', 3, PARAM_INT);
$sql = "select * from {notice_board} where delflag=0 and fbstatus = $fbstatus and category = $category order by id desc";
$contents = $DB->get_records_sql($sql);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body trem">
                <ul class="nav nav-tabs term-tab">
                    <ul class="mk-c-tab tab-event">
                        <li <?php if($category ==3){echo 'class="on"';} ?>><a href="/local/management/term/index.php?fbstatus=<?php echo $fbstatus;?>&category=3"><?php echo get_string('e-learning','local_management');?></a></li>
                        <li <?php if($category ==1){echo 'class="on"';} ?>><a href="/local/management/term/index.php?fbstatus=<?php echo $fbstatus;?>&category=1"><?php echo get_string('jobs','local_management');?> - <?php echo get_string('jobs_per','local_management');?></a></li>
                        <li <?php if($category == 2){echo 'class="on"';} ?>><a href="/local/management/term/index.php?fbstatus=<?php echo $fbstatus;?>&category=2"><?php echo get_string('jobs','local_management');?> - <?php echo get_string('jobs_com','local_management');?></a></li>
                    </ul>
                    <?php
                        $i = 0;
                        foreach($contents as $content){
                            $class = "";
                            if($i==0){
                                $class = "active";
                            }
                            switch (current_language()){
                                case 'ko' :
                                    $title = $content->title;
                                    break;
                                case 'en' :
                                    $title = $content->titleen;
                                    break;
                                case 'vi' :
                                    $title = $content->titlevi;
                                    break;
                            }
                    ?>
                    <li  class="<?php echo $class?>"  data-toggle="tab"><a href="#data<?php echo $content->id?>"><?php echo $title;?></a></li>
                    <?php
                        $i++;
                        }
                    ?>
                    <a href="./write.php?category=<?php echo $category;?>&fbstatus=<?php echo $fbstatus; ?>" class="btn btn-danger pull-right addpop"><?php echo get_string('write','local_management');?></a>
                </ul>

                <div class="term-txt">
                    <?php
                         $i = 0;
                        foreach($contents as $content2){
                            $class = "hide";
                            if($i==0){
                                $class = " show active";
                            }
                               switch (current_language()){
                                case 'ko' :
                                    $content_g = $content2->content;
                                    break;
                                case 'en' :
                                    $content_g = $content2->contenten;
                                    break;
                                case 'vi' :
                                    $content_g = $content2->contentvi;
                                    break;
                            }
                    ?>
                    <div class="tab-pane <?php echo $class?>" id="data<?php echo $content2->id?>">
                        <?php echo $content_g; ?>
                        <a href="write.php?id=<?php echo $content2->id?>&fbstatus=<?php echo $fbstatus; ?>" class="btn btn-danger pull-right addpop"><?php echo get_string('correct','local_management');?></a>
                    </div>
                    
                   
                    <?php
                    $i++;
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>
<script type="text/javascript">

</script>

