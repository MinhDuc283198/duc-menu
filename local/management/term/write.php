<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once("$CFG->libdir/editorlib.php");
$fbstatus = optional_param('fbstatus', 0, PARAM_INT); 
switch ($fbstatus){
    case 0 :
        $head =get_string('service1','local_management');
        break;
    case 1 :
        $head =get_string('service2','local_management');
        break;
    case 2 :
        $head = get_string('service3','local_management');
        break;
}
$pagesettings = array(
    'title' => $head.' '.get_string('write','local_management'),
    'heading' => $head.' '.get_string('write','local_management'),
    'subheading' => '',
    'menu' => 'term'.$fbstatus,
    'js' => array(
        // $CFG->wwwroot . '/chamktu/js/ckeditor-4.3/ckeditor.js',
        // $CFG->wwwroot . '/chamktu/js/ckfinder-2.4/ckfinder.js',
        $CFG->wwwroot . '/local/management/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot . '/local/management/js/ckfinder-2.4/ckfinder.js'
    ),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');
$id = optional_param('id', 0, PARAM_INT);
$category = optional_param('category', 1, PARAM_INT); //1 개인회원 2 기업회원
$content = $DB->get_record('notice_board', array('id' => $id));

if($content != null){
    $category = $content->category;
}
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <form id="form_encourage" class="form-horizontal" action="write_submit.php" method="post" enctype="multipart/form-data" onsubmit="return encourage_check_form();">
                <input type="hidden" class="form-control" name="id" value="<?php echo $content->id?>"/>
                <input type="hidden" class="form-control" name="fbstatus" value="<?php echo $fbstatus ?>"/>
                <div class="box box-body">
                     <table cellpadding="0" cellspacing="0" class="detail">
                         <tbody>
                             <tr>
                                 <td class="field_title" colspan="2"><?php echo get_string('division','local_management');?></td>
                                 <td class="field_value" colspan="3"> 
                                    <label><input type="checkbox" name="category" value="3" <?php if($category ==3){ echo 'checked="checked"';} ?>><?php echo get_string('e-learning','local_management');?></label>
                                    <label><input type="checkbox" name="category" value="1" <?php if($category ==1){ echo 'checked="checked"';} ?>><?php echo get_string('jobs','local_management');?> - <?php echo get_string('jobs_per','local_management');?></label>
                                    <label><input type="checkbox" name="category" value="2" <?php if($category ==2){ echo 'checked="checked"';} ?>><?php echo get_string('jobs','local_management');?> - <?php echo get_string('jobs_com','local_management');?></label>
                                </td>
                             </tr>
                             <tr>
                                 <td class="field_title" rowspan="2"><?php echo get_string('vi','local_management');?></td>
                                 <td class="field_title" ><?php echo get_string('title','local_management');?></td>
                                 <td class="field_value" colspan="3">
                                    <input type="text" class="form-control" name="title_vi" value="<?php echo $content->titlevi?>"/>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="field_title"><?php echo get_string('boardcontent','local_management');?></td>
                                 <td class="field_value" colspan="3">  
                                     <input type="hidden" name="val2" disabled value="<?php echo htmlspecialchars($content->contentvi,ENT_QUOTES); ?>" >
                                     <textarea class="ckeditor" id="editor1" rows="7" name="contents_vi" title="<?php echo get_string('boardcontent','local_management');?>" class="form-control" rows="5"><?php echo $content->contentvi; ?></textarea>
                                 </td>
                             </tr>
                              <tr>
                                 <td class="field_title" rowspan="2"><?php echo get_string('ko','local_management');?></td>
                                 <td class="field_title" ><?php echo get_string('title','local_management');?></td>
                                 <td class="field_value" colspan="3">
                                    <input type="text" class="form-control" name="title" value="<?php echo $content->title?>"/>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="field_title"><?php echo get_string('boardcontent','local_management');?></td>
                                 <td class="field_value" colspan="3">  
                                     <input type="hidden" name="val2" disabled value="<?php echo htmlspecialchars($content->content,ENT_QUOTES); ?>" >
                                     <textarea class="ckeditor" id="editor2" rows="7" name="contents" title="<?php echo get_string('boardcontent','local_management');?>" class="form-control" rows="5"><?php echo $content->content; ?></textarea>
                                 </td>
                             </tr>
                              <tr>
                                 <td class="field_title" rowspan="2"><?php echo get_string('en','local_management');?></td>
                                 <td class="field_title" ><?php echo get_string('title','local_management');?></td>
                                 <td class="field_value" colspan="3">
                                    <input type="text" class="form-control" name="title_en" value="<?php echo $content->titleen?>"/>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="field_title"><?php echo get_string('boardcontent','local_management');?></td>
                                 <td class="field_value" colspan="3">  
                                     <input type="hidden" name="val2" disabled value="<?php echo htmlspecialchars($content->contenten,ENT_QUOTES); ?>" >
                                     <textarea class="ckeditor" id="editor3" rows="7" name="contents_en" title="<?php echo get_string('boardcontent','local_management');?>" class="form-control" rows="5"><?php echo $content->contenten; ?></textarea>
                                 </td>
                             </tr>
                         </tbody> 
                     </table>
                    
                        <div class="btns text-right">
                         <?php if(!empty($content->id)){?>
                         <a href="write_submit.php?id=<?php echo $content->id?>&delete='delete" class="btn blue_btn"><?php echo get_string('del','local_management');?></a>
                         <?php }else{?>
                         <input id="goto-list" type="button" class="btn" value="<?php echo get_string('del','local_management');?>" />
                         <?php }?>
                         <button class="btn blue_btn"  id="submit_encourage" ><?php echo get_string('save','local_management');?></button> 
                     </div>

                </div>

                
            </form>
        </div>
    </div>
</section>
<div id="encourage_message_value2" style="display: none;"><?php echo $template->message;?></div>

<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script type="text/javascript">

    // ckeditor 툴바 설정
    $('.ckeditor').each(function(e){
        var editor = CKEDITOR.replace(this.id, {
            language: '<?php if(current_language()!='vi'){  echo current_language(); } else { echo 'vn'; } ?>',
            filebrowserBrowseUrl: '../js/ckfinder-2.4/ckfinder.html',
            filebrowserImageBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: '../js/ckfinder-2.4/ckfinder.html?type=Flash',
            filebrowserUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '../js/ckfinder-2.4/core/connector/php/connector.php?command=QuickUpload&type=Flash',
            enterMode: CKEDITOR.ENTER_BR,
            allowedContent: true
        });
        
        CKFinder.setupCKEditor(editor, '../');
    });
  

    $(document).ready(function() {
        $('input[type=button]#goto-list').click(function() {
            document.location.href = 'index.php';
        });
        
         $('input[type=button]#submit_encourage').click(function() {
            $('#form_encourage').submit();
        });



    });

 $(function() {
       $('input[name=category]').click((function (event) {
            if ($(this).prop('checked')) {
                $('input[name=category]').prop('checked', false);
                $(this).prop('checked', true);
            }
       }));

    });


</script>

