<?php
//require_once($CFG->dirroot . '/local/signup/loginapi_lib.php');
global $USER, $DB;

$context = context_system::instance();
//require_login($context);

// 사용자 메뉴 가져오기
$menus_arr = local_management_menu_get_user_menus();
$menus = local_management_menu_convert_class($menus_arr);
// /local/management/로 들어온 경우
// 첫번째 메뉴 페이지로 리다이렉트 한다.
if(empty($pagesettings['menu']) || $pagesettings['menu'] == 'home') {
    if(!empty($menus)) {
        $menu = local_management_leaf_child_menu($menus[0]);
        $href = local_management_make_absolute($menu->href);
        header('Location: '.$href);
        exit();
    }
}

// 메뉴 하이라이트
$menu = local_management_menu_set_active($menus, $pagesettings['menu']);

// DLP 홈으로 리다이렉트 되지 않도록 하기 위해 여기서 "DLP 홈 바로가기" 메뉴를 추가한다.
$menu_home = new \local_management\menu(
    array(
        'txt_vi' => 'Trang chủ mk job',
        'txt_ko' => 'mk jobs 바로가기',
        'txt_en' => 'mk jobs homepage',
        'text' => 'mk jobs 바로가기',
        'shortname' => 'mkjobs',
        'icon' => 'fa fa-home',
        'href' => $CFG->job_url.'/local/job',
        'target' => '_blank'));
array_unshift($menus, $menu_home);
$menu_home = new \local_management\menu(
    array(
        'txt_vi' => 'Trang chủ mk',
        'txt_ko' => 'mk 홈 바로가기',
        'txt_en' => 'mk homepage',
        'text' => 'mk 홈 바로가기',
        'shortname' => 'mkhome',
        'icon' => 'fa fa-home',
        'href' => $CFG->wwwroot,
        'target' => '_blank'));
array_unshift($menus, $menu_home);

$user_group=$DB->get_record('lmsdata_user',array('userid'=>$USER->id));
if($user_group->usergroup!='pr'&&$user_group->usergroup!='bp') {
    require_capability('moodle/course:view', $context);
}

// 권한 체크
// 사용자 메뉴에서 하이라이트한 메뉴가 없으면 권한이 없는 것임.
if(!$menu) {
    //include_once $CFG->dirroot.'/local/dlp/error.php';
    echo "<script>location.href='/'</script>";
    die;
//    header('HTTP/1.0 403 Forbidden');
//    exit();
    //$menu = $DB->get_record('management_menu', array('shortname' => $pagesettings['menu']));
    //print_error('nopermissions', 'error', '', $menu->title);ß
}else{
    //checksecondcertification();
}

$paths = local_management_menu_get_path($menu->id);

$breadcrumbs = array();
$activemenuid = 0;
// 메뉴 경로를 nav에 추가
foreach($paths as $path) {
    $nav = new stdClass();
    // 최하위 메뉴에만 링크를 건다
    if($path->isleaf) {
        $nav->url = $path->url;
    }
    $nav->text = $path->text;

    if($path->shortname == $pagesettings['menu']) {
        $activemenuid = $path->id;
    }

    $breadcrumbs[$path->id] = $nav;
}
// 추가적으로 선언된 경로
if(!empty($pagesettings['nav_bar'])) {
    foreach($pagesettings['nav_bar'] as $nav) {
        $nav = (object) $nav;
        if($nav->active) {
            $activemenuid = 0;
            $nav->url = null;
        }
        $breadcrumbs[] = $nav;
    }
}

if($activemenuid) {
    $breadcrumbs[$activemenuid]->active = 1;
    // active 메뉴의 url은 지우자...
    $breadcrumbs[$activemenuid]->url = null;
}


$userpicture = new user_picture($USER);
$url = $userpicture->get_url($PAGE);
$userpicture = $url->out(false);
$userpicture = str_replace("/f2?rev", "/f1?rev", $userpicture);

$fullname = fullname($USER);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $pagesettings['title']; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- noto sans font -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:300,400,500,700|Noto+Sans+KR:300,400,500,700|Noto+Sans+SC:300,400,500,700&display=swap&subset=korean,vietnamese" rel="stylesheet"/>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/plugins/iCheck/all.css">

    <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/local/management/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/local/management/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/local/management/colorpicker-master/jquery.colorpicker.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <?php
    $styles = local_management_load_css();
    foreach($styles as $css) {
        echo '<link rel="stylesheet" href="'.local_management_make_absolute($css).'">';
    }
    // 추가 선언된 css 파일
    if(isset($pagesettings['css'])) {
        foreach($pagesettings['css'] as $css) {
            echo '<link rel="stylesheet" href="'.local_management_make_absolute($css).'">';
        }
    }
    ?>
    <!-- jQuery 3 -->
    <script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $CFG->wwwroot; ?>/local/management/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/local/management/js/common.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo $CFG->wwwroot; ?>/local/management/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <!--<span class="logo-mini"><b>LG</b>A</span>-->
            <!-- logo for regular state and mobile devices -->
            <!--<span class="logo-lg"><b>LG</b>Academy</span>-->
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $userpicture; ?>" class="user-image" alt="<?php echo $fullname; ?>">
                            <span class="hidden-xs"><?php echo $fullname; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Menu Body -->
                            <!--<li class="user-body">
                <div class="row">
                  <div class="text-center">
                    <a href="<?php echo $CFG->wwwroot.'/local/mypage/index.php'; ?>">개인정보</a>
                  </div>
                </div>
              </li>--><?php
                            $langs = array('vn' => 'Vietnamese ‎(vn)', 'en' => 'English ‎(en)‎', 'ko' => '한국어 ‎(ko)');
                            foreach ($langs as $lang => $val) {
                                $currlang = current_language();
                                $on = '';
                                if ($lang == 'vn') {
                                    $lang_txt = 'vi';
                                } else {
                                    $lang_txt = $lang;
                                }
                                ?>
                                <li class="user-body <?php echo $currlang == $lang_txt ?  'active' : '' ?>">
                                    <div class="row">
                                        <div class="text-center">
                                            <a href="<?php echo $langurl . "?lang=$lang_txt"; ?>"><?php echo $on ?><?php echo strtoupper($lang) ?></a>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            <li class="user-body">
                                <div class="row">
                                    <div class="text-center">
                                        <a href="<?php echo $CFG->wwwroot.'/login/logout.php?sesskey='. sesskey(); ?>">로그아웃</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <?php
                foreach($menus as $menu) {
                    echo $menu->print_menu();
                }
                ?>

                <li id="menu_manage board">
                    <a href="/local/management/configs/index.php" target="_self" title="">
                        <i class="empty"></i> <span>Site Config</span></a></li>
                <li id="menu_manage board">
                    <a href="/local/management/users/customer_form_register.php" target="_self" title="">
                        <i class="empty"></i> <span>Customer Register</span></a>
                </li>


            </ul>
            </ul class="sidebar-menu">
            <ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php
                echo $pagesettings['heading'];
                if(!empty($pagesettings['subheading'])) {
                    echo '<small>'.$pagesettings['subheading'].'</small>';
                }
                ?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <?php
                foreach($breadcrumbs as $breadcrumb) {
                    $cssclass = '';
                    if($breadcrumb->active) {
                        $cssclass = ' class="active"';
                    }
                    echo '<li'.$cssclass.'>';
                    if(!empty($breadcrumb->url)) {
                        echo '<a href="'.local_management_make_absolute($breadcrumb->url).'">'.$breadcrumb->text.'</a>';
                    } else {
                        echo $breadcrumb->text;
                    }
                    echo '</li>';
                }
                ?>
            </ol>
        </section>