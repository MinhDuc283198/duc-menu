<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$pagesettings = array(
    'title' => 'Configs',
    'heading' => 'Configs',
    'subheading' => '',
    'menu' => 'menuM',
    'js' => array(),
    'css' => array(),
    'nav_bar'=> array()
);

include_once($CFG->dirroot.'/local/management/header.php');

?>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Config</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="box-body col-md-6">
                        <form id="frmEdit" role="form">
                            <div class="form-group">
                                <label for="text">Key</label>
                                <div class="input-group">
                                    <input type="text" class="form-control item-menu" name="javascript_footer_key" id="javascript_footer_key" placeholder="key" value="javascript_footer" disabled>
                                    <em class="text-danger key-javascript_footer" style="display: none">This field is required</em>
                                </div>
                                <input type="hidden" name="icon" class="item-menu">
                            </div>
                            <div class="form-group">
                                <label for="href">Value</label>
                                <textarea name="value" class="form-control item-menu" id="javascript_footer_value" cols="30" rows="10"><?php echo get_visang_config('javascript_footer') ?></textarea>
                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-success configUpdate" data-submit="javascript_footer"><i class="fa fa-plus"></i> Cập nhật</button>
                            </div>
                        </form>
                    </div>
                    <div class="box-body col-md-6">
                        <form id="frmEdit" role="form">
                            <div class="form-group">
                                <label for="text">Key</label>
                                <div class="input-group">
                                    <input type="text" class="form-control item-menu" name="javascript_header" id="javascript_header_key" placeholder="key" value="javascript_header" disabled>
                                    <em class="text-danger key-javascript_header" style="display: none">This field is required</em>
                                </div>
                                <input type="hidden" name="icon" class="item-menu">
                            </div>
                            <div class="form-group">
                                <label for="href">Value</label>
                                <textarea name="value" class="form-control item-menu" id="javascript_header_value" cols="30" rows="10"><?php echo get_visang_config('javascript_header') ?></textarea>
                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-success configUpdate" data-submit="javascript_header"><i class="fa fa-plus"></i> Cập nhật</button>
                            </div>
                        </form>
                    </div>
                    <div class="box-body col-md-6">
                        <form id="frmEdit" role="form">
                            <div class="form-group">
                                <label for="text">Key</label>
                                <div class="input-group">
                                    <input type="text" class="form-control item-menu" name="javascript_body" id="javascript_body_key" placeholder="key" value="javascript_body" disabled>
                                    <em class="text-danger key-javascript_body" style="display: none">This field is required</em>
                                </div>
                                <input type="hidden" name="icon" class="item-menu">
                            </div>
                            <div class="form-group">
                                <label for="href">Value</label>
                                <textarea name="javascript_body" class="form-control item-menu" id="javascript_body_value" cols="30" rows="10"><?php echo get_visang_config('javascript_body') ?></textarea>
                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-success configUpdate" data-submit="javascript_body"><i class="fa fa-plus"></i> Cập nhật</button>
                            </div>
                        </form>
                    </div>
                    <div class="box-body col-md-6">
                        <form id="frmEdit" role="form">
                            <div class="form-group">
                                <label for="text">Key</label>
                                <div class="input-group">
                                    <input type="text" class="form-control item-menu" name="mail_discount" id="mail_discount_key" placeholder="key" value="mail_discount" disabled>
                                    <em class="text-danger key-lotte_domain" style="display: none">This field is required</em>
                                </div>
                                <input type="hidden" name="icon" class="item-menu">
                            </div>
                            <div class="form-group">
                                <label for="href">Value</label>
                                <textarea name="mail_discount" class="form-control item-menu" id="mail_discount_value" cols="30" rows="10"><?php echo get_visang_config('mail_discount') ?></textarea>
                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-success configUpdate" data-submit="mail_discount"><i class="fa fa-plus"></i> Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php

include_once($CFG->dirroot.'/local/management/footer.php');

?>


<script>
$('.configUpdate').click(function() {
    let key = $(this).data('submit');
    console.log(key)

    key = $( '#' + key + '_key').val();

    if (! key) {
        $('.key-' + key).show();
        return;
    }

    let value = $( '#' + key + '_value').val();

    $.ajax({
        url: "/local/management/configs/ajax/update.php",
        type: 'post',
        dataType:'json',
        data: {
            key: key,
            value: value
        },
        success: function (response) {
            alert(response.message);
        }
    });
})
</script>
