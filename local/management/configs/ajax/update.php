<?php

define('AJAX_SCRIPT', true);

require_once('../../../../config.php');

$context = context_system::instance();

require_login($context);

if(!admin()) {
    die;
}

$key = $_POST['key'];
$value = $_POST['value'];

set_config($key, $value);

echo json_encode(['status' => 200, 'message' => "Success"]);

return;
