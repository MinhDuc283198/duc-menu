// This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// This function creates an <iframe> (and YouTube player)
// after the API code downloads.
var player;
var area_id;
var area_w;
var area_h;
var content_id;
var starttime;;
var mode;

var positionfrom = 0, positionto = 0, duration = 0, oldposition = 0, saveposition = 0, evented = 0;
//var satiscompleted = $('#satiscompleted').val();
var intervalID;
/*
var video = document.getElementById('contents_viewer');
var controls = document.getElementById('controls');
var playbtn = document.getElementById('playpausebtn');
var seekslider = document.getElementById('seekslider');
var totaltime = document.getElementById('totaltime');
*/
function onYouTubeIframeAPIReady() {
    positionto = 0;
    if(positionto > 1){
        positionfrom = positionto;
    }
    
    if(positionfrom > 0){
        starttime = positionfrom;
    }
    player = new YT.Player(area_id, {
        height: area_h,
        width: area_w,
        videoId: content_id,
        playerVars: {'start': starttime, 'autoplay': 1, 'controls': 1 },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange           
        }
    });
}
function onPlayerReady(event){
    duration = player.getDuration();
}

function onPlayerStateChange(event){
    var state = player.getPlayerState();
    switch(state) {
        case YT.PlayerState.ENDED:
            if(evented == 0){
                playtime_update(positionfrom, positionto, 3, duration, mode);
            }
            window.clearInterval(intervalID);
            evented = 1;
            break;
        case YT.PlayerState.PLAYING:
            positionfrom = Math.ceil(player.getCurrentTime());
            if(positionfrom == 1){ positionfrom = 0; }
            intervalID = setInterval(YouTube_updateCurrentTime, 1000);
            evented = 0;
            break;
        case YT.PlayerState.PAUSED:
            if (positionfrom < positionto) {
                playtime_update(positionfrom, positionto, 1, duration, mode);
            }
            window.clearInterval(intervalID);
            break;
        case YT.PlayerState.BUFFERING:
            break;
        case YT.PlayerState.CUED:
            break;
        default:
            break;
    }
}

function YouTube_Init(id,w,h,code,m){
    area_id=id;
    area_w=w;
    area_h=h;
    content_id=code;
    mode=m;
}

function YouTube_updateCurrentTime() {
    positionto = Math.ceil(player.getCurrentTime());
    
    //진도율 저장 (3분마다 진도율을 저장함.)
    //console.log(positionfrom+'::'+saveposition+'::'+positionto);
    if(evented == 0 && positionfrom < positionto && positionto % (60*3) == 0){
       playtime_update(positionfrom, positionto, 5, duration, mode);
       saveposition = positionto;
       positionfrom = positionto;
    }
    
    oldposition = parseInt(player.getCurrentTime());
//    caption_scroll(oldposition);
}

function roundNumber(number, decimalPlaces) {
    decimalPlaces = (!decimalPlaces ? 2 : decimalPlaces);
    return Math.round(number * Math.pow(10, decimalPlaces)) /
        Math.pow(10, decimalPlaces);
}

function popClosePause(){
    playtime_update_close(positionfrom, positionto, 4, duration, mode);
}
