<?php
require_once '../../../config.php';
require_once($CFG->dirroot . "/lib/filelib.php");
require_once $CFG->dirroot.'/local/repository/lib.php';
require_once $CFG->dirroot.'/local/repository/config.php';

require_login();

$id = required_param('id', PARAM_INT);
$qua = optional_param('qua', 0, PARAM_INT);
$context = context_system::instance();

$PAGE->set_url('/local/management/contentsI/viewer/video_player.php', array('id' => $id));

if ($id) {
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $id))) {
        print_error('lcms contents is incorrect');
    }
    if (!$file = $DB->get_record('lcms_contents_file', array('con_seq' => $id))) {
        print_error('lcms contents file is incorrect');
    }
} else {
    print_error('missingparameter');
}

$auto_play = 0;
$namearray = explode('.',$file->filename);
$realfilename = $namearray[0]. '_hd.'.$namearray[1];
$positionname = strrpos($file->filename, '.');
$realfilename =substr_replace($file->filename, '_hd', $positionname, 0);
$n = count($namearray);
unset($namearray[$n-1]);
$capture = implode('',$namearray).'.png';
//자막파일영역
$subtitles = get_subtitle_list($context->id,$contents->id);

$sturl = get_config('moodle', 'vodurl');
$trcurl = get_config('moodle', 'transcodingurl');
$sturl = $CFG->vodurl;

?>
<!DOCTYPE html>
<html>
    <head>
        <style>
            #hlsjsvod .fp-logo{
                width: 150px;
                padding-left: 10px;
            }
            .flowplayer .fp-header .fp-share { display: none; }
        </style>
        <link rel="stylesheet" href="<?php echo $CFG->wwwroot.'/local/management/contentsI/viewer/flowplayer7/skin/skin.css'?>">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width,  target-densityDpi=device-dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        <script src="<?php echo $CFG->wwwroot.'/local/management/contentsI/viewer/flowplayer7/flowplayer.min.js' ?>"></script>
        <script src="<?php echo $CFG->wwwroot.'/local/management/contentsI/viewer/flowplayer7/hlsjs.min.js' ?>"></script>
        <script type="text/javascript">
            var server = '<?php echo $sturl;?>';
            var storage = '<?php echo $file->filepath; ?>';
            var vodname = '<?php echo $realfilename; ?>'; 
            var imagename = '<?php echo $capture; ?>';
            var ccmark = '';
            var height = '500';
            var transurl = '<?php echo $trcurl;?>';
            
            var hlsurls = server + '/_definst_/' +storage + '/'+vodname+'/playlist.m3u8';
//            var videourl = server + '/' + storage + '/' + vodname;

            window.onload = function () {
                flowplayer("#hlsjsvod", {
                    key: "<?php echo get_config('local_repository','flowplayer_key')?>",
                    splash: false,
                    autoplay: true,
                    embed: false, // setup would need iframe embedding
                    ratio: 5 / 12,    
                    // manual HLS level selection for Drive videos
                    hlsQualities: "drive",

                    // manual VOD quality selection when hlsjs is not supported
                    //defaultQuality: "260p",
                    //qualities: ["160p", "260p", "530p", "800p"],

                    speeds: [0.75,1,1.25,1.5],
                    hlsjs: {recoverNetworkError: true},
                    clip: {
                        sources:[
                            {type: 'application/x-mpegurl', src: hlsurls},
//                            { type: 'video/mp4',  src: videourl,  suffix: 'mp4'  }
                        ]
                    }
                });
            }

        </script>

    </head>
    <body style="margin:0;overflow:hidden;">
    <div id="hlsjsvod" class="is-closeable video_area">

    </div>
    </body>
</html>