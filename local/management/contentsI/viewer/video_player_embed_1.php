<?php
require_once(__DIR__ . '/../../../../config.php');
require_once $CFG->dirroot.'/local/repository/config.php';
require_once '../lib.php';
require_once $CFG->dirroot.'/local/management/lib/contents_lib.php';

$id = required_param('id', PARAM_INT);
$context = context_system::instance();

$PAGE->set_url('/local/repository/viewer/video_player_embed.php', array('id' => $id));

if ($id) {
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $id))) {
        print_error('lcms contents is incorrect');
    }
} else {
    print_error('missingparameter');
}

require_login();

if ($contents->embed_type == 'youtube') { 
    //유튜브 데이터
    $vids = explode('/', $contents->embed_code);
    $vid1 = trim(str_replace("watch?v=", "", $vids[sizeof($vids) - 1])); 
    $vid1s = explode('&', $vid1);
    $cid = trim($vid1s[0]);
}

if ($contents->embed_type == 'vimeo') {

    $embs = explode('/', $contents->embed_code);
    $cid = trim($embs[sizeof($embs) - 1]);
    $embinfo = vimeoinfo($cid);
    $img_nm = $embinfo->thumbnail;
}

$auto_play = 0; //자동재생여부: 1이면 자동재생
$lrn_time = 0; //이어보기여부
//if ($track = $DB->get_record('lcms_track', array('lcms' => $lcms->id, 'userid' => $USER->id))) {
//    $lrn_time = $track->lasttime;
//    $progress = $track->progress;
//}

//자막파일영역
//$subtitles = get_subtitle_list($context->id,$contents->id);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width,   initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" type="text/css" href="jquery-ui-1.10.3.custom.css" />
        <link rel="stylesheet" href="//releases.flowplayer.org/7.0.4/skin/skin.css" />
        <script type="text/javascript" src="jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="jquery-ui-1.10.3.custom.min.js"></script>
        <script src="./flowplayer7/flowplayer.min.js"></script>
        <script src="./flowplayer7/flowplayer.speed-menu.js"></script>
        <?php if ($contents->embed_type == 'youtube') {?>
            <script type="text/javascript" src="player_youtube.js"></script>
        <?php } else if ($contents->embed_type == 'vimeo') { ?>
            <script src="https://player.vimeo.com/api/player.js"></script>
        <?php } ?>
        <script type="text/javascript">
            var height = '500';
            
            $(document).ready(function(){
               videoplayer('<?php echo $contents->embed_type;?>','<?php echo $lrn_time;?>'); 
            });

            function videoplayer(type,lrn_time) {

                //디바이스별 판별
                var _ua = window.navigator.userAgent.toLowerCase();

                var browser = {
                    ipod: /webkit/.test(_ua) && /\(ipod/.test(_ua),
                    ipad: /webkit/.test(_ua) && /\(ipad/.test(_ua),
                    iphone: /webkit/.test(_ua) && /\(iphone/.test(_ua),
                    android: /webkit/.test(_ua) && /android/.test(_ua),
                    msie: /msie/.test(_ua)
                };

                //디바이스별 플레이어 사용
                if (browser.ipod || browser.iphone || browser.ipad || browser.android|| type == 'youtube'|| type == 'vimeo') {
                    html5player(type,lrn_time);
                } else {
                    flashplayer(0,type);
                }
            }

            function flashplayer(auto) {
                $('#mediaplayer').flowplayer({
                    clip: {
                        sources: [
                            { 
                                type: "video/mp4",
                                src:  "<?php echo $contents->embed_code;?>" 
                            }
                        ]
                    },
                    speeds: [0.75,1,1.25,1.5,2.0],
                });
                
//                var ccmark = '';
//                jwplayer('mediaplayer').setup({
//                    'flashplayer': '<?php echo $CFG->wwwroot; ?>/local/repository/viewer/jwplayer/swf/player.swf',
//                    'id': 'mediaplayer',
//                    'menu': 'false',
//                    'width': '100%',
//                    'height': height,
//                    'skin': '<?php echo $CFG->wwwroot; ?>/local/repository/viewer/jwplayer/skins/darkrv5.zip',
//                    'file': 'http://www.youtube.com/watch?v=<?php echo $cid;?>',
//                    'plugins': {
//                        '<?php echo $CFG->wwwroot; ?>/local/repository/viewer/jwplaye/js/textoutput.js': {'text': ccmark},
//                        'captions-2': {
//                            files: '<?php echo $subtitles->path_ko; ?>,<?php echo $subtitles->path_en; ?>',
//                            labels: 'Korean,English',
//                            fontSize: '14',
//                            fontFamily: '나눔고딕, NanumGothic, ng, sans-serif'
//                         }
//                     },
//                     'autostart': auto
//                 });
             }
             
             function html5player(type, lrn_time) {
                var status_num = '<?php echo $status_num;?>';
                if(lrn_time>0){
                    //if(!confirm('<?php echo get_string('continueplease','lcms');?>')) lrn_time = 0;
                }
                if(type=='youtube') {
                    YouTube_Init("ytplayer","100%","400","<?php echo $cid;?>",0);
                } else if(type=='vimeo'){
                    $('#mediaplayer').empty().append('<iframe src="https://player.vimeo.com/video/<?php echo $cid;?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                }
            }
        </script>
    </head>
    <body style="margin:0;overflow:hidden;">
        <div id="player_area">
            <div <?php if($contents->embed_type == 'youtube'){ echo 'id="ytplayer"'; } else { echo 'id="mediaplayer"';}?> ><p>잠시만 기다려주세요. 로딩중입니다.</p></div>
        </div>
    </body>
</html>