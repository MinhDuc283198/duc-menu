<!-- Embed 폼 시작 -->
<script type="text/javascript">

$(document).ready(function () {
    var uploadFile = $('.fileBox input[type=file]');
    uploadFile.on('change', function () {
        if (window.FileReader) {
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.fileName').val(filename);
    });
});
</script>
<?php
require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.php');
if (!empty($_REQUEST)) {
    foreach ($_REQUEST as $key => $val) {
        ${$key} = $val;
    }
}

if(!isset($embed_code)){
    $embed_code = '';
}
 
$fs = get_file_storage();
$files = $fs->get_area_files(1, 'local_repository', 'subtitle', $con_id, 'timemodified', false);
$fileNameString = '';
if(!empty($files)) {
    foreach ($files as $file) {
        $fileNameString = $file->get_filename();
    }
}
?>
<table border="1" class="write_form">
    <caption>교안 등록 영역</caption>
    <input type="hidden" value="tplan">
<!--    <tr>
        <td>
            <input type="text" title="search" name="emb_search" placeholder="검색어를 입력하세요"/>
            <input type="button" value="<?php echo get_string('stats_search1', 'local_lmsdata'); ?>" onclick="search_embed_contents();" class="blue_btn">
        </td>
    </tr>-->
    <!--
    <tr>
        <th>동영상시간</th>
        <td>
            <input type="text" name="con_total_time1" size="5" maxlength="3"/> 
            분
            <input type="text" name="con_total_time2" size="5" maxlength="2"/> 
            초
             * 동영상 검색시 자동으로 들어갑니다.
        </td>
    </tr>
    -->
    <tr>
        <td>
            <input name="tplan" type="file" title="code" size="100" placeholder="" value="" class="tplan_file" accept=".pdf, .ppt, .pptx, .doc, .docx"/> 
        </td>
    </tr>
<!--    <tr>
        <td>
        <div class="fileBox"> custom 파일영역 클래스 
            <label for="captionFile" class="blue_btn">자막등록</label> input file id값과 동일하게 작성 
            <input type="file" title="자막파일" name="script" size="100" value="" id="captionFile" class="uploadBtn" style="display:none;"> input file 아이디 
            <input type="text" class="fileName" readonly="readonly" size="100" value="<?php echo $fileNameString; ?>"> 파일경로 텍스트박스 클래스 
        </div>
        </td>
    </tr>-->
</table>
<!-- Embed 폼 끝 -->

<script>
    $("input[name=tplan]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ['pdf', 'ppt', 'pptx', 'doc', 'docx']) == -1 && $('input[name=uploadfile]').val() != "") {
                    alert('pdf, ppt(pptx), doc(docx) 파일만 업로드 할수 있습니다.');
                    $(this).val("");
                    $('.tpan_file').val('');
                    return;
                }
                //thumbnailimg = $(this).val();
            }
        });
</script>
