<?php         
    require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.php');

?>
<!-- 비디오폼 시작 -->
<input type="hidden" name="con_type" value="video"/>
<?php 

if (!empty($_REQUEST)) {
    foreach ($_REQUEST as $key => $val) {
        ${$key} = $val;
    }
}
$mode = $_REQUEST['mode'];
$transcodingurl = get_config('moodle','transcodingurl');
$mediasave = get_config('moodle','mediasave');
$path = $USER->id.'/'.date('YmdAhis').'r'.  mt_rand(1, 99);
$transcording_src = $transcodingurl.'?id=0&path='.$mediasave.$path.'&userid=2&returnpath='.$CFG->wwwroot.'/local/management/contentsI/html/return_file_data.php';

?>
<table border="1" class="write_form">
    <tr>
        <td>
            <iframe src="<?php echo $transcording_src; ?>" width="100%" id="video_frame" height="306"></iframe>
            <input type="hidden" name="data_dir" value="<?php echo $path ?>">
            <input type="hidden" name="video_file_name" value="video_file_name">
            <input type="hidden" id="video_file_id" name="video_file_id">
            <input type="text" title="file" name="filename" id="video_file_name" style="display: none;" class="w100" readonly="true">
        </td>
    </tr>
</table>
<!--<table border='1' calss='thumbnail_box'>
    <tr>
        <td>
            <div class="fileBox"> custom 파일영역 클래스  
                <label for="captionFile" class="blue_btn" style='float:left'>썸네일</label> input file id값과 동일하게 작성 
                <input type="file" title="썸네일" name="script" size="100" value="" id="captionFile" class="uploadBtn" style="display:none;"> input file 아이디 
                <input type="text" class="fileName" readonly="readonly" size="100" value="<?php echo $fileNameString; ?>"> 파일경로 텍스트박스 클래스 
            </div>
        </td>
    </tr>
</table>-->
<script type="text/javascript">
$(document).ready(function () {
    var write_mode = "<?php echo $mode;?>";
    $('#thumbnail_btn').click(function(){
        if($('input:hidden[name=filename]').val() == '' && write_mode != 'edit'){
            alert("비디오 먼저 등록해주세요");
            return false;
        }
    });
    var uploadFile = $('.fileBox input[type=file]');
    uploadFile.on('change', function () {
        if (window.FileReader) {
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.fileName').val(filename);
    });
});
</script>
