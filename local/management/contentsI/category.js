/**
 * select box 변경시 이벤트
 * @param {type} sel
 * @returns {undefined}
 */
function con_cata1_changed(sel) {
    var selCata2 = $('#content_cata2');
    con_cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = con_get_child_categories($(sel).val());
    if (categories !== null) {
        con_add_select_options(selCata2, categories);
    }
}
/**
 * select box 변경시 이벤트
 * @param {type} sel
 * @returns {undefined}
 */
function con_cata1_changed2(sel) {
    var selCata2 = $('#content_cata2');
    con_cata_clean_select(selCata2);

    if ($(sel).val() == 0) {
        return;
    }
    var categories = con_get_child_categories2($(sel).val());
    selCata2.append(categories);
}

/**
 * select box 초기화
 * @param {type} sel
 * @returns {undefined}
 */
function con_cata_clean_select(sel) {
    $(sel[0].options).each(function () {
        if ($(this).val() != 0) {
            $(this).remove();
        }
        ;
    });
}

/**
 * select box 변경시 이벤트
 * @param {type} pid
 * @returns {unresolved}
 */
function con_get_child_categories(pid) {
    var categories = null;
    $.ajax({
        url: '/local/management/contentsI/child_categories.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
            if (data.status == 'success') {
                categories = data.categories;
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return categories;
}
/**
 * select box 변경시 이벤트
 * @param {type} pid
 * @returns {unresolved}
 */
function con_get_child_categories2(pid) {
    var categories = null;
    $.ajax({
        url: '/local/management/contentsI/child_categories.ajax.php',
        method: 'POST',
        dataType: 'json',
        async: false,
        data: {
            id: pid
        },
        success: function (data) {
//                console.log(data);
            if (data.status == 'success') {
                categories = data.selecttag;
//                console.log(categories);
            } else {
                //alert(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
    return categories;
}

/**
 * select box 변경시 옵션 리스트 출력
 * @param {type} sel
 * @param {type} options
 * @returns {undefined}
 */
function con_add_select_options(sel, options) {
    $.each(options, function (i, option) {
        sel.append($('<option>', {
            value: option.id,
            text: option.name
        }));
    });
}