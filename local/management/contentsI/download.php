<?php

require_once(__DIR__ . '/../../../config.php');

$id = optional_param('id', 0, PARAM_INT); // repository id


$sql = "select "
            . "rep.id , rep.referencecnt , rep.status, "
            . "con.id as con_id,con.con_name,con.con_type,con.con_des,con.update_dt,con.data_dir,con.embed_type,con.embed_code, "
            . "rep_group.name as gname "
            . "from {lcms_repository} rep "
            . "join {lcms_contents} con on con.id= rep.lcmsid "
            . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
            . "where rep.id= :id";

$data = $DB->get_record_sql($sql, array('id' => $id));

$video_file = $DB->get_record('lcms_contents_file',array('con_seq'=>$id));
$file = $CFG->wwwroot.$CFG->vodurl.$data->data_dir.'/'.$video_file->filename; // 파일의 전체 경로

$file_name = $video_file->fileoname; // 저장될 파일 이름

//header('Content-type: application/octet-stream');
//header('Content-Disposition: attachment; filename="' . $file_name . '"');
//header('Content-Transfer-Encoding: binary');
//header('Content-length: ' . filesize($file));
//header('Expires: 0');
//header("Pragma: public");
//
//$fp = fopen($file, 'rb');
//fpassthru($fp);
//fclose($fp);

header('Content-Type: application/force-download');
header('Content-Disposition: attachment; filename="' . $file_name . '"');
readfile($file);
