<?php
/**
 * 콘텐츠 관리 리스트 페이지
 */
require_once(__DIR__ . '/../../../config.php');
require_once("$CFG->libdir/excellib.class.php");
require_once($CFG->dirroot . '/local/management/contentsI/lib.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');


$pagesettings = array(
    'title' => get_string('contents:title','local_management'),
    'heading' => get_string('contents:title','local_management'),
    'subheading' => '',
    'menu' => 'contentsI',
    'js' => array(
        $CFG->wwwroot .'/chamktu/js/ckeditor-4.3/ckeditor.js',
        $CFG->wwwroot .'/chamktu/js/ckfinder-2.4/ckfinder.js',
        $CFG->wwwroot . '/local/management/contentsI/category.js'
        ),
    'css' => array(),
    'nav_bar' => array()
);


// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('index.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

require_once ($CFG->dirroot .  '/local/management/lib/paging.php');
require_once ($CFG->dirroot .  '/local/management/lib/contents_lib.php');

$userid = optional_param('userid', 0, PARAM_INT);
$ctype = optional_param('ctype', "all", PARAM_RAW);
$public = optional_param('public', "all", PARAM_RAW);

$search = optional_param('search', '', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$page_num = optional_param('page_num', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$excel = optional_param('excel', 0, PARAM_INT);

$category1 = optional_param('category1', 0, PARAM_INT); // 부모 카테고리
$category2 = optional_param('category2', 0, PARAM_INT); // 상세 카테고리

$con_type = optional_param_array('con_type',array(), PARAM_RAW);
$share_yn = optional_param_array('share_yn',array(), PARAM_RAW);
$learningstart_str = optional_param('learningstart', '', PARAM_RAW);
$learningend_str = optional_param('learningend', '', PARAM_RAW);

$sql_where = array();
$params = array();
if($learningstart_str){
    $learningstart = str_replace(".", "-", $learningstart_str); 
    $learningstart = strtotime($learningstart);
}
if($learningend_str){
    $learningend = str_replace(".", "-", $learningend_str); 
    $learningend = strtotime($learningend) + 86399;
}

if($learningstart){
    $sql_where[] = " con.reg_dt >= :learningstart ";
    $params['learningstart'] = $learningstart;
}
if($learningend){
    $sql_where[] = " con.reg_dt <= :learningend ";
    $params['learningend'] = $learningend;
}

if($share_yn){
    foreach($share_yn as $iskey => $isval){
        $sql_where_isused[] = " con.share_yn = :isused".$iskey;
        $params['isused'.$iskey] = $isval;
    }
    if (!empty($sql_where_isused)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_isused) . ')';
    } else {
        $sql_where[] = '';
    }
}

if($con_type){
    foreach($con_type as $iskey => $isval){
        $sql_where_type[] = " con.con_type = :con_type".$iskey;
        $params['con_type'.$iskey] = $isval;
    }
    if (!empty($sql_where_type)) {
        $sql_where[] = ' ( ' . implode(' or ', $sql_where_type) . ')';
    } else {
        $sql_where[] = '';
    }
}

$context = context_system::instance();
require_login();

$PAGE->set_context($context);
$PAGE->set_url('/local/management/contentsI/index.php');

$like = '';
if (!empty($search)) {
    $like = "and " . $DB->sql_like('title', ':search', false);
}
$perpage_array = [10,20,30,50];
$share_yn_array = array('Y'=>get_string('used', 'local_management'),'N'=>get_string('notused', 'local_management'));
$con_type_array = array('video'=> get_string('contents_video', 'local_lmsdata'), 'embed'=>get_string('contents_externalcontent', 'local_lmsdata'),'visangurl'=>get_string('contents:visang', 'local_management'), 'tplan'=>get_string('contents:tplan', 'local_management'));
if (!$excel) {
    include_once($CFG->dirroot.'/local/management/header.php');
    $datas = get_contents_list($userid, $category1, $category2,$sql_where,$params, $search, $page_num, $perpage);
    ?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
            <!-- 검색폼 시작 -->
            <form id="frm_content_search" class="search_area">
                <input type="hidden" title="page_num" name="page_num" value="1" />
                <input type="hidden" name="perpage" value="<?php echo $perpage?>" />
                <input type="hidden" name="type" value="<?php echo $type; ?>">
<!--                <select name="perpage" title="page" class="w_160" onchange="this.form.submit();">
                    <?php
                    $nums = array(10, 20, 30, 50);
                    foreach ($nums as $num) {
                        $selected = ($num == $perpage) ? 'selected' : '';

                        echo '<option value="' . $num . '" ' . $selected . '>' . get_string('showperpage', 'local_jinoboard', $num) . '</option>';
                    }
                    ?>
                </select>-->
                <div>
                <label><?php echo get_string('category','local_management')?></label>
                <select title="category01" name="category1" id="content_cata1" onchange="con_cata1_changed2(this);"  class="w_160">
                    <option value="0"><?php echo get_string('bigcategory', 'local_management') ?></option>
                    <?php
                    $catagories = siteadmin_get_content_category(1);
                    foreach ($catagories as $category) {
                        if ($category->id == $category1) {
                            $selected = ' selected="selected" ';
                        } else {
                            $selected = '';
                        }
                        echo '<option value="' . $category->id . ' " ' . $selected . '> ' . $category->name . '</option>';
                    }
                    ?>
                </select>
                <select title="category02" name="category2" id="content_cata2" class="w_160">
                    <option value="0"><?php echo get_string('middlecategory', 'local_management')?></option>
                    <?php
                    if ($category2) {
                        $catagories_detail = siteadmin_get_content_category(2, $category1);
                        foreach ($catagories_detail as $category) {
                            if ($category->id == $category2) {
                                $selected = 'selected';
                            } else {
                                $selected = '';
                            }
                            echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                        }
                    } else if($category1) {
                         $catagories_detail = siteadmin_get_content_category(2, $category1);
                        foreach ($catagories_detail as $category) {
                            echo '<option value="' . $category->id . '" > ' . $category->name . '</option>';
                        }
                    }
                    ?>
                </select>
                </div>
    <!--                <select name="target" title="target" class="w_160">
            <option value=""><?php echo get_string('contents_all', 'local_lmsdata'); ?></option>
                <?php
                $catagories_detail = siteadmin_get_content_category(2, $category1);
                foreach ($catagories_detail as $category) {
                    if ($category->id == $category2) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
                    echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                }
                ?>
        </select>-->
                <div>
                    <label><?php echo get_string('contents:type','local_management')?></label>
                    <?php foreach($con_type_array as $iakey => $iaval){?>
                    <label class="mg-bt10"><input type="checkbox" name="con_type[]" value="<?php echo $iakey?>" <?php foreach($con_type as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                    <?php }?>
                </div>
                <div>
                    <label><?php echo get_string('timecreated','local_management')?></label>
                    <input type="text" title="<?php echo get_string('time','local_management')?>" name="learningstart" class="w_120 date s_date" value="<?php echo $learningstart_str?>" placeholder="<?php echo get_string('click', 'local_management'); ?>  " >
                    <span class="dash">~</span> 
                    <input type="text" title="<?php echo get_string('time','local_management')?>" name="learningend" class="w_120 date e_date" value="<?php echo $learningend_str?>" placeholder="<?php echo get_string('click', 'local_management'); ?>  " > 
                </div>
                <div>
                    <label><?php echo get_string('isused','local_management')?></label> 
                    <?php foreach($share_yn_array as $iakey => $iaval){?>
                    <label class="mg-bt10"><input type="checkbox" name="share_yn[]" value="<?php echo $iakey?>" <?php foreach($share_yn as $ischeck){if($ischeck == $iakey){echo "checked";}}?>><?php echo $iaval ?></label>  
                    <?php }?>
                </div>
                <label><?php echo get_string('search','local_management')?></label>
                <input type="text" title="search" name="search"  id="search" value="<?php echo $search; ?>" class="search-text" placeholder="<?php echo get_string('contents:searchtext','local_management')?>">
                <input type="submit" class="search_btn" id="searchbtn" value="<?php echo get_string('search', 'local_jinoboard'); ?>">
                <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>"/>   
            </form>
            <span><?php echo get_string('total','local_management',$datas->total_count)?></span>
            <select name="perpage2" class="perpage2">
                <?php 
                foreach($perpage_array as $pa){
                    $selected = '';
                    if($pa == $perpage){
                        $selected = 'selected';
                    }
                ?>
                <option value="<?php echo $pa?>" <?php echo $selected?>><?php echo $pa?><?php echo get_string('viewmore','local_management')?>
                <?php
                }
                ?>
            </select>
            <span><input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="course_list_excel();"/></span>      
            <input type="button" value="<?php echo get_string('contents_registration', 'local_lmsdata') ?>" onclick="location.href = 'add.php'" class="blue_btn" style="float:right;"/>
            <!--input type="button" value="<?php //echo get_string('contents_contenthistory', 'local_lmsdata')      ?>" onclick="location.href = 'history.php'" class="blue_btn" style="float:right;"/-->
            <!-- 검색폼 종료 -->
            <form id="delete_form" action="deletes.php" method="POST">
                <table cellspacing="0" cellpadding="0">
                    <caption class="hidden-caption">콘텐츠 리스트</caption>
                    <tr>
<!--                        <th scope="row" style="width:50px;"><input type="checkbox" title="check" id="allcheck" style="margin: 0 !important;"/></th>-->
                        <th scope="row" style="width:5%;"><?php echo get_string('number', 'local_lmsdata'); ?></th>
                        <th scope="row" style="width:5%;"><?php echo get_string('contents:type', 'local_management'); ?></th>
                        <th scope="row"><?php echo get_string('contents:name', 'local_management'); ?></th>
                        <th scope="row" style="width:8%;"><?php echo get_string('contents:code', 'local_management'); ?></th>
                        <th scope="row" style="width:15%;"><?php echo get_string('contents:category', 'local_management'); ?></th>
                        <th scope="row" style="width:5%;"><?php echo get_string('isused', 'local_management'); ?></th>
                        <th scope="row" style="width:10%;"><?php echo get_string('update_date', 'local_lmsdata'); ?></th>
                    </tr>
                    <?php
                    
                    
                    foreach ($datas->files as $file) {
                        if (!$file->gname) {
                            $file->gname = '-';
                        }
                        switch ($file->con_type) {
                            case "word" : $type_txt = get_string('document', 'local_repository');
                                break;
                            case "html" :
                            case "html2" : $type_txt = get_string('html', 'local_repository');
                                break;
                            case "video" : $type_txt = get_string('video', 'local_repository');
                                break;
                            case "embed" : $type_txt = get_string('embed', 'local_repository');
                                break;
                            case "tplan" : $type_txt = get_string('contents:tplan', 'local_management');
                                break;
                            case "visangurl" : $type_txt = get_string('contents:visang', 'local_management');
                                break;
                            default : $type_txt = "-";
                                break;
                        }
                        ?>
                        <tr>
<!--                            <td style="width:5%;"><input type="checkbox" title="check" name="check[<?php echo $file->conid; ?>]" class="check_delete" style="margin: 0 !important;"/></td>-->
                            <td style="width:5%;"><?php echo $datas->num--; ?></td>
                            <td style="width:15%;"><?php echo $type_txt; ?></td>
                            <td style="text-align:left">
                                <a href="detail.php?id=<?php echo $file->id ?>&userid=<?php echo $userid; ?>">
                                    <?php echo $file->con_name_vi .'<br>'.$file->con_name; ?>
                                </a>
                            </td>
                            <td><?php echo $file->code?></td>
                            <td style="width:15%;text-align:left">
                                <?php
                                $cataname = '';
                                $catas2 = siteadmin_get_content_category(1);
                                foreach ($catas2 as $cata2) {
                                    if ($cata2->id == $file->parent) {
                                        $cataname .= $cata2->name;
                                    }
                                }
                                ?>
                                <?php
                                $cataname2 = '';
                                $catas = siteadmin_get_content_category(2); //카테고리 이름, 번호 구하는 함수
                                foreach ($catas as $cata) {
                                    if ($cata->id == $file->category) {
                                        $cataname2 = $cata->name;
                                    }
                                }
                                echo $cataname.' > '.$cataname2;
                                ?>
                            </td>

                            
                            
                            <td style="width:10%;">
                                <?php
                                $public = (!$file->share_yn || $file->share_yn == 'N') ? get_string('notused', 'local_management') : get_string('used', 'local_management');
                                echo $public;
                                ?>
                            </td>
                            <td style="width:10%;"><?php echo date('Y.m.d', $file->reg_dt); ?></td>
                        </tr>   
                        <?php
                    }
                    if (empty($datas->total_count)) {
                        ?>
                        <tr>
                            <td colspan="7"><?php echo get_string('nocontents', 'local_repository'); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <div class="btn_area">
                                
                    <!--input type="button" value="<?php //echo get_string('contents_filebatchregistration', 'local_lmsdata')      ?>" onclick="location.href = 'excell_add.php'" class="blue_btn" style="float:right;"/-->
                    <input type="button" value="<?php echo get_string('contents_registration', 'local_lmsdata') ?>" onclick="location.href = 'add.php'" class="blue_btn" style="float:right;"/>
<!--                    <input type="button" id="delete_notice" class="normal_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left" />-->
                </div>    
            </form>
            <?php
            $page_params = array();
            $page_params['category2'] = $category2;
            $page_params['category1'] = $category1;
            $page_params['perpage'] = $perpage;
            $page_params['search'] = $search;
            if (($datas->total_count / $perpage) > 1) {
                print_paging_navbar_script($datas->total_count, $page_num, $perpage, 'javascript:cata_page(:page);');
            }
            ?>
            <!-- Breadcrumbs End -->
        </div> <!-- Table Footer Area End -->
    </div>
    </div>
</section>
<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>
    <?php
} else {
    $chkdatas = get_contents($userid, $category2, $search, $page_num, $perpage);
    $datas = get_contents($userid, $category2, $search, $page_num, $chkdatas->total_count);
    $datas = get_contents_list($userid, $category1, $category2,$sql_where,$params, $search);
    
    $fields = array(
        get_string('number', 'local_lmsdata'),
        get_string('contents:type', 'local_management'),
        get_string('contents:name', 'local_management'),
        get_string('contents:code', 'local_management'),
        get_string('contents:category', 'local_management'),
        get_string('isused', 'local_management'),
         get_string('update_date', 'local_lmsdata')
    );

    $date = date('Y-m-d', time());
    $filename = '콘텐츠리스트_' . $date . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($fields as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }

    $row = 1;

    foreach ($datas->files as $file) {
        $public = (!$file->share_yn || $file->share_yn == 'N') ? get_string('notused', 'local_management') : get_string('used', 'local_management');
        switch ($file->con_type) {
            case "word" : $type_txt = get_string('document', 'local_repository');
                break;
            case "html" :
            case "html2" : $type_txt = get_string('html', 'local_repository');
                break;
            case "video" : $type_txt = get_string('video', 'local_repository');
                break;
            case "embed" : $type_txt = get_string('embed', 'local_repository');
                break;
            case "tplan" : $type_txt = get_string('contents:tplan', 'local_management');
                break;
            default : $type_txt = "-";
                break;
        }
        $catas2 = siteadmin_get_content_category(1);
        foreach ($catas2 as $cata2) {
            if ($cata2->id == $file->parent) {
                $categrytxt2 = $cata2->name;
            }
        }
        $catas = siteadmin_get_content_category(2);
        foreach ($catas as $cata) {
            if ($cata->id == $file->category) {
                $categrytxt = $cata->name;
            }
        }

        $col = 0;
        $worksheet[0]->set_column(1, 1, 15, array('v_align' => 'top')); // 상위 카테고리
        $worksheet[0]->set_column(1, 2, 25, array('v_align' => 'top')); // 하위 카테고리
        $worksheet[0]->set_column(1, 3, 25, array('v_align' => 'top')); // 제목
        $worksheet[0]->set_column(1, 4, 25, array('v_align' => 'top')); // 종류

        $worksheet[0]->write($row, $col++, $row);
        $worksheet[0]->write($row, $col++, $type_txt);
        $worksheet[0]->write($row, $col++, $file->con_name_vi .'<br>'.$file->con_name);
        $worksheet[0]->write($row, $col++, $file->code);
        $worksheet[0]->write($row, $col++, $categrytxt2 .' > '.$categrytxt);
        $worksheet[0]->write($row, $col++, $public);
        $worksheet[0]->write($row, $col++, date('Y.m.d', $file->reg_dt));

        $row++;
    }

    $workbook->close();
    die;
}
?>
<script>
    /**
     * 페이징 기능
     * @param {type} page
     * @returns {undefined}
     */
    function cata_page(page) {
        $('input[name=page_num]').val(page);
        $('#frm_content_search').submit();
    }

    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content",
            header: "h3",
            active: false
        });
        $("#accordion").accordion("option", "icons", null);
    });
//	$('#accordion input[type="checkbox"]').click(function(e) {
//		e.stopPropagation();
//	});

    $(document).ready(function () {
        $('#reset').on('click',function(){
            $('#content_cata1').val('0');
            $('#content_cata2').val('0');
            $('#copyright').val('0');
            $('.s_date').val('');
            $('.e_date').val('');
            $("input:checkbox[name='share_yn[]']").attr("checked", false);
            $("input:checkbox[name='con_type[]']").attr("checked", false);
            $('input[name=search]').val('');
            $("form[id=frm_content_search]").submit();
        });
        
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=frm_content_search]").submit();
        });
    
        /**
         * 체크박스 all 이벤트
         */
        $('#allcheck').click(function () {
            if ($('#allcheck').is(":checked")) {
                $(".check_delete").each(function () {
                    this.checked = true;
                });
            } else {
                $(".check_delete").each(function () {
                    this.checked = false;
                });
            }
        });
        /**
         * 삭제 기능
         */
        $('#delete_notice').click(function () {
            if (confirm("삭제 하시겠습니까? 선택된 콘텐츠와 연관된 모든 파일과 학습활동도 삭제됩니다.")) {
                $('#delete_form').submit();
            }
        });
        /**
         * 검색기능
         */
        $('#searchbtn').click(function () {
//            var content_cata1 = $('#content_cata1').val();
//            var content_cata2 = $('#content_cata2').val();
////            if (content_cata2 == 0) {
////                $('#content_cata1').val(0);
////                alert('세부 카테고리도 선택해주세요.');
////                return false;
////            }
//            var search = $('#search').val();
//            var page_num = $('input[name=page_num]').val();

//            location.href = "./notice.php?searchfield=" + searchfield + "&searchvalue=" + searchval + "&timestart=" + timestart + "&timeend=" + timeend;
        });
    });
    /**
     * 엑셀다운로드
     
     * @returns {undefined}     */
    function course_list_excel() {
//        $("input[name='excel']").val(1);
//        $("#course_search").submit();
//        $("input[name='excel']").val(0);
        var url = 'index.php?excel=1';
        document.location.href = url;
    }
    
    start_datepicker_c('', '');
    end_datepicker_c('', '');
</script>

