<?php

/**
 * 컨텐츠 카테고리 목록을 반환
 * @global type $DB
 * @param type $depth
 * @param type $parentid
 * @return type
 */
function siteadmin_get_content_category($depth, $parentid = 0) {
    global $DB;

    $sql = 'SELECT lc.id, lcn.name, lc.parent 
            FROM {lmsdata_category} lc 
            JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.id  ';

    $params = array(
        'lang' => current_language(),
        'depth' => $depth,
        'isused' => 1
    );
    $where[] = ' lc.depth = :depth ';
    $where[] = ' lcn.lang = :lang ';
    $where[] = ' lc.isused = :isused ';

    $orderby;

    if (!empty($parentid)) {
        $where[] = ' lc.parent = :parent ';
        $params['parent'] = $parentid;
        $orderby = ' order by lc.step ASC';
    } else {
        $orderby = ' order by lc.parent ASC';
    }

    $where = ' WHERE ' . implode(' AND ', $where);

    $category = $DB->get_records_sql($sql . $where . $orderby, $params);

    return $category;
}

/**
 * 썸네일 이미지 반환
 * @global type $CFG
 * @param type $con_id = lcms_contents->id
 * @return type $con_id
 */
function thumbnail_image($con_id) {
    global $CFG;
    //썸네일 파일 불러오기
    $fs = get_file_storage();
    //contextid = 1, $data->con->id = itemid and lcms_file->id
    $files = $fs->get_area_files(1, 'local_repository', 'thumbnail', $con_id, "", false);

    foreach ($files as $file) {
        //파일 이름
        $filename = $file->get_filename();
        //썸네일 패스
        $thpath = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/1/local_repository/thumbnail/' . $con_id . '/' . $filename);
    }

    return $thpath;
}

function visang_cdn_tokenmaker($mediapath) {
    global $CFG,$DB;
    
    require_once $CFG->dirroot . '/local/lmsdata/cdntokenmaker/src/JWT.php';
    
    $cdninfos = $DB->get_records_sql('select * from {vi_settings} where name like :key ',array('key'=>'%cdn%'));
    foreach ($cdninfos as $key => $value) {
      $cdninfo[$value->name] = $value->value;
    };
    
    $key = $cdninfo['cdnjwttoken'];
    $payload = array(
        'exp' => strtotime("+2 hours"),
        'path' => $path
    );
    $jwt = Firebase\JWT\JWT::encode($payload, $key);
    $https = $_SERVER['HTTPS'] == 'on' ? "https" : "http";
    $cdnpath = $https.'://'.$cdninfo['cdnbaseurl'].'/'.$mediapath.'?token='.$jwt;
    return $cdnpath;
};