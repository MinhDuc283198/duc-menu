<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');


$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$stext = optional_param('stext', '', PARAM_RAW); //검색어
$stype = optional_param('stype', 1, PARAM_INT); //검색타입
$Identification = 'mainjob2';
$menutext = get_string('job:manage2', 'local_management');

$pagesettings = array(
    'title' => $menutext,
    'heading' => $menutext,
    'subheading' => '',
    'menu' => $Identification,
    'js' => array($CFG->wwwroot . '/local/management/userjob/job.js'),
    'css' => array(),
    'nav_bar' => array()
);
include_once($CFG->dirroot . '/local/management/header.php');



$perpages = [10, 30, 50, 100];

//$contents = management_contnets_get_contents($type, $page, $perpage, $stype, $scate, $stext,$ordertype,$orderitem);
//$totalcount = management_contnets_get_total($type, $stype, $scate, $stext);
if ($stype == 1) {
    $where = $stext != null ? "  AND vj.id = " . $stext : "";
} else if ($stype == 2) {
    $where = $stext != null ? "  AND vj.title like '%" . $stext . "%'" : "";
} else if ($stype == 3) {
    $where = $stext != null ? "  AND ve.company_name like '%" . $stext . "%'" : "";
}
$query = "SELECT vj.*,mj.timecreated as maintime, ve.company_name, mj.id as mainid,mj.recommended
         FROM {lmsdata_main_job} mj
         JOIN {vi_jobs} vj on vj.id = mj.jobid
         JOIN {vi_employers} ve on vj.employer_id = ve.id WHERE mj.type = 1 $where";
$contents = $DB->get_records_sql($query);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">


                <div id = "data1" >
                    <div>

                        <form action="index.php" method="get" name='search_form' onchange="chageSelect()">
                            <input type='hidden' name='type' value="<?php echo $type ?>">
                            <select name="stype" id="stype">
                                <option value="1" <?php echo $stype == 1 ? 'selected' : '' ?>><?php echo get_string('job:id', 'local_management'); ?></option>
                                <option value="2" <?php echo $stype == 2 ? 'selected' : '' ?>><?php echo get_string('job:name', 'local_management'); ?></option>
                                <option value="3" <?php echo $stype == 3 ? 'selected' : '' ?>><?php echo get_string('job:comname', 'local_management'); ?></option>
                            </select>
                            <input type="text" name="stext" id='stext' value="<?php echo $stext != '' ? $stext : null ?>">
                            <input type="submit" value="<?php echo get_string('search', 'local_management'); ?>">
                            <select class="f-r" name="perpage">
                                <?php foreach ($perpages as $perp) { ?>
                                    <option value="<?php echo $perp ?>" <?php echo $perpage == $perp ? 'selected' : '' ?>><?php echo $perp ?></option>
                                <?php } ?>
                            </select>
                        </form>

                    </div>
                    <table>
                        <thead class = "thead-light">
                            <tr>
                                <th width="50px">
                                    <input type = "checkbox" class = "checkall" />
                                </th>
                                <th width="50px">NO</th>
                                <th width="70px"><?php echo get_string('job:id', 'local_management'); ?></th>
                                <th><?php echo get_string('job:comname', 'local_management'); ?></th>
                                <th><?php echo get_string('job:name', 'local_management'); ?></th>
                                <th><?php echo get_string('job:date', 'local_management'); ?></th>
                                <th><?php echo get_string('job:enddate', 'local_management'); ?></th>
                                <th><?php echo get_string('job:maindate', 'local_management'); ?></th>
                                <th><?php echo get_string('course:recommended', 'local_management') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($contents) {
                                $startnum = (($page - 1) * $perpage) + 1;
                                foreach ($contents as $content) {
                                    ?>
                                    <tr>
                                        <td>
                                            <input type = "checkbox" class="conslist"  conid="<?php echo $content->mainid ?>"/>
                                        </td>
                                        <td><?php echo $startnum++ ?></td>
                                        <td><?php echo $content->id ?></td>
                                        <td><?php echo $content->company_name ?> </td>
                                        <td><?php echo $content->title ?> </td>
                                        <td><?php echo date("Y.m.d", $content->startdate) ?></td>
                                        <td><?php echo date("Y.m.d", $content->enddate) ?></td>
                                        <td><?php echo date("Y.m.d", $content->maintime) ?> </td>
                                        <td>
                                            <input type = "checkbox" class="recommended"  <?php  echo $content->recommended ? 'checked':'' ?> conid="<?php echo $content->mainid ?>"/>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                            <td colspan="8"><?php echo get_string('job:nodata', 'local_management'); ?></td>
                        <?php } ?> 
                        </tbody>
                    </table>
                    <div class = "text-right">
                        <button class = "btn btn-primary" onclick="removecontents();"><?php echo get_string('del', 'local_management'); ?></button>
                    </div>
                    <nav aria-label = "Page navigation" class = "text-center">
                        <ul class = "pagination">
                            <?php echo $paging ?>
                        </ul>
                    </nav>
                </div>

                <!--/.box -->

                <div class = "search-form">
                    <form onsubmit="consearch(); return false">
                        <select name="stype2" id="stype2">
                                <option value="1" <?php echo $stype2 == 1 ? 'selected' : '' ?>><?php echo get_string('job:id', 'local_management'); ?></option>
                                <option value="2" <?php echo $stype2 == 2 ? 'selected' : '' ?>><?php echo get_string('job:name', 'local_management'); ?></option>
                                <option value="3" <?php echo $stype2 == 3 ? 'selected' : '' ?>><?php echo get_string('job:comname', 'local_management'); ?></option>
                        </select>
                        <input type = "text" class = "form-control" id = "consearchid" />
                        <input type = "button" class = "btn btn-default" onclick="consearch()" value = "<?php echo get_string('search', 'local_management'); ?>" />
                    </form>
                </div>
                <table>
                    <thead class = "thead-light">
                         <tr>
                            <th width="100px"><?php echo get_string('job:id', 'local_management'); ?></th>
                            <th><?php echo get_string('job:comname', 'local_management'); ?></th>
                            <th><?php echo get_string('job:name', 'local_management'); ?></th>
                            <th><?php echo get_string('job:date', 'local_management'); ?></th>
                            <th><?php echo get_string('job:enddate', 'local_management'); ?></th>
                            <th width="130px"><?php echo get_string('insert', 'local_management'); ?></th>
                        </tr>
                    </thead>
                    <tbody class="search_values">
                    </tbody>
                </table>
<!--                <div class = "btn_area">
                    <button class = "btn btn-primary btn-danger  pull-right searchadd" onclick="searchadd()"><?php echo get_string('insert', 'local_management'); ?></button>
                </div>-->
            </div>


        </div>
        <!--/.col -->
    </div>

</section>
<!--/.content -->


<?php
include_once($CFG->dirroot . '/local/management/footer.php');
?>

<script type="text/javascript">
    $(function(){
        $(".recommended").click(function(){
           var  id = $(this).attr("conid");
           var  checked = $(this).is(":checked") ? 1 :0;
           $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'recommended',data:{id:id,checked:checked}},
                success: function (data, status, xhr) {
                },
            });
        }); 
    });
    function removecontents() {
        if (confirm('<?php echo get_string('job:delalert', 'local_management'); ?>')) {
            var type = 1;
            var conarr = new Array();
            $(".conslist:checked").each(function (i, v) {
                conarr.push($(v).attr("conid"));
            });

            $.ajax({
                type: 'POST',
                url: 'ajax_del.php',
                data: {ids: conarr,type:type},
                success: function (data, status, xhr) {

                    location.reload();
                },
            });
        }
    }
    function chageSelect() {
        var selected = $('#stype').val();
        var textval = $('#stext').val();
        if (selected == 1) {
            if (isNaN(textval)) {
                alert('<?php echo get_string('job:numalert', 'local_management'); ?>');
                return false;
            }
        }
    }

</script>
