function call_ajax(action, data, async) {
    var rvalue = false;
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });
    
    return rvalue;
}

function consearch() {
    var conid = $("#consearchid").val();
    var stype = $("#stype2").val();
    if (conid == '') {
        alert('검색어를 입력해주세요'); 
    } else {
        var result = call_ajax('search', {code: conid,stype:stype}, false);
        if(result.status == 'success'){
            if (result.total == 0) {
                alert('검색 결과가 없습니다.');
            } else {
                 var $tbody = $(".search_values");
                $tbody.empty();
                 $.each(result.list, function () {
                    $tr = $('<tr>');
                    $tr.data(this);
                    var stitle = $('<td>').text(this.title);
                    var scname = $('<td>').text(this.company_name);
                    var scode = $('<td>').text(this.id);                   
                    var sstartdate = $('<td>').text(formatDate(Number(this.startdate+'000')));
                    var senddate = $('<td>').text(formatDate(Number(this.enddate+'000')));
                    var sbutton = $('<td>').html('<button class = "btn btn-primary btn-danger searchadd" onclick="searchadd('+this.id+')">+</button>');
                    $tr.append(scode).append(scname).append(stitle).append(sstartdate).append(senddate).append(sbutton);
                    $tbody.append($tr);
                 });
               
            }
        }else{
            alert(result.error);
        }
    }
}

function searchadd(code) {
//    var code = $("input[name=addid]").val();
    if (code == '') {
        alert('콘텐츠 정보 확인을 해주세요');
    } else {
        if (confirm(("해당 공고를 추천 채용정보에 추가하시겠습니까?"))) {
            var result = call_ajax('add', {code: code, type:0}, false);
            if(result.status == 'success'){
                location.reload();
            }else{
                alert(result.error);
            }

        }
    }
}



function formatDate(date) { 
    var d = new Date(date);
    var month = '' + (d.getMonth() + 1); 
    var day = '' + d.getDate(); 
    var year = d.getFullYear();

    
    if (month.length < 2) month = '0' + month; 
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('.');
}
