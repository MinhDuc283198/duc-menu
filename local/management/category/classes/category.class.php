<?php

namespace local_management;
require_once($CFG->dirroot . '/local/management/category/lib.php');
defined('MOODLE_INTERNAL') || die();

/**
 * @property-read int $id
 * @property-read string $text
 * @property-read string $shortname
 * @property-read string $icon
 * @property-read string $description
 * @property-read string $href
 * @property-read string $target
 * @property-read string $title
 * @property-read int $lft
 * @property-read int $rgt
 * @property-read int $ismain
 * @property-read int $parentid
 * @property-read int $creator
 * @property-read int $timecreated
 * @property-read int $depth
 * @property-read array $children
 * @property-read object $parent
 */
class category {
    private $id;
    private $name;
    private $vi_name;
    private $en_name;
    private $idnumber;
    private $description;
    private $descriptionformat;
    private $parent;
    private $sortorder;
    private $coursecount;
    private $visible;
    private $visibleold;
    private $timemodified;
    private $depth;
    private $path;
    private $theme;
    private $children = array();

    
    private static $standardproperties = array(
        'id' => false,
        'name' => false,
        'vi_name' => false,
        'en_name' => false,
        'idnumber' => false,
        'description' => false,
        'descriptionformat' => false,
        'parent' => false,
        'sortorder' => false,
        'coursecount' => false,
        'visible' => false,
        'visibleold' => false,
        'timemodified' => false,
        'depth' => false,
        'path' => false,
        'theme' => false,
        'children' => false
    );
    
    public function __construct($data) {
        foreach($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
    
    /**
     * Magic method getter
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        if (isset(self::$standardproperties[$name])) {
            if ($method = self::$standardproperties[$name]) {
                return $this->{$method}();
            } else {
                return $this->{$name};
            }
        } else {
            debugging('Invalid cm_info property accessed: '.$name);
            return null;
        }
    }
    
    public function __set( string $name , mixed $value ) {
        if (method_exists($this, 'set_'.$name)) {
            $method = 'set_'.$name;
            $this->{$method}($value);
        } else if (property_exists($this, $name)) {
            $this->{$name} = $value;
        } else {
            debugging('Invalid completion property accessed: '.$name);
        }
    }
    
    public function add_child($child) {
        $this->children[] = $child;
        //$child->parent = $this;
    }
    
    public function set_active($active) {
        $this->active = $active;
        
        // 부모도 active로 설정
        if($this->parent != null) {
            $this->parent->set_active($active);
        }
    }
    
    public function is_active() {
        return $this->active;
    }
    
    public function has_children() {
        return count($this->children) > 0;
    }


    public function print_menu() {
        $cssclass = array();
        if($this->is_active()) {
            $cssclass[] = 'active';
        }
        
        if($this->has_children()) {
            $cssclass[] = 'treeview';
        }
        
        $cssclassstr = '';
        if(count($cssclass) > 0) {
            $cssclassstr = ' class="'. implode(' ', $cssclass).'"';
        }
        
        $html = '<li'.$cssclassstr.' id="menu_'.$this->name.'">
          <a href="'.local_management_make_absolute($this->href).'" target="'.$this->name.'" title="'.$this->name.'">
            <i class="'.$this->name.'"></i> <span>'.$this->name.'</span>';
        if($this->has_children()) {
            $html .='    <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">';
            foreach($this->children as $child) {
                $html .= $child->print_menu();
            }
            $html .= '</ul>';
        } else {
            $html .= '</a>';
        }
        $html .= '</li>';
        
        return $html;
    }
    
    function print_json($category) {
        $aa = $this->has_children();
        $json = '{"id":"'.$this->id.'","href":"'.htmlentities($this->href).'","depth":"'.$this->depth
                .'","text":"'.htmlentities($this->text).'", "idnumber":"'.$this->idnumber
                .'", "target": "'.$this->target.'", "name": "'.$this->name
                .'","parent":"'.$this->parent.'","description":"'.htmlentities($this->description)
                .'","visible":"'.$this->visible.'","ismain":"'.$aa
                .'","category":"'.$category
                .'","vi_name":"'.$this->vi_name.'","en_name":"'.$this->en_name.'"';
        
        if($this->has_children()) {
            $jsonchildren = array();
            //$this->children = local_management_course_categories_convert_class($this->children);
            foreach($this->children as $child) {
                $jsonchildren[] = $child->print_json();
                //$jsonchildren[] = $child;
            }
            $json .= ',"children":['.implode(',', $jsonchildren).']';
        }
        $json .= '}';
        
        return $json;
    }
}
