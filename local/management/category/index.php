<?php

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');

$id = optional_param('id','course', PARAM_RAW);
if($id == 'course'){
    $pagesettings = array(
        'title' => '강좌카테고리',
        'heading' => '강좌카테고리',
        'subheading' => '',
        'menu' => 'ccategory',
        'js' => array(),
        'css' => array(),
        'nav_bar'=> array()
    );
}else{
    $pagesettings = array(
        'title' => '교재카테고리',
        'heading' => '교재카테고리',
        'subheading' => '',
        'menu' => 'bcategory',
        'js' => array(),
        'css' => array(),
        'nav_bar'=> array()
    );
}

include_once($CFG->dirroot.'/local/management/header.php');
?>
<section class="content">
    
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">카테고리</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <ul id="myEditor" class="sortableLists list-group">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">카테고리 편집</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form id="frmEdit" role="form">
                    <input type="hidden" name="category" value="<?php echo $id?>">
                    <input type="hidden" name="id" value="0" />
                    <input type="hidden" name="ismain" value="0" />
                    <input type="hidden" name="category" value="<?php echo $id?>">
                        <div class="form-group">
                            <label for="parent">상위범주</label>
                            <select name='parent' id='parent'  class="item-menu">
                                <option value="0">선택</option>
                                <?php
                                if($id == "course"){
                                    $query = 'select * from {course_categories} where parent = 0';
                                }else{
                                    $query = 'select * from {lmsdata_book_categories} where parent = 0';
                                }
                                $cata1 = $DB->get_records_sql($query);
                                foreach($cata1 as $c1){
                                    echo '<option value="'.$c1->id.'">'.$c1->name.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="book">
                            <div class="form-group">
                                <label for="name">범주명</label>
                                <input type="text" class="form-control item-menu" id="name" name="name" placeholder="범주명">
                            </div>
                        </div>
                        <div class="course">
                        <h3>범주명</h3>
                            <div class="form-group">
                                <label for="name">한국어범주명</label>
                                <input type="text" class="form-control item-menu" id="name" name="name" placeholder="범주명">
                            </div>
                            <div class="form-group">
                                <label for="name">영어범주명</label>
                                <input type="text" class="form-control item-menu" id="en_name" name="en_name" placeholder="범주명">
                            </div>
                            <div class="form-group">
                                <label for="name">베트남어범주명</label>
                                <input type="text" class="form-control item-menu" id="vi_name" name="vi_name" placeholder="범주명">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="idnumber">범주 식별번호</label>
                            <input type="text" class="form-control item-menu" id="idnumber" name="idnumber" placeholder="범주 식별번호">
                        </div>

                        <div class="form-group">
                            <label for="description">설명</label>
                            <textarea class="form-control item-menu" rows="3" name="description" placeholder="설명"></textarea>
                        </div>

                        <div class="form-group">
                            <label>사용여부</label>
                            <select name='visible' id='visible'  class="item-menu">
                                <option value="1">사용</option>
                                <option value="0">미사용</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <button type="button" id="btnUpdate" class="btn btn-primary" disabled="disabled"><i class="fa fa-refresh"></i> 수정</button>
                    <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> 추가</button>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
include_once($CFG->dirroot.'/local/management/footer.php');
?>
<!-- 삭제확인창 -->
<div class="modal modal-default fade" id="modal_del_confirm">
    
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header box-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="닫기">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><span class='name'></span> 카테고리 삭제</h4>
        </div>
        <div class="modal-body">
<!--            <span class='name'></span> 의 내용-->
            <form action="#" method="post" id="btnDelConfirm">
                <input type='hidden' name='id' id='id'>
            </form>
            
            <div id="del_confirm_list">
                
            </div>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
</div>