<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/category/lib.php');
require_once($CFG->dirroot.'/lib/coursecatlib.php');
$action = required_param('action', PARAM_ALPHAEXT);

$context = context_system::instance();

require_login($context);
$hasrole = local_management_has_role_menu('menu');
if(!$hasrole) {
    throw new moodle_exception('nopermissions', 'local_management');
}

switch ($action) {
    case 'load':
        $category = optional_param('category','course', PARAM_RAW);
        if($category == 'course'){
            $menus = local_management_course_categories_get_course_categories();
        }else{
            $menus = local_management_book_categories_get_course_categories();
        }
        $menus = local_management_course_categories_convert_class($menus);
        $json = array();
        
        foreach($menus as $menu) {
            $json[] = $menu->print_json($category);
        }
        echo '['.implode(',', $json).']';
        die();
        break;
    case 'add':
        $data = required_param_array('data', PARAM_RAW);
        $parentid = $data['parent'];
        if(empty($parentid)) {
            $parentid = 0;
        }
        
        unset($data['id']);
        unset($data['parentid']);
        $menu = (object) $data;
        $menu->ismain = 0;
        if($data['category'] == "course"){
            $category = coursecat::create($data);
             //course_categories_lang
            $lang = new stdClass();
            $lang->categoryid = $category->id;
            $lang->ko_name = $menu->name;
            $lang->en_name = $menu->en_name;
            $lang->vi_name = $menu->vi_name;
            $id =$DB->insert_record('course_categories_lang', $lang);
        }else{
            $id = local_management_book_categories_add($menu, $parentid);
        }
        
        echo '{"status":"success","id":"'.$id.'","parentid":"'.$parentid.'"}';
        die();
        break;
    case 'update':
        $data = required_param_array('data', PARAM_RAW);
        $coursecat = coursecat::get($data['id'], MUST_EXIST, true);
        $menu = (object) $data;

        //local_management_course_categories_update($menu);
        if ((int)$data['parent'] !== (int)$coursecat->parent && !$coursecat->can_change_parent($data['parent'])) {
            print_error('cannotmovecategory');
        }
        $coursecat->update($data);
        //여기
        $lang = $DB->get_record('course_categories_lang', array('categoryid' => $menu->id));
        $newlang = new stdClass();
        $newlang->ko_name = $menu->name;
        $newlang->en_name = $menu->en_name;
        $newlang->vi_name = $menu->vi_name;
        if($lang){
            //update
            $newlang->id = $lang->id;
            $DB->update_record('course_categories_lang', $newlang);
        }else{
            //insert
            $newlang->categoryid = $menu->id;
            $DB->insert_record('course_categories_lang', $newlang);
        }
      
        echo '{"status":"success"}';
        die();
        break;
    case 'move':
        $data = required_param_array('data', PARAM_RAW);
        $category = coursecat::get($data['id']);

        if($data['action'] == "up") {
            $redirectback = \core_course\management\helper::action_category_change_sortorder_up_one($category);

        } else if($data['action'] == "down") {
            $redirectback = \core_course\management\helper::action_category_change_sortorder_down_one($category);
        }
        
        echo '{"status":"success","parentid":"'.$data['parent'].'"}';
        die();
        break;
    case 'delete':
        require_once($CFG->dirroot.'/lib/coursecatlib.php');
        require_once($CFG->dirroot.'/course/lib.php');
        require_once($CFG->libdir . '/formslib.php');
        require_once($CFG->libdir . '/questionlib.php');

        
        $data = required_param_array('data', PARAM_RAW);


        /* 완전삭제 */
        if($data['fulldelete'] == 1){
            $category = coursecat::get($data['id']);
            $continueurl = new moodle_url('/course/management.php');
            if ($category->parent != '0') {
                $continueurl->param('categoryid', $category->parent);
            }
            $notification = get_string('coursecategorydeleted', '', $category->get_formatted_name());
            $deletedcourses = $category->delete_full(true);
            foreach ($deletedcourses as $course) {
                //echo $renderer->notification(get_string('coursedeleted', '', $course->shortname), 'notifysuccess');
            }
        }else{
            /* 옮기기 */
            //$continueurl = new moodle_url('/course/management.php', array('categoryid' => $data->newparent));
            $category = coursecat::get($data['id']);
            $category->delete_move($data['newparent'], true);
            //lmsdata_course 
            //coursearea roughprocess 위에꺼, coursetype detailprocess아래꺼
            //위에꺼 구하기
            $query = 'select * from {course_categories} where id=:id';
            $sql = $DB->get_record_sql($query, array('id'=>$data['newparent']));
            $newdata = new object();
            $newdata->coursearea = $sql->parent;
            $newdata->roughprocess = $sql->parent;
            $newdata->coursetype = $data['newparent'];
            $newdata->detailprocess = $data['newparent'];
            $DB->execute('update {lmsdata_course} set coursearea=:coursearea, roughprocess=:roughprocess,coursetype=:coursetype,detailprocess=:detailprocess where detailprocess=:id',
                    array('coursearea'=>$sql->parent, 'roughprocess'=>$sql->parent,'coursetype'=>$data['newparent'], 'detailprocess'=>$data['newparent'], 'id'=>$data['id'] ));
        }

        //echo $renderer->notification($notification, 'notifysuccess');
        //echo $renderer->continue_button($continueurl);
        
        //local_management_menu_delete($data['id']);


        
        echo '{"status":"success"}';
        die();
        break;
    case 'categoryid':
        $data = required_param_array('data', PARAM_RAW);
        $query='select * from {course_categories} where id=:id';
        $list = $DB->get_record('course_categories', array('id' => $data['id']));
                
        $rvalue = new stdClass();
        $rvalue->status = 'success';
        $rvalue->list = $list;
        
        $query2 = 'select count(id) from {course_categories} where parent=:id';
        $list2 = $DB->get_record_sql($query2, array('id' => $data['id']));
        $rvalue->sub = $list;
        
        $query3 = 'select * from {course_categories}  where depth<3 and id !=:id order by sortorder ASC';
        $list3 = $DB->get_records_sql($query3, array('id'=>$data['id']));
        $html = '';
        foreach($list3 as $li3){
            if($li3->parent != 0){
                $html .= category_parent($li3->id);
            }
        }

        $rvalue->html = $html;
        
        $query4 = 'select count(id) from {course_categories} where parent = :parent and depth < 3 order by sortorder ASC';
        $list4 = $DB->count_records_sql($query4, array('parent'=>$data['id']));
        $rvalue->childcnt = $list4;
        echo json_encode($rvalue);
        die();
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();
