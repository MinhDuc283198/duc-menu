<?php

require_once($CFG->dirroot . '/local/management/category/classes/category.class.php');



/**
 * 전체 메뉴를 반환한다.
 * $parentid를 지정하는 경우 해당 메뉴의 전체 하위 메뉴를 반환한다.
 * 
 * 기본적으로 최상위 메뉴는 제외 시킨다.
 * 포함 시키려면 $withtop 값을 true로 지정한다.
 * 
 * @global object $DB
 * @param int $parentid
 * @param boolean $withtop 최상위 메뉴($parentid가 지정된 경우 해당 메뉴)를 포함할지 여부
 * @return array|false
 */
function local_management_course_categories_get_course_categories($parentid = null) {
    global $DB;

    if($parentid == null){
        $menus = $DB->get_records_sql("SELECT cc.*, ccl.en_name, ccl.vi_name FROM {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid where cc.depth=1  order by cc.sortorder asc");
    }else{
        $menus = $DB->get_records_sql("SELECT cc.*, ccl.en_name, ccl.vi_nam FROM {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid where cc.parent=:parentid  order by cc.sortorder asc", array('parentid'=>$parentid));
    }
    if($menus){
        foreach($menus as $menu){
            $submenu = $DB->get_records_sql("SELECT cc.*, ccl.en_name, ccl.vi_name FROM {course_categories} cc LEFT JOIN {course_categories_lang} ccl ON cc.id = ccl.categoryid  where cc.parent=:parentid  order by cc.sortorder asc", array('parentid'=>$menu->id));
            if($submenu){
                $menu->children = local_management_course_categories_convert_class($submenu);
            }
            
            //$menu->children = local_management_course_categories_get_course_categories($menu->id);
        }
    }

    return $menus;
}

function local_management_book_categories_get_course_categories($parentid = null) {
    global $DB;

    if($parentid == null){
        $menus = $DB->get_records_sql("SELECT * FROM {lmsdata_book_categories} where depth=1  order by sortorder asc");
    }else{
        $menus = $DB->get_records_sql("SELECT * FROM {lmsdata_book_categories} where parent=:parentid  order by sortorder asc", array('parentid'=>$parentid));
    }
    if($menus){
        foreach($menus as $menu){
            $submenu = $DB->get_records_sql("SELECT * FROM {lmsdata_book_categories} where parent=:parentid  order by sortorder asc", array('parentid'=>$menu->id));
            if($submenu){
                $menu->children = local_management_course_categories_convert_class($submenu);
            }
            
            //$menu->children = local_management_course_categories_get_course_categories($menu->id);
        }
    }

    return $menus;
}

/**
 * DB에서 select 한 object를 \local_management\menu 클래스로 변환한다.
 * 
 * @param array $menus
 * @return array \local_management\menu 클래스 배열
 */
function local_management_course_categories_convert_class($menus) {
    $rvalue = array();

    $classes = array();
    foreach($menus as $menu) {
        $class = new \local_management\category($menu);
        if(isset($classes[$class->parent])) {
            $classes[$class->parent]->add_child($class);
        } else {
            $rvalue[] = $class;
        }
        $classes[$class->id] = $class;
    }

    return $rvalue;
}




/**
 * 메뉴를 추가한다.
 * 
 * @global object $DB
 * @global object $USER
 * @param object $menu
 * @param int $parentid
 * @return int
 */
function local_management_course_categories_add($menu, $parentid) {
    global $DB, $USER;
    
    // shortname이 중복되는지 체크
//    if($DB->record_exists('course_categories', array('name' => $menu->name))) {
//        print_error('duplicatedshortname', 'local_management');
//    }
//    
//    $parent = $DB->get_record('course_categories', array('id' => $parentid));
    

    $menu->descriptionformat = FORMAT_MOODLE;
    //$menu->name = $menu->name;
    //$menu->idnumber = $menu->idnumber;
     if (empty($menu->name)) {
        throw new moodle_exception('categorynamerequired');
    }
    if (core_text::strlen($menu->name) > 255) {
        throw new moodle_exception('categorytoolong');
    }
    if (isset($menu->idnumber)) {
        if (core_text::strlen($menu->idnumber) > 100) {
            throw new moodle_exception('idnumbertoolong');
        }
        if (strval($menu->idnumber) !== '' && $DB->record_exists('course_categories', array('idnumber' => $menu->idnumber))) {
            throw new moodle_exception('categoryidnumbertaken');
        }
        $menu->idnumber = $menu->idnumber;
    }
    //$menu->parent = $parentid;
    //$menu->visible = $menu->visible;
    $menu->timemodified = time();
    $menu->sortorder = 0;
    
    
    if (isset($menu->theme) && !empty($CFG->allowcategorythemes)) {
        $menu->theme = $data->theme;
    }

    if (empty($menu->parent)) {
        $menu->parent = 0;
        $menu->depth = 1;
    } else {
        $parent = $DB->get_record_sql('select * from {course_categories} where id=:parent', array('parent'=>$menu->parent));
        $menu->parent = $menu->parent;
        $menu->depth = $parent->depth + 1;
    }
    $menu->id = $DB->insert_record('course_categories', $menu);
    
    if($menu->parent == 0){
        $path =  '/' . $menu->id;
    }else{
        $path = $parent->path . '/' . $menu->id;
    }
    
    $DB->set_field('course_categories', 'path', $path, array('id' => $menu->id));
    
    //course_categories_lang
    $lang = new stdClass();
    $lang->categoryid = $menu->id;
    $lang->ko_name = $menu->name;
    $lang->en_name = $menu->en_name;
    $lang->vi_name = $menu->vi_name;
    $DB->insert_record('course_categories_lang', $lang);
    
    // We should mark the context as dirty.
    context_coursecat::instance($menu->id)->mark_dirty();

    fix_course_sortorder();

    // If this is data from form results, save embedded files and update description.
    $categorycontext = context_coursecat::instance($menu->id);
    if ($editoroptions) {
        $menu = file_postupdate_standard_editor($menu, 'description', $editoroptions, $categorycontext,
                                                       'coursecat', 'description', 0);

        // Update only fields description and descriptionformat.
        $updatedata = new stdClass();
        $updatedata->id = $menu->id;
        $updatedata->description = $menu->description;
        $updatedata->descriptionformat = $menu->descriptionformat;
        $DB->update_record('course_categories', $updatedata);
    }

    $event = \core\event\course_category_created::create(array(
        'objectid' => $menu->id,
        'context' => $categorycontext
    ));
    $event->trigger();

    cache_helper::purge_by_event('changesincoursecat');
        
        
    return true;
}

function local_management_book_categories_add($menu, $parentid) {
    global $DB, $USER;

    $menu->descriptionformat = FORMAT_MOODLE;
    //$menu->name = $menu->name;
    //$menu->idnumber = $menu->idnumber;
     if (empty($menu->name)) {
        throw new moodle_exception('categorynamerequired');
    }
    if (core_text::strlen($menu->name) > 255) {
        throw new moodle_exception('categorytoolong');
    }
    if (isset($menu->idnumber)) {
        if (core_text::strlen($menu->idnumber) > 100) {
            throw new moodle_exception('idnumbertoolong');
        }
        if (strval($menu->idnumber) !== '' && $DB->record_exists('lmsdata_book_categories', array('idnumber' => $menu->idnumber))) {
            throw new moodle_exception('categoryidnumbertaken');
        }
        $menu->idnumber = $menu->idnumber;
    }
    //$menu->parent = $parentid;
    //$menu->visible = $menu->visible;
    $menu->timemodified = time();
    
    //자기다음 없음
    $aa_query = "select * from {lmsdata_book_categories} where parent=:parent order by sortorder DESC limit 1";
    //select * from m_course_categories where parent=0 order by sortorder DESC
    $aa = $DB->get_record_sql($aa_query, array('parent'=>$menu->parent));

    $query = 'select * from {lmsdata_book_categories} where id=:prev';
    $sql = $DB->get_record_sql($query, array('prev' => $aa->id));
    $query2 = 'select * from {lmsdata_book_categories} where sortorder > :sortorder';
    $sql2 = $DB->get_records_sql($query2, array('sortorder' => $sql->sortorder));
    foreach($sql2 as $s2){
        $menu2 = new stdClass();
        $menu2->id = $s2->id;
        $menu2->sortorder = $s2->sortorder + 10000;
        $DB->update_record('lmsdata_book_categories', $menu2);
    }

    $menu->sortorder = $sql->sortorder + 10000;
        
        
    //$menu->sortorder = 0;
    
    
    if (isset($menu->theme) && !empty($CFG->allowcategorythemes)) {
        $menu->theme = $data->theme;
    }

    if (empty($menu->parent)) {
        $menu->parent = 0;
        $menu->depth = 1;
    } else {
        $parent = $DB->get_record_sql('select * from {lmsdata_book_categories} where id=:parent', array('parent'=>$menu->parent));
        $menu->parent = $menu->parent;
        $menu->depth = $parent->depth + 1;
    }
    $menu->id = $DB->insert_record('lmsdata_book_categories', $menu);
    
    if($menu->parent == 0){
        $path =  '/' . $menu->id;
    }else{
        $path = $parent->path . '/' . $menu->id;
    }
    
    $DB->set_field('lmsdata_book_categories', 'path', $path, array('id' => $menu->id));

    return true;
}

function local_management_course_categories_update($menu) {
    global $DB, $USER;
    
    $menuold = $DB->get_record('course_categories', array('id' => $menu->id));
    
    
    // shortname을 변경한 경우 중복되는지 체크
    if($menu->idnumber != $menuold->idnumber) {
        if($DB->record_exists('course_categories', array('idnumber' => $menu->idnumber))) {
            print_error('duplicatedshortname', 'local_management');
        }
    }
    
    if($menu->parent != 0){
        $menu->path = '/'.$menu->parent.'/'.$menu->id;
        $menu->depth = 2;
    }else{
        $menu->path = '/'.$menu->id;
        $menu->depth = 1;
    }
    //여기
    $lang = $DB->get_record('course_categories_lang', array('categoryid' => $menu->id));
    $newlang = new stdClass();
    $newlang->ko_name = $menu->name;
    $newlang->en_name = $menu->en_name;
    $newlang->vi_name = $menu->vi_name;
    if($lang){
        //update
        $newlang->id = $lang->id;
        $DB->update_record('course_categories_lang', $newlang);
    }else{
        //insert
        $newlang->categoryid = $menu->id;
        $DB->insert_record('course_categories_lang', $newlang);
    }
    
    return $DB->update_record('course_categories', $menu);
}

function local_management_book_categories_update($menu) {
    global $DB, $USER;
    
    $menuold = $DB->get_record('lmsdata_book_categories', array('id' => $menu->id));
    
    
    // shortname을 변경한 경우 중복되는지 체크
    if($menu->idnumber != $menuold->idnumber) {
        if($DB->record_exists('lmsdata_book_categories', array('idnumber' => $menu->idnumber))) {
            print_error('duplicatedshortname', 'local_management');
        }
    }
    if($menu->parent != 0){
        $menu->path = '/'.$menu->parent.'/'.$menu->id;
        $menu->depth = 2;
    }else{
        $menu->path = '/'.$menu->id;
        $menu->depth = 1;
    }
    return $DB->update_record('lmsdata_book_categories', $menu);
}
function category_parent($id){
    global $DB;
    $category_query = 'select * from {course_categories} where id = :id';
    $category_list = $DB->get_record_sql($category_query, array('id'=>$id));   
    

    $data = $category_list->name;
    
    
    if($category_list->parent != 0){
        $data = '<option value="'.$category_list->id.'">'.category_parent($category_list->parent) . ' / ' . $data.'</option>'.'<p>';
    }
    
    return $data;
}