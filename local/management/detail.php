<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot."/local/repository/lib.php");
require_once($CFG->dirroot . "/lib/coursecatlib.php");
require_once($CFG->dirroot . "/lib/filelib.php");
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
header('Access-Control-Allow-Origin: *');

$pagesettings = array(
    'title' => get_string('contents:title','local_management'),
    'heading' => get_string('contents:title','local_management'),
    'subheading' => '',
    'menu' => 'contentsI',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
$context = context_system::instance();
$PAGE->set_context($context);

$id = optional_param('id', 0, PARAM_INT); // repository id
$userid = optional_param('userid', 0, PARAM_INT);
$ref = optional_param('ref', 0, PARAM_INT);

$PAGE->set_url('/local/repository/detail.php?id=' . $id);
$PAGE->set_pagelayout('standard');

$strplural = get_string("pluginnameplural", "local_repository");
$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

require_login();

//echo $OUTPUT->header();

if (!$ref) {
    $sql = "select "
            . "rep.id , rep.referencecnt , rep.status, "
            . "con.id as con_id,con.con_name,con.con_type,con.con_des,con.update_dt,con.data_dir,con.embed_type,con.embed_code, "
            . "rep_group.name as gname "
            . "from {lcms_repository} rep "
            . "join {lcms_contents} con on con.id= rep.lcmsid "
            . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
            . "where rep.id= :id";
} else {
    $sql = "select "
            . "rep.id , "
            . "con.id as con_id,con.con_name,con.con_type,con.con_des,con.update_dt,con.data_dir,con.embed_type,con.embed_code,"
            . "rep_group.name as gname "
            . "from {lcms_repository_reference} rep "
            . "join {lcms_contents} con on con.id= rep.lcmsid "
            . "left join {lcms_repository_groups} rep_group on rep_group.id = rep.groupid "
            . "where rep.id= :id";
}

$data = $DB->get_record_sql($sql, array('id' => $id));
insert_lcms_history($data->con_id,'Admin Page Viewed',2);
$fileobj = '';
if($data->con_type == 'tplan'){
    $fs = get_file_storage();
    $contextid = $context->id;
    $itemid = $data->con_id;
    $filearea = 'tplan';
    $files = $fs->get_area_files($contextid, 'local_lmsdata', "tplan", $itemid);
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $contextid . '/local_lmsdata/' . $filearea . '/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            if ($type == 1) {
                $fileobj = "<span id ='file' name='file_link_1'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
            } else if ($type == 2) {
                $fileobj = "<span id ='file' name='file_link_2'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
            } else {
                $fileobj = "<span id ='file' name='file_link'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
            }
        }
    }
}

if($data->con_type == 'embed' || $data->con_type == 'visangurl'){
    $fileobj = "<a href='$data->embed_code' target='_blank'>$data->embed_code</a>";
}

if($data->con_type == 'video'){
    $file = $DB->get_record('lcms_contents_file', array('con_seq' => $data->con_id));

    $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon('video/mp4')) . '" class="icon" alt="video/mp4" />';

    $fileobj = $iconimage."<a href='./download.php?id=".$id."'>".$file->fileoname."</a>";
}
/// echo $OUTPUT->heading($data->con_name);
include_once($CFG->dirroot.'/local/management/header.php');
?>
<section class="content">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
        <h3 class="detail_title"><?php echo $data->con_name; ?></h3>
        
        <?php
//타이틀표시
        $output_lcms = html_writer::start_tag('div', array('class' => 'board-detail-area'));

        $output_lcms .= html_writer::start_tag('div', array('class' => 'detail-contents-area'));
        $attfile = '';
        
        if($data->status==3){
            $output_lcms .= html_writer::tag('div', '동영상 인코딩 중 입니다', array('class' => ""));
            echo $output_lcms;
            die;
        }else if($data->status==5){
            $output_lcms .= html_writer::tag('div', '인코딩이 진행되지 않은 동영상 입니다', array('class' => ""));
            echo $output_lcms;
            die;
        }

//콘텐츠 정보
        if ($data->con_type == 'word' || $data->con_type == 'ref') {
            //파일정보(문서일경우 파일명 가져오기)
            if($data->con_type == 'ref' && $data->ref_con_type == 'word'){
                $files = $DB->get_records('lcms_contents_file', array('con_seq' => $data->con_id, 'con_type' => $data->con_type));
                foreach ($files as $file) {

                    $filename = $file->fileoname;
                    $filepath = explode('/', $file->filepath);
                    if ($filepath[0] == 'lms' || $filepath[0] == 'lcms')
                        $lcmsdata = '/lcmsdata/';
                    else
                        $lcmsdata = '/';
                    $mimetype = mime_content_type(STORAGE2 . $lcmsdata . $file->filepath . '/' . $file->filename);
                    $path = 'viewer/download.php?id=' . $id . '&fileid=' . $file->id;

                    $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
                    $attfile .= '<li>';
                    $attfile .= "<a href=\"$path\"><p class='hidden'>icon</p>$iconimage</a> ";
                    $attfile .= format_text("<a href=\"$path\">" . s($filename) . "</a></li>", FORMAT_HTML, array('context' => $context)) . "</li>";
                }
            } else {
                $mimetype = strrchr($data->embed_code,'.');
                $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';
                $attfile .= '<li>';
                $attfile .= "<a href=\"$data->embed_code\"><p class='hidden'>icon</p>$iconimage</a> ";
                $attfile .= format_text("<a href=\"$data->embed_code\">" . $data->con_name . "</a></li>", FORMAT_HTML, array('context' => $context)) . "</li>";
            }
            $output_lcms .= html_writer::tag('ul', $attfile, array('class' => "detail-attachment"));
        }else if ($data->con_type == 'html') {
            //html일경우 콘텐츠 보기
            $viewer_url = 'viewer/package.php';
            $width = '1024';
            $height = '800';
            $attfile .= '<li><a href="#viewer" class="blue_btn" onclick="load_viewer_popup(' . $data->con_id . ');">' . get_string('viewcontent', 'lcms') . '</a></li>';
            $output_lcms .= html_writer::tag('ul', $attfile, array('class' => "detail-attachment"));
        } else if ($data->con_type == 'video') {
            $transcodingurl = get_config('moodle','vodurl');
            //동영상 콘텐츠 보기
            //$viewer_url = 'viewer/video_player.php?id=' . $data->con_id;
            $file = $DB->get_record('lcms_contents_file', array('con_seq' => $id));
            $viewer_url = $transcodingurl.$data->data_dir.'/'.$file->filename;
            $viewer_url = '/local/repository/viewer/video_player.php?id=' . $data->con_id .'&site=siteadmin';
            $attfile .= '<div class="v-wrap"><iframe  id="vod_viewer" src="' . $viewer_url . '"  width="900px" height="720px"></iframe></div>';
            $output_lcms .= html_writer::tag('div', $attfile, array('class' => ""));
        } else if  ($data->con_type == 'embed' || $data->con_type == 'visangurl') { 
                //Embed 콘텐츠 보기
                $viewer_url = 'viewer/video_player_embed.php?id=' . $data->con_id;
                $attfile .= '<div class="v-wrap"><iframe id="vod_viewer" src="' . $viewer_url . '"></iframe></div>';
                $output_lcms .= html_writer::tag('div', $attfile, array('class' => ""));
        } else if ($data->con_type == 'html2') {
            //html2일경우 콘텐츠 보기
            $viewer_url = 'viewer/package2.php';
            $width = '990'; 
            $height = '741';
            $attfile .= '<li><a href="#viewer" class="blue_btn" onclick="load_viewer_popup(' . $data->con_id . ');">' . get_string('viewcontent', 'lcms') . '</a></li>';
            $output_lcms .= html_writer::tag('ul', $attfile, array('class' => "detail-attachment"));
        }

        $output_lcms .= html_writer::end_tag('div');

//강의설명표시 
        if (!empty($data->con_des)) {
            $output_lcms .= html_writer::tag('div', $data->con_des, array('class' => 'detail-contents'));
        }

        $output_lcms .= html_writer::end_tag('div');

        echo $output_lcms;
        echo $fileobj;
        ?>
        <div class="table-footer-area">
            <div class="btn-area btn-area-right">
                <button class="blue-form blue_btn" onclick="location.href = 'add.php?mode=edit&id=<?php echo $id; ?>&ref=<?php echo $ref; ?>'"><?php echo get_string('detail:edit', 'local_repository'); ?></button>
<!--                <button class="blue-form gray_btn" onclick="if (confirm('<?php echo get_string('delete_content', 'local_repository'); ?>')) {
                            location.href = 'deletes.php?id=<?php echo $data->con_id; ?>&ref=<?php echo $ref; ?>'
                        }"><?php echo get_string('detail:delete', 'local_repository'); ?></button>-->
                <button class="red-form gray_btn" onclick="location.href = 'index.php?userid=<?php echo $userid; ?>'"><?php echo get_string('detail:list', 'local_repository'); ?></button>
            </div>
        </div> 
    </div>
</div>
</div>
</section>
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot . '/mod/lcms/jquery-ui-1.10.3.custom.css'; ?>" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/mod/lcms/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/mod/lcms/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/mod/lcms/player/js/jwplayer.js"></script>
<?php include_once 'loading.php'; ?>
<script type="text/javascript"> 
    /** 동영상 iframe 높이 조절 **/
//    $("iframe").load(function(){
//        $('iframe').height($('#vod_viewer').contents().find('body').height());
//    });

    function load_viewer_popup(id, qua) {

        var width = '<?php echo $width; ?>', height = '<?php echo $height; ?>';
        if ($(window).width() < width) {
            width = $(window).width();
        }

        if ($(window).height() < height) {
            height = $(window).height();
        }

        var tag = $("<div id='viewer_popup' style='overflow:hidden;'></div>");

        $.ajax({
            url: '<?php echo $viewer_url; ?>',
            data: {
                id: id,
                qua: qua
            },
            success: function (data) {

                $('body').css({'overflow': 'hidden'});

                tag.html(data).dialog({
                    title: '<?php echo $data->con_name; ?>',
                    modal: true,
                    width: width,
                    height: height,
                    close: function () {
                        //if($('video').length==0) jwplayer('mediaplayer').pause(); 
                        $(this).dialog('destroy').remove();
                        $('body').css({'overflow': 'auto'});
                    }
                }).dialog('open');

            }

        });
    }

    function close_viewer_popup() {
        $('#viewer_popup').dialog('destroy').remove();
        $('body').css({'overflow': 'auto'});
    }
</script>

<?php include_once($CFG->dirroot.'/local/management/footer.php'); ?>







