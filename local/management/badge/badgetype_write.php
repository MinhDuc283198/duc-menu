<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
$pagesettings = array(
    'title' => get_string('badgetypemanagement', 'local_management'),
    'heading' => get_string('badgetypemanagement', 'local_management'),
    'subheading' => '',
    'menu' => 'badgetype',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

$context = context_system::instance();
$PAGE->set_context($context);

include_once($CFG->dirroot . '/local/management/header.php');
$lang = current_language();
$id = optional_param('id', 0, PARAM_INT); //id값이 있으면 수정

if ($id) {
    $value = $DB->get_record('lmsdata_badge_category', array('id' => $id));
    $maxorder = $DB->get_field_sql("select MAX(ordered)  from {lmsdata_badge_category} ");
}

$orders = $DB->get_records_sql("select id,ordered,title_$lang name from {lmsdata_badge_category} order by ordered asc");
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form method='post' id='copyright_form' action='badgetype_submit.php'>
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <table cellspadding="0" cellspacing="0" class="detail">
                        <tr>
                            <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata') . "(KO)"; ?></td>
                            <td class="field_value"><input type="text" title="title" id="title_ko" value="<?php echo (!empty($value->title_ko)) ? $value->title_ko : ""; ?>" name="title_ko" class="w_100"/></td>
                        </tr>
                        <tr>
                            <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata') . "(EN)"; ?></td>
                            <td class="field_value"><input type="text" title="title" id="title_en" value="<?php echo (!empty($value->title_en)) ? $value->title_en : ""; ?>" name="title_en" class="w_100"/></td>
                        </tr>
                        <tr>
                            <td class="field_title"><font color="#F00A0D"  size="3px;"><strong>*</strong></font><?php echo get_string('title', 'local_lmsdata') . "(VI)"; ?></td>
                            <td class="field_value"><input type="text" title="title" id="title_vi" value="<?php echo (!empty($value->title_vi)) ? $value->title_vi : ""; ?>" name="title_vi" class="w_100"/></td>
                        </tr>
                        <tr class="">
                            <td class="field_title"><span class="required red">*</span>위치</td>
                            <td class="field_value">
                                <select class="w_160" name="step">
                                    <option value="0"  >맨 앞</option>
                                    <?php
                                    if (isset($orders)) {
                                        $disabled = "";
                                        foreach ($orders as $order) {
                                            if ($id) {
                                                $disabled = $order->ordered == $value->ordered ? "selected" : "";
                                            }
                                            ?>     

                                            <option value="<?php echo $order->ordered ?>" <?php echo $disabled ?>><?php echo $order->name ?> ( <?php echo $order->ordered ?> )</option>
                                        <?php }
                                    }
                                    ?>
                                    <option value="-1" >맨 뒤</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">사용여부</td>
                            <td class="field_value"> 
                                <select name="isused" class="w_90">
                                    <option value="1">사용</option>
                                    <option value="2" <?php
                                            if (isset($category) && $category->isused == 2) {
                                                echo 'selected';
                                            }
                                            ?>>미사용</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div id="btn_area">
                        <input type="button" id="copyright_submit" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/badge/badgetype.php"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                        <input type="button" id="popup_delete_button" class="red_btn" onclick="history.back()"value="<?php echo get_string('cancle', 'local_lmsdata'); ?>" style="float: left;"/>
                        <input type="button" id="popup_delete_button" class="red_btn" onclick="muldel(<?php echo $id ?>)"value="<?php echo get_string('del', 'local_management'); ?>" style="float: left;"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>


<script type="text/javascript">


    $(document).ready(function () {

        $('#copyright_submit').click(function () {
            $('#copyright_form').submit();
        });
        $('#copyright_form').submit(function (event) {
            var title_ko = $("#title_ko").val();
            var title_en = $("#title_en").val();
            var title_vn = $("#title_vi").val();
            if (title_ko.trim() == '' || title_en.trim() == '' || title_vn.trim() == '') {
                alert("<?php echo get_string('copy:alert', 'local_management'); ?>");
                return false;
            }
            ;

        });
    });
    function muldel(id) {
        var ids = [];
        if (confirm("<?php echo get_string("goods:delalert", "local_management") ?> ")) {
            $.ajax({
                url: "badgetype_delete.php",
                type: 'POST',
                dataType: 'json',
                data: {id: id},
                success: function (data) {
                    if (data) {
                        var str = data.join(",");
                        alert(str +" <?php echo get_string('usingtype', "local_management"); ?> ")
                    } else {
                        location.href = "badgetype.php";
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }
            });
        }

    }
</script>
