<?php
require_once(__DIR__ . '/../../../config.php');
$cid = optional_param('cid', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);
$lang= current_language();
$orders = $DB->get_records_sql("select id,ordered,title_$lang name from {lmsdata_badge} where category = ? order by ordered asc",array($cid));
if($id){
    $value = $orders[$id];
}
?>

    <td class="field_title"  colspan="2"> <span class=" red">*</span>위치</td>
    <td class="field_value">
        <select class="w_160" name="step">
            <option value="0"  >맨 앞</option>
            <?php
            if (isset($orders)) {
                $disabled = "";
                foreach ($orders as $order) {
                    if ($id) {
                        $disabled = $order->ordered == $value->ordered ? "selected" : "";
                    }
                    ?>     

                    <option value="<?php echo $order->ordered ?>" <?php echo $disabled ?>><?php echo $order->name ?> ( <?php echo $order->ordered ?> )</option>
                    <?php
                }
            }
            ?>
            <option value="-1" >맨 뒤</option>
        </select>
    </td>
