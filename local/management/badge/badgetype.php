<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';

$pagesettings = array(
    'title' => get_string('badgetypemanagement', 'local_management'),
    'heading' =>  get_string('badgetypemanagement', 'local_management'),
    'subheading' => '',
    'menu' => 'badgetype',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('badgetype');
    redirect(get_login_url());
}

$context = context_system::instance();
//require_capability('moodle/site:config', $context);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT); // $id 있으면 상세페이지
$type = optional_param('type', 0, PARAM_INT); 

$PAGE->set_context($context);

$totalcount = $DB->count_records_select('lmsdata_badge_category',array());


include_once($CFG->dirroot . '/local/management/header.php');

?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                    <table cellpadding="0" cellspacing="0" class="normal" width="100%">
                        <caption class="hidden-caption"><?php echo get_string('copyset', 'local_management'); ?></caption>
                        <thead>
                            <tr>
                                <th scope="row" width="5%">No.</th>
                                <th scope="row" width="35%"><?php echo get_string('name', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('copy:writer', 'local_management'); ?></th>
                                <!--<th scope="row" width="10%"><?php echo get_string('order','local_management'); ?></th>-->
                                <th scope="row" width="10%"><?php echo get_string('isused', 'local_management'); ?></th>
                                <th scope="row" width="10%"><?php echo get_string('timecreated', 'local_management'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $startnum = $totalcount - (($page - 1) * $perpage);
                            $offset = 0;
                            if ($page != 0) {
                                $offset = ($page - 1) * $perpage;
                            }
                            $sql = "SELECT lbc.*,u.firstname FROM {lmsdata_badge_category} lbc JOIN {user} u on lbc.userid = u.id  ORDER BY id DESC";
                            $badges = $DB->get_records_sql($sql,array(),$offset,$perpage);
                            if($badges){
                                foreach($badges as $badge){                                  ?>
                                    <tr>
                                        <td class="number"><?php echo $startnum; ?></td>
                                        <td class="title"><a href="./badgetype_write.php?id=<?php echo $badge->id; ?>"><?php echo $badge->title_ko.' / '.$badge->title_en.' / '.$badge->title_vi ?></a></td>
                                        <td><?php echo $badge->firstname; ?></td> 
                                        <!--<td><?php echo $badge->ordered; ?></td>--> 
                                        <td><?php echo $badge->isused == 1 ? get_string('used','local_management') : get_string('notused','local_management') ; ?></td> 
                                        <td><?php echo date("Y-m-d", $badge->timecreated); ?></td>
                                     </tr>
                                <?php
                                $startnum--;
                                }
                            } else {
                                ?>
                                    <tr>
                                        <td colspan = "7"> <?php  echo get_string('nobadge', 'local_management'); ?> </td>
                                    </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        </tbody>
                      
                    </table>
                     <div id="btn_area">
                         <?php if(!$id){
                             ?>
                            <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/badge/badgetype_write.php"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                             <?php
                         } else if($id) {?>
                            <input type="button"onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/badge/badgetype_write.php"' class="red_btn" value="목록" style="float: left;"/>
                         <?php } ?>
                    </div>
            <div class="pagination">
            <?php
            print_paging_navbar($totalcount, $page, $perpage, $CFG->wwwroot . '/local/management/badge/badgetype.php', array('id'=>$id,'type'=>$type));
            ?>
            </div><!-- Pagination End -->
            </div>
                
        </div>  
    </div>
 </section>


<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>
    
     function muldel() {
          var ids = [];
            $("input[name=delchk]:checked").each(function (index) {
                ids[index] = $(this).siblings(".idval").val();
            });
                    
         $.ajax({
            url: "badgetype_delete.php",
            type: 'POST',
            dataType: 'json',
            data: {ids: ids},
            success: function (data) {
                var str = data.join(","); 
                alert(str+" 는(은) 사용중인 유형입니다. <?php echo get_string('usingtype',"local_management");?> ")
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });

    }
    $(function () {
        $("#allcheck").click(function () {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function () {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function () {
                    this.checked = false;
                });
            }
        });

        $("#copyright_delete_button").click(function () {
            if (confirm("<?php echo get_string('job:delalert', 'local_management'); ?>")) {
                muldel();
            }
        });
        
    });
</script>