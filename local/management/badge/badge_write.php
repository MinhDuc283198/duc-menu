<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
$pagesettings = array(
    'title' => get_string('badgemanagement', 'local_management'),
    'heading' => get_string('badgemanagement', 'local_management'),
    'subheading' => '',
    'menu' => 'badge',
    'js' => array(
    ),
    'css' => array(),
    'nav_bar' => array()
);

$context = context_system::instance();
$PAGE->set_context($context);

include_once($CFG->dirroot . '/local/management/header.php');

$id = optional_param('id', 0, PARAM_INT); //id값이 있으면 수정

if ($id) {
    $badge = $DB->get_record('lmsdata_badge', array('id' => $id));
    $img = local_lmsdata_get_imgpath(1, 'bedge', $badge->id);
}
$lang = current_language();
$badgecategorys = $DB->get_records_sql("select id,title_$lang title from {lmsdata_badge_category} where isused = ? order by id desc",array(1));
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-body">
                <form id="frm_popup_submit" class="popup_submit" enctype="multipart/form-data" action="badge_submit.php?id=<?php echo $id ?>" method="POST">
                    <input type="hidden" title="id" class="w100" name="id" value="<?php echo $badge->id != 0 ? $badge->id : 0 ?>"/>
                    <table cellpadding="0" cellspacing="0" class="detail">    
                        <tbody>
                            <tr>
                                <td class="field_title" colspan="2"><?php echo get_string('badgetype', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value"  >
                                    <select name='category' id='publisher' class="w_160">
                                        <?php
                                        echo '<option value=0  >' . get_string('typeselect', 'local_management') . '</option>';
                                        foreach ($badgecategorys as $ca) {
                                            $select = '';
                                            if ($ca->id == $badge->category) {
                                                $select = 'selected';
                                            }

                                            echo '<option value=' . $ca->id . ' ' . $select . ' >' . $ca->title . '</option>';
                                            if ($ca->csid) {
                                                $set = '<input type="hidden" readonly=""  name="pub_setid" value="' . $ca->csid . '"/> ';
                                            }
                                        }
                                        echo $set;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title"  rowspan="3"><?php echo get_string('badgename', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_title"><?php echo get_string('vi', 'local_management'); ?></td>
                                <td class="field_value" >
                                    <input type="text" title="id" class="w100 required" name="title_vi" value="<?php echo (!empty($badge->title_vi)) ? $badge->title_vi : ""; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title"><?php echo get_string('ko', 'local_management'); ?></td>
                                <td class="field_value"  >
                                    <input type="text" title="id" class="w100 required" name="title_ko" value="<?php echo (!empty($badge->title_ko)) ? $badge->title_ko : ""; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title"><?php echo get_string('en', 'local_management'); ?></td>
                                <td class="field_value"  >
                                    <input type="text" title="id" class="w100 required" name="title_en" value="<?php echo (!empty($badge->title_en)) ? $badge->title_en : ""; ?>"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title" colspan="2"><?php echo get_string('badgecode', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_value"  >
                                    <span><?php echo ($badge->code) ? $badge->code : '※' . get_string('code_text', 'local_management') ?></span>
                                </td>
                            </tr>
                            <tr class = "thumbnailcl">                
                                <td class="field_title" colspan="2"> <?php echo get_string('badgeimage', 'local_management') ?>(png) <span class="red">*</span><td>
                                    <div class="fileBox" > 
                                        <input type="file" title="thumnail" name="script" size="100" value="" id="captionFile" class="uploadBtn <?php echo $id != 0 ? '' : 'required' ?>" accept="image/png"><span>(65*(최대)80px)</span>
                                        <?php if ($id) { ?>
                                            <span id ='file' name='file_link'>
                                                <img src="<?php echo $img['path'] ?>" class="icon"/>
                                            </span>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="field_title"  rowspan="3"><?php echo get_string('badgeexplanation', 'local_management') ?> <span class="red">*</span></td>
                                <td class="field_title" ><?php echo get_string('vi', 'local_management'); ?></td>
                                <td class="field_value"  >
                                    <textarea class="ta required" rows="4" name="content_vi"><?php echo (!empty($badge->content_vi)) ? $badge->content_vi : ""; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('ko', 'local_management'); ?></td>
                                <td class="field_value" >
                                    <textarea class="ta required" rows="4" name="content_ko"><?php echo (!empty($badge->content_ko)) ? $badge->content_ko : ""; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="field_title" ><?php echo get_string('en', 'local_management'); ?></td>
                                <td class="field_value" >
                                    <textarea class="ta required" rows="4" name="content_en"><?php echo (!empty($badge->content_en)) ? $badge->content_en : ""; ?></textarea>
                                </td>
                            </tr>
                        <tr class="cateorder">
                            
                        </tr>
                        <tr>
                            <td class="field_title"  colspan="2"><?php echo get_string('isused', 'local_management') ?> <span class="red">*</span></td>
                            <td class="field_value" > 
                                <input type="radio" name="used" value="1" <?php echo ($badge->used == 1) && $id ? "checked" : "checked" ?> ><?php echo get_string('used', 'local_management') ?>
                                <input type="radio" name="used" value="2" <?php echo ($badge->used == 2) && $id ? "checked" : "" ?>><?php echo get_string('notused', 'local_management') ?>   
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div id="btn_area" class="mg-bt30">
                        <input type="submit" id="add_user" class="blue_btn" value="<?php echo get_string('save', 'local_management'); ?>" style="float: right;" onclick="return bedge_submit_check()"   />
                        <input type="button" id="admin_list" class="normal_btn" onclick="location.href = '/local/management/badge/badge.php'" value="<?php echo get_string('list', 'local_management'); ?>" style="float: left;" />
                    </div> <!-- Bottom Button Area -->
                </form>
            </div>
        </div>
    </div>
</section>
<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>


<script type="text/javascript">


    $(document).ready(function () {
        
        $('#copyright_submit').click(function () {
            $('#copyright_form').submit();
        });
        $('#copyright_form').submit(function (event) {
            var title_ko = $("#title_ko").val();
            var title_en = $("#title_en").val();
            var title_vn = $("#title_vi").val();
            if (title_ko.trim() == '' || title_en.trim() == '' || title_vn.trim() == '') {
                alert("<?php echo get_string('copy:alert', 'local_management'); ?>");
                return false;
            }
            ;

        });
        get_orderofcategory();
        $('select[name=category]').change(function () {
            get_orderofcategory();
        });

        $("input[name=script]").change(function () {
            if ($(this).val() != "") {
                var ext = $(this).val().split(".").pop().toLowerCase();
                if ($.inArray(ext, ["png"]) == -1) {
                    alert("<?php echo get_string('png','local_management')?>");
                    $(this).val("");
                    return;
                }
            }
        });
    });
    function get_orderofcategory(cid) {
        var cid = $("select[name=category]").val();
        if (cid != 0) {
            var id = <?php echo $id ? $id : 0 ?>;
            $.ajax({
                url: "orderofcategory.php",
                type: 'POST',
                data: {id: id, cid: cid},
                success: function (data) {
                    $(".cateorder").empty().append(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }
            });
        }else{
            $(".cateorder").empty();
        }
    }


    function bedge_submit_check() {
        var check = 1;
        $.each($('#frm_popup_submit').find(".required"), function (key, val) {
            if (!value_null_check(val)) {
                check = 0;
                var msg = $(this).closest('td').prev().prev().text() == '' ?  $(this).closest('td').prev().text() : $(this).closest('td').prev().prev().text();
                msg = msg.replace('*','');
                alert(msg+'을 입력해주세요');
                return false;
            }
        });
        if (check) {
            return true;
        } else {
            return false;
        }
    }
    function value_null_check(select) {
        if ($(select).val() == null || $(select).val() == 0 || $(select).val() == undefined || $(select).val() == '') {
            return false;
        } else {
            return true;
        }
    }
</script>
