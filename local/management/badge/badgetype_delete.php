<?php

require_once(__DIR__ . '/../../../config.php');


require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('popup_submit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$id= optional_param("id", 0, PARAM_INT);
$lang= current_language();

if ($id) {
        if (!$DB->record_exists('lmsdata_badge', array( 'category' => $id))) {
              $regarcyordered = $DB->get_field('lmsdata_badge_category', 'ordered', array('id' => $id));
              $DB->execute("update  {lmsdata_badge_category} set ordered = ordered - 1 where  ordered > ? ", array($regarcyordered));
              $DB->delete_records('lmsdata_badge_category', array('id'=>$id));
        }else{
            $data[] = $DB->get_field('lmsdata_badge_category',"title_$lang",array("id"=>$id));
        }
}



echo(json_encode($data, JSON_UNESCAPED_UNICODE));
