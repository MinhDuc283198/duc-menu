<?php
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot . '/local/management/lib.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');
require_once($CFG->dirroot . '/local/management/menu/lib.php');
require_once dirname(dirname(__FILE__)) . '/lib/paging.php';


$searchtext = optional_param('searchtext', '', PARAM_RAW);
$badgetype = optional_param('badgetype', '', PARAM_INT);
$used = optional_param('used', '', PARAM_INT);

$pagesettings = array(
    'title' => get_string('badgemanagement', 'local_management'),
    'heading' => get_string('badgemanagement', 'local_management'),
    'subheading' => '',
    'menu' => 'badge',
    'js' => array(),
    'css' => array(),
    'nav_bar' => array()
);
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('badge');
    redirect(get_login_url());
}
$lang = current_language();
$context = context_system::instance();
//require_capability('moodle/site:config', $context);
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT); // $id 있으면 상세페이지
$type = optional_param('type', 0, PARAM_INT);

$PAGE->set_context($context);


$badgecategorys = $DB->get_records_sql("select id,title_$lang title from {lmsdata_badge_category}");

$where = '';
$param = array();
if ($searchtext) {
    $addwhere[] = "( lb.title_vi LIKE :searchtext1  or lb.title_ko LIKE :searchtext2  or lb.title_en LIKE :searchtext3 or lb.code LIKE :searchtext4 ) ";
    $param['searchtext1'] = "%$searchtext%";
    $param['searchtext2'] = "%$searchtext%";
    $param['searchtext3'] = "%$searchtext%";
    $param['searchtext4'] = "%$searchtext%";
}
if ($badgetype) {
    $addwhere[] = " lb.category = :badgetype ";
    $param['badgetype'] = $badgetype;
}
if ($used) {
    $addwhere[] = " lb.used = :used ";
    $param['used'] = $used;
}
$perpage_array = [10, 20, 30, 50];
if (!empty($addwhere)) {
    $where = " WHERE " . implode(' AND ', $addwhere);
}

$offset = 0;
if ($page != 0) {
    $offset = ($page - 1) * $perpage;
}
$sql = "SELECT lb.id 
       ,lb.title_$lang name
       ,lb.timecreated
       ,lb.used
       ,lb.code
       ,lbc.title_$lang cname ";
$from = " FROM {lmsdata_badge} lb 
 JOIN {lmsdata_badge_category} lbc on lb.category = lbc.id  
 ";
$order = "ORDER BY lb.id DESC ";
$badges = $DB->get_records_sql($sql . $from . $where . $order, $param, $offset, $perpage);
$totalcount = $DB->count_records_sql("SELECT count(lb.id)" . $from . $where, $param);

$startnum = $totalcount - (($page - 1) * $perpage);
include_once($CFG->dirroot . '/local/management/header.php');
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-body">
                <form name="course_search" id="course_search" class="search_area" action="badge.php" method="get">
                    <input type="hidden" name="page" value="1" />
                    <input type="hidden" name="perpage" value="<?php echo $perpage ?>" />
                    <input type="hidden" value="0" name="excel" >
                    <div>
                        <label><?php echo get_string('badgetype', 'local_management') ?></label>
                        <select title="badgetype" name="badgetype"   class="w_160">

<?php
echo '<option value=0  >' . get_string('nonetype', 'local_management') . '</option>';
foreach ($badgecategorys as $ca) {
    $select = '';
    if ($ca->id == $badgetype) {
        $select = 'selected';
    }

    echo '<option value=' . $ca->id . ' ' . $select . ' >' . $ca->title . '</option>';
}
echo $set;
?>
                        </select>
                    </div>
                    <div>
                        <label><?php echo get_string('isused', 'local_management') ?></label>
                        <label class="mg-bt10"><input type="radio" name="used" value="0" <?php echo $used == 0 ? 'checked' : ''; ?>><?php echo get_string('all', 'local_management'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="used" value="1" <?php echo $used == 1 ? 'checked' : ''; ?>><?php echo get_string('used', 'local_management'); ?></label>                        
                        <label class="mg-bt10"><input type="radio" name="used" value="2" <?php echo $used == 2 ? 'checked' : ''; ?>><?php echo get_string('notused', 'local_management'); ?></label>     
                    </div>

                    <div>
                        <label><?php echo get_string('search', 'local_management') ?></label>
                        <input type="text" title="search" name="searchtext" value="<?php echo $searchtext; ?>" placeholder="<?php echo get_string('searchholder', 'local_management'); ?>"  class="search-text w100"/>
                        <input type="submit" class="search_btn" value="<?php echo get_string('search', 'local_management'); ?>"/>     
                        <input type="button" class="search_btn" id="reset" value="<?php echo get_string('reset', 'local_management'); ?>"/>   
                    </div>
                </form>
                <span><?php echo get_string('total', 'local_management', $totalcount) ?></span>
                <select name="perpage2" class="perpage2">
<?php
foreach ($perpage_array as $pa) {
    $selected = '';
    if ($pa == $perpage) {
        $selected = 'selected';
    }
    ?>
                        <option value="<?php echo $pa ?>" <?php echo $selected ?>><?php echo $pa ?><?php echo get_string('viewmore', 'local_management') ?>
                        <?php
                    }
                    ?>
                </select>

<!--<input type="button" class="blue_btn" style="margin-right: 10px;" value="<?php echo get_string('excell_down', 'local_lmsdata'); ?>" onclick="excel_download();"/>--> 
                <div style="float:right;">            
                    <input type="submit" class="blue_btn" value="<?php echo get_string('add', 'local_lmsdata') ?>" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/badge/badge_write.php"' /> 
                </div>

                <table cellpadding="0" cellspacing="0" class="normal" width="100%">
                    <caption class="hidden-caption"><?php echo get_string('copyset', 'local_management'); ?></caption>
                    <thead>
                        <tr>
                            <th scope="row" width="5%">No.</th>
                            <th scope="row" width="10%"><?php echo get_string('badgetype', 'local_management'); ?></th>
                            <th scope="row" width="10%"><?php echo get_string('badgeimage', 'local_management'); ?></th>
                            <th scope="row" width="35%"><?php echo get_string('badgename', 'local_management'); ?></th>
                            <th scope="row" width="10%"><?php echo get_string('badgecode', 'local_management'); ?></th>
                            <th scope="row" width="15%"><?php echo get_string('timecreated', 'local_management'); ?></th>
                            <th scope="row" width="15%"><?php echo get_string('badgeusing', 'local_management'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
if ($badges) {
    foreach ($badges as $badge) {
        ?>
                                <tr>
                                    <td class="number"><?php echo $startnum; ?></td>
                                    <td><?php echo $badge->cname; ?></td> 
                                    <td><img src="<?php echo local_lmsdata_get_imgpath(1, 'bedge', $badge->id)['path']; ?> "></td> 
                                    <td class="title"><a href="./badge_write.php?id=<?php echo $badge->id; ?>"><?php echo $badge->name ?></a></td>
                                    <td><?php echo $badge->code; ?></td> 
                                    <td><?php echo date("Y-m-d", $badge->timecreated); ?></td> 
                                    <td><?php echo $badge->used == 1 ? get_string('used', 'local_management') : get_string('notused', 'local_management'); ?></td> 
                                </tr>
        <?php
        $startnum--;
    }
} else {
    ?>
                            <tr>
                                <td colspan = "7"> <?php echo get_string('nobadge', 'local_management'); ?> </td>
                            </tr>
    <?php
}
?>
                    </tbody>
                    </tbody>

                </table>
                <div id="btn_area">
<?php if (!$id) {
    ?>
                        <input type="button" class="blue_btn right" onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/badge/badge_write.php"' value="<?php echo get_string('add', 'local_lmsdata'); ?>" style="float: right;" />
                        <!--<input type="button" id="copyright_delete_button" class="red_btn" value="<?php echo get_string('delete', 'local_lmsdata'); ?>" style="float: left;"/>-->
    <?php } else if ($id) {
    ?>
                        <input type="button"onclick='document.location.href = "<?php echo $CFG->wwwroot ?>/local/management/badge/badge_write.php"' class="red_btn" value="목록" style="float: left;"/>
                    <?php } ?>
                </div>
                <div class="pagination">
<?php
print_paging_navbar($totalcount, $page, $perpage, $CFG->wwwroot . '/local/management/badge/badge.php', array('id' => $id, 'type' => $type));
?>
                </div><!-- Pagination End -->
            </div>

        </div>  
    </div>
</section>


<?php include_once($CFG->dirroot . '/local/management/footer.php'); ?>

<script>

    function muldel() {
        var ids = [];
        $("input[name=delchk]:checked").each(function (index) {
            ids[index] = $(this).siblings(".idval").val();
        });

        $.ajax({
            url: "badge_delete.php",
            type: 'POST',
            dataType: 'json',
            data: {ids: ids},
            success: function (data) {
                window.location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });

    }
    $(function () {
        $(".perpage2").change(function () {
            var perpage = $('select[name=perpage2]').val();
            $('input[name=perpage]').val(perpage);
            $("form[id=course_search]").submit();
        });
        $('#reset').click(function () {
            $('select[name="badgetype"]').val('');
            $('input[name=searchtext]').val('');
            $("input:radio[name='used']").val('');
            $("form[id=course_search]").submit();
        });

        $("#allcheck").click(function () {
            var chk = $("#allcheck").is(":checked");

            if (chk) {
                $(".chkbox input").each(function () {
                    this.checked = true;
                });
            } else {
                $(".chkbox input").each(function () {
                    this.checked = false;
                });
            }
        });

        $("#copyright_delete_button").click(function () {
            if (confirm("<?php echo get_string('job:delalert', 'local_management'); ?>")) {
                muldel();
            }
        });

    });
</script>