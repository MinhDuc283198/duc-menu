<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require_once(__DIR__ . '/../../../config.php');
require_once $CFG->dirroot . '/local/management/lib/contents_lib.php';
require_once $CFG->dirroot . '/lib/form/filemanager.php';
require_once $CFG->dirroot . '/lib/filelib.php';
require_once $CFG->dirroot . '/lib/phpexcel/PHPExcel/Shared/PCLZip/pclzip.lib.php';

// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('badge_submit.php');
    redirect(get_login_url());
}

$context = context_system::instance();
$fs = get_file_storage();
$bedge = new stdClass();
foreach ($_REQUEST as $key => $val) {
    $bedge->$key = $val;
}
$bedge->userid = $USER->id;

if (!$bedge->id) {
    if (!$bedge->step) {
        $DB->execute("update  {lmsdata_badge} set ordered = ordered + 1 where category = ?", array($bedge->category));
        $bedge->ordered = 1;
    } else if ($bedge->step == -1) {
        $bedge->ordered = $DB->get_field_sql("select MAX(ordered)  from {lmsdata_badge} where category = ?", array($bedge->category)) + 1;
    } else {
        $bedge->ordered = $bedge->step;
        $DB->execute("update  {lmsdata_badge} set ordered = ordered + 1 where ordered >= ? and category = ?", array($bedge->step, $bedge->category));
    }
    $bedge->code = date('y', time()) . 'B' . sprintf("%04d", $bedge->ordered);
    $bedge->timecreated = time();

    $newbedge = $DB->insert_record('lmsdata_badge', $bedge);
    if (!empty($_FILES['script']['tmp_name'])) {
        $file_record = array(
            'contextid' => $context->id,
            'component' => 'local_lmsdata',
            'filearea' => 'bedge',
            'itemid' => $newbedge,
            'filepath' => '/',
            'filename' => $_FILES['script']['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $USER->id,
            'author' => fullname($USER),
            'license' => 'allrightsreserved',
            'sortorder' => 0
        );
        $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['script']['tmp_name']);
    }
} else {
    $regarcy = $DB->get_record('lmsdata_badge',  array('id' => $bedge->id));
    if ($regarcy->category == $bedge->category) {
        if ($regarcy->ordered != $bedge->step) {
            if (!$bedge->step) {
                $DB->execute("update  {lmsdata_badge} set ordered = ordered + 1 where  ordered < ? and category = ?", array($regarcy->ordered, $bedge->category));
                $bedge->ordered = 1;
            } else if ($bedge->step == -1) {
                $DB->execute("update  {lmsdata_badge} set ordered = ordered - 1 where  ordered > ? and category = ?", array($regarcy->ordered, $bedge->category));
                $bedge->ordered = $DB->get_field_sql("select MAX(ordered)  from {lmsdata_badge} where category = ?", array($bedge->category)) + 1;
            } else if ($bedge->step > 0 && $regarcy->ordered > $bedge->step) {
                $bedge->ordered = $bedge->step;
                $DB->execute("update  {lmsdata_badge} set ordered = ordered + 1 where ordered >=  ? and ordered < ? and category = ?", array($bedge->step, $regarcy->ordered, $bedge->category));
            } else if ($bedge->step > 0 && $regarcy->ordered < $bedge->step) {
                $bedge->ordered = $bedge->step - 1;
                $DB->execute("update  {lmsdata_badge} set ordered = ordered - 1 where ordered < ? and ordered > ? and category = ?", array("step" => $bedge->step, $regarcy->ordered, $bedge->category));
            }
        }
    } else {
        $DB->execute("update  {lmsdata_badge} set ordered = ordered - 1 where  ordered > ? and category = ?", array($regarcy->ordered, $regarcy->category));
        if (!$bedge->step) {
            $DB->execute("update  {lmsdata_badge} set ordered = ordered + 1 where category = ?", array($bedge->category));
            $bedge->ordered = 1;
        } else if ($bedge->step == -1) {
            $bedge->ordered = $DB->get_field_sql("select MAX(ordered)  from {lmsdata_badge} where category = ?", array($bedge->category)) + 1;
        } else {
            $bedge->ordered = $bedge->step;
            $DB->execute("update  {lmsdata_badge} set ordered = ordered + 1 where ordered >= ? and category = ?", array($bedge->step, $bedge->category));
        }
    }
    $bedge->timemodified = time();
    $DB->update_record('lmsdata_badge', $bedge);
    if (!($_FILES['script']['name'] == null || $_FILES['script']['name'] == '')) {
        $file_del = 1;
        $overlap_files = $fs->get_area_files($context->id, 'local_lmsdata', 'bedge', $bedge->id);
        $cnt = 0;
        foreach ($overlap_files as $file) {
            if ($file->get_filesize() > 0) {
                if ($file_del) {
                    $filename = $file->get_filename();
                    $file->delete();
                }
                $cnt++;
            }
        }

        if (!empty($_FILES['script']['tmp_name'])) {
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'local_lmsdata',
                'filearea' => 'bedge',
                'itemid' => $bedge->id,
                'filepath' => '/',
                'filename' => $_FILES['script']['name'],
                'timecreated' => time(),
                'timemodified' => time(),
                'userid' => $USER->id,
                'author' => fullname($USER),
                'license' => 'allrightsreserved',
                'sortorder' => 0
            );
            $storage_id = $fs->create_file_from_pathname($file_record, $_FILES['script']['tmp_name']);
        }
    }
}
echo '<script type="text/javascript">document.location.href="./badge.php"</script>';



