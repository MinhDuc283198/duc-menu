<?php
 require_once(__DIR__ . '/../../../config.php');
    
    
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string)new moodle_url('popup_submit.php');
    redirect(get_login_url());
}
$context = context_system::instance();
//require_capability('moodle/site:config', $context);

$ids = optional_param_array("ids", 0, PARAM_INT);
   

if($ids){
    foreach($ids as $id){
       $result->$id=$DB->delete_records('lmsdata_badge', array('id'=>$id));
    }
}

    
    
echo(json_encode($result, JSON_UNESCAPED_UNICODE));