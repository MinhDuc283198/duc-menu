<?php
 require_once(__DIR__ . '/../../../config.php');
    
    require_login(0, false);
//    if (isguestuser()) {
//        $SESSION->wantsurl = (string)new moodle_url('popup_submit.php');
//        redirect(get_login_url());
//    }
    $context = context_system::instance();
    //require_capability('moodle/site:config', $context);
    
    $id = optional_param('id', 0, PARAM_INT);
    $title_ko = optional_param('title_ko', "", PARAM_TEXT);
    $title_en = optional_param('title_en', "", PARAM_TEXT);
    $title_vn = optional_param('title_vi', "", PARAM_TEXT);
    $isused = optional_param('isused', '', PARAM_INT);
    $step = optional_param('step', '', PARAM_INT);
    $values = new stdClass();
    $values->title_ko = $title_ko;
    $values->title_en = $title_en;
    $values->title_vi = $title_vn;
    $values->isused = $isused;
    $values->step = $step;

    if(!$id){
        $values->timecreated = time();
        $values->userid = $USER->id;
        if(!$step){
            $DB->execute("update  {lmsdata_badge_category} set ordered = ordered + 1");
            $values->ordered = 1;
        }else if($step == -1 ){
            $values->ordered = $DB -> get_field_sql("select MAX(ordered)  from {lmsdata_badge_category} ")+1;
        }else{
            $values->ordered = $step;
            $DB->execute("update  {lmsdata_badge_category} set ordered = ordered + 1 where ordered >= ? ",array("step"=>$step));
                    
        }
        $DB->insert_record('lmsdata_badge_category',$values);
    } else {
    $values->id = $id;
    $values->timemodified = time();
    $regarcyordered = $DB->get_field('lmsdata_badge_category', 'ordered', array('id' => $id));
    if ($regarcyordered != $step) {
        if (!$step) {
            $DB->execute("update  {lmsdata_badge_category} set ordered = ordered + 1 where  ordered < ? ", array($regarcyordered));
            $values->ordered = 1;
        } else if ($step == -1) {
            $DB->execute("update  {lmsdata_badge_category} set ordered = ordered - 1 where  ordered > ? ", array($regarcyordered));
            $values->ordered = $DB->get_field_sql("select MAX(ordered)  from {lmsdata_badge_category} ") + 1;
        } else if ($step > 0 && $regarcyordered > $step) {
            $values->ordered = $step;
            $DB->execute("update  {lmsdata_badge_category} set ordered = ordered + 1 where ordered >=  ? and ordered < ?", array($step, $regarcyordered));
        } else if ($step > 0 && $regarcyordered < $step) {
            $values->ordered = $step - 1;
            $DB->execute("update  {lmsdata_badge_category} set ordered = ordered - 1 where ordered < ? and ordered > ?", array("step" => $step, $regarcyordered));
        }
    }
    $DB->update_record('lmsdata_badge_category',$values);
    } 
    

    
    
?>
<script>
    window.onload = function(){
        location.href = '<?php echo $CFG->wwwroot;?>/local/management/badge/badgetype.php'
    }
</script>