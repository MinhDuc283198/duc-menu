<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();
?>  
<div id="error">
    <div class="group">
        <!-- logo start -->
        <div class="logo">
            <?php echo theme_oklassedu_get_logo_url(); ?>
        </div>
        <!-- logo end -->
        
        <!-- error message start -->
        <div class="error_message">
            <h2 class="error_title">페이지 오류안내</h2>

            <div class="error_text">
                <p class="orange text-bold">
                    페이지가 없거나 오류가 발생하였습니다.
                </p>
                <p class="text-bold">
                    홈페이지 이용에 불편을 드려 죄송합니다.
                </p>
            </div>
            <p class="go_main">
                <a href="/">
                    <span class="btn tab_btn orange">
                        <img src="/theme/oklassedu/pix/images/home_w.png" alt=""/>
                        메인으로
                    </span>
                </a>
                <a href="javascript:history.back();">
                    <span class="btn tab_btn">
                        &lt;
                        되돌아가기
                    </span>
                </a>
            </p>
        </div>
        <!-- error message start -->
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


