<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/okmanage/manage_form.php';

$id = optional_param('id', 0, PARAM_INT);  // Course ID
$search = optional_param('search', '', PARAM_CLEAN);  // 검색어
$background = optional_param('background', '1', PARAM_RAW);  // 배경

$context = get_context_instance(CONTEXT_COURSE, $id);

require_login();

$PAGE->set_context($context);
$PAGE->set_url('/local/okmanage/index.php?id=' . $id);
$PAGE->set_pagelayout('course');


$course = get_course($id);
$PAGE->set_course($course);


if (!has_capability('moodle/course:manageactivities', $context)) {
    return;
}

$strplural = get_string("pluginname", "local_okmanage");
$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$mform = new okmanage_form(null,array($course));
$courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
if ($mform->is_cancelled()) {
    redirect($courseurl);
} else if ($fromform = $mform->get_data()) {
    
    if($lmsdata_class = $DB->get_record('lmsdata_course',array('courseid'=>$fromform->id))){
        $tablename = 'lmsdata_course';
    }else if($lmsdata_class = $DB->get_record('lmsdata_class',array('courseid'=>$fromform->id))){
        $tablename = 'lmsdata_class';
    }
    
    if($lmsdata_class){   
    
        $lmsdata_class->notice = $fromform->notice;
        $lmsdata_class->useprogress = $fromform->useprogress;
        $lmsdata_class->background = $background;
        
        $DB->update_record($tablename,$lmsdata_class);
    
    }
    
    update_course($fromform, $editoroptions);
        // Set the URL to take them too if they choose save and display.
    redirect($courseurl);
} else {

    if($lmsdata_class = $DB->get_record('lmsdata_course',array('courseid'=>$id))){    
    }else if($lmsdata_class = $DB->get_record('lmsdata_class',array('courseid'=>$id))){    
    }

    $course->notice = $lmsdata_class->notice;
    $course->useprogress = $lmsdata_class->useprogress;

    $mform->set_data($course);

    echo $OUTPUT->header();

    echo $mform->display();

    echo $OUTPUT->footer();

}