<?php
$string['pluginname'] = 'Chỉnh sửa Bài giảng';
$string['pluginnameplural'] = 'Chỉnh sửa Bài giảng';
$string['attstandard'] = 'Thiêu trí tham dự trực tuyến';
$string['course_edit'] = 'Sửa Bài giảng';
$string['notice'] = 'Thông báo';
$string['format'] = 'Đinh dạng bài giảng';
$string['background'] = 'Cài đặt nền';
$string['progress'] = 'Cài đặt tiến trình';
$string['select_background'] = 'Chọn nền';
$string['thumbnail'] = 'Ảnh đại diện';
$string['useprogress'] = 'Tiến độ sử dụng';
$string['onlineatt'] = 'Tham dự trực tuyến';

$string['standard1'] = 'Tiêu chuẩn';
$string['standard2'] = 'Công khai';

$string['offatt'] = 'Tham dự ngoại tuyến';