<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$category = optional_param('category', '', PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);
$searchfield = optional_param('searchfield', '', PARAM_TEXT);
$searcharea = optional_param('searcharea', '', PARAM_TEXT);

$user = $USER->id;
$getusertype = $DB->get_record('lmsdata_user', array('userid' => $user));

$searchlog = new stdClass();
if ($getusertype->usertypecode == 30) {
    $searchresult = 0;
} else {
    $searchlog->userid = $user;
    $searchlog->usertypecode = $getusertype->usertypecode;
    $searchlog->searchword = $searchtext;
    $searchlog->timecreated = time();
    $searchlog->category = $category;
    $searchlog->searcharea = $searcharea;
    if ($searchfield == 'title') {
        $searchlog->titleorcon = 'T';
    } else if ($searchfield == 'contents') {
        $searchlog->titleorcon = 'C';
    } else {
        $searchlog->titleorcon = '';
    }

    $searchresult = $DB->insert_record('lcms_search', $searchlog);
}
echo $searchresult;
