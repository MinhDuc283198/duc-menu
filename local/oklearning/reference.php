<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/oklearning/lib.php');
require_once($CFG->dirroot . '/chamktu/contents/lib.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');

$context = context_system::instance();

require_login();

//교육생은 해당 페이지에 입장 못하게 처리
$usergroup = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));
if ($usergroup == 'rs') {
    redirect($CFG->wwwroot . '?re=6');
}

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("교육 콘텐츠");
$PAGE->set_url(new moodle_url('/local/oklearning/reference.php'));

$PAGE->requires->jquery();
$PAGE->requires->css('/mod/okmedia/viewer/flowplayer7.2.7/skin/skin.css');
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.speed-menu.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.hlsjs.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/player.js', true);
$PAGE->requires->js('/chamktu/contents/category.js', true);
$PAGE->requires->js('/lib/flowplayer/flowplayer-3.2.13.js', true);

$perpage_ref = optional_param('refperpage', 12, PARAM_INT); // PC 모바일 모두 12개씩 출력
$page_ref = optional_param('refpage', 1, PARAM_INT);
$sort = optional_param('sort', 'new', PARAM_TEXT); // category, new, recommend
$searchid = optional_param('searchid', 0, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_RAW);
$searchcate = optional_param('searchcate', 0, PARAM_INT);
$category1 = optional_param('category1', 0, PARAM_INT);

$maxdisplay = 10;


//pc인 경우 9개, 모바일인 경우 8개 출력 => PC 모바일 모두 12개씩 출력으로 변경
//$data = local_lmsdata_browser_check();
//
//if ($data->device == 'M') {
//    $perpage_ref = 8;
//} else if ($data->device == 'P') {
//    $perpage_ref = 9;
//}

echo $OUTPUT->header();
$querycate = '';
$queryadd = '';

if ($searchcate != 0) {
    $querycate = " and con.category = :cate ";
    $params['cate'] = $searchcate;
}
if ($searchcate == 0 && $category1 != 0) {
    $queryadd = "JOIN {lmsdata_category} lc ON lc.id = con.category and lc.parent= :catep 
                                JOIN {lmsdata_category_name} cn ON cn.categoryid = lc.id AND cn.lang = :lang";
    $params['catep'] = $category1;
    $params['lang'] = current_language();
}
$like = '';
if ($searchtext != '') {
    $likeword = '%' . $searchtext . '%';
    $conditional[] = ' con.con_name like ' . '"' . $likeword . '"';
}
$conditional[] = 'rep.status = 1 ';
$like = 'WHERE ' . implode(' AND ', $conditional);
$like .= ' AND con.share_yn = :share_yn AND con.onlymain = :onlymain ';
$nsort = ' ';
$newsql = ' ';
$timestamp = strtotime(date('Y-m-d', time()) . " -1 months");
$params['timestamp'] = $timestamp;
$params['share_yn'] = 'Y';
$params['onlymain'] = 1;
if ($sort == 'recommend') {
    $newsql = "SELECT rep.id, lcf.filename, lcf.filepath, lcf.duration, con.category, "
            . "con.id AS con_id, con.con_name, con.con_type, con.con_des, "
            . "con.update_dt, con.data_dir, con.embed_type, con.embed_code, clike.likes "
            . "FROM {lcms_repository} rep "
            . "JOIN {lcms_contents} con ON con.id= rep.lcmsid $querycate"
            . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
            . $queryadd
            . "LEFT JOIN (select contentid, SUM(DISTINCT stars) / COUNT(userid) AS likes from {lcms_contents_evaluation} where timemodified > :timestamp group by contentid) clike ON clike.contentid = con.id "
            . $like;
    $nsort = ' ORDER BY clike.likes DESC';
} else if ($sort == 'new') {
    $newsql = "SELECT rep.id, lcf.filename, lcf.filepath, lcf.duration, con.category, "
            . "con.id AS con_id, con.con_name, con.con_type, con.con_des, "
            . "con.update_dt, con.data_dir, con.embed_type, con.embed_code "
            . "FROM {lcms_repository} rep "
            . "JOIN {lcms_contents} con ON con.id= rep.lcmsid $querycate"
            . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
            . $queryadd
            . $like;
    $nsort = ' ORDER BY reg_dt DESC';
}
$newcontents = $DB->get_records_sql($newsql . $nsort, $params, ($page_ref - 1) * $perpage_ref, $perpage_ref);
$newsql_cnt = "SELECT COUNT(con.id) "
        . "FROM {lcms_repository} rep "
        . "JOIN {lcms_contents} con ON con.id= rep.lcmsid $querycate"
        . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
        . $queryadd
        . $like;
$totalcount_ref = $DB->count_records_sql($newsql_cnt, $params);
?>  
<script type="text/javascript" src="../../../lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js"></script>
<script type="text/javascript">
    var entered = 0;
    $(document).ready(function () {
        $(".thumb-list ul li").click(function () {
            // /local/publishing/pop_quiz.php
            var id = $(this).attr('class');
            if (id != undefined && id != '' && id != null) {
                call_layerPop("/local/mypage/pop_contents_main.php?id=" + id, "80%", "80%");
            }
        });
        $("#searchbtn").click(function () {
            search_con();
        });
        $(".allview").click(function () {
            location.href = "/local/oklearning/reference.php";
        });
        if ($(window).width() <= 480) {

        } else {

        }
    });
    function search_con() {
        var theForm = document.forms['contentsfrm'];
        var input1 = document.createElement('input');
        var input2 = document.createElement('input');
        if ($("#searchtext").val() != undefined && $("#searchtext").val() != null) {
            input1.type = 'hidden';
            input1.name = 'searchtext';
            input1.value = $("#searchtext").val();
            theForm.appendChild(input1);
        }
        input2.type = 'hidden';
        input2.name = 'refpage';
        input2.value = 1;
        theForm.appendChild(input2);
        var catanum;
        if ($("select[name=searchcate]").val() != '' && $("select[name=searchcate]").val() > 0 && $("select[name=searchcate]").val().length > 1) {
            catanum = $("select[name=searchcate]").val();
        } else {
            catanum = $("select[name=category1]").val();
        }
        searchreflog(catanum, $("#searchtext").val());
        
        $('#contentsfrm').submit();
    }

    // 새창 refresh
    function reference_cata_page(page) {
        $('[name=refpage]').val(page);
        var theForm = document.forms['contentsfrm'];
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'searchtext';
        input.value = ($("#searchtext").val());
        theForm.appendChild(input);
        $('#contentsfrm').submit();
    }
    function cate_view(target, cateid) {
        var theForm = document.forms['contentsfrm'];
        var input2 = document.createElement('input');
        $('input[name=searchcate]').val(cateid);
        input2.type = 'hidden';
        input2.name = 'refpage';
        input2.value = 1;
        theForm.appendChild(input2);
        $('#contentsfrm').submit();
    }
    function enterkey() {
        if (window.event.keyCode == 13) {
            search_con();
        }
    }

    function searchreflog(categoryid, searchText) {
        entered++;
        if (entered == 1) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/oklearning/search_log_ajax.php" ?>',
                method: 'POST',
                data: {
                    category: categoryid,
                    searchtext: searchText,
                    searcharea: 'reference',
                }, success: function (data) {
                    console.log(data);
                    entered = 0;
                }, error: function (request, status, error) {
                    console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
                }
            });
        }
    }
</script>
<form id="contentsfrm" action="./reference.php">
    <input type="hidden" name="sort" value="<?php echo $sort; ?>"/>
    <input type="hidden" name="refpage" value="<?php echo $page_ref; ?>"/>
    <input type="hidden" name="searchid" value="<?php echo $searchid; ?>"/>
    <input type="hidden" name="searchtext" value="<?php echo $searchtext; ?>"/>
    <input type="hidden" name="category1" value="<?php echo $category1; ?>"/>
    <input type="hidden" name="searchcate" value="<?php echo $searchcate; ?>"/>
</form>
    <?php if ($searchtext != '' || $searchcate != 0) { ?>
    <h4 class="search-title" id="">
        <?php
        if ($searchcate != 0) {
            $mycatenamesql = " SELECT lc.id,lc.parent,cn.name,cn.categoryid,
                        (SELECT name from {lmsdata_category_name} cn2 where cn2.categoryid = lc.parent and cn2.lang = :lang2 )panme
                        FROM {lmsdata_category} lc 
                        JOIN {lmsdata_category_name}  cn ON cn.categoryid = lc.id                                  
                        WHERE lc.isused = 1 and cn.lang = 'ko' and lc.id = :categoryid ";
            $mycatename = $DB->get_record_sql($mycatenamesql, array('depth' => 2, 'lang' => current_language(), 'lang2' => current_language(), 'categoryid' => $searchcate));
            if ($searchtext != '') {
                ?>
                <span class="t-pink"><?php echo $searchtext ?></span> 에 대한 검색결과 <?php echo '(' . $totalcount_ref . ' 건)' ?>
            <?php } else {
                ?>
                <span class="t-pink"><?php echo$mycatename->name ?></span> 에 대한 검색결과 <?php echo '(' . $totalcount_ref . ' 건)' ?>
        <?php } ?>
            <ul>
                <li><?php echo $mycatename->panme ?> </li>
                <li><?php echo $mycatename->name ?></li>
            </ul>
    <?php } ?>
    </h4>
<?php } ?>
<div class="page_title">
    교육콘텐츠
</div>
<div class="tab-table-section" class="white-bg">
    <form class="table-search-option">
        <select name="category1" title="category01" id="content_cata1" onchange="con_cata1_changed2(this);">
            <option value="0"> - 선택하세요 -</option>
            <?php
            $catagories = siteadmin_get_content_category(1);
            foreach ($catagories as $category) {
                if ($category->id == $category1) {
                    $selected = ' selected="selected" ';
                } else {
                    $selected = '';
                }
                echo '<option value="' . $category->id . ' " ' . $selected . '> ' . $category->name . '</option>';
            }
            ?>                                          
        </select>
        <select name="searchcate" title="category02" id="content_cata2">
            <option value="0"> - 선택하세요 -</option>
            <?php
            if ($searchcate) {
                $catagories_detail = siteadmin_get_content_category(2, $category1);
                foreach ($catagories_detail as $category) {
                    if ($category->id == $searchcate) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
                    echo '<option value="' . $category->id . '" ' . $selected . '> ' . $category->name . '</option>';
                }
            } else if ($category1) { // ok
                $catagories_detail = siteadmin_get_content_category(2, $category1);
                foreach ($catagories_detail as $category) {
                    echo '<option value="' . $category->id . '" > ' . $category->name . '</option>';
                }
            }
            ?>                                
        </select>
        <input type="text" title="search" src="javascript:alert('XSS');" id="searchtext" name="searchtext" value="<?php echo $searchtext; ?>" onkeyup="enterkey();" class="search-text" placeholder="검색 키워드를 입력하세요">
        <input type="submit" id="searchbtn" value="<?php echo get_string('search', 'local_jinoboard'); ?>" class="board-search"/>
        <br>

    </form>
    <?php
//    $rows = array();
//    $rows[0] = new tabobject(0, "$CFG->wwwroot/local/oklearning/reference.php?id=$board->id", get_string('all'));
//    if (!is_siteadmin()) {
//            foreach($boardcategory as $boardca) {
//                  $usertypes = explode('/', $boardca->usertype);   
//            foreach($usertypes as $usertype){       
//                if($usertype == $luser->usertypecode) {
//                    $rows[$boardca->id] = new tabobject($boardca->id,"$CFG->wwwroot/local/oklearning/reference.php?id=$board->id&boardca=$boardca->id",$boardca->name);                     
//                }
//            }
//         }
//    } else {
//            foreach($boardcategory as $boardca) {
//                  $rows[$boardca->id] = new tabobject($boardca->id,"$CFG->wwwroot/local/oklearning/reference.php?id=$board->id&boardca=$boardca->id",$boardca->name);
//                  }  
//            }
//    if($board->allowcategory == 1 ) {
//          
//        print_tabs(array($rows), $boardcavalue);
//    }
    ?>

</div>
<!-- thumb start -->
<div class="thumb-list">
    <ul>
        <?php
        foreach ($newcontents as $mc) {
            $thpath = thumbnail_image($mc->con_id);
            $getcounts = $DB->count_records('lcms_contents_viewcnt', array('contentid' => $mc->con_id));
            ?>
            <li class="<?php echo $mc->con_id ?>">
                <div class="t-img">
                    <img src="<?php echo $thpath; ?>" alt="" />
                    <!--<span class="t-time"><?php echo gmdate('i:s', $mc->duration); ?></span>-->
                    <span class="t-time"><?php
                        echo gmdate('H', $mc->duration) == 00 ? '' : gmdate('H:', $mc->duration);
                        echo gmdate('i:s', $mc->duration);
                        ?></span> 
                    <span class="t-play">play</span>
                </div>
                <div class="t-txt">
                    <?php
                    $getcategorysql = 'SELECT lc.id, lcn.name, lc.depth, lc.parent '
                            . 'FROM {lmsdata_category} lc '
                            . 'JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.id AND lcn.lang = :lang '
                            . 'where lc.depth = 2 and lc.id = :lcid';
                    $getcategory = $DB->get_record_sql($getcategorysql, array('lcid' => $mc->category, 'lang' => current_language()));
                    $getcategorypar = $DB->get_record('lmsdata_category_name', array('categoryid' => $getcategory->parent, 'lang' => current_language()));
                    ?>
                    <p class="cate">
                        <span><?php echo $getcategorypar->name; ?></span>
                        <span><?php echo $getcategory->name; ?></span>
                    </p>
                    <h4>
                        <a href="#"><?php echo $mc->con_name; ?></a>
                    </h4>
                    <p class="num"><?php echo '<span>조회 </span>' . $getcounts; ?></p>
                </div>
            </li>
            <?php
        }
        if (empty($newcontents)) {
            ?>
            <li class="no-data" style="padding-left:0px; width:calc(100% - 0px) !important;" >
                등록된 컨텐츠가 없습니다
            </li>        
<?php } ?>
    </ul>
</div>
<!-- thumb end -->

<!-- paging start -->
<?php
if ($newcontents) {
    if (($totalcount_ref / $perpage_ref) > 1) {
        local_oklearning_pagingbar_script($totalcount_ref, $page_ref, 'javascript:reference_cata_page(:page)', $perpage_ref, $maxdisplay);
    }
}
?>
<!-- paging end -->
<?php
echo $OUTPUT->footer();
?>
