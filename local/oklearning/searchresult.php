<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';

require_once($CFG->dirroot . '/chamktu/contents/lib.php');

//교육생은 해당 페이지에 입장 못하게 처리
$usergroup = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));
if ($usergroup == 'rs') {
    redirect($CFG->wwwroot . '?re=6');
}

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->requires->css('/local/oklearning/style.css');

$searchtext = optional_param('search', '', PARAM_RAW);
$searchtext = strip_tags($searchtext);
$searchtext = htmlspecialchars($searchtext);
$searchtext = preg_replace("/[#&+<]/", "", $searchtext);

$searchid = optional_param('returnvalue', 0, PARAM_INT); // lcms_search->id
$categoryid = optional_param('category', 0, PARAM_INT); // lmsdata_category->id

$PAGE->requires->jquery();
$PAGE->requires->css('/mod/okmedia/viewer/flowplayer7.2.7/skin/skin.css');
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.speed-menu.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.hlsjs.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/player.js', true);

echo $OUTPUT->header();

$searchcontentsfrom = 'SELECT 
                        rep.id, lcf.filename, lcf.filepath, lcf.duration, con.category, 
                        con.id as con_id, con.con_name, con.con_type, 
                        con.con_des, con.update_dt, con.data_dir, 
                        con.embed_type, con.embed_code ';

$searchcontentssql = 'FROM {lcms_repository} rep 
                      JOIN {lcms_contents} con on con.id= rep.lcmsid 
                      LEFT JOIN {lcms_contents_file} lcf on con.id = lcf.con_seq ';

$params['share_yn'] = 'Y';
$params['onlymain'] = 1;
$where[] = ' rep.status = 1 ';
if (!empty($categoryid)) {
    $where[] = ' con.category = :concata ';
    $params['concata'] = $categoryid;
}

if (!empty($searchtext)) {
    $where[] = $DB->sql_like('con.con_name', ':conname');
    $params['conname'] = '%' . $searchtext . '%';
}

if (!empty($where)) {
    $where = ' WHERE ' . implode(' AND ', $where);
    $where .= ' AND con.share_yn = :share_yn AND con.onlymain = :onlymain';
} else {
    $where = ' WHERE con.share_yn = :share_yn AND con.onlymain = :onlymain';
}

$consort = ' ORDER BY reg_dt DESC ';
$searchcontents = $DB->get_records_sql($searchcontentsfrom . $searchcontentssql . $where . $consort, $params, 0, 6);

$count_sql = "SELECT count(*) ";
$countsearchcontents = $DB->count_records_sql($count_sql . $searchcontentssql . $where . $consort, $params);

// 자료실 검색하기
$uid = $USER->id;

$board = $DB->get_record('jinoboard', array('engname' => 'storage'));

$role = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));

//$boardcategory = $DB->get_records('jinoboard_category', array('board' => $board->id), 'sortorder ASC');
$field = '';

$like = " and (title like :search1 or jc.contents like :search2)";

$wherebocate = '';
if ($categoryid) {
    $categoryparent = $DB->get_field('lmsdata_category', 'parent', array('id' => $categoryid));
    $categoryname = $DB->get_field('lmsdata_category_name', 'name', array('categoryid' => $categoryparent, 'lang' => 'en'));
    $boardcateogyid = $DB->get_field('jinoboard_category', 'id', array('engname' => $categoryname, 'board' => $board->id));

    $wherebocate .= ' and jc.category = ' . $boardcateogyid;
}
$count_sql = "select count(jc.id) from {jinoboard_contents} jc where jc.board = :board " . $like . " and jc.isnotice = 0 $wherebocate order by ref DESC, step ASC";
$totalcount = $DB->count_records_sql($count_sql, array('board' => $board->id, 'search1' => '%' . $searchtext . '%', 'search2' => '%' . $searchtext . '%'));
//$total_pages = jinoboard_get_total_pages($totalcount, $perpage);   

if (is_siteadmin()) {
    $role = 'sa';
} else if (empty($role)) {
    $role = 'gu';
}

$allows = $DB->get_records('jinoboard_allowd', array('board' => $board->id));
$access = array();
foreach ($allows as $allow) {
    $access[$allow->allowrole] = $allow;
}
$myaccess = $access[$role];

if ($myaccess->allowview != 'true') { // 자료실 못보는 경우
    redirect($CFG->wwwroot, 'Permission Denied');
}

$luser = $DB->get_record('lmsdata_user', array('userid' => $uid));
?>  
<?php if (!empty($searchtext)) { ?>
    <h4 class="search-title">
        <?php if (!empty($searchtext)) { ?>
            <span class="t-pink"><?php echo $searchtext; ?></span> 에 대한
        <?php } ?>
        검색결과
        <strong class="num">(<?php echo $countsearchcontents + $totalcount; ?>)</strong>
    </h4>
<?php } ?>
<div class="result-area">
    <div class="div_title line">
        <?php if (!empty($searchtext)) { ?>
            <a href="<?php echo $CFG->wwwroot . '/local/oklearning/reference.php?searchid=' . $searchid; ?>">교육 콘텐츠<strong class="num">(<?php echo $countsearchcontents; ?>)</strong></a> 
        <?php } else { ?>
            <a href="<?php echo $CFG->wwwroot . '/local/oklearning/reference.php?searchid=' . $searchid; ?>">교육 콘텐츠</a> 
        <?php } ?>
        <a href="<?php echo $CFG->wwwroot . '/local/oklearning/reference.php?searchcate=' . $categoryid . '&searchtext=' . $searchtext ?>" class="more">더보기</a>
    </div>

    <!-- thumb start -->
    <div class="thumb-list">
        <ul>
            <?php
            foreach ($searchcontents as $mc) {
                $thpath = thumbnail_image($mc->con_id);
                $getcounts = $DB->count_records('lcms_contents_viewcnt', array('contentid' => $mc->con_id));
                ?>
                <li class="<?php echo $mc->con_id ?>">
                    <div class="t-img">
                        <img src="<?php echo $thpath; ?>" alt="" />
                        <span class="t-time"><?php
                            echo gmdate('H', $mc->duration) == 00 ? '' : gmdate('H:', $mc->duration);
                            echo gmdate('i:s', $mc->duration);
                            ?></span> 
                        <span class="t-play">play</span>
                    </div>
                    <div class="t-txt">
                        <?php
                        $getcategorysql = 'SELECT lc.id, lcn.name, lc.depth, lc.parent '
                                . 'FROM {lmsdata_category} lc '
                                . 'JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.id AND lcn.lang = :lang '
                                . 'where lc.depth = 2 and lc.id = :lcid';
                        $getcatanew = $DB->get_record_sql($getcategorysql, array('lcid' => $mc->category, 'lang' => current_language()));
                        $getcataparnew = $DB->get_record('lmsdata_category_name', array('categoryid' => $getcatanew->parent, 'lang' => current_language()));
                        ?>
                        <p class="cate">
                            <span><?php echo $getcataparnew->name; ?></span>
                            <span><?php echo $getcatanew->name; ?></span>
                        </p>
                        <h4>
                            <a href="#"><?php echo $mc->con_name ?></a>
                        </h4>
                        <p class="num"><?php echo '<span>조회 </span>' . $getcounts; ?></p>
                    </div>
                </li>
                <?php
            }
            if (!empty($searchtext) && empty($searchcontents)) {
                echo '<li class="no-data">검색된 콘텐츠가 없습니다.</li>';
            }
            ?>
        </ul>
    </div>
    <!-- thumb end -->

    <!--자료실 부분-->

    <?php
    if ($myaccess->allowview != 'true') { // 자료실 못보는 경우
    } else {
        $boardcategorysql = "select jc.id
                            from m_lmsdata_category lc 
                            join m_lmsdata_category_name lcn on lc.parent = lcn.categoryid
                            join m_jinoboard_category jc on lcn.name = jc.name
                            where lc.id = ? ";
        $boardcategory = $DB->get_field_sql($boardcategorysql, array($categoryid));
        $sql = "select jc.*, lc.coursename from {jinoboard_contents} jc left join {lmsdata_course} lc on jc.lcourseid = lc.id where jc.board = :board " . $like . " and jc.isnotice = 0 $wherebocate order by ref DESC, step ASC";
        $contents = $DB->get_records_sql($sql, array('board' => $board->id, 'search1' => '%' . $searchtext . '%', 'search2' => '%' . $searchtext . '%'), 0, 4);
        ?>
        <div class="div_title line nomg">
            <?php if (!empty($searchtext)) { ?>
                <a href="<?php echo $CFG->wwwroot . '/local/jinoboard/list.php?id=' . $board->id . '&boardca=' . $boardcategory . '&searchfield=title&search=' . $searchtext; ?>" title="제목에 들어간 경우만 표시됩니다."><?php echo $board->name; ?><strong class="num">(<?php echo $totalcount; ?>)</strong></a>
            <?php } else { ?>
                <a href="<?php echo $CFG->wwwroot . '/local/jinoboard/list.php?id=' . $board->id . '&boardca=' . $boardcategory . '&searchfield=title&search=' . $searchtext; ?>" title="제목에 들어간 경우만 표시됩니다."><?php echo $board->name; ?></a>
            <?php } ?>
            <a href="<?php echo $CFG->wwwroot . '/local/jinoboard/list.php?id=' . $board->id . '&boardca=' . $boardcategory . '&searchfield=title&search=' . $searchtext; ?>" class="more">더보기</a>
        </div>
        <div class="thread-style">
            <ul class="thread-style-lists">
                <?php
                foreach ($contents as $content) {
                    $parent = $DB->get_record('jinoboard_contents', array('id' => $content->ref));
                    $fs = get_file_storage();
                    $files = $fs->get_area_files($context->id, 'local_jinoboard', 'attachment', $content->id, 'timemodified', false);

                    if (count($files) > 0) {
                        $filecheck = '<img src="' . $CFG->wwwroot . '/theme/oklassedu/pix/icon-attachment.png" alt="' . get_string('content:file', 'local_jinoboard') . '">';
                    } else {
                        $filecheck = "";
                    }
                    $step = ($content->lev <= 4) ? $content->lev : 4;
                    $date_left_len = $step * 30;
                    $calcwidth = ($content->lev <= 4) ? $content->lev * 30 : 120;
                    $date_left = 'style="padding-left:' . $date_left_len . 'px; width:calc(100% - ' . $calcwidth . 'px) !important;"';
                    $step_icon = ($content->lev) ? '<img src="' . $OUTPUT->pix_url('icon_reply', 'mod_jinotechboard') . '" alt="reply" /> ' : '';


                    $postuser = $DB->get_record('user', array('id' => $content->userid));
                    $postuserinfo = $DB->get_record('lmsdata_user', array('userid' => $content->userid));
                    $fullname = fullname($postuser);
                    $userdate = userdate($content->timecreated);
                    $by = new stdClass();
                    $by->name = $fullname;
                    if (is_siteadmin($content->userid)) {
                        $by->name = '관리자';
                    }
                    if (is_siteadmin()) {
                        $by->name .= '(' . $postuser->username . ')';
                    }
                    $by->date = $userdate;
                    $new = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? '<span class="new">N</span>' : '';
                    $newClass = ($content->timemodified + (60 * 60 * 24 * $board->newday) >= time() && $board->allownew == 1 ) ? 'has_new' : '';
                    $coursename = '';
                    $userpicture = new user_picture($postuser);
                    $userpicture->size = 1; // Size f1.
                    $url = $userpicture->get_url($PAGE)->out(false);
                    echo "<li>";
                    echo "<div class='thread-content'><span class='post-title'>" . $step_icon;
                    if ($board->allowcourseselection == 1) {
                        $coursename = '[' . $DB->get_field('lmsdata_course', 'coursename', array('id' => $content->lcourseid)) . ']';
                    }
                    if (($content->issecret && $USER->id != $content->userid && $parent->userid != $USER->id && $myaccess->allowsecret != 'true') && !is_siteadmin()) {
                        echo $content->title . $new . '1';
                    } else if ($myaccess->allowdetail != 'true') {
                        echo '<a href="#" class="' . $newClass . '" onclick="alert(' . "'해당 글을 볼 수 있는 권한이 없습니다.'" . ')"> ' . $coursename . ' ' . $content->title . $new . '"</a>"';
                    } else {
                        if ($page == 0 || empty($page)) {
                            $page = 1;
                        }
                        if ($perpage == 0 || empty($perpage)) {
                            $perpage = 10;
                        }
                        echo "<a href='" . $CFG->wwwroot . "/local/jinoboard/detail_uncore.php?id=" . $content->id . "&board=" . $content->board ."&search=" . $searchtext ."&rtnvalue=" . $searchid . "&cata=" . $categoryid . "' class='" . $newClass . "'>" . $coursename . " " . $content->title . $new . "</a>";
                    }
                    echo "  " . $filecheck;
                    if ($content->issecret) {
                        echo "<img src='" . $CFG->wwwroot . "/theme/oklassedu/pix/lock.png' width='20' height='20' alt='" . get_string('secreticon', 'local_jinoboard') . "' title='" . get_string('secreticon', 'local_jinoboard') . "'>";
                    }

                    echo '<br/><span class="post-date">' . get_string("bynameondate", "local_jinoboard", $by) . '</span>';
                    echo "</span></div><div class='thread-right'>";
                    echo "<span class='post-viewinfo area-right'>" . $content->viewcnt . "<br/><span>" . get_string('viewcount', 'local_jinoboard') . "</span></span>";
                    echo "</div></li>";
                }
                if (!empty($searchtext) && empty($contents)) {
                    echo '<li class="no-data">검색된 자료가 없습니다.</li>';
                }
                ?>
            </ul>
        </div>
<?php } ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".thumb-list ul li:not(.no-data)").click(function () {
            // /local/publishing/pop_quiz.php
            var id = $(this).attr('class');
            call_layerPop("/local/mypage/pop_contents_main.php?id=" + id, "80%", "80%");
        });
    });
</script>

<?php
echo $OUTPUT->footer();
?>


