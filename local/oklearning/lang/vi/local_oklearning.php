<?php
$string['pluginname'] = 'Cộng đồng UST';
$string['pluginnameplural'] = 'Công đồng UST';

$string['apply'] = 'Đăng ký';
$string['apply:start'] = 'Bắng đầu đăng ký';
$string['apply:end'] = 'Kết thúc đăng ký';
$string['apply:status'] = 'Trong thái';
$string['apply:possible'] = 'Có thể';
$string['apply:impossible'] = 'Không thể';
$string['apply:doyouwantto'] = 'Bạn muốn đăng ký?';
$string['apply:doyouwantcancel'] = 'Bạn muốn hủy bỏ?';
$string['cancelalert'] = 'Bạn muốn hủy bỏ?';
$string['cancelapply'] = 'Đã xóa';
$string['approval'] = 'Duyệt';
$string['approved'] = 'Phương thức duyệt';
$string['approved:auto'] = 'Tự động';
$string['approved:autodesc'] = 'Chiaw sẻ mọi người';
$string['approved:pass'] = 'Mật khẩu';
$string['approved:passdesc'] = 'Nhập mật khẩu ở đây';
$string['approved:review'] = 'Đánh giá';
$string['approved:reviewdesc'] = 'Khi được phê duyệt, Khóa học có thể được thực hiện';
$string['approved:passshow'] = 'Hiển thị mật khẩu';
$string['agree:privacy'] = 'Đồng ý điều khoản';
$string['agree:privacydesc'] = 'Thông tin cá nhân chỉ được sử dụng cho mục đích hoạt động Cộng đồng UST và tôi sẽ không dùng cho các mục đích khác.';
$string['wrong_password'] = 'Sai mật khẩu';
$string['category:all'] = 'Tất cả';
$string['category:sort'] = 'Sắp xếp';
$string['course:my'] = 'Khóa học của tôi';
$string['course:apply'] = 'Đăng ký khóa học';
$string['course:applydate'] = 'Ngày đăng ký';
$string['course:apply_input'] = 'Vui lòng nhập ngày đăng ký';
$string['course:name'] = 'Khóa học';
$string['course:empty'] = 'Không có khóa học';
$string['course:emptyinfo'] = 'Không có thông tin đăng ký';
$string['course:list'] = 'Danh sách khóa học';
$string['course:category'] = "Thể loại";
$string['course:applicant'] = 'Người đăng ký';
$string['course:add'] = 'Thêm khóa học';
$string['course:edit'] = 'Sửa khoá học';
$string['course:create'] = 'Tạo khóa học';
$string['course:date'] = 'Học kỳ';
$string['course:date_input'] = 'Vui lòng nhập thời lượng bài giảng';
$string['course:image'] = 'Ảnh';
$string['course:image_input'] = 'Chỉ tệp hình ảnh (.jpg, .png).';
$string['course:credit'] = 'Tín chỉ';
$string['course:registered'] = 'Đã đăng ký';
$string['course:delete'] = 'Bỏ theo dõi';
$string['course:app'] = 'Phê duyệt';
$string['course:wait'] = 'Chờ';
$string['course:approval'] = 'Yêu cầu phê duyệt';
$string['course:approval_cancel'] = 'Hủy phê duyệt';
$string['course:approval_delete'] = 'Xóa phê duyệt';
$string['course:enrol'] = 'Ghi danh';
$string['course:enrol_check'] = 'Muốn ghi danh';
$string['course:enrolcancel'] = 'Không ghi danh';
$string['course:section'] = 'Học phần';
$string['courseinfo'] = 'Thông tin khóa học';
$string['coursename:ko'] = 'Tên khóa học (KO)';
$string['coursename:en'] = 'Tên khóa học (EN)';
$string['course:summary'] = 'Tóm tắt khóa học';
$string['completedrequestenrol'] = 'Yêu cầu đăng ký được gửi đi';
$string['completedrequestcancel'] = 'Hủy bỏ thành công';
$string['couldnotfindenrolment'] = 'Không tìm thấy thông tin đăng ký';
$string['couldnotrequestenrol'] = 'Không thể đăng ký';
$string['cancel'] = 'Hủy bỏ';
$string['confirm'] = 'Xác nhận';
$string['close'] = 'Đóng';

$string['description'] = 'Mô tả';

$string['edit'] = 'Sửa';
$string['export:excel'] = 'Tải tệp Excel';

$string['finish'] = 'Kết thúc';

$string['invisible'] = 'Ẩn danh';
$string['isopened'] = 'Công khai';
$string['isopeneddesc'] = 'Nếu riêng tư, thì không xuất hiện trong danh sách.';

$string['lastlecture'] = 'Khóa học trước';
$string['lastlecture_list'] = 'Danh sách khóa học trước';

$string['manage'] = 'Quản lý';

$string['no'] = 'Không';
$string['note'] = 'Chú ý';

$string['ongoing'] = 'Đang thực hiện';

$string['progress'] = 'Tiến trình / Lịch giảng';
$string['progress_list'] = 'Danh sách tiến trình / lịch giảng';
$string['print'] = 'In';
$string['purpose'] = 'Mục tiêu';
$string['purpose:course'] = 'Khóa học';
$string['purpose:community'] = 'Cộng đồng';

$string['return'] = 'Return';
$string['request'] = 'Apply';

$string['success'] = 'Thành công';
$string['student'] = 'Học viên';
$string['students'] = 'Học viên';
$string['subject:lang'] = 'Môn học KO/EN';
$string['subject:lang_input'] = 'Vui lòng chọn khóa học KO/EN';
$string['subject:ko'] = 'KO';
$string['subject:ko_input'] = 'Vui lòng chọn tên khóa học (KO).';
$string['subject:en'] = 'EN';
$string['subject:en_input'] = 'Vui lòng chọn tên khóa học (EN).';
$string['search'] = 'Tìm kiếm';
$string['search:keywords'] = 'Nhập từ khóa';
$string['search:coursename'] = 'Nhập tên khóa học';
$string['showperpage'] = 'Hiển thị {$a} mỗi trang';
$string['selectcourses'] = 'Chọn khóa học đăng ký';
$string['section'] = 'Phần';
$string['subject:college_input'] = 'Vui lòng chọn trường';
$string['subject:request_input'] = 'Vui lòng chọn cách đăng ký';
$string['sel:yes'] = 'Có';
$string['sel:no'] = 'Không';
$string['standby'] = 'Chờ';
$string['select'] = 'Chọn';

$string['user:name'] = 'Tên';
$string['user:major'] = 'Liên kết';
$string['user:email'] = 'Thư điện tử';
$string['user:tel'] = 'Điện thoại';
$string['user:complete_status'] = 'Trạng thái hoàn thành';
$string['user:complete_success'] = 'Khóa học hoàn thành';
$string['user:complete_cancel'] = 'Hủy bỏ hoàn thành';
$string['user:complete'] = 'Hoàn thành';
$string['user:incomplete'] = 'Không hoàn thành';
$string['user:empty'] = 'Không có người dùng';
$string['user:search'] = 'Tìm kiếm ID / tên';
$string['user:sel_empty'] = 'Không có học viên được chọn';
$string['user:unenrol'] = 'Xóa người dùng';
$string['user:info'] = 'Thông tin người dùng';

$string['viewcourse'] = 'Xem khóa học';
$string['visible'] = 'Hiển thị';

$string['resumeclinic'] = 'Sơ yêu lích lịch';
$string['registrationresume'] = 'Đăng ký hệ thống phát triển nghề nghiệp';
$string['supportconsultation'] = 'Bộ phận hỗ trợ việc làm';
$string['password_plz'] = 'Mật khẩu không chính xác.';
$string['del_course'] = 'Xóa khóa học';
$string['del_course_mes'] = 'Bạn có chắc chắn muốn xóa khóa học này?';