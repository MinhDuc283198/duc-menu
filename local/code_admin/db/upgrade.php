<?php

function xmldb_local_code_admin_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    require_once($CFG->libdir . '/eventslib.php');

    $dbman = $DB->get_manager();

            if($oldversion < 2017062102) {
        // Define table competency_usercompplan to be created.
        $table = new xmldb_table('lmsdata_code');

        // Adding fields to table competency_usercompplan.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('code_category', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('depth', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('code', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null,0);
        $table->add_field('codename', XMLDB_TYPE_CHAR, '100', null, null, null, 0);
        $table->add_field('english_codename', XMLDB_TYPE_CHAR, '100', null, null, null, 0);
        $table->add_field('step', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        $table->add_field('parentid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        
        // Adding keys to table competency_usercompplan.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for competency_usercompplan.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }
    return true;
}


