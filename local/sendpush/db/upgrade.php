<?php
function xmldb_local_sendpush_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    
    $dbman = $DB->get_manager(); 
    
    if($oldversion < 2019060700) {     
        $table = new xmldb_table('pushsend_history');  
        $field = new xmldb_field('link', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }  
    if($oldversion < 2019070100) {     
        $table = new xmldb_table('pushsend_history');  
        $field = new xmldb_field('send_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    } 
    
    if($oldversion < 2019070500) {     
        $table = new xmldb_table('pushsend_history');  
        $field = new xmldb_field('send_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field ($table, $field); 
        }
    } 
    return true;
}

