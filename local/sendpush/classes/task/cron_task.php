<?php

namespace local_sendpush\task;
 
/**
 * An example of a scheduled task.
 */
class cron_task extends \core\task\scheduled_task {
    /**
     * Return the task's name as shown in admin screens.
     *
     * @return string
     */
    public function get_name() {
        return get_string('pluginname', 'local_sendpush');
    }
 
    /**
     * Execute the task.
     */
    public function execute() {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . '/chamktu/lib/lib.php');
        require_once($CFG->dirroot . '/message/output/appnoti/message_output_appnoti.php');

        $count = array('success'=>0,'fail'=>0);
        $mails = $DB->get_records('pushsend_history',array('send_status'=>0));

        foreach($mails as $mail){
            $subject = $mail->subject;
            $message = $mail->text;
            $link = $mail->link;
            $user = $DB->get_record('user',array('id'=>$mail->targets));
            print_object('userid = '.$user->id);
       
            //$sendmail = $this->send_fcm($usertoken->token,$subject,$message);
            $usertokens = $DB->get_records('lmsdata_pushtoken', array('username'=>$user->username));
            if(!empty($usertokens)) {
               foreach($usertokens as $usertoken) {
                    $sendmail = $this->send_fcm($usertoken->token,$subject,$message,$link);
               }
           }
       
            if($sendmail){
                if($updatedata = $DB->get_record('pushsend_history', array('id'=>$mail->id))){
                    $updatedata->send_status = 1;
                    $updatedata->done_date = time();
                    $upmail = $DB->update_record('pushsend_history',$updatedata);
                }
                $count['success']++;
             }else{
                 if($updatedata2 = $DB->get_record('pushsend_history', array('id'=>$mail->id))){
                   $updatedata2->send_status = 2;
                   $updatedata2->done_date = time();
                   $upmail = $DB->update_record('pushsend_history',$updatedata2);
                 }                 
                 $count['fail']++;
             }
        }
        return $count;
    
    }
    
    function send_fcm($id,$subject,$message,$link) {
        
        //$id = "fMhw8dBQE1Y:APA91bGJ8RBJc_GAQ0XtBAg7HgyMMEVkEVjy3H9ALk4d9tnouxPtcog3aEitUEsYzdChpuKv1Hmp9iMCfrXETokzK_zucEsXAQHeDfbos-32YXd1Sqr0SnaEciXVlkf-9UUwwFgNW6MQ";
        $url = get_config('moodle', 'fcmurl');
        $fcmserverkey = get_config('moodle','fcmserverkey');

        //$link = "https://www.nate.com";
        
        $headers = array (
        'Authorization: key=' . $fcmserverkey,
        'Content-Type: application/json'
        );
        
        $fields = array( 
            "to" => $id,
            "priority" => "high",
             "notification" => array (
                "title" => $subject, 
                "body" => $message,
                "sound" => ""
              ),
             "data" => array(
                    "link"=>$link
                )
        );
                

        $fields = json_encode ($fields);
        

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, false );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        if ($result === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
        } 
        curl_close ( $ch );
        return $result;
    }
  
}

