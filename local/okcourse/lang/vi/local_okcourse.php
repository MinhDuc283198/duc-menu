<?php

$string['pluginname'] = 'Khóa học';
$string['pluginnameplural'] = 'Khóa học';
$string['student:list'] = 'Danh sách học viên';
$string['user:name'] = 'Tên';
$string['user:role'] = 'Quy ước';
$string['user:attach'] = 'Đính kèm';
$string['user:usernumber'] = 'Mã học viên ';
$string['user:private'] = '-';
$string['email'] = 'Thư điện tử';
$string['message'] = 'Tin nhắn';

$string['humanities'] = 'Nhân văn';
$string['social'] = 'Xã hội';
$string['engineering'] = 'Kỹ thuật';
$string['sciences'] = 'Khoa học';
$string['education'] = 'Giáo dục';
$string['medicine'] = 'Y tế';
$string['artssports'] = 'Nghệ thuật';
$string['category'] = 'Thể loại';
$string['shortcuts'] = 'Lối tắt';
$string['startingdate'] = 'Ngày bắt đầu';
$string['alertmessage'] = 'Vui lòng chọn người dùng';

