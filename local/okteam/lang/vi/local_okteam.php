<?php
$string['pluginname'] = 'Chỉnh sửa nhóm';
$string['pluginnameplural'] = 'Chỉnh sửa nhóm';

$string['teamedit'] = 'Chỉnh sửa nhóm';
$string['team'] = 'Nhóm';
$string['team_grouping'] = 'Phân loại nhóm';
$string['teamname'] = 'Tên nhóm';
$string['save'] = 'Lưu';
$string['member'] = 'Thanhf viên';
$string['management'] = 'Quản trị';
$string['add_member'] = 'Thêm thành viên';
$string['search'] = 'Tìm';
$string['grouping'] = 'Phân nhóm';

$string['empty_team'] = 'Nhóm rỗng';
$string['empty_grouping'] = 'Phaan nhóm rỗng';

$string['edit'] = 'Sửa';

$string['num'] = 'No.';
$string['addteam'] = 'Thêm nhóm';

$string['team_group'] = 'Team Group';
$string['team_groupname'] = 'Team Group Name';
$string['add_teamgroup'] = 'Add Team Group';

$string['excel_upload'] = 'Excel Upload';
$string['manualcreation'] = 'Manual Creation';