<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$type = optional_param('type', null, PARAM_RAW);
$value = optional_param('value', null, PARAM_RAW);

$typelang = get_string($type,"local_signup");
$returndata = new stdClass();
$returndata ->status = 1;
$returndata ->text = get_string('available', 'local_signup',array("type"=>'joinpassword'));

$returndata ->text = get_string('pw7', 'local_my');
if($type =='newpassword'){
    if( $value != trim($value) || strpos($value,' ')){
        $returndata ->status = 0;
        $returndata ->text = get_string('blankcheck','local_signup');
    }else{
        if(!check_password_policy($value, $errmsg)){
            $returndata ->status = 0;
            $returndata ->text = get_string('passvalcheck','local_signup');
        }
    }
}

echo json_encode($returndata);



