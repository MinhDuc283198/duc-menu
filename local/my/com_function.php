<?php
defined('MOODLE_INTERNAL') || die();
function mkj_save_avatar($id,$userid, $data){
    $avatar = $_FILES['file'];
    $avatar_path = '';
    if ($avatar['name'] != '') {
        $fs = get_file_storage();
        $itemid = file_get_unused_draft_itemid();

        $file_record = array(
            'contextid' => $id,
            'component' => 'local_visang',
            'filearea' => 'images',
            'itemid' => $itemid,
            'filepath' => '/',
            'filename' => $avatar['name'],
            'timecreated' => time(),
            'timemodified' => time(),
            'userid' => $userid
        );
        $fs->create_file_from_pathname($file_record, $avatar['tmp_name']);

        $avatar_path = implode('/', array($id, 'local_visang', 'images', $itemid, $avatar['name']));
        return $avatar_path;
    }
}