<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('강좌목록');
$id = $USER->id;

$query = 'select u.id, u.username, u.auth, u.firstname, u.lastname, u.phone1, u.phone2, u.email, lu.address, lu.address_detail,lu.sns, lu.email2, lu.zipcode from {user} u JOIN {lmsdata_user} lu ON u.id=lu.userid WHERE u.id=:id';
$user = $DB->get_record_sql($query, array('id' => $id));
$avatar_url=$DB->get_record('avatar_user',array('user_id'=>$id));

echo $OUTPUT->header();
?>
<form action="#" method="post" enctype="multipart/form-data" name="user_info_form" id="user_info_form">
    <h2 class="pg-tit hasbtn">
        <?php echo get_string('personaledit', 'local_my') ?>
        <!--        <a href="#" class="btns br f-r"><?php echo get_string('withdrawal', 'local_mypage'); ?></a>-->
    </h2>
    <div class="my-box">
        <div class="rw company-info-area rw1" style="width: 20%; margin: 0 auto; float: none;">
            <div class="ci-img">
                <div class="info-txt" style="background-image: url('<?php echo $avatar_url->url; ?>') !important;">
                </div>
                <p class="logo-img <?php echo ($avatar_url->url != "" && $avatar_url->url != " ") ? "" : "d-none";?>"><img class="rounded-circle" id="logoImg" src="<?php echo $CFG->wwwroot."/pluginfile.php/".$avatar_url->url;?>" alt="" /></p>
                <label><input type="file" name="avatar" id="avatar"/></label>
            </div>
            <button type="button" id="btnavatar" style="background: #485cc7; color: #fff;width: 100%; margin-top: 20px;">Change avatar</button>
        </div>
    </div>
    <div class="my-box imprt">
        <div class="sub-tit"><?php echo get_string('reinformation', 'local_my') ?></div>
        <div class="rw">
            <strong><?php echo get_string('id', 'local_my') ?></strong>
            <p>
                <img src="/theme/oklassedu/pix/images/<?php echo $user->sns ? 'icon_' . $user->sns : 'icon_mail'; ?>.png" alt="<?php echo $user->sns ? $user->sns : 'email'; ?>" />
                <span><?php echo $user->sns ? $user->sns . get_string('join', 'local_my') : $user->username; ?></span>
            </p>
        </div>
        <div class="rw">
            <strong><?php echo get_string('name', 'local_my') ?></strong>
            <p><strong><?php echo $user->firstname . $user->lastname ?></strong></p>
        </div>

        <div class="rw">
            <strong><?php echo get_string('pw', 'local_my') ?></strong>
            <p>
                <?php if ($user->auth == 'email' || $user->auth == 'manual') { ?>
                    <a href="#" class="btns br openPop"><?php echo get_string('pwchange', 'local_my') ?></a>
                <?php } else { ?>
                    <span class="t-point"><?php echo get_string('EditInfoPass', 'local_job'); ?></span>
                <?php } ?>
            </p>
        </div>
        <div class="rw">
            <strong><?php echo get_string('birth', 'local_my') ?></strong>
            <?php
            $birth_array = explode('-', $user->phone2);
            ?>
            <p class="birth">
                <select name="birth_y">
                    <?php
                    foreach (range(date('Y'), 1960) as $year) {
                        if ($birth_array[0] == $year) {
                            echo '<option value="' . $year . '" selected>' . $year . '</option>';
                        } else {
                            echo '<option value="' . $year . '">' . $year . '</option>';
                        }
                    }
                    ?>
                </select>
                <select name="birth_m">
                    <?php
                    for ($m = 1; $m <= 12; $m++) {
                        if (strlen($m) == 1)
                            $m = "0" . $m;
                        if ($m == $birth_array[1]) {
                            $date_month .= "<option value='$m' selected>$m</option>\n";
                        } else {
                            $date_month .= "<option value='$m'>$m</option>\n";
                        }
                    }
                    echo $date_month;
                    ?>
                </select>
                <select name="birth_d">
                    <?php
                    for ($d = 1; $d <= 31; $d++) {
                        if (strlen($d) == 1)
                            $d = "0" . $d;
                        if ($d == $birth_array[2]) {
                            $date_day .= "<option value='$d' selected>$d</option>\n";
                        } else {
                            $date_day .= "<option value='$d'>$d</option>\n";
                        }
                    }
                    echo $date_day;
                    ?>
                </select>
            </p>
        </div>
        <div class="rw">
            <strong><?php echo get_string('email', 'local_my') ?></strong>
            <p class="e-mail">
                <input type="text" class="w-auto" name="email2" value="<?php echo $user->email2 ? $user->email2 : $user->email; ?>" placeholder="" />
            </p>
        </div>
        <div class="rw">
            <strong><?php echo get_string('phone', 'local_course') ?></strong>
            <p class="phone">
                <input type="text" class="w-auto" name="phone1" value="<?php echo $user->phone1 ?>" placeholder="" />
                <span class="txt"><?php echo get_string('phone_txt', 'local_my') ?></span>
            </p>
        </div>
    </div>

    <h5 class="bx-tit"><?php echo get_string('address', 'local_my') ?></h5>
    <div class="my-box">
        <div class="rw">
            <strong><?php echo get_string('adress', 'local_course') ?></strong>
            <p class="addr">
                <input type="text" class="w100" name="zipcode" value="<?php echo $user->zipcode ?>" placeholder="<?php echo get_string('zipcode', 'local_my'); ?>" />
                <input type="text" class="w100" name="address" value="<?php echo $user->address ?>" placeholder="<?php echo get_string('adress', 'local_my'); ?> " />
                <span class="txt"><?php echo get_string('textbookdelivered', 'local_my') ?></span>
            </p>
        </div>
    </div>

    <div class="btn-area text-center">
        <!--        <input type="button" value="<?php echo get_string("cancel", "local_my") ?>" class="btns gray big02" />-->
        <input type="button" value="<?php echo get_string("save", "local_my") ?>" class="btns point big02" onclick="info_submit()" />
    </div>
</form>
<script>
    $(document).ready(function(){
        $( "#btnavatar" ).click(function() {
            // var myFormData = new FormData();
            var fd = new FormData();
            var files = $('#avatar')[0].files;
            fd.append('file',files[0]);
            $.ajax({
                url: '/local/my/upload.php',
                type: 'POST',
                processData: false, // important
                contentType: false, // important
                dataType : 'json',
                data: fd,
                success: function(response){
                    window.location = "<?php echo $CFG->wwwroot .'/local/my/info.php'?>";
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("input[name=phone1]").on("keyup", function() {
            $(this).val($(this).val().replace(/[^0-9]/g, ""));
        });

        $(document).on("keyup", "#newpassword", function() {
            check_val('newpassword');
            match_password();
        });
        $(document).on("keyup", "#newpassword2", function() {
            match_password();
        });
        $(document).on("blur", "#cpassword", function() {
            var result = call_ajax('cpassword', {
                pw: $('#cpassword').val()
            }, false);

            if (result.status == 'sucess') {
                $('.cpasswordwarning').text(result.text);
                $('#cpasswordconfirm').val(1);
            } else {
                $('.cpasswordwarning').text(result.text);
                $('#cpasswordconfirm').val(0);
            }
        });

    })

    function check_val(type) {

        var target = $("#" + type);
        var regExpemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (type == 'email' && !regExpemail.test($("#email").val())) {
            var errortext = "<?php echo get_string("emailrule", "local_signup", array('rule' => "visang@visang.com")) ?>";
            alert(errortext);
            $("." + type + "warning").text(errortext);
            $("#" + type + "confirm").val(0);
        } else {
            if (/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/.test(target.val())) {
                $.ajax({
                    url: "/local/my/valueconfirm.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        type: type,
                        value: target.val()
                    },
                    success: function(result) {
                        if (result.status) {
                            $("#" + type + "confirm").val(result.status);
                            $("." + type + "warning").text(result.text);
                        } else {
                            $("#" + type + "confirm").val(result.status);
                            $("." + type + "warning").text(result.text);
                        }
                    }
                });
            } else {
                $("#newpasswordconfirm").val(0);
                $(".newpasswordwarning").text('<?php echo get_string('passvalcheck', 'local_signup') ?>');
            }
        }
    }

    function match_password() {
        if ($("#newpassword2").val() == $("#newpassword").val()) {
            $(".newpassword2warning").text("<?php echo get_string("matchedpass", "local_signup") ?>");
        } else {
            $(".newpassword2warning").text("<?php echo get_string("notmatchedpass", "local_signup") ?>");
        }
    }
    $(function() {
        //비밀번호 변경 팝업 이벤트
        $(".openPop").click(function() {
            utils.popup.call_layerpop("./popup_pwchange.php", {
                "width": "400px",
                "height": "auto",
                "callbackFn": function() {}
            }, {
                "param": "11"
            });
            return false;
        })
    });

    //값이 없는지 체크
    function value_null_check(select) {
        if ($(select).val() == null || $(select).val() == 0 || $(select).val() == undefined || $(select).val() == '') {
            return false;
        } else {
            return true;
        }
    }

    function pw_submit_check() {
        var new1 = $('input[name=newpassword]').val();
        var new2 = $('input[name=newpassword2]').val();

        if (value_null_check('input[name=password]') == false) {
            alert('<?php echo get_string('pw1', 'local_my') ?>');
            return false;
        }
        if (value_null_check('input[name=newpassword]') == false) {
            alert('<?php echo get_string('pw2', 'local_my') ?>');
            return false;
        }
        if (value_null_check('input[name=newpassword2]') == false) {
            alert('<?php echo get_string('pw3', 'local_my') ?>');
            return false;
        }

        if (new1 != new2) {
            alert('<?php echo get_string('pw4', 'local_my') ?>');
            return false;
        }

        var check1 = $('#cpasswordconfirm').val();
        var check2 = $('#newpasswordconfirm').val();
        if (check2 != 1) {
            alert('<?php echo get_string('passvalcheck', 'local_signup'); ?>');
            return false;
        }
        if (check1 != 1) {
            alert('<?php echo get_string('pw6', 'local_my') ?>');
            return false;
        }

        if (check1 != 1 || check2 != 1) {
            return false;
        }
        return true;
    }

    function pw_submit() {
        var pw = $('input[name=password]').val();
        var new1 = $('input[name=newpassword]').val();
        var new2 = $('input[name=newpassword2]').val();

        if (pw_submit_check() == true) {

            var result = call_ajax('passwordchange', {
                pw: pw,
                new1: new1,
                new2: new2
            }, false);

            if (result) {
                if (result.status == 'success') {
                    alert('<?php echo get_string('pw5', 'local_my') ?>');
                    utils.popup.close_pop($('.layerpop'));
                    location.href = "<?php echo $CFG->wwwroot ?>" + "/login/logout.php?sesskey=" + "<?php echo $USER->sesskey ?>&loginpage=/login/index.php";

                } else if (result.status == 'not') {
                    $('.not').show();
                    return false;
                } else {
                    alert(result.error);
                }
            }
        }
    }

    function info_submit() {
        var data = '';
        var info_arr = $("#user_info_form").serializeArray();

        $.each($('#user_info_form').serializeArray(), function(key, val) {
            data += ',"' + val['name'] + '":"' + val['value'] + '"';
        });
        data = '{' + data.substr(1) + '}';
        data = JSON.parse(data);
        var result = call_ajax('userinfochange', data, false);
        if (result) {
            if (result.status == 'success') {
                alert('<?php echo get_string('edit_complete', 'local_my') ?>');
            } else {
                alert(result.error);
            }
        }
    }

    function call_ajax(action, data, async) {

        var rvalue = false;
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            async: async,
            data: {
                action: action,
                data: data
            },
            success: function(result) {
                rvalue = result;
            },
            error: function(xhr, status, error) {}
        });

        return rvalue;
    }
</script>
<?php
echo $OUTPUT->footer();
?>