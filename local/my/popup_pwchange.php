<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('비밀번호 변경');

echo $OUTPUT->header();
//$usernew->id = $id;
//
//$usernew->password = hash_internal_user_password($password);
//user_update_user($usernew, false, false);

//암호화
//password_hash($password, PASSWORD_DEFAULT);
?>  
<div class="layerpop chng-pw">
    <div class="pop-title">
        <?php echo get_string('pwchange','local_my')?>
        <a href="#" class="pop-close"><?php echo get_string('close','local_my')?></a>
    </div>
    <div class="pop-contents">
        <div class="rw">
            <div class="input-tit"><?php echo get_string('cpw','local_my')?></div>
            <input type="password" class="w100" name="password" id="cpassword"/>
            <input type="hidden" id="cpasswordconfirm" name="cpasswordconfirm">
            <span class="not"><p class="t-red warning cpasswordwarning"><?php echo get_string('pw6','local_my')?></p></span>
        </div>
        <div class="rw">
            <div class="input-tit"><?php echo get_string('npw','local_my')?></div>
            <input type="password" class="w100" name="newpassword" id="newpassword"/>
            <input type="hidden" id="newpasswordconfirm" name="newpasswordconfirm" />
            <p class="t-red warning newpasswordwarning"><?php echo get_string('passvalcheck','local_signup'); ?></p>
        </div>
        <div class="rw">
            <div class="input-tit"><?php echo get_string('npw2','local_my')?></div>
            <input type="password" class="w100" name="newpassword2" id="newpassword2"/>
            <p class="t-red warning newpassword2warning"></p>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="<?php echo get_string('change','local_my')?>" class="btns point" onclick="pw_submit()" />
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>
<script>

</script>

