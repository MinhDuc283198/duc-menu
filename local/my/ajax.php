<?php

define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');


$action = required_param('action', PARAM_ALPHAEXT);

$context = context_system::instance();


switch ($action) {
    case 'passwordchange':
        require_once($CFG->libdir.'/gdlib.php');
        require_once($CFG->libdir.'/adminlib.php');
        require_once($CFG->dirroot.'/user/editadvanced_form.php');
        require_once($CFG->dirroot.'/user/editlib.php');
        require_once($CFG->dirroot.'/user/profile/lib.php');
        require_once($CFG->dirroot.'/user/lib.php');
        require_once($CFG->libdir.'/password_compat/lib/password.php');

        $data = required_param_array('data', PARAM_RAW);
        //현재비밀번호 맞는지 틀린지 체크
        $usernew = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);

        $user = authenticate_user_login($USER->username, $data['pw'], false);
        $message = "";
        $status = "success";
        if($user == false){
            $status = 'not';
            $message=get_string('pw6','local_my');
        }else{
            if(trim($data["new1"]) != trim($data["new2"])){
            
            }else{
                $usernew->password = hash_internal_user_password(trim($data["new1"]));
                user_update_user($usernew, false, false);
            }
        }

        
        $rvalue = new stdClass();
        $rvalue->message = $message;
        $rvalue->status = $status;
        echo json_encode($rvalue);
        die();
        break;
    case 'userinfochange':
        $data = required_param_array('data', PARAM_RAW);
        
        $usernew = $DB->get_record('lmsdata_user', array('userid' => $USER->id), '*', MUST_EXIST);
        $usernew->address = $data['address'];
        $usernew->phone1 = $data['phone1'];
        $usernew->email2 = $data['email2'];
        $usernew->zipcode = $data['zipcode'];
        $DB->update_record('lmsdata_user', $usernew);
        
        $user = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
        $user->phone1 = $data['phone1'];
        $user->phone2 = $data['birth_y'].'-'.$data['birth_m'].'-'.$data['birth_d'];
        $user->timemodified = time();
        $DB->update_record('user', $user);

        $rvalue = new stdClass();
        $rvalue->status = 'success';
        echo json_encode($rvalue);
        die();
        break;
    case 'newpassword' :
        $data = required_param_array('data', PARAM_RAW);
        $rvalue = new stdClass();
        $rvalue ->status = 1;
        $rvalue ->text = get_string('available', 'local_signup',array("type"=>$typelang));
        if( $data['value'] != trim($data['value']) || strpos($data['value'],' ')){
            $rvalue ->status = 0;
            $rvalue ->text = get_string('blankcheck','local_signup');
        }else{
            if(!check_password_policy($data['value'], $errmsg)){
                $rvalue ->status = 0;
                $rvalue ->text = get_string('passvalcheck','local_signup');
            }
        }
        echo json_encode($rvalue);
        die();
        break;
    case 'cpassword' :
        $data = required_param_array('data', PARAM_RAW);
        $rvalue = new stdClass();
        $rvalue ->status = 'sucess';
        $rvalue ->text = get_string('pw8','local_my');
        
        //현재비밀번호 맞는지 틀린지 체크
        //$usernew = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
        $data['pw'] = preg_replace("/\s+/","",$data['pw']);
        $user = authenticate_user_login($USER->username, $data['pw'], false);
        if($user == false){
            $rvalue ->status  = 'fail';
            $rvalue ->text = get_string('pw6','local_my');
        }
        
        echo json_encode($rvalue);
        die();
        break;
    default:
        throw new moodle_exception('invalidarguments');
}

//echo json_encode($action);
//die();
