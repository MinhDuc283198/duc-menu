<?php
// 별점 1번만 등록 가능하게 설정
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$contentid = required_param('contentid', PARAM_INT);
$star = required_param('star', PARAM_INT);

$user = $USER->id;

$stareval = '';
$countstar = '';

$getstarevaluation = $DB->get_record('lcms_contents_evaluation', array('contentid' => $contentid, 'userid' => $user));
if ($getstarevaluation) {
    if ($getstarevaluation->startimes == 0) {
        $getstarevaluation->startimes = 1;
        $getstarevaluation->timemodified = time();
    } else {
        $getstarevaluation->startimes = 2;
        $getstarevaluation->timemodified = time();
    }
    $stareval = $DB->update_record('lcms_contents_evaluation', $getstarevaluation);
} else {
    $getevaluation = new stdClass();
    $getevaluation->stars = $star;
    $getevaluation->userid = $user;
    $getevaluation->contentid = $contentid;
    $getevaluation->startimes = 1;
    $getevaluation->timecreated = time();
    $getevaluation->timemodified = time();

    $stareval = $DB->insert_record('lcms_contents_evaluation', $getevaluation);
}
if ($stareval) {
    $countstar = $DB->get_record('lcms_contents_evaluation', array('contentid' => $contentid, 'userid' => $user));
}
echo $countstar->stars;

