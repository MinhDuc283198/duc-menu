<?php
/**
 * 강사 강의목록 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/calendar/lib.php');
$context = context_system::instance();

require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string("teachersignup",'local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/mypage.php'));

$date = optional_param('date', 0, PARAM_INT);
$year = date('Y', $date);

if($date) $sql_where = " where lc.classyear = $year";
$sql_select = "select lc.*, lco.coursename  ";
$sql_from = "from {lmsdata_class} lc
            join {lmsdata_course} lco on lc.parentcourseid = lco.id
            join {course} co on co.id = lc.courseid
            join {enrol} e on e.courseid = co.id 
            join {user_enrolments} ue on ue.enrolid = e.id and ue.userid = $USER->id 
            JOIN {context} ctx ON ctx.instanceid = co.id AND contextlevel = 50 
            join {role_assignments} ra on ra.contextid = ctx.id and ra.userid = ue.userid
            JOIN {role} ro ON ra.roleid = ro.id and ro.id = 4 ";

$courses = $DB->get_records_sql($sql_select . $sql_from. $sql_where, $params, ($currentpage - 1) * $myperpage, $myperpage);
$courses_count = $DB->count_records_sql("SELECT COUNT(*) " . $sql_from. $sql_where, $params);
//if(!$courses_count){
//    redirect(get_login_url());
//}

echo $OUTPUT->header();
?>  
<h3 class="page_title"><?php get_string("teachersignup",'local_mypage') ?></h3>

<div class="tableblock">    
    <h5 class="div_title"><?php echo $date ? $year.get_string("yeardo",'local_mypage') : get_string("all",'local_mypage') ?><?php get_string("signuplist",'local_mypage') ?></h5>
    <div style="margin-bottom:10px">        
        <input type="button" value="allsee" class="btn orange" onclick="location.href = 'mycourse_teacher.php'" />
        <input type="button" value="nowyearsee" class="btn orange" onclick="location.href = 'mycourse_teacher.php?date=<?php echo time(); ?>'" />
    </div>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th><?php get_string("cer:coursename",'local_mypage') ?></th>
                <th><?php get_string("gisu",'local_mypage') ?></th>
                <th><?php get_string("cer:learningtime",'local_mypage') ?></th>
                <th><?php get_string("testtime",'local_mypage') ?></th>
                <th><?php get_string("signroom",'local_mypage') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($courses_count) {
                foreach ($courses as $course) {
                    ?>
                    <tr>
                        <td class="text-bold"><?php echo $course->coursename; ?></td>
                        <td class="text-bold"><?php echo $course->classnum.'기'; ?></td>
                        <td>
                            <?php echo date('Y-m-d', $course->learningstart) . '~' . date('Y-m-d', $course->learningend); ?>
                        </td>
                        <td>
                            <?php echo date('Y-m-d', $course->evaluationstart) . '~' . date('Y-m-d', $course->evaluationend); ?>
                        </td>
                        <td>
                            <input type="button" value=<?php get_string("ipjang",'local_mypage') ?> class="btn orange" onclick="location.href = '<?php echo $CFG->wwwroot; ?>/course/view.php?id=<?php echo $course->courseid; ?>'" />
                        </td>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="6"><?php get_string("nosignlist",'local_mypage') ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>



<?php
echo $OUTPUT->footer();
?>


