<?php

/** course_categories 테이블의 idnumber, 신임과정 */
define('JEL_COURSE_LEARNING_USING', 0);

/** course_categories 테이블의 idnumber, 연차별과정 */
define('JEL_COURSE_LEARNING_WAIT', 1);

/** course_categories 테이블의 idnumber, 연차별과정 */
define('JEL_COURSE_LEARNING_CLOSE', 2);

/**
 *  학습자가 등록되어 있는 강의 목록을 반환(lmsdata_course_applications 테이블 기준)
 * @global type $DB
 * @global type $USER
 * @param type $idnumber    - course_categories 테이블의 idnumber
 * @param type $userid      - user 테이블 id
 * @param type $learning    - lmsdata_class 테이블의 학습기간(learningstart, learningend) 체크 여부
 * @return type array      
 */
//function local_mypage_get_courses($idnumber = null, $userid = 0, $learning = JEL_COURSE_LEARNING_USING) {
//    global $DB, $USER;
//
//    if ($userid === 0) {
//        $userid = $USER->id;
//    }
//
//    $where = array();
//    $where[] = 'lca.userid = :userid';
//    $where[] = 'lca.status = :status';
//    $params = array(
//        'userid' => $userid,
//        'status' => 'apply'
//    );
//
//    if (!empty($idnumber)) {
//        $category = $DB->get_field('course_categories', 'id', array('idnumber' => $idnumber));
//        $where[] = 'co.category = :category';
//        $params['category'] = $category;
//    }
//
//    switch ($learning) {
//        case JEL_COURSE_LEARNING_USING:
//            $where[] = 'lc.learningstart <= :time1 AND :time2 <= lc.learningend';
//            $params['time1'] = time();
//            $params['time2'] = time();
//            break;
//        case JEL_COURSE_LEARNING_WAIT:
//            $where[] = 'lc.learningstart > :time1 ';
//            $params['time1'] = time();
//            break;
//        case JEL_COURSE_LEARNING_CLOSE:
//            $where[] = 'lc.learningend < :time1 ';
//            $params['time1'] = time();
//            break;
//    }
//    $params['contextlevel'] = CONTEXT_COURSE;
//    $params['archetype1'] = 'student';
//    $params['archetype2'] = 'editingteacher';
//
//    $sql = 'SELECT 
//                    co.id, co.fullname, 
//                    ca.name AS catename, ca.idnumber, 
//                    lc.learningstart, lc.learningend, lc.evaluationend, lc.evaluationstart, 
//                    lju.totalpoint, lju.sumpoint, ro.archetype 
//            FROM {course} co 
//            JOIN {course_categories} ca ON ca.id = co.category 
//            JOIN {lmsdata_course_applications} lca ON co.id = lca.courseid 
//            JOIN {lmsdata_class} lc ON lc.courseid = co.id 
//            JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid 
//            JOIN {context} ctx ON ctx.instanceid = co.id AND ctx.contextlevel = :contextlevel
//            JOIN {user} u ON u.id = lca.userid 
//            JOIN {role_assignments} ra ON ra.userid = u.id AND ra.contextid = ctx.id
//            JOIN {role} ro ON ro.id = ra.roleid AND (ro.archetype = :archetype1 OR ro.archetype = :archetype2) 
//            LEFT JOIN {local_jeipoint_user} lju ON lju.courseid = co.id AND lju.userid = lca.userid ';
//
//    $where = ' WHERE ' . implode(' AND ', $where);
//
//    $orderBy = ' ORDER BY lc.learningend DESC ';
//    
//    $courses = $DB->get_records_sql($sql . $where . $sql_where . $orderBy, $params);
//
//    return $courses;
//}

/**
 * 전달받은 courses에 생성되어 이수완료 하여 얻은 포인트를 추가하여 반환한다. 
 * @global type $DB
 * @global type $USER
 * @param array $courses    courseid값이 key로 되어있는 array
 * @param type $userid      user 테이블 id
 * @return type
 */
function local_mypage_get_pointList($idnumber, $userid = 0) {
    global $DB, $USER;

    if ($idnumber == JEL_COURSE_CATEGORY_TRUST) {
        $pointtype = 0;
    } else if ($idnumber == JEL_COURSE_CATEGORY_YEAR) {
        $pointtype = 1;
    }

    if ($userid === 0) {
        $userid = $USER->id;
    }

    $sql = 'SELECT 
                courseid, userid, totalpoint, sumpoint, yeartype 
            FROM {local_jeipoint_user} ';

    $where[] = ' userid = :userid ';
    $where[] = ' visible = :visible ';
    $where[] = ' pointtype = :pointtype ';
    $params['userid'] = $userid;
    $params['visible'] = 1;
    $params['pointtype'] = $pointtype;
    $where = ' WHERE ' . implode(' AND ', $where);

    $points = $DB->get_records_sql($sql . $where, $params);

    return $points;
}

/**
 * 강의설정 포인트 및 획득포인트를, progress를 계산해서 반환
 * @param type $courses 
 * @return stdclass 
 */
function local_mypage_get_pointCalculate($courses) {
    global $USER, $DB;
    $pointData = new stdclass();
    $pointData->lecturepoint = 0;
    $pointData->point = 0;
    $pointData->progress = 0;

    if (is_array($courses)) {
        foreach ($courses AS $course) {
            $pointData->lecturepoint += $course->totalpoint;
            $pointData->point += $course->sumpoint;
        }
    } else {
        $pointData->lecturepoint = $courses->totalpoint;
        $pointData->point = $courses->sumpoint;
    }

    if (empty($pointData->point) || empty($pointData->lecturepoint)) {
        $pointData->progress = 0;
    } else {
        $setprogress = round($pointData->point / $pointData->lecturepoint, 2) * 100;
        $pointData->progress = (round($setprogress) > 100) ? 100 : $setprogress;
    }

    return $pointData;
}

/**
 * 사용자가 최근 시청한 컨텐츠 목록 반환함
 * @global type $DB
 * @global type $USER
 * @global type $CFG
 * @param type $courses
 * @param type $userid
 * @param type $page
 * @param type $perpage
 * @return type
 */
function local_mypage_get_watchContent($userid = 0, $page = 1, $perpage = 10) {
    global $DB, $USER, $CFG;

    require_once($CFG->dirroot . '/mod/okmedia/lib.php');

    if ($userid === 0) {
        $userid = $USER->id;
    }

    $sql = 'SELECT 
                cm.*, ok.name, ok.timestart, ok.timeend, 
                ot.timeview, ok.id okid, 
                lc.duration ,lc.filepath, lc.filename 
            FROM {okmedia} ok
            JOIN {course_modules} cm ON cm.instance = ok.id 
            JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname 
            JOIN {course} co on co.id = ok.course 
            JOIN {lmsdata_class} lco on lco.courseid = co.id 
            JOIN {okmedia_track} ot ON ot.okmediaid = ok.id
            LEFT JOIN {lcms_contents_file} lc ON lc.con_seq = ok.contents ';

    $where[] = ' ot.userid = :userid ';
    $params['userid'] = $userid;
    $params['modname'] = 'okmedia';

    $where = ' WHERE ' . implode(' AND ', $where);
    $orderBy = ' ORDER BY ot.timeview DESC ';

    $offset = ($page - 1) * $perpage;

    $contents = $DB->get_records_sql($sql . $where . $orderBy, $params, $offset, $perpage);

    foreach ($contents AS $content) {
        $content->info = okmedia_get_coursemodule_info($content);
        $content->duration = okmedia_time_from_seconds($content->duration);
    }

    return $contents;
}

/**
 * 사용자가 최근 시청한 콘텐츠 목록, 콘텐츠 갯수 반환함
 * @global type $DB
 * @global type $USER
 * @global type $CFG
 * @param type $courses
 * @param type $userid
 * @param type $page
 * @param type $perpage
 * @return type
 */
function local_mypage_get_watchContent_cnt($userid = 0, $page = 1, $perpage = 10) {
    global $DB, $USER, $CFG;

    require_once($CFG->dirroot . '/mod/okmedia/lib.php');

    
    if ($userid === 0) {
        $userid = $USER->id;
    }

    $sql = 'SELECT 
                cm.*, ok.name, ok.timestart, ok.timeend,
                ot.timeview,
                lc.duration ,lc.filepath, lc.filename, ok.id okid 
            FROM {okmedia} ok
            JOIN {course_modules} cm ON cm.instance = ok.id 
            JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname
            JOIN {course} co on co.id = ok.course 
            JOIN {lmsdata_class} lco on lco.courseid = co.id 
            JOIN {okmedia_track} ot ON ot.okmediaid = ok.id
            LEFT JOIN {lcms_contents_file} lc ON lc.con_seq = ok.contents ';

    $sqlcnt = 'SELECT COUNT(*) 
            FROM {okmedia} ok
            JOIN {course_modules} cm ON cm.instance = ok.id 
            JOIN {modules} mo ON mo.id = cm.module AND mo.name = :modname
            JOIN {course} co on co.id = ok.course 
            JOIN {lmsdata_class} lco on lco.courseid = co.id 
            JOIN {okmedia_track} ot ON ot.okmediaid = ok.id
            LEFT JOIN {lcms_contents_file} lc ON lc.con_seq = ok.contents ';

    $where[] = ' ot.userid = :userid ';
    $params['userid'] = $userid;
    $params['modname'] = 'okmedia';

    $where = ' WHERE ' . implode(' AND ', $where);
    $orderBy = ' ORDER BY ot.timeview DESC ';

    $offset = ($page - 1) * $perpage;

    $contents = $DB->get_records_sql($sql . $where . $orderBy, $params, $offset, $perpage);

    foreach ($contents AS $content) {
        $content->info = okmedia_get_coursemodule_info($content);
        $content->duration = okmedia_time_from_seconds($content->duration);
    }
    $countcontents = $DB->count_records_sql($sqlcnt . $where . $orderBy, $params);
    $allcontents = $DB->get_records_sql($sql . $where . $orderBy, $params);

    $returnvalue = array(
        'contents' => $contents,
        'counts' => $countcontents
    );
    return $returnvalue;
}

/**
 * 페이징 함수
 * @param type $totalcount
 * @param type $page
 * @param type $baseurl
 * @param type $perpage_ref
 * @param type $maxdisplay
 */
function local_mypage_pagingbar_script($totalcount, $page, $baseurl, $perpage, $maxdisplay) {
    // 전체 페이징바
    $total_nav_pages = local_mypage_get_total_page($totalcount, $perpage);
    // 현재 페이징바
    $currentnav = (int) ($page / $maxdisplay);
    if (($page % $maxdisplay) > 0) {
        $currentnav += 1;
    }
    // 현재 페이징 바의 첫 페이지
    $page_start = ($currentnav - 1) * $maxdisplay + 1;
    // 현재 페이징 바의 마지막 페이지
    $page_end = $currentnav * $maxdisplay;
    if ($page_end > $total_nav_pages) {
        $page_end = $total_nav_pages;
    }

    echo '<div class="board-breadcrumbs">';

    if ($page > 1) {
        echo '<span class="board-nav-prev">';
        echo html_writer::link(str_replace(':page', $page - 1, $baseurl), '', array('class' => 'prev'));
        echo '</span>';
    }
//     else {
//        echo '<span class="board-nav-prev"><a class="prev" href="#">&lt;</a></span>';
//    }

    echo '<ul>';

    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i === $page) {
            echo '<li class="current" ><a href = "#">' . $i . '</a></li>';
        } else {
            echo '<li>';
            echo html_writer::link(str_replace(':page', $i, $baseurl), $i);
            echo '</li>';
        }
    }

    echo '</ul>';

    if ($page < $total_nav_pages) {
        echo '<span class="board-nav-next">';
        echo html_writer::link(str_replace(':page', $page + 1, $baseurl), '', array('class' => 'next'));
        echo '</span>';
    }
//    else {
//        echo '<span class="board-nav-next"><a class="next" href="#">&lt;</a></span>';
//    }
    echo '</div>';
}

/**
 * 페이징 함수
 * @global type $CFG
 * @param type $totalcount
 * @param type $page
 * @param type $perpage
 * @param type $baseurl
 * @param type $maxdisplay
 */
function local_mypage_pagingbar_script_course($totalcount, $page, $perpage, $baseurl, $maxdisplay = 18) {
    global $CFG;

    $pagelinks = array();

    // 전체 페이징바
    $totalpages = local_mypage_get_total_page($totalcount, $perpage);
    $lastpage = 1;
    if ($totalpages > 0) {
        $lastpage = ceil($totalcount / $perpage);
    }

    if ($page > $lastpage) {
        $page = $lastpage;
    }

    if ($page > round(($maxdisplay / 3) * 2)) {
        $currpage = $page - round($maxdisplay / 2);
        if ($currpage > ($lastpage - $maxdisplay)) {
            $currpage = $lastpage - $maxdisplay;
        }
    } else {
        $currpage = 1;
    }

    $prevlink = '';
    if ($page > 1) {
        $prevlink .= '<span class="board-nav-prev">';
        $prevlink .= html_writer::link(str_replace(':page', $page - 1, $baseurl), '', array('class' => 'prev'));
        $prevlink .= '</span>';
    } else {
//        $prevlink = '<a href="#" class="next"><img alt="next" src="'.$CFG->wwwroot.'/chamktu/img/pagination_left.png"/></a>';
        $prevlink = '<span class="board-nav-prev"><a class="prev" href="#">&lt;</a></span>';
    }

    $nextlink = '';
    if ($page < $lastpage) {
        $nextlink .= '<span class="board-nav-next">';
        $nextlink .= html_writer::link(str_replace(':page', $page + 1, $baseurl), '', array('class' => 'next'));
        $nextlink .= '</span>';
    } else {
//        $nextlink = '<a href="#" class="prev"><img alt="prev" src="'.$CFG->wwwroot.'/chamktu/img/pagination_right.png"/></a>';
        $nextlink = '<span class="board-nav-next"><a class="next" href="#">&lt;</a></span>';
    }

    echo '<div class="board-breadcrumbs">';
    echo $prevlink;
    echo '<ul>';

    if ($currpage > 1) {
        $params['page'] = 1;
        $firstlink = html_writer::link(str_replace(':page', 1, $baseurl), 1);

        $pagelinks[] = $firstlink;
        if ($currpage > 2) {
            $pagelinks[] = '...';
        }
    }

    $displaycount = 0;
    while ($displaycount <= $maxdisplay && $currpage <= $lastpage) {
        if ($page == $currpage) {
            $pagelinks[] = '<li class="current"><a href = "#">' . $currpage . '</a></li>';
        } else {
            $params['page'] = $currpage;
            $pagelink .= '<li>';
            $pagelink .= html_writer::link(str_replace(':page', $currpage, $baseurl), $currpage);
            $pagelink .= '</li>';
            $pagelinks[] = $pagelink;
        }

        $displaycount++;
        $currpage++;
    }

    if ($currpage - 1 < $lastpage) {
        $params['page'] = $lastpage;
        $lastlink = html_writer::link(str_replace(':page', $lastpage, $baseurl), $lastpage);

        if ($currpage != $lastpage) {
            $pagelinks[] = '...';
        }
        $pagelinks[] = $lastlink;
    }



    echo implode('&nbsp;', $pagelinks);

    echo '</ul>';
    echo $nextlink;
    echo '</div>';
}

/**
 * 페이징
 * 총 페이지 수 구하기
 * @param int $rows 총 글의 갯수
 * @param int $limit 한 페이지당 들어갈 글의 갯수
 * @return int 총 페이지 수
 */
function local_mypage_get_total_page($rows, $limit = 12) {
    if ($rows == 0) {
        return 1;
    }

    $totalpages = (int) ($rows / $limit);
    if (($rows % $limit) > 0) {
        $totalpages += 1;
    }
    return $totalpages;
}

/**
 * 학습활동 기준 진도율 반환
 * @global type $DB
 * @param type $courseid
 * @param type $userid
 * @return type
 */
function local_mypage_get_progress($courseid, $userid) {
    global $DB;

    $sql = 'SELECT 
                    cm.id,
                    CASE WHEN cmc.completionstate IS NULL THEN 0 ELSE completionstate END AS state
            FROM {course_modules} cm
            JOIN {course_sections} cs ON cs.id = cm.section AND cs.section <> :section
            LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND userid = :userid
            WHERE cm.course = :courseid AND cm.visible = :visible ';
    $params = array(
        'section' => 0,
        'courseid' => $courseid,
        'userid' => $userid,
        'visible' => 1,
    );

    $completions = $DB->get_records_sql($sql, $params);

    $total = 0;
    $complete = 0;
    foreach ($completions AS $com) {
        $total++;
        if (!empty($com->state)) {
            $complete++;
        }
    }

    if (empty($total) || empty($complete)) {
        $progress = 0;
    } else {
        $setprogress = round($complete / $total, 2) * 100;
        $progress = (round($setprogress) > 100) ? 100 : $setprogress;
    }

    return $progress;
}

function local_mypage_get_betterContent($userid) {
    global $DB;

    $subSql = ' SELECT 
                    MAX(id) AS pid
                FROM {local_jeipoint} 
                WHERE userid = :userid 
                GROUP BY section ';
    $params['userid'] = $userid;

    $sqlFrom = 'SELECT 
                    cm.*, 
                    co.fullname,
                    ok.name, ok.point,
                    lc.learningstart, lc.learningend, 
                    lj.timecreated AS learningtime ';
    $sql = 'FROM {local_jeipoint} lj
            JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = lj.cmid AND cmc.userid = lj.userid AND cmc.completionstate = 0 
            JOIN {course} co ON co.id = lj.courseid
            JOIN {course_modules} cm ON cm.id = lj.cmid
            JOIN {okmedia} ok ON ok.id = cm.instance
            JOIN {lmsdata_class} lc ON lc.courseid = lj.courseid
            JOIN (' . $subSql . ') pid ON pid.pid = lj.id ';
    $sqlWhere[] = ' lj.point < :point ';
    $sqlWhere[] = ' cm.visible = :visible ';
    $sqlWhere[] = ' lc.learningend < :time1 ';
    $sqlWhere[] = ' lj.timecreated < :time2 ';
    $sqlWhere[] = ' lj.timecreated + (60*60*24*30) > :time3 ';
    $sqlWhere = ' WHERE .' . implode(' AND ', $sqlWhere);

    $params['point'] = 0;
    $params['visible'] = 1;
    $params['time1'] = time();
    $params['time2'] = time();
    $params['time3'] = time();

    $contents = $DB->get_records_sql($sqlFrom . $sql . $sqlWhere, $params);

    return $contents;
}
        
function notifi_time($btime){
    $thistime = time(); 

    if(($thistime-86400)<$btime){
        $diff = $thistime - $btime;
        $s = 60; //1분 = 60초
        $h = $s * 60; //1시간 = 60분
        $d = $h * 24; //1일 = 24시간
        $y = $d * 10; //1년 = 1일 * 10일

        if ($diff < $s) {
            $time = $diff . get_string('secbefore','local_mypage');
        } elseif ($h > $diff && $diff >= $s) {
            $time = round($diff/$s) . get_string('minbefore','local_mypage');
        } elseif ($d > $diff && $diff >= $h) {
            $time = round($diff/$h) . get_string('hourbefore','local_mypage');
        } elseif ($y > $diff && $diff >= $d) {
            $time = round($diff/$d) . get_string('daybefore','local_mypage');
        } else {
            $time = date('Y.m.d.', strtotime($btime));
        }

    } else { $time = date('Y.m.d', $btime); }

        return $time;

}

/*
 * 유저가 등록된 강좌 리스트 or 갯수 반환
 * @global type $DB
 * @param type $type = all 전체 / ing 수강중 / end 수강종료 / re 복습기간
 * @param type $cnt == true 일시 갯수만 반환 
 * @param type $userid
 * @return type
 */
function local_mypage_get_mycourse($type = '',$cnt = false,$userid = 0){
    global $DB,$USER;
    if(!$userid){ $userid = $USER->id;}
    $thistime= time();
    switch($type){
        case 'all':
            $where =' z';
            break;
        case 'ing': 
            $where = "z WHERE $thistime < z.enddate";
            break;
        case 'end' :
            $where = "z WHERE $thistime > z.enddate";
            break;
        case 're' :
            $where = "z WHERE $thistime > z.enddate AND $thistime < z.redate";
            break;
    }
        $coursesql = "FROM (SELECT lc.id,lc.title,lc.courseperiod, lc.reviewperiod,lc.vi_title,lc.en_title,lco.courseid as lcocourseid, lco.lecturecnt, lc.courseid as lcid,
                    lc.parentcourseid, ue.timestart as timemodified, (ue.timeend - (86400*lc.reviewperiod)) as enddate, ue.timeend as redate ,
                    case when (select badgeid from {lmsdata_badge_user} WHERE userid = ra.userid AND course = co.id order by id ASC limit 1) then (select badgeid from {lmsdata_badge_user} WHERE userid = ra.userid AND course = co.id order by id ASC limit 1) else 0 end badge 
                    from {lmsdata_class}  lc
                    join {lmsdata_course}  lco on lc.parentcourseid = lco.id
                    join {course}  co on co.id = lc.courseid 
                    join {enrol}  e on e.courseid = co.id
                    join {user_enrolments}  ue on ue.enrolid = e.id AND ue.status = 0 
                    JOIN {context}  ctx ON ctx.instanceid = co.id AND contextlevel = 50
                    join {role_assignments}  ra on ra.contextid = ctx.id and ra.userid = ue.userid AND ra.userid = $userid
                    JOIN {role}  ro ON ra.roleid = ro.id and ro.id = 5  )";
    if($cnt==true){
        $select = "SELECT count(id) ";
        $cnt =$DB->count_records_sql($select.$coursesql.$where);
        return $cnt;
    } else {
        $select = "SELECT * ";
        $courses = $DB->get_records_sql($select.$coursesql.$where);
        return $courses;
    }
}


function local_mypage_get_mylikecourse($userid=0){
    global $DB,$USER;
    if(!$userid){ $userid = $USER->id;}
    $coursesql="select ll.id, lc.id as lcid ,lc.title,lc.courseperiod, lc.reviewperiod,lc.vi_title,lc.en_title,lco.courseid as lcocourseid, lco.lecturecnt,lc.parentcourseid,mc.id mcid, 
                (select c.instanceid from {role_assignments} ra
                JOIN {role} r on r.id = ra.roleid and r.id = 5
                JOIN {context} c on c.id = ra.contextid 
                where ra.userid = $userid and c.instanceid = lc.courseid) as selected
                from {lmsdata_like} ll
                join {lmsdata_class} lc on ll.classid = lc.id
                JOIN {course} mc ON lc.courseid = mc.id
                JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                where ll.userid = $userid AND lc.deleted = 0 AND lc.isused = 0 ORDER BY id DESC";
 
    $courses = $DB->get_records_sql($coursesql);
    return $courses;
}

/*
 * $instanceid(lmsdata_class->courseid), $userid
 * 해당 강의를 들을수 있는 날짜를 반환
 */
function local_mypage_course_date($instanceid,$userid = 0){
    global $DB,$USER;
    if(!$userid){ $userid = $USER->id;}
    $coursesql="select (ue.timeend) as redate, 
                (ue.timeend - (86400*lc.reviewperiod)) as enddate 
                from {role_assignments} ra
                JOIN {role} r on r.id = ra.roleid and r.id = 5
                JOIN {context} c on c.id = ra.contextid
                JOIN {lmsdata_class} lc on lc.courseid = c.instanceid
                JOIN {course} co ON lc.courseid = co.id 
                JOIN {enrol} e on e.courseid = co.id 
                JOIN {user_enrolments} ue ON ue.enrolid = e.id  AND ue.userid = ra.userid
                where ra.userid = $userid and c.instanceid = $instanceid";

    $date = $DB->get_records_sql($coursesql);

    return $date;
}

/*
 * $type = 1 강좌 2 교재
 */
function local_mypage_payment($type = 0,$userid = 0,$page = 1 ,$perpage = 10,$type2 = 0,$count = false){
    global $DB,$USER;
    $lang = current_language();
    if($lang == 'ko'){
        $titlelang = '';
    }else{
        $titlelang = $lang."_";        
    }
    if(!$userid){ $userid = $USER->id;}
    $addsql= ' AND lpd.delivery is null ';
    if($type2==1){
        $addsql =' AND lpd.delivery is not null ';
        $type=3;
    }
    if($type==0){ $addsql ='';}
    $addsql ='';
    //if($type>0){ $type == 1 ? $and =' AND lp.type = 1 ' : $and =' AND lp.type = 2 ';}
    if($type>0){ $and = ' AND lc.type = '.$type;}
     $a = " from m_lmsdata_payment lp
                join m_user u on lp.userid = u.id
                join m_lmsdata_class lc on lc.id = lp.ref
                left join {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                LEFT JOIN {lmsdata_textbook} tb ON tb.id = lc.bookid 
                LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                WHERE lp.userid = :userid $and $addsql
               ";
     if($count==true){
         $select = ' SELECT count(*) ';
         $pays = $DB->count_records_sql($select.$a,array('userid'=>$userid));
     } else {
        $select = 'select lp.*,u.firstname,u.lastname, lc.'.$titlelang.'title title, lc.price ,lc.type ctype,lpd.delivery, tb.id tbid, lr.id lrid, lr.completedate, lr.refundtype ';
        $orderby = 'ORDER BY id DESC';
        $pays = $DB->get_records_sql($select.$a.$orderby,array('userid'=>$userid),$page,$perpage);
     }
    return $pays;
}

//삭제처리되지않은 받은메세지 리턴
function local_mypage_get_message($userid){
    global $DB,$USER;
    if(!$userid){ $userid = $USER->id;}
    $nsql = "SELECT * FROM (SELECT id,useridfrom,useridto,subject,fullmessage,fullmessageformat,fullmessagehtml,smallmessage,notification,contexturl,contexturlname,timecreated,timeread,timeuserfromdeleted,timeusertodeleted 
            FROM {message_read} 
            UNION 
            SELECT id,useridfrom,useridto,subject,fullmessage,fullmessageformat,fullmessagehtml,smallmessage,notification,contexturl,contexturlname,timecreated,0 as timeread,timeuserfromdeleted,timeusertodeleted 
            FROM {message}) z
            WHERE z.useridto = :useridto and z.timeusertodeleted = 0 and timeuserfromdeleted = 0 
            ORDER BY z.timecreated DESC"; 
    $notifications = $DB->get_records_sql($nsql,array('useridto'=>$USER->id));
    
    return $notifications;
    
}

function local_mypage_payment_id($id){
    global $DB,$USER;
    $lang = current_language();
    if($lang == 'ko'){
        $titlelang = '';
    }else{
        $titlelang = $lang."_";        
    }

    //if($type>0){ $type == 1 ? $and =' AND lp.type = 1 ' : $and =' AND lp.type = 2 ';}
    //if($type>0){ $and = ' AND lc.type = '.$type;}
     $a = " from m_lmsdata_payment lp
                join m_user u on lp.userid = u.id
                join m_lmsdata_class lc on lc.id = lp.ref
                left join {lmsdata_productdelivery} lpd on lp.id = lpd.payid
                LEFT JOIN {lmsdata_textbook} tb ON tb.id = lc.bookid 
                LEFT JOIN {lmsdata_refund} lr ON lr.payid = lp.id 
                WHERE lp.id = :id
               ";

    $select = 'select lp.*,u.firstname,u.lastname, lc.'.$titlelang.'title title, lc.price ,lc.type ctype,lc.id lcid,lpd.delivery, lpd.address, lpd.cell, tb.id tbid, lr.id lrid, lr.completedate, lr.refundtype, lr.type lrtype, lr.price lrprice, lr.bank, lr.refundbanknr  ';
    $orderby = 'ORDER BY id DESC';
    $pays = $DB->get_record_sql($select.$a.$orderby,array('id'=>$id),$page,$perpage);
     
    return $pays;
}