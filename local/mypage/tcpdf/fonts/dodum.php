<?php
$type='TrueTypeUnicode';
$name='dodum';
$desc=array('Ascent'=>1070,'Descent'=>-231,'CapHeight'=>1070,'Flags'=>32,'FontBBox'=>'[-657 -371 4084 1377]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>600);
$up=-100;
$ut=50;
$dw=600;
$cw=array();
$enc='';
$diff='';
$file='dodum.z';
$ctg='dodum.ctg.z';
$originalsize=6583552;
// --- EOF ---