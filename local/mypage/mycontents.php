<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/chamktu/contents/lib.php');

$sort = optional_param('sort', 'main', PARAM_TEXT);

$usertype = $DB->get_record('lmsdata_user', array('userid' => $USER->id));
if($usertype -> usertypecode == 30) {
    $sort = 'course';
}
if ($sort == 'course') {
    $courseperpage = optional_param('courseperpage', 6, PARAM_INT);
    $coursepage = optional_param('coursepage', 1, PARAM_INT);
} else if ($sort == 'main') {
    $mainperpage = optional_param('mainperpage', 6, PARAM_INT);
    $mainpage = optional_param('mainpage', 1, PARAM_INT);
}

$maxdisplay = 10;

$context = context_system::instance();

require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("mylearningctn",'local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/mycontents.php'));

$PAGE->requires->jquery();
$PAGE->requires->css('/mod/okmedia/viewer/flowplayer7.2.7/skin/skin.css');
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.speed-menu.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/flowplayer7.2.7/flowplayer.hlsjs.min.js', true);
$PAGE->requires->js('/mod/okmedia/viewer/player.js', true);

echo $OUTPUT->header();
?>  
<!--<script type="text/javascript" src="/mod/lcms/video_player.js"></script>-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#thumb_list ul li").click(function () { // 메인 콘텐츠
            // /local/publishing/pop_quiz.php
            var idnum = $(this).attr('class');
            call_layerPop("/local/mypage/pop_contents_main.php?", "80%", "80%", {'id': idnum});
        });
//        $("#thumb-list ul li").click(function () { // 강의 콘텐츠
//            // /local/publishing/pop_quiz.php
//            var idnum = $(this).attr('class');
//            call_layerPop("/local/mypage/pop_contents.php?", "80%", "80%", {'id': idnum});
//        });
    });

// 새창 refresh
    function mycontents_cata_page(page, strsort) {
        if (strsort == "main") {
            $('[name=mainpage]').val(page);
            $('#contentsfrm').submit();
        } else if (strsort == "course") {
            $('[name=coursepage]').val(page);
            $('#contentsfrm').submit();
        }
    }
</script>
<form id="contentsfrm" action="./mycontents.php">
    <input type="hidden" name="sort" value="<?php echo $sort; ?>"/>
    <?php if ($sort == 'course') { ?>
        <input type="hidden" name="coursepage" value="<?php echo $coursepage; ?>"/>
    <?php } else if ($sort == 'main') { ?>
        <input type="hidden" name="mainpage" value="<?php echo $mainpage; ?>"/>
    <?php } ?>
</form>

<div class="tab_header margin_bt30 tab_event"> <!-- tab-event -->
    <?php
    if ($usertype->usertypecode != 30) {
        ?>
        <span class="<?php echo empty($mainpage) ? '' : 'on '; ?> m-col2 mypage-tab-learning"><a href="<?php echo $CFG->wwwroot . '/local/mypage/mycontents.php?sort=main'; ?>"><?php getg_string('learningctn','local_mypage') ?></a></span>
<?php } ?>
    <span class="<?php echo empty($coursepage) ? '' : 'on '; ?>m-col2 mypage-tab-learning" ><a href="<?php echo $CFG->wwwroot . '/local/mypage/mycontents.php?sort=course'; ?>"><?php getg_string('coursectn','local_mypage') ?></a></span>
</div>

<div class="textbox">
    <?php if ($sort == 'main') { ?><p><?php get_string('yeansu','local_mypage') ?></p><?php } ?>
<?php if ($sort == 'course') { ?><p><?php get_string('pilsu','local_mypage') ?> </p><?php } ?>
</div>
<?php if ($sort == 'course') { ?>
    <div class="div_title line"><?php get_string('mycoursectn','local_mypage') ?></div>
    <!-- thumb start -->
    <div class="thumb-list" id="thumb-list">
        <ul>
            <?php
            $resultval = local_mypage_get_watchContent_cnt($USER->id, $coursepage, $courseperpage);

            $mycontents = $resultval['contents'];
            $totalcount_course = $resultval['counts'];
            if ($mycontents) {
                foreach ($mycontents as $mc) {
                    $sql = 'SELECT * , okm.id okmid '
                            . 'FROM {course_modules} cm '
                            . 'JOIN {okmedia} okm ON cm.instance = okm.id '
                            . 'LEFT JOIN {lcms_contents_file} lcf ON okm.contents = lcf.con_seq '
                            . 'WHERE cm.id =:id';
                    $file = $DB->get_record_sql($sql, array('id' => $mc->id));

                    $thpath = thumbnail_image($mc->info->con_id);

                    $lecturecontent = $DB->get_record_sql('SELECT * FROM {okmedia} where id = ' . $file->okmid);
                    ?>
            <li class="<?php echo $mc->id ?>" onclick="<?php echo $mc->info->onclick ?>">
                        <div class="t-img">
                            <img src="<?php echo $thpath; ?>" alt="" />
                            
                            <!--<span class="t-time"><?php echo $mc->duration->m . ':' . $mc->duration->s; ?></span>-->
                            <?php if($mc->duration->h != '00'){?>
                            <span class="t-time"><?php echo $mc->duration->h . ':' . $mc->duration->m . ':' . $mc->duration->s; ?></span>
                            <?php }else{?>
                            <span class="t-time"><?php echo $mc->duration->m . ':' . $mc->duration->s; ?></span>
                            <?php }?>
                            <span class="t-play">play</span>
                        </div>
                        <div class="t-txt">
                            <h4>
                                <a href="#"><?php echo $mc->name ?></a>
                            </h4>
                            <!--<p><?php echo date('Y-m-d', $mc->timestart) . ' ~ ' . date('Y-m-d', $mc->timeend); ?></p>-->
                                <?php // echo '<p class="num">';  ?>
                            <p>
            <?php echo get_string('spanpoint ','local_mypage') . ( empty($lecturecontent->point) ? '0 P' : $lecturecontent->point . ' P') . '</span>'; ?>
                            </p>
                        </div>
                    </li>
                    <?php
                }
            } else {
                echo '<div class="no-data">'.get_string('learningnoctn.','local_mypage');'</div>';
            }
            ?>
        </ul>
    </div>
    <!-- thumb end -->
    <!-- paging start -->
    <?php
    if ($mycontents) {
        local_mypage_pagingbar_script($totalcount_course, $coursepage, 'javascript:mycontents_cata_page(:page, "course");', $courseperpage, $maxdisplay);
    }
    ?>
    <!-- paging end -->
<?php } else if ($sort == 'main') {
    ?> 
    <div class="div_title line"><?php get_string('mylearningctn','local_mypage') ?></div>
    <!-- thumb start -->
    <div class="thumb-list" id="thumb_list">
        <ul>
            <?php
// lcms_track 테이블에 insert할 수 있어야 제대로 나오는지를 확인할 수 있을 듯
            $mainsql = "SELECT "
                    . "rep.id , lcf.filename, lcf.filepath, lcf.duration, con.category, "
                    . "con.id AS con_id, con.con_name, con.con_type, con.con_des, "
                    . "con.update_dt, con.data_dir, con.embed_type, con.embed_code "
                    . "FROM {lcms_repository} rep "
                    . "JOIN {lcms_contents} con ON con.id= rep.lcmsid "
                    . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
                    . "JOIN {lcms_track} lt ON lt.lcms = con.id ";
            $msort = " WHERE lt.userid=:userid "; //최종학습 카테고리가 무엇언지...
            $morder = " ORDER BY lt.timeview DESC ";
            $mymaincontents = $DB->get_records_sql($mainsql . $msort . $morder, array('userid' => $USER->id), ($mainpage - 1) * $mainperpage, $mainperpage);

            $maincount_sql = "SELECT COUNT(con.id) "
                    . "FROM {lcms_repository} rep "
                    . "JOIN {lcms_contents} con ON con.id= rep.lcmsid "
                    . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
                    . "JOIN {lcms_track} lt ON lt.lcms = con.id ";
            $totalcount_main = $DB->count_records_sql($maincount_sql . $msort, array('userid' => $USER->id));

            if ($mymaincontents) {
                foreach ($mymaincontents as $mc) {
                    $thpath = thumbnail_image($mc->con_id);
                    $sql = 'SELECT * '
                            . 'FROM {course_modules} cm '
                            . 'JOIN {okmedia} okm ON cm.instance = okm.id '
                            . 'LEFT JOIN {lcms_contents_file} lcf ON okm.contents = lcf.con_seq '
                            . 'WHERE cm.id =:id';
                    $file = $DB->get_record_sql($sql, array('id' => $mc->id), 0, 12);

                    $getcounts = $DB->count_records('lcms_contents_viewcnt', array('contentid' => $mc->con_id));
                    ?>
                    <li class="<?php echo $mc->con_id ?>">
                        <div class="t-img">
                            <img src="<?php echo $thpath; ?>" alt="" />
                            <!--<span class="t-time"><?php echo gmdate('i:s', $mc->duration); ?></span>-->
                            <span class="t-time"><?php
                        echo gmdate('H', $mc->duration) == 00 ? '' : gmdate('H:', $mc->duration);
                        echo gmdate('i:s', $mc->duration);
                        ?></span> 
                            <span class="t-play">play</span>
                        </div>
                        <div class="t-txt">
                            <?php
                            $getcategorysql = 'SELECT lc.id, lcn.name, lc.depth, lc.parent '
                                    . 'FROM {lmsdata_category} lc '
                                    . 'JOIN {lmsdata_category_name} lcn ON lcn.categoryid = lc.id AND lcn.lang = :lang '
                                    . 'where lc.depth = 2 and lc.id = :lcid';
                            $getcategory = $DB->get_record_sql($getcategorysql, array('lcid' => $mc->category, 'lang' => current_language()));
                            $getcategorypar = $DB->get_record('lmsdata_category_name', array('categoryid' => $getcategory->parent, 'lang' => current_language()));
                            ?>
                            <p class="cate">
                                <span><?php echo $getcategorypar->name; ?></span>
                                <span><?php echo $getcategory->name; ?></span>
                            </p>
                            <h4>
                                <a href="#"><?php echo $mc->con_name ?></a>
                            </h4>
                            <p class="num"><?php echo get_string('spanjohal','local_mypage') . $getcounts; ?></p>
                        </div>
                    </li>
                    <?php
                }
            } else {
                echo '<div class="no-data">'.get_string('learningnoeductn','local_mypage');'</div>';
            }
            ?>
        </ul>
    </div>
    <!-- thumb end -->
    <?php
    if ($mymaincontents) {
        local_mypage_pagingbar_script($totalcount_main, $mainpage, 'javascript:mycontents_cata_page(:page, "main");', $mainperpage, $maxdisplay);
    }
    ?>
    <?php
}
?>

<script>
    $('.mypage-tab-learning').click(function () {
        location.href = ($(this).find('a')).attr('href');
    });
</script>
<!-- thumb end -->

<?php
echo $OUTPUT->footer();
?>


