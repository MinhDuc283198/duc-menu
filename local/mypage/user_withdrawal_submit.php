<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/lib/moodlelib.php');

require_login();

$password = required_param('password', PARAM_RAW);

$user = $DB->get_record('user', array('id'=>$USER->id));
if(validate_internal_user_password($user, $password)) {
    $DB->delete_records('lmsdata_user', array('userid'=>$user->id));
    delete_user($user);
    redirect($CFG->wwwroot);
} else {
    echo '<script type="text/javascript">alert("'.get_string('pwdnosame','local_mypage');'")</script>';
    redirect($CFG->wwwroot.'/local/mypage/user_withdrawal.php');
}