<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$mod = required_param('mod', PARAM_RAW);

$resultvalue = '';
if ($mod == 'write') {
    $contentid = required_param('contentid', PARAM_INT);
    $comment = required_param('comments', PARAM_TEXT);

    $commentdata = new stdClass();
    $commentdata->contentid = $contentid;
    $commentdata->userid = $USER->id;
    $commentdata->con_des = $comment;
    $commentdata->timecreated = time();

    $resultvalue = $DB->insert_record('lcms_contents_comments', $commentdata);
} else if ($mod == 'delete') {
    $commentid = required_param('commentid', PARAM_INT);

    $deleteable = $DB->get_record('lcms_contents_comments', array('id' => $commentid, 'userid' => $USER->id));
    if ($deleteable) {
        $resultvalue = $DB->delete_records('lcms_contents_comments', array('id' => $commentid));
    }
} else if ($mod == 'update') {
    $commentid = required_param('commentid', PARAM_INT);
    $content = optional_param('comments', '', PARAM_TEXT);

    $updateable = $DB->get_record('lcms_contents_comments', array('id' => $commentid, 'userid' => $USER->id));
    if ($updateable) {
        $commentdata = new stdClass();
        $commentdata -> id = $updateable -> id;
        $commentdata -> contentid = $updateable -> contentid;
        $commentdata -> userid = $USER -> id;
        $commentdata -> con_des = empty($content)? $updateable -> con_des : $content;
        $commentdata -> timecreated = $updateable -> timecreated;
        $commentdata -> timemodified = time();
        
        $resultvalue = $DB->update_record('lcms_contents_comments', $commentdata);
    }
}

echo $resultvalue;
