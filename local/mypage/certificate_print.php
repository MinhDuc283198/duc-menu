<?php

/**
 * 이수증 출력 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/lib/filelib.php';
require_once $CFG->dirroot . '/lib/pdflib.php';
require_once $CFG->dirroot . '/local/mypage/fpdi/fpdi.php';
require_once 'certi_sample.php';

setlocale(LC_ALL, "en_US.UTF-8");
$id = required_param('id', PARAM_INT);
$userid = required_param('userid', PARAM_INT);
$olddata = optional_param('olddata', 0, PARAM_INT);
// Check for valid admin user - no guest autologin
//전교조 홈페이지에서 바로 넘어 왔을 때 로그인 문제 처리
if (isset($_COOKIE['authToken']) && !isloggedin()) {
    $output = eduhope_api_login($_COOKIE['authToken'], $_SERVER['REMOTE_ADDR']);
    if ($output->dataResult == '00' && $output->taskResult == '00') {
        $user = $DB->get_record('user', array('username' => $output->returnData[0]->memberId));
        if ($user->id != $userid) {
            echo '<script type="text/javascript">alert("본인의 이수증이 아닙니다."); location.href="' . $CFG->wwwroot . '/local/mypage/certificate.php";</script>';
            die();
        }
    } else {
        echo '<script type="text/javascript">alert("로그인 API 오류 (dataResult : ' . $output->dataResult . ' / taskResult : ' . $output->taskResult . ')"); location.href="' . $CFG->wwwroot . '/local/mypage/certificate.php";</script>';
        die();
    }
} else {
    require_login();
}
$context = context_system::instance();

if ($olddata == 0) {
    //이수증에 넣을 데이터 가져옴
    $sql = "select lca.id, lca.userid, lca.neiscode, lca.locationnumber, lco.courseid, lco.coursename, "
            . "lca.birthday, u.lastname, lc.id as lcid, lc.courseid, lco.classobject as classtype, lc.classyear, lc.classnum, "
            . "lc.learningstart, lc.learningend, lco.learningtime, lc.certificatepre, lc.certificateimg ";
    $from = "
        from {lmsdata_course_applications} lca 
        join {user} u on u.id = lca.userid 
        join {lmsdata_class} lc on lc.courseid = lca.courseid 
        join {lmsdata_course} lco on lco.id = lc.parentcourseid 

        where lca.completionstatus = 1 and lca.userid = :userid and lca.id = :id";
    $cert = $DB->get_record_sql($sql . $from, array('userid' => $userid, 'id' => $id));
    if (empty($cert)) {
        echo '<script type="text/javascript">alert("출력 가능한 이수증이 없습니다."); location.href="' . $CFG->wwwroot . '/local/mypage/certificate.php";</script>';
        die();
    }



    if ($cert->classtype == 1) {
        $coursetype = get_string('cer:classtype1', 'local_mypage') . '-';
    } else if ($cert->classtype == 2) {
        $coursetype = get_string('cer:classtype2', 'local_mypage') . '-';
    } else if ($cert->classtype == 3) {
        $coursetype = get_string('cer:classtype3', 'local_mypage') . '-';
    } else if ($cert->classtype == 4) {
        $coursetype = get_string('cer:classtype4', 'local_mypage') . '-';
    }
    $sql2 = 'select * from {lmsdata_certificate_history} where classid =' . $cert->lcid . ' and userid = ' . $cert->userid . ''; //이수증 발급 내역 가져옴
    $history = $DB->get_record_sql($sql2);

//    if($cert->classtype)
    if (!$history) {
        $prefix = $cert->certificatepre;
        // 최대 8자리 숫자 lmsdata_course_application 아이디번호와 앞에 0을 합쳐 8자리 숫자를 만듬
        $defaultnum = '00000000';
        $index = strlen($cert->id);
        $number = substr($defaultnum, 0, 8 - $index);
        $indexnum = $number . $cert->id;
        if ($prefix) {
            $documentid = $prefix . '-' . $coursetype . $cert->classyear . '-' . $cert->classnum . '-' . $indexnum; //증서번호;    
        } else {
            $documentid = get_string('cer:chamedu', 'local_mypage') . '-' . $coursetype . $cert->classyear . '-' . $cert->classnum . '-' . $indexnum;
        }
    } else {
        $documentid = $history->certificatenum;
    }


    //classtype값에서 -만 뺴서 사용
    $coursetypestr = substr($coursetype, 0, 6);
    //이수이미지 가져옴
    $context1 = CONTEXT_COURSE::instance($cert->courseid);
    $fs = get_file_storage();
    $files = $fs->get_area_files($context1->id, 'local_courselist', 'certificateimg', 0, '', false);
    foreach ($files as $file) {
        if ($file->get_filesize() > 0) {
            $filename = $file->get_filename(); //파일이름가져옴
        }
    }

    $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context1->id . '/local_courselist/certificateimg/0/' . $filename); //이수이미지 파일경로


    $vars = new stdClass();
    $vars->background = ($cert->certificateimg) ? $path : $CFG->dirroot . '/local/mypage/img/certi/completion640890.jpg'; //배경사진
    $vars->cell = ($cert->certificateimg) ? $CFG->dirroot . '/local/mypage/img/certi/cell6.JPG' : $CFG->dirroot . '/local/mypage/img/certi/cell5.JPG'; //테이블 사진
    $vars->documentid = $documentid; //증서번호
    $vars->name = $cert->lastname; //성명
    $vars->neisbirth = ($cert->neiscode) ? $cert->neiscode : $cert->birthday; //네이스코드 없으면 생년월일
    $vars->coursetype = $coursetypestr . ' ' . $cert->codename; //연수종별
    $vars->coursetime = $cert->learningtime . get_string('cer:time', 'local_mypage'); //이수시간
    $vars->learntime = date(get_string('yymmdd','local_mypage'), $cert->learningstart) . ' ~ ' . date(get_string('yymmdd','local_mypage'), $cert->learningend); //연수기간
    $vars->coursename = $cert->coursename; //연수과정명
    $vars->location = $cert->locationnumber; //지명번호
    $vars->description = get_string('cer:text', 'local_mypage'); //이수 문구
    $vars->issuedate = date(get_string('yymmdd','local_mypage'), time()); //이수증 발급날짜 

    if ($history->id) {//이미 내역이 있으면 update
        $data->id = $history->id;
        $data->issuedcount = $history->issuedcount + 1;
        $data->timemodified = time();
        $DB->update_record('lmsdata_certificate_history', $data);
    } else {//내역이 없으면 insert
        $data = new stdClass();
        $data->userid = $cert->userid;
        $data->courseid = $cert->courseid;
        $data->place = $cert->locationnumber;
        $data->certificatenum = $vars->documentid;
        $data->classid = $cert->lcid;
        $data->issuedcount = 1;
        $data->timecreated = time();
        $data->timemodified = time();
        $DB->insert_record('lmsdata_certificate_history', $data);
    }
} else {
    //이수증에 넣을 데이터 가져옴                                                                           
    $sql = "select lao.id, lao.userid, lao.neiscode, lao.locationnumber, lco.courseid, lco.coursename, lu.birthday, u.lastname, c.codename, lao.chasunr, lao.finishnr,
        lco.classtype, lcro.iYear classyear, lcro.SeqNo classnum, lcro.iType ,lao.studystart learningstart, lao.studyend learningend, lco.learningtime ";
    $from = "
        FROM {lmsdata_applications_old} lao
        JOIN {lmsdata_course} lco on lco.coursecd = lao.coursecd 
        join {lmsdata_class_real_old} lcro on lcro.ChasuNR = lao.chasunr
        join {lmsdata_code} c on c.id = lco.coursegrade  
        join {user} u on u.id = lao.userid 
        join {lmsdata_user} lu on u.id = lu.userid 
        where lao.completionstatus = 1 and lao.userid = :userid and lao.id = :id ";

    $cert = $DB->get_record_sql($sql . $from, array('userid' => $userid, 'id' => $id));

    if (empty($cert)) {
        echo '<script type="text/javascript">alert("출력 가능한 이수증이 없습니다."); location.href="' . $CFG->wwwroot . '/local/mypage/certificate.php";</script>';
        die();
    }
    $classtype_gubun = array();
    $classtype_gubun = explode('-', $cert->finishnr);
    if ($cert->itype == 'Y' || $classtype_gubun[1] == get_string('roll','local_mypage')) {//classtype에 따라 값을 나눔
        $coursetype = get_string('cer:classtype1', 'local_mypage');
    } else {
        $coursetype = get_string('cer:classtype3', 'local_mypage');
    }

    $vars = new stdClass();
    $vars->background = $CFG->dirroot . '/local/mypage/img/certi/completion640890.jpg'; //배경사진
    $vars->cell = ($cert->certificateimg) ? $CFG->dirroot . '/local/mypage/img/certi/cell6.JPG' : $CFG->dirroot . '/local/mypage/img/certi/cell5.JPG'; //테이블 사진
    $vars->documentid = $cert->finishnr; //증서번호
    $vars->name = $cert->lastname; //성명
    $vars->neisbirth = ($cert->neiscode) ? $cert->neiscode : $cert->birthday; //네이스코드 없으면 생년월일
    $vars->coursetype = $coursetype . ' ' . $cert->codename; //연수종별
    $vars->coursetime = $cert->learningtime . get_string('cer:time', 'local_mypage'); //이수시간
    $vars->learntime = date(get_string('yymmdd','local_mypage'), $cert->learningstart) . ' ~ ' . date(get_string('yymmdd','local_mypage'), $cert->learningend); //연수기간
    $vars->coursename = $cert->coursename; //연수과정명
    $vars->location = $cert->locationnumber; //지명번호
    $vars->description = get_string('cer:text', 'local_mypage'); //이수 문구
    $vars->issuedate = date(get_string('yymmdd','local_mypage'), time()); //이수증 발급날짜 
}


$pdf = new completionPDF($vars->background); //이미지가 들어있는 폴더 
$pdf->Write($vars); //write 함수로 vars변수의 값들을 넘김
$pdf->Output('cert.pdf', 'I'); //pdf출력
