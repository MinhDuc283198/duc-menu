<?php
/**
 * 프로필 수정 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/local/mypage/lib.php';
$context = context_system::instance();

require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));
$PAGE->navbar->add(get_string('privacy', 'local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/editprofile.php'));

$sql = 'SELECT lu.*, mu.lastname, mu.email
        FROM {user} mu
        LEFT JOIN {lmsdata_user} lu on lu.userid = mu.id
        WHERE mu.id = :userid';
$userinfo = $DB->get_record_sql($sql, array('userid' => $USER->id));

$menu_gubun = optional_param("menu_gubun", 1, PARAM_INT);
$first_phone = substr($userinfo->phone1, 0, 3);
$middle_phone = substr($userinfo->phone1, 3, 4);
$last_phone = substr($userinfo->phone1, 7, 4);
$email = explode('@', $userinfo->email);

echo $OUTPUT->header();
?>  
<script type="text/javascript">
    var nickname_allow = false;
    var nickname = '<?php echo $userinfo->nickname; ?>';
    $(document).ready(function () {
        $(document).ready(function () {
            $(".eType").change(function () {
                var val = $(this).val();
                if (val == get_string("directinput",'local_mypage')) {
                    $(".email02").val("").removeAttr("disabled");
                } else {
                    $(".email02").val(val).attr("disabled", "disabled");
                }
            });

        });

    });

    /**
     * 닉네임 중복체크
     * @returns {undefined}
     */
    function nickname_chk() {
        $.ajax({
            url: 'nickname_chk.php',
            method: 'POST',
            dataType: 'json',
            data: {
                nickname: $('input[name=nickname]').val(),
            },
            success: function (data) {
                if (data.status == 'success') {
                    alert(get_string('canusename','local_mypage'));
                    nickname_allow = true;
                } else {
                    alert(get_string('samenamecantusename','local_mypage'));
                    nickname_allow = false;
                }
            }
        });
    }

    /**
     * 필명 validation
     * @returns {undefined}
     */
    $('#form1').submit(function () {
        if (!nickname_chk && $('input[name=nickname]').val() != nickname) {
            alert(get_string('checkname','local_mypage'));
            return false;
        }
    });
</script>

<h3 class="page_title">
    <?php get_string('privacymodified','local_mypage') ?>
</h3>
<form id="form1" action="editprofile_submit.php?type=1" method="POST" onsubmit="return onsubmit_chk();">
    <div class="block01 on">
        <table class="table">
            <colgroup>
                <col width="33%" />
                <col width="/" /> 
            </colgroup>
            <tbody>
                <!-- 일반회원 -->
                <tr>
                    <th><?php get_string("username",'local_mypage') ?></th>
                    <td class="text-left">
                        <input type="text" name="name" title=<?php get_string("username",'local_mypage') ?> class="m-col1" value="<?php echo $userinfo->lastname; ?>" disabled /> 
                    </td>
                </tr>
                <tr>
                    <th><?php get_string('phonenum','local_mypage') ?></th>
                    <td class="text-left phone">
                        <input type="text" name="phone1" title="number01" value="<?php echo $first_phone; ?>" size="10" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/> -
                        <input type="text" name="phone2" title="number02" value="<?php echo $middle_phone; ?>"size="10" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/> -
                        <input type="text" name="phone3" title="number03" value="<?php echo $last_phone; ?>" size="10" onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)' style='ime-mode:disabled;'/>
                    </td>
                </tr>
                <tr>
                    <th><?php get_string('elepost','local_mypage') ?></th>
                    <td class="text-left">
                        <input type="text" name="email1" value="<?php echo $email[0]; ?>" title=<?php get_string('elepost','local_mypage') ?> class="email01" />@
                        <input type="text" name="email2" value="<?php echo $email[1]; ?>" class="email02" title=<?php get_string('elepost','local_mypage') ?> placeholder=<?php get_string("directinput",'local_mypage') ?> />
                        <select class="eType">
                            <option value=<?php get_string("directinput",'local_mypage') ?>>-- 직접입력 --</option>
                            <option value="gmail.com">gmail.com</option>
                            <option value="naver.com">naver.com</option>
                            <option value="nate.com">nate.com</option>
                            <option value="korea.com">korea.com</option>
                            <option value="sen.go.kr">sen.go.kr</option>
                            <option value="cbe.go.kr">cbe.go.kr</option>
                            <option value="cnoe.go.kr">cnoe.go.kr</option>
                            <option value="jbedu.go.kr">jbedu.go.kr</option>
                            <option value="gyo6.go.kr">gyo6.go.kr</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><?php get_string('emailnum','local_mypage') ?></th>
                    <td class="text-left">
                        <input type="text" name="zipcode" value="<?php echo $userinfo->zipcode; ?>" class="w70px" title=<?php get_string("emailnum",'local_mypage') ?> /> 
                    </td>
                </tr>
                <tr>
                    <th><?php get_string('address','local_mypage') ?></th>
                    <td class="text-left">
                        <input type="text" name="address" value="<?php echo $userinfo->address; ?>" title=<?php get_string("address",'local_mypage') ?> m-width="80" />
                        <input type="button" name="search_adrs" value=<?php get_string("addresserch",'local_mypage') ?> class="btn black" />
                        <input type="text" name="address_detail" value="<?php echo $userinfo->address_detail; ?>" class="w100 margin_tp10" title=<?php get_string("address",'local_mypage') ?> />
                    </td>
                </tr>
                <tr>
                    <th><?php get_string('eamilsusin','local_mypage') ?></th>
                    <td class="text-left">
                        <input type="radio" value="1" name="allowemail" <?php if ($userinfo->allowemail == 1) echo 'checked'; ?> id="etrue" />
                        <label for="etrue"><?php get_string('accept','local_mypage') ?></label>
                        <input type="radio" value="0" name="allowemail" <?php if ($userinfo->allowemail == 0) echo 'checked'; ?> id="efalse" />
                        <label for="efalse"><?php get_string('baccept','local_mypage') ?></label>
                    </td>
                </tr>
            </tbody>
        </table>

        <div class="text-center">
            <input type="submit" value=<?php get_string("save",'local_mypage') ?> class="btn big pink "/>
        </div>
    </div>
</form>

<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/chamktu/css/jquery-ui.css">
<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script src="<?php echo $CFG->wwwroot; ?>/chamktu/js/lib/jquery.ui.datepicker_lang.js"></script>
<script>
                            /**
                             * load함수를 이용하여 core스크립트의 로딩이 완료된 후, 우편번호 서비스를 실행합니다.
                             */
                            $('input[name=search_adrs]').click(function () {
                                daum.postcode.load(function () {
                                    new daum.Postcode({
                                        oncomplete: function (data) {
                                            $('input[name=address]').val(data.jibunAddress);
                                        }
                                    }).open();
                                });
                            });


//    var phone1 = '<?php $userinfo->phone1; ?>'
//    var phone2 = '<?php $userinfo->phone2; ?>'
                            //var c = $('input[name=phone3]').val();

                            $(document).ready(function () {
                                $("#birthday").datepicker({
                                    dateFormat: 'yymmdd' //Input Display Format 변경
                                    , changeYear: true //콤보박스에서 년 선택 가능
                                    , changeMonth: true //콤보박스에서 월 선택 가능
                                });

                            });
                            //휴대폰 validation
                            $('#phonechk').click(function () {
                                var phone1 = $('input[name=phone1]').val();
                                var phone2 = $('input[name=phone2]').val();
                                var phone3 = $('input[name=phone3]').val();
                            });
                            /**
                             * form validation 함수
                             * @returns {Boolean}     
                             * */
                            function onsubmit_chk() {
                                if ($('input[name=phone_complete]').val() == 1) {
                                    alert(get_string('allbalen','local_mypage'));
                                    return false;
                                }
                            }

                            /**
                             * 입력데이터 유효성 검사
                             * @param {type} event
                             * @returns {undefined|Boolean}     */
                            function onlyNumber(event) {
                                event = event || window.event;
                                var keyID = (event.which) ? event.which : event.keyCode;
                                if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
                                    return;
                                else
                                    return false;
                            }

                            /**
                             * 기존 입력데이터 삭제함수
                             * @param {type} event
                             * @returns {undefined}     */
                            function removeChar(event) {
                                event = event || window.event;
                                var keyID = (event.which) ? event.which : event.keyCode;
                                if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
                                    return;
                                else
                                    event.target.value = event.target.value.replace(/[^0-9]/g, "");
                            }
</script>

<?php
echo $OUTPUT->footer();
?>
   