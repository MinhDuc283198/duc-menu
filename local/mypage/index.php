<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));

echo $OUTPUT->header();
?>  
<link href="/theme/oklassedu/style/fullcalendar.min.css" rel="stylesheet" />
<script src="/theme/oklassedu/javascript/plugin/moment.min.js" type="text/javascript"></script>
<script src="/theme/oklassedu/javascript/plugin/fullcalendar.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        /** calendar events **/
        var today = new Date();

        $('#calendar').fullCalendar({
            defaultDate: today,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: 'CBX fanmetting',
                    start: '2017-06-07'
                },
                {
                    title: 'CBX fanmetting',
                    start: '2017-06-07'
                },
                {
                    title: 'CBX fanmetting',
                    start: '2017-06-07'
                },
                {
                    title: 'CBX fanmetting',
                    start: '2017-06-07'
                }
            ]
        });
        /** calendar events **/

        /** pie chart **/
        call_piechart($(".pie"));
        /** pie chart **/

    });


</script>
<h3 class="page_title"><?php get_string('mypagehome','local_mypage') ?></h3>

<div class="myinfo">
    <div class="myimg l_content">
        <img src="/theme/oklassedu/pix/images/user.jpg" alt="img"/>
        <p class="addpic"><a href="#"><?php get_string('picture','local_mypage') ?></a></p>
    </div>
    <div class="mytext r_content">
        <h3><?php get_string('master','local_mypage') ?></h3>
        <ul>
            <li><?php get_string('jinhnag','local_mypage') ?></li>
            <li><?php get_string('wanruo','local_mypage') ?></li>
            <li><?php get_string('yeahjeong','local_mypage') ?></li>
        </ul>
        <p><?php get_string('choigenlogin','local_mypage') ?></p>
        <div class="buttons">
            <input type="button" class="btn orange" name="btn01" value=<?php get_string("privacy",'local_mypage') ?>/>
            <input type="button" class="btn gray" name="btn01" value=<?php get_string("logout",'local_mypage') ?>/>
        </div>
    </div>
</div>

<div class="boardblock">
    <div class="board tab">
        <h5 class="head">
            <!--data-url : 게시판 url을 넣어주세요-->
            <p class="on" data-url="#board01"><?php get_string('notice','local_mypage') ?></p>
            <p data-url="#board02"><?php get_string('qanda','local_mypage') ?></p>
            <span class="circle"><a href="#board01">&gt;</a></span>
        </h5>
        <ul class="board_list on">
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span><?php get_string('mylifeedu','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span><?php get_string('gangea','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span> <?php get_string('jilhigh','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span> <?php get_string('jilhigh','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
        </ul>

        <ul class="board_list">
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span><?php get_string('qandageasipan','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span><?php get_string('qandageasipan','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span><?php get_string('qandageasipan','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
            <li>
                <a href="#">
                    <span>
                        <span class="gray"><?php get_string('jasingam','local_mypage') ?></span><?php get_string('qandageasipan','local_mypage') ?>
                    </span>
                </a>
                <span class="date gray">
                    2017-05-29
                </span>
            </li>
        </ul>
    </div>
</div>

<div class="tableblock">
    <h5 class="div_title"><?php get_string('courseprogress','local_mypage') ?></h5>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th><?php get_string('classification','local_mypage') ?></th>
                <th><?php get_string('cer:coursename','local_mypage') ?></th>
                <th><?php get_string('cer:classnum','local_mypage') ?></th>
                <th><?php get_string('grades','local_mypage') ?></th>
                <th><?php get_string('cer:learningtime','local_mypage') ?></th>
                <th><?php get_string('progressrate','local_mypage') ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php get_string('jobtraining','local_mypage') ?></td>
                <td><?php get_string('jajonegam','local_mypage') ?></td>
                <td><?php get_string('ocha','local_mypage') ?></td>
                <td><?php get_string('ilhackjem','local_mypage') ?></td>
                <td>2017-07-04 ~ 2017-08-07</td>
                <td>
                    <div class="pie">
                        <div class="slice01">
                            <div></div>
                        </div>

                        <div class="slice02">
                            <div></div>
                        </div>
                        <div class="info">
                            <p class="percent">33%</p>
                            <p class="cplt">complete</p>
                        </div>
                    </div>



                </td>
            </tr>
            <tr>
                <td><?php get_string('jobtraining','local_mypage') ?></td>
                <td><?php get_string('jajonegam','local_mypage') ?></td>
                <td><?php get_string('ocha','local_mypage') ?></td>
                <td><?php get_string('ilhackjem','local_mypage') ?></td>
                <td>2017-07-04 ~ 2017-08-07</td>
                <td>
                    <div class="pie">
                        <div class="slice01">
                            <div></div>
                        </div>

                        <div class="slice02">
                            <div></div>
                        </div>
                        <div class="info">
                            <p class="percent">60%</p>
                            <p class="cplt">complete</p>
                        </div>
                    </div>



                </td>
            </tr>

        </tbody>
    </table>
</div>

<div class="myschedule l_content">
    <h5 class="div_title">
        <?php get_string('myplan','local_mypage') ?>
        <span class="more">+</span>
    </h5>
    <div id="calendar">

    </div>

</div>

<div class="quickview r_content">
    <div class="linkblock">
        <h5><?php get_string('educationsupportcenter','local_mypage') ?></h5>
        <ul>
            <li>
                <a href="#">
                    <img src="/theme/oklassedu/pix/images/bn_img01.png" alt=""/>
                    <span><?php get_string('groupattendance','local_mypage') ?></span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="/theme/oklassedu/pix/images/bn_img02.png" alt=""/>
                    <span><?php get_string('smokeandcancle','local_mypage') ?></span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="/theme/oklassedu/pix/images/bn_img03.png" alt=""/>
                    <span><?php get_string('learning','local_mypage') ?></span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="/theme/oklassedu/pix/images/bn_img04.png" alt=""/>
                    <span>NEIS &amp; <br /> <?php get_string('learningpicknum','local_mypage') ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $CFG->wwwroot.'/local/mypage/certificate.php'?>">
                    <img src="/theme/oklassedu/pix/images/bn_img05.png" alt=""/>
                    <span><?php get_string('cer:cerprint','local_mypage') ?></span>
                </a>
            </li>
        </ul>

        <div class="info01">
            <a href="#">
                <h5><?php get_string('servicecenter','local_mypage') ?></h5>
                <p class="number">
                    02-2670-<span class="orange">9465.9466</span>
                </p>
                <p class="time">
                    <?php get_string('sangdam','local_mypage') ?>
                </p>

            </a>
        </div>
    </div>
</div>


<?php
echo $OUTPUT->footer();
?>


