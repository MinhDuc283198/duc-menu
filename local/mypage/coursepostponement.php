<?php
/**
 * 수강 신청 연기 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));
$PAGE->navbar->add(get_string('courseinfo', 'local_mypage'));
$PAGE->navbar->add(get_string("signuplate",'local_mypage'));

require_login();

echo $OUTPUT->header();

$time1 = $time2 = $time3 = time();

//현재 수강중인 강의 정보 (직무인것만 처리 => classtype = 1)
$sql = 'select lca.id, lca.courseid, lca.status, lco.coursename, lc.classtype, lc.classyear, lc.classnum, lc.learningstart, lc.learningend  
from m_lmsdata_course_applications lca 
join m_user u on u.id = lca.userid 
join m_lmsdata_class lc on lc.id = lca.courseid 
join m_lmsdata_course lco on lco.id = lc.parentcourseid 
where lca.userid = :userid and lca.status = :status and lc.classtype = 1 
and lc.learningend > :time1
order by lca.id desc';
$classes = $DB->get_records_sql($sql,array('userid'=>$USER->id,'status'=>'apply','time1'=>$time1));

//일정표 가져오기
$sql = 'select * from m_lmsdata_schedule where classtype = 1 and learningstart > :time2 and openyn = 0 order by classnum asc';
$schedules = $DB->get_records_sql($sql,array('time2'=>$time2));

?>  
<h3 class="page_title"><?php get_string('courselate','local_mypage') ?></h3>
<font size="6"><b>※</b></font> <font size="6"><span style="background-color: yellow"><b style="color:red"><?php get_string('schoolboard','local_mypage') ?></b><b><?php get_string('nocourselate','local_mypage') ?></b></span><font><br><br>
<h5 class="div_title top"><?php echo get_string('mycourse','local_mypage');?></h5>
<form name="submit_form" action="coursepostponement.submit.php" id="id_submit_form" method="post">
<table class="table">
    <tbody>
        <tr>
            <th><?php echo get_string('selectdelaycourse','local_mypage');?></th>
            <td class="text-left">
                <select title="course" name="applicationid">
                    <option value=""><?php echo get_string('select','local_mypage');?></option>
                    <?php
                    //purge_all_caches();
                    foreach($classes as $class){
                        $classname = $class->coursename.' ('.get_string('yearterm','local_mypage', array('year'=>$class->classyear,'term'=>$class->classnum)).')';
                        echo '<option value="'.$class->id.'">'.$classname.'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('delayreason','local_mypage');?></th>
            <td class="text-left">
                <textarea cols="90" rows="10" name="reason"></textarea>
            </td>
        </tr>
    </tbody>
</table>

<h5 class="div_title top"><?php echo get_string('wantapplycourse','local_mypage');?></h5>
<table class="table">
    <tbody>
        <tr>
            <th><?php echo get_string('selectapplycourse','local_mypage');?></th>
            <td class="text-left">
                <select title="course_select" name="scheduleid">
                    <option value=""><?php echo get_string('select','local_mypage');?></option>
                    <?php
                    foreach($schedules as $schedule){
                        $learningstart = date('Y-m-d',$schedule->learningstart);
                        $learningend = date('Y-m-d',$schedule->learningend);
                        $classname = get_string('yearterm','local_mypage', array('year'=>$schedule->classyear,'term'=>$schedule->classnum)).' ('.$learningstart.'~'.$learningend.')';
                        echo '<option value="'.$schedule->id.'">'.$classname.'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
    </tbody>
</table>


<div class="table_btn">
    <input type="submit" value="<?php echo get_string('apply','local_mypage');?>" class="btn orange big" onclick = "return update_delay_apply();"/>
</div>
</form>
<script>
    /**
     * 수강신청 연기 폼체크 함수
     * @returns {Boolean}
     */
    function update_delay_apply(){
        
        var frm = $('#id_submit_form');
        if($.trim(frm.find('select[name=applicationid] option:selected').val()) == ''){
            alert('<?php echo get_string('alert:selectdelaycourse','local_mypage');?>');
            return false;
        }
        if($.trim(frm.find('textarea[name=reason]').val())  == ''){
            alert('<?php echo get_string('alert:delayreason','local_mypage');?>');
            return false;
        }
        if($.trim(frm.find('select[name=scheduleid] option:selected').val()) == ''){
            alert('<?php echo get_string('alert:selectapplycourse','local_mypage');?>');
            return false;
        }
        
         if(!confirm('<?php echo get_string('confirm:delaycourse','local_mypage');?>')){
            return false;
        }
       
    }
</script>
<?php
echo $OUTPUT->footer();
?>


