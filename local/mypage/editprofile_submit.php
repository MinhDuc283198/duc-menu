<?php
/**
 * 프로필 수정 submit
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

require_login();

$type = optional_param("type", 1, PARAM_INT);
$nickname = optional_param("nickname", '', PARAM_RAW);
$sex = optional_param("sex", '', PARAM_RAW);
$email1 = optional_param("email1", '', PARAM_RAW);
$email2 = optional_param("email2", '', PARAM_RAW);
$zipcode = optional_param("zipcode", '', PARAM_RAW);
$address = optional_param("address", '', PARAM_RAW);
$address_detail = optional_param("address_detail", '', PARAM_RAW);
$allowemail = optional_param("allowemail", 0, PARAM_INT);
$allowsms = optional_param("allowsms", 0, PARAM_INT);
$allowprofile = optional_param("allowprofile", 0, PARAM_INT);
$subject = optional_param("subject", '', PARAM_RAW);
$birthday = optional_param("birthday", null, PARAM_RAW);
$phone1 = optional_param("phone1", 0, PARAM_RAW);
$phone2 = optional_param("phone2", 0, PARAM_RAW);
$phone3 = optional_param("phone3", 0, PARAM_RAW);

$phone = $phone1.$phone2.$phone3;

$email = $email1.'@'.$email2;


$userinfo = $DB->get_record('lmsdata_user', array('userid'=>$USER->id));
$user = $DB->get_record('user', array('id'=>$USER->id));

$userinfo->nickname = $nickname;
$userinfo->sex = $sex;
$userinfo->phone1 = $phone;
$userinfo->email = $email;
$userinfo->zipcode = $zipcode;
$userinfo->address = $address;
$userinfo->address_detail = $address_detail;
$userinfo->allowemail = $allowemail;
$userinfo->allowsms = $allowsms;
$userinfo->allowprofile = $allowprofile;

if($birthday){
    $userinfo->birthday = $birthday;
}

$DB->update_record('lmsdata_user', $userinfo);

if($user->email != $userinfo->email) {
    $DB->set_field('user', 'email', $userinfo->email, array('id' => $user->id));
}

if($user->phone1  != $userinfo->phone1) {
    $DB->set_field('user', 'phone1', $userinfo->phone1, array('id' => $user->id));
    $DB->set_field('user', 'phone2', $userinfo->phone1, array('id' => $user->id));
}

redirect($CFG->wwwroot.'/local/mypage/editprofile.php',get_string('privacymodifiedda','local_mypage'));

