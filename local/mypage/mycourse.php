<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string('mycourse','local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/mycourse.php'));
$type = optional_param("type", 0, PARAM_INT); // 0 = 수강중인 강좌 / 1 = 담당 강좌 
$userinfos = $DB->get_records_sql('SELECT * FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid',array('userid'=>$USER->id));
foreach($userinfos as $userinfo){
    $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
}
if($type==1&&$prof!='pr'){
    redirect('/local/mypage/mypage.php&type=0');
}

if($type==1){
    $prsql = "FROM {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {lmsdata_course_professor} lcp ON lcp.courseid = lc.parentcourseid
            WHERE lcp.teacherid = :userid
            ORDER BY lc.id DESC";
    $prselect = "SELECT
                lc.id,lc.title,lc.courseperiod,  lc.reviewperiod,lc.vi_title,lc.en_title,lco.courseid as lcocourseid,  lco.lecturecnt, lc.courseid as lcid,
                lc.parentcourseid,lcp.teacherid,mc.id as mcid ";
    $prselect_c ="SELECT COUNT(*) ";
    $courses =$DB->get_records_sql($prselect.$prsql,array('userid'=>$USER->id));
} else {
    $courses = local_mypage_get_mycourse('ing');
    $courses_cnt2 = local_mypage_get_mycourse('all',true);//전체 강좌
}
echo $OUTPUT->header();
?>  
<ul class="mk-c-tab tab-event">
    <?php if($prof=='pr'){ ?> <li <?php if($type==1){ echo 'class="on"'; } ?>><a href="/local/mypage/mycourse.php?type=1"><?php echo get_string('coursecharge','local_mypage');?></a></li> <?php } ?>
    <li <?php if($type==0){ echo 'class="on"'; } ?>><a href="/local/mypage/mycourse.php?type=0"><?php echo get_string('mycourse:ing','local_mypage');?></a></li>
    <li><a href="/local/mypage/mycourse_end.php"><?php echo get_string('mycourse:end','local_mypage');?></a></li>
    <li><a href="/local/mypage/mycourse_like.php"><?php echo get_string('mycourse:like','local_mypage');?></a></li>
</ul>
<?php

if($courses){
    ?>
    <ul class="thumb-list style04 course no-bg">
            <?php 
            foreach($courses as $course){
             $context = context_course::instance($course->lcocourseid);
             $path = local_course_get_imgpath($context->id);
             $pro = format_lguplus_get_course_progress($course->lcid);
             $teacher_list_array = array();
             $teacher = '';
             $query = "select lcp.teacherid from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $course->parentcourseid;
             $teacher_array = $DB->get_records_sql($query);
             foreach ($teacher_array as $tak => $ta) {
                $user = local_course_teacher_id($ta->teacherid);
                $teacher_list_array[] = $user->firstname;
                $teacher = implode(',', $teacher_list_array);
             }
             switch (current_language()){
                case 'ko' : 
                    $coursename = $course->title;
                    break;
                case 'en' :
                    $coursename = $course->en_title;
                    break;
                case 'vi' :
                    $coursename = $course->vi_title;
                    break;
            }
            
            if($type==1){
                $getstudentsql = "SELECT count(*) 
                                        FROM m_lmsdata_payment lp
                                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                                        JOIN m_course  c ON c.id = ctx.instanceid 
                                        JOIN m_user u ON lp.userid = u.id
                                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                                        WHERE  c.id = :courseid2 and u.id !=$USER->id";
                 $getstudent = $DB->count_records_sql($getstudentsql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel1' => 50, 'archetype' => 'student'));
                     
                $quesql = 'select count(*)
                from {jinotechboard} j
                join {jinotechboard_contents} jc on j.id = jc.board
                where j.`type` =2  and step = 0 and jc.id not in (select ref from {jinotechboard_contents} where step = 1 and id!=ref ) and jc.course = :course'; 
                $getqna = $DB->count_records_sql($quesql, array('course' => $course->lcid));
                ?>
                  <li>
                    <div class="wp">
                        <div class="img">
                            <a href="/course/view.php?id=<?php echo $course->lcid ;?>"  target="_blank"> <img src="<?php echo $path; ?>" alt="<?php echo $coursename; ?>" /> </a>
                        </div>
                        <div class="txt">
                            <div class="tit"><a href="/course/view.php?id=<?php echo $course->lcid ;?>"  target="_blank"> <?php echo $coursename; ?></a></div>
                             <p><span><?php echo $teacher; ?></span><span><?php echo get_string('total','local_mypage');?> <?php echo $course->lecturecnt; ?><?php echo get_string('course','local_mypage');?></span></p>
                             <p class="num"><em><?php echo get_string('student','local_mypage'); ?></em> <?php echo number_format($getstudent); ?><?php echo get_string('people','local_mypage'); ?></p>
                             <p class="num"><em><?php echo get_string('unanswered','local_mypage'); ?>Q&amp;A</em> <?php echo number_format($getqna); ?><?php echo get_string('coursenum','local_mypage'); ?></p>
                        </div>
                    </div>
                </li>
            <?php
            } else {
                ?>
                    <li>
                        <div class="wp">
                            <div class="img">
                                <a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <img src="<?php echo $path; ?>" alt="<?php echo $coursename; ?>" /></a>
                                <?php
                                $iscom = $DB->get_fieldset_sql('SELECT timecompleted FROM {course_completions} WHERE userid = :userid and course = :courseid',array('userid'=>$USER->id,'courseid'=>$course->lcid));
                                ?>
                                <?php if(empty($iscom) || $iscom[0] <= 0){  } else { echo '<p class="lb"><span>'.get_string('completion','local_mypage').'</span></p>'; } ?>
                                <?php
                                if( (empty($iscom) || $iscom[0] <= 0) || $course->badge == 0 ){} else {
                                    require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                                    $img_path = local_lmsdata_get_imgpath(1, 'bedge', $course->badge)['path'];
                                ?>
                                <span class="ic-badge openPop"><img src="<?php echo $img_path?>">수료증</span>
                                <?php }?>
                            </div>
                            <div class="txt">
                                <div class="tit"><a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <?php echo $coursename; ?></a></div>
                                <p><span><?php echo $teacher; ?> </span><span><?php echo get_string('total','local_mypage');?> <?php echo $course->lecturecnt; ?><?php echo get_string('course','local_mypage');?></span></p>
                                <p class="t-gray"><?php echo get_string('registrationday','local_mypage');?>: <?php echo date('Y.m.d',$course->timemodified); ?>~<?php echo date('Y.m.d',$course->enddate); ?></p>
                            </div>
                            <div class="bar-area">
                                <span><?php echo get_string('progress','local_mypage');?>(<?php echo $pro['course']->courseprogress; ?>%)</span>
                                <p class="bar-event" data-num="<?php echo $pro['course']->courseprogress; ?>%"><span></span></p>
                            </div>
                        </div>
                    </li>
                    
                <?php
            }
             ?>
            <?php }?>
    </ul>    
    <?php
} else {
    ?>
    <div class="no-data style02">
    <div><?php if($type==1&&$prof=='pr'){ echo get_string('nocourse2','local_mypage'); } else { echo get_string('nocourse','local_mypage'); }?></div>
    <?php if($type==1&&$prof=='pr'){ } else { ?><a href="/local/course/main.php" class="btns point arrow"><?php echo get_string('viewallcourse','local_mypage');?></a><?php } ?>
</div><?php
}
?>
<?php
echo $OUTPUT->footer();
?>


