<?php
/**
 * course쪽 콘텐츠 팝업
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/mod/okmedia/lib.php');
require_once($CFG->dirroot . '/mod/okmedia/viewer/lib.php');

$id = required_param('id', PARAM_INT);
$ref = optional_param('mod', '', PARAM_RAW);

$sql = 'select okm.id, lcf.filepath, lcf.filename, okm.name, ot.playtime, ot.progress, 
        con.id as con_id, con.con_name, con.con_type, con.con_des,
        con.update_dt, con.data_dir, con.embed_type, con.embed_code
        from {course_modules} cm 
        join {okmedia} okm on cm.instance = okm.id 
        JOIN {lcms_contents} con ON con.id =  okm.contents
        left join {lcms_contents_file} lcf on con.id = lcf.con_seq 
        left join {okmedia_track} ot on okm.id = ot.okmediaid AND ot.userid =:userid 
        where cm.id = :id ';
$param = array('id' => $id, 'userid' => $USER->id);

$media = $DB->get_record_sql($sql, $param);
//print_object($media -> con_id);
//print_object($media);
list ($course, $cm) = get_course_and_cm_from_cmid($id, 'okmedia');
if (!$okmedia = $DB->get_record('okmedia', array('id' => $cm->instance))) {
    print_error('course module is incorrect');
}

require_course_login($course, true, $cm);

$conid = $okmedia->contents;

//LCMS 동영상 데이터 가져오기
if (!$content = $DB->get_record('lcms_contents', array('id' => $conid))) {
    print_error(get_string('nocontents', 'okmedia'));
    die();
}

$file = $DB->get_record('lcms_contents_file', array('con_seq' => $content->id));
$filename = $file->filename;
$viewcount = $okmedia->viewcount + 1;
$DB->set_field('okmedia', 'viewcount', $viewcount, array('id' => $okmedia->id));
$okmediaprogress = $DB->get_field('okmedia', 'completionprogress', array('id' => $okmedia->id));
$okmediatimestart = $okmedia->timestart;

//진도율 가져오기
$lastview = '0000-00-00 00:00';
$playtime = '00:00';
$progress = '0%';
$studying = get_string('beforestudy', 'okmedia');
if ($track = $DB->get_record('okmedia_track', array('okmediaid' => $okmedia->id, 'userid' => $USER->id))) {
    $lastview = date('Y-m-d H:i', $track->timeview);
    $ptm = okmedia_time_from_seconds($track->playtime);
    $playtime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
    $progress = $track->progress . '%';
    $studying = ($track->progress < 100) ? get_string('studying', 'okmedia') : get_string('complete', 'okmedia');
}

// okmedia 마감시간 
$timeend = $okmedia->timeend;

// okmedia 마감후 보기 기능 0 숨기기, 1 보기(진도체크 불가), 2 보기 (진도체크가능)
$islock = $okmedia->islock;

$fr = get_string('loading','local_mypage');

$broswer = browser_check();
$userrolecheck = $DB->get_field('lmsdata_user', usergroup, array('userid' => $USER->id));
//$play_url = $CFG->vodurl . '/uploads/' . $media->filepath . '/' . str_replace('.mp4', '_hd.mp4', $media->filename);
$play_url = $CFG->vodurl . '/_definst_/' . $media->filepath . '/' . str_replace('.mp4', '_hd.mp4', $media->filename);
?>

<div class="popwrap">
    <div class="pop_title">
        <?php echo $media->name; ?>
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_pop_close.png" alt="" onclick="onclosepopup()" />
    </div>

    <div class="pop_content">
        <div class="videoinfo">
            <?php
            if ($track->progress < 100) {
                ?>
                <p class="circletext">
                    <span><?php echo $studying; ?></span>
                </p>
                <?php
            } else {
                ?>
                <!--학습 완료--> 
                <p class="circletext inverse">
                    <span><?php echo $studying; ?></span>
                </p>    
                <!--학습 완료--> 
            <?php } ?>
            <ul class="info">
                <li class="m-col3">
                    <p class="text-bold"><?php get_string('lastedudate','local_mypage') ?></p>
                    <p><?php echo $lastview ?></p>
                </li>
                <li class="m-col3">
                    <p class="text-bold"><?php get_string('edutime','local_mypage') ?></p>
                    <p>
                        <?php
                        $timecheck = okmedia_time_from_seconds($media->playtime);
                        echo ($timecheck->h . ' : ' . $timecheck->m . ' : ' . $timecheck->s);
                        ?>
                    </p>
                </li>
                <li class="m-col3">
                    <p class="text-bold">
                        <?php get_string('progressrate','local_mypage') ?>
                    </p>
                    <p>
                        <?php
                        if ($media->progress < 100 && $media->progress > 0) {
                            echo $progress;
                        } else if ($media->progress >= 100) {
                            echo '100 %';
                        } else if ($media->progress <= 0) {
                            echo '0 %';
                        }
                        ?>
                    </p>
                </li>
            </ul>
        </div>
        <div class="videoarea">
            <?php
            if ($media->con_type == 'video') {
                echo '<input type="hidden" value="' . $media->id . '" id = "okmediaid">';
                echo '<div id="hlsjsvod" class="is-closeable video_area"></div>';
                ?>
                <script>
                    var device = okmedia_device_check();

                    var options = new Array();
                    // 플레이어 넓이
    //                options["width"] = 900;
                    // 플레이어 높이
    //                options["height"] = 500;
                    // 진도체크 여부
                    options["progresschk"] = true;
                    // 진도값 반환 url
                    options["returnurl"] = '<?php echo $CFG->wwwroot . "/local/mypage/pop_player_ajax.php" ?>';
                    // 동영상 시작 지점
                    options["startposition"] = popcontents_get_playtime('<?php echo $media->id; ?>', '<?php echo $CFG->wwwroot . "/local/mypage/pop_player_ajax.php" ?>');
                    // 자동재생 여부
                    options["autoplay"] = true;
                    // 동영상 비활성화 여부
                    options["disabled"] = false;
                    // 스피드 바 표시여부
                    options["speedbar"] = true;
                    // 전체화면기능 사용여부
                    options["fullscreen"] = true;
                    // 키보드 사용여부
                    options["keyboard"] = true;
                    // 건너뛰기기능 사용여부
                    options["seeking"] = true;
//                    // 비디오 타입 정의(mp4, hls, webm)
//                    options["videotype"] = 'mp4';
                    // 비디오 타입 정의(mp4, hls, webm)
                    options["videotype"] = 'hls';
                    // 키
                    options["key"] = '<?php echo get_config('local_repository','flowplayer_key')?>';
                    $(document).ready(function () {
                        Load_Player('hlsjsvod', '<?php echo $play_url; ?>', options);
                        //Load_Player('video-player-wp', '{{url}}', options);
                    });
                </script>
                <?php
            } else if ($media->con_type == 'embed') {
                if ($media->embed_type == 'youtube') {
                   $viewer_url = '/local/repository/viewer/video_player_embed_course.php?id=' . $media->con_id.'&okmediaid='.$media->id;?>
            <iframe id="videoiframe" src="<?php echo $viewer_url; ?>" frameborder="0" allowfullscreen></iframe>
             <?php   }
            }
            ?>
        </div>
    </div>
</div>