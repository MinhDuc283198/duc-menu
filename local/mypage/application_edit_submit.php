<?php
/**
 * 수강신청 정보 수정 submit 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$id = optional_param('id', 0, PARAM_INT);
$usertypecode = optional_param('usertypecode', 0, PARAM_INT);

if($usertypecode == 20) {
    $qualification = optional_param('qualification1', 0, PARAM_RAW);
}else if($usertypecode == 10) {
    $qualification = optional_param('qualification2', 0, PARAM_RAW);
}
$school = optional_param('school', '', PARAM_RAW);
$neis = optional_param_array('neis', array(), PARAM_RAW);
$no = optional_param_array('no', array(), PARAM_RAW);
$firstnumber = optional_param('firstnumber', '', PARAM_RAW);
$number = optional_param_array('number', array(), PARAM_RAW);
$year = optional_param('year', '', PARAM_RAW);
$month = optional_param('month', '', PARAM_RAW);
$day = optional_param('day', '', PARAM_RAW);
$email = optional_param('email', '', PARAM_RAW);

$application = $DB->get_record('lmsdata_course_applications', array('id'=>$id, 'userid'=>$USER->id));

if(!empty($application)){
    $neiscode = implode('', $neis);
    $locationnumber = implode('-', $no);
    $phone = $firstnumber;
    $phone .= implode('', $number);
    $birthday = $year.$month.$day;
    $email = $email;
  
    $application->usertypecode = $usertypecode;
    $application->school = $school;
    $application->neiscode = $neiscode;
    $application->locationnumber = $locationnumber;
    $application->phone = $phone;
    $application->birthday = $birthday;
    $application->email = $email;
    $application->qualification = $qualification;
    
    if($DB->update_record('lmsdata_course_applications', $application)){
        echo '<script type="text/javascript">alert("'.get_string('enrolmodified','local_mypage');'"); location.href="'.$CFG->wwwroot.'/local/mypage/mycourse.php";</script>';
    }
}
