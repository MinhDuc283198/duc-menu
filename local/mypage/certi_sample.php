<?php

require_once $CFG->dirroot.'/lib/tcpdf/include/tcpdf_fonts.php';

$UHC_widths = array(' '=>333,'!'=>416,'"'=>416,'#'=>833,'$'=>625,'%'=>916,'&'=>833,'\''=>250,
	'('=>500,')'=>500,'*'=>500,'+'=>833,','=>291,'-'=>833,'.'=>291,'/'=>375,'0'=>625,'1'=>625,
	'2'=>625,'3'=>625,'4'=>625,'5'=>625,'6'=>625,'7'=>625,'8'=>625,'9'=>625,':'=>333,';'=>333,
	'<'=>833,'='=>833,'>'=>916,'?'=>500,'@'=>1000,'A'=>791,'B'=>708,'C'=>708,'D'=>750,'E'=>708,
	'F'=>666,'G'=>750,'H'=>791,'I'=>375,'J'=>500,'K'=>791,'L'=>666,'M'=>916,'N'=>791,'O'=>750,
	'P'=>666,'Q'=>750,'R'=>708,'S'=>666,'T'=>791,'U'=>791,'V'=>750,'W'=>1000,'X'=>708,'Y'=>708,
	'Z'=>666,'['=>500,'\\'=>375,']'=>500,'^'=>500,'_'=>500,'`'=>333,'a'=>541,'b'=>583,'c'=>541,
	'd'=>583,'e'=>583,'f'=>375,'g'=>583,'h'=>583,'i'=>291,'j'=>333,'k'=>583,'l'=>291,'m'=>875,
	'n'=>583,'o'=>583,'p'=>583,'q'=>583,'r'=>458,'s'=>541,'t'=>375,'u'=>583,'v'=>583,'w'=>833,
	'x'=>625,'y'=>625,'z'=>500,'{'=>583,'|'=>583,'}'=>583,'~'=>750);
class completionPDF{

    protected $_srcdir;
    protected $pdf;

    public function __construct($srcdir, $page = 1) {
        $this->_srcdir = $srcdir;

        // initiate PDF
        if (is_null($this->pdf)) {
            $this->pdf = new FPDI();
        }

        $this->pdf->setPageUnit('mm'); // milimeter
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
        $this->pdf->SetAutoPageBreak(true, -5.2);
        $this->pdf->setFontSubsetting(true);
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);
        
    }

    function Write($vars) {
        // add a page
        $this->pdf->AddPage();
        
        
        $newfonts = new TCPDF_FONTS;
        //$fontname = $newfonts->addTTFfont('lib/tcpdf/fonts/goongseo.TTF');
          
        $fontname = TCPDF_FONTS::addTTFfont('../../lib/tcpdf/fonts/gung.TTF');
        $bareunfont = TCPDF_FONTS::addTTFfont('../../lib/tcpdf/fonts/BareunDotumPro1.ttf');
        /* Korean Font
                cid0kr: gothic
                dotum: myeongjo
        */
         
        $this->pdf->Image($vars->background,3.3,7.3,641.5,890,'', '', '', false, 300, '', false, false, 0);
        $this->pdf->Image($vars->cell,28.7,78.2,152,72,'', '', '', false, 300, '', false, false, 0);
        $this->pdf->setPageMark();

        $table = '<table border="1" cellspacing="0">
    <tr>
        <th align="center" height="10" width="20%">'.get_string('cer:number','local_mypage').'</th><td colspan="3" width="80%">'.$vars->documentid.'</td>
    </tr>
    
    <tr rowspan="2" style="vertical-align:middle">
        <th align="center" width="20%">'.get_string('cer:name','local_mypage').'</th><td width="30%">'.$vars->name.'</td><td width="20%">'.get_string('cer:neis','local_mypage').'<br>('.get_string('cer:birth','local_mypage').')</td><td width="30%">'.$vars->neisbirth.'</td>
    </tr>
    
    <tr>
        <td align="center" width="20%">'.get_string('cer:classtype','local_mypage').'</td><td width="30%">'.$vars->coursetype.'</td><td width="20%">'.get_string('cer:certime','local_mypage').'</td><td width="30%">'.$vars->coursetime.'</td>
    </tr>
    
    <tr>
        <th align="center" width="20%">'.get_string('cer:coursename','local_mypage').'</th><td colspan="3" width="80%">'.$vars->coursename.'</td>
    </tr>
    
    <tr>
        <th align="center" width="20%">'.get_string('cer:learningtime','local_mypage').'</th><td width="80%" colspan="3">'.$vars->learntime.'</td>
    </tr>
    
    <tr>
        <th align="center" width="20%">'.get_string('cer:locnumber','local_mypage').'</th><td width="80%" colspan="3">'.$vars->location.'</td>
    </tr>
</table>';
        // remove default footer

// set image scale factor
$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $this->pdf->SetXY(36, 84);
 //       $this->pdf->writeHTMLCell(140, 200, 36, 84, $table, '');
  $this->pdf->SetFont($bareunfont, '', 12);

//        $w=array(30,40,30,40);
//        $this->pdf->Cell($w[0],5,'한글 왜 안들어가요','LTR',0,'L',0);   // empty cell with left,top, and right borders
//        $this->pdf->Cell($w[1],5,'2',1,0,'L',0);
//        $this->pdf->Cell($w[2],5,3,1,0,'L',0);
//        $this->pdf->Cell($w[3],5,4,'LR',1,'C',0);  // cell with left and right borders
//        $this->pdf->Cell($w[0],5,5,1,0,'L',0);
//        $this->pdf->Cell($w[0],5,6,1,0,'L',0);
//        $this->pdf->Cell($w[0],5,7,'LBR',1,'L',0);   // empty cell with left,bottom, and right borders
//        $this->pdf->Cell($w[0],5,8,1,0,'L',0);
//        $this->pdf->Cell($w[0],5,9,1,0,'L',0);
//        $this->pdf->Cell($w[0],5,10,1,0,'L',0);
//        $this->pdf->Cell($w[0],5,11,1,0,'L',0);
//        $this->pdf->Cell($w[0],5,12,1,0,'L',0);
        

        $utf8text = $vars->documentid;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(62, 81.5);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->name;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(62, 97);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->neisbirth;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(133, 97);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->coursetype;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(62, 113);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->coursetime;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(133, 113);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->coursename;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(62, 122.5);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->learntime;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(62, 132.3);
        $this->pdf->Write(5, $utf8text);
        
        $utf8text = $vars->location;
        $this->pdf->SetFont($bareunfont, 'B', 12);
        $this->pdf->SetXY(62, 142);
        $this->pdf->Write(5, $utf8text);
        
        /*
        $utf8text = '생년월일 : '.$vars->birthday;
        $this->pdf->SetXY(124, 96);
        $this->pdf->Write(5, $utf8text);
        */
        $utf8text = $vars->description;
        $this->pdf->SetFont($fontname, 'B', 18);        
    //    $this->pdf->SetFont($fontname, 'B', 18);
        $this->pdf->SetXY(51, 195);
        $this->pdf->Write(5, $utf8text);
        //$this->pdf->MultiCell(165, 0, $utf8text, 0, 'L');

        //$this->pdf->SetFont('bareunbatangm', '', 12);


        $utf8text = $vars->issuedate;
    //    $this->pdf->SetFont('hysmyeongjostdmedium', 'B', 18);
        $this->pdf->SetXY(76, 216);
        $this->pdf->Write(5, $utf8text);
        //$this->pdf->MultiCell(170, 0, $utf8text, 0, 'C');

   

        $utf8text = $vars->author;
    //    $this->pdf->SetFont('hysmyeongjostdmedium', 'B', 23);
        $this->pdf->SetXY(0, 249);
        $this->pdf->MultiCell(210, 0, $utf8text, 0, 'C');

        
        
        
    }
    
    function Output($filename, $mod='D')
    {
        $this->pdf->Output($filename, $mod);
    }
    
    

}
/* Destination where to send the document. It can take one of the following values:
	I: send the file inline to the browser. The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
	D: send to the browser and force a file download with the name given by name.
	F: save to a local file with the name given by name (may include a path).
	S: return the document as a string. name is ignored.
*/

