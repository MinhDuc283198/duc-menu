<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$contentid = required_param('contentid', PARAM_INT);
$type = required_param('type', PARAM_INT);

$returnli = '';
$countcomments = $DB->count_records('lcms_contents_comments', array('contentid' => $contentid));

if ($type == 1) {
    if ($countcomments) {
        $getcommentsql = 'select lcc.*, u.username, u.firstname, u.lastname '
                . 'from {lcms_contents_comments} lcc '
                . 'join {user} u on u.id = lcc.userid '
                . 'where lcc.contentid = :contentid';
        $getcomments = $DB->get_records_sql($getcommentsql, array('contentid' => $contentid));
        foreach ($getcomments as $getcomment) {
            if ($getcomment->userid == $USER->id) {
                $returnli .= '<li id="' . $getcomment->id . '">' . $getcomment->con_des
                        . '<p class="r_info">'
                        . '<strong>' . $getcomment->firstname . $getcomment->lastname . '</strong>'
                        . '<span>' . date('Y-m-d', $getcomment->timecreated) . '</span>'
                        . '<a href="javascript:deletecomment(' . $getcomment->id . ', ' . $getcomment->userid . ');" class="btn del">'.get_string('delete','local_mypage');'</a>'
                        . '<a href="javascript:editcomment(' . $getcomment->id . ', ' . $getcomment->userid . ');" class="btn mod" id="updatebtn">'.get_string('modified','local_mypage');'</a>'
                        . '</p>'
                        . '</li>';
            } else {
                $returnli .= '<li id="' . $getcomment->id . '">' . $getcomment->con_des
                        . '<p class="r_info">'
                        . '<strong>' . $getcomment->firstname . $getcomment->lastname . '</strong>'
                        . '<span>' . date('Y-m-d', $getcomment->timecreated) . '</span>'
//                        . '<a href="#none" class="btn del">삭제</a>'
                        . '</p>'
                        . '</li>';
            }
        }
    } else {
        $returnli .= '<li>'.get_string('nocomment','local_mypage');'</li>';
    }
    echo $returnli;
} else if ($type == 2) { // 댓글 수
    $returnli = ''.geT_string('comment','local_mypage');' (' . $countcomments . ')';
    echo $returnli;
} else if ($type == 3) { // 별점 평균
    $sqlaverage = 'select (SUM(stars) / COUNT(userid)) AS averagestar from {lcms_contents_evaluation} where contentid = ' . $contentid;
    $getaverage = $DB->get_record_sql($sqlaverage);
    $returnaverage = array('roundaverage' => round($getaverage->averagestar), 'average' => $getaverage->averagestar);

    @header('Content-type: application/json; charset=utf-8');
    echo json_encode($returnaverage);
} else if ($type == 4) { // 댓글수정 나오도록
    $commentid = required_param('commentid', PARAM_INT);
    $replysql = 'select * from {lcms_contents_comments} where contentid = :contentsid and userid = :userid and id = :id ';
    $reply = $DB->get_record_sql($replysql, array('contentsid' => $contentid, 'userid' => $USER->id, 'id' => $commentid));
    if ($reply) {
        $editform = '<form class="reply_form" id="reply_update_forms">
                                <input type="hidden" name="contentid" value=" ' . $reply->contentid . ' " />
                                <input type="hidden" name="commentid" value=" ' . $reply->id . ' " />
                                <input type="hidden" name="mod" value="update" />
                                <textarea title="'.get_string('commentinput','local_mypage');'" id="textupdatecomments" name="comments" placeholder="'.get_string('plscomment','local_mypage');'">'.$reply->con_des.'</textarea>
                                <input type="button" class="btn reply" value="'.get_string('gomodified','local_mypage');'" onclick="updatecomment(' . $reply->id . ', ' . $reply->userid . ')" />
                            </form>';
    }
    echo $editform;
}   