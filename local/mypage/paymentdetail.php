<?php
/**
 * 결제 상세정보 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->js('/chamktu/js/lib/jquery.ui.datepicker_lang.js');
$PAGE->set_url(new moodle_url('/local/mypage/paymentdetail.php'));

$id = optional_param('id', 0, PARAM_INT);
$sdate = optional_param('sdate', '', PARAM_RAW);
$edate = optional_param('edate', '', PARAM_RAW);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("processguide",'local_mypage'));
$PAGE->navbar->add(get_string("jobtraining",'local_mypage'));

$sql = 'SELECT lca.*, lco.coursename, lco.memberdiscount, lco.groupdiscount
        FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.id = lca.courseid
        JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
        WHERE lca.userid = :userid AND lca.id = :id';

$payinfo = $DB->get_record_sql($sql, array('userid'=>$USER->id, 'id'=>$id));

if($payinfo->paymentmeans == 'card'){
    $params = array('code'=>$payinfo->cardcode, 'type'=>'card');
} else {
    if($payinfo->paymentmeans == 'unbank'){
        $payinfo->bankcode = '04';
    }
    $params = array('code'=>$payinfo->bankcode, 'type'=>'bank');
}
$payorg = $DB->get_field('inicode','name', $params);

echo $OUTPUT->header();
?>
<h3 class="page_title"><?php get_string('buylist','local_mypage') ?></h3>
<table class="table">
    <tbody>
        <tr>
            <th><?php get_string('paymentnumber','local_mypage') ?></th>
            <td class="text-left">
                <?php echo $payinfo->ordernumber;?>
                <?php if($payinfo->paymentstatus == 'complete'){?>
                <input type="button" value="<?php get_string("printreceipt",'local_mypage') ?>" class="btn brd r_content" onclick="window.open('receipt.php?id=<?php echo $payinfo->id;?>&userid=<?php echo $USER->id;?>', '','width=385, height=485')" />
                <?php }?>
            </td>
        </tr>
        <tr>
            <th><?php get_string('cer:coursename','local_mypage') ?></th>
            <td class="text-left"><?php echo $payinfo->coursename;?></td>
        </tr>
        <tr>
            <th><?php get_string('amountofpayment','local_mypage') ?></th>
            <td class="text-left">
                <span class="orange"><?php echo $payinfo->price;?><?php get_string('won','local_mypage') ?></span>
                <?php 
                if($payinfo->discounttype == 'group'){
                    echo get_string("groupdiscount",'local_mypage').$payinfo->groupdiscount.get_string("pstapply",'local_mypage');
                } else if($payinfo->discounttype == 'member'){
                    echo get_string('memberdiscount','local_mypage').$payinfo->memberdiscount.get_string("pstapply",'local_mypage');
                }
                ?>
            </td>
        </tr>
        <tr>
            <th><?php get_string('howpayment','local_mypage') ?></th>
            <td class="text-left">
                <?php 
                if($payinfo->paymentmeans == 'card'){
                    echo get_string('card','local_mypage');
                } else if($payinfo->paymentmeans == 'bank'){
                    echo get_string('nowtimebanktransfer','local_mypage');
                } else if($payinfo->paymentmeans == 'vbank'){
                    echo get_string('virtualaccount','local_mypage');
                } else if($payinfo->paymentmeans == 'unbank'){
                    echo get_string('notong','local_mypage');
                }
                ?>
            </td>
        </tr>
        <tr>
            <?php if($payinfo->paymentmeans == 'card'){ ?>
            <th><?php get_string('cardname','local_mypage') ?></th>
            <td class="text-left">
                <?php echo $payorg;?><?php get_string('card','local_mypage') ?>
            </td>
            <?php } else { ?>
            <th><?php get_string('bankname','local_mypage') ?></th>
            <td class="text-left">
                <?php echo $payorg;?>
            </td>
            <?php } ?>
        </tr>
        <?php if($payinfo->paymentmeans == 'vbank'){ ?>
        <tr>
            <th><?php get_string('payaccount','local_mypage') ?></th>
            <td class="text-left"><?php echo $payinfo->account;?></td>
        </tr>
        <?php }?>
        <?php if($payinfo->paymentmeans == 'unbank'){ ?>
        <tr>
            <th><?php get_string('payaccount','local_mypage') ?></th>
            <td class="text-left">011201-04-162798</td>
        </tr>
        <?php }?>
        <tr>
            <th><?php get_string('applydate','local_mypage') ?></th>
            <td class="text-left"><?php echo (!empty($payinfo->timecreated))?date('Y-m-d H:i:s',$payinfo->timecreated):'-';?></td>
        </tr>
        <tr>
            <th><?php get_string('paymentcondition','local_mypage') ?></th>
            <td class="text-left"><?php if($payinfo->paymentstatus == 'cancel') echo get_string('chuiso','local_mypage'); else if($payinfo->paymentstatus == 'complete') echo get_string('paymentok','local_mypage'); else echo get_string('paymentwait','local_mypage')?></td>
        </tr>
        <tr>
            <th><?php get_string('okaydate','local_mypage') ?></th>
            <td class="text-left"><?php echo (!empty($payinfo->paymentdate))?date('Y-m-d H:i:s',$payinfo->paymentdate):'-';?></td>
        </tr>
    </tbody>
</table>

<div class="table_btn">
    <?php //if($payinfo->paymentmeans != 'card' && $payinfo->price > 0 && $payinfo->paymentstatus == 'complete' && $payinfo->status == 'apply' && empty($payinfo->cshrresult)){?>
    <!--<input type="button" value="현금영수증발급신청" class="btn orange big" onclick="cashbill('<?php //echo $payinfo->id;?>')" />-->
    <?php //}?>
    <input type="button" value="<?php get_string('golist','local_mypage') ?>" onclick="location.href='<?php echo $CFG->wwwroot;?>/local/mypage/payment.php?sdate=<?php echo $sdate;?>&edate=<?php echo $edate;?>'" class="btn gray big" />
</div>

<script>
    /**
     * 현금영수증 발급 팝업 함수
     * @param {type} id
     * @returns {undefined}
     */
   function cashbill(id) {
        call_layerPop("/local/mypage/cashbill.php", "800px", "auto", {id:id});
    }
</script>

<?php
echo $OUTPUT->footer();
?>


