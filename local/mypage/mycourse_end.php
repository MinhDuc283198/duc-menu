<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string('mycourse','local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/mycourse_end.php'));
echo $OUTPUT->header();
$userinfos = $DB->get_records_sql('SELECT * FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid',array('userid'=>$USER->id));

foreach($userinfos as $userinfo){

    $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
}
?>  
<ul class="mk-c-tab tab-event">
   <?php if($prof=='pr'){ ?> <li><a href="/local/mypage/mycourse.php?type=1"><?php echo get_string('coursecharge','local_mypage'); ?></a></li> <?php } ?>
    <li><a href="/local/mypage/mycourse.php?type==0"><?php echo get_string('mycourse:ing','local_mypage'); ?></a></li>
    <li class="on"><a href="/local/mypage/mycourse_end.php"><?php echo get_string('mycourse:end','local_mypage'); ?></a></li>
    <li><a href="/local/mypage/mycourse_like.php"><?php echo get_string('mycourse:like','local_mypage'); ?></a></li>
</ul>
<div class="btn-area text-right">
    <!--<input type="button" value="배지 전체보기" class="btns openPop02" />-->
</div>
<?php
$courses = local_mypage_get_mycourse('end');
$today = time();
if($courses){ ?>
<ul class="thumb-list style04 course">
     <?php 
        foreach($courses as $course){
         $context = context_course::instance($course->lcocourseid);
         $path = local_course_get_imgpath($context->id);
         $pro = format_lguplus_get_course_progress($course->lcid);
         $iscom = $DB->get_fieldset_sql('SELECT timecompleted FROM {course_completions} WHERE userid = :userid and course = :courseid',array('userid'=>$USER->id,'courseid'=>$course->lcid));
             switch (current_language()){
                case 'ko' :
                    $coursename = $course->title;
                    break;
                case 'en' :
                    $coursename = $course->en_title;
                    break;
                case 'vi' :
                    $coursename = $course->vi_title;
                    break;
            }
            $teacher_list_array = array();
             $teacher = '';
             $query = "select lcp.teacherid from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $course->parentcourseid;
             $teacher_array = $DB->get_records_sql($query);
             foreach ($teacher_array as $tak => $ta) {
                $user = local_course_teacher_id($ta->teacherid);
                $teacher_list_array[] = $user->firstname;
                $teacher = implode(',', $teacher_list_array);
             }
         ?>
    <li>
        <div class="wp">
            <div class="img">
                <img src="<?php echo $path; ?>" alt="<?php echo $coursename; ?>" />
                <p class="lb"><span><?php if(empty($iscom) || $iscom[0] <= 0){ echo get_string('notcompletion','local_mypage'); } else { echo get_string('completion','local_mypage'); } ?></span>
                    <?php if($today<$course->redate){ echo get_string('review2','local_mypage').' : ~ '. date('Y.m.d',$course->redate); } ?> 
                </p>
                <?php
                if( $iscom[0] > 0 && $course->badge != 0 ){
                    require_once($CFG->dirroot . '/local/lmsdata/lib.php');
                    $img_path = local_lmsdata_get_imgpath(1, 'bedge', $course->badge)['path'];
                ?>
                <span class="ic-badge openPop" onclick="location.href='templates/certificates.mustache'"><img src="/theme/oklassedu/pix/images/badge_yellow04.png" alt="" /><?php echo get_string('certificates','local_mypage') ?></span>
                <?php }else {}?>
                                
                <?php
                $badgequery = "
                            select lbu.*, lb.id lbid, lb.ordered, c.id cid    
                            from m_lmsdata_badge_user lbu
                            JOIN m_lmsdata_badge lb ON lbu.badgeid=lb.id
                            JOIN m_course c ON lbu.course = c.id
                            JOIN m_lmsdata_class lc ON c.id = lc.courseid AND lc.isused = 0  
                            JOIN m_lmsdata_course lco ON lc.parentcourseid = lco.id AND lco.isused = 0  
                            JOIN m_course  mc2 ON lco.courseid = mc2.id
                            JOIN m_course_categories  ca ON ca.id = mc2.category AND ca.visible = 1
                            left JOIN m_course_categories  ca2 ON ca.parent = ca2.id AND ca2.visible = 1
                            WHERE lbu.status = 1 AND c.id = :cid AND lbu.userid = :userid";
                $badgecompl = $DB->get_record_sql($badgequery, array('cid'=>$course->lcid, 'userid'=>$USER->id));
                ?>
            </div>
            <div class="txt">
                <div class="tit"><?php 
                if($today>$course->redate){ ?> 
                    <a href="/local/course/detail.php?id=<?php echo $course->id ;?>" target="_blank"> <?php echo $coursename; ?></a>
                <?php } else{ ?> <a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <?php echo $coursename; ?></a> <?php }
                ?></div>
                <p><span><?php echo $teacher; ?> <!--<?php echo get_string('teacher','local_mypage'); ?> --></span><span><?php echo get_string('total','local_mypage'); ?> <?php echo $course->lecturecnt; ?><?php echo get_string('course','local_mypage'); ?></span></p>
               <p class="t-gray"><?php get_string('registrationday','local_mypage'); ?> <?php echo date('Y.m.d',$course->timemodified); ?>~<?php echo date('Y.m.d',$course->enddate); ?></p>
            </div>
            <div class="bar-area">
                <span><?php echo get_string('progress','local_mypage'); ?>(<?php echo $pro['course']->courseprogress; ?>%)</span>
                <p class="bar-event" data-num="<?php echo $pro['course']->courseprogress; ?>%"><span></span></p>
            </div>
        </div>
    </li> 
        <?php
        } ?> 
</ul> <?php
        } else {
            ?>
<div class="no-data style02">
     <div><?php echo get_string('nocomcourse','local_mypage'); ?></div> 
    <a href="/local/course/main.php" class="btns point arrow"><?php echo get_string('viewallcourse','local_mypage'); ?></a>
</div> <?php
        }?>
<script type="text/javascript">
    $(function() {
        //배지 전체보기 팝업
        $(".openPop02").click(function() {
            utils.popup.call_layerpop("/local/course/pop_allbadge.php", {
                "width": "800px",
                "height": "auto",
                "bg": true,
                "callbackFn": function() {}
            }, {
                "param": "11"
            });
        });
        //배지 안내 팝업
        $(document).on("click", ".bdg-list>li", function () {
            utils.popup.call_layerpop("/local/course/pop_badge_1.php", {
                "width": "400px",
                "height": "auto",
                "bg": true,
                "callbackFn": function () {}
            }, {
                "id": $(this).data('id')
            });
        });
    });
    function completepopup(courseid) {
        var h = 1130;
        if(window.screen.availHeight < 1130){
            var h = window.screen.availHeight - 100;
        }
        utils.popup.call_windowpop("./coursecompletepopup.php?id="+courseid, "815px", h);
    }
</script>
<?php
echo $OUTPUT->footer();
?>


