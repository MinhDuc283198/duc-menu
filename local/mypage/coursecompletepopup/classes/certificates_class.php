<?php

/* 
 * @title 수료증 객체
 * @author 김성룡
 * @date 2020.04.08 
 * @description 수료증 객체
 */

class certificates {
    public $courseid;
    public $firstname; // 수료자
    public $userid; // 수료자 아이디
    public $email; // 수료자 이메일
    public $title; // 수료 강좌명
    public $teacher; // 강사
    public $playtime; // 학습시간
    public $auth;
    public $parentid;
    public $userinfotxt = '';
    public $certificatenumber = '';   //이수번호
    public $certificatedate = ''; //이수증 발행일

    public function __construct($courseid = null) {
        $this->courseid = $courseid;
    }
    /**
     * @title 수료증 정보 가져옴
     * @global type $DB
     * @return $this
    */
    public function get_info() {
        global $DB,$USER;
        
        $course_sql = $DB->get_record_sql('
            select auth,firstname,username,email from {user} u 
            join {lmsdata_user} lu on u.id = lu.userid
            where u.id = :id',array('id'=>$USER->id));
        
        $this->firstname = $course_sql->firstname;
        $this->userid = $course_sql->username;
        $this->email = $course_sql->email;
        $this->auth = $course_sql->auth;
        
            if($this->auth = 'google' || $this->auth = 'manual') {
                $this->userinfotxt = $this->userid.'/'.$this->email;
            }
        return $this;
    }
    
    public function user() {
        global $DB;
        
        $user_sql = $DB->get_record_sql('
            select lc.*, cc.parentcourseid, cc.title  
            from {course} c 
            join {lmsdata_class} cc on cc.courseid = c.id 
            join {lmsdata_course}  lc on cc.parentcourseid  = lc.id
            where c.id = :courseid',array('courseid'=> $this->courseid));
        
        $this->title = $user_sql->title;
        $this->parentid = $user_sql->parentcourseid;
        
        return $this;
    }
    public function completetime() {
        global $DB,$USER;
        
        $completetime_sql = $DB->get_record_sql('
            SELECT userid,sum(playtime) as sumplaytime
            FROM {okmedia_track}
            WHERE userid = :id and okmediaid
            IN(select id from {okmedia} where course = :courseid)
            GROUP BY userid',array('id'=>$USER->id,'courseid'=> $this->courseid));
        return $completetime_sql->sumplaytime;
    }
    
    public function teacher() {
        global $DB;
        
        $query = "
            SELECT lcp.teacherid
            FROM {user} u
            JOIN {lmsdata_user} lu ON u.id=lu.userid
            JOIN {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid
            WHERE lu.usergroup='pr'and lcp.courseid=:courseid";
        $teacher_array = $DB->get_records_sql($query,array('courseid'=>$this->parentid));
        foreach ($teacher_array as $tak => $ta) {
                    $user = local_course_teacher_id($ta->teacherid);
                    $teacher_list_array[] = $this->teacher = $user->firstname;
                    
        }
        return implode(',', $teacher_list_array);        
    }

    public function get_certificate() {
        global $DB,$USER;

        $certificatenumberrow = new stdClass();

        $query_sql = $DB->get_record_sql('
            SELECT id
            FROM {lmsdata_certificate}
            where userid = :userid and courseid = :courseid',array('userid'=>$USER->id,'courseid'=>$this->courseid));

        if($query_sql->id){
            $certificatenumberrow->id = $query_sql->id;            
            $certificatenumberrow->userid = $USER->id;
            $certificatenumberrow->courseid = $this->courseid;
            $certificatenumberrow->timemodified = time();                                                          
            $DB->update_record('lmsdata_certificate',$certificatenumberrow);
        }else{
            //이수증 번호 생성 시작
            $query_sql = $DB->get_record_sql('
            SELECT id
            FROM {lmsdata_certificate}
            where userid = :userid and courseid = :courseid',array('userid'=>$USER->id,'courseid'=>$this->courseid)); 

            
            $certino = $DB->get_record_sql("select code from m_lmsdata_certificateno");
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 5; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $certificatenum = $certino->code."-".date(Ymd)."-".$randomString  ;    
            //이수증 번호 생성 끝
            $user_sql = $DB->get_record_sql('select usergroup from {lmsdata_user} where userid = :userid',array('userid'=>$USER->id));
            $certificatenumberrow->userid = $USER->id;
            $certificatenumberrow->courseid = $this->courseid;
            $certificatenumberrow->certificatenumber = $certificatenum; 
            $certificatenumberrow->timecreated = time(); 
            $certificatenumberrow->timemodified = time();    
            $certificatenumberrow->usergroup = $user_sql->usergroup;                                              
            $DB->insert_record('lmsdata_certificate',$certificatenumberrow);          
        }
                                                 
        $certificate_sql = $DB->get_record_sql('
            SELECT certificatenumber, timemodified
            FROM {lmsdata_certificate}
            where userid = :userid and courseid = :courseid',array('userid'=>$USER->id,'courseid'=>$this->courseid));
            
        $this->certificatenumber = $certificate_sql->certificatenumber;
        $this->certificatedate = date("d.m.Y", $certificate_sql->timemodified);                
        return $this;
    }    

    public function get_certificatemo() {
        global $DB,$USER;

        $certificatenumberrow = new stdClass();

        $query_sql = $DB->get_record_sql('
            SELECT id, reconfirm
            FROM {lmsdata_certificate}
            where userid = :userid and courseid = :courseid',array('userid'=>$USER->id,'courseid'=>$this->courseid));

        //이수증 번호 생성 시작
        $certino = $DB->get_record_sql("select code from m_lmsdata_certificateno");
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 5; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $certificatenum = $certino->code."-".date(Ymd)."-".$randomString  ;    
        //이수증 번호 생성 끝
        $certificatenumberrow->id = $query_sql->id;            
        $certificatenumberrow->userid = $USER->id;
        $certificatenumberrow->courseid = $this->courseid;
        $certificatenumberrow->certificatenumber = $certificatenum; 
        $certificatenumberrow->reconfirm = $query_sql->reconfirm+1;         
        $certificatenumberrow->timemodified = time();                                                                  
        $DB->update_record('lmsdata_certificate',$certificatenumberrow);
                                            
        $certificate_sql = $DB->get_record_sql('
            SELECT certificatenumber, timemodified
            FROM {lmsdata_certificate}
            where userid = :userid and courseid = :courseid',array('userid'=>$USER->id,'courseid'=>$this->courseid));
            
        $this->certificatenumber = $certificate_sql->certificatenumber;
        $this->certificatedate = date("d.m.Y", $certificate_sql->timemodified);                
        return $this;
    }    

    
//    public function timeset() {
//        $this->playtime = gmdate('H:i:s',$this->completetime());
//    }
    public function displaytime() {
        return gmdate('H:i:s',$this->completetime());
    }
}
