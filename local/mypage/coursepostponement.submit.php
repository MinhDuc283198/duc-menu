<?php
/**
 * 수강신청 연기 submit 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

require_login();

$applicationid = required_param('applicationid',PARAM_INT);
$reason = optional_param('reason', '', PARAM_RAW);
$scheduleid = required_param('scheduleid',PARAM_INT);

if($data = $DB->get_record('lmsdata_applications_delay',array('applicationid'=>$applicationid))){
    redirect($CFG->wwwroot . '/local/mypage/coursepostponement.php',get_string('message:existdelay','local_mypage'));
    die();
}else{
     $data = new stdClass();
     $data->applicationid = $applicationid;
     $data->courseid = $DB->get_field('lmsdata_course_applications','courseid',array('id'=>$applicationid));
     $data->userid = $USER->id;
     $data->reason = $reason;
     $data->scheduleid = $scheduleid;
     $data->timecreated = time();
     
     $dataid = $DB->insert_record('lmsdata_applications_delay', $data);
     redirect($CFG->wwwroot . '/local/mypage/coursepostponement.php',get_string('message:completedelay','local_mypage'));
  
}
   









