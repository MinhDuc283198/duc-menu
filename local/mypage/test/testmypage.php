<?php
/**
 * 마이페이지
 */
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/local/jeipoint/lib.php');

$context = context_system::instance();

require_login();

$trustPage = optional_param('trustpage', 1, PARAM_INT);
$trustPerpage = optional_param('trustperpage', 5, PARAM_INT);
$yearPage = optional_param('yearpage', 1, PARAM_INT);
$yearPerpage = optional_param('yearperpage', 5, PARAM_INT);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("마이페이지");
$PAGE->set_url(new moodle_url('/local/mypage/mypage.php'));

echo $OUTPUT->header();
?>
<div class="tab_header margin_bt30 tab_event"> <!-- tab-event -->
    <span class="m-col2 mypage-tab-learning"><a href="<?php echo $CFG->wwwroot . '/local/mypage/mycourse.php'; ?>">나의 수강현황</a></span>
    <span class="on m-col2 mypage-tab-learning" ><a href="<?php echo $CFG->wwwroot . '/local/mypage/mypoint.php'; ?>">나의 학습포인트 현황</a></span>
</div>
<div class="myinfo">
    <div class="brbox w100">
        <h5 class="point">
            나의 학습 포인트
            <a href="#" class="btn br" >포인트안내</a>
        </h5>
        <?php
        /* 나의 학습포인트 시작 */
        //신임포인트 관련
        $idnumber = JEL_COURSE_CATEGORY_TRUST;
        $trustCourses = local_mypage_get_pointList($idnumber, 19);
        //신임포인트 계산
        $trustPoint = local_mypage_get_pointCalculate($trustCourses);

        //연차별포인트 관련
        $idnumber = JEL_COURSE_CATEGORY_YEAR;
        $yearCourses = local_mypage_get_pointList($idnumber, 19);
        //연차별포인트 계산
        $yearPoint = local_mypage_get_pointCalculate($yearCourses);
        /* 나의 학습포인트 종료 */
        ?>
        <div class="chart_area">
            <div>
                <span>자격충족</span>
                <p class="bar_chart green" data-num="<?php echo $trustPoint->progress . '%'; ?>">
                    <span>
                        <?php if($trustPoint->lecturepoint != 0){?>
                        <span><?php echo $trustPoint->point . '/' . $trustPoint->lecturepoint . 'p'; ?></span>
                        <?php }?>
                    </span>
                </p>
                <?php if($trustPoint->lecturepoint != 0){?>
                <span class="<?php echo $trustPoint->progress == 100 ? 'finished' : 'unfinished'; ?>" ><?php echo $yearPoint->progress == 100 ? '이수' : '미이수'; ?></span>
                <?php }?>
            </div>
            <div>
                <span>연차교육</span>
                <p class="bar_chart orange" data-num="<?php echo $yearPoint->progress . '%'; ?>">
                    <span>
                        <?php if($yearPoint->lecturepoint != 0){?>
                        <span><?php echo $yearPoint->point . '/' . $yearPoint->lecturepoint . 'p'; ?></span>
                        <?php }?>
                    </span>
                </p>
                <?php if($yearPoint->lecturepoint != 0){?>
                <span class="<?php echo $trustPoint->progress == 100 ? 'finished' : 'unfinished'; ?>"><?php echo $yearPoint->progress == 100 ? '이수' : '미이수'; ?></span>
                <?php }?>
            </div>
        </div> 
    </div>
</div>
<h5 class="div_title">자격충족 포인트 내역</h5>
<table class="table">
    <colgroup>
        <col width="45%" />
        <col width="/" />
        <col width="/" />
    </colgroup>
    <thead>
        <tr>
            <th>지급내용</th> 
            <th>지급일</th>
            <th>지급(차감)포인트</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //신임포인트는 pointtype = 0
        $trustPoint = local_jeipoint_getUserPoint(19, 0, $trustPage, $trustPerpage);
        if (!empty($trustPoint->count)) {
            foreach ($trustPoint->points AS $point) {
                echo '<tr>';
                echo '<td class="text-left">' . $point->title . '</td>';
                echo '<td>' . date('Y.m.d', $point->timecreated) . '</td>';
                echo '<td>' . $point->point . 'P</td>';
                echo '</tr>';
            }
        } else {
            echo '<tr><td colspan="3">자격충족 포인트 내역이 없습니다.</td></tr>';
        }
        ?>
    </tbody>
</table>
<div class="table-footer-area">
    <?php
    $trust_params = array('yearpage' => $yearPage);
    $total_pages = local_jeipoint_total_pages($trustPoint->count, $trustPerpage);
    if (!empty($trustPoint->count)) {
        local_jeipoint_paging_bar($CFG->wwwroot . "/local/mypage/mypoint.php", $trust_params, $total_pages, $trustPage, 'trustpage');
    }
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->

<h5 class="div_title">연차교육 포인트 내역</h5>
<table class="table">
    <colgroup>
        <col width="45%" />
        <col width="/" />
        <col width="/" />
    </colgroup>
    <thead>
        <tr>
            <th>지급내용</th>
            <th>지급일</th>
            <th>지급(차감)포인트</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //연차별포인트는 pointtype = 1
        $yearPoint = local_jeipoint_getUserPoint(19, 1, $yearPage, $yearPerpage);
        if (!empty($yearPoint->count)) {
            foreach ($yearPoint->points AS $point) {
                echo '<tr>';
                echo '<td class="text-left">' . $point->title . '</td>';
                echo '<td>' . date('Y.m.d', $point->timecreated) . '</td>';
                echo '<td>' . $point->point . 'P</td>';
                echo '</tr>';
            }
        } else {
            echo '<tr><td colspan="3">연차교육 포인트 내역이 없습니다.</td></tr>';
        }
        ?>
    </tbody>
</table>
<div class="table-footer-area">
    <?php
    $year_params = array('trustpage' => $trustPage);
    $total_pages = local_jeipoint_total_pages($yearPoint->count, $yearPerpage);
    if (!empty($yearPoint->count)) {
        local_jeipoint_paging_bar($CFG->wwwroot . "/local/mypage/mypoint.php", $year_params, $total_pages, $yearPage, 'yearpage');
    }
    ?>
    <!-- Breadcrumbs End -->
</div> <!-- Table Footer Area End -->
<script type="text/javascript">
    $("document").ready(function () {
        $(".mypage-tab-learning").click(function () {
            var tabhref = $(this).children('a').attr('href');
            document.location.href = tabhref;
        })
    });
</script>
<?php
echo $OUTPUT->footer();
?>
