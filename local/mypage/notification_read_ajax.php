<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');

$id = optional_param('id',0, PARAM_INT);
$userid = optional_param('userid',0, PARAM_INT);


if($id>0){
    $message = $DB->get_record('message',array('id'=>$id));

    $value = message_mark_message_read($message, time());
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($value);
?>
