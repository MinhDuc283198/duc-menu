<?php
/**
 * 수강취소 페이지
 */
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';

$id = required_param('id', PARAM_INT);

$returnvalue = new stdClass();

$refund = $DB->get_record('lmsdata_course_applications', array('id' => $id));
$lmscourse = $DB->get_record('lmsdata_class', array('id' => $refund->courseid));
$refund->status = 'cancel';
$refund->paymentstatus = 'cancel';
$refund->timemodified = time();

$refund_cancel->courseid = $refund->courseid;
$refund_cancel->userid = $refund->userid;
$refund_cancel->status = 'complete';
$refund_cancel->timecreated =  time();    
$refund_cancel->applicationid = $refund->id;
$refund_cancel->refund = 0;
$refund_cancel->reason = get_strnig('studentcancel','local_mypage');    

if($refund->groupcode){
    $group = $DB->get_record('lmsdata_grouplist', array('groupcode' => $refund->groupcode));
    $groupapply = $DB->get_record('lmsdata_groupapply', array('groupid' => $group->id,'userid'=>$refund->userid));
    $groupapply->status = 2;
    $DB->update_record('lmsdata_groupapply', $groupapply);
}

if ($DB->insert_record('lmsdata_applications_cancel', $refund_cancel)) {
    $returnvalue->status = 'success';
    $DB->update_record('lmsdata_course_applications', $refund);
    $apiresult = eduhope_update_training_history_status($refund->id);
}
@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);