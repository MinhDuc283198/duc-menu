<?php
/**
 * 수강신청 정보 변경 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$id = optional_param('id', 0, PARAM_INT);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));
$PAGE->navbar->add(get_string("courseinfo",'local_mypage'));
$PAGE->navbar->add(get_string("mycoursestatus",'local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/application_edit.php'));

$sql = 'SELECT lca.*, lco.coursename
        FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.id = lca.courseid
        JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
        WHERE lca.userid = :userid AND lca.paymentstatus IS NOT NULL AND lca.paymentstatus != :cancel';

$payments = $DB->get_records_sql($sql, array('userid'=>$USER->id, 'cancel'=>'cancel'));

echo $OUTPUT->header();


if(empty($payments)){
    echo '<script type="text/javascript">alert("'.get_string('nochangeenrol','local_mypage');'"); location.href="'.$CFG->wwwroot.'/local/mypage/mycourse.php";</script>';
    die();
}
?>  
<h3 class="page_title"><?php get_string('changecourse','local_mypage')?></h3>

<h5 class="div_title top"><?php get_string('mycourse','local_mypage')?></h5>
<table class="table">
    <tbody>
        <tr>
            <th><?php get_string('coursechoose','local_mypage')?></th>
            <td class="text-left">
                <select title="course" name="course">
                    <?php
                    foreach($payments as $payment){
                        $selected = '';
                        if($payment->id == $id){
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        echo '<option value="'.$payment->id.'" '.$selected.'>'.$payment->coursename.'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
    </tbody>
</table>

<form id="apllication_edit_form" method="post" action="application_edit_submit.php">
    
</form>

<script>
    $(document).ready(function(){
        set_application_data($('select[name=course]').val());
    });
    
    /**
     * 강좌 선택 시 수강신청 데이터 셋팅
     * @returns {Boolean}
     */
    $('select[name=course]').change(function(){
        set_application_data($(this).val());
    });
    
    /**
     * 수강신청 폼에 데이터 셋팅 함수
     * @param {int} id
     * @returns {undefined}
     */
    function set_application_data(id){
        $.ajax({
            url: '<?php echo $CFG->wwwroot."/local/mypage/application_edit_form.php"?>',
            method: 'POST',
            data : {
                id : id
            },
            success: function(data) {
                $('#apllication_edit_form').html(data);
            }
        });
    }
    
    /**
    * form validation 함수
     */
    $('#apllication_edit_form').submit(function(){        
        var returnvalue = true;
        if($('input:radio[name="usertypecode"]:checked').val() == 20 && $("select[name=qualification1]").val() == '0'){
            alert(get_striing("finalchoose",'local_mypage'));
            return false;
        }
        if($('input:radio[name="usertypecode"]:checked').val() == 10 && $("select[name=qualification2]").val() == '0'){
            alert(get_striing("finalchoose",'local_mypage'));          
            return false;
        }
        
        var neiscode = '';
        $('input[name="neis[]"]').each(function(){
            neiscode += $(this).val();
        });
        
        //neis코드 체크
        if(neiscode.length != 10 && neiscode.length != 0){ //neis코드가 1자이상 10자 미만일때 경고
            alert('<?php echo get_string('validation:neis', 'local_course_application');?>');
            return false;
        }
        
        //연수지명번호 체크
        $('input[name="no[]"]').each(function(){
            if(!$(this).val()){
                alert('<?php echo get_string('validation:locationnumber', 'local_course_application');?>');
                returnvalue = false;
                return false;
            }
        });
        
        if(returnvalue == false) return returnvalue;
        
        //핸드폰 체크
        $('input[name="number[]"]').each(function(){
            if(!$(this).val()){
                alert('<?php echo get_string('validation:phone', 'local_course_application');?>');
                returnvalue = false;
                return false;
            }
        });
        
        if(returnvalue == false) return returnvalue;
        
        //이메일 체크
        $('input[name="email"]').each(function(){
            if(!$(this).val()){
                alert('<?php echo get_string('validation:email', 'local_course_application');?>');
                returnvalue = false;
                return false;
            }
        });
        
        if(returnvalue == false) return returnvalue;
    });
</script>

<?php
echo $OUTPUT->footer();
?>