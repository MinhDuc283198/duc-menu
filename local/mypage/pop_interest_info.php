<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$elid = optional_param('elid', '', PARAM_RAW);


$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

echo $OUTPUT->header();

?>
<div class="layerpop no-header">
    <a href="#" class="pop-close">닫기</a>
    <div class="pop-contents">
        <?php
        if ($elid == 'skill_groups') :
        ?>
            <div class="cont-tit">
                <strong><?php echo get_string('my_inter:skill_group', 'local_job'); ?></strong>
                <span><?php echo get_string('my_inter:limit_3_desc', 'local_job'); ?></span>
            </div>

            <div class="chk-label-group" style="max-height: 400px; overflow: auto;">
                <?php
                $sql = "SELECT s.id, s.name
                		FROM {vi_skill_groups} s
                		ORDER BY s.name";
                $skill_groups = $DB->get_records_sql($sql, array());
                if (count($skill_groups) == 0) {
                    $skill_groups = $DB->get_records('vi_skill_groups', array(), 'name', '*', 0, 10);
                }
                ?>
                <?php foreach ($skill_groups as $skill_group) : ?>
                    <label>
                        <input class="group_choice" onclick="count_checked_item(this)" name="pop_inter_chk[]" data-name="<?php echo $skill_group->name; ?>" value="<?php echo $skill_group->id; ?>" type="checkbox" />
                        <span><?php echo $skill_group->name; ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        <?php elseif ($elid == 'user_skills') : ?>
            <div class="cont-tit">
                <strong><?php echo get_string('my_inter:skill', 'local_job'); ?></strong>
                <span><?php echo get_string('my_inter:limit_3_desc', 'local_job'); ?></span>
            </div>

            <div class="chk-label-group" style="max-height: 400px; overflow: auto;">
                <?php
                $sql = "SELECT s.*
                    FROM {vi_skills} s
                    ORDER BY s.name";
                $skills = $DB->get_records_sql($sql, array());
                if (count($skills) == 0) {
                    $skills = $DB->get_records('vi_skills', array(), 'name', '*', 0, 10);
                }
                ?>
                <?php foreach ($skills as $skill) : ?>
                    <label>
                        <input class="skill_choice" onclick="count_checked_item(this)" name="pop_inter_chk[]" data-name="<?php echo $skill->name; ?>" value="<?php echo $skill->id; ?>" type="checkbox" />
                        <span><?php echo $skill->name; ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        <?php elseif ($elid == 'user_workplaces') : ?>
            <div class="cont-tit">
                <strong><?php echo get_string('my_inter:workplace', 'local_job'); ?></strong>
                <span><?php echo get_string('my_inter:unlimited', 'local_job'); ?></span>
            </div>

            <div class="chk-label-group">
                <?php
                $districts = $DB->get_records('vi_districts', array(), 'name', '*', 0, 10);
                ?>
                <?php foreach ($districts as $district) : ?>
                    <label>
                        <input class="district_choice" onclick="count_checked_item(this)" name="pop_inter_chk[]" data-name="<?php echo $district->name; ?>" value="<?php echo $district->id; ?>" type="checkbox" />
                        <span><?php echo $district->name; ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

    </div>
    <div class="btn-area text-center">
        <input onclick="btnAddAction('<?php echo $elid; ?>')" type="button" value="<?php echo get_string('my_inter:save', 'local_job'); ?>" class="btns point w100" />
    </div>
</div>
<?php
echo $OUTPUT->footer();
