<?php
/**
 * 아이디 비밀번호 찾기 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("idpwdfind",'local_mypage'));
$PAGE->navbar->add(get_string("idpwdfind",'local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/find_idpw.php'));

$ftype = optional_param('ftype', 'id', PARAM_RAW);
$name = optional_param('name', '', PARAM_RAW);
$birthday = optional_param('birthday', '', PARAM_RAW);
$phone = optional_param('phone', '', PARAM_RAW);
$id = optional_param('id', '', PARAM_RAW);
$submit = optional_param('submit', 0, PARAM_INT);

echo $OUTPUT->header();
?>  
<script type="text/javascript">
    /**
     * 이메일 직접입력과 선택 함수
     */
    $(document).ready(function () {
        /** email change event **/
        $(".eType").change(function () {
            var val = $(this).val();
            if (val == get_string("directinput",'local_mypage')) {
                $(".email02").val("").removeAttr("disabled");
            } else {
                $(".email02").val(val).attr("disabled", "disabled");
            }
        });
        /** email change event **/

    });
</script>
<h3 class="page_title"><?php get_string("idpwdfind",'local_mypage') ?> </h3>
<?php
//submit 일때
if($submit == 1){    
    if($ftype == 'id'){  //아이디찾기
        $output = eduhope_find_id($name, $birthday, $phone);
        
        if($output->dataResult == '00' && $output->taskResult == '00' && !empty($output->returnData[0]->memberId)){
            redirect($CFG->wwwroot.'/local/mypage/find_idpw.php',get_string('useridn','local_mypage'),'"'.$output->returnData[0]->memberId.'"',get_string('is','local_mypage'),20);
        } else {
            redirect($CFG->wwwroot.'/local/mypage/find_idpw.php',get_string('nomatchingaccount','local_mypage'), 20);
        }
    } else {  //비밀번호찾기
        $output = eduhope_find_pw($id, $phone);
        
        if($output->dataResult == '00' && $output->taskResult == '00' && $output->returnData[0]->memberYn == 'Y'){
            $temp_password = PHPRandom::getString(2).PHPRandom::getInteger(100, 999).PHPRandom::getString(3).PHPRandom::getInteger(10, 99);
            $output = eduhope_password_update($output->returnData[0]->memberSeq, $temp_password);
            if($output->dataResult == '00' && $output->taskResult == '00'){
                eduhope_balsong_sms(get_string('imsipwd ','local_mypage').$temp_password.get_string('is.','local_mypage'), $phone);
                redirect($CFG->wwwroot.'/local/mypage/find_idpw.php',get_string('imsipwdmessage','local_mypage'), 20);
            } else {
                redirect($CFG->wwwroot.'/local/mypage/find_idpw.php',get_string('nomatchingaccount','local_mypage'), 20);
            }
        } else {
            redirect($CFG->wwwroot.'/local/mypage/find_idpw.php',get_string('nomatchingaccount','local_mypage'), 20);
        }
    }
}else {
?>
    <div class="tab_header style02">
        <span class="on col2"><?php get_string('idfind','local_mypage') ?></span>
        <span class="col2"><?php get_string('pwdfind','local_mypage') ?></span>
    </div>
    <!-- tab start -->
    <div class="tab_contents nobg">
        <!-- 아이디 찾기 -->
        <div class="find_id on">
            <ul class="dash">
                <li><?php get_string('nowuseelepost','local_mypage') ?></li>
                <li><?php get_string('idshapshap','local_mypage') ?></li>
            </ul>
            <form id="form01" action="">
                <input type="hidden" name="ftype" value="id">
                <input type="hidden" name="submit" value="1">
                <table class="table">
                    <tbody>
                        <tr>
                            <th><?php get_string('name','local_mypage') ?></th>
                            <td class="text-left">
                                <input type="text" name="name" title=<?php get_string("name",'local_mypage') ?> class="name" placeholder=<?php get_string("name",'local_mypage') ?> />
                            </td>
                        </tr>
                        <tr>
                            <th><?php get_string('cer:birth','local_mypage') ?></th>
                            <td class="text-left">
                                <input type="text" name="birthday" title=<?php get_string("cer:birth",'local_mypage') ?>  placeholder=<?php get_string("yeahgu",'local_mypage') ?> class="name" placeholder=<?php get_string("cer:birth",'local_mypage') ?> />
                            </td>
                        </tr>
                        <tr>
                            <th><?php get_string('phonenum','local_mypage') ?></th>
                            <td class="text-left">
                                <input type="text" name="phone" title=<?php get_string("phonenum",'local_mypage') ?> class="name"  placeholder=<?php get_string("yeahgong",'local_mypage') ?> placeholder=<?php get_string("phonenum",'local_mypage') ?> />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <input type="submit" value=<?php get_string("idfind",'local_mypage') ?> class="btn orange big"/>
                </div>
            </form>
        </div>
        <!-- 아이디 찾기 -->

        <!-- 비밀번호 찾기 -->
        <div class="find_pw ">
            <input type="hidden" name="ftype" value="pw">
            <input type="hidden" name="submit" value="1">
            <ul class="dash">
                <li><?php get_string('useridpwdname','local_mypage') ?></li>
            </ul>
            <form id="form02" action="">
                <table class="table">
                    <tbody>
                        <tr>
                            <th><?php get_string('id','local_mypage') ?></th>
                            <td class="text-left">
                                <input type="text" name="id" title=<?php get_string("id",'local_mypage') ?> class="id" placeholder=<?php get_string("d",'local_mypage') ?> />
                            </td>
                        </tr>
                        <tr>
                            <th><?php get_string('phonenum','local_mypage') ?></th>
                            <td class="text-left">
                                <input type="text" name="phone" title=<?php get_string("phonenum",'local_mypage') ?>  placeholder=<?php get_string("yeahgong",'local_mypage') ?> class="name" placeholder=<?php get_string("phonenum",'local_mypage') ?> />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <input type="submit" value=<?php get_string("imsipwdIssued",'local_mypage') ?> class="btn orange big"/>
                </div>
            </form>
            <!-- 비밀번호 찾기 -->
        </div>
    </div>
<!-- tab end -->

<?php
}

echo $OUTPUT->footer();
?>


