<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string('notification','local_mypage'));
$thistime = time();
echo $OUTPUT->header();
$type = optional_param('type', 3, PARAM_INT);
require_login();
if($type==1){ // mk 알림만
    $notifications = local_mypage_get_message($USER->id); 
} else if ($type==2){ // mk jobs 알림만
    $notifications = ''; 
} else if ($type==3) {
    $notifications = local_mypage_get_message($USER->id); 
}

?>
<script src="<?php echo $CFG->wwwroot?>/local/mypage/notification.js"></script>  
<h2 class="pg-tit"> <?php echo get_string('notification','local_mypage');?></h2>
<ul class="bar-list f-l">
    <li <?php if($type==3){ echo 'class="on"'; } ?> ><a href="/local/mypage/notification.php?type=3"><?php echo get_string('All','local_job');?></a></li>
    <li <?php if($type==1){ echo 'class="on"'; } ?> ><a href="/local/mypage/notification.php?type=1">mk</a></li>
    <li <?php if($type==2){ echo 'class="on"'; } ?>><a href="/local/mypage/notification.php?type=2">mk jobs</a></li>
</ul>
<div class="alram-right">
    <input type="button" class="btns br" value="<?php echo get_string('alldel','local_mypage');?>" onclick='message_delete(0,0,<?php echo $USER->id;?>,"<?php echo get_string('delallnoti','local_mypage');?>")'/>
</div>

<ul class="alram-list">
    <?php
    if (!empty($notifications)) {
        foreach ($notifications as $message) {
            $time = notifi_time($message->timecreated);
          ?>
            <li class ="noti<?php echo $message->id;?> <?php echo $message->timeread ? 'read' : '' ; ?>"> 
                <div class="tit" onclick="message_read(<?php echo $message->id;?>,<?php echo $USER->id?>)">[mk] <?php echo $message->smallmessage; ?></div>
                <p><?php
        echo (strlen($message->fullmessage) > 100) ? substr($message->fullmessage, 0, 100) . '...' : $message->fullmessage;
        ?></p>
                 <p class="tm"><?php echo $time; ?></p>
                <a href="#" class="al-close" onclick='message_delete(<?php echo $message->id;?>,<?php echo $message->timeread;?>,<?php echo $USER->id?>,"<?php echo get_string('delnoti','local_mypage');?>")'> <?php echo get_string('close','local_mypage');?></a>
            </li>
            <?php
        }
    } else {
        ?>
        <div class="no-data">
            <?php echo get_string('nodata:notifi','local_mypage');?>
        </div>
    <?php } ?>
</ul>
<?php
echo $OUTPUT->footer();
?>
<script>



</script>