<?php
/**
 * 프로필 사진 수정 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$user = $DB->get_record('user',array('id'=>$USER->id));

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
?>  
<link href="/theme/oklassedu/style/common.css" rel="stylesheet" />
<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<div id="main_popup">
    <h3 id="pop_title">
        <span class="l_content">    
            <?php get_string('changeprofilepicture','local_mypage') ?>
      </span>
        <span class="r_content">
            <img class="close r_content" id="close<?php echo $id;?>" data-id="<?php echo $id;?>" src="/theme/oklassedu/pix/images/close_w.png" alt="" />
        </span>
    </h3>
    
    <div class="pop_contents">
        <form action="userpicture_submit.php" id="userpicture_form" method="post" enctype="multipart/form-data" >
            <table class="table table-condensed">
                <tbody>
                    <tr>
                        <th class="w200px"><?php get_string('nowpicture','local_mypage') ?></th>
                        <td class="text-left">
                            <?php
                            $user = $DB->get_record('user', array('id'=>$USER->id));
                            $userpicture = new user_picture($user);
                            $userpicture->size = 1; // Size f1.
                            $url = $userpicture->get_url($PAGE)->out(false);
                            ?>
                            <img src="<?php echo $url;?>" alt="user_img" />
                        </td>
                    </tr>
                    <tr>
                        <th class="w200px"><?php get_string('delete','local_mypage') ?></th>
                        <td class="text-left">
                            <input type="checkbox" name="picturedelete" value="1" /> <?php get_string('delete','local_mypage') ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="w200px"><?php get_string('newpicture','local_mypage') ?></th>
                        <td class="text-left">
                            <input type="file" name="ufile" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="table-footer-area">
                <div class="btn-area btn-area-right">
                    <input type="submit" style="float:right;" value="<?php get_string('save','local_mypage') ?>" class="btn orange" />
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    /**
     * 업로드 파일 검사 함수
     */
    $('#userpicture_form').submit(function(){
        var ext = $('input[name=ufile]').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1 && $('input[name=ufile]').val() != "" ) {
            alert('gif,png,jpg,jpeg '.get_string('fileupload','local_mypage');'');
            return false;
        }
    });
</script>