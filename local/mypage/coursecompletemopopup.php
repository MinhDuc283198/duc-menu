<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');
require_once($CFG->dirroot . '/local/mypage/coursecompletepopup/classes/certificates_class.php');
$courseid = optional_param('id', 0, PARAM_INT);

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('print');

echo $OUTPUT->header();

$certificates = new certificates($courseid);
$certificates->get_info();
$certificates->user();
$certificates->completetime();
$certificates->teacher();
$certificates->get_certificatemo();

$templatecontext = [
    'certificates' => $certificates
];
echo $OUTPUT->render_from_template('local_mypage/certificates', $templatecontext);
echo $OUTPUT->footer();
?>