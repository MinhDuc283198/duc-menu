<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * 초로 되어있는 것을 00:00:00 형티의 시간으로 반환
 * @param string $seconds
 */

function lcms_time_from_seconds($seconds) {
    $time = new stdclass();
    if($seconds != 0) {
        $time->h = sprintf('%02d', floor($seconds / 3600));
        $time->m = sprintf('%02d', floor(($seconds % 3600) / 60));
        $time->s = sprintf('%02d', $seconds - ($time->h * 3600) - ($time->m * 60));
    } else {
        $time->h = '00';
        $time->m = '00';
        $time->s = '00';
    }
    return $time;
}


//function lcms_can_update_progress($lcmsorid) {
//    global $DB;
//    
//    if (is_object($lcmsorid)) {
//        $lcms = $lcmsorid;
//    } else {
//        $lcms = $DB->get_record('lcms', array('id' => $lcmsorid)); // 이거 수정
//    }
//    
//    if($lcms->progress != 1) {
//        return false;
//    }
//    
//    if($lcms->islock == 2) {
//        return true;
//    }
//    
//    $timenow = time();
//    if($lcms->timestart <= $timenow
//            && $lcms->timeend >= $timenow) {
//        return true;
//    }
//    
//    return false;
//}