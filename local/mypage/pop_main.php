<?php
/**
 * 메인 팝업 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$id = optional_param("id", 0, PARAM_INT);

$popup = $DB->get_record('popup', array('id'=>$id));

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
?>  
<div id="main_popup">
    <h3 id="pop_title">
        <span class="l_content">    
            <?php echo $popup->title;?>
        </span>
        <span class="r_content">
            <img class="close r_content" id="close<?php echo $id;?>" data-id="<?php echo $id;?>" src="/theme/oklassedu/pix/images/close_w.png" alt="" />
        </span>
    </h3>
    
    <div class="pop_contents" <?php if($popup->availablescroll == 1) echo 'style="overflow-y: scroll;"'?>>
        <?php echo $popup->description;?>
    </div>

    <div class="pop_footer text-right">
        <input type="checkbox" name="hidepop<?php echo $id;?>" value="<?php echo $id;?>" id="hidepop<?php echo $id;?>" />
        <label for="hidepop<?php echo $id;?>"><?php get_string('oneweekendnopen','local_mypage') ?></label>
    </div>
</div>