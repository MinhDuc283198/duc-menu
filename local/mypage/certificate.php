<?php
/**
 * 이수증 리스트 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
require_once($CFG->dirroot . '/local/jinoboard/lib.php');

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));
$PAGE->navbar->add(get_string('courseinfo', 'local_mypage'));
$PAGE->navbar->add(get_string('cer:cerprint', 'local_mypage'));

require_login();

echo $OUTPUT->header();

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$searchtext = optional_param('searchtext', '', PARAM_TEXT);
?>  
<h3 class="page_title">
    <span><?php echo get_string('cer:cerprint', 'local_mypage'); ?></span>
    <div class="r_content">
        <input type="button" onclick="location.href='<?php echo $CFG->wwwroot.'/local/mypage/application_edit.php';?>'" value= <?php get_string("changecourse",'local_mypage')?> class="btn orange big" />
    </div>
</h3>
<div class="textbox">
    <?php echo get_string('cer:cerconfirm', 'local_mypage', $USER->lastname); ?>
</p>
</div>

<div class="search_option">
    <input type="text" name="search" class="search" placeholder="<?php echo get_string('cer:coursesearch', 'local_mypage'); ?>" />
    <input type="submit" value="<?php echo get_string('cer:search', 'local_mypage'); ?>" class="btn gray h30" />
</div>


<table class="table">
    <thead>
        <tr>
            <th><?php echo get_string('cer:coursename', 'local_mypage'); ?>/<?php echo get_string('cer:classnum', 'local_mypage'); ?></th>
            <th><?php echo get_string('cer:learningtime', 'local_mypage'); ?></th>
            <th><?php echo get_string('cer:cerprint', 'local_mypage'); ?></th>
        </tr>       
    </thead>
    <tbody>
        <?php
        $sql = "select lca.id, lca.neiscode, lca.locationnumber, lco.coursename, lc.classyear, lc.classnum, lc.learningstart, lc.learningend, 1 as old 
                from {lmsdata_course_applications} lca             
                join {lmsdata_class} lc on lc.id = lca.courseid 
                join {lmsdata_course} lco on lco.id = lc.parentcourseid 
                where lca.completionstatus = 1 and lca.userid = :userid  and lca.status = 'apply'
                union all 
                select lao.id, lao.neiscode, lao.locationnumber, lco.coursename, null classyear, null classnum, lao.studystart learningstart, lao.studyend learningend, 2 as old 
                FROM {lmsdata_applications_old} lao
                JOIN {lmsdata_course} lco on lco.coursecd = lao.coursecd 
                where lao.completionstatus = 1 and lao.userid = $USER->id ";

        $lists = $DB->get_records_sql($sql, array('userid' => $USER->id));
        foreach ($lists as $list) {
            ?>

            <tr>
                <td class="text-bold"><?php echo $list->coursename . ' / ' . get_string('yearterm', 'local_mypage', array('year' => $list->classyear, 'term' => $list->classnum)); ?></td>
                <td>
                    <?php echo date('Y-m-d', $list->learningstart) ?><br />
                    ~<?php echo date('Y-m-d', $list->learningend) ?>
                </td>
                <td>
                    <?php if ($list->old == 1) { ?>
                        <a href="<?php echo $CFG->wwwroot . '/local/mypage/certificate_print.php?id=' . $list->id . '&userid=' . $USER->id . '&olddata=0'; ?>" target="_blank"><input type="button" value="<?php echo get_string('cer:print', 'local_mypage'); ?>" name="print" class="btn brd" target="_blank"/></a>
                    <?php } else { ?>
                        <a href="<?php echo $CFG->wwwroot . '/local/mypage/certificate_print.php?id=' . $list->id . '&userid=' . $USER->id . '&olddata=1'; ?>" target="_blank"><input type="button" value="<?php echo get_string('cer:print', 'local_mypage'); ?>" name="print" class="btn brd" target="_blank"/></a>
                    <?php } ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div class="table-footer-area"> 
    <?php
    $totalcount = $DB->count_records_sql('select count(*) from (' . $sql . ') z', array('userid' => $USER->id));
    $total_pages = ceil($totalcount / $perpage);
    $page_params = array();
    $page_params['perpage'] = $perpage;
    jinoboard_get_paging_bar($CFG->wwwroot . "/local/mypage/certificate.php", $page_params, $total_pages, $page, 0);
    ?>
</div>

<?php
echo $OUTPUT->footer();
?>


