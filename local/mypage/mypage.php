<?php
/**
 * 마이페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/course/format/lguplus/lib.php');

$context = context_system::instance();

require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string('pluginname','local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/mypage.php'));

$PAGE->requires->jquery();

$userinfos = $DB->get_records_sql('SELECT u.id,u.timecreated, u.username, u.auth, u.firstname, u.lastname, u.phone1, u.phone2, u.email, lu.address, lu.address_detail,lu.usergroup,lu.sns, lu.email2  FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid',array('userid'=>$USER->id));
$avatar_url=$DB->get_record('avatar_user',array('user_id'=>$USER->id));
echo $OUTPUT->header();
$today = time();
foreach($userinfos as $userinfo){
    $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
}
if($prof=='pr'){
    $prtext = get_string('coursecharge','local_mypage');
    $prsql = "FROM {lmsdata_class} lc
            JOIN {course} mc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {lmsdata_course_professor} lcp ON lcp.courseid = lc.parentcourseid
            WHERE lcp.teacherid = :userid
            ORDER BY lc.id DESC";
    $prselect = "SELECT
                lc.id,lc.title,lc.courseperiod,  lc.reviewperiod,lc.vi_title,lc.en_title,lco.courseid as lcocourseid,  lco.lecturecnt, lc.courseid as lcid,
                lc.parentcourseid,lcp.teacherid,mc.id as mcid ";
    $prselect_c ="SELECT COUNT(*) ";
    $courses =$DB->get_records_sql($prselect.$prsql,array('userid'=>$USER->id));
    $courses_cnt2 = $DB->count_records_sql($prselect_c.$prsql,array('userid'=>$USER->id));//담당 강좌 
} else {
    $prtext = get_string('mycourse:buy','local_mypage');
    $courses_cnt2 = local_mypage_get_mycourse('all',true);//전체 강좌
    $courses = local_mypage_get_mycourse('ing'); 
}
$courses_cnt1 = local_mypage_get_mycourse('ing',true);//수강중인 강좌
$courses_cnt3 = local_mypage_get_mycourse('end',true);//핛습 완료 강좌 -> 복습기간 남아있더래도 일단 기간 끝난거.
$courses_cnt4 = $DB->count_records_sql('SELECT COUNT(*) FROM {course_completions} WHERE timecompleted > 0 AND userid = :userid;',array('userid'=>$USER->id));//수료한 강좌
?>  

<div class="my-box imprt half">
    <div class="rw">
       <div style=" width: 150px; height: 150px; margin: 0 auto">
           <?php if($avatar_url==false) :?>
               <img width="100%" height="100%" style="object-fit: cover;" src="<?php echo $CFG->wwwroot . '/pluginfile.php/1/local_visang/images/973322105/logo ngang Visangvietnam (nền trắng).png'?>" alt="Image of Employee">
           <?php else: ?>
               <img width="100%" height="100%" style="object-fit: cover;" src="<?php echo $CFG->wwwroot . '/pluginfile.php/'.$avatar_url->url;?>" alt="Image of Employee">
           <?php endif; ?>
       </div>
    </div>
    <?php foreach($userinfos as $userinfo){?>
    <div class="rw">
        <strong class="t-black"><?php echo fullname($USER); ?></strong>
        <p>
            <span class="t-gray"><?php echo get_string('dateofaccession','local_mypage');?></span>
            <span class="t-blue02"><?php echo date('Y.m.d', $userinfo->timecreated);?></span>
            <a href="<?php echo $CFG->wwwroot; ?>/local/my/info.php" class="btns br"><?php echo get_string('myinfoedit','local_mypage');?></a>
        </p>
    </div>
    <div class="rw">
        <strong><?php echo get_string('userid','local_mypage');?></strong>
        <p>
            <img src="/theme/oklassedu/pix/images/<?php echo $userinfo->sns ? 'icon_'.$userinfo->sns : 'icon_mail'; ?>.png" width="24" alt="<?php echo $userinfo->sns ? $userinfo->sns : 'email'; ?>">
            <span><?php echo $userinfo->sns ? $userinfo->sns.get_string('join','local_my') : $userinfo->username; ?></span>
        </p>
    </div>
    <div class="rw">
        <strong><?php echo get_string('email','local_mypage');?></strong>
        <p><strong><?php echo $userinfo->email2 ? $userinfo->email2 : $userinfo->email ; ?></strong></p>
    </div>
    <div class="rw">
        <strong><?php echo get_string('phonenum','local_mypage');?></strong>
        <p><strong><?php echo $userinfo->phone1; ?></strong></p>
    </div>
<!--    <div class="rw tp-line">
        <strong>한국어 활용 수준</strong>
        <p class="birth">
            <select>
                <option value="">일상대화 가능</option>
            </select>
        </p>
    </div>-->

    <div class="my-crs-list">
        <div class="bg-tit"><?php echo get_string('mycourse','local_mypage');?></div>
        <div class="num">
            <ul>
                <li>
                    <strong><?php echo get_string('mycourse:ing','local_mypage');?></strong>
                    <span class="t-blue02"><em><?php echo $courses_cnt1; ?></em><?php echo get_string('coursenum','local_mypage');?></span>
                </li>
                <li>
                    <strong><?php echo $prtext;?></strong>
                    <span class="t-blue02"><em><?php echo $courses_cnt2; ?></em><?php echo get_string('coursenum','local_mypage');?></span>
                </li>
                <li>
                    <strong><?php echo get_string('mycourse:complete','local_mypage');?></strong>
                    <span class="t-blue02"><em><?php echo $courses_cnt4; ?></em><?php echo get_string('coursenum','local_mypage');?></span>
                </li>
                <li>
                    <strong><?php echo get_string('mycourse:end','local_mypage');?></strong>
                    <span class="t-blue02"><em><?php echo $courses_cnt3; ?></em><?php echo get_string('coursenum','local_mypage');?></span>
                </li>
            </ul>
        </div>
    </div>
    <?php } ?>
</div>

<h4 class="pg-tit"><?php echo $prof=='pr' ? get_string('coursecharge','local_mypage') :  get_string('mycourse:ing','local_mypage');?> <a href="/local/mypage/mycourse.php<?php echo $prof=='pr' ? '?type=1' :  '';?>" class="mr_all"><?php echo get_string('viewall','local_mypage');?></a></h4>
<?php 
if($courses){
    ?>
    <ul class="thumb-list style04 course slider">
        <?php foreach($courses as $course){
             $context = context_course::instance($course->lcocourseid);
             $path = local_course_get_imgpath($context->id);
             $starttime = $course->timemodified; //강좌 듣기 시작한 날
             $endtime = $course->enddate; // 강좌 끝나는 날 
             $pro = format_lguplus_get_course_progress($course->lcid);
             $teacher_list_array = array();
             $teacher = '';
             $query = "select lcp.teacherid from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $course->parentcourseid;
             $teacher_array = $DB->get_records_sql($query);
             foreach ($teacher_array as $ta) {
                $user = local_course_teacher_id($ta->teacherid);
                $teacher_list_array[] = $user->firstname;
                $teacher = implode(',', $teacher_list_array);
             }
             switch (current_language()){
                case 'ko' :
                    $coursename = $course->title;
                    break;
                case 'en' :
                    $coursename = $course->en_title;
                    break;
                case 'vi' :
                    $coursename = $course->vi_title;
                    break;
            } 
            
            if($prof=='pr'){ 
                 $getstudentsql = "SELECT count(*) 
                                        FROM m_lmsdata_payment lp
                                        JOIN m_lmsdata_class lc ON lc.id = lp.ref AND lp.status = 1
                                        JOIN m_context ctx ON ctx.instanceid = lc.courseid and ctx.contextlevel = 50
                                        JOIN m_role_assignments ra ON ctx.id = ra.contextid AND ra.userid = lp.userid 
                                        JOIN m_role ro ON ro.id = ra.roleid and ro.id = 5
                                        JOIN m_course  c ON c.id = ctx.instanceid 
                                        JOIN m_user u ON lp.userid = u.id
                                        JOIN m_lmsdata_user lu ON u.id = lu.userid 
                                        WHERE  c.id = :courseid2 and u.id !=$USER->id";
                 $getstudent = $DB->count_records_sql($getstudentsql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel1' => 50, 'archetype' => 'student'));
                     
                $quesql = 'select count(*)
                from {jinotechboard} j
                join {jinotechboard_contents} jc on j.id = jc.board
                where j.`type` =2  and step = 0 and jc.id not in (select ref from {jinotechboard_contents} where step = 1 and id!=ref ) and jc.course = :course'; 
                $getqna = $DB->count_records_sql($quesql, array('course' => $course->lcid));
                
                ?>
                  <li>
                    <div class="wp">
                        <div class="img">
                            <a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <img src="<?php echo $path; ?>" alt="<?php echo $coursename; ?>" /> </a>
                        </div>
                        <div class="txt">
                            <div class="tit"><a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <?php echo $coursename; ?></a></div>
                             <p><span><?php echo $teacher; ?> <!--<?php echo get_string('teacher','local_mypage');?>--> </span><span><?php echo get_string('total','local_mypage');?> <?php echo $course->lecturecnt; ?><?php echo get_string('course','local_mypage');?></span></p>
                              <p class="num"><em><?php echo get_string('student','local_mypage'); ?></em> <?php echo number_format($getstudent); ?><?php echo get_string('people','local_mypage'); ?></p>
                            <p class="num"><em><?php echo get_string('unanswered','local_mypage'); ?>Q&amp;A</em> <?php echo number_format($getqna); ?><?php echo get_string('coursenum','local_mypage'); ?></p>
                        </div>
                    </div>
                </li> 
        <?php
            } else {
                           ?>
                <li>
                    <div class="wp">
                        <div class="img">
                            <a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <img src="<?php echo $path; ?>" alt="<?php echo $coursename; ?>" /> </a>
                        </div>
                        <div class="txt">
                            <div class="tit"><a href="/course/view.php?id=<?php echo $course->lcid ;?>" target="_blank"> <?php echo $coursename; ?></a></div>
                            <p><span><?php echo $teacher; ?> <!--<?php echo get_string('teacher','local_mypage');?>--> </span><span><?php echo get_string('total','local_mypage');?> <?php echo $course->lecturecnt; ?><?php echo get_string('course','local_mypage');?></span></p>
                            <p class="t-gray"><?php echo get_string('registrationday','local_mypage');?>: <?php echo date('Y.m.d',$course->timemodified); ?>~<?php echo date('Y.m.d',$course->enddate); ?></p>
                        </div>
                        <div class="bar-area">
                            <span><?php echo get_string('progress','local_mypage');?>(<?php echo $pro['course']->courseprogress; ?>%)</span>
                            <p class="bar-event" data-num="<?php echo $pro['course']->courseprogress; ?>%"><span></span></p>
                        </div>
                    </div>
                </li>
        <?php  
            }

        }?>
    </ul>
     <?php
} else {
    ?><div class="no-data style02 mg-bt30">
    <div><?php if($prof=='pr'){ echo get_string('nocourse2','local_mypage'); } else { echo get_string('nocourse','local_mypage'); }?></div>
        <?php if($prof=='pr'){ } else { ?><a href="/local/course/main.php" class="btns point arrow"><?php echo get_string('viewallcourse','local_mypage');?></a><?php } ?>
</div>
        <?php
}?>

<?php

        $selectcnt = "SELECT COUNT(*) ";
        $select = "select jc.id,jc.viewcnt,jc.title,jc.userid,u.firstname,u.lastname,jc.ref,jc.step,jc.timecreated ";
        $sql = " from {jinoboard_contents} jc join {user} u on u.id=jc.userid  where jc.board = 2 and jc.isnotice = 0  AND (jc.userid = $USER->id OR jc.ref IN (SELECT id FROM {jinoboard_contents} WHERE userid = $USER->id)) order by jc.ref DESC, jc.step ASC limit 3";
        $contents = $DB->get_records_sql($select.$sql, array());
        $totalcount = $DB->count_records_sql($selectcnt.$sql, array());

?>
<h4 class="pg-tit"><?php echo get_string('qna','local_mypage');?><a href="/local/jinoboard/index.php?type=2" class="mr_all"><?php echo get_string('viewall','local_mypage');?></a></h4>
<table class="table m-block mg-bt50">
    <thead>
        <tr>
            <th width="10%"><?php echo get_string('num','local_jinoboard');?></th>
            <th><?php echo get_string('title','local_jinoboard');?></th>
            <th width="15%"><?php echo get_string('writer','local_jinoboard');?></th>
            <th width="15%"><?php echo get_string('date','local_jinoboard');?></th>
            <th width="10%"><?php echo get_string('view:cnt','local_jinoboard');?></th>
        </tr>
    </thead>
    <tbody>
            <?php 
            if($contents){
                $board = $DB->get_record('jinoboard', array('type' => 2));
            foreach($contents as $content){
                ?>
                <tr>
                    <td class="m-hide"><?php echo $totalcount; ?></td>
                    <td class="text-left overflow"><a href="/local/jinoboard/detail.php?id=<?php echo $content->id; ?>"><?php echo $content->title; ?></a><?php if($board->newday){ echo ($board->timemodified + (86400*$board->newday))>$today ? '<span class="ic-new">N</span>' : '' ;} ?></td>
                    <td><?php echo $content->firstname.$content->lastname; ?></td>
                    <td><?php echo date('Y-m-d',$content->timecreated); ?></td>
                    <td class="m-f-r"><span class="m-show inline"><?php echo get_string('view:cnt','local_jinoboard');?></span><?php echo $content->viewcnt; ?></td>      
                </tr>
                <?php
                $totalcount--;
            }} else {
                ?><tr><td colspan="5"><?php echo get_string('nodata:qna','local_mypage');?></td></tr><?php
            } ?>
    </tbody>
</table>
<?php

    $pays =local_mypage_payment(0,$USER->id,0,5);
    $paycount = $DB->count_records_sql('select count(id) from {lmsdata_payment} where userid = :userid', array('userid'=>$USER->id));
    $paynum = $paycount;
?>
<h4 class="pg-tit"><?php echo get_string('paymentlist','local_mypage');?><a href="/local/mypage/mypayment.php" class="mr_all"><?php echo get_string('viewall','local_mypage');?></a></h4>
<table class="table m-block">
    <thead>
        <tr>
            <th width="5%"><?php echo get_string('num','local_jinoboard');?></th>
            <th width="7%"><?php echo get_string('goods','local_mypage');?></th>
            <th><?php echo get_string('goods:buy','local_mypage');?></th>
            <th width="11%"><?php echo get_string('goods:pay','local_mypage');?></th>
            <th width="10%"><?php echo get_string('goods:date','local_mypage');?></th>
            <th width="10%"><?php echo get_string('goods:way','local_mypage');?></th>
            <th width="8%"><?php echo get_string('post:state','local_mypage');?></th>
            <!--<th width="60px">영수증</th>-->
        </tr>
    </thead>
    <tbody>
        <?php
        if($pays){
            foreach($pays as $pay){
                  switch ($pay->status){
                    case 1 :
                        $string = get_string('beforepayment','local_mypage');
                        break;
                    case 2 :
                        $str =get_string('refund','local_mypage');
                        $string = '<a href="/local/mypage/refund.php" class="btns br_blue refund">'.$str.'</a></td>';
                        break;
                    case 3 :
                        $str =get_string('cantrefund','local_mypage');
                        $string = '<span class="t-red">'.$str.'</span>';
                        break;
                    case 4 :
                        $str =get_string('wait','local_mypage');
                        $string = '<span class="t-black">'.$str.'</span>';
                        break;
                    case 5 :
                        $str =get_string('sucrefund','local_mypage');
                        $string = '<span class="t-blue02">'.$str.'</span>';
                        break;
                    default : $string ="-";
                }
                if($pay->ctype != 1 ){
                    if($pay->delivery == 0 || $pay->delivery == '' || $pay->delivery == null ){
                        $deliverytext = get_string("post:pre",'local_mypage');
                    }else if ($pay->delivery == 2){
                        $deliverytext = get_string("post:ing",'local_mypage');
                    }else{
                        $deliverytext = get_string("post:end",'local_mypage');
                    }
                }else{
                    $deliverytext = "-";
                }
                if($pay->ctype == 1){
                    $type_txt = get_string('course', 'local_management');
                }else if($pay->ctype == 3){
                        $type_txt = get_string('course', 'local_management') .'+' .get_string('book', 'local_management');
                }else{
                    $type_txt = get_string('book', 'local_management');
                }
                $price_txt = number_format($pay->price).' VND';
                if(($pay->forced == 1)){
                    $price_txt = get_string('free', 'local_management');
                }
                ?>
                <tr>
                    <td class="m-hide"><?php echo $paynum;?></td>
                    <td class="m-hide"><?php echo $type_txt; ?></td>
                    <td class="text-left"><span class="m-show inline">[<?php echo $pay->type==1 ? get_string('course','local_mypage') : get_string('book','local_mypage'); ?>]</span><?php echo $pay->title; ?></td>
                    <td><?php echo $price_txt ?></td>
                    <td class="m-f-r"><?php echo date('Y.m.d',$pay->timemodified); ?></td>
                    <td>-
                        <?php // echo $pay->paytype==1 ? get_string('credit','local_mypage') : get_string('cash','local_mypage'); ?>
                    </td>
                    <td class="m-col3 m-clear"><?php echo $deliverytext ?></td>
                    <!--<td class="m-col3"><a href="#" class="btns br">인쇄</a></td>-->
                </tr>
                <?php
                $paynum--;
            }
        } else {
            ?> <tr><td colspan="7"><?php echo get_string('nodata:pay','local_mypage');?></td></tr> <?php
        }
        ?> 
    </tbody>
</table>
<?php require ($CFG->dirroot . '/local/course/recommendpage.php');?>
<?php
echo $OUTPUT->footer();
?>


<!--<script>
$(document).ready(function () {
      $(".refund").click(function () {
           if (confirm('환불 신청')) {
                    
                }
    });
});
</script>-->