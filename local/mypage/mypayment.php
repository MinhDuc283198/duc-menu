<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('결제내역');
$PAGE->set_url(new moodle_url('/local/mypage/mypayment.php'));
echo $OUTPUT->header();

$type = optional_param('type', 0, PARAM_INT); // 강좌인지 교재인지 
$type2 = optional_param('type2', 0, PARAM_INT); // 값있으면 강좌+교재 출력 
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);

$paycount = local_mypage_payment($type,$USER->id,$offset,$perpage,$type2,true);
$total_pages = jinoboard_get_total_pages($paycount, $perpage);
$offset = 0;
if ($page != 0) {
    $offset = ($page - 1) * $perpage;
}
$num = $paycount - $offset;
$pays =local_mypage_payment($type,$USER->id,$offset,$perpage,$type2,false);
?>  
<h2 class="pg-tit"><?php echo get_string('paymentlist','local_mypage');?></h2>
<div class="tp-tb-area text-right">
    <ul class="bar-tab">
        <li <?php if($type==0){ echo 'class="on"';}?>><a href="/local/mypage/mypayment.php"><?php echo get_string('all_list','local_jinoboard');?></a></li>
        <li <?php if($type==1&&$type2==0){ echo 'class="on"';}?>><a href="/local/mypage/mypayment.php?type=1"><?php echo get_string('course','local_mypage');?></a></li>
        <li <?php if($type==2){ echo 'class="on"';}?>><a href="/local/mypage/mypayment.php?type=2"><?php echo get_string('book','local_mypage');?></a></li>
        <li <?php if($type2==1){ echo 'class="on"';}?>><a href="/local/mypage/mypayment.php?type=1&type2=1"><?php echo get_string('course', 'local_management') .'+' .get_string('book', 'local_management');?></a></li>
    </ul>
</div>
<table class="table m-block">
    <thead>
        <tr>
            <th width="6%"><?php echo get_string('num','local_jinoboard');?></th>
            <th width="10%"><?php echo get_string('goods','local_mypage');?></th>
            <th><?php echo get_string('goods:buy','local_mypage');?></th>
            <th width="11%"><?php echo get_string('goods:pay','local_mypage');?></th>
            <th width="10%"><?php echo get_string('goods:date','local_mypage');?></th>
            <th width="10%"><?php echo get_string('goods:way','local_mypage');?></th>
            <th width="9%"><?php echo get_string('post:state','local_mypage');?></th>
            <th width="9%"><?php echo get_string('remark','local_management');?></th>
            <!--<th width="60px">영수증</th>-->
        </tr>
    </thead>
    <tbody>
      <?php
        if($pays){
            foreach($pays as $pay){
                  switch ($pay->status){
                    case 1 :
                        $string = get_string('beforepayment','local_mypage');
                        break;
                    case 2 :
                        $str =get_string('refund','local_mypage');
                        $string = '<a href="/local/mypage/refund.php" class="btns br_blue refund">'.$str.'</a></td>';
                        break;
                    case 3 :
                        $str =get_string('cantrefund','local_mypage');
                        $string = '<span class="t-red">'.$str.'</span>';
                        break;
                    case 4 :
                        $str =get_string('wait','local_mypage');
                        $string = '<span class="t-black">'.$str.'</span>';
                        break;
                    case 5 :
                        $str =get_string('sucrefund','local_mypage');
                        $string = '<span class="t-blue02">'.$str.'</span>';
                        break;
                    default : $string ="-";
                }
                if($pay->ctype != 1 ){
                    if($pay->delivery == 0){
                        $deliverytext = get_string("post:pre",'local_mypage');
                    }else if ($pay->delivery == 2){
                        $deliverytext = get_string("post:ing",'local_mypage');
                    }else{
                        $deliverytext = get_string("post:end",'local_mypage');
                    }
                }else{
                    $deliverytext = "-";
                }
                
                if($pay->ctype == 1){
                    $type_txt = get_string('course', 'local_management');
                }else if($pay->ctype == 3){
                    $type_txt = get_string('course', 'local_management') .'+' .get_string('book', 'local_management');
                }else{
                    $type_txt = get_string('book', 'local_management');
                }
                $price_txt = number_format($pay->price).' VND';
                if(($pay->forced == 1)){
                    $price_txt = get_string('free', 'local_management');
                }
                ?>
                <tr>
                    <td class="m-hide"><?php echo $num;?></td>
                    <td class="m-hide"><?php echo $type_txt; ?></td>
                    <td class="text-left"><a herf="#" onclick="payment('<?php echo $pay->id ?>')"><span class="m-show inline">[<?php echo $pay->type==1 ? get_string('course','local_mypage') : get_string('book','local_mypage'); ?>]</span><?php echo $pay->title; ?></a></td>
                    <td><?php echo $price_txt ?></td>
                    <td class="m-f-r"><?php echo date('Y.m.d',$pay->timemodified); ?></td>
                    <td>-
                        <?php // echo $pay->paytype==1 ? get_string('credit','local_mypage') : get_string('cash','local_mypage'); ?>
                    </td>
                    <td class="m-col3 m-clear"><?php echo $deliverytext ?></td>
                    <td><a herf="#" onclick="payment('<?php echo $pay->id ?>')"><?php echo ($pay->lrid && $pay->refundtype == 1) ?  get_string('refundcon','local_management').date("Y.m.d", $pay->completedate)  : '-'?></a></td>
                    <!--<td><a href="#" class="btns br">인쇄</a></td>-->
                </tr>
                <?php $num--;
            }
        } else {
            ?> <tr><td colspan="7"><?php echo get_string('nodata:pay','local_mypage');?></td></tr> <?php
        }
        ?> 
    </tbody>
</table>
<span class="tb-info">※<?php echo get_string('getrefund','local_mypage');?></span>

<?php
$page_params = array();
$page_params['type'] = $type;
$page_params['type2'] = $type2;
$page_params['perpage'] = $perpage;
jinoboard_get_paging_bar($CFG->wwwroot . "/local/mypage/mypayment.php", $page_params, $total_pages, $page);

echo $OUTPUT->footer();
?>
<script type="text/javascript">
    //배송정보입력 팝업 
    function payment(id){
        if (not_login()) {
            utils.popup.call_layerpop("/local/mypage/pop_refund.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"id": id});
            return false;
        }
    }
</script>
