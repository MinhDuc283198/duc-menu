<?php
/**
 * 수강신청 정보 수정 폼
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$id = optional_param('id', 0, PARAM_INT);

$data = $DB->get_record('lmsdata_course_applications', array('id'=>$id));

$c_year = date('Y');
$c_month = date('n');
$c_day = date('j');

$birthyear = substr($data->birthday, 0, 4);
$birthmonth = substr($data->birthday, 4, 2);
$birthday = substr($data->birthday, 6, 2);

$phone_first_number = substr($data->phone, 0, 3);
$phone_middle_number = substr($data->phone, 3, 4);
$phone_last_number = substr($data->phone, 7, 4);

?>

<input type="hidden" value="<?php echo $data->id;?>" name="id">

<table class="table table-condensed">
    <tbody>
        <tr>
            <th><?php echo get_string('school:classify', 'local_course_application');?></th>
            <td class="text-left">
                   <select name="schooltype" id="schooltype" title=<?php get_string("jobdictionarycode",'local_mypage') ?>>
                        <option value="elem" <?php echo $data->schooltype == 'elem' ? 'selected' : ''; ?> ><?php get_string('elementaryschool','local_mypage')?></option>
                        <option value="midd" <?php echo $data->schooltype == 'midd' ? 'selected' : ''; ?> ><?php get_string('middleschool','local_mypage')?></option>
                        <option value="high" <?php echo $data->schooltype == 'high' ? 'selected' : ''; ?> ><?php get_string('highschool','local_mypage')?></option>
                    </select>
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('affiliation', 'local_course_application');?></th>
            <td class="text-left">
                <input type="text" name="school" m-width="80" value="<?php echo $data->school; ?>" title="<?php echo get_string('affiliation', 'local_course_application');?>" readonly/>
                <input type="button" class="btn gray" name="schoolselect" title="<?php echo get_string('selectschool', 'local_course_application');?>" value="<?php echo get_string('selectschool', 'local_course_application');?>" />
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('school:year', 'local_course_application');?></th>
            <td class="text-left">
               <select class="classlevel" id="classlevel" name="classlevel">
                    <?php
                    for($i=1; $i <= 6; $i++){
                        $selected = '';
                        if($i == $data->classlevel) {
                            $selected = 'selected';
                        }
                        echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';  
                    }
                    ?>
                </select>
                <label for="classlevel"><?php echo get_string('classlevel', 'local_course_application');?></label>
                <select class="classgroup" id="classgroup" name="classgroup">
                    <?php
                    for($i=1; $i <= 15; $i++){
                        $selected = '';
                        if($i == $data->classgroup) {
                            $selected = 'selected';
                        }
                        echo '<option value="'.$i.'" '.$selected.' >'.$i.'</option>';  
                    }
                    ?>
                </select>
                <label for="classgroup"><?php echo get_string('classgroup', 'local_course_application');?></label>
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('dateofbirth', 'local_course_application');?></th>
            <td class="text-left">
                <select class="chyear" id="year" name="year">
                    <?php
                    for($i=$c_year; $i >= 1900; $i--){
                        if($i == (int)$birthyear){
                            $select = 'selected';
                        } else {
                            $select = ' ';
                        }
                        echo '<option value="'.$i.'" '.$select.'>'.$i.'</option>';  
                    }
                    ?>
                </select>
                <label for="year"><?php echo get_string('year', 'local_course_application');?></label>
                <select class="chmonth" id="month" name="month">
                    <?php
                    for($i=1; $i <= 12; $i++){
                        if($i < 10){
                            $value = '0'.$i;
                        } else {
                            $value = $i;
                        }
                        if($i == (int)$birthmonth){
                            $select = 'selected';
                        } else {
                            $select = ' ';
                        }
                        echo '<option value="'.$value.'" '.$select.'>'.$i.'</option>';  
                    }
                    ?>
                </select>
                <label for="month"><?php echo get_string('month', 'local_course_application');?></label>
                <select class="chday" id="day" name="day">
                </select>
                <label for="day"><?php echo get_string('day', 'local_course_application');?></label>
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('phonenumber', 'local_course_application');?></th>
            <td class="text-left">
                <select class="number" name="firstnumber" id="number" title="number">
                    <option value="010" <?php if($phone_first_number == '010') echo "selected";?>>010</option>
                    <option value="011" <?php if($phone_first_number == '011') echo "selected";?>>011</option>
                    <option value="016" <?php if($phone_first_number == '016') echo "selected";?>>016</option>
                    <option value="017" <?php if($phone_first_number == '017') echo "selected";?>>017</option>
                    <option value="019" <?php if($phone_first_number == '019') echo "selected";?>>019</option>
                </select>
                <input type="text" name="number[]" title="number02" value="<?php echo $phone_middle_number;?>" />
                <input type="text" name="number[]" title="number03" value="<?php echo $phone_last_number;?>" />
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('email', 'local_course_application');?></th>
            <td class="text-left">
                <input type="email" name="email" m-width="80" value="<?php echo $USER->email;?>" title="<?php echo get_string('email', 'local_course_application');?>" />
            </td>
        </tr>
    </tbody>
</table>

<div class="table_btn">
    <input type="submit" value=<?php get_srting("change",'local_mypage') ?> class="btn orange big" />
    <input type="button" value=<?php get_srting("list",'local_mypage') ?> class="btn gray big" onclick="location.href='mycourse.php';"/>
</div>

<script>
    $(document).ready(function(){
        daylist('<?php echo $birthday;?>');
    });
    
    $(".chmonth").change(function () {
        daylist('0');
    });
    $(".chyear").change(function () {
        daylist('0');
    });
    
    /**
     * 생년월일 일 수 셋팅
     * @param {type} birthday
     * @returns {undefined}
     */
    function daylist(birthday) {
        try {
            var year = $(".chyear").val();
            var month = $(".chmonth").val();
            var lastDay = find_lastDay(year, month);
            $(".chday").html("");
            for (i = 0; i < lastDay; i++) {
                var day = i + 1;
                if (day < 10) {
                    day = "0" + day;
                }
                if(birthday != '0'){
                    var selected = '';
                    if(birthday == day){
                        selected = 'selected';
                    } else {
                        selected = '';
                    }
                }
                $(".chday").append("<option value=" + day + " "+selected+">" + day + "</option>");
            }
        } catch (e) {

        }
    }
    
    /**
     * 학교검색 팝업 띄우기 함수
     * @type type
     */
    $('input[name=schoolselect]').click(function () { 
        var host = $(location).attr('host');
        if(host.indexOf('eduhope.net') != -1){
            call_layerPop("/local/mypage/schoolsearch.php", "700px", "550px", {});
        } else {
            alert(get_string('silserver','local_mypage'));
        }
    });
</script>