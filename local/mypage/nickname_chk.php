<?php
/**
 * 닉네임 중복체크 페이지(AJAX)
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$nickname = required_param('nickname', PARAM_INT);

$returnvalue = new stdClass();

$output = eduhope_nickname_chk('ykpark');

$returnvalue->status = 'fail';

if($output['dataResult'] == '00' && $output['taskResult'] == '00'){
    if($output['returnData'][0]['duplYn'] == 'N'){
        $returnvalue->status = 'success';
    }
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);