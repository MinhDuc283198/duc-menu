<?php
/**
 * 결제 취소신청 팝업
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

// lmsdata_class->id
$classid = optional_param('id', 0, PARAM_INT);
$menu_gubun = optional_param('menu_gubun', 0, PARAM_INT);

$sql = 'SELECT lca.*, lc.classnum, lc.classyear, lco.coursename, lco.memberdiscount, lco.groupdiscount
        FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.courseid = lca.courseid
        JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
        WHERE lca.userid = :userid AND lc.id = :id';

$payinfo = $DB->get_record_sql($sql, array('userid'=>$USER->id, 'id'=>$classid));

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
?>  
<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<link href="/theme/oklassedu/style/media.css" rel="stylesheet" />
<script type="text/javascript" src="/lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js"></script>
<script src="/theme/oklassedu/javascript/theme.js" type="text/javascript"></script>
<div id="popwrap">
    <div class="pop_title">
        <?php get_string('cancelap','local_mypage') ?>
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_pop_close.png" alt="" />
    </div>

    <div id="pop_content">
        <form id="cancel_form" action="<?php echo $CFG->wwwroot . '/local/mypage/payment_cancel_submit.php' ;?>">
            <input type="hidden" name="classid" value="<?php echo $classid;?>">
            <input type="hidden" name="menu_gubun" value="<?php echo $menu_gubun;?>">
            <table class="table">
                <tbody>
                <input type="hidden" name="title" value="autotitle" />
                <input type="hidden" name="reason" value="autoreason" />
                    <tr>
                        <td colspan="2" class="text-bold text-left"><?php echo $payinfo->coursename.' / '.$payinfo->classyear.get_string('cer:year','local_mypage').$payinfo->classnum.get_string('cer:classnum2','local_mypage');?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><?php get_string('realcancel','local_mypage') ?></td>
                    </tr>
                </tbody>
            </table>
        
            <div class="table_btn">
                <input type="submit" value="<?php get_string('signupcancel','local_mypage') ?>" class="btn orange big" />
            </div>
        </form>
    </div>
</div>