
function message_delete(messageid,timeread,userid,msg){
     if (confirm(msg)) {
        $.ajax({
            url: "notification_ajax.php",
            type: 'POST',
            dataType: 'json',
            data: {id: messageid, timeread : timeread,userid:userid
                },
            success: function (data) {
                if(messageid>0){
                    $('.noti'+messageid).hide();
                } else {
                     window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            }
        });    
    }
}
function message_read(messageid,userid){

    $.ajax({
        url: "notification_read_ajax.php",
        type: 'POST',
        dataType: 'json',
        data: {id: messageid, userid:userid
            },
        success: function (data) {
            if(messageid>0){
                $('.noti'+messageid).addClass('read');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);
        }
    });    

}

$(document).ready(function(){
//    $('.al-close').click(function(){
//       if (confirm("알림을 삭제하시겠습니까?")) {
//            
//        }
//    });
});