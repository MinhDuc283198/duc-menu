<?php
/**
 * 프로필 수정 submit
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/lib/gdlib.php');
require_once($CFG->dirroot.'/lib/filelib.php');
require_once($CFG->libdir . '/filestorage/file_storage.php');
require_once($CFG->libdir . '/filestorage/stored_file.php');

$picturedelete = optional_param('picturedelete', 0, PARAM_INT);

$filename = $_FILES['ufile']['name'];
$filepath = $_FILES['ufile']['tmp_name'];

$coursecontext = context_system::instance();   // SYSTEM context.

$usercontext = context_user::instance($USER->id);

$user = $DB->get_record('user', array('id'=>$USER->id));

$fs = get_file_storage();

if($picturedelete == 1){
    $fs->delete_area_files($usercontext->id, 'user', 'newicon');
    $newpicture = 0;
    $DB->set_field('user', 'picture', $newpicture, array('id' => $user->id));
}

if(!empty($_FILES['ufile']['tmp_name'])){
    
    $draftitemid = file_get_unused_draft_itemid();
    
    $filerecord = array(
        'contextid' => $usercontext->id,
        'component' => 'user',
        'filearea'  => 'draft',
        'itemid'    => $draftitemid,
        'filepath'  => '/',
        'filename'  => $filename,
        'userid' => $USER->id,
        'license' => 'allrightsreserved'
    );    
    
    $file = $fs->create_file_from_pathname($filerecord, $filepath);

    if ($iconfile = $file->copy_content_to_temp()) {
        // There is a new image that has been uploaded.
        // Process the new image and set the user to make use of it.
        // NOTE: Uploaded images always take over Gravatar.
        $newpicture = (int)process_new_icon($usercontext, 'user', 'icon', 0, $iconfile);
        // Delete temporary file.
        @unlink($iconfile);
        // Remove uploaded file.
        $fs->delete_area_files($usercontext->id, 'user', 'newicon');
    } else {
        // Something went wrong while creating temp file.
        // Remove uploaded file.
        $fs->delete_area_files($usercontext->id, 'user', 'newicon');
        return false;
    }

    if ($newpicture != $user->picture) {
        $DB->set_field('user', 'picture', $newpicture, array('id' => $user->id));
    }
}

redirect($CFG->wwwroot.'/local/mypage/mypage.php');