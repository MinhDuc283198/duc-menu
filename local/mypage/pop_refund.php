<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');

$context = context_system::instance();
$id = optional_param('id', 0, PARAM_INT);

$content = local_mypage_payment_id($id);
$goodstype = $DB->get_field('lmsdata_class', 'type', array('id' => $content->lcid));
$delivery_txt = "";
switch ($content->delivery) {
    case 0 :
        $delivery_txt = get_string('delivery:preparing','local_management');
        break;
    case 1:
        $delivery_txt = get_string('delivery:completed','local_management');
        break;
    case 2:
        $delivery_txt = get_string('delivery:shipping','local_management');
        break;
}
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('결제');

echo $OUTPUT->header();
?>  
<div class="layerpop">
    <?php ?>
    <div class="pop-title">
        결제 상세 내역
        <a href="#" class="pop-close"><?php echo get_string('close','local_management')?></a>
    </div>
    <div class="pop-contents">
        <h5 class="pg-tit"><?php echo get_string('paymentinfo','local_management')?></h5>
        <div class="mg-bt20">
            <table class="table detail">
                <tr>
                    <th width="30%"><?php echo get_string('ordernumber','local_management')?></th>
                    <td><?php echo $content->paycode ?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('goodsname','local_management')?></th>
                    <td><?php echo $content->title ?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('amount','local_management')?></th>
                    <td><?php echo number_format($content->price).' VND'; ?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('order:date','local_management')?></th>
                    <td><?php echo date("Y.m.d H:i", $content->timecreated)?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('paymentdate','local_management')?><br>(<?php echo get_string('paydatecon','local_management')?>)</th>
                    <td><?php echo ($content->status == 1) ? date("Y.m.d H:i",$content->timemodified ) : '-'?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('paymentmethod','local_management')?></th>
                    <td><?php echo $content->paymethod?></td>
                </tr>
            </table>
        </div>

        <?php if ($goodstype != 1) { ?>
        <h5 class="pg-tit hasbtn dinfo"><?php echo get_string('deleveryinfo', 'local_payment'); ?></h5>
        
        <div class="mg-bt20">
            <table class="table detail">
                <tr>
                    <th width="30%"><?php echo get_string('phone', 'local_payment'); ?></th>
                    <td><?php echo $content->cell ?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('address', 'local_payment'); ?></th>
                    <td><?php echo $content->address ?></td>
                </tr>
                <tr>
                    <th>배송 상태</th>
                    <td><?php echo $delivery_txt?></td>
                </tr>
            </table>
        </div>

        <?php } ?>
        <?php if ($content->lrid > 0) { ?>
        <h5 class="pg-tit hasbtn dinfo"><?php echo get_string('refundinfo','local_management')?></h5>
        
        <div class="mg-bt20">
            <table class="table detail">
                <tr>
                    <th width="30%"><?php echo get_string('refundmethod','local_management')?></th>
                    <td><?php echo ($content->lrtype == 0) ? get_string('approval','local_management') : get_string('bankbook', 'local_management') ?></td>
                </tr>
                <tr>
                    <th><?php echo  get_string('refunddate', 'local_management') ?></th>
                    <td><?php echo date("Y.m.d H:i",$content->completedate) ?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('refundamount','local_management')?></th>
                    <td><?php echo number_format($content->lrprice).' VND'; ?></td>
                </tr>
                <tr>
                    <th><?php echo get_string('refundaccount','local_management')?></th>
                    <td><?php echo $content->bank?> | <?php echo $content->refundbanknr?></td>
                </tr>
            </table>
        </div>

        <?php } ?>

    </div>
    <div class="btn-area text-center">
        <input type="button" value="확인" class="btns point w100 pop-close" />
    </div>
    <script>
        $(document).ready(function () {
            var phone = '<?php echo $userdata->phone1 ? $userdata->phone1 :''?>';
                    var zipcode = '<?php echo $userdata->zipcode ? $userdata->zipcode :  ''?>';
                    var address = '<?php echo $userdata->address ? $userdata->address : '' ?>';
                    var appendtext = "<span class='t-blue t-small savetext'>ᆞ<?php echo get_string('changeinfo','local_payment') ?>.</span>";
                    $('.uinfo').keyup(function(){
                        if (phone == $('input[name=phone]').val() && address == $('input[name=address]').val() && zipcode == $('input[name=zipcode]').val()) {
                            $('.savetext').remove();
                        } else {
                            if (!$('.dinfo').children('.savetext').length) {
                                $('.dinfo').append(appendtext);
                            }
                        }
                    }
            );
        });
        function sava_userinfo(){
            var phone = $('input[name=phone]').val();
            var zipcode = $('input[name=zipcode]').val();
            var address = $('input[name=address]').val();
                $.ajax({
                    type: 'POST',
                    url: '/local/payment/save_address.php',
                    dataType: 'JSON',
                    data: {phone: phone, zipcode: zipcode,address: address},
                    success: function() {
                        alert('<?php echo get_string('savesuccess','local_payment') ?>');
                    },
                    error: function(xhr, status, error) {
                    }
                });
            
        }
        function payment_progress(){
        <?php if($list->price == 0) { ?>
            var paytype = 'free';
        <?php }else{ ?> 
            var paytype = $("input[name=paytype]:checked").val();
        <?php } ?>
            
            var form =  document.createElement("form");
            var url = "/local/payment/init_payment.php";
            form.setAttribute("charset", "UTF-8");
            form.setAttribute("method", "Post");
            form.setAttribute("action", url); 
            form.setAttribute("target", "payform"); 
            var input1 = document.createElement("input");
            input1.setAttribute("type", "hidden");
            input1.setAttribute("name", "id");
            input1.setAttribute("value", '<?php echo $id ?>');
            form.appendChild(input1);
            <?php if ($goodstype != 1){ ?> 
                var address = $('input[name=address]').val();
                var phone =   $('input[name=phone]').val();
                var zipcode = $('input[name=zipcode]').val();
                console.log(zipcode);
                console.log(phone);
                console.log(address);
                if(address && phone && zipcode ){
                    var input2 = document.createElement("input");
                    input2.setAttribute("type", "hidden");
                    input2.setAttribute("name", "address");
                    input2.setAttribute("value", address);
                    form.appendChild(input2);
                    var input3 = document.createElement("input");
                    input3.setAttribute("type", "hidden");
                    input3.setAttribute("name", "phone");
                    input3.setAttribute("value", phone);
                    form.appendChild(input3);
                    var input4 = document.createElement("input");
                    input4.setAttribute("type", "hidden");
                    input4.setAttribute("name", "zipcode");
                    input4.setAttribute("value", zipcode);
                    form.appendChild(input4);
                }else{
                     alert("<?php echo get_string('enterinfo', 'local_payment', get_string('deleveryinfo','local_payment'))?> ")
                     return false;
                }
            <?php  } ?>
            if(paytype){
                var input4 = document.createElement("input");
                input4.setAttribute("type", "hidden");
                input4.setAttribute("name", "paytype");
                input4.setAttribute("value", paytype);
                form.appendChild(input4);
                document.body.appendChild(form);
            }else{
                alert("<?php echo get_string('enterinfo', 'local_payment', get_string('selectpaytype','local_payment'))?> ")
                return false;
            }
                window.open("","payform","width=759 ,height=847,scrollorbars=yes,resizable=no");  
                form.submit();
        }
    </script>
</div>


<?php
echo $OUTPUT->footer();
?>


