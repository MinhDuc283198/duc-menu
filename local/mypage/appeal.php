<?php
/**
 * 성적 이의제기 신청 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/course/report/statistics/lib.php';
require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->libdir/excellib.class.php");

$id = required_param('id', PARAM_INT); // course id

require_login();


$context = context_course::instance($id);
$PAGE->set_context($context);

$course = get_course($id);

$PAGE->set_url('/local/mypage/appeal.php?id=' . $id);
$PAGE->set_course($course);
$PAGE->set_pagelayout('course');

$PAGE->navbar->add(get_string('coursenformation','local_mypage'));
$PAGE->navbar->add(get_string('counternotification','local_mypage'));
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$PAGE->requires->jquery();

echo $OUTPUT->header();

?>  
<h3 class="page_title"><?php get_string('counternotification','local_mypage') ?></h3>

<form name="submit_form" action="appeal.submit.php" id="id_submit_form" method="post">
<table class="table">
    <tbody>
    <input type="text" name="courseid" value="<?php echo $COURSE->id ?>">
    <input type="text" name="userid" value="<?php echo $USER->id ?>">
        <tr>
            <th><?php echo get_string('title','local_mypage');?></th>
            <td class="text-left">
                <input type="text" name="title"  class="w_160" >
            </td>
        </tr>
        <tr>
            <th><?php echo get_string('objectioncontent','local_mypage');?></th>
            <td class="text-left">
                <textarea cols="90" rows="10" name="contents"></textarea>
            </td>
        </tr>
    </tbody>
</table>



<div class="table_btn">
    <input type="submit" value="<?php echo get_string('apply','local_mypage');?>" class="btn orange big" onclick = "return update_delay_apply();"/>
</div>
</form>
<script>
    /**
     * 이의신청 폼체크 함수
     * @returns {Boolean}
     */
    function update_delay_apply(){
        
        var frm = $('#id_submit_form');
        if($.trim(frm.find('input[name=title]').val()) == ''){
            alert('<?php echo get_string('titleplsenter.','local_mypage');?>');
            return false;
        }
        if($.trim(frm.find('textarea[name=contents]').val())  == ''){
            alert('<?php echo get_string('objectioncontenter','local_mypage');?>');
            return false;
        }
        
         if(!confirm('<?php echo get_string('confirm:delaycourse','local_mypage');?>')){
            return false;
        }
       
    }
</script>
<?php
echo $OUTPUT->footer();
?>

