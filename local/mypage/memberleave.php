<?php
/**
 * 회원 탈퇴 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();

require_login();

$memberleave = optional_param('memberleave', '', PARAM_RAW);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("withdrawal",'local_mypage'));
$PAGE->navbar->add(get_string("withdrawal",'local_mypage'));

echo $OUTPUT->header();

if($memberleave == 'memberleave'){
    $userinfo = $DB->get_record('lmsdata_user',array('userid'=>$USER->id));
    
    $output = eduhope_member_leave($userinfo->userno);
    
    $delete = new stdClass();
    $delete->id = $userinfo->userid;
    $delete->deleted = 1;
    
    $DB->update_record('user', $delete);
    
    $logout_url = new moodle_url('/login/logout.php', array('sesskey' => sesskey()));
    echo '<script type="text/javascript">alert("'.get_string('withdrawalok','local_mypage');'")</script>';
    redirect($logout_url);
}
?>  
<h3 class="page_title"><?php get_string("withdrawal",'local_mypage') ?></h3>
<div class="textbox">
    <p><?php get_string("hanyangsoft",'local_mypage') ?></p> 
</div>
<form id="leave_form">
    <input type="hidden" value="memberleave" name="memberleave" />
    <div class="text-center">
        <input type="submit" value=<?php get_string("leaving",'local_mypage') ?> class="btn orange big"/> 
    </div>
</form>

<script>
    /**
     * 탈퇴여부 재확인 함수
     */
    $('#leave_form').submit(function(){
        if(!confirm(get_string('realleaving','local_mypage'))){
            return false;
        }
    })
</script>

<?php
echo $OUTPUT->footer();
?>