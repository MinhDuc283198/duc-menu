<?php

/**
 * 결제 취소신청 submit페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require(dirname(dirname(dirname(__FILE__))) . '/chamktu/lib.php');
//require(dirname(dirname(dirname(__FILE__))) . '/enrol/locallib.php');

require_login();

$classid = optional_param('classid', 0, PARAM_INT);
$title = optional_param('title', 0, PARAM_RAW);
$reson = optional_param('reason', 0, PARAM_TEXT);
$menu_gubun = optional_param('menu_gubun', 0, PARAM_INT);
//$bank = optional_param('bank', 0, PARAM_RAW);
//$count = optional_param('count', 0, PARAM_RAW);
//$name = optional_param('name', 0, PARAM_RAW);

$sql = 'SELECT lca.*, lc.classnum, lc.classyear, lco.coursename, lc.occasional, lco.memberdiscount, lco.groupdiscount
        FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.courseid = lca.courseid
        JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
        WHERE lca.userid = :userid AND lc.id = :id';

$payinfo = $DB->get_record_sql($sql, array('userid' => $USER->id, 'id' => $classid));

$lmsdata = $DB->get_record('lmsdata_course_applications', array('id' => $payinfo->id)); //106

$lmsdata->status = 'cancel';
$lmsdata->paymentstatus = 'cancel';
$lmsdata->completionstatus = 0;
$lmsdata->timemodified = time();

$updateresult = $DB->update_record('lmsdata_course_applications', $lmsdata);
if ($updateresult) {

    $new = new stdClass();

    $new->userid = $USER->id; // 6
    $new->courseid = $payinfo->courseid; // 187
    $new->applicationid = $payinfo->id; // 106
    $new->title = $title;
    $new->reason = $reson;
//$new->bank = $bank;
//$new->account = $count;
//$new->accountname = $name;
    $new->refund = $payinfo->price; // 0
    $new->status = 'complete';
    $new->timecreated = time();
    $new->timemodified = time();

    $cancel = $DB->insert_record('lmsdata_applications_cancel', $new);
    $role = $DB->get_record('role', array('shortname' => 'student'), 'id, shortname');
    $data = $DB->get_record('course', array('id' => $payinfo->courseid)); // 187
    set_unassign_user($data, $payinfo->userid, $role->id); // 6
}

if (!empty($cancel) && !empty($updateresult)) {
//    연수이력 수정
    if ($menu_gubun) {
        echo '<script type="text/javascript">alert("'.get_string('cancel','local_mypage');'"); location.href="' . $CFG->wwwroot . '/local/course_application/course_list.php?menu_gubun=' . $menu_gubun . '";</script>';
    } else {
        echo '<script type="text/javascript">alert("'.get_string('cancel','local_mypage');'"); location.href="' . $CFG->wwwroot . '";</script>';
    }
} else {
    if ($menu_gubun) {
        echo '<script type="text/javascript">alert("'.get_string('cancelhappen','local_mypage');'"); location.href="' . $CFG->wwwroot . '/localcourse_application/course_list.php?menu_gubun=' . $menu_gubun . '";</script>';
    } else {
        echo '<script type="text/javascript">alert("'.get_string('cancelhappen','local_mypage');'"); location.href="' . $CFG->wwwroot . '";</script>';
    }
}
