<?php

/**
 * Visang
 *
 * @package    Hoang.N <nvhoangag@gmail.com>
 * @copyright  2019 Jinotech
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/job/lib/employers.php');


$view = optional_param('view', '', PARAM_RAW);

$context = context_system::instance();
//mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
$context = context_system::instance();
//mkjobs_require_login();
$PAGE->set_context($context);
// =====================================================================================================
// handles
if(!$DB->get_field('lmsdata_user','statuscode',array('userid'=>$USER->id))){
    redirect('/local/mypage/mypage.php');
}

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));

echo $OUTPUT->header();
?>
<link href="/local/job/assets/dist/css/app.css" rel="stylesheet" />
<script src="/local/job/assets/dist/js/app.js" type="text/javascript"></script>
<div class="cont">
    <div class="">
        <div class="">
            <div role="main">
                <h2 class="pg-tit"><?php echo get_string('Interestinformation', 'local_job'); ?></h2>
                <ul class="mk-c-tab tab-event">
                    <li class="w-30"><a href="<?php echo $CFG->wwwroot . '/local/mypage/inter_jobs.php'; ?>"><?php echo get_string('LikeJob', 'local_job'); ?></a></li>
                    <li class="w-30 on"><a href="<?php echo $CFG->wwwroot . '/local/mypage/inter_com.php'; ?>"><?php echo get_string('companyInInterested', 'local_job'); ?></a></li>
                    <li class="w-30"><a href="<?php echo $CFG->wwwroot . '/local/mypage/inter.php'; ?>"><?php echo get_string('InterestInfo', 'local_job'); ?></a></li>
                </ul>

                <?php
                $employers = mkjobs_get_follow_employers($USER->id);
                ?>
                <?php if (count($employers['data']) < 1) { ?>
                    <p class="t-gray mb-4">※ <?php echo get_string('companyInInterestedNone', 'local_job'); ?></p>
                <?php } ?>

                <?php if ($employers['total']) : ?>
                    <div id="employersGroup">
                        <div class="row mx-0 my-3">
                            <?php foreach ($employers['data'] as $employer) : ?>
                                <div class="col-sm-6 col-md-4 col-lg-3 px-2 mb-3">
                                    <div class="compa-card border">
                                        <div class="compa-card-img position-relative">
                                            <a class="d-flex justify-content-center h-100" href="<?php echo new moodle_url($CFG->wwwroot . '/local/job/companies/view.php', array('employerid' => $employer->id)); ?>">
                                                <img class="align-self-center contain-image" src="<?php echo $employer->company_logo; ?>">
                                            </a>
                                            <div class="position-absolute top-0 right-0 p-2">
                                                <i class="cursor-pointer ic ic-heart-on" onclick="followEmployer(this, <?php echo $employer->id; ?>)"></i>
                                            </div>
                                        </div>
                                        <div class="border-top p-jbox">
                                            <div class="tit-jbox text-truncate text-truncate-2 mb-2">
                                                <a href="<?php echo new moodle_url($CFG->wwwroot . '/local/job/companies/view.php', array('employerid' => $employer->id)); ?>"><?php echo $employer->company_name; ?></a>
                                            </div>
                                            <p class="t-gray text-truncate text-truncate-2 mb-3"><?php echo $employer->company_short_description; ?></p>
                                            <div class="d-flex justify-content-between">
                                                <div><?php echo $employer->location ? $employer->location->name : ''; ?></div>

                                                <?php if ($employer->job_cnt) : ?>
                                                    <a href="<?php echo new moodle_url($CFG->wwwroot . '/local/job/companies/view.php', array('employerid' => $employer->id, 'view' => 'our-jobs')); ?>" class="text-primary">
                                                        <span><?php echo $employer->job_cnt; ?><?php echo get_string('Jobview', 'local_job'); ?></span>
                                                        <span class="ic ic-arrow-right-on"></span>
                                                    </a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div v-for="(item, idx) in items" v-cloak class="col-sm-6 col-md-4 col-lg-3 px-2 mb-3">
                                <div class="compa-card border">
                                    <div class="compa-card-img position-relative">
                                        <a class="d-flex justify-content-center h-100" :href="'<?php echo $CFG->wwwroot . '/local/job/companies/view.php?employerid='; ?>' + item.id">
                                            <img class="align-self-center contain-image" :src="item.company_logo">
                                        </a>
                                        <div class="position-absolute top-0 right-0 p-2">
                                            <i :class="['cursor-pointer ic', item.has_follow_employer==1 ? 'ic-heart-on':'ic-heart']" @click="followEmployer(idx)"></i>
                                        </div>
                                    </div>
                                    <div class="border-top p-jbox">
                                        <div class="tit-jbox text-truncate text-truncate-2 mb-2">
                                            <a :href="'<?php echo $CFG->wwwroot . '/local/job/companies/view.php?employerid='; ?>' + item.id">{{ item.company_name }}</a>
                                        </div>
                                        <p class="t-gray text-truncate text-truncate-2 mb-3"><a :href="'<?php echo $CFG->wwwroot . '/local/job/companies/view.php?employerid='; ?>' + item.id">{{ item.company_short_description }}</a></p>
                                        <div class="d-flex justify-content-between">
                                            <div>{{ item.location.name }}</div>
                                            <a :href="'<?php echo $CFG->wwwroot . '/local/job/companies/view.php?employerid='; ?>' + item.id + '&view=our-jobs'" class="text-primary" v-if="item.job_cnt">
                                                <span>{{ item.job_cnt }} <?php echo get_string('Jobview', 'local_job'); ?></span>
                                                <span class="ic ic-arrow-right-on"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php if ($employers['total'] > 20) :  ?>
                            <div :class="['px-2 my-3', loadmore ? '':'d-none']">
                                <button class="btn btn-block border btn-lg rounded-0 btn-spinner" :disabled="onfetch" @click="fetch">
                                    <span><?php echo get_string('ViewMore', 'local_job'); ?></span>
                                    <span class="btn-lbl ic ic-arrow-down"></span>
                                    <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>

                    <script>
                        $(function() {
                            $("body").addClass('mk-jobs jobs-site');
                            new Vue({
                                el: "#employersGroup",
                                mixins: [mkjobs_search_mixin()],
                                data: {
                                    action: "mkjobs_get_follow_employers",
                                    sesskey: "<?php echo sesskey(); ?>",
                                    perpage: 20,
                                },
                                methods: {
                                    followEmployer: function(idx) {
                                        $.post("<?php echo $CFG->wwwroot . '/local/job/ajax.php'; ?>", {
                                            sesskey: "<?php echo sesskey(); ?>",
                                            action: "mkjobs_follow_employer",
                                            employerid: this.items[idx].id
                                        }, function(data) {
                                            if (data == -1) {
                                                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                                                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                                                }
                                            } else if (data == 2) {
                                                alert("<?php echo get_string('FibeAdd', 'local_job'); ?>");
                                            } else if (data == 1) {
                                                this.items[idx].has_follow_employer = 1;
                                            } else {
                                                this.items[idx].has_follow_employer = 0;
                                            }
                                        }.bind(this));
                                    }
                                }
                            });
                        });
                    </script>
                <?php else : ?>
                    <div class="no-data style02">
                        <a href="<?php echo $CFG->wwwroot .'/local/job/companies/index.php'; ?>" class="btns point arrow"><?php echo get_string('wiewCompany', 'local_job'); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    function followEmployer(el, id, classon, classoff) {
        $.post("<?php echo $CFG->wwwroot .'/local/job/ajax.php'; ?>", {
            sesskey: "<?php echo sesskey(); ?>",
            action: "mkjobs_follow_employer",
            employerid: id
        }, function(data) {
            if (data == -1) {
                if (confirm("<?php echo get_string('Loginrequired', 'local_job'); ?>")) {
                    window.location.href = "<?php echo $VISANG->wwwroot . '/auth/login.php?urltogo=' . $baseurl; ?>";
                }
            } else if (data == 2) {
                alert("<?php echo get_string('FibeAdd', 'local_job'); ?>");
            } else if (data == 1) {
                $(el).removeClass(classoff || "ic-heart");
                $(el).addClass(classon || "ic-heart-on");
            } else {
                $(el).removeClass(classon || "ic-heart-on");
                $(el).addClass(classoff || "ic-heart");
            }
        });
    }
    $(document).ready(function(){
    $(".crs-left-block>ul li a[href*='<?php echo $CFG->wwwroot .'/local/mypage/inter.php'?> ']").parent().addClass("on");
    });
</script>

<?php
echo $OUTPUT->footer();
