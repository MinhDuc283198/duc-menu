<?php
/**
 * 메인콘텐츠 뷰어
 * */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/local/mypage/lcmsliib.php');

$id = required_param('id', PARAM_INT);

//$sql = 'select lr.id, lc.con_name, lcf.filename, lcf.filepath, lcf.duration 
//    from {lcms_repository} lr 
//    join {lcms_contents} lc on lr.lcmsid = lc.id 
//    join {lcms_contents_file} lcf on lc.id = lcf.con_seq 
//    left join {lcms_track} lt on lr.lcmsid = lcf.id = lt.lcms
//    where lr.id =:id ';
$sql = "SELECT rep.id, lcf.filename, lcf.filepath, lcf.duration, "
        . "con.id as con_id, con.con_name, con.con_type, con.con_des, "
        . "con.update_dt, con.data_dir, con.embed_type, con.embed_code "
        . "FROM {lcms_repository} rep "
        . "JOIN {lcms_contents} con ON con.id= rep.lcmsid "
        . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
        . "LEFT JOIN {lcms_track} lt ON rep.lcmsid = con.id = lt.lcms "
        . "WHERE con.id= :id";
$data = $DB->get_record_sql($sql, array('id' => $id));

$lcmscontents = $DB->get_record('lcms_contents', array('id' => $id));
$lcmsrepository = $DB->get_record('lcms_repository', array('lcmsid' => $id));

if (!$lcmscontents) {
    print_error('content_id is incorrect');
}
if (!$data) {
    print_error(get_string('nocontents', 'local_repository'));
    die();
}

//$lcmsprogress = $DB->get_field('okmedia', 'completionprogress', array('id'=>$okmedia->id));
//$lcmstimestart = $lcmscontents->timestart;
//진도율 가져오기
$lastview = '0000-00-00 00:00';
$playtime = '00:00';
$progress = '0%';
$studying = get_string('beforestudy', 'okmedia');
$track = $DB->get_record('lcms_track', array('lcms' => $lcmscontents->id, 'userid' => $USER->id));
$contentsfile = $DB->get_record('lcms_contents_file', array('con_seq' => $lcmscontents->id));
if ($track) {
    $lastview = date('Y-m-d H:i', $track->timeview);
    $ptm = lcms_time_from_seconds($track->playtime);
    $playtime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
    $progress = ($track->progress > 100) ? '100%' : $track->progress . '%';
    $studying = ($track->progress <= 100) ? get_string('studying', 'okmedia') : get_string('complete', 'okmedia');
}
// okmedia 마감시간 
//$timeend = $lcmscontents->timeend;
//insert_lcms_history($data->con_id, 'Repository Viewed', $USER->id);
?>
<link href="/theme/oklassedu/style/style.css" rel="stylesheet" />
<link href="/theme/oklassedu/style/media.css" rel="stylesheet" />
<!--<script type="text/javascript" src="/mod/lcms/video_player.js"></script>-->
<script src="<?php echo $CFG->wwwroot . '/lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js' ?>"></script>
<script src="/theme/oklassedu/javascript/theme.js" type="text/javascript"></script>

<div class="popwrap" name="popupwrapper">
    <div class="pop_title hasstar">
        <?php
        echo $data->con_name;

        $sqlaverage = 'select (SUM(stars) / COUNT(userid)) AS averagestar from {lcms_contents_evaluation} where contentid = ' . $lcmscontents->id;
        $getaverage = $DB->get_record_sql($sqlaverage);
        ?>
        <div class="star-area" data-num="<?php echo (empty($getaverage) ? "0" : round($getaverage->averagestar)); ?>" title="<?php echo (empty($getaverage) ? '' : (get_string('average','local_mypage') . $getaverage->averagestar)); ?>"><!-- data-num 점수 -->
        </div>
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_pop_close.png" alt="" />
    </div>

    <div class="pop_content">
        <?php
        echo '<input type="hidden" id= "lcmsconid" value=" ' . $data->con_id . ' ">';
        ?>
        <div class="videoinfo">
            <?php
            if ($progress != '100%') {
                ?>
                <p class="circletext">
                    <span><?php get_string('learn','local_mypage') ?></span>
                </p>
                <?php
            } else {
                ?>
                <p class="circletext inverse">
                    <span><?php get_string('learnclear','local_mypage') ?></span>
                </p>    
                <?php
            }
            ?>
            <!--  학습 완료 -->
            <ul class="info">

                <li class="m-col3">
                    <p class="text-bold"><?php get_string('lastedudate','local_mypage') ?></p>
                    <p id = "id_lastview"><?php echo empty($lastview) ? get_string('firstlearn','local_mypage') : $lastview; ?></p>
                </li>
                <li class="m-col3">
                    <p class="text-bold"><?php get_string('edutime','local_mypage') ?></p>
                    <p id="id_playtime"><?php echo $playtime; ?></p>
                </li>
                <li class="m-col3">
                    <p class="text-bold"><?php get_string('progressrate','local_mypage') ?></p>
                    <p id="id_progress"><?php echo $progress; ?></p>
                </li>
            </ul>

        </div>
        <div class="videoarea">
            <?php
            $useseek = $progress == 100 ? 'use' : 'unuse';
//              include ($CFG->dirroot . '/local/repository/viewer/video_player_mainpage.php');
            if ($data->con_type == 'video') {
                $viewer_url = '/local/repository/viewer/video_player.php?id=' . $data->con_id . '&useseek=' . $useseek;
                
                
                
                
                
//                require_once($CFG->dirroot . "/lib/filelib.php");
//                require_once $CFG->dirroot . '/local/repository/lib.php';
//                require_once $CFG->dirroot . '/local/repository/config.php';
//                $transcodingurl = get_config('moodle','vodurl');
//                //동영상 콘텐츠 보기
//                
//                $file = $DB->get_record('lcms_contents_file', array('con_seq' => $id));
//                $viewer_url = $transcodingurl.'/jmedia/'.$data->data_dir.'/'.$file->filename. '?useseek=' . $useseek;
            } else if ($data->con_type == 'embed') {
                if ($data->embed_type == 'youtube') {
                    $viewer_url = '/local/repository/viewer/video_player_embed.php?id=' . $data->con_id;
                } else if ($data->embed_type == 'vimeo') {
                    $viewer_url = '/local/repository/viewer/video_player_embed.php?id=' . $data->con_id;
                }
            }
            ?>
            <iframe id="videoiframe" src="<?php echo $viewer_url; ?>" frameborder="0" allowfullscreen></iframe> 

        </div>
       
        <!-- 별점 영역 -->
        <div class="v_bottom_info">

            <?php
            $starnum = $DB->get_record('lcms_contents_evaluation', array('contentid' => $data->con_id, 'userid' => $USER->id));
            ?>

            <div class="star-area" id="star_area" data-num="<?php echo (empty($starnum) ? "0" : $starnum->stars); ?>"> <!-- data-num 점수 --> 
            </div>
            <input type="button" id="btnstar" value="<?php get_string('enrollment','local_mypage') ?>"/>

        </div>
        
         <div class="v_text">
            <?php
                echo $data->con_des;
            ?>
        </div>
        
        <!-- 댓글/공유 -->
        <div class="tab_header">
            <span class="on m-col2" id='cntcomment'></span>
            <span class="m-col2"><?php get_string('share','local_mypage') ?></span>
        </div>
        <div class="tab_contents nobrd">
            <!-- 댓글 영역 -->
            <div class="on">
                <form class="reply_form" id='reply_forms'>
                    <input type="hidden" name="contentid" value="<?php echo $data->con_id; ?>" />
                    <input type="hidden" name="mod" value="write" />
                    <textarea title="<?php get_string('commentinput','local_mypage') ?>" id="textareacomments" name="comments" placeholder="<?php get_string('plscomment','local_mypage') ?>"></textarea>
                    <input type="button" class="btn reply" value="<?php get_string('goenrollment','local_mypage') ?>" onclick="addcomment()" />
                </form>

                <ul class="reply_list">
                </ul>
            </div>
            <!-- 공유 영역 -->
            <div>
                <div class="div_title">URL</div>
                <div class="link_txt">
                    <?php echo $CFG->vodurl . '/_definst_/' . $data->filepath . '/' . str_replace('.mp4', '_hd.mp4', $data->filename).'/playlist.m3u8'; ?>
                </div>
                <div class="div_title">Embeded Code</div>
                <div class="link_txt">
                    <iframe src="<?php echo $CFG->vodurl . '/_definst_/' . $data->filepath . '/' . str_replace('.mp4', '_hd.mp4', $data->filename).'/playlist.m3u8'; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    var alereadylike;
    var numstar;
    var using;
    $(document).ready(function () {
        using = '';
        // 이미 별점 저장한 횟수
        alereadylike = '<?php echo $starnum->startimes; ?>';

        // 댓글수, 댓글 나타내는 함수
        reply_list_refresh();
        plus_viewcounts();

        /** 설문 **/
        // 기본 설정
        numstar = '<?php echo (empty($starnum) ? "0" : $starnum->stars); ?>';
//        $('#star_area').attr('data-num', numstar);

        // click 함수를 사용할 경우 append로 생성된 하위 span 태그를 인식하지 못함
        // on함수를 사용하여 span 태그 클릭 이벤트 생성
        // index() : 순서 관련된 것일때 사용
        var evt = "click";
        if (check_mobile()) {
            evt = "touchend";
        }
        $('#star_area').off(evt).on(evt, 'span', function () {
            var idx = $(this).index();//클릭한 span의 index값
            var parent = $(this).parent();

            $('#star_area').attr('data-num', idx + 1);

            parent.find("span").removeClass("on");
            parent.find("span").each(function (i) {
                if (i <= idx) {
                    $(this).addClass("on");
                } else {
                    return false;
                }
            });
        });


        $('#btnstar').on('click', function () {
            var starnumber = $('#star_area').attr('data-num');
            var contentid = '<?php echo $data->con_id; ?>';
            if (starnumber < 1) {
                alert(get_string('inputstarating','local_mypage'));
            } else {
                if (alereadylike < 1) {
                    if (confirm(get_string("savestaratingnomodified",'local_mypage'))) {
                        alereadylike++;
                        savestars(contentid, starnumber);
                    }
                } else {
                    var bool = true;
                    var indexnum = 0;
                    $('#star_area span').each(function () {
                        if (bool) {
                            $(this).attr('class', 'on');
                            indexnum++;
                            if (indexnum == numstar) {
                                bool = false;
                            }
                        } else {
                            $(this).attr('class', '');
                        }
                    });
                    alert(get_string("alreadybeenstarating",'local_mypage'));
                }
            }
        });
    });

    function savestars(contentid, starnumber) {
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_evaluation_submit.php" ?>',
            method: 'POST',
            data: {
                contentid: contentid,
                star: starnumber
            },
            success: function (data) {
                if (data) {
                    $('#star_area').attr('data-num', data);
                    var bool = true;
                    var indexnum = 0;
                    $('#star_area span').each(function () {
                        if (bool) {
                            $(this).attr('class', 'on');
                            indexnum++;
                            if (indexnum == data) {
                                bool = false;
                            }
                        } else {
                            $(this).attr('class', '');
                        }
                    });
                    numstar = data;
                    indexnum = 0;
                    get_average_star();
                    alert(get_string('savestarating','local_mypage'));
                }
            }
        });
    }

    /** 댓글 **/
    function replysub() {
        var comments = $('textarea[name="comments"]').val();
        if (!(comments)) {
            alert(get_string('plscomment','local_mypage'));
            return false;
        }
        return true;
    }
    // 수정폼 나오게 하기
    function editcomment(commentid, commuserid) {
        var userid = '<?php echo $USER->id; ?>';
        var commuserid = commuserid;

        var strr = "#" + eval(commentid);
        if (userid == commuserid && using == '') {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_get_comments.php" ?>',
                method: 'POST',
                data: {
                    contentid: '<?php echo $data->con_id; ?>',
                    commentid: commentid,
                    type: 4
                }, success: function (data) {
                    $(".reply_list").find(strr).after(data);
//                    $(strr).find("#updatebtn").remove(); // 버튼 아예 삭제
//                    $(strr).find("#updatebtn").bind('click', false); // 버튼의 클릭 기능 삭제
//                    $(strr).find("#updatebtn").removeAttr("href"); // 댓글의 href 부분 삭제
                    using = commentid;
                }, error: function (request, status, error) {
                    console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
                }
            });
        }
    }

    // 수정처리하기
    function updatecomment(commentid, commuserid) {
        var userid = '<?php echo $USER->id; ?>';
        var commuserid = commuserid;
        var params = $("#reply_update_forms").serialize();
        if (userid == commuserid) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_comment_submit.php" ?>',
                method: 'POST',
                data: params,
                success: function (data) {
                    if (data) {
                        reply_list_refresh();
                        using = '';
                        alert(get_string('commentmodified','local_mypage'));
                    } else {
                        alert(get_string('nocommentmodified','local_mypage'));
                    }
                }
            });
        } else {
            alert(get_string('cantmodifide','local_mypage'));
        }
    }
    function deletecomment(commentid, commuserid) {
        var userid = '<?php echo $USER->id; ?>';
        var commuserid = commuserid;
        if (userid == commuserid) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_comment_submit.php" ?>',
                method: 'POST',
                data: {
                    commentid: commentid,
                    mod: 'delete'
                },
                success: function (data) {
                    if (data) {
                        reply_list_refresh();
                        alert(get_string('commentdelete','local_mypage'));
                    } else {
                        alert(get_string('cantcommentdelete','local_mypage'));
                    }
                }
            });
        } else {
            alert(get_string('cantdelete','local_mypage'));
        }
    }

    function addcomment() {
        var result = replysub();
        if (result) {
            var params = $("#reply_forms").serialize();
            $.ajax({
                url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_comment_submit.php" ?>',
                method: 'POST',
                data: params,
                success: function (data) {
                    if (data) {
                        console.log(data);
                        reply_list_refresh();
                        alert(get_string('commentenroll','local_mypage'));
                    }
                }
            });
        }
    }
    function reply_list_refresh() {
        $(".reply_list").empty();
        $("#cntcomment").empty();
        $("#textareacomments").attr('value', '');
        if ($("#textareacomments").val()) {
            $("#textareacomments").val('');
        }
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_get_comments.php" ?>',
            method: 'POST',
            data: {
                contentid: '<?php echo $data->con_id; ?>',
                type: 1
            },
            success: function (data) {
                if (data) {
                    $(".reply_list").append(data);
                }
            },
            error: function (request, status, error) {
                console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
            }
        });
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_get_comments.php" ?>',
            method: 'POST',
            data: {
                contentid: '<?php echo $data->con_id; ?>',
                type: 2
            },
            success: function (data) {
                if (data) {
                    $("#cntcomment").append(data);
                }
            },
            error: function (request, status, error) {
                console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
            }
        });
    }

    function get_average_star() {
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_get_comments.php" ?>',
            method: 'POST',
            data: {
                contentid: '<?php echo $data->con_id; ?>',
                type: 3
            },
            success: function (data) {
                if (data) {
                    $('.hasstar .star-area').attr('data-num', data.roundaverage);
                    var bool = true;
                    var indexnum = 0;
                    $('.hasstar .star-area span').each(function () {
                        if (bool) {
                            $(this).attr('class', 'on');
                            indexnum++;
                            if (indexnum == data.roundaverage) {
                                bool = false;
                            }
                        } else {
                            $(this).attr('class', '');
                        }
                    });
                    indexnum = 0;
                    $('.hasstar .star-area').attr('title', get_string('average','local_mypage') + data.average);
                }
            },
            error: function (request, status, error) {
                console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
            }
        });
    }

    function plus_viewcounts() {
        $.ajax({
            url: '<?php echo $CFG->wwwroot . "/local/mypage/pop_contents_like_submit.php" ?>',
            method: 'POST',
            data: {
                contentid: '<?php echo $data->con_id; ?>',
                type: 'viewcnt'
            },
            success: function (data) {
                if (data.status == 'success') {
//                    console.log(data);
//                    console.log('<?php echo $data->con_id; ?>');
                }
            },
            error: function (request, status, error) {
                console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
            }
        });
    }

    $(".pop_title .close").click(function () {
        //document.getElementById('videoiframe').contentWindow.closepopupFunction();
    });

</script>