<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();
require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string('mycourse:','local_mypage'));
$PAGE->set_url(new moodle_url('/local/mypage/mycourse_like.php'));
echo $OUTPUT->header();
$courses = local_mypage_get_mylikecourse($USER->id);
$today = time();
$userinfos = $DB->get_records_sql('SELECT * FROM {user} u JOIN {lmsdata_user} lu on u.id=lu.userid WHERE lu.userid = :userid',array('userid'=>$USER->id));
foreach($userinfos as $userinfo){
    $prof = $userinfo->usergroup == 'pr' ? 'pr' : '';
}
?>  
<ul class="mk-c-tab tab-event">
    <?php if($prof=='pr'){ ?> <li><a href="/local/mypage/mycourse.php?type=1"><?php echo get_string('coursecharge','local_mypage'); ?></a></li> <?php } ?>
    <li><a href="/local/mypage/mycourse.php?type==0"><?php echo get_string('mycourse:ing','local_mypage'); ?></a></li>
    <li><a href="/local/mypage/mycourse_end.php"><?php echo get_string('mycourse:end','local_mypage');?></a></li>
    <li class="on"><a href="/local/mypage/mycourse_like.php"><?php echo get_string('mycourse:like','local_mypage');?></a></li>
</ul>
<?php if($courses){
    ?>
    <ul class="thumb-list course style04">
            <?php 
            foreach($courses as $course){
                $teacher_list_array = array();
                $teacher = '';
             $query = "select lcp.teacherid from {user} u JOIN  {lmsdata_user}  lu ON u.id=lu.userid JOIN  {lmsdata_course_professor} lcp ON lu.userid = lcp.teacherid WHERE lu.usergroup='pr' and lcp.courseid=" . $course->parentcourseid;
             $teacher_array = $DB->get_records_sql($query);
             foreach ($teacher_array as $tak => $ta) {
                $user = local_course_teacher_id($ta->teacherid);
                $teacher_list_array[] = $user->firstname;
                $teacher = implode(',', $teacher_list_array);
             }
             $context = context_course::instance($course->lcocourseid);
             $path = local_course_get_imgpath($context->id);
             switch (current_language()){
                case 'ko' :
                    $coursename = $course->title;
                    break;
                case 'en' :
                    $coursename = $course->en_title;
                    break;
                case 'vi' :
                    $coursename = $course->vi_title;
                    break;
            }
             ?>
        <li>
            <div class="wp">
                <div class="img">
                    <span class="ic-heart on" id="like<?php echo $course->lcid;?>" data-target="<?php echo $course->lcid;?>"><?php echo get_string('like','local_mypage');?></span>
                    <?php 
                    if($course->selected){ //수강한적있는지
                        $dates = local_mypage_course_date($course->selected,$USER->id);
                        foreach($dates as $date){
                            if($today > $date->redate){  //이미 기간이 끝남
                                $courseurl = "/local/course/detail.php?id=".$course->lcid;
                            } else if ($today < $date->redate && $today > $date->enddate){ //복습기간중
                                $courseurl = "/course/view.php?id=".$course->mcid;
                            } else if ($today < $date->enddate){ //수강중
                                 $courseurl = "/course/view.php?id=".$course->mcid;
                            }
                         }
                    } else {   $courseurl = "/local/course/detail.php?id=".$course->lcid; }?>
                    <a href="<?php echo $courseurl;?>" class="bt"> <img src="<?php echo $path; ?>" alt="<?php echo $coursename; ?>" /></a>
                </div>
                <div class="txt">
                    <div class="tit"><a href="<?php echo $courseurl; ;?>"> <?php echo $coursename; ?></a></div>
                    <p><span><?php echo $teacher; ?> <!--<?php echo get_string('teacher','local_mypage');?>--> </span><span><?php echo get_string('total','local_mypage');?> <?php echo $course->lecturecnt; ?><?php echo get_string('course','local_mypage');?></span></p>
                    <p class="t-gray"><?php echo get_string('registrationday','local_mypage');?>: <?php echo get_string('frombought','local_mypage');?> <?php echo $course->courseperiod; ?><?php echo get_string('days','local_mypage');?></p>
                    <?php 
                    if($course->selected){ //수강한적있는지
                        $dates = local_mypage_course_date($course->selected,$USER->id);
                        foreach($dates as $date){
                            if($today > $date->redate){  //이미 기간이 끝남
                                ?> <a href="/local/course/detail.php?id=<?php echo $course->lcid ;?>" class="bt"><?php echo get_string('courseenrol','local_mypage');?></a> <?php 
                            } else if ($today < $date->redate && $today > $date->enddate){ //복습기간중
                                 ?> <a href="/course/view.php?id=<?php echo $course->mcid ;?>" class="bt gray"><?php echo get_string('review','local_mypage');?></a> <?php 
                            } else if ($today < $date->enddate){ //수강중
                                 ?> <a href="/course/view.php?id=<?php echo $course->mcid ;?>" class="bt gray"><?php echo get_string('mycourse:going','local_mypage');?></a> <?php 
                            } 
                         }
                    } else { ?> <a href="/local/course/detail.php?id=<?php echo $course->lcid ;?>" class="bt"><?php echo get_string('courseenrol','local_mypage');?></a> <?php }?>
                </div>

            </div>
        </li>
    <?php }?>
    </ul>
    <?php
} else {
   ?>
    <div class="no-data style02">
     <div><?php echo get_string('nolikecourse','local_mypage');?></div> 
    <a href="/local/course/main.php" class="btns point arrow"><?php echo get_string('viewallcourse','local_mypage');?></a>
</div>   
    <?php 
}?>

<?php
echo $OUTPUT->footer();
?>


