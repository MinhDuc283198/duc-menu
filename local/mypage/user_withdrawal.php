<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

echo $OUTPUT->header();
?>  
<div class="page_title">withdrawal</div> 
<div class="outbox text-center">
    <form id="form1" action="user_withdrawal_submit.php" method="POST">
        <input type="password" autocapitalize="off" name="password" autocomplete="new-password" class="h35" title="<?php get_string('password','local_mypage') ?>" placeholder="<?php get_string('pwdinputpls','local_mypage') ?>" />
       <input type="button" value="<?php get_string('withdrawal','local_mypage') ?>" id="withdrawal" class="btn pink big" />
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#withdrawal').click(function () {
            var password = $('input[name=password]').val();
            if(password.trim() == 0) {
                alert(get_string('pwdinputpls','local_mypage'));
                return false;
            } else {
                if(confirm(get_string('memberleavego','local_mypage'))) {
                    $('#form1').submit();
                }
            }
            
        }); 
    });
</script>
<?php
echo $OUTPUT->footer();
?>
