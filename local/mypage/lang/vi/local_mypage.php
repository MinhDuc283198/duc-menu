<?php
$string['pluginname'] = 'Trang của tôi';
$string['pluginnameplural'] = 'Trang của tôi';

//수강연기신청 페이지 (/local/mypage/coursepostponement.php)
$string['mycourse'] = 'Khóa học của tôi';
$string['selectdelaycourse'] = 'Chọn tạm hoãn khóa học';
$string['select'] = 'Chọn';
$string['delayreason'] = 'Lý do tạm hoãn';
$string['wantapplycourse'] = 'Yêu cầu tham gia khóa học';
$string['selectapplycourse'] = 'Chọn khóa học đăng ký';
$string['apply'] = 'Đăng ký';
$string['list'] = 'Danh sách';
$string['confirm:delaycourse'] = 'Bạn chắc chắn muốn yêu cầu tạm hoãn?';
$string['alert:selectdelaycourse'] = 'Vui lòng chọn khóa học muốn tạm hoãn';
$string['alert:delayreason'] = 'Vui lòng nhập lý do tạm hoãn';
$string['alert:selectapplycourse'] = 'Vui lòng chọn khóa học';
$string['yearterm'] = '{$a->year} năm {$a->term} kì hạn';

//수강연기신청 전송 (/local/mypage/coursepostponement.submit.php)
$string['message:existdelay'] = 'Tồn tại lịch sử tạm hoãn cho khóa học';
$string['message:completedelay'] = 'Áp dụng thành công. Yêu cầu tạm hoãn sẽ được được kiểm tra và thực hiện sau khi bởi quản trị khóa học';

$string['facebook'] = 'Tư vấn qua ';
$string['counseling'] = 'Facebook';

//이수증
$string['cer:number'] = 'Số chứng chỉ';
$string['cer:name'] = 'Tên';
$string['cer:neis'] = 'Số neis';
$string['cer:birth'] = 'Ngày sinh';
$string['cer:classtype'] = 'Loại hình đào tạo';
$string['cer:certime'] = 'Thời gian nghe giảng';
$string['cer:coursename'] = 'Tên Khóa học';
$string['cer:learningtime'] = 'Thời gian học';
$string['cer:time'] = 'Thời gian';
$string['cer:text'] = 'Tôi xác nhận bạn đã hoàn thành những điều trên';
$string['cer:classtype4'] = 'Tất cả';
$string['cer:cerprint'] = 'Cấp giấy chứng nhận hoàn thành khóa học';
$string['cer:search'] = 'Tìm kiếm';
$string['cer:coursename'] = 'Tên Khóa học';
$string['cer:classnum'] = 'Số lớp';
$string['cer:year'] = 'Năm';
$string['cer:classnum2'] = 'Lớp';
$string['cer:print'] = 'In';

$string['courseinfo'] = 'Thông tin khóa học';
$string['privacy'] = 'Thông tin cá nhân';



$string['dateofaccession'] = 'Ngày tham gia';
$string['myinfoedit'] = 'Sửa thông tin cá nhân';
$string['email'] = 'Thư điện tử';
$string['userid'] = 'ID';
$string['phonenum'] = 'Số điện thoại';
$string['mycourse'] = 'Khóa học của tôi';
$string['mycourse:ing'] = 'Khóa học đang nghe giảng';
$string['mycourse:going'] = 'Đi đến khóa học';
$string['mycourse:buy'] = 'Khóa học đã mua';
$string['mycourse:end'] = 'Khóa học đã hết hạn';
$string['mycourse:complete'] = 'Khóa học đã hoàn thành';
$string['mycourse:like'] = 'Khóa học yêu thích';
$string['coursenum'] = ' khóa';
$string['viewall'] = 'Xem tất cả';
$string['teacher'] = 'Giảng viên';
$string['progress'] = 'Tiến trình';

$string['total'] = 'Tổng cộng';
$string['course'] = 'Khóa học';
$string['registrationday'] = 'Ngày đăng kí';
$string['nocourse'] = "Không có lớp học phù hợp bây giờ.";
$string['nocourse2'] = "không có giờ học về phần có trách nhiệm.";
$string['viewallcourse'] = 'Xem tất cả khóa học';

$string['qna'] = 'Tư vấn 1:1';
$string['nodata:qna'] = 'Không có dữ liệu Hỏi đáp 1:1';
$string['nodata:pay'] = 'Không có dữ liệu thanh toán';

$string['paymentlist'] = 'Danh sách thanh toán';
$string['goods'] = 'Mặt hàng';
$string['goods:buy'] = 'Tên hàng';
$string['goods:pay'] = 'Đơn giá';
$string['goods:date'] = 'Ngày thanh toán';
$string['goods:way'] = 'Phương thức thanh toán';
$string['post:state'] = 'Tình trạng giao hàng';
$string['getrefund'] = 'Nhận tiền hoàn trả';
$string['goods'] = 'Sản phẩm';
$string['goods:buy'] = 'Tên sản phẩm';
$string['goods:pay'] = 'Đơn giá';
$string['goods:date'] = 'Ngày thanh toán';
$string['goods:way'] = 'Phương thức thanh toán';
$string['post:state'] = 'Trạng thái giao hàng';
$string['getrefund'] = 'Vui lòng đăng ký để được hỗ trợ tư vấn !';

$string['beforepayment'] = 'Trước khi thanh toán';
$string['refund'] = 'Hoàn tiền';
$string['cantrefund'] = 'Không thể hoàn trả tiền';
$string['wait'] = 'Đang chờ xác nhận';
$string['sucrefund'] = 'Hoàn tất hoàn trả tiền';

$string['course'] = 'Khóa học';
$string['book'] = 'Giáo trình';
$string['credit'] = 'Thẻ tính dụng';
$string['cash'] = 'Tiền mặt';
$string['post:pre'] = 'Chuẩn bị giao hàng';
$string['post:ing'] = 'Đang vận chuyển';
$string['post:end'] = 'Đã giao hàng';

$string['notification'] = 'Thông báo';
$string['alldel'] = 'Xóa tất cả';
$string['nodata:notifi'] = 'Không có thông báo';
$string['close'] = 'Đóng';
$string['delallnoti'] = 'Bạn có chắc chắn xóa tất cả thông báo?';
$string['delnoti'] = 'Bạn có chắc chắc xóa thông báo?';

$string['like'] = 'Thích';
$string['courseenrol'] = 'Đang ký khóa học';
$string['review'] = 'Đánh giá';
$string['course:ing'] = 'Toàn khóa';
$string['frombought'] = '';
$string['days'] = 'ngày';
$string['nolikecourse'] = 'Không có khóa học yêu thích';
$string['completion'] = 'Hoàn thành';
$string['notcompletion'] = 'Not Completion_vi';
$string['review2'] = 'Đánh giá bài học';
$string['nocomcourse'] = 'Không có khóa học được hoàn thành';

$string['withdrawal'] = 'Rút thành viên';
$string['ApplyState'] = 'Trạng thái Đăng ký';
$string['InterestInfo'] = 'Thông tin quan tâm';
$string['explanejob'] = 'Các thông tin này được dùng để gợi ý các việc làm phù hợp cho bạn từ Master Korean Jobs. <br/>(Ngày chia sẻ thông tin : {$a})';

$string['secbefore'] = 'Giây trước';
$string['minbefore'] = 'Phút trước';
$string['hourbefore'] = 'Giờ trước';
$string['daybefore'] = 'Ngày trước';

$string['facebook'] = 'Tư vấn';
$string['coursecharge'] = 'Khóa học phụ trách';
$string['student'] = 'Học viên';
$string['unanswered'] = 'Câu hỏi chưa được trả lời';
$string['people'] = 'Người';

//마이페이지 관심직무택
$string['minselect'] = '({$a} lựa chọn)';
$string['maxselect'] = '(Tối đa {$a} lựa chọn)';
$string['mulselect'] = 'Nhiều lựa chọn';
$string['career'] = 'Kinh nghiệm';
$string['occupation'] = 'Nhóm lợi ích';
$string['duty'] = 'Công việc quan tâm';
$string['hopearea'] = 'Khu vực hy vọng';
$string['languagelevel'] = 'Trình độ tiếng hàn';

$string['learningstatus:name'] = 'Tên';
$string['learningstatus:lastaccess'] = 'Truy cập gần nhất';
$string['learningstatus:coursetype'] = 'Loại khóa học';
$string['learningstatus:progress'] = 'Tiến độ';
$string['learningstatus:completed'] = 'Đã hoàn thành';
$string['learningstatus:paid_course'] = 'Có phí';
$string['learningstatus:free_course'] = 'Miễn phí';
$string['learningstatus:iscomplete_2'] = 'Đã hoàn thành';
$string['learningstatus:iscomplete_1'] = 'Chưa hoàn thành';
$string['learningstatus:iscomplete_0'] = 'Đang tiến hành';
$string['learningstatus:filter_iscomp'] = 'Hoàn thành';
$string['learningstatus:filter_period'] = 'Thời gian khóa học';
