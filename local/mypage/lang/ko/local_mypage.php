<?php
$string['pluginname'] = '마이페이지';
$string['pluginnameplural'] = '마이페이지';

//수강연기신청 페이지 (/local/mypage/coursepostponement.php)
$string['mycourse'] = '수강과정';
$string['selectdelaycourse'] = '연기과정선택';
$string['select'] = '선택하세요';
$string['delayreason'] = '연기사유';
$string['wantapplycourse'] = '희망신청기수';
$string['selectapplycourse'] = '기수선택';
$string['apply'] = '신청하기';
$string['list'] = '목록보기';
$string['confirm:delaycourse'] = '정말로 연기 신청을 하시겠습니까?';
$string['alert:selectdelaycourse'] = '연기할 과정을 선택하세요.';
$string['alert:delayreason'] = '연기사유를 입력하세요.';
$string['alert:selectapplycourse'] = '희망기수를 선택하세요.';
$string['yearterm'] = '{$a->year}년도 {$a->term}기';

//수강연기신청 전송 (/local/mypage/coursepostponement.submit.php)
$string['message:existdelay'] = '해당 과정에 이미 신청한 내역이 있습니다.';
$string['message:completedelay'] = '신청이 완료되었습니다. 관리자 확인 후 연기 신청이 진행됩니다.';


//이수증
$string['cer:number'] = '증서번호';
$string['cer:name'] = '성명';
$string['cer:neis'] = 'neis개인번호';
$string['cer:birth'] = '생년월일';
$string['cer:classtype'] = '교육종별';
$string['cer:certime'] = '이수시간';
$string['cer:coursename'] = '교육과정명';
$string['cer:learningtime'] = '교육기간';
$string['cer:locnumber'] = '지명번호';
$string['cer:chamedu'] = 'JEI 온라인 연수원';
$string['cer:time'] = '시간';
$string['cer:text'] = '위와 같이 이수하였음을 증명합니다.';
$string['cer:classtype1'] = '임직원';
$string['cer:classtype2'] = '멘토';
$string['cer:classtype3'] = '교육생';
$string['cer:classtype4'] = '전체';
$string['cer:cerprint'] = '이수증발급';
$string['cer:locnumchange'] = '교육지명번호 변경';
$string['cer:coursesearch'] = '과정명검색';
$string['cer:search'] = '검색';
$string['cer:cerconfirm'] = '<p>{$a}님께서는 아래의 과정을 이수하셨습니다.</p>';
$string['cer:coursename'] = '과정명';
$string['cer:classnum'] = '차수';
$string['cer:neisnum'] = 'NEIS번호';
$string['cer:locnum'] = '교육지명번호';
$string['cer:year'] = '년도';
$string['cer:classnum2'] = '차';
$string['cer:print'] = '출력';

$string['courseinfo'] = '수강정보';
$string['privacy'] = '개인정보';


$string['dateofaccession'] = '가입일';
$string['myinfoedit'] = '개인정보수정';
$string['email'] = '이메일';
$string['userid'] = '아이디';
$string['phonenum'] = '휴대폰번호';
$string['mycourse'] = '내 강좌';
$string['mycourse:ing'] = '수강중인 강좌';
$string['mycourse:going'] = '수강중인 강좌로';
$string['mycourse:buy'] = '내가 구매한 강좌';
$string['mycourse:end'] = '수강 완료 강좌';
$string['mycourse:complete'] = '수료한 강좌';
$string['mycourse:like'] = '관심 강좌';
$string['coursenum'] = '개';
$string['viewall'] = '전체보기';
$string['teacher'] = '선생님';
$string['progress'] = '진도율';

$string['total'] = '총';
$string['course'] = '강';
$string['registrationday'] = '수강기간';
$string['nocourse'] = '현재 수강중인 강좌가 없습니다.';
$string['nocourse2'] = '담당 강좌가 없습니다.';
$string['viewallcourse'] = '전체 강좌 보러가기';

$string['qna'] = '1:1 상담';
$string['nodata:qna'] = '1:1 상담 내역이 없습니다.';
$string['nodata:pay'] = '결제 내역이 없습니다.';

$string['paymentlist'] = '결제 내역';
$string['goods'] = '품목';
$string['goods:buy'] = '결제상품명';
$string['goods:pay'] = '결제금액';
$string['goods:date'] = '결제일';
$string['goods:way'] = '결제수단';
$string['post:state'] = '배송상태';
$string['getrefund'] = '환불은 1:1 상담을 통해 신청해주세요.';

$string['beforepayment'] = '결제전';
$string['refund'] = '환불';
$string['cantrefund'] = '환불불가';
$string['wait'] = '승인대기';
$string['sucrefund'] = '환불완료';

$string['course'] = '강좌';
$string['book'] = '교재';
$string['credit'] = '신용카드';
$string['cash'] = '입금';
$string['post:pre'] = '배송준비';
$string['post:ing'] = '배송중';
$string['post:end'] = '배송완료';


$string['notification'] = '알림';
$string['alldel'] = '전체삭제';
$string['nodata:notifi'] = '수신된 알림이 없습니다.';
$string['close'] = '닫기';
$string['delallnoti'] = '모든 알림을 삭제하시겠습니까?';
$string['delnoti'] = '알림을 삭제하시겠습니까?';

$string['like'] = '좋아요';
$string['courseenrol'] = '수강신청';
$string['review'] = '복습중';
$string['like'] = '수강중';
$string['frombought'] = '';
$string['days'] = '일';
$string['nolikecourse'] = '관심 설정한 강좌가 없습니다.';
$string['nocomcourse'] = '수강 완료한 강좌가 없습니다.';

$string['completion'] = '수료';
$string['notcompletion'] = '미수료';
$string['review2'] = '복습';

$string['withdrawal'] = '회원탈퇴';
$string['ApplyState'] = '지원 현황';
$string['InterestInfo'] = '관심정보';
$string['explanejob'] = 'master korean jobs의 관심정보와 동일하게 관리됩니다.<br/>(정보 공유 동의일 : {$a})';
$string['secbefore'] = '초 전';
$string['minbefore'] = '분 전';
$string['hourbefore'] = '시간 전';
$string['daybefore'] = '일 전';

$string['facebook'] = '페이스 북';
$string['counseling'] = '상담';
$string['coursecharge'] = '담당 강좌';
$string['student'] = '수강생';
$string['unanswered'] = '미답변';
$string['people'] = '명';

//마이페이지 관심직무택
$string['minselect'] = '({$a}개 선택)';
$string['maxselect'] = '(최대 {$a}개 선택)';
$string['mulselect'] = '다중 선택 가능';
$string['career'] = '경력 유무';
$string['occupation'] = '관심 직군';
$string['duty'] = '관심 직무';
$string['hopearea'] = '희망 지역';
$string['languagelevel'] = '한국어 활용 수준';

$string['learningstatus:name'] = '이름';
$string['learningstatus:lastaccess'] = '최종 접속일';
$string['learningstatus:coursetype'] = '수강 유형';
$string['learningstatus:progress'] = '진도율';
$string['learningstatus:completed'] = '수료 여부';
$string['learningstatus:paid_course'] = '정상결제';
$string['learningstatus:free_course'] = '무료수강';
$string['learningstatus:iscomplete_2'] = '수료';
$string['learningstatus:iscomplete_1'] = '미수료';
$string['learningstatus:iscomplete_0'] = '진행중';
$string['learningstatus:filter_iscomp'] = '수료 여부';
$string['learningstatus:filter_period'] = '수강기간';