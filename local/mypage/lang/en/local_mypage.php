<?php
$string['pluginname'] = 'Mypage';
$string['pluginnameplural'] = 'Mypage';

//수강연기신청 페이지 (/local/mypage/coursepostponement.php)
$string['mycourse'] = 'my course';
$string['selectdelaycourse'] = 'select delay course';
$string['select'] = 'select';
$string['delayreason'] = 'delay reason';
$string['wantapplycourse'] = 'want apply course';
$string['selectapplycourse'] = 'select apply course';
$string['apply'] = 'apply';
$string['list'] = 'list';
$string['confirm:delaycourse'] = 'Are you sure you want to ask for a delay?';
$string['alert:selectdelaycourse'] = 'Please select a course to postpone.';
$string['alert:delayreason'] = 'Please enter a reason for the delay.';
$string['alert:selectapplycourse'] = 'Please select the desired course.';
$string['yearterm'] = '{$a->year}year {$a->term}term';

//수강연기신청 전송 (/local/mypage/coursepostponement.submit.php)
$string['message:existdelay'] = 'There is a history that you have already applied for the course.';
$string['message:completedelay'] = 'Your application has been completed. The delay request will proceed after checking the administrator.';


//이수증
$string['cer:number'] = 'Certificate number';
$string['cer:name'] = 'name';
$string['cer:neis'] = 'neis number';
$string['cer:birth'] = 'birth';
$string['cer:classtype'] = 'class type';
$string['cer:certime'] = 'the length of the course';
$string['cer:coursename'] = 'course name';
$string['cer:learningtime'] = 'learning time';
$string['cer:time'] = 'time';
$string['cer:text'] = 'I certify that you have completed the above.';
$string['cer:classtype4'] = 'all';
$string['cer:cerprint'] = 'issuance of a certificate of completion';
$string['cer:search'] = 'search';
$string['cer:coursename'] = 'course name';
$string['cer:classnum'] = 'class num';
$string['cer:year'] = 'year';
$string['cer:classnum2'] = 'class';
$string['cer:print'] = 'print';

$string['courseinfo'] = 'Course information';
$string['privacy'] = 'Personal information';



$string['dateofaccession'] = 'date of accession';
$string['myinfoedit'] = 'Edit Personal Info';
$string['email'] = 'e-mail';
$string['userid'] = 'id';
$string['phonenum'] = 'phone number';
$string['mycourse'] = 'my course';
$string['mycourse:ing'] = 'my enrolled  course';
$string['mycourse:going'] = 'Go to the course';
$string['mycourse:buy'] = 'my bought course';
$string['mycourse:end'] = 'my end course';
$string['mycourse:complete'] = 'my completed course';
$string['mycourse:like'] = 'my like course';
$string['coursenum'] = 'things';
$string['viewall'] = 'view all';
$string['teacher'] = 'teacher';
$string['progress'] = 'progress';

$string['total'] = 'tota;';
$string['course'] = 'course';
$string['registrationday'] = 'registration day';
$string['nocourse'] = "don't have any classes right now.";
$string['nocourse2'] = "don't have any courses in charge.";
$string['viewallcourse'] = 'view all courses';

$string['qna'] = '1:1 Counselling';
$string['nodata:qna'] = 'no data in 1:1 qna board';
$string['nodata:pay'] = 'no data in my payment list';

$string['paymentlist'] = 'Payment list';
$string['goods'] = 'goods';
$string['goods:buy'] = 'goods name';
$string['goods:pay'] = 'goods price';
$string['goods:date'] = 'payment date';
$string['goods:way'] = 'method of payment';
$string['post:state'] = 'Delivery status';
$string['getrefund'] = 'Please apply for a refund through 1:1 consultation.';

$string['beforepayment'] = 'Before payment';
$string['refund'] = 'refund';
$string['cantrefund'] = 'Can not refund';
$string['wait'] = 'wait for approval';
$string['sucrefund'] = 'Refund completed';

$string['course'] = 'course';
$string['book'] = 'book';
$string['credit'] = 'credit';
$string['cash'] = 'Deposit';
$string['post:pre'] = 'preparing for delivery';
$string['post:ing'] = 'Shipping';
$string['post:end'] = 'Shipment complete';

$string['notification'] = 'notification';
$string['alldel'] = 'all delete';
$string['nodata:notifi'] = 'There is no notification';
$string['close'] = 'close';
$string['delallnoti'] = 'Are you sure you want to delete all notifications?';
$string['delnoti'] = 'Are you sure you want to delete the notification?';

$string['like'] = 'like';
$string['courseenrol'] = 'course enrol';
$string['review'] = 'review';
$string['course:ing'] = 'during the course';
$string['frombought'] = '';
$string['days'] = 'days';
$string['nolikecourse'] = 'There are no courses of interest';
$string['completion'] = 'Completion';
$string['notcompletion'] = 'Not Completion';
$string['review2'] = 'review lessons';
$string['nocomcourse'] = 'No courses have been completed.';

$string['withdrawal'] = 'Membership Withdrawal';
$string['ApplyState'] = 'Apply status';
$string['InterestInfo'] = 'Interest Info';
$string['explanejob'] = 'Managed the same as your interest in master korean jobs .<br/>(Date of sharing information : {$a})';
$string['secbefore'] = ' seconds ago';
$string['minbefore'] = ' minutes ago';
$string['hourbefore'] = ' time ago';
$string['daybefore'] = ' day ago';

$string['facebook'] = 'Facebook ';
$string['counseling'] = 'Counseling';
$string['coursecharge'] = 'a course in charge';
$string['student'] = 'student';
$string['unanswered'] = 'unanswered questions';
$string['people'] = 'people';

//마이페이지 관심직무택
$string['minselect'] = '({$a}choices)';
$string['maxselect'] = '(maximum {$a}choices)';
$string['mulselect'] = 'Multi selectable';
$string['career'] = 'Experience';
$string['occupation'] = 'Interest group';
$string['duty'] = 'Job of Interest';
$string['hopearea'] = 'Hope area';
$string['languagelevel'] = 'Korean Language Level';

$string['learningstatus:name'] = 'Name';
$string['learningstatus:lastaccess'] = 'Last access';
$string['learningstatus:coursetype'] = 'Course type';
$string['learningstatus:progress'] = 'Progress';
$string['learningstatus:completed'] = 'Completed';
$string['learningstatus:paid_course'] = 'Paid course';
$string['learningstatus:free_course'] = 'Free course';
$string['learningstatus:iscomplete_2'] = 'Completed';
$string['learningstatus:iscomplete_1'] = 'Not completed';
$string['learningstatus:iscomplete_0'] = 'In process';
$string['learningstatus:filter_iscomp'] = 'Completion';
$string['learningstatus:filter_period'] = 'Course period';