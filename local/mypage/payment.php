<?php
/**
 * 결제정보 리스트 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

require_login();

$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->js('/chamktu/js/lib/jquery.ui.datepicker_lang.js');
$PAGE->set_url(new moodle_url('/local/mypage/payment.php'));

$sdate = optional_param('sdate', '', PARAM_RAW);
$edate = optional_param('edate', '', PARAM_RAW);

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add(get_string("pluginname",'local_mypage'));
$PAGE->navbar->add(get_stirng("paymentinformation",'local_mypage'));

$sql = "SELECT lca.id, lca.neiscode, lca.locationnumber
    , lca.timecreated, lca.ordernumber, lca.price, lca.discounttype, lca.paymentmeans, lca.paymentstatus
    ,lco.coursename, lc.classyear, lc.classnum,  lc.learningstart, lc.learningend, 1 as old 
        FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc on lc.id = lca.courseid
        JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
        WHERE lca.userid = :userid AND lca.paymentstatus IS NOT NULL 
        union all 
        select lao.id, lao.neiscode, lao.locationnumber
        , lao.timecreated, lao.ordernumber, lao.price, lao.discounttype, lao.paymentmeans, lao.paymentstatus
        , lco.coursename, null classyear, null classnum, lao.studystart learningstart, lao.studyend learningend, 2 as old 
        FROM {lmsdata_applications_old} lao
        JOIN {lmsdata_course} lco on lco.coursecd = lao.coursecd 
        where lao.receipt = 'Y' and lao.userid = $USER->id 
        order by timecreated desc
";

$params = array('userid'=>$USER->id);

$datewhere = '';
if($sdate && $edate){
    $stime = strtotime($sdate);
    $etime = strtotime($edate.' 23:59:59');
    
    $datewhere = ' AND lca.timecreated >= :stime AND lca.timecreated <= :etime';
    $params['stime'] = $stime;
    $params['etime'] = $etime;
}

$payments = $DB->get_records_sql($sql.$datewhere, $params);

echo $OUTPUT->header();
?>  
<script type="text/javascript">
    $(document).ready(function () {
        $(".sDate, .eDate").datepicker({
            dateFormat: "yy-mm-dd"
        });        
    });
    function cancel_apply(id){
        call_layerPop("/local/mypage/pop_payment_cancel.php", "650px", "auto", {id:id});
    }

    function apply_cancel(id){
        $.ajax({
            url: 'apply_cancel.ajax.php',
            method: 'POST',
            dataType: 'json',
            data: {
                id: id,
            },
            success: function (data) {
                if (data.status == 'success') {
                    alert(get_string('cancel','local_mypage'));
                    location.reload();
                }
            }
        });
    }
</script>

<h3 class="page_title"><?php get_string('buylist','local_mypage') ?></h3>

<form method="post">
    <div class="search_option">
        <span class="text-bold title"><?php get_string('termlookup','local_mypage') ?></span>
        <span class="month_term m_block">
            <input type="button" name="month01" class="btn" title=<?php get_string("term",'local_mypage') ?> value=<?phpget_string("onemonth",'local_mypage') ?>/>
            <input type="button" name="month03" class="btn" title=<?php get_string("term",'local_mypage') ?> value=<?phpget_string("threemonth",'local_mypage') ?>/>
            <input type="button" name="month06" class="btn" title=<?php get_string("term",'local_mypage') ?> value=<?phpget_string("sixmonth",'local_mypage') ?>/>    
        </span>
        <span class="term m_block">
            <input type="text" name="sdate" title="<?php get_string("term",'local_mypage') ?>" class="sDate m-col3" readonly="readonly" value="<?php echo $sdate;?>" />
            ~<input type="text" name="edate" title="<?php get_string("term",'local_mypage') ?>" class="eDate m-col3" readonly="readonly" value="<?php echo $edate;?>" />
            <input type="submit" value="<?php get_string("cer:search",'local_mypage') ?>" class="btn gray h30" />
        </span>
    </div>
</form>
<div class="margin_bt15 text-right">
    * <span class="orange"><?php get_string("paymentnumber",'local_mypage') ?></span><?php get_string("oneclick",'local_mypage') ?> <span class="orange"><?phpget_string("detailpayment",'local_mypage') ?></span><?phpget_string("cando",'local_mypage') ?>
</div>

<table class="table">
    <thead>
        <tr>
            <th><?php get_string("applicationdate",'local_mypage') ?></th>
            <th><?php get_string("cer:coursename",'local_mypage') ?></th>
            <th><?php get_string("paymentdivision",'local_mypage') ?></th>
            <th><?php get_string("amountofpayment",'local_mypage') ?></th>
            <th><?php get_string("howpayment",'local_mypage') ?></th>
            <th><?php get_string("paymentcondition",'local_mypage') ?></th>
            <th><?php get_string("request",'local_mypage') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(!empty($payments)){
            foreach($payments as $payment){
                $cancel = $DB->get_record('lmsdata_applications_cancel', array('applicationid'=>$payment->id, 'userid'=>$USER->id));
        ?>
            <tr>
                <td>
                    <?php echo date('Y-m-d',$payment->timecreated);?>
                    <p class="orange pointer" onclick="location.href='<?php echo $CFG->wwwroot;?>/local/mypage/paymentdetail.php?id=<?php echo $payment->id;?>&sdate=<?php echo $sdate;?>&edate=<?php echo $edate;?>'">
                        <?php echo $payment->ordernumber;?>
                    </p>
                </td>
                <td class="text-bold"><?php echo $payment->coursename;?></td>
                <td>
                    <?php 
                    if($payment->discounttype == 'group'){
                        echo get_string('group','local_mypage');
                    } else if($payment->discounttype == 'member'){
                        echo get_string('member','local_mypage');
                    } else {
                        echo get_string('each','local_mypage');
                    }
                    ?>
                </td>
                <td><?php echo number_format($payment->price);?>원</td>
                <td>
                    <?php 
                    if($payment->paymentmeans == 'card'){
                        echo get_string('card','local_mypage');
                    } else if($payment->paymentmeans == 'bank'){
                        echo get_string('nowtimebanktransfer','local_mypage');
                    } else if($payment->paymentmeans == 'vbank'){
                        echo get_string('virtualaccount','local_mypage');
                    } else if($payment->paymentmeans == 'unbank'){
                        echo get_string('notong','local_mypage');
                    }
                    ?>
                </td>
                <td>
                    <?php 
                    if($payment->paymentstatus == 'stand'){
                        echo 'wait';
                    } else if($payment->paymentstatus == 'complete'){
                        if(empty($cancel)){
                            echo get_string('paymentok','local_mypage');
                        } else {
                            echo get_string('cancelapply','local_mypage');
                        }
                    } else if($payment->paymentstatus == 'cancel'){
                        echo get_string('paymentcancel','local_mypage');
                    } else if($payment->paymentstatus == 'fali'){
                        echo get_string('paymentfail','local_mypage');
                    }
                    ?>
                </td>
                <td>
                    <?php if($payment->paymentstatus == 'complete' && empty($cancel) && $payment->old == 1){?>
                    <p><input type="button" value="<?php get_string("receiptprint",'local_mypage') ?>" class="btn brd w80" onclick="window.open('receipt.php?id=<?php echo $payment->id;?>&userid=<?php echo $USER->id;?>', '','width=385, height=485')" /></p>
                    <?php }else if($payment->old == 2){
                    ?>
                    <p><input type="button" value="<?php get_string("receiptprint",'local_mypage') ?>" class="btn brd w80" onclick="window.open('receipt.php?id=<?php echo $payment->id;?>&userid=<?php echo $USER->id;?>&olddata=1', '','width=385, height=485')" /></p>
                    <?php }
                    if($payment->paymentstatus == 'stand'){
                    ?>
                    <p><input type="button" value="<?php get_string("chuiso",'local_mypage') ?>" class="btn gray w80 cancelbtn" onclick="apply_cancel('<?php echo $payment->id;?>')" /></p>
                    <?php } else if(empty($cancel) && $payment->learningend > time() && $payment->paymentstatus != 'cancel'){?>
                    <p><input type="button" value="<?php get_string("cancelap",'local_mypage') ?>" class="btn gray w80 cancelbtn" onclick="cancel_apply('<?php echo $payment->id;?>')"/></p>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        <?php 
            } 
        } else {
        ?>
            <tr>
                <td colspan="7"><?php get_string('nopayment','local_mypage') ?></td>
            </tr>
        <?php }?>
        
    </tbody>
</table>

<!--<div class="pagenav">
    <span class="prev"><a href="#">&lt;</a></span>
    <span class="on"><a href="#">1</a></span>
    <span class="next"><a href="#">&gt;</a></span>
</div>-->

<?php

echo $OUTPUT->footer();
?>


