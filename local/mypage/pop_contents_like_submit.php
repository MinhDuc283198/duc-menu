<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

// 조회수 새로고침
$contentid = required_param('contentid', PARAM_INT);
$type = required_param('type', PARAM_TEXT);
$user = $USER->id;

$resultvalues = new stdClass();
$getviewrecord = $DB->get_record('lcms_contents_viewcnt', array('contentid' => $contentid, 'userid' => $user));

if ($type == 'viewcnt') {
    if (empty($getviewrecord)) {
        $setviewrecord = new stdClass();
        $setviewrecord->contentid = $contentid;
        $setviewrecord->userid = $user;
        $setviewrecord->timecreated = time();
        
        if ($DB->insert_record('lcms_contents_viewcnt', $setviewrecord)) {
//            $resultvalues['views'] = $setviewrecord;
            $viewcnt = $DB->count_records('lcms_contents_viewcnt', array('contentid' => $contentid));
            $resultvalues->viewcnt = $viewcnt ;
            $resultvalues->status = 'success';
        } else {
             $resultvalues->status = 'fail';
        }
    } else {
        $getviewrecord->timerepeat = time();
        
        if ($DB->update_record('lcms_contents_viewcnt', $getviewrecord)) {
//            $resultvalues['views'] = $getviewrecord;
            $viewcnt = $DB->count_records('lcms_contents_viewcnt', array('contentid' => $contentid));
            $resultvalues->viewcnt = $viewcnt ;
            $resultvalues->status = 'success';
        } else {
             $resultvalues->status = 'fail';
        }
    }
}
@header('Content-type: application/json; charset=utf-8');
echo json_encode($resultvalues);



// 좋아요
/*
  $ifchecked = required_param('ifchecked', PARAM_INT); // 이미 좋아요 눌려있음 : 1, 좋아요 눌려있지않음 : 2
  $contentid = required_param('contentid', PARAM_INT);

  $user = $USER->id;

  $resultvalues;
  $getlikecnt;

  $getlikerecord = $DB->get_record('lcms_contents_like', array('contentid' => $contentid, 'userid' => $user));

  // 좋아요 취소를 눌렀을 경우 db의 상태를 취소 상태로 변경하는 방법
  //if($getlikerecord) {
  //    if ($ifchecked == 1) { // 좋아요를 취소
  //    } else { // 좋아요 누름
  //
  //    }
  //} else { // 좋아요 누름
  //}

  // 좋아요 취소를 눌렀을 경우 해당 db를 삭제하는 방법
  if ($getlikerecord) { // 좋아요를 취소
  $resultvalues = $DB->delete_records('lcms_contents_like', array('contentid' => $contentid, 'userid' => $user));
  } else { // 좋아요 누름
  $likerecord = new stdClass();
  $likerecord->contentid = $contentid;
  $likerecord->userid = $user;
  $likerecord->timecreated = time();

  $resultvalues = $DB->insert_record('lcms_contents_like', $likerecord);
  }

  if($resultvalues) {
  $getlikecnt = $DB->count_records('lcms_contents_like', array('contentid' => $contentid));
  }

  echo $getlikecnt;
 */