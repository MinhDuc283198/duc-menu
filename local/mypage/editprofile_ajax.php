<?php
/**
 * 휴대폰 인증 (AJAX)
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$phone1 = optional_param("phone1", 0, PARAM_RAW);
$phone2 = optional_param("phone2", 0, PARAM_RAW);
$phone3 = optional_param("phone3", 0, PARAM_RAW);

$phone = $phone1.$phone2.$phone3;
$phone1 = $phone1.'-'.$phone2.'-'.$phone3;

$sql = 'SELECT lu.* FROM {lmsdata_user} lu '
        . 'where lu.phone1 = '.$phone.' or lu.phone1 = '.$phone1; 

$userinfo = $DB->get_record_sql($sql);

if($userinfo){
   $retrunval->alert = (get_string("samenumbercallmanager",'local_mypage'));
   $retrunval->status = 'false';
}else{
   $phone = str_replace('-','', $phone);
   $rand = mt_rand(1000,10000);
   $content = get_string("certificationnumber",'local_mypage');
//   eduhope_balsong_sms($content, $phone);
   $retrunval->alert = (get_string("innumbalsong",'local_mypage'));
   $retrunval->status = 'true';
   $retrunval->rand = $rand;
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($retrunval);