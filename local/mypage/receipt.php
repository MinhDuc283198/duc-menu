<?php
/**
 * 영수출력 페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$id = optional_param('id', 0, PARAM_INT);
$userid = required_param('userid', PARAM_INT);
$olddata = optional_param('olddata', 0, PARAM_INT);


if($USER->id != $userid){
    echo '<script type="text/javascript">alert("'.get_string('noyourreceipt','local_mypage');'"); window.close();</script>';
    die();
}

if($olddata == 0){
    $sql = 'SELECT lca.*, lco.coursename, lco.hours
            FROM {lmsdata_course_applications} lca
            JOIN {lmsdata_class} lc on lc.id = lca.courseid
            JOIN {lmsdata_course} lco on lco.id = lc.parentcourseid
            WHERE lca.userid = :userid AND lca.id = :id';
} else {
    $sql = 'SELECT lao.*, lco.coursename, lco.hours
            FROM {lmsdata_applications_old} lao
            JOIN {lmsdata_course} lco on lco.coursecd = lao.coursecd 
            JOIN {lmsdata_class_real_old} lcro on lcro.ChasuNR = lao.chasunr
            JOIN {lmsdata_code} c on c.id = lco.coursegrade  
            JOIN {user} u on u.id = lao.userid 
            JOIN {lmsdata_user} lu on u.id = lu.userid 
            WHERE lao.userid = :userid AND lao.id = :id';
}
$payment = $DB->get_record_sql($sql, array('userid'=>$userid, 'id'=>$id));

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();
?>
<div id="receipt">
    <input type="button" value="Print" class="btn print r_content" onclick="window.print();" />
    <h3><?php get_string('receipt','local_mypage') ?></h3>
    <table class="table">
        <tbody>
            <?php if($olddata == 0){?>
                <?php if(empty($payment->cshrresult)){?>
                    <tr>
                        <th class="w100px"><?php get_string('ordernumber','local_mypage') ?></th>
                        <td class="text-left"><?php echo (!empty($payment->ordernumber))?$payment->ordernumber:'-';?></td>
                    </tr>
                <?php } else {?>
                    <tr>
                        <th class="w100px"><?php get_string('lssuedtype','local_mypage') ?></th>
                        <?php if($payment->cshrtype == 0){?>
                        <td class="text-left"><?php get_string('incomededuction','local_mypage') ?></td>
                        <?php } else {?>
                        <td class="text-left"><?php get_string('businessnumber','local_mypage') ?></td>
                        <?php 
                        }
                        ?>
                    </tr>
                <?php
                    $cash_receipt = $DB->get_record('lmsdata_receipt',array('application'=>$payment->id));
                    if(!empty($cash_receipt)){
                ?>
                    <tr>
                        <th class="w100px"><?php get_string('primarykeynumber','local_mypage') ?></th>
                        <td class="text-left"><?php echo $cash_receipt->identitynum;?></td>
                    </tr>
                <?php
                    }
                }
                ?>
            <?php } else {?>
                <tr>
                    <th class="w100px"><?php get_string('ordernumber','local_mypage') ?></th>
                    <td class="text-left"><?php echo (!empty($payment->ordernumber))?$payment->ordernumber:'-';?></td>
                </tr>
            <?php }?>
                <tr>
                    <th class="w100px"><?php get_string('cer:coursename','local_mypage') ?></th>
                    <td class="text-left"><?php echo $payment->coursename.'('.$payment->hours.get_string('time','local_mypage');?></td>
                </tr>
                <tr>
                    <th class="w100px"><?php get_string('price','local_mypage') ?></th>
                    <td class="text-left"><?php echo number_format($payment->price);?> <?php get_string('won','local_mypage') ?></td>
                </tr>
                <?php if($payment->paymentmeans == 'card'){?>
                <tr>
                    <th class="w100px"><?php get_string('paymentday','local_mypage') ?></th>
                    <td class="text-left"><?php echo (!empty($payment->paymentdate))?date(get_string('yymydd','local_mypage'), $payment->paymentdate):'-';?></td>
                </tr>
                <tr>
                    <th class="w100px"><?php get_string('cardnumber','local_mypage') ?></th>
                    <td class="text-left"><?php echo (!empty($payment->cardnumber))?$payment->cardnumber:'-';?></td>
                </tr>
                <?php } else {?>
                <tr>
                    <th class="w100px"><?php get_string('depositdate','local_mypage') ?></th>
                    <td class="text-left"><?php echo (!empty($payment->paymentdate))?date(get_string('yymydd','local_mypage'), $payment->paymentdate):'-';?></td>
                </tr>
                <tr>
                    <th class="w100px"><?php get_string('depositpeople','local_mypage') ?></th>
                    <td class="text-left"><?php echo (!empty($USER->lastname))?$USER->lastname:'-';?></td>
                </tr>
                <?php }?>
                <tr>
                    <th class="w100px"><?php get_string('Issueddate','local_mypage') ?></th>
                    <td class="text-left"><?php echo date(get_string('yymydd','local_mypage'), time());?></td>
                </tr>
        </tbody>
    </table>
    <div class="text-center stamp">
        <p><?php get_string('asabovereceipt','local_mypage') ?></p>
        <p class="margin_bt15"><?php get_string('hanyangsoftwarelaps','local_mypage') ?></p>
        <img id="stamp" src="/theme/oklassedu/pix/images/stamp.jpg" alt="stamp" />
    </div>
</div>

<?php

echo $OUTPUT->footer();
?>