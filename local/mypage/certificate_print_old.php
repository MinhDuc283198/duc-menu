<?php
/**
 * 기존 수강이력 이수증 출력페이지
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . '/lib/filelib.php';
require_once $CFG->dirroot . '/lib/pdflib.php';
require_once $CFG->dirroot . '/local/mypage/fpdi/fpdi.php';

setlocale(LC_ALL, "en_US.UTF-8");
$id = required_param('id', PARAM_INT);
$userid = required_param('userid', PARAM_INT);
$olddata = optional_param('olddata', 0, PARAM_INT);
// Check for valid admin user - no guest autologin
require_login(0, false);
if (isguestuser()) {
    $SESSION->wantsurl = (string) new moodle_url('/local/mypage/certificate_print.php');
    redirect(get_login_url());
}
$context = context_system::instance();

//이수증에 넣을 데이터 가져옴                                                                           
$sql = "select lao.id, lao.userid, lao.neiscode, lao.locationnumber, lco.courseid, lco.coursename, lu.birthday, u.lastname, c.codename, lao.chasunr, lao.finishnr,
    lco.classtype, lcro.iYear classyear, lcro.SeqNo classnum, lcro.iType ,lao.studystart learningstart, lao.studyend learningend, lco.learningtime ";
$from = "
    FROM m_lmsdata_applications_old lao
    JOIN m_lmsdata_course lco on lco.coursecd = lao.coursecd 
    join m_lmsdata_class_real_old lcro on lcro.ChasuNR = lao.chasunr
    join m_lmsdata_code c on c.id = lco.coursegrade  
    join m_user u on u.id = lao.userid 
    join m_lmsdata_user lu on u.id = lu.userid 
    where lao.completionstatus = 1 and lao.userid = :userid and lao.id = :id ";

$cert = $DB->get_record_sql($sql.$from, array('userid'=>$userid,'id'=>$id));

if(empty($cert)){
    echo '<script type="text/javascript">alert("출력 가능한 이수증이 없습니다."); location.href="'.$CFG->wwwroot.'/local/mypage/certificate.php";</script>';
    die();
}

require_once 'certi_sample.php';

if ($cert->itype == 'Y') {//classtype에 따라 값을 나눔
    $coursetype = get_string('cer:classtype1','local_mypage');
} else {
    $coursetype = get_string('cer:classtype3','local_mypage');
}

$vars = new stdClass();
$vars->background = $CFG->dirroot . '/local/mypage/img/certi/completion640890.jpg'; //배경사진
$vars->cell = ($cert->certificateimg) ? $CFG->dirroot . '/local/mypage/img/certi/cell6.JPG' : $CFG->dirroot . '/local/mypage/img/certi/cell5.JPG'; //테이블 사진
$vars->documentid = $cert->finishnr; //증서번호
$vars->name = $cert->lastname; //성명
$vars->neisbirth = ($cert->neiscode) ? $cert->neiscode : $cert->birthday; //네이스코드 없으면 생년월일
$vars->coursetype = $coursetype . ' ' . $cert->codename; //연수종별
$vars->coursetime = $cert->learningtime . get_string('cer:time','local_mypage'); //이수시간
$vars->learntime = date(get_string('yymmdd','local_mypage'), $cert->learningstart) . ' ~ ' . date(get_string('yymmdd','local_mypage'), $cert->learningend); //연수기간
$vars->coursename = $cert->coursename; //연수과정명
$vars->location = $cert->locationnumber; //지명번호
$vars->description = get_string('cer:text','local_mypage');//이수 문구
$vars->issuedate = date(get_string('yymmdd','local_mypage'), time()); //이수증 발급날짜 

//$sql2 = 'select * from {lmsdata_certificate_history} where classid =' . $cert->lcid . ' and userid = ' . $cert->userid . '';//이수증 발급 내역 가져옴
//$history = $DB->get_record_sql($sql2);
//if ($history->id) {//이미 내역이 있으면 update
//    $data->id = $history->id;
//    $data->issuedcount = $history->issuedcount + 1;
//    $data->timemodified = time();
//    $DB->update_record('lmsdata_certificate_history', $data);
//} else {//내역이 없으면 insert
//    $data = new stdClass();
//    $data->userid = $cert->userid;
//    $data->courseid = $cert->courseid;
//    $data->place = $cert->locationnumber;
//    $data->certificatenum = $vars->documentid;
//    $data->classid = $cert->lcid;
//    $data->issuedcount = 1;
//    $data->timecreated = time();
//    $data->timemodified = time();
//    $DB->insert_record('lmsdata_certificate_history', $data);
//}


$pdf = new completionPDF($vars->background); //이미지가 들어있는 폴더 
$pdf->Write($vars); //write 함수로 vars변수의 값들을 넘김
$pdf->Output('cert.pdf', 'I');//pdf출력
