<?php
/**
 * 현금영수증 발급신청 팝업
 */
require_once dirname(dirname(dirname (__FILE__))).'/config.php';

$id = optional_param('id', 0, PARAM_INT);

$sql = 'SELECT lca.*, lco.coursename, lc.classnum, lc.classyear FROM {lmsdata_course_applications} lca
        JOIN {lmsdata_class} lc ON lc.id = lca.courseid
        JOIN {lmsdata_course} lco ON lco.id = lc.parentcourseid
        WHERE lca.id = :id';

$application = $DB->get_record_sql($sql, array('id'=>$id));
$userinfo = $DB->get_record('user', array('id'=>$application->userid));

?>

<div id="popwrap">
    <h3 id="pop_title">
        <?php get_string('cashreceipts','local_mypage') ?>
        <img class="close r_content" src="/theme/oklassedu/pix/images/close.png" alt="" />
    </h3>
    <div id="pop_content">
        <form id="frm_cashbill" name="frm_cashbill" method="post" action="cashbill.submit.php">
            <table cellpadding="0" cellspacing="0" class="table">
                <input type="hidden" name="customerName" value="<?php echo $userinfo->lastname;?>" />
                <input type="hidden" name="email" value="<?php echo $application->email;?>" />
                <input type="hidden" name="hp" value="<?php echo $application->phone;?>" />
                <input type="hidden" name="orderNumber" value="<?php echo $application->ordernumber;?>" />
                <input type="hidden" name="itemName" value="<?php echo $application->coursename.' '.$application->classyear.get_string('year ','local_mypage').$application->classnum.get_string('gi','local_mypage');?>" />
                <input type="hidden" name="userid" value="<?php echo $userinfo->id;?>" />
                <input type="hidden" name="application" value="<?php echo $application->id;?>" />
                <input type="hidden" name="totalAmount" value="<?php echo $application->price;?>" />
                <input type="hidden" name="taxationType" value=<?php get_string("taxfree",'local_mypage')?> />
                <input type="hidden" name="supplyCost" value="<?php echo $application->price;?>">
                <input type="hidden" name="tax" value="0">
                <input type="hidden" name="serviceFee" value="0">
                <tbody>
                    <tr>
                        <td><?php get_string('productname','local_mypage') ?></td>
                        <td class="text-left"><?php echo $application->coursename.' '.$application->classyear.get_string('year ','local_mypage').$application->classnum.get_string('gi','local_mypage');?></td>
                    </tr>
                    <tr>
                        <td><?php get_string('cashreceiptstype','local_mypage') ?></td>
                        <td class="text-left">
                            <input type="radio" name="tradeUsage" checked value=<?php get_string("incomededuction",'local_mypage')?>> <?php get_string("incomededuction",'local_mypage')?>
                            <input type="radio" name="tradeUsage" value=<?php get_string("proofofexpenditure",'local_mypage')?>> <?php get_string("proofofexpenditure",'local_mypage')?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php get_string('sicknumber','local_mypage') ?></td>
                        <td class="text-left"><input type="text" name="identityNum" value="<?php echo $application->phone;?>"> <?php get_string('nonumber','local_mypage') ?></td>
                    </tr>
                    <tr>
                        <td><?php get_string('price','local_mypage') ?></td>
                        <td class="text-left"><?php echo number_format($application->price).get_string('won','local_mypage');?></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>