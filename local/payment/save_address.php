<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
global $DB,$USER;
$zipcode = optional_param('zipcode', '', PARAM_RAW);
$phone = optional_param('phone', '', PARAM_RAW);
$address = optional_param('address', '', PARAM_RAW);

$usernew = $DB->get_record('lmsdata_user', array('userid' => $USER->id), '*', MUST_EXIST);
$usernew->address = $address;
$usernew->phone1 = $phone;
$usernew->zipcode = $zipcode;
$DB->update_record('lmsdata_user', $usernew);

$user = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
$user->phone1 = $phone;
$user->timemodified = time();
$DB->update_record('user', $user);
