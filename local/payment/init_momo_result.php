<?php

header('Content-type: text/html; charset=utf-8');
require('../../config.php');
include "momo/common/helper.php";

$secretKey = $moopaykey["accessKey"]->value;

if (!empty($_GET)) {
//    $partnerCode = $_GET["partnerCode"];
//    $accessKey = $_GET["accessKey"];
    $orderId = $_GET["orderId"];
    $localMessage = $_GET["localMessage"];
    $message = $_GET["message"];
    $transId = $_GET["transId"];
    $orderInfo = $_GET["orderInfo"];
    $amount = $_GET["amount"];
    $errorCode = $_GET["errorCode"];
    $responseTime = $_GET["responseTime"];
    $requestId = $_GET["requestId"];
    $extraData = $_GET["extraData"];
    $payType = $_GET["payType"];
    $orderType = $_GET["orderType"];
    $extraData = $_GET["extraData"];
    $m2signature = $_GET["signature"]; //MoMo signature
    $extradata = explode('/', $_GET['extraData']);

    $transfer["orderId"] = $_GET["orderId"];
    $transfer["message"] = $_GET["message"];
    $transfer["orderInfo"] = $_GET["orderInfo"];
    $transfer["amount"] = $_GET["amount"];
    $transfer["responseTime"] = $_GET["responseTime"];
    $transfer["errorCode"] = $_GET["errorCode"];
    $transfer["extraData"] = $_GET["extraData"];
    $transfer["phone"] = $extradata[1];
    $transfer["zipcode"] = $extradata[2];
    $transfer["address"] = $extradata[3];
    $transfer["selectpaytype"] = $extradata[4];
    $transfer["localMessage"] = $_GET["localMessage"];
    $transfer["orderType"] = $_GET["orderType"];
    //Checksum
    $rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo .
            "&orderType=" . $orderType . "&transId=" . $transId . "&message=" . $message . "&localMessage=" . $localMessage . "&responseTime=" . $responseTime . "&errorCode=" . $errorCode .
            "&payType=" . $payType . "&extraData=" . $extraData;

    $partnerSignature = hash_hmac("sha256", $rawHash, $secretKey);
    $resultjson = json_encode($transfer);


    // 결제 배송 테이블 insert


    $datatype = $DB->get_field_sql("select type from {lmsdata_class} where id = ? ", array($extradata[0]));
    $lmsdata->userid = $USER->id;
    $lmsdata->status = 1;
    $lmsdata->type = $datatype == 2 ? 2 : 1;
    $lmsdata->ref = $extradata[0];
    $lmsdata->paytype = $_GET['errorCode'] == 0 ? 1 : 2;
    $lmsdata->paymethod = $extradata[4]; 
    $lmsdata->paycode = $_GET['orderId'];
    $lmsdata->timecreated = time();
    $lmsdata->timemodified = time();
    
    if ($_GET['errorCode'] == 0&&$extradata[5]==false) {
        $isExistPayCode = $DB->get_field_sql("select paycode from {lmsdata_payment} where paycode = ?", array($lmsdata->paycode));
        if($isExistPayCode == false) {
            $payid = $DB->insert_record('lmsdata_payment', $lmsdata);
            if ($datatype != 2) {
                require_once $CFG->dirroot . '/local/lmsdata/lib.php';
                require_once $CFG->dirroot . '/local/course/lib.php';
                $courseid = $DB->get_field("lmsdata_class", 'courseid', array("id" => $extradata[0]));
                $enrolresult = local_course_whether_enrol($courseid, $USER->id, 'student');
                if ($enrolresult == false) {
                    $result = local_lmsdata_enrol_user($courseid, $USER->id, 'student');
                    $returnvalues = new stdClass();
                    $returnvalues->status = $result;
                    if ($result) {

                    } else {
                        $aa = local_lmsdata_enrol_edit_user($courseid, $USER->id, 'student');
                    }
                }
            }
            if ($datatype != 1) {
                $lmsdata2->payid = $payid;
                $lmsdata2->bookid = $extradata[0];
                $lmsdata2->address = $extradata[2] . '-' . $extradata[3];
                $lmsdata2->cell = $extradata[1];
                $lmsdata2->timecreated = time();
                $lmsdata2->timemodified = time();
                $DB->insert_record('lmsdata_productdelivery', $lmsdata2);
            }
        }
    }
    $credit_id = $extradata[5];
    $employer_id = $extradata[6];
    $query="SELECT 
            cm.id,
            cm.credit_name,
            cm.points,
            cm.price,
            cm.banner_image_url,
            cm.expired_date
            FROM {vi_credit_menu} cm
            WHERE cm.id=$credit_id
            ";
    $credit_menu=$DB->get_record_sql($query);
    $sql="SELECT 
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$employer_id
            ";
    $credit=$DB->get_record_sql($sql);

    if ($_GET['errorCode'] == 0&&$extradata[5]!=null) {
        if(!$credit)
        {
            $data = new stdClass();
            $data->employer_id=$employer_id;
            $data->credit_point=$credit_menu->points;
            $data->expire_date=$credit_menu->expired_date;
            $data->timecreated = time();
            $DB->insert_record('vi_credits',$data);


        }
        else{
            $credit->credit_point+=$credit_menu->points;
            $credit->timemodified=time();
            $DB->update_record('vi_credits',$credit);
        }

        $data = new stdClass();
        $data->member_credit_id=$credit->id;
        $data->transaction_code='1001';
        $data->pg_code='3001';
        $data->price=$credit_menu->price;
        $data->point=$credit_menu->points;
        $data->timelogged = time();
        $DB->insert_record('vi_credits_logs',$data);
    }
    echo "<script>var raw = '$resultjson'; opener.parent.pay_resultpop(raw); window.close();</script>";
//    if ($m2signature == $partnerSignature) {
//        if ($errorCode == '0') {
//            $result = '<div class="alert alert-success"><strong>Payment status: </strong>Success</div>';
//        } else {
//            $result = '<div class="alert alert-danger"><strong>Payment status: </strong>' . $message . '/' . $localMessage . '</div>';
//        }
//    } else {
//        $result = '<div class="alert alert-danger">This transaction could be hacked, please check your signature and returned signature</div>';
//    }
}    