<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();
$jdata= optional_param('data', '', PARAM_RAW);
$jdata2= optional_param('jdata', '', PARAM_RAW);
$data = json_decode($jdata);
if($jdata2){
    $data = json_decode($jdata2);
}
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('결제');

echo $OUTPUT->header();
?>  
<div class="layerpop">
    <?php    ?>
    <div class="pop-title">
        <?php if($data->errorCode == 0){  
            echo get_string('successpay','local_payment');
        }else{
            echo get_string('falsepay','local_payment');
        } ?>
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <p class="t-blue t-end">
        <?php if($data->errorCode == 0){  
            echo get_string('successpayfull','local_payment');
        }else{
            echo get_string('falsepayfull','local_payment');
        } ?>
        </p>

        <h5 class="pg-tit"><?php echo get_string('payinfo','local_payment'); ?></h5>
        <table class="table detail">
            <tbody>
                <tr>
                    <th width="20%"><?php echo get_string('ordernum','local_payment'); ?></th>
                    <td class="text-left"><?php echo $data->orderId ?></td>
                </tr>
                <tr>
                    <th width="20%"><?php echo get_string('goodsname','local_payment'); ?></th>
                    <td class="text-left"><?php echo $data->orderInfo ?></td>
                </tr>
                <tr>
                    <th width="20%"><?php echo get_string('paytype','local_payment'); ?></th>
                    <td class="text-left"><?php echo $data->orderType ?></td>
                </tr>
                <?php if($data->errorCode == 0){   ?>
                 <!--무통장인경우--> 
                 <?php if($data->orderType == 'directdeposit'){   ?>
                <tr>
                    <th width="20%">결제 수단</th>
                    <td class="text-left">
                        <span><?php echo get_string('directdeposit','local_payment'); ?></span>
                        <div class="rd-info">
                            <ul class="dash-list">
                                <li><?php echo get_string('deposittext1', 'local_payment'); ?></li>
                                <li><?php echo get_string('deposittext2', 'local_payment'); ?></li>
                                <li><?php echo get_string('deposittext3', 'local_payment'); ?></li>
                            </ul>
                           <p class="t-info"><?php echo get_string('deposittext4', 'local_payment', date("Y-m-d", strtotime("+5 days"))); ?></p>
                           <p class="t-info"><?php
                               if($jdata){
                               echo get_string('deposittext5', 'local_payment');}
                               else echo '';?></p>
                        </div>
                    </td>
                </tr>
                 <?php } ?>
                 <!--무통장인경우--> 
                <tr>
                    <th width="20%"><?php echo get_string('price','local_payment'); ?></th>
                    <td class="text-left"><?php echo $data->amount ?>VND</td>
                </tr>
                <tr>
                    <th width="20%"><?php echo get_string('accepttime','local_payment'); ?></th>
                    <td class="text-left"><?php echo $data->responseTime ?></td>
                </tr>
            </tbody>
        </table>
        <?php if($data->zipcode && $data->address){?>
        <h5 class="pg-tit"><?php echo get_string('deleveryinfo','local_payment'); ?></h5>
        <table class="table detail">
            <tbody>
                <tr>
                    <th width="20%"><?php echo get_string('phone','local_payment'); ?></th>
                    <td class="text-left"><?php echo $data->phone ?></td>
                </tr>
                <tr>
                    <th width="20%"><?php echo get_string('address','local_payment'); ?></th>
                    <td class="text-left">
                        <p><?php echo $data->zipcode ?></p>
                        <p><?php echo $data->address ?></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php }?>
                <?php }else{ ?>

                <tr>
                    <th width="20%"><?php echo get_string('errorinfo','local_payment'); ?></th>
                    <td class="text-left">
                        <div class="rd-info">
                            <ul class="dash-list">
                                <li><?php echo get_string('errorcode','local_payment').': '. $data->errorCode;?></li>
                                <li><?php echo get_string('errortext1','local_payment').': '. $data->message;?></li>
                                <li><?php echo get_string('errortext2','local_payment').': '. $data->localMessage;?></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
                <?php } ?>
    </div>
    <div class="btn-area text-center">
        <input type="button"  value="<?php echo get_string('confirm','local_payment'); ?>" class="btns point w100 confirmpay" />
    </div>
<script>
    $(document).ready(function(){
       $('.popbg, .confirmpay, .pop-close').click(function(){
           location.reload();
       });
    });
</script>
</div>

<?php
echo $OUTPUT->footer();
?>


