<?php
global $DB,$USER,$CFG;
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$id  = optional_param('id',0, PARAM_INT);
$couponcode= optional_param('couponcode', '', PARAM_RAW);
if($couponcode!==''){
    $couponinfor= get_coupon_by_code1($couponcode);
}
$thistime=time();
$course=$DB->get_record('vi_coupons_courses',array('coupon_id'=>$couponinfor->coupon_id));
if($couponinfor){
    if($couponinfor->count>0){
        if($course->course_id==$id){
            if($couponinfor->expire_date>=$thistime &&$couponinfor->start_date<=$thistime){
                $responsedata = (object)[
                    'success' => true,
                    'isexpired'=>false,
                    'couponid'=>$couponinfor->coupon_id,
                    'outofcode'=>false,
                ];
                header('Content-type: application/json');
                echo json_encode($responsedata);
            }else{
                $responsedata = (object)[
                    'success' => true,
                    'isexpired'=>true,
                    'couponid'=>$couponinfor->coupon_id,
                    'outofcode'=>false,
                ];
                header('Content-type: application/json');
                echo json_encode($responsedata);
            }
        }
        else{
            $responsedata = (object)[
                'success' => false,
                'iscourse'=>false,
            ];
            header('Content-type: application/json');
            echo json_encode($responsedata);
        }
    }else{
        $responsedata = (object)[
            'success' => true,
            'outofcode'=>true,
        ];
        header('Content-type: application/json');
        echo json_encode($responsedata);
    }

}
else{
    $responsedata = (object)[
        'success' => false,
        'notfound'=>true,
    ];
    header('Content-type: application/json');
    echo json_encode($responsedata);
}


