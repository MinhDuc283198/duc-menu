<?php
session_start();
require 'restful_api.php';
require 'VideoStream.php';

class api extends restful_api {

	function __construct(){
		parent::__construct();
	}

	function login() {
		if ($this->method == 'POST'){
			$param = $this->params;
			$_SESSION['name'] = $param["name"];
			$_SESSION['email'] = $param["email"];
	
			$data = [
				"result" => "success",
				"data" => [
					"name" => $param["name"],
					"email" => $param["email"]
				]
			];
			$this->response(200, $data);
		} else {
			$dataError = [
				"result" => "error",
				"msg" => "This is function POST"
			];
			$this->response(200, $dataError);
		}
	}

	function logout() {
		if ($this->method == 'POST'){
			$param = $this->params;
			$_SESSION["email"] = "";
			$_SESSION["name"] = "";
			$_SESSION["isPayment"] = null;

			$data = [
				"result" => "success",
				"msg" => "Logout successful"
			];
			$this->response(200, $data);
		} else {
			$dataError = [
				"result" => "error",
				"msg" => "This is function POST"
			];
			$this->response(200, $dataError);
		}
	}

	function update_payment() {
		$_SESSION["isPayment"] = true;
		if(isset($_SERVER["SCRIPT_NAME"])) {
			$folder = str_replace("api/api.php","",$_SERVER["SCRIPT_NAME"]);
		} else {
			$folder = "";
		}
		
		$url = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].$folder ."#/success";
		header("Location: ".$url);
	}

	function user(){
		if ($this->method == 'GET'){
			$this->response(200, "abc");
			// Hãy viết code xử lý LẤY dữ liệu ở đây
			// trả về dữ liệu bằng cách gọi: $this->response(200, $data)
			
		}
		elseif ($this->method == 'POST'){
			$this->response(200, ["POST", "TEst"]);
			// Hãy viết code xử lý THÊM dữ liệu ở đây
			// trả về dữ liệu bằng cách gọi: $this->response(200, $data)
		}
		elseif ($this->method == 'PUT'){
			// Hãy viết code xử lý CẬP NHẬT dữ liệu ở đây
			// trả về dữ liệu bằng cách gọi: $this->response(200, $data)
		}
		elseif ($this->method == 'DELETE'){
			// Hãy viết code xử lý XÓA dữ liệu ở đây
			// trả về dữ liệu bằng cách gọi: $this->response(200, $data)
		}
	}

	function momo_pay() {
		if ($this->method == 'POST'){
			$notifyUrl=$_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"]."/update_payment";
			$config = parse_ini_file('config.ini'); 
			$param = $this->params;
			$orderId = "MOMO".time();
			$data_array =  array(
				"accessKey" => $config["ACCESS_KEY"],
				"amount" => $param["amount"],
				"extraData" => $param["extraData"],
				"notifyUrl"=> $notifyUrl,
				"orderId"=> $orderId,
				"orderInfo"=> $param["orderInfo"],
				"partnerCode"=> $config["PARTNER_CODE"],
				"requestId"=> $orderId,
				"requestType"=> $config["TYPE_CAPTURE"],
				"returnUrl"=> $notifyUrl,
				"signature"=> ""
			);
			$dataSig = "partnerCode=".$config["PARTNER_CODE"]."&accessKey=".$config["ACCESS_KEY"]."&requestId=".$orderId."&amount=".$param["amount"]."&orderId=".$orderId."&orderInfo=".$param["orderInfo"]."&returnUrl=".$notifyUrl."&notifyUrl=".$notifyUrl."&extraData=".$param["extraData"];
			$signature = hash_hmac ( "sha256" , $dataSig , $config["SECRET_KEY"]);
			$data_array["signature"] = $signature;
			$make_call = $this->callAPI('POST', $config["API_ENDPOINTL"], json_encode($data_array));
			$response = json_decode($make_call, true);
			$data = [
				"result" => "success",
				"data" => $response
			];
			
			$this->response(200, $data);
			
		}
		else {
			$dataError = [
				"result" => "error",
				"msg" => "This is function POST"
			];
			$this->response(200, $dataError);
		}
	}

	function momo_atm() {
		if ($this->method == 'POST'){
			$config = parse_ini_file('config.ini'); 
			$param = $this->params;
			$orderId = "MOMO".time();

			$data_array =  array(
				"partnerCode"=> $config["PARTNER_CODE"],
				"accessKey"=> $config["ACCESS_KEY"],
				"requestId"=> $orderId,
				"orderId"=> $orderId,
				"amount"=> $param["amount"],
				"orderInfo"=> $param["orderInfo"],
				"requestType"=> $config["TYPE_ATM"],
				"returnUrl"=> $param["returnUrl"],
				"notifyUrl"=> $param["notifyUrl"],
				"extraData"=> $param["extraData"],
				"signature"=> "",
				"bankCode"=> $param["bankCode"]
			);
			$dataSig = "partnerCode=".$config["PARTNER_CODE"]."&accessKey=".$config["ACCESS_KEY"]."&requestId=".$orderId."&bankCode=".$param["bankCode"]."&amount=".$param["amount"]."&orderId=".$orderId."&orderInfo=".$param["orderInfo"]."&returnUrl=".$param["returnUrl"]."&notifyUrl=".$param["notifyUrl"]."&extraData=".$param["extraData"]."&requestType=".$config["TYPE_ATM"];
			$signature = hash_hmac ( "sha256" , $dataSig , $config["SECRET_KEY"]);
			$data_array["signature"] = $signature;
			$make_call = $this->callAPI('POST', $config["API_ENDPOINTL"], json_encode($data_array));
			$response = json_decode($make_call, true);
			$data = [
				"result" => "success",
				"data" => $response
			];
			
			$this->response(200, $data);
			
		}
		else {
			$dataError = [
				"result" => "error",
				"msg" => "This is function POST"
			];
			$this->response(200, $dataError);
		}
	}
	function study() {
		// $isBuy = $_SESSION[$_SESSION["email"]]["isPayment"];
		if($_GET["id"] == 1) {
			$path= $_GET["id"].".mp4";
			$stream = new VideoStream($path);
			$stream->start();
		} else {
			if(isset($_SESSION["isPayment"])) {
				$path= $_GET["id"].".mp4";
				$stream = new VideoStream($path);
				$stream->start();
			} else {
				$this->response(200, "Please payment for view this video");
			}
		}
	}
	function get_status() {
		$data = [
			"result" => "success",
			"data" => [
				"isPayment" => $_SESSION["isPayment"]
			]
		];
		$this->response(200, $data);
	}
}

$user_api = new api();

?>
