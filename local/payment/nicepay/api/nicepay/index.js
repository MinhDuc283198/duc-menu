$("#va-btn").click(function() {
	$("#va-form").show();
	$("#cc-form").hide();
	$("#domestic-form").hide();
	$("#va-btn").addClass("active");
	$("#cc-btn").removeClass("active");
	$("#domestic-btn").removeClass("active");
});

$("#cc-btn").click(function() {
	$("#cc-form").show();
	$("#domestic-form").hide();
	$("#va-form").hide();  
	$("#va-btn").removeClass("active");
	$("#cc-btn").addClass("active");
	$("#domestic-btn").removeClass("active");
});

$("#domestic-btn").click(function() {
	  $("#va-form").hide();
	  $("#cc-form").hide();
	  $("#domestic-form").show();
	  $("#va-btn").removeClass("active");
	  $("#cc-btn").removeClass("active");
	  $("#domestic-btn").addClass("active");
});

