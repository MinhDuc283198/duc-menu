<?php
/*
 * ____________________________________________________________
 *
 * Copyright (C) 2016 NICE IT&T
 *
 * Please do not modify this module.
 * This module may used as it is, there is no warranty.
 *
 * @ description : PHP SSL Client module.
 * @ name        : NicepayLite.php
 * @ author      : NICEPAY I&T (tech@nicepay.co.kr)
 * @ date        :
 * @ modify      : 22.02.2016
 *
 * 2016.02.22 Update Log
 * Please contact it.support@ionpay.net for inquiry
 *
 * ____________________________________________________________
 */

include_once ('NicepayLogger.php');
include_once ('NicepayConfig.php');

class NicepayRequestor {
    public $sock = 0;
    public $apiUrl;
    public $port = 443;
    public $status;
    public $headers = "";
    public $body = "";
    public $request;
    public $errorcode;
    public $errormsg;
    public $log;
    public $timeout;

    public function __construct() {
        $this->log = new NicepayLogger();
    }

    public function operation($type) {

        if ($type == "requestVA") {
            $this->apiUrl = NICEPAY_REQ_VA_URL;
        } else if ($type == "creditCard") {
            $this->apiUrl = NICEPAY_REQ_CC_URL;
        } else if ($type == "checkPaymentStatus") {
            $this->apiUrl = NICEPAY_ORDER_STATUS_URL;
        } else if ($type == "cancelVA") {
            $this->apiUrl = NICEPAY_CANCEL_VA_URL;
        } else if ($type == "domesticCard") {
            $this->apiUrl = NICEPAY_REQ_DC_URL;
        } else if ($type == "cancel") {
            $this->apiUrl = NICEPAY_CANCEL_URL;
        } else if ($type == "deleteToken") {
            $this->apiUrl = NICEPAY_DELETE_TOKEN_URL;
        }
    }

    public function openSocket() {
        $host = parse_url($this->apiUrl, PHP_URL_HOST);
        $tryCount = 0;
        /* if (! $this->sock = @fsockopen ("ssl://".$host, $this->port, $errno, $errstr, NICEPAY_TIMEOUT_CONNECT )) {
            while ($tryCount < 5) {
                if ($this->sock = @fsockopen("ssl://".$host, $this->port, $errno, $errstr, NICEPAY_TIMEOUT_CONNECT )) {
                    return true;
                }
                sleep(2);
                $tryCount++;
            }
            $this->errorcode = $errno;
            switch ($errno) {
                case - 3 :
                    $this->errormsg = 'Socket creation failed (-3)';
                case - 4 :
                    $this->errormsg = 'DNS lookup failure (-4)';
                case - 5 :
                    $this->errormsg = 'Connection refused or timed out (-5)';
                default :
                    $this->errormsg = 'Connection failed (' . $errno . ')';
                    $this->errormsg .= ' ' . $errstr;
            }
            
            echo "<pre>";
            echo 'openSocket = false';
            echo "</pre>";
            return false;
        }
        return true; */
    
        if ($this->sock = @fopen (apiUrl, 'r')) {
            // echo "<pre>";
            // echo 'openSocket = true';
            // echo "</pre>";
            return true;
        }
        else if(! $this->sock = @fopen (apiUrl, 'r')){
            // echo "<pre>";
            // echo 'openSocket = false';
            // echo "</pre>";
            return false;
        }
    
    }
    
    public function getFromUrl($method, $data)
    {
        
        $postdata = $this->buildQueryString ($data);
        
        // echo "<pre>";
        // echo 'DATA = ' .$postdata;
        // echo "</pre>";
        
        $ch = curl_init();
        $agent = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)';
        
        $info = parse_url($this->apiUrl);
        // echo "<pre>";
        // echo 'apiUrl - host = ' .$info['host'];
        //echo 'apiUrl - port = ' .$info['port'];
        // echo 'apiUrl - path = ' .$info['path'];
        // echo "</pre>";
        
        $url = null;
        if( $info['port'] === null ) { //real
            $url = $info['scheme'] . '://' . $info['host']. $info['path'];
        }
        else {
            $url = $info['scheme'] . '://' . $info['host']. ':'.$info['port']. $info['path'];
        }   
        
        //$url = $info['scheme'] . '://' . $info['host']. $info['path'];
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

        // echo "<pre>";
        // echo 'URL = ' .$url;
        // echo "</pre>";
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);

        $res = curl_exec($ch);
        curl_close($ch);
        
        // echo "<pre>";
        // echo 'RESPONSE = ' .$res;
        // echo "</pre>";
        
        return $res;
    }

    public function apiRequest($data) {
        
        // echo "<pre>";
        // echo "apiRequest IN";
        // echo "</pre>";
        
        $host = parse_url($this->apiUrl, PHP_URL_HOST);
        $port = parse_url($this->apiUrl, PHP_URL_PORT);
        $uri = parse_url($this->apiUrl, PHP_URL_PATH);
        
        // echo "<pre>";
        // echo 'HOST = ' .$host ."\r\n";;
        // echo 'PORT = ' .$port ."\r\n";;
        // echo 'URI = ' .$uri;
        // echo "</pre>";
        
        $this->headers = "";
        $this->body = "";
        $postdata = $this->buildQueryString ($data);
        
        // echo "<pre>";
        // echo 'POST DATA = ' .$postdata;
        // echo "</pre>";

        /* Write */
        $request = "POST " . $uri . " HTTP/1.0\r\n";
        $request .= "Connection: close\r\n";
        $request .= "Host: " . $host . "\r\n";
        $request .= "Port: " . $port . "\r\n";
        $request .= "Content-type: application/x-www-form-urlencoded\r\n";
        $request .= "Content-length: " . strlen ( $postdata ) . "\r\n";
        $request .= "Accept: */*\r\n";
        $request .= "\r\n";
        $request .= $postdata . "\r\n";
        $request .= "\r\n";
        
        // echo "<pre>";
        // echo 'REQUEST = ' .$request;
        // echo "</pre>";
        
        if($this->sock) {
            
            // echo "<pre>";
            // echo 'isTrue ';
            // echo "</pre>";
            
            fwrite ( $this->sock, $request );

            /* Read */
            stream_set_blocking ($this->sock, FALSE);
            
            $atStart = true;
            $IsHeader = true;
            $timeout = false;
            $start_time = time ();
            while ( ! feof ($this->sock ) && ! $timeout) {
                
                $line = fgets ($this->sock, 4096);
                $diff = time () - $start_time;
                if ($diff >= NICEPAY_TIMEOUT_READ) {
                    
                    // echo "<pre>";
                    // echo 'READ TIMEOUT ';
                    // echo "</pre>";
                    
                    $timeout = true;
                }
                if ($IsHeader) {
                    if ($line == "") // for stream_set_blocking
                    {
                        continue;
                    }
                    if (substr ($line, 0, 2) == "\r\n") // end of header
                    {
                        $IsHeader = false;
                        continue;
                    }
                    $this->headers .= $line;
                    
                    if ($atStart) {
                        
                        // echo "<pre>";
                        // echo 'atStart is true ';
                        // echo "</pre>";
                        
                        $atStart = false;
                        if (! preg_match ( '/HTTP\/(\\d\\.\\d)\\s*(\\d+)\\s*(.*)/', $line, $m )) {
                            
                            // echo "<pre>";
                            // echo 'preg_match is false ';
                            // echo "</pre>";
                            
                            $this->errormsg = "Status code line invalid: " . htmlentities ( $line );
                            fclose ( $this->sock );
                            return false;
                        }
                        
                        // echo "<pre>";
                        // echo 'preg_match is true ';
                        // echo "</pre>";
                        
                        $http_version = $m [1];
                        $this->status = $m [2];
                        $status_string = $m [3];
                        continue;
                    }
                } else {
                    
                    // echo "<pre>";
                    // echo 'line = ' . $line;
                    // echo "</pre>";
                    
                    $this->body .= $line;
                }
            }
            // echo "<pre>";
            // echo 'isClosing ';
            // echo "</pre>";
            
            fclose ( $this->sock );

            if ($timeout) {
                $this->errorcode = NICEPAY_READ_TIMEOUT_ERR;
                $this->errormsg = "Socket Timeout(" . $diff . "SEC)";
                return false;
            }
            // return true
            if(!$this->parseResult($this->body)) {
                $this->body =   substr($this->body, 4);
    //            var_dump($this->body);
    //            exit();
                return $this->parseResult($this->body);
            }
            return $this->parseResult($this->body);
        } else {
            
            // echo "<pre>";
            // echo 'RESULT = FALSE';
            // echo "</pre>";
            
            //echo "Connection Timeout. Please retry.";
            return false;
        }
    }

    public function buildQueryString($data) {
        $querystring = '';
        if (is_array ($data)) {
            foreach ($data as $key => $val) {
                if (is_array ($val)) {
                    foreach ($val as $val2) {
                        if ($key != "key")
                            $querystring .= urlencode ($key) . '=' . urlencode ( $val2 ) . '&';
                    }
                    } else {
                    if ($key != "key")
                        $querystring .= urlencode ($key) . '=' . urlencode ($val) . '&';
                    }
            }
        $querystring = substr ($querystring, 0, - 1);
        } else {
            $querystring = $data;
        }
            return $querystring;
    }

    public function netCancel() {
        return true;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getBody() {
        return $this->body;
    }

    public function getHeaders() {
        return $this->headers;
    }

    public function getErrorMsg() {
        return $this->errormsg;
    }

    public function getErrorCode() {
        return $this->errorcode;
    }

    public function parseResult($result) {
        return json_decode($result);
    }
}