<?php
/*
 * ____________________________________________________________
 *
 * Copyright (C) 2016 NICE IT&T
 *
 *
 * This config file may used as it is, there is no warranty.
 *
 * @ description : PHP SSL Client module.
 * @ name        : NicepayLite.php
 * @ author      : NICEPAY I&T (tech@nicepay.co.kr)
 * @ date        :
 * @ modify      : 09.03.2019
 *
 * 09.03.2016 Update Log
 *
 * ____________________________________________________________
 */

// Please set the following
/**
* Check it!
* TODO : Must be edited to the merchant payment results URL.
*/
define("NICEPAY_CALLBACK_URL",      "$CFG->wwwroot"."/local/payment/init_card_result.php");  
//define("NICEPAY_CALLBACK_URL",      "$CFG->wwwroot"."/local/payment/nicepay/api/nicepay/result.html");  
/**
* Check it!
* TODO : Must be edited to the merchant notification URL.
*/
define("NICEPAY_DBPROCESS_URL",     "http://httpresponder.com/nicepay");          

/* TIMEOUT - Define as needed (in seconds) */
define( "NICEPAY_TIMEOUT_CONNECT", 15 );
define( "NICEPAY_TIMEOUT_READ", 25 );


// Please do not change
define("NICEPAY_IMID",              "NICESCTEST");                                                  // Merchant ID
define("NICEPAY_MERCHANT_KEY",      "33F49GnCMS1mFYlGXisbUDzVf2ATWCl9k3R++d5hDd3Frmuos/XLx8XhXpe+LDYAbpGKZYSwtlyyLOtS/8aD7A=="); // API Key
define("NICEPAY_PROGRAM",           "NicepayLite");
define("NICEPAY_VERSION",           "1.11");
define("NICEPAY_BUILDDATE",         "20190309");

//define("NICEPAY_REQ_CC_URL",        "https://www.nicepay.com.vn/vietnam_pg_was/api/orderRegist.do");     // Credit Card API URL
//define("NICEPAY_REQ_VA_URL",        "https://www.nicepay.com.vn/nicepay/api/onePass.do");                // Request Virtual Account API URL
//define("NICEPAY_CANCEL_VA_URL",     "https://www.nicepay.com.vn/nicepay/api/onePassAllCancel.do");       // Cancel Virtual Account API URL
//define("NICEPAY_ORDER_STATUS_URL",  "https://www.nicepay.com.vn/vietnam_pg_was/api/checkStatus.do");          // Check payment status URL
//define("NICEPAY_REQ_DC_URL",        "https://www.nicepay.com.vn/vietnam_pg_was/api/orderRegist.do");     // Domestic Card API URL
//define("NICEPAY_CANCEL_URL",        "https://www.nicepay.com.vn/vietnam_pg_was/api/cardAllCancel.do");   // Card Cancel API URL
//define("NICEPAY_DELETE_TOKEN_URL",  "https://www.nicepay.com.vn/vietnam_pg_was/api/deleteToken.do");   // Card Cancel API URL


define("NICEPAY_REQ_CC_URL",        "https://dev.nicepay.com.vn:15010/vietnam_pg_was/api/orderRegist.do");     // Credit Card API URL
define("NICEPAY_REQ_VA_URL",        "https://dev.nicepay.com.vn:15010/nicepay/api/onePass.do");                // Request Virtual Account API URL
define("NICEPAY_CANCEL_VA_URL",     "https://dev.nicepay.com.vn:15010/nicepay/api/onePassAllCancel.do");       // Cancel Virtual Account API URL
define("NICEPAY_ORDER_STATUS_URL",  "https://dev.nicepay.com.vn:15010/vietnam_pg_was/api/checkStatus.do");          // Check payment status URL
define("NICEPAY_REQ_DC_URL",        "https://dev.nicepay.com.vn:15010/vietnam_pg_was/api/orderRegist.do");     // Domestic Card API URL
define("NICEPAY_CANCEL_URL",        "https://dev.nicepay.com.vn:15010/vietnam_pg_was/api/cardAllCancel.do");   // Card Cancel API URL
define("NICEPAY_DELETE_TOKEN_URL",  "https://dev.nicepay.com.vn:15010/vietnam_pg_was/api/deleteToken.do");   // Card Cancel API URL

define("NICEPAY_READ_TIMEOUT_ERR",  "10200");

/* LOG LEVEL */
define("NICEPAY_LOG_CRITICAL", 1);
define("NICEPAY_LOG_ERROR", 2);
define("NICEPAY_LOG_NOTICE", 3);
define("NICEPAY_LOG_INFO", 5);
define("NICEPAY_LOG_DEBUG", 7);