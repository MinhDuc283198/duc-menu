# Contribution Guideline

Hello Team! First off, thanks for taking the time to contribute! :tada::+1:

## Step for Contributing
1. Fork the project to your repository
2. Commit and push to your repository
3. Always put a clear and proper comment while commit the code
4. Once the code passed build test, do merge request to Original Repository

## Bug Report and Feature Request

Contribution not always about code, you can always report bug and request greate feature

## Project Structure

1. Always create `README.md` and `CONTRIBUTING.md`
2. Put SDK/Module Documentation on `docs` folder

Hereby example project structure;

```
project
├── README.md
├── CONTRIBUTING.md
├── file001.txt    
└── lib
│   ├── file011.jar
│   ├── file012.jar
│   ├── ...
│   ├── src    
│   │   ├── file011.java
│   │   └── file012.java
├── docs
│   ├── doc1.pdf
│   ├── doc2.pdf
│   ├──  ...
│   ├── assets
│   │   └── images
│   │   │   img1.png
│   │   │   img2.png
│   │   │   ...
│   │   └── file022.txt
│   │
...
```

## Merge Request

1. Remove sensitive information (eg: `MID`, `Merchant Key`, etc.)
2. Merge only code that passed build test
3. Do not accept your merge request by yourself, even if you have access to original repository
4. Code Review while accepting merge request

## Project Wiki

Always put sensitive and important things in Project Wiki
