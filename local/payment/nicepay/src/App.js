import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import * as Momo from "./api/index.js/index.js";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.captureMoMoWallet = this.captureMoMoWallet.bind(this);
  }

  componentDidMount() {
    this.captureMoMoWallet();
  }

  async captureMoMoWallet() {
    let params = {
      accessKey: "Y9PbwlDR4kLoVCpD",
      partnerCode: "MOMOYZNA20191118",
      requestType: "captureMoMoWallet",
      notifyUrl: "https://momo.vn",
      returnUrl: "https://momo.vn",
      orderId: "MM1540456472575",
      amount: "150000",
      orderInfo: "SDK team.",
      requestId: "MM1540456472575",
      extraData: "email=abc@gmail.com",
      signature:
        "4f183ef11f17771bf36b049403d420ce1ad5c610a6045b30831ae574f7c2f78e"
    };

    let headers = new Headers();

    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");

    // headers.append("Access-Control-Allow-Origin", "http://localhost:3000");
    // headers.append("Access-Control-Allow-Credentials", "true");

    // headers.append("GET", "POST", "OPTIONS");
    let response = await Momo.captureMoMoWallet(params);
    // fetch("/gw_payment/transactionProcessor", {
    //   method: "post",
    //   body: JSON.stringify(params),
    //   headers: headers
    // })
    //   .then(function(response) {
    //     return response.json();
    //   })
    //   .then(function(data) {
    //     console.log("response", data);
    //   });
  }

  render() {
    return <div>ABCC</div>;
  }
}

// export default App;

const routes = [
  {
    path: "/",
    exact: true,
    component: Home,
    title: () => "Home"
  },
  {
    path: "/cart",
    component: Cart,
    title: () => "Cart"
  },
  {
    path: "/buy",
    component: BuyTicket,
    title: () => "Buy Ticket"
  },
  {
    path: "/my",
    component: MyTicket,
    title: () => "My Tickets"
  }
];

export default function Main() {

  return (
    <Router>
      <div style={{ display: "flex" }}>
        <div
          style={{
            padding: "10px",
            width: "40%",
            background: "#f0f0f0"
          }}
        >
          <ul style={{ listStyleType: "none", padding: 0 }}>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/bubblegum">Bubblegum</Link>
            </li>
            <li>
              <Link to="/shoelaces">Shoelaces</Link>
            </li>
          </ul>

          <Switch>
            {routes.map((route, index) => (
              // You can render a <Route> in as many places
              // as you want in your app. It will render along
              // with any other <Route>s that also match the URL.
              // So, a sidebar or breadcrumbs or anything else
              // that requires you to render multiple things
              // in multiple places at the same URL is nothing
              // more than multiple <Route>s.
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                children={<route.sidebar />}
              />
            ))}
          </Switch>
        </div>

        <div style={{ flex: 1, padding: "10px" }}>
          <Switch>
            {routes.map((route, index) => (
              // Render more <Route>s with the same paths as
              // above, but different components this time.
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                children={<route.main />}
              />
            ))}
          </Switch>
        </div>
      </div>
    </Router>
}
