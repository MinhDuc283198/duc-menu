const auth = (state = {}, action) => {
  switch (action.type) {
    case "SIGN_IN":
      return {
        ...state,
        user: action.auth
      };

    default:
      return state;
  }
};

export default auth;
