import React from "react";
import Home from "screens/Home";
import Checkout from "screens/Checkout";
import PaymentSuccess from "screens/PaymentSuccess";
import SignIn from "screens/SignIn";
import Dashboard from "screens/Dashboard";
import Detail from "screens/Detail";
import Study from "screens/Study";
import Helpdesk from "screens/Helpdesk";
import HelpdeskNicepay from "screens/HelpdeskNicepay";

const routes = [
  {
    path: "/",
    exact: true,
    component: Dashboard,
    title: "Dashboard"
  },
  {
    path: "/buy",
    component: Home,
    title: "Home"
  },
  {
    path: "/checkout",
    component: Checkout,
    title: "Checkout"
  },
  {
    path: "/sign-in",
    component: SignIn,
    title: "SignIn"
  },
  {
    path: "/my",
    component: Home,
    title: "My Tickets"
  },
  {
    path: "/detail/:id",
    component: Detail,
    title: "Detail"
  },
  {
    path: "/study",
    component: Study,
    title: "Study"
  },
  {
    path: "/success",
    component: PaymentSuccess,
    title: "success"
  },
  {
    path: "/helpdesk",
    component: Helpdesk,
    title: "Helpdesk"
  },
  {
    path: "/helpdesk-nicepay",
    component: HelpdeskNicepay,
    title: "Helpdesk Nicepay"
  }
];

export { routes };
