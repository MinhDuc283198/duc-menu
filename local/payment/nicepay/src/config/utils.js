import HmacSHA256 from "crypto-js/hmac-sha256";
import { momo, requestType } from "config";

const createSignature = (params, type) => {
  // let keys = Object.keys(params);
  // let items = keys.map(key => {
  //   return `${key}=${params[key]}`;
  // });
  // let data = items.join("&");
  // console.log("signature", data);
  let {
    partnerCode,
    accessKey,
    requestId,
    amount,
    orderId,
    orderInfo,
    returnUrl,
    notifyUrl,
    extraData,
    bankCode,
    requestType
  } = params;
  let data = "";
  console.log("type", type);
  switch (type) {
    case "captureMoMoWallet":
      data = `partnerCode=${partnerCode}&accessKey=${accessKey}&requestId=${requestId}&amount=${amount}&orderId=${orderId}&orderInfo=${orderInfo}&returnUrl=${returnUrl}&notifyUrl=${notifyUrl}&extraData=${extraData}`;
      break;
    case "payWithMoMoATM":
      data = `partnerCode=${partnerCode}&accessKey=${accessKey}&requestId=${requestId}&bankCode=${bankCode}&amount=${amount}&orderId=${orderId}&orderInfo=${orderInfo}&returnUrl=${returnUrl}&notifyUrl=${notifyUrl}&extraData=${extraData}&requestType=${requestType}`;
      break;
    case "transactionStatus":
      data = `partnerCode=${partnerCode}&accessKey=${accessKey}&requestId=${requestId}&orderId=${orderId}&requestType=${requestType}`;
      break;
    default:
      break;
  }

  console.log("data", data);

  let signature = HmacSHA256(data, momo.SECRET_KEY);

  return signature.toString();
};

export { createSignature };
