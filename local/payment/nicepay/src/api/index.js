import * as AxiosApp from "./axiosApp";

const configEmart = {
  //   "Content-Type": "application/json",
  //   "Access-Control-Allow-Origin": "https://test-payment.momo.vn",
  //   "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS",
  //   "Access-Control-Allow-Headers":
  //     "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
};

const URL = {
  payUrl: "/gw_payment/transactionProcessor",
  momoPay: "/api/api.php/momo_pay",
  momoAtm: "/api/api.php/momo_atm",
  login: "/api/api.php/login",
  logout: "/api/api.php/logout",
  getStatus: "/api/api.php/get_status",
  nicePayD: "api/nicepay/checkout.php"
};

export const captureMoMoWallet = params => AxiosApp.post(URL.payUrl, params);
export const momoPay = params => AxiosApp.post(URL.momoPay, params);
export const momoAtm = params => AxiosApp.post(URL.momoAtm, params);
export const login = params => AxiosApp.post(URL.login, params);
export const logout = params => AxiosApp.post(URL.logout, params);
export const getStatus = params => AxiosApp.post(URL.getStatus, params);
export const nicePayD = params => AxiosApp.post(URL.nicePayD, params);
