import axios from "axios";
import { apiConfig } from "config";
const qs = require("qs");
axios.defaults.baseURL = apiConfig.url;
// axios.defaults.headers["Content-Type"] = "application/json;charset=UTF-8";
axios.defaults.headers["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers["X-Requested-With"] = "XMLHttpRequest";
axios.defaults.timeout = 90000;
axios.defaults.headers["Content-Type"] = "application/x-www-form-urlencoded";
axios.defaults.headers["Accept"] = "application/json";
axios.defaults.headers["Cache-Control"] = "no-cache";
axios.defaults.headers["Access-Control-Allow-Credentials"] = true;
axios.defaults.headers["Access-Control-Allow-Headers"] =
  "Origin, X-Requested-With, Content-Type, Accept";
axios.defaults.withCredentials = true;
axios.defaults.credentials = "include";
axios.defaults.headers["Access-Control-Allow-Origin"] =
  "http://global3.hanbiro.com/thai/gw/";

axios.interceptors.request.use(
  function(config) {
    if (config.method == "post") {
      config.data = qs.stringify(config.data);
    }

    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

export const post = (url, data, config = {}) => {
  let newConfig = {
    method: "post",
    url,
    data,
    ...config
  };

  return axios(newConfig)
    .then(response => {
      console.log("POST_SUCCESS", response);
      return response;
    })
    .catch(error => {
      console.log(error);
    });
};
