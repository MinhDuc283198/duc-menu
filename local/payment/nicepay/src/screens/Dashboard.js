import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Images from "config/image";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: 20
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  //   root: {
  //     display: "flex",
  //     flexWrap: "wrap",
  //     justifyContent: "space-around",
  //     overflow: "hidden",
  //     backgroundColor: theme.palette.background.paper
  //   },
  gridList: {
    // width: 500,
    // height: 450
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)"
  },
  card: {
    maxWidth: 345,
    height: 300,
    margin: 5
    // padding: 5
  },
  media: {
    height: 140
  }
}));

const tileData = [
  {
    img: Images.react,
    title: "Learn ReactJS ",
    author: "Nguyen Van A",
    price: "15,000"
  },
  {
    img: Images.n3,
    title: "N3",
    author: "Nguyen Van B",
    price: 5
  },
  {
    img: Images.nguvan,
    title: "Literature",
    author: "Nguyen Van C",
    price: 11
  },
  {
    img: Images.tienganh,
    title: "English",
    author: "Nguyen Van D",
    price: 12
  },
  {
    img: Images.toan,
    title: "Math",
    author: "Nguyen Van E",
    price: 3
  },
  {
    img: Images.tv,
    title: "Vietnamese book",
    author: "Nguyen Van F",
    price: 9
  }
];

function MediaCard(props) {
  const classes = useStyles();
  let history = useHistory();
  let { img, title, author, price } = props.data;

  const goDetail = () => {
    history.push("/detail/1");
  };

  return (
    <Card className={classes.card} onClick={goDetail}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={img}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Author: ${author}`}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            style={{ color: "#f50057" }}
          >
            {`Price: $${price}`}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={() => goDetail()}>
          View More
        </Button>
      </CardActions>
    </Card>
  );
}

export default function TitlebarGridList() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <GridList cellHeight={150} className={classes.gridList} cols={3}>
        {tileData.map((data, index) => (
          <GridListTile key={index} style={{ height: 320, padding: 2 }}>
            <MediaCard data={data} />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
  //   return (
  //     <div className={classes.root}>
  //       <Grid container spacing={3}>
  //         <Grid item xs>
  //           <MediaCard />
  //         </Grid>
  //         <Grid item xs>
  //           <Paper className={classes.paper}>xs</Paper>
  //         </Grid>
  //         <Grid item xs>
  //           <Paper className={classes.paper}>xs</Paper>
  //         </Grid>
  //       </Grid>
  //     </div>
  //   );
}
