import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Images from "config/image";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useLocation,
  Link as LinkR,
  useHistory,
  useParams
} from "react-router-dom";

const useStyles = makeStyles({
  card: {
    paddingTop: 15,
    paddingBottom: 15,
    textAlign: "center"
  },
  media: {
    // width: 130,
    backgroundSize: "contain",
    height: 130,
    margin: 0
    // textAlign: "center",
    // justify: "center",
    // alignContent: "center"
  }
});

export default function MediaCard(props) {
  const classes = useStyles();
  const history = useHistory();

  return (
    <Card className={classes.card}>
      <CardActionArea style={{ textAlign: "center" }}>
        <CardMedia
          className={classes.media}
          image={Images.success}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Thank you for your order.
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Your order number is #{props.orderId}. We have emailed your order
            confirmation, and will send you an update when your order has
            shipped.
          </Typography>
        </CardContent>
      </CardActionArea>

      <Button
        variant="contained"
        size="small"
        color="primary"
        onClick={() => history.push("/")}
      >
        Done
      </Button>
      {/* <Button size="small" color="primary">
          Learn More
        </Button> */}
    </Card>
  );
}
