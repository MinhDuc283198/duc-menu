import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useLocation,
  useHistory,
  Link as LinkR
} from "react-router-dom";
import { routes } from "router";
import { connect } from "react-redux";
import { signIn } from "actions";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import * as Api from "api";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    },
    ul: {
      margin: 0,
      padding: 0
    },
    li: {
      listStyle: "none"
    }
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  toolbar: {
    flexWrap: "wrap"
  },
  toolbarTitle: {
    flexGrow: 1
  },
  link: {
    margin: theme.spacing(1, 1.5)
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6)
  },
  cardHeader: {
    backgroundColor: theme.palette.grey[200]
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2)
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6)
    }
  },
  user: {
    color: "#3f51b5"
  }
}));

const tiers = [
  {
    title: "Free",
    price: "0",
    description: [
      "10 users included",
      "2 GB of storage",
      "Help center access",
      "Email support"
    ],
    buttonText: "Sign up for free",
    buttonVariant: "outlined"
  },
  {
    title: "Pro",
    subheader: "Most popular",
    price: "15",
    description: [
      "20 users included",
      "10 GB of storage",
      "Help center access",
      "Priority email support"
    ],
    buttonText: "Get started",
    buttonVariant: "contained"
  },
  {
    title: "Enterprise",
    price: "30",
    description: [
      "50 users included",
      "30 GB of storage",
      "Help center access",
      "Phone & email support"
    ],
    buttonText: "Contact us",
    buttonVariant: "outlined"
  }
];
const footers = [
  {
    title: "Company",
    description: ["Team", "History", "Contact us", "Locations"]
  },
  {
    title: "Features",
    description: [
      "Cool stuff",
      "Random feature",
      "Team feature",
      "Developer stuff",
      "Another one"
    ]
  },
  {
    title: "Resources",
    description: [
      "Resource",
      "Resource name",
      "Another resource",
      "Final resource"
    ]
  },
  {
    title: "Legal",
    description: ["Privacy policy", "Terms of use"]
  }
];

// function usePageViews() {
//   let location = useLocation();
//   console.log(location);
//   //   React.useEffect(() => {
//   //     ga.send(["pageview", location.pathname]);
//   //   }, [location]);
// }

function PreventingTransitionsExample() {
  return (
    <Router>
      <CssBaseline />
      <ul>
        <li>
          <LinkR to="/">Form</LinkR>
        </li>
        <li>
          <LinkR to="/one">One</LinkR>
        </li>
        <li>
          <LinkR to="/two">Two</LinkR>
        </li>
      </ul>

      <Switch>
        <Route path="/" exact children={<h3>3</h3>} />
        <Route path="/one" children={<h3>One</h3>} />
        <Route path="/two" children={<h3>Two</h3>} />
      </Switch>
    </Router>
  );
}

const Pricing = props => {
  let history = useHistory();
  const classes = useStyles();
  let { auth } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const signOut = () => {
    let params = {
      name: auth.user.name,
      email: auth.user.email
    };
    Api.logout(params).then(response => {
      let { signIn } = props;
      signIn({});
      handleClose();
    });
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar
        position="static"
        color="default"
        elevation={0}
        className={classes.appBar}
      >
        <Toolbar className={classes.toolbar}>
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
            onClick={() => history.push("/")}
          >
            Company name
          </Typography>
          <nav>
            <Button
              color="primary"
              variant="outlined"
              className={classes.link}
              onClick={() => history.push("/helpdesk")}
            >
              Helpdesk Momo
            </Button>
            <Button
              color="primary"
              variant="outlined"
              className={classes.link}
              onClick={() => history.push("/helpdesk-nicepay")}
            >
              Helpdesk NicePay
            </Button>
            {/* <Link
              variant="button"
              color="textPrimary"
              // href="#"
              className={classes.link}
              onClick={() => history.push("/helpdesk")}
            >
              Helpdesk
            </Link> */}
          </nav>
          {auth.user && auth.user.hasOwnProperty("name") ? (
            <div>
              <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                endIcon={<ArrowDropDownIcon />}
              >
                {`${auth.user.name}`}
              </Button>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={signOut}>Logout</MenuItem>
              </Menu>
            </div>
          ) : (
            <Button
              color="primary"
              variant="outlined"
              className={classes.link}
              onClick={() => history.push("/sign-in")}
            >
              Login
            </Button>
          )}
        </Toolbar>
      </AppBar>
      {/* Hero unit */}

      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Switch>
          {routes.map((route, index) => (
            <Route
              exact={index == 0}
              key={index}
              path={route.path}
              children={<route.component />}
            />
          ))}
        </Switch>
      </Container>
      {/* Footer */}
      <Container maxWidth="md" component="footer" className={classes.footer}>
        <Grid container spacing={4} justify="space-evenly">
          {footers.map(footer => (
            <Grid item xs={6} sm={3} key={footer.title}>
              <Typography variant="h6" color="textPrimary" gutterBottom>
                {footer.title}
              </Typography>
              <ul>
                {footer.description.map(item => (
                  <li key={item}>
                    <Link href="#" variant="subtitle1" color="textSecondary">
                      {item}
                    </Link>
                  </li>
                ))}
              </ul>
            </Grid>
          ))}
        </Grid>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
      {/* End footer */}
    </React.Fragment>
  );
};

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  signIn: auth => dispatch(signIn(auth))
});

export default connect(mapStateToProps, mapDispatchToProps)(Pricing);
