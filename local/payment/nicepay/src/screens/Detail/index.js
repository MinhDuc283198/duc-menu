import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import CardMedia from "@material-ui/core/CardMedia";
import Images from "config/image";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import { withStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import FolderIcon from "@material-ui/icons/Folder";
import DeleteIcon from "@material-ui/icons/Delete";
import Button from "@material-ui/core/Button";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import PersonIcon from "@material-ui/icons/Person";
import QueryBuilderIcon from "@material-ui/icons/QueryBuilder";
import { useHistory, useParams } from "react-router-dom";
import { routes } from "router";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import ReactPlayer from "react-player";
import { apiConfig } from "config";
import * as Api from "api";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3, 2)
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  square: {
    width: "90%",
    height: 300,
    margin: 10
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}));

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

const ExpansionPanel = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    },
    "&$expanded": {
      margin: "auto"
    }
  },
  expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56
    }
  },
  content: {
    "&$expanded": {
      margin: "12px 0"
    }
  },
  expanded: {}
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiExpansionPanelDetails);

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1)
  }
}))(MuiDialogActions);

const items = [
  {
    id: 1,
    title: "Intro tO JSX (03: 00)",
    isFree: true
  },
  {
    id: 1,
    title: "Advanced JSX (09: 00)",
    isFree: true
  },
  {
    id: 2,
    title: "Curly Braces in JSX (04: 00)",
    isFree: false
  },
  {
    id: 2,
    title: "Variables in JSX (03: 00)",
    isFree: false
  }
];

function ItemStudy(props) {
  let { title, isFree } = props.item;
  let { handleClickOpen, pay } = props;

  return (
    <ListItem button key={title}>
      <ListItemIcon>
        <PlayCircleOutlineIcon />
      </ListItemIcon>
      <ListItemText primary={title} style={{ flex: 1 }} />
      <ListItemSecondaryAction>
        {pay ? (
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={handleClickOpen}
          >
            View
          </Button>
        ) : isFree ? (
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={handleClickOpen}
          >
            Free
          </Button>
        ) : (
          <Button
            size="small"
            variant="contained"
            color="secondary"
            onClick={() => props.onBuy()}
          >
            Buy
          </Button>
        )}
      </ListItemSecondaryAction>
    </ListItem>
  );
}

const Main = props => {
  let { id } = useParams();
  const history = useHistory();
  const classes = useStyles();
  const [pay, setPay] = React.useState(false);
  const [expanded, setExpanded] = React.useState("panel1");
  const [dense, setDense] = React.useState(false);
  const [secondary, setSecondary] = React.useState(false);
  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const { auth } = props;
  const [open, setOpen] = React.useState(false);
  let urlC = apiConfig.url + "api/api.php/study?id=" + id;
  const [url, setUrl] = React.useState(urlC);
  const handleClickOpen = item => {
    // let urlCNew = apiConfig.url + "api/api.php/study?" + item.id;
    // setUrl(urlCNew);
    history.push("/detail/" + item.id);
    // window.location.href = window.location.href;
    window.location.reload(false);
    // setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    Api.getStatus().then(response => {
      if (response.data && response.data.data) {
        setPay(response.data.data.isPayment);
      }
    });
  });

  const onBuy = () => {
    if (auth.user && auth.user.hasOwnProperty("name")) {
      let tier = {
        title: "Learn ReactJS",
        price: "15000",
        description: [
          "ReactJS presents graceful solutions to some of front-end programming’s most persistent issues. It’s fast, scalable, flexible, powerful, and has a robust developer community that’s rapidly growing. There’s never been a better time to learn React."
        ],
        buttonText: "Sign up for free",
        buttonVariant: "outlined"
      };
      // history.push("/checkout", { tier: tier });
      history.push({
        pathname: "/checkout",
        state: { tier: tier }
      });
    } else {
      history.push("/sign-in");
    }
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Paper className={classes.root}>
            <Typography variant="h5" component="h3">
              Learn ReactJS
            </Typography>
            <ReactPlayer
              className="react-player"
              width="100%"
              height="100%"
              url={url}
              pip={false}
              playing={true}
              controls={true}
              light={false}
              loop={false}
            />
            <Typography component="p">
              ReactJS presents graceful solutions to some of front-end
              programming’s most persistent issues. It’s fast, scalable,
              flexible, powerful, and has a robust developer community that’s
              rapidly growing. There’s never been a better time to learn React.
            </Typography>

            <Typography variant="h5" component="h3">
              WHAT YOU'LL LEARN
            </Typography>

            <ExpansionPanel
              square
              expanded={expanded === "panel1"}
              onChange={handleChange("panel1")}
            >
              <ExpansionPanelSummary
                aria-controls="panel1d-content"
                id="panel1d-header"
                expandIcon={<ExpandMoreIcon />}
              >
                <Typography>JSX</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <div className={classes.demo}>
                      <List dense={dense}>
                        {items.map((item, index) => (
                          <ItemStudy
                            item={item}
                            onBuy={onBuy}
                            key={index}
                            handleClickOpen={() => handleClickOpen(item)}
                            pay={pay}
                          />
                        ))}
                      </List>
                    </div>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel
              square
              expanded={expanded === "panel2"}
              onChange={handleChange("panel2")}
            >
              <ExpansionPanelSummary
                aria-controls="panel2d-content"
                id="panel2d-header"
                expandIcon={<ExpandMoreIcon />}
              >
                <Typography>React Components</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Learn to make React Components, the building blocks of all
                  React.js applications
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel
              square
              expanded={expanded === "panel3"}
              onChange={handleChange("panel3")}
            >
              <ExpansionPanelSummary
                aria-controls="panel3d-content"
                id="panel3d-header"
                expandIcon={<ExpandMoreIcon />}
              >
                <Typography>Components Interacting</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  Learn how to make React components interact with one another
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image="/static/images/cards/contemplative-reptile.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  $15,000
                </Typography>
                {pay ? null : (
                  <Button
                    variant="contained"
                    color="secondary"
                    style={{ width: "100%" }}
                    onClick={onBuy}
                  >
                    BUY
                  </Button>
                )}
                <List component="nav" aria-label="main mailbox folders">
                  <ListItem button>
                    <ListItemIcon>
                      <PersonIcon />
                    </ListItemIcon>
                    <ListItemText primary="Nguyen Van A" />
                  </ListItem>
                  <ListItem button>
                    <ListItemIcon>
                      <QueryBuilderIcon />
                    </ListItemIcon>
                    <ListItemText primary="124 hours" />
                  </ListItem>
                </List>
              </CardContent>
            </CardActionArea>
            {/* <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions> */}
          </Card>
        </Grid>
        {/* <video controls="" autoplay="" name="media">
              <source
                src="http://global3.hanbiro.com/thai/gw/api/api.php/study"
                type="video/mp4"
              />
            </video> */}
      </Grid>

      <div>
        {/* <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
          Open dialog
        </Button> */}
        <Dialog
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogTitle id="customized-dialog-title" onClose={handleClose}>
            Learn ReactJS
          </DialogTitle>
          <DialogContent>
            <ReactPlayer
              className="react-player"
              width="100%"
              height="100%"
              url={url}
              pip={false}
              playing={true}
              controls={true}
              light={false}
              loop={false}
            />
            <Typography gutterBottom>
              ReactJS presents graceful solutions to some of front-end
              programming’s most persistent issues. It’s fast, scalable,
              flexible, powerful, and has a robust developer community that’s
              rapidly growing. There’s never been a better time to learn React.
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleClose} color="primary">
              Done
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  // signIn: auth => dispatch(signIn(auth))
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
