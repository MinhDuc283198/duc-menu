import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

export default function AddressForm(props) {
  let { title, price, description } = props.tier;
  const [firstName, setFirstName] = useState("Thai");
  const [lastName, setLastName] = useState("Nguyen");
  const [address1, setAddress1] = useState(
    "85 Hoang Van Thai St., Tan Phu Ward, Dist 7, HCMC, Vietnam"
  );
  const [address2, setAddress2] = useState(
    "18th Fr. KOTRA 3-3-2 Kasmigaseki Chiyoda-ku, Tokyo, Japan"
  );
  const [city, setCity] = useState("Ho Chi Minh");
  const [state, setState] = useState("Dist 7");
  const [zip, setZip] = useState("700000");
  const [country, setCountry] = useState("Viet Nam");
  const [saveAddress, setSaveAddress] = useState(true);

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Information Bill
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            disabled
            required
            id="firstName"
            name="firstName"
            label="Title"
            value={title}
            fullWidth
            autoComplete="fname"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            disabled
            required
            id="firstName"
            name="firstName"
            label="Price"
            value={price}
            fullWidth
            autoComplete="fname"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            disabled
            style={{ width: "100%" }}
            id="outlined-multiline-static"
            label="Description"
            multiline
            rows="4"
            value={description}
            margin="normal"
            variant="outlined"
          />
        </Grid>
      </Grid>
      <Typography variant="h6" gutterBottom>
        Shipping address
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="firstName"
            name="firstName"
            label="First name"
            fullWidth
            autoComplete="fname"
            value={firstName}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="Last name"
            fullWidth
            autoComplete="lname"
            value={lastName}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="address1"
            name="address1"
            label="Address line 1"
            fullWidth
            autoComplete="billing address-line1"
            value={address1}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="address2"
            name="address2"
            label="Address line 2"
            fullWidth
            autoComplete="billing address-line2"
            value={address2}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="city"
            name="city"
            label="City"
            fullWidth
            autoComplete="billing address-level2"
            value={city}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            id="state"
            name="state"
            label="State/Province/Region"
            fullWidth
            value={state}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="zip"
            name="zip"
            label="Zip / Postal code"
            fullWidth
            autoComplete="billing postal-code"
            value={zip}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="country"
            name="country"
            label="Country"
            fullWidth
            autoComplete="billing country"
            value={country}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={
              <Checkbox
                color="secondary"
                name="saveAddress"
                value={saveAddress}
                checked={saveAddress}
              />
            }
            label="Use this address for payment details"
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
