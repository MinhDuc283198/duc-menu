import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Images from "config/image";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    width: "100%"
    // height: 450
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)"
  },
  avatar: {
    // margin: 10
    width: "100%",
    // height: 65
    height: "auto",
    padding: 5,
    borderWidth: 1
  },
  avatarItem: {
    // margin: 10
    width: 50,
    // height: 65
    height: "auto",
    padding: 5,
    borderWidth: 1
  }
}));

const tileData = [
  {
    logo: Images.VIB,
    name: "Image",
    code: "VIB"
  },
  {
    logo: Images.ABB,
    name: "Image",
    code: "ABB"
  },
  {
    logo: Images.STB,
    name: "Image",
    code: "STB"
  },
  {
    logo: Images.MSB,
    name: "Image",
    code: "MSB"
  },

  {
    logo: Images.NVB,
    name: "Image",
    code: "NVB"
  },
  {
    logo: Images.CTG,
    name: "Image",
    code: "CTG"
  },
  {
    logo: Images.DAB,
    name: "Image",
    code: "DAB"
  },
  {
    logo: Images.HDB,
    name: "Image",
    code: "HDB"
  },
  {
    logo: Images.VAB,
    name: "Image",
    code: "VAB"
  },
  {
    logo: Images.VPB,
    name: "Image",
    code: "VPB"
  },
  {
    logo: Images.ACB,
    name: "Image",
    code: "ACB"
  },
  {
    logo: Images.BVB,
    name: "Image",
    code: "BVB"
  },
  {
    logo: Images.KLB,
    name: "Image",
    code: "KLB"
  },
  {
    logo: Images.VCB,
    name: "Image",
    code: "VCB"
  },
  {
    logo: Images.MB,
    name: "Image",
    code: "MB"
  },
  {
    logo: Images.GPB,
    name: "Image",
    code: "GPB"
  },
  {
    logo: Images.EIB,
    name: "Image",
    code: "EIB"
  },
  {
    logo: Images.OJB,
    name: "Image",
    code: "OJB"
  },
  {
    logo: Images.NASB,
    name: "Image",
    code: "NASB"
  },
  {
    logo: Images.OCB,
    name: "Image",
    code: "OCB"
  },
  {
    logo: Images.LPB,
    name: "Image",
    code: "LPB"
  },
  {
    logo: Images.TPB,
    name: "Image",
    code: "TPB"
  },
  {
    logo: Images.SEAB,
    name: "Image",
    code: "SEAB"
  },
  {
    logo: Images.VARB,
    name: "Image",
    code: "VARB"
  },
  {
    logo: Images.BIDV,
    name: "Image",
    code: "BIDV"
  },
  {
    logo: Images.SHB,
    name: "Image",
    code: "SHB"
  },
  {
    logo: Images.SCB,
    name: "Image",
    code: "SCB"
  },
  {
    logo: Images.TCB,
    name: "Image",
    code: "TCB"
  }
];

export default function PaymentForm(props) {
  const classes = useStyles();
  const [typeMomo, setTypeMomo] = useState("aio");
  const [code, setCode] = useState("VIB");

  useEffect(() => {
    let data = {
      typeMomo: typeMomo,
      bankCode: code
    };
    props.dataPayment(data);
  });

  const handleListItemClick = (event, index) => {
    setTypeMomo(index);
  };

  const updateCode = code => {
    setCode(code);
  };

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Payment method
      </Typography>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem
          button
          selected={typeMomo === "aio"}
          onClick={event => handleListItemClick(event, "aio")}
        >
          <ListItemIcon>
            <Avatar
              src={Images.momo}
              className={classes.avatarItem}
              variant={"square"}
            />
          </ListItemIcon>
          <ListItemText primary="Momo Wallet " />
        </ListItem>
        {/* <ListItem
          button
          selected={typeMomo === "atm"}
          onClick={event => handleListItemClick(event, "atm")}
        >
          <ListItemIcon>
            <Avatar
              src={Images.momo}
              className={classes.avatarItem}
              variant={"square"}
            />
          </ListItemIcon>
          <ListItemText primary="MoMo ATM Card, internet banking of Vietnam banks." />
        </ListItem> */}
        <ListItem
          button
          selected={typeMomo === "domestic"}
          onClick={event => handleListItemClick(event, "domestic")}
        >
          <ListItemIcon>
            <Avatar
              src={Images.atm_card}
              className={classes.avatarItem}
              variant={"square"}
            />
          </ListItemIcon>
          <ListItemText primary="Domestic card (ATM Card)" />
        </ListItem>
        <ListItem
          button
          selected={typeMomo === "international"}
          onClick={event => handleListItemClick(event, "international")}
        >
          <ListItemIcon>
            <Avatar
              src={Images.internation_card}
              className={classes.avatarItem}
              variant={"square"}
            />
          </ListItemIcon>
          <ListItemText primary="International card (Visa, Master, Amex, JCB)" />
        </ListItem>
      </List>
      {typeMomo == "atm" ? (
        <GridList cellHeight={180} className={classes.gridList} cols={8}>
          {tileData.map(bank => (
            <GridListTile
              key={bank.code}
              style={{ height: "auto" }}
              onClick={() => updateCode(bank.code)}
            >
              <Box
                border={bank.code == code ? 2 : 0}
                borderColor={bank.code == code ? "red" : "white"}
              >
                <Avatar
                  src={bank.logo}
                  className={classes.avatar}
                  variant={"square"}
                />
              </Box>
            </GridListTile>
          ))}
        </GridList>
      ) : null}
      {/* <GridListTile key="Subheader" cols={2} style={{ height: "auto" }}>
        <ListSubheader component="div">December</ListSubheader>
      </GridListTile> */}
      {/* <Typography variant="h6" gutterBottom>
        Card information
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField required id="cardName" label="Name on card" fullWidth />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField required id="cardNumber" label="Card number" fullWidth />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField required id="expDate" label="Expiry date" fullWidth />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cvv"
            label="CVV"
            helperText="Last three digits on signature strip"
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox color="secondary" name="saveCard" value="yes" />}
            label="Remember credit card details for next time"
          />
        </Grid>
      </Grid> */}
    </React.Fragment>
  );
}
