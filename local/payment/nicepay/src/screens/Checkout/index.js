import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import AddressForm from "./AddressForm";
import PaymentForm from "./PaymentForm";
import Review from "./Review";
import * as Api from "api";
import { momo, requestType } from "config";
import * as Utils from "config/utils";
import HmacSHA256 from "crypto-js/hmac-sha256";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useLocation,
  Link as LinkR,
  useHistory,
  useParams
} from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import { connect } from "react-redux";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  }
}));

const steps = ["Information", "Payment details", "Review your order"];

let tier = {
  title: "Learn ReactJS",
  price: "15000",
  description: [
    "ReactJS presents graceful solutions to some of front-end programming’s most persistent issues. It’s fast, scalable, flexible, powerful, and has a robust developer community that’s rapidly growing. There’s never been a better time to learn React."
  ],
  buttonText: "Sign up for free",
  buttonVariant: "outlined"
};

const Checkout = props => {
  let { auth } = props;
  let location = useLocation();
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [activeStep, setActiveStep] = React.useState(0);
  let dataPayment = {};
  const handleNext = () => {
    if (activeStep + 1 == steps.length - 1) {
      // momoPayment();
      payment();
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const getStepContent = step => {
    switch (step) {
      case 0:
        return <AddressForm tier={tier} />;
      case 1:
        return <PaymentForm dataPayment={data => (dataPayment = data)} />;
      case 2:
        return <Review />;
      default:
        throw new Error("Unknown step");
    }
  };

  const payment = async () => {
    setLoading(true);
    let { typeMomo, bankCode } = dataPayment;
    let { price, title } = tier;
    // console.log("dataPayment", dataPayment);
    // return;
    var d = new Date();
    var n = d.getTime();
    var idOrder = `MM${n}`;
    let params = {};
    let paramNews = {};
    let signature = "";
    let result = null;
    let urlCallBack = window.location.href.replace("checkout", "success");
    switch (typeMomo) {
      case "aio":
        params = {
          accessKey: momo.ACCESS_KEY,
          amount: price,
          extraData: auth.user.email,
          notifyUrl: urlCallBack,
          orderId: idOrder,
          orderInfo: title,
          partnerCode: momo.PARTNER_CODE,
          requestType: requestType.captureMoMoWallet,
          requestId: idOrder,
          requestType: "captureMoMoWallet",
          returnUrl: urlCallBack
          // signature: ""
        };
        signature = Utils.createSignature(
          params,
          requestType.captureMoMoWallet
        );
        paramNews = {
          ...params,
          signature: signature
        };
        result = await Api.momoPay(params);
        setLoading(false);
        if (
          result &&
          result.data.result == "success" &&
          result.data.data.errorCode == 0
        ) {
          let data = result.data.data;
          window.location.href = data.payUrl;
        } else {
          alert("Error CORS");
        }

        return;
      case "atm":
        params = {
          partnerCode: momo.PARTNER_CODE,
          accessKey: momo.ACCESS_KEY,
          requestId: idOrder,
          orderId: idOrder,
          amount: price,
          orderInfo: title,
          requestType: requestType.payWithMoMoATM,
          returnUrl: urlCallBack,
          notifyUrl: urlCallBack,
          extraData: auth.user.email,
          signature: "",
          bankCode: "SML"
        };
        signature = Utils.createSignature(params, requestType.payWithMoMoATM);
        paramNews = {
          ...params,
          signature: signature
        };
        result = await Api.momoAtm(params);
        setLoading(false);
        if (
          result &&
          result.data.result == "success" &&
          result.data.data.errorCode == 0
        ) {
          let data = result.data.data;
          window.location.href = data.payUrl;
        } else {
          alert("Error CORS");
        }
        return;
      case "domestic":
        params = {
          payMethod: "06",
          iMid: "NICEVETEST",
          amt: price,
          callBackUrl: urlCallBack
        };
        Api.nicePayD(params).then(response => {
          console.log("response", response);
          if (response.data && response.data.data) {
            window.location.href = response.data.data.payUrl;
          }
        });
        return;
      case "international":
        params = {
          payMethod: "01",
          iMid: "NICEVETEST",
          amt: price,
          callBackUrl: urlCallBack
        };
        Api.nicePayD(params).then(response => {
          if (response.data && response.data.data) {
            window.location.href = response.data.data.payUrl;
          }
        });
        return;
      default:
        break;
    }

    // let result = await Api.captureMoMoWallet(paramNews);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      {/* <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
            Company name
          </Typography>
        </Toolbar>
      </AppBar> */}
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Checkout
          </Typography>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant="h5" gutterBottom>
                  Thank you for your order.
                </Typography>
                <Typography variant="subtitle1">
                  Your order number is #2001539. We have emailed your order
                  confirmation, and will send you an update when your order has
                  shipped.
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <div className={classes.buttons}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} className={classes.button}>
                      Back
                    </Button>
                  )}
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? "Place order" : "Next"}
                    {loading ? (
                      <CircularProgress
                        color="secondary"
                        size={20}
                        style={{ marginLeft: 5, color: "white" }}
                      />
                    ) : null}
                  </Button>
                </div>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
        <Copyright />
      </main>
    </React.Fragment>
  );
};

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch, ownProps) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
