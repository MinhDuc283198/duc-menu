import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Divider, Paper } from "@material-ui/core";
import Images from "config/image";
// import FacebookLogin from "react-facebook-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import { connect } from "react-redux";
import { signIn } from "actions";
import { useHistory } from "react-router-dom";
import * as Api from "api";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  facebook: {
    width: "100%",
    backgroundColor: "#3C5A98",
    "&:hover": {
      backgroundColor: "#3C5A98"
    }
  },
  google: {
    width: "100%",
    backgroundColor: "#DC4B38",
    "&:hover": {
      backgroundColor: "#DC4B38"
    }
  },
  grid: {
    magrinTop: 10,
    marginBottom: 15
  }
}));

const SignIn = function(props) {
  const [email, setEmail] = useState("test@gmail.com");
  const [password, setPassword] = useState("12345678");
  const history = useHistory();
  const classes = useStyles();
  let { signIn } = props;

  const loginNormal = response => {
    let params = {
      name: "Nguyen Van A",
      email: email,
      password: password
    };
    Api.login(params).then(response => {
      if (response.data.result == "success") {
        signIn(response.data.data);
        history.push("/");
      } else {
        alert("Login  fail !");
      }
    });
  };

  const responseFacebook = response => {
    alert("Login facebook only support HTTPS");
    // console.log(response);
    // if (response && response.name) {
    //   signIn(response);
    //   history.push("/");
    // } else {
    //   alert("Login facebook fail !");
    // }
  };
  const responseGoogle = res => {
    Api.login(res.profileObj).then(response => {
      console.log("response", response);
      if (response.data.result == "success") {
        signIn(response.data.data);
        history.push("/");
      } else {
        alert("Login Google fail !");
      }
    });
  };

  const changeEmail = event => {
    setEmail(event.target.value);
  };

  const changePassword = event => {
    setPassword(event.target.value);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            value={email}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={changeEmail}
          />
          <TextField
            value={password}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={changePassword}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            // type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() =>
              loginNormal({ name: "test", email: "test@gmail.com" })
            }
          >
            Sign In
          </Button>

          <Grid container alignItems="center" className={classes.grid}>
            <Grid item xs={5}>
              <Divider></Divider>
            </Grid>
            <Grid item xs={2}>
              <Typography align={"center"}>OR</Typography>
            </Grid>
            <Grid item xs={5}>
              <Divider></Divider>
            </Grid>
          </Grid>

          <Grid container spacing={3} className={classes.grid}>
            <Grid item xs>
              {/* <Button
                variant="contained"
                color="primary"
                size="large"
                className={classes.facebook}
                startIcon={
                  <Avatar
                    variant="square"
                    className={classes.square}
                    src={Images.facebook}
                  ></Avatar>
                }
              >
                FACEBOOK
              </Button> */}
              <FacebookLogin
                version={"5.0"}
                fields="name,email,picture"
                scope="email"
                appId="525373801395119"
                autoLoad
                callback={responseFacebook}
                render={renderProps => (
                  <Button
                    onClick={renderProps.onClick}
                    variant="contained"
                    color="primary"
                    size="large"
                    className={classes.facebook}
                    startIcon={
                      <Avatar
                        variant="square"
                        className={classes.square}
                        src={Images.facebook}
                      ></Avatar>
                    }
                  >
                    FACEBOOK
                  </Button>
                )}
              />
            </Grid>
            <Grid item xs>
              <GoogleLogin
                clientId="298369361616-pcuglbhm05c98ondunkvkh7irlbab51e.apps.googleusercontent.com"
                render={renderProps => (
                  <Button
                    onClick={renderProps.onClick}
                    variant="contained"
                    color="primary"
                    size="large"
                    className={classes.google}
                    startIcon={
                      <Avatar
                        variant="square"
                        className={classes.square}
                        src={Images.google}
                      ></Avatar>
                    }
                  >
                    GOOGLE
                  </Button>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                // cookiePolicy={"single_host_origin"}
              />
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
};

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  signIn: auth => dispatch(signIn(auth))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
