<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$extradata = explode('/',$extraData);

$lang = current_language();
$otitle = $lang == 'ko' ? "title" : $lang . '_title';
$classname = $DB->get_field("lmsdata_class", "$otitle", array('id' => $id));
$transfer["orderId"] = $class->code . time();
$transfer["message"] = get_string('wait', 'local_payment');
$transfer["orderInfo"] = $classname;
$transfer["amount"] = $class->price;
if($credit!=false)
{
    $transfer["amount"]=$credit->price;
    $transfer["orderInfo"]=$credit->credit_name;
}
$transfer["responseTime"] = date("Y-m-d H:i:s", time());
$transfer["errorCode"] = 0;
$transfer["extraData"] = $extraData;
$transfer["phone"] = $phone;
$transfer["zipcode"] = $zipcode;
$transfer["address"] = $address;
$transfer["selectpaytype"] = $paytype;
$transfer["localMessage"] = 'Success';
$transfer["orderType"] = $paytype;

//Checksum
$resultjson = json_encode($transfer);

// 결제 배송 테이블 insert


$datatype = $DB->get_field_sql("select type from {lmsdata_class} where id = ? ", array($id));
$lmsdata->userid = $USER->id;
$lmsdata->status = 0;
$lmsdata->type = $datatype == 2 ? 2 : 1;
$lmsdata->ref = $id;
$lmsdata->paytype = $transfer["errorCode"] == 0 ? 1 : 2;
if($credit!=false)
{
    $lmsdata->paytype=4;
    $lmsdata->ref = $credit_id;
    $lmsdata->userid = $employer_id;
}
$lmsdata->paymethod = $paytype;
$lmsdata->paycode = $transfer["orderId"];
$lmsdata->timecreated = time();
$lmsdata->timemodified = time();
//if($credit==false){
$payid = $DB->insert_record('lmsdata_payment', $lmsdata);
//}
if ($transfer["errorCode"] == 0) {
//    if ($datatype != 2) {
//        require_once $CFG->dirroot . '/local/lmsdata/lib.php';
//        require_once $CFG->dirroot . '/local/course/lib.php';
//        $courseid = $DB->get_field("lmsdata_class", 'courseid', array("id" => $extradata[0]));
//        $enrolresult = local_course_whether_enrol($courseid, $USER->id, 'student');
//        if ($enrolresult == false) {
//            $result = local_lmsdata_enrol_user($courseid, $USER->id, 'student');
//            $returnvalues = new stdClass();
//            $returnvalues->status = $result;
//            if ($result) {
//                
//            } else {
//                $aa = local_lmsdata_enrol_edit_user($courseid, $USER->id, 'student');
//            }
//        }
//    }
    if ($datatype != 1) {
        $lmsdata2->payid = $payid;
        $lmsdata2->bookid = $id;
        $lmsdata2->address = $zipcode. '-' . $address;
        $lmsdata2->cell = $phone;
        $lmsdata2->timecreated = time();
        $lmsdata2->timemodified = time();
        $DB->insert_record('lmsdata_productdelivery', $lmsdata2);
    }
}
echo "<script>var raw = '$resultjson'; opener.parent.pay_resultpop(raw); window.close();</script>";

