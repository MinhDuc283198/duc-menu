<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$extradata = explode('/', $_GET['description']);

$lang = current_language();
$otitle = $lang == 'ko' ? "title" : $lang . '_title';
$classname = $DB->get_field("lmsdata_class","$otitle",array('id'=>$extradata[0]));

if($_GET["paymethod"] == '06'){
    $paytype = 'Domestic card';
}else if($_GET["paymethod"] == '01'){
    $paytype = 'International card';
}


$_GET["errorCode"] = $_GET["resultCd"] == '0000' ? 0 : $_GET["resultCd"] ;
$date = strtotime($_GET["transDt"].$_GET["transTm"]);
    $transfer["orderId"] = $_GET["referenceNo"];
    $transfer["message"] = $_GET["resultMsg"];
    $transfer["orderInfo"] =$classname;
    $transfer["amount"] = $_GET["amount"];
    $transfer["responseTime"] = date("Y-m-d H:i:s",$date);
    $transfer["errorCode"] = $_GET["errorCode"];
    $transfer["extraData"] = $_GET['description'];
    $transfer["phone"] = $extradata[1];
    $transfer["zipcode"] = $extradata[2];
    $transfer["address"] = $extradata[3];
    $transfer["selectpaytype"] = $paytype;
    $transfer["localMessage"] = $_GET["resultMsg"];
    $transfer["orderType"] = $paytype;
    //Checksum
    $resultjson = json_encode($transfer);

    // 결제 배송 테이블 insert


    $datatype = $DB->get_field_sql("select type from {lmsdata_class} where id = ? ", array($extradata[0]));
    $lmsdata->userid = $USER->id;
    $lmsdata->status = 1;
    $lmsdata->type = $datatype == 2 ? 2 : 1;
    $lmsdata->ref = $extradata[0];
    $lmsdata->paytype = $transfer["errorCode"] == 0 ? 1 : 2;
    $lmsdata->paymethod = $paytype;
    $lmsdata->paycode = $transfer["orderId"];
    $lmsdata->timecreated = time();
    $lmsdata->timemodified = time();
    $payid = $DB->insert_record('lmsdata_payment', $lmsdata);
    if ($transfer["errorCode"] == 0) {
        if ($datatype != 2) {
            require_once $CFG->dirroot . '/local/lmsdata/lib.php';
            require_once $CFG->dirroot . '/local/course/lib.php';
            $courseid = $DB->get_field("lmsdata_class", 'courseid', array("id" => $extradata[0]));
            $enrolresult = local_course_whether_enrol($courseid, $USER->id, 'student');
            if ($enrolresult == false) {
                $result = local_lmsdata_enrol_user($courseid, $USER->id, 'student');
                $returnvalues = new stdClass();
                $returnvalues->status = $result;
                if ($result) {

                } else {
                    $aa = local_lmsdata_enrol_edit_user($courseid, $USER->id, 'student');
                }
            }
        }
        if ($datatype != 1) {
            $lmsdata2->payid = $payid;
            $lmsdata2->bookid = $extradata[0];
            $lmsdata2->address = $extradata[2] . '-' . $extradata[3];
            $lmsdata2->cell = $extradata[1];
            $lmsdata2->timecreated = time();
            $lmsdata2->timemodified = time();
            $DB->insert_record('lmsdata_productdelivery', $lmsdata2);
        }
    }

//    echo "<script>console.log('Debug huhu Objects: " . $rawHash . "' );</script>";
//    echo "<script>console.log('Debug huhu Objects: " . $partnerSignature . "' );</script>";


    echo "<script>var raw = '$resultjson'; opener.parent.pay_resultpop(raw); window.close();</script>";


//    if ($m2signature == $partnerSignature) {
//        if ($errorCode == '0') {
//            $result = '<div class="alert alert-success"><strong>Payment status: </strong>Success</div>';
//        } else {
//            $result = '<div class="alert alert-danger"><strong>Payment status: </strong>' . $message . '/' . $localMessage . '</div>';
//        }
//    } else {
//        $result = '<div class="alert alert-danger">This transaction could be hacked, please check your signature and returned signature</div>';
//    }
