<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
/**
 * Description of vnpay_ajax
 *
 * @author xonv
 */

require_once("vnpayphp/config.php");
$vnp_TxnRef = date("YmdHis") . '_' . $USER->id; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
$vnp_OrderInfo = $extraData;
$vnp_OrderType = $paytype;
$vnp_Amount = $class->price * 100;
if($credit!=false)
{
    $vnp_Amount=$credit->price* 100;
}
$vnp_Locale = 'vn';
$vnp_BankCode = '';
$vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

// create lmsdata_payment
if($credit==false){
$datatype = $DB->get_field_sql("select type from {lmsdata_class} where id = ? ", array($id));

$lmsdata->userid = $USER->id;
$lmsdata->status = 0;
$lmsdata->type = $datatype == 2 ? 2 : 1;
$lmsdata->ref = $id;
$lmsdata->paytype = 1;
$lmsdata->paymethod = $paytype;
$lmsdata->paycode = $vnp_TxnRef;
$lmsdata->timecreated = time();
$lmsdata->timemodified = time();
$DB->insert_record('lmsdata_payment', $lmsdata);
}
// end create lms data payment

$inputData = array(
    "vnp_Version" => "2.0.0",
    "vnp_TmnCode" => $vnp_TmnCode,
    "vnp_Amount" => $vnp_Amount,
    "vnp_Command" => "pay",
    "vnp_CreateDate" => date('YmdHis'),
    "vnp_CurrCode" => "VND",
    "vnp_IpAddr" => $vnp_IpAddr,
    "vnp_Locale" => $vnp_Locale,
    "vnp_OrderInfo" => $vnp_OrderInfo,
    "vnp_OrderType" => $vnp_OrderType,
    "vnp_ReturnUrl" => $vnp_Returnurl,
    "vnp_TxnRef" => $vnp_TxnRef,
);
if (partnerDiscount() && lotteMail()) {
    $inputData["vnp_BankCode"] = "visa";
}

if (isset($vnp_BankCode) && $vnp_BankCode != "") {
    $inputData['vnp_BankCode'] = $vnp_BankCode;
}

ksort($inputData);
$query = "";
$i = 0;
$hashdata = "";
foreach ($inputData as $key => $value) {

    if ($i == 1) {
        $hashdata .= '&' . $key . "=" . $value;
    } else {

        $hashdata .= $key . "=" . $value;
        $i = 1;
    }

    $query .= urlencode($key) . "=" . urlencode($value) . '&';
}

$vnp_Url = $vnp_Url . "?" . $query;

if (isset($vnp_HashSecret)) {
    $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
    $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
}

$returnData = array('code' => '00'
, 'message' => 'success'
, 'data' => $vnp_Url);

header('Location: ' . $vnp_Url);
