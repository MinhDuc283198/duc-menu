
<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/book/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
require_once($CFG->dirroot . '/local/credit/lib.php');
$context = context_system::instance();
$id = optional_param('id', 0, PARAM_INT);
$employer_id = optional_param('employer_id', 0, PARAM_INT);

$goodstype = "1";

//if ($goodstype == 2) {
//    $list = local_book_id($id);
//} else {
//    $list = local_course_class_id2($id);
//}
$list=local_credit_get_by_id($id);
$goodtitle = 'Credit';
.
$lang = current_language();
if ($lang == 'ko') {
    $title = 'title';
    $lttitle = 'lttitle';
} else {
    $title = $lang . '_title';
    $lttitle = $lang . '_lttitle';
}
$userdata = $DB->get_record('lmsdata_user', array('userid' => $USER->id));
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('결제');
// 개발전 임시 작업
//$list->price = 0;
echo $OUTPUT->header();
?>
<div class="layerpop">
    <?php ?>
    <div class="pop-title">
        <?php echo get_string("payment", "local_payment"); ?>
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <h5 class="pg-tit"><?php echo get_string("goodsinfo", "local_payment"); ?></h5>
        <div class="bg-box">
            <div class="crs-tit goodstitle"><?php
                echo '[' . $goodtitle . ']  ';
//                echo $list->$title ? $list->$title : $list->title
                echo $list->credit_name
                ?></div>
            <ul class="crs-txt">
                <?php if ($goodstype == 3) { ?>
                    <li><strong><?php echo get_string("goodtitle1", "local_payment"); ?></strong><span><?php echo $list->$title ? $list->$title : $list->title ?></span></li>
                    <li><strong><?php echo get_string("goodtitle2", "local_payment"); ?></strong><span><?php echo $list->$lttitle ? $list->$lttitle : $list->lttitle ?></span></li>
                <?php } else { ?>
                    <li><strong><?php echo $goodtitle ?>  </strong><span><?php echo $list->$title ? $list->$title : $list->title ?></span></li>
                <?php } ?>
            </ul>
            <p class="price">
                <?php if ($list->discount != 0) { ?>
                    <strong class="p-down"><span><?php echo $list->lcoprice ? $list->lcoprice : $list->originprice ?>VND</span><span><?php echo $list->discount ?>%</span></strong>
                <?php } ?>
                <em class="t-blue02 goodsprice"> <?php echo $list->price ?></em>VND
            </p>
        </div>

        <?php if ($goodstype != 1) { ?>
        <h5 class="pg-tit hasbtn dinfo"><?php echo get_string('deleveryinfo', 'local_payment'); ?><a href="#" class="btns small mg-l10" onclick="sava_userinfo();"><?php echo get_string('save', 'local_payment'); ?></a></h5>
            <div class="bg-box">
                <div class="rw">
                    <strong class="dot"><?php echo get_string('phone', 'local_payment'); ?></strong>
                    <input type="text" name='phone' class="uinfo" placeholder="<?php echo get_string('phonetext', 'local_payment'); ?>" <?php echo $userdata->phone1 ? "value='$userdata->phone1'" : null ?> />
                </div>
                <div class="rw">
                    <strong class="dot"><?php echo get_string('address', 'local_payment'); ?></strong>
                    <input type="text" name='zipcode' onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" class="uinfo" placeholder="ZIPCODE" <?php echo $userdata->zipcode ? "value='$userdata->zipcode'" : null ?>/>
                    <input type="text" name='address' class="w100 uinfo"  <?php echo $userdata->address ? "value='$userdata->address'" : null ?> />
                </div>
            </div>
        <?php } ?>
         <?php if($list->price != 0) { ?>
        <h5 class="pg-tit"><?php echo get_string('selectpaytype', 'local_payment'); ?></h5>
        <div class="bg-box">
            <p class="rd-rw"><input type="radio" name="paytype" class='paytype'  id="rd01"  value="momo"/><label for="rd01"><?php echo get_string('paymenttype1', 'local_payment'); ?></label></p>
            <p class="rd-rw"><input type="radio" name="paytype" class='paytype'  id="rd02" value="domestic" /><label for="rd02"><?php echo get_string('paymenttype2', 'local_payment'); ?></label></p>
            <!--<p class="rd-rw"><input type="radio" name="paytype" class='paytype'  id="rd03" value="international" /> <label for="rd03">International card (Visa, Master, Amex, JCB)</label></p>-->
            <div class="rd-rw">
                <input type="radio" name="paytype"  class='paytype'  id="rd04" value="directdeposit" /> <label for="rd04"><?php echo get_string('directdeposit', 'local_payment'); ?></label>
                <div class="rd-info">
                    <ul class="dash-list">
                        <li><?php echo get_string('deposittext1', 'local_payment'); ?></li>
                        <li><?php echo get_string('deposittext2', 'local_payment'); ?></li>
                        <li><?php echo get_string('deposittext3', 'local_payment'); ?></li>
                        <li><?php echo get_string('deposittext6', 'local_payment'); ?></li>
                    </ul>
                    <p class="t-info"><?php echo get_string('deposittext4', 'local_payment', date("Y-m-d", strtotime("+5 days"))); ?></p>
                    <p class="t-info"><?php echo get_string('deposittext5', 'local_payment'); ?></p>
                </div>
            </div>
        </div>
             <h5 class="pg-tit">Coupon Code</h5>
             <div class="bg-box">
                 <p class="rd-rw">
                     <input style="width: 100%; border-radius: 6px" type="text" name="coupon" class='paytype' value=""/>
                 </p>
             </div>
          <?php } ?>
    </div>
    <div class="btn-area text-center" id="btn_payment">
        <input type="button" value="<?php echo get_string("payment", "local_payment"); ?>" class="btns point w100" onclick="payment_progress();" />
    </div>
    <script>

        $(document).ready(function () {
            var phone = '<?php echo $userdata->phone1 ? $userdata->phone1 :''?>';
                    var zipcode = '<?php echo $userdata->zipcode ? $userdata->zipcode :  ''?>';
                    var address = '<?php echo $userdata->address ? $userdata->address : '' ?>';
                    var appendtext = "<span class='t-blue t-small savetext'>ᆞ<?php echo get_string('changeinfo','local_payment') ?>.</span>";
                    $('.uinfo').keyup(function(){
                        if (phone == $('input[name=phone]').val() && address == $('input[name=address]').val() && zipcode == $('input[name=zipcode]').val()) {
                            $('.savetext').remove();
                        } else {
                            if (!$('.dinfo').children('.savetext').length) {
                                $('.dinfo').append(appendtext);
                            }
                        }
                    }
            );
        });
        function sava_userinfo(){
            var phone = $('input[name=phone]').val();
            var zipcode = $('input[name=zipcode]').val();
            var address = $('input[name=address]').val();
                $.ajax({
                    type: 'POST',
                    url: '/local/payment/save_address.php',
                    dataType: 'JSON',
                    data: {phone: phone, zipcode: zipcode,address: address},
                    success: function() {
                        alert('<?php echo get_string('savesuccess','local_payment') ?>');
                    },
                    error: function(xhr, status, error) {
                    }
                });

        }
        function payment_progress(){
        <?php if($list->price == 0) { ?>
            var paytype = 'free';
        <?php }else{ ?>
            var paytype = $("input[name=paytype]:checked").val();
        <?php } ?>
            var coupon = $("input[name=coupon]").val();
            var form =  document.createElement("form");
            var url = "/local/payment/init_payment2.php";
            form.setAttribute("charset", "UTF-8");
            form.setAttribute("method", "Post");
            form.setAttribute("action", url);
            form.setAttribute("target", "payform");
            var input1 = document.createElement("input");
            input1.setAttribute("type", "hidden");
            input1.setAttribute("name", "id");
            input1.setAttribute("value", '<?php echo $id ?>');
            form.appendChild(input1);
            var input5 = document.createElement("input");
            input5.setAttribute("type", "hidden");
            input5.setAttribute("name", "employer_id");
            input5.setAttribute("value", '<?php echo $employer_id ?>');
            form.appendChild(input5);
            if (coupon) {
                var couponInput = document.createElement("input");
                couponInput.setAttribute("name", "coupon");
                couponInput.setAttribute("value", coupon)
                couponInput.setAttribute("type", "hidden");
                form.appendChild(couponInput);
            }
            <?php if ($goodstype != 1){ ?>
                var address = $('input[name=address]').val();
                var phone =   $('input[name=phone]').val();
                var zipcode = $('input[name=zipcode]').val();
                if(address && phone && zipcode ){
                    var input2 = document.createElement("input");
                    input2.setAttribute("type", "hidden");
                    input2.setAttribute("name", "address");
                    input2.setAttribute("value", address);
                    form.appendChild(input2);
                    var input3 = document.createElement("input");
                    input3.setAttribute("type", "hidden");
                    input3.setAttribute("name", "phone");
                    input3.setAttribute("value", phone);
                    form.appendChild(input3);
                    var input4 = document.createElement("input");
                    input4.setAttribute("type", "hidden");
                    input4.setAttribute("name", "zipcode");
                    input4.setAttribute("value", zipcode);
                    form.appendChild(input4);
                }else{
                     alert("<?php echo get_string('enterinfo', 'local_payment', get_string('deleveryinfo','local_payment'))?> ")
                     return false;
                }
            <?php  } ?>
            if(paytype){
                var input4 = document.createElement("input");
                input4.setAttribute("type", "hidden");
                input4.setAttribute("name", "paytype");
                input4.setAttribute("value", paytype);
                form.appendChild(input4);
                document.body.appendChild(form);
            }else{
                alert("<?php echo get_string('enterinfo', 'local_payment', get_string('selectpaytype','local_payment'))?> ")
                return false;
            }
                window.open("","payform","width=759 ,height=847,scrollorbars=yes,resizable=no");
                form.submit();
        }
    </script>
</div>


<?php
echo $OUTPUT->footer();
?>


