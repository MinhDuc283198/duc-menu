<?php
header('Content-type: text/html; charset=utf-8');
include "momo/common/helper.php";
$returnUrl = $CFG->wwwroot . "/local/payment/init_momo_result.php";
$notifyurl = $CFG->wwwroot . "/local/payment/init_momo_result.php";
//momo payment key
$partnerCode = $moopaykey["partnerCode"]->value;
$accessKey = $moopaykey["accessKey"]->value;
$serectkey = $moopaykey["secretKey"]->value;

$orderId = $class->code.time();
$lang = current_language();
$otitle = $lang == 'ko' ? "title" : $lang . '_title';
$orderInfo = $class->$otitle;

$amount = $class->price;
if($credit!=false)
{
    $amount=$credit->price;
    $orderInfo=$credit->credit_name;
}
$endpoint = "https://payment.momo.vn/gw_payment/transactionProcessor";
$extraData = $id.'/'.$phone.'/'.$zipcode.'/'.$address.'/'.$paytype.'/'.$credit_id.'/'.$employer_id;
$requestType = "captureMoMoWallet";
$requestId = time() . "";
//before sign HMAC SHA256 signature
$rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&returnUrl=" . $returnUrl . "&notifyUrl=" . $notifyurl . "&extraData=" . $extraData;

$signature = hash_hmac("sha256", $rawHash, $serectkey);
$data = array('partnerCode' => $partnerCode,
    'accessKey' => $accessKey,
    'requestId' => $requestId,
    'amount' => $amount,
    'orderId' => $orderId,
    'orderInfo' => $orderInfo,
    'orderInfo2' => $orderInfo,
    'orderInfo3' => $orderInfo,
    'returnUrl' => $returnUrl,
    'notifyUrl' => $notifyurl,
    'extraData' => $extraData,
    'requestType' => $requestType,
    'signature' => $signature);
$result = execPostRequest($endpoint, json_encode($data));
$jsonResult = json_decode($result, true);  // decode json
//    print_object($data);
//    print_object($result);
//    print_object($jsonResult);
//    die;
//Just a example, please check more in there

header('Location: ' . $jsonResult['payUrl']);
?>
