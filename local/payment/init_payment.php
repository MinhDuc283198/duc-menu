<?php
global $CFG,$DB,$USER;
require(dirname (dirname (dirname (__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');

$id  = optional_param('id',0, PARAM_INT);
$credit_id  = optional_param('credit_id',0, PARAM_INT);
$employer_id  = optional_param('employer_id',0, PARAM_INT);
$address = optional_param('address', '', PARAM_RAW);
$phone = optional_param('phone', '', PARAM_RAW);
$zipcode = optional_param('zipcode', '', PARAM_RAW);
$paytype = optional_param('paytype', '', PARAM_RAW);
$class = $DB->get_record("lmsdata_class",array('id'=>$id));
$credit = $DB->get_record("vi_credit_menu",array('id'=>$credit_id));

$extraData = $id.'/'.$USER->id.'/'.$zipcode.'/'.$address.'/'.$paytype.'/'.$credit_id.'/'.$employer_id;

// hard code discount amnote
$coupon = optional_param('coupon', '', PARAM_RAW);
if($coupon!==''){
    $coupon_infor=get_coupon_by_code($coupon);
}
if($coupon_infor){
    if($coupon_infor->count>0){
        $class->price =(string) ($class->price - $class->price * $coupon_infor->discount_rate / 100);
//    $receicoupon=$DB->get_record('vi_coupons_receivers',array('id'=>$coupon_infor->id));
//    $timeused=time();
//    $receicoupon->is_used=1;
//    $receicoupon->used_time=$timeused;
//    $DB->update_record('vi_coupons_receivers',$receicoupon);
        $coupon_old=$DB->get_record('vi_coupons',array('id'=>$coupon_infor->coupon_id));
        $count=(string)($coupon_old->count-1);
        $coupon_old->count=$count;
        $DB->update_record('vi_coupons',$coupon_old);
    }
}
if ($coupon == $CFG->amnoteCoupon) {
    $class->price = $class->price - $class->price * $CFG->amnoteDiscountPercent;
    $extraData = $extraData .'/' . $CFG->amnoteCoupon;
}



if ($discount = partnerDiscount()) {
    $class->price = $class->price - $class->price * $discount / 100;
}
// end  hard code discount amnote

include 'init_'.$paytype.'.php';

?>
