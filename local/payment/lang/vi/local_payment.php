<?php
$string['payment'] = 'Thanh toán';
$string['goodsinfo'] = 'Sản phẩm';
$string['directdeposit'] = 'Gửi tiền trực tiếp';
$string['creditcard'] = 'thẻ tín dụng';
$string['selectpaytype'] = 'Phương thức thanh toán';
$string['deleveryinfo'] = 'Thông tin vận chuyển';
$string['phone'] = 'Số điện thoại di động';
$string['address'] = 'Địa chỉ';
$string['save'] = 'Lưu lại';
$string['phonetext'] = "nhập ngoại trừ '-' ";
$string['savesuccess'] = "Lưu xong";
$string['changeinfo'] = "Có thông tin thay đổi.";
$string['enterinfo'] = 'Vui lòng nhập {$a}.';
$string['confirm'] = 'Hoàn thành';


$string['successpay'] = 'Đăng ký thanh toán bằng hình thức chuyển khoản.';
$string['check_pay'] = 'Bạn có muốn thay đổi để thanh toán thành công?';
$string['falsepay'] = 'Thất bại trong thanh toán';
$string['successpayfull'] = 'Bạn đã đăng ký thành công thanh toán bằng hình thức chuyển khoản.';
$string['falsepayfull'] = 'Thanh toán của bạn đã bị từ chối.';
$string['payinfo'] = 'Thông tin thanh toán';
$string['ordernum'] = 'Số thứ tự';
$string['goodsname'] = 'Tên sản phẩm';
$string['paytype'] = 'Phương thức thanh toán';
$string['price'] = 'Số tiền thanh toán';
$string['accepttime'] = 'Ngày phê duyệt';
$string['errorinfo'] = 'Thông tin lỗi';
$string['errorcode'] = 'Mã lỗi';
$string['errortext1'] = 'Thông báo lỗi';
$string['errortext2'] = 'Tin nhắn địa phương';
$string['wait'] = 'Chờ thanh toán';


$string['deposittext1'] = 'Account name : VISANG VIETNAM';
$string['deposittext2'] = 'Bank(swift Code) : SHINHAN BANK VIETNAM(SHBKYNVXXXX)';
$string['deposittext3'] = 'Account number : 700-016-099145';
$string['deposittext4'] = 'Nếu thanh toán không được nhận cho đến khi <span class="t-red">{$a}</span>, ứng dụng sẽ bị hủy.';
$string['deposittext5'] = 'Các khóa học có thể được thực hiện và giao từ ngày thanh toán được xác nhận.';


$string['goodtitle1'] = 'Khóa học';
$string['goodtitle2'] = 'Sách';
$string['goodtitle3'] = 'Khóa học + Sách';

$string['paymenttype1'] = 'Ví MOMO';
$string['paymenttype2'] = 'Thẻ ATM/Visa/Mastercard';

$string['atm_card'] = 'Thẻ ATM';
$string['visa'] = 'Visa';
$string['ewallet'] = 'Ví điện tử';
$string['virtual_account'] = 'Tài khoản chuyên dụng';
$string['user_name'] = 'Tên TK';
$string['name'] = 'Tên';
$string['email'] = 'E-mail';

$string['use_lotte_mail'] = 'Thay đổi';
$string['label_use_lotte_mail'] = 'Sử dụng Email Lotte để được giảm giá';

$string['cp_notcourse']='Mã giảm giá này không áp dụng cho khóa học này';
$string['cp_isusedalert']='Mã giảm giá này đã được sử dụng';
$string['cp_notused']='Mã giảm giá này chưa được áp dụng hoặc đã hết hạn';
$string['cp_notfound']='Không tìm thấy mã giảm giá này';
$string['cp_outofcode']='Mã giảm giá này đã hết';