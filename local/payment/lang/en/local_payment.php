<?php
$string['payment'] = 'payment';
$string['goodsinfo'] = 'goodsinfo';
$string['directdeposit'] = 'Direct deposit';
$string['creditcard'] = 'Credit card';
$string['selectpaytype'] = 'method of payment';
$string['deleveryinfo'] = 'Shipping Information';
$string['phone'] = 'Phone Number';
$string['address'] = 'address';
$string['save'] = 'save';
$string['phonetext'] = "enter except '-'";
$string['savesuccess'] = "Saved";
$string['changeinfo'] = "There is changed information.";
$string['enterinfo'] = 'Please enter {$a}.';
$string['confirm'] = 'Confirm';


$string['successpay'] = 'Payment application';
$string['check_pay'] = 'Do you want to change to payment successfully?';
$string['falsepay'] = 'Payment failure';
$string['successpayfull'] = 'Payment application completed.';
$string['falsepayfull'] = 'Your payment was declined.';
$string['payinfo'] = 'Billing Information';
$string['ordernum'] = 'Order Number';
$string['goodsname'] = 'product name';
$string['paytype'] = 'method of payment';
$string['price'] = 'Amount of payment';
$string['accepttime'] = 'Approval date';
$string['errorinfo'] = 'Error Information';
$string['errorcode'] = 'Error code';
$string['errortext1'] = 'Error message';
$string['errortext2'] = 'Local message';
$string['wait'] = 'Waiting for payment';

$string['deposittext1'] = 'Account name : VISANG VIETNAM';
$string['deposittext2'] = 'Bank(swift Code) : SHINHAN BANK VIETNAM(SHBKYNVXXXX)';
$string['deposittext3'] = 'Account number : 700-016-099145';
$string['deposittext4'] = 'If payment is not received until <span class="t-red">{$a}</span>, the application will be cancelled.';
$string['deposittext5'] = 'Courses can be taken and delivered from the day the payment is confirmed.';


$string['goodtitle1'] = 'lecture';
$string['goodtitle2'] = 'book';
$string['goodtitle3'] = 'lecture + book';

$string['paymenttype1'] = 'MOMO E-Wallet';
$string['paymenttype2'] = 'ATM/Visa/Master Card';


$string['atm_card'] = 'ATM card';
$string['visa'] = 'Visa';
$string['ewallet'] = 'E-Wallet';
$string['virtual_account'] = 'Virtual Account';
$string['user_name'] = 'Account Name';
$string['name'] = 'Name';
$string['email'] = 'E-mail';

$string['use_lotte_mail'] = 'Change';
$string['label_use_lotte_mail'] = 'Change lotte email to get discount';

$string['cp_notcourse']='This discount code does not apply to this course';
$string['cp_isusedalert']='This discount code has already been used';
$string['cp_notused']='This discount code has not been applied or has expired';
$string['cp_notfound']='Can\'t find this discount code';
$string['cp_outofcode']='This discount code is no longer available';

