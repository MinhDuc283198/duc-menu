<?php
$string['payment'] = '결제하기';
$string['goodsinfo'] = '상품정보';
$string['directdeposit'] = '무통장 입금';
$string['creditcard'] = '신용카드';
$string['selectpaytype'] = '결제수단 선택';
$string['deleveryinfo'] = '배송정보';
$string['phone'] = '휴대폰 번호';
$string['address'] = '배송 주소';
$string['save'] = '저장';
$string['phonetext'] = "'-'제외 입력";
$string['savesuccess'] = "저장완료";
$string['changeinfo'] = "변경된 정보가 있습니다.";
$string['enterinfo'] = '{$a}를 입력해주세요.';
$string['confirm'] = '확인';


$string['successpay'] = '결제신청';
$string['check_pay'] = '결제 완료로 변경하시겠습니까?';
$string['falsepay'] = '결제실패';
$string['successpayfull'] = '결제가 신청되었습니다.';
$string['falsepayfull'] = '결제가 실패되었습니다.';
$string['payinfo'] = '결제 정보';
$string['ordernum'] = '주문번호';
$string['goodsname'] = '상품명';
$string['paytype'] = '결제수단';
$string['price'] = '결제 금액';
$string['accepttime'] = '승인 일시';
$string['errorinfo'] = '에러정보';
$string['errorcode'] = '에러코드';
$string['errortext1'] = '에러 메세지';
$string['errortext2'] = '지역 메시지';
$string['wait'] = '결제대기';



$string['deposittext1'] = 'Account name : VISANG VIETNAM';
$string['deposittext2'] = 'Bank(swift Code) : SHINHAN BANK VIETNAM(SHBKYNVXXXX)';
$string['deposittext3'] = 'Account number : 700-016-099145';
$string['deposittext4'] = '<span class="t-red">{$a}</span> 까지 입금되지 않는 경우 신청 취소됩니다.';
$string['deposittext5'] = '입금이 확인된 날부터 수강 및 배송 가능합니다.';


$string['goodtitle1'] = '강좌';
$string['goodtitle2'] = '교재';
$string['goodtitle3'] = '강좌 + 교재';

$string['paymenttype1'] = 'MOMO 전자지갑';
$string['paymenttype2'] = 'ATM/Visa/Master Card';

$string['use_lotte_mail'] = '변화';
$string['label_use_lotte_mail'] = '롯대 이메일 사용하시면 할인해 드립니다';

$string['cp_notcourse']='이 할인 코드는 이 과정에 적용되지 않습니다';
$string['cp_isusedalert']='이 할인 코드는 이미 사용되었습니다';
$string['cp_notused']='이 할인 코드는 적용되지 않았거나 만료되었습니다.';
$string['cp_notfound']='이 할인 코드를 찾을 수 없습니다';
$string['cp_outofcode']='이 할인 코드는 더 이상 사용할 수 없습니다';