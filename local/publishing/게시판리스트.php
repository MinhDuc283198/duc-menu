<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('게시판리스트');

echo $OUTPUT->header();
?>  
<h2 class="pg-tit">재밌는 한국어</h2>
<ul class="thumb-board">
    <li>
        <div class="img">
            <img src="/theme/oklassedu/pix/images/ex_img03.png" alt="" />
        </div>
        <div class="txt">
            <div class="tit"><a href="#">[어휘랑 한국탐방] 제주편#3 화산섬</a></div>
            <p class="t-gray">제주도는 무엇이 유명할까요?부글부글 화산 활동으로 생겨난 ‘화산섬’</p>
            <p><span>작성일: 2019.08.23</span><span>조회수: 93</span></p>
        </div>
    </li>
    <li>
        <div class="img">
            <img src="/theme/oklassedu/pix/images/ex_img03.png" alt="" />
        </div>
        <div class="txt">
            <div class="tit"><a href="#">[어휘랑 한국탐방] 제주편#3 화산섬</a></div>
            <p class="t-gray">제주도는 무엇이 유명할까요?부글부글 화산 활동으로 생겨난 ‘화산섬’</p>
            <p><span>작성일: 2019.08.23</span><span>조회수: 93</span></p>
        </div>
    </li>
    <li>
        <div class="txt">
            <div class="tit"><a href="#">[어휘랑 한국탐방] 제주편#3 화산섬</a></div>
            <p class="t-gray">제주도는 무엇이 유명할까요?부글부글 화산 활동으로 생겨난 ‘화산섬’</p>
            <p><span>작성일: 2019.08.23</span><span>조회수: 93</span></p>
        </div>
    </li>
</ul>

<ul class="mk-paging">
    <li class="first"><a href="#">first</a></li>
    <li class="prev"><a href="#">prev</a></li>
    <li class="on"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">6</a></li>
    <li><a href="#">7</a></li>
    <li><a href="#">8</a></li>
    <li><a href="#">9</a></li>
    <li><a href="#">10</a></li>
    <li class="next"><a href="#">next</a></li>
    <li class="end"><a href="#">end</a></li>
</ul>
<?php
echo $OUTPUT->footer();
?>


