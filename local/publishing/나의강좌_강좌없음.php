<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('나의강좌');

echo $OUTPUT->header();
?>  
<ul class="mk-c-tab tab-event">
    <li class="on"><a href="#">수강중인 강좌</a></li>
    <li><a href="#">수강완료 강좌</a></li>
    <li><a href="#">관심 강좌</a></li>
</ul>

<div class="no-data style02">
    <div>현재 수강중인 강좌가 없습니다.</div>
    <!-- 수강완료강좌 <div>수강을 완료한 강좌가 없습니다.</div> -->
    <!-- 관심강좌 <div>관심 설정한 강좌가 없습니다.</div> -->
    <a href="#" class="btns point arrow">전체 강좌 보러가기</a>
</div>
<?php
echo $OUTPUT->footer();
?>


