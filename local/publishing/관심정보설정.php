<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('frontpage');
$PAGE->navbar->add('');

echo $OUTPUT->header();
?>  
<script type="text/javascript">
    $(function () {
        //관심정보설정 팝업 
        utils.popup.call_layerpop("./팝업_관심정보설정.php", {"width": "800px", "height": "auto", "callbackFn": function () {}}, {"param": "11"});
        return false;
    });
</script>

<?php

echo $OUTPUT->footer();
?>


