<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('서브메인');

echo $OUTPUT->header();
?>  
<div class="page-nav">
    <p>전체강좌</p>
    <div class="sub-mn">
        <span>메뉴아이콘</span>
        <ul>
            <li class="selected">
                <a href="#">한국어 강좌</a>
                <ul>
                    <li class="selected"><a href="#">초급과정</a></li>
                    <li><a href="#">중급과정</a></li>
                    <li><a href="#">고급과정</a></li>
                </ul>
            </li>
            <li>
                <a href="#">한국어 패키지</a>
                <ul>
                    <li class="selected"><a href="#">초급과정2</a></li>
                    <li><a href="#">중급과정2</a></li>
                    <li><a href="#">고급과정2</a></li>
                </ul>
            </li>
            <li>
                <a href="#">TOPIK 1,2급</a>
                <ul>
                    <li class="selected"><a href="#">초급과정3</a></li>
                    <li><a href="#">중급과정3</a></li>
                    <li><a href="#">고급과정3</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<div class="page-nav tit">
    <p>카테고리1</p>
    <p>하위카테고리1</p>

    <a href="#" class="mr">더보기</a>
</div>

<ul class="thumb-list style04">
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt gray">수강중</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
</ul>


<div class="page-nav tit">
    <p>카테고리1</p>
    <p>하위카테고리1</p>

    <a href="#" class="mr">더보기</a>
</div>

<ul class="thumb-list style04">
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt gray">수강중</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
</ul>

<?php
echo $OUTPUT->footer();
?>


