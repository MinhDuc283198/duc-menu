<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('FAQ');

echo $OUTPUT->header();
?>  


<h2 class="pg-tit">FAQ</h2>

<ul class="mk-c-tab col4 tab-event">
    <li class="on" data-target=".data01"><a href="#">전체</a></li>
    <li data-target=".data02"><a href="#">강좌 수강</a></li>
    <li data-target=".data03"><a href="#">결제/환불</a></li>
    <li data-target=".data04"><a href="#">사이트이용</a></li>
</ul>


<div class="target-area">
    <div class="data01 on">
        <ul class="faq-list">
            <li>
                <div class="hd">사이트 이용] 비밀번호를 잊어버렸어요.사이트 이용] 비밀번호를 잊어버렸어요.사이트 이용] 비밀번호를 잊어버렸어요.사이트 이용] 비밀번호를 잊어버렸어요.사이트 이용] 비밀번호를 잊어버렸어요.</div>
                <div class="cn">
                    <p>비상 한국어의 회원 비밀번호는 암호화 저장되어 분실 시 찾아드릴 수 없습니다.<br />아래의 경로로 비밀번호를 재설정해주시기 바랍니다.</p>
                    <br />
                    <p>■ 비밀번호 재설정하기</p>
                    <ul>
                        <li>1. 비상 한국어 로그인 화면에서 [비밀번호 찾기] 선택</li>
                        <li>2. 회원정보 확인: 이름, 아이디(이메일)입력</li>
                        <li>3. 비밀번호 찾는 방법 선택 (휴대폰 또는 이메일)</li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="hd">[강좌 수강] 동영상 학습창이 안 열려요.</div>
                <div class="cn">
                    <p>비상 한국어의 회원 비밀번호는 암호화 저장되어 분실 시 찾아드릴 수 없습니다.<br />아래의 경로로 비밀번호를 재설정해주시기 바랍니다.</p>
                    <br />
                    <p>■ 비밀번호 재설정하기</p>
                    <ul>
                        <li>1. 비상 한국어 로그인 화면에서 [비밀번호 찾기] 선택</li>
                        <li>2. 회원정보 확인: 이름, 아이디(이메일)입력</li>
                        <li>3. 비밀번호 찾는 방법 선택 (휴대폰 또는 이메일)</li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="hd">[사이트 이용] 개인정보와 관련된 피해를 입은 경우 어떻게 하나요?</div>
                <div class="cn">
                    <p>비상 한국어의 회원 비밀번호는 암호화 저장되어 분실 시 찾아드릴 수 없습니다.<br />아래의 경로로 비밀번호를 재설정해주시기 바랍니다.</p>
                    <br />
                    <p>■ 비밀번호 재설정하기</p>
                    <ul>
                        <li>1. 비상 한국어 로그인 화면에서 [비밀번호 찾기] 선택</li>
                        <li>2. 회원정보 확인: 이름, 아이디(이메일)입력</li>
                        <li>3. 비밀번호 찾는 방법 선택 (휴대폰 또는 이메일)</li>
                    </ul>
                </div>
            </li>
             <li>
                <div class="hd">[사이트 이용] 개인정보와 관련된 피해를 입은 경우 어떻게 하나요?</div>
                <div class="cn">
                    <p>비상 한국어의 회원 비밀번호는 암호화 저장되어 분실 시 찾아드릴 수 없습니다.<br />아래의 경로로 비밀번호를 재설정해주시기 바랍니다.</p>
                    <br />
                    <p>■ 비밀번호 재설정하기</p>
                    <ul>
                        <li>1. 비상 한국어 로그인 화면에서 [비밀번호 찾기] 선택</li>
                        <li>2. 회원정보 확인: 이름, 아이디(이메일)입력</li>
                        <li>3. 비밀번호 찾는 방법 선택 (휴대폰 또는 이메일)</li>
                    </ul>
                </div>
            </li>
             <li>
                <div class="hd">[사이트 이용] 개인정보와 관련된 피해를 입은 경우 어떻게 하나요?</div>
                <div class="cn">
                    <p>비상 한국어의 회원 비밀번호는 암호화 저장되어 분실 시 찾아드릴 수 없습니다.<br />아래의 경로로 비밀번호를 재설정해주시기 바랍니다.</p>
                    <br />
                    <p>■ 비밀번호 재설정하기</p>
                    <ul>
                        <li>1. 비상 한국어 로그인 화면에서 [비밀번호 찾기] 선택</li>
                        <li>2. 회원정보 확인: 이름, 아이디(이메일)입력</li>
                        <li>3. 비밀번호 찾는 방법 선택 (휴대폰 또는 이메일)</li>
                    </ul>
                </div>
            </li>
             <li>
                <div class="hd">[사이트 이용] 개인정보와 관련된 피해를 입은 경우 어떻게 하나요?</div>
                <div class="cn">
                    <p>비상 한국어의 회원 비밀번호는 암호화 저장되어 분실 시 찾아드릴 수 없습니다.<br />아래의 경로로 비밀번호를 재설정해주시기 바랍니다.</p>
                    <br />
                    <p>■ 비밀번호 재설정하기</p>
                    <ul>
                        <li>1. 비상 한국어 로그인 화면에서 [비밀번호 찾기] 선택</li>
                        <li>2. 회원정보 확인: 이름, 아이디(이메일)입력</li>
                        <li>3. 비밀번호 찾는 방법 선택 (휴대폰 또는 이메일)</li>
                    </ul>
                </div>
            </li>
        </ul>

        <ul class="mk-paging">
            <li class="first"><a href="#">first</a></li>
            <li class="prev"><a href="#">prev</a></li>
            <li class="on"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">6</a></li>
            <li><a href="#">7</a></li>
            <li><a href="#">8</a></li>
            <li><a href="#">9</a></li>
            <li><a href="#">10</a></li>
            <li class="next"><a href="#">next</a></li>
            <li class="end"><a href="#">end</a></li>
        </ul>
    </div>
    <div class="data02">2</div>
    <div class="data03">3</div>
    <div class="data04">4</div>
</div>
<script type="text/javascript">
    $(function () {
        //토글이벤트
        $(".faq-list li .hd").click(function () {
            $(this).parent().toggleClass("on");
            $(this).parent().siblings().removeClass("on")
        });
    });

</script>
<?php
echo $OUTPUT->footer();
?>


