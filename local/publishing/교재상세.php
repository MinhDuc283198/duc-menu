<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('교재목록');

echo $OUTPUT->header();
?>  


<div class="bk-detail">
    <div class="bk-img"><img src="/theme/oklassedu/pix/images/ex_book03.png" alt="안녕하세요! 한국어 [입문자용]"/></div>
    <div class="bk-txt">
        <div class="tit">안녕하세요! 한국어 [입문자용]</div>
        <div class="t-gray"><span>도서출판 밍밍</span><span>김한글 외 4명</span></div>
        <p>
            <span>금액</span>
            <strong>
                <strong class="p-down"><span>600,000 VND</span><span>15%</span></strong>
                <em>10,000</em>VND
            </strong>
        </p>
        <a href="#" class="btns point big openPop">결제하기</a>
        <div class="txt">
            <div>교재소개</div>
            <p>서울대학교 대학원 외국어로서의 한국어 학과에서 강의 중인 5명의 교수가 집필한, 총 24개의 주제로 구성된 이 교재는 영국에서 최초로 시작되어 일년에 한 바퀴 돌면서 공부하는 사람에게 행운을 주었고 지금은 당신에게 옮겨진 이 교재는 60일 안에 당신 곁을 떠나야 합니다.</p>
            <p>이 교재를 포함해서 7권을 한국어 공부가 필요한 사람에게 보내 주셔야 합니다. 할부로 구매해도 좋습니다.혹 미신이라 하실 지 모르지만 사실입니다. 영국에서 Henry Carpenter 라는 사람은 2013년에 이 교재를 받았습니다. 그는 비서에게 더 구매하여 보내라고 했습니다. 며칠 뒤에 복권에 당첨되어 200만 유로를 받았습니다. </p>
        </div>
    </div>

</div>



<h5 class="pg-tit">이 교재의 관련 강좌</h5>
<div class="crs-bx">
    <ul class="crs-list">
        <li>
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_list_img01.png" alt="TOPIK 4급 취득 실전 모의고사 – 한국어ver." />
            </div>
            <div class="txt">
                <div>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</div>
                <p><span>쩡 레 응우옌 선생님</span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns blue">수강신청</a>
            </div>

        </li>
        <li>
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_list_img01.png" alt="TOPIK 4급 취득 실전 모의고사 – 한국어ver." />
            </div>
            <div class="txt">
                <div>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</div>
                <p><span>쩡 레 응우옌 선생님</span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns gray">수강중</a>
            </div>
        </li>
    </ul>
</div>



<h5 class="pg-tit">이 교재의 관련 교재</h5>
<div class="crs-bx">
    <ul class="crs-list book">
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16">정가 12,000 VND</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>

        </li>
    </ul>
</div>
<script type="text/javascript">
    $(function () {
        //배송정보입력 팝업 
        $(".openPop").click(function () {
            utils.popup.call_layerpop("./팝업_배송정보입력.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"param": "11"});
            return false;
        })
    });
</script>
<?php
echo $OUTPUT->footer();
?>


