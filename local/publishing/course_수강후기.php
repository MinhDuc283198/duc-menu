<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add('수강후기');


echo $OUTPUT->header();
?>  
<h6 class="has-star pg-tit">
    수강후기
    <div class="f-r">
        <div class="star-event big" data-num="4"></div>
        <span>3점</span>
        <span class="t-gray">(65개)</span>
    </div>
</h6>

<div class="input-review">
    <div class="tp">
        별점을 선택해주세요
        <div class="star-event hover" data-num="0"></div>
    </div>
    <div class="bt">
         <textarea placeholder="수강후기를 입력하세요"></textarea>
         <input type="submit" class="btns" value="등록"/>
    </div>
</div>


<!-- 수강후기 없는경우 
<div class="no-data">
        <div>등록된 수강후기가 없습니다.<br/>수강을 완료하고 첫 번째로 후기를 등록해보세요!</div>
</div>
-->

<ul class="review-list">
    <li>
        <div class="tp">
            <strong>홍길동</strong>
            <span class="tm">15분 전</span>
            <div class="star-event" data-num="4"></div>
        </div>
        <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
        <div class="btn-area text-right">
            <a href="#" class="btns br">수정</a>
            <a href="#" class="btns br">삭제</a>
        </div>
    </li>
    <li>
        <div class="tp">
            <strong>홍길동</strong>
            <span class="tm">15분 전</span>
            <div class="star-event" data-num="3"></div>
        </div>
        <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
    </li>
    <li>
        <div class="tp">
            <strong>홍길동</strong>
            <span class="tm">15분 전</span>
            <div class="star-event" data-num="2"></div>
        </div>
        <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
    </li>
    <li>
        <div class="tp">
            <strong>홍길동</strong>
            <span class="tm">15분 전</span>
            <div class="star-event" data-num="3"></div>
        </div>
        <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
    </li>
</ul>
<?php
echo $OUTPUT->footer();
?>


