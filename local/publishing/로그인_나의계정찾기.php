<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('계정조회');

echo $OUTPUT->header();
?>  
<div class="login-bx">
    <div class="bg">
        <div class="center-tit">나의 계정 찾기</div>
        <p class="center-txt">회원가입 시 등록한 정보로 나의 계정을 찾을 수 있습니다.</p>
    </div>
    <p class="ft-dot">필수입력정보입니다.</p>
    <form class="email-bx join">
        <p>
            <strong>이름</strong>
            <input type="text" placeholder="이름입력" />
        </p>
        <p>
            <strong>생년월일</strong>
            <select>
                <option value="" hidden>년도</option>
                <option value="">1989</option>
            </select>
            <select>
                <option value="" hidden>월</option>
                <option value="">05</option>
            </select>
            <select>
                <option value="" hidden>일</option>
                <option value="">07</option>
            </select>
        </p>
        <p>
            <strong>이메일</strong>
            <input type="text" placeholder="이메일입력" />
        </p>

    </form>
    <div class="text-center btn-area">
        <input type="button" value="확인" class="btns point big" />
    </div>

</div>


<?php
echo $OUTPUT->footer();
?>


