<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('검색결과');

echo $OUTPUT->header();
?>  
<div class="tp-search-area">
    <from>
        <div class="input-div">
            <input type="text" />
            <input type="submit" class="btn-search" value="검색" />
        </div>
    </from>
</div>

<h4 class="pg-tit">강좌 검색결과 (6)</h4>
<div class="crs-bx no-mg">
    <ul class="crs-list">
        <li>
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_list_img01.png" alt="TOPIK 4급 취득 실전 모의고사 – 한국어ver." />
            </div>
            <div class="txt">
                <div>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</div>
                <p><span>쩡 레 응우옌 선생님</span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns blue">수강신청</a>
            </div>

        </li>
        <li>
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_list_img01.png" alt="TOPIK 4급 취득 실전 모의고사 – 한국어ver." />
            </div>
            <div class="txt">
                <div>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</div>
                <p><span>쩡 레 응우옌 선생님</span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns gray">수강중</a>
            </div>
        </li>
    </ul>
</div>
<a href="#" class="btn-more mg-bt60">더보기</a>



<h4 class="pg-tit">교재 검색결과 (5)</h4>
<div class="crs-bx">
    <ul class="crs-list book big">
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16 t-blue">
                    <strong class="p-down"><span>600000 VND</span><span>15</span></strong>
                    12,000 VND
                </p>
                <p class="f18">[관련강좌]</p>
                <ul>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                </ul>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>
        </li>
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16 t-blue">
                    <strong class="p-down"><span>600000 VND</span><span>15</span></strong>
                    12,000 VND
                </p>
                <p class="f18">[관련강좌]</p>
                <ul>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                </ul>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>
        </li>
    </ul>
</div>
<a href="#" class="btn-more">더보기</a>
<?php
echo $OUTPUT->footer();
?>


