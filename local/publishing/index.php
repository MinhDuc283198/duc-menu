<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link href="/theme/oklassedu/style/common.css" rel="stylesheet"/>
<link href="/theme/oklassedu/style/style.old.css" rel="stylesheet"/>
<link href="/theme/oklassedu/style/style.css" rel="stylesheet"/>
<style type="text/css">
    body{
        padding: 50px;
    } 
    h3{
        font-size: 25px;
        margin-bottom: 10px;
    }
    ul li{
        list-style-type: decimal; 
        list-style-position: inside;
        font-size: 18px;
        padding: 5px 0;
    }
    a{
        text-decoration: underline;
    }
    a:hover{
        text-decoration: underline;
        font-weight: bold;
    }
    .table td, .table th{
        font-size: 16px !important;
    }
    .brbox{
        border: 1px solid #ddd;
        padding: 20px;
        margin-bottom: 20px;
    }
    .brbox > p{
        margin-bottom: 5px;
        font-size: 15px;
        color: #707070;
    }
    table{
        border-spacing: 0; 
        border-collapse: collapse;
    }
    .table {
        margin-bottom: 50px !important;
        margin-top: 0;
        border-top: 2px solid #505050;
        border-bottom: 1px solid #ddd;
        font-size: 14px;
        width: 100%;
        margin-bottom: 20px;
        table-layout: fixed;
        width: 100%;
    }
    .table-bordered {
        border-left-width: 0;
        border-right-width: 0;
    }
    .table.margin-top{margin-top: 10px;}
    thead {
        display: table-header-group;
        vertical-align: middle;
        border-color: inherit;
    }
    tr {
        display: table-row;
        vertical-align: inherit;
        border-color: inherit;
    }

    .table > thead > tr > th {
        text-align: center;
        border-bottom-width: 1px;
        background-color: #f6f6f6;
        vertical-align: middle;
        color: #202020;
        word-break: keep-all;
        word-wrap: break-word;
    }
    .table-bordered > thead > tr > th
    , .table-bordered > tbody > tr > th
    , .table-bordered > tfoot > tr > th {
        border-left-width: 0;
        border-right-color: #eee;
    }
    .table > thead > tr > th:last-child{
        background-image: none;
    }
    .table-bordered > thead > tr > th:first-child
    , .table-bordered > tbody > tr > td:first-child{
        border-left-width: 0;
    }
    .table-bordered > thead > tr > th:last-child
    , .table-bordered > tbody > tr > td:last-child{
        border-right-width: 0;
    }

    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }
    .table > tbody > tr > th {
        padding: 10px;
        text-align: center;
        background-color: #f6f6f6;
        vertical-align: middle;
        min-width: 50px;
        color: #202020;
    }
    .table > tbody > tr > td{
        font-size: 14px;
        color: #707070;
        font-weight: normal;
    }
    .table-bordered > thead > tr > td
    , .table-bordered > tbody > tr > td
    , .table-bordered > tfoot > tr > td {
        border-left-width: 0;
        border-right-color: #e0e0e0;
    }
    .table > thead > tr > th, .table > tbody > tr > th
    , .table > tfoot > tr > th, .table > thead > tr > td
    , .table > tbody > tr > td
    , .table > tfoot > tr > td {
        border-top-color: #e0e0e0;
    }
    .table-bordered > thead > tr > th
    , .table-bordered > tbody > tr > th
    , .table-bordered > tfoot > tr > th
    , .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td
    , .table-bordered > tfoot > tr > td {
        border: 1px solid #e0e0e0;
    }
    .table-condensed > thead > tr > th
    , .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th{
        padding: 10px;
    }
    .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td
    , .table-condensed > tfoot > tr > td {
        padding: 25px 10px;
    }
    .table > thead > tr > th, .table > tbody > tr > th
    , .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td
    , .table > tfoot > tr > td {
        line-height: 1.42857143;
        vertical-align: middle;
        border-top: 1px solid #e0e0e0;
        text-align: center;
        padding: 10px;
        word-break: keep-all;
        word-wrap: initial;
    }
    .table tbody tr td{color: #000;font-size: 18px;}
</style>
<h3>비상 퍼블리싱 목록</h3>

<table class="table table-bordered">
    <colgroup>
        <col width="100px" />
        <col width="/" />
        <col width="/" />
        <col width="/" />
    </colgroup>
    <thead>
        <tr>
            <th>No</th>
            <th>페이지명</th>
            <th>파일위치</th>
            <th>비고</th>
        </tr> 
    </thead>
    <tbody>
        <tr>
            <td>01</td>
            <td>강좌목록</td>
            <td class="text-left">
                <a href="/local/publishing/강좌목록.php" target="_blank">/local/publishing/강좌목록.php</a><br/>
                <a href="/local/publishing/강좌목록_데이터없음.php" target="_blank">/local/publishing/강좌목록_데이터없음.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>02</td>
            <td>강좌상세</td>
            <td class="text-left"><a href="/local/publishing/강좌상세.php" target="_blank">/local/publishing/강좌상세.php</a></td>
            <td class="text-left">
                <p class="t-blue">※ 결제하기 버튼 클릭 시 배송정보 팝업 추가</p>
                <p class="t-blue">※ 수강후기 없음 퍼블 주석 확인 필요</p>
            </td>
        </tr> 
        <tr>
            <td>03</td>
            <td>개인정보수정</td>
            <td class="text-left"><a href="/local/publishing/개인정보수정.php" target="_blank">/local/publishing/개인정보수정.php</a></td>
            <td class="text-left">
                <p class="t-blue">※ 비밀번호 변경 버튼 클릭 시 팝업 이벤트</p>
                <p class="t-blue">※ pc디자인 확정 전</p>
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>04</td>
            <td>회원가입</td>
            <td class="text-left">
                <a href="/local/publishing/회원가입정보동의.php" target="_blank">/local/publishing/회원가입정보동의.php</a><br/>
                <a href="/local/publishing/회원가입유형선택.php" target="_blank">/local/publishing/회원가입유형선택.php</a><br/>
                <a href="/local/publishing/회원가입필수입력정보.php" target="_blank">/local/publishing/회원가입필수입력정보.php</a><br/>
                <a href="/local/publishing/이메일인증완료.php" target="_blank">/local/publishing/이메일인증완료.php</a><br/>
                <a href="/local/publishing/인증메일발송완료.php" target="_blank">/local/publishing/인증메일발송완료.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 약관내용 추가예정</p>
            </td>
        </tr>
        <tr>
            <td>05</td>
            <td>계정찾기</td>
            <td class="text-left">
                <a href="/local/publishing/로그인_나의계정찾기.php" target="_blank">/local/publishing/로그인_나의계정찾기.php</a><br/>
                <a href="/local/publishing/로그인_계정찾기_계정조회.php" target="_blank">/local/publishing/로그인_계정찾기_계정조회.php</a><br/>
                <a href="/local/publishing/로그인_계정찾기_계정조회_소셜.php" target="_blank">/local/publishing/로그인_계정찾기_계정조회_소셜.php</a>
            </td>
            <td class="text-left">
            </td>
        </tr>
        <tr>
            <td>06</td>
            <td>교재목록</td>
            <td class="text-left">
                <a href="/local/publishing/교재목록.php" target="_blank">/local/publishing/교재목록.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>07</td>
            <td>교재상세</td>
            <td class="text-left">
                <a href="/local/publishing/교재상세.php" target="_blank">/local/publishing/교재상세.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 결제하기 버튼 클릭 시 배송정보 팝업 추가</p>
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>08</td>
            <td>관심정보설정 팝업</td>
            <td class="text-left">
                <a href="/local/publishing/관심정보설정.php" target="_blank">/local/publishing/관심정보설정.php</a>
            </td>
            <td class="text-left">
            </td>
        </tr>
        <tr>
            <td>09</td>
            <td>나의강좌</td>
            <td class="text-left">
                <a href="/local/publishing/나의강좌_수강중인강좌.php" target="_blank">/local/publishing/나의강좌_수강중인강좌.php</a> <br/>
                <a href="/local/publishing/나의강좌_수강완료강좌.php" target="_blank">/local/publishing/나의강좌_수강완료강좌.php</a> <br/>
                <a href="/local/publishing/나의강좌_관심강좌.php" target="_blank">/local/publishing/나의강좌_관심강좌.php</a> <br/>
                <a href="/local/publishing/나의강좌_강좌없음.php" target="_blank">/local/publishing/나의강좌_강좌없음.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>10</td>
            <td>강좌서브메인</td>
            <td class="text-left">
                <a href="/local/publishing/강좌_서브메인.php" target="_blank">/local/publishing/강좌_서브메인.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>11</td>
            <td>FAQ</td>
            <td class="text-left">
                <a href="/local/publishing/FAQ.php" target="_blank">/local/publishing/FAQ.php</a>
            </td>
            <td class="text-left">
            </td>
        </tr>
        <tr>
            <td>12</td>
            <td>검색결과</td>
            <td class="text-left">
                <a href="/local/publishing/검색결과.php" target="_blank">/local/publishing/검색결과.php</a><br/>
                <a href="/local/publishing/검색결과_데이터없음.php" target="_blank">/local/publishing/검색결과_데이터없음.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>13</td>
            <td>알림</td>
            <td class="text-left">
                <a href="/local/publishing/알림.php" target="_blank">/local/publishing/알림.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 알림없음 주석 확인 필요</p>
            </td>
        </tr>
        <tr>
            <td>14</td>
            <td>1:1상담내역 & 공지사항 & 자료실</td>
            <td class="text-left">
                <a href="/local/publishing/1대1상담내역.php" target="_blank">/local/publishing/1대1상담내역.php</a><br/>
                <a href="/local/publishing/1대1상담내역_상세.php" target="_blank">/local/publishing/1대1상담내역_상세.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 공지사항 배경색 추가</p>
            </td>
        </tr>
        <tr>
            <td>15</td>
            <td>결제내역</td>
            <td class="text-left">
                <a href="/local/publishing/결제내역.php" target="_blank">/local/publishing/결제내역.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>16</td>
            <td>내정보</td>
            <td class="text-left">
                <a href="/local/publishing/내정보.php" target="_blank">/local/publishing/내정보.php</a><br/>
                <a href="/local/publishing/내정보_데이터없음.php" target="_blank">/local/publishing/내정보_데이터없음.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>17</td>
            <td>회원탈퇴</td>
            <td class="text-left">
                <a href="/local/publishing/회원탈퇴.php" target="_blank">/local/publishing/회원탈퇴.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 회원탈퇴 안내 내용 추가 예정</p>
            </td>
        </tr>
        <tr>
            <td>18</td>
            <td>커뮤니티</td>
            <td class="text-left">
                <a href="/local/publishing/커뮤니티.php" target="_blank">/local/publishing/커뮤니티.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>19</td>
            <td>게시판</td>
            <td class="text-left">
                <a href="/local/publishing/게시판리스트.php" target="_blank">/local/publishing/게시판리스트.php</a><br/>
                <a href="/local/publishing/게시판리스트_데이터없음.php" target="_blank">/local/publishing/게시판리스트_데이터없음.php</a><br/>
                <a href="/local/publishing/게시판상세.php" target="_blank">/local/publishing/게시판상세.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>20</td>
            <td>이용안내 & 서비스 운영규정 & 개인정보 보호방침</td>
            <td class="text-left">
                <a href="/local/publishing/이용약관.php" target="_blank">/local/publishing/이용약관.php</a>
            </td>
            <td class="text-left">
                <p class="t-blue">※ 약관내용 추후 작업 예정</p>
            </td>
        </tr>
        <tr>
            <td>21</td>
            <td>인증 메일 내용</td>
            <td class="text-left">
                <a href="/local/publishing/인증메일내용.html" target="_blank">/local/publishing/인증메일내용.html</a>
            </td>
            <td class="text-left">
            </td>
        </tr>
    </tbody>
</table>



<h3>비상 강의실 퍼블리싱 목록</h3>

<table class="table table-bordered">
    <colgroup>
        <col width="100px" />
        <col width="/" />
        <col width="/" />
        <col width="/" />
    </colgroup>
    <thead>
        <tr>
            <th>No</th>
            <th>페이지명</th>
            <th>파일위치</th>
            <th>비고</th>
        </tr> 
    </thead>
    <tbody>
        <tr>
            <td>01</td>
            <td>강의목록</td>
            <td class="text-left"><a href="/local/publishing/course.php" target="_blank">/local/publishing/course.php</a></td>
            <td class="text-left">
                <p class="t-blue">※ 모바일 퍼블 추후 작업 예정</p>
                <p class="t-blue">※ 복습기간 영역 추가</p>
            </td>
        </tr>
        <tr>
            <td>02</td>
            <td>수강후기</td>
            <td class="text-left"><a href="/local/publishing/course_수강후기.php" target="_blank">/local/publishing/course_수강후기.php</a></td>
            <td class="text-left">
            </td>
        </tr>
    </tbody>
</table>