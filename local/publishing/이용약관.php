<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('이용약관');

echo $OUTPUT->header();
?>  

<ul class="mk-c-tab tab-event">
    <li class="on" data-target=".data01"><a href="#">이용약관</a></li>
    <li data-target=".data02"><a href="#">서비스 운영 규정</a></li>
    <li data-target=".data03"><a href="#">개인정보 보호 방침</a></li>
</ul>

<div class="target-area">
    <div class="data01 on">
        <div class="mg-bt10 text-right">
            <span class="t-gray">시행일</span>
            <select class="margin_l10">
                <option value="">2019.11.11</option>
            </select>
        </div>
        이용약관 내용 업데이트 예정
    </div>
    <div class="data02">
        <div class="mg-bt10 text-right">
            <span class="t-gray">시행일</span>
            <select class="margin_l10">
                <option value="">2019.11.11</option>
            </select>
        </div>
        서비스 운영 규정 내용 업데이트 예정
    </div>
    <div class="data03">
        <div class="mg-bt10 text-right">
            <span class="t-gray">시행일</span>
            <select class="margin_l10">
                <option value="">2019.11.11</option>
            </select>
        </div>
        개인정보 보호 방침 내용 업데이트 예정
    </div>
</div>



<?php
echo $OUTPUT->footer();
?>


