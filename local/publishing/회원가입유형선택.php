<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  

<form class="email-bx">
    <p>
        <input type="text" placeholder="이메일 입력" />
        <input type="button" class="btns gray" value="중복확인" />
        <span class="warning">이메일 입력조건 안내 문구</span>
    </p>
    <p>
        <input type="password" placeholder="비밀번호 입력" />
        <span class="warning">비밀번호 입력조건 안내 문구</span>
    </p>
    <div class="btns-area text-center">
        <input type="submit" value="이메일로 시작하기" class="btns br" />
    </div>

</form>

<div class="sns-area">
    <a href="#" class="ic-fb">
        <span class="ic"><img src="/theme/oklassedu/pix/images//icon_facebook_big.png" alt="facebook"/></span>
        <span>Facebook 계정으로 로그인</span>
    </a>
    <a href="#" class="ic-gl">
        <span class="ic"><img src="/theme/oklassedu/pix/images//icon_google.png" alt="google"/></span>
        <span>Google 계정으로 로그인</span>
    </a>
    <a href="#" class="ic-zl">
        <span class="ic"><img src="/theme/oklassedu/pix/images//icon_zalo.png" alt="zalo"/></span>
        <span>zalo 계정으로 로그인</span>
    </a>
</div>

<?php
echo $OUTPUT->footer();
?>


