<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('비밀번호 변경');

echo $OUTPUT->header();
?>  
<div class="layerpop chng-pw">
    <div class="pop-title">
        비밀번호 변경
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <div class="rw">
            <div class="input-tit">현재 비밀번호</div>
            <input type="password" class="w100" />
            <p class="t-red warning">현재 비밀번호가 일치하지 않습니다.</p>
        </div>
        <div class="rw">
            <div class="input-tit">새로운 비밀번호</div>
            <input type="password" class="w100" />
            <p class="t-red warning">비밀번호 입력조건 안내 문구</p>
        </div>
        <div class="rw">
            <div class="input-tit">새로운 비밀번호 확인</div>
            <input type="password" class="w100" />
            <p class="t-red warning">비밀번호 입력조건 안내 문구</p>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="변경하기" class="btns point" />
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


