<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('결제');

echo $OUTPUT->header();
?>  
<div class="layerpop window">
    <div class="pop-title">
        Checkout
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <ul class="mk-c-tab">
            <li><a href="#"><span class="nm">1</span>Information</a></li>
            <li class="on"><a href="#"><span class="nm">2</span>Payment details</a></li>
            <li><a href="#"><span class="nm">3</span>Review your order</a></li>
        </ul>

        <div class="addr-info style02">
            <div class="rw">
                <p class="tit">Payment method</p>
            </div>
            <ul class="pay-type">
                <li class="on">
                    <img src="/theme/oklassedu/pix/images/ex_momo.jpeg"/>
                    MoMo AIO Payment
                </li>
                <li>
                    <img src="/theme/oklassedu/pix/images/ex_momo.jpeg"/>
                    MoMo ATM Card, internet banking of Vietnam banks.
                </li>
                <li>
                    <img src="/theme/oklassedu/pix/images/ex_momo.jpeg"/>
                    MoMo ATM Card, internet banking of Vietnam banks.
                </li>
            </ul>

        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="NEXT" class="btns point w100" />
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


