<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('결제내역');

echo $OUTPUT->header();
?>  
<h2 class="pg-tit">결제내역</h2>
<div class="tp-tb-area text-right">
    <ul class="bar-tab">
        <li class="on"><a href="#">전체</a></li>
        <li><a href="#">강좌</a></li>
        <li><a href="#">교재</a></li>
    </ul>
</div>
<table class="table m-block">
    <thead>
        <tr>
            <th width="7%">번호</th>
            <th width="7%">품목</th>
            <th>결제상품명</th>
            <th width="11%">결제금액</th>
            <th width="10%">결제일</th>
            <th width="10%">결제수단</th>
            <th width="9%">배송상태</th>
            <th width="60px">환불하기</th>
            <th width="60px">영수증</th>
        </tr>
    </thead>
    <tbody>
        <!-- 데이터 없는경우 
        <tr><td colspan="9">결제 내역이 없습니다.</td></tr> -->
        <tr>

            <td class="m-hide">7</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">배송중</td>
            <td class="m-col3"><a href="#" class="btns br_blue">환불</a></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>

        </tr>
        <tr>
            <td class="m-hide">9</td>
            <td class="m-hide">강좌</td>
            <td class="text-left">TOPIKⅡ(5,6급) – 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">-</td>
            <td class="m-col3"><a href="#" class="btns br_blue">환불</a></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
        <tr>
            <td class="m-hide">8</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>무통장입금</td>
            <td class="m-col3 m-clear">-</td>
            <td class="m-col3"><span class="t-black">승인대기</span></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
        <tr>
            <td class="m-hide">7</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">배송완료</td>
            <td class="m-col3"><span class="t-red">환불불가</span></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
        <tr>
            <td class="m-hide">6</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">구매취소</td>
            <td class="m-col3"><span class="t-blue02">환불완료</span></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
    </tbody>
</table>
<ul class="mk-paging">
    <li class="first"><a href="#">first</a></li>
    <li class="prev"><a href="#">prev</a></li>
    <li class="on"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">6</a></li>
    <li><a href="#">7</a></li>
    <li><a href="#">8</a></li>
    <li><a href="#">9</a></li>
    <li><a href="#">10</a></li>
    <li class="next"><a href="#">next</a></li>
    <li class="end"><a href="#">end</a></li>
</ul>
<?php
echo $OUTPUT->footer();
?>


