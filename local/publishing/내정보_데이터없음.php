<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('내정보');

echo $OUTPUT->header();
?>  
<div class="my-box imprt half">
    <div class="rw">
        <strong class="t-black">김비상</strong>
        <p>
            <span class="t-gray">가입일</span>
            <span class="t-blue02">2018.07.23</span>
            <a href="#" class="btns br">개인정보수정</a>
        </p>
    </div>
    <div class="rw">
        <strong>아이디</strong>
        <p>
            <img src="/theme/oklassedu/pix/images/icon_sns.png" alt="페이스북" />
            <span>visang@facebook.com</span>
        </p>
    </div>
    <div class="rw">
        <strong>이메일</strong>
        <p><strong>visang@gmail.com </strong></p>
    </div>
    <div class="rw">
        <strong>휴대폰 번호</strong>
        <p><strong>010-0408-0506</strong></p>
    </div>
    

    <div class="my-crs-list">
        <div class="bg-tit">내 강좌 내역</div>
        <div class="num">
            <ul>
                <li>
                    <strong>수강중인 강좌</strong>
                    <span class="t-blue02"><em>3</em>개</span>
                </li>
                <li>
                    <strong>내가 구매한 강좌</strong>
                    <span class="t-blue02"><em>10</em>개</span>
                </li>
                <li>
                    <strong>학습 완료 강좌</strong>
                    <span class="t-blue02"><em>37</em>개</span>
                </li>
                <li>
                    <strong>수료한 강좌</strong>
                    <span class="t-blue02"><em>33</em>개</span>
                </li>
            </ul>
        </div>
    </div>

</div>

<h4 class="pg-tit">수강중인 강좌 <a href="#" class="mr_all">전체보기</a></h4>
<div class="no-data style02 mg-bt30">
    <div>현재 수강중인 강좌가 없습니다.</div>
    <a href="#" class="btns point arrow">전체 강좌 보러가기</a>
</div>


<h4 class="pg-tit">1:1 상담 내역<a href="#" class="mr_all">전체보기</a></h4>
<table class="table mg-bt50 m-block">
    <thead>
        <tr>
            <th width="10%">번호</th>
            <th>제목</th>
            <th width="15%">작성자</th>
            <th width="20%">작성일</th>
            <th width="10%">조회수</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5">1:1 상담내역이 없습니다.</td>
        </tr>

    </tbody>
</table>

<h4 class="pg-tit">결제내역<a href="#" class="mr_all">전체보기</a></h4>
<table class="table m-block">
    <thead>
        <tr>
            <th width="5%">번호</th>
            <th width="7%">품목</th>
            <th>결제상품명</th>
            <th width="11%">결제금액</th>
            <th width="10%">결제일</th>
            <th width="10%">결제수단</th>
            <th width="8%">배송상태</th>
            <th width="60px">환불하기</th>
            <th width="60px">영수증</th>
        </tr>
    </thead>
    <tbody>
        <tr><td colspan="9">결제 내역이 없습니다.</td></tr> 
    </tbody>
</table>
<?php
echo $OUTPUT->footer();
?>


