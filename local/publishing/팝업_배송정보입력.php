<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('배송정보입력');

echo $OUTPUT->header();
?>  
<div class="layerpop">
    <div class="pop-title">
        배송정보입력
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <div class="addr-info">
            <div class="rw">
                <p class="tit">주소</p>
                <input type="text" value="Vinhome central park . Park 6A" />
                <input type="text" value="208 nguyễn hữu cảnh phường 22 quận bình thạnh.tp HCM" />
                <p class="t-gray">입력한 주소로 교재가 배송됩니다. 주소를 정확하게 입력해주세요.</p>
            </div>
            <div class="rw phone">
                <p class="tit">휴대폰 번호</p>
                <input type="text" value="010" />
                <span>-</span>
                <input type="text" value="222" />
                <span>-</span>
                <input type="text" value="3333" />

                <p class="t-gray">연락 가능한 번호를 입력해주세요.</p>
            </div>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="결제하기" class="btns point w100" />
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


