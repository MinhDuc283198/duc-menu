<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  

<div class="mail-end-bx style02">
    <div>인증 메일 발송 완료</div>
    <p>
        가입한 이메일 주소로 인증 메일이 발송되었습니다.<br />
        메일 확인 후 인증을 완료하시기 바랍니다.
    </p>
    <p class="small">
        메일이 정상적으로 확인되지 않을 경우 스팸함을 확인해주시고,<br/>
        스팸함에서도 확인되지 않을 경우 인증 메일 재발송 버튼을 클릭하거나 담당자에게 문의바랍니다.<br/>
        (담당자 0000-0000)
    </p>
    <a href="#" class="btns point big">인증 메일 재발송</a>
    <a href="#" class="btns big">홈으로 이동</a>
</div>


<?php
echo $OUTPUT->footer();
?>


