<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('검색결과');

echo $OUTPUT->header();
?>  
<div class="tp-search-area">
    <from>
        <div class="input-div">
            <input type="text" />
            <input type="submit" class="btn-search" value="검색" />
        </div>
    </from>
</div>

<div class="no-data style02">
    <div class="f24">"<strong class="t-blue">TOPIC</strong>"에 대한 검색결과가 없습니다</div>
    <p>검색어를 맞게 입력했는지 확인하시고 다시 검색해주세요.</p>
</div>
<?php
echo $OUTPUT->footer();
?>


