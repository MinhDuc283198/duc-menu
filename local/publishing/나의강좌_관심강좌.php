<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('나의강좌');

echo $OUTPUT->header();
?>  
<ul class="mk-c-tab tab-event">
    <li><a href="#">수강중인 강좌</a></li>
    <li><a href="#">수강완료 강좌</a></li>
    <li class="on"><a href="#">관심 강좌</a></li>
</ul>

<ul class="thumb-list course style04">
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt gray">수강중</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img01.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>

        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="[초급]한국어 초급 문법(베트남어)" />
            </div>
            <div class="txt">
                <div class="tit">[초급]한국어 초급 문법(베트남어)</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
                <a href="#" class="bt">수강신청</a>
            </div>
        </div>
    </li>
</ul>

<?php
echo $OUTPUT->footer();
?>


