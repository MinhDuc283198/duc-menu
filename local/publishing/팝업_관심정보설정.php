<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

echo $OUTPUT->header();
?>  
<div class="layerpop choose">
    <div class="pop-title">
        관심정보 설정
        <span class="sub">관심정보를 설정하고 강좌를 추천 받으세요.</span>
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <div class="ch-btns-area tab-event">
            <a href="#" class="btns on">한국어 못함</a>
            <a href="#" class="btns">짧은 대화 가능</a>
            <a href="#" class="btns">일상대화 가능</a>
            <a href="#" class="btns">비즈니스 회화 가능</a>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="선택완료" class="btns point big" />
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


