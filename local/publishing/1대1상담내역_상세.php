<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('1:1상담내역');

echo $OUTPUT->header();
?>  
<h2 class="pg-tit">1:1상담내역</h2>

<div class="tb-detail-tit">
    <!-- 답글인 경우 추가 -->
    <span class="tb-reply">re</span>
    결제했는데 강의실 입장이 안 됩니다.</div>
<div class="tb-info">
    <p class="f-l">
        <strong>김비상</strong>
        <span>2019.08.12 월요일 오후 14:02</span>
    </p>
    <div class="f-r">
        <span>조회 2</span>
    </div>
</div>
<div class="tb-cont">
    안녕하세요. 저 결제 했는데 왜 강의실 못 들어가요? 내 돈 돌려내.<br />
    안녕하세요. 저 결제 했는데 왜 강의실 못 들어가요? 내 돈 돌려내.<br />
    안녕하세요. 저 결제 했는데 왜 강의실 못 들어가요? 내 돈 돌려내.<br />
    안녕하세요. 저 결제 했는데 왜 강의실 못 들어가요? 내 돈 돌려내.
</div>
<div class="tb-file-area">
    <div class="tit">첨부파일</div>
    <ul>
        <li><a href="#">190718_TOPIKⅡ(5,6급)_-_문법편_결제영수증_vising.pdf</a></li>
    </ul>
</div>

<div class="tb-btns-area">
    <input type="button" value="목록" class="btns br h40" />
    <div class="f-r">
        <input type="button" value="수정" class="btns br h40" />
        <input type="button" value="삭제" class="btns br h40" />
    </div>
</div>
<?php
echo $OUTPUT->footer();
?>


