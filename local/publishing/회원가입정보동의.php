<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  
<div class="login-bx">
    <ul class="login-tab tab-event">
        <li><a href="#">로그인</a></li>
        <li class="on"><a href="#">회원가입</a></li>
    </ul>

    <div class="login-tit">이용약관(<a href="#">전체보기</a>)</div>
    <div class="scrl-box">약관 내용 추가 예정</div>


    <div class="login-tit">서비스 운영 규정(<a href="#">전체보기</a>)</div>
    <div class="scrl-box">약관 내용 추가 예정</div>


    <div class="login-tit">개인정보 보호 방침(<a href="#">전체보기</a>)</div>
    <div class="scrl-box">약관 내용 추가 예정</div>


    <p class="info">위 항목에 동의하지 않는 경우 회원가입이 불가합니다.</p>

    <div class="text-center btn-area">
        <input type="button" value="동의합니다." class="btns point big" />
    </div>

</div>

<?php
echo $OUTPUT->footer();
?>


