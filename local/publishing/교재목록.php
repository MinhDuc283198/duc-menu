<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('교재목록');

echo $OUTPUT->header();
?>  

<div class="crs-bx">
    <ul class="crs-list book big">
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16 t-blue">
                    <strong class="p-down"><span>600,000 VND</span><span>15%</span></strong>
                    12,000 VND
                </p>
                <p class="f18">[관련강좌]</p>
                <ul>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                </ul>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>
        </li>
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16 t-blue">
                    <strong class="p-down"><span>600,000 VND</span><span>15%</span></strong>
                    12,000 VND
                </p>
                <p class="f18">[관련강좌]</p>
                <ul>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                </ul>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>
        </li>
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16 t-blue">
                    <strong class="p-down"><span>600,000 VND</span><span>15%</span></strong>
                    12,000 VND
                </p>
                <p class="f18">[관련강좌]</p>
                <ul>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                </ul>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>
        </li>
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16 t-blue">
                    <strong class="p-down"><span>600,000 VND</span><span>15%</span></strong>
                    12,000 VND
                </p>
                <p class="f18">[관련강좌]</p>
                <ul>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                    <li>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</li>
                </ul>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>
        </li>
    </ul>
</div>


<?php
echo $OUTPUT->footer();
?>


