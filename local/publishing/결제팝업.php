<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');

echo $OUTPUT->header();
?>  
<input type="button" value="결제정보01"  class="btns pop01" />
<input type="button" value="결제정보02" class="btns pop02" >

<script type="text/javascript">
    $(function () {
        $(".pop01").click(function () {
            utils.popup.call_windowpop("./팝업_결제01.php", 1200, 800);
            return false;
        });
         $(".pop02").click(function () {
            utils.popup.call_windowpop("./팝업_결제02.php", 1200, 800);
            return false;
        })
    });
</script>
<?php
echo $OUTPUT->footer();
?>


