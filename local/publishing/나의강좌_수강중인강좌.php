<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('나의강좌');

echo $OUTPUT->header();
?>  
<ul class="mk-c-tab tab-event">
    <li class="on"><a href="#">수강중인 강좌</a></li>
    <li><a href="#">수강완료 강좌</a></li>
    <li><a href="#">관심 강좌</a></li>
</ul>

<ul class="thumb-list style04 course">
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(90%)</span>
                <p class="bar-event" data-num="90%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
</ul>
<?php
echo $OUTPUT->footer();
?>


