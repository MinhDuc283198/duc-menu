<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('강좌상세');

echo $OUTPUT->header();
?>  
<div class="page-nav has-icon">
    <p>한국어 강좌</p>
    <p>초급과정</p>
    <div class="sub-mn">
        <span>메뉴아이콘</span>
        <ul>
            <li class="selected">
                <a href="#">한국어 강좌</a>
                <ul>
                    <li class="selected"><a href="#">초급과정</a></li>
                    <li><a href="#">중급과정</a></li>
                    <li><a href="#">고급과정</a></li>
                </ul>
            </li>
            <li>
                <a href="#">한국어 패키지</a>
                <ul>
                    <li class="selected"><a href="#">초급과정2</a></li>
                    <li><a href="#">중급과정2</a></li>
                    <li><a href="#">고급과정2</a></li>
                </ul>
            </li>
            <li>
                <a href="#">TOPIK 1,2급</a>
                <ul>
                    <li class="selected"><a href="#">초급과정3</a></li>
                    <li><a href="#">중급과정3</a></li>
                    <li><a href="#">고급과정3</a></li>
                </ul>
            </li>
            <li class="bt"><a href="#">커리큘럼(로드맵) 보기</a></li>
        </ul>
    </div>

    <div class="f-r">
        <a href="#" class="ic-share">공유</a>
        <a href="#" class="ic-heart">좋아요</a><!-- on -->
    </div>
</div>


<div class="course-info clearfix">
    <div class="crs-img">
        <img src="/theme/oklassedu/pix/images/ex_img_big.png" alt="[초급] 한국어 초급 문법 [베트남어]" />
        <p class="play">
            <span>play</span>
            <a href="#">강좌 미리보기</a>
        </p>
    </div>
    <div class="crs-txt">
        <div class="tit">[초급] 한국어 초급 문법 [베트남어]</div>
        <div class="star-area">
            <div class="star-event red" data-num="4"></div>
            <span>3점</span><span class="gray">(65개)</span>
        </div>
        <ul>
            <li>
                <span>강사</span>
                <strong>쩡 레 응우옌 선생님</strong>
            </li>
            <li>
                <span>교재</span>
                <strong>안녕하세요! 한국어 [입문자용]</strong>
            </li>
            <li>
                <span>수강기간</span>
                <strong>구매일로부터 60일</strong>
            </li>
            <li>
                <span>학습시간</span>
                <strong>02:12:00(25강)</strong>
            </li>

        </ul>
        <!--<a href="#" class="btns blue">수강신청</a>-->
    </div>
    <div class="sel-crs-list">
        <p class="info">교재가 포함된 강좌입니다.</p>
        <ul>
            <li>
                <span>강좌</span>
                <strong>[초급] 한국어 초급 문법 [베트남어]</strong>
            </li>
            <li>
                <span>교재</span>
                <strong>안녕하세요! 한국어 [입문자용]</strong>
            </li>
        </ul>
        <p class="price">
            <strong class="p-down"><span>600,000 VND</span><span>15%</span></strong>
            580,000<span>VND</span>
        </p>
        <input type="button" value="결제하기" class="bt gray" />
    </div>
</div>

<ul class="crs-tab tab-event">
    <!-- tab event -->
    <li class="on" data-target=".data01"><a href="#">강좌소개</a></li>
    <li data-target=".data02"><a href="#">강사소개</a></li>
    <li data-target=".data03"><a href="#">교재소개</a></li>
    <li data-target=".data04"><a href="#">강좌목록</a></li>
    <li data-target=".data05"><a href="#">수강후기</a></li>
</ul>

<div class="crs-tab-cont target-area">
    <div class="data01 on">
        <h6>강좌소개</h6>

        <div class="bx">
            <div class="tit">강좌범위</div>
            <p>한국어를 처음 배우는 학습자를 위한 기초 강좌입니다.</p>
        </div>

        <div class="bx">
            <div class="tit">강좌특징</div>
            <p>한국어를 공부할 때 반드시 알아야 할 기초 문법을 배울 수 있어요.</p>
            <p>한국 친구와 자유롭게 말할 수 있도록 일상생활 표현을 배워요.</p>
            <p>혼자 공부하기 어려운 문법을 쉽고 재미있게 배울 수 있어요.</p>
        </div>

        <div class="bx">
            <div class="tit">수강 대상</div>
            <p>한국어 문법을 기초부터 탄탄히 배우고 싶은 학습자</p>
        </div>
    </div>
    <div class="data02">
        <h6>강사소개</h6>

        <div class="teacher-bx">
            <div class="grp clearfix">
                <div class="f-l">
                    <img src="/theme/oklassedu/pix/images/sample_user02.jpg" alt="쩡 레 응우옌" />
                    <p><strong>쩡 레 응우옌</strong>선생님</p>
                </div>
                <div class="f-r">
                    <div class="bx">
                        <div class="tit">프로필</div>
                        <p>한국외국어대학교 교육대학원 외국어로서의 한국어교육 박사</p>
                        <p>밍밍대학교 교육대학원 한국어교육 석사</p>
                        <p>몽몽대학교 한국어학과 학사</p>
                        <p>現) 망망대학교 국제교육원 한국어 강의</p>
                        <p>前) 미국 Moongmoong University 한국어학과 파견 강의</p>
                        <p>저서 : Press of Vietnam) Basic Korean Grammar in use</p>
                    </div>
                    <div class="bx">
                        <div class="tit">제자들에게 한 마디</div>
                        <p>the class tailored to your pace. Let`s study Korean with accuracy, not haste.</p>
                        <p>여러분의 속도에 맞춘 한국어 수업! 천천히, 그리고 정확히 한국어를 공부합시다.</p>
                    </div>
                </div>

            </div>
            <div class="grp clearfix">
                <div class="f-l">
                    <img src="/theme/oklassedu/pix/images/sample_user01.jpg" alt="쩡 레 응우옌" />
                    <p><strong>쩡 레 응우옌</strong>선생님</p>
                </div>
                <div class="f-r">
                    <div class="bx">
                        <div class="tit">프로필</div>
                        <p>한국외국어대학교 교육대학원 외국어로서의 한국어교육 박사</p>
                        <p>밍밍대학교 교육대학원 한국어교육 석사</p>
                        <p>몽몽대학교 한국어학과 학사</p>
                        <p>現) 망망대학교 국제교육원 한국어 강의</p>
                        <p>前) 미국 Moongmoong University 한국어학과 파견 강의</p>
                        <p>저서 : Press of Vietnam) Basic Korean Grammar in use</p>
                    </div>
                    <div class="bx">
                        <div class="tit">제자들에게 한 마디</div>
                        <p>the class tailored to your pace. Let`s study Korean with accuracy, not haste.</p>
                        <p>여러분의 속도에 맞춘 한국어 수업! 천천히, 그리고 정확히 한국어를 공부합시다.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="data03">
        <h6>교재 소개</h6>
        <div class="book-bx">
            <div class="bk">
                <img src="/theme/oklassedu/pix/images/ex_book02.png" alt="안녕하세요!한국어[입문자용]" />
            </div>
            <div class="bk-info">
                <div class="tit">안녕하세요! 한국어 [입문자용]</div>
                <div class="t-gray"><span>도서출판 밍밍</span><span>김한글 외 4명</span></div>
                <p><span>금액</span><strong>10,000 VND</strong></p>

                <div class="txt">
                    서울대학교 대학원 외국어로서의 한국어 학과에서 강의 중인 5명의 교수가 집필한, 총 24개의 주제로 구성된 이 교재는 영국에서 최초로 시작되어
                    일년에 한 바퀴 돌면서 공부하는 사람에게 행운을 주었고 지금은 당신에게 옮겨진 이 교재는 60일 안에 당신 곁을 떠나야 합니다.
                    이 교재를 포함해서 7권을 한국어 공부가 필요한 사람에게 보내 주셔야 합니다. 할부로 구매해도 좋습니다.
                    혹 미신이라 하실 지 모르지만 사실입니다. 영국에서 Henry Carpenter 라는 사람은 2013년에 이 교재를 받았습니다. 그는 비서에게 더 구매하여
                    보내라고 했습니다. 며칠 뒤에 복권에 당첨되어 200만 유로를 받았습니다...
                </div>
            </div>
        </div>
    </div>
    <div class="data04">
        <h6>강의목록</h6>
        <ul class="dt-crs-list">
            <li>
                <span class="tit">1강 어/아요, 았/었어요, ㅂ니다</span>
                <span class="tm">5분 14초</span>
            </li>
            <li>
                <span class="tit">2강 어/아요, 았/었어요, ㅂ니다</span>
                <span class="tm">5분 14초</span>
            </li>
            <li>
                <span class="tit">3강 어/아요, 았/었어요, ㅂ니다</span>
                <span class="tm">5분 14초</span>
            </li>
            <li>
                <span class="tit">4강 어/아요, 았/었어요, ㅂ니다4강 어/아요, 았/었어요, ㅂ니다4강 어/아요, 았/었어요, ㅂ니다4강 어/아요, 았/었어요, ㅂ니다4강 어/아요, 았/었어요, ㅂ니다4강 어/아요, 았/었어요, ㅂ니다</span>
                <span class="tm">5분 14초</span>
            </li>
            <li>
                <span class="tit">5강 어/아요, 았/었어요, ㅂ니다</span>
                <span class="tm">5분 14초</span>
            </li>
            <li>
                <span class="tit">6강 어/아요, 았/었어요, ㅂ니다</span>
                <span class="tm">5분 14초</span>
            </li>

        </ul>
    </div>
    <div class="data05">
        <h6 class="has-star">
            수강후기
            <div class="f-r">
                <div class="star-event big" data-num="4"></div>
                <span>3점</span>
                <span class="t-gray">(65개)</span>
            </div>
        </h6>

        <ul class="review-list">
            <li>
                <div class="tp">
                    <strong>홍길동</strong>
                    <span class="tm">15분 전</span>
                    <div class="star-event" data-num="4"></div>
                </div>
                <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
            </li>
            <li>
                <div class="tp">
                    <strong>홍길동</strong>
                    <span class="tm">15분 전</span>
                    <div class="star-event" data-num="3"></div>
                </div>
                <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
            </li>
            <li>
                <div class="tp">
                    <strong>홍길동</strong>
                    <span class="tm">15분 전</span>
                    <div class="star-event" data-num="2"></div>
                </div>
                <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
            </li>
            <li>
                <div class="tp">
                    <strong>홍길동</strong>
                    <span class="tm">15분 전</span>
                    <div class="star-event" data-num="3"></div>
                </div>
                <div class="txt">국어 입문자에게 아주 좋은 강좌입니다. 추천!</div>
            </li>
        </ul>

        <!-- 수강후기 없는경우 
       <div class="no-data">
               <div>등록된 수강후기가 없습니다.<br/>수강을 완료하고 첫 번째로 후기를 등록해보세요!</div>
       </div>
        -->

    </div>
</div>

<h5 class="pg-tit">이 강좌의 관련 강좌</h5>
<div class="crs-bx">
    <ul class="crs-list">
        <li>
            <div class="img">
                <span class="ic-heart on">좋아요</span>
                <img src="/theme/oklassedu/pix/images//ex_list_img01.png" alt="TOPIK 4급 취득 실전 모의고사 – 한국어ver." />
            </div>
            <div class="txt">
                <div>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</div>
                <p><span>쩡 레 응우옌 선생님</span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns blue">수강신청</a>
            </div>

        </li>
        <li>
            <div class="img">
                <span class="ic-heart">좋아요</span>
                <img src="/theme/oklassedu/pix/images//ex_list_img01.png" alt="TOPIK 4급 취득 실전 모의고사 – 한국어ver." />
            </div>
            <div class="txt">
                <div>TOPIK 4급 취득 실전 모의고사 – 한국어ver.</div>
                <p><span>쩡 레 응우옌 선생님</span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 구매일로부터 60일</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns gray">수강중</a>
            </div>
        </li>
    </ul>
</div>



<h5 class="pg-tit">이 강좌의 관련 교재</h5>
<div class="crs-bx">
    <ul class="crs-list book">
        <li>
            <div class="img">
                <img src="/theme/oklassedu/pix/images//ex_book.png" alt="안녕하세요! 한국어 [TOPIK 4급] - 듣기편" />
            </div>
            <div class="txt">
                <div>안녕하세요! 한국어 [TOPIK 4급] - 듣기편</div>
                <p><span>도서출판 밍밍</span><span>김한글 외 4명</span></p>
                <p class="f16">정가 12,000 VND</p>
            </div>
            <div class="bt-area">
                <a href="#" class="btns br">자세히보기</a>
            </div>

        </li>
    </ul>
</div>
<script type="text/javascript">
    $(function () {
        //배송정보입력 팝업 
        $(".sel-crs-list .bt").click(function () {
            utils.popup.call_layerpop("./팝업_배송정보입력.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"param": "11"});
            return false;
        })
    });
</script>
<?php
echo $OUTPUT->footer();
?>


