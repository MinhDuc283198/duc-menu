<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('내정보');

echo $OUTPUT->header();
?>  
<div class="my-box imprt half">
    <div class="rw">
        <strong class="t-black">김비상</strong>
        <p>
            <span class="t-gray">가입일</span>
            <span class="t-blue02">2018.07.23</span>
            <a href="#" class="btns br">개인정보수정</a>
        </p>
    </div>
    <div class="rw">
        <strong>아이디</strong>
        <p>
            <img src="/theme/oklassedu/pix/images/icon_sns.png" alt="페이스북" />
            <span>visang@facebook.com</span>
        </p>
    </div>
    <div class="rw">
        <strong>이메일</strong>
        <p><strong>visang@gmail.com </strong></p>
    </div>
    <div class="rw">
        <strong>휴대폰 번호</strong>
        <p><strong>010-0408-0506</strong></p>
    </div>

    <div class="my-crs-list">
        <div class="bg-tit">내 강좌 내역</div>
        <div class="num">
            <ul>
                <li>
                    <strong>수강중인 강좌</strong>
                    <span class="t-blue02"><em>3</em>개</span>
                </li>
                <li>
                    <strong>내가 구매한 강좌</strong>
                    <span class="t-blue02"><em>10</em>개</span>
                </li>
                <li>
                    <strong>학습 완료 강좌</strong>
                    <span class="t-blue02"><em>37</em>개</span>
                </li>
                <li>
                    <strong>수료한 강좌</strong>
                    <span class="t-blue02"><em>33</em>개</span>
                </li>
            </ul>
        </div>
    </div>

</div>

<h4 class="pg-tit">수강중인 강좌 <a href="#" class="mr_all">전체보기</a></h4>
<ul class="thumb-list style04 course slider">
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(100%)</span>
                <p class="bar-event" data-num="100%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(100%)</span>
                <p class="bar-event" data-num="100%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
    <li>
        <div class="wp">
            <div class="img">
                <img src="/theme/oklassedu/pix/images/ex_img02.png" alt="TOPIKⅡ(5,6급) - 문법편" />
            </div>
            <div class="txt">
                <div class="tit">TOPIKⅡ(5,6급) - 문법편</div>
                <p><span>쩡 레 응우옌 선생님 </span><span>총 25강</span></p>
                <p class="t-gray">수강기간: 19.09.01~19.10.30</p>
            </div>
            <div class="bar-area">
                <span>진도율(30%)</span>
                <p class="bar-event" data-num="30%"><span></span></p>
            </div>
        </div>
    </li>
</ul>


<h4 class="pg-tit">1:1 상담 내역<a href="#" class="mr_all">전체보기</a></h4>
<table class="table m-block mg-bt50">
    <thead>
        <tr>
            <th width="10%">번호</th>
            <th>제목</th>
            <th width="15%">작성자</th>
            <th width="20%">작성일</th>
            <th width="10%">조회수</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="m-hide">3</td>
            <td class="text-left overflow"><a href="#">동영상 재생 오류가 납니다. 하라는 대로 다 해봤는데도 안 돼요동영상 재생 오류가 납니다. 하라는 대로 다 해봤는데도 안 돼요</a></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
        <tr>
            <td class="m-hide">2</td>
            <td class="text-left overflow"><a href="#">결제했는데 강의실 입장이 안 됩니다.</a></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
        <tr>
            <td class="m-hide">1</td>
            <td class="text-left overflow"><a href="#"><span class="tb-reply">re</span>결제했는데 강의실 입장이 안 됩니다.</a></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
    </tbody>
</table>

<h4 class="pg-tit">결제내역<a href="#" class="mr_all">전체보기</a></h4>
<table class="table m-block">
    <thead>
        <tr>
            <th width="5%">번호</th>
            <th width="7%">품목</th>
            <th>결제상품명</th>
            <th width="11%">결제금액</th>
            <th width="10%">결제일</th>
            <th width="10%">결제수단</th>
            <th width="8%">배송상태</th>
            <th width="60px">환불하기</th>
            <th width="60px">영수증</th>
        </tr>
    </thead>
    <tbody>
        <!-- 데이터 없는경우 
        <tr><td colspan="9">결제 내역이 없습니다.</td></tr> -->
        <tr>

            <td class="m-hide">7</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">배송중</td>
            <td class="m-col3"><a href="#" class="btns br_blue">환불</a></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>

        </tr>
        <tr>
            <td class="m-hide">9</td>
            <td class="m-hide">강좌</td>
            <td class="text-left">TOPIKⅡ(5,6급) – 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">-</td>
            <td class="m-col3"><a href="#" class="btns br_blue">환불</a></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
        <tr>
            <td class="m-hide">8</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>무통장입금</td>
            <td class="m-col3 m-clear">-</td>
            <td class="m-col3"><span class="t-black">승인대기</span></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
        <tr>
            <td class="m-hide">7</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">배송완료</td>
            <td class="m-col3"><span class="t-red">환불불가</span></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
        <tr>
            <td class="m-hide">6</td>
            <td class="m-hide">교재</td>
            <td class="text-left">한방에 합격하는 TOPIK II (5, 6급) - 문법편</td>
            <td>12,000 VND</td>
            <td class="m-f-r">2019.09.05</td>
            <td>신용카드</td>
            <td class="m-col3 m-clear">구매취소</td>
            <td class="m-col3"><span class="t-blue02">환불완료</span></td>
            <td class="m-col3"><a href="#" class="btns br">인쇄</a></td>
        </tr>
    </tbody>
</table>
<?php
echo $OUTPUT->footer();
?>


