<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('강좌목록');

echo $OUTPUT->header();
?>  
<h2 class="pg-tit">개인정보수정</h2>

<div class="my-box imprt">
    <div class="sub-tit">필수정보</div>

    <div class="rw">
        <strong>아이디</strong>
        <p>
            <img src="/theme/oklassedu/pix/images/icon_mail.png" width="24" alt="" />
            <span>visang@facebook.com</span>
        </p>
    </div>
    <div class="rw">
        <strong>이름</strong>
        <p><strong>김비상</strong></p>
    </div>

    <div class="rw">
        <strong>비밀번호</strong>
        <p>
            <a href="#" class="btns br openPop">비밀번호 변경</a>
            <!--<span class="t-point">로그인한 SNS에서 비밀번호 변경 가능합니다.</span>-->
        </p>
    </div>
    <div class="rw">
        <strong>생년월일</strong>
        <p class="birth">
            <select>
                <option value="1994">1994</option>
            </select>
            <select>
                <option value="03">03</option>
            </select>
            <select>
                <option value="31">31</option>
            </select>
        </p>
    </div>
    <div class="rw">
        <strong>이메일</strong>
        <p class="e-mail">
            visang@gmail.com
        </p>
    </div>
    <div class="rw">
        <strong>휴대폰 번호</strong>
        <p class="phone">
            <input type="text" value="010" />
            <input type="text" value="2222" />
            <input type="text" value="0408" />
        </p>
    </div>
</div>

<h5 class="bx-tit">부가정보 - 주소<span>강좌 구매 및 교재 배송을 위한 정보입니다.</span></h5>
<div class="my-box">
    <div class="rw">
        <strong>주소</strong>
        <p class="addr">
            <input type="text" class="w100" value="Vinhome central park . Park 6A" />
            <input type="text" class="w100" value="208 nguyễn hữu cảnh phường 22 quận bình thạnh.tp HCM" />
            <span class="txt">입력한 주소로 교재가 배송됩니다.</span>
        </p>
    </div>
</div>

<div class="btn-area text-center">
    <input type="button" value="저장" class="btns point big02" />
</div>
<script type="text/javascript">
    $(function () {
        //비밀번호 변경 팝업 이벤트
        $(".openPop").click(function () {
            utils.popup.call_layerpop("./팝업_비밀번호변경.php", {"width": "400px", "height": "auto", "callbackFn": function () {}}, {"param": "11"});
            return false;
        })
    });
</script>
<?php
echo $OUTPUT->footer();
?>


