<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add('1:1상담내역');

echo $OUTPUT->header();
?>  
<h2 class="pg-tit">1:1상담내역</h2>

<div class="tp-tb-area text-right">
    <a href="#" class="btns br_blue h40">글 작성</a>
</div>

<table class="table m-block">
    <thead>
        <tr>
            <th width="10%">번호</th>
            <th>제목</th>
            <th width="15%">작성자</th>
            <th width="20%">작성일</th>
            <th width="10%">조회수</th>
        </tr>
    </thead>
    <tbody>
        <tr class="bg">
            <td class="m-hide">공지</td>
            <td class="text-left overflow hasnew"><a href="#">공지입니다.</a><span class="ic-new">N</span></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
        <tr>
            <td class="m-hide">3</td>
            <td class="text-left overflow hasnew"><a href="#">동영상 재생 오류가 납니다. 하라는 대로 다 해봤는데도 안 돼요동영상 재생 오류가 납니다. 하라는 대로 다 해봤는데도 안 돼요</a><span class="ic-new">N</span></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
        <tr>
            <td class="m-hide">2</td>
            <td class="text-left overflow"><a href="#">결제했는데 강의실 입장이 안 됩니다.</a></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
        <tr>
            <td class="m-hide">1</td>
            <td class="text-left overflow"><a href="#"><span class="tb-reply">re</span>결제했는데 강의실 입장이 안 됩니다.</a></td>
            <td>김비상</td>
            <td>2019.10.05</td>
            <td class="m-f-r"><span class="m-show inline">조회수</span>1</td>
        </tr>
    </tbody>
</table>

<ul class="mk-paging">
    <li class="first"><a href="#">first</a></li>
    <li class="prev"><a href="#">prev</a></li>
    <li class="on"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">6</a></li>
    <li><a href="#">7</a></li>
    <li><a href="#">8</a></li>
    <li><a href="#">9</a></li>
    <li><a href="#">10</a></li>
    <li class="next"><a href="#">next</a></li>
    <li class="end"><a href="#">end</a></li>
</ul>
<?php
echo $OUTPUT->footer();
?>


