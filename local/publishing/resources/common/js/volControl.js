var volProgressWidth=0;
var isVolDragMc;
var isVolProgressBar;
var isVolDragMcHWidth;
var isVolDragPercent=0;
var isOldVolDragPercent=0;
var goVolPercent=0;

var isVolDragingFlag=false;
var _dragVol_Rect;

var isCookVol=1;
var volStr="myVolPct"

	
function _doVolumeInit(){
	/*볼륨제어*/
	if(isExp && isIEVersion<10){
		isMediaObj.muted = false;
	}

	$("#muteBtn").unbind("click");	//##노하우에서 팝업영상 2곳 재생시 2번 호출되 이벤트 제거코드 추가##
	$("#muteBtn").bind("click",function(){
		//alert(isMediaObj.muted)
		
		isMediaObj.muted=!isMediaObj.muted;
		if(isMediaObj.muted){
			$("#muteBtn").addClass("mute");
			isOldVolDragPercent = isVolDragPercent;
			isVolDragPercent=0;
			_Vol_Seek(0);
		}else{
			$("#muteBtn").removeClass("mute");
			
			isVolDragPercent = isOldVolDragPercent;
			if(isVolDragPercent<0.1){
				isVolDragPercent=0.5;
			}
			_Vol_Seek(isVolDragPercent);
		}
		
	});
	
	volProgressWidth=0;
	isVolDragPercent=0;
	isOldVolDragPercent=0;
	goVolPercent=0;

	isVolDragingFlag=false;
	
	isCookVol=1;

	isVolDragMc  = $("#volJogbtn");
	isVolProgressBar = $("#volJogProgress");
	volProgressWidth = $("#volJogBar")[0].clientWidth;
	isVolDragMcHWidth = isVolDragMc[0].clientWidth/2;
	_dragVol_Rect = new Object();

	isVolDragPercent=0;
	goVolPercent=0;
	isVolDragingFlag=false;
	
	
	
	if(isExp && isIEVersion<10){
		if(isVolDragMc[0]){
			isVolDragMc[0].mousedown = _Vol_SeekProgress;
			
		}
		if($('#volJogBar')[0]){
			$('#volJogBar')[0].onclick = _Vol_SeekProgress;
		}	
	}else{
		if(isVolDragMc[0]){
			isVolDragMc[0].addEventListener("mousedown",_Vol_DragStart);
		}
		if($('#volJogBar')[0]){
			$('#volJogBar')[0].addEventListener("click",_Vol_SeekProgress);
		}	
	}
	
	
	
	var _volC = getCookie(volStr).split("/");
	isVolDragPercent = _volC[0];
	isOldVolDragPercent = _volC[1];	//update...
	

	if(isVolDragPercent==null || isVolDragPercent==""){
		
		isVolDragPercent=0.5;	//1에서 0.5로 기본값 변경
		
	}
	isVolDragPercent = Number(isVolDragPercent);
	
	if(isVolDragPercent<=0.01){
		isMediaObj.muted = true;
		$("#muteBtn").addClass("mute");
		isVolDragPercent=0;
	}
	//alert(isVolDragPercent)
	_Vol_Seek(isVolDragPercent)	

	
	function _Vol_DragStart(e){
		var e = e || window.event;
		isVolDragingFlag=true;
		if(isExp && isIEVersion<10){
			document.onmousemove = _Vol_mouseMove;
			document.mouseup = _Vol_mouseMoveStop;
		}else{
			window.addEventListener("mousemove",_Vol_mouseMove);
			window.addEventListener("mouseup",_Vol_mouseMoveStop);
		}
	}
	
	function _Vol_mouseMove(e){
		var e = e || window.event;

		var nPos = e.clientX;
		
		var tmpL = $("#container")[0].offsetLeft + $('#volJogBar')[0].offsetLeft;
		volProgressWidth = $("#volJogBar")[0].clientWidth;
		var isMLeft =  tmpL;
		var isMRight = isMLeft+volProgressWidth-isVolDragMcHWidth;
		var isMWidth =volProgressWidth-isVolDragMcHWidth;


		var nPosX = nPos;
		if(nPos<isMLeft){
			nPosX = nPos;
		}
		if(nPos>isMRight){
			nPosX = isMRight;
		}
		nPosX = nPosX * nScale;
		isVolDragPercent = (nPosX - isMLeft) / isMWidth;
		if(isVolDragPercent<0.05){
			isVolDragPercent=0;
		}
		if(isVolDragPercent>1){
			isVolDragPercent=1;
		}
		
		_Vol_Seek(isVolDragPercent);
		isOldVolDragPercent = isVolDragPercent;
		
	}
	
	function _Vol_mouseMoveStop(e){
		var e = e || window.event;
		isVolDragingFlag=false;
		if(isExp && isIEVersion<10){
			document.onmousemove = null;
			document.onmouseup = null;
		}else{
			window.removeEventListener("mousemove",_Vol_mouseMove);
			window.removeEventListener("mouseup",_Vol_mouseMoveStop);
		}
		if(isVolDragPercent<0.05){
			isVolDragPercent=0;
		}
		//alert(isVolDragPercent);
		_Vol_Seek(isVolDragPercent);
		isOldVolDragPercent = isVolDragPercent;

	}
	
	function _Vol_SeekProgress(e){
		var e = e || window.event;
		//alert("_doSeekProgress")
		var nPos = e.clientX;
		
		var tmpL = $("#container")[0].offsetLeft + $('#volJogBar')[0].offsetLeft;
		//var isMLeft =  $('#footerSet')[0].offsetLeft + $('#volJogBar')[0].offsetLeft;
		var isMLeft =  tmpL;
		volProgressWidth = $("#volJogBar")[0].clientWidth;
		var isMRight = isMLeft+volProgressWidth-isVolDragMcHWidth;
		var isMWidth =volProgressWidth-isVolDragMcHWidth;


		var nPosX = nPos;
		if(nPos<isMLeft){
			nPosX = nPos;
		}
		if(nPos>isMRight){
			nPosX = isMRight;
		}
		nPosX = nPosX * nScale;
		goPercent = (nPosX - isMLeft) / isMWidth;
		if(goPercent<0.05){
			goPercent=0;
		}
		if(goPercent>1){
			goPercent=1;
		}

		if(goPercent<0.01){goPercent=0;}
		if(goPercent>0.99){goPercent=0.99;}
		
		isVolDragPercent = goPercent;
		
		//isVolProgressBar.css("width",(volProgressWidth*goPercent+isVolDragMcHWidth)+"px");
		//isVolDragMc.css("left",((volProgressWidth)*goPercent-isVolDragMcHWidth)+"px");

		_Vol_Seek(goPercent)
		isOldVolDragPercent = isVolDragPercent;
	}
	if(isExp && isIEVersion<10){
		//document.mousemove = null;
		//document.mouseup = null;
	}else{
		$( "#controller").mouseleave(function(){
			window.removeEventListener("mousemove",_Vol_mouseMove);
			window.removeEventListener("mouseup",_Vol_mouseMoveStop);
			isVolDragingFlag=false;
		})
	}
	
	
}



function _Vol_Seek(st){
	st = Number(st);
	volProgressWidth = $("#volJogBar")[0].clientWidth;
	isVolProgressBar.css("width",(volProgressWidth*isVolDragPercent+isVolDragMcHWidth)+"px");
	isVolDragMc.css("left",((volProgressWidth)*isVolDragPercent-isVolDragMcHWidth)+"px");
	
	try{
		//console.log(document.location+" : "+volProgressWidth+" // "+st);	
	}catch(e){}
	
	//top.status = st = st;
	if(isExp && isIEVersion<10){
		if(isExp && isIEVersion==8){
			doControlCall("volume",st*1);
		}else{
			doControlCall("volume",st*1);	
		}

	}else{
		isMediaObj.volume = st;
	}
	setCookie(volStr,st + "/" + isOldVolDragPercent,360);

	if(isVolDragPercent>0){
		isMediaObj.muted = false;
		$("#muteBtn").removeClass("mute");
	}else{
		isMediaObj.muted = true;
		if(!$("#muteBtn").hasClass("mute")){
			$("#muteBtn").addClass("mute");	
		}

	}


	if($("#popClosing").length){
	}

}