// JavaScript Document
var pre_isMediaPlayFlag = false;	//재생여부값 기억용

function _doNaviInit(){
	var naviLeft=0;
	var naviTop=0;
	var naviWidth=0;
	var isNaviOpenFlag=false;
	
	naviLeft  = parseInt($("#navi").css("left").split(".")[0],10);
	naviWidth = parseInt($("#navi").css("width").split(".")[0],10)+1;
	//isNaviOpenFlag=true;
	if(!isNaviOpenFlag){
		$("#lnb").hide();
		$("#navi").addClass("ready");
	}
	
	$("#lnbBtn").bind("click",function(e){
		if(!isNaviOpenFlag){
			pre_isMediaPlayFlag = isMediaPlayFlag;	//재생중 여부 기억(가장 먼저)

			$("#navi").removeClass("ready");
			$("#lnb").fadeIn();
			

			

			if(isMediaEndFlag){
				
			}else{
				if(isExp && isIEVersion<10){
					doControlCall("pause");
				}else{
					isMediaObj.pause();	
				}
			}

			

			isMediaPlayFlag=false;
			 new TweenMax($("#lnb"), 0.5, {left:naviLeft-1,onComplete:function(){
				 $("#lnbBtn").addClass("off");
			 },ease: Power1.easeOut});
		}else{
			$("#lnb").fadeOut();
			if(isMediaEndFlag){
				
			}else{
				if(isExp && isIEVersion<10){
					//재생중 이었을때만 play
					if (pre_isMediaPlayFlag)
					{
						doControlCall("play");
						isMediaPlayFlag=true;
					}
				}else{
					//재생중 이었을때만 play
					if (pre_isMediaPlayFlag)
					{
						isMediaObj.play();
						isMediaPlayFlag=true;
					}
				}
			}
			 new TweenMax($("#lnb"), 0.3, {left:-naviWidth,onComplete:function(){
				$("#lnbBtn").removeClass("off");
				$("#navi").addClass("ready");
			 },ease: Power1.easeIn});
		}
		isNaviOpenFlag= !isNaviOpenFlag;
	})
	
	$("#indexCloseBar").bind("click",function(){
		$("#lnb").fadeOut();
		if(isMediaEndFlag){
				
		}else{
			if(isExp && isIEVersion<10){
				//재생중 이었을때만 play
				if (pre_isMediaPlayFlag)
				{
					doControlCall("play");
					isMediaPlayFlag=true;
				}
			}else{
				//재생중 이었을때만 play
				if (pre_isMediaPlayFlag)
				{
					isMediaObj.play();	
					isMediaPlayFlag=true;
				}
			}
			
		}
		 new TweenMax($("#lnb"), 0.3, {left:-naviWidth,onComplete:function(){
			$("#lnbBtn").removeClass("off");
			$("#navi").addClass("ready");
		 },ease: Power1.easeIn});
		isNaviOpenFlag=false;
	});
	
	
	$(".tMenu").each(function(idx){
		var tmp = this.id.split("_")[1];
		if(tmp == isCurrentT){
			
			$(this).addClass("active");
		}else{
			$(this).removeClass("active");
		}
	});
	$(".tMenu").bind("click",function(){
		var tmp = this.id.split("_")[1];
		_gotoSubjectMove(tmp);
	})
	
	$(".sMenu").each(function(idx){
		var tmp = this.id.split("_")[1];
		var tmp2 = this.id.split("_")[2];
		if(tmp == isCurrentT && tmp2 == isCurrentS){
			$(this).addClass("active");
			$(this).children(".dot").attr("src","../common/img/index/dot_Active.png");
		}else{
			$(this).removeClass("active");
		}
	})
	$(".sMenu").bind("click",function(){
		var tmp = this.id.split("_")[1];
		var tmp2 = this.id.split("_")[2];
		_gotoSubjectMove(tmp,tmp2);
	})
	
}