//alert('2 : common');
// JavaScript Document
var isNarShowFlag = false;
var isNarPosX;




var captureMode = 0;	//0 캡쳐X, 1 캡쳐 O <-----------------------------------





var isNextMovieShowFlag=false;

var isChasiImgURL /*= "../common/img/main/chasi_"+itostr(thisChapNum)+".png"*/



$(document).ready(function(e) {
	setTop(); //과정명차시명
	setIndex(); //index
	setFooter(); //footer

	if (captureMode == 1)
	{
		$(".chapDiv").hide();
		$("#course").hide();
		$("#navi").hide();
	}
	
	$("body").show();
	$("#container").css("opacity",'0');
	$("#mobileLoading").show();
	
	_doMobileSizeChk();
	_doModuleChk();
	_doNaviInit();

	try{
		_doMovieInit();
	}catch(e){}

	try{
		_pageInit();
	}catch(e){}
		
	_loadFrameAct();
	_do_IE8Chk();
	_do_IE7Chk();
	
	$(document).bind("contextmenu", function(e){
        return false;
    });
	
	
	$("#dicBtn").hover(function() {
	   $("#dicBtn_over").css("display", "block");
	}, function(){
		$("#dicBtn_over").css("display", "none");
	});
	
	$("#bookBtn").hover(function() {
	   $("#bookBtn_over").css("display", "block");
	}, function(){
		$("#bookBtn_over").css("display", "none");
	});
	
	$("#jogbar").hover(function() {
	   $("#jogbar_over").css("display", "block");
	}, function(){
		$("#jogbar_over").css("display", "none");
	});
	
	$("#goPage").hover(function() {
	   $("#goPage_over").css("display", "block");
	}, function(){
		$("#goPage_over").css("display", "none");
	});
	
	$("#noteBtn").hover(function() {
	   $("#noteBtn_over").css("display", "block");
	}, function(){
		$("#noteBtn_over").css("display", "none");
	});
	
	$("#downBtn").hover(function() {
	   $("#downBtn_over").css("display", "block");
	}, function(){
		$("#downBtn_over").css("display", "none");
	});
	
	$("#playBtn").hover(function() {
		$("#playBtn_over").css("display", "block");
	}, function(){
		$("#playBtn_over").css("display", "none");
	});

	$("#stopBtn").hover(function() {
	   $("#stopBtn_over").css("display", "block");
	}, function(){
		$("#stopBtn_over").css("display", "none");
	});
	
	$("#replayBtn").hover(function() {
	   $("#replayBtn_over").css("display", "block");
	}, function(){
		$("#replayBtn_over").css("display", "none");
	});
	
	$("#scriptBtn").hover(function() {
	   $("#scriptBtn_over").css("display", "block");
	}, function(){
		$("#scriptBtn_over").css("display", "none");
	});
	
	$("#fullBtnMy").hover(function() {
		$("#fullBtnMy_over").css("display", "block");
		if(isfullMode){
			$("#fullBtnMy_over").attr("src", "../common/img/mal/full_re.png");
		}else{
			$("#fullBtnMy_over").attr("src", "../common/img/mal/full.png");
		}
	}, function(){
		$("#fullBtnMy_over").css("display", "none");
	});
	
	$("#muteBtn").hover(function() {
	   $("#muteBtn_over").css("display", "block");
	   if(isMediaObj.muted)
		{
		   $("#muteBtn_over").attr("src", "../common/img/main/mute_overmal.png");
		}else
		{
		   $("#muteBtn_over").attr("src", "../common/img/main/volume_overmal.png");
		}
	}, function(){
		$("#muteBtn_over").css("display", "none");
	});
	
	$("#prevBtn").hover(function() {
	   $("#prevBtn_over").css("display", "block");
	}, function(){
		$("#prevBtn_over").css("display", "none");
	});
	

	$("#nextBtn").hover(function() {
	   $("#nextBtn_over").css("display", "block");
	}, function(){
		$("#nextBtn_over").css("display", "none");	
	});
})

$(window).load(function(e){
	setTimeout(function(){
        //resizeScale(); 
		$("#container").css("opacity",'1');
		$("#mobileLoading").hide();
		//return;
		
		setTimeout(function(){
			if(loadMediaType=="mp3"){
				if(isExp && isIEVersion<10){
						doControlCall("play");
						isMediaPlayFlag=true;
				}else{
					isMediaObj.play();
					isMediaPlayFlag=true;
				}	
			}	
		},500);	
		
	},1500)
	
});

function _doMobileDesign(){
	$("#container").addClass("isMobile");
	$("#footer").addClass("isMobile");
	$("#footer").find("div").addClass("isMobile");
	$("#footer").find("button").addClass("isMobile");
	$("#navi").addClass("isMobile");
	$("#naviClose").addClass("isMobile");
	
	$("#lnb").find("ul").find(".depth1").addClass("isMobile");
}


function  _do_IE8Chk(){
	if(isExp && isIEVersion==8){
		$("body").addClass("ie8");
		$(".chapDiv").addClass("ie8");
		$("#narration").addClass("ie8");
		$("#currentNum").addClass("ie8");
		$("#totalNum").addClass("ie8");
		$("#time").addClass("ie8");
		$("#lnb").addClass("ie8");
		$("#popClosing2").addClass("ie8");
		
		$("#lnb").find("ul").find("li").find("ul").addClass("ie8");
		$("#lnb").find("ul").find("li").find("ul").find("li").find("button").addClass("ie8");
		$("#lnb").find("ul").find("li").find("ul").find("li").find("button").addClass("ie8");
	}
	
	var agent = navigator.userAgent.toLowerCase();
	if (!isMobile && agent.indexOf("chrome") != -1) {
		$("button").css("outline",0);
	}
}

function  _do_IE7Chk(){
	if(isExp && isIEVersion==7){
		$("#lnb").addClass("ie7");
		$("#logo").addClass("ie7");
		$("#popClosing2").addClass("ie7");
		$(".index_line").addClass("ie7");

		$("#lnb").find("ul").find("li").find("ul").find("li").find("button").addClass("ie7");
		$("#lnb").find("ul").find("li").find("ul").find("li").find("button").addClass("ie7");
	}
	
	var agent = navigator.userAgent.toLowerCase();
	if (!isMobile && agent.indexOf("chrome") != -1) {
		$("button").css("outline",0);
	}
}



function _pageInit(){
	$("#currentNum").html(itostr(thisPageNum));
	$("#totalNum").html(itostr(totalPageNum));
	$("#container").addClass(bgClassName);
	_doXmlLoad();
	
	/*나레이션*/
	if($("#narration").length){
		isNarPosX = Number($("#narration").css("top").split("px")[0]);
		$("#narration").hide();
		$("#narration").css("top",(isNarPosX+44)+"px");
	}
	$("#nar_close").bind("click",function(){
		_showNar();
	});
	
	
	$("#nextBtn").bind("click",_doNext);
	$("#mNextBtn").bind("click",_doNext);
	$("#nextBubble").bind("click",_doNext);
	
	$("#prevBtn").bind("click",_doPrev);
	$("#mPrevBtn").bind("click",_doPrev);
	
	if(thisPageNum == 1){
		$("#mPrevBtn").hide();
		$("#btn_skip").fadeIn();
	}else
	{
		//차시명이미지
		$("#chapName").attr("src",isChasiImgURL);
		$("#chapName").css("position","absolute");
		$("#chapName").css("top","10px");
		$("#chapName").css("right","10px");
	}
	if(thisPageNum == totalPageNum){
		$("#mNextBtn").hide();
	}
	

	/*1페이지 스킵버튼*/
	var isBSkipFlag=false;
	if($("#btn_skip").length){
		var bSkipWidth = $("#btn_skip")[0].clientWidth;
		var bSkipHeight = $("#btn_skip")[0].clientHeight;
		isBSkipFlag=true;
		$("#btn_skip").bind("click",function(){
			$("#btn_skip").hide();
			isBSkipFlag=false;
			isMediaObj.play();
			isMediaObj.currentTime = isSkipTime;

		})
	}
	
	/***학습지원도구**/
	
	$('#downBtn').on('click', function() {
		otherEvent("down");
	});

	$('#dicBtn').on('click', function() {
		otherEvent("dic");
	});

	$('#noteBtn').on('click', function() {
		otherEvent("note");
	});
	
	$('#bookBtn').on('click', function() {
		otherEvent("book");
		console.log('asdasdasd');
	});

//Gopage부분
	$("#go_page").click(function(){
		goPNumPage();
//		alert('준비중 입니다.');
	});
	/////////////////////////////////////////////////
	// 차시명 : 로마자 대응 코드 추가
	/////////////////////////////////////////////////
	var tmpM = moduleName;
	var tmpA = "<span class='chap_ex2'>";
	var tmpB = "</span>";
	var tmpC = "<span class='chap_ex1'>";
	var tmpD = "<span class='chap_ex3'>";

	var tmpS = ["Ⅰ", "Ⅱ", "Ⅲ", "Ⅳ", "Ⅴ", "Ⅵ", "Ⅶ", "Ⅷ", "Ⅸ", "Ⅹ"];
	var tmpAct = false;
	for (k = 0; k<tmpS.length; k++ )
	{
		var _s = moduleName.indexOf(tmpS[k]);
		if (_s > -1)
		{
			tmpAct = true;
			tmpM = tmpC+moduleName.substring(0, _s) +tmpB +tmpA+ moduleName.substring(_s, _s+1) +tmpB+ tmpD +moduleName.substring(_s+1, moduleName.length) + tmpB;
			$("#chapName").html(tmpM);
		}
	}

	if (tmpAct == false)
	{
		$("#chapName").html(moduleName);
	}
	/////////////////////////////////////////////////

	var tmpS = moduleName.length * 20;
	//##//$(".chapDiv").css("width",tmpS+"px");
}


/***학습지원도구**/
function otherEvent(_str)
{
	switch (_str)
	{
		case "down" :
			alert("준비중 입니다.");
			/*
			nPopupWidth = 880;
			nPopupHeight = 600;
			strFPopupPath = "../swf/help.swf";
			var popup = window.open(strHPopupPath, "_help", "width=" + nPopupWidth + ", height=" + nPopupHeight, "toolbar=no");
			popup.document.title = "학습도우미";

			*/
			break;
		case "dic" :
            window.open("../common/search.html","인터넷_검색",'width=500,height=50,','_blank');
			break;
		case "note" :
			window.open("../common/thinkup/thinkup.html","메모장",'width=465,height=465','_blank');
			break;
			
		case "book" : 
			alert("준비중 입니다.");
			//window.open("../common/down/summary_"+itostr(thisChapNum)+".zip");
			break;
	};
};


function _loadFrameAct(){
	if(isNextMovieShowFlag)return;
	if(loadFrameURL){
		$("#htmlContents").html('<iframe id="loadActFrame" src="'+loadFrameURL+'" frameborder="0" scrolling="no"></iframe>');
		$("#loadActFrame").load(function(){
			$("#htmlContents").fadeIn();
		});	
	}
}

function _actitiveInit(){

	//오늘의 노하우 액티비티+영상 혼합될때 팝업영상 완료시 말풍선 뜨지 않아 아래코드 비활성 처리(#replayBtn 클릭에만 _actitiveInit() 쓰이고있음)
	//최초 페이지로딩시 page_info.js에서 isConNext값 설정됨 / 유지하도록

	//if(isConNextArray[thisPageNum]){
	//	isConNext = isConNextArray[thisPageNum];
	//}


	try{
		$("#loadActFrame")[0].contentWindow.ssInit();	
	}catch(e){
		//alert(e)
	}
	
}

function _doNext(){
	var gogoNum = thisPageNum + 1;
	thisNextPageNum = gogoNum;
	if(thisPageNum == totalPageNum){
		alert("마지막 페이지입니다.");
		return;
	}
	_gotoPageMove(gogoNum);
}
function _doPrev(){
	var gogoNum = thisPageNum - 1;
	thisNextPageNum = gogoNum + 1;
	if(thisPageNum == 1){
		alert("첫 페이지입니다.");
		return;
	}
	_gotoPageMove(gogoNum);
}

function _gotoSubjectMove(t,s){
	if(!s){
		s = 1;
	}
	var gogoNum=-1;
	for(i=1;i<pageInfoArray.length;i++){
		var t1 = pageInfoArray[i][0];
		var t2 = pageInfoArray[i][1];
		
		if(t == t1 && s == t2){
			gogoNum = i;
			break;
		}
	}
	if(gogoNum>0){
//		alert("gogoNum : " + gogoNum);
		thisNextPageNum = thisPageNum + 1;
		_gotoPageMove(gogoNum);
	}
}

function _gotoPageMove(n){
	//페이지 이동시 학습시간 저장

	if(parent.bolPorted){
		if(parent.bolTest)
		{
			var lastPage = parent.bookmarkNum;
		}else
		{
			var lastPage = parent.lastPage;
		}

		if(lastPage == ""){
			lastPage = thisPageNum;
		}
		if(!isMediaEndFlag)
		{
			if(bolTLock)
			{
				if(n > lastPage)
				{
						alert("학습이 완료되지 않았습니다.");
				}else
				{
					var gotoPage = pageInfoArray[n][2];
					document.location.href=gotoPage;
				}
	
			}else
			{
				if(n == Number(thisNextPageNum) || lastPage >= Number(n))
				{
					var gotoPage = pageInfoArray[n][2];
					document.location.href=gotoPage;
				}
			}
		}else
		{
			if(n == Number(thisNextPageNum) || lastPage >= Number(n))
			{
				var gotoPage = pageInfoArray[n][2];
				document.location.href=gotoPage;
			}
		}
		/*
		if(progressCheck(Number(n))){
			parent.nCurTotalTime += parent.nCurTime;
			var gotoPage = pageInfoArray[n][2];
			document.location.href=gotoPage;
		}else
		{
			alert("학습이 완료되지 않았습니다.");
		}
		*/
	}else
	{
		var gotoPage = pageInfoArray[n][2];
		document.location.href=gotoPage;
	}

}


function _showNar(){
	

	if(!isNarShowFlag){
		if($("#nextBubble").hasClass("last") || $("#nextBubble").hasClass("show")){
			//$("#nextBubble").hide();	
		}
		$("#narration").fadeIn(500);

		$("#nar_txt").scrollTop(0);	//스크롤 항상 top으로

		var tween = new TweenMax($("#narration"), 0.3, {top:isNarPosX, onComplete:function(){
			isNarShowFlag=true;
		}, ease: Power1.easeIn});
		var tween = new TweenMax($("#narration"), 0.3, {height:100, onComplete:function(){
		}, ease: Power1.easeIn});
		isNarShowFlag=true;
	}else{
		if($("#nextBubble").hasClass("last") || $("#nextBubble").hasClass("show")){
			//$("#nextBubble").show();	
		}
		var tween = new TweenMax($("#narration"), 0.3, {top:isNarPosX+100, onComplete:function(){
			$("#narration").hide();
			isNarShowFlag=false;
		}, ease: Power1.easeIn});
		var tween = new TweenMax($("#narration"), 0.3, {height:0, onComplete:function(){
		}, ease: Power1.easeIn});
		isNarShowFlag=false;
	}
	
}



function _doXmlLoad(){
	/*xml 로드 및 페이지 설정*/
	$.ajax({
		type: "GET" ,
		url: "xml/caption.xml",
		dataType: "xml",
		success: function(xml) {
			var sNum;
			var sNar;
			$(xml).find("caption").each(function(idx)
			{
				sNum = idx + 1;
				if(sNum == thisPageNum){
					sNar = $(this).text().split("\n").join("<br/>");
				}
			});
			
			
			
			$("#nar_txt").html(sNar);
				

		},
		error: function(err) {
			//alert(err);
		}
	});
}



function _doQuizLoad(){
	if(isMediaEndFlag){
		
	}else{
	if(isExp && isIEVersion<10){

			doControlCall("play");
			doControlCall("seek",isMediaObj.duration-0.1);
			isMediaPlayFlag=false;
		}else{
			isMediaObj.play();
			isMediaObj.currentTime=isMediaObj.duration-0.1;
			isMediaPlayFlag=false;
		}
	}
	setTimeout(function(){
		isStartWatchNum = -1;
		$("#mvEndContents").html('<iframe id="loadQuizFrame" src="quiz.html" frameborder="0" scrolling="no"></iframe>');
		$("#mvEndContents").fadeIn();
		$("#loadQuizFrame").load(function(){
			$("#mvEndContents").fadeIn();
		});		
	},600);
	
	
}

function _doQuizFrameHide(){
	$("#mvEndContents").html("");
	$("#mvEndContents").hide();

	//isConNext = true;	//말풍선 안뜨도록 초기화

	// ### 퀴즈 페이지 일때만 말풍선값 초기화 ###
	if (loadFrameURL == "quiz_init.html")
	{
		if(isConNextArray[thisPageNum]){
			isConNext = isConNextArray[thisPageNum];
		}
	}
}


function progressCheck(_page){ //최종 학습페이지보다 _page번호가 작을 경우 타임바제어 가능, 넥스트버튼 클릭 가능
	var lastPage = parent.lastPage;
	if(bolPLock)
	{
		/*
		var nTemp = Number(parent.doGetValue("cmi.progress_measure"));
		var nTempPage = parseInt(_page, 10);
		var nTempTotal = parseInt(totalPageNum, 10);
		var nTempProgress = Number((nTempPage/nTempTotal).toFixed(2));
		if(nTemp >= nTempProgress){
			return true;
		}
		else{
			return false;			
		}
		*/

		if(_page <= lastPage)
		{
			return true;  //제어가능
		}else
		{
			return false; //제어불가
		}
	}else{
		return true; //제어가능
	}
}


function goPNumPage(){
		//Gopage부분
	 var pNum = Number(document.getElementById("inPNum").value);
	 _gotoPageMove(pNum);
}



function progressSet(){
	if(parent.bolPorted){
		var nTemp = parseInt(thisPageNum,10);
		//alert("bolPLock : " + bolPLock);
		if(progressCheck(nTemp)){
			bolTLock = false; //트랙제어이동관련(false : 제어가능, true : 제어불가)
		}
	}
}


/**
 * startContent		시작 시 호출 함수
 */

function startContent()
{
	if(parent.bolPorted){
		if(parent.bolTest)
		{
			alert("strTemp : " + strTemp + "\n" + "nTempPage : " + nTempPage + "\n" + "nTempTotal : " + nTempTotal +"\n"+"\n"+"북마크페이지 : "+parent.bookmarkPage+"\n"+"bolTLock : " + bolTLock);
		}
	}else
	{
		//alert("parent.PageLocation("+strTemp+", "+nTempPage+", "+nTempTotal+")");
	}
	
}


//-----------------------------------------------------------------------------

/**
 *[getBrowser][브라우져 체크]
*/
function getBrowser(){
	var _strAgent = navigator.userAgent.toLowerCase();

	if(_strAgent.indexOf("msie") != -1){
		return "IE"
	}else{
		if(_strAgent.indexOf("trident") != -1){
			return "IE"
		}else{
			if(_strAgent.indexOf("chrome") != -1) return "Chrome";
			if(_strAgent.indexOf("opera") != -1) return "Opera";
			if(_strAgent.indexOf("firefox") != -1) return "Firefox";
			if(_strAgent.indexOf("safari") != -1) return "Safari";			
		}	
	}
}


window.addEventListener('resize',function(){
	var w_width = window.innerWidth;
	
	if(w_width < 1020){
		$("video").attr("controls",true);
		$("#paging").css("width",w_width);
		$("#prevBtn").hover(function(){
			$("#prevBtn_over").css("display","none");
		});
		$("#nextBtn").hover(function(){
			$("#nextBtn_over").css("display","none");
		});
	}else{
		$("video").removeAttr("controls");
		$("#paging").css("width","115px");
		$("#prevBtn").hover(function() {
		   $("#prevBtn_over").css("display", "block");
		}, function(){
			$("#prevBtn_over").css("display", "none");
		});

		$("#nextBtn").hover(function() {
		   $("#nextBtn_over").css("display", "block");
		}, function(){
			$("#nextBtn_over").css("display", "none");	
		});
	}
});

$(window).ready(function(){
	var w_width = window.innerWidth;
	if(w_width < 1020){
		$("video").attr("controls",true);
		$("#paging").css("width",w_width);
		$("#prevBtn").hover(function(){
			$("#prevBtn_over").css("display","none");
		});
		$("#nextBtn").hover(function(){
			$("#nextBtn_over").css("display","none");
		});
	}else{
		$("video").removeAttr("controls");
		$("#paging").css("width","115px");
		$("#prevBtn").hover(function() {
		   $("#prevBtn_over").css("display", "block");
		}, function(){
			$("#prevBtn_over").css("display", "none");
		});

		$("#nextBtn").hover(function() {
		   $("#nextBtn_over").css("display", "block");
		}, function(){
			$("#nextBtn_over").css("display", "none");	
		});
	}
});















