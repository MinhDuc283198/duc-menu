/*////////////////////////////////////////

for _ 환경부
st_jsj
ver_2018.07

////////////////////////////////////////*/


/*** 전차시 공통 ***/
//alert('0 : st_jindo');
var con_w = 1020;
var con_h = 592;

var startPage;
var startPageURL = "01.html";         //첫페이지
var bookmarkPage;                         //저장된 페이지
var bookmarkNum;                          //마지막에 학습한 페이지
var lastPage                                       //학습한페이지중 가장 높은 숫자의 페이지
var totalPage = pageInfoArray.length-1; //총페이지수


var nCurTotalTime = 0;
var nCurTime = 0;
var bolPorted = true;      //포팅 여부(포팅버전일때 true; 포팅 버전 아닐시 false;)
var bolTest = false; //포팅 테스트할때만 true; 포팅시 false;

/**
 * openContent 컨텐츠가 열렸을 경우 실행
 */
function openContent()
{

	var contentFrame=document.getElementById("contentFrame");

	if(bolPorted)
	{
		if(!bolTest)
		{
			loadPage(totalPage);
			var strTempStudent = doGetValue("cmi.learner_name");	//학습자
			bookmarkPage = doGetValue("cmi.location");			//Bookmark
			lastPage = doGetValue("cmi.suspend_data");
		}else
		{
			//포팅테스트 시 임의 북마크 페이지 설정
			bookmarkPage = "03.html";
		}


	

		if(bookmarkPage == "" || bookmarkPage == "undefined" || bookmarkPage == undefined || bookmarkPage == null)
		{
			 //학습 기록이 없을 경우 첫페이지 부터
			startPage = startPageURL;
	
		}else
		{

			if(confirm("이전에 학습하신 곳으로\n이동하시겠습니까?"))
			{
				//예 클릭시 마지막학습페이지부터
				startPage = bookmarkPage;
			}else
			{
				//아니요 클릭 시 마지막학습 위치 무시하고 첫페이지 부터
				startPage = startPageURL;
			}
		}
	}else
	{
		//포팅버전 아닐때 첫페이지부터 학습
		startPage = startPageURL;
	}

	bookmarkNum = Number(startPage.split(".")[0]);

	contentFrame.width = con_w;
	contentFrame.height = con_h;
	contentFrame.src = startPage;

}


/**
 * closeContent	컨텐츠가 닫혔을 경우 실행
 */			
function closeContent(){
	if(bolPorted) doTerminate();
}


function itostr(n){
	if(n<10){
		return "0"+n;
	}else{
		return ""+n;
	}
}


