var bodyObj;
var wrapperObj;
var fullScreenBtn;
var isfullMode = false;
var contentFrame;

// fullScreen
function fullScreenFnc2() {
    windowObj = parent.window;
    bodyObj = parent.document.querySelector("body");
    wrapperObj = parent.document.querySelector('#wrapper');
    fullScreenBtn = document.querySelector('#fullBtnMy');
	contentFrame = parent.document.querySelector('#contentFrame');

    // fullScreen을 지원하는지 여부
    var fullScreenEnabled = parent.document.fullscreenEnabled || parent.document.mozFullScreenEnabled || parent.document.msFullscreenEnabled || parent.document.webkitSupportsFullscreen || parent.document.webkitFullscreenEnabled ? true : false;

    // 지원하지 않으면 버튼 안 보이게
    if (!fullScreenEnabled) {
        fullScreenBtn.style.display = 'none';
    }

    fullScreenBtn.addEventListener('click', function() {
        // fullScreen일 경우 원래대로(PC에서 esc로 fullScreen 모드를 해제하는 경우에는 호출되지 않음)
         if (isFullScreen()) {
			isfullMode = false;
            if (parent.document.exitFullscreen) parent.document.exitFullscreen();
            else if (parent.document.mozCancelFullScreen) parent.document.mozCancelFullScreen();
            else if (parent.document.webkitCancelFullScreen) parent.document.webkitCancelFullScreen();
            else if (parent.document.msExitFullscreen) parent.document.msExitFullscreen();
        }
        else { // fullScreen이 아닐 경우 fullScreen으로(필히 실행됨)
			isfullMode = true;
            if (wrapperObj.requestFullscreen) wrapperObj.requestFullscreen();
            else if (wrapperObj.mozRequestFullScreen) wrapperObj.mozRequestFullScreen();
            else if (wrapperObj.webkitRequestFullScreen) wrapperObj.webkitRequestFullScreen();
            else if (wrapperObj.msRequestFullscreen) wrapperObj.msRequestFullscreen();
        }
    });
}

// fullScreen 상태 리턴
function isFullScreen() {
    return !!(parent.document.fullScreen || parent.document.webkitIsFullScreen || parent.document.mozFullScreen || parent.document.msFullscreenElement || parent.document.fullscreenElement);
}

// responsive.js에 정의됨. PC인 경우에만 호출함
function fullScreenFnc() {
    responsiveFnc.currentContainerSize.containerWidth = bodyObj.clientWidth;
    responsiveFnc.currentContainerSize.containerHeight = bodyObj.clientHeight;
    responsiveFnc.setScaleElement(contentFrame, wrapperObj);

    windowObj.removeEventListener("resize", originScreenFnc, false);
}

function originScreenFnc() {
    responsiveFnc.currentContainerSize.containerWidth = responsiveFnc.defaultContainerSize.width;
    responsiveFnc.currentContainerSize.containerHeight = responsiveFnc.defaultContainerSize.height;
    responsiveFnc.setScaleElement(contentFrame, wrapperObj);

    windowObj.removeEventListener("resize", fullScreenFnc, false);
}

// fullscreenchange 시 호출
function handleFullScreen(status) {
    if (status) { // fullScreen
        if (1 == 1) {
            wrapperObj.classList.add("fullScreen");
            fullScreenFnc(); // for chrome
            windowObj.addEventListener("resize", fullScreenFnc, false); // for IE
			
			$('#fullBtnMy').hover(function(){
				$(this).css({"background":"url(../common/img/main/full_re_over.png) no-repeat"});
			},function(){
				$(this).css({"background":"url(../common/img/main/full_re.png) no-repeat"});	
			});
        }
    }
    else { // originScreen(esc로 누른 경우 포함)
        wrapperObj.classList.remove("fullScreen");
        if (parent.document.exitFullscreen) parent.document.exitFullscreen();
        else if (parent.document.mozCancelFullScreen) parent.document.mozCancelFullScreen();
        else if (parent.document.webkitCancelFullScreen) parent.document.webkitCancelFullScreen();
        else if (parent.document.msExitFullscreen) parent.document.msExitFullscreen();
		
		$('#fullBtnMy').hover(function(){
				$(this).css({"background":"url(../common/img/main/full_over.png) no-repeat"});
			},function(){
				$(this).css({"background":"url(../common/img/main/full.png) no-repeat"});	
			});

        if (1 == 1) {
            originScreenFnc(); // for chrome
            windowObj.addEventListener("resize", originScreenFnc, false); // for IE
        }
    }
}