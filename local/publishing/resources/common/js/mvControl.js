//alert('1 : mvControl');
// JavaScript Document
var mvObject;
var isMediaObj;
var isMediaPlayFlag=true;
var isMediaEndFlag=false;

var progressWidth=0;
var isDragMc;
var isProgressBar;
var isDragMcHWidth;
var isDragPercent=0;
var goPercent=0;

var isDragingFlag=false;
var _dragRect = new Object();
var isSpeedFlag=true;

var isMediaRate=1;

var isSpeed;
var isSkipTime=0;


var _nextloadMediaURL;
var _nextloadMediaType;


var strTemp = itostr(thisPageNum) + ".html";
var nTempPage = parseInt(thisPageNum, 10);
var nTempTotal = parseInt(totalPageNum, 10);


if(isExp && isIEVersion<10){
	
}else{
	var isNextSnd = new Audio();
	isNextSnd.autoplay = true;
	isNextSnd.controls = false;

}
function _doStartNextMovieAct(){
	//alert(isMediaObj.volume);
	var preVol = isMediaObj.volume;
	//_nextloadMediaURL = mediaInfoArray[thisPageNum][2];
	//_nextloadMediaType = mediaInfoArray[thisPageNum][3];
	isNextMovieShowFlag=true;
	if(isExp && isIEVersion<10){
		doControlCall("pause");
	}else{
		if(isMediaObj){
			isMediaObj.pause();
		}	
	}
	
	if(isExp && isIEVersion<10){
		//alert(_nextloadMediaType+"@@../"+_nextloadMediaURL);
		doControlCall("volume",isVolDragPercent);
		doControlCall("movieChange",_nextloadMediaType+"@@"+_nextloadMediaURL);
		//doControlCall("volume",isVolDragPercent);
	}else{
		if(_nextloadMediaType=="mp4"){
			$("#mobileLoading").show();

			mvObject = $("#myVideo");
			isMediaObj= $("#myVideo")[0];
			$("#myVideoSrc").attr("src",_nextloadMediaURL);
			$("#myAudio").hide();
			$("#myVideo").show();
			$('#myVideo').attr('poster', '../common/img/main/poster2.png');
			mvObject.get(0).load();
			mvObject.get(0).onloadeddata =  function() {
				$("#mobileLoading").hide();
					//if(mvObject.get(0).readyState >= 2) mvObject.get(0).play();
	
			};
		}else if(_nextloadMediaType=="mp3"){
			mvObject = $("#myAudio");
			isMediaObj= $("#myAudio")[0];
			$("#myAudioSrc").attr("src",_nextloadMediaURL);
			$("#myVideo").hide();
			$("#myAudio").show();
		}else{

		}	
		isMediaObj.volume = preVol;
	}
	_doEventInit();
	isConNext=false;	//true면 말풍선이 안뜸...
	
	if(preVol==0){
		isMediaObj.muted = true;
	}
	
	
	
	$("#loadActFrame").load("about:blank");
	$("#loadActFrame").hide();
	$("#htmlContents").html("");
	$("#htmlContents").hide();
	
}


function _doMovieInit(){
    fullScreenFnc2();
	/*xml 로드 및 페이지 설정*/
	$.ajax({
		type: "GET" ,
		url: "../common/config/media_url.xml",
		dataType: "xml",
		success: function(xml) {
			mp4Url = $(xml).find("info").find("mp4Url").text();
			mp3Url = $(xml).find("info").find("mp3Url").text();
			$(xml).find("nav").each(function(idx)
			{
					if(thisChapNum == (idx+1)){
						$(this).find("meialLink").each(function(ssx){
							if(thisPageNum == (ssx+1)){
								_nextloadMediaType = $(this).attr("nextType")
								loadMediaType = $(this).attr("type");
								
								if(loadMediaType=="mp4"){
									loadMediaURL = mp4Url+""+$(this).attr("file");	
									_nextloadMediaURL = mp4Url+""+$(this).attr("nextFile");	
								}else{
									loadMediaURL = mp3Url+""+$(this).attr("file");	
									_nextloadMediaURL = mp4Url+""+$(this).attr("nextFile");	
								}
								
								
							}
						});
					}
					

						
			});
			_doMovieInit2();
		},
		error: function(err) {
			//alert(err);
		}
	});
	
	
}


function _doMovieInit2(){
	
	isDragMc = $('#jogbtn');
	isProgressBar = $('#jogProgress');
	
	
	_dragRect._left = isProgressBar[0].offsetLeft;
	_dragRect._width = isProgressBar[0].clientWidth;
	
	isDragMcHWidth = parseInt(isDragMc.css("width").split("px")[0],10)/2;
	
	progressWidth =  $('#jogbar')[0].clientWidth;
	_dragRect._right = _dragRect._left+progressWidth+isDragMcHWidth*2;
	
	isProgressBar.css("width",(progressWidth*0)+"px");
	isDragMc.css("left",((progressWidth)*0)+"px");
	if(isExp && isIEVersion<10){
		isDragMc[0].onmousedown = _DragStart;
		$('#jogbar')[0].onclick = _doSeekProgress;
	}else{
		//
		progressSet();
		if(!bolTLock)
		{
			isDragMc[0].addEventListener("mousedown",_DragStart,false);	
			$('#jogbar')[0].addEventListener("click",_doSeekProgress);	
		}
	}

	if(isExp && isIEVersion<10){
		_doShowSWF(loadMediaURL,loadMediaType);
		isMediaObj = thisMovie(swfMediaID);
	}else{
		if(loadMediaType=="mp4"){
			mvObject = $("#myVideo");
			isMediaObj= $("#myVideo")[0];
			$("#myVideoSrc").attr("src",loadMediaURL);
			$('#myVideo').attr('poster', '../common/img/main/poster.png');
			$("#myAudio").hide();
			mvObject.get(0).load();
		}else if(loadMediaType=="mp3"){
			mvObject = $("#myAudio");
			isMediaObj= $("#myAudio")[0];
			$("#myAudioSrc").attr("src",loadMediaURL);
			$("#myVideo").hide();
		}else{

		}

		isMediaObj.load();
		
	}
	
	if(isMobile){
		isMediaObj.autoplay=false;
		$("#mobileStart").show();
		isMediaPlayFlag=false;
	}else{
		//
		
		if(loadMediaType=="mp3"){
			if(isExp && isIEVersion<10){
				doControlCall("pause");
				isMediaPlayFlag=false;
			}else{
				isMediaObj.autoplay=false;	
			}	
		}
		
		
		
		
		isMediaPlayFlag=false;
		$("#mobileStart").hide();
	}
	
	$("#mobileStart").bind("click",function(){
		isMediaObj.play();
	})
	_doEventInit();
	
	/*if(isMediaPlayFlag){
		$("#playBtn").addClass("pause");
	}*/
	
	$("#stopBtn").bind("click",function(){
        isMediaObj.pause();
	});
        
    $("#playBtn").bind("click",function(){
        isMediaObj.play();
	});
        
	$("#replayBtn").bind("click",function(){
		/////////////////////
		//### 해당 페이지만 말풍선 초기화 세팅 ###
		if (loadFrameURL == "term.html" || loadFrameURL == "quiz_init.html" || loadFrameURL == "summary.html")
		{
			isConNext = true;	//말풍선 안뜨도록 초기화
		}
		/////////////////////

		_actitiveInit();
		_loadFrameAct();

		_doQuizFrameHide();
		if(isExp && isIEVersion<10){
			doControlCall("seek",0);
			doControlCall("play");

			////////////////////////////////////////////
			No_Sound_PlayFull();	//NO sound device 대응
			////////////////////////////////////////////

			return;
		}

		try{
			isMediaObj.play();
			isMediaObj.currentTime=0;
		} catch(e){

			////////////////////////////////////////////
			No_Sound_PlayFull();	//NO sound device 대응
			////////////////////////////////////////////

		}
		
		isMediaObj.playbackRate = isMediaRate;
		
		isMediaPlayFlag=true;
        
        //$(".sumBtn").css({"display":"none"});
        
        //setTimeout(function(){
        //    $(".sumBtn").css({"display":"block"});
        //},9000)
		
	});

	//전체화면보기버튼
	
	$('#fullBtn').on('click', function() {
		if(isConNextArray[thisPageNum] == true)
		{
			alert('해당 페이지에서는 지원하지 않는 기능입니다.');
		}else
		{
//			alert('준비중입니다');
			_doFullScreen();
		
		}
	});


	//NO sound device 대응
	function No_Sound_PlayFull(){
			if (isNoSoundDev)
			{
				$("#nextBubble").removeClass("show last");
				$("#nextBubble").hide();
				
				if(isConNextArray[thisPageNum]){
					isConNext = isConNextArray[thisPageNum];
				}
				_doSync(0, null);
				setTimeout(function(){
					_doSyncFullView();
					_Mvended();
				}, 500);
			}
	}
	
	



	$("#scriptBtn").bind("click",function(){
		_showNar();
	})
	
	_mvRateInit();
	
	try{
		_doVolumeInit();
	}catch(e){}
	
}

/*--------- SWF --------*/
function _doShowSWF(_url,_type){
	var mediaStr="";
	if(_type=="mp4"){
	}
	var volStr="myVolPct"
	
	var _volC = getCookie(volStr).split("/");
	var tmpVolDragPercent = _volC[0];

	if(tmpVolDragPercent==null || tmpVolDragPercent==""){
		
		tmpVolDragPercent=0.5;	//1에서 0.5로 기본값 변경
		
	}
	tmpVolDragPercent = Number(tmpVolDragPercent);
	var myFlashVars="&mediaURL="+_url+"&mediaType="+_type+"&isVolDragPercent="+(tmpVolDragPercent*100);
		mediaStr = mediaStr + '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" ';
		mediaStr = mediaStr + 'codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'+swfMediaWidth+'" height="'+swfMediaHeight+'" id='+swfMediaID+'>';
		mediaStr = mediaStr + '	<param name="movie" value='+flash_url+'>';
		mediaStr = mediaStr + '	<param name="quality" value="high">';
		mediaStr = mediaStr + '	<param name="menu" value="false">';
		mediaStr = mediaStr + '	<param name="wmode" value="transparent">';
		mediaStr = mediaStr + '	<param name="flashVars" value="'+myFlashVars+'">';
		mediaStr = mediaStr + '	<param name="allowScriptAccess" value="always" />';
		mediaStr = mediaStr + '	<param name="allowFullScreen" value="true" />';
		mediaStr = mediaStr + '	<embed src='+flash_url+' allowFullScreen="true" quality="high" menu="false" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" ';
		mediaStr = mediaStr + '	type="application/x-shockwave-flash" flashVars="'+myFlashVars+'" name="'+swfMediaID+'" width="'+swfMediaWidth+'" height="'+swfMediaHeight+'"></embed></object>';
	$("#swfDiv").html(mediaStr);
	$("#swfDiv").show();
	
}

function doControlCall(f,str){
	var sTarget=thisMovie(swfMediaID);
	try{
	sTarget._doControlCall(f,str);
	}catch(e){}
}


function _doAlertSnd(str){
	doControlCall("alertSnd",str);
}

/*--------- SWF --------*/

function _mvRateInit(){
	var isRateCookStr="myRateSC"
	var Cook_isMediaRate = String(getCookie(isRateCookStr));
	$("#sp10").bind("click",function(){
		isMediaObj.playbackRate = 1;
		isMediaRate = 1;
		$("#sp10").addClass("active")
		$("#sp12").removeClass("active")
		$("#sp13").removeClass("active")
		setCookie(isRateCookStr,"1",360);
	});	
	$("#sp12").bind("click",function(){
		isMediaObj.playbackRate = 1.2;
		isMediaRate = 1.2;
		$("#sp10").removeClass("active")
		$("#sp12").addClass("active")
		$("#sp13").removeClass("active")
		setCookie(isRateCookStr,"1.2",360);
	});	
	$("#sp13").bind("click",function(){
		isMediaObj.playbackRate = 1.3;
		isMediaRate = 1.3;
		$("#sp10").removeClass("active")
		$("#sp12").removeClass("active")
		$("#sp13").addClass("active")
		setCookie(isRateCookStr,"1.3",360);
	});	
	switch(Cook_isMediaRate){
		case "1":
			$("#sp10").addClass("active")
		break;
			
		case "2":
			$("#sp12").addClass("active")
		break;
			
		case "3":
			$("#sp13").addClass("active")
		break;
			
		default:
			$("#sp10").addClass("active")
		break;

	}
	
}


function _DragStart(e){
	var e = e || window.event;
	isDragingFlag=true;
	if(isExp && isIEVersion<10){
		document.onmousemove = _mouseMove;
	}else{
		window.addEventListener("mousemove",_mouseMove);
	}
	
	if(isExp && isIEVersion<10){
		document.onmouseup = _mouseMoveStop;
		document.click = _mouseMoveStop;
	}else{
		window.addEventListener("mouseup",_mouseMoveStop);
		window.addEventListener("click",_mouseMoveStop);
	}
	
	$("#controller").on("mouseleave",function(){
		_mouseMoveStop(e);
	});
	
}


function _mouseMove(e){
	
	var e = e || window.event;
	var nPos = e.clientX;
	
	if(!isDragingFlag){return;}
	
    var tmpL = $("#container")[0].offsetLeft + ($('#jogbar')[0].offsetLeft*1);
	
	var isMLeft = tmpL;
    var isMRight = (isMLeft+$('#jogbar')[0].clientWidth) * 1;
    var isMWidth = $('#jogbar')[0].clientWidth * 1;
	
	var nPosX = nPos;
    nPosX = nPosX; 
	if(nPos<isMLeft){
		nPosX = nPos;
	}
	if(nPos>isMRight){
		nPosX = isMRight;
	}
	isDragPercent = (nPosX - isMLeft) / isMWidth;
	if(isDragPercent<0){
		isDragPercent=0;
	}
	if(isDragPercent>1){
		isDragPercent=1;
	}
	//console.log("_mouseMove!! "+isDragPercent);
	progressWidth =  $('#jogbar')[0].clientWidth;
	isProgressBar.css("width",(progressWidth*isDragPercent+isDragMcHWidth/4)+"px");
	isDragMc.css("left",((progressWidth)*isDragPercent-isDragMcHWidth)+"px");
	
}

function _doMediaSeek(st){
	
	 n = st * isMediaObj.duration;
	 //n = Math.floor(n);
	
	if(n<0){
		n = 0;
	}
	if(n > isMediaObj.duration-0.3){
		n = isMediaObj.duration-0.3
	}
	//console.log("_doMediaSeek > "+n+" : "+st);
	if(isExp && isIEVersion<10){
		//doControlCall("play");
		doControlCall("seek",n);
	}else{
		isMediaObj.play();
		try
		{
			isMediaObj.currentTime  = n;	
		} catch (e) { }
		isMediaObj.playbackRate = isMediaRate;	
	}
	
	
	
}

function _mouseMoveStop(e){
	var e = e || window.event;
	isDragingFlag=false;
	if(isExp && isIEVersion<10){
		document.onmousemove = null;
		document.onmouseup = null;
		document.click = null;
	}else{
		window.removeEventListener("mousemove",_mouseMove);
		window.removeEventListener("mouseup",_mouseMoveStop);
		window.removeEventListener("click",_mouseMoveStop);
	}
	
	//console.log("\n\n_mouseMoveStop!! "+isDragPercent+" // "+isDragingFlag)
	if(isDragPercent>0){
		_doMediaSeek(isDragPercent);
		_doQuizFrameHide();
	}
	$("#controller").off("mouseleave");
		
}
function _doSeekProgress(e){
    var e = e || window.event;
    //alert("_doSeekProgress")
    var nPos = e.clientX;

    // 1 적용 - edumaru
    var tmpL = $("#container")[0].offsetLeft + ($('#jogbar')[0].offsetLeft*1);

    var isMLeft = tmpL;
    var isMRight = (isMLeft+$('#jogbar')[0].clientWidth) * 1;
    var isMWidth = $('#jogbar')[0].clientWidth * 1;

    var nPosX = nPos;
    nPosX = nPosX; // 1 삭제 - edumaru

    if(nPos<isMLeft){
        nPosX = nPos;
    }
    if(nPos>isMRight){
        nPosX = isMRight;
    }
    goPercent = (nPosX - isMLeft) / isMWidth;
    if(goPercent<0){
        goPercent=0;
    }
    if(goPercent>1){
        goPercent=1;
    }

    //if(goPercent<0.01){goPercent=0.01;}
    //if(goPercent>0.99){goPercent=0.99;}

	//console.log("_doSeekProgress!! : "+goPercent);
	_doMediaSeek(goPercent);
	_doQuizFrameHide();
}

function rectime(secs){
	if(isNaN(secs)){
		secs=0;	
	}
	var hr = Math.floor(secs / 3600);
	var min = Math.floor((secs - (hr*3600))/60);
	var sec = Math.floor(secs - (hr*3600) - (min*60));
	
	if(hr < 10) {hr = '0'+hr;}	
	if(min < 10) {min = '0'+min;}
	if(sec < 10) {sec = '0'+sec;}
	if(hr){hr='00';}
	return min+':'+sec;
}


function _doTimeChk(p,c,t){
	//최대치 세팅
	if (t < c)
	{
		c = t;
	}
	$("#time").html(rectime(c)+" / "+rectime(t));

}


function _doEventInit(){
	/*동영상 이벤트 리스너*/
	if(isExp && isIEVersion<10){
		_SWF_INIT();
		return;
	}
	isMediaObj.addEventListener("timeupdate",_timeUpdate);
	isMediaObj.addEventListener("ended",_Mvended);
	isMediaObj.addEventListener("seeking",_seek);
	isMediaObj.addEventListener("seeked",_onSeek);
	isMediaObj.addEventListener("onprogress",_onProgress);
	isMediaObj.addEventListener("pause",_pause);
	isMediaObj.addEventListener("play",_play);

}

var isMediaPercent;

function _timeUpdate(e){
	if (isNoSoundDev) { return }	//###


	var e = e || window.event;
	isMediaPercent=0;
	//if(isMediaObj.duration>0){
		var tmp1 = Number(isMediaObj.currentTime) ;
		var tmp2 = Number(isMediaObj.duration);
		if(Number(tmp1)>=0){
		}else{
			tmp1=0
		};
		if(Number(tmp2)>=0){
		}else{
			tmp2=0
		};

		progressWidth =  $('#jogbar')[0].clientWidth;
		isMediaPercent = Math.floor(progressWidth*tmp1/tmp2);
		_doTimeChk(isMediaPercent,isMediaObj.currentTime,isMediaObj.duration);
	//}
	progressSet();
	if(!isDragingFlag){
		if(isMediaPercent==0){
			isProgressBar.css("width",isMediaPercent+"px");
			isDragMc.css("left",isMediaPercent-7+"px");
		}else{
			isProgressBar.css("width",isMediaPercent+"px");
			isDragMc.css("left",isMediaPercent-7+"px");
		}
	}
    
    //배속기능
    $("#bae1").click(function(){
        isMediaObj.playbackRate = 1;
    });

    $("#bae12").click(function(){
        isMediaObj.playbackRate = 1.2;
    });

    $("#bae15").click(function(){
        isMediaObj.playbackRate = 1.5;
    });
    
	///////////////////////////////////
	// 컨트롤바 최대로 세팅
	///////////////////////////////////
	var _left = parseInt(isProgressBar.css("width"), 10);
	if (progressWidth < _left)
	{
		isProgressBar.css("width", progressWidth);
	}
	///////////////////////////////////

	//console.log(isMediaPercent+" : "+isMediaObj.currentTime+"/"+isMediaObj.duration)
	isMediaEndFlag=false;
	
	if(isSkipTime>0){
		if(isMediaObj.currentTime>isSkipTime && $("#btn_skip") && $("#btn_skip").length){
			$("#btn_skip").hide();
		}
	}
	$("#nextBubble").removeClass("show last");
	$("#nextBubble").hide();
	//try{$("#mvEndContents").html("");}catch(e){}
	try{_doSync(isMediaObj.currentTime,isMediaObj.duration);}catch(e){}
	
}

function _SWF_INIT(){
	
}

function _doSWFTimeView(c,t){
	isMediaObj.currentTime = c;
	isMediaObj.duration = t;
	_timeUpdate();
	//top.status = isMediaObj.currentTime+" / "+isMediaObj.duration+" : "+isMediaEndFlag;
}

function _doSWFStateChangeSwf(str){
	switch(str){
		case "playing"	:
			_play();
			break;
		case "paused"	:
			_pause();
			break;
	}
}

var isPopClosingFlag=false;
function _play(e){
	var e = e || window.event;
	isMediaPlayFlag=true;
	if($("#playBtn").hasClass("pause")){
		
	}else{
		$("#playBtn").addClass("pause");
	}
	$("#mobileStart").hide();
	_doNormalScreen();
	_doQuizFrameHide();
	
	
	if($("#popClosing").length){
		isPopClosingFlag=true;
		setTimeout(function(){
			if(isPopClosingFlag){
				var tmpCloseStr2="<button id='popClosing2' onClick='parent._asideClose2();'></button>"
				$('#contents').append(tmpCloseStr2);		
				isPopClosingFlag=false;
			}
			
		},1000)
		
	}
}
function _pause(e){
	var e = e || window.event;
	$("#mvEndContents").hide();
	if($("#playBtn").hasClass("pause")){
		$("#playBtn").removeClass("pause");
	}else{
		
	}
	isMediaPlayFlag=false;
}

function _ended(e){
	var e = e || window.event;
	isMediaPlayFlag=false;
}
function _seek(e){
	var e = e || window.event;
	//isMediaObj.playbackRate = isMediaRate;
}
function _onSeek(e){
	var e = e || window.event;
	isMediaObj.playbackRate = isMediaRate;
}

function _onProgress(e){
	var e = e || window.event;
}
function _Mvended(){
	//isConNext=false;	//true면 말풍선이 안뜸...


	if(!$("#playBtn").hasClass("pause")){
		$("#playBtn").addClass("pause");
	}else{
		
	}
	
	isMediaEndFlag=true;
	$("#btn_skip").hide();
	if(isConNext){
		//$("#mvEndContents").show();
	}else{
		_showBubble();
	}
	$("#startQuizBtn").show();
	
	
	
	//
	if($("#popClosing").length){
		setTimeout(function(){
			parent._asideClose2();	
		},1000)
		
	}
	
	//top.status = "_Mvended!! : "+isMediaObj.currentTime+" / "+isMediaObj.duration+" : "+isMediaEndFlag;
}

function _isEndCurrentPageAct(){
	isConNext=false;
	//alert("_isEndCurrentPageAct!!! / isMediaEndFlag = "+isMediaEndFlag);
	if(isMediaEndFlag){
		setTimeout("_showBubble()", 1300);
	}
	//isConNext=true;
	
}

function _isReplayActivity(){
	$("#nextBubble").fadeOut();
}

function _showBubble(){
	//alert("_showBubble!!")
	if ($("#nextBubble").css("display") != "none")
	{
		 return;
	}

	//말풍선 값 다시 체크(퀴즈 결과에서 다시풀기 빠르게 누를때 발생)
	if (isConNext)
	{
		return;
	}

	if(isNarShowFlag){
		$("#nextBubble").fadeIn();
	}else{
		$("#nextBubble").fadeIn();
	}
	
	
	if($("#popClosing").length){
				
	}else{
  //다음페이지 안내말풍선
		if(isExp && isIEVersion<10){
			_doAlertSnd("../common/mp3/nextSnd.mp3");
		}else{

			isNextSnd.volume = isMediaObj.volume;
			isNextSnd.src="../common/mp3/nextSnd.mp3";
			isNextSnd.load=function(){
				isNextSnd.volume = isMediaObj.volume;
				isNextSnd.play();
			}	
		}
	}

		end_pageF();

	if(thisPageNum == totalPageNum){
		$("#nextBubble").addClass("last");	//마지막 페이지 말풍선
	}else{
  //다음페이지 안내말풍선
		$("#nextBubble").addClass("show");
	}

	
}

//마지막 페이지 말풍선 : 진도 호출
function end_pageF() {
	if(parent.bolPorted){
		if(parent.bolTest)
		{
			parent.bookmarkPage = strTemp;
			parent.bookmarkNum = nTempPage;
		}else
		{
			parent.PageLocation(strTemp, nTempPage, nTempTotal);
			parent.bookmarkPage = parent.doGetValue("cmi.location");
			parent.bookmarkNum = Number(parent.bookmarkPage.split(".")[0]);
		}
	}
}




function _doFullScreen(){
				
	if($.isFunction(isMediaObj.webkitEnterFullscreen)) {
		isMediaObj.webkitEnterFullscreen();
		mvObject.attr('controls',"true");
	}	else if ($.isFunction(isMediaObj.mozRequestFullScreen)) {
		isMediaObj.mozRequestFullScreen();
	}	else if ($.isFunction(isMediaObj.exitFullscreen)) {
		isMediaObj.exitFullscreen();
		mvObject.attr('controls',"true");
	}	else if ($.isFunction(isMediaObj.msRequestFullscreen)) {
		isMediaObj.msRequestFullscreen();
		mvObject.attr('controls',"true");
		//alert('익스플로러에서는 지원하지 않는 기능입니다.');
	}
	else 
	{
		alert('Your browsers doesn\'t support fullscreen');
	}
	
}

function _doNormalScreen(){
	
	
	if(isMediaObj.exitFullscreen) {
		isMediaObj.exitFullscreen();
	} else if(isMediaObj.mozCancelFullScreen) {
		isMediaObj.mozCancelFullScreen();
	} else if(isMediaObj.webkitExitFullscreen) {
		isMediaObj.webkitExitFullscreen();
	}else if (isMediaObj.msExitFullScreen) {
		isMediaObj.msExitFullScreen();
	}
}
