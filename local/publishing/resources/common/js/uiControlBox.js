function setTop(){

	var setTop = "";
	setTop +='				<div class="chapDiv">';
	setTop +='					<img id="chapName">';
	setTop +='				</div>';
	setTop +='				<div id="course"></div>';

	$("#top").append(setTop);

}



function setFooter(){

	var footStr = "";

	footStr +='		<div id="footerSet">';
	footStr +='			<div id="narration" name="narration">';
	footStr +='                <div id="nar_txt" class="scroll-pane"></div>';
	footStr +='                <button id="nar_close" class="controlBtn">자막</button>';
	footStr +='        	</div>';
	footStr +='			<div id="controller">';
	footStr +='				<div id="supportBox">';
	footStr +='					<button id="lnbBtn" class="controlSet controlBtn">목차</button>';
	footStr +='					<button id="downBtn" class="controlSet controlBtn"></button>';
    footStr +='					<button id="downBtn_over" class="controlSet controlBtn"></button>';
	footStr +='					<button id="dicBtn" class="controlSet controlBtn"></button>';
    footStr +='					<button id="dicBtn_over" class="controlSet controlBtn"></button>';
	footStr +='					<button id="noteBtn" class="controlSet controlBtn"></button>';
    footStr +='					<button id="noteBtn_over" class="controlSet controlBtn"></button>';
	footStr +='					<button id="bookBtn" class="controlSet controlBtn"></button>';
    footStr +='					<button id="bookBtn_over" class="controlSet controlBtn"></button>';
	footStr +='				</div>';
	footStr +='				<div id="jogbar">';
	footStr +='					<div id="jogProgress"></div>';
	footStr +='					<div id="jogbtn"></div>';
	footStr +='				</div>';
    //footStr +='				<button id="jogbar_over" class="controlSet controlBtn"></button>';
	footStr +='				<div id="time">00:00 / 00:00</div>';
	footStr +='				<div id="contBtnBox">';
	footStr +='					<button id="playBtn" class="controlSet controlBtn">재생</button>';
    footStr +='					<img id="playBtn_over" class="controlSet controlBtn">';
    footStr +='					<button id="stopBtn" class="controlSet controlBtn">일시정지</button>';
    footStr +='					<img id="stopBtn_over" class="controlSet controlBtn">';
	footStr +='					<button id="replayBtn" class="controlSet controlBtn">리플레이</button>';
    footStr +='					<button id="replayBtn_over" class="controlSet controlBtn">리플레이</button>';
	footStr +='					<button id="scriptBtn" class="controlSet controlBtn">자막</button>';
    footStr +='					<button id="scriptBtn_over" class="controlSet controlBtn">자막</button>';
	footStr +='					<button id="fullBtnMy" class="controlSet controlBtn">전체화면</button>';
    footStr +='					<img id="fullBtnMy_over" class="controlSet controlBtn">';
	footStr +='					<button id="muteBtn" class="controlSet controlBtn">음소거</button>';
    footStr +='					<button id="muteBtn_over" class="controlSet controlBtn"></button>';
	footStr +='				</div>';
    footStr +='				<div id="bae">';
    footStr +='				    <div id="bae1"></div>';
    footStr +='				    <div id="bae12"></div>';
    footStr +='				    <div id="bae15"></div>';
    footStr +='				</div>';
	footStr +='				<div id="volJogBar">';
	footStr +='					<div id="volJogProgress"></div>';
	footStr +='					<div id="volJogbtn"></div>';
	footStr +='				</div>';
	footStr +='			</div>';
	footStr +='			<div id="paging">';
	footStr +='				<button id="prevBtn" class="controlSet controlBtn">이전페이지이동</button>';
    footStr +='				<button id="prevBtn_over" class="controlSet controlBtn">이전페이지이동</button>';
	footStr +='				<div id="pageNum">';
	footStr +='					<div id="currentNum">1</div>';
	footStr +='					<div id="pageLine"></div>';
	footStr +='					<div id="totalNum">1</div>';
	footStr +='				</div>';
	footStr +='				<button id="nextBtn" class="controlSet controlBtn">다음페이지이동</button>';
    footStr +='				<button id="nextBtn_over" class="controlSet controlBtn">다음페이지이동</button>';
	footStr +='			</div>';
	footStr +='			<div id="nextAram"><button id="nextBubble">다음페이지로 이동하세요.</button></div>';
	footStr +='		</div>'

	$("#footer").append(footStr);
	

}