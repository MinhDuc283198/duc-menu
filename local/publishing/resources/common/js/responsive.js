var responsiveFnc = responsiveFnc || {};

responsiveFnc = (function() {
    var responsive = {
        // 기본 템플릿 사이즈
        defaultContainerSize : {
            width : 1020,
            height : 592
        },
        // 현재 템플릿 사이즈, setScaleElement 함수 호출 시 수동 셋팅 또는 resize(mobile) 시 자동 셋팅
        currentContainerSize : {
            containerWidth : window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            containerHeight : window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
        },
        // scale 셋팅
        setScaleElement : function(targetElement, adjust) {
            if (currentOS == "nomobile") {
                var zoomWidth = responsive.currentContainerSize.containerWidth / responsive.defaultContainerSize.width;
                var zoomHeight = responsive.currentContainerSize.containerHeight / responsive.defaultContainerSize.height;
				adjust.style.width = responsive.defaultContainerSize.width * zoomWidth + "px";
				adjust.style.height = responsive.defaultContainerSize.height * zoomHeight + "px";
            }
            else {
                var zoomWidth = responsive.currentContainerSize.containerWidth / targetElement.clientWidth;
                var zoomHeight = responsive.currentContainerSize.containerHeight / targetElement.clientHeight;
            }
            // 가로 배율, 세로 배율에 따라 setTransformCSS 호출
            if (zoomWidth < zoomHeight) {
                this.setTransformCSS(targetElement, zoomWidth);
				targetElement.style.top = ((this.currentContainerSize.containerHeight - (targetElement.clientHeight * zoomWidth)) / 2)  + 'px';
            }
            else {
                this.setTransformCSS(targetElement, zoomHeight);
                targetElement.style.left = ((this.currentContainerSize.containerWidth - (targetElement.clientWidth * zoomHeight)) / 2)  + 'px';
            }
        },
        // scale 설정
        setTransformCSS : function (targetElement, zoom) {
            zoomRate = zoom;
            targetElement.setAttribute('style', '-ms-transform: scale(' + zoomRate + ',' + zoomRate + ');'
                + '-webkit-transform: scale(' + zoomRate + ',' + zoomRate + ');' + 'transform: scale(' + zoomRate + ',' + zoomRate + ');'
                + 'transform-origin: 0 0; -webkit-transform-origin: 0 0; -ms-transform-origin: 0 0;');
        }
    };
    return responsive;
})();

