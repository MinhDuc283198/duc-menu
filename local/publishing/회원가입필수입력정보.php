<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  

<div class="login-bx">
    <ul class="login-tab tab-event">
        <li><a href="#">로그인</a></li>
        <li class="on"><a href="#">회원가입</a></li>
    </ul>
    <p class="ft-dot">필수입력정보입니다.</p>
    <form class="email-bx join">
        <p>
            <strong>이름</strong>
            <input type="text" placeholder="이름입력" />
        </p>
        <p>
            <strong>생년월일</strong>
            <select>
                <option value="" hidden>년도</option>
                <option value="">1989</option>
            </select>
            <select>
                <option value="" hidden>월</option>
                <option value="">05</option>
            </select>
            <select>
                <option value="" hidden>일</option>
                <option value="">07</option>
            </select>
        </p>
        <p>
            <strong>이메일</strong>
            <input type="text" placeholder="이메일입력" />
            <span class="warning">이메일 부적합 사유 안내</span>
        </p>
        <p class="phone">
            <strong>휴대폰 번호</strong>
            <input type="text"/>
            <span class="dash">-</span>
            <input type="text"/>
            <span class="dash">-</span>
            <input type="text"/>
            <span class="warning">휴대폰 번호 부적합 사유 안내</span>
        </p>
    </form>
    <div class="text-center btn-area">
        <input type="button" value="가입완료" class="btns point big" />
    </div>

</div>

<?php
echo $OUTPUT->footer();
?>


