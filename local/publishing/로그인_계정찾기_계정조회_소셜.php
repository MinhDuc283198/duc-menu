<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  

<div class="login-bx">
    <div class="bg">
        <div class="center-tit">나의 계정 찾기</div>
        <p class="center-txt">회원가입 시 등록한 정보로 나의 계정을 찾을 수 있습니다.</p>
    </div>
    <div class="fd-u-info">
        <div>
            <strong>아이디</strong>
            <span><img src="/theme/oklassedu/pix/images/icon_facebook.png" alt="" />vi****@facebook.com</span>
        </div>
        <div>
            <strong>비밀번호</strong>
            <p class="warning">소셜 로그인으로 가입한 경우 해당 SNS에서 비밀번호 변경 가능합니다.</p>
        </div>

    </div>

</div>


<?php
echo $OUTPUT->footer();
?>


