<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$PAGE->navbar->add('강의목록');


echo $OUTPUT->header();
?>  


<div class="tp-crs-info">
    <div class="tp">
        <h3>
            <span>한국어 강좌</span>
            [초급]한국어 초급문법[베트남어]
        </h3>
        <p class="dt"><span>수강기간:2019.09.01~2019.10.30</span><span>복습기간: 30일</span></p>
    </div>
    <div class="bt">
        <a href="#" class="btns">최근학습보기</a><span>3강</span><span class="t-blue">고,지만</span>

        <div class="f-r">
            <p>진도율<strong>62</strong>%</p>
            <div class="bar-event" data-num="62%">
                <span></span>
            </div>
        </div>
    </div>
</div>


<!--주차 리스트-->
<ul class="newweeks">
    <li id="section-1" class="section main clearfix current current_week" role="region" aria-label="2주차">
        <div class="right side"><img class="icon spacer" width="1" height="1" alt="" aria-hidden="true" src="http://open.jinotech.com:13080/theme/image.php?theme=jinui&amp;component=core&amp;rev=1566956125&amp;image=spacer"></div>
        <div class="content">
            <h3 class="sectionname">
                <a href="#">1강 어/아요, 았/었어요, ㅂ니다</a>

                <!--오른쪽-->
                <div class="f-r">
                    <span class="dt">2019.09.01</span>
                    <span class="btns complete">완료</span>
                </div>
                <!--오른쪽-->
            </h3>
            <div class="section_availability"></div>
            <div class="summary"></div>
            <!-- 학습활동 리스트 -->
            <ul class="section img-text p-0 list-group list-group-dividered list-group-full">
                <li class="activity resource modtype_resource px-30 py-10 list-group-item" id="module-52">
                    <div>
                        <div class="mod-indent-outer">
                            <div class="mod-indent"></div>
                            <div>
                                <div class="activityinstance"><a class="" onclick="" href="#"><img src="http://open.jinotech.com:13080/theme/image.php?theme=oklassedu&amp;component=core&amp;rev=1566956125&amp;image=f%2Fjpeg-24" class="iconlarge activityicon" alt=" " role="presentation">
                                        <span class="instancename">1강 어/아요, 았/었어요, ㅂ니다<span class="accesshide "> 파일</span></span>
                                    </a>
                                </div>
                                <!-- 진도율-->
                                <div class="f-r">
                                    <span class="dt">2019.12.31</span>
                                    <strong class="crs-progress">100%</strong>
                                </div>
                                <!-- 진도율-->
                            </div>
                        </div>
                    </div>
                </li>
                <li class="activity resource modtype_resource px-30 py-10 list-group-item" id="module-52">
                    <div>
                        <div class="mod-indent-outer">
                            <div class="mod-indent"></div>
                            <div>
                                <div class="activityinstance"><a class="" onclick="" href="#"><img src="http://open.jinotech.com:13080/theme/image.php?theme=oklassedu&amp;component=core&amp;rev=1566956125&amp;image=f%2Fjpeg-24" class="iconlarge activityicon" alt=" " role="presentation">
                                        <span class="instancename">1강 어/아요, 았/었어요, ㅂ니다<span class="accesshide "> 파일</span></span>
                                    </a>
                                </div>
                                <div class="f-r">
                                    <span class="dt">2019.12.31</span>
                                    <strong class="crs-progress">100%</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </li>

    <li id="section-1" class="section main clearfix current current_week" role="region" aria-label="1주차">
        <div class="right side"><img class="icon spacer" width="1" height="1" alt="" aria-hidden="true" src="http://open.jinotech.com:13080/theme/image.php?theme=jinui&amp;component=core&amp;rev=1566956125&amp;image=spacer"></div>
        <div class="content">
            <h3 class="sectionname">
                <a href="#">2강 어/아요, 았/었어요, ㅂ니다</a>

                <!--오른쪽-->
                <div class="f-r">
                    <span class="dt">2019.09.01</span>
                    <span class="btns">진행중</span>
                </div>
                <!--오른쪽-->
            </h3>
            <div class="section_availability"></div>
            <div class="summary"></div>
            <!-- 학습활동 리스트 -->
            <ul class="section img-text p-0 list-group list-group-dividered list-group-full">
                <li class="activity resource modtype_resource px-30 py-10 list-group-item" id="module-52">
                    <div>
                        <div class="mod-indent-outer">
                            <div class="mod-indent"></div>
                            <div>
                                <div class="activityinstance"><a class="" onclick="" href="#"><img src="http://open.jinotech.com:13080/theme/image.php?theme=oklassedu&amp;component=core&amp;rev=1566956125&amp;image=f%2Fjpeg-24" class="iconlarge activityicon" alt=" " role="presentation">
                                        <span class="instancename">1강 어/아요, 았/었어요, ㅂ니다<span class="accesshide "> 파일</span></span>
                                    </a>
                                </div>
                                <div class="f-r">
                                    <span class="dt">2019.12.31</span>
                                    <strong class="crs-progress">완료</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="activity resource modtype_resource px-30 py-10 list-group-item" id="module-52">
                    <div>
                        <div class="mod-indent-outer">
                            <div class="mod-indent"></div>
                            <div>
                                <div class="activityinstance"><a class="" onclick="" href="#"><img src="http://open.jinotech.com:13080/theme/image.php?theme=oklassedu&amp;component=core&amp;rev=1566956125&amp;image=f%2Fjpeg-24" class="iconlarge activityicon" alt=" " role="presentation">
                                        <span class="instancename">1강 어/아요, 았/었어요, ㅂ니다<span class="accesshide "> 파일</span></span>
                                    </a>
                                </div>
                                <div class="f-r">
                                    <span class="dt">2019.12.31</span>
                                    <strong class="crs-progress">미완료</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>
<!--주차 리스트-->

<?php
echo $OUTPUT->footer();
?>


