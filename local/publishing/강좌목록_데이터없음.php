<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('강좌목록');

echo $OUTPUT->header();
?>  
<div class="page-nav">
    <p>한국어 강좌</p>
    <p>초급과정</p>
    <div class="sub-mn">
        <span>메뉴아이콘</span>
        <ul>
            <li class="selected">
                <a href="#">한국어 강좌</a>
                <ul>
                    <li class="selected"><a href="#">초급과정</a></li>
                    <li><a href="#">중급과정</a></li>
                    <li><a href="#">고급과정</a></li>
                </ul>
            </li> 
            <li>
                <a href="#">한국어 패키지</a>
                <ul>
                    <li class="selected"><a href="#">초급과정2</a></li>
                    <li><a href="#">중급과정2</a></li>
                    <li><a href="#">고급과정2</a></li>
                </ul>
            </li> 
            <li>
                <a href="#">TOPIK 1,2급</a>
                <ul>
                    <li class="selected"><a href="#">초급과정3</a></li>
                    <li><a href="#">중급과정3</a></li>
                    <li><a href="#">고급과정3</a></li>
                </ul>
            </li> 
            <li class="bt"><a href="#">커리큘럼(로드맵) 보기</a></li>
        </ul>
    </div>
</div>

<div class="no-data">
    <div>등록된 강좌가 없습니다.</div>
</div>

<?php
echo $OUTPUT->footer();
?>


