<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('join02');
$PAGE->navbar->add('회원가입');

echo $OUTPUT->header();
?>  

<div class="mail-end-bx">
    <div>이메일 인증</div>
    <p>이메일 인증이 완료되었습니다.</p>
    <p class="small">로그인 후 서비스를 이용해주세요.</p>
    <a href="#" class="btns point big">로그인</a>
</div>

<?php
echo $OUTPUT->footer();
?>


