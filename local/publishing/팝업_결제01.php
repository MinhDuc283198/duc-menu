<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('결제');

echo $OUTPUT->header();
?>  
<div class="layerpop window">
    <div class="pop-title">
        Checkout
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <ul class="mk-c-tab">
            <li class="on"><a href="#"><span class="nm">1</span>Information</a></li>
            <li><a href="#"><span class="nm">2</span>Payment details</a></li>
            <li><a href="#"><span class="nm">3</span>Review your order</a></li>
        </ul>

        <div class="addr-info style02">
            <div class="rw">
                <p class="tit">Information Bill</p>
                <p class="grp hlf">
                    <span>Title*</span>
                    <input type="text" value="Learn ReactJS" readonly />
                </p>
                <p class="grp hlf">
                    <span>Price*</span>
                    <input type="text" value="15,000" readonly />
                </p>
            </div>
            <div class="rw">
                <span class="gr">Description*</span>
                <div class="bx">
                    ReactJS presents graceful solutions to some of front-end programming’s most persistent issues. It’s fast, scalable, flexible, powerful, and has a robust developer community that’s rapidly growing. There’s never been a better time to learn React.
                </div>
            </div>
            <div class="rw">
                <p class="tit mg-tp">Shipping address</p>
                <p class="grp hlf">
                    <span>Nguyen*</span>
                    <input type="text" value="" />
                </p>
                <p class="grp hlf">
                    <span>Last name *</span>
                    <input type="text" value="Nguyen" />
                </p>
            </div>
            <div class="rw">
                <p class="grp">
                    <span>Address line 1 *</span>
                    <input type="text" value="" />
                </p>
                <p class="grp">
                    <span>Address line 2*</span>
                    <input type="text" value="" />
                </p>
            </div>
            <div class="rw">
                <p class="grp hlf">
                    <span>City *</span>
                    <input type="text" value="" />
                </p>
                <p class="grp hlf">
                    <span>State/Province/Region *</span>
                    <input type="text" value="" />
                </p>
            </div>
            <div class="rw">
                <p class="grp hlf">
                    <span>Zip / Postal code  *</span>
                    <input type="text" value="" />
                </p>
                <p class="grp hlf">
                    <span>Country  *</span>
                    <input type="text" value="" />
                </p>
            </div>

            <p><label class="custom-ck"><input type="checkbox"><span>Use this address for payment details</span></label></p>
        </div>
    </div>
    <div class="btn-area text-center">
        <input type="button" value="NEXT" class="btns point w100" />
    </div>
</div>


<?php
echo $OUTPUT->footer();
?>


