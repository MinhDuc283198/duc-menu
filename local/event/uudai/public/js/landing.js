
// var getUrlParameter = function getUrlParameter(sParam) {
//     var sPageURL = window.location.search.substring(1),
//         sURLVariables = sPageURL.split('&'),
//         sParameterName,
//         i;

//     for (i = 0; i < sURLVariables.length; i++) {
//         sParameterName = sURLVariables[i].split('=');

//         if (sParameterName[0] === sParam) {
//             return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
//         }
//     }
// };
// var langurl = getUrlParameter('lang') ? getUrlParameter('lang'):'vi';
// console.log(langurl);
// var js = document.createElement("script");
// js.type = "text/javascript";
// js.src = `public/js/language/${langurl}.js`;
// $('body').append(js);
// $.each(lang,(key,e)=>{
//     $(`#${key}`).html(e);
// });
var domain = document.domain;
var protocol = window.location.protocol;
if (domain == "127.0.0.1") {
  domain = protocol + "//" + domain + ":8000/";
}
else if (domain == "localhost")
  domain = protocol + '//' + domain + '/landing/public/';
else {
  domain = protocol + "//" + domain + "/local/event/uudai/";
}
var _token = $('meta[name="csrf-token"]').attr('content');
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('document').ready(function(){
  resize_element();
  header_height = $('#header').height();
  same_height($('.each-choose h3'));
  same_height($('.each-choose p'));
  if($(window).width()<992){
    change_topik2();
  }
});
$('#video i').click(function(){
  $('.modal').css('padding-right',0);
});
$('#open_right_menu').click(function(){
  $('#bg-dark-header').show();
  $('#menu-right').show();
});
$('#close_menu_right').click(function(){
  $('#menu-right').hide();
  $('#bg-dark-header').hide();
});
$('#scroll_to_topik2').click(function(){
  $('html, body').animate({
    scrollTop: $("#topik2").offset().top-100
  }, 1000);
});
$('#scroll_to_business').click(function(){
  $('html, body').animate({
    scrollTop: $("#business").offset().top-100
  }, 1000);
});
$('.scrool_to_register').click(function(){
  $('html, body').animate({
    scrollTop: $("#email").offset().top-100
  }, 1000);
});
$('#btn_to_business_learn').click(function(){
  window.open('https://www.masterkorean.vn/local/course/detail.php?id=68', '_blank');
  // window.location.href("https://www.masterkorean.vn/local/course/detail.php?id=68");
});
$('#btn_to_topik2_learn').click(function(){
  window.open('https://www.masterkorean.vn/local/course/detail.php?id=67', '_blank');
  // window.location.replace("https://www.masterkorean.vn/local/course/detail.php?id=67");
});
$("#myModal").on('hidden.bs.modal', function (e) {
  $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
});
$('#btn_register').click(function(){
  $(this).html(`ĐĂNG KÝ <i class="fa fa-spinner fa-spin"></i>`);
  // $(this).attr('disabled',true);
  $('.val_email').each(function(){
    $(this).css('border','1px solid #DDDDDD');
  });
  $('#success_email').hide(300);
  var error = 0;
  $('.val_email').each(function(){
    if($(this).val()== ''){
      $(this).css('border','1px solid red');
      error++;
    }
  });
  if(error>0){
    $(this).html(`ĐĂNG KÝ`);
    $(this).attr('disabled',false);
    $('#success_email').hide(300);
    $('#warn_email').html('Phải điền đầy đủ thông tin');
    $('#warn_email').show(300);
    return;
  }else{
    $('#success_email').hide(300);
    $('.val_email').each(function(){
      $(this).css('border','1px solid #DDDDDD');
    });
    $('#warn_email').hide(300);
  }
  var class_name = $('#class_name').val();
  var teacher = $('#teacher').val();
  var email_send = $('#email_send').val();
  var phone_number = $('#phone_number').val();
  var url = domain+'sendemail';
  console.log(url);
  var data = {
    class_name:class_name,
    teacher:teacher,
    email_send:email_send,
    phone_number:phone_number,
    // "_token":$('meta[name="csrf-token"]').attr('content')
    // "_token":$('#token').val()
  };
  $.ajax({
    url: url,
    type:"POST",
    // async:false,
    data:data
  }).done((data)=>{
    console.log(data);
    if(data.status==0){
      $('#btn_register').html(`ĐĂNG KÝ`);
      $('#btn_register').attr('disabled',false);
      var mes = '';
      data.message.forEach((e,key)=>{
        if(key !=0){
          mes+='<br>';
        }
        mes+= e;
      });
      $('#success_email').hide(300);
      $('#warn_email').html(mes);
      $('#warn_email').show(300);
      return;
    }
    if(data.status==1){
      $('#btn_register').html(`ĐĂNG KÝ`);
      $('#btn_register').attr('disabled',false);
      $('#warn_email').hide(300);
      $('#success_email').html(data.message);
      $('#success_email').show(300);
    }
  });
});
$(window).resize(function(){
  resize_element();
  same_height($('.each-choose h3'));
  same_height($('.each-choose p'));
  if($(window).width()<992){
    change_topik2();
  }else{
    recove_topik2();
  }
});
header_height = $('#header').height();
$(window).scroll(function(){
  if ($(this).scrollTop() > 0) {
    $('#header').addClass('fixed-top');
    $('#header2').addClass('fixed-top');
    $('#slide-header').css('margin-top',header_height);
  } else {
    $('#header').removeClass('fixed-top');
    $('#header2').removeClass('fixed-top');
    $('#slide-header').css('margin-top',0);
  }
  $('.scroll-animations .animated').each(function() {
    if (isScrolledIntoView(this) === true) {
      $(this).css('opacity',1);
      $(this).addClass($(this).attr('animate'));
    }
  });
});
same_height = (element) => {
  var height = 0;
  element.each(function(){
    if($(this).height()>height){
      height = $(this).height();
    }
  });
  element.each(function(){
    $(this).height(height);
  });
}
change_topik2 = ()=>{
  var parent = $('#topik2 .img-teacher').closest('.col-lg-6');
  var place = $('#topik2').find('.row').first();
  // $(parent.clone()).prependTo($('#topik2').first('.row'));
  place.prepend(parent.clone());
  parent.remove();
}
recove_topik2 = ()=>{
  var parent = $('#topik2 .img-teacher').closest('.col-lg-6');
  var place = $('#topik2').find('.row').first();
  // $(parent.clone()).prependTo($('#topik2').first('.row'));
  place.append(parent.clone());
  parent.remove();
}
resize_element = () =>{
  if($(window).width()<1000){
    $('#video').find('iframe').attr('height',$(window).width()*56/100);
    // $('#email').find('.col-md-6').css('padding-left','');
    // $('#about-text').css('padding-left','0');
    // alert($(window).width());
    // $('#email').find('img').css('margin-left','0');
  }
}
function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top;
  // var elemBottom = $(elem).offset().top;
  var elemBottom = elemTop + ($(elem).height()/2);

  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}
