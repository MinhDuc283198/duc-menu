<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href={{asset("public/bootstrap/css/bootstrap.min.css")}} rel="stylesheet">
    <link href={{asset("public/fontawesome/css/all.css")}} rel="stylesheet">
    <link href={{asset("public/css/landing.css")}} rel="stylesheet">
    <link href={{asset("public/css/animate.min.css")}} rel="stylesheet">
    <style>
        table tr {
            counter-increment: row-num;
        }
        table tr td:first-child::before {
            content: counter(row-num);
        }
    </style>
</head>
<body>
    <div id="header2">
        <div class="d-flex justify-content-between align-items-center">
            <div id="bg-dark-header"></div>
            <div id="header2-left">
                <a href=""><i class="fas fa-lock"></i></a>
            </div>
            <div id="header2-center" class="text-center">
                <a href=""><img src="{{asset('public/images/logo_beta_m.png')}}" alt=""></a>
            </div>
            <div id="header2-right">
                <button id="open_right_menu"><i class="fas fa-bars"></i></button>
            </div>
            <div id="menu-right">
                <div class="text-right" style="padding:10px 15px 0 0">
                    <i class="fas fa-times" id="close_menu_right"></i>
                </div>
                <div id="menu-right-login" class="text-center">
                    <p>Khả dụng sau khi đăng nhập</p>
                    <a href="https://www.masterkorean.vn/login/index.php" target="_blank">Đăng nhập/Đăng ký</a>
                </div>
                <div id="menu-right-link">
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/course/main.php" target="_blank">Khóa học</a></div>
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/book/list.php" target="_blank">Giáo trình</a></div>
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/jinoboard/community.php" target="_blank">Cộng đồng</a></div>
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/instructor/list.php" target="_blank">Giảng viên</a></div>
                </div>
                <div id="menu-right-on-bottom">
                    <div id="menu-right-link-mkj">
                        <div id="link-mkj-logo">
                            <a href="https://job.masterkorean.vn/local/job/" target="_blank"><img src="{{asset('public/images/logo_job.png')}}" alt="" srcset=""></a>
                        </div>
                        <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=1" target="_blank">Điều kiện</a> &nbsp; | &nbsp;
                        <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=0" target="_blank">Chính sách bảo mật</a>
                        <p>Copyright © VISANG Education Group Vietnam Company</p>
                    </div>
                    <div id="menu-right-service-register">
                        <p class="text-center" style="border-right:1px solid #d6d6d6"><a href="https://www.masterkorean.vn/local/jinoboard/index.php?type=1&b_t=custom"  target="_blank">Dịch vụ khách hàng</a></p>
                        <p class="text-center"><a href="https://www.masterkorean.vn/login/index.php" target="_blank">Đăng nhập/Đăng ký</a></p>
                    </div>
                    <div id="menu-right-ask-help" class="row">
                        <div class="each-ask-help col-6 d-flex justify-content-center align-items-center">
                            <i class="far fa-comment-dots"></i>
                            <a href="https://www.masterkorean.vn/local/jinoboard/write.php?type=2&b_t=custom" class="text-center" target="_blank">Hỏi đáp<br>1:1</a>
                        </div>
                        <div class="each-ask-help col-6 d-flex align-items-center justify-content-center">
                            <i class="fab fa-facebook-messenger"></i>
                            <a href="https://www.messenger.com/t/111453797011701" class="text-center" target="_blank">Tư vấn<br>Facebook</a>
                        </div>
                    </div>
                    <div id="menu-right-bottom">
                        <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" style="margin-right:3px; padding:3px 8px" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" style="padding:3px 6px" target="_blank"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="header">
        <div id="top-line-header">
            <div class="float-right" id="top-MKJ">
                <ul>
                    <li class="d-flex align-items-center"><a href="https://job.masterkorean.vn/" target="_blank"></a></li>
                    <li><a href="https://www.masterkorean.vn/local/jinoboard/index.php?type=1&b_t=custom" target="_blank">Dịch vụ khách hàng</a></li>
                </ul>
            </div>
        </div>
        <div id="menu-search-header-frame">
            <div id="menu-search-header">
                <div id="menu-search-logo">
                    <img src="{{asset('public/images/logo_beta.png')}}"/>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/course/main.php" target="_blank">Khóa học</a>
                    <ul>
                        <li>
                            <a>Tiếng Hàn</a>
                            <ul>
                                <li><a href="https://www.masterkorean.vn/local/course/list.php?id=10" target="_blank">Tiếng Hàn Visang</a></li>
                            </ul>
                        </li>
                        <li>
                            <a>TOPIK</a>
                            <ul>
                                <li><a href="https://www.masterkorean.vn/local/course/list.php?id=19" target="_blank">Luyện đề giải thi</a></li>
                            </ul>
                        </li>
                        <li>
                            <a>COMBO</a>
                            <ul>
                                <li><a href="https://www.masterkorean.vn/local/course/list.php?id=22" target="_blank">Gói khóa học</a></li>
                            </ul>
                        </li>
                        <li><a href="https://www.masterkorean.vn/local/course/list.php?id=5" target="_blank">Tiếng Hàn Business</a></li>
                    </ul>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/book/list.php" target="_blank">Giáo trình</a>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/jinoboard/community.php" target="_blank">Cộng đồng</a>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/instructor/list.php" target="_blank">Giảng viên</a>
                </div>
                <div class="float-right">
                    <div id="menu-search-register">
                        <a href="https://www.masterkorean.vn/login/index.php" class="d-flex align-items-center" target="_blank">Đăng ký/Đăng nhập</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="max1320" style="margin:20px auto">
        <table class="table table-bordered">
            <thead>
                <tr class="text-center">
                    <th>STT</th>
                    <th>Trường</th>
                    <th>Giáo viên</th>
                    <th>Email</th>
                    <th>Điện thoại</th>
                    <th>Thời gian đăng ký</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(count($info)==0){
                        echo '<tr><td colspan="6" class="text-center">Chưa có thông tin đăng ký</td></tr>';
                    }
                    foreach ($info as $key => $value) {
                        echo '<tr>
                                <td></td>
                                <td>'.$value['class_name'].'</td>
                                <td>'.$value['teacher'].'</td>
                                <td>'.$key.'</td>
                                <td>'.$value['phone_number'].'</td>
                                <td>'.$value['time'].'</td>
                            </tr>';
                    }
                ?>
            </tbody>
        </table>
    </div>
    {{-- <div id="footer">
        <div class="max1320">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{asset('public/images/ft_logo.png')}}" alt="" srcset="">
                </div>
                <div class="col-md-8">
                    <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=1&category=1" class="each-link" target="_blank">Quy chế hoạt động</a>
                    <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=2&category=2" class="each-link" target="_blank">Chính sách giải quyết tranh chấp</a>
                    <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=0&category=2" class="each-link" target="_blank">Chính sách bảo mật</a>
                    <a href="https://www.masterkorean.vn/local/jinoboard/index.php?type=1" class="each-link" target="_blank">Dịch vụ khách hàng</a>
                    <p>Tên công ty: Công ty TNHH Giáo dục Visang Việt Nam<br>
                        Trụ sở chính: Tầng 2, FLC Landmark Tower, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, thành phố Hà Nội, Việt Nam<br>
                        MST: 0109066143 do Sở KH & ĐT thành phố Hà Nội cấp ngày 14/01/2020<br>
                        Người đại diện: Mr. Yoon Tae In<br>
                        Điện thoại: 0243-6886-333 | E-mail: visang@masterkorean.vn<br>
                        Copyright © VISANG Education Group Vietnam Company</p>
                </div>
                <div class="col-md-2" style="padding:0">
                    <div class="text-right" id="footer-right">
                        <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" style="margin-right:3px; padding:4px 10px" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank"><i class="fab fa-youtube"></i></a>
                        <a href="https://job.masterkorean.vn/" style="padding:4px 20px" target="_blank"><img src="{{asset('public/images/logo_btn_footer.png')}}" alt="" srcset=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <script src={{asset("public/js/jquery.min.js")}}></script>
    <script src={{asset("public/bootstrap/js/bootstrap.min.js")}}></script>
    {{-- <script src={{asset("public/fontawesome/js/all.js")}}></script> --}}
    <script src={{asset("public/js/landing.js")}}></script>
</body>
</html>