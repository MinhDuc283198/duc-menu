<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href={{asset("public/bootstrap/css/bootstrap.min.css")}} rel="stylesheet">
    <link href={{asset("public/fontawesome/css/all.css")}} rel="stylesheet">
    <link href={{asset("public/css/landing.css")}} rel="stylesheet">
    <link href={{asset("public/css/animate.min.css")}} rel="stylesheet">
</head>
<body>
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div id="header2">
        <div class="d-flex justify-content-between align-items-center">
            <div id="bg-dark-header"></div>
            <div id="header2-left">
                <a href=""><i class="fas fa-lock"></i></a>
            </div>
            <div id="header2-center" class="text-center">
                <a href=""><img src="{{asset('public/images/logo_beta_m.png')}}" alt=""></a>
            </div>
            <div id="header2-right">
                <button id="open_right_menu"><i class="fas fa-bars"></i></button>
            </div>
            <div id="menu-right">
                <div class="text-right" style="padding:10px 15px 0 0">
                    <i class="fas fa-times" id="close_menu_right"></i>
                </div>
                <div id="menu-right-login" class="text-center">
                    <p>Khả dụng sau khi đăng nhập</p>
                    <a href="https://www.masterkorean.vn/login/index.php" target="_blank">Đăng nhập/Đăng ký</a>
                </div>
                <div id="menu-right-link">
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/course/main.php" target="_blank">Khóa học</a></div>
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/book/list.php" target="_blank">Giáo trình</a></div>
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/jinoboard/community.php" target="_blank">Cộng đồng</a></div>
                    <div class="each_link"><a href="https://www.masterkorean.vn/local/instructor/list.php" target="_blank">Giảng viên</a></div>
                </div>
                <div id="menu-right-on-bottom">
                    <div id="menu-right-link-mkj">
                        <div id="link-mkj-logo">
                            <a href="https://job.masterkorean.vn/local/job/" target="_blank"><img src="{{asset('public/images/logo_job.png')}}" alt="" srcset=""></a>
                        </div>
                        <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=1" target="_blank">Điều kiện</a> &nbsp; | &nbsp;
                        <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=0" target="_blank">Chính sách bảo mật</a>
                        <p>Copyright © VISANG Education Group Vietnam Company</p>
                    </div>
                    <div id="menu-right-service-register">
                        <p class="text-center" style="border-right:1px solid #d6d6d6"><a href="https://www.masterkorean.vn/local/jinoboard/index.php?type=1&b_t=custom"  target="_blank">Dịch vụ khách hàng</a></p>
                        <p class="text-center"><a href="https://www.masterkorean.vn/login/index.php" target="_blank">Đăng nhập/Đăng ký</a></p>
                    </div>
                    <div id="menu-right-ask-help" class="row">
                        <div class="each-ask-help col-6 d-flex justify-content-center align-items-center">
                            <i class="far fa-comment-dots"></i>
                            <a href="https://www.masterkorean.vn/local/jinoboard/write.php?type=2&b_t=custom" class="text-center" target="_blank">Hỏi đáp<br>1:1</a>
                        </div>
                        <div class="each-ask-help col-6 d-flex align-items-center justify-content-center">
                            <i class="fab fa-facebook-messenger"></i>
                            <a href="https://www.messenger.com/t/111453797011701" class="text-center" target="_blank">Tư vấn<br>Facebook</a>
                        </div>
                    </div>
                    <div id="menu-right-bottom">
                        <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" style="margin-right:3px; padding:3px 8px" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" style="padding:3px 6px" target="_blank"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="header">
        <div id="top-line-header">
            <div class="float-right" id="top-MKJ">
                <ul>
                    <li class="d-flex align-items-center"><a href="https://job.masterkorean.vn/" target="_blank"></a></li>
                    <li><a href="https://www.masterkorean.vn/local/jinoboard/index.php?type=1&b_t=custom" target="_blank">Dịch vụ khách hàng</a></li>
                </ul>
            </div>
        </div>
        <div id="menu-search-header-frame">
            <div id="menu-search-header">
                <div id="menu-search-logo">
                    <img src="{{asset('public/images/logo_beta.png')}}"/>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/course/main.php" target="_blank">Khóa học</a>
                    <ul>
                        <li>
                            <a>Tiếng Hàn</a>
                            <ul>
                                <li><a href="https://www.masterkorean.vn/local/course/list.php?id=10" target="_blank">Tiếng Hàn Visang</a></li>
                            </ul>
                        </li>
                        <li>
                            <a>TOPIK</a>
                            <ul>
                                <li><a href="https://www.masterkorean.vn/local/course/list.php?id=19" target="_blank">Luyện đề giải thi</a></li>
                            </ul>
                        </li>
                        <li>
                            <a>COMBO</a>
                            <ul>
                                <li><a href="https://www.masterkorean.vn/local/course/list.php?id=22" target="_blank">Gói khóa học</a></li>
                            </ul>
                        </li>
                        <li><a href="https://www.masterkorean.vn/local/course/list.php?id=5" target="_blank">Tiếng Hàn Business</a></li>
                    </ul>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/book/list.php" target="_blank">Giáo trình</a>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/jinoboard/community.php" target="_blank">Cộng đồng</a>
                </div>
                <div class="menu-search-each d-flex  align-items-center">
                    <a href="https://www.masterkorean.vn/local/instructor/list.php" target="_blank">Giảng viên</a>
                </div>
                <div class="float-right">
                    <div id="menu-search-register">
                        <a href="https://www.masterkorean.vn/login/index.php" class="d-flex align-items-center" target="_blank">Đăng ký/Đăng nhập</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="slide-header" class="w-100">
        <img src="{{asset('public/images/banner2.png')}}" />
    </div>
    <div id="middle" class="scroll-animations ">
        <div class="max1320">
            <div class="row " animate="" id="about">
                <div class="col-lg-4 text-center">
                    <img src="{{asset('public/images/image 25.png')}}" alt="" srcset="">
                </div>
                <div class="col-lg-8">
                    <div id="about-text" class="d-flex align-items-center">
                        <div>
                            <h2 class="clearfix w-100">Hỗ trợ đợt dịch COVID-19</h2>
                            <p>Nhằm chia sẻ khó khăn với người học Tiếng Hàn nói riêng và ngành giáo dục nói chung trong mùa dịch COVID-19, và tạo điều kiện cho sinh viên năm cuối tại các trường giảng dạy Tiếng Hàn có thể bổ trợ kiến thức, Master Korean cung cấp miễn phí toàn bộ khóa học:</p>
                            <button id="scroll_to_topik2">Luyện thi TOPIK II</button>
                            <button id="scroll_to_business">Tiếng Hàn Business</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="max1320">
            <div id="why-choose">
                <h2 class="animated" animate="zoomIn">VÌ SAO BẠN NÊN CHỌN <br> MASTER KOREAN?</h2>
                <div class="row row-eq-height">
                    <div class="col-lg-3 animated" animate="slideInUp">
                        <div class="each-choose">
                            <img src="{{asset('public/images/image 15.png')}}" alt="" class="">
                            <h3>Thương hiệu uy tín</h3>
                            <p>Đến từ VISANG - Tập đoàn giáo dục hàng đầu Hàn Quốc</p>
                        </div>
                    </div>
                    <div class="col-lg-3 animated " animate="slideInLeft">
                        <div class="each-choose">
                            <img src="{{asset('public/images/image 16.png')}}" alt="" class="">
                            <h3>Cách học dễ dàng nhất</h3>
                            <p>Chỉ cần một thiết bị có kết nối internet, bạn có thể học Tiếng Hàn với Master Korean ở mọi lúc, mọi nơi.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 animated " animate="slideInRight">
                        <div class="each-choose">
                            <img src="{{asset('public/images/image 17.png')}}" alt="" class="">
                            <h3>Dịch vụ hỗ trợ tận tình nhất</h3>
                            <p>Được tư vấn trực tiếp 1:1 với giảng viên lâu năm của Master Korean.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 animated " animate="slideInDown">
                        <div class="each-choose">
                            <img src="{{asset('public/images/image 18.png')}}" alt="" class="">
                            <h3>Nhanh chóng tìm được việc làm</h3>
                            <p>Master Korean liên kết trực tiếp với Master Korean Jobs, giúp bạn nhận đề xuất công việc chất lượng từ các công ty Hàn Quốc.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="topik2">
            <div class="max1320">
                <h2 class="animated" animate="slideInLeft">LUYỆN THI TOPIK II</h2>
                <div class="row">
                    <div class="col-lg-6 d-flex align-items-center">
                        <div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="flipInX">
                                        <i class="fas fa-chart-pie" style="font-size:56.4px"></i>
                                        <h3>Phân tích</h3>
                                        <hr>
                                        <p>Phân tích chi tiết các dạng đề thi Topik</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="flipInY">
                                        <i class="fas fa-headphones" style="font-size:56.4px"></i>
                                        <h3>Hướng dẫn</h3>
                                        <hr>
                                        <p>Hướng dẫn giải đề thi theo từng phần Nghe - Đọc - Viết</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="jello" style="margin:20px 0 00px">
                                        <i class="fas fa-chalkboard-teacher" style="font-size:56.4px"></i>
                                        <h3>Chuyên gia</h3>
                                        <hr>
                                        <p>Học với chuyên gia TOPIK người Việt của Master Korean</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="tada" style="margin:20px 0 00px">
                                        <i class="fas fa-check" style="font-size:56.4px"></i>
                                        <h3>Chia sẻ mẹo</h3>
                                        <hr>
                                        <p>Nắm bắt các mẹo và chiến lược giải đề để đạt điểm số cao trong thời gian ngắn nhất</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="bounceInLeft">
                                        <button class="btn-page scrool_to_register">đăng ký ngay</button>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="bounceInRight">
                                        <button class="btn-page" id="btn_to_topik2_learn">tìm hiểu khóa học</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6" style="float:right !important">
                        <div class="img-teacher text-center animated" animate="bounceIn">
                            <img src="{{asset('public/images/teach1.png')}}" alt="">
                            <div class="circle-teach"></div>
                            <a href="https://www.masterkorean.vn/local/instructor/detail.php?id=32" target="_blank">Giảng viên<br>Ninh Thị Thúy<hr></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="business">
            <div class="max1320">
                <div class="animated" animate="bounceInRight">
                    <h2>TIẾNG HÀN BUSINESS</h2>
                    <p id="title_business">(Giao tiếp trong doanh nghiệp)</p>
                </div>
                <div class="row">
                    <div class="col-lg-6" style="float:right !important">
                        <div class="img-teacher text-center animated" animate="bounceIn">
                            <img src="{{asset('public/images/teach2.png')}}" alt="">
                            <div class="circle-teach"></div>
                            <a href="https://www.masterkorean.vn/local/instructor/detail.php?id=147" target="_blank">Giảng viên<br>Cheon Seong Ok<hr></a>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-items-center">
                        <div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="fadeInLeft">
                                        <i class="fas fa-film" style="font-size:56.4px"></i>
                                        <h3>Đoạn phim ngắn</h3>
                                        <hr>
                                        <p>Các đoạn phim ngắn giúp học viên gián tiếp trải nghiệm những tình huống thực tế tại công ty Hàn Quốc</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="fadeInRight">
                                        <i class="fas fa-address-card" style="font-size:56.4px"></i>
                                        <h3>Thuận lợi xin việc</h3>
                                        <hr>
                                        <p>Thuận lợi khi xin việc vào doanh nghiệp Hàn Quốc từ vòng phỏng vấn, cho đến khi làm việc chính thức</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="each-part-teach text-center animated" animate="fadeIn" style="margin:20px 0 00px">
                                        <i class="fas fa-comments" style="font-size:56.4px"></i>
                                        <h3>Giao tiếp trôi chảy</h3>
                                        <hr>
                                        <p>Dễ dàng giao tiếp trôi chảy với đồng nghiệp và cấp trên người Hàn, có thêm cơ hội thăng tiến và nâng cao thu nhập</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="each-part-teach d-flex align-items-center text-center" style="margin:20px 0 0px; height:100%">
                                        <div>
                                            <button class="btn-page scrool_to_register animated" animate="bounceInDown">đăng ký ngay</button>
                                            <button class="btn-page animated" animate="bounceInUp" id="btn_to_business_learn">tìm hiểu khóa học</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="video" class=" animated" animate="zoomIn" style="animation-fill-mode:unset;">
            <div class="modal fade bd-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="padding-right:0 !important">
                <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width:1000px">
                    <div class="modal-content" style="background:none">
                        <iframe style="width:100%" height="560" src="https://www.youtube.com/embed/uZ-rVhrlTb4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div style="height:100%; min-height:330px; overflow:hidden; text-align:center">
                <img src="{{asset('public/images/bannervid.png')}}" alt="">
            </div>
            <div id="black-bg"></div>
            <div id="fron-vid" class="d-flex align-items-center text-center" style="height:100%">
                <div class="w-100">
                    <i id="icon-vid" class="fas fa-play" data-toggle="modal" data-target=".bd-example-modal-sm"></i>
                    <h2>giới thiệu<br> khóa luyện thi topik ii & tiếng hàn business</h2>
                    <p>Master Korean | Master Your Job</p>
                </div>
            </div>
        </div>
        <div id="email" class="max1320">
            <div class="row">
                <div class="col-md-6 text-center animated" animate="bounceInRight">
                    <img src="{{asset('public/images/image 23.png')}}" alt="">
                </div>
                <div class="col-md-6 animated" animate="bounceInLeft" style="padding-left:15px">
                    <h2>Chỉ dành cho giảng viên* các trường Đại học/ Cao đẳng giảng dạy chương trình Tiếng Hàn</h2>
                    <p>Thời gian: từ 01/04 đến 31/08/2020</p>
                    <input type="text" name="" id="class_name" class="val_email" placeholder="Tên trường(*)">
                    <input type="text" name="" id="teacher" class="val_email" placeholder="Giảng viên phụ trách(*)">
                    <input type="text" name="" id="email_send" class="val_email" placeholder="Email(*)">
                    <input type="text" name="" id="phone_number" class="val_email" placeholder="Số điện thoại(*)">
                    <div class="clearfix"></div>
                    <div class="alert alert-primary" id="success_email" style="display:none;font-size:0.8em; width:80%"></div>
                    <div class="alert alert-danger" id="warn_email" style="display:none;font-size:0.8em; width:80%"></div>
                    <div class="clearfix"></div>
                    <button class="btn-page" id="btn_register">ĐĂNG KÝ</button>
                    <div class="clearfix"></div>
                    <span>(*Master Korean sẽ liên hệ trực tiếp với giảng viên khoa để tiến hành triển khai chương trình học miễn phí)</span>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="max1320">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{asset('public/images/ft_logo.png')}}" alt="" srcset="">
                </div>
                <div class="col-md-8">
                    <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=1&category=1" class="each-link" target="_blank">Quy chế hoạt động</a>
                    <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=2&category=2" class="each-link" target="_blank">Chính sách giải quyết tranh chấp</a>
                    <a href="https://www.masterkorean.vn/local/management/term/personalinfo.php?fbstatus=0&category=2" class="each-link" target="_blank">Chính sách bảo mật</a>
                    <a href="https://www.masterkorean.vn/local/jinoboard/index.php?type=1" class="each-link" target="_blank">Dịch vụ khách hàng</a>
                    <p>Tên công ty: Công ty TNHH Giáo dục Visang Việt Nam<br>
                        Trụ sở chính: Tầng 2, FLC Landmark Tower, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, thành phố Hà Nội, Việt Nam<br>
                        MST: 0109066143 do Sở KH & ĐT thành phố Hà Nội cấp ngày 14/01/2020<br>
                        Người đại diện: Mr. Yoon Tae In<br>
                        Điện thoại: 0243-6886-333 | E-mail: visang@masterkorean.vn<br>
                        Copyright © VISANG Education Group Vietnam Company</p>
                </div>
                <div class="col-md-2" style="padding:0">
                    <div class="text-right" id="footer-right">
                        <a href="https://www.facebook.com/Masterkorean_vietnam-111453797011701/" style="margin-right:3px; padding:4px 10px" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.youtube.com/channel/UC-MmFW6JlEIPj4BztZ-uG9g" target="_blank"><i class="fab fa-youtube"></i></a>
                        <a href="https://job.masterkorean.vn/" style="padding:4px 20px" target="_blank"><img src="{{asset('public/images/logo_btn_footer.png')}}" alt="" srcset=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src={{asset("public/js/jquery.min.js")}}></script>
    <script src={{asset("public/bootstrap/js/bootstrap.min.js")}}></script>
    {{-- <script src={{asset("public/fontawesome/js/all.js")}}></script> --}}
    <script src={{asset("public/js/landing.js")}}></script>
</body>
</html>