<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MasterKorean Email</title>
    <style>
        table{
            /* margin:0 auto; */
            /* border: 1px solid #ddd; */
        }
         td,  th {
            /* border: 1px solid #ddd; */
            padding: 8px;
            min-width: 150px;
            /* text-align: center */
        }
    </style>
</head>
<body>
    <h2 >Thông tin đăng ký Event MasterKorean</h2>
    {{-- <table id="customer">
        <thead>
            <tr>
                <th>Trường</th>
                <th>Giáo viên</th>
                <th>Email</th>
                <th>Số điện thoại</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$class}}</td>
                <td>{{$teacher}}</td>
                <td>{{$email}}</td>
                <td>{{$phone}}</td>
            </tr>
        </tbody>
    </table> --}}
    <table>
        <tr>
            <td>Trường</td>
            <td><b>{{$class}}</b></td>
        </tr>
        <tr>
            <td>Giáo viên</td>
            <td><b>{{$teacher}}</b></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><b>{{$email}}</b></td>
        </tr>
        <tr>
            <td>Số điện thoại</td>
            <td><b>{{$phone}}</b></td>
        </tr>
    </table>
</body>
</html>