<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Validator;
use Mail;

class Email extends Controller
{
    function send_email(Request $rq){
        $validation = Validator::make(
            $rq->all(),
            [
                'class_name' => 'required',
                'teacher'=>'required',
                'email_send'=>'required|email',
                'phone_number'=>'required|numeric|min:10',
            ],
        
            [
                'required' => ':attribute không được để trống',
                'email' => 'Email Không đúng định dạng',
                'numeric' => ':attribute phải là dạng số',
                'min' => ':attribute tối thiểu :min ký tự',
            ],
        
            [
                'class_name' => 'Tên trường',
                'teacher'=> 'Giảng viên phụ trách',
                'email_send'=> 'Email',
                'phone_number'=> 'Số điện thoại',
            ]
        );
        if($validation->fails()){
            return response()->json([
                "status"=>0,
                'message'  => $validation->errors()->all(),
            ]);
        }
        // $file = fopen("register.txt",'w');
        try {
            $all_info =  $rq->all();
            $all_info['time'] = date('d/m/Y -- H:i:s',time());
            $email = $all_info['email_send'];
            unset($all_info['email_send']);
            // $info_old = file_get_contents("public/register.txt",true);
            $info_old = file_get_contents("https://www.masterkorean.vn/local/event/uudai/public/register.txt",true);
            $info_old = json_decode($info_old,true);
            if(array_key_exists($email,$info_old)){
                return response()->json(['status'=>0,'message'=>['Email đã đăng ký']]);
            }
            $info_old[$email] = $all_info;
            $file = fopen("public/register.txt",'w');
            $info_new = json_encode($info_old);
            fwrite($file,$info_new);
            fclose($file);
            return response()->json(['status'=>1,'message'=>'Đăng ký thông tin thành công']);
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
            return response()->json(['status'=>0,'message'=>['Đăng ký thông tin thất bại'],'code'=>$th->getMessage()]);
        }
        
        // array_push($rq->all(),$info_old);
        // $info_old = fread($file);
        // echo '<pre>';
        // var_dump(json_decode(file_get_contents("register.txt",true),true));
        // echo '</pre>';
        // die();
        // $class_name = $rq->class_name;
        // $teacher = $rq->teacher;
        // $email_send = $rq->email_send;
        // $phone_number = $rq->phone_number;
        // try {
        //     Mail::send('email',['class'=>$class_name,'teacher'=>$teacher,'email'=>$email_send,'phone'=>$phone_number],function($message){
        //         $message->to('anhlk@masterkorean.vn','Khac Anh')->subject('Thông tin đăng ký từ LandingPage');
        //     });
        //     return response()->json(['status'=>1,'message'=>'Đăng ký thông tin thành công']);
        // } catch (\Exception $th) {
        //     Log::info($th->getMessage());
        //     return response()->json(['status'=>0,'message'=>['Đăng ký thông tin thất bại']]);
        // }
    }
    function view_info(){
        $info = file_get_contents("public/register.txt",true);
        $info = json_decode($info,true);
        return view('get_register',['info'=>$info]);
    }
}