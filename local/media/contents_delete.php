<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";
require_once $CFG->dirroot . '/lib/form/filemanager.php';

require_login();

$id = required_param('id', PARAM_INT);

$auth = local_media_userauth();

if(!$auth){
    redirect($CFG->wwwroot.'/local/media/list.php');
    die();
}

if(!$media = $DB->get_record('lmsmedia_contents',array('id'=>$id))){
    redirect($CFG->wwwroot.'/local/media/list.php');
    die();
}

$contentmanage = local_media_manage_check($auth, $media);

if($contentmanage == 1){
    $result = local_media_delete_contents($id);
}else{
    $result = 0;
}
    
redirect('list.php');



