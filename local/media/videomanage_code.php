<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('search');
echo $OUTPUT->header();
?>  

<div id="contents">
            <div class="bg topline">
                <div class="group">
                     <div class="sub_contents full padding margin">
                        <!-- 타이틀-->
                         <div class="blocks no_padding ">
                             <h3 class="title line_style">
                                <p class="left button_right">
                                    1차시 LG디스플레이 동영상01
                                </p>
                                <p class="right">
                                    <input type="button" value="이전" title="이전" class="button_style02" />
                                    <input type="button" value="취소" title="이전" class="button_cancel" />
                                </p>
                            </h3>
                        </div>
                        <!-- 타이틀-->
                       
                      
                     
                     <!--내용-->
                     <div id="" class="video_area gray info">
                        <span class="loading">동영상 변환중입니다.</span>
                     </div>
                     
                     <div class="video_info sub">
                         <h3 class="page_title">동영상 정보</h3>
                         <div class="filename">
                             <p>동영상 파일명</p>
                             <p class="name">LG디스플레이 동영상01.mp4</p>
                         </div>
                          <p class="icontext">
                            <span>등록일 : 2016-11-01 10:00</span>
                         </p> 
                          <p class="icontext">
                            <span>등록자 : 홍길동(사번)</span>
                         </p>   
                          <p class="icontext">
                            <span>재생시간 : 14:06</span>
                         </p>  
                         <p class="icontext">
                            <span>조회수 : 206</span>
                         </p>
                         <p class="icontext">
                            <span>좋아요 : 78%</span>
                         </p>
                        
                         <p class="icontext">
                            <span>구분 : 디튜브</span>
                         </p> 
                          <p class="icontext">
                            <span>사용여부 : 사용</span>
                         </p>      
                     </div>
                     
                     <div class="file_info">
                      <ul class="tab">
                          <li><a href="#">기본정보</a></li>
                          <li><a href="#">추가정보</a></li>
                          <li><a href="#">썸네일</a></li>
                          <li><a href="#">자막</a></li>
                          <li class="on"><a href="#">Embed code</a></li>
                      </ul>

                      <div class="caption_area">
                          코드영역
                      </div>
                     </div>
                     <!--내용-->
                      
                        
                        
                    </div> 
                </div>
            </div>
        </div>  
       


 <?php
    echo $OUTPUT->footer();
 ?>


