<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$mediaid = optional_param('mediaid', 0, PARAM_INT);
$filename = optional_param('filename', '', PARAM_RAW);

$context = context_system::instance();
require_login();

$thumbnail = new stdClass();
$thumbnail->mediaid = $mediaid;
$thumbnail->filepath = '/lcmsdata/media/' . $mediaid . '/thumb';
$thumbnail->filename = $filename;
$thumbnail->isused = 0;

local_media_add_thumbnail($thumbnail);
?>


