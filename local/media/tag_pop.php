<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();
?>  
<div id="pop_wrap" class="autoheight">
        <div class="pop_title">
            <h2>선택된 분류 tag</h2>
            <p class="popclose"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></p>
        </div>
        
        <div id="contents">
                    <div class="filter ">
                        <!--<span class="title">선택된 분류 tag</span>-->
                        <span class="button">Excel <span class="del"></span></span>
                        <span class="button">경영 일반 <span class="del"></span></span>
                    </div>
                    
                     <div class="explain marginTop pink">
                            분류 Tag는 분류검색에 활용되며, 하나의 과정에 여러개의  분류를 지정할 수 있습니다.<br />
                           Matching이 되는 분류 Tag가 없을 경우에는 시스템 담당자에게 연락주시기 바랍니다.
                    </div>

                        <div class="">
                            <div class="listgroup">
                                <div class="list list3 paddingleft">
                                    <h3>경영지원</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check02" />
                                           <label for="check02">인사, 총무, HRD</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check03" />
                                           <label for="check03">법무, 회계, 세무, 원가</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>리더십</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">사람관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">업무관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">자기관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">조직관리</label>
                                       </li>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">리더십일반</label>
                                       </li>
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>영업마케팅</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>직무공통</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                 <div class="list list3 paddingleft">
                                    <h3>경영지원</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check02" />
                                           <label for="check02">인사, 총무, HRD</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check03" />
                                           <label for="check03">법무, 회계, 세무, 원가</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>리더십</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">사람관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">업무관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">자기관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">조직관리</label>
                                       </li>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">리더십일반</label>
                                       </li>
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>영업마케팅</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>직무공통</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                 <div class="list list3 paddingleft">
                                    <h3>경영지원</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check02" />
                                           <label for="check02">인사, 총무, HRD</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check03" />
                                           <label for="check03">법무, 회계, 세무, 원가</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>리더십</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">사람관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">업무관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">자기관리</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">조직관리</label>
                                       </li>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">리더십일반</label>
                                       </li>
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>R &amp; D</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">LCD 기구</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>영업마케팅</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                <div class="list list3 paddingleft">
                                    <h3>직무공통</h3>
                                    <ul>
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                       <li>
                                           <input type="checkbox" id="check01" />
                                           <label for="check01">경영, 전략, 기획</label>
                                       </li> 
                                    </ul>  
                                </div>
                                
                            </div>
                        </div>
        </div>
        
        
    </div>

 <?php
    echo $OUTPUT->footer();
 ?>


