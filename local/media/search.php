<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();
?>

<script src="./js/youtube/search.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/client.js?onload=onClientLoad" type="text/javascript"></script>


 <div id="pop_wrap">
        <div class="pop_title">
            <h2>Youtube</h2>
            <p class="popclose"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></p>
        </div>

        <div id="contents" class="sub_contents">
            <div class="search_style01">
                <input type="text" id="query"  title="search"/>
                <input type="submit" value="Search" onclick="search()"/>
                <input type="button" id="id_pre" value="pre" class="button_style01 gray" style= 'display:none' onclick="searchPage('pre')" />
                <input type="button" id="id_next" value="next" class="button_style01 gray" style= 'display:none' onclick="searchPage('next')" />
                <input type="hidden" id="pageToken" value ="" />
                <input type="hidden" id="id_next_value" value ="" />

            </div>

            <div class="scrolly" style="height:420px;border-top: 3px solid #7d7d84;">
                <div id="response"></pre>
        </div>
    </div>


 <?php
    echo $OUTPUT->footer();
 ?>
