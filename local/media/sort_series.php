<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$groupid = optional_param('groupid', 0, PARAM_INT);

$sql = "select * from {lmsmedia_contents} where groupid = $groupid order by sortorder asc ";
$countsql = "select count(*) from {lmsmedia_contents} where groupid = $groupid ";
$course = $DB->get_records_sql($sql);
$total_count = $DB->count_records_sql($countsql, $params);
$num = 1;
?>
<div class="popup_content" id="class_students">
    <form name="mainForm" method="post">
        <table class="table table-border table-condensed">
            <caption class="hidden-caption"><?php echo get_string('series_sort_button','local_media') ?></caption>
            <thead>
                <tr>
                    <th><?php echo get_string('number','local_lmsdata') ?></th>
                    <th scope="row"><?php echo get_string('videoname','local_media') ?></th>
                    <th scope="row"><?php echo get_string('seriesorder','local_media') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($course as $courses) { ?>
            <input name="id<?php echo $num; ?>" type="hidden" value="<?php echo $courses->id; ?>" >
                    <tr>
                        <td><?php echo $num; ?></td>
                        <td><?php echo $courses->title; ?></td>
                        <td>
                            <select name="sortselect<?php echo $num; ?>" id="sortselect">
                                <?php
                                for ($x = 1; $x <= $total_count; $x++) {
                                    $selected = '';
                                    if ($x == $courses->sortorder) {
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="<?php echo $x; ?>" <?php echo $selected; ?>><?php echo $x; ?></option>    
                                <?php }
                                ?>

                            </select>
                        </td>
                    </tr>
                    <?php
                    $num++;
                }
                ?>
            </tbody>
        </table><!--Table End-->
    </form>
</div>
