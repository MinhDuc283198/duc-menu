<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/formslib.php');

class local_media_upload_form extends moodleform {

    function definition() {
        global $CFG, $DB, $USER, $OUTPUT;

        $mform = $this->_form;
        $context = $this->_customdata['context'];
        $mode = $this->_customdata['mode'];
        $search = $this->_customdata['search'];
        $sort = $this->_customdata['sort'];
        $page = $this->_customdata['page'];
        $id = $this->_customdata['id'];

        // 업로드폼 or 수정폼(기본정보) 인 경우
        $mform->addElement('header', 'defaultheader', get_string('basicinfo','local_media'));
        $mform->setExpanded('defaultheader', true);

        // 동영상 제목
        $mform->addElement('text', 'title', get_string('videoname','local_media'), array('size'=>80));
        $mform->setType('title', PARAM_CLEAN);
        $mform->addRule('title', null, 'required', null, 'client');

        // 동영상 설명
        $editor = $mform->addElement('editor', 'intro', get_string('videoexplain','local_media'), null, self::editor_options($context, (empty($content->id) ? null : $content->id)));
        $mform->setType('intro', PARAM_RAW);

        $reqicon = '<img class="req" title="필수 항목" alt="필수 항목" src="' . $OUTPUT->pix_url('req', 'core') . '">';
        
        

        // 공유범위        
        $targetarray = array();
        $targetarray[] = & $mform->createElement('radio', 'target', '', get_string('coopconfig','local_media'), 1);
        $targetarray[] = & $mform->createElement('radio', 'target', '', get_string('selectuser','local_media'), 2);
        $targetarray[] = & $mform->createElement('radio', 'target', '', get_string('sharelink','local_media'), 3);
        $targetarray[] = & $mform->createElement('static', 'targetcorps_area','',''
                . '<div id="targetcorps_area"><button id="targetcorpsbtn" class="selected_target_btn">'.get_string('cosearch','local_media').'</button><br/><div id="targetcorpstext" class="selected_target"></div></div>');
        $targetusersarea = '<div style="float:left; width: 48%;"><span style="line-height:30px;">'.get_string('targetuserauthtext','local_media').'</span><button id="targetusersbtn" class="selected_target_btn">'.get_string('searchuser','local_media').'</button><br/><div id="targetuserstext" class="selected_target"></div></div>';
        $targetusersarea .= '<div style="float:right; width: 48%;"><button id="targetorgsbtn" class="selected_target_btn">'.get_string('groupsearch','local_media').'</button><br/><div id="targetorgstext" class="selected_target"></div></div>';
        $targetarray[] = & $mform->createElement('static','targetusers_area' ,'' ,'<div id="targetusers_area" style="display:none;">'.$targetusersarea.'</div>');        
        $mform->addGroup($targetarray, 'targetgroup', get_string('howmuchshare','local_media') . $reqicon, array(' '), false);
        $targetgrouprules = array();
        $targetgrouprules['target'][] = array(null, 'required', null, 'client', 3);
        $mform->addGroupRule('targetgroup', $targetgrouprules);
        $mform->setDefault('target', 1);

        $mform->addElement('hidden', 'corps_id');
        $mform->addElement('hidden', 'user_ids');
        $mform->addElement('hidden', 'org_codes');
        
        // 과정에 사용여부
        $mform->addElement('advcheckbox', 'isshared', get_string('isshared','local_media'), get_string('issharedtext','local_media'));

        // 공지여부 (TODO: Global 마스터만 공지 가능)
        $mform->addElement('advcheckbox', 'notice', get_string('noticeornot','local_media'), get_string('noticecon','local_media'));

        // 공지기간
        $noticetime = array();
        $noticetime[] = & $mform->createElement('static', 'noticetimelabel', get_string('noticetime','local_media'));
        $noticetime[] = & $mform->createElement('date_selector', 'timestartnotice', get_string('noticestart','local_media'));
        $noticetime[] = & $mform->createElement('static', 'fromto', ' ~ ');
        $noticetime[] = & $mform->createElement('date_selector', 'timeendnotice', get_string('endnotice','local_media'));
        $mform->addGroup($noticetime, 'noticetimegroup', get_string('noticetime','local_media'), array(' '), false);
        $mform->setDefault('timeendnotice', strtotime("+3 day"));
        $mform->disabledIf('noticetimegroup', 'notice');

        // 구분
        $gubunOptions = array('1' => 'DTube', '2' => get_string('courseconfirm','local_media'),'3' => get_string('alluse','local_media'));
        $mform->addElement('select', 'gubun', get_string('category','local_media'), $gubunOptions);
        $mform->addRule('gubun', null, 'required', null);

        // 시리즈 그룹
        $groupid = $DB->get_field('lmsmedia_contents', 'groupid', array('id' => $id));
        $series = array();
        $series[] = & $mform->createElement('static', 'labelSeriesGroup', get_string('series','local_media').' Group');
        $series[] = & $mform->createElement('text', 'series_group', '', array('disabled' => true));
        $series[] = & $mform->createElement('hidden', 'groupid');
        $series[] = & $mform->createElement('static', 'btnSearchSeries', '', '<input type="button" onclick="javascript:seriesCtrl.searchSeries(seriesCtrl.openSeriesGroupPop);" value="'.get_string('series','local_media').' Group '.get_string('search1','local_media').'">');
        if($groupid){
            $series[] = & $mform->createElement('static', 'seriessort', '', '<input type="button" onclick="javascript:seriessort('.$groupid.');" value="'.get_string('series_sort_button','local_media').'">');
        }        
        $mform->addGroup($series, 'seriesgroup', get_string('series','local_media').' Group', array(' '), false);


        // 관리용 분류체계        
        $case = array();
        $case[] = & $mform->createElement('select', 'categoryBig', get_string('p_cate','local_media'), array(''=>get_string('p_cate','local_media')));
        $case[] = & $mform->createElement('select', 'categoryMiddle', get_string('c_cate','local_media'), array(''=>get_string('c_cate','local_media')));
        $case[] = & $mform->createElement('select', 'categorySmall', get_string('s_cate','local_media'), array(''=>get_string('s_cate','local_media')));
        $case[] = & $mform->createElement('hidden', 'category1');
        $case[] = & $mform->createElement('hidden', 'category2');
        $case[] = & $mform->createElement('hidden', 'category3');
        $mform->addGroup($case, 'casegroup', get_string('admincate','local_media'). $reqicon, array(' '), false);
        $casegrouprules = array();
        $casegrouprules['categoryBig'][] = array(null, 'required', null);
        $casegrouprules['categoryMiddle'][] = array(null, 'required', null);
        $casegrouprules['categorySmall'][] = array(null, 'required', null);
        $mform->addGroupRule('casegroup', $casegrouprules);

        // 분류 Tag
        $catetag = array();
        $catetag[] = & $mform->createElement('static', 'labelCategoryTags', get_string('categorytag','local_media'));
        $catetag[] = & $mform->createElement('text', 'categorytags', '', array('readonly' => true));
        $catetag[] = & $mform->createElement('hidden', 'categorycodevalues'); // 분류 Tag codevalue 저장용
        $catetag[] = & $mform->createElement('static', 'btnSearchTag', '', '<input type="button" onclick="javascript:tagsCtrl.setTags(tagsCtrl.openTagsPop);" value="'.get_string('search').'">');
        $catetag[] = & $mform->createElement('static', 'catetagexplain', '', '<div class="tag_explain01">'
                        . '*'.get_string('mediatext16','local_media').'</div>');
        $mform->addGroup($catetag, 'catetaggroup', get_string('categorytag','local_media') . $reqicon, array(' '), false);
        $catetaggrouprules = array();
        $catetaggrouprules['categorytags'][] = array(null, 'required', null);
        //$catetaggrouprules['categorycodevalues'][] = array(null, 'required', null, 'client');
        $mform->addGroupRule('catetaggroup', $catetaggrouprules);

        // Tag
        $tag = array();
        $tag[] = & $mform->createElement('text', 'keywords', '');
        $tag[] = & $mform->createElement('static', 'tagexplain', '', '<div class="tag_explain02">'.get_string('tagexample','local_media').''
                        . '<br/>'.get_string('mediatext18','local_media').'.</div>');
        $mform->addGroup($tag, 'taggroup', 'Tag', array(' '), false);
        $mform->setType('keywords', PARAM_RAW);

        // 사내외 구분
        $inouteoptions = array('A' => get_string('all','local_media'), 'I' => get_string('incompany','local_media'));
        $mform->addElement('select', 'inouttype', get_string('inoutcompany','local_media'), $inouteoptions);
        $mform->addRule('inouttype', null, 'required', null);
        $mform->setDefault('inouttype', 'I');

        // 동영상 사용여부
        $isusedOptions = array("1" => get_string('use','local_media'), "2" => get_string('notuse','local_media'));
        $mform->addElement('select', 'isused', get_string('videouseornot','local_media'), $isusedOptions);
        $mform->addRule('isused', null, 'required', null);
        
        // 컨텐츠 만료일
        $mform->addElement('date_selector', 'expiredate', get_string('contentlife','local_media'));
        $mform->setType('expiredate', PARAM_INT);
        $mform->setDefault('expiredate', strtotime('+1 year')); // 기본 1년
        $mform->addRule('expiredate', null, 'required', null);
        
        // 업로드파일명
        $mform->addElement('hidden', 'uploaded_file');
        $mform->setType('uploaded_file', PARAM_RAW);
        
        // 입력 URL명
        $mform->addElement('hidden', 'mediafile');
        $mform->setType('mediafile', PARAM_RAW);

        // 동영상 타입 (TODO : 수정일 경우, 디폴트 값은 DB의 값을 기준으로 변경해야함)
        $mform->addElement('hidden', 'upload_type');
        $mform->setType('upload_type', PARAM_RAW);
        $mform->setDefault('upload_type', 'video');
        
        
        // 추가정보
        $mform->addElement('header', 'additionheader', get_string('addedinfo','local_media'));
        $mform->setExpanded('additionheader', true);

        // 강의자
        $mform->addElement('text', 'teacher', get_string('courser','local_media'));
        $mform->setType('teacher', PARAM_RAW);
        
        // 저작권마크(저작표시)
        $ccmarkarray = array();
        /*
        $ccmarkarray[] = & $mform->createElement('radio', 'ccmark', '', '저작권없음', 1);
        $ccmarkarray[] = & $mform->createElement('radio', 'ccmark', '', '직접입력', 2);
         * 
         */
        $ccmarkarray[] = & $mform->createElement('hidden', 'ccmark', 2);
        $ccmarkarray[] = & $mform->createElement('text', 'ccmarktext', get_string('inputauthority','local_media'));
        $mform->addGroup($ccmarkarray, 'ccradioar', get_string('viewauthority','local_media'), array(' '), false);
        
        // 로고표시
        $islogoarray = array();
        $islogoarray[] = & $mform->createElement('radio', 'islogo', '', get_string('viewthis','local_media'), 1);
        $islogoarray[] = & $mform->createElement('radio', 'islogo', '', get_string('viewnotthis','local_media'), 0);
        $mform->addGroup($islogoarray, 'logoradioar', get_string('viewlogo','local_media'), array(' '), false);
        
        
        // 썸네일
        $mform->addElement('header', 'thumbnailheader', get_string('thumnail','local_media'));
        $mform->setExpanded('thumbnailheader', true);
        
        $mform->addElement('html', '<div class="file_info_input">');
        $mform->addElement('html', '<div class="search_thumb">');
        $mform->addElement('file', 'uploadthumbnail', '');
        $mform->addElement('static', 'maxthumbsize', get_string('maxthumbsize','local_media'));
        //$mform->addElement('html', '<input type="button" value="업로드" class="button_style01" />');
        $mform->addElement('html', '</div>');
        //기본 썸네일 선택
        $mform->addElement('html', '<div class="radio_thumb_list">');
        if($thumbs = $DB->get_records('lmsmedia_thumbs', array('mediaid'=>$id))){
            foreach($thumbs as $thumb){
                if(file_exists($thumb->filepath)){
                    $mform->addElement('html', '<div class="radio_thumb list list4 ">');
                    $mform->addElement('radio', 'thumsid', '', '', $thumb->id);
                    if($thumb->isused == 1){ $mform->setDefault('thumsid', $thumb->id); }
                    $mform->addElement('html', '<label for="id_thumsid_'.$thumb->id.'" class="caption">');
                    $thumbimage = str_replace($CFG->local_media_file_path,$CFG->wwwroot.'/storage',$thumb->filepath);
                    $mform->addElement('html', '<img src="'.$thumbimage.'" alt="'.$thumb->filename.'" style="width:130px; height:110px;"/>');
                    $mform->addElement('html', '<p><span class="filename">'.$thumb->filename.'</span><a href="javascript:thumbnailCtrl.removeThummbnail('.$thumb->id.');"><span data-brackets-id="864" class="boxclose">X</span></a></p>');              
                    $mform->addElement('html', '</label>');
                    $mform->addElement('html', '</div>');
                }
            }
        }
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');   

        // 자막등록
        $mform->addElement('header', 'captionheader', get_string('addsubtitle','local_media'));
        $mform->setExpanded('captionheader', true);
       
        $langs = get_string_manager()->get_list_of_translations();
        foreach($langs as $key=>$lang){
        $langtitle = trim(str_replace('('.$key.')','',$lang));
        $mform->addElement('file', 'captionfile'.$key, $langtitle);
        if($caption = $DB->get_record('lmsmedia_captions',array('mediaid'=>$id,'lang'=>$key))){
            $mform->addElement('html', ' <p class="filename">'.$caption->filename.''
                    . '<a href="javascript:captionCtrl.removeCaption('.$id.','.$caption->id.');"><span class="boxclose">X</span></a>'
                    . '<a href="javascript:captionCtrl.editCaption('.$id.','.$caption->id.');"><span class="boxedit">편집</span></a></p>');
            $mform->addElement('hidden','captionid'.$key,$caption->id);
        }
        }

        // Embed Code
        $mform->addElement('header', 'embedheader', get_string('share','local_media'));
        $mform->setExpanded('embedheader', true);
        
        $mform->addElement('static', 'linkurl', 'Link URL', '<div id="linkurl" style="border:solid;padding:20px;"></div>');
        $mform->addElement('static', 'btnCopylink', '', '<input type="button" onclick="javascript:embedCtrl.copy(document.getElementById(\'linkurl\'));" value="복사">');
        
        $mform->addElement('static', 'embed', 'Embed Code', '<div id="embed_code" style="border:solid;padding:20px;"></div>');
        $mform->addElement('static', 'btnCopy', '', '<input type="button" onclick="javascript:embedCtrl.copy(document.getElementById(\'embed_code\'));" value="복사">');
        
        if(!$id){
            //동영상 업로드
            $mform->addElement('header', 'mediauploadheader', get_string('videoupload','local_media'));
            $mform->setExpanded('mediauploadheader', true);
            $mediaupload = $this->media_upload_component();
            $mform->addElement('static','mediauploadarea','',$mediaupload);
        }

        // Set mode
        $mform->addElement('hidden', 'mode');
        $mform->setType('mode', PARAM_RAW);
        $mform->setDefault('mode', $mode);
        // Set id
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $id);
        
        $mform->addElement('hidden', 'search');
        $mform->setType('search', PARAM_RAW);
        $mform->setDefault('search', $search);
        
        $mform->addElement('hidden', 'sort');
        $mform->setType('sort', PARAM_RAW);
        $mform->setDefault('sort', $sort);
        
        $mform->addElement('hidden', 'page');
        $mform->setType('page', PARAM_INT);
        $mform->setDefault('page', $page);

        $this->add_action_buttons();
    }
    
    function media_upload_component(){
        
        $output = '';
        $output .= html_writer::start_div('aside style02', array('style'=>'clear:both;'));
        $output .= html_writer::start_div('sub_menu noshadow height300');
        $output .= html_writer::start_tag('ul');
        $output .= html_writer::tag('li', html_writer::tag('a',get_string('fileupload','local_media'),array('href'=>'javascript:uploadAreaCtrl.openUploadDiv(\'video\')')), array('class'=>'uploadlist','id'=>'video_uploadlist'));
        $output .= html_writer::tag('li', html_writer::tag('a','YouTube',array('href'=>'javascript:uploadAreaCtrl.openUploadDiv(\'youtube\')')), array('class'=>'uploadlist','id'=>'youtube_uploadlist'));
        $output .= html_writer::tag('li', html_writer::tag('a','Vimeo',array('href'=>'javascript:uploadAreaCtrl.openUploadDiv(\'vimeo\')')), array('class'=>'uploadlist','id'=>'vimeo_uploadlist'));
        $output .= html_writer::end_tag('ul');
        $output .= html_writer::end_div();
        $output .= html_writer::end_div();
        $output .= html_writer::start_div('uploadVideo');
        $output .= html_writer::start_div('_upload_div select_file',array('id'=>'video_uploader'));
        $output .= html_writer::tag('p','Your browser doesn\'t have Flash, Silverlight or HTML5 support.');
        $output .= html_writer::end_tag('div');
        $output .= html_writer::start_div('_upload_div youtube',array('id'=>'youtube_uploader','style'=>'display:none;'));   
        $output .= html_writer::tag('h3','YouTube '.get_string('videoupload','local_media').'',array('class'=>'page_title'));
        $output .= html_writer::start_div('video_search');
        $output .= html_writer::div(get_string('inputurl','local_media').'.','video_explain');
        $output .= html_writer::empty_tag('input',array('type'=>'text','id'=>'youtube_url','placeholder'=>'예시) https://youtu.be/onwxHUnenQc','title'=>'동영상 주소'));
        $output .= html_writer::end_div();
        $output .= html_writer::end_div();
        $output .= html_writer::start_div('_upload_div youtube',array('id'=>'vimeo_uploader','style'=>'display:none;'));
        $output .= html_writer::tag('h3','Vimeo'.get_string('videoupload','local_media'),array('class'=>'page_title'));
        $output .= html_writer::start_div('video_search');
        $output .= html_writer::div(get_string('inputurl','local_media').'.','video_explain');
        $output .= html_writer::empty_tag('input',array('type'=>'text','id'=>'vimeo_url','placeholder'=>'예시) https://vimeo.com/205183632','title'=>'동영상 주소'));
        $output .= html_writer::end_div();
        $output .= html_writer::end_div();
            
        return $output;
        
    }

    public static function editor_options($context, $contentid = null) {
        global $PAGE, $CFG;
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'local_media', 'intro', $contentid)
        );
    }

    function validation($data, $files) {

        $errors = parent::validation($data, $files);
        
        if($data['mode'] == 'default'){
            if($data['target'] == '1' && !$data['corps_id']){
                $errors['targetgroup'] = get_string('selectsharewho','local_media').'.';
            }

            if($data['target'] == '2' && (!$data['user_ids'] && !$data['org_codes'])){
                $errors['targetgroup'] = get_string('selectsharewho','local_media').'.';
            }
        }
        
        if($data['mode'] == 'thumbnail'){
            $thumbnailfile = $_FILES['uploadthumbnail'];
            if($thumbnailfile['size'] > 1024*100){
                $errors['uploadthumbnail'] = get_string('maxthumbsize','local_media');
            }
        }
        
        if (!$data['id'] && !$data['uploaded_file'] && $data['upload_type'] == 'video') {
            $errors['uploaded_file'] = get_string('addvideofile','local_media').'.';
            echo '<script>alert(\''.get_string('addvideofile','local_media').'\');</script>';
        }

        if (!$data['id'] && !$data['mediafile'] && $data['upload_type'] == 'youtube') {
            $errors['mediafile'] = get_string('addyoutube','local_media').'.';
            echo '<script>alert(\''.get_string('addyoutube','local_media').'.\');</script>';
        }

        if (!$data['id'] && !$data['mediafile'] && $data['upload_type'] == 'vimeo') {
            $errors['mediafile'] = get_string('addvimeo','local_media').'.';
            echo '<script>alert(\''.get_string('addvimeo','local_media').'.\');</script>';
        }
        

        return $errors;
    }

}
