<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

global $DB, $USER;

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();

$id = required_param('id', PARAM_INT);

$satis = $DB->get_record('lmsmedia_satisfaction', array('mediaid' => $id, 'userid' => $USER->id));
$progress = $DB->get_field('lmsmedia_track', 'progress', array('mediaid' => $id, 'userid' => $USER->id));
?>

<div id="pop_wrap" class="videoview_pop" >
    <div class="pop_title">
        <h2><?php echo get_string('videorecommend','local_media'); ?></h2>
        <p class="popclose"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></p>
    </div>
    <div id="contents">

        <?php
        if (!$satis) {
            if($progress == 100){
            ?>  


            <div class="modal-text">
                <p><?php echo get_string('mediatext3','local_media'); ?></p>
                <p><?php echo get_string('mediatext4','local_media'); ?></p>
                <p class="pink"><?php echo get_string('mediatext5','local_media'); ?></p>
            </div>
            <form name="satis_submit_form" id="id_satis_submit_form">
                <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                <div class="explan_group"><span class="left_cont"><?php echo get_string('notatall','local_media'); ?></span><span class="right_cont"><?php echo get_string('itreallyis','local_media'); ?></span></div>
                
                <?php
                $output = '';
                $output .= html_writer::start_tag('ul', array('class' => 'satis-score-list'));
                for ($i = 0; $i <= 10; $i++) {
                    $output .= html_writer::start_tag('li') . html_writer::tag('input', '', array('type' => 'radio', 'value' => $i, 'id' => 'score_' . $i, 'name' => 'score', 'class' => 'scoreradio'));
                    $output .= html_writer::tag('label', $i, array('for' => 'score_' . $i)) . html_writer::end_tag('li');
                }
                $output .= html_writer::end_tag('ul');
                echo $output;
                ?>

                <div class="center">
                    <a class="save button_style01" style="display:inline-block;" onclick="return update_satisfaction();"><?php echo get_string('submit','local_media'); ?></a>
                </div>
            </form>

            <?php
            }else{?>
                <div class="modal-text">
                    <p><?php echo get_string('mediatext6','local_media'); ?></p>
                    <p class="pink"><?php echo get_string('mediatext7','local_media'); ?></p>
            </div>
            <?php }
        } else {
            ?>
            <div class="modal-text">
                <p><?php echo get_string('mediatext3','local_media'); ?></p>
                <p><?php echo get_string('mediatext8','local_media'); ?></p>
                <p class="pink"><?php echo get_string('mediatext9','local_media'); ?></p>
            </div>
            <?php
        }
        echo '</div></div>';
        echo $OUTPUT->footer();
        ?>



