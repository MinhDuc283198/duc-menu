<?php
/**
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require(dirname(__FILE__) . '/config.php');
require(dirname(__FILE__) . '/lib.php');

require_login();

$returnvalue = new stdClass();

$id = optional_param('id', 0, PARAM_INT);
$type = optional_param('type', 'video', PARAM_RAW);     

if($id){
    $mediafile = $DB->get_record('lmsmedia_files',array('id'=>$id));
}
    
$newfileid = local_media_save_file($_FILES['file'], $type, $mediafile);

$returnvalue->ok = 1;
$returnvalue->fileid = $newfileid;

die(json_encode($returnvalue));
    
?>
