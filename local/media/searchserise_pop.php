<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$name = optional_param('name', '', PARAM_RAW);

$context = context_system::instance();
require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');

echo $OUTPUT->header();

$groupList = local_media_get_group($name);

?>
    <div id="pop_wrap" class="search_serise">
        <div class="pop_title inbutton">
            <h2><?php echo get_string('searchseries','local_media'); ?></h2>
            <p class="popclose"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></p>
        </div>
        <div id="contents">
            <div class="search_style01">
                <input type="text" class="serise_input" placeholder="<?php echo get_string('searchseries','local_media'); ?>" value="<?=$name?>"/>
                <input type="submit" class="button_style01 gray" value="<?php echo get_string('search','local_media'); ?>">
                <input type="submit" class="button_style01" value="<?php echo get_string('search','local_media'); ?>">
            </div>
    <?php
        if (count($groupList) > 0) {
    ?>
            <div class="serise_list">
                <ul>
                <?php
                    for ($i = 0, $l = count($groupList); $i < $l; $i++) {
                ?>
                    <li>
                        <a href="#" class="serise_text" onclick="javascript:seriesCtrl.setSeriesGroup('<?= $groupList[$i+1]->id ?>', '<?= $groupList[$i+1]->name ?>')"><?= $groupList[$i+1]->name ?><img src="/theme/oklasscompany/pix/images/pencil..png" height="13px" /></a>
                        <input type="button" value="<?php echo get_string('select','local_media'); ?>" class="button_style02" />
                    </li>
                <?php
                    }
                ?>
                </ul>
            </div>
    <?php
        } else {
    ?>
            <div class="serise_list">
                <?php echo get_string('nosearchresult'); ?>.
            </div>
    <?php
        }
    ?>
        </div>
    </div>

<script type="text/javascript">

</script>
<?php
    echo $OUTPUT->footer();
?>


