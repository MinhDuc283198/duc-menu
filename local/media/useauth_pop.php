<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();
?>  
<div id="pop_wrap" class="auth_search">
        <div class="pop_title inbutton">
            <h2>User 권한검색</h2>
            <p class="popclose"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></p>
        </div>
        <div id="contents">
            <div class="search_style01">
                <label for="department">부서명</label>
                <input type="text" id="department" class="margin-input" />
                <label for="user">이용자</label>
                <input type="text" id="user" />
                <input type="submit" class="button_style01" value="조회">
            </div>
            
            <div class="scroll_table">
                <table class="table table-border table-condensed">
                    <colgroup>
                        <col width="30%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="15%" />
                        <col width="30%" />
                    </colgroup>
                    <thead>
                        <th>부서</th>
                        <th>성명</th>
                        <th>직급</th>
                        <th>사무실전화번호</th>
                        <th>휴대폰</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                        <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="scroll_table">
               <table class="table table-border table-condensed">
                    <colgroup>
                         <col width="30%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="15%" />
                        <col width="30%" />
                    </colgroup>
                    <thead>
                        <th>부서</th>
                        <th>성명</th>
                        <th>직급</th>
                        <th>사무실전화번호</th>
                        <th>휴대폰</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                         <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                        <tr>
                            <td>LED 연구 1팀</td>
                            <td> 최원석</td>
                            <td>차장</td>
                            <td>1002-1234</td>
                            <td>010-1234-5678</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="bottom_buttons center">
                <button class="save button_style01 gray">선택</button>
            </div>
            
        </div>
    </div>
    
      


 <?php
    echo $OUTPUT->footer();
 ?>


