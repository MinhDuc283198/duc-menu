<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__) . '/playerlib.php');

global $CFG, $DB, $USER;

@header('Content-type: application/json; charset=utf-8');
$returnvalue = new stdClass();

$amode = required_param('amode', PARAM_RAW);
$id = required_param('id', PARAM_INT);
$fileid = required_param('fileid', PARAM_INT);

$caption = $DB->get_record('lmsmedia_captions', array('id' => $fileid, 'userid' => $USER->id));
$revision = $DB->get_record('lmsmedia_captions_revision',array('mediaid'=>$id,'captionid'=>$caption->id));

if ($caption) {
    if ($amode == 'get') {
        $path = $caption->filepath;
        if (file_exists($path)) {
            $subs = local_media_get_captions($path);
            $subtitle = new StdClass();
            $caption_table = '<ol class="subtitle_list">';
            $num = 1;
            foreach ($subs as $sub) {
                $starts = explode(',', $sub->starttime);
                $ends = explode(',', $sub->stoptime);
                $subtitle->stime = local_media_seconds_from_time($starts[0]);
                $subtitle->etime = local_media_seconds_from_time($ends[0]);
                $stimeobj = local_media_time_from_seconds($subtitle->stime);
                $etimeobj = local_media_time_from_seconds($subtitle->etime);
                $subtitle->caption = preg_replace('/\r\n|\r|\n/', '', $sub->text);
                $subtitle->caption = stripslashes($subtitle->caption);

                $caption_table .= '<li class="subtitle">
                         <a id="a' . $num . '" />
                         <p class="subtitle_time">
                         <input type="text" maxlength="2" class="w_20" id="stime_hh_' . $num . '" readonly value="' . $stimeobj->h . '"> : 
                         <input type="text" maxlength="2" class="w_20" id="stime_mm_' . $num . '" readonly value="' . $stimeobj->m . '"> : 
                         <input type="text" maxlength="2" class="w_20" id="stime_ss_' . $num . '" readonly value="' . $stimeobj->s . '">
                          ~ 
                         <input type="text" maxlength="2" class="w_20" id="etime_hh_' . $num . '" readonly value="' . $etimeobj->h . '"> : 
                         <input type="text" maxlength="2" class="w_20" id="etime_mm_' . $num . '" readonly value="' . $etimeobj->m . '"> : 
                         <input type="text" maxlength="2" class="w_20" id="etime_ss_' . $num . '" readonly value="' . $etimeobj->s . '">
                         </p>
                         <p class="subtitle_text" data-start="' . $subtitle->stime . '" data-end="' . $subtitle->etime . '">
                         <textarea id="textprint_' . $num . '" rows="1"  readonly>' . $subtitle->caption . '</textarea>
                         </p>';

                $caption_table .= '<p class="subtitle_edit">
                         <input type="button" class="smallbtn" value="'.get_string('edit','local_media').'" edit="true" onclick="caption_update($(this),\'' . $caption->lang . '\',' . $num . ');">
                         </p>';

                $caption_table .= '</li>';
                $num++;
            }
            $caption_table .= '</ol>';

            $returnvalue->captions = $caption_table;
        }
    } else {
        $num = optional_param('num', 0, PARAM_INT);
        $stime_hh = trim(optional_param('stime_hh', 0, PARAM_INT));
        $stime_mm = trim(optional_param('stime_mm', 0, PARAM_INT));
        $stime_ss = trim(optional_param('stime_ss', 0, PARAM_INT));
        $etime_hh = trim(optional_param('etime_hh', 0, PARAM_INT));
        $etime_mm = trim(optional_param('etime_mm', 0, PARAM_INT));
        $etime_ss = trim(optional_param('etime_ss', 0, PARAM_INT));
        $textprint = trim(optional_param('textprint', '', PARAM_RAW));
        $userid = $USER->id;

        $path = $caption->filepath;
        if (file_exists($path)) {
            $captions = local_media_get_captions($path);

            if (!$userid) {
                $err_msg = get_string('afterlogin','local_media').'.';
            }

            if (!$textprint) {
                $err_msg = get_string('inputsubtitle','local_media').'.';
            }

            $stime = local_media_seconds_from_hms($stime_hh, $stime_mm, $stime_ss);     //초단위로 환산
            $stimeobj = local_media_time_from_seconds($stime);

            $etime = local_media_seconds_from_hms($etime_hh, $etime_mm, $etime_ss);
            $stimeobj = local_media_time_from_seconds($etime);

            if ($stime > $etime) {
                $err_msg = get_string('timeselectagain','local_media').'.';
            }
            
            $stime = $stime_hh . ':' . $stime_mm . ':' . $stime_ss;
            $stime2 = local_media_seconds_from_time($stime);
            $stime = local_media_time_from_seconds($stime2);
            $stime = $stime->h.':'.$stime->m.':'.$stime->s;
            $etime = $etime_hh . ':' . $etime_mm . ':' . $etime_ss;
            $etime2 = local_media_seconds_from_time($etime);
            $etime = local_media_time_from_seconds($etime2);
            $etime = $etime->h.':'.$etime->m.':'.$etime->s;

            //자막 파일 처리
            $numkey = 0;

            //새로 등록하는 경우 마지막에 배열로 우선 집어 넣는다.
            if (!$num) {
                $numkey = (!$newnum || $newnum > sizeof($captions)) ? sizeof($captions) : $newnum - 1;
            } else {
                $numkey = $num - 1;
            }

            $prestime = $captions[$numkey - 1]->starttime;
            $preetime = $captions[$numkey - 1]->stoptime;
            $prestime2 = ($prestime) ? local_media_seconds_from_time($prestime) : '';
            $preetime2 = ($preetime) ? local_media_seconds_from_time($preetime) : '';

            if ($newnum) {
                $nxtstime = $captions[$numkey]->starttime;
                $nxtetime = $captions[$numkey]->stoptime;
            } else {
                $nxtstime = $captions[$numkey + 1]->starttime;
                $nxtetime = $captions[$numkey + 1]->stoptime;
            }
            $nxtstime2 = ($nxtstime) ? local_media_seconds_from_time($nxtstime) : '';
            $nxtetime2 = ($nxtetime) ? local_media_seconds_from_time($nxtetime) : '';

            if ($prestime2 && $stime2 <= $prestime2) {
                $err_msg = get_string('timeselectagain2','local_media').'.';
            }

            if ($nxtetime2 && $etime2 >= $nxtetime2) {
                $err_msg = get_string('timeselectagian3','local_media').'.';
            }

            if (($stime2 == $prestime2 && $etime2 == $preetime2) && ($stime2 == $nxtstime2 && $etime2 == $nxtetime2)) {
                $err_msg = get_string('timeselectagain4','local_media').'.';
            }

            if (!empty($err_msg)) {
                $returnvalue->status = 'error';
                $returnvalue->message = $err_msg;
                echo json_encode($returnvalue);
                die;
            }

            //번호값 정리부터
            if($newnum){
                for ($i=sizeof($captions);$i>=$numkey;$i--) {
                    $captions[$i]->starttime = $captions[$i-1]->starttime;
                    $captions[$i]->stoptime = $captions[$i-1]->stoptime;
                    $captions[$i]->text = $captions[$i-1]->text;
                }
            }
    
            //이전, 다음 시작/끝시간 설정
            if($preetime2&&$preetime2>$stime2){
                $captions[$numkey-1]->stoptime = $stime;
            }
            if($nxtstime2&&$nxtstime2<$etime2){
                $captions[$numkey+1]->starttime = $etime;
            }
        
            //입력한 값을 해당 위치에 넣는다.
            $captions[$numkey]->starttime = $stime;
            $captions[$numkey]->stoptime = $etime;
            $captions[$numkey]->text = preg_replace('/\r\n|\r|\n/', '', $textprint);
            $captions[$numkey]->text = addslashes($captions[$numkey]->text);
            
            local_media_caption_put_srt($path, $captions);  
            
            $contents = serialize($captions[$numkey]);
            
            if(!$revision){
                $revision = new stdClass();
                $revision->mediaid = $id;
                $revision->captionid = $caption->id;
                $revision->contents = $contents;
                $revision->eventtype = 'C';
                $revision->userid = $USER->id;
                $revision->timemodified = time();
                $revisionid = $DB->insert_record('lmsmedia_captions_revision',$revision);

                $caption->revisionid = $revisionid;
                $caption->timemodified = time();
                $DB->update_record('lmsmedia_captions', $caption);
            
            }
            
        }
    }
}

$returnvalue->status = 'success';

echo json_encode($returnvalue);
?>

