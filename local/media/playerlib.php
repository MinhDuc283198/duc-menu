<?php

function local_media_seconds_from_time($time) {
    list($h, $m, $s) = explode(':', $time);
    return local_media_seconds_from_hms($h, $m, $s);
}

function local_media_seconds_from_hms($h, $m, $s) {
    return ($h * 3600) + ($m * 60) + $s;
}

function local_media_time_from_seconds($seconds) {
    $time = new stdclass();
    $time->h = sprintf('%02d', floor($seconds / 3600));
    $time->m = sprintf('%02d', floor(($seconds % 3600) / 60));
    $time->s = sprintf('%02d', $seconds - ($time->h * 3600) - ($time->m * 60));
    return $time;
}

function local_media_get_captions($srtpath){
    
    if ($srtpath) {
        $pathinfo = pathinfo($srtpath);
        $extension = strtolower($pathinfo['extension']);
        define('SRT_STATE_VTT', 1);
        define('SRT_STATE_SUBNUMBER', 2);
        define('SRT_STATE_TIME', 3);
        define('SRT_STATE_TEXT', 4);
        define('SRT_STATE_BLANK', 5);

        $lines = file($srtpath);
        $total = count($lines);
        $numb = 0;

        $subs = array();
        if($extension == 'vtt'){
            $state = SRT_STATE_VTT;
        }else{
            $state = SRT_STATE_SUBNUMBER;
        }
        $subNum = 0;
        $subText = '';
        $subTime = '';

        foreach ($lines as $line) {

            $numb++;

            switch ($state) {
                case SRT_STATE_VTT:
                    if (trim($line) == ''){
                        $state = SRT_STATE_SUBNUMBER;
                    }else{
                        $line = str_replace('WEBVTT FILE','',$line);
                        $state = SRT_STATE_VTT;
                    }
                    break;
                
                case SRT_STATE_SUBNUMBER:
                    preg_match('/[0-9]+/', $line, $numarr);
                    $subNum = trim($numarr[0]);
                    $state = SRT_STATE_TIME;
                    break;

                case SRT_STATE_TIME:
                    $subTime = trim($line);
                    $state = SRT_STATE_TEXT;
                    break;

                case SRT_STATE_TEXT:
                    if (trim($line) == '' || $numb == $total) {
                        if ($numb == $total) {
                            $subText .= $line;
                        }
                        $sub = new stdClass;
                        $sub->number = $subNum;

                        list($startTime, $stopTime) = explode(' --> ', $subTime);
                        if($extension == 'vtt'){
                            $startTimes = explode('.',$startTime);
                            $stopTimes = explode('.',$stopTime);
                        }else{
                            $startTimes = explode(',',$startTime);
                            $stopTimes = explode(',',$stopTime);
                        }

                        $sub->text = $subText;
                        $sub->starttime = $startTimes[0];
                        $sub->stoptime = $stopTimes[0];
                        $subText = '';
                        $subs[] = $sub;
                        $state = SRT_STATE_SUBNUMBER;
                    } else {
                        $subText .= $line;
                    }
                    break;
            }
        }
        
    }
    
    return $subs;
    
}

function local_media_caption_put_srt($path,$captions) {

    $num = 1;
    $caption = '';
    
    $dirs = file($path);
    $dir = trim($dirs[0]);
    
    foreach ($captions as $entry) {
    
        $start = $entry->starttime;
        $end = $entry->stoptime;
        $text = trim($entry->text);

        $caption .= $num . PHP_EOL;
        $caption .= $start . ' --> ' . $end . PHP_EOL;
        $caption .= $text . PHP_EOL . PHP_EOL;

        $num++;
    }
    
    file_put_contents($path,$caption) or die("file_put_contents failed");
    
}

function local_media_get_progress($mediaid, $userid) {
    global $DB;

    $progress = 0;

    $sql = "SELECT * FROM {lmsmedia_playtime} 
        WHERE mediaid = :mediaid and userid = :userid  and positionfrom < positionto ";

    $params = array('mediaid' => $mediaid, 'userid' => $userid);
    $sql .= 'ORDER BY positionfrom ASC, positionto ASC';

    if ($details = $DB->get_records_sql($sql, $params)) {

        $start = -1;
        $end = -1;
        foreach ($details as $detail) {
            if ($start == -1) {
                $progress = ($detail->positionto - $detail->positionfrom);

                $start = $detail->positionfrom;
                $end = $detail->positionto;
            } else {
                if (($start <= $detail->positionfrom && $detail->positionfrom <= $end) && ($end <= $detail->positionto)) {
                    $progress += ($detail->positionto - $end);

                    $end = $detail->positionto;
                } else if ($end < $detail->positionfrom) {
                    $progress += ($detail->positionto - $detail->positionfrom);

                    $start = $detail->positionfrom;
                    $end = $detail->positionto;
                }
            }
        }
    }

    return $progress;
}
