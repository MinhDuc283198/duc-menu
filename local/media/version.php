<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017032400;
$plugin->requires = 2012061700;
$plugin->component = 'local_media'; // Full name of the plugin (used for diagnostics)
