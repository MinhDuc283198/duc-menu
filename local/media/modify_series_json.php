<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$id = optional_param('id', '', PARAM_RAW);
$name = optional_param('name', '', PARAM_RAW);

$context = context_system::instance();
require_login();

$result = local_media_update_group($id, $name);
echo json_encode($result);
?>


