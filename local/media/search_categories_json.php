<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$code = optional_param('code', '', PARAM_RAW);

$context = context_system::instance();
require_login();

$categories_list = local_media_get_categories($code);
echo json_encode($categories_list);
?>
