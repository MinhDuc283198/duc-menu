<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$mediaid = optional_param('mediaid', '', PARAM_INT);
$filename = optional_param('filename', '', PARAM_RAW);
$lang = optional_param('lang', '', PARAM_RAW);

$context = context_system::instance();
require_login();

$caption = new stdClass();
$caption->mediaid = $mediaid;
$caption->filepath = '/lcmsdata/media/' . $mediaid . '/caption';
$caption->filename = $filename;
$caption->lang = $lang;
$caption->timecreated = time();
$caption->timemodified = time();

local_media_add_caption($caption);
?>


