<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__) . '/playerlib.php');

global $DB, $USER;

$context = context_system::instance();
require_login();

$PAGE->set_context($context);
$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');

$id = required_param('id', PARAM_INT);
$fileid = required_param('fileid', PARAM_INT);

$caption = $DB->get_record('lmsmedia_captions', array('id' => $fileid, 'userid' => $USER->id));

if ($caption) {
    //자막 편집
    $PAGE->set_pagelayout('popup');
    echo $OUTPUT->header();
    ?>

    <div id="pop_wrap" class="videoview_pop" >
        <div class="pop_title">
            <h2><?php echo get_string('editsubtitle','local_media'); ?></h2>
            <a href="javascript:self.close();"><p class="popclose"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></p></a>
        </div>

        <div class="modal-text">
            <p><span class="pink"><?php echo $caption->filename; ?></span> <?php echo get_string('caneditsubtitle','local_media'); ?>.</p>
            <p>※ <?php echo get_string('mediatext2','local_media'); ?>.</p>
        </div>
        <form name="caption_submit_form" id="id_caption_submit_form">
            <div class="subtitle_all_area">

                <!-- Subtitle Table Wrap Start -->
                <div class="subtitle_edit" id="subtitle_<?php echo $caption->lang; ?>">

                    <?php
                    $path = $caption->filepath;
                    if (file_exists($path)) {
                        $subs = local_media_get_captions($path);
                        $subtitle = new StdClass();
                        $caption_table = '<ol class="subtitle_list">';
                        $num = 1;
                        foreach ($subs as $sub) {
                            $starts = explode(',', $sub->starttime);
                            $ends = explode(',', $sub->stoptime);
                            $subtitle->stime = local_media_seconds_from_time($starts[0]);
                            $subtitle->etime = local_media_seconds_from_time($ends[0]);
                            $stimeobj = local_media_time_from_seconds($subtitle->stime);
                            $etimeobj = local_media_time_from_seconds($subtitle->etime);
                            
                            $subtitle->caption = preg_replace('/\r\n|\r|\n/', '', $sub->text);
                            $subtitle->caption = stripslashes($subtitle->caption);

                            $caption_table .= '<li class="subtitle">
                                     <a id="a' . $num . '" />
                                     <p class="subtitle_time">
                                     <input type="text" maxlength="2" class="w_20" id="stime_hh_' . $num . '" readonly value="' . $stimeobj->h . '"> : 
                                     <input type="text" maxlength="2" class="w_20" id="stime_mm_' . $num . '" readonly value="' . $stimeobj->m . '"> : 
                                     <input type="text" maxlength="2" class="w_20" id="stime_ss_' . $num . '" readonly value="' . $stimeobj->s . '">
                                      ~ 
                                     <input type="text" maxlength="2" class="w_20" id="etime_hh_' . $num . '" readonly value="' . $etimeobj->h . '"> : 
                                     <input type="text" maxlength="2" class="w_20" id="etime_mm_' . $num . '" readonly value="' . $etimeobj->m . '"> : 
                                     <input type="text" maxlength="2" class="w_20" id="etime_ss_' . $num . '" readonly value="' . $etimeobj->s . '">
                                     </p>
                                     <p class="subtitle_text" data-start="' . $subtitle->stime . '" data-end="' . $subtitle->etime . '">
                                     <textarea id="textprint_' . $num . '" rows="1"  readonly>' . $subtitle->caption . '</textarea>
                                     </p>';

                            $caption_table .= '<p class="subtitle_edit">
                                     <input type="button" class="smallbtn" value="'.get_string('edit','local_media').'" edit="true" onclick="caption_update($(this),\'' . $caption->lang . '\',' . $num . ');">
                                     </p>';

                            $caption_table .= '</li>';
                            $num++;
                        }
                        $caption_table .= '</ol>';
                        echo $caption_table;
                    } else {
                        echo '<span>'.get_string('nosubtitle','local_media').'.</span>';
                    }

                    echo '</div>';
                    ?>

                </div>
            </div>

        </form>

    </div>

    <script type="text/javascript">

        function lang_change(lang, cur) {
            $('.subtitle_area').hide();
            $('#subtitle_' + lang).show();
            $('input[name=lang]').val(lang);
            $('.nav-tabs li').removeClass('active');
            cur.addClass('active');
        }

        function caption_get(id, fileid, lang) {
            $.ajax({
                url: 'caption_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {
                    amode: 'get',
                    id: id,
                    fileid: fileid
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'success') {
                        status = true;
                        $('#subtitle_' + lang).empty().append(data.captions);
                    } else {
                        alert(data.message);
                        status = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                    status = false;
                }
            });
        }

        function caption_add(id) {

            var add = id.attr('add');
            if (add == 'true') {
                $('.subtitle_add_area').show();
                id.val('<?php echo get_string('submitcancel','local_media'); ?>');
                id.attr('add', 'false');
            } else {
                $('.subtitle_add_area').hide();
                id.val('<?php echo get_string('addsubtitle','local_media'); ?>');
                id.attr('add', 'true');
            }

        }

        function caption_update(id, lang, num) {
            if (id.attr('edit') == 'true') {
                id.removeClass('orange_btn').addClass('blue_btn').val('<?php echo get_string('complete','local_media'); ?>');
                id.attr('edit', 'false');
                $('#subtitle_' + lang + ' #stime_hh_' + num).add('#subtitle_' + lang + ' #stime_mm_' + num).add('#subtitle_' + lang + ' #stime_ss_' + num).attr('readonly', false).css('background', '#fffbe8');
                $('#subtitle_' + lang + ' #etime_hh_' + num).add('#subtitle_' + lang + ' #etime_mm_' + num).add('#subtitle_' + lang + ' #etime_ss_' + num).attr('readonly', false).css('background', '#fffbe8');
                $('#subtitle_' + lang + ' #textprint_' + num).attr('readonly', false).css('background', '#fffbe8');
            } else {
                var status = caption_edit_ajax(lang, num);
                if (status) {
                    id.removeClass('blue_btn').addClass('orange_btn').val('<?php echo get_string('edit','local_media'); ?>');
                    id.attr('edit', 'true');
                    $('#subtitle_' + lang + ' #stime_hh_' + num).add('#stime_mm_' + num).add('#stime_ss_' + num).attr('readonly', true).css('background', '#eee');
                    $('#subtitle_' + lang + ' #etime_hh_' + num).add('#etime_mm_' + num).add('#etime_ss_' + num).attr('readonly', true).css('background', '#feee');
                    $('#subtitle_' + lang + ' #textprint_' + num).attr('readonly', true).css('background', '#eee');
                }
            }
        }

        function caption_edit_ajax(lang, num) {
            var status = true;
            var id = <?php echo $id; ?>;
            var fileid = <?php echo $fileid; ?>;

            if ($('#subtitle_' + lang + ' #stime_hh_' + num).val() == '' || $('#subtitle_' + lang + ' #stime_mm_' + num).val() == '' || $('#subtitle_' + lang + ' #stime_ss_' + num).val() == '') {
                alert('<?php echo get_string('inputstarttime','local_media'); ?>.');
                return false;
            }

            if ($('#subtitle_' + lang + ' #etime_hh_' + num).val() == '' || $('#subtitle_' + lang + ' #etime_mm_' + num).val() == '' || $('#subtitle_' + lang + ' #etime_ss_' + num).val() == '') {
                alert('<?php echo get_string('inputendtime','local_media'); ?>.');
                return false;
            }

            if ($('#subtitle_' + lang + ' #textprint_' + num).val() == '') {
                alert('<?php echo get_string('inputsubtitlecontent','local_media'); ?>.');
                return false;
            }

            $.ajax({
                url: 'caption_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {
                    amode: 'edit',
                    num: num,
                    id: id,
                    fileid: fileid,
                    stime_hh: $('#subtitle_' + lang + ' #stime_hh_' + num).val(),
                    stime_mm: $('#subtitle_' + lang + ' #stime_mm_' + num).val(),
                    stime_ss: $('#subtitle_' + lang + ' #stime_ss_' + num).val(),
                    etime_hh: $('#subtitle_' + lang + ' #etime_hh_' + num).val(),
                    etime_mm: $('#subtitle_' + lang + ' #etime_mm_' + num).val(),
                    etime_ss: $('#subtitle_' + lang + ' #etime_ss_' + num).val(),
                    textprint: $('#subtitle_' + lang + ' #textprint_' + num).val()
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'success') {
                        status = true;
                        caption_get(id, fileid, lang);
                    } else {
                        alert(data.message);
                        status = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                    status = false;
                }
            });

            return status;
        }

        function caption_add_ajax(lang) {

            var status = true;
            var id = <?php echo $id; ?>;
            var fileid = <?php echo $fileid; ?>;

            if ($('#stime_hh').val() == '' || $('#stime_mm').val() == '' || $('#stime_ss').val() == '') {
                alert('<?php echo get_string('inputstarttime','local_media'); ?>.');
                return false;
            }

            if ($('#etime_hh').val() == '' || $('#etime_mm').val() == '' || $('#etime_ss').val() == '') {
                alert('<?php echo get_string('inputendtime','local_media'); ?>.');
                return false;
            }
            if ($('#textprint').val() == '') {
                alert('<?php echo get_string('inputsubtitlecontent','local_media'); ?>.');
                return false;
            }

            $.ajax({
                url: 'caption_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {
                    amode: 'add',
                    lang: lang,
                    id: id,
                    fileid: fileid,
                    stime_hh: $('#stime_hh').val(),
                    stime_mm: $('#stime_mm').val(),
                    stime_ss: $('#stime_ss').val(),
                    etime_hh: $('#etime_hh').val(),
                    etime_mm: $('#etime_mm').val(),
                    etime_ss: $('#etime_ss').val(),
                    textprint: $('#textprint').val()
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'success') {
                        status = true;
                        caption_get(id, fileid, lang);
                    } else {
                        alert(data.message);
                        status = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                    status = false;
                }
            });

            return status;
        }

        function onlynumber(t) {
            t.value = t.value.replace(/[^0-9]/g, '');
        }

    </script>

    <?php
    echo '';
    echo $OUTPUT->footer();
}
?>



