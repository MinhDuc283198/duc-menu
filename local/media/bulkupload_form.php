<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/formslib.php');

class local_media_bulkupload_form extends moodleform {

    public static function attachment_options() {
        global $COURSE, $PAGE, $CFG;
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'subdirs' => 0,
            'maxbytes' => $maxbytes,
            'maxfiles' => 1,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL
        );
    }
    
    function definition() {
        global $CFG, $DB, $USER, $OUTPUT;

        $mform = $this->_form;
        $context = $this->_customdata['context'];
        $mode = $this->_customdata['mode'];

        $mform->addElement('header', 'defaultheader', get_string('basicinfo','local_media'));
        $mform->setExpanded('defaultheader', true);

        // 엑셀 파일
        $mform->addElement('filemanager', 'excelfile', 'Excel '.get_string('file','local_media'), null, self::attachment_options());
        $mform->addRule('excelfile', null, 'required', null, 'client');
        
        // 샘플 엑셀 다운로드
        $mform->addElement('static', 'excelsample', get_string('excelsampletitle','local_media'), '<a onclick="location.href=\'sample/media_bulkupload.xlsm\';">'.get_string('excelsampledownload','local_media').'</a>');

        // 업로드파일명
        $mform->addElement('hidden', 'uploaded_file');
        $mform->setType('uploaded_file', PARAM_RAW);
        //$mform->addRule('uploaded_file', null, 'required', null);
        
        // Set mode
        $mform->addElement('hidden', 'mode');
        $mform->setType('mode', PARAM_RAW);
        $mform->setDefault('mode', $mode);

        $this->add_action_buttons();
    }

    public static function editor_options($context, $contentid = null) {
        global $PAGE, $CFG;
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext' => true,
            'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
            'subdirs' => file_area_contains_subdirs($context, 'local_media', 'contents', $contentid)
        );
    }

    function validation($data, $files) {

        $errors = parent::validation($data, $files);

        if (!$data['uploaded_file']) {
            $errors['uploaded_file'] = get_string('addvideofile','local_media').'.';
            echo '<script>alert(\''.get_string('addvideofile','local_media').'\');</script>';
        }

        return $errors;
    }

}
