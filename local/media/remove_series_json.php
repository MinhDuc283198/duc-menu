<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$id = optional_param('id', 0, PARAM_INT);

$context = context_system::instance();
require_login();

$result = local_media_delete_group($id);
echo json_encode($result);
?>


