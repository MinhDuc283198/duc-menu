<?php

if ( $hassiteconfig ){
 
    $settings = new admin_settingpage( 'local_media', get_string('pluginname', 'local_media') );

    $ADMIN->add( 'localplugins', $settings );

    $settings->add(
        new admin_setting_configtext(
            'local_media_player_url',
            get_string('setting:playerurl', 'local_media'),
            get_string('setting:playerurldesc', 'local_media'),
            '',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_storage_path',
            get_string('setting:storagepath', 'local_media'),
            get_string('setting:storagepathdesc', 'local_media'),
            '/appdata/lcmsdata/media',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_xml_path',
            get_string('setting:xmlpath', 'local_media'),
            get_string('setting:xmlpathdesc', 'local_media'),
            '/appdata/xml',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_file_path',
            get_string('setting:filepath', 'local_media'),
            get_string('setting:filepathdesc', 'local_media'),
            '/appdata/lcmsdata/files',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_system_url',
            get_string('setting:requestsystemurl', 'local_media'),
            get_string('setting:requestsystemurldesc', 'local_media'),
            'http://165.243.186.203:9050',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_system_id',
            get_string('setting:requestsystemid', 'local_media'),
            get_string('setting:requestsystemiddesc', 'local_media'),
            '',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_system_password',
            get_string('setting:requestsystempassword', 'local_media'),
            get_string('setting:requestsystempassworddesc', 'local_media'),
            '',
            PARAM_TEXT
    ) );
    
    $settings->add(
        new admin_setting_configtext(
            'local_media_youtube_key',
            get_string('setting:youtubekeyvalue', 'local_media'),
            get_string('setting:youtubekeyvaluedesc', 'local_media'),
            '',
            PARAM_TEXT
    ) );
}
