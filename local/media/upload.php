<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/lib/filelib.php";
require_once $CFG->dirroot . '/lib/form/filemanager.php';
require_once $CFG->dirroot . "/local/media/config.php";
require_once $CFG->dirroot . "/local/media/lib.php";
require_once $CFG->dirroot . "/local/lmsdata/lib.php";
require_once $CFG->dirroot . '/local/media/upload_form.php';

$id = optional_param('id', 0, PARAM_INT); // Moodle Repository DB id
$mode = optional_param('mode', 'default', PARAM_RAW);
$page = optional_param('page', 1, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$sort = optional_param('sort', 'id', PARAM_RAW);

$context = context_system::instance();
require_login();

$auth = local_media_userauth();

if (!$auth) {
    redirect($CFG->wwwroot);
    die();
}

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->set_context($context);
$PAGE->set_pagelayout('subpage');
$PAGE->set_url('/local/media/upload.php', array('mode' => $mode, 'id' => $id));

$strplural = ':DTube';

$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

// Get Record from DB
if ($id) {

    $content = $DB->get_record('lmsmedia_contents', array('id' => $id));

    /*
      if($auth->auth == 'HRD Training Manager' && $USER->id != $content->userid){
      redirect($CFG->wwwroot);
      die();
      }
     */
    $contentmanage = local_media_manage_check($auth, $content);
}

$options = array('noclean' => true, 'subdirs' => true, 'maxfiles' => -1, 'maxbytes' => 0, 'context' => $context);

$mform = new local_media_upload_form(null, array('options' => $options,
    'context' => $context,
    'contents' => $content,
    'mode' => $mode,
    'search' => $search,
    'sort' => $sort,
    'page' => $page,
    'id' => $id)
);

if ($mform->is_cancelled()) {

    redirect("list.php?search=" . $search . '&sort=' . $sort . '&page=' . $page);
    die();
} else if ($fromform = $mform->get_data()) {

    if ($content) {

        local_media_update_instance($fromform, $id, $context);
        redirect("upload.php?id=" . $id . "&mode=" . $fromform->mode . "&search=" . $search . '&sort=' . $sort . '&page=' . $page);
        die();
    } else {

        local_media_add_instance($fromform, $context);
        redirect("list.php");
        die();
    }
} else {

    $draftid_editor = file_get_submitted_draft_itemid('intro');

    if ($content) {

        $media_file = null;
        if ($content->contype == 'video') {
            $media_file = $DB->get_record('lmsmedia_files', array('mediaid' => $id));
        }

        $currenttext = file_prepare_draft_area($draftid_editor, $context->id, 'local_media', 'intro', $id, local_media_upload_form::editor_options($context, $id), $content->intro);

// edit form
        $form_data = array(
            'title' => htmlspecialchars_decode($content->title),
            'intro' => array(
                'text' => $currenttext,
                'format' => 1,
                'itemid' => $draftid_editor
            ),
            'contype' => $content->contype,
            'target' => $content->target,
            'gubun' => $content->gubun,
            'groupid' => $content->groupid,
            'category1' => $content->category1,
            'category2' => $content->category2,
            'category3' => $content->category3,
            'isshared' => $content->isshared,
            'notice' => $content->isnoticed,
            'timestartnotice' => $content->timestartnotice,
            'timeendnotice' => $content->timeendnotice,
            'categorytags' => htmlspecialchars_decode($content->categorytags),
            'keywords' => htmlspecialchars_decode($content->keywords),
            'mediafile' => $content->mediafile,
            'isused' => $content->isused,
            'teacher' => $content->teacher,
            'ccmark' => $content->ccmark == '1' || $content->ccmark == '2' ? $content->ccmark : '3',
            'ccmarktext' => $content->ccmark != '1' && $content->ccmark != '2' ? $content->ccmark : '',
            'islogo' => $content->islogo,
            'inouttype' => $content->inouttype,
            'expiredate' => $content->expiredate
        );

        // 시리즈 그룹정보
        $media_group = $DB->get_record('lmsmedia_groups', array('id' => $content->groupid));
        $form_data['series_group'] = $media_group->name;

        // 등록한 사용자 정보
        $sql = "select u.id, lu.organizationname, lu.titlename, u.firstname, u.lastname from {user} u left join {lmsdata_user} lu on u.id=lu.userid where u.id = :userid";
        $userinfo = $DB->get_record_sql($sql, array('userid' => $content->userid));

        //공유범위 선택 테이터

        switch ($content->target) {
            case '1':
                $sql = "select mc.id, mc.corporationcode, c.name from {lmsmedia_corps} mc join {lmsdata_corporation} c on "
                        . "c.code = mc.corporationcode where mc.mediaid = :mediaid order by mc.id asc";
                $media_corps_list = $DB->get_records_sql($sql, array('mediaid' => $id));

                if ($media_corps_list) {
                    $corps_id = '';
                    $selected_target = '<ul>';
                    foreach ($media_corps_list as $media_corps) {
                        $corps_id .= $media_corps->corporationcode . '#DMT#';
                        $selected_target .= '<li>' . $media_corps->corporationcode . '</li>';
                    }
                    $selected_target .= '</ul>';
                    $form_data['corps_id'] = substr($corps_id, 0, -5);
                }
                break;
            case '2':
                $sql = "select ulc.* from 
                (select mu.id, mu.userid, mu.gubun, mu.organizationcode, org.name as organizationname,  '' as firstname, '' as lastname, '' as titlename 
                from tb_lmsmedia_users mu 
                join tb_lmsdata_organization org on org.code = mu.organizationcode  
                where mu.mediaid = :mediaid1 and mu.gubun = '1' 
                union 
                select mu.id, mu.userid, mu.gubun, mu.organizationcode, lu.organizationname, u.firstname, u.lastname, lu.titlename from tb_lmsmedia_users mu 
                join tb_lmsdata_user lu on lu.userid = mu.userid 
                join tb_user u on u.id = lu.userid 
                where mu.mediaid = :mediaid2 and mu.gubun = '2') as ulc 
                order by ulc.id asc;";
                $media_users_list = $DB->get_records_sql($sql, array('mediaid1' => $id, 'mediaid2' => $id));

                if ($media_users_list) {
                    $user_ids = '';
                    $selected_targetusers = '<ul>';
                    $selected_targetorgs = '<ul>';
                    $org_codes = '';
                    foreach ($media_users_list as $media_users) {
                        switch ($media_users->gubun) {
                            case '2':
                                $user_ids .= $media_users->userid . '#DMT#';
                                $selected_targetusers .= '<li>' . $media_users->organizationname . ' ' . $media_users->lastname . ' ' . $media_users->titlename . '</li>';
                                break;
                            case '1':
                                $org_code .= $media_users->organizationcode . '#DMT#';
                                $selected_targetorgs .= '<li>' . $media_users->organizationname . '</li>';
                                break;
                            default:
                                break;
                        }
                    }
                    $selected_targetusers .= '</ul>';
                    $selected_targetorgs .= '</ul>';
                    $form_data['user_ids'] = substr($user_ids, 0, -5);
                    $form_data['org_codes'] = substr($org_code, 0, -5);
                }
                break;
            case '3':
            default:
                break;
        }

        //등록된 태그 정보 가져오기
        $categorycodevalues = '';
        if ($categorytags = $DB->get_records('lmsdata_course_tag', array('curriculumid' => $id))) {
            foreach ($categorytags as $tag) {
                $categorycodevalues .= $tag->tagcode . '#DMT#';
            }
            $form_data['categorycodevalues'] = substr($categorycodevalues, 0, -5);
        }

        $media_corps = null;
        $share_mediaurl = local_lmsdata_shared_media_url($content->id, 'player');
        $share_src = local_lmsdata_shared_media_url_link($content->id, 'player');
        $embed_src = local_lmsdata_shared_media_url_link($content->id, 'embed');
        $embed_code = '<iframe width="560" height="315" src="' . $embed_src . '" frameborder="0" allowfullscreen></iframe>';
    } else {
        $currenttext = '';
        $form_data = array(
            'intro' => array(
                'text' => $currenttext,
                'format' => 1,
                'itemid' => $draftid_editor
            )
        );
    }

    $mform->set_data($form_data);

    echo $OUTPUT->header();

    $pluploadlang = (current_language() == 'zh_cn') ? 'zh_CN' : current_language();

    if ($content->contype == 'vimeo' || $content->contype == 'youtube') {
        $embedinfo = local_media_get_embed_info($content->mediafile, $content->contype);
        $media_file->duration = $embedinfo->duration;
    }
    ?>

    <!-- 타이틀-->

    <div class="blocks no_padding">
        <h3 class="title line_style">
            <p class="left button_right <?php echo $content ? '' : 'full'; ?>">
                <?php echo $content ? $content->title : ' <img src="/theme/oklasscompany/pix/images/computer.png" alt="video" title="video" /> ' . get_string('videosubmit', 'local_media') . ''; ?>
            </p>
            <?php if ($content) { ?>
                <p class="right">
                    <input type="button" value="<?php echo get_string('back', 'local_media'); ?>" title="<?php echo get_string('back', 'local_media'); ?>" class="button_style02" onclick="javascript:location.href = 'list.php?search=<?php echo $search; ?>&sort=<?php echo $sort; ?>&page=<?php echo $page; ?>';"/>
                    <?php if ($contentmanage == 1) { ?>
                        <input type="button" value="<?php echo get_string('delete', 'local_media'); ?>" title="<?php echo get_string('delete', 'local_media'); ?>" class="button_cancel" onclick="javascript:delete_contents(<?php echo $id; ?>);"/>
                    <?php } ?>
                </p>
            <?php } ?>
        </h3>
    </div>

    <!-- 타이틀-->

    <link rel="stylesheet" href="js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />
    <link href="/theme/oklasscompany/style/popup.css" rel="stylesheet">
    <script type="text/javascript" src="js/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
    <script type="text/javascript" src="js/i18n/<?php echo $pluploadlang; ?>.js"></script>
    <script type="text/javascript" src="js/moxie.js"></script>
    <script type="text/javascript" src="js/plupload.dev.js"></script>
    <script type="text/javascript" src="./js/youtube/search.js?t=20170207"></script>
    <script type="text/javascript" src="./js/youtube/searchV.js?t=20170207"></script>
    <script src="https://apis.google.com/js/client.js?onload=onClientLoad" type="text/javascript"></script>

    <?php
    if ($content) {
        ?>

        <div id="" class="video_area gray info">
            <?php
            //기본 썸네일을 보여줌
            $thumbimage = '/theme/oklasscompany/pix/images/sample.jpg';
            $defaultthumb = $DB->get_field('lmsmedia_thumbs', 'filepath', array('mediaid' => $id, 'isused' => 1));

            if (file_exists($defaultthumb)) {
                $thumbimage = str_replace($CFG->local_media_file_path, $CFG->wwwroot . '/storage', $defaultthumb);
            } else {
                //vimeo, youtube일 경우는 api로 썸네일 호출
                if ($content->contype == 'vimeo' || $content->contype == 'youtube') {
                    $thumbimage = $embedinfo->thumbnail;
                }
            }

            if ($thumbimage) {
                echo '<image src="' . $thumbimage . '" alt="' . $content->title . '" title="' . $content->title . '"/>';
            }
            if ($content->isenabled == 1) {
                echo '<button onclick="' . $share_mediaurl . '">' . get_string('viewvideo', 'local_media') . '</button>';
            } else {
                echo "<span class='vtext'>" . get_string('isencoding', 'local_media') . "</span>";
            }
            ?>
        </div>

        <div class="video_info sub">
            <h3 class="page_title"><?php echo get_string('videoinfo', 'local_media'); ?></h3>
            <?php
            echo '<div class="filename"><p>' . get_string('contentstype', 'local_media') . '</p><p class="name">' . $content->contype . '</p></div>';
            echo '<div class="filename">' . ($media_file ? '<p>' . get_string('videofilename', 'local_media') . '</p><p class="name">' . $media_file->filename . '</p>' : '') . '</div>';
            echo '<p class="icontext"><span>' . get_string('submitday', 'local_media') . ' : ' . date('Y-m-d H:i:s', $content->timecreated) . '</span></p>';
            echo '<p class="icontext"><span>' . get_string('playtime', 'local_media') . ' : ' . local_media_get_duration_hms($media_file->duration) . '</span></p>';
            echo '<p class="icontext"><span>' . get_string('resister', 'local_media') . ' : ' . $userinfo->organizationname . ' ' . fullname($userinfo) . ' ' . $userinfo->titlename . '</span></p>';
            echo '<p class="icontext"><span>' . get_string('readnum', 'local_media') . ' : ' . $content->viewcount . '</span></p>';
            echo '<p class="icontext"><span>' . get_string('like', 'local_media') . ' : ' . local_media_get_npsscore($id) . '</span></p>';

            $gubun_text = array('1' => 'DTube', '2' => get_string('courseconfirm', 'local_media'), '3' => get_string('alluse', 'local_media'));
            echo '<p class="icontext"><span>' . get_string('category', 'local_media') . ' : ' . $gubun_text[$content->gubun] . '</span></p>';
            $isused_text = array('1' => get_string('use', 'local_media'), '2' => get_string('notuse', 'local_media'));
            echo '<p class="icontext"><span>' . get_string('useornot', 'local_media') . ' : ' . $isused_text[$content->isused] . '</span></p>';
            ?>                       
        </div>
        <?php
        if ($contentmanage == 1) {
            ?>
            <div class="file_info">
                <ul class="tab">
                    <li id="tab_default"><a href="javascript:load_edit_form('default');"><?php echo get_string('basicinfo', 'local_media'); ?></a></li>
                    <li id="tab_addition"><a href="javascript:load_edit_form('addition');"><?php echo get_string('addedinfo', 'local_media'); ?></a></li>
                    <li id="tab_thumbnail"><a href="javascript:load_edit_form('thumbnail');"><?php echo get_string('thumnail', 'local_media'); ?></a></li>
                    <li id="tab_caption"><a href="javascript:load_edit_form('caption');"><?php echo get_string('subtitle', 'local_media'); ?></a></li>
                    <li id="tab_embed"><a href="javascript:load_edit_form('embed');"><?php echo get_string('share', 'local_media'); ?></a></li>
                </ul>
            </div>                
            <?php
        }
    }
    ?>

    <!-- 법인별 설정 -->
    <div class="_corps" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1110; background-color:#757b8c; opacity:0.9;display:none;"></div>
    <div id="pop_wrap" class="search_corps _corps" style="position:fixed; top:5%; left:10%; width:60%; height:90% !important; z-index:1111; padding:20px; display:none;">
        <div class="pop_title inbutton">
            <h2><?php echo get_string('coselect', 'local_media'); ?></h2>
            <p class="popclose">
                <a href="#" onclick="javascript:corpsCtrl.closeCorpsPop();return false;"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></a>
            </p>
        </div>
        <div id="contents" style="overflow-y:scroll;">
            <div class="serise_list">
                <ul id="corps_list"></ul>
            </div>
        </div>
        <div class="bottom_buttons center">
            <button class="save button_style01 gray" onclick="javascript:corpsCtrl.setCorps();"><?php echo get_string('select', 'local_media'); ?></button>
        </div>
    </div>

    <!-- 대상자 지정 -->
    <div class="_useauth _user_ids" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1110; background-color:#757b8c; opacity:0.9;display:none;"></div>
    <div id="pop_wrap" class="auth_search _useauth _user_ids" style="position:fixed; top:10%; left:10%; width:80%; height:80% !important; z-index:1111; padding:20px; display:none;">
        <div class="pop_title inbutton">
            <h2>User <?php echo get_string('authoritysearch', 'local_media'); ?></h2>
            <p class="popclose">
                <a href="#" onclick="javascript:userCtrl.closeUserIdsPop();return false;"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></a>
            </p>
        </div>
        <div id="contents">
            <div class="search_style01">
                <label for="department"><?php echo get_string('department', 'local_media'); ?></label>
                <input type="text" id="search_orgname" class="margin-input" onkeydown="javascript:if (event.keyCode == 13) {
                            userCtrl.searchUserIds();
                            return false;
                        }"/>
                <label for="user"><?php echo get_string('user', 'local_media'); ?></label>
                <input type="text" id="search_username" onkeydown="javascript:if (event.keyCode == 13) {
                            userCtrl.searchUserIds();
                            return false;
                        }"/>
                <input type="submit" class="button_style01" value="<?php echo get_string('search1', 'local_media'); ?>" onclick="javascript:userCtrl.searchUserIds();">
            </div>

            <div class="scroll_table">
                <table class="table table-border table-condensed">
                    <colgroup>
                        <col width="30%" />
                        <col width="25%" />
                        <col width="25%" />
                        <col width="20%" />
                    </colgroup>
                    <thead>
                    <th><?php echo get_string('workcate', 'local_media'); ?></th>
                    <th><?php echo get_string('name', 'local_media'); ?></th>
                    <th><?php echo get_string('rank', 'local_media'); ?></th>
                    <th><?php echo get_string('depthnum', 'local_media'); ?></th>
                    </thead>
                    <tbody id="user_list"></tbody>
                </table>
            </div>

            <div class="scroll_table" style="max-height:120px;overflow-y:scroll;">
                <table class="table table-border table-condensed">
                    <colgroup>
                        <col width="30%" />
                        <col width="25%" />
                        <col width="25%" />
                        <col width="20%" />
                    </colgroup>
                    <thead>
                    <th><?php echo get_string('workcate', 'local_media'); ?></th>
                    <th><?php echo get_string('name', 'local_media'); ?></th>
                    <th><?php echo get_string('rank', 'local_media'); ?></th>
                    <th><?php echo get_string('depthnum', 'local_media'); ?></th>
                    </thead>
                    <tbody id="selected_user_list" >
                        <?php
                        if ($id) {
                            $sql = "select mu.id, mu.userid, u2.username, concat(u2.firstname,u2.lastname) as fullname, o.name, o.code, u.titlename, u2.phone1, u2.phone2 from {lmsmedia_users} mu "
                                    . "join {lmsdata_user} u on u.userid = mu.userid "
                                    . "join {lmsdata_organization} o on o.code = u.organizationcode "
                                    . "join {user} u2 on u2.id = u.userid "
                                    . "where mu.mediaid = :mediaid order by mu.id asc";
                            $selectedusers = $DB->get_records_sql($sql, array('mediaid' => $id));
                            foreach ($selectedusers as $select) {
                                $userfullname = $select->name . ' ' . $select->fullname . ' ' . $select->titlename;
                                echo '<tr onclick="javascript:userCtrl.deleteUserIds(\'' . $select->userid . '\')" id="selected_user_' . $select->userid . '" class="_user_ids" user_name="' . $userfullname . '" user_id="' . $select->userid . '">';
                                echo '<td>' . $select->name . '</td>';
                                echo '<td>' . $select->fullname . '</td>';
                                echo '<td>' . $select->titlename . '</td>';
                                echo '<td>' . $select->username . '</td>';
                                echo '</tr>';
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <div class="bottom_buttons center">
                <button class="save button_style01 gray" onclick="javascript:userCtrl.setUserIds();"><?php echo get_string('select', 'local_media'); ?></button>
            </div>

        </div>
    </div>

    <div class="_useauth _org_codes" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1110; background-color:#757b8c; opacity:0.9;display:none;"></div>
    <div id="pop_wrap" class="auth_search _useauth _org_codes" style="position:fixed; top:10%; left:10%; width:80%; height:80% !important; z-index:1111; padding:20px; display:none;">
        <div class="pop_title inbutton">
            <h2>Group <?php echo get_string('authoritysearch', 'local_media'); ?></h2>
            <p class="popclose">
                <a href="#" onclick="javascript:userCtrl.closeOrgCodesPop();return false;"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></a>
            </p>
        </div>
        <div id="contents">
            <div class="search_style01">
                <label for="department"><?php echo get_string('department', 'local_media'); ?></label>
                <input type="text" id="search_org" class="margin-input"  onkeydown="javascript:if (event.keyCode == 13) {
                            userCtrl.searchOrgCodes();
                            return false;
                        }"/>
                <input type="submit" class="button_style01" value="<?php echo get_string('search', 'local_media'); ?>" onclick="javascript:userCtrl.searchOrgCodes();">
            </div>

            <div class="scroll_table">
                <table class="table table-border table-condensed">
                    <colgroup>
                        <col width="40%" />
                        <col width="60%" />
                    </colgroup>
                    <thead>
                    <th><?php echo get_string('catecode', 'local_media'); ?></th>
                    <th><?php echo get_string('department', 'local_media'); ?></th>
                    </thead>
                    <tbody id="organization_list"></tbody>
                </table>
            </div>

            <div class="scroll_table" style="max-height:120px;overflow-y:scroll;">
                <table class="table table-border table-condensed">
                    <colgroup>
                        <col width="40%" />
                        <col width="60%" />
                    </colgroup>
                    <thead>
                    <th><?php echo get_string('catecode', 'local_media'); ?></th>
                    <th><?php echo get_string('department', 'local_media'); ?></th>
                    </thead>
                    <tbody id="selected_org_list" >

                        <?php
                        if ($id) {
                            $sql = "select mu.id, o.name, o.code from {lmsmedia_users} mu "
                                    . "join {lmsdata_organization} o on o.code = mu.organizationcode "
                                    . "where mu.mediaid = :mediaid order by mu.id asc";
                            $selectedorgs = $DB->get_records_sql($sql, array('mediaid' => $id));

                            foreach ($selectedorgs as $select) {
                                echo '<tr onclick="javascript:userCtrl.deleteOrgCodes(\'' . $select->code . '\')" id="selected_org_' . $select->code . '" class="_org_codes" org_name="' . $select->name . '" org_code="' . $select->code . '">';
                                echo '<td>' . $select->code . '</td>';
                                echo '<td>' . $select->name . '</td>';
                                echo '</tr>';
                            }
                        }
                        ?>

                    </tbody>
                </table>
            </div>

            <div class="bottom_buttons center">
                <button class="save button_style01 gray" onclick="javascript:userCtrl.setOrgCodes();">선택</button>
            </div>

        </div>
    </div>

    <!-- 시리즈 검색 -->
    <div class="_series" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1110; background-color:#757b8c; opacity:0.9;display:none;"></div>
    <div id="pop_wrap" class="search_serise _series" style="position:fixed; top:20%; left:10%; width:80%; height:70% !important; z-index:1111; padding:20px; display:none;">
        <div class="pop_title inbutton">
            <h2><?php echo get_string('searchseries', 'local_media'); ?></h2>
            <p class="popclose">
                <a href="#" onclick="javascript:seriesCtrl.closeSeriesGroupPop();
                        return false;"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></a>
            </p>
        </div>
        <div id="contents" style="overflow-y:scroll;">
            <div class="search_style01">
                <input type="text" id="search_series_name" class="serise_input" placeholder="<?php echo get_string('searchseriesname', 'local_media'); ?>" value="" onkeydown="javascript:if (event.keyCode == 13) {
                            seriesCtrl.searchSeries();
                            return false;
                        }"/>
                <input type="submit" class="button_style01 gray" value="<?php echo get_string('search1', 'local_media'); ?>" onclick="javascript:seriesCtrl.searchSeries();">
                <input type="submit" class="button_style01" value="<?php echo get_string('submit', 'local_media'); ?>" onclick="javascript:seriesCtrl.registerSeriesForm();">
            </div>
            <div class="_series_form search_style01" id="register_series_form" style="display:none;">
                <input type="text" id="register_series_name" class="serise_input" placeholder="<?php echo get_string('inputseries', 'local_media'); ?>" value=""/>
                <input type="submit" class="button_style01 gray" value="<?php echo get_string('submit', 'local_media'); ?>" onclick="javascript:seriesCtrl.registerSeries();">
            </div>


            <div class="_series_form search_style01" id="modify_series_form" style="display:none;">
                <img title="close" alt="close" src="/theme/oklasscompany/pix/images/close.png" />
                <input type="hidden" id="modify_series_id" />
                <input type="text" id="modify_series_name" class="serise_input" placeholder="<?php echo get_string('inputseries', 'local_media'); ?>" value=""/>
                <input type="submit" class="button_style01" value="<?php echo get_string('modify', 'local_media'); ?>" onclick="javascript:seriesCtrl.modifySeries();">
                <?php if (is_siteadmin()) { ?>
                    <input type="submit" class="button_style01 gray" value="<?php echo get_string('delete', 'local_media'); ?>" onclick="javascript:seriesCtrl.removeSeries();">
                <?php } ?>
            </div>


            <div class="serise_list">
                <ul id="series_list"></ul>
            </div>
        </div>
    </div>

    <!-- 검색태그 검색 -->
    <div class="_tags" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1110; background-color:#757b8c; opacity:0.9;display:none;"></div>
    <div id="pop_wrap" class="autoheight _tags" style="position:fixed; top:10%; left:10%; width:80%; height:80% !important; z-index:1111; padding:20px; display:none;">
        <div class="pop_title">
            <h2><?php echo get_string('selectedcate', 'local_media'); ?> tag</h2>
            <p class="popclose">
                <a href="#" onclick="tagsCtrl.closeTagsPop();
                        return false;"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close" /></a>
            </p>
        </div>

        <div id="contents" style="overflow-y:scroll;">
            <div class="filter" id="tags_selected"></div>

            <div class="explain marginTop pink">
                <?php echo get_string('mediatext10', 'local_media'); ?>.<br />
                <?php echo get_string('mediatext11', 'local_media'); ?>.

                <input style="float:right;" type="button" value="<?php echo get_string('select', 'local_media'); ?>" class="button_style02" onclick="javascript:tagsCtrl.setSelectedTags();
                        tagsCtrl.closeTagsPop();
                        return false;">
            </div>

            <div class="">
                <div id="tags_list" class="listgroup"></div>
            </div>
        </div>                
    </div>

    <div id="loading" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1200; background-color:#757b8c; opacity:0.7; display:none;">
        <div style="position: absolute; top: 50%; left: 47%;"><img src="loading.gif"></div>
    </div>


    <script type="text/javascript">
        function seriessort(groupid) {
            var tag = $("<div id='course_select_popup'></div>");
            $.ajax({
                url: 'sort_series.php',
                method: 'POST',
                data: {
                    groupid: groupid
                },
                success: function (data) {
                    tag.html(data).dialog({
                        title: '<?php echo get_string('series_sort_button', 'local_media'); ?>',
                        modal: true,
                        width: 600,
                        resizable: false,
                        height: 600,
                        buttons: [{id: 'save',
                                text: '저장',
                                click: function () {
                                    var btnSave = $(this).parent().find("button[id='save']");
                                    btnSave.button('disable');

                                    var status = sort_series_change(groupid);
                                    if (status) {
                                        $('#frm_coupon_search').remove();
    //                                    $('#frm_category_search').submit();
                                        $(this).dialog("close");
                                    } else {
                                        btnSave.button('enable');
                                    }
                                }},
                            {id: 'close',
                                text: '<?php echo get_string('cancle', 'local_lmsdata'); ?>',
                                disable: true,
                                click: function () {
                                    $(this).dialog("close");
                                }}],
                        close: function () {
                            $('#course_select_popup').remove();
    //                        $(this).dialog('destroy').remove()
                        }
                    }).dialog('open');
                }
            });
        }
        function sort_series_change(groupid) {
            var temp = [];
            var obj = $('select[id="sortselect"]');
            var result = false;
            $(obj).each(function (i) {
                temp[i] = $(this).val();
            });
            $(temp).each(function (i) {
                var x = 0;
                $(obj).each(function () {
                    if (temp[i] == $(this).val()) {
                        x++;
                    }
                });
                if (x > 1) {
                    alert('<?php echo get_string('series_sort_nocomplete', 'local_media'); ?>');
                    result = false;
                    return false;
                }else{
                    result = true;
                }
            });
            if(result == false){
                 return false;
            }
            var status = true;
            $.ajax({
                url: 'sort_series.ajax.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: $('form[name=mainForm]').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.status == 'success') {
                        status = true;
                        alert('<?php echo get_string('series_sort_complete', 'local_media'); ?>');
                    } else {
                        alert(data.message);
                        status = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                    status = false;
                }
            });
            return status;
        }
        var loadingCtrl = {
            startLoading: function () {
                $('#loading').show();
            },
            endLoading: function () {
                $('#loading').hide();
            }
        };
        var thumbnailCtrl = {
            getList: function (id) {
                loadingCtrl.startLoading();

                var self = this;
                $.get('thumbnail_list_json.php?id=' + id, function (thumbnailList) {
                    if (typeof thumbnailList === 'string') {
                        thumbnailList = JSON.parse(thumbnailList);
                    }
                    var htmlTemplate = '<li style="margin:10px;padding:0px;float:left;"><input type="radio" name="radio_thumb" class="_radio_thumb" value="__ID__" __CHECKED__><img src="__FULL_PATH__" width="100"><a href="" onclick="javascript:thumbnailCtrl.removeThummbnail(__ID__);"><img src="/theme/oklasscompany/pix/images/close.png" alt="close" title="close"></a></li>';
                    var thumbnailListHtml = '';
                    for (var key in thumbnailList) {
                        thumbnailListHtml += htmlTemplate.replace(/__FULL_PATH__/g, thumbnailList[key].filepath + '/' + thumbnailList[key].filename)
                                .replace(/__ID__/g, thumbnailList[key].id)
                                .replace(/__CHECKED__/g, thumbnailList[key].isused == 1 ? 'checked="checked"' : '');
                    }

                    $('#thumbnail_list').html(thumbnailListHtml);

                    $('input._radio_thumb').click(function (e) {
                        $('input[name=thumsid]').val($(this).val());
                    });

                    loadingCtrl.endLoading();
                });
            },
            removeThummbnail: function (id) {
                var self = this;
                $.post('remove_thumbnail_json.php',
                        {id: id},
                        function (data) {
                            if (typeof data === 'string') {
                                data = JSON.parse(data);
                            }

                            switch (data.result) {
                                case 'P':
                                    alert("<?php echo get_string('parametererror', 'local_media'); ?>");
                                    break;
                                case 'Y':
                                    alert("<?php echo get_string('deleted', 'local_media'); ?>.");
                                    location.reload();
                                    break;
                            }
                        }
                );
            }
        };
        var uploadAreaCtrl = {
            closeUploadDiv: function () {
                $('div._upload_div').hide();
                $('.uploadlist').removeClass('on');
            },
            openUploadDiv: function (category) {
                this.closeUploadDiv();
                $('#' + category + '_uploader').show();
                $('#' + category + '_uploadlist').addClass('on');

                $('input[name=upload_type]').val(category);
            }
        };
        var youtubeCtrl = {
            setYoutubeUrl: function (url) {
                $('#youtube_url').val(url);
                $('input[name=mediafile]').val(url);
                this.closeYoutubePop();
            },

            openYoutubePop: function () {
                $('div._youtube').show();
            },

            closeYoutubePop: function () {
                $('div._youtube').hide();
            }
        };
        var vimeoCtrl = {
            setVimeoUrl: function (url) {
                $('#vimeo_url').val(url);
                $('input[name=mediafile]').val(url);

                this.closeVimeoPop();
            },

            openVimeoPop: function () {
                $('div._vimeo').show();
            },

            closeVimeoPop: function () {
                $('div._vimeo').hide();
            }
        };
        var seriesCtrl = {
            setSeriesGroup: function (id, name) {
                $('input[name=series_group]').val(name);
                $('input[name=groupid]').val(id);

                this.closeSeriesGroupPop();
            },

            openSeriesGroupPop: function () {
                $('#search_series_name').val('');
                $('div._series').show();
            },

            closeSeriesGroupPop: function () {
                $('div._series').hide();
                $("body").css("overflow-y", "auto");
            },

            searchSeries: function (callback) {
                loadingCtrl.startLoading();
                
                var seriesName = encodeURIComponent($('#search_series_name').val());

                var self = this;
                $.get('search_series_json.php?name=' + seriesName, function (seriesList) {
                    if (typeof seriesList === 'string') {
                        seriesList = JSON.parse(seriesList);
                    }
                    var iseditauth = '<?php echo is_siteadmin(); ?>';
                    var myuserid = '<?php echo $USER->id ?>';
                    var myserieslist = new Array();
                    var htmlTemplate = '<li><span class="serise_text">'
                            + '__NAME__<img src="/theme/oklasscompany/pix/images/pencil..png" height="13px" id = "editicon___ID__" style="display:none;" onclick="javascript:seriesCtrl.modifySeriesForm(__ID__, \'__NAME__\');"/></span>'
                            + '<input type="button" value="<?php echo get_string('select', 'local_media'); ?>" class="button_style02" onclick="javascript:seriesCtrl.setSeriesGroup(__ID__, \'__NAME__\')"/></li>';
                    var seriesListHtml = '';
                    for (var key in seriesList) {
                        seriesListHtml += htmlTemplate.replace(/__ID__/g, seriesList[key].id)
                                .replace(/__NAME__/g, seriesList[key].name);
                        if (iseditauth || myuserid == seriesList[key].userid) {
                            myserieslist.push(seriesList[key].id);
                        }
                    }

                    $('#series_list').html(seriesListHtml);
                    for (var key in myserieslist) {
                        $('#editicon_' + myserieslist[key]).show();
                    }

                    $("body").css("overflow-y", "hidden");
                    if (typeof callback === 'function') {
                        callback();
                    }

                    loadingCtrl.endLoading();
                });
            },

            registerSeriesForm: function () {
                $('._series_form').hide();
                $('#register_series_form').show();
            },
            registerSeries: function () {
                if ($('#register_series_name').val().length < 1) {
                    alert('<?php echo get_string('inputseries', 'local_media'); ?>.');
                    $('#register_series_name').focus();
                    return;
                }

                var self = this;
                $.post('register_series_json.php',
                        {name: $('#register_series_name').val()},
                        function (data) {
                            if (typeof data === 'string') {
                                data = JSON.parse(data);
                            }

                            switch (data.result) {
                                case 'D':
                                    alert("<?php echo get_string('alreadyname', 'local_media'); ?>.");
                                    break;
                                case 'P':
                                    alert("<?php echo get_string('parametererror', 'local_media'); ?>");
                                    break;
                                case 'Y':
                                    alert("<?php echo get_string('submited', 'local_media'); ?>.");

                                    $('#search_series_name').val($('#register_series_name').val());
                                    self.searchSeries();
                                    $('#register_series_name').val('');
                                    break;
                            }

                            $('._series_form').hide();
                        }
                );
            },

            modifySeriesForm: function (id, name) {
                $('#modify_series_id').val(id);
                $('#modify_series_name').val(name);
                $(document).off("mouseup");

                $('._series_form').hide();
                $('#modify_series_form').show();
                $("#modify_series_form img[title='close']").click(function () {
                    $("#modify_series_form ").hide();
                });
                var offset = $("#pop_wrap.search_serise").position();
                /*상대좌표*/
                var y = event.clientY - (offset.top + 60);
                var x = event.clientX - offset.left;

                $("#modify_series_form ").css({"position": "absolute", "top": y, "left": x});

                $(document).mouseup(function (e) {
                    if ($(e.target).parents('#modify_series_form').length == 0 & $(e.target).attr("id") != "modify_series_form") {
                        $("#modify_series_form").hide();
                        $(document).off("mouseup");
                    }
                });


            },
            modifySeries: function () {
                if ($('#modify_series_name').val().length < 1) {
                    alert('<?php echo get_string('inputseries', 'local_media'); ?>.');
                    $('#modify_series_name').focus();
                    return;
                }

                var self = this;
                $.post('modify_series_json.php',
                        {id: $('#modify_series_id').val(), name: $('#modify_series_name').val()},
                        function (data) {
                            if (typeof data === 'string') {
                                data = JSON.parse(data);
                            }

                            switch (data.result) {
                                case 'P':
                                    alert("<?php echo get_string('parametererror', 'local_media'); ?>");
                                    break;
                                case 'Y':
                                    alert("<?php echo get_string('changed', 'local_media'); ?>.");

                                    $('#search_series_name').val($('#modify_series_name').val());
                                    self.searchSeries();
                                    $('#modify_series_name').val('');
                                    break;
                            }

                            $('._series_form').hide();
                        }
                );
            },
            removeSeries: function () {
                var self = this;
                var conf = confirm('<?php echo get_string('confirmdelete', 'local_media'); ?>');
                if (conf) {
                    $.post('remove_series_json.php',
                            {id: $('#modify_series_id').val()},
                            function (data) {
                                if (typeof data === 'string') {
                                    data = JSON.parse(data);
                                }

                                switch (data.result) {
                                    case 'P':
                                        alert("<?php echo get_string('parameter', 'local_media'); ?>");
                                        break;
                                    case 'Y':
                                        alert("<?php echo get_string('deleted', 'local_media'); ?>.");

                                        $('#search_series_name').val('');
                                        self.searchSeries();
                                        $('#modify_series_name').val('');
                                        break;
                                }

                                $('._series_form').hide();
                            }
                    );
                }
            }
        };

        var categoryCtrl = {
            category1: {},
            category2: {},
            category3: {},

            init: function (callback) {
                this.setCategories(callback);
            },

            setCat1Options: function (callback) {
                $('select[name=categoryBig]').empty().append('<option value=""><?php echo get_string('p_cate', 'local_media'); ?></option>');
                for (var cat1Key in this.category1) {
                    var cat1Item = this.category1[cat1Key];
                    $('select[name=categoryBig]').append('<option value="' + cat1Key + '">' + cat1Item + '</option>');
                }

                if (typeof callback === 'function') {
                    callback();
                } else {
                    this.onChangeCat1();
                }
            },

            onChangeCat1: function () {
                var sCat1Key = $('select[name=categoryBig] option:selected').val();
                $('select[name=categoryMiddle]').empty().append('<option value=""><?php echo get_string('c_cate', 'local_media'); ?></option>');
                ;
                for (var cat2Key in this.category2[sCat1Key]) {
                    var cat2Item = this.category2[sCat1Key][cat2Key];
                    $('select[name=categoryMiddle]').append('<option value="' + cat2Key + '">' + cat2Item + '</option>');
                }

                $('input[name=category1]').val(sCat1Key);

                this.onChangeCat2();
            },

            onChangeCat2: function () {
                $('select[name=categorySmall]').empty().append('<option value=""><?php echo get_string('s_cate', 'local_media'); ?></option>');
                ;
                var sCat2Key = $('select[name=categoryMiddle] option:selected').val();
                for (var cat3Key in this.category3[sCat2Key]) {
                    var cat3Item = this.category3[sCat2Key][cat3Key];
                    $('select[name=categorySmall]').append('<option value="' + cat3Key + '">' + cat3Item + '</option>');
                }

                $('input[name=category2]').val(sCat2Key);

                this.onChangeCat3();
            },

            onChangeCat3: function () {
                var sCat3Key = $('select[name=categorySmall] option:selected').val();
                $('input[name=category3]').val(sCat3Key);
            },

            setCategories: function (callback) {
                var self = this;
                var bigCode = 'LGD_ACT_DEFINITIONS_VS';
                var middleCode = 'LGD_ACT_CATEGORY_VS';
                var smallCode = 'LGD_ACT_SMALL_CATEGORY_VS';

                $.get('search_categories_json.php?code=' + bigCode, function (bigCategoryList) {
                    if (typeof bigCategoryList === 'string') {
                        bigCategoryList = JSON.parse(bigCategoryList);
                    }

                    for (var bKey in bigCategoryList) {
                        self.category1[bigCategoryList[bKey].codevalue] = bigCategoryList[bKey].name;
                    }

                    $.get('search_categories_json.php?code=' + middleCode, function (middleCategoryList) {
                        if (typeof middleCategoryList === 'string') {
                            middleCategoryList = JSON.parse(middleCategoryList);
                        }

                        for (var mKey in middleCategoryList) {
                            var middleCodeValue = middleCategoryList[mKey].codevalue;
                            var middleCodeName = middleCategoryList[mKey].name;

                            for (var bKey in bigCategoryList) {
                                var tmpKey = Number(bKey).toString();

                                if (middleCodeValue.indexOf(tmpKey) == 0) {
                                    if (typeof self.category2[bKey] === 'undefined') {
                                        self.category2[bKey] = {};
                                    }
                                    self.category2[bKey][middleCodeValue] = middleCodeName;
                                    break;
                                }
                            }
                        }

                        $.get('search_categories_json.php?code=' + smallCode, function (smallCategoryList) {
                            if (typeof smallCategoryList === 'string') {
                                smallCategoryList = JSON.parse(smallCategoryList);
                            }

                            for (var mKey in smallCategoryList) {
                                var smallCodeValue = smallCategoryList[mKey].codevalue;
                                var smallCodeName = smallCategoryList[mKey].name;

                                for (var mKey in middleCategoryList) {

                                    var tmpKey = Number(mKey).toString();

                                    if (smallCodeValue.indexOf(tmpKey) == 0) {
                                        if (typeof self.category3[mKey] === 'undefined') {
                                            self.category3[mKey] = {};
                                        }
                                        self.category3[mKey][smallCodeValue] = smallCodeName;
                                        break;
                                    }
                                }
                            }

                            self.setCat1Options(callback);
                        });
                    });
                });
            }
        };

        var tagsCtrl = {
            setTags: function (callback) {
                loadingCtrl.startLoading();

                var self = this;
                $.get('tag_json.php?mediaid=<?php echo $id; ?>', function (tagsList) {
                    if (typeof tagsList === 'string') {
                        tagsList = JSON.parse(tagsList);
                    }

                    var htmlCategoryTemplate = '<div class="list list3 paddingleft"><h3>__CATEGORY_NAME__</h3><ul>__TAGS_LIST__</ul>__HIGH__TAGS__LIST__</div>';
                    var htmlTagsTemplate = '<li><input type="checkbox" class="_chk_tag" id="chk_t___CODE_VALUE__" tag_id="__CODE_VALUE__" tag_name="__TAG_NAME__" tag_high = "__HIGH_CODE__" tag_high_name = "__HIGH_NAME__" code_value="__CODE_VALUE__" __CHECKED__/><label for="chk_t__CODE_VALUE__">__TAG_NAME__</label></li>';
                    var htmlHighTagsTemplate = '<input type="hidden" class="_chk_tag" id="chk_t___CODE_VALUE__" tag_id="__CODE_VALUE__" tag_name="__TAG_NAME__" code_value="__CODE_VALUE__"/>';
                    var categoryListHtml = '';
                    for (var catKey in tagsList) {
                        var tags = tagsList[catKey];

                        var tagListHtml = '';
                        var highTagListHtml = '';
                        var checked = '';
                        for (var tagIdx in tags) {
                            if (tags[tagIdx].cnt > 0) {
                                checked = 'checked';
                            } else {
                                checked = '';
                            }
                            if (tagIdx.indexOf('h') != 0) {
                                tagListHtml += htmlTagsTemplate.replace(/__TAG_NAME__/g, tags[tagIdx].name).replace(/__CODE_VALUE__/g, tags[tagIdx].codevalue).replace(/__HIGH_CODE__/g, tags[tagIdx].highcode).replace(/__HIGH_NAME__/g, tags['h' + tags[tagIdx].highcode].name).replace(/__CHECKED__/g, checked);
                            } else {
                                highTagListHtml += htmlHighTagsTemplate.replace(/__TAG_NAME__/g, tags[tagIdx].name).replace(/__CODE_VALUE__/g, tags[tagIdx].codevalue);
                            }

                            if (checked && $('span#st_' + tags[tagIdx].codevalue).length == 0) {
                                tagsCtrl.onSelectTag(tags[tagIdx].name, tags[tagIdx].codevalue);
                            }

                        }

                        categoryListHtml += htmlCategoryTemplate.replace(/__CATEGORY_NAME__/g, catKey).replace(/__TAGS_LIST__/g, tagListHtml).replace(/__HIGH__TAGS__LIST__/g, highTagListHtml);

                    }

                    $('#tags_list').html(categoryListHtml);
                    $('input._chk_tag').click(function (e) {
                        if ($(this).prop('checked')) {
                            tagsCtrl.onSelectTag($(this).attr('tag_name'), $(this).attr('code_value'), $(this).attr('tag_high'), $(this).attr('tag_high_name'));
                        } else {
                            tagsCtrl.delSelectedTag($(this).attr('tag_id'), $(this).attr('tag_high'));
                        }
                    });

                    if ($('input._chk_tag:checked').length > 4) {
                        $('input._chk_tag').prop('disabled', true);
                        $('input._chk_tag:checked').prop('disabled', false);
                    } else {
                        $('input._chk_tag').prop('disabled', false);
                    }


                    if (typeof callback === 'function') {
                        callback();
                    }

                    loadingCtrl.endLoading();
                })
            },

            onSelectTag: function (name, codevalue, highcode, highname) {

                var htmlSelectedTemplate = '<span class="button _t_selected" code_value="__CODE_VALUE__" tag_id="__CODE_VALUE__" id="st___CODE_VALUE__">__TAG_NAME__<span class="del" onclick="javascript:tagsCtrl.delSelectedTag(\'__CODE_VALUE__\',\'__HIGH_VALUE__\');"></span></span>';
                if (highcode !== undefined) {

                    if ($('#tags_selected span#st_' + highcode).length == 0) {
                        $('#tags_selected').append(htmlSelectedTemplate.replace(/__CODE_VALUE__/g, highcode).replace(/__TAG_NAME__/g, highname).replace(/__HIGH_VALUE__/g, highcode));
                    }
                }
                $('#tags_selected').append(htmlSelectedTemplate.replace(/__CODE_VALUE__/g, codevalue).replace(/__TAG_NAME__/g, name).replace(/__HIGH_VALUE__/g, highcode));

                if ($('input._chk_tag:checked').length > 4) {
                    $('input._chk_tag').prop('disabled', true);
                    $('input._chk_tag:checked').prop('disabled', false);
                } else {
                    $('input._chk_tag').prop('disabled', false);
                }

            },

            openTagsPop: function () {
                $('div._tags').show();
            },

            closeTagsPop: function () {
                $('div._tags').hide();
                $("body").css("overflow-y", "auto");
            },

            delSelectedTag: function (tagId, highid) {
                $('#st_' + tagId).remove();
                $('#chk_t_' + tagId).prop('checked', false);
                if ($('input._chk_tag:checked').length > 5) {
                    $('input._chk_tag').prop('disabled', true);
                    $('input._chk_tag:checked').prop('disabled', false);
                } else {
                    $('input._chk_tag').prop('disabled', false);
                }
                var tagcount = 0;

                $.each($('input._chk_tag:checked'), function (index, value) {
                    if ($(value).attr('tag_high') == highid) {
                        tagcount++;
                    }
                });
                if (tagcount == 0) {
                    $('#st_' + highid).remove();
                }
            },

            setSelectedTags: function () {
                var selectedTags = '';
                var selectedCodeValue = '';
                $.each($('span._t_selected'), function (index, value) {
                    selectedTags += '#' + $(value).text() + ' ';
                    selectedCodeValue += $(value).attr('code_value') + '#DMT#';
                });

                $('input[name=categorytags]').val(selectedTags);
                $('input[name=categorycodevalues]').val(selectedCodeValue);
            }
        };

        /**
         * 공유범위 > 법인별 설정
         */
        var corpsCtrl = {
            setCorps: function () {
                var selectedCorps = $('input._corps_code');

                var corpsIds = '';
                var corpsnames = '<ul>';
                for (var i = 0, l = selectedCorps.length; i < l; i++) {
                    var $item = $(selectedCorps[i]);
                    if ($item.prop('checked')) {
                        corpsIds += $item.val() + '#DMT#';
                        corpsnames += '<li>' + $item.val() + '</li>';
                    }
                }
                corpsnames += '</ul>';
                $('input[name=corps_id]').val(corpsIds.substring(0, corpsIds.length - 5));
                $('#targetcorpstext').html(corpsnames);
                this.closeCorpsPop();
            },

            openCorpsPop: function () {
                $('div._corps').show();
            },

            closeCorpsPop: function () {
                $('div._corps').hide();
                $("body").css("overflow-y", "auto");
            },

            searchCorps: function (callback) {
                loadingCtrl.startLoading();

                var self = this;
                $.get('search_corps_json.php?mediaid=<?php echo $id; ?>', function (corpsList) {
                    if (typeof corpsList === 'string') {
                        corpsList = JSON.parse(corpsList);
                    }

                    var targetcorps = $('input[name=corps_id]').val().split('#DMT#');

                    var htmlTemplate = '<li><a href="#" class="serise_text">'
                            + '__NAME__&nbsp;&nbsp;</a>'
                            + '<input type="checkbox" value="__CODE__" data="__NAME__" class="_corps_code" __CHECKED__></li>';
                    var corpsListHtml = '';
                    for (var key in corpsList) {
                        var checked = '';
                        for (var corpkey in targetcorps) {
                            if (corpsList[key].corpid || corpsList[key].code == targetcorps[corpkey]) {
                                checked = 'checked';
                                break;
                            }
                        }
                        corpsListHtml += htmlTemplate.replace(/__CODE__/g, corpsList[key].code)
                                .replace(/__NAME__/g, corpsList[key].code).replace(/__CHECKED__/g, checked);
                    }

                    $('#corps_list').html(corpsListHtml);
                    if (typeof callback === 'function') {
                        callback();
                    }

                    loadingCtrl.endLoading();
                });
            }
        };

        /**
         * 공유범위 > 대상자 지정
         */
        var userCtrl = {
            togglePop: function (type) {
                $('div._useauth').hide();
                $('div._' + type).show();
            },

            /**
             * User 권한검색
             */
            setUserIds: function () {
                var selectedUserIds = '';
                var selectedUserNames = '<ul>';
                $.each($('#selected_user_list').find('tr._user_ids'), function (index, value) {
                    selectedUserIds += $(value).attr('user_id') + '#DMT#';
                    selectedUserNames += '<li>' + $(value).attr('user_name') + '</li>';
                });
                selectedUserNames += '</ul>';

                $('input[name=user_ids]').val(selectedUserIds.substring(0, selectedUserIds.length - 5));
                $('#targetuserstext').html(selectedUserNames);

                this.closeUserIdsPop();
            },
            openUserIdsPop: function () {
                $('div._user_ids').show();
            },
            closeUserIdsPop: function () {
                $('div._user_ids').hide();
                $("body").css("overflow-y", "auto");
            },
            selectUserIds: function (userId, userName, id) {
                var selectedUserIds = $('#selected_user_list').find('tr._user_ids');
                for (var i = 0, l = selectedUserIds.length; i < l; i++) {
                    var $item = $(selectedUserIds[i]);
                    if ($item.attr('user_id') == userId) {
                        return;
                    }
                }

                $('#selected_user_list').append('<tr onclick="javascript:userCtrl.deleteUserIds(\'' + userId + '\')" id="selected_user_' + userId + '" class="_user_ids" user_name="' + userName + '" user_id="' + userId + '">' + $('#user_' + userId).html() + '</tr>');
            },
            deleteUserIds: function (userId) {
                $('#selected_user_' + userId).remove();
            },
            searchUserIds: function (callback) {

                loadingCtrl.startLoading();

                var self = this;

                var userName = encodeURIComponent($('#search_username').val());
                var orgName = encodeURIComponent($('#search_orgname').val());

                $.get('useauth_json.php?mediaid=<?php echo $id; ?>&type=user&username=' + userName + '&orgname=' + orgName, function (userIdsList) {
                    if (typeof userIdsList === 'string') {
                        userIdsList = JSON.parse(userIdsList);
                    }

                    var htmlTemplate = '<tr onclick="javascript:userCtrl.selectUserIds(__USERID__, \'__USERFULLNAME__\', \'__MUID__\');" id="user___USERID__"><td>__ORGNAME__</td><td>__USERNAME__</td><td>__TITLENAME__</td><td>__EMPLOYNO__</td></tr>';
                    var userIdsListHtml = '';
                    var userfullname = '';
                    for (var key in userIdsList) {
                        userfullname = userIdsList[key].organizationname + ' ' + userIdsList[key].userfullname + ' ' + userIdsList[key].titlename;
                        userIdsListHtml += htmlTemplate.replace(/__USERID__/g, userIdsList[key].userid)
                                .replace(/__ORGNAME__/g, userIdsList[key].organizationname)
                                .replace(/__USERNAME__/g, userIdsList[key].userfullname)
                                .replace(/__USERFULLNAME__/g, userfullname)
                                .replace(/__EMPLOYNO__/g, userIdsList[key].employno)
                                .replace(/__TITLENAME__/g, userIdsList[key].titlename).replace(/__MEDIAID__/g, userIdsList[key].muid);
                    }
                    if (!userIdsListHtml) {
                        userIdsListHtml = '<tr><td colspan="4"><?php echo get_string('usersearch', 'local_media'); ?>.</td></tr>';
                    }
                    $('#user_list').html(userIdsListHtml);
                    if (typeof callback === 'function') {
                        callback();
                    }


                    loadingCtrl.endLoading();
                });

            },

            /**
             * Group 권한검색
             */
            setOrgCodes: function () {
                var selectedOrgCodes = '';
                var selectedOrgNames = '<ul>';
                $.each($('#selected_org_list').find('tr._org_codes'), function (index, value) {
                    selectedOrgCodes += $(value).attr('org_code') + '#DMT#';
                    selectedOrgNames += '<li>' + $(value).attr('org_name') + '</li>';
                });
                selectedOrgNames += '</li>';

                $('input[name=org_codes]').val(selectedOrgCodes.substring(0, selectedOrgCodes.length - 5));
                $('#targetorgstext').html(selectedOrgNames);

                this.closeOrgCodesPop();
            },
            openOrgCodesPop: function () {
                $('div._org_codes').show();
            },
            closeOrgCodesPop: function () {
                $('div._org_codes').hide();
                $("body").css("overflow-y", "auto");
            },
            selectOrgCodes: function (orgCode, orgName, id) {
                var selectedOrdCodes = $('#selected_org_list').find('tr._org_codes');
                for (var i = 0, l = selectedOrdCodes.length; i < l; i++) {
                    var $item = $(selectedOrdCodes[i]);
                    if ($item.attr('org_code') == orgCode) {
                        return;
                    }
                }

                $('#selected_org_list').append('<tr onclick="javascript:userCtrl.deleteOrgCodes(\'' + orgCode + '\')" id="selected_org_' + orgCode + '" class="_org_codes" org_name="' + orgName + '" org_code="' + orgCode + '">' + $('#org_' + orgCode).html() + '</tr>');
            },
            deleteOrgCodes: function (orgCode) {
                $('#selected_org_' + orgCode).remove();
            },
            searchOrgCodes: function (callback) {
                loadingCtrl.startLoading();

                var self = this;

                var orgName = encodeURIComponent($('#search_org').val());
                $.get('useauth_json.php?mediaid=<?php echo $id; ?>&type=group&orgname=' + orgName, function (orgCodesList) {
                    if (typeof orgCodesList === 'string') {
                        orgCodesList = JSON.parse(orgCodesList);
                    }
                    var htmlTemplate = '<tr onclick="javascript:userCtrl.selectOrgCodes(\'__ORGCODE__\',\'__ORGNAME__\',\'__ORGID__\');" id="org___ORGCODE__"><td>__ORGCODE__</td><td>__ORGNAME__</td></tr>';
                    var orgCodesListHtml = '';
                    for (var key in orgCodesList) {
                        orgCodesListHtml += htmlTemplate.replace(/__ORGCODE__/g, orgCodesList[key].code)
                                .replace(/__ORGNAME__/g, orgCodesList[key].name).replace(/__MEDIAID__/g, orgCodesList[key].orgid);
                    }
                    if (!orgCodesListHtml) {
                        orgCodesListHtml = '<tr><td colspan="2"><?php echo get_string('searchdepart', 'local_media'); ?>.</td></tr>';
                    }
                    $('#organization_list').html(orgCodesListHtml);
                    if (typeof callback === 'function') {
                        callback();
                    }

                    loadingCtrl.endLoading();
                });
            }
        };

        var captionCtrl = {
            init: function () {
                var captionType = {'KR': '한글', 'EN': '<?php echo get_string('en', 'local_media'); ?>', 'CN': '<?php echo get_string('cn', 'local_media'); ?>', 'JP': '<?php echo get_string('jp', 'local_media'); ?>', 'VN': '<?php echo get_string('vn', 'local_media'); ?>'};
                var captionTypeHtml = '';
                for (var cKey in captionType) {
                    captionTypeHtml += '<input type="radio" name="caption_lang" value="' + cKey + '" ' + (cKey == 'KR' ? 'checked="checked"' : '') + '>' + captionType[cKey] + '<br/><br/>';
                }

                $('#caption_type').html(captionTypeHtml);
            },
            removeCaption: function (id, fileid) {
                var self = this;
                $.post('remove_caption_json.php',
                        {id: id, fileid: fileid},
                        function (data) {
                            if (typeof data === 'string') {
                                data = JSON.parse(data);
                            }

                            switch (data.result) {
                                case 'P':
                                    alert("<?php echo get_string('parametererror', 'local_media'); ?>");
                                    break;
                                case 'Y':
                                    alert("<?php echo get_string('deleted', 'local_media'); ?>.");
                                    location.reload();
                                    break;
                            }
                        }
                );
            },
            editCaption: function (id, fileid) {
                var self = this;
                window.open('pop_caption_edit.php?id=' + id + '&fileid=' + fileid, 'popcaption', 'width=700px, height=600px');
            }
        };

        var embedCtrl = {
            init: function (embedLink, embedCode) {
                $('#linkurl').text(embedLink);
                $('#embed_code').text(embedCode);
            },
            copy: function (elEmbed) {
                //var elEmbed = document.getElementById("embed_code");
                var range = document.createRange();
                var selection = window.getSelection();
                range.selectNodeContents(elEmbed);
                selection.removeAllRanges();
                selection.addRange(range);

                var copyIsArray = false;
                try {
                    copyIsArray = document.execCommand("copy");
                    if (copyIsArray) {
                        alert("<?php echo get_string('copied', 'local_media'); ?>.");
                    } else {
                        if (isMobile.any()) {
                            prompt("<?php echo get_string('mediatext12', 'local_media'); ?>.", elEmbed.text());
                        } else {
                            prompt("<?php echo get_string('mediatext13', 'local_media'); ?>.", elEmbed.text());
                        }
                    }
                } catch (g) {
                }
            }
        }

        $(document).ready(function () {

            //공지설정시에만 공지기간을 보여줌
            if ($('#id_notice:checked').length > 0) {
                $('#fgroup_id_noticetimegroup').show();
            } else {
                $('#fgroup_id_noticetimegroup').hide();
            }
            $('#fitem_id_notice').click(function (e) {
                if ($('#id_notice:checked').length > 0) {
                    $('#fgroup_id_noticetimegroup').show();
                } else {
                    $('#fgroup_id_noticetimegroup').hide();
                }
            });


            //수정일 때 콘텐츠 타입에 따라서 상단 보여줌 처리
            var contype = $('input[name=upload_type]').val();
            $('div._upload_div').hide();
            if (!contype)
                contype = 'video';
            $('#' + contype + '_uploader').show();
            $('#' + contype + '_uploadlist').addClass('on');

            $('input[name=target]').click(function (e) {
                switch ($(this).val()) {
                    case '1':
                        $('#targetusers_area').hide();
                        $('#targetcorps_area').show();
                        break;
                    case '2':
                        $('#targetcorps_area').hide();
                        $('#targetusers_area').show();
                        break;
                    case '3':
                        $('#targetcorps_area').hide();
                        $('#targetusers_area').hide();
                        break;
                    default:
                        break;
                }
            });

            $('#targetcorpsbtn').click(function (e) {
                corpsCtrl.searchCorps(corpsCtrl.openCorpsPop);
                $("body").css("overflow-y", "hidden");
                return false;
            });

            $('#targetusersbtn').click(function (e) {
                userCtrl.searchUserIds(userCtrl.openUserIdsPop);
                $("body").css("overflow-y", "hidden");
                return false;
            });

            $('#targetorgsbtn').click(function (e) {
                userCtrl.searchOrgCodes(userCtrl.openOrgCodesPop);
                $("body").css("overflow-y", "hidden");
                return false;
            });

            switch ($('input[name=target]:checked').val()) {
                case '1':
                    $('#targetcorpstext').html('<?php echo $selected_target; ?>');
                    $('#targetusers_area').hide();
                    $('#targetcorps_area').show();
                    break;
                case '2':
                    $('#targetuserstext').html('<?php echo $selected_targetusers; ?>');
                    $('#targetorgstext').html('<?php echo $selected_targetorgs; ?>');
                    $('#targetcorps_area').hide();
                    $('#targetusers_area').show();
                    break;
                case '3':
                    $('#targetcorps_area').hide();
                    $('#targetusers_area').hide();
                    break;
                default:
                    break;
            }


            if (<?php echo $id; ?> != 0) {
                categoryCtrl.init(function () {

                    var categoryBig = '<?php echo $content->category1; ?>';
                    var categoryMiddle = '<?php echo $content->category2; ?>';
                    var categorySmall = '<?php echo $content->category3; ?>';

                    $('select[name=categoryBig').val(categoryBig);
                    categoryCtrl.onChangeCat1();
                    $('select[name=categoryMiddle').val(categoryMiddle);
                    categoryCtrl.onChangeCat2();
                    $('select[name=categorySmall').val(categorySmall);
                    categoryCtrl.onChangeCat3();
                });

                embedCtrl.init('<?php echo $share_src; ?>', '<?php echo $embed_code; ?>');

            } else {
                categoryCtrl.init();
            }

            $('select[name=categoryBig]').change(function (e) {
                categoryCtrl.onChangeCat1();
            });
            $('select[name=categoryMiddle]').change(function (e) {
                categoryCtrl.onChangeCat2();
            });
            $('select[name=categorySmall]').change(function (e) {
                categoryCtrl.onChangeCat3();
            });

            $('#youtube_url').keyup(function (e) {
                $('input[name=mediafile]').val($(this).val());
            });

            $('#vimeo_url').keyup(function (e) {
                $('input[name=mediafile]').val($(this).val());
            });

            $('#custom_url').keyup(function (e) {
                $('input[name=mediafile]').val($(this).val());
            });

            load_edit_form('<?php echo $mode; ?>');

            // 동영상 파일 업로더
            if ($('#video_uploader').length > 0) {
                // Setup html5 version
                $("#video_uploader").plupload({
                    // General settings
                    runtimes: 'html5,flash,silverlight,html4',
                    url: "upload_com.php",
                    chunk_size: '1024mb',
                    rename: false,
                    dragdrop: true,
                    filters: {
                        // Maximum file size
                        max_file_size: '1024mb',
                        // Specify what files to browse for
                        mime_types: [
                            {title: "video files", extensions: "mp4"}
                        ]
                    },
                    // Flash settings
                    flash_swf_url: 'js/Moxie.swf',
                    // Silverlight settings
                    silverlight_xap_url: 'js/Moxie.xap',
                    preinit: {
                        Init: function (up, info) {

                        }
                    },
                    multipart_params: {
                        "id": "<?= $fileid ?>"
                    },
                    // Post init events, bound after the internal events
                    init: {
                        FilesAdded: function (up, files) {
                            var maxfiles = 1;
                            if (up.files.length > maxfiles)
                            {
                                up.splice(maxfiles);
                                alert('<?php echo get_string('mediatext14', 'local_media', maxfiles); ?>.');
                            }
                            if (up.files.length === maxfiles) {
                                $('#uploader_browse').hide("slow"); // provided there is only one #uploader_browse on page
                            }
                            $('#id_submitbutton').prop('disabled', true);
                        },
                        FileUploaded: function (up, file, info) {
                            // Called when file has finished uploading
                            var info = jQuery.parseJSON(info['response']);
                            $('input[name=uploaded_file]').val(info.fileid);
                            $('#id_submitbutton').prop('disabled', false);
                        },
                        UploadComplete: function (up, files) {

                        },
                        Error: function (up, args) {
                            // Called when error occurs
                            //alert('[Error] ' + args);
                        }
                    }
                });
            }

        });

        //콘텐츠 삭제
        function delete_contents(id) {
            var conf = confirm('<?php echo get_string('mediatext15', 'local_media'); ?>?');
            if (conf) {
                location.href = 'contents_delete.php?id=' + id;
            }
        }

        //기본정보,추가정보,썸네일 등 로드
        function load_edit_form(mode) {
            $('#id_defaultheader').hide();
            $('#id_additionheader').hide();
            $('#id_thumbnailheader').hide();
            $('#id_captionheader').hide();
            $('#id_embedheader').hide();
            $('.file_info .tab li').removeClass('on');
            $('#id_' + mode + 'header').show();
            $('#tab_' + mode).addClass('on');
            $('input[name=mode]').val(mode);
        }

    </script>

    <?php
    if (!$content || $contentmanage == 1) {
        $mform->display();
    }

    echo $OUTPUT->footer();
}
?>  