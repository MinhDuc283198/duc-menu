<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/lib/filelib.php";
require_once $CFG->dirroot . '/lib/form/filemanager.php';
require_once $CFG->dirroot . "/local/media/config.php";
require_once $CFG->dirroot . "/local/media/lib.php";
require_once $CFG->dirroot . "/local/lmsdata/lib.php";
require_once $CFG->dirroot . '/local/media/bulkupload_form.php';


$mode = optional_param('mode', 'default', PARAM_RAW);

$context = context_system::instance();
require_login();

$auth = local_media_userauth();

if(!$auth){
    redirect($CFG->wwwroot);
    die();
}

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->css('/local/media/flowplayer/skin/functional.css');
$PAGE->requires->css('/local/media/flowplayer/skin/quality-selector.css');
$PAGE->set_context($context);
$PAGE->set_pagelayout('subpage');
$PAGE->set_url('/local/media/bulkupload.php', array('mode' => $mode));

$strplural = ':DTube';

$PAGE->navbar->add($strplural);
$PAGE->set_title($strplural);
$PAGE->set_heading($strplural);

$options = array('noclean' => true, 'subdirs' => true, 'maxfiles' => -1, 'maxbytes' => 0, 'context' => $context);

$mform = new local_media_bulkupload_form(null, array('options' => $options,
    'context' => $context,
    'contents' => $content,
    'mode' => $mode)
);

$draftitemid = file_get_submitted_draft_itemid('excelfile');
file_prepare_draft_area($draftitemid, $context->id, 'local_media', 'excelfile', $USER->id, local_media_bulkupload_form::attachment_options());

if ($mform->is_cancelled()) {

    redirect("list.php");
    die();

    
} else if ($fromform = $mform->get_data()) {
    // 엑셀 파일 저장
    file_save_draft_area_files($draftitemid, $context->id, 'local_media', 'excelfile', $USER->id);
    
    // 저장된 엑셀 파일 가져오기
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'local_media', 'excelfile', $USER->id, "filename", true);
    error_log(var_export($files, true), 3, $CFG->dataroot.'/excelfile.log');
    
    $exelfilepath = '';
    foreach ($files as $file) {
        if(!$file->is_directory()) {
            if (isset($CFG->filedir)) {
                $filedir = $CFG->filedir;
            } else {
                $filedir = $CFG->dataroot.'/filedir';
            }
            
            $contenthash = $file->get_contenthash();
            $l1 = $contenthash[0].$contenthash[1];
            $l2 = $contenthash[2].$contenthash[3];
            $exelfilepath = "$filedir/$l1/$l2/$contenthash";
            
            break;
        }
    }
    
    // 미디어 파일
    // bulkupload_com.php로 업로드가 미리되고 아이디만 ','로 구분된 string으로 받음.
    $uploadedfile = $fromform->uploaded_file;
    $contentfileids = explode(',', $uploadedfile);
    
    // lmsmedia_files 테이블에서 업로드된 파일 정보를 가져온다.
    $mediafiles = array();
    foreach ($contentfileids as $contentfileid) {
        $mediafile = $DB->get_record('lmsmedia_files', array('id'=>$contentfileid));
        if($mediafile) {
            $mediafiles[$mediafile->filename] = $mediafile;
        }
    }
    
    $upcounts = local_media_add_bulk($exelfilepath, $mediafiles);
    
    // 엑셀 파일 삭제
    $fs->delete_area_files($context->id, 'local_media', 'excelfile', $USER->id);
    
    if($upcounts['nocompcount'] > 0){
        echo '<script>alert("'.get_string('nocompmessage','local_media').'");location.href="bulkupload.php";</script>';
        die();
    }
        
    redirect("list.php");
    die();
} else {

    $form_data = array(
        'excelfile'=>$draftitemid
    );

    $mform->set_data($form_data);

    echo $OUTPUT->header();
    ?>

    <!-- 타이틀-->

    <div class="blocks no_padding">
        <h3 class="title line_style">
            <p class="left button_right <?php echo $content ? '':'full';?>">
                <?php echo $content ? $content->title : ' <img src="/theme/oklasscompany/pix/images/computer.png" alt="video" title="video" /> '.get_string('videoatonce','local_media').'';?>
            </p>
        </h3>
    </div>

    <!-- 타이틀-->

    <link rel="stylesheet" href="js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />
    <link href="/theme/oklasscompany/style/popup.css" rel="stylesheet">
    <script type="text/javascript" src="js/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
    <script type="text/javascript" src="js/i18n/ko.js"></script>
    <script type="text/javascript" src="js/moxie.js"></script>
    <script type="text/javascript" src="js/plupload.dev.js"></script>
    <script type="text/javascript" src="./js/youtube/search.js?t=20170207"></script>
    <script type="text/javascript" src="./js/youtube/searchV.js?t=20170207"></script>
    <script src="https://apis.google.com/js/client.js?onload=onClientLoad" type="text/javascript"></script>

    <!-- Register -->
    <div class="aside style02">
        <div class="sub_menu noshadow height300" style="clear:both;">
            <ul>
                <li class="uploadlist" id="video_uploadlist"><a href="javascript:uploadAreaCtrl.openUploadDiv('video')"><?php echo get_string('fileupload','local_media'); ?></a></li>
            </ul>
        </div>
    </div>
    <!-- Uploader Area -->
    <div id="uploadVideo">
        <div id="video_uploader" class="_upload_div select_file">
            <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
        </div>
    </div>
    <!-- // Uploader Area -->




    <div id="loading" style="position:fixed; left:0; right:0; top:0; bottom:0; width:100%; height:100%; z-index:1200; background-color:#757b8c; opacity:0.7; display:none;">
        <div style="position: absolute; top: 50%; left: 47%;"><img src="loading.gif"></div>
    </div>
     

    <script type="text/javascript">
        var loadingCtrl = {
            startLoading: function () {
                $('#loading').show();
            },
            endLoading: function () {
                $('#loading').hide();
            }
        };
        var uploadAreaCtrl = {
            openUploadDiv: function (category) {
                $('#video_uploader').show();
                $('#video_uploadlist').addClass('on');

                $('input[name=upload_type]').val(category);
            }
        };

        $(document).ready(function () {
            $('#video_uploader').show();
            $('#video_uploadlist').addClass('on');

            load_edit_form('<?php echo $mode;?>');

            // 동영상 파일 업로더
            if ($('#video_uploader').length > 0) {
                // Setup html5 version
                $("#video_uploader").plupload({
                    // General settings
                    runtimes: 'html5,flash,silverlight,html4',
                    url: "bulkupload_com.php",
                    chunk_size: '1024mb',
                    rename: true,
                    dragdrop: true,
                    filters: {
                        // Maximum file size
                        max_file_size: '1024mb',
                        // Specify what files to browse for
                        mime_types: [
                            {title: "video files", extensions: "avi,mp4,wmv,mkv"}
                        ]
                    },
                    // Flash settings
                    flash_swf_url: 'js/Moxie.swf',
                    // Silverlight settings
                    silverlight_xap_url: 'js/Moxie.xap',
                    preinit: {
                        Init: function (up, info) {
                            
                        }
                    },
                    // Post init events, bound after the internal events
                    init: {
                        FilesAdded: function(up, files) {
                            var maxfiles = 10;
                            if(up.files.length > maxfiles )
                             {
                                up.splice(maxfiles);
                                alert('<?php echo get_string('mediatext14','local_media',maxfiles); ?>.');
                             }
                            if (up.files.length === maxfiles) {
                                $('#uploader_browse').hide("slow"); // provided there is only one #uploader_browse on page
                            }
                            $('#id_submitbutton').prop('disabled',true);
                        },
                        FileUploaded: function (up, file, info) {
                            var info = jQuery.parseJSON(info['response']);
                            var fileidstr = $('input[name=uploaded_file]').val();
                            var fileidarr = fileidstr.split(",");
                            fileidarr.push(info.fileid);
                            fileidarr = fileidarr.filter(function(n){ return n !== ""; });
                            
                            $('input[name=uploaded_file]').val(fileidarr.join(","));
                            $('#id_submitbutton').prop('disabled',false);
                        },
                        UploadComplete: function (up, files) {
                            $('.loading').show();
                        },
                        Error: function (up, args) {
                            // Called when error occurs
                            alert('[Error] ' + args);
                        }
                    }
                });
            }
        });

        //기본정보,추가정보,썸네일 등 로드
        function load_edit_form(mode){
            $('#id_defaultheader').hide();
            $('#id_additionheader').hide();
            $('#id_thumbnailheader').hide();
            $('#id_captionheader').hide();
            $('#id_embedheader').hide();
            $('.file_info .tab li').removeClass('on');
            $('#id_'+mode+'header').show();
            $('#tab_'+mode).addClass('on');
            $('input[name=mode]').val(mode);
        }
        
    </script>

<?php
                     
    $mform->display();
    
    echo $OUTPUT->footer();
}