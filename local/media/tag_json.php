<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');

$mediaid = optional_param('mediaid', 0, PARAM_INT);

$currentlang = strtoupper(current_language());
if ($currentlang == 'ZH_CN') $currentlang = 'ZH';

$user_corporationcode = $DB->get_field('lmsdata_user', 'corporationcode', array('userid' => $USER->id));

$course_param = array('corporationcode' => $user_corporationcode, 'code' => 'CURRICULUM_CLASS_TAG_CODE', 'lang1' => $currentlang,'lang2' => $currentlang, 'lcode' => 'CURRICULUM_CLASS_TAG_L2_CODE', 'mediaid' => $mediaid);

$category_sql = 'SELECT ca.id, ca.codevalue, ca.highcode, ln.name, (SELECT COUNT(*) FROM {lmsdata_course_tag} lt JOIN {lmsmedia_contents} lc ON lt.curriculumid = lc.id '
        . 'WHERE lt.tagcode = ca.codevalue and lt.type = 2 and lc.id = :mediaid) AS cnt, substr(ln.name,1,1) AS prefix';
$category_from = ' FROM {lmsdata_categories} ca
JOIN {lmsdata_categories_lang} ln on ln.codevalue = ca.codevalue AND ln.corporationcode = ca.corporationcode AND ln.code = ca.code ';
$category_where = ' WHERE ca.corporationcode = :corporationcode AND ca.code = :code AND ln.langcode = :lang1 ';
$orderby = ' ORDER BY ln.name ';
$category_info = $DB->get_records_sql($category_sql . $category_from . $category_where . $orderby, $course_param);

$result_json = array();

foreach ($category_info as $category_infos) {
    $L2_where = ' WHERE ca.corporationcode = :corporationcode AND ca.code = :lcode AND ca.highcode = :highcode AND ln.langcode = :lang2 ';
    $course_param['highcode'] = $category_infos->codevalue;
    $L2_category_info = $DB->get_records_sql($category_sql . $category_from . $L2_where . $orderby, $course_param);
    $result_json[$category_infos->name] = $L2_category_info;
    $result_json[$category_infos->name]['h'.$category_infos->codevalue] =  $category_infos;
}

echo json_encode($result_json);
?>