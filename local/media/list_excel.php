<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/lmsdata/lib.php";
require_once $CFG->dirroot . "/local/media/lib.php";

$page = optional_param('page', 1, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$sort = optional_param('sort', 'id', PARAM_RAW);

$context = context_system::instance();
require_login();

$list = local_media_get_contents($page, $search, $sort, 1);

require_once $CFG->dirroot . "/lib/phpexcel/PHPExcel.php";
require_once $CFG->dirroot . "/lib/phpexcel/PHPExcel/IOFactory.php";

// PHPExcel 시작
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator('LGD')
 ->setLastModifiedBy('LGD')
 ->setTitle(get_string('videolist','local_media'))
 ->setSubject(get_string('videolist','local_media'))
 ->setDescription(get_string('videolist','local_media'));

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', get_string('useornot','local_media'))
->setCellValue('B1', get_string('p_cate','local_media'))
->setCellValue('C1', get_string('c_cate','local_media'))
->setCellValue('D1', get_string('s_cate','local_media'))
->setCellValue('E1', get_string('cateforsearch','local_media'))
->setCellValue('F1', get_string('videoname','local_media'))
->setCellValue('G1', get_string('resistname','local_media'))
->setCellValue('H1', get_string('resistdepth','local_media'))
->setCellValue('I1',  get_string('resistemail','local_media'))
->setCellValue('J1', 'NPS')
->setCellValue('K1',  get_string('howmanylike','local_media'))
->setCellValue('L1', get_string('readnum','local_media'))
->setCellValue('M1', get_string('playtime','local_media'))
->setCellValue('N1', get_string('averageplaytime','local_media'))
->setCellValue('O1', get_string('submitday','local_media'))
->setCellValue('P1', get_string('howmuchshare','local_media'))
->setCellValue('Q1', get_string('coopshare','local_media'))
->setCellValue('R1', get_string('usecate','local_media'))
->setCellValue('S1', get_string('seriesname','local_media'))
->setCellValue('T1', get_string('seriesorder','local_media'))
->setCellValue('U1', get_string('inoutcompany','local_media'))
->setCellValue('V1', get_string('videolink','local_media'))
->setCellValue('W1', get_string('uniqueid','local_media'));
//$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFont()->setBold(true);
/*
$category1 = array('1' => '리더십', '2' => '기본', '3' => '전문직무', '4' => '글로벌', '5' => '기타');
$category2 = array (
	'1' => array('11' => '리더', '12' => '사업가/핵심', '13' => '리더십공통'),
	'2' => array('21' => '신입/경력', '22' => '직급', '23' => '기본공통'),
	'3' => array('31' => 'R&D', '32' => '생산', '33' => '영업마케팅', '34' => '관리/지원', '35' => '직무 공통'),
	'4' => array('41' => 'FSE', '42' => 'Global 공통'),
	'5' => array('51' => '조직활성화', '52' => '자기계발', '53' => '상생지원', '54' => '기타')
);
$category3 = array (
	'11' => array('111' => '리더'),
	'12' => array('121' => '사업가/핵심인재'),
	'13' => array('131' => '리더십공통'),
	'21' => array('211' => '신입', '212' => '경력'),
	'22' => array('221' => '직급'),
	'23' => array('231' => '기본공통'),
	'31' => array('311' => 'LCD Panel', '312' => 'LCD 회로', '313' => 'LCD 기구', '314' => 'LCD 광학', '315' => 'R&D공통', '316' => 'OLED Panel', '317' => 'OLED 회로', '318' => 'OLED 기구', '319' => 'OLED 공통', '3110' => 'Panel 공통', '3111' => '회로 공통', '3112' => '기구 공통'),
	'32' => array('321' => '공정기술', '322' => '장비기술', '323' => '구매', '324' => '품질', '325' => '환경/안전', '326' => '생산지원', '327' => '생산공통'),
	'33' => array('331' => '영업', '332' => '마케팅', '333' => '고객지원', '334' => '상품기획', '335' => 'SCM', '336' => '영업/마케팅 공통'),
	'34' => array('341' => '경영지원', '342' => '재경', '343' => 'HR', '344' => '관리/지원 공통'),
	'35' => array('351' => '직무공통'),
	'41' => array('411' => 'FSE'),
	'42' => array('421' => '어학', '422' => 'Global소양'),
	'51' => array('511' => '조직활성화'),
	'52' => array('521' => '자기계발'),
	'53' => array('531' => '상생지원'),
	'54' => array('541' => '기타')
);
*/
$targetinfo = array('1' => get_string('coopconfig','local_media'), '2' => get_string('selectuser','local_media'), '3' => get_string('sharelink','local_media'));
$gubuninfo = array('1' => 'DTube', '2' => get_string('courseconfirm','local_media'), '3' => get_string('alluse','local_media'));

$arrList = array();
foreach($list['list'] as $key=>$item) {
    $share_src = local_lmsdata_shared_media_url_link($item->id, 'player');
    $cate = $DB->get_record_sql("select cat1.id, cat1.name as cat1, cat2.name as cat2, cat3.name as cat3 from tb_lmsdata_categories_lang cat1 
join tb_lmsdata_categories_lang cat2 on cat2.corporationcode = cat1.corporationcode and cat2.langcode = 'KO' and cat2.code = 'LGD_ACT_CATEGORY_VS' 
join tb_lmsdata_categories_lang cat3 on cat3.corporationcode = cat2.corporationcode and cat3.langcode = 'KO' and cat3.code = 'LGD_ACT_SMALL_CATEGORY_VS' 
where cat1.code = 'LGD_ACT_DEFINITIONS_VS' and cat1.langcode = 'KO' and cat1.codevalue = '$item->category1' and cat2.codevalue = '$item->category2' and cat3.codevalue = '$item->category3' and cat1.corporationcode = 'LGDKR' ");
if($item->npscount > 0){
    $nps = ($item->nps*100).'%';
    $likes = (($item->nps/2+0.5)*100).'%';
}else{
    $nps = '';
    $likes = '';
}

if($item->inouttype == 'A'){
    $inouttype = get_string('all','local_media');
}else if($item->inouttype == 'I'){
    $inouttype = get_string('incompany','local_media');
}
    array_push($arrList, array(
		'isused' => ($item->isused == 1 ? 'Y' : 'N'),
		'category1' => $cate->cat1,
		'category2' => $cate->cat2,
		'category3' => $cate->cat3,
		'categorytags' => htmlspecialchars_decode($item->categorytags),
		'title' => htmlspecialchars_decode($item->title),
		'author_name' => $item->lastname,
		'author_div' => $item->organizationname,
		'author_email' => $item->email,
		'nps' => $nps,
		'likes' => $likes,
		'viewcount' => $item->viewcount,
		'duration' => $item->duration,
		'avgplaytime' => $item->avg_playtime,
		'timecreated' => date('Y-m-d H:i:s', $item->timecreated),
		'target' => $targetinfo[$item->target],
		//'corps_id' => $item->target == 1 ? $item->corps_codes : ($item->target == 2 ? $item->user_ids : ''),
                'corps_id' => $item->corps_codes,
		'gubun' => $gubuninfo[$item->gubun],
		'groupid' => $item->groupname,
                'sortorder' => $item->sortorder,
                'inouttype' => $inouttype,
		'embed_code' => $share_src,
                'id' => $item->id
	));
}

$objPHPExcel->getActiveSheet()->fromArray($arrList, NULL, 'A2');
$excelfilename = 'DTube_list_'.date('Ymd').'.xlsx';
header('Content-Type: application/vnd.ms-excel;charset=utf-8');
header('Content-Type: application/x-msexcel;charset=utf-8');
header('Content-Disposition: attachment; filename="'.$excelfilename.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>