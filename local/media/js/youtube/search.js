// Your use of the YouTube API must comply with the Terms of Service:
// https://developers.google.com/youtube/terms
// Called automatically when JavaScript client library is loaded.
function onClientLoad() {
    gapi.client.load('youtube', 'v3', onYouTubeApiLoad);
}
// Called automatically when YouTube API interface is loaded (see line 9).
function onYouTubeApiLoad() {
    gapi.client.setApiKey('AIzaSyAYLY5zlo2yI_oc6P2uYqYyng9Dcp8m-TI');
}

function y_searchPage(type) {

  var query = document.getElementById('y_query').value;
  var pageToken = document.getElementById('y_pageToken').value;
  // Use the JavaScript client library to create a search.list() API call.
  var request = gapi.client.youtube.search.list({
      part: 'snippet',
      q:query,
      maxResults:10,
      pageToken:pageToken,

  });
  // Send the request to the API server, call the onSearchYoutubeResponse function when the data is returned
  request.execute(onSearchYoutubeResponse);

}

// Called when the search button is clicked in the html code
function y_search() {
    var query = document.getElementById('y_query').value;
    // Use the JavaScript client library to create a search.list() API call.
    var request = gapi.client.youtube.search.list({
        part: 'snippet',
        q:query,
        maxResults:10
    });
    // Send the request to the API server, call the onSearchYoutubeResponse function when the data is returned
    request.execute(onSearchYoutubeResponse);
}

function selectData(data) {
  window.opener.getReturnValue(data);
  window.close();
}

// Triggered by this line: request.execute(onSearchYoutubeResponse);
function onSearchYoutubeResponse(response) {
    loadingCtrl.startLoading();

    var responseString ="";

    console.log(response.nextPageToken);
    console.log(response.prevPageToken);

    if(response.nextPageToken) {
      document.getElementById('y_id_next').style.display =  "inline";
      document.getElementById('y_pageToken').value = response.nextPageToken;

    }

    if(response.prevPageToken) {
      document.getElementById('y_id_pre').style.display =  "inline";
      document.getElementById('y_pageToken').value = response.prevPageToken;
    }
    // token 이 존재할 경우  버튼 생성하기

    //console.

    var entries = response.items || [];

    for (var i = 0; i < entries.length; i++) {
        var entry = entries[i];
        id = entry.id.videoId;
        title = entry.snippet.title;
        description = entry.snippet.description;
        imgSrc = entry.snippet.thumbnails.default.url;

        //returnText = "<table boder='1'>"
      //               + " <tr><td><a href='https://www.youtube.com/embed/"+id+"' target=blank><img src='"+ imgSrc +"'  width='120' height='90' /></a></td>"
      //               + "    <td>"
      //               + "<b>"+ title  +"</b><br/> -"+ description/
//
  //                   + "<br><br><input type='button' title='선택' value='선택' class='button_style01 gray' onclick=selectData('https://www.youtube.com/embed/"+id+"') /></td>"
    //                 + "</tr>"
      //               + "</table>";

       returnText = "<div class='course_list'>"
                        + "<p class='thumb_img'> "
                        + "    <img src='"+ imgSrc +"' alt='course01' title='course01' /> "
                        + "</p>"
                        + " <div class='course_text'> "
                        + "     <h3 class='title'>" + title +"</h3> "
                        + "     <p> https://www.youtube.com/embed/"+id+"</p>"
                        + "     <p class='marginTop'> "
                        + "         <input type='button' class='button_style02' value='선택' onclick=youtubeCtrl.setYoutubeUrl('https://www.youtube.com/embed/"+id+"') > "
                        + "     </p>"
                        + " </div> "
                    + " </div>";

        responseString += returnText;

    }
  //  alert(feed);




    //try {
  	//	JSONObject jsonObj = (JSONObject)jsonParser.parse(jsonStr);
  	//	String name = (String) jsonObj.get("nextPageToken");

      //console.log(name);
      /*
  		JSONArray jsonArr = (JSONArray) jsonObj.get("items");
  		for(int i=0;i<jsonArr.size();i++){
  			String hobby = (String) jsonArr.get(i);
  			System.out.println(hobby);
		}
	} catch (ParseException e) {
		e.printStackTrace();
	}

*/



//    JSONObject json = new JSONParser(response);

  //  JSONObject dataObject = json.getJSONObject("data"); // this is the "data": { } part
  //  JSONArray items = dataObject.getJSONArray("items"); // this is the "items: [ ] part


  //  for (int i = 0; i < items.length(); i++) {
  //    JSONObject videoObject = items.getJSONObject(i);
  //    String title = videoObject.getString("title");
  //    String videoId = videoObject.getString("id");
  //  }

    document.getElementById('y_response').innerHTML = responseString;

    loadingCtrl.endLoading();
}
