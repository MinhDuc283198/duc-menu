<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

global $DB, $USER;

$returnvalue = new stdClass();
$returnvalue->status = 'fail';

if($USER){

    $id = optional_param('id', 0, PARAM_INT);  
    $mode = optional_param('mode', 'add', PARAM_RAW);
      
    if($mode == 'add'){
        
        $mediaid = required_param('mediaid', PARAM_INT);
        $comments = required_param('comments', PARAM_CLEANHTML);
        
        if($id){
            if(!$comment = $DB->get_record('lmsmedia_comments', array('id' => $id))){

                $comment->comments = $comments;
                $comment->timemodified = time();
                $DB->update_record('lmsmedia_comments',$comment);
            }

        }else{

            $comment = new stdClass();
            $comment->mediaid = $mediaid;
            $comment->userid = $USER->id;
            $comment->comments = $comments;
            $comment->timecreated = time();

            $commentid = $DB->insert_record('lmsmedia_comments',$comment);     

        }

        $ssousername = $DB->get_field('lmsdata_user','ssousername',array('userid'=>$USER->id));
        $comment_count = $DB->count_records('lmsmedia_comments', array('mediaid'=>$mediaid));
        
        $returnvalue->id = $commentid;
        $returnvalue->count = $comment_count;
        $returnvalue->comments = $comments;
        $returnvalue->user = $ssousername;
        $returnvalue->date =  date('Y-m-d H:i', $comment->timecreated);
        $returnvalue->status = 'success';
        
    }else if($mode == 'del'){
        $comment = $DB->get_record('lmsmedia_comments', array('id'=>$id));
        $mediaid = $comment->mediaid;
        
        if($comment->userid == $USER->id || is_siteadmin()){
            $DB->delete_records('lmsmedia_comments', array('id'=>$id));
            
            $comment_count = $DB->count_records('lmsmedia_comments', array('mediaid'=>$mediaid));
            $returnvalue->count = $comment_count;
            $returnvalue->status = 'success';           
        }
        
    }

}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
