<?php

function xmldb_local_media_upgrade($oldversion) {
    
    global $CFG, $DB, $OUTPUT;

    $dbman = $DB->get_manager();

    if($oldversion < 2017021100) {     
        $table = new xmldb_table('lmsmedia_contents');  

        $field = new xmldb_field('inouttype', XMLDB_TYPE_CHAR, '1', null, null, null, 'A');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }else{
            $dbman->add_field($table, $field);
        }
    }
    
    if($oldversion < 2017021101) {     
        $table = new xmldb_table('lmsmedia_contents');  

        $field = new xmldb_field('encrypted', XMLDB_TYPE_CHAR, '255', null, null, null, '');
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_type($table, $field);
        }else{
            $dbman->add_field($table, $field);
        }
    }
    
    if($oldversion < 2017022801) {     
        $table = new xmldb_table('lmsmedia_contents');
        
        $field = new xmldb_field('expiredate', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    if($oldversion < 2017030400) {     
        $table = new xmldb_table('lmsmedia_contents');
        
        $field = new xmldb_field('isenabled', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    if($oldversion < 2017032400) {     
        $table = new xmldb_table('lmsmedia_contents');
        
        $field = new xmldb_field('isshared', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    
    return true;
}
