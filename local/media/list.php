<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$search = optional_param('search', '', PARAM_RAW);
$sort = optional_param('sort', 'id', PARAM_RAW);

$context = context_system::instance();
require_login();

$auth = local_media_userauth();

if(!$auth){
    redirect($CFG->wwwroot);
    die();
}

$PAGE->set_context($context);
$PAGE->set_pagelayout('subpage');

echo $OUTPUT->header();

$list = local_media_get_contents($page, $search, $sort, 0, $perpage);

$total_pages = local_media_get_total_pages($list['count'], $perpage);

?>  
                <!-- 타이틀-->
                <div class="blocks no_padding LETO">
                    <h3 class="title line_style">
                        <p class="left">
                            <img src="/theme/oklasscompany/pix/images/computer.png" alt="dtube" title="dtube" />
                            :DTube
                        </p>
                        <p class="right pink">
                           * <?php echo get_string('mediatext','local_media'); ?>.
                        </p>
                    </h3>
                </div>
                <!-- 타이틀-->
                <!--검색 영역-->
                <div class="search_style01">
                    <div class="left_cont"> 
                        <span class="uplodvideo">
                            <?php echo get_string('uploadedvideo','local_media'); ?>(<?=$list['count'] ?>) 
                            <a href="#" onclick="javascript:location.reload();"><img src="/theme/oklasscompany/pix/images/round.png" alt="올린동영상" title="올린동영상" /></a>
                        </span>
                        <a href="#" onclick="javascript:listExcel();">
                        <span class="exceldown">
                            <?php echo get_string('excellistdown','local_media'); ?>
                            <img src="/theme/oklasscompany/pix/images/download.png" alt="엑셀다운로드" title="엑셀다운로드" />
                        </span>
                        </a>
                    </div>
                    <div class="right_cont">  
                        <form id="search_form" method="get">
                            <input type="hidden" name="page" value="<?php echo $page;?>"/>
                            <input type="hidden" name="sort" value="<?php echo $sort;?>"/>
                            <input type="text" id="title_keyword" name="search" placeholder="<?php echo get_string('searchvideo','local_media'); ?>" class="search-video" title="동영상 검색" value="<?=$search?>" onKeyPress="key_code();" />
                            <input type="submit" id="submit_search" value="<?php echo get_string('search'); ?>" title="조회"/>
                        </form>
                    </div>

                </div>
                <!--검색 영역-->
                <!--메뉴 영역-->
                <div class="line_menu">
                    <ul>
                        <li <?php echo $sort == 'id' ? 'class="on"' : ''; ?>><a href="javascript:listSort('id');"><?php echo get_string('desc1','local_media'); ?></a></li>
                        <li <?php echo $sort == 'title' ? 'class="on"' : ''; ?>><a href="javascript:listSort('title');"><?php echo get_string('desc2','local_media'); ?></a></li>
                        <li <?php echo $sort == 'duration' ? 'class="on"' : ''; ?>><a href="javascript:listSort('duration');"><?php echo get_string('desc3','local_media'); ?></a></li>
                        <li <?php echo $sort == 'viewcount' ? 'class="on"' : ''; ?>><a href="javascript:listSort('viewcount');"><?php echo get_string('desc4','local_media'); ?></a></li>
                        <li <?php echo $sort == 'likecount' ? 'class="on"' : ''; ?>><a href="javascript:listSort('likecount');"><?php echo get_string('desc5','local_media'); ?></a></li>
                    </ul>
                    
                    <button class="button_style01 gray" onclick="location.href='upload.php';"><?php echo get_string('uploadvideo','local_media'); ?></button>
                    <button class="button_style01 gray" onclick="location.href='bulkupload.php';"><?php echo get_string('submitatonce','local_media'); ?></button>
                    
                </div>
                <!--메뉴 영역-->
                <!--리스트-->
                <div class="lists padding">
                    <?php
                        $contentsList = $list['list'];
                        foreach ($contentsList as $key => $item) {
                            if($item->contype == 'vimeo' || $item->contype == 'youtube'){
                                $embedinfo = local_media_get_embed_info($item->mediafile, $item->contype);
                                $item->duration = $embedinfo->duration;
                            }
                        
                            $thumbimage = '/theme/oklasscompany/pix/images/sample.jpg';
                            if(file_exists($item->thumbpath)){
                                $thumbimage = str_replace($CFG->local_media_file_path,$CFG->wwwroot.'/storage',$item->thumbpath);
                            }else{
                                //vimeo, youtube일 경우는 api로 썸네일 호출
                                if ($item->contype == 'vimeo' || $item->contype == 'youtube'){
                                    $thumbimage = $embedinfo->thumbnail;
                                }
                            }
                        
                    ?>
                    <div class="course_list <?php echo $i == $l - 1 ? 'noline' : ''; ?>">
                        <p class="thumb_img">
                            <img src="<?php echo $thumbimage; ?>" alt="<?=$item->title;?>" title="<?=$item->title;?>" width="173" height="119"/>
                        </p>     
                        <div class="text_group autowidth" >
                            <div class="course_text col2">
                                <a href="upload.php?id=<?php echo $item->id;?>&search=<?php echo $search;?>&sort=<?php echo $sort;?>&page=<?php echo $page;?>"><h3 class="title small"><?=$item->title;?></h3></a>
                                <p class="info col2">
                                    <span class="date"><?=date('Y-m-d H:i:s', $item->timecreated)?></span>
                                    <span class="name"><?=$item->lastname?>(<?=$item->username?>)</span>
                                </p>  
                            </div>
                            <div class="col2">
                                <div class="float_group">
                                    <p class="icontext">
                                        <img src="/theme/oklasscompany/pix/images/search_light.png" alt="search" title="search" />
                                        <span><?php echo get_string('readnum','local_media'); ?> : <?=$item->viewcount?></span>
                                    </p>
                                    <p class="icontext">
                                        <img src="/theme/oklasscompany/pix/images/like.png" alt="좋아요" title="좋아요" />
                                        <span><?php echo get_string('like','local_media'); ?> : <?php echo local_media_get_npsscore($item->id);?></span>
                                    </p>
                                    <p class="icontext">
                                        <img src="/theme/oklasscompany/pix/images/time.png" alt="재생시간" title="재생시간" />
                                        <span><?php echo get_string('playtime','local_media'); ?> : <?php echo local_media_get_duration_hms($item->duration); ?></span>
                                    </p>  
                                    <p class="icontext">
                                        <img src="/theme/oklasscompany/pix/images/dtube_book.png" alt="구분" title="구분" />
                                        <?php
                                            $gubun_text = array('1' => 'DTube', '2' => get_string('courseconfirm','local_media'), '3' => get_string('alluse','local_media'));;
                                        ?>
                                        <span><?php echo get_string('category','local_media'); ?> : <?php echo $gubun_text[$item->gubun]; ?></span>
                                    </p>      
                                </div>
                                <?php
                                    if ($item->isnoticed == 1) {
                                ?>
                                <div class="float_group">
                                    <p class="icontext">
                                        <img src="/theme/oklasscompany/pix/images/notice.png" class="noticeicon" alt="구분" title="구분" />
                                        <span>
                                            <span class="notice_title"><?php echo get_string('notice','local_media'); ?> : </span>
                                            <span class="ntime"><?=date('Y-m-d', $item->timestartnotice)?> ~ </span>
                                            <span class="ntime"><?=date('Y-m-d', $item->timeendnotice)?></span>
                                        </span>
                                    </p>
                                </div>  
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                        }
                    ?>
                </div>
                <!--리스트-->

                <!--paging-->
                <div class="paging">

                <?php
                $page_params = array();
                $page_params['search'] = $search;
                $page_params['sort'] = $sort;
                local_media_search_paging($list['count'], $page, $perpage, $CFG->wwwroot . '/local/media/list.php', $page_params);
                ?>

                </div>
                <!--paging-->


<script type="text/javascript">

var listExcel = function() {
	event.preventDefault();
        $('#search_form').attr('action','list_excel.php');
        $('#search_form').submit();
};

var listSort = function(sort) {
	//event.preventDefault();
        $('#search_form').find('input[name=sort]').val(sort);
        $('#search_form').attr('action','list.php');
        $('#search_form').submit();
};

function key_code(){
    if (event.keyCode == 13){
        $('#search_form').submit();
    }
}
</script>


<?php
echo $OUTPUT->footer();
?>


