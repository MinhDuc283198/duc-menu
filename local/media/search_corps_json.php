<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$context = context_system::instance();
require_login();

$mediaid = optional_param('mediaid', 0, PARAM_INT);

$corpsList = local_media_get_corps($mediaid);
echo json_encode($corpsList);
?>


