<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot . "/local/media/lib.php";

$result_json = null;
$type = optional_param('type', 'user', PARAM_RAW);
$mediaid = optional_param('mediaid', 0, PARAM_INT);
$page = optional_param('page', 1, PARAM_INT);
$orgname = optional_param('orgname', '', PARAM_RAW);
switch ($type) {
    case 'user':
        $username = optional_param('username', '', PARAM_RAW);
        $result_json = local_media_get_user($username, $orgname, $mediaid, $page);
        break;
    case 'group':
        $result_json = local_media_get_organization($orgname, $mediaid, $page);
        break;
}

echo json_encode($result_json);
?>