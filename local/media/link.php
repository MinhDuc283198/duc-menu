<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot.'/login/lib.php';
require_once $CFG->dirroot.'/local/lmsdata/lib.php';
require_once $CFG->dirroot.'/local/media/lib.php';
require_once $CFG->dirroot.'/local/ssologin/lib.php';

$v = required_param('v', PARAM_RAW);
$decrypted = base64_decode($v);
list($id, $mode) = explode('||', $decrypted);

$userid = $USER->id;
if(!$userid){
    $sso_userid = $_SERVER['HTTP_IV_USER'];
    $sso_username = $_SERVER['HTTP_EMPLOYEENUMBER'];
    If (empty($sso_userid) || $sso_userid == 'Unauthenticated' || $sso_userid == 'unauthenticated'){
        $userid = 0;
    }else{
        if(!$sso_username){
            $sso_username = $sso_userid;
        }
        $userid = $DB->get_field('user','id',array('username'=>$sso_username));
    }
}

if(!$userid){
    require_login();
    die();
}

$encrypted = base64_encode($id.'||'.$userid.'||'.$mode);

redirect($CFG->local_media_player_url.'/local/media/linked.php?v='.$encrypted);



