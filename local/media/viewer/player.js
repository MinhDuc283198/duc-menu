function device_check(){
    
    //디바이스별 판별
    var _ua = window.navigator.userAgent.toLowerCase();
    var device = '';
    var browser = {
        ipod : /webkit/.test( _ua )&&/\(ipod/.test( _ua ),
        ipad : /webkit/.test( _ua )&&/\(ipad/.test( _ua ),
        iphone : /webkit/.test( _ua )&&/\(iphone/.test( _ua ),
        android : /webkit/.test( _ua )&&/android/.test( _ua ),
        msie : /msie/.test( _ua )
    };   
    
    if(browser.ipod || browser.iphone) {
        device = 'iphone';
    } else if(browser.ipad) {
        device = 'ipad';
    } else if(browser.android) {
        device = 'android';
    } else if(browser.msie){
        device = 'msie';
    } else {
        device = 'etc';
    }
    
    return _ua;
    
}


function playtime_update(posfrom,posto,posevent,duration,mode){
    var status = true;
    if(mode == 'player'){
    var device = device_check();
    
    $.ajax({
        url: 'playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: true,
        data: {
            act : 'update',
            positionto : posto,
            positionfrom : posfrom,
            positionevent : posevent,
            duration : duration,
            device : device,
            id : $('#mediaid').val()
        },
        success: function(data) {
            if(data.status == 'success') {
                status = true;
                var satiscompleted = $('#satiscompleted').val();
                var progress = data.progress;
                $('#id_lastview').text(data.last);
                $('#id_playtime').text(data.totaltime);
                if(progress == 100 && (satiscompleted == 0 || satiscompleted == undefined)){
                    progress = progress - 1;
                }
                $('#id_progress').text(progress+'%');
                
                //종료되었을 때 설문 팝업
                if (data.progress == 100 && (duration - posto) <= 2) {
                    pop_satisfaction();
                }
                
            }
        },
        error: function(e) {
            
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
            //playtime_update(id,posfrom,posto,duration,second);
            status = false;
        }
    });
    }
        
    return status;
}

function playtime_update_close(posfrom,posto,posevent,duration,mode){
    var status = false;
    if(mode == 'player'){
        var device = device_check();

        $.ajax({
            url: 'playtime_ajax.php',
            type: 'POST',
            dataType: 'json',
            async: true,
            data: {
                act : 'update',
                positionto : posto,
                positionfrom : posfrom,
                positionevent : posevent,
                duration : duration,
                device : device,
                id : $('#mediaid').val()
            },
            success: function(data) {
                if(data.status == 'success') {
                    status = true;
                    self.close();
                }
            },
            error: function(e) {

                //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                //playtime_update(id,posfrom,posto,duration,second);
                status = false;
            }
        });
    }else{
        status = true;
        self.close();
    }   
    return status;
}

function playtime_get(mode){
     
    var positionfrom = 0;
    
    if(mode == 'player'){
        
    var device = device_check();
        
    $.ajax({
        url: 'playtime_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            act : 'get',
            device : device, 
            id : $('#mediaid').val()
        },
        success: function(data) {
            if(data.status == 'success') {
                
                $('#id_lastview').text(data.last);
                //console.log(data.positionto);
                if(data.positionto > 0){
                     //이어보기
                     positionfrom = data.positionto;
                     
                }
                
            } else {
                //alert(data.message);
            }
        },
        error: function(e) {
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
        }
    });
    }
        
    return positionfrom;
    
}

function pop_satisfaction() {

    /*설문조사 팝업*/
    var mediaid = $('#mediaid').val();

    $("#popbg").height($("body").prop("scrollHeight"));
    $.ajax({
        url: "/local/media/pop_satisfaction.php",
        data: {id : mediaid},
        success: function (html) {
            $("#layerPop").html('<div id="pop_wrap"  class="learning_point">' + $(html).find("#pop_wrap").html() + '</div>');
            var top = $("body").scrollTop() + ((screen.availHeight - $("#layerPop").height()))/2 - 100;
            //var left = (screen.availWidth - 580)/2;
            //var top = "calc(50% - 100px)";
            var left = "calc(50% - 290px)";
            $("#layerPop").css({
                "left": left,
                "top": top
            });

            $("#popbg, #layerPop").fadeIn();

            $(".popclose").click(function () {
                $("#popbg, #layerPop").fadeOut();
            });
        }
    });


    $("#popbg, .popclose").click(function () {
        $("#popbg, #layerPop").fadeOut();
    });
    /*설문조사 팝업*/

}

function update_satisfaction(){
    
    if(!$('#id_satis_submit_form').find('input[name=score]:checked').val()){
        alert('점수를 선택하세요.');
        return false;
    }
    
    $.ajax({
        url: '/local/media/pop_satisfaction_ajax.php',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: $('#id_satis_submit_form').serialize(),
        success: function(data) {
            $("#popbg, #layerPop").fadeOut();
            $('.satisfaction_btn').empty().append('<p><button onclick="pop_satisfaction();">추천 완료</button></p>');
            $('#id_progress').text('100%');
           
        },
        error: function(e) {
            //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
        }
    });
    
}

function caption_scroll(position){
    
    var lang = $('#subtitleslang').val();
    if($('.script_list.'+lang).length > 0 && $('.script_list.'+lang+' div.subtext.sub'+position).length > 0){
        var scroll = $('.script_list.'+lang).scrollTop();
        var pos = $('.script_list.'+lang+' div.subtext.sub'+position).position().top - 200;
        var cur = scroll + pos;

        $('.script_list.'+lang+' div.subtext').css({'color':'#7d7d84','font-weight':'normal'});
        $('.script_list.'+lang+' div.subtext.sub'+position).css({'color':'#f34d78','font-weight':'600'});
        $('.script_list.'+lang).animate({scrollTop:cur}, 600);
    }
    
}

$(document).ready(function () {
    
    $('.tab a').click(function () {
        $('.mediainfo').hide();
        $('.tab li').removeClass('on');
        $('.mediainfo' + $(this).attr('number')).show();
        $('.tab' + $(this).attr('number')).addClass('on');
    });
    
    $('.mediainfo').hide();
    $('.mediainfo1').show();
    
    if($('.caption_area').length > 0){
        $('.captiontab').change(function () {
            $('.captioninfo').hide();
            $('.captioninfo' + $(this).val()).show();
            $('#subtitleslang').val($(this).find('option:selected').attr('lang'));
        });
        $('.captioninfo').hide();
        $.each($('.captiontab option'), function (index, value) {
            if($(value).attr('lang') == $('#subtitleslang').val()){
                $(value).attr('selected','selected');
                $('.captioninfo'+$(value).val()).show();
            }
        });
        $('.player_area').addClass("width70");
    }else{
        $('.player_area').addClass("width100");
    }
     
    /*동영상 학습페이지 닫기버튼*/
    $("#page-local-media-viewer-player .sub_contents").prepend("<div class='viewer_close'></div>");
    /*동영상 학습페이지 닫기버튼*/
    
    /**동영상 팝업 닫기**/
     $('.viewer_close').click(function(){
         //창 닫기 전에 진도율 저장
         popClosePause();
     });
     /**동영상 팝업 닫기**/
});

function pop_url_player(url){
    window.open(url,'urlpop','width=700, height=600');
}

/*
window.addEventListener("beforeunload", function (e) {
    var cook = $.cookie('video_event_<?php echo $id;?>_<?php echo $USER->id;?>');
    $.cookie('video_event_<?php echo $id;?>_<?php echo $USER->id;?>',null);
    if(cook=='1' || cook=='4' || cook=='5'){
        var confirmationMessage = "<?php echo get_string('beforeunload','lcms');?>";
        (e || window.event).returnValue = confirmationMessage;     // Gecko and Trident
        return confirmationMessage;
    }// Gecko and WebKit
});
*/

