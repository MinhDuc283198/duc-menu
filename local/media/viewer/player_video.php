<?php

//스트리밍 주소 가져오기

$hlsurls = '';
$streamurls = local_media_beecast_get_streaming_url($content->id);
$proxies = array();
foreach ($streamurls->ContentStreamingInfoData as $stream) {
    $proxies[$stream->UrlType] = array();
    $proxies[$stream->UrlType]['idx'] = $stream->UrlType;
    $proxies[$stream->UrlType]['url'] = $stream->StreamingUrl;
    $proxies[$stream->UrlType]['token'] = $stream->Token;
}

//테스트용
/*
$hlsurls = '';
$proxies = array();

$proxies['001']['idx'] = '001';
$proxies['001']['url'] = 'http://cdn.flowplayer.org/202777/84049-bauhaus.m3u8';
*/

$hlsurls = json_encode($proxies);

if($content->islogo == 1){
    $playerlogo = 'logo: "/theme/oklasscompany/pix/images/main_logo.png",';
}

?>

<style>
    #hlsjsvod .fp-logo{
        width: 150px;
        padding-left: 10px;
    }
</style>

<script src="../flowplayer7/flowplayer.min.js"></script>
<script src="../flowplayer7/hlsjs.min.js"></script>
<script src="player.js"></script>

<div id="hlsjsvod" class="is-closeable video_area">
    <div class="buttons">
        <span>0.75x</span>
        <span class="active">1x</span>
        <span>1.25x</span>
        <span>1.5x</span>
    </div>
</div>

<script>
    var hlsurls = <?php echo $hlsurls;?>;
    var positionfrom = 0, positionto = 0, duration = 0, oldposition = 0, saveposition = 0, ispause = false, capposition = 0, evented = 0;
    var satiscompleted = $('#satiscompleted').val();
    var mode = '<?php echo $mode?>';
    
    var urlExists = function(url) {

        var result = false;
        if(url != undefined && url != ''){
            $.ajax({
                type: 'HEAD',
                url: url,
                async: false,
                success: function() {result = true;}//$.proxy(callback, this, true),
            });
        }
        
        return result;
    };
    
    window.onload = function () {
        $.each(hlsurls, function(idx) {
            
            var result = urlExists(hlsurls[idx]['url']);
            
            if(result == true){     
                flowplayer_load(hlsurls[idx]['url']);
                return false;
            }
        });
    };
    
    function flowplayer_load(hlsurl){
        
        flowplayer("#hlsjsvod", {
            key: "<?php echo get_config('local_repository','flowplayer_key')?>",
            <?php echo $playerlogo;?>
            splash: false,
            autoplay: true,
            embed: false, // setup would need iframe embedding
            ratio: 5 / 12,    
            // manual HLS level selection for Drive videos
            hlsQualities: "drive",

            // manual VOD quality selection when hlsjs is not supported
            //defaultQuality: "260p",
            //qualities: ["160p", "260p", "530p", "800p"],
            
            speeds: [0.75,1,1.25,1.5],

            clip: {
                sources: [
                   {
                       type: "application/x-mpegurl",
                       src: hlsurl
                   }
                ]
                <?php echo $flowsubtitles; ?>
            }

        });

    }
    
    
    
    /* global event listeners for demo purposes, omit in production */
    flowplayer(function (api, root) {
        var instanceId = root.getAttribute("data-flowplayer-instance-id"),
                engineInfo = document.getElementById("engine" + instanceId),
                vtypeInfo = document.getElementById("vtype" + instanceId),
                detail = document.getElementById("detail" + instanceId);
                
                var speedelement = root.querySelector(".buttons"),
                buttonElements = speedelement.getElementsByTagName("span"),
                buttons = [],
                i;
                
                for (i = 0; i < buttonElements.length; i += 1) {
                    buttons.push(buttonElements[i]);
                };

        api.on("ready", function (e, api, video) {
            var engineName = api.engine.engineName;

            // remove speed buttons if playback rate changes are not available
            if (api.engine.engineName == "flash" || !flowplayer.support.inlineVideo) {
              speedelement.parentNode.removeChild(speedelement);
            } 

            positionto = playtime_get(mode);
            positionfrom = positionto;
            duration = Math.ceil(video.duration);
            if(positionfrom < duration){
                api.seek(positionfrom);
            }
        }).on("speed", function (e, api, rate) {
            // mark the current speed button as active
            var i;

            speedelement.querySelector(".active").removeAttribute("class");
            for (i = 0; i < api.conf.speeds.length; i += 1) {
                if (api.conf.speeds[i] == rate) {
                    buttons[i].className = "active";
                    break;
                }
            }
        }).on("resume", function (e) {
            //플레이 버튼 클릭 시 호출
            //console.log('resume::'+positionfrom+'::'+positionto);
            if(positionto > 1){
                positionfrom = positionto;
            }
            evented = 0;
        }).on("pause", function (e, api) {
            //Pause 버튼 클릭 시 호출
            //console.log('pause::'+positionfrom+'::'+positionto);
            if (positionfrom < positionto) {
                playtime_update(positionfrom, positionto, 1, duration, mode);
            }
        }).on("beforeseek", function (e, api) {
            //seek 버튼 클릭 시 호출
            //console.log('beforeseek::'+positionfrom+'::'+positionto);
            if (positionfrom < positionto) {
                playtime_update(positionfrom, positionto, 2, duration, mode);
            }
        }).on("seek", function (e, api) {
            //seek 버튼 클릭 후 seek되면 호출
            //console.log('seek::'+positionfrom+'::'+positionto);
            if(positionto > 1){
                positionfrom = positionto;
            }
            evented = 0;
        }).on("finish", function (e, api) {
            //console.log('finish::'+positionfrom+'::'+positionto);
            if (evented == 0 && positionfrom < duration) {
                playtime_update(positionfrom, duration, 3, duration, mode);
            }
            positionfrom = positionto = 0;
            evented = 1;
        }).on("progress", function (e, api) {
            //타임라인
            positionto = Math.ceil(api.video.time);
            
            //진도율 저장 (3분마다 진도율을 저장함.)
            //console.log(positionfrom+'::'+saveposition+'::'+positionto);
            if(evented == 0 && positionfrom < positionto && positionto % (60*3) == 0){
               playtime_update(positionfrom, positionto, 5, duration, mode);
               saveposition = positionto;
               positionfrom = positionto;
            }
            

        }).on("cuepoint", function (e, api, cuepoint) {

            if (cuepoint.subtitle) {
                oldposition = parseInt(cuepoint.time);
                caption_scroll(oldposition);
            }

        });
        
        // bind speed() call to click on buttons
        buttons.forEach(function (button) {

            button.onclick = function () {
                var i;

                if (button.classNme != "active") {
                    for (i = 0; i < api.conf.speeds.length; i += 1) {
                        if (buttons[i] === button) {
                            api.speed(api.conf.speeds[i]);
                            break;
                        }
                    }
                }
            };

        });
        
    });
    /* end global event listeners setup */

    function popClosePause(){
        playtime_update_close(positionfrom, positionto, 4, duration, mode);
    }
    
</script>


