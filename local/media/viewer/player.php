<?php
require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/lmsdata/lib.php');
require_once(dirname(dirname(__FILE__)) . '/lib.php');
require_once(dirname(dirname(__FILE__)) . '/playerlib.php');
require_once($CFG->libdir.'/filelib.php');

$v = required_param('v', PARAM_RAW);
$decrypted = base64_decode($v);
list($id, $userid, $mode) = explode('||', $decrypted);

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->set_url('/local/media/viewer/player.php', array('v' => $v));

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->css('/local/media/flowplayer7/skin/skin.css');
//$PAGE->requires->css('/local/media/flowplayer/skin/functional.css');
//$PAGE->requires->css('/local/media/flowplayer/skin/quality-selector.css');
$PAGE->requires->css('/theme/oklasscompany/style/popup.css');
$PAGE->requires->js('/theme/oklasscompany/javascript/lightslider.js');
$PAGE->requires->css('/theme/oklasscompany/style/lightslider.css');
echo $OUTPUT->header();

//권한 체크 (To-do)
//사내 사외 여부 체크함.
local_media_inout_check($id);

//디튜브 동영상 데이터 가져오기
if (!$content = $DB->get_record('lmsmedia_contents', array('id' => $id))) {
    print_error(get_string('nocontent','local_media'));
    die();
}

$viewcount = $content->viewcount + 1;
$DB->set_field('lmsmedia_contents', 'viewcount', $viewcount, array('id' => $content->id));

//디튜브 동영상 파일 가져오기 (파일은 API로 가져올 예정 todo)
$file = $DB->get_record('lmsmedia_files', array('mediaid' => $id));

//설문 데이터 가져오기
$satiscompleted = 0;
if ($satis = $DB->get_record('lmsmedia_satisfaction', array('mediaid' => $content->id, 'userid' => $USER->id))) {
    $satiscompleted = 1;
}

//진도율 가져오기
$lastview = '0000-00-00 00:00';
$playtime = '00:00';
$progress = '0%';
$progress2 = 0;
$studying = get_string('beforelearn','local_media');
$satisenable = 0;
if ($track = $DB->get_record('lmsmedia_track', array('mediaid' => $content->id, 'userid' => $USER->id))) {
    $lastview = date('Y-m-d H:i', $track->timeview);
    $ptm = local_media_time_from_seconds($track->playtime);
    $playtime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
    $progress = ($satiscompleted == 1 || $track->progress < 100) ? $track->progress . '%' : ($track->progress - 1) . '%';
    $studying = ($track->progress < 100) ? get_string('learning','local_media') : get_string('complete','local_media');
    if($track->progress == 0){$studying = get_string('beforelearn','local_media');}
    if ($tarack->progress == 100) {
        $satisenable = 1;
    }
}

//좋아요 수치
$scoretext = local_media_get_npsscore($content->id);

//댓글 데이터 가져오기
$comment_count = $DB->count_records('lmsmedia_comments', array('mediaid' => $content->id));
$sql = "select c.*, u.ssousername from {lmsmedia_comments} c join {lmsdata_user} u on u.userid = c.userid where c.mediaid = :mediaid order by c.id desc";
$comments = $DB->get_records_sql($sql, array('mediaid' => $content->id));

//공유 URL
$share_src = local_lmsdata_shared_media_url_link($content->id, 'player');
$embed_src = local_lmsdata_shared_media_url_link($content->id, 'embed');
$embed_code = '<iframe width="560" height="315" src="' . $embed_src . '" frameborder="0" allowfullscreen></iframe>';

//자막 데이터 가져오기
$captions = $DB->get_records('lmsmedia_captions', array('mediaid' => $content->id));
$currentlang = current_language() == 'zh_cn' ? 'zh' : current_language();
$subtitles = '';
$flowsubtitles = '';
$azursubtitles = '';
if ($captions) {
    $flowsubtitles .= ', subtitles: ';
    $azursubtitles .= ', ';
    $subtitles .= '[';
    $labels = array('ko' => 'Korean', 'en' => 'English', 'zh' => 'Chinese', 'ja' => 'Japanese', 'vi' => 'Vietnamese');
    $i = 0;
    foreach ($captions as $caption) {
        if($i == 0){ 
            $firstlang = $caption->lang; 
        }
        if($caption->lang == $currentlang){
            $defaultlang = $currentlang;
            break;
        }
        $i++;
    }
    if(!$defaultlang){
        $currentlang = $firstlang;
    }
    foreach ($captions as $caption) {
        $captionpath = str_replace($CFG->local_media_file_path, $CFG->wwwroot . '/storage', $caption->filepath);
        $subtitles .= '{';
        if ($currentlang == $caption->lang) {
            $subtitles .= '"default": true, ';
            $defaultsubtitle = $captionpath;
            $captionurl = 'captionUrl:"'.$defaultsubtitle.'",';
        }
        $subtitles .= 'kind: "subtitles", srclang: "' . $caption->lang . '", label: "' . $labels[$caption->lang] . '",
              src:  "' . $captionpath . '"';
        $subtitles .= '},';
    }
    $subtitles .= ']';
    $flowsubtitles .= $subtitles;
    $azursubtitles .= $subtitles;
}

$userinfo = $DB->get_record('lmsdata_user', array('userid' => $USER->id));

//시리즈 데이터 가져오기
if ($content->groupid) {
    
    $dtube_sql = "SELECT lc.*
     , f.duration, g.name as groupname 
     , lnps.npsscore, lnps.responses ";
    $dtube_from = "FROM tb_lmsmedia_contents lc 
    JOIN {lmsmedia_groups} g ON g.id = lc.groupid 
    LEFT JOIN {lmsmedia_files} f ON f.mediaid = lc.id
    LEFT JOIN {lmsdata_nps} lnps ON lnps.type = 2 AND lnps.curriculumid = lc.id ";
    $dtube_where = "WHERE lc.isused = 1 and lc.isenabled = 1 
  AND lc.gubun in ('1','3') 
  AND (       
        lc.userid = $USER->id  
      or   
      exists 
      (
         select 1 from tb_lmsmedia_corps co 
            where lc.id = co.mediaid
            and   lc.target = '1' 
            and co.corporationcode = '$userinfo->corporationcode'        
         union all             
            select 1 from tb_lmsmedia_users lu
            where lc.id = lu.mediaid
            and lc.target = '2' 
            and lu.gubun = '2' 
            and lu.userid = $USER->id             
        union all            
            select 1 from tb_lmsmedia_users lu2
            where lc.id = lu2.mediaid
            and lc.target = '2' 
            and lu2.gubun = '1' 
            and INSTR('$organizationcodes', lu2.organizationcode) > 0 
        )
    )
    AND lc.target != 3 AND lc.groupid = :groupid order by lc.sortorder asc, lc.id asc";

$dtube_params = array('userid1'=>$USER->id, 
    'userid2'=>$USER->id, 
    'corporationcode'=>$userinfo->corporationcode,
    'organizationcodes'=>'d',
    'groupid'=>$content->groupid);
 
    $series = $DB->get_records_sql($dtube_sql.$dtube_from.$dtube_where, $dtube_params);
}

//관련 동영상 가져오기
//태그 가져오기
//get_records_sql_menu($sql, array $params=null, $limitfrom=0, $limitnum=0) //CURRICULUM_CLASS_TAG_L2_CODE
$tagssql = "select lt.id, lt.tagcode from {lmsdata_course_tag} lt join {lmsdata_categories} ca on lt.tagcode = ca.codevalue "
        . "where lt.curriculumid = :curriculumid and type = :type and ca.code = :code";
$existing_tags = $DB->get_records_sql_menu($tagssql, array('curriculumid' => $id, 'type' => '2', 'code' => 'CURRICULUM_CLASS_TAG_L2_CODE'));
if ($existing_tags) {
    $taglist = "'" . implode("','", $existing_tags) . "'";
 
    $dtube_sql = "SELECT lc.*
     , f.duration
     , lnps.npsscore, lnps.responses ";
    $dtube_from = "FROM tb_lmsmedia_contents lc
    LEFT JOIN {lmsmedia_files} f ON f.mediaid = lc.id
    LEFT JOIN {lmsdata_nps} lnps ON lnps.type = 2 AND lnps.curriculumid = lc.id ";
    $dtube_where = "WHERE lc.isused = 1 and lc.isenabled = 1 
  AND lc.gubun in ('1','3') 
  AND (
      exists(
          select 1 from {lmsdata_course_tag} lt 
          where lt.tagcode in (" . $taglist . ") and lt.curriculumid = lc.id and type = '2' 
      )
  )
  AND (       
        lc.userid = $USER->id  
      or   
      exists 
      (
         select 1 from tb_lmsmedia_corps co
            where lc.id = co.mediaid
            and   lc.target = '1' 
            and co.corporationcode = '$userinfo->corporationcode'        
         union all             
            select 1 from tb_lmsmedia_users lu
            where lc.id = lu.mediaid
            and lc.target = '2' 
            and lu.gubun = '2' 
            and lu.userid = $USER->id             
         union all            
            select 1 from tb_lmsmedia_users lu2
            where lc.id = lu2.mediaid
            and lc.target = '2' 
            and lu2.gubun = '1' 
            and INSTR('$organizationcodes', lu2.organizationcode) > 0 
      )          
   )
  AND lc.target != '3' order by lc.id desc";

    $dtube_params = array('userid1'=>$USER->id, 
        'userid2'=>$USER->id, 
        'corporationcode'=>$userinfo->corporationcode,
        'organizationcodes'=>'d',
    );
 
    $tags = $DB->get_records_sql($dtube_sql.$dtube_from.$dtube_where, $dtube_params, 0, 10);
}
?>  

<form name="playtime_form" id="playtime_form">
    <input type="hidden" name="id" id="mediaid" value="<?php echo $content->id; ?>"/>
    <input type="hidden" name="satiscompleted" id="satiscompleted" value="<?php echo $satiscompleted; ?>"/>
    <input type="hidden" name="subtitleslang" id="subtitleslang" value="<?php echo $currentlang;?>"/>
</form>

<div id="contents">
    <div class="bg">
        <div class="group">

            <div class="sub_contents full padding margin">
                <span class="close_text"><?php echo get_string('close_text','local_media'); ?></span>
                <!--title-->
                <h3 class="video_title"><?php echo $content->title; ?></h3>
                <!--title-->
                <div class="video_group">
                    <!--학습정보-->
                    <div class="video_info">
                        <div class="left_cont">         
                            <div class="learn_button inverse"><?php echo $studying; ?></div>
                            <div class="edu_con">
                                <p><?php echo get_string('lastedu','local_media'); ?></p>
                                <p id="id_lastview"><?php echo $lastview; ?></p>
                            </div>
                            <div class="edu_con">
                                <p><?php echo get_string('edutime','local_media'); ?></p>
                                <p id="id_playtime"><?php echo $playtime; ?></p>
                            </div>
                            <div class="edu_con">
                                <p><?php echo get_string('studyrate','local_media'); ?></p>
                                <p id="id_progress"><?php echo $progress; ?></p>
                            </div>       
                            <div class="edu_con satisfaction_btn">
<?php if ($satiscompleted == 0) { ?>
                                    <p><button onclick="pop_satisfaction();"><?php echo get_string('videorecommend','local_media'); ?></button></p>   
<?php } else { ?>
                                    <p><button onclick="pop_satisfaction();"><?php echo get_string('recommendcomplete','local_media'); ?></button></p>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="right_cont">
                            <span class="search_count">
                                <img src="/theme/oklasscompany/pix/images/search_light.png" alt="조회수" title="조회수" /><?php echo $content->viewcount; ?> 
                            </span>
                            <span class="like">
                                <img src="/theme/oklasscompany/pix/images/like.png" alt="좋아요" title="좋아요" /><?php echo $scoretext; ?>
                            </span>
                        </div>
                    </div>
                    <!--학습정보-->
                    <!--플레이어 영역 시작-->
                    <div class="player_area">
<?php
if ($content->contype == 'video') {
    $brows = local_media_browser_check();
    if ($brows->device == 'P' && $brows->browser == 'IE') {
        include_once('player_flash.php');
    } else {
        include_once('player_video.php');
    }
} else if ($content->contype == 'youtube') {
    include_once('player_youtube.php');
} else if ($content->contype == 'vimeo') {
    include_once('player_vimeo.php');
} else if ($content->contype == 'url') {
    include_once('player_url.php');
}
?>
                    </div>
                    <!--플레이어 영역 끝-->

                    <!--자막영역 시작-->

<?php
if ($captions) {
    echo '<div class="caption_area">';
    foreach ($captions as $caption) {
        $path = $caption->filepath;
        echo '<div class="captioninfo captioninfo' . $caption->id . '">';
        if (file_exists($path)) {
            $subs = local_media_get_captions($path);
            echo '<div class="script_list ' . $caption->lang . '">';
            foreach ($subs as $sub) {
                $timestart = local_media_seconds_from_time($sub->starttime);
                echo '<div class="subtext sub' . $timestart . '">' . $sub->text . '</div>';
            }
            echo '</div>';
        } else {
            echo '<span>'.get_string('nosubtitle','local_media').'.</span>';
        }
        echo '</div>';
    }
    echo '</div>';
}
?>

                    <!--자막영역 끝-->

                </div>

                <!--시리즈 관련동영상보기-->
                <div class="more_video">
<?php if ($series) { ?>
                    <input type="button" value="<?php echo get_string('series','local_media'); ?> ▼" class="button_style01 button gray" data-block="more_serise" />
<?php } ?>
<?php if ($tags) { ?>
                    <input type="button" value="<?php echo get_string('similarvideo','local_media'); ?> ▼" class="button_style01 button gray" data-block="more_ivideo" />
                    <?php } ?>
                        <?php if($captions){
                            echo '<input type="button" value="'.get_string('viewsubtitle','local_media').'" class="button_style01 gray show" />';
                            echo '<select class="captiontab">';
                            foreach ($captions as $caption) {
                                $captionpath = str_replace($CFG->local_media_file_path, $CFG->wwwroot . '/storage', $caption->filepath);
                                echo '<option class="captiontab' . $caption->id . '" value = ' . $caption->id . ' lang = "' . $caption->lang . '" subtitle = "'.$captionpath.'">' . $labels[$caption->lang] . '</option>';
                            }
                            echo '</select>';
                        }?>

                    <div class="more_video_view">
                    <?php if ($series) { ?>   
                            <div class="more_serise blocks">
                                <div class="dtube_thumbnail">
                                    <div class="thumbslide">
    <?php
    foreach ($series as $seri) {
        $encrypted = base64_encode($seri->id.'||'.$userid.'||'.$mode);
        $serititle = str_replace($seri->groupname,'',$seri->title);
        $serititle = trim(str_replace(':','',$serititle));
        $thumbimage = '/theme/oklasscompany/pix/images/sample02.jpg';
        $thumbpath = $DB->get_field('lmsmedia_thumbs','filepath',array('mediaid'=>$seri->id, 'isused'=>1));
        if ($seri->contype == 'vimeo' || $seri->contype == 'youtube') {  
            $embedinfo = local_media_get_embed_info($seri->mediafile, $seri->contype);
            $duration = local_media_get_duration_hms($embedinfo->duration);
            $point = ceil($embedinfo->duration / 1800) / 2;
        }else{
            $duration = local_media_get_duration_hms($seri->duration);
            $point = ceil($seri->duration / 1800) / 2;
        }
        if (file_exists($thumbpath)) {
            $thumbimage = str_replace($CFG->local_media_file_path, $CFG->wwwroot . '/storage', $thumbpath);
        } else {
            //vimeo, youtube일 경우는 api로 썸네일 호출
            if ($seri->contype == 'vimeo' || $seri->contype == 'youtube') {    
                $thumbimage = $embedinfo->thumbnail;
            }
        }
        ?>
                                            <div class="thumb thumb02 <?php if($id == $seri->id){ echo "on";} ?>" onclick="location.href='player.php?v=<?php echo $encrypted;?>';">
                                                <p class="img">
                                                    <img src="<?php echo $thumbimage;?>" alt="list" title="list" />
                                                    <span class="time"><?php echo $duration; ?></span>
                                                </p>
                                                <div class="text">
                                                    <h4><?php echo $serititle; ?></h4>
                                                    <p class="sub_title">
                                                        :DTube <span class="pink"><?php echo $point; ?>p</span>
                                                    </p>
                                                </div>
                                                <span class="hover">
                                                    <img src="/theme/oklasscompany/pix/images/play_hover.png" alt="play" title="play" />
                                                </span>
                                            </div> 
    <?php } ?>  

                                    </div>
                                </div>
                            </div>
                                    <?php } ?>  
<?php if ($tags) { ?>
                            <div class="more_ivideo blocks">
                                <div class="dtube_thumbnail">
                                    <div class="thumbslide">
                            <?php
                            foreach ($tags as $tag) {
                                $encrypted = base64_encode($tag->id.'||'.$userid.'||'.$mode);
                                $thumbimage = '/theme/oklasscompany/pix/images/sample02.jpg';
                                $thumbpath = $DB->get_field('lmsmedia_thumbs','filepath',array('mediaid'=>$tag->id, 'isused'=>1));
                                if ($tag->contype == 'vimeo' || $tag->contype == 'youtube') {  
                                    $embedinfo = local_media_get_embed_info($tag->mediafile, $tag->contype);
                                    $duration = local_media_get_duration_hms($embedinfo->duration);
                                    $point = ceil($embedinfo->duration / 1800) / 2;
                                }else{
                                    $duration = local_media_get_duration_hms($tag->duration);
                                    $point = ceil($tag->duration / 1800) / 2;
                                }      
                                if (file_exists($thumbpath)) {
                                    $thumbimage = str_replace($CFG->local_media_file_path, $CFG->wwwroot . '/storage', $thumbpath);
                                } else {
                                    //vimeo, youtube일 경우는 api로 썸네일 호출
                                    if ($tag->contype == 'vimeo' || $tag->contype == 'youtube') {    
                                        $thumbimage = $embedinfo->thumbnail;
                                    }
                                }
                                ?> 
                                            <div class="thumb thumb02" onclick="location.href='player.php?v=<?php echo $encrypted;?>';">
                                                <p class="img">
                                                    <img src="<?php echo $thumbimage;?>" alt="list" title="list" />
                                                    <span class="time"><?php echo $duration; ?></span>
                                                </p>
                                                <div class="text">
                                                    <h4><?php echo str_replace('&','&amp;',$tag->title); ?></h4>
                                                    <p class="sub_title">
                                                        :DTube <span class="pink"><?php echo $point; ?>p</span>
                                                    </p>
                                                </div>
                                                <span class="hover">
                                                    <img src="/theme/oklasscompany/pix/images/play_hover.png" alt="play" title="play" />
                                                </span>
                                            </div> 
    <?php } ?>  

                                    </div>
                                </div>

                            </div>
<?php } ?>  
                    </div>
                </div>
                <!--시리즈 관련동영상보기-->

                <!--동영상정보-->
                <div class="infodiv">
                    <div class="more_text">
<?php 
$content->intro = file_rewrite_pluginfile_urls($content->intro, 'pluginfile.php', $context->id, 'local_media', 'intro', $content->id);
echo $content->intro; 
?>
                    </div>
                    <button class="moreview"><?php echo get_string('viewmore','local_media'); ?></button>
                </div>
                <div class="line_menu tab">
                    <ul class="noline">
                        <li class="tab1 on"><a href="#1" number="1"><?php echo get_string('comment','local_media'); ?>(<span id="comment-count"><?php echo $comment_count; ?></span>)</a></li>
                        <li class="tab2"><a href="#2" number="2"><?php echo get_string('share','local_media'); ?></a></li>
                    </ul>
                </div>
                <div class="tab_con">
                    <div class="infodiv mediainfo mediainfo1">
                        <div class="comment-regis-area">
                            <form name="comment_submit_form" id="id_comment_submit_form">
                                <input type="hidden" name="mediaid" value="<?php echo $content->id; ?>"/>
                                <textarea name="comments" id="comments"  placeholder="<?php echo get_string('inputcomment','local_media'); ?>."></textarea>
                                <div class="comment-regis-btn">
                                    <a class="comment-btn" onclick="return update_comment();"><?php echo get_string('submit','local_media'); ?></a>
                                </div>
                                
                        <ul class="comment-list-area">
<?php
if ($comments) {
    foreach ($comments as $comment) {
        $commentdel = '';
        if($comment->userid == $USER->id || is_siteadmin()){
            $commentdel = '<a href="javascript:comment_del('.$comment->id.');"><span class="comment_del">'.get_string('delete','local_media').'</span></a>';
        }
        echo '<li id="comment_list_'.$comment->id.'">
                                    ' . $comment->comments . '
                                    ' . $commentdel . '
                                    <p class="comment-regis-info">
                                    <span class="comment-regis-user">' . $comment->ssousername . '</span>
                                    <span class="comment-regis-user">' . date('Y-m-d H:i', $comment->timecreated) . '</span>
                                    </p>
                                    </li>';
    }
} else {
    echo '<li class="comment-empty">'.get_string('nocomment','local_media').'.</li>';
}
?>
                        </ul>
                    </div>
                </div>
               <div class="infodiv mediainfo mediainfo2">
                        <h6>URL</h6>
                        <div id="linkurl" style="border:1px solid #ccc;padding:20px;margin-bottom:20px;"></div>
                        <h6>Embeded Code</h6>
                        <div id="embed_code" style="border:1px solid #ccc;padding:20px;margin-bottom:20px;"></div>
                </div>
                <!--동영상정보-->


            </div>

        </div>
    </div>
</div>

<div id="layerPop" class="learning_pointPop"></div>
<div id="popbg"></div>

<script>
    
    
    var slider = [null, null];
    $(document).ready(function () {

        $('#linkurl').text('<?php echo $share_src; ?>');
        $('#embed_code').text('<?php echo $embed_code; ?>');
        
        if($(".caption_area").is(":visible")){$(".more_video > .show").attr("value","<?php echo get_string('hidesubtitle','local_media'); ?>");}
        $(".more_video > .show").click(function(){
             if($(".caption_area").is(":visible")){
                 $(".caption_area").hide();
                 $(".player_area").width("100%");
                 $(".more_video > .show").attr("value","<?php echo get_string('viewsubtitle','local_media'); ?>");
             }else{
                 $(".caption_area").show();
                 $(".player_area").width("70%");
                 $(".more_video > .show").attr("value","<?php echo get_string('hidesubtitle','local_media'); ?>");
             }
         });
         
    /*시리즈리스트 넘버추가 & 시리즈명 지우기*/
    $(".more_serise .thumb02").each(function(){
        var num = $(this).index() +1;
        $(this).append("<div class='sNum'>" + num +"</div>");

        //var chtx = $(this).find(".text > h4").text().split(": ");
       // $(this).find(".text > h4").text(chtx[chtx.length - 1]);
    });
    /*시리즈리스트 넘버추가*/
        
    /*동영상 썸네일*/   
     $(".more_video > .button").click(function(){
         var showBlock = $(this).attr("data-block") ;
         $(".more_video .button").each(function(){
             $(this).attr("value", $(this).attr("value").split("▲").join("▼")); 
         });   
        
        if(!$("." + showBlock).hasClass("on")){
            $(".more_video_view .blocks").removeClass("on");
            $("." + showBlock).addClass("on");
            $(".more_video > .button").addClass("gray");
            $(this).removeClass("gray");
            
            $(this).attr("value", $(this).attr("value").split("▼").join("▲"));
            
            if(slider[$(this).index()] != null){
                slider[$(this).index()].destroy();
            }
            var goTo = null, loop = false;
            if(showBlock == 'more_serise'){
                goTo = $(".thumb02.on").index();
                loop = true;
            }
            slider[$(this).index()] = imgSlider($("." + showBlock).find(".thumbslide"), [4.5, 3.5, 3, 2], 1, false, goTo, loop);
        }else{
           $(this).addClass("gray");
           $("." + showBlock).removeClass("on");
           $(this).attr("value", $(this).attr("value").split("▲").join("▼"));
        }
     });
    
     /*동영상 썸네일*/ 

    });
    
    function update_comment(){
    var text = $('textarea#comments').val().length;
    if(text>100){
        alert('<?php echo get_string('limittext','local_media','100'); ?>');
        die();
    }else
        if(!$('#id_comment_submit_form').find('textarea[name=comments]').val()){
            
            alert('<?php echo get_string('inputcomment','local_media');?>');
            return false;
        }

        $.ajax({
            url: '/local/media/comment_ajax.php',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: $('#id_comment_submit_form').serialize(),
            success: function(data) {
                if(data.status == 'success'){
                    $('#id_comment_submit_form').find('textarea[name=comments]').val('');
                    $('.comment-empty').hide();
                    $('#comment-count').html(data.count);
                    var commenthtml = '<li id="comment_list_'+data.id+'">'+data.comments+'<a href="javascript:comment_del('+data.id+');"><span class="comment_del"><?php echo get_string('delete','local_media');?></span></a><p class="comment-regis-info"><span class="comment-regis-user">'
                            +data.user+'</span><span class="comment-regis-user">'+data.date+'</span></p>'
                            +'</li>';
                                    
    $('.comment-list-area').prepend(commenthtml);

                }
            },
            error: function(e) {
                //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
            }
        });

    }
    
    function comment_del(id){
        var conf = confirm('<?php echo get_string('confirmdelete','local_media');?>');
        if(conf){    
            $.ajax({
                url: '/local/media/comment_ajax.php',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {id: id, mode: 'del'},
                success: function(data) {
                    if(data.status == 'success'){
                        $('#comment-count').html(data.count);
                        $('#comment_list_'+id).remove();
                    }
                },
                error: function(e) {
                    //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                }
            });   
        }
    }
    
    

</script>


<?php
echo $OUTPUT->footer();
?>


