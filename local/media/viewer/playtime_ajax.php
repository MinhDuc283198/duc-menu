<?php

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)).'/playerlib.php');

global $DB, $USER;

$returnvalue = new stdClass();

$act = required_param('act', PARAM_RAW);
$device = required_param('device', PARAM_RAW);
$event =  optional_param('positionevent', 0, PARAM_INT);
$id = required_param('id', PARAM_INT);
$media = $DB->get_record('lmsmedia_contents', array('id' => $id));

if ($USER && $media) {
    
    $track = $DB->get_record('lmsmedia_track', array('mediaid' => $media->id, 'userid' => $USER->id));

    if ($act == 'get') {

        if ($track) {
            $track->attempts = $track->attempts + 1;
            $track->device = $device;
            $track->timeview = time();
            $DB->update_record('lmsmedia_track', $track);
            
        } else {

            $track = new stdClass();
            $track->mediaid = $media->id;
            $track->userid = $USER->id;
            $track->timestart = time();
            $track->timeview = time();
            $track->attempts = 1;
            $track->device = $device;
            $DB->insert_record('lmsmedia_track', $track);
        }
        
        $returnvalue->last = date('Y-m-d H:i:s', $track->timeview);
        $returnvalue->progress = $track->progress;
        $returnvalue->positionto = $track->lasttime;
        
    } else if ($act == 'update') {

        $lcms = new stdClass();
        $lcms->userid = $USER->id;
        $lcms->mediaid = $media->id;
        $lcms->positionto = optional_param('positionto', 0, PARAM_INT);
        $lcms->positionfrom = optional_param('positionfrom', 0, PARAM_INT);
        $lcms->positionevent = optional_param('positionevent', 0, PARAM_INT);
        $lcms->lastdevice = $device;
        $duration = optional_param('duration', 0, PARAM_INT);
        $lcms->timecreated = time();

        if ($lcms->positionto > 0 && $duration > 1) {
            
            if($lcms->positionto > $duration){
                $lcms->positionto = $duration;
            }

            $query = 'select count(*) from {lmsmedia_playtime} 
                where userid=:userid and mediaid=:mediaid and positionfrom<=:positionfrom and positionto>=:positionto';
            $params = array('userid' => $USER->id, 'mediaid' => $media->id, 'positionto' => $lcms->positionto, 'positionfrom' => $lcms->positionfrom);
            $playcount = $DB->count_records_sql($query, $params);

            //플레이시간을 저장한다. 
            if ($playcount == 0) {
                $DB->insert_record('lmsmedia_playtime', $lcms);
            }
            
            if (!$track) {
                $track = new stdClass();
                $track->mediaid = $media->id;
                $track->userid = $USER->id;
                $track->timestart = time();
                $track->attempts = 1;
            }
            if($duration == $lcms->positionto){
                $track->lasttime = 0;
            }else{
                $track->lasttime = $lcms->positionto;
            }
            $track->playtime = local_media_get_progress($media->id, $USER->id);
            $track->progress = round($track->playtime / $duration * 100);
            $track->timeview = time();
            $track->device = $device;
            
            if ($track) {
                $DB->update_record('lmsmedia_track', $track);
            } else {
                $DB->insert_record('lmsmedia_track', $track);
            }

            $returnvalue->last = date('Y-m-d H:i:s', $track->timeview);
            $ptm = local_media_time_from_seconds($track->playtime);
            $returnvalue->totaltime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
            $returnvalue->progress = $track->progress;
        }
    }

    $returnvalue->status = 'success';
    
    //setcookie('video_event_'.$id.'_'.$USER->id, $event,time()+3600);
}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

