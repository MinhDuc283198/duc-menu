<?php

/*
* DeliveryType - 001: 내부, 002: 외부CDN
* DeliveryStep - 001:원본입수, 002:Transcoding, 003:입수완료, 004:배포
* StatusCode - 001: 대기, 002: 진행중, 003: 완료
*/
//배포상태 확인

$pathinfo = pathinfo($file->filepath);
$filename = $pathinfo['filename'];
//$streamurl1 = 'http://10.118.67.48:8090/'.$filename.'/'.$filename.'.m3u8';
$streamurl1 = 'http://lgdisplaymediasvc.streaming.mediaservices.windows.net/3f0bbe34-cf96-4b4a-bc2a-7dddfa2bb91a/HR_NewsRoom_1080.ism/manifest(format=m3u8-aapl)';
//$streamurl1 = 'http://165.243.185.246:8090/'.$filename.'/'.$filename.'.m3u8';
//$streamurl1 = 'http://lgdisplaymediasvc.streaming.mediaservices.windows.net/82807e90-334d-4233-80c8-b902fee6e98d/HR_NewsRoom_1080.ism/manifest(format=m3u8-aapl)';
//$streamurl1 = 'http://172.19.120.60:8090/hr-newsroom1/hr-newsroom1.m3u8';
$streamsource = '{';
$streamsource .= 'type: "application/x-mpegurl",';
$streamsource .= 'src: "'.$streamurl1.'",';
$streamsource .= '},';

if($content->islogo == 1){
    $playerlogo = 'logo: "/theme/oklasscompany/pix/images/main_logo.png",';
}

?>

<style>
    #hlsjsvod .fp-logo{
        width: 150px;
        padding-left: 10px;
    }
</style>

<script src="../flowplayer7/flowplayer.min.js"></script>
<script src="../flowplayer7/hlsjs.min.js"></script>
<script src="player.js"></script>

<script>

    /* global event listeners for demo purposes, omit in production */
    flowplayer(function (api, root) {
        var instanceId = root.getAttribute("data-flowplayer-instance-id"),
                engineInfo = document.getElementById("engine" + instanceId),
                vtypeInfo = document.getElementById("vtype" + instanceId),
                detail = document.getElementById("detail" + instanceId);

        var speedelement = root.querySelector(".buttons"),
                buttonElements = speedelement.getElementsByTagName("span"),
                buttons = [],
                i;

        var positionfrom = 0, positionto = 0, duration = 0, oldposition = 0;

        var satiscompleted = $('#satiscompleted').val();

        for (i = 0; i < buttonElements.length; i += 1) {
            buttons.push(buttonElements[i]);
        };
        
        /*
        root.querySelector(".fp-ui").onclick = function () {
            if (api.splash) {
         
              // allow one key retrieval; see php code
              // use flowplayer's xhr function
              //flowplayer.common.xhrGet("hlsengine.php", api.load);
            }
        };
        */
        api.on("ready", function (e, api, video) {
            var engineName = api.engine.engineName;
            
            // remove speed buttons if playback rate changes are not available
            if (api.engine.engineName == "flash" || !flowplayer.support.inlineVideo) {
              speedelement.parentNode.removeChild(speedelement);
            } 
    
            positionto = playtime_get('<?php echo $mode;?>');
            positionfrom = positionto;
            if(positionfrom < video.duration){
                api.seek(positionfrom);
            }

        }).on("speed", function (e, api, rate) {
            // mark the current speed button as active
            var i;

            speedelement.querySelector(".active").removeAttribute("class");
            for (i = 0; i < api.conf.speeds.length; i += 1) {
                if (api.conf.speeds[i] == rate) {
                    buttons[i].className = "active";
                    break;
                }
            }
        }).on("resume", function (e) {
            //플레이 버튼 클릭 시 호출
            positionfrom = positionto;
        }).on("pause", function (e, api) {
            //Pause 버튼 클릭 시 호출
            if (positionfrom < positionto) {
                playtime_update(positionfrom, positionto, 1, duration, '<?php echo $mode;?>');
            }
        }).on("beforeseek", function (e, api) {
            //seek 버튼 클릭 시 호출
            if (positionfrom < positionto) {
                playtime_update(positionfrom, positionto, 2, duration, '<?php echo $mode;?>');
            }
        }).on("seek", function (e, api) {
            //seek 버튼 클릭 후 seek되면 호출
            positionfrom = positionto;
        }).on("finish", function (e, api) {
            if (positionfrom < positionto) {
                playtime_update(positionfrom, positionto, 3, duration, '<?php echo $mode;?>');
            }
        }).on("progress", function (e, api) {
            //타임라인
            positionto = Math.ceil(api.video.time);
            duration = Math.ceil(api.video.duration);

            //console.log('from:'+positionfrom+'//to:'+positionto+'//dur:'+duration);

            if (positionto > oldposition) {
                // caption_scroll();
            }

            oldposition = positionto;

        }).on("cuepoint", function (e, api, cuepoint) {

            if (cuepoint.subtitle) {
                oldposition = parseInt(cuepoint.time);
                caption_scroll(oldposition);
            }

        });



        /* end global event listeners setup */

        // bind speed() call to click on buttons
        buttons.forEach(function (button) {

            button.onclick = function () {
                var i;

                if (button.classNme != "active") {
                    for (i = 0; i < api.conf.speeds.length; i += 1) {
                        if (buttons[i] === button) {
                            api.speed(api.conf.speeds[i]);
                            break;
                        }
                    }
                }
            };

        });

    });

    window.onload = function () {

        flowplayer("#hlsjsvod", {
            key: "<?php echo get_config('local_repository','flowplayer_key')?>",
            <?php echo $playerlogo;?>
            splash: true,
            autoplay: true,
            embed: false, // setup would need iframe embedding
            ratio: 5 / 12,    
            // manual HLS level selection for Drive videos
            hlsQualities: "drive",

            // manual VOD quality selection when hlsjs is not supported
            //defaultQuality: "260p",
            //qualities: ["160p", "260p", "530p", "800p"],
            
            speeds: [0.75,1,1.25,1.5],

            clip: {
<?php echo $subtitles; ?>
                sources: [
<?php echo $streamsource; ?>
                ]
            }

        });

    };
</script>

<div id="hlsjsvod" class="is-closeable video_area">
    <div class="buttons">
        <span>0.75x</span>
        <span class="active">1x</span>
        <span>1.25x</span>
        <span>1.5x</span>
    </div>
    <div class="fp-context-menu fp-menu">
      <strong>Custom context menu</strong>
      <a href="https://flowplayer.org/docs/">Documentation</a>
   </div>
</div>
