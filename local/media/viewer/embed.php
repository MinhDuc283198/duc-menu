<?php

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)).'/lib.php');
require_once(dirname(dirname(__FILE__)).'/playerlib.php');

$v = required_param('v', PARAM_RAW);
$decrypted = base64_decode($v);
list($id, $userid, $mode) = explode('||', $decrypted);

$context = context_system::instance();
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->set_url('/local/media/viewer/embed.php', array('v' => $v));

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->css('/local/media/flowplayer/skin/functional.css');
$PAGE->requires->css('/local/media/flowplayer/skin/quality-selector.css');
$PAGE->requires->css('/theme/oklasscompany/style/popup.css');
echo $OUTPUT->header();

//권한 체크 (To-do)
//사내 사외 여부 체크함.
local_media_inout_check($id);

//디튜브 동영상 데이터 가져오기
if(!$content = $DB->get_record('lmsmedia_contents', array('id'=>$id))){
    print_error(get_string('nocontent','local_media'));
    die();
}

$viewcount = $content->viewcount + 1;
$DB->set_field('lmsmedia_contents','viewcount',$viewcount,array('id'=>$content->id));

//디튜브 동영상 파일 가져오기 (파일은 API로 가져올 예정 todo)
$file = $DB->get_record('lmsmedia_files', array('mediaid'=>$id));


?>  

<!--플레이어 영역 시작-->
<div class="player_area">
<?php 
if($content->contype == 'video'){
    $brows = local_media_browser_check();                       
    if($brows->platform == 'Win7'){
        include_once('player_flash.php');
    }else{
       include_once('player_video.php');
    }
}else if($content->contype == 'youtube'){
    include_once('player_youtube.php');
}else if($content->contype == 'vimeo'){
    include_once('player_vimeo.php');
}else if($content->contype == 'url'){
    include_once('player_url.php');
}
?>
</div>
<!--플레이어 영역 끝-->

<?php

echo $OUTPUT->footer();
?>


