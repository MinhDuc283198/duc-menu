<?php
session_start();

if (isset($_SESSION['secret']) &&
    isset($_SESSION['timestamp']) && (time() - 7) < $_SESSION['timestamp']) {
  header('Content-Type: binary/octet-stream');
  header('Cache-Control: no-store');
  readfile('/home/tarzisius/hls-aes/drive-crypt2.key');
} else {
  header('HTTP/1.0 403 Forbidden');
}

session_write_close();

