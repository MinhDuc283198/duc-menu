<?php
define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require(dirname(__FILE__) . '/config.php');
require(dirname(__FILE__) . '/lib.php');

require_login();

$returnvalue = new stdClass();

$type = optional_param('type', 'video', PARAM_RAW);  

// 콘텐츠 등록 (오류방지를 위해 선등록함).
$content = new stdClass();
$content->title = 'empty data';
$content->intro = '';
$content->target = '';
$content->isnoticed = 0;
$content->gubun = 1; // 1: 디튜브, 2: 강의자료활용, 3:모두활용
$content->groupid = 0;
$content->sortorder = 0;
$content->category1 = '';
$content->category2 = '';
$content->category3 = '';
$content->categorytags = ''; // 검색용분류코드, # 붙은 택스트가 들어감
$content->keywords = '';
$content->isshared = 0; //타 과정 공유여부
$content->userid = $USER->id;
$content->inouttype = '';
$content->isused = 0;
$content->managerid = 0;
$content->expiredate = 0;
$content->timecreated = time();
$content->timemodified = 0;
$content->contype = $type;
$content->mediafile = '';

$content->id = $DB->insert_record('lmsmedia_contents', $content);

$newfileid = local_media_save_file($_FILES['file'], $type, '', $content->id);

$returnvalue->ok = 1;
$returnvalue->fileid = $newfileid;

echo json_encode($returnvalue);
die();
    
