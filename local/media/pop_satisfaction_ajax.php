<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');

global $DB, $USER;

$returnvalue = new stdClass();

if($USER){

    $id = required_param('id', PARAM_INT);
    $score = required_param('score', PARAM_INT);
    
    if(!$satis = $DB->get_record('lmsmedia_satisfaction', array('mediaid' => $id, 'userid' => $USER->id))){
        
        $usersql = "select u.*, c.attribute2 from {lmsdata_user} u "
        . "join {lmsdata_code} c on c.codevalue = u.surveygradecode and c.code = 'XXHR_GRADE_TITLE_KO' where u.userid = :userid";
        $userinfo = $DB->get_record_sql($usersql, array('userid'=>$USER->id));
        
        $satis = new stdClass();
        $satis->mediaid = $id;
        $satis->titlecode = $userinfo->attribute2;
        $satis->jobname =  $userinfo->jobname;
        $satis->userid = $USER->id;
        $satis->score = $score;
        $satis->timecreated = time();
        
        $satisid = $DB->insert_record('lmsmedia_satisfaction',$satis);
        
        $returnvalue->status = 'success';
        
    }

}

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);
