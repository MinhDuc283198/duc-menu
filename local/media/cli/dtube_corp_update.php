<?php

//디튜브 법인 업데이트(2017-06-14)

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');

cron_setup_user();

global $DB, $PAGE;

if (!is_siteadmin($USER)) {
    redirect($CFG->wwwroot);
}

$PAGE->set_pagelayout('popup');
echo $OUTPUT->header();

$filename = 'dtube_corp_update.xlsx';
$filepath = 'dtube_corp_update.xlsx';

$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
}

$maxRow = $objWorksheet->getHighestRow();

$count = array('empty'=>0,'insert'=>0,'exist'=>0);

for ($i = 2; $i <= $maxRow; $i++) {
    $mediaid = trim($objWorksheet->getCell('A' . $i)->getValue());   //콘텐츠 고유번호
    $corporations = trim($objWorksheet->getCell('B' . $i)->getValue()); //신규법인데이터
    
    $corps = explode(',',$corporations);
    
    if($existcontents = $DB->count_records('lmsmedia_contents', array('id'=>$mediaid))){   
        foreach($corps as $corp){
            $existcorpcount = $DB->count_records('lmsmedia_corps', array('mediaid'=>$mediaid, 'corporationcode'=>trim($corp)));
            if($existcorpcount == 0){
                $data = new stdClass();
                $data->mediaid = $mediaid;
                $data->corporationcode = trim($corp);
                $newdataid = $DB->insert_record('lmsmedia_corps', $data);
                echo $i . '행 : '.$mediaid.' -> ' . $corp  . '을 생성하였습니다.'."\n";
                $count['insert']++;
            }else{
                echo $i . '행 : '.$mediaid.' -> ' . $corp  . '이 이미 존재합니다.'."\n";
                $count['exist']++;
            }
        }
    }else{
        echo $i . '행 : '.$mediaid.' -> 콘텐츠가 존재하지 않습니다.'."\n";
        $count['empty']++;
    }
    
}

print_object($count);
echo 'compete!!';
//echo '<script type="text/javascript">window.scrollTo(0, document.body.scrollHeight);</script>';

echo $OUTPUT->footer();

