<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->libdir.'/clilib.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->libdir . '/excellib.class.php');

require_once $CFG->dirroot . "/lib/filelib.php";
require_once $CFG->dirroot . "/local/media/config.php";
require_once $CFG->dirroot . "/local/media/lib.php";
require_once $CFG->dirroot . "/local/lmsdata/lib.php";

// Emulate normal session - we use admin accoutn by default
cron_setup_user();

// 동영상 파일 위치
//$videodir = $CFG->local_media_storage_path.'/';
$videodir = '';

// 엑셀 파일
$filename = 'media.xlsx';
$filepath = $CFG->dirroot.'/local/media/cli/'.$filename;

$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);


$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
}

$maxRow = $objWorksheet->getHighestRow();

$timecurrent = time();

$xmlobjs = array();
$contentid = 1;
for ($i = 12; $i <= $maxRow; $i++) {
    $categorycode = trim($objWorksheet->getCell('Q' . $i)->getValue());
//    $category = $DB->get_record_sql("SELECT ca.id, cl.id AS langid, cl.codevalue, cl.name
//            FROM {lmsdata_categories} ca
//            JOIN {lmsdata_categories_lang} cl ON cl.code = ca.code 
//                                              AND cl.codevalue = ca.codevalue 
//                                              AND cl.corporationcode = ca.corporationcode
//            WHERE cl.codevalue = :codevalue
//              AND cl.langcode = :langcode
//              AND cl.corporationcode = :corporationcode",
//            array('codevalue'=>$categorycode,'langcode'=>'KO','corporationcode'=>'LGDKR'));
    
    
    $videofilename = trim($objWorksheet->getCell('V' . $i)->getValue());//.'.mp4';
    $videofileextension = '.mp4';
    
    $length = strlen($videofileextension);
    if(substr(strtolower($videofilename), -$length) !== $videofileextension) {
        $videofilename .= $videofileextension;
    }
    
    $videofilepath = $videodir.$videofilename;
    
    $deliverytype = 'Internal'; // A: 전체, I: 사내, E: 사외
    $useout = trim($objWorksheet->getCell('U' . $i)->getValue());
    if($useout == '1') {
        $deliverytype = 'All';
    }
    
    $xmlobj = new stdClass();
    $xmlobj->ProviderNo = 1;
    $xmlobj->ServiceNo = 1;
    $xmlobj->ContentId = $contentid;
    $xmlobj->CategoryNo = $categorycode;//$category->id;
    $xmlobj->CategoryName = trim($objWorksheet->getCell('N' . $i)->getValue());
    $xmlobj->Date = userdate($timecurrent, '%Y-%m-%d');
    $xmlobj->FileName = $videofilename;
    $xmlobj->FilePath = $videofilepath;
    $xmlobj->FileSize = 0;
    $xmlobj->Title = trim($objWorksheet->getCell('A' . $i)->getValue());
    $xmlobj->Synopsis = '';
    $xmlobj->Memo = '';
    $xmlobj->MediaGuid = '';
    $xmlobj->DeliveryType = $deliverytype;
    $xmlobj->ReContentAcqYN = 'N';
    $xmlobj->ExpirationDate = '2999-12-31';
    
    $xmlobjs[] = $xmlobj;
    
    $contentid += 1;
}

$date = date('Y-m-d', time());
$filename = 'media_' . $date . '.xlsx';

$workbook = new MoodleExcelWorkbook('-');
$workbook->send($filename);
$worksheet = array();

$worksheet[0] = $workbook->add_worksheet('');

$col = 0;
$worksheet[0]->write(0, $col++, 'ProviderNo');
$worksheet[0]->write(0, $col++, 'ServiceNo');
$worksheet[0]->write(0, $col++, 'ContentId');
$worksheet[0]->write(0, $col++, 'CategoryNo');
$worksheet[0]->write(0, $col++, 'CategoryName');
$worksheet[0]->write(0, $col++, 'Date');
$worksheet[0]->write(0, $col++, 'FileName');
$worksheet[0]->write(0, $col++, 'FilePath');
$worksheet[0]->write(0, $col++, 'FileSize');
$worksheet[0]->write(0, $col++, 'Title');
$worksheet[0]->write(0, $col++, 'Synopsis');
$worksheet[0]->write(0, $col++, 'Memo');
$worksheet[0]->write(0, $col++, 'MediaGuid');
$worksheet[0]->write(0, $col++, 'DeliveryType');
$worksheet[0]->write(0, $col++, 'ReContentAcqYN');
$worksheet[0]->write(0, $col++, 'ExpirationDate');

$row = 1;
foreach($xmlobjs as $xmlobj) {
    $col = 0;
    
    $worksheet[0]->write($row, $col++, $xmlobj->ProviderNo);
    $worksheet[0]->write($row, $col++, $xmlobj->ServiceNo);
    $worksheet[0]->write($row, $col++, $xmlobj->ContentId);
    $worksheet[0]->write($row, $col++, $xmlobj->CategoryNo);
    $worksheet[0]->write($row, $col++, $xmlobj->CategoryName);
    $worksheet[0]->write($row, $col++, $xmlobj->Date);
    $worksheet[0]->write($row, $col++, $xmlobj->FileName);
    $worksheet[0]->write($row, $col++, $xmlobj->FilePath);
    $worksheet[0]->write($row, $col++, $xmlobj->FileSize);
    $worksheet[0]->write($row, $col++, $xmlobj->Title);
    $worksheet[0]->write($row, $col++, $xmlobj->Synopsis);
    $worksheet[0]->write($row, $col++, $xmlobj->Memo);
    $worksheet[0]->write($row, $col++, $xmlobj->MediaGuid);
    $worksheet[0]->write($row, $col++, $xmlobj->DeliveryType);
    $worksheet[0]->write($row, $col++, $xmlobj->ReContentAcqYN);
    $worksheet[0]->write($row, $col++, $xmlobj->ExpirationDate);
    
    $row++;
}

$workbook->close_tofile();