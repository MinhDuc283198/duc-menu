<?php

define('CLI_SCRIPT', true);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';
require_once($CFG->libdir . '/clilib.php');

cron_setup_user();

global $DB;

$termmonth = strtotime('-6 month', time());
$termyear1 = strtotime('-12 month', time());
$termyear2 = strtotime('-24 month', time());
//NPS 지수 과정, 차수별 계산하여 테이블에 저장함(검색용)
$sql = "select s.mediaid, avg(s.score) as satisscore,((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count "
        . " from {lmsmedia_satisfaction} s "
        . "group by s.mediaid ";
$satis = $DB->get_records_sql($sql, array());

foreach ($satis as $sati) {

    if (!$data = $DB->get_record('lmsdata_nps', array('type' => '2', 'curriculumid' => $sati->mediaid))) {
        $data = new stdClass();
        $data->type = '2';
        $data->curriculumid = $sati->mediaid;
        $data->satisscore = $sati->satisscore;
        $data->npsscore = $sati->nps;
        $data->responses = $sati->count;
        $data->timemodified = time();
        $DB->insert_record('lmsdata_nps', $data);
    } else {
        if ($data->npsscore != $sati->nps || $data->satisscore != $sati->satisscore || $data->responses != $sati->responses) {
            $data->satisscore = $sati->satisscore;
            $data->npsscore = $sati->nps;
            $data->responses = $sati->count;
            $data->timemodified = time();
            $DB->update_record('lmsdata_nps', $data);
        }
    }
}

//NPS 지수 직급별 계산하여 테이블에 저장함(검색용)
$termmonth = strtotime('-6 month', time());
$termyear1 = strtotime('-12 month', time());
$termyear2 = strtotime('-24 month', time());
$sql = "select s.titlecode, s.mediaid, ((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count, 6 as term "
        . " from {lmsmedia_satisfaction} s "
        . "where s.timecreated > $termmonth "
        . "group by s.titlecode, s.mediaid "
        . "union all select s.titlecode, s.mediaid, ((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count, 12 as term "
        . " from {lmsmedia_satisfaction} s "
        . "where s.timecreated > $termyear1 "
        . "group by s.titlecode, s.mediaid "
        . " union all select s.titlecode, s.mediaid, ((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count, 24 as term "
        . " from {lmsmedia_satisfaction} s "
        . "where s.timecreated > $termyear2 "
        . "group by s.titlecode, s.mediaid ";

$satis = $DB->get_recordset_sql($sql);

foreach ($satis as $sati) {

    if (!$data = $DB->get_record('lmsdata_nps_titlecode', array('type' => '2', 'curriculumid' => $sati->mediaid, 'titlecode' => $sati->titlecode, 'term' => $sati->term))) {
        $data = new stdClass();
        $data->type = '2';
        $data->curriculumid = $sati->mediaid;
        $data->titlecode = $sati->titlecode;
        $data->npsscore = $sati->nps;
        $data->responses = $sati->count;
        $data->term = $sati->term;
        $data->timemodified = time();
        $DB->insert_record('lmsdata_nps_titlecode', $data);
    } else {
        if ($data->npsscore != $sati->nps || $data->responses != $sati->count) {
            $data->npsscore = $sati->nps;
            $data->responses = $sati->count;
            $data->term = $sati->term;
            $data->timemodified = time();
            $DB->update_record('lmsdata_nps_titlecode', $data);
        }
    }
}

//NPS 지수 직무별 계산하여 테이블에 저장함(검색용)
$sql = "select s.jobname, s.mediaid, ((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count, 6 as term  "
        . " from {lmsmedia_satisfaction} s "
        . "where s.timecreated > $termmonth "
        . "group by s.jobname, s.mediaid "
        . " union all select s.jobname, s.mediaid, ((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count, 12 as term  "
        . " from {lmsmedia_satisfaction} s "
        . "where s.timecreated > $termyear1 "
        . "group by s.jobname, s.mediaid "
        . " union all select s.jobname, s.mediaid, ((sum(CASE WHEN s.score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN s.score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps, count(*) as count, 24 as term  "
        . " from {lmsmedia_satisfaction} s "
        . "where s.timecreated > $termyear2 "
        . "group by s.jobname, s.mediaid ";

$satis = $DB->get_recordset_sql($sql);

foreach ($satis as $sati) {

    if (!$data = $DB->get_record('lmsdata_nps_jobname', array('type' => '2', 'curriculumid' => $sati->mediaid, 'jobname' => $sati->jobname, 'term' => $sati->term))) {
        $data = new stdClass();
        $data->type = '2';
        $data->curriculumid = $sati->mediaid;
        $data->jobname = $sati->jobname;
        $data->npsscore = $sati->nps;
        $data->responses = $sati->count;
        $data->term = $sati->term;
        $data->timemodified = time();
        $DB->insert_record('lmsdata_nps_jobname', $data);
    } else {
        if ($data->npsscore != $sati->nps || $data->responses != $sati->count) {
            $data->npsscore = $sati->nps;
            $data->responses = $sati->count;
            $data->term = $sati->term;
            $data->timemodified = time();
            $DB->update_record('lmsdata_nps_jobname', $data);
        }
    }
}


