<?php

define('CLI_SCRIPT', true);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php';

$CFG->debug = (E_ALL | E_STRICT);
$CFG->debugdisplay = 1;
error_reporting($CFG->debug);
ini_set('display_errors', '1');
ini_set('log_errors', '1');

require_once($CFG->libdir . '/clilib.php');
require_once('../lib.php');

cron_setup_user();

global $DB;

//배포정보를 가지고 크론 업데이트
$contents = $DB->get_records('lmsmedia_contents', array('isenabled'=>0,'contype'=>'video'));

foreach($contents as $content){
    $file = $DB->get_record('lmsmedia_files', array('mediaid'=>$content->id));
    $delivinfo = local_media_beecast_get_delivery_info($content->id);
    
    if($delivinfo->ContentDeliveryInfoData){
        foreach($delivinfo->ContentDeliveryInfoData as $deliv){
            if($content->inouttype == 'I' || $content->inouttype == 'A'){
                if($deliv->DeliveryStatusCode == '016' && $deliv->DeliveryType == '001'){
                    $DB->set_field('lmsmedia_files', 'duration', $deliv->RunningTime, array('mediaid' => $content->id));
                    $DB->set_field('lmsmedia_contents', 'isenabled', 1, array('id' => $content->id));
                }
            }else{
                if($deliv->DeliveryStatusCode == '009' && $deliv->DeliveryType == '002'){
                    $DB->set_field('lmsmedia_files', 'duration', $deliv->RunningTime, array('mediaid' => $content->id));
                    $DB->set_field('lmsmedia_contents', 'isenabled', 1, array('id' => $content->id));
                }
            }
        }
    }
}

