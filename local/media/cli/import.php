<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once($CFG->libdir.'/clilib.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->libdir . '/excellib.class.php');

require_once $CFG->dirroot . "/lib/filelib.php";
require_once $CFG->dirroot . "/local/media/config.php";
require_once $CFG->dirroot . "/local/media/lib.php";
require_once $CFG->dirroot . "/local/lmsdata/lib.php";

$CFG->debug = (E_ALL | E_STRICT);
$CFG->debugdisplay = 1;
error_reporting($CFG->debug);
ini_set('display_errors', '1');
ini_set('log_errors', '1');

// Emulate normal session - we use admin accoutn by default
cron_setup_user();

// 동영상 파일 위치
$videodir = $CFG->local_media_storage_path.'/hunet/';

// 엑셀 파일
$filename = 'media_20170403_3.xlsx';
$filepath = $CFG->dirroot.'/local/media/cli/'.$filename;

$objReader = PHPExcel_IOFactory::createReaderForFile($filepath);
$objReader->setReadDataOnly(true);
$objExcel = $objReader->load($filepath);

// 콘텐츠 등록 시작

$objExcel->setActiveSheetIndex(0);
$objWorksheet = $objExcel->getActiveSheet();
$rowIterator = $objWorksheet->getRowIterator();

foreach ($rowIterator as $row) { // 모든 행에 대해서
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
}

$maxRow = $objWorksheet->getHighestRow();

$timecurrent = time();

$counts = array (
    'total'=>0,
    'success'=>0,
    'error'=>0);


for ($i = 2; $i <= $maxRow; $i++) {
    echo "ROW $i\n";
    $counts['total'] += 1;
    
    $videofilename = trim($objWorksheet->getCell('V' . $i)->getValue());
    $videofilepath = $videodir.$videofilename;
    /*
    if(!file_exists($videofilepath)) {
        $counts['error'] += 1;
        echo "Error: File does not exists! ($i, $videofilepath)\n";
        //continue;
    }
    */
    $groupid = 0;
    $serisename = trim($objWorksheet->getCell('I' . $i)->getValue());
    if(!empty($serisename)) {
        // $content->groupid 를 찾기 위해
        $groupid = $DB->get_field('lmsmedia_groups', 'id', array('name'=>$serisename));
        if(!$groupid) {
            $group = new stdClass();
            $group->name = $serisename;
            $group->userid = $USER->id;
            $group->timecreated = $timecurrent;
            $group->timemodified = $timecurrent;
            $groupid = $DB->insert_record('lmsmedia_groups', $group);
        }
    }
    
    $inouttype = 'I'; // A: 전체, I: 사내, E: 사외
    $useout = trim($objWorksheet->getCell('U' . $i)->getValue());
    if($useout == '1') {
        $inouttype = 'A';
    }
    
    // 관리용 분류코드
    $categoriesstr = trim($objWorksheet->getCell('K' . $i)->getValue());
    $categories = explode(',', $categoriesstr);
    
    // 검색용 분류코드 categorytags
    $categorytagsstr = trim($objWorksheet->getCell('R' . $i)->getValue());
    $categorytags = explode(',', $categorytagsstr);
    foreach($categorytags as $key=>$value) {
        $categorytags[$key] = '#'.$value;
    }
    
    // 콘텐츠 등록
    $content = new stdClass();
    $content->id = 0;
    $content->conid = (int)(trim($objWorksheet->getCell('W' . $i)->getValue()));
    $content->title = trim($objWorksheet->getCell('A' . $i)->getValue());
    $content->intro = trim($objWorksheet->getCell('B' . $i)->getValue());
    $content->target = trim($objWorksheet->getCell('C' . $i)->getValue());
    $content->isnoticed = trim($objWorksheet->getCell('G' . $i)->getValue());
    $content->gubun = trim($objWorksheet->getCell('H' . $i)->getValue()); // 1: 디튜브, 2: 강의자료활용, 3:모두활용
    $content->groupid = $groupid;
    $content->category1 = trim($objWorksheet->getCell('O' . $i)->getValue());
    $content->category2 = trim($objWorksheet->getCell('P' . $i)->getValue());
    $content->category3 = trim($objWorksheet->getCell('Q' . $i)->getValue());
    $content->categorytags = implode(' ', $categorytags); // 검색용분류코드, # 붙은 택스트가 들어감
    $content->keywords = trim($objWorksheet->getCell('T' . $i)->getValue());
    $content->sortorder = (int)(trim($objWorksheet->getCell('J' . $i)->getValue()));
    $content->userid = $USER->id;
    $content->inouttype = $inouttype;
    $content->isused = 1;
    $content->managerid = 0;
    $content->timecreated = $timecurrent;
    $content->timemodified = $timecurrent;
    $content->contype = 'video';
    $content->expiredate = strtotime('2037-12-31 23:59:59');
    $content->mediafile = '';
    
    //print_object($content);
    //die();
    
    $newid = $DB->insert_record('lmsmedia_contents', $content);
    //$DB->set_field('lmsmedia_contents', 'id', $content->id, array('id' => $newid));
    $content->id = $content->conid;
    // 이력 등록
    $revision = new stdClass();
    $revision->id = 0;
    $revision->mediaid = $content->id;
    $revision->title = $content->title;
    $revision->contents = serialize($content);
    $revision->eventtype = 'I';
    $revision->userid = $USER->id;
    $revision->timecreated = $timecurrent;
    //print_r($revision);
    $revision->id = $DB->insert_record('lmsmedia_revision', $revision);
    
    // 콘텐츠의 revisionid 업데이트
    $content->revisionid = $revision->id;
    $content->id = $newid;
    $DB->update_record('lmsmedia_contents', $content);
    $content->id = $content->conid;
    
    $filesize = 0;
    if(file_exists($videofilepath)) {
        $filesize = filesize($videofilepath);
    }
    // 파일 등록
    $mediafile = new stdClass();
    $mediafile->mediaid = $content->id;
    $mediafile->servertype = $content->inouttype;
    $mediafile->filepath = $videofilepath;
    $mediafile->filename = $videofilename;
    $mediafile->filesize = $filesize;
    $mediafile->duration = 0;
    $mediafile->bitrate = '';
    //print_r($mediafile);
    $DB->insert_record('lmsmedia_files', $mediafile);
    
    // 공유범위 입력
    switch ($content->target) {
        case '1' :
            $targets = trim($objWorksheet->getCell('D' . $i)->getValue());
            if(!empty($targets)) {
                $corps = explode(',', $targets);
                foreach ($corps as $corp) {
                    $DB->insert_record('lmsmedia_corps', array('mediaid' => $content->id, 'corporationcode' => trim($corp)));
                }
            }
            break;
        case '2' :
            $targetsuser = trim($objWorksheet->getCell('E' . $i)->getValue()); // 개인
            if(!empty($targetsuser)) {
                $usernames = explode(',', $targetsuser);
                foreach ($usernames as $username) {
                    $username = core_text::strtolower(trim($usernew->$username));
                    if($user = $DB->get_record('user', array('username'=>$username))) {
                        $DB->insert_record('lmsmedia_users', array('mediaid' => $content->id, 'gubun' => 2, 'userid' => $user->id));
                    }
                }
            }
            $targetsgroup = trim($objWorksheet->getCell('F' . $i)->getValue()); // 그룹
            if(!empty($targetsgroup)) {
                $orgcodes = explode(',', $targetsgroup);
                foreach ($orgcodes as $orgcode) {
                    $DB->insert_record('lmsmedia_users', array('mediaid' => $content->id, 'gubun' => 1, 'organizationcode' => trim($orgcode)));
                }
            }
            break;
        case '3' :
        default :
            break;
    }
    
    // 분류태그 코드값 입력, 검색용분류태그코드, 
    $tagcodestr = trim($objWorksheet->getCell('S' . $i)->getValue());
    if (!empty($tagcodestr)) {
        $coursetags = array();
        
        $tagcodes = explode(",", $tagcodestr);
        foreach ($tagcodes as $tagcode) {
            $c_tags = new stdClass();
            $c_tags->type = 2;
            $c_tags->curriculumid = $content->id;
            $c_tags->tagcode = $tagcode;

            array_push($coursetags, $c_tags);
        }

        if (count($coursetags) > 0) {
            //print_r($coursetags);
            $DB->insert_records('lmsdata_course_tag', $coursetags);
        }
    }
    
    // 썸네일 등록
    $thumbdata = new stdClass();
    $thumbdata->mediaid = $content->id;
    $thumbdata->filepath = $CFG->local_media_file_path.'/Thumbnail/'.$content->id.'_01.jpg';
    $thumbdata->filename = 'defaut01.jpg';
    $thumbdata->isused = 1;
    $DB->insert_record('lmsmedia_thumbs', $thumbdata);
    
    $thumbdata->filepath = $CFG->local_media_file_path.'/Thumbnail/'.$content->id.'_02.jpg';
    $thumbdata->filename = 'defaut02.jpg';
    $thumbdata->isused = 0;
    $DB->insert_record('lmsmedia_thumbs', $thumbdata);

    // XML 생성
    // $xmldir = $CFG->local_media_xml_path;
    //local_media_beecast_create_xml($content->id);
    
    $counts['success'] += 1;
}

print_r($counts);