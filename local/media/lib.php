<?php
require_once($CFG->libdir . '/phpexcel/PHPExcel.php');
require_once($CFG->libdir . '/phpexcel/PHPExcel/IOFactory.php');
require_once($CFG->libdir . '/excellib.class.php');

/**
 * 법인별 설정 리스트 조회
 */
function local_media_get_corps($mediaid) {
    global $DB;
    $sql = "select c.*, mc.id as corpid from {lmsdata_corporation} c "
            . "left join {lmsmedia_corps} mc on mc.corporationcode = c.code and mc.mediaid = :mediaid order by c.sortorder asc";
    $corpsList = $DB->get_records_sql($sql, array('mediaid'=>$mediaid));
    return $corpsList;
}

/**
 * 시리즈 Group 리스트 조회
 */
function local_media_get_group($name) {
    global $DB;

    $query = 'SELECT * FROM {lmsmedia_groups}';
    $query_param = null;

    if (!(empty($name))) {
        $query .= ' WHERE name LIKE CONCAT(\'%\', :name, \'%\') ';
        $query_param = array('name' => $name);
    }

    $groupList = $DB->get_records_sql($query, $query_param);
    return $groupList;
}

/**
 * 시리즈 Group 입력
 */
function local_media_add_group($name) {
    global $DB, $USER;

    if (!empty($name)) {
        $groups = new stdClass();
        $groups->name = $name;
        $groups->userid = $USER->id;
        $groups->timecreated = time();
        $groups->timemodified = time();

        $groupInDB = $DB->get_records('lmsmedia_groups', array('name' => $name));
        if (count($groupInDB) > 0) {
            return array('result' => 'D');
        }

        $newid = $DB->insert_record('lmsmedia_groups', $groups);
        return array('result' => 'Y','id' => $newid);
    }
    return array('result' => 'P');
}

/**
 * 시리즈 Group 수정
 */
function local_media_update_group($id, $name) {
    global $DB;

    if (!empty($id) && !empty($name)) {
        $groups = new stdClass();
        $groups->id = $id;
        $groups->name = $name;

        $DB->update_record('lmsmedia_groups', $groups);
        return array('result' => 'Y');
    }
    return array('result' => 'P');
}

/**
 * 시리즈 Group 수정
 */
function local_media_delete_group($id) {
    global $DB;

    if (!empty($id)) {
        $DB->delete_records('lmsmedia_groups', array('id' => $id));
        return array('result' => 'Y');
    }
    return array('result' => 'P');
}

/**
 * 관리용 분류체계 조회
 */
function local_media_get_categories($code) {
    global $DB, $USER, $CFG;

    $query .= 'SELECT lcl.codevalue, lcl.name ';
    $query .= 'FROM {lmsdata_categories} lc ';
    $query .= 'INNER JOIN {lmsdata_categories_lang} lcl ON lc.code = lcl.code AND lc.codevalue = lcl.codevalue AND lc.corporationcode = lcl.corporationcode ';
    $query .= 'WHERE lc.code = :code ';
    $query .= 'AND lc.corporationcode = (SELECT corporationcode FROM {lmsdata_user} WHERE userid = :userid) ';
    $query .= 'AND lcl.langcode = :langcode ';
    $query .= 'GROUP BY lcl.codevalue ';
    $query .= 'ORDER BY lcl.codevalue ';
    
    $currentlang = (current_language() == 'zh_cn')? 'ZH':strtoupper(current_language());

    $params = array('code' => $code, 'userid' => $USER->id, 'langcode' => $currentlang);

    $result_list = $DB->get_records_sql($query, $params);
    return $result_list;
}

/**
 * 사용자 리스트 조회 (업로드폼 > 대상자지정 > User권한)
 */
function local_media_get_user($username, $orgname, $mediaid, $page) {
    global $DB;
    
    if($username || $orgname){
    $search_count = 'select count(*) ';

    $search_query = 'SELECT u.id, u.userid, u.userno, u.ssousername, u.usertypecode, u.organizationcode, u.organizationname, u.titlecode, u.titlename, u.positioncode, u.positionname, u.jobname, u.corporationcode, ';
    $search_query .= 'o.organizationid, o.code as orgcode, o.name as orgname, u2.username as employno, concat(u2.firstname, u2.lastname) as userfullname, ';
    $search_query .= 'c.code as corcode, c.name as corname, c.countrycode, c.langcode, c.lgdname, mu.id as muid ';
    $search_from .= 'FROM {lmsdata_user} u join {user} u2 on u2.id = u.userid ';
    $search_from .= 'LEFT JOIN {lmsdata_organization} o ON u.organizationcode = o.code ';
    $search_from .= 'LEFT JOIN {lmsdata_corporation} c ON u.corporationcode = c.code ';
    $search_from .= 'LEFT JOIN {lmsmedia_users} mu ON mu.userid = u.userid and mu.mediaid = :mediaid ';

    $query_param = array('usertypecode' => 'C', 'mediaid' => $mediaid);
    $where_clause = 'WHERE u.usertypecode = :usertypecode ';
    if (!empty($username)) {
        $like_name = $DB->sql_like('LOWER(CONCAT(u2.firstname, u2.lastname))', ':fullname');
        $query_param['fullname'] = '%' . strtolower($username) . '%';
        $where_clause .= ' and '.$like_name;
    }
    if (!empty($orgname)) {
        $where_clause .= 'AND u.organizationname LIKE LOWER(CONCAT(\'%\', :organizationname, \'%\')) ';
        $query_param['organizationname'] = strtolower($orgname);
    }
    
    $totalcount = $DB->count_records_sql($search_count . $search_from . $where_clause, $query_param);
    
    if($username || $orgname){
        $offset = 0;
        $perpage = $totalcount;
    }else{
        $offset = 0;
        $perpage = 10;
    }
   
    $result_list = $DB->get_records_sql($search_query . $search_from . $where_clause, $query_param, $offset, $perpage);
    }
    return $result_list;
}

/**
 * 조직 리스트 조회 (업로드폼 > 대상자지정 > Group권한)
 */
function local_media_get_organization($orgname, $mediaid, $page) {
    global $DB;
    if($orgname){
     $search_count = 'select count(*) ';
    
    $search_query = 'SELECT o.id, o.organizationid, o.code, o.name, u.id as orgid ';
    $search_from .= 'FROM {lmsdata_organization} o left join {lmsmedia_users} u on u.organizationcode = o.code and u.mediaid = :mediaid ';

    $where_clause = 'where o.islastorganization = :isorganization ';
    $query_param = array('mediaid'=>$mediaid, 'isorganization'=>'Y');
    if (!empty($orgname)) {
        $where_clause .= 'and o.name LIKE LOWER(CONCAT(\'%\', :name, \'%\')) ';
        $query_param['name'] = strtolower($orgname);
    }
    
    $totalcount = $DB->count_records_sql($search_count . $search_from . $where_clause, $query_param);
    
    if($orgname){
        $offset = 0;
        $perpage = $totalcount;
    }else{
        $offset = 0;
        $perpage = 10;
    }

    $result_list = $DB->get_records_sql($search_query . $search_from . $where_clause, $query_param, $offset, $perpage);
    }
    return $result_list;
}

/**
 * 컨텐츠 리스트 조회
 */
function local_media_get_contents($page, $title, $sort, $excel = 0, $perpage = 10) {
    global $DB, $USER;
    
    $auth = local_media_userauth();

    $query_param = array();
    $conditions = array();
    $conditions2 = array();
    if(!$excel){
        $main_query = 'SELECT c.*, IFNULL(f.duration, 0) as duration, u.username, u.firstname, u.lastname, lu.organizationname, t.filepath as thumbpath,'
            . '(select ((sum(CASE WHEN score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN score <= 6 THEN 1 ELSE 0 END))/count(*)/2+0.5)*100 '
            . 'from {lmsmedia_satisfaction} where mediaid = c.id) as nps  ';
    }else{
        $main_query = 'SELECT c.*, IFNULL(f.duration, 0) as duration, u.username, u.firstname, u.lastname, u.email,'
                . 'lu.organizationname, t.filepath as thumbpath, track.avg_playtime, corps.corps_codes, users.user_ids, g.name as groupname,'
            . '(select (sum(CASE WHEN score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN score <= 6 THEN 1 ELSE 0 END))/count(*) '
            . 'from {lmsmedia_satisfaction} where mediaid = c.id) as nps,'
            . '(select count(*) from {lmsmedia_satisfaction} where mediaid = c.id) as npscount ';
    }
    $main_query .= 'FROM {lmsmedia_contents} c ';
    $main_query .= 'JOIN {user} u ON c.userid = u.id ';
    $main_query .= 'LEFT JOIN {lmsdata_user} lu ON lu.userid = u.id ';
    $main_query .= 'LEFT JOIN {lmsmedia_files} f ON c.id = f.mediaid ';
    $main_query .= 'LEFT JOIN {lmsmedia_thumbs} t ON c.id = t.mediaid AND t.isused = 1 '; 
    if($excel == 1){
        $main_query .= 'LEFT JOIN {lmsmedia_groups} g ON c.groupid = g.id '; 
        $main_query .= 'LEFT JOIN ( SELECT mediaid, GROUP_CONCAT(corporationcode) corps_codes FROM {lmsmedia_corps} GROUP BY mediaid ) corps ON c.id = corps.mediaid ';
        $main_query .= 'LEFT JOIN ( SELECT mediaid, GROUP_CONCAT(userid) user_ids FROM {lmsmedia_users} GROUP BY mediaid ) users ON c.id = users.mediaid ';
        $main_query .= 'LEFT JOIN ( SELECT mediaid, AVG(playtime) avg_playtime FROM {lmsmedia_track} GROUP BY mediaid ) track ON c.id = track.mediaid ';
    }

    $where_clause = '';
    
    /*
    if($auth->auth == 'Local Master'){
        $main_query .= 'JOIN {lmsmedia_corps} co ON co.corporationcode = :corporationcode ';
        $query_param['corporationcode'] = $auth->corp;
    }
    
    if($auth->auth == 'HRD Training Manager'){
        $conditions[] = 'u.id = :userid';
        $query_param['userid'] = $USER->id;
    }
    * 
    */
    
    if (!empty($title)) {
        $conditions2[] = $DB->sql_like('c.title',':title1',false);
        $conditions2[] = $DB->sql_like('c.keywords',':title2',false);
        $conditions2[] = $DB->sql_like('c.categorytags',':title3',false);
        $query_param['title1'] = $query_param['title2'] = $query_param['title3'] = '%' . $title . '%';
        $conditions[] = '(' . implode(' or ',$conditions2) . ')';
    }
    
    if($conditions){
        $where_clause = ' WHERE '.implode(' and ',$conditions);
    }

    $sort_clause = '';
    $query_param['timestartnotice'] = $query_param['timeendnotice'] = time();
    
    switch ($sort) {
        case 'id':
            $sort_clause = 'ORDER BY c.timecreated DESC, c.id DESC';
            break;
        case 'title':
            $sort_clause = 'ORDER BY c.title ASC, c.timecreated DESC, c.id DESC';
            break;
        case 'duration':
            $sort_clause = 'ORDER BY f.duration DESC, c.timecreated DESC, c.id DESC';
            break;
        case 'viewcount':
            $sort_clause = 'ORDER BY c.viewcount DESC, c.timecreated DESC, c.id DESC';
            break;
        case 'likecount':
            $sort_clause = 'ORDER BY nps DESC, c.timecreated DESC, c.id DESC';
            break;
        default:
            break;
    }

    $count_query = 'SELECT COUNT(*) FROM {lmsmedia_contents} c INNER JOIN {user} u ON c.userid = u.id ' . $where_clause;
    $result_count = $DB->count_records_sql($count_query, $query_param);
    
    if($excel == 1){
        $result_list = $DB->get_records_sql($main_query . $where_clause . $sort_clause, $query_param);
    }else{     
        $result_list = $DB->get_records_sql($main_query . $where_clause . $sort_clause, $query_param, ($page - 1) * $perpage, $perpage);
    }
    return array('count' => $result_count, 'list' => $result_list);
}

/**
 * 컨텐츠 리스트 조회
 */
function local_media_get_contents_excel($page, $title, $sort) {
    global $DB, $USER;

    $query_param = array();
    
    $main_query = 'SELECT c.*, IFNULL(f.duration, 0) as duration, f.filepath as media_filepath, f.filename as media_filename, u.username, u.firstname, u.lastname, u.email,	ud.organizationname, IFNULL(CONCAT(t.filepath, \'/\', t.filename), \'\') AS thumbnail_image, corps.corps_codes, users.user_ids, IFNULL(track.avg_playtime, 0) AS avg_playtime ';
    $main_query .= 'FROM {lmsmedia_contents} c ';
    $main_query .= 'INNER JOIN {user} u ON c.userid = u.id ';
    $main_query .= 'LEFT JOIN {lmsdata_user} ud ON u.id = ud.userid ';
    $main_query .= 'LEFT JOIN {lmsmedia_files} f ON c.id = f.mediaid ';
    $main_query .= 'LEFT JOIN {lmsmedia_thumbs} t ON c.id = t.mediaid AND t.isused = 1 ';
    $main_query .= 'LEFT JOIN ( SELECT mediaid, GROUP_CONCAT(corporationcode) corps_codes FROM {lmsmedia_corps} GROUP BY mediaid ) corps ON c.id = corps.mediaid ';
    $main_query .= 'LEFT JOIN ( SELECT mediaid, GROUP_CONCAT(userid) user_ids FROM {lmsmedia_users} GROUP BY mediaid ) users ON c.id = users.mediaid ';
    $main_query .= 'LEFT JOIN ( SELECT mediaid, AVG(playtime) avg_playtime FROM {lmsmedia_track} GROUP BY mediaid ) track ON c.id = track.mediaid ';


    $where_clause = '';
    if (!empty($title)) {
        $where_clause = 'WHERE c.title LIKE CONCAT(\'%\', :title, \'%\') ';
        $query_param['title'] = $title;
    }

    $sort_clause = '';
    switch ($sort) {
        case 'id':
            $sort_clause = 'ORDER BY c.id DESC';
            break;
        case 'title':
            $sort_clause = 'ORDER BY c.title ASC';
            break;
        case 'viewcount':
            $sort_clause = 'ORDER BY c.viewcount DESC';
            break;
        default:
            break;
    }

    $count_query = 'SELECT COUNT(1) FROM {lmsmedia_contents} c INNER JOIN {user} u ON c.userid = u.id ' . $where_clause;
    $result_count = $DB->count_records_sql($count_query, $query_param);

    $result_list = $DB->get_records_sql($main_query . $where_clause . $sort_clause, $query_param);
    return array('count' => $result_count, 'list' => $result_list);
}

/**
 * 컨텐츠 정보 입력
 */
function local_media_add_instance($data, $context) {
    global $DB, $USER, $CFG;

    // insert into lmsmedia_contents
    $content = new stdClass();
    $content->title = $data->title;
    $content->target = $data->target;
    $content->intro = $data->intro;
    $content->isshared = isset($data->isshared) ? $data->isshared : 0;
    $content->isnoticed = isset($data->notice) ? $data->notice : 0;
    if (isset($data->notice)) {
        $content->timestartnotice = $data->timestartnotice;
        $content->timeendnotice = $data->timeendnotice;
    }
    $content->gubun = $data->gubun;
    $content->groupid = $data->groupid;
    $content->category1 = $data->category1;
    $content->category2 = $data->category2;
    $content->category3 = $data->category3;
    $content->categorytags = $data->categorytags;
    $content->keywords = $data->keywords;
    $content->userid = $USER->id;
    $content->inouttype = $data->inouttype;
    $content->isused = $data->isused;
    $content->expiredate = $data->expiredate;
    
    //Set group sortorder
    if($content->groupid){
        $contentcount = $DB->count_records('lmsmedia_contents',array('groupid'=>$content->groupid));
        $content->sortorder = $contentcount+1;
    }

    // Set managerid
    $query_managerid = 'SELECT managerid FROM moodle.tb_lmsdata_categories WHERE code = \'LGD_ACT_CATEGORY_VS\' AND corporationcode = (SELECT corporationcode FROM tb_lmsdata_user WHERE userid = :userid) AND codevalue = :category2 ORDER BY managerid DESC';
    $param_managerid = array('userid' => $USER->id, 'category2' => $data->category2);
    $managerid_info = $DB->get_records_sql($query_managerid, $param_managerid, 0, 1);
    foreach ($managerid_info as $key => $m_item) {
        $content->managerid = $m_item->managerid;
    }

    $content->timecreated = time();
    $content->timemodified = time();

    $content->contype = $data->upload_type;
    $content->mediafile = $data->mediafile;
    $content->itemid = $content->intro['itemid'];
    
    if($content->contype == 'video'){
        $content->isenabled = 0;
    }else{
        $content->isenabled = 1;
    }
    
    $last_insert_id = $DB->insert_record('lmsmedia_contents', $content);
    
    //intro 정보
    $content->intro = file_save_draft_area_files($content->itemid, $context->id, 'local_media', 'intro', $last_insert_id, local_media_editor_options($context, $last_insert_id), $content->intro['text']);
    $DB->set_field('lmsmedia_contents', 'intro', $content->intro, array('id' => $last_insert_id));

    //이력 등록하기
    $revision = new stdClass();
    $revision->mediaid = $last_insert_id;
    $revision->title = $content->title;
    $revision->contents = serialize($content);
    $revision->eventtype = 'I';
    $revision->userid = $USER->id;
    $revision->timecreated = time();
    $revisionid = $DB->insert_record('lmsmedia_revision', $revision);
    $content->id = $last_insert_id;

    if ($content->id && $revisionid) {
        $content->revisionid = $revisionid;
        $DB->update_record('lmsmedia_contents', $content);
    }

    // insert into lmsmedia_files
    $fileid = $data->uploaded_file;
    if ($fileid && $data->upload_type == 'video') {
        if ($mediafile = $DB->get_record('lmsmedia_files', array('id' => $fileid))) {
            $mediafile->mediaid = $last_insert_id;
            $mediafile->servertype = $content->inouttype;
            $DB->update_record('lmsmedia_files', $mediafile);
        }
    }

    // 공유범위 입력
    switch ($data->target) {
        case '1' :
            $arr_corps_id = explode("#DMT#", $data->corps_id);
            for ($j = 0; $j < count($arr_corps_id); $j++) {
                if (!empty($arr_corps_id[$j])) {
                    $DB->insert_record('lmsmedia_corps', array('mediaid' => $last_insert_id, 'corporationcode' => $arr_corps_id[$j]));
                }
            }
            break;
        case '2' :
            
            $arr_user_ids = explode("#DMT#", $data->user_ids);
            for ($j = 0; $j < count($arr_user_ids); $j++) {
                if (!empty($arr_user_ids[$j])) {
                    $DB->insert_record('lmsmedia_users', array('mediaid' => $last_insert_id, 'gubun' => 2, 'userid' => $arr_user_ids[$j]));
                }
            }

            $arr_org_codes = explode("#DMT#", $data->org_codes);
            for ($j = 0; $j < count($arr_org_codes); $j++) {
                if (!empty($arr_org_codes[$j])) {
                    $DB->insert_record('lmsmedia_users', array('mediaid' => $last_insert_id, 'gubun' => 1, 'organizationcode' => $arr_org_codes[$j]));
                }
            }
         
            break;
        case '3' :
        default :
            break;
    }

    // 분류태그 코드값 입력
    $category_tag_codes = $data->categorycodevalues;
    if (!empty($category_tag_codes)) {
        $arr_category_tag_code = explode("#DMT#", $category_tag_codes);

        $course_tags = array();
        for ($i = 0; $i < count($arr_category_tag_code); $i++) {
            if (!empty($arr_category_tag_code[$i])) {
                $c_tags = new stdClass();
                $c_tags->type = 2;
                $c_tags->curriculumid = $last_insert_id;
                $c_tags->tagcode = $arr_category_tag_code[$i];

                array_push($course_tags, $c_tags);
            }
        }

        if (count($course_tags) > 0) {
            $DB->insert_records('lmsdata_course_tag', $course_tags);
        }
    }
    
    if($data->upload_type == 'video'){
        //썸네일 등록
        $thumbdata = new stdClass();
        $thumbdata->mediaid = $last_insert_id;
        $thumbdata->filepath = $CFG->local_media_file_path.'/Thumbnail/'.$last_insert_id.'_01.jpg';
        $thumbdata->filename = 'defaut01.jpg';
        $thumbdata->isused = 1;
        $DB->insert_record('lmsmedia_thumbs', $thumbdata);
        $thumbdata->filepath = $CFG->local_media_file_path.'/Thumbnail/'.$last_insert_id.'_02.jpg';
        $thumbdata->filename = 'defaut02.jpg';
        $thumbdata->isused = 0;
        $DB->insert_record('lmsmedia_thumbs', $thumbdata);
        
        //xml 파일 생성
        local_media_beecast_create_xml($last_insert_id);
    }
    
    //알림보내기
    $auth = local_media_userauth();
    if($auth->auth == 'HRD Training Manager'){
        $sql = "select u.* from {user} u join {lmsdata_user} lu on lu.userid = u.id join {lmsdata_categories} ca on ca.managerid = lu.userno "
                . "where code = 'LGD_ACT_CATEGORY_VS' and codevalue = :codevalue";
        $manager = $DB->get_record_sql($sql, array('codevalue'=>$content->category2));
        if($manager){
            $eventdata = new stdClass();
            $eventdata->component = 'moodle';
            $eventdata->name = 'courserequestapproved';
            $eventdata->userfrom = $USER;
            $eventdata->userto = $manager;
            $eventdata->subject = get_string('addeddtube','local_media').'.';
            $eventdata->fullmessage = fullname($USER).'님께서 DTube에 ['.$content->title.']를 등록하셨습니다.';
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml = $eventdata->fullmessage;
            $eventdata->smallmessage = $eventdata->fullmessage;
            $eventdata->notification = 1;
            message_send($eventdata);
        }
    }
    
    return $last_insert_id;
}

/**
 * 시리즈 Group 리스트 조회
 */
function local_media_get_thumbnails($id) {
    global $DB;

    $thumbsParam = array();
    if (!(empty($id))) {
        $thumbsParam["mediaid"] = $id;
    }

    $thumbList = $DB->get_records('lmsmedia_thumbs', $thumbsParam);
    return $thumbList;
}

/**
 * 썸네일 정보 입력
 */
function local_media_add_thumbnail($thumbdata) {
    global $CFG, $DB;
    
    $file = $thumbdata->file;
    
    $thumbdata->filename = $file["name"];
    
    $dir = $CFG->local_media_file_path.'/Thumbnail';
    $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $thumbdata->filename);
    $filename = $thumbdata->mediaid.'_'.date('YmdHis').'.'.$ext;
    if (!file_exists($dir)) {
        mkdir($dir, $CFG->directorypermissions, true);
    }
    
    if (move_uploaded_file($file['tmp_name'], $dir.'/'.$filename)) {
        $thumbdata->filepath = $dir.'/'.$filename;
        $thumbid = $DB->insert_record('lmsmedia_thumbs', $thumbdata);
    }else{
        $thumbid = 0;
    }
    
    return $thumbid;

}

/**
 * 섬네일 삭제
 */
function local_media_delete_thumbnail($fileid) {
    global $DB;

    if($file = $DB->get_record('lmsmedia_thumbs', array('id'=>$fileid))) {
        $filepath = $file->filepath;
        if(unlink ($filepath)) {
            $DB->delete_records('lmsmedia_thumbs', array('id' => $fileid));
            return array('result' => 'Y');
        }
    }
    return array('result' => 'P');
}

/**
 * 자막 삭제
 */
function local_media_delete_caption($id, $fileid) {
    global $DB;

    if($file = $DB->get_record('lmsmedia_captions', array('id'=>$fileid))) {
        $filepath = $file->filepath;
        if(unlink ($filepath)) {
            $DB->delete_records('lmsmedia_captions', array('id' => $fileid)); 
            $revision = new stdClass();
            $revision->mediaid = $id;
            $revision->captionid = $fileid;
            $revision->contents = serialize($file);
            $revision->eventtype = 'D';
            $revision->userid = $USER->id;
            $revision->timemodified = time();
            $revisionid = $DB->insert_record('lmsmedia_captions_revision', $revision);
            
            return array('result' => 'Y');
        }
    }
    return array('result' => 'P');
}

function local_media_add_captions($captiondata, $data) {
    global $CFG, $DB;
    
    $files = $captiondata->files;
    $langs = get_string_manager()->get_list_of_translations();
    
    foreach($langs as $key=>$lang){
        
        $file = $files['captionfile'.$key];
        $captiondata->lang = $key;
        if($file){         
            
            $captiondata->filename = $file["name"];
            $dir = $CFG->local_media_file_path.'/caption/'.$captiondata->userid;
            $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $captiondata->filename);
            $filename = $captiondata->mediaid.'_'.date('YmdHis').'.'.$ext;
            if (!file_exists($dir)) {
                mkdir($dir, $CFG->directorypermissions, true);
            }
            
            if($caption = $DB->get_record('lmsmedia_captions',array('id'=>$data->{'captionid'.$key}))){
                unlink($cation->filepath);
            }

            if (move_uploaded_file($file['tmp_name'], $dir.'/'.$filename)) {
                $captiondata->filepath = $dir.'/'.$filename;
                if(!$caption){
                    $captiondata->id = $DB->insert_record('lmsmedia_captions', $captiondata);
                }else{
                    $captiondata->id = $caption->id;
                    $captiondata->timemodified = time();
                    $DB->update_record('lmsmedia_captions', $captiondata);
                }
                $revision = new stdClass();
                $revision->mediaid = $id;
                $revision->captionid = $captiondata->id;
                $revision->contents = serialize($captiondata);
                $revision->eventtype = (!$caption) ? 'I' : 'U';
                $revision->userid = $captiondata->userid;
                $revision->timemodified = time();
                $revisionid = $DB->insert_record('lmsmedia_captions_revision', $revision);
                
                $captiondata->revisionid = $revisionid;
                $DB->update_record('lmsmedia_captions', $captiondata);
            }
        }
    }
   
}

/**
 * 컨텐츠 정보 업데이트
 */
function local_media_update_instance($data, $id, $context) {
    global $DB, $USER;

    switch ($data->mode) {
        case 'default':
            
            //기존 정보 가져오기
            $content = $DB->get_record('lmsmedia_contents',array('id'=>$id));
            $expiredate = $content->expiredate;
            $inouttype = $content->inouttype;
            $oldgroupid = $content->groupid;
            
            // 기본정보 업데이트
            $content->title = $data->title;
            $content->target = $data->target;
            $content->intro = $data->intro;
            $content->groupid = $data->groupid;
            $content->isshared = isset($data->isshared) ? $data->isshared : 0;
            $content->isnoticed = isset($data->notice) ? $data->notice : 0;
            if (isset($data->notice)) {
                $content->timestartnotice = $data->timestartnotice;
                $content->timeendnotice = $data->timeendnotice;
            } else {
                $content->timestartnotice = 0;
                $content->timeendnotice = 0;
            }
            $content->gubun = $data->gubun;
            $content->category1 = $data->category1;
            $content->category2 = $data->category2;
            $content->category3 = $data->category3;
            $content->categorytags = $data->categorytags;
            $content->keywords = $data->keywords;
            $content->timemodified = time();
            $content->inouttype = $data->inouttype;
            $content->isused = $data->isused;
            $content->expiredate = $data->expiredate;
            $content->itemid = $content->intro['itemid'];
            
            //Set group sortorder
            if($content->groupid != $oldgroupid){
                $contentcount = $DB->count_records('lmsmedia_contents',array('groupid'=>$content->groupid));
                $content->sortorder = $contentcount+1;
            }
            
            $DB->update_record('lmsmedia_contents', $content);
            
            //intro 정보
            $content->intro = file_save_draft_area_files($content->itemid, $context->id, 'local_media', 'intro', $id, local_media_editor_options($context, $id), $content->intro['text']);
            $DB->set_field('lmsmedia_contents', 'intro', $content->intro, array('id' => $id));

            //이력 등록하기
            $revision = new stdClass();
            $revision->mediaid = $content->id;
            $revision->title = $content->title;
            $revision->contents = serialize($content);
            $revision->eventtype = 'U';
            $revision->userid = $USER->id;
            $revision->timecreated = time();
            $revisionid = $DB->insert_record('lmsmedia_revision', $revision);

            if ($content->id && $revisionid) {
                $content->revisionid = $revisionid;
                $DB->update_record('lmsmedia_contents', $content);
            }

            // 공유범위 입력
            switch ($data->target) {
                case '1' :
                    $DB->delete_records('lmsmedia_users', array('mediaid' => $id));
                    $existing_corps = $DB->get_records_menu('lmsmedia_corps', array('mediaid' => $id), '', 'corporationcode, id');
                    $arr_corps_id = explode("#DMT#", $data->corps_id);
                    $arrdellist = $existing_corps;
                    for ($j = 0; $j < count($arr_corps_id); $j++) {
                        if (!empty($arr_corps_id[$j])) {
                            if (!$existing_corps[$arr_corps_id[$j]]) {
                                $DB->insert_record('lmsmedia_corps', array('mediaid' => $id, 'corporationcode' => $arr_corps_id[$j]));
                            }
                            unset($arrdellist[$arr_corps_id[$j]]); 
                        }
                    }
                    foreach($arrdellist as $arrdel){
                        $DB->delete_records('lmsmedia_corps', array('id'=>$arrdel));
                    }
                    break;
                case '2' :
                    
                    $DB->delete_records('lmsmedia_corps', array('mediaid' => $id));
                
                    $existing_users = $DB->get_records_menu('lmsmedia_users', array('mediaid' => $id, 'gubun' => '2'), '', 'userid, id');
                    $arr_user_ids = explode("#DMT#", $data->user_ids);
                    $arrdellist = $existing_users;
                    for ($j = 0; $j < count($arr_user_ids); $j++) {
                        if (!empty($arr_user_ids[$j])) {
                            if (!$existing_users[$arr_user_ids[$j]]) {
                                $DB->insert_record('lmsmedia_users', array('mediaid' => $id, 'gubun' => 2, 'userid' => $arr_user_ids[$j]));
                            }
                            unset($arrdellist[$arr_user_ids[$j]]); 
                        }
                    }
                    foreach($arrdellist as $arrdel){
                        $DB->delete_records('lmsmedia_users', array('id'=>$arrdel));
                    }

                    $existing_orgs = $DB->get_records_menu('lmsmedia_users', array('mediaid' => $id, 'gubun' => '1'), '', 'organizationcode, id');
                    $arr_org_codes = explode("#DMT#", $data->org_codes);
                    $arrdellist = $existing_orgs;
                    for ($j = 0; $j < count($arr_org_codes); $j++) {
                        if (!empty($arr_org_codes[$j])) {
                            if (!$existing_orgs[$arr_org_codes[$j]]) {
                                $DB->insert_record('lmsmedia_users', array('mediaid' => $id, 'gubun' => 1, 'organizationcode' => $arr_org_codes[$j]));
                            }
                            unset($arrdellist[$arr_org_codes[$j]]); 
                        }
                    }
                    foreach($arrdellist as $arrdel){
                        $DB->delete_records('lmsmedia_users', array('id'=>$arrdel));
                    }
                    
                    break;
                case '3' :
                    $DB->delete_records('lmsmedia_corps', array('mediaid' => $id));
                    $DB->delete_records('lmsmedia_users', array('mediaid' => $id));
                    break;
                default :
                    break;
            }
            
            //분류 태그 수정
            $category_tag_codes = $data->categorycodevalues;
            $arr_tag_codes = explode("#DMT#", $category_tag_codes);
            
            $existing_tags = $DB->get_records_menu('lmsdata_course_tag', array('curriculumid' => $id, 'type' => '2'), '', 'tagcode, id');
            $arrdellist = $existing_tags;
            for ($j = 0; $j < count($arr_tag_codes); $j++) {
                if (!empty($arr_tag_codes[$j])) {
                    if (!$existing_tags[$arr_tag_codes[$j]]) {
                        $DB->insert_record('lmsdata_course_tag', array('type' => '2', 'curriculumid' => $id, 'tagcode' => $arr_tag_codes[$j]));
                    }
                    unset($arrdellist[$arr_tag_codes[$j]]); 
                }
            }
            
            foreach($arrdellist as $arrdel){
                $DB->delete_records('lmsdata_course_tag', array('id'=>$arrdel));
            }
            
            if ($content->contype == 'video' && ($expiredate != $data->expiredate || $inouttype != $data->inouttype)) {
                local_media_beecast_create_xml($id, true);
            }
            

            break;
        case 'addition':
            // 추가정보 업데이트
            $content = new stdClass();
            $content->id = $id;
            $content->teacher = $data->teacher;
            $content->ccmark = $data->ccmark == '3' ? $data->ccmarktext : $data->ccmark;
            $content->islogo = $data->islogo;
            $content->timemodified = time();

            var_dump($content);
            $DB->update_record('lmsmedia_contents', $content);

            break;
        case 'thumbnail':    
            //썸네일 등록
            $thumbdata = new stdClass();
            $thumbdata->mediaid = $id;
            $thumbdata->file = $_FILES['uploadthumbnail'];
            $newthumbid = local_media_add_thumbnail($thumbdata);
            
            if(!$data->thumsid){
                $data->thumsid = $newthumbid;
            }
            
            // 썸네일 사용여부 업데이트
            if($thumbs = $DB->get_records('lmsmedia_thumbs', array('mediaid'=>$id))){
                foreach($thumbs as $thumb){
                    $thumb->isused = ($thumb->id == $data->thumsid)? 1 : 0;
                    $DB->update_record('lmsmedia_thumbs', $thumb);
                }
            }
            break;
        case 'caption':
            // 자막 등록
            if($_FILES){
                
                $captiondata = new stdClass();
                $captiondata->mediaid = $id;
                $captiondata->userid = $USER->id;
                $captiondata->timecreated = time();
                
                $captiondata->files = $_FILES;
                
                local_media_add_captions($captiondata, $data);
   
            }
            
            break;
        case 'embed':
            // Embed Code
            
            break;
        default:
            break;
    }
}

function local_media_delete_contents($mediaid){    
    global $DB, $USER;
    
    $context = context_system::instance();
    
    if($media = $DB->get_record('lmsmedia_contents',array('id'=>$mediaid))){
    
        //공유 대상 삭제
        $DB->delete_records('lmsmedia_corps', array('mediaid'=>$mediaid));
        $DB->delete_records('lmsmedia_users', array('mediaid'=>$mediaid));

        //콘텐츠 파일 삭제
        if($files = $DB->get_records('lmsmedia_files',array('mediaid'=>$mediaid))){
            
            if ($media->contype == 'video') {
                $DB->set_field('lmsmedia_contents', 'expiredate', time(), array('id' => $mediaid));
                local_media_beecast_create_xml($mediaid, true);
            }
            
            foreach($files as $file){
                if(unlink ($file->filepath)) {
                    $DB->delete_records('lmsmedia_files', array('id'=>$file->id));
                }
            }
        }

        //썸네일 삭제
        if($files = $DB->get_records('lmsmedia_thumbs',array('mediaid'=>$mediaid))){
            foreach($files as $file){
                if(unlink ($file->filepath)) {
                    $DB->delete_records('lmsmedia_thumbs', array('id'=>$file->id));
                }
            }
        }

        //자막 삭제
        if($files = $DB->get_records('lmsmedia_captions',array('mediaid'=>$mediaid))){
            foreach($files as $file){
                if(unlink ($file->filepath)) {
                    $DB->delete_records('lmsmedia_captions', array('id'=>$file->id));
                }
            }
        }

        //댓글 삭제
        $DB->delete_records('lmsmedia_comments', array('mediaid'=>$mediaid));

        //Revision 등록
        $revision = new stdClass();
        $revision->mediaid = $mediaid;
        $revision->title = $media->title;
        $revision->contents = serialize($media);
        $revision->eventtype = 'D';
        $revision->userid = $USER->id;
        $revision->timecreated = time();
        $revisionid = $DB->insert_record('lmsmedia_revision', $revision);
        
        //intro 파일 삭제
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'local_media', 'intro', $mediaid, 'id');
        if(!empty($files)) {
            foreach($files as $file) {
                $file->delete();
            }
        }

        //콘텐츠 정보 삭제
        $DB->delete_records('lmsmedia_contents', array('id'=>$mediaid));
    
    }
    
    return 1;
    
}

function local_media_beecast_create_xml($mediaid, $update=false) {
    global $CFG, $DB;
    
    $xmldir = $CFG->local_media_xml_path;
    if(!file_exists($xmldir)) {
        mkdir($xmldir, $CFG->directorypermissions, true);
    }
    
    $media = $DB->get_record('lmsmedia_contents', array('id'=>$mediaid));
    $category = $DB->get_record_sql("SELECT ca.id, cl.id AS langid, cl.codevalue, cl.name
            FROM {lmsdata_categories} ca
            JOIN {lmsdata_categories_lang} cl ON cl.code = ca.code 
                                              AND cl.codevalue = ca.codevalue 
                                              AND cl.corporationcode = ca.corporationcode
            WHERE cl.codevalue = :codevalue
              AND cl.langcode = :langcode
              AND cl.corporationcode = :corporationcode",
            array('codevalue'=>$media->category3,'langcode'=>'KO','corporationcode'=>'LGDKR'));
    $mediafile = $DB->get_record('lmsmedia_files', array('mediaid'=>$mediaid));
    
    $pathinfo = pathinfo($mediafile->filepath);
    
    $xmlobj = new stdClass();
    $xmlobj->ProviderNo = 1;
    $xmlobj->ServiceNo = 1;
    $xmlobj->ContentId = $media->id;
    $xmlobj->CategoryNo = $media->category3;
    $xmlobj->CategoryName = $category->name;
    $xmlobj->Date = date('Y-m-d',$media->timemodified);
    $xmlobj->FileName = $pathinfo['basename'];
    $xmlobj->FilePath = '';
    $xmlobj->FileSize = $mediafile->filesize;
    $xmlobj->Title = $media->title;
    $xmlobj->Synopsis = '';
    $xmlobj->Memo = '';
    $xmlobj->MediaGuid = '';
    $xmlobj->DeliveryType = '';
    $xmlobj->ReContentAcqYN = '';
    $xmlobj->MediaExpireDate = date('Y-m-d',$media->expiredate);
    
    switch ($media->inouttype) {
        case 'A':
            $xmlobj->DeliveryType = 'All';
            break;
        case 'I':
            $xmlobj->DeliveryType = 'Internal';
            break;
        case 'E':
            $xmlobj->DeliveryType = 'External';
            break;
        default:
            break;
    }
    
    if($update) {
        $xmlobj->ReContentAcqYN = 'Y';
    } else {
        $xmlobj->ReContentAcqYN = 'N';
    }
    
    $xmlstr = local_media_generate_xml($xmlobj, 'FileLog', true);
    
    $xmlfilename = $pathinfo['filename'].'.xml';
    
    $xmlfile = fopen($xmldir.'/'.$xmlfilename, "w");
    fwrite($xmlfile, $xmlstr);
    fclose($xmlfile);
}

function local_media_generate_xml(stdClass $obj, $node_block, $newline=false) {
    $newlinestr = '';
    if($newline) {
        $newlinestr = "\n";
    }
    
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>' . $newlinestr;
    $xml .= '<' . $node_block . '>';
    
    $arr = get_object_vars($obj);
    $xml .= local_media_generate_xml_from_array($arr, $newline);
    $xml .= '</' . $node_block . '>';
    
    return $xml;
}

function local_media_generate_xml_from_array($array, $newline=false) {
    $newlinestr = '';
    if($newline) {
        $newlinestr = "\n";
    }
    
    $xml = '';

    if (is_array($array) || is_object($array)) {
        foreach ($array as $key=>$value) {
            $xml .= '<' . $key . '>' . local_media_generate_xml_from_array($value) . '</' . $key . '>' . $newlinestr;
        }
    } else {
        $xml = htmlspecialchars($array, ENT_QUOTES);
    }

    return $xml;
}


function local_media_beecast_get_streaming_url($contentid) {
    global $CFG;
    /* 스트리밍 URL 확인
     * ContentStremingInfo
     * UrlType - Url 타입: 001(내부), 002(내부:Alternate), 003(외부:CDN)
     * StreaminUrl - 스트리밍 주소
     * ResponseHeader
     * error_code  - 101: 성공
     * error_information - 에러메시지
     */
    $url = $CFG->local_media_system_url.'/CoreService.svc/GetContentStreamingInfoByIP/qVc1bLpGsgvQqrXiRcWyOw%3d%3d';
    $postdata = array(
        'request_system_ID' => $CFG->local_media_system_id,
        'request_system_password' => $CFG->local_media_system_password,
        'serviceNo' => 1,
        'content_ID' => $contentid,
        'IPAddress' => local_media_get_client_ip()
        );
    
    return local_media_beecast_post($url, $postdata);
}

function local_media_beecast_get_delivery_info($contentid) {
    global $CFG;
    /* 배포상태 확인
     * ContentDeliveryInfo
     * DeliveryType - 001: 내부, 002: 외부CDN
     * Delivery Step - 001:원본입수, 002:Transcoding, 003:입수완료, 004:배포
     * StatusCode - 001: 대기, 002: 진행중, 003: 완료
     * ResponseHeader
     * error_code  - 101: 성공
     * error_information - 에러메시지
     */
    $url = $CFG->local_media_system_url.'/CoreService.svc/GetContentDeliveryInfoByContentId/qVc1bLpGsgvQqrXiRcWyOw%3d%3d';
    $postdata = array(
        'request_system_ID' => $CFG->local_media_system_id,
        'request_system_password' => $CFG->local_media_system_password,
        'content_ID' => $contentid
    );

    return local_media_beecast_post($url, $postdata);
}

function local_media_beecast_post($url, $postdata) {
    $ch = curl_init ();
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $res = curl_exec ($ch);
    
    $errorno = curl_errno($ch);
    
    if($errorno){
        $error = new stdClass();
        $error->code = $errorno;
        $error->msg = curl_error($ch);
        
        return $error;
    }
    
    curl_close($ch);
    
    return json_decode($res);
}

function local_media_get_client_ip() {
    $ipaddress = '';
    
    if (getenv('HTTP_IV_REMOTE_ADDRESS')) {
        $ipaddress = getenv('HTTP_IV_REMOTE_ADDRESS');
    } else if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
    } else if(getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    } else if(getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
    } else if(getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    } else if(getenv('HTTP_FORWARDED')) {
       $ipaddress = getenv('HTTP_FORWARDED');
    } else if(getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }
    
    return $ipaddress;
}

//사내망, 사외망 체크 (IP대역에서 현재 나의 IP와 비교)
function local_media_ipband_check(){
    global $DB;
    $ipaddrs = explode('.', local_media_get_client_ip());
    $ipaddr = $ipaddrs[0].'.'.$ipaddrs[1].'.'.$ipaddrs[2];
    if($ipband = $DB->get_record('lmsdata_ipband', array('ipband'=>$ipaddr))){
        $inout = 1; //사내
    }else{
        $inout = 2; //사외
    }
    return $inout;
}

function local_media_inout_check($id){
    global $DB;

    //사내 사외 여부 체크하여 사내에서 볼 수 있는 것은 사내에서만 접속 가능하게 처리
    $inout = local_media_ipband_check(); //1: 사내, 2: 사외

    //미디어 데이터 가져오기
    if($inouttype = $DB->get_field('lmsmedia_contents', 'inouttype', array('id'=>$id))){

        //사내사외구분(전체: A, 내부: I, 외부: E)         
        if($inout == 2 && $inouttype == 'I'){
            echo '<script>alert("'.get_string('onlyincom','local_media').'.");self.close();</script>';
            die();
        }

    }else{
        echo '<script>alert("'.get_string('noaddedvideo','local_media').'.");self.close();</script>';
        die();
    }
   
}


//좋아요 수치 가져오기
function local_media_get_npsscore($id){
    global $DB;
    //좋아요 수치
    $sql = "select ((sum(CASE WHEN score >= 9 THEN 1 ELSE 0 END) - sum(CASE WHEN score <= 6 THEN 1 ELSE 0 END))/count(*)/2 + 0.5)*100 as nps 
        from {lmsmedia_satisfaction} where mediaid = :id";
    $score = $DB->get_field_sql($sql, array('id'=>$id));
    $scoretext = round($score, 0).'%';
    return $scoretext;
}

function local_media_save_file($file, $filearea, $mediafile = '', $mediaid = 0, $userid = 0) {
    global $DB, $USER, $CFG;
    
    if($userid == 0) {
        $userid = $USER->id;
    }
    
    if(!$mediafile){
        //files 테이블에 우선 등록
        $newfile = new stdClass();
        $newfile->servertype = 'A';
        $newfile->mediaid = $mediaid;
        $newfile->filename = $file["name"];
        $newfile->filesize = $file['size'];
        $newfile->id = $DB->insert_record('lmsmedia_files',$newfile);
    }else{
        $newfile = $mediafile;
    }
    
    $dir = $CFG->local_media_storage_path;
    $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $newfile->filename);
    $filename = $newfile->id.'_'.date('YmdHis').'.'.$ext;
    if (!file_exists($dir)) {
        mkdir($dir, $CFG->directorypermissions, true);
    }
    
    if (move_uploaded_file($file['tmp_name'], $dir.'/'.$filename)) {
        $newfile->filepath = $dir.'/'.$filename;
        $DB->update_record('lmsmedia_files',$newfile);

        return $newfile->id;
    } else {
        return 0;
    }
}

function local_media_delete_file($fileid) {
    global $DB, $USER;
    
    if($file = $DB->get_record('lmsmedia_files', array('id'=>$fileid))) {
        $filepath = $file->filepath;
        if(unlink ($filepath)) {
            $DB->delete_records('lmsmedia_files', array('id'=>$fileid));
        }
    }
}

function local_media_send_file($id, $forcedownload=1) {
    global $SITECFG, $DB, $CFG, $COURSE;
    if($file = $DB->get_record('lmsmedia_files', array('id'=>$id))) {
        $filepath = $file->filepath;
        $filename = $file->filename;
        $filesize = $file->filesize;
        
        $mimetype = "application/octet-stream";
        if(!empty($snufile->mimetype)) {
            $mimetype = $snufile->mimetype;
        }
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: $mimetype; charset=UTF-8");
        if($forcedownload) {
            header("Content-Disposition: attachment; filename=\"".urlencode($filename)."\"");
        }
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: $filesize");
        
        readfile($filepath);
    }
}

function local_media_browser_check(){
    
    $data = new stdClass();
    $data->device = '';
    
    $agent = get_browser(null, true);
    $data->browser = $agent['browser'];          // 브라우저 종류
    $data->version = $agent['version'];           // 브라우저 버전
    $data->platform = $agent['platform'];         // OS 종류 Win7, Win10, iOS, Android
    $data->device   = $agent['device_type'];     // Desktop, Mobile Phone
    
    if($data->device == 'Mobile Phone' || $data->device == 'Mobile Device') {
        $data->device = 'M';
    }else if($data->device == 'Desktop') {
        $data->device = 'P';
    }else {
        $data->device = 'E';
    }
    
    return $data;
    
}

function local_media_mobile_check() {
    // 모바일 기종(배열 순서 중요, 대소문자 구분 안함)
    $ary_m = array("iPhone","iPod","IPad","Android","Blackberry","SymbianOS|SCH-M\d+","Opera Mini","Windows CE","Nokia","Sony","Samsung","LGTelecom","SKT","Mobile","Phone");
    for($i=0; $i<count($ary_m); $i++){
        if(preg_match("/$ary_m[$i]/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return $ary_m[$i];
            break;
        }
    }
    return "PC";
}

function local_media_userauth() {
    global $DB, $USER;
    
    $data = new stdClass();
    
    if(is_siteadmin($USER)){
       $data->auth = 'Administrator';
       $data->corp = 'LGDKR';
       return $data;
    }
    
    $mo_auths = $DB->get_records('lmsdata_moodle_auth', array('isdtube'=>1), 'id asc');
    $user = $DB->get_record('lmsdata_user', array('userid' => $USER->id));
    foreach($mo_auths as $mo_auth){
        $sql = "select * from {lmsdata_user_auth} where userno = :userno and authcode = :authcode and startdate <= :startdate and enddate >= :enddate";
        if($auth = $DB->get_record_sql($sql, array('userno'=>$user->userno,'authcode'=>$mo_auth->code, 'startdate'=>date('Ymd'), 'enddate'=>date('Ymd')))){
            $data->auth = $mo_auth->name;
            $data->corp = $user->corporationcode;
            return $data;
        }
    }
    
    return '';
    
}

function local_media_manage_check($auth, $content){
    global $DB, $USER;
    
    $contentmanage = 0;
    
    if($auth->auth == 'Administrator' || $auth->auth == 'Global Master'){
        $contentmanage = 1;
    }
    
    if($auth->auth == 'Local Master'){
        if($corp = $DB->get_record('lmsmedia_corps',array('mediaid'=>$id,'corporationcode'=>$auth->corp))){
            $contentmanage = 1;
        }
    }
    
    if($auth->auth == 'HRD Training Manager'){
        if($content->userid == $USER->id){
            $contentmanage = 1;
        }
    }
    
    return $contentmanage;
}

/**
 * 엑셀파일로 부터 미디어 콘텐츠 등록
 * 
 * 동영상 파일은 미리 업로드 되어 있어야 한다.
 * 
 * @global object $CFG
 * @global object $DB
 * @global object $USER
 * @param string $excelfile 엑셀파일 경로
 * @param array $mediafiles 업로드된 동영상 정보 배열. lmsmedia_files 테이블 정보 array(filename=>rowdata,...)
 * @return array
 */
function local_media_add_bulk($excelfile, $mediafiles) {
    global $CFG, $DB, $USER;

    $objReader = PHPExcel_IOFactory::createReaderForFile($excelfile);
    $objReader->setReadDataOnly(true);
    $objExcel = $objReader->load($excelfile);

    // 콘텐츠 등록 시작
    $objExcel->setActiveSheetIndex(0);
    $objWorksheet = $objExcel->getActiveSheet();
    $rowIterator = $objWorksheet->getRowIterator();

    foreach ($rowIterator as $row) { // 모든 행에 대해서
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
    }

    $maxRow = $objWorksheet->getHighestRow();

    $timecurrent = time();

    $counts = array (
        'total'=>0,
        'nocompcount'=>0,
        'success'=>0,
        'error'=>0);

    // $content->groupid 를 찾기 위해
    $existgroups = $DB->get_records_menu('lmsmedia_groups', null, '','name, id');
    
    //complete가 없는 것이 있는지 체크
    for ($i = 2; $i <= $maxRow; $i++) {
        if(strtolower(trim($objWorksheet->getCell('Y' . $i)->getValue())) != 'complete' && strtolower(trim($objWorksheet->getCell('Y' . $i)->getValue())) != ''){
            $counts['nocompcount'] += 1;
            //업로드가 안됨. 등록된 파일 삭제
            foreach($mediafiles as $mediafile) {
                $mediafile1 = $DB->get_record('lmsmedia_files', array('id'=>$mediafile->id));
                //if($mediafile1->mediaid == 0) {
                local_media_delete_file($mediafile1->id);
                //콘텐츠도 선등록되었으면 삭제해야함.
                $DB->delete_records('lmsmedia_contents', array('id'=>$mediafile1->mediaid));
                //}
            }
        }
    }
    
    if($counts['nocompcount'] > 0){
        return $counts;
    }

    for ($i = 2; $i <= $maxRow; $i++) {
        $counts['total'] += 1;
        if(strtolower(trim($objWorksheet->getCell('Y' . $i)->getValue())) != 'complete'){
            $counts['error'] += 1;
            continue;
        }else{
            $videofilename = trim($objWorksheet->getCell('V' . $i)->getValue());
            if(!isset($mediafiles[$videofilename])) {
                $counts['error'] += 1;
                continue;
            }

            $groupid = 0;
            $serisename = trim($objWorksheet->getCell('I' . $i)->getValue());
            if(!empty($serisename)) {
                // $content->groupid 를 찾기 위해
                $groupid = $DB->get_field('lmsmedia_groups', 'id', array('name'=>$serisename));
                if(!$groupid) {
                    $group = new stdClass();
                    $group->name = $serisename;
                    $group->userid = $USER->id;
                    $group->timecreated = $timecurrent;
                    $group->timemodified = $timecurrent;
                    $groupid = $DB->insert_record('lmsmedia_groups', $group);
                }
            }

            $inouttype = 'I'; // A: 전체, I: 사내, E: 사외
            $useout = trim($objWorksheet->getCell('U' . $i)->getValue());
            if($useout == '1') {
                $inouttype = 'A';
            }

            // 검색용 분류코드 categorytags
            $categorytagsstr = trim($objWorksheet->getCell('R' . $i)->getValue());
            $categorytags = explode(',', $categorytagsstr);
            foreach($categorytags as $key=>$value) {
                $categorytags[$key] = '#'.$value;
            }

            // 콘텐츠 만료일
            $expiredate = strtotime(trim($objWorksheet->getCell('W' . $i)->getValue()));

            // 콘텐츠 등록
            $existmedia = $mediafiles[$videofilename];
            if(!$content = $DB->get_record('lmsmedia_contents', array('id'=>$existmedia->mediaid))){
                $content = new stdClass();
            }
            
            $content->title = trim($objWorksheet->getCell('A' . $i)->getValue());
            $content->intro = trim($objWorksheet->getCell('B' . $i)->getValue());
            $content->target = trim($objWorksheet->getCell('C' . $i)->getValue());
            $content->isnoticed = trim($objWorksheet->getCell('G' . $i)->getValue());
            $content->gubun = trim($objWorksheet->getCell('H' . $i)->getValue()); // 1: 디튜브, 2: 강의자료활용, 3:모두활용
            $content->groupid = $groupid;
            $content->sortorder = trim($objWorksheet->getCell('J' . $i)->getValue());
            $content->category1 = trim($objWorksheet->getCell('O' . $i)->getValue());
            $content->category2 = trim($objWorksheet->getCell('P' . $i)->getValue());
            $content->category3 = trim($objWorksheet->getCell('Q' . $i)->getValue());
            $content->categorytags = implode(' ', $categorytags); // 검색용분류코드, # 붙은 택스트가 들어감
            $content->keywords = trim($objWorksheet->getCell('T' . $i)->getValue());
            $content->isshared = trim($objWorksheet->getCell('X' . $i)->getValue()); //타 과정 공유여부
            $content->userid = $USER->id;
            $content->inouttype = $inouttype;
            $content->isused = 1;
            $content->managerid = 0;
            $content->expiredate = $expiredate;
            $content->timecreated = $timecurrent;
            $content->timemodified = $timecurrent;
            $content->contype = 'video';
            $content->mediafile = '';
            
            if($content->id == 0){
                $content->id = $DB->insert_record('lmsmedia_contents', $content);
            }else{
                $DB->update_record('lmsmedia_contents', $content);
            }

            // 이력 등록
            $revision = new stdClass();
            $revision->id = 0;
            $revision->mediaid = $content->id;
            $revision->title = $content->title;
            $revision->contents = serialize($content);
            $revision->eventtype = 'I';
            $revision->userid = $USER->id;
            $revision->timecreated = $timecurrent;
            $revision->id = $DB->insert_record('lmsmedia_revision', $revision);

            // 콘텐츠의 revisionid 업데이트
            $content->revisionid = $revision->id;
            $DB->update_record('lmsmedia_contents', $content);

            // lmsmedia_files 업데이트
            $mediafile = $mediafiles[$videofilename];
            $mediafile->mediaid = $content->id;
            $mediafile->servertype = $content->inouttype;
            $DB->update_record('lmsmedia_files', $mediafile);

            // 공유범위 입력
            switch ($content->target) {
                case '1' :
                    $targets = trim($objWorksheet->getCell('D' . $i)->getValue());
                    if(!empty($targets)) {
                        $corps = explode(',', $targets);
                        foreach ($corps as $corp) {
                            $DB->insert_record('lmsmedia_corps', array('mediaid' => $content->id, 'corporationcode' => trim($corp)));
                        }
                    }
                    break;
                case '2' :
                    $targetsuser = trim($objWorksheet->getCell('E' . $i)->getValue()); // 개인
                    if(!empty($targetsuser)) {
                        $usernames = explode(',', $targetsuser);
                        foreach ($usernames as $username) {
                            $username = core_text::strtolower(trim($username));
                            if($user = $DB->get_record('user', array('username'=>$username))) {
                                $DB->insert_record('lmsmedia_users', array('mediaid' => $content->id, 'gubun' => 2, 'userid' => $user->id));
                            }
                        }
                    }
                    $targetsgroup = trim($objWorksheet->getCell('F' . $i)->getValue()); // 그룹
                    if(!empty($targetsgroup)) {
                        $orgcodes = explode(',', $targetsgroup);
                        foreach ($orgcodes as $orgcode) {
                            $DB->insert_record('lmsmedia_users', array('mediaid' => $content->id, 'gubun' => 1, 'organizationcode' => trim($orgcode)));
                        }
                    }
                    break;
                case '3' :
                default :
                    break;
            }

            // 분류태그 코드값 입력, 검색용분류태그코드, 
            $tagcodestr = trim($objWorksheet->getCell('S' . $i)->getValue());
            if (!empty($tagcodestr)) {
                $coursetags = array();

                $tagcodes = explode(",", $tagcodestr);
                foreach ($tagcodes as $tagcode) {
                    $c_tags = new stdClass();
                    $c_tags->type = 2;
                    $c_tags->curriculumid = $content->id;
                    $c_tags->tagcode = $tagcode;

                    array_push($coursetags, $c_tags);
                }

                if (count($coursetags) > 0) {
                    $DB->insert_records('lmsdata_course_tag', $coursetags);
                }
            }

            // 썸네일 등록
            $thumbdata = new stdClass();
            $thumbdata->mediaid = $content->id;
            $thumbdata->filepath = $CFG->local_media_file_path.'/Thumbnail/'.$content->id.'_01.jpg';
            $thumbdata->filename = 'defaut01.jpg';
            $thumbdata->isused = 1;
            $DB->insert_record('lmsmedia_thumbs', $thumbdata);

            $thumbdata->filepath = $CFG->local_media_file_path.'/Thumbnail/'.$content->id.'_02.jpg';
            $thumbdata->filename = 'defaut02.jpg';
            $thumbdata->isused = 0;
            $DB->insert_record('lmsmedia_thumbs', $thumbdata);

            // XML 생성
            local_media_beecast_create_xml($content->id);

            $counts['success'] += 1;
        }
        
    }
    
    // 업로드한 미디어 파일 중 엑셀에서 찾지 못해 등록이 안된 파일 삭제
    foreach($mediafiles as $mediafile) {
        $mediafile1 = $DB->get_record('lmsmedia_files', array('id'=>$mediafile->id));
        if($mediafile1->mediaid == 0) {
            local_media_delete_file($mediafile1->id);
        }
    }

    return $counts;
}

function local_media_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB;

    $fileareas = array('attachment', 'intro');
    if (!in_array($filearea, $fileareas)) {
        return false;
    }


    $fs = get_file_storage();
    $relativepath = implode('/', $args);

    $fullpath = "/$context->id/local_media/$filearea/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }


    // finally send the file
    send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}

function local_media_editor_options($context, $contentid) {
    global $COURSE, $PAGE, $CFG;
// TODO: add max files and max size support
    $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
    return array(
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'maxbytes' => $maxbytes,
        'trusttext' => true,
        'return_types' => FILE_INTERNAL | FILE_EXTERNAL,
        'subdirs' => file_area_contains_subdirs($context, 'local_media', 'intro', $contentid)
    );
}

function local_media_cron(){
    global $DB;
   
    
}

function local_media_get_embed_info($mediafile, $type) {
    global $CFG;
    
    if($type == 'vimeo'){
        $url = 'https://vimeo.com/api/oembed.json?url='.urlencode($mediafile);
    }else if($type == 'youtube'){
        $vids = explode('/', $mediafile);
        $vid1 = trim(str_replace("watch?v=", "", $vids[sizeof($vids) - 1])); 
        $vid1s = explode('&', $vid1);
        $vid = trim($vid1s[0]);
        $url = 'https://www.googleapis.com/youtube/v3/videos?id='.$vid.'&part=contentDetails&key='.$CFG->local_media_youtube_key;
    }
    
    $ch = curl_init ();
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $res = curl_exec($ch);
    
    $errorno = curl_errno($ch);
    
    if($errorno){
        $error = new stdClass();
        $error->code = $errorno;
        $error->msg = curl_error($ch);
        
        return $error;
    }
    
    curl_close($ch);
    
    $result = json_decode($res);
    
    $obj = new stdClass();
    
    if($type == 'vimeo'){
        $obj->thumbnail = $result->thumbnail_url;
        $obj->duration = $result->duration;
    }else if($type == 'youtube'){
        $obj->thumbnail = 'https://img.youtube.com/vi/'.$vid.'/hqdefault.jpg';
        //PT2H2M53S
        $duration = trim(str_replace('PT','',$result->items[0]->contentDetails->duration));
        $time1 = strpos('H',$duration)? explode('H',$duration):'';
        $time2 = ($time1[1])? explode('M',$time1[1]):explode('M',$duration);
        $time3 = ($time2[1])? explode('S',$time2[1]):explode('S',$duration);
        $h = (int) $time1[0];
        $m = (int) $time2[0];
        $s = (int) $time3[0];
        $obj->duration = ($h * 3600) + ($m * 60) + $s;
    }    
    
    return $obj;
}

function local_media_get_duration_hms($duration){
    $getHours = floor($duration / 3600);
    $getMins = floor(($duration - ($getHours * 3600)) / 60);
    $getSecs = floor($duration % 60);

    $getMins = str_pad($getMins, 2, '0', STR_PAD_LEFT);
    $getSecs = str_pad($getSecs, 2, '0', STR_PAD_LEFT);

    $durationtime = ($getHours) ? str_pad($getHours, 2, '0', STR_PAD_RIGHT) . ':' . $getMins . ':' . $getSecs : $getMins . ':' . $getSecs;
    
    return $durationtime;
}

function local_media_get_total_pages($rows, $limit = 10) {
    if ($rows == 0) {
        return 1;
    }

    $total_pages = (int) ($rows / $limit);

    if (($rows % $limit) > 0) {
        $total_pages += 1;
    }

    return $total_pages;
}

function local_media_get_paging_bar($url, $params, $total_pages, $current_page, $max_nav = 10) {
    $total_nav_pages = local_media_get_total_pages($total_pages, $max_nav);
    $current_nav_page = (int) ($current_page / $max_nav);
    if (($current_page % $max_nav) > 0) {
        $current_nav_page += 1;
    }
    $page_start = ($current_nav_page - 1) * $max_nav + 1;
    $page_end = $current_nav_page * $max_nav;
    if ($page_end > $total_pages) {
        $page_end = $total_pages;
    }

    if (!empty($params)) {
        $tmp = array();
        foreach ($params as $key => $value) {
            $tmp[] = $key . '=' . $value;
        }
        $tmp[] = "page=";
        $url = $url . "?" . implode('&', $tmp);
    } else {
        $url = $url . "?page=";
    }
    echo html_writer::start_tag('div', array('class' => 'board-breadcrumbs'));
    if ($current_nav_page > 1) {
        // echo '<span class="board-nav-prev"><a class="prev" href="'.$url.(($current_nav_page - 2) * $max_nav + 1).'"><</a></span>';
    } else {
        // echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    if ($current_page > 1) {
        echo '<span class="board-nav-prev"><a class="prev" href="' . $url . ($current_page - 1) . '"><</a></span>';
    } else {
        echo '<span class="board-nav-prev"><a class="prev" href="#"><</a></span>';
    }
    echo '<ul>';
    for ($i = $page_start; $i <= $page_end; $i++) {
        if ($i == $current_page) {
            echo '<li class="current"><a href="#">' . $i . '</a></li>';
        } else {
            echo '<li><a href="' . $url . '' . $i . '">' . $i . '</a></li>';
        }
    }
    echo '</ul>';
    if ($current_page < $total_pages) {
        echo '<span class="board-nav-next"><a class="next" href="' . $url . ($current_page + 1) . '">></a></span>';
    } else {
        echo '<span class="board-nav-next"><a class="next" href="#">></a></span>';
    }
    if ($current_nav_page < $total_nav_pages) {
        //echo '<a class="next_" href="' . $url . ($current_nav_page * $max_nav + 1) . '"></a>';
    } else {
        //echo '<a class="next_" href="#"></a>';
    }
    echo html_writer::end_tag('div');
}

function local_media_search_paging($totalcount, $page, $perpage, $baseurl, $params = null, $maxdisplay = 9) {
    global $CFG;
    $pagelinks = array();
   
    $lastpage = 1;
    if($totalcount > 0) {
        $lastpage = ceil($totalcount / $perpage);
    }
   
    if($page > $lastpage) {
        $page = $lastpage;
    }
           
    if ($page > round(($maxdisplay/3)*2)) {
        $currpage = $page - round($maxdisplay/2);
        if($currpage > ($lastpage - $maxdisplay)) {
            if(($lastpage - $maxdisplay) > 0){
                $currpage = $lastpage - $maxdisplay;
            }
        }
    } else {
        $currpage = 1;
    }
   
   
   
    if($params == null) {
        $params = array();
    }
   
    $prevlink = '';
    if ($page > 1) {
        $params['page'] = 1;
        $prevlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="next" src="'.$CFG->wwwroot.'/siteadmin/img/pagination_left.png"/>', array('class'=>'next'));
//        $prevlink = '<span class="prev"><a href="'.$prevlink.'">&lt;</a></span>';
    } else {
        $prevlink = '<a href="#noclick" class="next"><img alt="next" src="'.$CFG->wwwroot.'/siteadmin/img/pagination_left.png"/></a>';
        $prevlink = '<span class="prev"><a href="#noclick">&lt;</a></span>';
    }
   
    $nextlink = '';
     if ($page < $lastpage) {
        $params['page'] = $lastpage;
        $nextlink = html_writer::link(new moodle_url($baseurl, $params), '<img alt="prev" src="'.$CFG->wwwroot.'/siteadmin/img/pagination_right.png"/>', array('class'=>'prev'));
 //       $nextlink = '<span class="next"><a href="#">&gt;</a></span>';
    } else {
        $nextlink = '<a href="#noclick" class="prev"><img alt="prev" src="'.$CFG->wwwroot.'/siteadmin/img/pagination_right.png"/></a>';
        $nextlink = '<span class="next"><a href="#noclick">&gt;</a></span>';
    }
   
   
    echo '<div class="pagination">';
   
    $pagelinks[] = $prevlink;
   
    if ($currpage > 1) {
        $params['page'] = 1;
        $firstlink = html_writer::link(new moodle_url($baseurl, $params), 1);
       
        $pagelinks[] = $firstlink;
        if($currpage > 2) {
            $pagelinks[] = '...';
        }
    }
   
    $displaycount = 0;
    while ($displaycount <= $maxdisplay and $currpage <= $lastpage) {
        if ($page == $currpage) {
            $pagelinks[] = '<strong>'.$currpage.'</strong>';
        } else {
            $params['page'] = $currpage;
            $pagelink = html_writer::link(new moodle_url($baseurl, $params), $currpage);
            $pagelinks[] = $pagelink;
        }
       
        $displaycount++;
        $currpage++;
    }
   
    if ($currpage - 1 < $lastpage) {
        $params['page'] = $lastpage;
        $lastlink = html_writer::link(new moodle_url($baseurl, $params), $lastpage);
       
        if($currpage != $lastpage) {
            $pagelinks[] = '...';
        }
        $pagelinks[] = $lastlink;
    }
   
    $pagelinks[] = $nextlink;
   
   
    echo implode('&nbsp;', $pagelinks);
   
    echo '</div>';
}