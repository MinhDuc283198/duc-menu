The package contains 3 files:

- The main file is for vod based streams with no debug logging.
- The live file includes live streaming support with no debug logging.
- The debug file includes both vod and live streaming support with debug logging.
- The redirect file is for vod based streams that do 302 redirections like with Vimeo Akamai HLS.