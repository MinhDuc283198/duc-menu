<?php
require(dirname(dirname(dirname(__FILE__))) . '/../config.php');
//require_once($CFG->dirroot . '/local/book/lib.php');
//require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();
$id  = optional_param('id',0, PARAM_INT);
$credit_id  = optional_param('credit_id',0, PARAM_INT);
$employer_id = optional_param('employer_id', 0, PARAM_INT);
$credit_name = optional_param('credit_name', '', PARAM_TEXT);
$point = optional_param('point', 0, PARAM_INT);
$price = optional_param('price', 0, PARAM_FLOAT);
$price_format = number_format($price, 0, '', '.');
$image = optional_param('image', '', PARAM_TEXT);

//$goodstype = $DB->get_field('lmsdata_class', 'type', array('id' => $id));

//if ($goodstype == 2) {
//    $list = local_book_id($id);
//} else {
//    $list = local_course_class_id2($id);
//}
//$goodtitle = get_string("goodtitle$goodstype", "local_payment");

//$lang = current_language();
//if ($lang == 'ko') {
//    $title = 'title';
//    $lttitle = 'lttitle';
//} else {
//    $title = $lang . '_title';
//    $lttitle = $lang . '_lttitle';
//}
//$userdata = $DB->get_record('lmsdata_user', array('userid' => $USER->id));
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->navbar->add('결제');
// 개발전 임시 작업
//$list->price = 0;
echo $OUTPUT->header();
?>
<div class="layerpop">
    <?php ?>
    <div class="pop-title">
        <?php echo get_string("payment", "local_payment"); ?>
        <a href="#" class="pop-close">닫기</a>
    </div>
    <div class="pop-contents">
        <h5 class="pg-tit"><?php echo get_string("goodsinfo", "local_payment"); ?></h5>
        <div class="bg-box">
            <div class="row">
                <div class="col-4" style="margin: auto">
                    <img src="<?php echo $CFG->wwwroot . '/pluginfile.php/' . $image ?>" alt="<?php echo $credit_name ?>">
                </div>
                <div class="col-8" style="margin: auto">
                    <div class="crs-tit goodstitle"><?php echo get_string("pluginname", "local_credit"); ?>: <?php echo $credit_name ?></div>
                    <ul class="crs-txt">
                        <li>
                            <strong><?php echo get_string("points", "local_credit"); ?></strong><span><?php echo $point ?></span>
                        </li>
                    </ul>
                    <p class="price">
                        <em class="t-blue02 goodsprice"> <?php echo $price_format ?></em>VND
                    </p>
                </div>
            </div>
        </div>

        <?php if ($price != 0) { ?>
            <h5 class="pg-tit"><?php echo get_string('selectpaytype', 'local_payment'); ?></h5>
            <div class="bg-box">
                <p class="rd-rw"><input type="radio" name="paytype" class='paytype' id="rd01" value="momo"/><label
                            for="rd01"><?php echo get_string('paymenttype1', 'local_payment'); ?></label></p>
<!--                <p class="rd-rw"><input type="radio" name="paytype" class='paytype' id="rd02" value="domestic"/><label-->
<!--                            for="rd02">--><?php //echo get_string('paymenttype2', 'local_payment'); ?><!--</label></p>-->
                <p class="rd-rw">
                    <input type="radio" name="paytype" class='paytype'  id="rd02" value="vnpay" />
                    <label for="rd02"><?php echo get_string('paymenttype2', 'local_payment'); ?></label>
                </p>
                <!--<p class="rd-rw"><input type="radio" name="paytype" class='paytype'  id="rd03" value="international" /> <label for="rd03">International card (Visa, Master, Amex, JCB)</label></p>-->
                <div class="rd-rw">
                    <input type="radio" name="paytype" class='paytype' id="rd04" value="directdeposit"/> <label
                            for="rd04"><?php echo get_string('directdeposit', 'local_payment'); ?></label>
                    <div class="rd-info">
                        <ul class="dash-list">
                            <li><?php echo get_string('deposittext1', 'local_payment'); ?></li>
                            <li><?php echo get_string('deposittext2', 'local_payment'); ?></li>
                            <li><?php echo get_string('deposittext3', 'local_payment'); ?></li>
                            <li><?php echo get_string('deposittext6', 'local_payment'); ?></li>
                        </ul>
                        <p class="t-info"><?php echo get_string('deposittext4', 'local_payment', date("Y-m-d", strtotime("+5 days"))); ?></p>
<!--                        <p class="t-info">--><?php //echo get_string('deposittext5', 'local_payment'); ?><!--</p>-->
                    </div>
                </div>
            </div>
<!--            <h5 class="pg-tit">Coupon Code</h5>-->
<!--            <div class="bg-box">-->
<!--                <p class="rd-rw">-->
<!--                    <input style="width: 100%; border-radius: 6px" type="text" name="coupon" class='paytype' value=""/>-->
<!--                </p>-->
<!--            </div>-->
        <?php } ?>
    </div>
    <div class="btn-area text-center" id="btn_payment">
        <input type="button" value="<?php echo get_string("payment", "local_payment"); ?>" class="btns point w100"
               onclick="payment_progress();"/>
    </div>
    <script>
        // $("input[name=paytype][type=radio]").change(function(){
        //     const type_pay = $(this).val();
        //     if(type_pay =='directdeposit'){
        //         $('#btn_payment').hide();
        //     }else{
        //         $('#btn_payment').show();
        //     }
        // });
        //$(document).ready(function () {
        //    var phone = '<?php //echo $userdata->phone1 ? $userdata->phone1 : ''?>//';
        //    var zipcode = '<?php //echo $userdata->zipcode ? $userdata->zipcode : ''?>//';
        //    var address = '<?php //echo $userdata->address ? $userdata->address : '' ?>//';
        //    var appendtext = "<span class='t-blue t-small savetext'>ᆞ<?php //echo get_string('changeinfo', 'local_payment') ?>//.</span>";
        //    $('.uinfo').keyup(function () {
        //            if (phone == $('input[name=phone]').val() && address == $('input[name=address]').val() && zipcode == $('input[name=zipcode]').val()) {
        //                $('.savetext').remove();
        //            } else {
        //                if (!$('.dinfo').children('.savetext').length) {
        //                    $('.dinfo').append(appendtext);
        //                }
        //            }
        //        }
        //    );
        //});
        //
        //function sava_userinfo() {
        //    var phone = $('input[name=phone]').val();
        //    var zipcode = $('input[name=zipcode]').val();
        //    var address = $('input[name=address]').val();
        //    $.ajax({
        //        type: 'POST',
        //        url: '/local/payment/save_address.php',
        //        dataType: 'JSON',
        //        data: {phone: phone, zipcode: zipcode, address: address},
        //        success: function () {
        //            alert('<?php //echo get_string('savesuccess', 'local_payment') ?>//');
        //        },
        //        error: function (xhr, status, error) {
        //        }
        //    });
        //
        //}

        function payment_progress() {
            <?php if($price == 0) { ?>
            var paytype = 'free';
            <?php }else{ ?>
            var paytype = $("input[name=paytype]:checked").val();
            <?php } ?>
            // var coupon = $("input[name=coupon]").val();
            var form = document.createElement("form");
            var url = "/local/payment/init_payment.php";
            form.setAttribute("charset", "UTF-8");
            form.setAttribute("method", "Post");
            form.setAttribute("action", url);
            form.setAttribute("target", "payform");
            var input1 = document.createElement("input");
            input1.setAttribute("type", "hidden");
            //input1.setAttribute("name", "id");
            //input1.setAttribute("value", '<?php //echo $id ?>//');
            input1.setAttribute("name", "credit_id");
            input1.setAttribute("value", '<?php echo $credit_id ?>');
            form.appendChild(input1);
            var input2 = document.createElement("input");
            input2.setAttribute("type", "hidden");
            input2.setAttribute("name", "employer_id");
            input2.setAttribute("value", '<?php echo $employer_id ?>');
            form.appendChild(input2);
            // if (coupon) {
            //     var couponInput = document.createElement("input");
            //     couponInput.setAttribute("name", "coupon");
            //     couponInput.setAttribute("value", coupon)
            //     couponInput.setAttribute("type", "hidden");
            //     form.appendChild(couponInput);
            // }

            if (paytype) {
                var input4 = document.createElement("input");
                input4.setAttribute("type", "hidden");
                input4.setAttribute("name", "paytype");
                input4.setAttribute("value", paytype);
                form.appendChild(input4);
                document.body.appendChild(form);
            } else {
                alert("<?php echo get_string('enterinfo', 'local_payment', get_string('selectpaytype', 'local_payment'))?> ")
                return false;
            }
            window.open("", "payform", "width=759 ,height=847,scrollorbars=yes,resizable=no");
            form.submit();
        }
    </script>
</div>


<?php
echo $OUTPUT->footer();
?>


