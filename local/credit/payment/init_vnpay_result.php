<?php

require('../../../config.php');
require_once("vnpayphp/config.php");

$vnp_SecureHash = $_GET['vnp_SecureHash'];

$inputData = array();

foreach ($_GET as $key => $value) {
    if (substr($key, 0, 4) == "vnp_") {
        $inputData[$key] = $value;
    }
}

unset($inputData['vnp_SecureHashType']);
unset($inputData['vnp_SecureHash']);

ksort($inputData);

$i = 0;
$hashData = "";

foreach ($inputData as $key => $value) {

    if ($i == 1) {
        $hashData = $hashData . '&' . $key . "=" . $value;
    } else {
        $hashData = $hashData . $key . "=" . $value;
        $i = 1;
    }
}

//$secureHash = md5($vnp_HashSecret . $hashData);
$secureHash = hash('sha256',$vnp_HashSecret . $hashData);

if ($secureHash != $vnp_SecureHash) {

    echo "Chu ky khong hop le";
    die;
}

if ($_GET['vnp_ResponseCode'] != '00') {

    echo "GD Khong thanh cong";
    die;
}

$extradata = explode('/', $_GET['vnp_OrderInfo']);

$lang = current_language();
$otitle = $lang == 'ko' ? "title" : $lang . '_title';

$classname = $DB->get_field("lmsdata_class","$otitle",array('id'=>$extradata[0]));

$paytype = 'Domestic card';

$_GET["errorCode"] = $_GET["resultCd"] ==  $_GET["vnp_ResponseCode"];
$date = strtotime($_GET["vnp_PayDate"]);

$transfer["orderId"] = $_GET["vnp_TxnRef"];
$transfer["message"] = $_GET["success"];
$transfer["orderInfo"] = $classname;
$transfer["amount"] = $_GET["vnp_Amount"] / 100;
$transfer["responseTime"] = date("Y-m-d H:i:s",$date);
$transfer["errorCode"] = "";
$transfer["extraData"] = $_GET['vnp_OrderInfo'];

$transfer["phone"] = $extradata[1];
$transfer["zipcode"] = $extradata[2];
$transfer["address"] = $extradata[3];
$transfer["selectpaytype"] = $paytype;
$transfer["localMessage"] = $_GET["vnp_OrderInfo"];
$transfer["orderType"] = $paytype;
//Checksum
$resultjson = json_encode($transfer);
// 결제 배송 테이블 insert

$id = $extradata[0];
$employer_id = $extradata[2];
$query = "SELECT 
            cm.id,
            cm.credit_name,
            cm.points,
            cm.price,
            cm.expired_date
            FROM {vi_credit_menu} cm
            WHERE cm.id=$id
            ";
$credit_menu = $DB->get_record_sql($query);
$sql = "SELECT 
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$employer_id
            ";
$credit = $DB->get_record_sql($sql);
$member_credit_id = $DB->get_field('vi_credits', 'id', array('employer_id' => $employer_id));
if ($_GET['vnp_ResponseCode'] == '00') {
    if (!$credit) {
        $data = new stdClass();
        $data->employer_id = $employer_id;
        $data->credit_point = $credit_menu->points;
        $data->expire_date = $credit_menu->expired_date;
        $data->timecreated = time();
        $DB->insert_record('vi_credits', $data);

        $data_log = new stdClass();
        $data_log->member_credit_id = $member_credit_id;
        $data_log->transaction_code = 1001;
        $data_log->pg_code = 3002;
        $data_log->point = $credit_menu->points;
        $data_log->price = $credit_menu->price;
        $data_log->timelogged = time();
        $DB->insert_record('vi_credits_logs', $data_log);
    } else {
        $credit->credit_point += $credit_menu->points;
        $credit->timemodified = time();
        $DB->update_record('vi_credits', $credit);

        $data_log = new stdClass();
        $data_log->member_credit_id = $member_credit_id;
        $data_log->transaction_code = 1001;
        $data_log->pg_code = 3002;
        $data_log->point = $credit_menu->points;
        $data_log->price = $credit_menu->price;
        $data_log->timelogged = time();
        $DB->insert_record('vi_credits_logs', $data_log);
    }
    echo "giao dich thanh cong";
}


echo "<script>var raw = '$resultjson'; opener.parent.pay_resultpop(raw); window.close();</script>";

