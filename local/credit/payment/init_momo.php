<?php

header('Content-type: text/html; charset=utf-8');
include "momo/common/helper.php";

$returnUrl = $CFG->wwwroot . "/local/credit/payment/init_momo_result.php";
$notifyurl = $CFG->wwwroot . "/local/payment/init_momo_result.php";
//momo payment key
$partnerCode = $moopaykey["partnerCode"]->value;
$accessKey = $moopaykey["accessKey"]->value;
$serectkey = $moopaykey["secretKey"]->value;

$orderId = $class->code . time();
$lang = current_language();
$otitle = $lang == 'ko' ? "title" : $lang . '_title';
//$orderInfo = $class->$otitle;
$orderInfo = $class->credit_name;

$amount = $class->price;
$endpoint = "https://payment.momo.vn/gw_payment/transactionProcessor";
$extraData = $id . '/' . $phone . '/' . $zipcode . '/' . $address . '/' . $paytype.'/'.$employer_id;
$requestType = "captureMoMoWallet";
$requestId = time() . "";
//before sign HMAC SHA256 signature
$rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&returnUrl=" . $returnUrl . "&notifyUrl=" . $notifyurl . "&extraData=" . $extraData;

$signature = hash_hmac("sha256", $rawHash, $serectkey);
$data = array('partnerCode' => $partnerCode,
    'accessKey' => $accessKey,
    'requestId' => $requestId,
    'amount' => $amount,
    'orderId' => $orderId,
    'orderInfo' => $orderInfo,
    'orderInfo2' => $orderInfo,
    'orderInfo3' => $orderInfo,
    'returnUrl' => $returnUrl,
    'notifyUrl' => $notifyurl,
    'extraData' => $extraData,
    'requestType' => $requestType,
    'signature' => $signature);
$result = execPostRequest($endpoint, json_encode($data));
$jsonResult = json_decode($result, true);  // decode json


header('Location: ' . $jsonResult['payUrl']);

