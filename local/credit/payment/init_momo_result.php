<?php

header('Content-type: text/html; charset=utf-8');
require('../../../config.php');
include "momo/common/helper.php";
global $CFG ;
$notifyurl = $CFG->wwwroot . "";
$secretKey = $moopaykey["accessKey"]->value;

if (!empty($_GET)) {
    $orderId = $_GET["orderId"];
    $localMessage = $_GET["localMessage"];
    $message = $_GET["message"];
    $transId = $_GET["transId"];
    $orderInfo = $_GET["orderInfo"];
    $amount = $_GET["amount"];
    $errorCode = $_GET["errorCode"];
    $responseTime = $_GET["responseTime"];
    $requestId = $_GET["requestId"];
    $extraData = $_GET["extraData"];
    $payType = $_GET["payType"];
    $orderType = $_GET["orderType"];
    $extraData = $_GET["extraData"];
    $m2signature = $_GET["signature"]; //MoMo signature
    $extradata = explode('/', $_GET['extraData']);

    $transfer["orderId"] = $_GET["orderId"];
    $transfer["message"] = $_GET["message"];
    $transfer["orderInfo"] = $_GET["orderInfo"];
    $transfer["amount"] = $_GET["amount"];
    $transfer["responseTime"] = $_GET["responseTime"];
    $transfer["errorCode"] = $_GET["errorCode"];
    $transfer["extraData"] = $_GET["extraData"];
    $transfer["phone"] = $extradata[1];
    $transfer["zipcode"] = $extradata[2];
    $transfer["address"] = $extradata[3];
    $transfer["selectpaytype"] = $extradata[4];
    $transfer["localMessage"] = $_GET["localMessage"];
    $transfer["orderType"] = $_GET["orderType"];
    //Checksum
    $rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo .
        "&orderType=" . $orderType . "&transId=" . $transId . "&message=" . $message . "&localMessage=" . $localMessage . "&responseTime=" . $responseTime . "&errorCode=" . $errorCode .
        "&payType=" . $payType . "&extraData=" . $extraData;

    $partnerSignature = hash_hmac("sha256", $rawHash, $secretKey);
    $resultjson = json_encode($transfer);


    // 결제 배송 테이블 insert
    $id = $extradata[0];
    $employer_id = $extradata[5];
    $query="SELECT 
            cm.id,
            cm.credit_name,
            cm.points,
            cm.price,
            cm.banner_image_url,
            cm.expired_date
            FROM {vi_credit_menu} cm
            WHERE cm.id=$id
            ";
    $credit_menu=$DB->get_record_sql($query);
    $sql="SELECT 
            *
            FROM {vi_credits} cre
            WHERE cre.employer_id=$employer_id
            ";
    $credit=$DB->get_record_sql($sql);
    if ($_GET["errorCode"] == 0) {
        if(!$credit)
        {
            $data = new stdClass();
            $data->employer_id=$employer_id;
            $data->credit_point=$credit_menu->points;
            $data->expire_date=$credit_menu->expired_date;
            $data->timecreated = time();
            $DB->insert_record('vi_credits',$data);


        }
        else{
            $credit->credit_point+=$credit_menu->points;
            $credit->timemodified=time();
            $DB->update_record('vi_credits',$credit);
        }

        $data = new stdClass();
        $data->member_credit_id=$credit->id;
        $data->transaction_code='1001';
        $data->pg_code='3001';
        $data->price=$credit_menu->price;
        $data->point=$credit_menu->points;
        $data->timelogged = time();
        $DB->insert_record('vi_credits_logs',$data);


//        $data2 = new stdClass();
//        $data2->credit_menu_id=$credit_menu->id;
//        $data2->task_code='3001';
//        $data2->banner_image_url=$credit_menu->banner_image_url;
//        $data2->points=$credit_menu->points;
//        $data2->expired_date=$credit_menu->expired_date;
//        $data2->timelogged = time();
//        $DB->insert_record('vi_credit_menu_logs',$data2);

    }




    echo "<script>var raw = '$resultjson'; opener.parent.pay_resultpop(raw); window.close();</script>";
//    if ($m2signature == $partnerSignature) {
//        if ($errorCode == '0') {
//            $result = '<div class="alert alert-success"><strong>Payment status: </strong>Success</div>';
//        } else {
//            $result = '<div class="alert alert-danger"><strong>Payment status: </strong>' . $message . '/' . $localMessage . '</div>';
//        }
//    } else {
//        $result = '<div class="alert alert-danger">This transaction could be hacked, please check your signature and returned signature</div>';
//    }
}