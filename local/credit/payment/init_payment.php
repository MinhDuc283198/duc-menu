<?php
require('../../../config.php');

$id  = optional_param('id',0, PARAM_INT);
$employer_id  = optional_param('employer_id',0, PARAM_INT);
$paytype = optional_param('paytype', '', PARAM_RAW);
$credit = $DB->get_record("vi_credit_menu",array('id'=>$id));
$class = $DB->get_record("vi_credit_menu",array('id'=>$id));

$extraData = $id.'/'.$paytype.'/'.$employer_id;

// hard code discount amnote
$coupon = optional_param('coupon', '', PARAM_RAW);

if ($coupon == $CFG->amnoteCoupon) {
    $credit->price = $credit->price - $credit->price * $CFG->amnoteDiscountPercent;
    $extraData = $id.'/'.$paytype.'/'.$employer_id.'/'. $CFG->amnoteCoupon;
}
// end  hard code discount amnote

include 'init_'.$paytype.'.php';


?>
