<?php

/*
 * IPN URL: Ghi nhận kết quả thanh toán từ VNPAY
 * Các bước thực hiện:
 * Kiểm tra checksum
 * Tìm giao dịch trong database
 * Kiểm tra tình trạng của giao dịch trước khi cập nhật
 * Cập nhật kết quả vào Database
 * Trả kết quả ghi nhận lại cho VNPAY
 */

require_once("../../config.php");
require_once("vnpayphp/config.php");

$inputData = array();
$returnData = array();
$data = $_REQUEST;
foreach ($data as $key => $value) {
    if (substr($key, 0, 4) == "vnp_") {
        $inputData[$key] = $value;
    }
}

$vnp_SecureHash = $inputData['vnp_SecureHash'];
unset($inputData['vnp_SecureHashType']);
unset($inputData['vnp_SecureHash']);
ksort($inputData);
$i = 0;
$hashData = "";
foreach ($inputData as $key => $value) {
    if ($i == 1) {
        $hashData = $hashData . '&' . $key . "=" . $value;
    } else {
        $hashData = $hashData . $key . "=" . $value;
        $i = 1;
    }
}
$vnpTranId = $inputData['vnp_TransactionNo']; //Mã giao dịch tại VNPAY
$vnp_BankCode = $inputData['vnp_BankCode']; //Ngân hàng thanh toán
//$secureHash = md5($vnp_HashSecret . $hashData);
$secureHash = hash('sha256',$vnp_HashSecret . $hashData);
$orderId = $inputData['vnp_TxnRef'];
$paytype = $inputData['vnp_CardType'];

// Payment status
$Status = 0; // pending

$paycode = $inputData['vnp_TxnRef'];
// get data payment from paycode

$lmsdata_payment = $DB->get_record("lmsdata_payment", ["paycode" => $paycode]);

$vnp_Amount = $inputData['vnp_Amount'];
$vnp_Amount = (int)$vnp_Amount / 100;
$extradata = explode('/', $inputData['vnp_OrderInfo']);

try {
    //Check Orderid
    //Kiểm tra checksum của dữ liệu
    if ($secureHash == $vnp_SecureHash) {
        if ($lmsdata_payment) {
            if ( $vnp_Amount == $DB->get_field("lmsdata_class", 'price', array("id" => $extradata[0]))) {
                if ($lmsdata_payment->status == 0) {

                    $USER->id = $extradata[1];

                    $datatype = $DB->get_field_sql("select type from {lmsdata_class} where id = ? ", array($extradata[0]));

                    if ($transfer["errorCode"] == 0) {

                        if ($datatype != 2) {

                            require_once $CFG->dirroot . '/local/lmsdata/lib.php';
                            require_once $CFG->dirroot . '/local/course/lib.php';

                            $courseid = $DB->get_field("lmsdata_class", 'courseid', array("id" => $extradata[0]));
                            $enrolresult = local_course_whether_enrol($courseid, $USER->id, 'student');

                            if ($enrolresult == false) {

                                $result = local_lmsdata_enrol_user($courseid, $USER->id, 'student');

                                $returnvalues = new stdClass();
                                $returnvalues->status = $result;

                                if (!$result) {
                                    local_lmsdata_enrol_edit_user($courseid, $USER->id, 'student');
                                }
                            }
                        }

                        if ($datatype != 1) {

                            $lmsdata2->payid = $lmsdata_payment->id;
                            $lmsdata2->bookid = $extradata[0];
                            $lmsdata2->address = $extradata[2] . '-' . $extradata[3];
                            $lmsdata2->cell = $extradata[1];
                            $lmsdata2->timecreated = time();
                            $lmsdata2->timemodified = time();

                            $DB->insert_record('lmsdata_productdelivery', $lmsdata2);
                        }

                        // update payment status
                        $lmsdata_payment->status = 1;
                        $DB->update_record('lmsdata_payment', $lmsdata_payment);
                    }

                    $returnData['RspCode'] = '00';
                    $returnData['Message'] = 'Confirm Success';
                } else {

                    $returnData['RspCode'] = '02';
                    $returnData['Message'] = 'Order already confirmed';
                }
            } else {
                $returnData['RspCode'] = '04';
                $returnData['Message'] = 'Invalid Amount';
            }
        } else {

            $returnData['RspCode'] = '01';
            $returnData['Message'] = 'Order not found';
        }
    } else {

        $returnData['RspCode'] = '97';
        $returnData['Message'] = 'Chu ky khong hop le';
    }
} catch (Exception $e) {

    $returnData['RspCode'] = '99';
    $returnData['Message'] = "unknow error";
}

echo json_encode($returnData);