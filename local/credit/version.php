<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2021080500;
$plugin->requires  = 2016051900;
$plugin->component = 'local_credit';