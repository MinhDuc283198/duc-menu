<?php
// Standard GPL and phpdocs
namespace local_credit\output;

use renderable;
use renderer_base;
use templatable;
use stdClass;

class index_page implements renderable, templatable
{
    /** @var string $sometext Some text to show how to pass data to a template. */
    public function __construct()
    {

    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output)
    {
        return array(
            'credits' => $this->get_credits(),
            'creditsmobile' => $this->get_credits_mobile(),
            'pagenumber' => $this->page_number(),
            'quantitypage' => $this->quantity_pages(),
            'url' => $this->get_link(),
//            'login'=>$this->mkjobs_require_login(),
            'id_employer' => $this->mkcom_get_companyID(),
            'id_employee' => $this->id_employee(),
            'creditstatus' => $this->get_credit_status(),
            'quantitycredit' => $this->quantity_credit(),
            'pagenumbermobile' => $this->page_number_mobile(),
        );
    }

//    Get list of credits in DB to display on Credit page PC (3 credits / page)
    function get_credits()
    {
        global $DB;

        $credits_info = array();
        $credits = $DB->get_records('vi_credit_menu');
        $count_credit = 0;
        foreach ($credits as $credit) {
            $credit->price_format = number_format($credit->price, 0, '', '.');
            array_push($credits_info, $credit);
            if ($count_credit % 3 == 0) {
                if ($count_credit == 0) {
                    $credit->pagination_begin_active = true;
                } else {
                    $credit->pagination_begin = true;
                }
            }
            $count_credit++;
            if ($count_credit % 3 == 0 || $count_credit == count($credits)) {
                $credit->pagination_end = true;
            }
        }
        return $credits_info;
    }

//    Get list of credits in DB to display on Credit page PC moblie (1 credit / page)
    function get_credits_mobile()
    {
        global $DB;

        $credits_info = array();
        $credits = $DB->get_records('vi_credit_menu');
        $count_credit = 0;
        foreach ($credits as $credit) {
            $credit->price_format = number_format($credit->price, 0, '', '.');
            array_push($credits_info, $credit);
            if ($count_credit == 0) {
                $credit->pagination_begin_active = true;
            } else {
                $credit->pagination_begin = true;
            }
            $count_credit++;
        }
        return $credits_info;
    }

//    Get data-slide-to and class for carousel-indicators on Credit page PC (3 credits / page)
    function page_number()
    {
        $list_page = array();
        $page_number = ceil(count($this->get_credits()) / 3);
        for ($i = 0; $i < $page_number; $i++) {
            if ($i == 0) {
                array_push($list_page, [
                    'data_slide' => $i,
                    'class' => 'active',
                ]);
            } else {
                array_push($list_page, [
                    'data_slide' => $i,
                ]);
            }
        }
        return $list_page;
    }

//    Get data-slide-to and class for carousel-indicators on Credit page mobile (1 credit / page)
    function page_number_mobile()
    {
        $list_page = array();
        $page_number = count($this->get_credits());
        for ($i = 0; $i < $page_number; $i++) {
            if ($i == 0) {
                array_push($list_page, [
                    'data_slide' => $i,
                    'class' => 'active',
                ]);
            } else {
                array_push($list_page, [
                    'data_slide' => $i,
                ]);
            }
        }
        return $list_page;
    }

//    Get total number of Credit pages
    function quantity_pages()
    {
        return count($this->page_number());
    }

//    Get url for a tag in menu on the left of Credit page
    function get_link()
    {
        global $CFG, $VISANG;
        return array(
            'image' => $CFG->wwwroot . '/pluginfile.php/',
            'counseling' => $VISANG->wwwroot . '/my/qna/index.php?type=13',
            'editpersoninfo' => $VISANG->wwwroot . '/my/edit.php',
            'creditinfo' => '#',
        );
    }

//    Require employer login before go to Credit page
    function mkjobs_require_login()
    {
        require_once(dirname(__FILE__) . '/../../../job/lib.php');
        global $CFG, $PAGE, $SESSION, $VISANG;
        if (!isloggedin()) {
            $SESSION->wantsurl = $PAGE->url;
            redirect($VISANG->wwwroot . '/employer/login.php');
        }

    }

//    Get current user ID
    function mkcom_get_userID()
    {
        global $USER;
        foreach ($USER as $id) {
            return $id;
        }
    }

//    Get current company ID
    function mkcom_get_companyID()
    {


        require_once(dirname(__FILE__) . '/../../../job/lib.php');

        global $DB, $VISANG, $USER;
        $userID = $this->mkcom_get_userID();

        $sql = "SELECT ad.employer_id as val
    FROM {vi_admin_employers} ad
    WHERE ad.user_id = :id";
        $id = $DB->get_record_sql($sql, array('id' => $userID));
        if ($id != null) {
//         if(!mkcom_company_completed_info($id->val)) redirect($VISANG->wwwroot . '/employer/info.php?step=1');
            if (!mkcom_company_denied($id->val)) {
                $authsequence = get_enabled_auth_plugins(); // auths, in sequence
                foreach ($authsequence as $authname) {
                    $authplugin = get_auth_plugin($authname);
                    $authplugin->logoutpage_hook();
                }
                require_logout();
                redirect($VISANG->wwwroot . '/employer/denied.php');
            }
            if (!mkcom_company_comfirmed($id->val)) {
                $authsequence = get_enabled_auth_plugins(); // auths, in sequence
                foreach ($authsequence as $authname) {
                    $authplugin = get_auth_plugin($authname);
                    $authplugin->logoutpage_hook();
                }
                require_logout();
                redirect($VISANG->wwwroot . '/employer/waiting-confirm.php');
            }
            return $id->val;
        }
        redirect($VISANG->wwwroot . '/employer/join.php?step=1');
    }

//    Get ID of current user
    public function id_employee()
    {
        global $USER;
        return $USER->id;
    }

//    Get list of credit status in 'vi_credits' table
    function get_credit_status()
    {
        global $DB;

        $sql = "SELECT cre.*, emp.company_name FROM {vi_credits} cre JOIN {vi_employers} emp ON cre.employer_id = emp.id WHERE cre.employer_id = :id";
        $credit_status = $DB->get_records_sql($sql, array('id' => $this->mkcom_get_companyID()));
        $list_status = array();
        foreach ($credit_status as $status) {
            $status->timemodified_long = userdate($status->timemodified, get_string('strftimedatetimeshort'));
            $status->expire_date_long = userdate($status->expire_date, get_string('strftimedatetimeshort'));
            array_push($list_status, $status);
        }
        return $list_status;
    }

//    Get quantity of credits
    function quantity_credit()
    {
        return count($this->get_credits());
    }
}