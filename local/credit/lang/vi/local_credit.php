<?php



$string['pluginname'] = 'Tín dụng';
$string['credit-status-title'] = 'Trạng thái tín dụng của tôi';
$string['credit-log-title'] = 'Nhật ký giao dịch của nhà tuyển dụng';
$string['credit-purchase-title'] = 'Mua điểm tín dụng';
$string['credit-description-title'] = 'Chọn một tín dụng sau (Tín dụng của bạn sẽ được sử dụng cho đăng tuyển dụng, xem và tải xuống CV)';
$string['credit-info-link'] = 'Thông tin tín dụng';
$string['counseling'] = 'Tư vấn 1-1';
$string['edit-person-info'] = 'Chỉnh sửa thông tin cá nhân';
$string['employer'] = 'Nhà tuyển dụng';
$string['credit-points'] = 'Điểm tín dụng';
$string['last-modified-date'] = 'Ngày sửa đổi cuối';
$string['expired-date'] = 'Ngày hết hạn';
$string['details'] = 'Chi tiết';
$string['transaction-type'] = 'Loại giao dịch';
$string['redraw-type'] = 'Giao dịch';
$string['payment-gateway'] = 'Cổng thanh toán';
$string['points'] = 'Điểm';
$string['price'] = 'Giá';
$string['transaction-date'] = 'Ngày Giao dịch';
$string['view-details'] = 'Xem chi tiết';
$string['point-details'] = 'điểm';
$string['buy-now'] = 'MUA NGAY';
$string['not-enough-point'] = 'Bạn không đủ điểm vui lòng mua thêm điểm';
$string['noti_post_job1'] = 'các khoản tín dụng hiện tại của bạn vẫn còn';
$string['noti_post_job2'] = 'điểm khi bạn đăng tuyển dụng điểm tín dụng của bạn sẽ giảm {$a}điểm ';