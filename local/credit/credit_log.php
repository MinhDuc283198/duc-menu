<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
global $USER, $DB;

$id = $_POST["id"];

$lang = current_language();
if ($lang == 'vi') {
    $code_name_field = 'name_vi';
} elseif ($lang == 'ko') {
    $code_name_field = 'name_ko';
} else {
    $code_name_field = 'name_en';
}

$credit_log = $DB->get_records('vi_credits_logs', array('member_credit_id' => $id), 'timelogged DESC');
$list_log = array();
foreach ($credit_log as $log) {
    $log->timelogged_long = userdate($log->timelogged, get_string('strftimedatetimeshort'));
    $log->price_format = number_format($log->price, 0, '', '.');
    $log->code_name_transaction = $DB->get_field('vi_common_code', $code_name_field, array('code' => $log->transaction_code));
    $log->code_name_withdraw = $DB->get_field('vi_common_code', $code_name_field, array('code' => $log->withdraw_code));
    $log->code_name_payment = $DB->get_field('vi_common_code', $code_name_field, array('code' => $log->pg_code));
    array_push($list_log, $log);
}
echo json_encode($list_log);