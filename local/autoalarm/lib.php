<?php

require_once($CFG->dirroot . '/chamktu/lib/lib.php');
require_once($CFG->dirroot . '/local/lmsdata/lib.php');
require_once($CFG->dirroot . '/local/mypage/lib.php');

function auto_alarm() {
    //개선과정추가 마감 일주일전, 마감일 알람
    better_auto_alarm();
    //개선콘텐츠마감 일주일전, 마감일 알람
    add_auto_alarm();
    //개선과정추가 시작일 알람
    right_add_auto_alarm();
}

function add_auto_alarm() {
    global $DB;

    $now = strtotime("midnight", time());

    $sql = "SELECT lc.*,ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, mc.id mcid, mc.fullname, 
                                   mc2.category, lco.coursename, lco.classobject lcoclassobject, lco.classtype lcoclasstype, lco.yeartype locyeartype   ";
    $sql .= " FROM {course} mc
            JOIN {lmsdata_class} lc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category 
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            where learningend >= :learningend and learningstart <= :learningstart  and ca2.idnumber = 'required_course'";
    //교육기간 중인거 검색
    $contents = $DB->get_records_sql($sql, array("learningend" => $now, "learningstart" => $now));

    if (!empty($contents)) {
        foreach ($contents as $content) {
            $course_params = array();
            $course_where_sql = " and ca.idnumber=:ca1idnumber";
            $course_params["ca1idnumber"] = $content->ca1idnumber;

            //연차별과정
            if ($content->ca1idnumber == "year_course") {
                $course_where_sql .= " and lco.yeartype = :locyeartype";
                $course_params["locyeartype"] = $content->locyeartype;
            }

            $course_sql = "SELECT lc.*,ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, mc.id mcid, mc.fullname, 
                                   mc2.category, lco.coursename, lco.classobject lcoclassobject, lco.classtype lcoclasstype, lco.yeartype locyeartype  ";

            $course_sql .= " FROM {course} mc
                            JOIN {lmsdata_class} lc ON lc.courseid = mc.id
                            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                            JOIN {course} mc2 ON lco.courseid = mc2.id
                            JOIN {course_categories} ca ON ca.id = mc2.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            WHERE ca2.idnumber = 'required_course' and lc.id != :id";
            $course_params["id"] = $content->id;
            // 신임별과정, 연차별과정(1년~10년) 한묶음으로(총 11개묶음) 그전에 기수가 있었는지 검색
            $courses = $DB->get_records_sql($course_sql . $course_where_sql, $course_params);

            if (!empty($courses)) {
                $getcompleteArray = array();
                foreach ($courses as $course) {
                    //과거의 묶음 이수완료
                    $getcompletesql = "SELECT ra.userid 
                                    FROM {role_assignments} ra 
                                    JOIN {role} ro ON ra.roleid = ro.id 
                                    JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel2 
                                    JOIN {course_completions} cc ON cc.course = :courseid1 AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                    WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                    group by ra.userid";
                    $getcomplete = $DB->get_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => CONTEXT_COURSE, 'archetype' => 'student'));
                    $getcompleteArray = array_merge($getcompleteArray, $getcomplete);
                }
                if (!empty($getcompleteArray)) {
                    //새로생성된 강의 수강자
                    $getstudentsql = "SELECT ra.userid  
                                   FROM {role_assignments} ra 
                                   JOIN {role} ro ON ra.roleid = ro.id 
                                   JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel1 
                                   JOIN {lmsdata_course_applications} lca ON lca.courseid = :courseid1 AND status = 'apply' 
                                   WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                   group by ra.userid";
                    $getstudent = $DB->get_records_sql($getstudentsql, array('courseid1' => $content->mcid, 'courseid2' => $content->mcid, 'contextlevel1' => CONTEXT_COURSE, 'archetype' => 'student'));

                    //새로생성된 강의 수강자중 이수자
                    $getcompletesql2 = "SELECT ra.userid 
                                   FROM {role_assignments} ra 
                                   JOIN {role} ro ON ra.roleid = ro.id 
                                   JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel2 
                                   JOIN {course_completions} cc ON cc.course = :courseid1 AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                   WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                   group by ra.userid";
                    $getcomplete2 = $DB->get_records_sql($getcompletesql2, array('courseid1' => $content->mcid, 'courseid2' => $content->mcid, 'contextlevel2' => CONTEXT_COURSE, 'archetype' => 'student'));
                    $student_array = array();
                    $complete_array = array();
                    $nocomplete_array = array();
                    //새로생성된 강의 수강자
                    if (!empty($getstudent)) {
                        foreach ($getstudent as $student) {
                            array_push($student_array, $student->userid);
                        }
                    }
                    //새로생성된 강의 수강자중 이수자
                    if (!empty($getcomplete2)) {
                        foreach ($getcomplete2 as $complete2) {
                            array_push($complete_array, $complete2->userid);
                        }
                    }
                    //새로생성된 강의 수강자중 미이수자 = 알람보내야할 대상자
                    $nocomplete_array = array_diff($student_array, $complete_array);

                    if (!empty($nocomplete_array)) {
                        //과거 이수완료
                        $completion = array();
                        //새로생성된 강의 수강자중 미이수자 = 알람보내야할 대상자
                        $student = array();
                        $scdiff = array();
                        $final = array();
                        foreach ($getcompleteArray as $gc) {
                            $completion[] = $gc->userid;
                        }
                        foreach ($nocomplete_array as $gs) {
                            $student [] = $gs;
                        }
                        //echo print_object($aa);
                        //echo print_object($bb);
                        $scdiff = array_diff($student, $completion);
                        $final = array_diff($student, $scdiff);

                        if (!empty($final)) {
                            foreach ($final as $student) {
                                $udtuser = new stdclass();
                                $udtuser->id = $student;
                                $depth1 = $content->ca1name;
                                $depth2 = $content->ca2name;

                                $learningend_week = strtotime("midnight", $content->learningend) - 604800;
                                if ($learningend_week == $now) {
                                    $subject = $content->fullname . "_추가 콘텐츠 수강 2차 안내";
                                    $massage = $depth2 . "의 " . $depth1 . " 추가 콘텐츠 수강기간이 1주일 뒤 종료됩니다.현재 추가 콘텐츠 학습을 완료하지 않으셨습니다. 학습 미수료 시 포인트 부족으로 멘토 자격기준에 미달되오니 기간 내 학습을 완료해주시기 바랍니다.";
                                    reday_to_send_email($udtuser, $subject, $massage);
                                    reday_to_send_push($udtuser, $subject, $massage);
                                }

                                $learningend_day = strtotime("midnight", $content->learningend);
                                if ($learningend_day == $now) {
                                    $subject = $content->fullname . "_추가 콘텐츠 수강 3차 안내";
                                    $massage = $depth2 . "의 " . $depth1 . " 추가 콘텐츠 수강기간이 금일 종료됩니다.현재 추가 콘텐츠 학습을 완료하지 않으셨습니다.학습 미수료 시 포인트 부족으로 멘토 자격기준에 미달되오니 기간 내 학습을 완료해주시기 바랍니다.";
                                    reday_to_send_email($udtuser, $subject, $massage);
                                    reday_to_send_push($udtuser, $subject, $massage);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function right_add_auto_alarm() {
    global $DB;
    $now = strtotime("midnight", time());
    $sql = "SELECT lc.*,ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, mc.id mcid, mc.fullname, 
                                   mc2.category, lco.coursename, lco.classobject lcoclassobject, lco.classtype lcoclasstype, lco.yeartype locyeartype  ";
    $sql .= " FROM {course} mc
            JOIN {lmsdata_class} lc ON lc.courseid = mc.id
            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
            JOIN {course} mc2 ON lco.courseid = mc2.id
            JOIN {course_categories} ca ON ca.id = mc2.category 
            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
            where lc.learningstart = :learningstart and ca2.idnumber = 'required_course'";
    //교육기간이 오늘 시작하는 기수가 있는지
    $contents = $DB->get_records_sql($sql, array("learningstart" => $now));
    //있다면
    if (!empty($contents)) {
        foreach ($contents as $content) {
            $course_params = array();
            $course_where_sql = " and ca.idnumber=:ca1idnumber";
            $course_params["ca1idnumber"] = $content->ca1idnumber;

            //연차별과정
            if ($content->ca1idnumber == "year_course") {
                $course_where_sql .= " and lco.yeartype = :locyeartype";
                $course_params["locyeartype"] = $content->locyeartype;
            }

            $course_sql = "SELECT lc.*,ca.name ca1name, ca2.name ca2name, ca.idnumber ca1idnumber, ca2.idnumber ca2idnumber, mc.id mcid, mc.fullname, 
                                   mc2.category, lco.coursename, lco.classobject lcoclassobject, lco.classtype lcoclasstype, lco.yeartype locyeartype  ";

            $course_sql .= " FROM {course} mc
                            JOIN {lmsdata_class} lc ON lc.courseid = mc.id
                            JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                            JOIN {course} mc2 ON lco.courseid = mc2.id
                            JOIN {course_categories} ca ON ca.id = mc2.category 
                            left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                            WHERE ca2.idnumber = 'required_course' and lc.id != :id";


            $course_params["id"] = $content->id;
            // 신임별과정, 연차별과정(1년~10년) 한묶음으로(총 11개묶음) 그전에 기수가 있었는지 검색
            $courses = $DB->get_records_sql($course_sql . $course_where_sql, $course_params);

            if (!empty($courses)) {
                $getcompleteArray = array();
                foreach ($courses as $course) {
                    //과거의 묶음 이수완료
                    $getcompletesql = "SELECT ra.userid 
                                    FROM {role_assignments} ra 
                                    JOIN {role} ro ON ra.roleid = ro.id 
                                    JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel2 
                                    JOIN {course_completions} cc ON cc.course = :courseid1 AND cc.timecompleted  IS NOT NULL AND ra.userid = cc.userid 
                                    WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                    group by ra.userid";
                    $getcomplete = $DB->get_records_sql($getcompletesql, array('courseid1' => $course->mcid, 'courseid2' => $course->mcid, 'contextlevel2' => CONTEXT_COURSE, 'archetype' => 'student'));
                    $getcompleteArray = array_merge($getcompleteArray, $getcomplete);
                }
                if (!empty($getcompleteArray)) {
                    //새로생성된 강의 수강자
                    $getstudentsql = "SELECT ra.userid  
                                    FROM {role_assignments} ra 
                                    JOIN {role} ro ON ra.roleid = ro.id 
                                    JOIN {context} ctx ON ra.contextid = ctx.id AND contextlevel = :contextlevel1 
                                    JOIN {lmsdata_course_applications} lca ON lca.courseid = :courseid1 AND status = 'apply' 
                                    WHERE ctx.instanceid = :courseid2 AND ro.archetype = :archetype 
                                    group by ra.userid";

                    $getstudent = $DB->get_records_sql($getstudentsql, array('courseid1' => $content->mcid, 'courseid2' => $content->mcid, 'contextlevel1' => CONTEXT_COURSE, 'archetype' => 'student'));
                    //echo print_object($getcomplete);
                    //echo print_object($getstudent);
                    if (!empty($getstudent)) {
                        //이수완료 배열
                        $completion = array();
                        //새로생성된 강의 수강자
                        $student = array();
                        $scdiff = array();
                        $final = array();
                        //이수완료
                        foreach ($getcompleteArray as $gc) {
                            $completion[] = $gc->userid;
                        }
                        //수강자
                        foreach ($getstudent as $gs) {
                            $student[] = $gs->userid;
                        }

                        $scdiff = array_diff($student, $completion);
                        $final = array_diff($student, $scdiff);

                        if (!empty($final)) {
                            foreach ($final as $student) {
                                $udtuser = new stdclass();
                                $udtuser->id = $student;
                                $depth1 = $content->ca1name;
                                $depth2 = $content->ca2name;

                                $subject = $content->fullname . "_추가 콘텐츠 수강 1차 안내";
                                $massage = $depth2 . "의 " . $depth1 . " 콘텐츠가 추가되었습니다.기존의 자격충족 포인트 기준이 변경되오니 수강 기간 내 학습을 완료하여 포인트를 획득하시기 바랍니다.";
                                reday_to_send_email($udtuser, $subject, $massage);
                                reday_to_send_push($udtuser, $subject, $massage);
                            }
                        }
                    }
                }
            }
        }
    }
}

function right_better_auto_alarm($user, $course) {
    global $DB;
    $udtuser = new stdclass();
    $udtuser->id = $user->userid;
    $courses = local_course_category_name($course->courseid);

    $depth1 = $courses->ca1name;
    $depth2 = $courses->ca2name;
    $subject = "<" . $course->fullname . "_개선 콘텐츠 수강 1차 안내>";
    $massage = $depth2 . "의 " . $depth1 . " 콘텐츠 내용이 개선되었습니다.콘텐츠 개선으로 기존의 자격충족 포인트가 차감되오니 수강 기간 내 학습을 완료하여 포인트를 재획득하시기 바랍니다.";

    reday_to_send_email($udtuser, $subject, $massage);
    reday_to_send_push($udtuser, $subject, $massage);
}

function better_auto_alarm() {
    global $DB;
    // 일주일전
    $subSql = ' SELECT 
                    MAX(id) AS pid
                FROM {local_jeipoint} 
                GROUP BY section,userid  ';

    $sqlFrom = 'SELECT 
                        lj.id as ljid,
                        cm.*,
                        co.fullname,
                        co.id as courseid,
                        ok.name, ok.point,
                        lc.learningstart, lc.learningend, 
                        lj.timecreated AS learningtime, lj.userid  ';
    $sql = 'FROM {local_jeipoint} lj
                JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = lj.cmid AND cmc.completionstate = 0 
                JOIN {course} co ON co.id = lj.courseid
                JOIN {course_modules} cm ON cm.id = lj.cmid
                JOIN {okmedia} ok ON ok.id = cm.instance
                JOIN {lmsdata_class} lc ON lc.courseid = lj.courseid
                JOIN (' . $subSql . ') pid ON pid.pid = lj.id ';
    $sqlWhere[] = ' lj.point < :point ';
    $sqlWhere[] = ' cm.visible = :visible ';
    $sqlWhere[] = ' lc.learningend < :time1 ';
    $sqlWhere[] = ' lj.timecreated < :time2 ';
    $sqlWhere[] = ' lj.timecreated + (60*60*24*30) > :time3 ';
    $sqlWhere = ' WHERE .' . implode(' AND ', $sqlWhere);
    $sqlWhere = $sqlWhere . " GROUP BY lj.id";
    $params['point'] = 0;
    $params['visible'] = 1;
    $params['time1'] = time();
    $params['time2'] = time();
    $params['time3'] = time();

    $contents = $DB->get_records_sql($sqlFrom . $sql . $sqlWhere, $params);

    if (!empty($contents)) {
        foreach ($contents AS $content) {
            $udtuser = new stdclass();
            $udtuser->id = $content->userid;
            $courses = local_course_category_name($content->courseid);
            $depth1 = $courses->ca1name;
            $depth2 = $courses->ca2name;

            $week_close = $content->learningtime + (60 * 60 * 24 * 23);
            $week_close = strtotime("midnight", $week_close);

            if ($week_close == strtotime("midnight", time())) {
                $subject = $content->fullname . "_개선 콘텐츠 수강 2차 안내";
                $massage = $depth2 . "의 " . $depth1 . " 개선 콘텐츠 수강기간이 1주일 뒤 종료됩니다.현재 개선 콘텐츠 학습을 완료하지 않으셨습니다. 학습 미수료 시 포인트 부족으로 멘토 자격기준에 미달되오니 기간 내 학습을 완료해주시기 바랍니다.";
                reday_to_send_email($udtuser, $subject, $massage);
                reday_to_send_push($udtuser, $subject, $massage);
            }

            $day_close = $content->learningtime + (60 * 60 * 24 * 29);
            $day_close = strtotime("midnight", $day_close);
            if ($day_close == strtotime("midnight", time())) {
                $subject = $content->fullname . "_개선 콘텐츠 수강 마감 안내";
                $massage = $depth2 . "의 " . $depth1 . " 개선 콘텐츠 수강기간이 금일 종료됩니다.현재 개선 콘텐츠 학습을 완료하지 않으셨습니다. 학습 미수료 시 포인트 부족으로 멘토 자격기준에 미달되오니 학습을 완료해주시기 바랍니다.";

                reday_to_send_email($udtuser, $subject, $massage);
                reday_to_send_push($udtuser, $subject, $massage);
            }
        }
    }
}

function local_course_category_name($courseid) {
    global $DB;

    $sql = 'select mc.*, ca.name ca1name, ca2.name ca2name FROM {course} mc 
                JOIN {course_categories} ca ON ca.id = mc.category 
                left JOIN {course_categories} ca2 ON ca.parent = ca2.id where mc.id = :courseid';
    $courses = $DB->get_record_sql($sql, array("courseid" => $courseid));

    return $courses;
}
