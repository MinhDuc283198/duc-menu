<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/book/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('교재목록');

echo $OUTPUT->header();
$list = local_book_list()->list;
?>
<script type="text/javascript">
    document.title = "Master Korean - <?php echo get_string('title', 'local_book') ?>"
</script>
<div class="crs-bx">
    <ul class="crs-list book big">    
<?php if ($list) { ?>
    <?php
    foreach ($list as $li) { 
        ?>

            
                <li>
                    <div class="img">
                        <a href="./detail.php?id=<?php echo $li->id ?>"><img src="<?php echo $li->img ?>" alt="<?php echo $li->lang_title ?>" /></a>
                    </div>
                    <div class="txt">
                        <div><a href="./detail.php?id=<?php echo $li->id ?>"><?php echo $li->lang_title ?></a></div>
                        <p><a href="./detail.php?id=<?php echo $li->id ?>"><span><?php echo $li->lang_publisher ?></span><span><?php echo $li->lang_author ?></span></a></p>

                        <p class="f16 t-blue">
                            <a href="./detail.php?id=<?php echo $li->id ?>">
                                <?php if ($li->discount) { ?><strong class="p-down"><span><?php echo $li->originprice_com ?> VND</span><span><?php echo $li->discount ?>%</span></strong>
                                    <?php echo $li->price_com ?> VND<?php }else{ ?>
                                    <?php echo '<span>'.$li->price_com. ' VND</span>'; }?> 
                            </a>
                        </p>

                        <div class="m-out">
                            <?php
                            $recourse_list = local_course_relate_course($li->id);
                            if ($recourse_list) {
                                ?>
                                <p class="f18">[<?php echo get_string('rcourse', 'local_book') ?>]</p>
                                <ul>
                                    <?php
                                    foreach ($recourse_list as $rc) {
                                        ?>
                                        <li><a href="/local/course/detail.php?id=<?php echo $rc->id ?>"><?php echo $rc->lang_title; ?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>



                            <?php
                            $rebook_list = local_book_relate_book($li->id);
                            if ($rebook_list) {
                                ?>
                                <p class="f18">[<?php echo get_string('rtextbook', 'local_book') ?>]</p>
                                <ul>
                                    <?php
                                    foreach ($rebook_list as $ra) {
                                        ?>
                                        <li><a href="./detail.php?id=<?php echo $ra->id ?>"><?php echo $ra->lang_title; ?></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="bt-area">
                        <a href="./detail.php?id=<?php echo $li->id ?>" class="btns br"><?php echo get_string('readmore', 'local_course') ?></a>
                    </div>
                </li>     
      

    <?php } ?>  
<?php } else { ?>
    <div class="no-data">
        <div><?php echo get_string('empty', 'local_course') ?></div>
    </div>
<?php } ?>
    </ul>
</div>
<?php
echo $OUTPUT->footer();
?>

