<?php
$string['title'] = "List of curriculums";
$string['rcourse'] = 'Related Courses';
$string['rtextbook'] = 'Related Texts';
$string['empty'] = 'There are no textbooks for the selected category.';
$string['originprice'] = 'List price';
$string['brcourse'] = 'Related Courses in This Book';
$string['brtextbook'] = 'Related Books in This Book';
$string['paymentcompleted'] = 'Your payment has been completed.';
$string['ready'] = 'Ready to Ship';
$string['completed'] = 'Delivery completed';
$string['shipping'] = 'shipping';
$string['loginconfirm'] = 'This service requires a login. Do you want to log in?';
$string['possible4'] = 'Available from April.';