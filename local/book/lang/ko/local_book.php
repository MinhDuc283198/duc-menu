<?php
$string['title'] = "교재";
$string['rcourse'] = '관련 강좌';
$string['rtextbook'] = '관련 교재';
$string['empty'] = '선택한 분류에 해당하는 교재가 없습니다.';
$string['originprice'] = '정가';
$string['brcourse'] = '이 교재의 관련 강좌';
$string['brtextbook'] = '이 교재의 관련 교재';
$string['paymentcompleted'] = '결제가 완료되었습니다.';
$string['ready'] = '배송준비';
$string['completed'] = '배송완료';
$string['shipping'] = '배송중';
$string['loginconfirm'] = '로그인이 필요한 서비스 입니다. 로그인 하시겠습니까?';
$string['possible4'] = '4월부터 구매 가능합니다.';