<?php
$string['title'] = "Danh sách giáo trình";
$string['rcourse'] = 'Khóa học liên quan';
$string['rtextbook'] = 'Giáo trình liên quan';
$string['empty'] = 'Không có giáo trình nào cho tùy chọn hiện tại';
$string['originprice'] = 'Giá';
$string['brcourse'] = 'Khóa học liên quan đến giáo trình này';
$string['brtextbook'] = 'Giáo trình liên quan đến giáo trính này';
$string['paymentcompleted'] = 'Bạn đã hoàn thành thanh toán';
$string['ready'] = 'Sẵn sàng vận chuyển';
$string['completed'] = 'Giao hàng xong';
$string['shipping'] = 'Đang chuyển hàng';
$string['loginconfirm'] = 'Dịch vụ này yêu cầu đăng nhập. Bạn có muốn đăng nhập?';
$string['possible4'] = 'Có sẵn từ tháng Tư.';