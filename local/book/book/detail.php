<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/book/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('교재목록');

echo $OUTPUT->header();
$id = optional_param('id', 0, PARAM_INT);
$list = local_book_id($id);
$bookimgpath = local_course_book_get_imgpath($list->bookid);
?>  

<div class="bk-detail">
    <div class="bk-img"><img src="<?php echo $bookimgpath ?>" alt="<?php echo $list->title ?>"/></div>
    <div class="bk-txt">
        <div class="tit"><?php echo $list->title ?></div>
        <div class="t-gray"><span><?php echo $list->publisher ?></span><span><?php echo $list->author ?></span></div>
        <p>
            <!-- 모바일 용  영역 -->
            <span class="m-show">
                <em>교재</em>
                <span><?php echo $list->title ?></span>
            </span>
            <span>금액</span>
            <strong>
                <?php if ($list->discount) { ?><strong class="p-down"><?php } ?><span><?php echo $list->originprice ?> VND</span><?php if ($list->discount) { ?><span><?php echo $list->discount ?>%</span></strong>
                    <em><?php echo number_format($list->price) ?></em>VND<?php } ?>
            </strong>

        </p>

        <a href="#" class="btns point big payment"><?php echo get_string('payment', 'local_course') ?></a>
        <div class="txt">
            <div><?php echo get_string('tIntroduction', 'local_course') ?></div>
            <?php echo $list->text ?>
        </div>
    </div>
</div>




<?php
$recourse_list = local_course_relate_course($list->id);
if ($recourse_list) {
    ?>

    <h5 class="pg-tit"><?php echo get_string('brcourse', 'local_book') ?></h5>
    <div class="crs-bx">
        <ul class="crs-list">
            <?php
            foreach ($recourse_list as $ra) {
                ?>
                <li>
                    <div class="img">
                        <span class="ic-heart <?php echo $ra->likes ? 'on' : ''; ?>" id="like<?php echo $ra->id ?>" data-target="<?php echo $ra->id ?>">좋아요</span>
                        <img src="<?php echo $ra->img ?>" alt="<?php echo $ra->lang_title ?>" />
                    </div>
                    <div class="txt">
                        <div><?php echo $ra->lang_title ?></div>
                        <p>
                            <span><?php echo $ra->teacher_list;?><?php echo get_string('teacher', 'local_course') ?></span>
                            <span><?php echo get_string('total', 'local_course') ?> <?php echo $ra->lecturecnt ?><?php echo get_string('class', 'local_course') ?></span>
                        </p>
                        <p class="t-gray"><?php echo $ra->courseperiod_txt ?></p>
                    </div>
                    <div class="bt-area">
                        <a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $ra->id ?>" class="btns <?php echo $ra->button_color?>"><?php echo $ra->button_txt?></a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>


<?php
$recourse_list = local_book_relate_book($list->id);
if ($recourse_list) {
    ?>
    <h5 class="pg-tit"><?php echo get_string('brtextbook', 'local_book') ?></h5>

    <div class="crs-bx">
        <ul class="crs-list book">
            <?php
            foreach ($recourse_list as $ra) {
                ?>
                <li>
                    <div class="img">
                        <img src="<?php echo $ra->img ?>" alt="<?php echo $ra->lang_title ?>" />
                    </div>
                    <div class="txt">
                        <div><?php echo $ra->lang_title ?></div>
                        <p><span><?php echo $ra->lang_publisher ?></span><span><?php echo $ra->lang_author ?></span></p>
                        
                        <p class="f16">
                            <?php if($ra->discount) {?><strong class="p-down"><?php }?><span><?php echo $ra->originprice_com?> VND</span><?php if($ra->discount) {?><span><?php echo $ra->discount?>%</span></strong><?php }?>
                            <?php if($ra->discount) {echo $ra->price_com?> VND<?php }?>
                        </p>
                        
                    </div>
                    <div class="bt-area">
                        <a href="<?php echo $CFG->wwwroot ?>/local/book/detail.php?id=<?php echo $ra->id ?>" class="btns br"><?php echo get_string('readmore', 'local_course') ?></a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
<?php
echo $OUTPUT->footer();
?>

<script type="text/javascript">
    $(function () {
        //배송정보입력 팝업 
        $(".payment").click(function () {
            utils.popup.call_layerpop("/local/course/pop_shoppinginfo.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"id": '<?php echo $id ?>'});
            return false;
        })
    });


    function productdelivery(bookid, type) {
        var address = $('input[name=address]').val();
        var address_detail = $('input[name=address_detail]').val();
        var phone1 = $('input[name=phone1]').val();
        var phone2 = $('input[name=phone2]').val();
        var phone3 = $('input[name=phone3]').val();

        var result = call_ajax('shoppinginfo', {bookid: bookid, address: address + ' ' + address_detail, cell: phone1 + phone2 + phone3, type: type}, false);
        console.log(result.status);
        if (result.status == 'success') {
            alert('<?php echo get_string('paymentcompleted', 'local_book') ?>');
            utils.popup.close_pop($('.layerpop'));
        }
    }


</script>