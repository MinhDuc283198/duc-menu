<?php

$string['rcourse'] = 'Related Courses';
$string['rtextbook'] = 'Related Texts';
$string['empty'] = 'There are no textbooks for the selected category.';
$string['originprice'] = 'List price';
$string['brcourse'] = 'Related Courses in This Book';
$string['brtextbook'] = 'Related Books in This Book';
$string['paymentcompleted'] = 'Your payment has been completed.';