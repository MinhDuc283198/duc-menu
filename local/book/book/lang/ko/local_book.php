<?php

$string['rcourse'] = '관련 강좌';
$string['rtextbook'] = '관련 교재';
$string['empty'] = '선택한 분류에 해당하는 교재가 없습니다.';
$string['originprice'] = '정가';
$string['brcourse'] = '이 교재의 관련 강좌';
$string['brtextbook'] = '이 교재의 관련 교재';
$string['paymentcompleted'] = '결제가 완료되었습니다.';