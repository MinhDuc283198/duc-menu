<?php

$string['rcourse'] = 'Related Courses_vi';
$string['rtextbook'] = 'Related Texts_vi';
$string['empty'] = 'There are no textbooks for the selected category._vi';
$string['originprice'] = 'List price_vi';
$string['brcourse'] = 'Related Courses in This Book_vi';
$string['brtextbook'] = 'Related Books in This Book_vi';
$string['paymentcompleted'] = 'Your payment has been completed._vi';