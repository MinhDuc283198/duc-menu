<?php

function lmsdata_book_get_file($contextid, $filearea, $itemid = 0) {
    global $CFG, $OUTPUT;

    $fs = get_file_storage();
    $files = $fs->get_area_files(1, 'local_lmsdata', 'textbook', $itemid, "", false);
    //print_object($files);exit;

    $fileobj = '';
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $mimetype = $file->get_mimetype();
        $iconimage = '<img src="' . $OUTPUT->pix_url(file_mimetype_icon($mimetype)) . '" class="icon" alt="' . $mimetype . '" />';

        $path = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . 1 . '/local_lmsdata/textbook/' . $itemid . '/' . $filename);
        if ($file->get_filesize() > 0) {
            if ($type == 1) {
                $fileobj = "<span id ='file' name='file_link_1'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_1' value='삭제' onclick='remove_file_1()'/></span>";
            } else if ($type == 2) {
                $fileobj = "<span id ='file' name='file_link_2'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button_2' value='삭제' onclick='remove_file_2()'/></span>";
            } else {
                $fileobj = "<span id ='file' name='file_link'><a href=\"$path\">$iconimage</a>";
                $fileobj .= format_text("<a href=\"$path\">" . s($filename) . "</a>", FORMAT_HTML, array('context' => $context));
                $fileobj .= "<input type='button' class='gray_btn_small' name='remove_button' value='삭제' onclick='remove_file()'/></span>";
            }

            //if($type == 1){
            //$fileobj = '<img class="small" src="'.$path.'">';//클래스가 small 이거나 on일때 이미지 경로 나오게 
            //}
        }
    }
    return $fileobj;
}

function local_book_discount($pay, $rate) {
    return round(((1 - $rate * 0.01) * $pay * 0.01) * 100);
}

//교재 리스트
function local_book_list() {
    global $DB;

    $currentlang = current_language();
    $lang = '';
    $lang2 = '';
    if ($currentlang == 'ko') {
        $lang = '';
        $lang2 = '';
    } else if ($currentlang == 'vi') {
        $lang = 'vi_';
        $lang2 = 'vn';
    } else if ($currentlang == 'en') {
        $lang = 'en_';
        $lang2 = 'en';
    }

    $now = time();
    $query = "select lc.id, lt.price as originprice, lt.id as bookid ,lc." . $lang . "title as title, lc.price, lc.relationcourse, lc.discount, lt." . $lang . "author as author, (select title" . $lang2 . " from {lmsdata_copyright} where id=lt.publisher)  as publisher    
        from {lmsdata_class} lc
        JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
        WHERE lc.type = 2    AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0 ";

    return $DB->get_records_sql($query);
}

function local_book_list2() {
    global $DB;
    $now = time();

    $select = 'SELECT lc.id,lc.bookid, lc.price,lc.discount,lt.price as originprice,lc.type, lc.learningstart, lc.learningend, lc.isused as lcisused, lt.isused, 
                    lc.title, lc.vi_title, lc.en_title, lt.author, lt.vi_author, lt.en_author,
                    lt.text, lt.vi_text, lt.en_text, 
                    (select title from {lmsdata_copyright} where id=lt.publisher)  as publisher,(select titlevn from {lmsdata_copyright} where id=lt.publisher)  as vi_publisher,(select titleen from {lmsdata_copyright} where id=lt.publisher)  as en_publisher';
    $select_cnt = "SELECT count(lc.id) ";
    $query = " FROM {lmsdata_class} lc
                JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
                WHERE lc.type = 2   AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0 AND lt.isused = 0 
                ORDER BY lc.id DESC ";
    
    $return = new stdClass();
    $return->list = $DB->get_records_sql($select.$query);
    $return->cnt = $DB->count_records_sql($select_cnt.$query);
    
    if($return->cnt > 0){
        foreach($return->list as $key => $rl){
            $bookimgpath = local_course_book_get_imgpath($rl->bookid);
            $return->list[$key]->img = $bookimgpath;
            $return->list[$key]->originprice_com = number_format($rl->originprice);
            $return->list[$key]->price_com = number_format($rl->price);
            switch (current_language()){
                case 'ko' :
                    $return->list[$key]->lang_title = $rl->title;
                    $return->list[$key]->lang_publisher = $rl->publisher;
                    $return->list[$key]->lang_author = $rl->author;
                    $return->list[$key]->lang_text = $rl->text;
                    break;
                case 'en' :
                    $return->list[$key]->lang_title = $rl->en_title;
                    $return->list[$key]->lang_publisher = $rl->en_publisher;
                    $return->list[$key]->lang_author = $rl->en_author;
                    $return->list[$key]->lang_text = $rl->en_text;
                    break;
                case 'vi' :
                    $return->list[$key]->lang_title = $rl->vi_title;
                    $return->list[$key]->lang_publisher = $rl->vi_publisher;
                    $return->list[$key]->lang_author = $rl->vi_author;
                    $return->list[$key]->lang_text = $rl->vi_text;
                    break;
            }
        }
    }
    
    return $return;
}

//교재
function local_book_id($id = 0) {
    global $DB;
    $currentlang = current_language();
    $lang = '';
    $lang2 = '';
    if ($currentlang == 'ko') {
        $lang = '';
        $lang2 = '';
    } else if ($currentlang == 'vi') {
        $lang = 'vi_';
        $lang2 = 'vn';
    } else if ($currentlang == 'en') {
        $lang = 'en_';
        $lang2 = 'en';
    }
    $select = 'lc.' . $lang . "title as title, lt." . $lang . "author as author, (select title" . $lang2 . " from {lmsdata_copyright} where id=lt.publisher)  as publisher, lt." . $lang . "text as text";
    $now = time();
    if ($id) {
        $sql_where[] = " lc.id=:id ";
        $params['id'] = $id;
    }

    if (!empty($sql_where)) {
        $sql_where = ' and ' . implode(' and ', $sql_where);
    } else {
        $sql_where = '';
    }

    $query = "select lc.id, lt.id as bookid , lcp.title as publisher,lc.price, lc.relationcourse, lc.discount, lt.price as originprice,  $select   
        from {lmsdata_class} lc
        JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id
        JOIN {lmsdata_copyright} lcp ON lcp.id = lt.publisher 
        WHERE lc.type = 2 and lcp.type = 1   AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0 " . $sql_where;

    return $DB->get_record_sql($query, $params);
}

//교재 검색
function local_book_search_title($search, $page, $perpage = 5) {
    global $DB, $USER;
    $now = time();

    $currentlang = current_language();
    $select = 'SELECT lc.id,lc.bookid, lc.price,lc.discount,lt.price as originprice, 
                        lc.title, lc.vi_title, lc.en_title, lt.author, lt.vi_author, lt.en_author,
                        (select title from {lmsdata_copyright} where id=lt.publisher)  as publisher,(select titlevn from {lmsdata_copyright} where id=lt.publisher)  as vi_publisher,(select titleen from {lmsdata_copyright} where id=lt.publisher)  as en_publisher';

    $select_cnt = "SELECT count(lc.id) ";
    $from = ' FROM {lmsdata_class} lc
              JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id';
    $where = " WHERE lc.type = 2 AND (lc.title like :title or lc.vi_title like :vi_title or lc.en_title like :en_title)  AND lc.learningstart <= $now AND $now <= lc.learningend AND lc.isused = 0 ";
    $order_by = " ORDER BY lc.id DESC ";
    $params = array('title' => '%' . $search . '%', 'vi_title' => '%' . $search . '%', 'en_title' => '%' . $search . '%');

    $return = new stdClass();
    $return->list = $DB->get_records_sql($select . $from . $where . $order_by, $params, $page * $perpage, $perpage);
    $return->cnt = $DB->count_records_sql($select_cnt . $from . $where, $params);

    if ($return->cnt > 0) {
        foreach ($return->list as $key => $rl) {
            $bookimgpath = local_course_book_get_imgpath($rl->bookid);
            $return->list[$key]->img = $bookimgpath;
            $return->list[$key]->originprice_com = number_format($rl->originprice);
            $return->list[$key]->price_com = number_format($rl->price);
            //관련강좌
            $relation_course_query = "SELECT lc.id, lc.title, lc.vi_title, lc.en_title  
                      FROM {lmsdata_class_relation} cr 
                      JOIN {lmsdata_class} lc ON cr.dataid = lc.id
                      JOIN {course} mc ON lc.courseid = mc.id
                      JOIN {lmsdata_course} lco ON lc.parentcourseid = lco.id
                      JOIN {course} mc2 ON lco.courseid = mc2.id
                      JOIN {course_categories} ca ON ca.id = mc2.category
                      left JOIN {course_categories} ca2 ON ca.parent = ca2.id
                      WHERE ( lc.type = :type1 or lc.type = :type2) AND cr.classid = :classid  AND cr.type = :crtype 
                      AND lc.learningstart <= :learningstart AND :learningend <= lc.learningend AND lc.isused = :isused ";
            $relation_course_params = array('type1' => 1, 'type2' => 3, 'learningstart' => $now, 'learningend' => $now, 'isused' => 0, 'classid' => $rl->id, 'crtype' => 1);
            $relation_course = $DB->get_records_sql($relation_course_query . $order_by, $relation_course_params);
            $return->list[$key]->relation_course = $relation_course;
            foreach ($relation_course as $rkey => $rc) {
                switch (current_language()) {
                    case 'ko' :
                        $return->list[$key]->relation_course[$rkey]->lang_title = $rc->title;
                        break;
                    case 'en' :
                        $return->list[$key]->relation_course[$rkey]->lang_title = $rc->en_title;
                        break;
                    case 'vi' :
                        $return->list[$key]->relation_course[$rkey]->lang_title = $rc->vi_title;
                        break;
                }
            }

            switch (current_language()) {
                case 'ko' :
                    $return->list[$key]->lang_title = $rl->title;
                    $return->list[$key]->lang_publisher = $rl->publisher;
                    $return->list[$key]->lang_author = $rl->author;
                    $return->list[$key]->lang_text = $rl->text;
                    break;
                case 'en' :
                    $return->list[$key]->lang_title = $rl->en_title;
                    $return->list[$key]->lang_publisher = $rl->en_publisher;
                    $return->list[$key]->lang_author = $rl->en_author;
                    $return->list[$key]->lang_text = $rl->en_text;
                    break;
                case 'vi' :
                    $return->list[$key]->lang_title = $rl->vi_title;
                    $return->list[$key]->lang_publisher = $rl->vi_publisher;
                    $return->list[$key]->lang_author = $rl->vi_author;
                    $return->list[$key]->lang_text = $rl->vi_text;
                    break;
            }
        }
    }
    return $return;
}

//관련 교재
function local_book_relate_book($id) {
    global $DB, $USER;
    $now = time();
    //관련교재
    $query = "SELECT lc.id,lc.bookid, lc.price,lc.discount,lt.price as originprice,  cr.type, lc.type as lctype, lc.isused as lcisused, lt.isused,lc.learningstart, lc.learningend,    
                        lc.title, lc.vi_title, lc.en_title, lt.author, lt.vi_author, lt.en_author,
                        (select title from {lmsdata_copyright} where id=lt.publisher)  as publisher,(select titlevn from {lmsdata_copyright} where id=lt.publisher)  as vi_publisher,(select titleen from {lmsdata_copyright} where id=lt.publisher)  as en_publisher   
              FROM {lmsdata_class_relation} cr 
              JOIN {lmsdata_class} lc ON cr.dataid = lc.id
              JOIN {lmsdata_textbook} lt ON lc.bookid = lt.id 
              WHERE lc.type = :type1 AND cr.classid = :classid  AND cr.type = :crtype 
              AND lc.learningstart <= :learningstart AND :learningend <= lc.learningend AND lc.isused = :isused AND lt.isused=0
              ORDER BY lc.id DESC ";
    $params = array('type1' => 2, 'learningstart' => $now, 'learningend' => $now, 'isused' => 0, 'classid' => $id, 'crtype' => 2);
    $list = $DB->get_records_sql($query, $params);
    $return = new stdClass();
    $return->list = $list;

    if ($list) {
        foreach ($list as $key => $rl) {
            $return->list[$key]->img = local_course_book_get_imgpath($rl->bookid);
            $return->list[$key]->originprice_com = number_format($rl->originprice);
            $return->list[$key]->price_com = number_format($rl->price);
            switch (current_language()) {
                case 'ko' :
                    $return->list[$key]->lang_title = $rl->title;
                    $return->list[$key]->lang_publisher = $rl->publisher;
                    $return->list[$key]->lang_author = $rl->author;
                    break;
                case 'en' :
                    $return->list[$key]->lang_title = $rl->en_title;
                    $return->list[$key]->lang_publisher = $rl->en_publisher;
                    $return->list[$key]->lang_author = $rl->en_author;
                    break;
                case 'vi' :
                    $return->list[$key]->lang_title = $rl->vi_title;
                    $return->list[$key]->lang_publisher = $rl->vi_publisher;
                    $return->list[$key]->lang_author = $rl->vi_author;
                    break;
            }
        }
    }
    return $list;
}
