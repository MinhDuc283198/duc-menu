<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/book/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('교재목록');

echo $OUTPUT->header();
$list = local_book_list();
?>  

<?php if($list){?>
<?php
foreach($list as $li){
    $bookimgpath = local_course_book_get_imgpath($li->bookid);
?>
<div class="crs-bx">
    <ul class="crs-list book big">    
        <li>
            <div class="img">
                <a href="./detail.php?id=<?php echo $li->id?>"><img src="<?php echo $bookimgpath?>" alt="<?php echo $li->title?>" /></a>
            </div>
            <div class="txt">
                <a href="./detail.php?id=<?php echo $li->id?>"><div><?php echo $li->title?></div></a>
                <a href="./detail.php?id=<?php echo $li->id?>"><p><span><?php echo $li->publisher?></span><span><?php echo $li->author?></span></p></a>

                <a href="./detail.php?id=<?php echo $li->id?>">
                <p class="f16 t-blue">
                    <?php if($li->discount) {?><strong class="p-down"><?php }?><span><?php echo number_format($li->originprice)?> VND</span><?php if($li->discount) {?><span><?php echo $li->discount?>%</span></strong><?php }?>
                    <?php if($li->discount) {echo number_format($li->price)?> VND<?php }?>
                </p></a>
                
                
                <?php 
                $recourse_list = $DB->get_records('lmsdata_class_relation', array('type'=>1, 'classid'=>$li->id));
                if($recourse_list){
                ?>
                <p class="f18">[<?php echo get_string('rcourse', 'local_book') ?>]</p>
                <ul>
                    <?php
                    foreach($recourse_list as $ra){
                        $rcourse_list = local_course_class_id2($ra->dataid);
                ?>
                    <li><a href="/local/course/detail.php?id=<?php echo $ra->dataid?>"><?php echo $rcourse_list->title;?></a></li>
                <?php }?>
                </ul>
                <?php }?>
                
                
                
                <?php 
                $recourse_list = $DB->get_records('lmsdata_class_relation', array('type'=>2, 'classid'=>$li->id));
                if($recourse_list){
                ?>
                <p class="f18">[<?php echo get_string('rtextbook', 'local_book') ?>]</p>
                <ul>
                    <?php
                    foreach($recourse_list as $ra){
                        $rcourse_list = local_book_id($ra->dataid);
                ?>
                    <li><a href="./detail.php?id=<?php echo $ra->dataid?>"><?php echo $rcourse_list->title;?></a></li>
                <?php }?>
                </ul>
                <?php }?>
            </div>
            <div class="bt-area">
                <a href="./detail.php?id=<?php echo $li->id?>" class="btns br"><?php echo get_string('readmore', 'local_course') ?></a>
            </div>
        </li>     
    </ul>
</div>
<?php }?>  
<?php }else{?>
<div class="no-data">
    <div><?php echo get_string('empty', 'local_course') ?></div>
</div>
<?php }?>

<?php
echo $OUTPUT->footer();
?>


