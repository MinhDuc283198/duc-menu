<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/book/lib.php');
require_once($CFG->dirroot . '/local/course/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('교재목록');

echo $OUTPUT->header();
$id = optional_param('id', 0, PARAM_INT);
$list = local_book_id($id);

$alreadybuyingsql =   "select * from {lmsdata_payment} lp 
join {lmsdata_productdelivery} lpd on lp.id = lpd.payid
where lp.userid = ? and lp.ref = ? order by lp.id desc" ;
$alreadybuying = $DB->get_record_sql($alreadybuyingsql, array($USER->id, $id));
if ($alreadybuying) {
    switch ($alreadybuying->delivery) {
        case 0 :
            $delivery_txt = get_string('ready','local_book');
            break;
        case 1:
            $delivery_txt = get_string('completed','local_book');
            break;
        case 2:
            $delivery_txt = get_string('shipping','local_book');
            break;
    }
}
$bookimgpath = local_course_book_get_imgpath($list->bookid);
?>  

<div class="bk-detail">
    <div class="bk-img"><img src="<?php echo $bookimgpath ?>" alt="<?php echo $list->title ?>"/></div>
    <div class="bk-txt">
        <div class="tit"><?php echo $list->title ?></div>
        <script type="text/javascript">
            document.title = "Master Korean - <?php echo $list->title ?>"
        </script>
        <div class="t-gray"><span><?php echo $list->publisher ?></span><span><?php echo $list->author ?></span></div>
        <p>
            <!-- 모바일 용  영역 -->
            <span class="m-show">
                <em><?php echo get_string('textbook', 'local_course') ?></em>
                <span><?php echo $list->title ?></span>
            </span>
            <span><?php echo get_string('price', 'local_course') ?></span>
            <strong>
                <?php if ($list->discount) { ?><!-- 할인 있는 경우-->
                    <strong class="p-down"><span><?php echo number_format($list->originprice) ?></span> VND<span><?php echo $list->discount ?>%</span></strong>
                    <em><?php echo number_format($list->price) ?></em>VND
                <?php }else{ ?><!-- 할인 없는 경우-->
                          <em><?php echo number_format($list->price) ?></em>VND
                <?php } ?>
            </strong>

        </p>
        <?php if($alreadybuying){ ?>
            <a href="#" class="btns point big gray"><?php echo $delivery_txt ?></a>
        <?php }else{ ?>
            <a href="#" class="btns point big payment"><?php echo get_string('payment', 'local_course') ?></a>
        <?php } ?>
        <div class="txt">
            <div><?php echo get_string('tIntroduction', 'local_course') ?></div>
            <?php echo $list->text ?>
        </div>
        <?php 
        $keywords = explode('#', $list->lang_keywords);
        $keywords = array_filter($keywords);
        if(!empty($keywords)){
        ?>
        <ul class="hash-area">
            <?php foreach($keywords as $keyword){?>
            <li data-title="<?php echo $keyword?>"><a href="#"><?php echo $keyword?></a></li>
            <?php }?>
        </ul>
        <?php }?>
    </div>
</div>




<?php
$recourse_list = local_course_relate_course($list->id);
if ($recourse_list) {
    ?>

    <h5 class="pg-tit"><?php echo get_string('brcourse', 'local_book') ?></h5>
    <div class="crs-bx">
        <ul class="crs-list">
            <?php
            foreach ($recourse_list as $ra) {
                ?>
                <li>
                    <div class="img">
                        <span class="ic-heart <?php echo $ra->likes ? 'on' : ''; ?>" id="like<?php echo $ra->id ?>" data-target="<?php echo $ra->id ?>"><?php echo get_string('like', 'local_course') ?></span>
                        <a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $ra->id ?>"><img src="<?php echo $ra->img ?>" alt="<?php echo $ra->lang_title ?>" /></a>
                    </div>
                    <a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $ra->id ?>">
                    <div class="txt">
                        <div><?php echo $ra->lang_title ?></div>
                        <p>
                            <span><?php echo $ra->teacher_list; ?><?php echo get_string('teacher', 'local_course') ?></span>
                            <span><?php echo get_string('total', 'local_course') ?> <?php echo $ra->lecturecnt ?><?php echo get_string('class', 'local_course') ?></span>
                        </p>
                        <p class="t-gray"><?php echo $ra->courseperiod_txt ?></p>
                    </div>
                    </a>
                    <div class="bt-area">
                        <a href="<?php echo $CFG->wwwroot ?>/local/course/detail.php?id=<?php echo $ra->id ?>" class="btns <?php echo $ra->button_color ?>"><?php echo $ra->button_txt ?></a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>


<?php
$recourse_list = local_book_relate_book($list->id);
if ($recourse_list) {
    ?>
    <h5 class="pg-tit"><?php echo get_string('brtextbook', 'local_book') ?></h5>

    <div class="crs-bx">
        <ul class="crs-list book">
            <?php
            foreach ($recourse_list as $ra) {
                ?>
                <li>
                    <a href="<?php echo $CFG->wwwroot ?>/local/book/detail.php?id=<?php echo $ra->id ?>">
                    <div class="img">
                        <img src="<?php echo $ra->img ?>" alt="<?php echo $ra->lang_title ?>" />
                    </div>
                    </a>
                    <a href="<?php echo $CFG->wwwroot ?>/local/book/detail.php?id=<?php echo $ra->id ?>">
                    <div class="txt">
                        <div><?php echo $ra->lang_title ?></div>
                        <p><span><?php echo $ra->lang_publisher ?></span><span><?php echo $ra->lang_author ?></span></p>
                        
                        <p class="f16">
                            <?php if ($ra->discount) { ?><strong class="p-down"><?php } ?><span><?php echo $ra->originprice_com ?> VND</span><?php if ($ra->discount) { ?><span><?php echo $ra->discount ?>%</span></strong><?php } ?>
                            <?php if ($ra->discount) {
                                echo $ra->price_com ?> VND<?php } ?>
                        </p>

                    </div>
                    </a>
                    <div class="bt-area">
                        <a href="<?php echo $CFG->wwwroot ?>/local/book/detail.php?id=<?php echo $ra->id ?>" class="btns br"><?php echo get_string('readmore', 'local_course') ?></a>
                    </div>
                </li>
    <?php } ?>
        </ul>
    </div>
<?php } ?>
<?php require ($CFG->dirroot . '/local/course/recommendpage.php');?>
<?php
echo $OUTPUT->footer();
?>

<script type="text/javascript">
    $(function () {
        //배송정보입력 팝업 
        $(".payment").click(function () {
//            alert("<?php echo get_string('possible4','local_book') ?>");
            //4월 1일 오픈 
            if (not_login()) {
                utils.popup.call_layerpop("/local/payment/paypop.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"id": <?php echo $id ?>});
                return false;
            }
        })
    });
    function pay_resultpop(result){ 
        utils.popup.close_pop($('.layerpop'));
        if (not_login()) {
            utils.popup.call_layerpop("/local/payment/payokpop.php", {"width": "600px", "height": "auto", "callbackFn": function () {}}, {"jdata": result});
            return false;
        }
    }
    function not_login() {
<?php if (!isloggedin()) { ?>
            if (confirm('<?php echo get_string('loginconfirm','local_book')?>')) {
                location.href = '/login/index.php';
            } else {
                return false;
            }
<?php } else { ?>
            return true;
<?php } ?>
    }
    
    function productdelivery(bookid, type) {
        var address = $('input[name=address]').val();
        var address_detail = $('input[name=address_detail]').val();
        var phone1 = $('input[name=phone1]').val();
        
        if(address == '' || address_detail == '' || phone1 == ''){
            alert('<?php echo get_string('productdelivery:msg','local_course')?>');
            return false;
        }

        var result = call_ajax('shoppinginfo', {bookid: bookid, address: address + ' ' + address_detail, cell: phone1, type: type}, false);
        if (result.status == 'success') {
            alert('<?php echo get_string('paymentcompleted', 'local_book') ?>');
            location.reload();
            utils.popup.close_pop($('.layerpop'));
        }
    }
    

</script>