<?php

$string['pluginname'] = 'Quản trị khóa học';
$string['pluginnameplural'] = 'Quản trị khóa học';

$string['regular'] = 'Giáo trình';
$string['regularcourse'] = 'Khóa học bình thường';
$string['irregular'] = 'Khóa học cao cấp';
$string['elearning'] = 'Khóa học điện tử (e-Learning)';
$string['regular:short'] = 'Bình thường';
$string['irregular:short'] = 'Cao cấp';
$string['visible'] = 'Hiển thị';
$string['invisible'] = 'Ẩn';

$string['category:all'] = 'Tất cả';
$string['category:sort'] = 'Sắp xếp';

$string['apply:start'] = 'Đăng ký bắt đầu';
$string['apply:end'] = 'Đăng ký kết thúc';
$string['apply:isnotreged'] = 'Không đăng ký';

$string['course:apply'] = 'Đăng ký khóa học';
$string['course:apply_input'] = 'Vui lòng nhập thời gian đăng ký';
$string['course:overview'] = 'Thông báo khóa học';
$string['course:name'] = 'Khóa học';
$string['course:empty'] = 'Không có khóa học';
$string['course:list'] = 'Danh sách khóa học';
$string['course:plan'] = 'Kế hoạch khóa học';
$string['course:sititon'] = 'Kiểm toán';
$string['course:category'] = "Thể loại";
$string['course:subjectid'] = 'Mã môn học';
$string['course:subjectid_input'] = 'Vui lòng nhập mã môn học';
$string['course:professor'] = 'Giáo sư';
$string['course:add'] = 'Thêm khóa học';
$string['course:edit'] = 'Sửa khóa học';
$string['course:create'] = 'Tạo khóa học';
$string['course:adding'] = 'Bạn có thể thêm bài giảng trên GTEC.';
$string['course:date'] = 'Thời gian';
$string['course:date_input'] = 'Vui lòng nhập thời lượng bài giảng';
$string['course:start'] = 'Khóa học bắt đầu';
$string['course:end'] = 'Khóa học kết thúc';
$string['course:image'] = 'Hình ảnh khóa học';
$string['course:image_input'] = 'Bạn chỉ có thể chọn ảnh (.jpg, .png).';
$string['course:classes'] = 'Lớp học';
$string['course:classeslist'] = 'Danh sách lớp học';
$string['course:classes_drive'] = 'Điều hành lớp học';
$string['course:create_class'] = 'Tạo điều hành lớp học';
$string['course:apply_drive'] = 'Đăng ký điều hành lớp học';
$string['course:classes_drive_log'] = 'Lịch sử điều hành lớp học';
$string['course:classes_restore'] = 'Khôi phục lớp học';
$string['course:classes_restore_log'] = 'Khôi phục lịch sử khóa học';
$string['course:credit'] = 'Tín dụng';
$string['course:sel_more'] = 'Bạn phải chọn nhiều hơn 2 khóa học';
$string['course:sel_standard'] = 'Chọn khóa học tiêu chuẩn';
$string['course:standard'] = 'Khóa học tiêu chuẩn';
$string['course:standard_name'] = 'Tên khóa học tiêu chuẩn';
$string['course:subject_name'] = 'Chuyển tên khóa học';
$string['course:invisible'] = 'Ẩn khóa học';
$string['course:visible'] = 'Hiển thị khóa học';
$string['course:restore'] = 'Khôi phục';
$string['course:restore_exe'] = 'Bạn có muốn khôi phục?';
$string['course:registered'] = 'Đã đăng ký';
$string['course:delete'] = 'Xóa';
$string['course:app'] = 'Chấp nhận';
$string['course:wait'] = 'Đang chờ';
$string['course:irregular_list'] = 'Danh sách khóa học cao cấp';
$string['course:participants_list'] = 'Tuyển sinh và danh sách sinh viên';
$string['course:open'] = 'Thời gian mở';
$string['course:completion'] = 'Hoàn thành';
$string['course:manage'] = 'Thêm quản trị';
$string['course:ctl'] = 'Trung tâm hỗ trợ dạy và học';
$string['course:other'] = 'Khác';
$string['course:major'] = 'Khoa';
$string['course:approval'] = 'Chờ phê duyệt';
$string['course:approval_cancel'] = 'Hủy phê duyệt';
$string['course:approval_delete'] = 'Xóa phê duyệt';
$string['course:enrol'] = 'Đăng ký';
$string['course:enrol_check'] = 'Đồng ý đăng ký?';
$string['course:section'] = 'Học phần/ định dạng khóa học';
$string['course:status'] = 'Trạng thái';
$string['course:ongoing'] = 'Đang thực hiện';
$string['course:finish'] = 'Kết thúc';
$string['course:thumnail'] = 'Hình ảnh khóa học';
$string['course:goal'] = 'Mục tiêu học tập';
$string['course:role'] = 'Quy ước';

$string['format:weeks'] = 'Tuần';
$string['format:topics'] = 'Chủ đề';
$string['format:oklass_grid'] = 'Thư viện';

$string['coursename:ko'] = 'Tên khóa học(KO)';
$string['coursename:en'] = 'Tên khóa học(EN)';

$string['edit'] = 'Sữa';
$string['cancel'] = 'Hủy';
$string['confirm'] = 'Xác nhận';
$string['success'] = 'Thành công';
$string['student'] = 'Học viên';
$string['students'] = 'Học viên';
$string['manage'] = 'Quản lý';
$string['always:operate'] = 'Hoạt động thường xuyên';

$string['merge:manage'] = 'Quảng lý lớp học';
$string['merge:sel'] = 'Tích hợp phân khúc';

$string['possible'] = 'Có thể';
$string['impossible'] = 'Không thể';

$string['subject:lang'] = 'Môn học KO/EN';
$string['subject:lang_input'] = 'Vui lòng chọn các khóa học KO/EN.';
$string['subject:ko'] = 'KO';
$string['subject:ko_input'] = 'Vui lòng chọn một tên khóa học (KO).';
$string['subject:en'] = 'EN';
$string['subject:en_input'] = 'Vui lòng chọn một tên khóa học (EN).';

$string['search'] = 'Tìm kiếm';
$string['search:keywords'] = 'Từ khóa';
$string['professor'] = 'GS.';

$string['sititon:loginpage'] = 'Sử dụng sau đăng nhập';
$string['sititon:guest'] = 'Người dùng khách không tồn tại';
$string['sititon:enrolled'] = 'Khóa học đã đăng ký';
$string['sititon:progress'] = 'Đang chờ phê duyệt';
$string['sititon:success'] = 'Hoàn thành đăng ký';
$string['sititon:apply'] = 'Đăng ký khoá học';

$string['apply:doyouwantto'] = 'Bạn có muốn đăng ký khóa học đã chọn?';
$string['sititon:doyouwantto'] = 'Bạn có muốn đăng ký khóa học đã chọn?';

$string['showperpage'] = 'Hiển thị {$a} mỗi trang';

$string['selectcourses'] = 'Chọn khóa học đăng ký';
$string['completedrequestaudit'] = 'Bạn đã hoàn thành đăng ký khóa học';
$string['completedrequestenrol'] = 'Bạn đã hoàn thành đăng ký khóa học';
$string['completedrequestcancel'] = 'Hủy bỏ thành công';
$string['couldnotfindenrolment'] = 'Không tìm thấy thông tin đăng ký của bạn';

$string['courseinfo'] = 'Thông tin khóa học';
$string['syllabus'] = 'Giáo trình';
$string['close'] = 'Đóng';

$string['viewcourse'] = 'Xem khóa học';

$string['year'] = '{$a} Năm';
$string['year:sel'] = 'Năm';
$string['year:now'] = 'Hiện tại';
$string['term'] = '{$a} Kỳ';
$string['gubun'] = '{$a}';
$string['term:all'] = 'Tất cả kỳ';
$string['term:sel'] = 'Kỳ';
$string['term:current'] = 'Kỳ hiện tại';
$string['date:execute'] = 'Ngày thực hiện';

$string['constant'] = 'Cố định';
$string['section'] = 'Phần';
$string['no'] = 'No';
$string['major'] = 'Khoa';
$string['students'] = 'Học viên';
$string['user:nomove'] = 'Không có người dùng để di chuyển';
$string['user:move'] = 'Người dùng đã được di chuyển.';
$string['user:execute'] = 'Người thực hiện';
$string['course_notice'] = 'Thông báo khóa học';

$string['always:operate'] = 'Luôn mở';

$string['merge:manage'] = 'Quản lý khóa học';
$string['merge:sel'] = 'Tích hợp phân khúc';

$string['possible'] = 'Có thể';
$string['impossible'] = 'Không thể';

$string['subject:lang'] = ' Môn học KO/EN';
$string['subject:lang_input'] = 'Vui lòng chọn khóa học KO/EN';
$string['subject:ko'] = 'KO';
$string['subject:ko_input'] = 'Vui lòng chọn tên khóa học (KO).';
$string['subject:en'] = 'EN';
$string['subject:en_input'] = 'Vui lòng chọn tên khóa học (EN).';

$string['search'] = 'Tìm';
$string['search:keywords'] = 'Nhập từ khóa';
$string['prof'] = 'GS.';
$string['professor'] = 'Giáo sư';
$string['professorname'] = 'Giáo sư : {$a}';

$string['sititon:loginpage'] = 'Sử dụng sau khi đăng nhập';
$string['sititon:guest'] = 'Người dùng khách không tồn tại';
$string['sititon:enrolled'] = 'Đã đăng ký khóa học';
$string['sititon:progress'] = 'Yêu cầu phê duyệt';
$string['sititon:success'] = 'Hoàn tất đăng ký';
$string['sititon:apply'] = 'Đăng ký khóa học';

$string['apply:doyouwantto'] = 'Ban có muốn đăng ký khóa học đã chọn?';
$string['sititon:doyouwantto'] = 'Ban có muốn đăng ký khóa học đã chọn?';

$string['showperpage'] = 'Hiển thị {$a} mỗi trang';

$string['selectcourses'] = 'Chọn khóa học đăng ký';
$string['completedrequestaudit'] = 'Đăng ký khóa học thành công';
$string['completedrequestenrol'] = 'Đăng ký khóa học thành công';

$string['courseinfo'] = 'Thông tin khóa học';
$string['syllabus'] = 'Giáo trình';
$string['close'] = 'Đóng';

$string['viewcourse'] = 'Xem khóa học';

$string['year'] = '{$a} Năm';
$string['year:sel'] = 'Năm';
$string['year:now'] = 'Hiện tại';
$string['term'] = '{$a} Kỳ';
$string['term:sel'] = 'Kỳ';
$string['term:current'] = 'Kỳ hiện tại';
$string['date:execute'] = 'Ngày thực hiện';

$string['constant'] = 'Cố định';
$string['section'] = 'Phần';
$string['no'] = 'No.';
$string['univ'] = 'Đại học';
$string['major'] = 'Chính quy';
$string['student'] = 'Học viên';
$string['students'] = 'Học viên';
$string['user:nomove'] = 'Không có người dùng để di chuyển';
$string['user:move'] = 'Người dùng đã di chuyển thành công';
$string['user:execute'] = 'Người thực hiện';
$string['user:number'] = 'Số';
$string['user:name'] = 'Tên';
$string['user:major'] = 'Liên kết';
$string['user:email'] = 'Thư điện tử';
$string['user:complete_status'] = 'Trạng thái hoàn thành';
$string['user:complete_success'] = 'Hoàn thành khóa học';
$string['user:complete_cancel'] = 'Hủy thành công';
$string['user:complete'] = 'Hoàn thành';
$string['user:incomplete'] = 'Không hoàn thành';
$string['user:empty'] = 'Không có người dùng đăng ký';
$string['user:search'] = 'Tìm kiếm mã / tên học viên';
$string['user:sel_empty'] = 'Không có học viên kiểm tra';
$string['user:unenrol'] = 'Bạn đã xóa người dùng';
$string['user:info'] = 'Thông tin người dùng';

$string['college'] = 'Khoa';
$string['college:sel'] = 'Vui lòng chọn Khoa';
$string['subject:college_input'] = 'Vui lòng chọn Khoa';
$string['student_num'] = 'Tham gia một lớp học';
$string['subject:student_num_input'] = 'Vui lòng nhập số lượng học viên cho khóa học';
$string['student_num:limit'] = 'Nếu nhập 0, số lượng học viên không giới hạn';
$string['course:stunum_input'] = 'Học viên, bạn chỉ có thể nhập số.';
$string['request'] = 'Đăng ký';
$string['subject:request_input'] = 'Vui lòng chọn phương thức đăng ký';
$string['grade'] = 'Bậc';
$string['grade:score'] = '{$a} bậc';
$string['subject:grade_input'] = 'Vui lòng chọn bậc';
$string['arrival'] = 'Sắp xếp hàng đợi';
$string['approval'] = 'Chấp nhận';
$string['coruse:summary'] = 'Tóm tắt khóa học';
$string['end_date'] = 'Ngày kết thúc';
$string['export:excel'] = 'Tải tập tin Excel';

$string['complete:history_list'] = 'Lịch sử hoàn thành';
$string['complete:history_detail'] = 'Chi tiết lịch sử hoàn thành';
$string['complete:coruse_count'] = 'Hoành thành khóa học';
$string['complete:grade_sum'] = 'Tổng số tín chỉ';
$string['complete:detail'] = 'Chi tiết';
$string['complete:show'] = 'Hiển thị';
$string['complete:grade'] = 'Tính chỉ hoàn thành';
$string['complete:grade_frm'] = 'Tính chỉ hoàn thành : {$a}';
$string['complete:empty'] = 'Không có bài giảng';
$string['complete:certificate'] = 'Chứng chỉ khóa học';

$string['issue'] = 'Vấn đề';
$string['employment_support'] = 'Hỗ trợ việc làm';
$string['print'] = 'In';
$string['return'] = 'Trở lại';

$string['certificate:sel'] = 'Vấn đề cấp chứng chỉ';
$string['certiform:selko'] = 'Chọn mẫu chứng chỉ (korean)';
$string['certiform:selen'] = 'Chọn mẫu chứng chỉ (english)';
$string['sel:yes'] = 'Có';
$string['sel:no'] = 'Không';

$string['progress'] = 'Tiến trình / Lịch biểu bài giảng';
$string['progress_list'] = 'Danh sách tiến trình /  lịch biểu bài giảng';
$string['lastlecture'] = 'Bài giảng trước';
$string['lastlecture_list'] = 'Danh sách bài giảng trước';
$string['employment'] = '* được cấp tín dụng áp dụng cho công việc của Bộ phận hỗ trợ';

$string['hakbu'] = 'Khoa';
$string['graduate'] = 'Tốt nghiệp';

$string['course:alert01'] = 'Vui lòng chọn năm';
$string['course:alert02'] = 'Vui lòng chọn thể loại';
$string['course:alert03'] = 'Vui lòng chọn tên bài giảng (Korean).';
$string['course:alert04'] = 'Vui lòng chọn tên bài giảng (English).';
$string['course:alert05'] = 'Vui lòng chọn thời gian đăng ký';
$string['course:alert06'] = 'Vui lòng chọn thời gian mở';
$string['course:alert07'] = 'Vui lòng chọn hình thức nhận';
$string['course:alert08'] = 'Chỉ tồn tại các dạng ảnh (.jpg, .png)';
$string['course:alert09'] = 'Vui lòng chọn Thời gian nghe giảng';
$string['course:alert10'] = 'Vui lòng chọn tầng';
$string['course:alert11'] = 'Vui lòng chọn thời gian đăng ký';
$string['course:alert12'] = 'Vui lòng nhập Thời gian nghe giảng';
$string['course:alert13'] = 'Vui lòng chọn một giáo sư';
$string['professorsearch'] = 'Tìm giáo sư';
$string['cansel'] = 'Hủy';

$string['enrolment'] = 'Đăng ký';
$string['notenrolment'] = 'Chưa đăng ký';
$string['yes'] = 'Có';
$string['nono'] = 'Không';
$string['lecture'] = 'Bài giảng';
$string['lang'] = 'lang';
$string['empty:courseoverview'] = 'Không có thông báo';
$string['enrol_yes'] = 'Bạn có muốn phê duyệt?';
$string['enrol_no'] = 'Ban có muốn hủy phê duyệt?';

$string['course_check'] = 'Kiểm tra khóa học';