<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot."/local/courselist/lib.php";

$context = context_system::instance();


$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("과정안내");
$PAGE->navbar->add("직무교육");

echo $OUTPUT->header();

$courseid = optional_param("courseid", "", PARAM_INT);
$boardid=get_review_boardid();
$reviews = get_course_reviews($page-1,$perpage,$boardid,$id);
$totalcount = get_course_reviews_count($boardid);

$sql = "select  * from {lmsdata_course} where courseid=$courseid";
$content = $DB->get_record_sql($sql);
 
$sql2="select codename from {lmsdata_code} where id=$content->coursegrade and depth=2";
$grade = $DB->get_field_sql($sql2);
    
$sql3="select codename from {lmsdata_code} where id=$content->courserating and depth=2";
$rate = $DB->get_field_sql($sql3);

$sql4="select lmsuserid from {lmsdata_course_teacher} where courseid=$content->courseid";
$teacher= $DB->get_records_sql($sql4);

$sql5 = "select * from {lmsdata_class} lcl where courseid=$courseid";
$class= $DB->get_record_sql($sql5);

?>  

<style>
    div {margin-bottom:1000px;}
</style>


<h3 class="page_title">
    직무교육 
    <span class="r_content">
        <a href='<?php echo $CFG->wwwroot?>/local/courselist/course_list.php'><input type="button" class="btn h30 orange" value="목록으로" /></a>
    </span>
</h3>   

<div class="wrap-contentsbox">
<div class="colorbox">
    <div class="l_content">
        <img src="/theme/oklassedu/pix/images/sample.jpg" alt="" width="260px" height="145px" />
        <div class="buttons">
            <input type="button" value="맛보기" class="btn tab_btn m-col2" />
            <input type="button" value="수강신청" class="btn tab_btn orange m-col2" />

        </div>
    </div>
    <div class="l_content">
        <h4><?php echo $content->coursename?></h4>
        <dl>
            <dt>교육종별</dt>
            <dd>직무교육/<?php echo $grade?></dd>
            <dt>강사</dt>
            <dd><?php 
            foreach($teacher as $tea){//강사 정보 가져옴
                $sql5 = "select eng_name from {lmsdata_user} where id=$tea->lmsuserid";
                $result = $DB->get_field_sql($sql5);
                echo $result;
            ?>
                &nbsp;&nbsp;
                <?php
            }
            $price=$content->price*(1-0.01*$content->memberdiscount);
            ?></dd>
            <dt>모집인원</dt>
            <dd><?php echo $content->classlimit?></dd>
            <dt>수강료</dt>
            <dd>63,000원(10%조합지원) <?php echo number_format($price);?> 원 <?php if($content->memberdiscont){
                echo '('.$content->memberdiscount.'%)';
            }else if($content->membergroupcount){
                echo '('.$content->membergroupcount.'%)';
            }else{
                echo '';
            }?></dd>
            <dt>수강신청기간</dt>
            <dd>17/05/30`17/07/03 <?php echo date('y-m-d',$class->enrolmentstart);?> ~ <?php echo date('y-m-d',$class->enrolmentend);?></dd>
            <dt>학습기간</dt>
            <dd>17/07/04~17/08/07(4주/17시간)<?php echo date('y-m-d',$class->learningstart);?> ~ <?php echo date('y-m-d',$class->learningend);?> &nbsp;(<?php echo $content->week?> / <?php echo $content->hours?>)</dd>
        </dl>
    </div>
</div>

<div class="tab_header">
    <span class="m-col3"><a href="#list01" onclick="fnMove('1')">강의정보</a></span>
    <span class="m-col3"><a href="#list02" onclick="fnMove('2')">교육내용</a></span>
    <span class="m-col3"><a href="#list03" onclick="fnMove('3')">교육후기</a></span>
</div>
<div class="nobg">
    <div class="on textlist">
        <h5 id="list01" class="orange" id="div1">강의소개</h5>
        <p>
            21세기 지식정보화 시대에서 필수적인 정보 활용능력. 그렇지만 지금 현재의 교육환경에서 교과수업 시간에 독서교육을 기반으로 한 정보활용능력을 키워주는것이 쉽지 않습니다. 이때 최대한 활용할 수 있는 방법 중 가장 좋은 것이 학교에 있는 도서관을 활용하는 것이지만, 도서관 자료, 인력 등을 가장 효과적으로 사용할 수 있는 활용방법을 잘 모르는 선생님들이 많습니다. 이 강좌는 도서관 활용방법에 대해 고민하고 있는 선생님, 학생들이 주체적으로 참여할 수 있는 독서활동에는 어떤 것이 있을 지에 대해 생각하고 있는 모든 선생님들을 위한 강좌입니다.
        </p>

        <h5 class="orange">평가방법</h5>
        <p>진도율50점, 온라인시험30점, 토론20점.</p>

        <h5 class="orange">이수기준</h5>
        <p>수료점수60점이상(100점기준), 수료를위한 최소진도율80%이상</p>

        <h5 class="orange">수강대상</h5>
        <p>효과적인 독서교육에 대해 고민하고 있는 유,초,중,고등의 모든교사, 교육전문가, 학부모님</p>

        <h5 class="orange">추천도서(개별 구매)</h5>
        <div class="book">
            <img src="/theme/oklassedu/pix/images/book.jpg" alt="book01" />
            <div class="binfo">
                <div class="l_content">
                    <h5>성장과 발달을 돕는 초등 평가 혁신</h5>
                    <p>
                        <span class="division">이형빈</span>
                        <span>맘에드림</span>
                    </p>
                    <p>
                        판매가: 15,500원
                    </p>
                </div>
                <div class="r_content buttons">
                    <input type="button" class="btn gray" value="인터파크 바로가기" />
                    <input type="button" class="btn gray" value="YES24 바로가기" />
                    <input type="button" class="btn gray" value="알라딘 바로가기" />
                    <input type="button" class="btn gray" value="교보문고 바로가기" />
                </div>
            </div>


        </div>

        <h5 id="list02" class="orange" id="div2">교육내용</h5>
        <dl>
            <dd>[Module1] 지금, 왜 독서교육 인가?</dd>
            <dd>1차시 정답이 하나인 우리 교육 돌아보기</dd>
            <dd>2차시 자존감을 높여주는 책 읽기</dd>
            <dd>3차시 지식정보화 시대, 읽고 또 읽어라</dd>
            <dd>4차시 "인성과 밥, 독서교육에서 대안 찾기"</dd>
        </dl>
        <dl>
            <dd>[Module2] 독서교육, 프로젝트 수업으로 풀어가기</dd>
            <dd>5차시 도서관을 활용하는 교과연계수업 - 고전시가</dd>
            <dd>6차시 도서관을 활용하는 통합주제수업 - 환경</dd>
            <dd>7차시 도서관을 활용하는 창체활동</dd>
            <dd>8차시 책과 함께하는 자유학기제</dd>
        </dl>
        <dl>
            <dd>[Module3] 독서교육, 도서관 수업으로 풀어가기</dd>
            <dd>9차시 수업에 도서관 100% 활용하기</dd>
            <dd>10차시 사서교사와 도서관 200% 활용하기</dd>
            <dd>11차시 정보역량을 키우는 도서관 300% 활용하기</dd>
            <dd>12차시 도서관 활용수업, 소통과 협력!</dd>
            <dd>13차시 학교를 넘어 마을로, 사람책 만나기!</dd>
        </dl>
        <dl>
            <dd>[Module4] 독서교육, 동아리로 풀어가기</dd>
            <dd>14차시 책도 얻고 친구도 얻는 독서동아리 만들기</dd>
            <dd>15차시 전국에서 꽃핀 독서동아리</dd>
            <dd>16차시 학교 교육, 진로독서동아리가 답!</dd>
            <dd>17차시 "누구나 쉽게 말문을 여는 독서토론 방법"</dd>
        </dl>

    </div>
</div>
<h5 id="list03" class="div_title" id="div3">
    교육후기
    <p class="r_content">
        <a href="<?php echo $CFG->wwwroot?>"><input type="button" value="후기작성" name="btn01" class="btn orange h30" /></a>
    </p>
</h5>

<table class="table">
    <thead>
        <tr>
            <th>No.</th>
            <th>제목</th>
            <th>작성자</th>
            <th>등록일</th>
            <th>조회수</th>
        </tr>
    </thead>

    <tbody>
            <?php foreach($reviews as $review){
                $offset = 0;
            if ($page != 0) {
                $offset = ($page - 1) * $perpage;
            }
            $num = $totalcount - $offset;
        
            $userid = $review->userid;
            $username = get_review_username($userid);
            
            ?>
        
        <tr>
            <td><?php echo $num;?></td>
    <td class="text-left"><a href="<?php echo $CFG->wwwroot?>/local/jinoboard/detail_uncore.php?id=<?php echo $review->id?>"><?php echo $review->title?></a></td>
            <td><?php echo $username?></td>
            <td><?php echo date('Y-m-d', $review->timecreated)?></td>
            <td><?php echo $review->viewcnt?></td>
        </tr>
<?php
$num--;
}
?>
    </tbody>
</table>
</div>








<?php
echo $OUTPUT->footer();
?>


<script>
    function write_review(){
        window.location.href("<?php echo $CFG->wwwroot?>/local/jinoboard/write_uncore.php?board=6");
    }
    
    function fnMove(seq){
        var offset = $("#div" + seq).offset();
        $('html, body').animate({scrollTop : offset.top}, 400);
    }


    
    // 처음 문서를 열었을 때 Top 버튼 위치 설정

$(document).ready(function () {

        //var currentHeight = $(window).scrollTop() + 300; 

  var currentHeight = $(window).scrollTop() + ($(window).height() - 430); 

        $(".btn-top-2").css("top", currentHeight);

    })

// 스크롤시 Top버튼 위치 변경

    $(window).scroll(function () {

        var divHeight = $(".wrap-contentsbox").height();

        // var currentHeight = $(window).scrollTop() + 300; 300을 더한 이유는 위치가 항상 밑에서 30px 정도 높게 

        //나오길 원해서 브라우저 height 값이 바뀔 경우 제대로 대응하지 못함

var currentHeight = $(window).scrollTop() + ($(window).height() - 430);

      // 해당 Div의 높이를 넘어가지 않도록 설정,  

if (divHeight > currentHeight) {

            $(".btn-top-2").animate({ top: currentHeight + "px" }, { queue: false, duration: 300 });

        }

// 넘어갈 경우 div 값으로 고정

        else {

            $(".btn-top-2").animate({ top: divHeight + "px" }, { queue: false, duration: 300 });

        }

    });


    </script>