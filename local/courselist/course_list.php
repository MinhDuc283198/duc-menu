<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot."/local/courselist/lib.php";

$context = context_system::instance();


$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add("과정안내");
$PAGE->navbar->add("직무교육");

echo $OUTPUT->header();

$boardid=get_review_boardid();
$gubun = optional_param("gubun",1,PARAM_INT);
function get_course_codename($id){
    global $DB;
    $sql="select codename from {lmsdata_code} where id=$id";
    return $DB->get_field_sql($sql);
}
function get_course_teacher($userid){
    global $DB;
    $sql = "select eng_name from {lmsdata_user} where userid=$userid";
    return $DB->get_field_sql($sql);
}
function get_c_cate(){   //course_categories db에서 가져옴
    global $DB;    
    $sql="select * from {course_categories} where depth=2";
    return $DB->get_records_sql($sql);
}
function get_c_grade(){   //학점 코드값 db에서 가져옴 코드분류=grades
    global $DB;    
    $sql="select * from {lmsdata_code} where code_category='grades' and depth=2";
    return $DB->get_records_sql($sql);
}
function get_c_rate(){   //급별 코드값 db에서 가져옴 코드분류=class
    global $DB;    
    $sql="select * from {lmsdata_code} where code_category='class' and depth=2";
    return $DB->get_records_sql($sql);
}
function get_ccate($id){
    global $DB;
    $sql="select name from {course_categories} where id=$id";
    return $DB->get_field_sql($sql);
}
?>  


<h3 class="page_title">상시과정</h3>   
<div class="textbox">
상시과정에 대한 설명이 이곳에 노출됩니다.
</div>
<div class="tab_header"><!-- 학점:score=1 / 주제별:title=2 / 급별:grade=3 -->
    <?php
    if($gubun==1){
        ?>
        <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=1"><span class="on m-col3">학점별</span></a>
    <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=2"><span class="m-col3">주제별</span></a>
    <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=3"><span class="m-col3">급별</span></a>
    <?php
    }
    ?>
        <?php
    if($gubun==2){
        ?>
        <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=1"><span class="m-col3">학점별</span></a>
    <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=2"><span class="on m-col3">주제별</span></a>
    <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=3"><span class="m-col3">급별</span></a>
    <?php
    }
    ?>
        <?php
    if($gubun==3){
        ?>
        <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=1"><span class="m-col3">학점별</span></a>
    <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=2"><span class="m-col3">주제별</span></a>
    <a href="<?php echo $CFG->wwwroot?>/local/courselist/course_list.php?gubun=3"><span class="on m-col3">급별</span></a>
    <?php
    }
    ?>

</div>



    <?php 
    
if($gubun==1){  //학점별
    $c_grade = get_c_grade();
        foreach($c_grade as $cg){
            ?>
<div class="tableblock">
<h5 class="div_title"><?php echo $cg->codename;?></h5>

<table class="table table-condensed">
<thead>
    <tr>
        <th>과정명</th>
        <th>학점</th>
        <th>주제</th>
        <th>급별</th>
        <th>교육비</th>
        <th>미리보기</th>
        <th>신청</th>
    </tr>
</thead>
    <?php    

    global $DB;

    $sql="select * from {lmsdata_course} where coursegrade=$cg->id";
    
    $courses = $DB->get_records_sql($sql);
?>

<?php
if(!$courses){
echo '<tr><td colspan="7">등록된 교육 과정이 없습니다.</td></tr>';
}else{
    foreach($courses as $course){
        $area=get_ccate($course->coursearea);
        $grade=get_course_codename($course->coursegrade);
        $rate=get_course_codename($course->courserating);
        ?>
<tr>
<tbody>
        
            <?php
                if($courses){?>
        <td class="text-left"><a href="<?php echo $CFG->wwwroot?>/local/courselist/course_info.php?courseid=<?php echo $course->courseid?>"><?php echo $course->coursename?></a></td>
        <td><?php echo $grade;?></td>
        <td><?php echo $area;?></td>
        <td><?php echo $rate;?></td>
        <td><?php echo number_format($course->price)?>원</td>
        <td><input type="button" class="btn brd" name="btn01" value="미리보기"></td>
        <td><input type="button" class="btn brd orange" value="수강신청" name="btn02"></td>
        <?php
    }
    
}
            ?>
        
        
        
    </tr>
    </tbody>
    <?php
    }
    ?>
    
</table>
</div>
<br><br>
    <?php
        }
    }
    ?>





    <?php 
    
if($gubun==2){  //주제별
    $c_category = get_c_cate();
        foreach($c_category as $cc){
?>
<div class="tableblock">
<h5 class="div_title"><?php echo $cc->name;?></h5>
<table class="table table-condensed">
            <tr>
        <th>과정명</th>
        <th>학점</th>
        <th>주제</th>
        <th>급별</th>
        <th>교육비</th>
        <th>미리보기</th>
        <th>신청</th>
    </tr>
    <?php    
    global $DB;

    $sql="select * from {lmsdata_course} where coursearea=$cc->id";
    
    $courses = $DB->get_records_sql($sql);

    ?>

    <?php
    if(!$courses){
    echo '<tr><td colspan="7">등록된 교육 과정이 없습니다.</td></tr>';
    }else{
    foreach($courses as $course){
        $area=get_ccate($course->coursearea);
        $grade=get_course_codename($course->coursegrade);
        $rate=get_course_codename($course->courserating);
        ?>
    
    <?php
                if($courses){
        ?>
        <tr>
        <td class="text-left"><a href="<?php echo $CFG->wwwroot?>/local/courselist/course_info.php?courseid=<?php echo $course->courseid?>"><?php echo $course->coursename?></a></td>
        <td><?php echo $grade;?></td>
        <td><?php echo $area;?></td>
        <td><?php echo $rate;?></td>
        <td><?php number_format($course->price)?>원</td>
        <td><input type="button" class="btn brd" name="btn01" value="미리보기"></td>
        <td><input type="button" class="btn brd orange" value="수강신청" name="btn02"></td>
        
                    <?php
    }
    
    }
            ?>
        

        
    </tr>
    <?php
    }
    ?>
    
</table>
</div>
<br><br>
    <?php
        }
    }
?>


    <?php 
    
if($gubun==3){  //급별
    $c_rate = get_c_rate();
        foreach($c_rate as $cr){
?>
<div class="tableblock">
<h5 class="div_title"><?php echo $cr->codename;?></h5>
<table class="table table-condensed">
            <tr>
        <th>과정명</th>
        <th>학점</th>
        <th>주제</th>
        <th>급별</th>
        <th>교육비</th>
        <th>미리보기</th>
        <th>신청</th>
    </tr>
    <?php    

    global $DB;

    $sql="select * from {lmsdata_course} where courserating=$cr->id";
    
    $courses = $DB->get_records_sql($sql);
   ?> 
       <?php
       if(!$courses){
 echo '<tr><td colspan="7">등록된 교육 과정이 없습니다.</td>'; 
       }else{
    foreach($courses as $course){
        $area=get_ccate($course->coursearea);
        $grade=get_course_codename($course->coursegrade);
        $rate=get_course_codename($course->courserating);
        ?>
    
    <?php
                if($courses){
        ?>
        <tr>                  
        <td class="text-left"><a href="<?php echo $CFG->wwwroot?>/local/courselist/course_info.php?courseid=<?php echo $course->courseid?>"><?php echo $course->coursename?></a></td>
        <td><?php echo $grade;?></td>
        <td><?php echo $area;?></td>
        <td><?php echo $rate;?></td>
        <td><?php echo number_format($course->price)?>원</td>
        <td><input type="button" class="btn brd" name="btn01" value="미리보기"></td>
        <td><input type="button" class="btn brd orange" value="수강신청" name="btn02"></td>  
          <?php          
    }
    }
            ?>

        
        
    </tr>
    <?php
    }
    ?>
    
</table>
</div>
<br><br>
    <?php
        }
    }
    ?>

