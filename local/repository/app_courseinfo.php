<?php

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
/* Lim */
// 타입
$type = required_param('type', PARAM_RAW);
//콘텐츠 아이디
$contentid = required_param('contentid', PARAM_INT);
//기타 데이타 
$etc = optional_param('etc', '', PARAM_RAW); // courseid=

$array = array('courseid' => 29, 'cmid' => 785, 'userid' => 2, 'sessionid' => sesskey());
$etc = json_encode($array);

$etc = json_decode($etc);

if ($contentid) {
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $contentid))) {
        print_error('lcms contents is incorrect');
    }
    if (!$file = $DB->get_record('lcms_contents_file', array('con_seq' => $contentid))) {
        print_error('lcms contents file is incorrect');
    }
}


// courseid , cm->name ,  , userid , sessionid = sesskey(); end = date('Ymdhis');

if ($etc->cmid) {
    if (!$cm = get_coursemodule_from_id('lcms', $etc->cmid)) {
        print_error('30 Line Course Module ID was incorrect => 잘못된 CMID');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('33 Line course is misconfigured => 잘못된 CMID');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('36 Line course module is incorrect => 잘못된 CMID');
    }
    $conname = $lcms->name;
} else {
    $conname = $contents->con_name;
}

$data = new stdClass();
$data->content = new stdClass();
$data->result = new stdClass();
if ($file) {
    $data->content->tracking = $CFG->wwwroot . '/local/repository/app_progress.php';
    $data->content->tracking_interval = 60;
    $data->content->serviceid = 'SNUT';
    $data->content->servicename = '서울과기대';

    $file->filename = str_replace('.mp4', '_hd.mp4', $file->filename);

    $data->content->vodurl = $CFG->vodserver . '/uploads/' . $file->filepath . '/' . str_replace(' ', '', $file->filename);
    $data->content->vodfilename = $file->filename;

    $data->content->vodtitle = $conname;
    $data->content->vodtitlesub = $conname;
    $data->content->vodtime = 60;
    $data->content->type = 'play';
    if ($etc->userid) {
        $data->content->userid = $etc->userid;
    } else {
        $data->content->userid = 0;
    }
    $data->content->sessionid = $etc->sesskey;
    if ($lcms->timeend) {
        $data->content->end = date('Ymdhis', $lcms->timeend);
    } else {
        $data->content->end = 0;
    }
    $data->content->continue_time = 0;
    if ($etc->courseid) {
        $course = $DB->get_record('course', array('id' => $etc->courseid));
        $data->content->course_no = $course->id;
        $data->content->course_name = $course->fullname;
    } else {
        $data->content->course_no = null;
        $data->content->course_name = null;
    }
    $data->content->caption_url = null;


    $data->result->code = 200;
    $data->result->message = 'success';
} else {
    $data->result->code = 601;
    $data->result->message = '"콘텐츠 정보에 문제가 있습니다.\n관리자에게 문의하세요."';
}
@header('Content-type: application/json; charset=utf-8');
echo json_encode($data);

/*
 *  jinomobile://?type=play&contentid=[식별코드]&etc=[ETC데이터]  호출
 * 
 * 
 * 
 *  jinomobile://?type=play&contentid=wpfdjk3gjskdflYbslfWERjsRsdfkjwR&etc=Hwerlskr4bs 
 *   contentid = 
 *   etc =
 * 
 *   http://m.jinotech.kr/api/contentinfo?type=play&contentid=[식별코드]&etc=[ETC 데이터]
 *   type: play  
 *   contentid: 컨텐츠 식별코드
 *   etc: 서버에서 필요한 기타 파라미터
 */