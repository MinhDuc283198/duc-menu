<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once $CFG->dirroot.'/mod/lcms/lib.php';
/* Lim */
$type = required_param('type', PARAM_RAW);
$contentid = required_param('contentid', PARAM_INT);
$etc = optional_param('etc', '', PARAM_RAW); 

$array = array('courseid'=>29,'cmid'=>785,'userid'=>2,'sessionid'=>sesskey());
$etc = json_encode($array);

$etc = json_decode($etc);

$event = optional_param('event', '', PARAM_RAW); // pause,seek,finish,progress

$insflag = required_param('insflag', PARAM_RAW);  // (최초값:S , 진행중값:M , 종료값:E )

$user_id = required_param('user_id',PARAM_INT);

$positionfrom = optional_param('positionfrom', 0, PARAM_INT);
$positionto = optional_param('positionto', 0, PARAM_INT);

if($etc->cmid){
   if (!$cm = get_coursemodule_from_id('lcms', $etc->cmid)) {
        print_error('30 Line Course Module ID was incorrect => 잘못된 CMID');
    }
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('33 Line course is misconfigured => 잘못된 CMID');
    }
    if (!$lcms = $DB->get_record('lcms', array('id' => $cm->instance))) {
        print_error('36 Line course module is incorrect => 잘못된 CMID');
    }
    $conname = $lcms->name;
} else {
    $conname = $contents->con_name;
}

$duration = optional_param('duration', 0, PARAM_INT);

$track = $DB->get_record('lcms_track',array('lcms'=>$lcms->id,'userid'=>$user_id));


 if($user_id && $positionfrom < $positionto){
        $lcms = new stdClass();
        $lcms->userid = $user_id;
        $lcms->lcmsid = $lcms->id;
        $lcms->rtype = '';
        $lcms->positionpage = 1;
        
        $lcms->positionto = $positionfrom;
        $lcms->positionfrom = $positionto;
        
        $lcms->timereg = time();
        
        if($lcms->positionto>0&&$duration>1){
            
            if($cour->duration!=$duration){
                //$cour->duration = $duration;
                //$DB->update_record('lcms',$cour);
            }
            
            $query = 'select count(*) from {lcms_playtime} 
                where userid=:userid and lcmsid=:lcmsid and positionpage=:positionpage 
                and positionfrom<=:positionfrom and positionto>=:positionto';
            $params = array('userid'=>$user_id,'lcmsid'=>$lcmsid,'positionpage'=>$lcms->positionpage,
                'positionto'=>$lcms->positionto,'positionfrom'=>$lcms->positionfrom);
            $playcount = $DB->count_records_sql($query,$params);
            
            //플레이시간을 저장한다. 
            if($playcount==0) $DB->insert_record('lcms_playtime',$lcms);
            
            if($cour->duration!=$duration){
                $cour->duration = $duration;
                $DB->update_record('lcms',$cour);
            }
            
            if(!$track->id){
                $track = new stdClass();
                $track->lcms = $lcmsid;
                $track->userid = $user_id;
                $track->attempts = 1;
            }
            $track->lasttime = $lcms->positionto;
            $track->lastpage = $lcms->positionpage;
            $track->playtime = lcms_get_progress($lcmsid,$user_id);
            $track->playpage = lcms_get_progress_page($lcmsid,$user_id);
            if($lcms->rtype==''){
                if($duration>1) $track->progress = round($track->playtime/$duration*100);
            }else{
                if($duration>1) $track->progress = round($track->playpage/$duration*100);
            }
            $track->timeview = time();
            if($track->id){
                $DB->update_record('lcms_track',$track);
            }else{
                $DB->insert_record('lcms_track',$track);
            }
            
            //lcmsattend_update_attendance_score($cour->course, $user_id);
            lcmsprogress_update_progress_score($cour->course, $user_id);
            
            $returnvalue->last = date('Y-m-d H:i:s',$track->timeview);
            $ptm = time_from_seconds($track->playtime);
            $returnvalue->totaltime = $ptm->h.':'.$ptm->m.':'.$ptm->s;
            $returnvalue->totalpage = $track->playpage;
            $returnvalue->progress = $track->progress.' %';
            
        }
    }



$returnvalue = new stdClass();
$returnvalue->status = 'success';

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);