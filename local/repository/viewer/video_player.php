
<?php   
require_once '../../../config.php';
require_once($CFG->dirroot . "/lib/filelib.php");
require_once $CFG->dirroot . '/local/repository/lib.php';
require_once $CFG->dirroot . '/local/repository/config.php';
require_once $CFG->dirroot . '/local/management/contentsI/lib.php';

require_login();

$id = required_param('id', PARAM_INT);
$qua = optional_param('qua', 0, PARAM_INT);
$useseek = optional_param('useseek', 'use', PARAM_RAW);
$site = optional_param('site', "", PARAM_RAW);

$context = context_system::instance();

$PAGE->set_url('/chamktu/contents/viewer/video_player.php', array('id' => $id));

if ($id) {
    if (!$contents = $DB->get_record('lcms_contents', array('id' => $id))) {
        print_error('lcms contents is incorrect');
    }
    if (!$file = $DB->get_record('lcms_contents_file', array('con_seq' => $id))) {
        print_error('lcms contents file is incorrect');
    }
} else { 
    print_error('missingparameter');
}

$auto_play = 0;
$positionname = strrpos($file->filename, '.');
$namearray = explode('.', $file->filename);
$realfilename =substr_replace($file->filename, '_hd', $positionname, 0);

$n = count($namearray);
unset($namearray[$n - 1]);
$capture = implode('', $namearray) . '.png';
//자막파일영역
$subtitles = get_subtitle_list($context->id, $contents->id);

$sturl = get_config('moodle', 'vodurl');
$trcurl = get_config('moodle', 'transcodingurl');
$sturl = $CFG->vodurl;
                


                $sql = "SELECT rep.id, lcf.filename, lcf.filepath, lcf.duration, "
        . "con.id as con_id, con.con_name, con.con_type, con.con_des, "
        . "con.update_dt, con.data_dir, con.embed_type, con.embed_code "
        . "FROM {lcms_repository} rep "
        . "JOIN {lcms_contents} con ON con.id= rep.lcmsid "
        . "LEFT JOIN {lcms_contents_file} lcf ON con.id = lcf.con_seq "
        . "LEFT JOIN {lcms_track} lt ON rep.lcmsid = con.id = lt.lcms "
        . "WHERE con.id= :id";
$data = $DB->get_record_sql($sql, array('id' => $id));

                $transcodingurl = get_config('moodle','vodurl');
                //동영상 콘텐츠 보기
                
                //$file = $DB->get_record('lcms_contents_file', array('con_seq' => $id));
                $viewer_url = $transcodingurl.$data->data_dir.'/'.$file->filename;
if($contents->con_type == 'visangurl'){
    $playtyme = "application/x-mpegurl";
    $viewer_url = visang_cdn_tokenmaker($contents->embed_code);
    
}else{
    $playtyme = "video/mp4";
}
?>

<!DOCTYPE html>
<html>
    <head>
        <style>
            #hlsjsvod .fp-logo{
                width: 150px;
                padding-left: 10px;
            }
            .flowplayer .fp-header .fp-share { display: none; }
        </style>
        <link rel="stylesheet" href="<?php echo $CFG->wwwroot . '/chamktu/contents/viewer/flowplayer7.2.7/skin/skin.css' ?>">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width,  target-densityDpi=device-dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script src="<?php echo $CFG->wwwroot . '/lib/javascript.php/1548723315/lib/jquery/jquery-1.12.1.min.js' ?>"></script>
        <script src="<?php echo $CFG->wwwroot . '/chamktu/contents/viewer/flowplayer7.2.7/flowplayer.min.js' ?>"></script>
        <script src="<?php echo $CFG->wwwroot . '/chamktu/contents/viewer/flowplayer7.2.7/flowplayer.hlsjs.min.js' ?>"></script>
        <script src="../../../mod/lcms/player/js/jwplayer.js"></script>
        <script src="../../../local/repository/viewer/jwplayer/js/jwplayer.js"></script>
        <script type="text/javascript">
               
            ///////////
            var positionfrom = 0, positionto = 0, saveposition = 0, oldposition = 0, ispause = false, capposition = 0, saveposition = 0, evented = 0;
            var dbduration = '<?php echo $contentsfile->duration; ?>';
            var videourl = '<?php echo $play_url ?>';

            var firstseek = 0;
            var duration = 0, timestamps = [], timer;
            ///////////


            var server = '<?php echo $sturl; ?>';
            var storage = '<?php echo $file->filepath; ?>';
            var vodname = '<?php echo $realfilename; ?>';
            var imagename = '<?php echo $capture; ?>';
            var ccmark = '';
            var height = '500';
            var transurl = '<?php echo $trcurl; ?>';

            var hlsurls = '<?php echo $viewer_url?>';
//            var videourl = server + '/' + storage + '/' + vodname;
            var url = '<?php echo $viewer_url?>';
            // $transcodingurl.'/jmedia/'.$data->data_dir.'/'.$file->filename;


            window.onload = function () {
                <?php if($site != "siteadmin"){?>
                positionto = video_playtime_get();

//                if (trackstatus != 0 && positionto != 0) {
                if (positionto != 0) {
                    if (confirm("<?php echo get_string('okmedia:play_confirm', 'local_management'); ?>")) {
                        positionfrom = positionto;
                        video_flowplayer_load(videourl);
                    } else {
                        positionfrom = 0;
                        video_flowplayer_load(videourl);
                    }
                } else {
                    positionfrom = 0;
                    video_flowplayer_load(videourl);
                }
                <?php }?>
                    positionfrom = 0;
                    video_flowplayer_load(videourl);
                return false;

            };

            function video_flowplayer_load(videourl) {
                flowplayer("#hlsjsvod", {
                    key: "<?php echo get_config('local_repository','flowplayer_key')?>",
                    splash: false,
                    fullscreen: true,
                    nativesubtitles:true,
                    autoplay: true,
                    embed: false, // setup would need iframe embedding
                    native_fullscreen: true,
                    ratio: 9 / 16,
                    hlsQualities: "drive",                       
                    speeds: [0.75, 1, 1.25, 1.5],
                    hlsjs: {recoverNetworkError: true},
                    clip: {
                        sources: [
                            { type: '<?php echo $playtyme?>', src: url},
                        ]
                    }
                }).on("ready", function (e, api, video) {
                    api.seek(positionfrom);
//                    pop_progress();
                    console.log(video.duration);
                    duration = Math.floor(video.duration);
                    firstseek = 1;
                    if (dbduration == '' || !dbduration) {
                        setdbduration('<?php echo $id; ?>', duration);
                    }
                });
            }

            /* global event listeners for demo purposes, omit in production */
            flowplayer(function (api, root) {
                seekback = '<?php echo $useseek ?>';
                if (seekback == 'unuse') {
                    api.on("beforeseek", function (e) {
                        if (firstseek != 0) {
                            e.preventDefault();
                        }
                    });

                    $(".fp-buffer, .fp-progress", root).on("mousedown touchstart", function (e) {

                        e.stopPropagation();
                    });
                    $(root).removeClass("is-touch");
                }
                var instanceId = root.getAttribute("data-flowplayer-instance-id"),
                        engineInfo = document.getElementById("engine" + instanceId),
                        vtypeInfo = document.getElementById("vtype" + instanceId),
                        detail = document.getElementById("detail" + instanceId);
                api.on("resume", function (e) {
                    //플레이 버튼 클릭 시 호출
                    if (positionto > 1) {
                        positionfrom = positionto;
                    }
                    evented = 0;
                }).on("pause", function (e, api) {
                    //Pause 버튼 클릭 시 호출
                    if (positionfrom < positionto) {
                        video_playtime_update(positionfrom, positionto, duration, 1);
                    }
                }).on("beforeseek", function (e, api) {
                    //seek 버튼 클릭 시 호출
                    if (positionfrom < positionto) {
                        video_playtime_update(positionfrom, positionto, duration, 2);
                    }
                }).on("seek", function (e, api) {
                    //seek 버튼 클릭 후 seek되면 호출
                    if (positionto > 1) {
                        positionfrom = positionto;
                    }
                    evented = 0;
                }).on("finish", function (e, api) {
                    console.log('finish::' + positionfrom + '::' + positionto);
                    if (evented != 3 && positionfrom < duration) {
                        video_playtime_update(positionfrom, duration, duration, 3);
                    }
                    positionfrom = positionto = 0;
                    evented = 1;
                }).on("progress", function (e, api) {
                    //타임라인
                    positionto = Math.ceil(api.video.time);
                    // 진도율 저장 (10초마다 진도율을 저장함.)
                    if (evented == 0 && positionfrom < positionto && positionto % (10) == 0) {
                        video_playtime_update(positionfrom, positionto, duration, 5);
                        saveposition = positionto;
                        positionfrom = positionto;
                    }
                }).on("cuepoint", function (e, api, cuepoint) {
                    if (cuepoint.subtitle) {
                        oldposition = parseInt(cuepoint.time);
                        caption_scroll(oldposition);
                    }
                });
            });

            function video_playtime_get() {

                var frm = $(top.document).find('#playtime_form');
                frm.find('input[name=act]').val('get');
                var positionfrom = 0;
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    async: false,
                    data: {
                        act: 'get',
                        lcmsid: '<?php echo $id; ?>'
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (data.status == 'success') {
                            if (data.positionto > 0 && data.progress < 100) {
                                console.log($('video').length);
                                if ($('video').length > 0) {
                                    var v = document.createElement('video');
                                    var supported = v.canPlayType('video/mp4; codecs="avc1.58A01E, mp4a.40.2"');
                                    if (supported == 'probably') {
                                        $('#position').val(data.positionto);
                                    }
                                } else {
                                    positionfrom = data.positionto;
                                    return positionfrom;
                                }
                            }

                        } else {
                            //alert(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                        console.log('video_playtime_get');
                        console.log('textStatus : ' + textStatus);
                        console.log('errorThrown : ' + errorThrown);
                        console.log('jqXHR.status : ' + jqXHR.status);
                        console.log('jqXHR.statusText : ' + jqXHR.statusText);
                        console.log('jqXHR : ' + jqXHR.responseText);
                    }
                });
                return positionfrom;
            }

            /* end global event listeners setup */
            function closepopupFunction() {
                video_playtime_update_close(positionfrom, positionto, duration, <?php echo $id; ?>);
            }
            function video_playtime_update_close(posfrom, posto, duration, id, second) {
                var status = true;
                if (!second) {
                    second = 0;
                }
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    async: true,
                    data: {
                        act: 'update',
                        positionpage: 1,
                        positionfrom: posfrom,
                        positionto: posto,
                        duration: duration,
                        lcmsid: id
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            status = true;
                            $('#study_last_time').text(data.last);
                            $('#study_total_time').text(data.totaltime);
                            $('#study_rate').text(data.progress);
                        } else {
                            status = false;
                        }
                    },
                    error: function (e) {

                        //alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                        //playtime_update(id,posfrom,posto,duration,second);
                        console.log('video_playtime_update_close');
                        console.log('textStatus : ' + textStatus);
                        console.log('errorThrown : ' + errorThrown);
                        console.log('jqXHR.status : ' + jqXHR.status);
                        console.log('jqXHR.statusText : ' + jqXHR.statusText);
                        console.log('jqXHR : ' + jqXHR.responseText);
                        status = false;
                    }
                });
                return status;
            }
            function video_playtime_update(posfrom, posto, duration, second) {

                var status = true;
                if (!second) {
                    second = 0;
                }
                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    async: true,
                    data: {
                        act: 'update',
                        positionpage: 1,
                        positionto: posto,
                        positionfrom: posfrom,
                        duration: duration,
                        lcmsid: '<?php echo $id; ?>'
                    },
                    success: function (data, textStatus, jqXHR) {
                        var d = new Date();
                        if (data.status == 'success') {

                            status = true;
                            var lasttime = data.last;
                            var lltime = lasttime.substring(0,16);

                            $('#id_lastview', parent.document).text(lltime);
                            $('#id_playtime', parent.document).text(data.totaltime)
                            $('#id_progress', parent.document).text(data.progress);
                        } else {
                            status = false;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                        alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
                        //playtime_update(posfrom,posto,duration,second);
                        console.log('video_playtime_update');
                        console.log('textStatus : ' + textStatus);
                        console.log('errorThrown : ' + errorThrown);
                        console.log('jqXHR.status : ' + jqXHR.status);
                        console.log('jqXHR.statusText : ' + jqXHR.statusText);
                        console.log('jqXHR : ' + jqXHR.responseText);
                        status = false;
                    }
                });
                return status;
            }

            function setdbduration(conid, inputduration) {

                $.ajax({
                    url: '<?php echo $CFG->wwwroot; ?>/mod/lcms/playtime_ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    async: true,
                    data: {
                        act: 'setduration',
                        duration: inputduration,
                        conid: conid
                    },
                    success: function (data, textStatus, jqXHR) {
                        var d = new Date();
                        if (data.status == 'success') {
                            status = true;
                            duration = data.duration;
                        } else {
                            status = false;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

//                        alert('네트워크 연결이 일시적으로 중단되었습니다. 다시 시도하시기 바랍니다.');
//                        //playtime_update(posfrom,posto,duration,second);
//                        console.log('video_playtime_update');
//                        console.log('textStatus : ' + textStatus);
//                        console.log('errorThrown : ' + errorThrown);
//                        console.log('jqXHR.status : ' + jqXHR.status);
//                        console.log('jqXHR.statusText : ' + jqXHR.statusText);
//                        console.log('jqXHR : ' + jqXHR.responseText);
//                        status = false;
                    }
                });
            }
        </script>
        <style>
            html, body, #hlsjsvod{height: 100%;}
        </style>
    </head>
    <body style="margin:0;overflow:hidden;">
        <div id="hlsjsvod" class="is-closeable">
        </div>
    </body>
</html>