<?php
require_once '../../../config.php';
require_once($CFG->dirroot.'/mod/okmedia/lib.php');
require_once $CFG->dirroot.'/mod/lcmsprogress/locallib.php';
require_once $CFG->dirroot.'/mod/okmedia/locallib.php';
require_once $CFG->dirroot.'/mod/lcms/lib.php';
require_once $CFG->dirroot . '/local/repository/config.php';

global $DB, $USER;

$returnvalue = new stdClass();

$act = required_param('act', PARAM_RAW);
$duration = optional_param('duration', 0, PARAM_INT);
//$cour = $DB->get_record('lcms',array('id'=>$lcmsid));

if ($act == 'get') {
    $lcmsid = required_param('id', PARAM_INT);
    $track = $DB->get_record('lcms_track', array('lcms' => $lcmsid, 'userid' => $USER->id));

    if ($track->id) {
        $track->attempts = $track->attempts + 1;
        $track->timeview = time();
        $DB->update_record('lcms_track', $track);
    } else {
        if ($cour->type == 'Flash') {
            $play = new stdClass();
            $play->userid = $USER->id;
            $play->lcmsid = $lcmsid;
            $play->positionpage = 1;
            $play->positionto = 1;
            $play->positionfrom = 0;
            $play->timereg = time();

            $DB->insert_record('lcms_playtime', $play);
        }
        $track = new stdClass();
        $track->lcms = $lcmsid;
        $track->userid = $USER->id;
        $track->timeview = time();
        $track->attempts = 1;
        $DB->insert_record('lcms_track', $track);
    }

    $returnvalue->progress = $track->progress;
    $returnvalue->positionto = $track->lasttime;
    $returnvalue->positionpage = $track->lastpage;
} else if ($act == 'update') {
    $lcmsid = required_param('id', PARAM_INT);
    $track = $DB->get_record('lcms_track', array('lcms' => $lcmsid, 'userid' => $USER->id));
    if ($USER->id) {
        $lcms = new stdClass();
        $lcms->userid = $USER->id;
        $lcms->lcmsid = $lcmsid;
        $lcms->rtype = optional_param('rtype', '', PARAM_RAW);
        $lcms->positionpage = optional_param('positionpage', 1, PARAM_INT);
        $lcms->positionto = optional_param('positionto', 0, PARAM_INT);
        $lcms->positionfrom = optional_param('positionfrom', 0, PARAM_INT);
        $lcms->timereg = time();
        $lcms->timecreated = time();
        $lcms->timemodified = time();

        if ($lcms->positionto > 0 && $duration > 1) {
//            if ($cour->duration != $duration) {
//                //$cour->duration = $duration;
//                //$DB->update_record('lcms',$cour);
//            }

            $query = 'select count(*) from {lcms_playtime} 
                where userid=:userid and lcmsid=:lcmsid and positionpage=:positionpage 
                and positionfrom<=:positionfrom and positionto>=:positionto';
            $params = array('userid' => $USER->id, 'lcmsid' => $lcmsid, 'positionpage' => $lcms->positionpage,
                'positionto' => $lcms->positionto, 'positionfrom' => $lcms->positionfrom);
            $playcount = $DB->count_records_sql($query, $params);

            //플레이시간을 저장한다. 
            if ($playcount == 0) {
                $DB->insert_record('lcms_playtime', $lcms);
            }

//            if ($cour->duration != $duration) {
////                $cour->duration = $duration;
////                $DB->update_record('lcms',$cour);
//            }

            if (!$track->id) {
                $track = new stdClass();
                $track->lcms = $lcmsid;
                $track->userid = $USER->id;
                $track->attempts = 1;
            }
            $track->lasttime = $lcms->positionto;
            $track->lastpage = $lcms->positionpage;
            $track->playtime = lcms_get_progress($lcmsid, $USER->id);
            $track->playpage = lcms_get_progress_page($lcmsid, $USER->id);
            if ($lcms->rtype == '') {
                if ($duration > 1) {
                    $track->progress = round($track->playtime / $duration * 100);
                }
            } else {
                if ($duration > 1) {
                    $track->progress = round($track->playpage / $duration * 100);
                }
            }
            if($track->progress > 97){
                $track->progress = 100;
            }
            $track->timeview = time();
            if ($track->id) {
                $DB->update_record('lcms_track', $track);
            } else {
                $DB->insert_record('lcms_track', $track);
            }

            //lcmsattend_update_attendance_score($cour->course, $USER->id);
//            lcmsprogress_update_progress_score($cour->course, $USER->id);

            $returnvalue->last = date('Y-m-d H:i:s', $track->timeview);
            $ptm = time_from_seconds($track->playtime);
            $returnvalue->totaltime = $ptm->h . ':' . $ptm->m . ':' . $ptm->s;
            $returnvalue->totalpage = $track->playpage;
            $returnvalue->progress = $track->progress . ' %';
        }
    }
} else if ($act == 'setduration') { // 
    $conid = required_param('conid', PARAM_INT);
    $changevalue = $DB->get_record('lcms_contents_file', array('con_seq' => $conid));
    $changevalue->duration = empty($duration) ? $changevalue->duration : $duration;
    $DB->update_record('lcms_contents_file', $changevalue);
    $returnvalue = $DB->get_record('lcms_contents_file', array('con_seq' => $conid));
}

$returnvalue->status = 'success';

@header('Content-type: application/json; charset=utf-8');
echo json_encode($returnvalue);

