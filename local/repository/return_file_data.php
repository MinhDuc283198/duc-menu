<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');


$path = optional_param('path', '', PARAM_RAW);
$t_file = optional_param('t_file', '', PARAM_RAW);
$o_file = optional_param('o_file', '', PARAM_RAW);
$user_id = optional_param('user_id', 0, PARAM_INT);
$d_num = optional_param('d_num', 0, PARAM_INT);
$f_num = optional_param('f_num', '', PARAM_RAW);

if ($path && $t_file && $user_id) {
    $data = new stdClass();
    $data->con_seq = 0;
    $data->filepath = $path;
    $data->fileoname = $o_file;

    $name_ary = explode('.', $o_file);
    $ext = $name_ary[count($name_ary) - 1];
    $mp4file = preg_replace('/\.' . $ext . '$/', '.mp4', $o_file);
    $mp4file = str_replace(' ', '',$mp4file); 
    $mp4file = preg_replace ("/[ #\&\+\-%@=\/\\\:;,'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $mp4file); 
    $data->filename = $mp4file;
    $data->filesize = '0';
    $data->con_type = 'video';
    $data->user_no = $user_id;
    $up = $DB->insert_record('lcms_contents_file', $data);
}
if ($up) {
    $url =  $CFG->transcodingserver.'/duration.php'; //접속할 url 입력

    $post_data = array(
"path" => '/var/www/html/uploads/'.$path.'/',
"filename" => $mp4file,        
"id" => $up
);

//    $post_data["path"] = $transfilepath;
//    $post_data["filename"] = $filename;

//    $access_token_value = "올바른 access token 입력";
//
//    //$header_data = array('Authorization: Bearer access_token_value'); //에러 발생
//    $header_data = [];
//    $header_data[] = 'Authorization: Bearer '.$access_token_value;

    $ch = curl_init(); //curl 사용 전 초기화 필수(curl handle)

    curl_setopt($ch, CURLOPT_URL, $url); //URL 지정하기
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, 1); //0이 default 값이며 POST 통신을 위해 1로 설정해야 함
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); //POST로 보낼 데이터 지정하기
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $res = curl_exec($ch);

//    var_dump($res);//결과값 확인하기
//    echo '<br>';
//    print_r(curl_getinfo($ch));//마지막 http 전송 정보 출력
//    echo curl_errno($ch);//마지막 에러 번호 출력
//    echo curl_error($ch);//현재 세션의 마지막 에러 출력
    curl_close($ch);
    ?>
    <script type="text/javascript">
        parent.document.getElementById('video_frame').style.display = 'none';
        parent.document.getElementById('video_file_name').style.display = 'block';
        parent.document.getElementById('video_file_name').value = '<?php echo $mp4file; ?>';
        parent.document.getElementById('video_file_id').value = '<?php echo $up; ?>';
    </script>  
<?php } else {
    echo 'Error 500';
    echo $t_file . ' is used';
} ?>