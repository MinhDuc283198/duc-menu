<?php

$string['pluginname'] = 'Tài nguyên';
$string['pluginnameplural'] = 'Tài nguyên';

$string['author_description'] = "* Nhập tối đa 30 ký tự";

$string['all'] = "Tất cả";
$string['add'] = "Thêm";
$string['author'] = "Tác giả";

$string['add:contents'] = 'Nội dung đăng ký';
$string['add:reference'] = 'Tham khảo đăng ký';
$string['edit:contents'] = 'Nội dung chỉnh sửa';
$string['edit:reference'] = 'Chỉnh sửa tham khảo';

$string['back'] = "Trở lại";

$string['change_group'] = "Bạn muốn thay đổi nhóm?";
$string['delete_group'] = 'Bạn muốn xóa nhóm?';
$string['select_group'] = "Vui lòng chọn nhóm";
$string['select_content'] = 'Vui lòng chọn nội dung';
$string['delete_content'] = 'Nội dung tham khảo sẽ bị xóa, tiếp tục?';

$string['copyright'] = "Bảng quyền";

$string['ccby'] = 'Creative Commons Attribution(CC BY)';
$string['ccbync'] = 'Creative Commons Attribution-Noncommercial(CC BY-NC)';
$string['ccbynd'] = 'Creative Commons No Derivative Works(CC BY-ND)';
$string['ccbysa'] = 'Creative Commons Share-alike(CC BY-SA)';
$string['ccbyncsa'] = 'Creative Commons Attribution-Noncommercial-Share-alike(CC BY-NC-SA)';
$string['ccbyncnd'] = 'Creative Commons Attribution-Noncommercial-No Derivative Works(CC BY-NC-ND)';
$string['con_tag'] = 'Thẻ tìm kiếm';
$string['doc_type'] = 'Loại tài liệu';
$string['document'] = "Tài liệu";
$string['rst'] = "srt";
$string['description'] = "Mô tả";

$string['embed'] = "Nhúng";

$string['file_type'] = 'Dạng tệp';

$string['groupselect'] = 'Chọn nhóm';

$string['group'] = 'Nhóm';
$string['groupname'] = 'Tên nhóm';

$string['hidden'] = "Ẩn";
$string['html'] = "Tệp HTML";
$string['video'] = "Video";

$string['videoadd'] = "Thêm Video";
$string['videoaddplease'] = "Vui lòng thêm video";
$string['videotranscoding'] = "Đang chuyển mã...";
$string['videotranscomplete'] = "Chuyển mã thành công";

$string['noreference'] = 'Không có bài tham khảo';
$string['nocontents'] = 'Chưa có nội dung';
$string['nogroups'] = 'Nhóm trống';

$string['teacher'] = "Giáo sư";
$string['title'] = "Tiêu đề";

$string['public'] = "Công khai";

$string['reference'] = 'Tài liệu tham khảo';
$string['referenceadd'] = 'Thêm tài liệu tham khảo';

$string['copy_none'] = 'Không';
$string['copy_creative'] = 'Creative Commons';
$string['copy_etc'] = 'Nhập trực tiếp';

$string['filename'] = 'Tên tệp';

$string['tag'] = 'Thẻ tệp tin';

$string['preview'] = 'Đánh giá';

$string['list:no'] = 'NO.';
$string['list:groupname'] = 'Tên nhóm';
$string['list:title'] = 'Tiêu đề';
$string['list:contents'] = 'Nội dung';
$string['list:reference'] = 'Tài liệu tham khảo';
$string['list:isopen'] = 'Công khai';
$string['list:timecreated'] = 'Ngày';
$string['list:contentscount'] = 'Tổng số nội dung';
$string['list:type'] = 'Loại nội dung';

$string['groupmanage'] = 'Quản lý nhóm';
$string['selectgroupjump'] = 'Chọn nhóm';
$string['selectgroupdelete'] = 'Chọn nhóm để xóa';
$string['contentsregister'] = 'Đăng ký nội dung';

$string['groupregister'] = 'Đăng ký nhóm';
$string['namechange'] = 'Sửa';
$string['delete'] = 'Xóa';
$string['search'] = 'Tìm kiếm';
$string['y'] = 'Có';
$string['n'] = 'Không';
$string['close'] = 'Đóng';

$string['insert:groupname'] = 'Nhập tên nhóm';

$string['insert:delete'] = 'Xóa?';

$string['professor'] = "Giáo sư";
$string['teachingassistant'] = "Trợ giảng";
$string['insert:delete'] = 'Xóa?';

$string['detail:delete'] = 'Xóa';
$string['detail:edit'] = 'Sửa';
$string['detail:list'] = 'Danh sách';

$string['fileadd'] = 'Đăng ký tệp';
$string['filechange'] = 'Thay đổi tệp';
$string['fileallchange'] = 'Thay đổi tất cả các tệp';
$string['filechangeadd'] = 'Thêm tệp';

$string['error:notallowfolder'] = 'Không có thư mục nào';
$string['error:notallowextfile'] = 'Không có tệp hợp lê. Thử lại.';
$string['error:notaddfile'] = 'Đăng ký tệp';
$string['error:addfilecount'] = 'Đăng ký tối đa {$a} tệp.';
$string['error:notembed'] = 'Đăng ký nội dung nhúng';
$string['error:mediaid'] = 'Đăng kí đa phương tiện';

$string['lowq'] = 'SD Video';
$string['highq'] = 'HD Video';
$string['mid'] = 'Media ID';
$string['cdms'] = 'CDMS';
$string['add_dir'] = 'Thêm thư mục';
$string['add_file'] = 'Thêm tệp';
$string['year'] = 'Năm';

//CDMS
$string['review'] = 'Đang xem xét';
$string['contentsname'] = 'Tên nội dung';
$string['type'] = 'Loại';
$string['lasteditdate'] = 'Ngày cập nhận gần nhất';
$string['empty_review'] = 'Ngày rỗng';
$string['approved'] = 'Phê duyệt';
$string['empty_approved'] = 'Dữ liệu rỗng';
$string['hold'] = 'Đang chờ';
$string['empty_hold'] = 'Dữ liệu rỗng';
$string['mycontents'] = 'Nội dung của tôi';
$string['empty_contents'] = 'Dữ liệu rỗng';
$string['sharedcontents'] = 'Chi sẻ nội dung';
$string['owner'] = 'Người tạo';
$string['empty_sharedcontents'] = 'Dữ liệu rỗng';
$string['mycontentsfile'] = 'Nội dung tệp của tôi';
$string['empty_file'] = 'Dữ liệu rỗng.';
$string['sharedcontentsfile'] = 'Chia sẻ nội dung tệp';
$string['empty_sharedcontentsfile'] = 'Dữ liệu rỗng';
$string['contentsfile_manage'] = 'Quản lý nội dung tệp';
$string['insert_foldername_msg'] = 'Thêm thư mục'; 
$string['cdms_manage'] = 'Quản lý CDMS';
$string['review_contents_list'] = 'Danh sách đánh giá';
$string['approved_contents_list'] = 'Danh sách đã phê duyệt';
$string['hold_contents_list'] = 'Danh sách nội dung đang chờ xử lý';
$string['approve'] = 'Phê duyệt';
$string['holdreason'] = 'Hole reason';

$string['form:caption'] = "Tệp ghi chú";
$string['captionpreview'] = "Ghi chú đánh giá";
$string['folder'] = "Thên thư mục (nội dung)";

$string['ftp_server'] = 'FTP Server Address';
$string['ftp_user'] = 'FTP User';
$string['ftp_pw'] = 'FTP Password';

$string['course_management'] = 'Quản lý khóa học';
$string['flowplayer_key'] = 'Phím trình chiếu';