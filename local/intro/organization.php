<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');

echo $OUTPUT->header();
?>  
<!-- 왼쪽 서브메뉴 영역 -->
<div id="lnb">
    <h5>교육원안내</h5> 
    <ul>
        <li>
            <a href="/local/intro/greeting.php">인사말</a>
        </li>
        <li>
            <a href="/local/intro/history.php">연혁</a>
        </li>
        <li class="on">
            <a href="/local/intro/organization.php">조직도</a>
        </li>
        <li>
            <a href="/local/intro/map.php">찾아오시는길</a>
        </li>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
<!-- 왼쪽 서브메뉴 영역 -->


<div id="content">

    <!-- 상단 페이지 타이틀 -->
    <h3 class="page_title">
        조직도
    </h3>  
    <!-- 상단 페이지 타이틀 -->

    <div class="terms terms02">
        <div class="orga_img">
            <img src="<?php echo $CFG->wwwroot;?>/theme/oklassedu/pix/images/organization.png" alt="organization_img">
        </div>
        
        <div class="orga_table table">
            <table>
                <tr>
                    <th>선정심사위원회</th>
                    <td>영재교육 대상자의 선정과 수료에 대한 심의 및 영재원 학칙 개정을 수행한다.</td>
                </tr>
                <tr>
                    <th>운영위원회</th>
                    <td>교육원 운영의 기본계획, 교육원의 제 규정 제정 및 개폐, 교육원의 예산 및 결산에 관한 사항 등 교육원 운영의 주요 사항을 심의한다.</td>
                </tr>
                <tr>
                    <th>교육과정 연구부</th>
                    <td>영재교육연구, 교육과정개발, 교재개발을 행한다.</td>
                </tr>
                <tr>
                    <th>기초반 운영부</th>
                    <td>컴퓨터과학 기초반의 실제적인 수업과 학생관리를 행한다.</td>
                </tr>
                <tr>
                    <th>심화반 운영부</th>
                    <td>컴퓨터과학 심화반의 실제적인 수업과 학생관리를 행한다.</td>
                </tr>
                <tr>
                    <th>사사반 운영부</th>
                    <td>컴퓨터과학 사사반의 실제적인 수업과 학생관리를 행한다.</td>
                </tr>
                <tr>
                    <th>행정지원부</th>
                    <td>학사관리, 서무 및 회계 업무, 기타 사업 지원 업무를 담당한다.</td>
                </tr>
            </table>
        </div>


    </div> 
</div>

<?php
echo $OUTPUT->footer();
?>


