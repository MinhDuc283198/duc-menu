<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add(get_string('privacy_policy', 'local_intro'));

echo $OUTPUT->header();
?>  <!-- 왼쪽 서브메뉴 영역 -->
<div id="lnb">
    <h5>연수원소개</h5>
    <ul>
        <li>
            <a href="/local/intro/greeting.php">연수원소개</a>
        </li>
        <li>
            <a href="/local/intro/sitemap.php">사이트맵</a>
        </li>
        <li class="on">
            <a href="/local/intro/terms.php">사내통신망 정보보안지침</a>
        </li>
        <li>
            <a href="/local/intro/protect.php">개인정보취급방침</a>
        </li>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
<div id="content">
    <div id="pagenav">
        <ul>
            <li class="home">
                <a href="/">home</a>
            </li>
            <li>
                <a href="#none">사내통신망 정보보안지침</a>
                <span class="arrow"></span>
                <ul>
                    <li class="on">
                        <a href="/local/intro/greeting.php">연수원소개</a>
                    </li>
                    <li>
                        <a href="/local/intro/sitemap.php">사이트맵</a> 
                    </li>
                    <li>
                        <a href="/local/intro/terms.php">사내통신망 정보보안지침</a>
                    </li>
                    <li>
                        <a href="/local/intro/protect.php">개인정보취급방침</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="policy_box type2">

        <h1>사내통신망 정보보안지침</h1>

        <h2>제 1조 (목적)</h2>
        <p>
            본 지침은 edu.esls.io(JEI 온라인 연수원)을 통한 사내 중요 정보자료의 유출을 방지하기 위해 개인 아이디 등에 대한 보안관리 사항을 정하는 데 그 목적이 있다.
        </p>

        <h2>제 2조 (적용범위)</h2>
        <p>본 지침은 재능e아카데미 및 관계사(이하 '회사')에 근무하는 전 임직원과 회사와 일정한 계약관계를 맺고</p>
        <p>edu.esls.io 아이디를 부여받은 재능선생님 등 모든 계약자들에게 적용한다.</p>

        <h2>제 3조 (기본정책)</h2>
        <ol>
            <li><strong>1. 각 개인은 업무용으로 하나의 아이디만 사용할 수 있다.</strong></li>
            <li><strong>2. 회사에서 제공하는 개인용 컴퓨터가 없는 임직원 및 재능선생님은 조직 내 업무용 컴퓨터 중 한 대의 컴퓨터를 이용하는 것을 원칙으로 한다.</strong></li>
            <li><strong>3. 퇴직자(계약해지자)는 퇴직일(계약해지일)로부터 접근권한이 차단된다.</strong></li>
            <li>
                <strong>4. 모든 전산시스템에 사용하는 비밀번호는 유추가 힘들도록 다음과 같은 구성기준을 따라야 한다.</strong>
                <ol>
                    <li>가. 알파벳과 숫자, 특수문자를 혼합하여 사용하여야 한다.</li>
                    <li>나. 자신이 기억할 수 있어야 하며 최소 6자리 이상이어야 한다. <br/> 예) TOLL + 61, BILLI + 623899 등</li>
                    <li>다. 사용자 ID와 같지 않아야 한다.</li>
                    <li>라. 변경시 현재의 비밀번호와 같지 않아야 한다.</li>
                    <li>마. 최초 로그인시 본인이 직접 비밀번호를 설정해야 한다</li>
                </ol>
            </li>
        </ol>

        <h2>제 4조 (모니터링 및 점검)</h2>
        <ol>
            <li>
                <strong>1. 재능e아카데미 경영관리팀은 아이디 유출, 도용 등 이상징후와 규정 준수 현황을 6개월 단위로 분석한다.</strong>
            </li>
            <li>
                <strong>2. 업무외 목적으로 사용하거나 이상 징후가 의심되는 아이디는 회사에서 일방적으로 아이디를 해지할 수 있다.</strong>
            </li>
        </ol>


        <h2>제 5조 (이용자 준수사항)</h2>
        <ol>
            <li>
                <strong>11. 1년에 1회 이상 비밀번호를 변경하는 것을 원칙으로 한다. </strong><br />
                1년 이내 미 변경시 1년차 시점부터 1개월 동안 변경토록 권고하고 이후 미 변경시에는 개인 신원확인 및 비밀번호 변경 절차를 거친 후 로그인하는 것을 허용한다.
            </li>
            <li>
                <strong>2. 이상 징후 적발시 관련된 임직원 및 재능선생님은 조사에 적극적으로 협조한다.</strong>
            </li>
            <li>
                <strong>3. 신입사원, 신임계약자는 최초 로그인 시 "정보보안서약서"를 작성하여야 하고, 기존 사원, 계약자, 임직원은 중요 내용 변경 시 재작성하여야 한다.</strong>
            </li>
        </ol>


        <h2>제 6조 (징계)</h2>
        <ol>
            <li>
                <strong>1. 아이디를 유출하거나 도용한 이상 징후가 의심되는 경우에는 재능e아카데미 EdTech실 Business기획팀장은사내 감사팀에게 감사를 의뢰한다</strong>
            </li>
            <li>
                <strong>2. 아이디 및 비밀번호 등의 의도적인 도용 및 도용 제공자는 징계위원회에 회부하고 회사의 사업비밀관리규정, 징계규정을 엄격히 적용한다.</strong>
            </li>
            <li>
                <strong>3. 아이디 및 비밀번호를 타인에게 알려 주어 업무를 수행토록 하는 행위 1회 적발시는 경고조치하며, 2회 적발시는 알려 준 자, 사용한 자 모두 징계위원회에 회부하여 징계한다</strong>
            </li>
            <li>
                <strong>4. 아이디 및 비밀번호 도용 및 유출로 인한 회사의 이미지 및 금전적인 피해 등이 발생된 경우에는 민•형사상의 책임을 묻는다.</strong>
            </li>
        </ol>

        <h2>제 7조 (정보관리)</h2>
        <ol>
            <li>
                <strong>1. 정보 총괄 : 재능e아카데미 EdTech실장으로 한다.</strong>
            </li>
            <li>
                <strong>2. 기술 및 운영 지원 : 재능e아카데미 Mechatronics팀에서 담당한다.</strong>
            </li>
            <li>
                <strong>3. 각 부서 책임자</strong>
                <ol>
                    <li>
                        가. 재능e아카데미 및 관계사
                        <ul>
                            <li>- 본부장 및 실장급이상 : 대표이사</li>
                            <li>- 팀장 : 각 실장</li>
                            <li>- 팀원 : 각 팀장</li>
                        </ul>
                    </li>
                </ol>

                <ol>
                    <li>
                        나. 사업조직
                        <ul>
                            <li>- 사업부장 : 재능교육 스스로사업본부 스스로사업지원실장</li>
                            <li>- 사업부 팀원,팀장/사업국장 : 사업부장</li>
                            <li>- 지국장 : 사업국장</li>
                            <li>- 지구장, 지국지원담당, 재능선생님 : 지국장</li>
                        </ul>
                    </li>
                </ol>
            </li>
        </ol>
    </div>
</div>


<?php
echo $OUTPUT->footer();
?>


