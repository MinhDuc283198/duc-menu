<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');

echo $OUTPUT->header();
?>  
<!-- 왼쪽 서브메뉴 영역 -->
<div id="lnb">
    <h5>교육원안내</h5> 
    <ul>
        <li>
            <a href="/local/intro/greeting.php">인사말</a>
        </li>
        <li class="on">
            <a href="/local/intro/history.php">연혁</a>
        </li>
        <li>
            <a href="/local/intro/organization.php">조직도</a>
        </li>
        <li>
            <a href="/local/intro/map.php">찾아오시는길</a>
        </li>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
<!-- 왼쪽 서브메뉴 영역 -->


<div id="content">

    <!-- 상단 페이지 타이틀 -->
    <h3 class="page_title">
        연혁
    </h3>  
    <!-- 상단 페이지 타이틀 -->

    <div class="terms terms02">
        <div class="history_table table">
            <table>
                <tr>
                    <th>2017.02.15</th>
                    <td>한양대 공과대학 산하에 소프트웨어 영재교육원 설치<br>- 초대 원장 : 유민수 교수</td>
                </tr>
                <tr>
                    <th>2017.01.05</th>
                    <td>영재교육원 신규설치 승인 (서울특별시 교육청)</td>
                </tr>
                <tr>
                    <th>2016.11.28</th>
                    <td>한양대 부설 영재교육원 설립 신청 (서울특별시 교육청)<br>- 설립추진위원회 : 유민수(위원장), 박희진, 이인환, 이춘화, 노미나, 김광욱 교수 등</td>
                </tr>
            </table>
        </div>


    </div> 
</div>

<?php
echo $OUTPUT->footer();
?>


