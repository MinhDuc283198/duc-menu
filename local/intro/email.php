<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->navbar->add("이메일무단수집거부");

echo $OUTPUT->header();
?>  
    <h3 class="page_title">
        이메일무단수집거부
    </h3> 
    <div class="borderbox terms terms02">
        <div>
            <p>
                본 웹사이트에 게시된 이메일 주소가 전자우편 수집 프로그램이나 그 밖의 기술적 장치 를 이용하여 무단으로 수집되는 것을 거부하며, 이를 위반시 정보통신망법에 의해 형사 처벌됨을 유념하시기 바랍니다.
            </p>

            <h5 class="blue">정보통신망 이용촉진 및 정보보호 등에 관한 법률</h5>
            <h5>제50조의2 (전자우편주소의 무단 수집행위 등 금지)</h5>
            <ul class="indent">
                <li>1. 누구든지 인터넷 홈페이지 운영자 또는 관리자의 사전 동의 없이 인터넷 홈페이지에서 자동으로 전자우편주소를 수집하는 프로그램이나 그 밖의 기술적 장치를 이용하여 전자우편주소를 수집하여서는 아니된다.</li>
                <li>2. 누구든지 제1항을 위반하여 수집된 전자우편주소를 판매·유통하여서는 아니된다.</li>
                <li>3. 누구든지 제1항 및 제2항의에 따라 수집·판매 및 유통이 금지된 전자우편주소임을 알면서도 이를 정보 전송에 이용하여서는 아니된다.</li>
            </ul>

            <h5>제74조(벌칙) 다음 각호의 어느 하나에 해당하는 자는 1년 이하의 징역 또는 1천만원 이하의 벌금에 처한다.&lt;개정 2012.2.17&gt; </h5>
            <ul class="indent">
                <li>1.제50조제6항을 위반하여 기술적 조치를 한 자</li>
                <li>2. 제50조제8을 위반하여 광고성 정보를 전송한 자</li>
                <li>3. 제50조의2를 위반하여 전자우편주소를 수집ㆍ판매ㆍ유통 또는 정보 전송에 이용한 자</li>
            </ul>
        </div>
    </div> 

    <?php
    echo $OUTPUT->footer();
    ?>


