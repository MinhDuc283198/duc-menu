<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');

echo $OUTPUT->header();
?>  
<div id="lnb">
    <h5>사이트맵</h5>
    <ul>
        <li>
            <a href="/local/intro/greeting.php">연수원소개</a>
        </li>
        <li class="on">
            <a href="/local/intro/sitemap.php">사이트맵</a>
        </li>
        <li>
            <a href="/local/intro/protect.php">개인정보취급방침</a>
        </li>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
<div id="content">
    <div id="pagenav">
        <ul>
            <li class="home">
                <a href="#">home</a>
            </li>
            <li>
                <a href="#none">사이트맵</a>
                <span class="arrow"></span>
                <ul>
                    <li>
                        <a href="/local/intro/greeting.php">연수원소개</a>
                    </li>
                    <li>
                        <a href="/local/intro/sitemap.php">사이트맵</a> 
                    </li>
                    <li>
                        <a href="/local/intro/protect.php">개인정보취급방침</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <h3 class="page_title">사이트맵</h3>
    <!-- sitemap area -->
    <div id="sitemap" class="margin_bt30">
        <!-- sitemap list -->
        <?php
        $sql = 'select * from {main_menu} where isused = 1 and depth = 1 order by step asc';
        $menus = $DB->get_records_sql($sql);
        $myusergroup = $DB->get_field('lmsdata_user', 'usergroup', array('userid' => $USER->id));
        if (empty($myusergroup)) {
            $myusergroup = 'rs';
        }
        foreach ($menus as $menu) {
            $menu_apply = $DB->get_record('main_menu_apply', array('menuid' => $menu->id));
            if (!is_siteadmin() && (empty($menu_apply) || (strpos($menu->url, 'oklearning') && $myusergroup == 'rs'))) {
                continue;
            }
            $sql = 'select *, min(step) as minstep from {main_menu} where parent = :parent and isused = :isused and depth = :depth';
            $first_sub_menu = $DB->get_record_sql($sql, array('parent' => $menu->id, 'isused' => 1, 'depth' => 2));
            $link = (preg_match('/http/i', $first_sub_menu->url)) ? $first_sub_menu->url : $CFG->wwwroot . $first_sub_menu->url;
            $link2 = (preg_match('/http/i', $menu->url)) ? $menu->url : $CFG->wwwroot . $menu->url;
            switch ($menu->type) {
                case 3:
                    $class = 'link';
                    $link = (preg_match('/http/i', $menu->url)) ? $menu->url : $CFG->wwwroot . $menu->url;
                    break;
                case 4:
                    $class = 'link';
                    $link = (preg_match('/http/i', $menu->url)) ? $menu->url : $CFG->wwwroot . $menu->url;
                    break;
            }
            $lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $menu->id, 'lang' => current_language()));
            ?>
            <div class="site_list"> 
                <h2><a href="<?php echo $link2; ?>"><?php echo $lang; ?></a></h2>
                <ul>
                    <?php if($lang != "커뮤니티"){ ?>
                        <li><a href="<?php echo $link2; ?>"><?php echo $lang; ?> 홈</a></li>
                    <?php } ?>
                    <?php
                    if ($menu->type == 1) {
                        $sql = 'select * from {main_menu} where isused = 1 and depth = 2 and type = 2 and parent = :parent order by step asc';
                        $sub_menus = $DB->get_records_sql($sql, array('parent' => $menu->id));
                        foreach ($sub_menus as $sub_menu) {
                            $link = (preg_match('/http/i', $sub_menu->url)) ? $sub_menu->url : $CFG->wwwroot . $sub_menu->url;
                            $link = str_replace('{userid}', $USER->id, $link);
                            $sub_lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $sub_menu->id, 'lang' => current_language()));
                            $target = '';
                            if ($sub_menu->ispopup == 2) {
                                $target = 'target="_blank"';
                            }
                            ?>
                            <li> <a href="<?php echo $link; ?>" <?php echo $target; ?>><?php echo $sub_lang; ?></a>

                                <ul>
                                    <?php
                                    $sql = 'select * from {main_menu} where isused = 1 and depth = 3 and type = 2 and sub_parent = :parent order by step asc';
                                    $lower_menus = $DB->get_records_sql($sql, array('parent' => $sub_menu->id));
                                    foreach ($lower_menus as $lower_menu) {
                                        $link = (preg_match('/http/i', $lower_menu->url)) ? $lower_menu->url : $CFG->wwwroot . $lower_menu->url;
                                        $link = str_replace('{userid}', $USER->id, $link);
                                        $lower_lang = $DB->get_field('main_menu_name', 'name', array('menuid' => $lower_menu->id, 'lang' => current_language()));
                                        $target = '';
                                        if ($lower_menu->ispopup == 2) {
                                            $target = 'target="_blank"';
                                        }
                                        ?>
                    <!--                                        <li><a href="<?php echo $link; ?>" <?php echo $target; ?>><?php echo $lower_lang; ?></a></li>-->
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        <?php } ?>
        <!-- sitemap list -->
    </div>
    <!-- sitemap area -->
</div>

<?php
echo $OUTPUT->footer();
?>


