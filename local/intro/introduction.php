<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');

echo $OUTPUT->header();
?>  
<div id="lnb">
    <h5>교육원안내</h5>
    <ul>
        <li class="on"> 
            <a href="/local/intro/introduction.php"><?php echo get_string('introduction', 'local_intro') ?></a>
        </li>
        <li>
            <a href="/local/intro/greeting.php">인사말</a>
        </li>
        <li>
            <a href="/local/intro/history.php">연혁</a>
        </li>
        <li>
            <a href="/local/intro/organization.php">조직도</a>
        </li>
        <li>
            <a href="/local/intro/map.php">찾아오시는길</a>
        </li>
        <li>
            <a href="/local/intro/sitemap.php">사이트맵</a>
        </li>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
<div id="content">
    <h3 class="page_title">
        <?php echo get_string('introduction', 'local_intro') ?>
    </h3>  
    <div class="borderbox">
        <div class="red_circle l_content">
            <?php echo get_string('roundtext', 'local_intro') ?>
        </div>
        <div class="r_content summary">
            <strong class="orange">
                <?php echo get_string('borderbox01', 'local_intro') ?>
            </strong>
            <p>
                <?php echo get_string('borderbox02', 'local_intro') ?>
            </p>
        </div>
    </div> 

    <h5 class="div_title orange">
        <?php echo get_string('intro_title01', 'local_intro') ?>
    </h5>

    <table class="table black">
        <tbody>
            <tr>
                <th><?php echo get_string('sitename', 'local_intro') ?></th>
                <td><?php echo get_string('chamku', 'local_intro') ?></td>
                <th><?php echo get_string('domain', 'local_intro') ?></th>
                <td>http://campus.eduhope.net</td>
            </tr>
            <tr>
                <th><?php echo get_string('detail', 'local_intro') ?></th>
                <td>
                    <?php echo get_string('detailtext', 'local_intro') ?>
                </td>
                <th><?php echo get_string('addr', 'local_intro') ?></th>
                <td><?php echo get_string('addrtext', 'local_intro') ?></td>
            </tr>
            <tr>
                <th><?php echo get_string('establish_year', 'local_intro') ?></th>
                <td>2003<?php echo get_string('year', 'local_intro') ?> 3<?php echo get_string('month', 'local_intro') ?></td>
                <th><?php echo get_string('tel', 'local_intro') ?></th>
                <td>02-2670-9465</td>
            </tr>
        </tbody>
    </table>

    <h5 class="div_title orange">
        <?php echo get_string('intro_title02', 'local_intro') ?>
    </h5>
    <ul class="dash">
        <li><?php echo get_string('characteristic01', 'local_intro') ?></li>
        <li><?php echo get_string('characteristic02', 'local_intro') ?></li>
        <li><?php echo get_string('characteristic03', 'local_intro') ?></li>
    </ul>

    <h5 class="div_title orange">
        <?php echo get_string('intro_title03', 'local_intro') ?>
    </h5>
    <div class="brd_btn"><?php echo get_string('developmentbox01', 'local_intro') ?></div>
    <ul class="dash">
        <li><?php echo get_string('development01', 'local_intro') ?></li>
        <li><?php echo get_string('development02', 'local_intro') ?></li>
    </ul>

    <div class="brd_btn"><?php echo get_string('developmentbox02', 'local_intro') ?></div>
    <ul class="dash">
        <li><?php echo get_string('development03', 'local_intro') ?></li>
        <li><?php echo get_string('development04', 'local_intro') ?></li>
        <li><?php echo get_string('development05', 'local_intro') ?></li>
        <li><?php echo get_string('development06', 'local_intro') ?></li>
    </ul>


</div>



<?php
echo $OUTPUT->footer();
?>


