<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * user signup page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require($CFG->dirroot.'/login/lib.php');

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$PAGE->set_url('/local/intro/signup.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add("회원가입");

echo $OUTPUT->header();

$mod = optional_param('mod', 'write', PARAM_RAW);

if ($USER->id != null) {
    redirect($CFG->wwwroot . 'main_index.php');
}
$js = array(
    '../../login/js/ckeditor-4.3/ckeditor.js',
    '../../login/js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>  
<script type="text/javascript">
    $(document).ready(function () {
        confirmemail = 1;
        $(".confirmtext").hide();
    })


</script>
<style>
    #page-local-intro-signup div[role=main] {
        padding: 40px;
        border: 1px solid #ddd;
    }
    .sign-box {
        border: 1px solid #ddd;
        height: 150px;
        padding: 20px;
        overflow: auto;
    }
</style>
<link href="/chamktu/css/jquery-ui.css" rel="stylesheet" />
<script type="text/javascript" src="/chamktu/js/lib/jquery.ui.datepicker_lang.js"></script>
<h3 class="page_title margin_bt30">
    회원가입
</h3>

<h3 class="div_title sub"> 이용약관 </h3>
<div class="sign-box">
    <strong>제1장 총 칙</strong><br>
    제1조(목적)<br>
    이 이용약관(이하 '약관')은  한양 소프트웨어랩스(이하 '한양SW랩스'와 이용 고객(이하 '회원')간에 한양SW랩스가 제공하는 모든 서비스(이하 ‘서비스’)의 가입조건 및 이용에 관한 제반 사항과 기타 필요한 사항을 구체적으로 규정함을 목적으로 합니다. <br><br>
    제2조(이용약관의 효력 및 변경)<br>
    (1) 이 약관은 한양소프트웨어랩스 웹사이트(이하 ‘한양SW랩스 웹’이라 합니다)에서 온라인으로 공시함으로써 효력을 발생하며, 합리적인 사유가 발생할 경우 개정될 수 있습니다. 개정된 약관은 온라인에서 공지함으로써 효력을 발휘하며, 이용자의 권리 또는 의무 등 중요한 규정의 개정은 사전에 공지합니다. <br>
    (2) 한양SW랩스는 합리적인 사유가 발생될 경우에는 이 약관을 변경할 수 있으며, 약관을 변경할 경우에는 이를 사전에 공시합니다. <br>
    (3) 이 약관에 동의하는 것은 정기적으로 웹을 방문하여 약관의 변경사항을 확인하는 것에 동의함을 의미합니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 이용자의 피해는 한양SW랩스에서 책임지지 않습니다. <br>
    (4) 회원은 변경된 약관에 동의하지 않을 경우 회원 탈퇴(해지)를 요청할 수 있으며, 변경된 약관의 효력 발생일로부터 7일 이후에도 거부의사를 표시하지 아니하고 서비스를 계속 사용할 경우 약관의 변경 사항에 동의한 것으로 간주됩니다. <br>
    <br>
    제 3 조 (약관외 준칙) <br>
    (1) 이 약관은 한양SW랩스가 제공하는 개별서비스에 관한 이용안내(이하 ‘서비스별 안내’라 합니다)와 함께 적용합니다. <br>
    (2) 이 약관에 명시되지 아니한 사항에 대해서는 관계법령 및 서비스별 안내의 취지에 따라 적용할 수 있습니다. <br>
    <br>
    제 4 조 (용어의 정의) )<br>
    1) 이 약관에서 사용하는 용어의 정의는 다음과 같습니다. <br>
    ① '이용고객'이라 함은 회원제서비스를 이용하는 이용자를 말합니다. <br>
    ② '이용계약'이라 함은 서비스 이용과 관련하여 한양SW랩스와 이용고객 간에 체결 하는 계약을 말합니다. <br>
    ③ '이용자번호(ID)'라 함은 이용고객의 식별과 이용고객의 서비스 이용을 위하여 이용고객이 선정하고 한양SW랩스가 부여하는 문자와 숫자의 조합을 말합니다. <br>
    ④ '비밀번호'라 함은 이용고객이 부여 받은 이용자번호와 일치된 이용고객 임을 확인하고 이용고객의 권익보호를 위하여 이용고객이 선정한 문자와 숫자의 조합을 말합니다. <br>
    ⑤ '단말기'라 함은 한양SW랩스(이)가 제공하는 서비스를 이용하기 위해 이용고객이 설치한 개인용 컴퓨터 및 모뎀 등을 말합니다. <br>
    ⑥ '해지'라 함은 한양SW랩스 또는 회원이 이용계약을 해약하는 것을 말합니다. <br>
    (2) 이 약관에서 사용하는 용어의 정의는 제①항에서 정하는 것을 제외하고는 관계법령 및 서비스별 안내에서 정하는 바에 의합니다. <br>
    <br>
    
    <strong>제 2 장 이용계약 체결 </strong><br>
    제 5 조 (이용 계약의 성립) <br>
    (1) 이용계약은 이용고객의 본 이용약관 내용에 대한 동의와 이용신청에 대하여 한양SW랩스의 이용승낙으로 성립합니다. <br>
    (2) 본 이용약관에 대한 동의는 이용신청 당시 해당 한양SW랩스 웹의 '동의함' 버튼을 누름으로써 의사표시를 합니다. <br>
    <br>
    제 6 조 (서비스 이용 신청) <br>
    (1) 회원으로 가입하여 본 서비스를 이용하고자 하는 이용고객은 한양SW랩스에서 요청하는 제반정보(이름, 연락처 등)를 제공하여야 합니다. <br>
    (2) 모든 회원은 반드시 회원 본인의 이름를 제공하여야만 서비스를 이용할 수 있으며, 실명으로 등록하지 않은 사용자는 일체의 권리를 주장할 수 없습니다. <br>
    (3) 회원가입은 반드시 실명으로만 가입할 수 있으며 한양SW랩스가 실명확인조치를 할 수 있습니다. <br>
    (4) 타인의 명의를 도용하여 이용신청을 한 회원의 모든 ID는 삭제되며, 관계법령에 따라 처벌을 받을 수 있습니다. <br>
    <br>
    제 7 조 (개인정보의 보호 및 사용)  <br>
    한양SW랩스는 관계법령이 정하는 바에 따라 이용자 등록정보를 포함한 이용자의 개인정보를 보호하기 위해 노력합니다. 이용자 개인정보의 보호 및 사용에 대해서는 관련법령 및 한양SW랩스의 개인정보 보호정책이 적용됩니다. 단, 한양SW랩스의 공식사이트 이외의 웹에서 링크된 사이트에서는 한양SW랩스의 개인정보 보호정책이 적용되지 않습니다. 또한 한양SW랩스는 이용자의 귀책사유로 인해 노출된 정보에 대해서 일체의 책임을 지지 않습니다.  <br>
    <br>
    제 8 조 (이용 신청의 승낙과 제한)  <br>
    (1) 한양SW랩스는 제 6조의 규정에 의한 이용신청고객에 대하여 업무 수행상 또는 기술상 지장이 없는 경우에 원칙적으로 접수순서에 따라 서비스 이용을 승낙합니다.  <br>
    (2) 한양SW랩스는 아래사항에 해당하는 경우에 대해서 승낙하지 아니 합니다.  <br>
    - 실명이 아니거나 타인의 명의를 이용하여 신청한 경우  <br>
    - 이용계약 신청서의 내용을 허위로 기재한 경우  <br>
    - 사회의 안녕과 질서, 미풍양속을 저해할 목적으로 신청한 경우  <br>
    - 부정한 용도로 본 서비스를 이용하고자 하는 경우  <br>
    - 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우  <br>
    - 기타 규정한 제반사항을 위반하며 신청하는 경우  <br>
    - 본 서비스와 경쟁관계에 있는 이용자가 신청하는 경우  <br>
    - 기타 규정한 제반사항을 위반하며 신청하는 경우  <br>
    (3) 한양SW랩스는 서비스 이용신청이 다음 각 호에 해당하는 경우에는 그 신청에 대하여 승낙 제한사유가 해소될 때까지 승낙을 유보할 수 있습니다.  <br>
    - 한양SW랩스가 설비의 여유가 없는 경우  <br>
    - 한양SW랩스의 기술상 지장이 있는 경우  <br>
    - 기타 한양SW랩스의 귀책사유로 이용승낙이 곤란한 경우  <br>
    (4) 한양SW랩스는 이용신청고객이 관계법령에서 규정하는 미성년자일 경우에 서비스별 안내에서 정하는 바에 따라 승낙을 보류할 수 있습니다.  <br>
     <br>
     제 8 조 (이용 신청의 승낙과 제한)  <br>
    (1) 한양SW랩스는 제 6조의 규정에 의한 이용신청고객에 대하여 업무 수행상 또는 기술상 지장이 없는 경우에 원칙적으로 접수순서에 따라 서비스 이용을 승낙합니다.  <br>
    (2) 한양SW랩스는 아래사항에 해당하는 경우에 대해서 승낙하지 아니 합니다.  <br>
    - 실명이 아니거나 타인의 명의를 이용하여 신청한 경우  <br>
    - 이용계약 신청서의 내용을 허위로 기재한 경우  <br>
    - 사회의 안녕과 질서, 미풍양속을 저해할 목적으로 신청한 경우  <br>
    - 부정한 용도로 본 서비스를 이용하고자 하는 경우  <br>
    - 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우  <br>
    - 기타 규정한 제반사항을 위반하며 신청하는 경우  <br>
    - 본 서비스와 경쟁관계에 있는 이용자가 신청하는 경우  <br>
    - 기타 규정한 제반사항을 위반하며 신청하는 경우  <br>
    (3) 한양SW랩스는 서비스 이용신청이 다음 각 호에 해당하는 경우에는 그 신청에 대하여 승낙 제한사유가 해소될 때까지 승낙을 유보할 수 있습니다.  <br>
    - 한양SW랩스가 설비의 여유가 없는 경우  <br>
    - 한양SW랩스의 기술상 지장이 있는 경우  <br>
    - 기타 한양SW랩스의 귀책사유로 이용승낙이 곤란한 경우  <br>
    (4) 한양SW랩스는 이용신청고객이 관계법령에서 규정하는 미성년자일 경우에 서비스별 안내에서 정하는 바에 따라 승낙을 보류할 수 있습니다.  <br>
     <br>
     제 9 조 (이용자ID 부여 및 변경 등) <br>
    (1) 한양SW랩스는 이용고객에 대하여 약관에 정하는 바에 따라 이용자 ID를 부여합니다. <br>
    (2) 이용자ID는 원칙적으로 변경이 불가하며 부득이한 사유로 인하여 변경 하고자 하는 경우에는 해당 ID를 해지하고 재가입해야 합니다. <br>
    (3) 한양대학교 SW영재교육원 이용자ID는 한양대학교 SW영재교육원 운영방침에 따라 한양SW랩스 사이트의 회원ID와 연결될 수 있습니다. <br>
    (4) 이용자ID는 다음 각 호에 해당하는 경우에는 이용고객 또는 한양SW랩스의 요청으로 변경할 수 있습니다. <br>
    ① 이용자ID가 이용자의 전화번호 또는 이용자 고유식별번호 등으로 등록되어 사생활침해가 우려되는 경우 <br>
    ② 타인에게 혐오감을 주거나 미풍양속에 어긋나는 경우 <br>
    ③ 기타 합리적인 사유가 있는 경우 <br>
    (5) 서비스 이용자ID 및 비밀번호의 관리책임은 이용자에게 있습니다. 이를 소홀이 관리하여 발생하는 서비스 이용 상의 손해 또는 제3자에 의한 부정이용 등에 대한 책임은 이용자에게 있으며 한양SW랩스는 그에 대한 책임을 일절 지지 않습니다. <br>
    (6) 기타 이용자 개인정보 관리 및 변경 등에 관한 사항은 서비스별 안내에 정하는 바에 의합니다. <br>
    <br>
    <strong>제 3 장 계약 당사자의 의무 </strong><br>
    제 10 조 (한양SW랩스의 의무) <br>
    (1) 한양SW랩스는 이용고객이 희망한 서비스 제공 게시일에 특별한 사정이 없는 한 서비스를 이용할 수 있도록 하여야 합니다. <br>
    (2) 한양SW랩스는 계속적이고 안정적인 서비스의 제공을 위하여 설비에 장애가 생기거나 멸실된 때에는 부득이한 사유가 없는 한 지체 없이 이를 수리 또는 복구합니다. <br>
    (3) 한양SW랩스는 개인정보 보호를 위해 보안시스템을 구축하며 개인정보 보호정책을 공시하고 준수합니다. <br>
    (4) 한양SW랩스는 이용고객으로부터 제기되는 의견이나 불만이 정당하다고 객관적으로 인정될 경우에는 적절한 절차를 거쳐 즉시 처리하여야 합니다. 다만, 즉시 처리가 곤란한 경우는 이용자에게 그 사유와 처리일정을 통보하여야 합니다. <br>
    <br>
    제 11 조 (이용자의 의무) <br>
    (1) 이용자는 회원가입 신청 또는 회원정보 변경 시 실명으로 모든 사항을 사실에 근거하여 작성하여야 하며, 허위 또는 타인의 정보를 등록할 경우 일체의 권리를 주장할 수 없습니다. <br>
    (2) 회원은 본 약관에서 규정하는 사항과 기타 한양SW랩스가 정한 제반 규정, 공지사항 등 한양SW랩스가 공지하는 사항 및 관계법령을 준수하여야 하며, 기타 한양SW랩스의 업무에 방해가 되는 행위, 한양SW랩스의 명예를 손상시키는 행위를 해서는 안 됩니다. <br>
    (3) 회원은 주소, 연락처, 전자우편 주소 등 이용계약사항이 변경된 경우에 해당 절차를 거쳐 이를 한양SW랩스에 즉시 알려야 합니다. <br>
    (4) 한양SW랩스가 관계법령 및 '개인정보 보호정책'에 의거하여 그 책임을 지는 경우를 제외하고 회원에게 부여된 ID의 비밀번호 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다. <br>
    (5) 회원은 한양SW랩스의 사전 승낙 없이 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업활동의 결과에 대해 한양SW랩스는 책임을 지지 않습니다. 또한 회원은 이와 같은 영업활동으로 한양SW랩스가 손해를 입은 경우, 회원은 한양SW랩스에 대해 손해배상의무를 지며, 한양SW랩스는 해당 회원에 대해 서비스 이용제한 및 적법한 절차를 거쳐 손해배상 등을 청구할 수 있습니다. <br>
    (6) 회원은 한양SW랩스의 명시적 동의가 없는 한 서비스의 이용권한, 기타 이용계약상의 지위를 타인에게 양도, 증여할 수 없으며 이를 담보로 제공할 수 없습니다. <br>
    (7) 회원은 한양SW랩스 및 제 3자의 지적 재산권을 침해해서는 안 됩니다. <br>
    (8) 회원은 다음 각 호에 해당하는 행위를 하여서는 안 되며, 해당 행위를 하는 경우에 한양SW랩스는 회원의 서비스 이용제한 및 적법 조치를 포함한 제재를 가할 수 있습니다. <br>
    - 회원가입 신청 또는 회원정보 변경 시 허위내용을 등록하는 행위 <br>
    - 다른 이용자의 ID, 비밀번호를 도용하는 행위 <br>
    - 이용자 ID를 타인과 거래하는 행위 <br>
    - 한양SW랩스의 운영진, 직원 또는 관계자를 사칭하는 행위 <br>
    - 한양SW랩스로부터 특별한 권리를 부여받지 않고 한양SW랩스의 클라이언트 프로그램을 변경하거나, 웹사이트 또는 게시된 정보의 일부분 또는 전체를 임의로 변경하는 행위 <br>
    - 서비스에 위해를 가하거나 고의로 방해하는 행위 <br>
    - 본 서비스를 통해 얻은 정보를 한양SW랩스의 사전 승낙 없이 서비스 이용 외의 목적으로 복제하거나, 이를 출판 및 방송 등에 사용하거나, 제 3자에게 제공하는 행위 <br>
    - 공공질서 및 미풍양속에 위반되는 저속, 음란한 내용의 정보, 문장, 도형, 음향, 동영상을 전송, 게시, 전자우편 또는 기타의 방법으로 타인에게 유포하는 행위 <br>
    - 모욕적이거나 개인 신상에 대한 내용이어서 타인의 명예나 프라이버시를 침해할 수 있는 내용을 전송, 게시, 전자우편 또는 기타의 방법으로 타인에게 유포하는 행위 <br>
    - 다른 이용자를 희롱 또는 위협하거나, 특정 이용자에게 지속적으로 고통 또는 불편을 주는 행위 <br>
    - 한양SW랩스의 승인을 받지 않고 다른 사용자의 개인정보를 수집 또는 저장하는 행위 <br>
    - 범죄와 결부된다고 객관적으로 판단되는 행위 <br>
    - 본 약관을 포함하여 기타 한양SW랩스가 정한 제반 규정 또는 이용 조건을 위반하는 행위 <br>
    - 기타 관계법령에 위배되는 행위 <br>
    <br>
    <strong>제 4 장 서비스의 이용 </strong><br>
    제 12 조 (서비스 이용 시간) <br>
    (1) 서비스 이용은 한양SW랩스의 업무상 또는 기술상 특별한 지장이 없는 한 연중무휴, 1일 24시간 운영을 원칙으로 합니다. 단, 한양SW랩스는 시스템 정기점검, 증설 및 교체를 위해 한양SW랩스가 정한 날이나 시간에 서비스를 일시 중단할 수 있으며, 예정되어 있는 작업으로 인한 서비스 일시중단은 한양SW랩스 웹을 통해 사전에 공지합니다. <br>
    (2) 한양SW랩스는 긴급한 시스템 점검, 증설 및 교체 등 부득이한 사유로 인하여 예고 없이 일시적으로 서비스를 중단할 수 있으며, 새로운 서비스로의 교체 등 한양SW랩스가 적절하다고 판단하는 사유에 의하여 현재 제공되는 서비스를 완전히 중단할 수 있습니다. <br>
    (3) 한양SW랩스는 국가비상사태, 정전, 서비스 설비의 장애 또는 서비스 이용의 폭주 등으로 정상적인 서비스 제공이 불가능할 경우, 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다. 다만 이 경우 그 사유 및 기간 등을 회원에게 사전 또는 사후에 공지합니다. <br>
    (4) 한양SW랩스는 한양SW랩스가 통제할 수 없는 사유로 인한 서비스중단의 경우(시스템관리자의 고의, 과실 없는 디스크장애, 시스템다운 등)에 사전통지가 불가능하며 타인의 고의, 과실로 인한 시스템중단 등의 경우에는 통지하지 않습니다. <br>
    (5) 한양SW랩스는 서비스를 특정범위로 분할하여 각 범위별로 이용가능시간을 별도로 지정할 수 있습니다. 다만 이 경우 그 내용을 공지합니다. <br>
    <br>
    제 13 조 (이용자ID 관리) <br>
    (1) 이용자ID와 비밀번호에 관한 모든 관리책임은 회원에게 있습니다. <br>
    (2) 한양SW랩스는 이용자 ID에 의하여 제반 이용자 관리업무를 수행 하므로 회원이 이용자 ID를 변경하고자 하는 경우 한양SW랩스가 인정할 만한 사유가 없는 한 이용자 ID의 변경을 제한할 수 있습니다. <br>
    (3) 이용고객이 등록한 이용자 ID 및 비밀번호에 의하여 발생되는 사용상의 과실 또는 제 3자에 의한 부정사용 등에 대한 모든 책임은 해당 이용고객에게 있습니다. <br>
    <br>
    제 14 조 (게시물의 관리) <br>
    한양SW랩스는 다음 각 호에 해당하는 게시물이나 자료를 사전통지 없이 삭제하거나 이동 또는 등록 거부를 할 수 있습니다. <br>
    - 다른 회원 또는 제 3자에게 심한 모욕을 주거나 명예를 손상시키는 내용인 경우 <br>
    - 공공질서 및 미풍양속에 위반되는 내용을 유포하거나 링크시키는 경우 <br>
    - 불법복제 또는 해킹을 조장하는 내용인 경우 <br>
    - 영리를 목적으로 하는 광고일 경우 <br>
    - 범죄와 결부된다고 객관적으로 인정되는 내용일 경우 <br>
    - 다른 이용자 또는 제 3자의 저작권 등 기타 권리를 침해하는 내용인 경우 <br>
    - 한양SW랩스에서 규정한 게시물 원칙에 어긋나거나, 게시판 성격에 부합하지 않는 경우 <br>
    - 기타 관계법령에 위배된다고 판단되는 경우 <br>
    <br>
    제 15 조 (게시물에 대한 저작권) <br>
    (1) 회원이 서비스화면 내에 게시한 게시물의 저작권은 게시한 회원에게 귀속됩니다. 또한 한양SW랩스는 게시자의 동의 없이 게시물을 상업적으로 이용할 수 없습니다. 다만 비영리 목적인 경우는 그러하지 아니하며, 또한 서비스내의 게재권을 갖습니다. <br>
    (2) 회원은 서비스를 이용하여 취득한 정보를 임의 가공, 판매하는 행위 등 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다. <br>
    (3) 한양SW랩스는 회원이 게시하거나 등록하는 서비스 내의 내용물, 게시 내용에 대해 제 14조 각 호에 해당된다고 판단되는 경우 사전통지 없이 삭제하거나 이동 또는 등록 거부할 수 있습니다. <br>
    <br>
    제 16 조 (정보의 제공) <br>
    (1) 한양SW랩스는 회원에게 서비스 이용에 필요가 있다고 인정되는 각종 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. <br>
    (2) 한양SW랩스는 서비스 개선 및 회원 대상의 서비스 소개 등의 목적으로 회원의 동의 하에 추가적인 개인 정보를 요구할 수 있습니다. <br>
    <br>
    <strong>제 5 장 계약 해지 및 이용 제한 </strong><br>
    제 18 조 (계약 변경 및 해지) <br>
    회원이 이용계약을 해지하고자 하는 때에는 회원 본인이 한양SW랩스 웹 내의 메뉴를 이용해 가입해지를 해야 합니다. <br>
    <br>
    제 19 조 (서비스 이용제한) <br>
    (1) 한양SW랩스는 회원이 서비스 이용내용에 있어서 본 약관 제 11조 내용을 위반하거나, 다음 각 호에 해당하는 경우 서비스 이용을 제한할 수 있습니다. <br>
    - 미풍양속을 저해하는 비속한 ID 및 이용자명 사용 <br>
    - 타 이용자에게 심한 모욕을 주거나, 서비스 이용을 방해한 경우 <br>
    - 기타 정상적인 서비스 운영에 방해가 될 경우 <br>
    - 정보통신 윤리위원회 등 관련 공공기관의 시정 요구가 있는 경우 <br>
    - 1년 이상 서비스를 이용한 적이 없는 경우 <br>
    (2) 상기 이용제한 규정에 따라 서비스를 이용하는 회원에게 서비스 이용에 대하여 별도 공지 없이 서비스 이용의 일시정지, 초기화, 이용계약 해지 등을 불량이용자 처리규정에 따라 취할 수 있습니다. <br>
    <br>
    <strong>제 6 장 손해배상 및 기타사항 </strong><br>
    제 20 조 (손해배상) <br>
    한양SW랩스는 서비스에서 무료로 제공하는 서비스의 이용과 관련하여 개인정보보호정책에서 정하는 내용에 해당하지 않는 사항에 대하여는 어떠한 손해도 책임을 지지 않습니다. <br>
    <br>
    제 21 조 (면책조항) <br>
    (1) 한양SW랩스는 천재지변, 전쟁 및 기타 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없는 경우에는 서비스 제공에 대한 책임이 면제됩니다. <br>
    (2) 한양SW랩스는 기간통신 사업자가 전기통신 서비스를 중지하거나 정상적으로 제공하지 아니하여 손해가 발생한 경우 책임이 면제됩니다. <br>
    (3) 한양SW랩스는 서비스용 설비의 보수, 교체, 정기점검, 공사 등 부득이한 사유로 발생한 손해에 대한 책임이 면제됩니다. <br>
    (4) 한양SW랩스는 회원의 귀책사유로 인한 서비스 이용의 장애 또는 손해에 대하여 책임을 지지 않습니다. <br>
    (5) 한양SW랩스는 이용자의 컴퓨터 오류에 의해 손해가 발생한 경우, 또는 회원이 신상정보 및 전자우편 주소를 부실하게 기재하여 손해가 발생한 경우 책임을 지지 않습니다. <br>
    (6) 한양SW랩스는 회원이 서비스를 이용하면서 얻은 자료로 인한 손해에 대하여 책임을 지지 않습니다. 또한 한양SW랩스는 회원이 서비스를 이용하며 타 회원으로 인해 입게 되는 정신적 피해에 대하여 보상할 책임을 지지 않습니다. <br>
    (7) 한양SW랩스는 이용자 상호간 및 이용자와 제 3자 상호 간에 서비스를 매개로 발생한 분쟁에 대해 개입할 의무가 없으며, 이로 인한 손해를 배상할 책임도 없습니다. <br>
    (8) 한양SW랩스에서 회원에게 무료로 제공하는 서비스의 이용과 관련해서는 어떠한 손해도 책임을 지지 않습니다. <br>
    <br>
    제 21 조 (재판권 및 준거법) <br>
    (1) 이 약관에 명시되지 않은 사항은 전기통신사업법 등 관계법령과 상관습에 따릅니다. <br>
    (2) 한양SW랩스의 유료 서비스 회원의 경우 한양SW랩스가 별도로 정한 약관 및 정책에 따릅니다. <br>
    (3) 서비스 이용으로 발생한 분쟁에 대해 소송이 제기되는 경우 한양SW랩스의 소재지를 관할하는 법원을 관할 법원으로 합니다. <br>
</div>
<br><br>

<!--<div class="input_group full">
    <input type="checkbox" name="check01" class="chkbox" id="check01" value="1"/>
    <label for="check01">회원 약관에 모두 동의합니다.</label>
</div>-->

<h3 class="div_title sub">개인정보 수집 및 이용 동의</h3>
<div class="sign-box">
    한양소프트웨어랩스는(이하‘한양SW랩스’) 회원의 개인정보를 중시하며,“정보통신망 이용촉진 및 정보보호"에 관한 법률을 준수하고 있습니다. 개인정보취급방침을 통하여 회원님께서 제공하시는 개인정보가 어떠한 용도와 방식으로 이용되고 있으며, 개인정보보호를 위해 어떠한 조치가 취해지고 있는지 알려드립니다.  개인정보취급방침을 개정하는 경우 웹사이트 공지사항(또는 개별공지)을 통하여 공지할 것입니다. <br>
    <br><br>
    <strong>■ 개인정보의 처리 목적</strong><br>
    한양SW랩스는 수집한 개인정보를 다음의 목적을 위해 처리 및 활용합니다.<br>
    <br>
    ο 서비스 제공에 관한 계약 이행 및 서비스 제공에 따른 요금정산<br>
    - 콘텐츠(수업, 강의, 학습콘텐츠 등) 제공, 구매/수강신청 및 결제<br>
    <br>
    ο 비회원 상담 관리<br>
    - 한양SW랩스에 문의(상담) 접수 사항에 대한 적절하고 안정적인 안내 및 본인 확인을 위한 정보로 활용<br>
    <br>
    ο 회원 관리<br>
    - 서비스 이용에 따른 본인확인, 개인 식별, 불량회원의 부정 이용 방지와 비인가 사용 방지, 가입 의사 확인, 연령확인, 만14세 미만 아동 개인정보 수집 시 법정 대리인 Off-Line확인, 불만처리 등 민원처리, 고지사항 전달<br>
    <br>
    ο 기타<br>
    - 분쟁조정을 위한 기록보존, 회원의 각종 통계자료 산출 <br>
    <br><br>
    
    <strong>■ 개인정보의 처리 및 보유 기간</strong><br>
    원칙적으로, 개인정보 수집 및 처리 목적이 달성된 후에는 해당 정보를 지체 없이 파기합니다. 단, 다음의 정보에 대해서는 아래의 이유로 명시한 기간 동안 보존합니다.<br>
    <br>
   ο 보존 근거<br>
   - 이용약관 및 개인정보취급방침<br>
<br>
   ο 보존 기간<br>
   - 이용계약에 따른 회원 자격이 유지되는 기간동안<br>
   - 그리고 관계법령의 규정에 의하여 보존할 필요가 있는 경우 한양SW랩스는 아래와 같이 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다.<br>
   ο 표시 광고에 관한 기록 : 6개월 (전자상거래등에서의 소비자보호에 관한 법률)<br>
   ο 계약 또는 청약철회 등에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
   ο 대금결제 및 재화 등의 공급에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
   ο 소비자의 불만 >또는 분쟁처리에 관한 기록 : 3년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
   ο 신용정보의 수집 처리 및 이용 등에 관한 기록 : 3년 (신용정보의 이용 및 보호에 관한 법률)<br>
   ο 통신비밀보호법에 따른 통신사실확인자료 : 3개월<br>
    <br><br>
    <strong>■ 수집하는 개인정보 항목 </strong><br>
    한양SW랩스는 회원가입, 상담, 서비스 신청 등등을 위해 아래와 같은 개인정보를 수집하고 있습니다.<br>
<br>
    ο 수집항목<br>
    - 이름,생년월일, 로그인ID, 비밀번호,주소, 휴대전화번호, 일반연락처, 이메일, 학교, 학년, 전공, 소속, 서비스 이용기록, 접속 로그, 접속 IP 정보, 결제기록<br>
<br>
    ο 개인정보 수집방법<br>
    - 홈페이지(회원가입, 상담게시판, 기타 개인정보를 바탕으로 하는 서비스 등)<br>
    <br><br>
    <strong>■ 개인정보의 파기절차 및 방법 </strong><br>
    한양SW랩스는 원칙적으로 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체없이 파기합니다. 파기절차 및 방법은 다음과 같습니다. <br>
<br>
    ο 파기절차 <br>
    회원님이 회원가입 등을 위해 입력하신 정보는 목적이 달성된 후 별도의 DB로 옮겨져(종이의 경우 별도의 서류함) 내부 방침 및 기타 관련 법령에 의한 정보보호 사유에 따라(보유 및 이용기간 참조) 일정 기간 저장된 후 파기되어집니다. <br>
    별도 DB로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 보유되어지는 이외의 다른 목적으로 이용되지 않습니다. <br>
<br>
    ο 파기방법 <br>
    - 전자적 파일형태로 저장된 개인정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제합니다. <br>
    - 종이에 출력된 개인정보는 분쇄기로 분쇄하여 파기합니다. <br>
    <br><br>
    <strong>■ 개인정보의 안전성 확보조치 </strong><br>
    한양SW랩스는 개인정보의 안정성 확보를 위해 다음과 같은 조치를 취하고 있습니다.<br>
<br>
    ο 관리적 조치<br>
    - 내부관리계획 수립 및 시행, 직원 등에 대한 정기적 교육, 회원 구분에 따른 접속 권한 설정 관리<br>
<br>
    ο 기술적 조치<br>
    - 홈페이지에 대한 서버(WEB/DB) 보안 시스템 적용<br>
    - 직원이 사용하는 PC에 대한 윈도우 암호 잠금 설정<br>
    - 직원이 사용하는 PC에 백신소프트웨어 등 보안 프로그램 설치<br>
    - 개인정보가 저장된 파일의 암호화 등<br>
<br>
    ο 비밀번호 암호화<br>
    - 회원 아이디(ID)의 비밀번호는 암호화되어 저장 및 관리되고 있어 본인만이 알고 있으며, 개인정보의 확인 및 변경도 비밀번호를 알고 있는 본인에 의해서만 가능 (단, 회원 본인의 부주의나 인터넷상의 문제로 ID, 비밀번호 등 개인정보가 유출되어 발생한 문제에 대해 한양SW랩스는 일체의 책임을 지지 않습니다.)<br>
<br>
    ο 물리적 조치<br>
    - 개인정보가 저장/보관된 장소의 시건, 출입통제 등<br>
    <br><br>
    <strong>■ 개인정보 제3자 제공</strong><br>
    한양SW랩스는 이용자의 개인정보를 원칙적으로 외부에 제공하지 않습니다. 다만, 아래의 경우에는 예외로 합니다. <br>
<br>
    - 이용자들이 사전에 동의한 경우 <br>
    - 법령의 규정에 의거하거나, 수사 목적으로 법령에 정해진 절차와 방법에 따라 수사기관의 요구가 있는 경우<br>
</div>
<br><br>
<div class="input_group full" style="">
    <h3 class="div_title sub">한양 소프트웨어랩스 홈페이지 이용에 따른 개인정보 수집·이용에 대한 안내</h3>
    <br>
    한양 소프트웨어랩스는 회원 가입 및 이용을 위한 개인정보 수집 • 이용을 위하여 『개인정보보호법 제15조 및 제22조』에 따라 정보주체의 동의를 받고자 하오니 내용을 자세히 읽으신 후 동의 여부를 결정하여 주시기 바랍니다. <br>
    <br>
    
    <strong>-개인정보 수집 • 이용 안내</strong><br>
    <table border="1" cellsapcing="0" style="margin: 10px 0">
        <tr>
            <th style="padding:10px; text-align:center">수집하는 개인정보 항목</th>
            <th style="padding:10px; text-align:center">개인정보 수집 및 이용 목적</th>
            <th style="padding:10px; text-align:center">개인정보의 보유 및 이용 기간 </th>
        </tr>
        <tr>
            <td style="padding:10px">(필수)아이디,성명,이메일,휴대폰번호, 소속정보,최종학력,전공</td>
            <td style="padding:10px">
                ·서비스 제공에 관한 계약 이행 및 서비스 제공에 따른 요금정산<br>
                - 콘텐츠(수업, 강의, 학습콘텐츠 등) 제공, 구매/수강신청 및 결제<br>
                ·회원관리 <br>
                 회원제 서비스 이용에 따른 본인확인, 개인 식별, 불량회원의 부정 이용 방지와 비인가 사용 방지, 가입 의사 확인, 연령확인<br>
                ο 기타<br>
                - 분쟁조정을 위한 기록보존, 회원의 각종 통계자료 산출 <br>
            </td>
            <td style="padding:10px">
                ·이용계약에 따른 회원 자격이 유지되는 기간동안<br>
                그리고 관계법령의 규정에 의하여 보존할 필요가 있는 경우에는 아래와 같이 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다.<br>
               ο 표시 광고에 관한 기록 : 6개월 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 계약 또는 청약철회 등에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 대금결제 및 재화 등의 공급에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 소비자의 불만 >또는 분쟁처리에 관한 기록 : 3년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 신용정보의 수집 처리 및 이용 등에 관한 기록 : 3년 (신용정보의 이용 및 보호에 관한 법률)<br>
               ο 통신비밀보호법에 따른 통신사실확인자료 : 3개월<br>
                <br>
               ·회원의 개인정보 삭제 요청 시 즉시 삭제 조치<br>
            </td>
        </tr>
    </table>
    <p style="padding-bottom:5px; margin-top:10px;"><strong>-동의를 거부할 권리 및 동의 거부에 따른 불이익</strong></p>
    <div style="line-height:25px;">
        귀하는 개인정보의 수집·이용에 동의를 거부할 권리가 있습니다.<br>
       그러나 필수항목에 대한 동의를 거부할 경우, 한양 소프트웨어랩스 회원가입 및 이용이 제한됨을 안내 드립니다. <br>
       <label for="agreeok">동의</label><input type="radio" name="agreement" id="agreeok" value="1" /> &nbsp;&nbsp;
       <label for="agreeno">비동의</label><input type="radio" name="agreement" id="agreeno" value="0" />
    </div>
</div>

<form id="frm_popup_submit" name="sub" method="POST" action="#">
    <input type="hidden" name="mod" value="write">

    <table class="table">
        <tbody>
            <!-- 일반회원 -->
            <tr>
                <th class="w150px">아이디<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="username" title="아이디" class="m-col1" /> 
            <?php if ($mod == 'write') { ?><input type="button" name="id_chk" value="<?php echo '중복확인'; ?>" class="btn gray"><?php } ?>
        </td>
        </tr>
        <tr>
            <th class="w150px">비밀번호<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="password" name="password" id="password" title="비밀번호" class="m-col1" onkeyup="passwordConfirmed1();" maxlength="16"/>&nbsp;&nbsp; <div style="color:grey;">※비밀번호는 6~16자 영문 대 소문자, 숫자, 특수문자 2개 이상 조합을 사용하세요. <br> ※보안수준 보통 이상 필요</div><br>
            <div id="passwordtext"></div>
        </td>
        </tr>
        <tr>
            <th class="w150px">비밀번호 확인<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="password" name="password2" id="password2" title="비밀번호" class="m-col1" onkeyup="passwordConfirmed2()" maxlength="16"/> <div id="passwordchecktext"></div>
        </td>
        </tr>
        <tr>
            <th class="w150px">이름<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="lastname" title="이름" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">생년월일</th>
        <td class="text-left">
            <input type="text"  name="birthday" id="birthday" title="생년월일"  class="w_120" placeholder="클릭하세요"/> 
        </td>
        </tr>
        <tr>
            <th class="w150px">주소</th>
        <td class="text-left">
            <input type="text" name="zipcode" id="address1" title="우편번호" placeholder="우편번호"><input type="button" onclick="execDaumPostcode()" class="btn gray email-btn" value="우편번호 찾기"><br>
            <input type="text" name="city" id="address2" title="주소" class="m-col1" placeholder="주소" /><input type="text" name="address" title="상세주소" id="address3"  class="m-col1" placeholder="상세주소" />
        </td>
        </tr>
        <tr>
            <th class="w150px">기관명<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="workplace" title="기관명" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">부서</th>
        <td class="text-left">
            <input type="text" name="functionsysname" title="부서" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">이메일<sapn class="red">*</sapn></th>
        <td class="text-left">
            <div class="confirmed">
                <input type="text" name="email01" title="전자우편" class="email01" />@
                <input type="text" name="email02" class="email02" title="전자우편"  />
                <input type="hidden" name="econfirm" id="econfirm"  />
                <input type="button" name="email_chk" class="btn gray email-btn" value="이메일인증">
            </div>
            <div class="confirmtext">인증되었습니다</div>
            <div class="colorbox gray emailbox" id="emailbox">
                <input type="text" title="인증번호" name="userconfirm" placeholder="인증번호 입력" />
                <input type="button" value="인증확인" onclick="email_confirm()" class="btn gray" />
            </div>
        </td>
        </tr>

        <tr>
            <th class="w150px">일반연락처</th><!--일반전화 phone1-->
            <td class="text-left">
                <input type="text" name="tel01" title="number01" class="w100px " />
                <input type="text" name="tel02" title="number02" class="w100px" />
                <input type="text" name="tel03" title="number03" class="w100px"/>
            </td>
        </tr>
        <tr>
            <th class="w150px">휴대폰<sapn class="red">*</sapn></th><!--연락처 phone2-->
        <td class="text-left">
            <input type="text" name="phone01" title="number01" class="w100px " />
            <input type="text" name="phone02" title="number02" class="w100px" />
            <input type="text" name="phone03" title="number03" class="w100px"/>
        </td>
        </tr>
        <tr>
            <th class="w150px">직위<sapn class="red">*</sapn></th>
        <td class="text-left">
            <select name="positionsysname" class="m-col1">
                <option value="원급">원급</option>
                <option value="선임급">선임급</option>
                <option value="책임급 이상">책임급 이상</option>
                <option value="주임/사원/대리">주임/사원/대리</option>
                <option value="과장/차장">과장/차장</option>
                <option value="부장급 이상">부장급 이상</option>
                <option value="대학생">대학생</option>
                <option value="대학원생">대학원생</option>
                <option value="교수">교수</option>
                <option value="없음">없음</option>
                <option value="주무관 이하">주무관 이하</option>
                <option value="사무관">사무관</option>
                <option value="서기관 이상">서기관 이상</option>
                <option value="기타">기타</option>
            </select>
        </td>
        </tr>
        <tr>
            <th class="w150px">최종학력<sapn class="red">*</sapn></th>
        <td class="text-left">
            <select name="lastlearn" class="m-col1">
                <option value="초등학교">초등학교</option>
                <option value="중학교">중학교</option>
                <option value="고등학교">고등학교</option>
                <option value="대학(2,3년제)">대학(2,3년제)</option>
                <option value="대학교">대학교</option>
                <option value="대학원(석사)">대학원(석사)</option>
                <option value="대학원(박사)">대학원(박사)</option>
            </select>
        </td>
        </tr>
        <tr>
            <th class="w150px">전공</th>
        <td class="text-left">
            <input type="text" name="functionsysid" title="전공" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">중점 연구분야<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="target" title="중점 연구분야" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">리눅스 활용 수준<sapn class="red">*</sapn></th>
        <td class="text-left">
            <select title="리눅스 활용 수준" name="linux_level" class="w50px m-col1">
                <option value="상">상</option>
                <option value="중">중</option>
                <option value="하">하</option>
            </select>
        </td>
        </tr>
        <tr>
            <th class="w150px">프로그래밍 수준<sapn class="red">*</sapn></th>
        <td class="text-left">
            <select title="프로그래밍 수준" name="programming" class="w50px m-col1">
                <option value="상">상</option>
                <option value="중">중</option>
                <option value="하">하</option>
            </select>
        </td>
        </tr>


        <!-- 일반회원 -->
        </tbody>
    </table>



    <div class="text-right">
        <input type="submit" value="가입하기" class="btn big "/>
    </div>
</form>











<?php
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    $(function () { //전체선택 체크박스 클릭 
        $("#check03").click(function () {
            //만약 전체 선택 체크박스가 체크된상태일경우 
            if ($("#check03").prop("checked")) {
                //해당화면에 전체 checkbox들을 체크해준다 
                $("input[type=checkbox]").prop("checked", true);
                // 전체선택 체크박스가 해제된 경우
            } else {
                //해당화면에 모든 checkbox들의 체크를해제시킨다. 
                $("input[type=checkbox]").prop("checked", false);
            }
        })
    })
    function randomString() {
        var chars = "0123456789";
        var string_length = 6;
        var randomstring = '';
        for (var i = 0; i < string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
//document.randform.randomfield.value = randomstring;
        return randomstring;
    }
    var random = randomString();
    $("input[name=email_chk]").click(function () {
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

        var email = $(this).siblings("input[name=email01]").val() + "@"
        email += $(this).siblings("input[name=email02]").val();

        if (email.match(regExp) == null) {
            confirmemail = 0;
            alert("이메일 주소를 정확히 입력해주세요.");
            $(this).siblings("input[name=email01]").focus();

        } else {
            confirmemail = 1;
        }

        if (confirmemail != 0) {
            $.ajax({
                type: 'POST',
                data: {
                    email01: $("input[name=email01]").val(),
                    email02: $("input[name=email02]").val(),
                    random: random,
                },
                url: './email_submit.php',
                success: function (data) {
                    if (data == 'used') {
                        alert('사용중인 이메일입니다.');
                    } else {
                        // var e = $(data).find('#ran');
                        alert('이메일이 발송되었습니다.');
                        $('.email-btn').val('재발송');
                        $(".emailbox").show();
                        $("#econfirm").val(random);
                    }
                },
            })
        }
    });
           var econ = 0;
    function email_confirm() {

        var econfirm = $("input[name=econfirm]").val();
        var userconfirm = $("input[name=userconfirm]").val();
        if (econfirm.trim() != userconfirm.trim()) {
            alert("인증번호가 다릅니다.");
            econ = 0;
        } else {
            alert("인증되었습니다.");
            $("#emailbox").hide();
            $(".confirmed").hide();
            $(".confirmtext").show();
            econ = 1;
        }
        ;
    }

    $(document).ready(function () {
        $(' input[name=pw_edit]').click(function () {
            if ($(' input[name=pw_edit]').prop('checked') == true) {
                $(" input[name=password]").prop('disabled', false);
                $(" input[name=repassword]").prop('disabled', false);
            } else {
                $(" input[name=password]").prop('disabled', true);
                $(" input[name=repassword]").prop('disabled', true);
            }
        });

        $('#admin_list').click(function () {
            location.href = "./infadmin.php";
        });

        var check_true = 0;
        $("input[name=userid]").keyup(function () {
            check_true = 0;
        });

        $("input[name=id_chk]").click(function () {
            $.ajax({
                type: 'POST',
                data: {
                    userid: $("input[name=username]").val(),
                },
                url: './signup_id_check.php',
                success: function (data) {
                    if ($("input[name=username]").val() == null || $("input[name=username]").val() == '') {
                        alert('아이디를 입력해 주세요.');
                        $("input[name=username]").focus();
                        return false;
                    } else {
                        if (data == false) {
                            alert("현재 사용중인 아이디 입니다.");
                        }else if(!(isValidFormId($("input[name=username]").val()))){
                            alert("아이디는 첫 문자 영문, 5~20자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다.");
                        } else {
                            alert("사용 가능한 아이디 입니다.");
                            check_true = 1;
                        }
                    }
                },
            })
        });


        $('#frm_popup_submit').submit(function (event) {
            var username = $("input[name=username]").val();
            if (username.trim() == '') {
                alert("아이디를 입력해 주세요");
                return false;
            }

<?php
if ($mod == 'write') {
    ?>
                if (check_true == 0) {
                    alert("중복조회를 해주세요");
                    return false;
                }
    <?php
}
?>
            if ($(" input[name=password]").prop('disabled') == false) {
                var password = $("input[name=password]").val();
                if (password.trim() == '') {
                    alert("비밀번호를 입력해 주세요");
                    return false;
                } else {
                    if (!isValidFormPassword(password)) {
                        alert("비밀번호는 6~16자 영문 대 소문자, 숫자, 특수문자 2개 이상 조합을 사용하세요.");
                        return false;
                    }
                }

                var password2 = $("input[name=password2]").val();
                if (password2.trim() == '') {
                    alert("비밀번호 확인을 입력해 주세요");
                    return false;
                }

                if (password != password2) {
                    alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
                    return false;
                }
            }
            var lastname = $("input[name=lastname]").val();
            if (lastname.trim() == '') {
                alert("이름을 입력해 주세요");
                return false;
            }


            var workplace = $("input[name=workplace]").val();
            if (workplace.trim() == '') {
                alert("소속기관을 입력해 주세요");
                return false;
            }

            var email01 = $("input[name=email01]").val();
            if (email01.trim() == '') {
                alert("<?php echo get_string('user_emailalert', 'local_lmsdata'); ?>");
                return false;
            }

            var email02 = $("input[name=email02]").val();
            if (email02.trim() == '') {
                alert("<?php echo get_string('user_emailalert', 'local_lmsdata'); ?>");
                return false;
            }



            if (econ == 0) {
                alert('이메일 인증을 해주세요');
                return false;
            }
            var target = $("input[name=target]").val();
            if (target.trim() == '') {
                alert("중점 연구분야를 입력해 주세요");
                return false;
            }

            var phone01 = $("input[name=phone01]").val();
            var phone02 = $("input[name=phone02]").val();
            var phone03 = $("input[name=phone03]").val();

            if (phone01.trim() == '') {
                alert("연락처를 입력해주세요.");
                return false;
            }
            if (phone02.trim() == '') {
                alert("연락처를 입력해주세요.");
                return false;
            }
            if (phone03.trim() == '') {
                alert("연락처를 입력해주세요.");
                return false;
            }


            var positionsysname = $("input[name=positionsysname]").val();
            if (positionsysname.trim() == '') {
                alert("직위를 입력해 주세요");
                return false;
            }

            var lastlearn = $("input[name=lastlearn]").val();
            if (lastlearn.trim() == '') {
                alert("최종학력을 입력해 주세요");
                return false;
            }

            $(function () {
                $("#check03").click(function () {
                    var chk = $("#check03").is(":checked");

                    if (chk) {
                        $(".chkbox input").each(function () {
                            this.checked = true;
                        });
                    } else {
                        $(".chkbox input").each(function () {
                            this.checked = false;
                        });
                    }
                });

                $("#popup_delete_button").click(function () {
                    if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
                        $('#form').submit();
                    }
                });
            });
            if ($('input:checkbox[name="check01"]').is(":checked") == false) {
                alert("회원약관에 동의해 주세요");
                return false;
            }
            if ($('input:checkbox[name="check02"]').is(":checked") == false) {
                alert("개인정보 수집 및 이용약관에 동의해 주세요");
                return false;
            }


        });
    });
    /**
     * 비밀번호 유효성 체크
     
     * @param {type} pw
     * @returns {Boolean}     */
    function isValidFormPassword(pw) {
        var check1 = passwordConfirmed1();

        if (!check1) {
            return false;
        }


        return true;
    }
    
    /**
     * 아이디 유효성 체크
     
     * @param {type} id
     * @returns {Boolean}     */
    function isValidFormId(id) {
        var check2 = /^[a-z]{1}[a-z0-9\d_-]{5,20}$/;

        if (!check2.test(id)) {
            return false;
        }


        return true;
    }
    
    function speacialChar(){
        var specialchar = /[$@$!:<>;,/.%*?=&_#|+"'-]/;
        if(!specialchar.test($("#password").val())){
            return false;
        }
        return true;
    }
    function senglishChar(){
        var senglishchar = /[a-z]/;
        if(!senglishchar.test($("#password").val())){
            return false;
        }
        return true;
    }
    function benglishChar(){
        var benglishchar = /[A-Z]/;
        if(!benglishchar.test($("#password").val())){
            return false;
        }
        return true;
    }
    function numberNum(){
        var numbernum = /[0-9]/;
        if(!numbernum.test($("#password").val())){
            return false;
        }
        return true;
    }
    
    function passwordConfirmed1(){
        var password = $("#password").val();
        var passwordCheck = $("#password2").val();
        var special = speacialChar();
        var senglish = senglishChar();
        var benglish = benglishChar();
        var number = numberNum();
        var private = 0;
        
        if(passwordCheck === ""){
            $("#passwordchecktext").html("");
        }else if(password !== passwordCheck){
            $("#passwordchecktext").html("<b><font color=red size=2pt>비밀번호가 일치하지 않습니다</font></b>");
        }else{
            $("#passwordchecktext").html("<b><font color=green size=2pt>비밀번호가 일치합니다</font></b>");
        }
        
        if(password.length < 6){
            $("#passwordtext").html("<b>보안 수준 : <font color=red size=2pt>취약</font></b>");
        }else if(password.length >= 6){
            private = private+1;
            if(special===true){
                private = private+1;
            }
            if(senglish===true){
                private = private+1;
            }
            if(benglish===true){
                private = private+1;
            }
            if(number===true){
                private = private+1;
            }
            if(private===2){
                $("#passwordtext").html("<b>보안 수준 : <font color=red size=2pt>취약</font></b>");
            }else if(private===3){
                $("#passwordtext").html("<b>보안 수준 : <font color=orange size=2pt>보통</font></b>");
            }else if(private===4){
                $("#passwordtext").html("<b>보안 수준 : <font color=green size=2pt>안전</font></b>");
            }else{
                $("#passwordtext").html("<b>보안 수준 : <font color=blue size=2pt>매우 안전</font></b>");
            }
                
        }
        if(private < 3){
            return false;
        }else{
            return true;
        }
    }
    
    function passwordConfirmed2(){
        var password = $("#password").val();
        var passwordCheck = $("#password2").val();
        if(passwordCheck === ""){
            $("#passwordchecktext").html("");
        }else if(password !== passwordCheck){
            $("#passwordchecktext").html("<b><font color=red size=2pt>비밀번호가 일치하지 않습니다</font></b>");
        }else{
            $("#passwordchecktext").html("<b><font color=green size=2pt>비밀번호가 일치합니다</font></b>");
        }
        
    }
    
</script>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script>
    function execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('address1').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('address2').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('address3').focus();
            }
        }).open();
    }
    $(document).ready(
            /**
             * 달력
             * @returns {undefined} */
                    function () {

                        $("#birthday").datepicker({
                            dateFormat: "yy-mm-dd",
                            maxDate:0
                                });
                    });


</script>