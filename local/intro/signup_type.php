<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * user signup page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require($CFG->dirroot.'/login/lib.php');

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$PAGE->set_url('/local/intro/signup_type.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add("회원가입유형");

echo $OUTPUT->header();

$mod = optional_param('mod', 'write', PARAM_RAW);

if ($USER->id != null) {
    redirect($CFG->wwwroot);
}
$js = array(
    '../../login/js/ckeditor-4.3/ckeditor.js',
    '../../login/js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?> 
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script type="text/javascript">
    $(document).ready(function () {
        confirmemail = 1;
        $(".confirmtext").hide();
    })
</script>

<ul class="signup-type-area" s>
    <li class="signup-type-area-tab">
        <a class="" href="<?php echo $CFG->wwwroot.'/local/intro/signup_youth.php'; ?>" >
            <i class="fa fa-child fa-5x"></i>
            <br>
            <span><strong>청소년회원</strong>가입</span>
        </a>
    </li>
    <li class="signup-type-area-tab">
        <a href="<?php echo $CFG->wwwroot.'/local/intro/signup_general.php'; ?>">
            <i class="fa fa-user-tie fa-5x"></i>
            <br>
            <sapn><strong>일반회원</strong>가입</sapn>
        </a>
    </li>
</ul>
<script type="text/javascript"></script>
<?php
echo $OUTPUT->footer();
?>
