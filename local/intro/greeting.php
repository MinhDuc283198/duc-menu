<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu02');

echo $OUTPUT->header();
?>  
<!-- 왼쪽 서브메뉴 영역 -->
<div id="lnb">
    <h5>연수원소개</h5>
    <ul>
        <li class="on">
            <a href="/local/intro/greeting.php">연수원소개</a>
        </li>
        <li>
            <a href="/local/intro/sitemap.php">사이트맵</a>
        </li>
        <li>
            <a href="/local/intro/protect.php">개인정보취급방침</a>
        </li>
    </ul>
    <div class="m_menu">&gt;</div>
</div>
<div id="content">
    <div id="pagenav">
        <ul>
            <li class="home">
                <a href="#">home</a>
            </li>
            <li>
                <a href="#none">연수원소개</a>
                <span class="arrow"></span>
                <ul>
                    <li class="on">
                        <a href="/local/intro/greeting.php">연수원소개</a>
                    </li>
                    <li>
                        <a href="/local/intro/sitemap.php">사이트맵</a> 
                    </li>
                    <li>
                        <a href="/local/intro/protect.php">개인정보취급방침</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="introduction_area">
        <h4>
            온라인 연수원은<br />
            재능그룹 임직원과 멘토의 학습 및 지식공유를 위한 통합 교육 플랫폼입니다.
        </h4>
        <p>
            빠르게 변화하는 사회에서 기업과 개인이 경쟁력을 갖추기 위해서는 끊임없는 자기개발과 혁신이 필요합니다. <br />
            온라인 연수원은 ‘A Better Life Through Better Education’의 비전을 공유하는 구성원 모두가 각 분야 전문가로서 탄탄한 역량을 갖추고 재능 안에서 자신의 목표를 이룰 수 있도록 지원합니다. 
        </p>
        <p>이를 위해 온라인 연수원은 다음과 같은 온라인 학습환경을 제공합니다.</p>

        <div class="br_box">
            <h6>Smart Learning</h6>
            <ul>
                <li>온라인 연수원 콘텐츠는 PC, Mobile 등 모든 디지털기기에서 학습 가능합니다.</li>
                <li>온라인 연수원은 학습자 맞춤형 콘텐츠를 추천합니다.</li>
            </ul>
            <h6>Formal+Informal Learning</h6>
            <ul>
                <li>Moodle 기반의 교육과정을 운영하여 체계적 과정 학습이 가능합니다.</li>
                <li>5분 내외 마이크로러닝 콘텐츠의 제공을 통해 효율적 학습을 지원합니다.</li>
            </ul>
            <h6>Self-Learning</h6>
            <ul>
                <li>학습자 스스로 목표 학습포인트를 관리하며 교육계획을 수립합니다.</li>
                <li>다양한 학습관련 이벤트를 통해 학습자가 지속적으로 동기부여될 수 있도록 합니다.</li>
            </ul>
        </div>
        <p>온라인 연수원은 임직원과 멘토의 다양한 학습요구를 적극적으로 수용하여 교육과정과 콘텐츠를 개선・혁신함으로써, 학습자의 핵심역량 개발 및 전문성 신장을 위해 늘 최선의 노력을 다하겠습니다.</p>
        <p>감사합니다.</p>
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>


