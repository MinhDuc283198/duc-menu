<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
require('../config.php');
require('../user/lib.php');
require_once($CFG->libdir.'/authlib.php');
require_once($CFG->libdir.'/moodlelib.php');
require_once($CFG->libdir.'/testing/generator/data_generator.php');
require_once $CFG->dirroot . '/local/jinoboard/lib.php';
require_once('lib.php');
// Check for valid admin user - no guest autologin
$context = context_system::instance();

require_once($CFG->libdir . '/datalib.php');

$mod = optional_param("mod", "", PARAM_TEXT);
$id = optional_param("id", 0, PARAM_INT);
$file_id = optional_param("file_id", 0, PARAM_INT);
$file_del = optional_param("file_del", 0, PARAM_INT);
$itemid = optional_param("itemid", 0, PARAM_INT);

$usercreated = false;
$usernew = new stdClass();
$usernew->auth = 'manual';
$usernew->timemodified = time();
$usernew->lang = "ko";
  
$usernew->firstname = '&nbsp;';
$usernew->lastname = trim(optional_param('lastname', ' ', PARAM_RAW));
   


if(empty(trim(optional_param('email01', ' ', PARAM_RAW))) || empty(trim(optional_param('email02', ' ', PARAM_RAW)))){
    
}else{
$usernew->email = trim(optional_param('email01', ' ', PARAM_RAW)).'@'.trim(optional_param('email02', ' ', PARAM_RAW));
}
$usernew->username = trim(optional_param('username', ' ', PARAM_RAW));
if(empty($usernew->phone1 = optional_param('tel01', ' ', PARAM_RAW)) || empty($usernew->phone1 = optional_param('tel02', ' ', PARAM_RAW)) || empty($usernew->phone1 = optional_param('tel03', ' ', PARAM_RAW))){
    $usernew->phone1 = '';
}else{
    $usernew->phone1 = optional_param('tel01', ' ', PARAM_RAW).'-'.optional_param('tel02', ' ', PARAM_RAW).'-'.optional_param('tel03', ' ', PARAM_RAW);
}
if(empty(optional_param('phone01', ' ', PARAM_RAW)) || empty(optional_param('phone02', ' ', PARAM_RAW)) || empty(optional_param('phone03', ' ', PARAM_RAW))){
    $usernew->phone2= '';
}else{
    $usernew->phone2 = optional_param('phone01', ' ', PARAM_RAW).'-'.optional_param('phone02', ' ', PARAM_RAW).'-'.optional_param('phone03', ' ', PARAM_RAW);
}




$usernew->mnethostid = $CFG->mnet_localhost_id; // Always local user.
$usernew->confirmed  = 1;
$usernew->timecreated = time();

if ($mod == 'write') {
   // $usernamefield = optional_param('userid', false, PARAM_RAW);
    //$usernew->username = trim(core_text::strtolower($usernamefield)); 
    $password = trim(optional_param('password', ' ', PARAM_RAW));
    $usernew->password = hash_internal_user_password($password);
    $authplugin = get_auth_plugin($usernew->auth);
    $usernew->id = $DB->insert_record('user',$usernew);
    
    $filename = $_FILES['uploadfile']['name'];
    $filepath = $_FILES['uploadfile']['tmp_name'];
    
    
    $lmsdatauser = new stdClass();
    $lmsdatauser->userid = $usernew->id;
    $lmsdatauser->eng_name = optional_param('eng_name',' ',PARAM_RAW);    
    $lmsdatauser->zh_name = optional_param('zh_name',' ',PARAM_RAW);    
    $lmsdatauser->status = 2;
    $entdate =optional_param('entdate', ' ', PARAM_RAW);
    $lmsdatauser->entdate = strtotime($entdate);
    $lmsdatauser->isused = 'Y';
    $lmsdatauser->istemp = 'N';
    $lmsdatauser->deptname = optional_param('deptname',' ', PARAM_RAW);
    $lmsdatauser->workplace = optional_param('workplace',' ', PARAM_RAW);
    $lmsdatauser->positionsysname = optional_param('positionsysname', ' ', PARAM_RAW);
    $lmsdatauser->lastlearn = optional_param('lastlearn',' ', PARAM_RAW);
    $lmsdatauser->target = optional_param('target',' ', PARAM_RAW);
    $lmsdatauser->linux_level = optional_param('linux_level',' ', PARAM_RAW);
    $lmsdatauser->comphoneno = optional_param('comphoneno',' ', PARAM_RAW);
    $lmsdatauser->insertdate = time();
    $lmsdatauser->lastupdatedate = time();
    
    $DB->insert_record('lmsdata_user',$lmsdatauser);


    if (!$authplugin->is_internal() and $authplugin->can_change_password() and !empty($usernew->password)) {
        if (!$authplugin->user_update_password($usernew, $usernew->password)) {
            // Do not stop here, we need to finish user creation.
            debugging(get_string('cannotupdatepasswordonextauth', '', '', $usernew->auth), DEBUG_NONE);
        }
    }
    

    $usercreated = true;

    $usercontext = context_user::instance($usernew->id);
    
    
    //save user picture
    if(!empty($_FILES['uploadfile']['tmp_name'])){
    $draftitemid = file_get_unused_draft_itemid();
    $filerecord = array(
        'contextid' => $usercontext->id,
        'component' => 'user',
        'filearea'  => 'draft',
        'itemid'    => $draftitemid,
        'filepath'  => '/',
        'filename'  => $filename,
        'userid' => $usernew->id,
        'license' => 'allrightsreserved'
    );

    $fs = get_file_storage();
    $file = $fs->create_file_from_pathname($filerecord, $filepath);

    $newrev = process_new_icon($usercontext, 'user', 'icon', 0, $filepath);
    
    $DB->set_field('user', 'picture', $newrev, array('id'=>$usernew->id));
    }
    // Trigger update/create event, after all fields are stored.
    \core\event\user_created::create_from_userid($usernew->id)->trigger();
   
}
if($mod == 'edit'){
    
    if($usernew->password != null && $usernew->passsword != ''){
    $usernew->password = trim(optional_param('password', ' ', PARAM_RAW));
    $usernew->password = hash_internal_user_password($usernew->password);
}
    
    $user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST);
    $PAGE->set_context(context_user::instance($user->id));
    
    $usercontext = context_user::instance($user->id);

    
    $authplugin = get_auth_plugin($usernew->auth);
    $usernew->id = $id; 
    $usernew->username = $user->username; 
    
    user_update_user($usernew, false, false);
    
    
    $lmsdatauser = $DB->get_record_sql('select * from {lmsdata_user} where userid = :userid ', array('userid'=>$id));    
    
    $lmsdatauser->eng_name = optional_param('eng_name',' ',PARAM_RAW);    
    $lmsdatauser->zh_name = optional_param('zh_name',' ',PARAM_RAW);    
    $lmsdatauser->status = optional_param('status', ' ', PARAM_INT);
    $entdate =optional_param('entdate', ' ', PARAM_RAW);
    $lmsdatauser->entdate = strtotime($entdate);
    $lmsdatauser->isused = 'Y';
    $lmsdatauser->istemp = 'N';
    $lmsdatauser->deptname = optional_param('deptname',' ', PARAM_RAW);
    $lmsdatauser->workplace = optional_param('workplace',' ', PARAM_RAW);
    $lmsdatauser->positionsysname = optional_param('positionsysname', ' ', PARAM_RAW);
    $lmsdatauser->lastlearn = optional_param('lastlearn',' ', PARAM_RAW);
    $lmsdatauser->target = optional_param('target',' ', PARAM_RAW);
    $lmsdatauser->linux_level = optional_param('linux_level',' ', PARAM_RAW);
    $lmsdatauser->comphoneno = optional_param('comphoneno',' ', PARAM_RAW);
    $lmsdatauser->insertdate = time();
    $lmsdatauser->lastupdatedate = time();
    
    $DB->update_record('lmsdata_user', $lmsdatauser);
    
    if (!$authplugin->is_internal() and $authplugin->can_change_password() and !empty($usernew->password)) {
        if (!$authplugin->user_update_password($usernew, $usernew->password)) {
            // Do not stop here, we need to finish user creation.
            debugging(get_string('cannotupdatepasswordonextauth', '', '', $usernew->auth), DEBUG_NONE);
        }
    }
    $usercreated = true;
    
    

    if ($user->id == $USER->id) {
        // Override old $USER session variable.
        foreach ((array)$usernew as $variable => $value) {
            if ($variable === 'description' or $variable === 'password') {
                // These are not set for security nad perf reasons.
                continue;
            }
            $USER->$variable = $value;
    }
}
    
if($file_id != 0){
        $context = context_system::instance();

        $fs = get_file_storage();
        
        if(!empty($_FILES['uploadfile']['tmp_name'])) $file_del = 1;

        if($mod == 'edit' && !empty($id) && $file_del == 1){
            $overlap_files = $DB->get_records('files', array('itemid'=> $file_id));
            foreach($overlap_files as $file){
                $fs->get_file_instance($file)->delete();
                $icon_files = $DB->get_records('files', array('contextid'=> $file->contextid, 'component'=>'user', 'filearea'=>'icon'));
                foreach($icon_files as $ifile){
                    $fs->get_file_instance($ifile)->delete();
                }
            }
        }
        
    if(!empty($_FILES['uploadfile']['tmp_name'])){

        //save user picture
        $filename = $_FILES['uploadfile']['name'];
        $filepath = $_FILES['uploadfile']['tmp_name'];

        $draftitemid = file_get_unused_draft_itemid();
        $filerecord = array(
            'contextid' => $usercontext->id,
            'component' => 'user',
            'filearea'  => 'draft',
            'itemid'    => $draftitemid,
            'filepath'  => '/',
            'filename'  => $filename,
            'userid' => $usernew->id,
            'license' => 'allrightsreserved'
        );

        $fs = get_file_storage();
        $file = $fs->create_file_from_pathname($filerecord, $filepath);

        $newrev = process_new_icon($usercontext, 'user', 'icon', 0, $filepath);

        $DB->set_field('user', 'picture', $newrev, array('id'=>$usernew->id));
    }
}


}

echo '<script type="text/javascript">document.location.href="./index.php"</script>';