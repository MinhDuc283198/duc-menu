<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="popwrap">
    <div class="pop_title">
        <span class="line">포인트안내</span>
        <img class="close r_content" src="/theme/oklassedu/pix/images/icon_pop_close.png" alt="" />
    </div>

    <div class="pop_content">
           <div class="point_txt">
               
               <div class="bx">
                    <h6>자격충족 포인트</h6>
                    <p>멘토로서 업무를 수행하기 위해 기본적으로 획득해야하는 학습 포인트입니다. <br/>자격충족 포인트는 항상 기준의 100%를 유지해야 합니다.</p>
                   <ul>
                       <li> <span class="tit">1) 획   득:</span> 신임멘토과정(가칭)을 이수하면 자격유지 포인트를 100% 얻게 됩니다.</li>
                        <li>
                            <span class="tit">2) 차   감:</span> 신임멘토과정 중 일부 콘텐츠가 교체되면 그 콘텐츠의 포인트가 줄어듭니다. <br/>
                            <span>(차감내역은 마이페이지>수강정보>학습포인트현황에서 확인 가능)<span>
                        </li>
                        <li><span class="tit">3) 재획득:</span> 교체된 새 콘텐츠를 다시 학습하면 차감된 포인트를 다시 획득해 멘토 자격을 유지할 수 있습니다.</li>
                   </ul>
                   <p class="t-pink">* 포인트가 차감된 시점부터 3개월 안에 해당 포인트를 재획득하지 않으면 멘토자격이 정지됩니다.</p>
               
                   <h6 class="top_line">연차교육 포인트</h6>
                   <p>멘토의 연차별 업무역량 강화를 위해 필수 이수하고 획득해야 하는 학습 포인트입니다. <br/>연차별 교육 대상자로 학습 안내를 받으면 기간내 해당과정을 학습하고 포인트를 획득해야합니다.</p>
                   <ul>
                       <li><span class="tit">1) 획   득:</span> 1년차, 3년차, 5년차 멘토를 위해 준비된 연차별과정의 대상자로 선정이 되면 기간내에 해당 과정을 <br/>
                            <span>이수하고 포인트를 얻게 됩니다.</span>
                        </li>
                   </ul>

                   <p class="t-pink">* 대상자로 선정된 시점부터 1년 안에 연차별 교육과정을 이수하지 않으면 패널티가 부여됩니다.</p>
               </div>
              
           </div>
           
           
       </div>
</div>