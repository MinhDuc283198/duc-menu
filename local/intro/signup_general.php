<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * user signup page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require($CFG->dirroot.'/login/lib.php');

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$PAGE->set_url('/local/intro/signup.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('edu02');
$PAGE->navbar->add("회원가입");

echo $OUTPUT->header();

$mod = optional_param('mod', 'write', PARAM_RAW);

if ($USER->id != null) {
    redirect($CFG->wwwroot);
}
$js = array(
    '../../login/js/ckeditor-4.3/ckeditor.js',
    '../../login/js/ckfinder-2.4/ckfinder.js',
    $CFG->wwwroot . '/chamktu/manage/course_list.js'
);
?>  
<script type="text/javascript">
    $(document).ready(function () {
        confirmemail = 1;
        $(".confirmtext").hide();
    })


</script>
<style>
    #page-local-intro-signup div[role=main] {
        padding: 40px;
        border: 1px solid #ddd;
    }
    .sign-box {
        border: 1px solid #ddd;
        height: 150px;
        padding: 20px;
        overflow: auto;
    }
</style>
<link href="/chamktu/css/jquery-ui.css" rel="stylesheet" />
<script type="text/javascript" src="/chamktu/js/lib/jquery.ui.datepicker_lang.js"></script>
<h3 class="page_title margin_bt30">
    일반회원 가입
</h3>

<h3 class="div_title sub"> 이용약관 </h3>
<div class="sign-box">
    <?php echo get_string('terms01', 'local_intro')  ?>
</div>
<br><br>
<h3 class="div_title sub">개인정보 수집 및 이용 동의</h3>
<div class="sign-box">
    <?php echo get_string('policytext', 'local_intro')  ?>
</div>
<br><br>
<div class="input_group full" style="">
    <h3 class="div_title sub">한양브레인소프트 홈페이지 이용에 따른 개인정보 수집·이용에 대한 안내</h3>
    <br>
    한양브레인소프트는 회원 가입 및 이용을 위한 개인정보 수집 • 이용을 위하여 『개인정보보호법 제15조 및 제22조』에 따라 정보주체의 동의를 받고자 하오니 내용을 자세히 읽으신 후 동의 여부를 결정하여 주시기 바랍니다. <br>
    <br>
    
    <strong>-개인정보 수집 • 이용 안내</strong><br>
    <table border="1" cellsapcing="0" style="margin: 10px 0">
        <tr>
            <th style="padding:10px; text-align:center">수집하는 개인정보 항목</th>
            <th style="padding:10px; text-align:center">개인정보 수집 및 이용 목적</th>
            <th style="padding:10px; text-align:center">개인정보의 보유 및 이용 기간 </th>
        </tr>
        <tr>
            <td style="padding:10px">(필수)아이디,성명,이메일,휴대폰번호, 소속정보,최종학력,전공</td>
            <td style="padding:10px">
                ·서비스 제공에 관한 계약 이행 및 서비스 제공에 따른 요금정산<br>
                - 콘텐츠(수업, 강의, 학습콘텐츠 등) 제공, 구매/수강신청 및 결제<br>
                ·회원관리 <br>
                 회원제 서비스 이용에 따른 본인확인, 개인 식별, 불량회원의 부정 이용 방지와 비인가 사용 방지, 가입 의사 확인, 연령확인<br>
                ο 기타<br>
                - 분쟁조정을 위한 기록보존, 회원의 각종 통계자료 산출 <br>
            </td>
            <td style="padding:10px">
                ·이용계약에 따른 회원 자격이 유지되는 기간동안<br>
                그리고 관계법령의 규정에 의하여 보존할 필요가 있는 경우에는 아래와 같이 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다.<br>
               ο 표시 광고에 관한 기록 : 6개월 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 계약 또는 청약철회 등에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 대금결제 및 재화 등의 공급에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 소비자의 불만 >또는 분쟁처리에 관한 기록 : 3년 (전자상거래등에서의 소비자보호에 관한 법률)<br>
               ο 신용정보의 수집 처리 및 이용 등에 관한 기록 : 3년 (신용정보의 이용 및 보호에 관한 법률)<br>
               ο 통신비밀보호법에 따른 통신사실확인자료 : 3개월<br>
                <br>
               ·회원의 개인정보 삭제 요청 시 즉시 삭제 조치<br>
            </td>
        </tr>
    </table>
    <p style="padding-bottom:5px; margin-top:10px;"><strong>-동의를 거부할 권리 및 동의 거부에 따른 불이익</strong></p>
    <div style="line-height:25px;">
        귀하는 개인정보의 수집·이용에 동의를 거부할 권리가 있습니다.<br>
       그러나 필수항목에 대한 동의를 거부할 경우, 한양브레인소프트 회원가입 및 이용이 제한됨을 안내 드립니다. <br>
       <label for="agreeok">동의</label><input type="radio" name="agreement" id="agreeok" value="1" /> &nbsp;&nbsp;
       <label for="agreeno">비동의</label><input type="radio" name="agreement" id="agreeno" value="0" />
    </div>
</div>

<form id="frm_popup_submit" name="sub" method="POST" action="#">
    <input type="hidden" name="mod" value="write">

    <table class="table">
        <tbody>
            <!-- 일반회원 -->
            <tr>
                <th class="w150px">아이디<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="username" title="아이디" class="m-col1" /> 
            <?php if ($mod == 'write') { ?><input type="button" name="id_chk" value="<?php echo '중복확인'; ?>" class="btn gray"><?php } ?>
        </td>
        </tr>
        <tr>
            <th class="w150px">비밀번호<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="password" name="password" id="password" title="비밀번호" class="m-col1" onkeyup="passwordConfirmed1();" maxlength="16"/>&nbsp;&nbsp; <div style="color:grey;">※비밀번호는 6~16자 영문 대 소문자, 숫자, 특수문자 2개 이상 조합을 사용하세요. <br> ※보안수준 보통 이상 필요</div><br>
            <div id="passwordtext"></div>
        </td>
        </tr>
        <tr>
            <th class="w150px">비밀번호 확인<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="password" name="password2" id="password2" title="비밀번호" class="m-col1" onkeyup="passwordConfirmed2()" maxlength="16"/> <div id="passwordchecktext"></div>
        </td>
        </tr>
        <tr>
            <th class="w150px">이름<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="lastname" title="이름" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">생년월일</th>
        <td class="text-left">
            <input type="text"  name="birthday" id="birthday" title="생년월일"  class="w_120" placeholder="클릭하세요"/> 
        </td>
        </tr>
        <tr>
            <th class="w150px">주소</th>
        <td class="text-left">
            <input type="text" name="zipcode" id="address1" title="우편번호" placeholder="우편번호"><input type="button" onclick="execDaumPostcode()" class="btn gray email-btn" value="우편번호 찾기"><br>
            <input type="text" name="city" id="address2" title="주소" class="m-col1" placeholder="주소" /><input type="text" name="address" title="상세주소" id="address3"  class="m-col1" placeholder="상세주소" />
        </td>
        </tr>
        <tr>
            <th class="w150px">기관명<sapn class="red">*</sapn></th>
        <td class="text-left">
            <input type="text" name="workplace" title="기관명" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">부서</th>
        <td class="text-left">
            <input type="text" name="functionsysname" title="부서" class="m-col1" /> 
        </td>
        </tr>
        <tr>
            <th class="w150px">이메일<sapn class="red">*</sapn></th>
        <td class="text-left">
            <div class="confirmed">
                <input type="text" name="email01" title="전자우편" class="email01" />@
                <input type="text" name="email02" class="email02" title="전자우편"  />
                <input type="hidden" name="econfirm" id="econfirm"  />
                <input type="button" name="email_chk" class="btn gray email-btn" value="이메일인증">
            </div>
            <div class="confirmtext">인증되었습니다</div>
            <div class="colorbox gray emailbox" id="emailbox">
                <input type="text" title="인증번호" name="userconfirm" placeholder="인증번호 입력" />
                <input type="button" value="인증확인" onclick="email_confirm()" class="btn gray" />
            </div>
        </td>
        </tr>

        <tr>
            <th class="w150px">일반연락처</th><!--일반전화 phone1-->
            <td class="text-left">
                <input type="text" name="tel01" title="number01" class="w100px " />
                <input type="text" name="tel02" title="number02" class="w100px" />
                <input type="text" name="tel03" title="number03" class="w100px"/>
            </td>
        </tr>
        <tr>
            <th class="w150px">휴대폰<sapn class="red">*</sapn></th><!--연락처 phone2-->
        <td class="text-left">
            <input type="text" name="phone01" title="number01" class="w100px " />
            <input type="text" name="phone02" title="number02" class="w100px" />
            <input type="text" name="phone03" title="number03" class="w100px"/>
        </td>
        </tr>
        <tr>
            <th class="w150px">최종학력<sapn class="red">*</sapn></th>
        <td class="text-left">
            <select name="lastlearn" class="m-col1">
                <option value="초등학교">초등학교</option>
                <option value="중학교">중학교</option>
                <option value="고등학교">고등학교</option>
                <option value="대학(2,3년제)">대학(2,3년제)</option>
                <option value="대학교">대학교</option>
                <option value="대학원(석사)">대학원(석사)</option>
                <option value="대학원(박사)">대학원(박사)</option>
            </select>
        </td>
        </tr>
        <tr>
            <th class="w150px">전공</th>
        <td class="text-left">
            <input type="text" name="functionsysid" title="전공" class="m-col1" /> 
        </td>
        </tr>
        <!-- 일반회원 -->
        </tbody>
    </table>
    <div class="text-right">
        <input type="submit" value="가입하기" class="btn big "/>
    </div>
</form>

<?php
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    $(function () { //전체선택 체크박스 클릭 
        $("#check03").click(function () {
            //만약 전체 선택 체크박스가 체크된상태일경우 
            if ($("#check03").prop("checked")) {
                //해당화면에 전체 checkbox들을 체크해준다 
                $("input[type=checkbox]").prop("checked", true);
                // 전체선택 체크박스가 해제된 경우
            } else {
                //해당화면에 모든 checkbox들의 체크를해제시킨다. 
                $("input[type=checkbox]").prop("checked", false);
            }
        })
    })
    function randomString() {
        var chars = "0123456789";
        var string_length = 6;
        var randomstring = '';
        for (var i = 0; i < string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
//document.randform.randomfield.value = randomstring;
        return randomstring;
    }
    var random = randomString();
    $("input[name=email_chk]").click(function () {
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

        var email = $(this).siblings("input[name=email01]").val() + "@"
        email += $(this).siblings("input[name=email02]").val();

        if (email.match(regExp) == null) {
            confirmemail = 0;
            alert("이메일 주소를 정확히 입력해주세요.");
            $(this).siblings("input[name=email01]").focus();

        } else {
            confirmemail = 1;
        }

        if (confirmemail != 0) {
            $.ajax({
                type: 'POST',
                data: {
                    email01: $("input[name=email01]").val(),
                    email02: $("input[name=email02]").val(),
                    random: random,
                },
                url: './email_submit.php',
                success: function (data) {
                    if (data == 'used') {
                        alert('사용중인 이메일입니다.');
                    } else {
                        // var e = $(data).find('#ran');
                        alert('이메일이 발송되었습니다.');
                        $('.email-btn').val('재발송');
                        $(".emailbox").show();
                        $("#econfirm").val(random);
                    }
                },
            })
        }
    });
           var econ = 0;
    function email_confirm() {

        var econfirm = $("input[name=econfirm]").val();
        var userconfirm = $("input[name=userconfirm]").val();
        if (econfirm.trim() != userconfirm.trim()) {
            alert("인증번호가 다릅니다.");
            econ = 0;
        } else {
            alert("인증되었습니다.");
            $("#emailbox").hide();
            $(".confirmed").hide();
            $(".confirmtext").show();
            econ = 1;
        }
        ;
    }

    $(document).ready(function () {
        $(' input[name=pw_edit]').click(function () {
            if ($(' input[name=pw_edit]').prop('checked') == true) {
                $(" input[name=password]").prop('disabled', false);
                $(" input[name=repassword]").prop('disabled', false);
            } else {
                $(" input[name=password]").prop('disabled', true);
                $(" input[name=repassword]").prop('disabled', true);
            }
        });

        $('#admin_list').click(function () {
            location.href = "./infadmin.php";
        });

        var check_true = 0;
        $("input[name=userid]").keyup(function () {
            check_true = 0;
        });

        $("input[name=id_chk]").click(function () {
            $.ajax({
                type: 'POST',
                data: {
                    userid: $("input[name=username]").val(),
                },
                url: './signup_id_check.php',
                success: function (data) {
                    if ($("input[name=username]").val() == null || $("input[name=username]").val() == '') {
                        alert('아이디를 입력해 주세요.');
                        $("input[name=username]").focus();
                        return false;
                    } else {
                        if (data == false) {
                            alert("현재 사용중인 아이디 입니다.");
                        }else if(!(isValidFormId($("input[name=username]").val()))){
                            alert("아이디는 첫 문자 영문, 5~20자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다.");
                        } else {
                            alert("사용 가능한 아이디 입니다.");
                            check_true = 1;
                        }
                    }
                },
            })
        });


        $('#frm_popup_submit').submit(function (event) {
            var username = $("input[name=username]").val();
            if (username.trim() == '') {
                alert("아이디를 입력해 주세요");
                return false;
            }

<?php
if ($mod == 'write') {
    ?>
                if (check_true == 0) {
                    alert("중복조회를 해주세요");
                    return false;
                }
    <?php
}
?>
            if ($(" input[name=password]").prop('disabled') == false) {
                var password = $("input[name=password]").val();
                if (password.trim() == '') {
                    alert("비밀번호를 입력해 주세요");
                    return false;
                } else {
                    if (!isValidFormPassword(password)) {
                        alert("비밀번호는 6~16자 영문 대 소문자, 숫자, 특수문자 2개 이상 조합을 사용하세요.");
                        return false;
                    }
                }

                var password2 = $("input[name=password2]").val();
                if (password2.trim() == '') {
                    alert("비밀번호 확인을 입력해 주세요");
                    return false;
                }

                if (password != password2) {
                    alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
                    return false;
                }
            }
            var lastname = $("input[name=lastname]").val();
            if (lastname.trim() == '') {
                alert("이름을 입력해 주세요");
                return false;
            }


            var workplace = $("input[name=workplace]").val();
            if (workplace.trim() == '') {
                alert("소속기관을 입력해 주세요");
                return false;
            }

            var email01 = $("input[name=email01]").val();
            if (email01.trim() == '') {
                alert("<?php echo get_string('user_emailalert', 'local_lmsdata'); ?>");
                return false;
            }

            var email02 = $("input[name=email02]").val();
            if (email02.trim() == '') {
                alert("<?php echo get_string('user_emailalert', 'local_lmsdata'); ?>");
                return false;
            }



            if (econ == 0) {
                alert('이메일 인증을 해주세요');
                return false;
            }
            var target = $("input[name=target]").val();
            if (target.trim() == '') {
                alert("중점 연구분야를 입력해 주세요");
                return false;
            }

            var phone01 = $("input[name=phone01]").val();
            var phone02 = $("input[name=phone02]").val();
            var phone03 = $("input[name=phone03]").val();

            if (phone01.trim() == '') {
                alert("연락처를 입력해주세요.");
                return false;
            }
            if (phone02.trim() == '') {
                alert("연락처를 입력해주세요.");
                return false;
            }
            if (phone03.trim() == '') {
                alert("연락처를 입력해주세요.");
                return false;
            }


            var positionsysname = $("input[name=positionsysname]").val();
            if (positionsysname.trim() == '') {
                alert("직위를 입력해 주세요");
                return false;
            }

            var lastlearn = $("input[name=lastlearn]").val();
            if (lastlearn.trim() == '') {
                alert("최종학력을 입력해 주세요");
                return false;
            }

            $(function () {
                $("#check03").click(function () {
                    var chk = $("#check03").is(":checked");

                    if (chk) {
                        $(".chkbox input").each(function () {
                            this.checked = true;
                        });
                    } else {
                        $(".chkbox input").each(function () {
                            this.checked = false;
                        });
                    }
                });

                $("#popup_delete_button").click(function () {
                    if (confirm("<?php echo get_string('delete_confirm', 'local_lmsdata'); ?>")) {
                        $('#form').submit();
                    }
                });
            });
            if ($('input:checkbox[name="check01"]').is(":checked") == false) {
                alert("회원약관에 동의해 주세요");
                return false;
            }
            if ($('input:checkbox[name="check02"]').is(":checked") == false) {
                alert("개인정보 수집 및 이용약관에 동의해 주세요");
                return false;
            }


        });
    });
    /**
     * 비밀번호 유효성 체크
     
     * @param {type} pw
     * @returns {Boolean}     */
    function isValidFormPassword(pw) {
        var check1 = passwordConfirmed1();

        if (!check1) {
            return false;
        }


        return true;
    }
    
    /**
     * 아이디 유효성 체크
     
     * @param {type} id
     * @returns {Boolean}     */
    function isValidFormId(id) {
        var check2 = /^[a-z]{1}[a-z0-9\d_-]{5,20}$/;

        if (!check2.test(id)) {
            return false;
        }


        return true;
    }
    
    function speacialChar(){
        var specialchar = /[$@$!:<>;,/.%*?=&_#|+"'-]/;
        if(!specialchar.test($("#password").val())){
            return false;
        }
        return true;
    }
    function senglishChar(){
        var senglishchar = /[a-z]/;
        if(!senglishchar.test($("#password").val())){
            return false;
        }
        return true;
    }
    function benglishChar(){
        var benglishchar = /[A-Z]/;
        if(!benglishchar.test($("#password").val())){
            return false;
        }
        return true;
    }
    function numberNum(){
        var numbernum = /[0-9]/;
        if(!numbernum.test($("#password").val())){
            return false;
        }
        return true;
    }
    
    function passwordConfirmed1(){
        var password = $("#password").val();
        var passwordCheck = $("#password2").val();
        var special = speacialChar();
        var senglish = senglishChar();
        var benglish = benglishChar();
        var number = numberNum();
        var private = 0;
        
        if(passwordCheck === ""){
            $("#passwordchecktext").html("");
        }else if(password !== passwordCheck){
            $("#passwordchecktext").html("<b><font color=red size=2pt>비밀번호가 일치하지 않습니다</font></b>");
        }else{
            $("#passwordchecktext").html("<b><font color=green size=2pt>비밀번호가 일치합니다</font></b>");
        }
        
        if(password.length < 6){
            $("#passwordtext").html("<b>보안 수준 : <font color=red size=2pt>취약</font></b>");
        }else if(password.length >= 6){
            private = private+1;
            if(special===true){
                private = private+1;
            }
            if(senglish===true){
                private = private+1;
            }
            if(benglish===true){
                private = private+1;
            }
            if(number===true){
                private = private+1;
            }
            if(private===2){
                $("#passwordtext").html("<b>보안 수준 : <font color=red size=2pt>취약</font></b>");
            }else if(private===3){
                $("#passwordtext").html("<b>보안 수준 : <font color=orange size=2pt>보통</font></b>");
            }else if(private===4){
                $("#passwordtext").html("<b>보안 수준 : <font color=green size=2pt>안전</font></b>");
            }else{
                $("#passwordtext").html("<b>보안 수준 : <font color=blue size=2pt>매우 안전</font></b>");
            }
                
        }
        if(private < 3){
            return false;
        }else{
            return true;
        }
    }
    
    function passwordConfirmed2(){
        var password = $("#password").val();
        var passwordCheck = $("#password2").val();
        if(passwordCheck === ""){
            $("#passwordchecktext").html("");
        }else if(password !== passwordCheck){
            $("#passwordchecktext").html("<b><font color=red size=2pt>비밀번호가 일치하지 않습니다</font></b>");
        }else{
            $("#passwordchecktext").html("<b><font color=green size=2pt>비밀번호가 일치합니다</font></b>");
        }
        
    }
    
</script>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script>
    function execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('address1').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('address2').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('address3').focus();
            }
        }).open();
    }
    $(document).ready(
            /**
             * 달력
             * @returns {undefined} */
                    function () {

                        $("#birthday").datepicker({
                            dateFormat: "yy-mm-dd",
                            maxDate:0
                                });
                    });


</script>