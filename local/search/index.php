<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');
require_once($CFG->dirroot . '/local/book/lib.php');
$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_pagelayout('edu');
$PAGE->navbar->add('검색결과');
$searchtext = htmlspecialchars(optional_param('searchtext', '', PARAM_RAW));

$c_page = optional_param('c_page', 0, PARAM_INT);
$c_perpage = optional_param('c_perpage', 5, PARAM_INT);
$b_page = optional_param('b_page', 0, PARAM_INT);
$b_perpage = optional_param('b_perpage', 5, PARAM_INT);
echo $OUTPUT->header();


$c_list = local_course_class_search_title($searchtext,$c_page,$c_perpage)->list;
$c_cnt = local_course_class_search_title($searchtext)->cnt;
$b_list = local_book_search_title($searchtext, $b_page, $b_perpage)->list;
$b_cnt = local_book_search_title($searchtext)->cnt;
?>
<div class="tp-search-area">
    <form action="<?php echo $CFG->wwwroot?>/local/search/index.php" method="get">
        <div class="input-div">
            <input type="text" name="searchtext" value="<?php echo $searchtext?>"  placeholder="<?php echo get_string('search_txt','theme_oklassedu')?>"/>
            <input type="submit" class="btn-search" value="<?php echo get_string('search', 'local_course')?>" />
        </div>
    </form>
</div>
<?php if($c_list && $c_cnt>0){ ?>
<h4 class="pg-tit"><?php echo get_string('course_result','local_course')?> (<?php echo $c_cnt?>)</h4>
<div class="crs-bx no-mg">
    <input type="hidden" name="c_page" value="<?php echo $c_page?>">
    <ul class="crs-list" id='course_list'>
        <?php foreach($c_list as $cl){ ?>
        <li>
            <div class="img">

                <?php if($cl->badge){?>
                <p class="badge-area">
                    <?php foreach($cl->badge as $badge){?>
                    <img src="<?php echo $badge->img?>">
                    <?php }?>
                </p>
                <?php }?>

                <span class="ic-heart <?php echo $cl->likes ? 'on' : ''; ?>" id="like<?php echo $cl->id?>" data-target="<?php echo $cl->id?>"><?php echo get_string('like','local_course')?></span>
                <a href='<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $cl->id?>'>
                <img src="<?php echo $cl->img?>" alt="<?php echo $cl->title?>" />
                </a>
            </div>

            <div class="txt cursor" onclick="location.href='<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $cl->id?>'">
                <div><?php echo $cl->lang_title?></div>
                <p><span><?php echo $cl->teacher_list;?> <?php echo get_string('teacher','local_course')?></span><span><?php echo get_string('total','local_course')?> <?php echo $cl->lecturecnt?><?php echo get_string('class','local_course')?></span></p>
                <p class="t-gray"><?php echo $cl->courseperiod_txt?></p>
                <?php
                $keywords = explode('#', $cl->lang_keywords);
                $keywords = array_filter($keywords);
                if(!empty($keywords)){
                ?>
                <ul class="hash-area">
                    <?php foreach($keywords as $keyword){?>
                    <li data-title="<?php echo $keyword?>"><a href="#"><?php echo $keyword?></a></li>
                    <?php }?>
                </ul>
                <?php }?>
            </div>

            <div class="bt-area">
                <a href='<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $cl->id?>' class="btns <?php echo $cl->button_color?>"><?php echo $cl->button_txt?></a>
            </div>

        </li>
        <?php }?>
    </ul>
</div>
<?php if($c_cnt > 5){?>
<a href="#" class="btn-more mg-bt60" id="coruse_plus" data-page="<?php echo $c_page+1?>"><?php echo get_string('viewmore', 'local_course')?></a>
<?php }?>
<?php }?>

<?php if($b_list && $b_cnt>0){ ?>
<h4 class="pg-tit"><?php echo get_string('book_result','local_course')?> (<?php echo $b_cnt?>)</h4>
<div class="crs-bx">
    <ul class="crs-list book big" id='book_list'>
        <?php
        foreach($b_list as $bl){
        ?>
        <li>
            <a href="/local/book/detail.php?id=<?php echo $bl->id?>">
            <div class="img">
                <img src="<?php echo $bl->img?>" alt="<?php echo $bl->title?>" />
            </div>
            </a>
<!--            <div class="txt cursor">-->
            <div class="txt cursor" onclick="location.href='<?php echo $CFG->wwwroot?>/local/book/detail.php?id=<?php echo $bl->id?>'">
                <div><?php echo $bl->lang_title?></div>
                <p><span><?php echo $bl->lang_publisher?></span><span><?php echo $bl->lang_author?></span></p>
                <p class="f16 t-blue">
                    <?php if ($bl->discount) { ?><strong class="p-down"><span><?php echo $bl->originprice_com ?> VND</span><span><?php echo $bl->discount ?>%</span></strong>
                    <?php echo $bl->price_com ?> VND<?php }else{ ?>
                    <?php echo '<span>'.$bl->price_com. 'VND</span>'; }?>
                </p>
                <?php
                $keywords = explode('#', $bl->lang_keywords);
                $keywords = array_filter($keywords);
                if(!empty($keywords)){
                ?>
                <ul class="hash-area">
                    <?php foreach($keywords as $keyword){?>
                    <li data-title="<?php echo $keyword?>"><a href="#"><?php echo $keyword?></a></li>
                    <?php }?>
                </ul>
                <?php }?>
                <?php if($bl->relation_course){ ?>
                <p class="f18">[<?php echo get_string('rcourse','local_book')?>]</p>
                <ul>
                    <?php foreach($bl->relation_course as $blrc){ ?>
                    <a href='<?php echo $CFG->wwwroot?>/local/course/detail.php?id=<?php echo $blrc->id?>'><li><?php echo $blrc->lang_title?></li></a>
                    <?php }?>
                </ul>
                <?php }?>


                <?php if($bl->relation_book){ ?>
                <p class="f18">[<?php echo get_string('rtextbook','local_book')?>]</p>
                <ul>
                    <?php foreach($bl->relation_book as $blrb){ ?>
                    <a href='<?php echo $CFG->wwwroot?>/local/book/detail.php?id=<?php echo $blrb->id?>'><li><?php echo $blrb->lang_title?></li></a>
                    <?php }?>
                </ul>
                <?php }?>
            </div>
            <div class="bt-area">
                <a href="/local/book/detail.php?id=<?php echo $bl->id?>" class="btns br"><?php echo get_string('readmore','local_course')?></a>
            </div>
        </li>
        <?php }?>
    </ul>
</div>
<?php if($b_cnt > 5){?>
<a href="#" class="btn-more mg-bt60" id="book_plus" data-page="<?php echo $b_page+1?>"><?php echo get_string('viewmore', 'local_course')?></a>
<?php }?>
<?php }?>

<?php if($c_cnt < 1 && $b_cnt < 1){?>
<div class="no-data style02">
    <div class="f24">"<strong class="t-blue"><?php echo $searchtext?></strong>"<?php echo get_string('searcn_empty','local_course')?>
</div>
<?php }?>
<?php require ($CFG->dirroot . '/local/course/recommendpage.php');?>
<script type="text/javascript">
function isNotEmpty(value){
    return value != "";
}
$(document).ready(function () {
    $('#coruse_plus').on('click', function() {
       var page = $(this).data("page");
       var searchtext = '<?php echo $searchtext?>';
       var result = call_ajax('course', {page: page, search:searchtext}, false)
       if(result.status == 'success'){
           $(this).data("page",page+1);
           $.each(result.list, function(key, val) {
               //좋아요
               $likeson = '';
               if(val.likes){
                   $likeson = 'on';
               }
               $keywords = '';
               $keywords_list = val.lang_keywords;
               $keywords_list = $keywords_list.split('#');
               $keywords_list = $keywords_list.filter(isNotEmpty);
               if($keywords_list){
                    //$keywords = '<a href="/local/course/detail.php?id='+val.id+'">';
                    $keywords += '<ul class="hash-area">';
                     for ( var i in $keywords_list ) {
                        $keywords += '<li data-title="'+$keywords_list[i]+'"><a href="#">' + $keywords_list[i] + '</a></li>';
                      }
                    $keywords += '</ul>';
               }

               $badge = '';
               $badges = '';
               $badge_list = val.badge;
               if($badge_list){
                   $badge += '<p class="badge-area">';
                    for ( var i in $badge_list ) {
                        $badges += '<img src="'+$badge_list[i]['img']+'">';
                    }
                    $badge += $badges+'</p>';
                }


               $sli = '<li>\n';
               $img = '<div class="img">'+$badge+'<span class="ic-heart '+$likeson+'" id="like'+val.id+'" data-target="'+val.id+'"><?php echo get_string('like','local_course')?></span><a href="/local/course/detail.php?id='+val.id+'"><img src="'+val.img+'" alt="'+val.title+'"></a> </div>';
               $txt = '<div class="txt"><div><a href="/local/course/detail.php?id='+val.id+'">'+val.lang_title+'</a></div><p><a href="/local/course/detail.php?id='+val.id+'"><span>'+val.teacher_list+'<?php echo get_string('teacher','local_course')?></span><span><?php echo get_string('total','local_course')?> '+val.lecturecnt+'<?php echo get_string('class','local_course')?></span></a></p><p class="t-gray"><a href="/local/course/detail.php?id='+val.id+'">'+val.courseperiod_txt+'</p></a>'+$keywords+'</div>';
               $btn = '<div class="bt-area"><a href="/local/course/detail.php?id='+val.id+'" class="btns '+val.button_color+'">'+val.button_txt+'</a></div>';
               $eli = '</li>\n';
               $result = $sli+$img+$txt+$btn+$eli;
               $('#course_list').append($result);
           });
           if(result.next != true){
               $('#coruse_plus').remove();
           }
        }
        return false;
    });



    $('#book_plus').on('click', function() {
       var page = $(this).data("page");
       var searchtext = '<?php echo $searchtext?>';
       var result = call_ajax('book', {page: page, search:searchtext}, false);

       if(result.status == 'success'){
           $(this).data("page",page+1);
           $.each(result.list, function(key, val) {

               $keywords = '';
               $keywords_list = val.lang_keywords;
               $keywords_list = $keywords_list.split('#');
               $keywords_list = $keywords_list.filter(isNotEmpty);
               if($keywords_list){
                    //$keywords = '<a href="/local/course/detail.php?id='+val.id+'">';
                    $keywords += '<ul class="hash-area">';
                     for ( var i in $keywords_list ) {
                        $keywords += '<li data-title="'+$keywords_list[i]+'"><a href="#">' + $keywords_list[i] + '</a></li>';
                      }
                    $keywords += '</ul>';
               }


               $sli = '<li>';
               $price = '<span>'+val.price_com+' VND</span>';
               if(val.discount != 0){
                   $discount = '<strong class="p-down"><span>'+val.originprice_com+' VND </span><span>'+val.discount+'%</span></strong>'+val.price_com+'VND';
               }else{
                   $discount = $price;
               }
               $img = '<a href="/local/book/detail.php?id='+val.id+'"><div class="img"><img src="'+val.img+'" alt="'+val.lang_title+'" /></div></a>';
               $txt = '<div class="txt"><div><a href="/local/book/detail.php?id='+val.id+'">'+val.title+'</a></div><p><a href="/local/book/detail.php?id='+val.id+'"><span>'+val.lang_publisher+'</span><span>'+val.lang_author+'</span></p><p class="f16 t-blue">'+$discount+'</a></p>'+$keywords;

               $rcourse = '';
               if(val.relation_course != ''){
                   $rcourse += '<p class="f18">['+'<?php echo get_string('rcourse','local_book');?>'+']</p>';
                   $rcourse += '<ul>';
                   $.each(val.relation_course, function(ckey, cval) {
                       $rcourse += "<a href='<?php echo $CFG->wwwroot?>/local/course/detail.php?id="+cval.id+"'<li>"+cval.lang_title+"</li></a> ";
                   });
               }

               if(val.relation_course != ''){
                   $rcourse += '</ul>';
               }


               $rbook = '';
               if(val.relation_book != ''){
                   $rbook += '<p class="f18">['+'<?php echo get_string('rtextbook','local_book');?>'+']</p>';
                   $rbook += '<ul>';
                   $.each(val.relation_book, function(bkey, bval) {
                       $rbook += "<a href='<?php echo $CFG->wwwroot?>/local/book/detail.php?id="+bval.id+"'<li>"+bval.lang_title+"</li></a> ";
                   });
               }

               if(val.relation_book != ''){
                   $rbook += '</ul>';
               }


               $div_end = '</div>';

               $btn = '<div class="bt-area"><a href="/local/book/detail.php?id='+val.id+'" class="btns br"><?php echo get_string('readmore','local_course')?></a></div>';
               $eli = '</li>';
               $result = $sli+$img+$txt+$rcourse+$rbook+$div_end+$btn+$eli;
               $('#book_list').append($result);
           });
           if(result.next != true){
               $('#book_plus').remove();
           }
        }
    });
});



function call_ajax(action, data, async) {
    var rvalue = false;

    $.ajax({
        type: 'POST',
        url: '/local/search/ajax.php',
        dataType: 'JSON',
        async: async,
        data: {action: action, data: data},
        success: function(result) {
            rvalue = result;
        },
        error: function(xhr, status, error) {
        }
    });

    return rvalue;
}

$('#book_plus').click(function (e) {
    e.preventDefault();
});

</script>
<?php
echo $OUTPUT->footer();
?>


