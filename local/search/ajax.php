<?php
define('AJAX_SCRIPT', true);

require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/local/course/lib.php');
require_once($CFG->dirroot . '/local/book/lib.php');
$action = optional_param('action','',PARAM_RAW);

switch ($action) {
    case 'course':
        $data = required_param_array('data', PARAM_RAW);
        $rvalue = new stdClass();
        $rvalue->status = 'fail';
        //강좌
        $list = local_course_class_search_title($data["search"],$data["page"])->list;
        if($list){
            $rvalue->next = false;
            $rvalue->status = 'success';
            $rvalue->list = $list;
            $nextlist = local_course_class_search_title($data["search"],$data["page"]+1)->list;
            if($nextlist){
                $rvalue->next = true;
            }
        }
        echo json_encode($rvalue);
        break;
    case 'book' :
        $data = required_param_array('data', PARAM_RAW);
        $rvalue = new stdClass();
        $rvalue->status = 'fail';

        $list = local_book_search_title($data["search"],$data["page"])->list;
        if($list){
            $rvalue->next = false;
            $rvalue->status = 'success';
            $rvalue->list = $list;
            $nextlist = local_book_search_title($data["search"],$data["page"]+1)->list;
            if($nextlist){
                $rvalue->next = true;
            }
        }
        echo json_encode($rvalue);
        break;
    default:
        throw new moodle_exception('invalidarguments');
}
