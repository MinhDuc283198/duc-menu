<?php
function xmldb_local_sendmail_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    
    $dbman = $DB->get_manager(); 
    if($oldversion < 2019032501) {     
        $table = new xmldb_table('mailsend_history');  

        $field = new xmldb_field('send_status', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
            
     if($oldversion < 2019032502) {     
        $table = new xmldb_table('mailsend_history');  

            $field = new xmldb_field('done_date', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }
    
     if($oldversion < 2019032600) {     
        $table = new xmldb_table('mailsend_history');  

            $field = new xmldb_field('send_status', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->change_field_default($table, $field); 
        }
                     
    }
    
    if($oldversion < 2019070100) {     
        $table = new xmldb_table('mailsend_history');  

        $field = new xmldb_field('send_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
                     
    }
    
    if($oldversion < 2019070502) {     
        $table = new xmldb_table('mailsend_history');  

        $field = new xmldb_field('send_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field); 
        }            
    }
    
    if($oldversion < 2019070800) {     
        $table = new xmldb_table('mailsend_history');  

        $field = new xmldb_field('targets', XMLDB_TYPE_TEXT, null, null, null, null, null);
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_default($table, $field); 
        }     
        $field = new xmldb_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_default($table, $field); 
        }   
        $field = new xmldb_field('done_date', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_default($table, $field); 
        }   
        $field = new xmldb_field('send_status', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->change_field_default($table, $field); 
        }   
    }
    
    
    return true;
}
