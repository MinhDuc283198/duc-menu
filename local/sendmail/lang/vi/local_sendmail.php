<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$string['pluginname'] = 'Gửi thư';

$string['send'] = 'Gửi thư';
$string['apply'] = 'Gửi';

$string['mailtitle'] = 'Tiêu đề';
$string['matilcontents'] = 'Nội dung';

$string['all_check'] = 'Chọn tất cả';
$string['all_uncheck'] = 'Bỏ chọn tất cả';

$string['user_picture'] = 'Ảnh người dùng';
$string['mobile'] = 'Di động';
 
$string['role'] = 'Quy ước';
$string['student_number'] = 'Số học viên';
$string['affiliation'] = 'Liên kết';
$string['target'] = 'Mục tiêu';

