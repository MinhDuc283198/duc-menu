<?php

namespace local_sendmail\task;
 
/**
 * An example of a scheduled task.
 */
class cron_task extends \core\task\scheduled_task {
    /**
     * Return the task's name as shown in admin screens.
     *
     * @return string
     */
    public function get_name() {
        return get_string('pluginname', 'local_sendmail');
    }
 
    /**
     * Execute the task.
     */
    public function execute() {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . '/chamktu/lib/lib.php');
//        $supportuser = core_user::get_support_user();

        $count = array('success'=>0,'fail'=>0);

        $mails = $DB->get_records('mailsend_history',array('send_status'=>0));

        foreach($mails as $mail){
            $subject = $mail->subject;
            $message = $mail->text;
            $user = $DB->get_record('user',array('id'=>$mail->targets));
            print_object('userid = '.$user->id);
            $sendmail = email_to_user($user, $USER,$subject, $message); 
            if($sendmail){
                if($updatedata = $DB->get_record('mailsend_history', array('id'=>$mail->id))){
                    $updatedata->send_status = 1;
                    $updatedata->done_date = time();
                    $upmail = $DB->update_record('mailsend_history',$updatedata);
                }
                $count['success']++;
             }else{
                 $count['fail']++;
             }
        }
        return $count;
    
    }
}

