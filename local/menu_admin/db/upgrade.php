<?php

function xmldb_local_menu_admin_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    require_once($CFG->libdir . '/eventslib.php');

    $dbman = $DB->get_manager();
    
    if ($oldversion < 2017061300) {  
       $table = new xmldb_table('main_menu');
        
        $field = new xmldb_field('sub_parent', XMLDB_TYPE_INTEGER, '10', null, null, null, '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }
    if ($oldversion < 2019050200) {
        $admenuapply = $DB->get_record('main_menu_apply', array('usergroup'=>'ad'));
        if(empty($admenuapply)){
            $menuapplys = $DB->get_records('main_menu_apply', array('usergroup'=>'rs'));
            foreach($menuapplys as $menuapply) {
                $menuapply->usergroup ='ad';
                $DB->insert_record('main_menu_apply',$menuapply);
            }
        }
        
    }

    return true;
}


