<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function xmldb_local_menu_admin_install() {
    global $DB,$CFG;
    //require_once('../../../config.php');
    require_once($CFG->dirroot.'/local/menu_admin/lib.php');
    
    //depth 1인것들 우선 생성
    createmain_menu(1, 5, 1, '/local/course_application/main.php', 1, '', 2, 1, 1, 0, '과정 안내', 'Course guidance' ,1);
    createmain_menu(1, 7, 1, '/local/mypage/mypage.php',           1, '', 2, 1, 1, 0, '마이페이지', 'My Page', 2);
    createmain_menu(1, 8, 1, '/local/oklearning/searchresult.php', 1, '', 2, 1, 1, 0, '연수자료실', 'training reference', 3);
    createmain_menu(1, 11, 1, '/local/jinoboard/list.php?id=1',    1, '', 2, 1, 1, 0, '커뮤니티', 'Community', 4);

}