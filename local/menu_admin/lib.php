<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// install.php 메뉴 생성
function createmain_menu($depth, $step, $ispopup, $url, $type, $icon, $userid, $required, $isused, $sub_parent, $ko_name, $en_name, $mode){
    global $DB;
    
    $main_menu = new stdClass();
    $main_menu->depth = $depth;
    $main_menu->step = $step;
    $main_menu->ispopup = $ispopup;
    $main_menu->url = $url;
    $main_menu->type = $type;
    $main_menu->icon = icon;
    $main_menu->userid = $userid;
    $main_menu->required = $required;
    $main_menu->isused = $isused;
    $main_menu->timecreated = time();
    $main_menu->timemodified = time();
    $main_menu->sub_parent = $sub_parent;
    
    $id = $DB->insert_record('main_menu',$main_menu);
    
    if($depth == 1) {
        $main_menu->id = $id;
        $main_menu->parent = $id;
        $DB->update_record('main_menu', $main_menu);
    }
    if($mode == 1) {
        createmain_menu_depth($id,2, 2, 1, '/local/course_application/course_list.php?menu_gubun=1', 2, '', 2, 1, 1, 0, '필수과정', 'Required Course');
        createmain_menu_depth($id,2, 3, 1, '/local/course_application/course_list.php?menu_gubun=5', 2, '', 2, 1, 1, 0, '선택과정', 'Selective Course');
        createmain_menu_depth($id,2, 4, 1, '/local/course_application/course_list.php?menu_gubun=6', 2, '', 2, 1, 1, 0, '자격인증', 'Certification Course');
    }else if ($mode ==2) {
        createmain_menu_depth($id,2, 2, 1, '/local/mypage/mycourse.php',            2, '', 2, 1, 1, 0, '수강정보', 'Course Information',$mode);
        createmain_menu_depth($id,2, 6, 1, '/local/mypage/mycontents.php',          2, '', 2, 1, 1, 0, '학습한 콘텐츠', 'My learned content',$mode);
        createmain_menu_depth($id,2, 8, 1, '/local/jinoboard/mylist.php?id=2',      2, '', 2, 1, 1, 0, '1:1 Q&A', '1:1 Q&A',$mode);
        createmain_menu_depth($id,2, 7, 1, '/local/evaluation/index.php?type=2',    2, '', 2, 1, 1, 0, '설문조사', 'Survey',$mode);
        createmain_menu_depth($id,2, 0, 1, '/local/mypage/mycourse.php',            2, '', 2, 1, 2, 0, '나의수강현황', 'mycourse',$mode);
    }else if ($mode ==3) {
        createmain_menu_depth($id,2, 5, 1, '/local/okbooks/index.php',              2, '', 2, 1, 1, 0, '추천도서', 'recommend book list');
        createmain_menu_depth($id,2, 0, 1, '/local/oklearning/reference.php',       2, '', 2, 1, 1, 0, '교육 콘텐츠', 'educational content');
        createmain_menu_depth($id,2, 1, 1, '/local/jinoboard/list.php?id=5',        2, '', 2, 1, 1, 0, '자료실', 'reference Room');
    }else if ($mode ==4) {
        createmain_menu_depth($id,2, 1, 1, '/local/jinoboard/list.php?id=4',        2, '', 2, 1, 1, 0, '자료공유', 'reference');
        createmain_menu_depth($id,2, 2, 1, '/local/jinoboard/list.php?id=3',        2, '', 2, 1, 1, 0, 'FAQ', 'FAQ');
        createmain_menu_depth($id,2, 0, 1, '/local/jinoboard/list.php?id=1',        2, '/local/jinoboard/list.php?id=1', 2, 1, 1, 0, '공지사항', 'notice');
    }
    createmain_menu_name($id, $ko_name, $en_name);
    createmain_menu_apply($id, $mode);
    
}
//install.php menu이름 생성 
function createmain_menu_name($id, $ko_name, $en_name){
    global $DB;
    $main_menu_name_en = new stdClass();
    $main_menu_name_en->lang = 'en';
    $main_menu_name_en->name = $en_name;
    $main_menu_name_en->menuid = $id;
    $main_menu_name_en->timemodified = time();
    
    $DB->insert_record('main_menu_name', $main_menu_name_en);
    
    $main_menu_name_ko = new stdClass();
    $main_menu_name_ko->lang = 'ko';
    $main_menu_name_ko->name = $ko_name;
    $main_menu_name_ko->menuid = $id;
    $main_menu_name_ko->timemodified = time();
    $DB->insert_record('main_menu_name', $main_menu_name_ko);
}

//install.php 권한 설정
function createmain_menu_apply($id, $mode){
    global $DB;
    $main_menu_apply = new stdClass();
    $main_menu_apply->usergroup ='rs';
    $main_menu_apply->menuid =$id;
    $main_menu_apply->timecreated =time();
    $main_menu_apply->timemodified =time();
    if($mode != 3) {
        $DB->insert_record('main_menu_apply', $main_menu_apply);
    }
    $main_menu_apply = new stdClass();
    $main_menu_apply->usergroup ='pr';
    $main_menu_apply->menuid =$id;
    $main_menu_apply->timecreated =time();
    $main_menu_apply->timemodified =time();
    
    $DB->insert_record('main_menu_apply', $main_menu_apply);
    
    $main_menu_apply = new stdClass();
    $main_menu_apply->usergroup ='ad';
    $main_menu_apply->menuid =$id;
    $main_menu_apply->timecreated =time();
    $main_menu_apply->timemodified =time();
    
    $DB->insert_record('main_menu_apply', $main_menu_apply);
    
}

function createmain_menu_depth($parentid,$depth, $step, $ispopup, $url, $type, $icon, $userid, $required, $isused, $sub_parent, $ko_name, $en_name,$mode){
    global $DB;
    $main_menu = new stdClass();
    $main_menu->depth = $depth;
    $main_menu->step = $step;
    $main_menu->parent = $parentid;
    $main_menu->ispopup = $ispopup;
    $main_menu->url = $url;
    $main_menu->type = $type;
    $main_menu->icon = $icon;
    $main_menu->userid = $userid;
    $main_menu->required = $required;
    $main_menu->isused = $isused;
    $main_menu->timecreated = time();
    $main_menu->timemodified = time();
    $main_menu->sub_parent = $sub_parent;
    
    $id = $DB->insert_record('main_menu',$main_menu);
    
    createmain_menu_name($id, $ko_name, $en_name);
    createmain_menu_apply($id, $mode);
    if($mode == 2 && $url =='/local/mypage/mycourse.php') {
        createmain_menu_depth2($parentid, 1,1, '/local/mypage/mypoint.php',2,'',2,1,1,$id,'나의학습포인트현황','mypoint');
    }
}

function createmain_menu_depth2($parentid, $step, $ispopup, $url, $type, $icon, $userid, $required, $isused, $sub_parent, $ko_name, $en_name){
    global $DB;
    $main_menu = new stdClass();
    $main_menu->depth = 3;
    $main_menu->step = $step;
    $main_menu->parent = $parentid;
    $main_menu->ispopup = $ispopup;
    $main_menu->url = $url;
    $main_menu->type = $type;
    $main_menu->icon = $icon;
    $main_menu->userid = $userid;
    $main_menu->required = $required;
    $main_menu->isused = $isused;
    $main_menu->timecreated = time();
    $main_menu->timemodified = time();
    $main_menu->sub_parent = $sub_parent;
    
    $id = $DB->insert_record('main_menu',$main_menu);
    
    createmain_menu_name($id, $ko_name, $en_name);
    createmain_menu_apply($id);
}