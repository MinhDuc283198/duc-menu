<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019050200;
$plugin->requires = 2012061700;
$plugin->component = 'local_menu_admin'; // Full name of the plugin (used for diagnostics)
