<?php

require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot .'/local/popup/lib.php');

$id = optional_param('id', 0, PARAM_INT);
$PAGE->set_pagelayout('admin');
$strplural = '팝업관리';
$PAGE->navbar->add($strplural);
$PAGE->set_url('/local/popup/view.php');

$context = get_system_context();
require_capability('local/popup:managepopup', $context);

$PAGE->set_context($context);

$data = $DB->get_record('popup', array('id'=>$id));

echo $OUTPUT->header();
?>

<script  LANGUAGE="JavaScript">
    function deletepopup(id) {
        if(confirm("<?php echo get_string('suredeleteselectedcontents', 'local_popup'); ?>")) {
                document.deleteform.id.value = id;
                document.deleteform.action = '<?php echo($CFG->wwwroot);?>/local/popup/delete.php';
                document.deleteform.submit();
        }
    }
        
</script>
	<section id="page">
					
					<section id="breadcrumbs">
						<p><img src="<?php echo $CFG->wwwroot; ?>/theme/ust/pix/images/home_icon.png" alt="home" />&nbsp;<?php echo get_string('home', 'local_popup'); ?> > <?php echo get_string('myclassroom', 'local_popup'); ?></p>
					</section> <!-- Breadcrumbs Area End -->
					
					<hgroup class="page_header">
						<h3 class="page_title"><?php echo get_string('popupmanagement', 'local_popup'); ?></h3>
						<p class="page_description"><?php echo get_string('explain', 'local_popup'); ?></p>
					</hgroup> <!-- Page Title Area End -->
					
					<section id="content">
						
						<!-- <form class="search_area">
							
							<input type="text" class="w_5" placeholder="내용 + 제목" />
							<input type="submit" class="black_bg" value="확인" />
							<input type="submit" class="red_bg right" value="등록" />
						</form> Search Form Area End -->
						
						<!-- <p class="search_result">(1/XX 페이지, 총 X,XXX건)</p> Search Result -->
						
						<table cellpadding="0" cellspacing="0" class="detail" width="100%">
							
							<tbody>
								<tr>
									<td class="option"><?php echo get_string('title', 'local_popup'); ?></td>
									<td class="value"><?php echo $data->title; ?></td>
									<td class="option"><?php echo get_string('location', 'local_popup'); ?></td>
									<td class="value"><?php if($data->type == 1) echo get_string('login', 'local_popup'); else echo get_string('myclassroom', 'local_popup'); ?></td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('publicationperiod', 'local_popup'); ?></td>
									<td class="value number"><?php echo date("Y-m-d",$data->timeavailable);?> ~ <?php echo date("Y-m-d",$data->timedue);?></td>
									<td class="option"><?php echo get_string('whethertopublish', 'local_popup'); ?></td>
									<td class="value">
										<?php 
											$now = date("Y-m-d");
											if($now > date("Y-m-d",$data->timeavailable) && $now < date("Y-m-d",$data->timedue)){
										?>
										<?php echo get_string('publishing', 'local_popup'); ?>
										<?php } else if($now < date("Y-m-d",$data->timeavailable)){ ?>
										<?php echo get_string('waiting', 'local_popup'); ?>
										<?php } else { ?>
										<?php echo get_string('close', 'local_popup'); ?>
										<?php } ?>
									</td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('content', 'local_popup'); ?></td>
									<td class="value number" colspan="3">
										 <?php 
											echo file_rewrite_pluginfile_urls($data->description, 'pluginfile.php', $context->id, 'local_popup', 'popup', $data->id);
											//echo ($data->description);
										?>
									</td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('horizontalsize', 'local_popup'); ?></td>
									<td class="value number" colspan="3"><?php echo $data->popupwidth;?> px</td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('verticalsize', 'local_popup'); ?></td>
									<td class="value number" colspan="3"><?php echo $data->popupheight;?> px</td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('horizontalposition', 'local_popup'); ?></td>
									<td class="value number" colspan="3"><?php echo $data->popupx;?> px</td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('verticalposition', 'local_popup'); ?></td>
									<td class="value number" colspan="3"><?php echo $data->popupy;?> px</td>
								</tr>
								<tr>
									<td class="option"><?php echo get_string('scroll', 'local_popup'); ?></td>
									<td class="value number" colspan="3"><?php if($data->availablescroll == 1){ echo get_string('display', 'local_popup');; } else { echo get_string('nodisplay', 'local_popup');; }?></td>
								</tr>
							</tbody>
							
						</table>
						
						<a href="#" onclick="document.location.href='<?php echo $CFG->wwwroot.$_SESSION['listpage'];?>'"><p class="gray_bg"><?php echo get_string('list', 'local_popup'); ?></p></a>
						<a href="#" onclick="document.location.href='<?php echo($CFG->wwwroot);?>/local/popup/write.php?delete=<?php echo($data->id);?>';"><p class="red_bg right"><?php echo get_string('delete', 'local_popup'); ?></p></a>
						<a href="#" onclick="document.location.href='<?php echo($CFG->wwwroot);?>/local/popup/write.php?mode=edit&id=<?php echo($data->id);?>';"><p class="gray_bg right"><?php echo get_string('edit', 'local_popup'); ?></p></a>
												
					</section> <!-- Content end -->
				
				</section> <!-- Page End -->


<?php 
  echo $OUTPUT->footer();
?>


