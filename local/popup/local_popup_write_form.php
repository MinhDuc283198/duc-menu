<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 */

require_once($CFG->libdir.'/formslib.php');//putting this is as a safety as i got a class not found error.
class local_popup_write_form extends moodleform {
    function definition() {
        $mform = $this->_form;
        $instance = $this->_customdata;
        
        $mform->addElement('checkbox', 'isactive', get_string('isactive', 'local_popup'));
        $mform->addElement('text', 'title', get_string('title', 'local_popup'), array('size'=>'50'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('title', PARAM_TEXT);
        } else {
            $mform->setType('title', PARAM_CLEANHTML);
        }
        $mform->addRule('title', null, 'required', null, 'client');

        $mform->addElement('date_time_selector', 'timeavailable', get_string('timeavailable', 'local_popup'), array('optional'=>true));
        $mform->setDefault('timeavailable', time());

        $mform->addElement('date_time_selector', 'timedue', get_string('timedue', 'local_popup'), array('optional'=>true));
        $mform->setDefault('timedue', time()+(7*24*3600));
        $cookiedays = array("1"=>1,"2"=>2,"3"=>3,"4"=>4,"5"=>5,"6"=>6,"7"=>7,"8"=>8,"9"=>9,"10"=>10,"14"=>14,"30"=>30);
        $mform->addElement('select', 'cookieday', get_string('cookieday', 'local_popup'), $cookiedays);
        
        $mform->addElement('text', 'popupx', get_string('popupx', 'local_popup'), array('size'=>4));
        $mform->setType('popupx', PARAM_INT);
        $mform->setDefault('popupx', 0);
        $mform->addRule('popupx', null, 'required', null, 'client');

        $mform->addElement('text', 'popupy', get_string('popupy', 'local_popup'), array('size'=>4));
        $mform->setType('popupy', PARAM_INT);
        $mform->setDefault('popupy', 0);
        $mform->addRule('popupy', null, 'required', null, 'client');
        
        $mform->addElement('text', 'popupwidth', get_string('popupwidth', 'local_popup'), array('size'=>4));
        $mform->setType('popupwidth', PARAM_INT);
        $mform->setDefault('popupwidth', 300);
        $mform->addRule('popupwidth', null, 'required', null, 'client');

        $mform->addElement('text', 'popupheight', get_string('popupheight', 'local_popup'), array('size'=>4));
        $mform->setType('popupheight', PARAM_INT);
        $mform->setDefault('popupheight', 400);
        $mform->addRule('popupheight', null, 'required', null, 'client');
        
         $mform->addElement('checkbox', 'availablescroll', get_string('availablescroll', 'local_popup'));
        
        // hidden params
        $mform->addElement('hidden', 'edit', $instance['edit']);
        $mform->setType('contextid', PARAM_TEXT);
        
        $mform->addElement('hidden', 'id', $instance['id']);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('editor', 'description', get_string('description', 'local_popup'), null, self::editor_options());
        
        $mform->setType('description', PARAM_RAW);
        $mform->addRule('description', get_string('required'), 'required', null, 'client');

        // buttons
        $this->add_action_buttons(true, get_string('savechanges', 'admin'));
    }
    
    public static function editor_options() {
        global $COURSE, $PAGE, $CFG;
        require_once($CFG->dirroot . "/repository/lib.php");
        // TODO: add max files and max size support
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext'=> true,
            'return_types'=> FILE_INTERNAL | FILE_EXTERNAL
        );
    }
}
?>
