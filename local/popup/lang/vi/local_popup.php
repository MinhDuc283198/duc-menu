<?php

$string['pluginname'] = 'Thông báo';
$string['no'] = 'No.';
$string['title'] = 'Tiêu đề';
$string['description'] = 'Mô tả';

$string['popup'] = 'Thông báo';

$string['period'] = 'Ngày';
$string['isactive'] = 'Kích hoạt';
$string['emptycontent'] = 'Chưa có nội dung';
$string['addnewpopup'] = 'Tạo thông báo';
$string['prevpage'] = 'Lùi';
$string['nextpage'] = 'Tiến';
$string['popupmanagement'] = 'Quản lý thông báo';

$string['availablescroll'] = 'Hiển thị thanh cuộn';

$string['submit'] = 'Gửi';
$string['golist'] = 'Xem danh sách';

$string['position'] = 'Vị trí';
$string['popupx'] = 'Bên trái';
$string['popupy'] = 'Bên trên';

$string['popupwidth'] = 'Chiều rộng';
$string['popupheight'] = 'Chiều cao';


$string['popupsize'] = 'Kích thước';
$string['suredeleteselectedcontents'] = 'Bạn có muốn xóa mục đang chọn?';

$string['confirmdremovepopup'] = 'Bạn muốn xóa thông báo \'{$a}\'?';

$string['Deletionshavebeencompleted']	= 'Thông báo đã xóa thành công';

$string['description'] = 'Mô tả';
$string['cookieday'] = 'Không bật thông báo';

$string['active'] = 'Kích hoạt';
$string['inactive'] = 'Vô hiệu';

$string['closepopupdays'] = 'Không bật thông báo trong {$a} ngày.';
$string['closepopupday'] = 'Trong bật thông báo trong 24 giờ';

$string['timeavailable'] = 'Ngày bắt đầu';
$string['timedue'] = 'Ngày kết thúc';

$string['invalidpopupid'] = 'ID thông báo không tồn tại';
$string['deletesure'] = 'Bạn muốn xóa thông báo này?';

$string['home'] = 'Trang chủ';
$string['myclassroom'] = 'Lớp học của tôi';
$string['popupmanagement'] = 'Quản lý thông báo';
$string['explain'] = 'Bạn có thể tùy chọn và quản lý thông báo trong Hệ thống Quản lý học tập UST.';
$string['title'] = 'Tiêu đề';
$string['location'] = 'Vị trí';
$string['publicationperiod'] = 'Ngày khởi tạo';
$string['whethertopublish'] = 'Trạng thái phát hành';
$string['publishing'] = 'Phát hành';
$string['waiting'] = 'Chờ đợi';
$string['close'] = 'Đóng';
$string['content'] = 'Nội dung';
$string['horizontalsize'] = 'Kích thước chiều ngang';
$string['verticalsize'] = 'Kích thước chiều dọc';
$string['horizontalposition'] = 'Vị trí chiều ngang';
$string['verticalposition'] = 'Vị trí chiều dọc';
$string['scroll'] = 'Hiển thị thanh cuộn';
$string['list'] = 'Danh sách';
$string['delete'] = 'Xóa';
$string['edit'] = 'Sửa';
$string['login'] = 'Đăng nhập';
$string['display'] = 'Hiển thị';
$string['nodisplay'] = 'Ẩn';




/*$string['add'] = 'Add';
$string['addcategory'] = 'Add Category';
$string['attachfile'] = 'Attach file';
$string['all--'] = '----- All -----';
$string['cancel'] = '취소';
$string['cannotdeletecategory'] = 'Can not delete this category.({$a})';
$string['category'] = 'Category';
$string['categorynamech'] = 'Chiness Name';
$string['categorynameen'] = 'English Name';
$string['categorynamejp'] = 'Japaness Name';
$string['categorynameko'] = 'Korean Name';
$string['categorydescription'] = '분류설명';
$string['contentcategory'] = 'Content Category';
$string['contentdescription'] = 'Content Description';
$string['contentname'] = 'Content Name';
$string['contentsdir'] = 'Contents Directory';
$string['contentsdirdesc'] = 'Contents Directory';
$string['contenttype'] = 'Content Type';
$string['contenttype.url'] = 'URL';
$string['defaultlearningtime'] = 'Default learning time';
$string['defaultpopupheight'] = 'Default pupup window height';
$string['defaultpopupwidth'] = 'Default pupup window width';
$string['delete'] = 'Delete';
$string['deletecategorysure'] = 'Are you sure you want to delete this category? (" + catname + ")';
$string['edit'] = 'Edit';
$string['entercategorynameen'] = 'Enter category english name.';
$string['entercategorynameko'] = 'Enter category korean name.';
$string['entercontentname'] = 'Enter content name.';
$string['enterindexfilepath'] = 'Enter indexfile path.';
$string['enterlearningtime'] = 'Enter learning time';
$string['enterpopupwindowsize'] = 'Enter popup window size.';
$string['filetypenotallowed'] = 'File type not allowed,\nAllowed file: *.zip';
$string['gotolist'] = 'Go to list';
$string['height'] = 'Height';
$string['indexfilepath'] = 'Indexfile path';
$string['ispublic'] = 'Is public?';
$string['learningtime'] = 'Learning time';
$string['learningtimedescription'] = 'Learning time';
$string['learningtimeshouldbeint'] = 'Learning time should be integer.';
$string['learningtimevalue'] = 'Learning time should be greater than 0.';
$string['managecategories'] = 'Manage Categories';
$string['managecontents'] = 'Manage Contents';
$string['maxfilesize'] = 'Maximum uploaded file size(MB)';
$string['maxfilesize2'] = 'Maximum uploaded file size: {$a}MB';
$string['maxfilesizedesc'] = 'This specifies a maximum size that uploaded files';
$string['minutes'] = 'Minutes';
$string['modify'] = 'Modify';
$string['movedown'] = 'Move down';
$string['moveup'] = 'Move up';
$string['nextpage'] = 'Next';
$string['nocategory'] = 'No Category';
$string['nocontent'] = 'No Content';
$string['notpublic'] = 'Not public';
$string['no.'] = 'No.';
$string['preview'] = 'Preview';
$string['prevpage'] = 'Previous';
$string['popupwindowsize'] = 'Popup window size';
$string['popupwindowsizedescription'] = 'Popup window size';
$string['popupwindowsizeshouldbeint'] = 'Popup window size should be integer.';
$string['popupwindowsizevalue'] = 'Popup window size should be 0 or greater than 0.';
$string['public'] = 'Public';
$string['registdate'] = 'Registration Date';
$string['register'] = 'Register';
$string['registuser'] = 'Registration User';
$string['save'] = 'Save';
$string['search'] = 'Search';
$string['select'] = 'Select';
$string['selectall'] = 'Select All';
$string['selectcontentdelete'] = 'Select contents that you want to delete.';
$string['selectcontenttype'] = 'Select content type.';
$string['selectfirstcategory'] = 'Select first category.';
$string['selectsecondcategory'] = 'Select second category.';
$string['selectthirdcategory'] = 'Select third category.';
$string['selectuploadfile'] = 'Select file to upload';
$string['select--'] = '----- Select -----';
$string['suredeleteselectedcontents'] = 'Are you sure you want to delete selected contents?';
$string['width'] = 'Width';
*/
?>
