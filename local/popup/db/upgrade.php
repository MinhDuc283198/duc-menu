<?php
function xmldb_local_popup_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;
    
    $dbman = $DB->get_manager(); 

    
    if($oldversion < 2019122600) {     
        $table = new xmldb_table('popup');  
        $field = new xmldb_field('site', XMLDB_TYPE_INTEGER, '1', null, null, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field); 
        }
    }

    return true;
}

