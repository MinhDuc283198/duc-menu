<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<?php 

require_once('../../config.php');

$context = get_system_context();
require_capability('local/popup:managepopup', $context);

$id   = required_param('id', PARAM_INT);
$DB->delete_records("popup", array("id"=>$id));

$msg = get_string('Deletionshavebeencompleted','jinotechboard');

echo '<script language="javascript">';
echo 'alert("'.$msg.'");';
echo 'document.location.href="'.$CFG->wwwroot.'/ustmanaging/popup.php?top_menu=1"';
echo '</script>';

?>
</body>
</html>