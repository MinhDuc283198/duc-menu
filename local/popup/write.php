<?php

require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot .'/local/popup/lib.php');

$edit    = optional_param('edit', 0, PARAM_INT);
$delete  = optional_param('delete', 0, PARAM_INT); 
$confirm = optional_param('confirm', 0, PARAM_INT);

$PAGE->set_pagelayout('admin');

$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('optionalservices', 'theme_aardvark_postit'));
$PAGE->navbar->add(get_string('popup:management', 'theme_aardvark_postit'), new moodle_url('/local/popup/index.php'));

$PAGE->set_url('/local/popup/write.php', array(
    'edit'  => $edit,
    'delete'=> $delete,
    'confirm'=>$confirm,
));

$page_params = array('edit'=>$edit);

$context = get_system_context();
$PAGE->set_context($context);
require_capability('local/popup:managepopup', $context);
require_once('local_popup_write_form.php');

$mform = new local_popup_write_form("write.php");
if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot."/ustmanaging/popup.php");

}

$draftid_editor = file_get_submitted_draft_itemid('description');

if(!empty($edit)){
    if(!$popup = $DB->get_record("popup", array("id" => $edit))){
        print_error('invalidpopupid', 'local_popup');
    }
    
    $popup->edit = $edit;
    $popup = trusttext_pre_edit($popup, 'description', $context);
    
    

    $currenttext = file_prepare_draft_area($draftid_editor, $context->id, 'local_popup', 'popup', empty($popup->id) ? null : $popup->id, local_popup_write_form::editor_options(), $popup->description);

    $mform->set_data(array(
        "isactive"=>$popup->isactive,
        "title"=>$popup->title,
        'description'=>array(
            'text'=>$currenttext,
            'format'=>empty($popup->descriptionformat ) ? editors_get_preferred_format() : $popup->descriptionformat ,
            'itemid'=>$draftid_editor
        ),
        "url"=>$popup->url,
        "timedue"=>$popup->timedue,
        "timeavailable"=>$popup->timeavailable,
        "popupx"=>$popup->popupx,
        "popupy"=>$popup->popupy,
        "popupwidth"=>$popup->popupwidth,
        "popupheight"=>$popup->popupheight,
        "availablescroll"=>$popup->availablescroll?$popup->availablescroll:0,
        "cookieday"=>$popup->cookieday,
        "user"=>$popup->user)
        +$page_params
    );
    
}else if(!empty($delete)){
    if(!$popup = $DB->get_record("popup", array("id" => $delete))){
        print_error('invalidpopupid', 'local_popup');
    }
    
    if (!empty($confirm) && confirm_sesskey()) {    // User has confirmed the delete
         $DB->delete_records("popup", array("id" => $popup->id));
         add_to_log(SITEID, "popup", "delete popup",
                           "view.php?id=$popup->id", "$popup->id", $USER->id);
                redirect($CFG->wwwroot."/ustmanaging/popup.php");
     }else{// User just asked to delete something
        $PAGE->navbar->add(get_string('delete'));
        $PAGE->set_title("");
        $PAGE->set_heading("");
        
        echo $OUTPUT->header();
        
        echo $OUTPUT->confirm(get_string("confirmdremovepopup", "local_popup", $popup->title),
            "write.php?delete=$delete&confirm=$delete",
            $CFG->wwwroot.'/local/popup/index.php');
        //echo (get_string("title", "local_popup"). " : ".$popup->title);
//        forum_print_post($post, $discussion, $forum, $cm, $course, false, false, false);
        echo $OUTPUT->footer();
        die;
            
     }
    
}else {
    $popup = new stdClass();
    
    $popup->isactive = '1';
    $popup->title = '';
    $popup->url = '';
    $popup->timedue = '';
    $popup->timeavailable = '';
    $popup->popupx = 0;
    $popup->popupy = 0;
    
    $popup->popupwidth = 300;
    $popup->popupheight = 400;
    $popup->availablescroll = '';
    $popup->cookieday = '';
    $popup->description = '';
    $popup->descriptionformat  = editors_get_preferred_format();
    $popup->descriptiontrust   = 0;
    $popup->user = $USER->id;
    
    $mform->set_data(array(
        "isactive"=>1,
        "title"=>$popup->title,
        'description'=>array(
            'text'=>"",
            'format'=>empty($popup->descriptionformat ) ? editors_get_preferred_format() : $popup->descriptionformat ,
            'itemid'=>$draftid_editor
        ),
        "url"=>$popup->url,
        
        "user"=>$popup->user)
        +$page_params
    );
    
    
}
require_login();






if ($fromform = $mform->get_data()) {
    //$fromform->description       = $fromform->description['text'];
    
    $fromform->modified = time();
    if ($fromform->edit) {           // Updating a post
        
        
        $fromform->id = $fromform->edit;
        
        $fromform->isactive = !empty($fromform->isactive)?1:0;
        $fromform->availablescroll = !empty($fromform->availablescroll)?1:0;
        
        if (!empty($fromform->description['itemid'])) {   // In "single simple discussions" this may not exist yet
            $text = file_save_draft_area_files($fromform->description['itemid'], $context->id, 'local_popup', 'popup', $fromform->edit,
                    local_popup_write_form::editor_options(), $fromform->description['text']);
            //$DB->set_field('popup', 'description', $text, array('id'=>$fromform->edit));
            
            $fromform->description  = $text;
        }
        
        //print_r($fromform);
        
        $DB->update_record('popup', $fromform);
        

       redirect($CFG->wwwroot."/ustmanaging/popup.php");

    }else{
        
        
        $fromform->timecreated = time();
        $fromform->timemodified = time();
        //$fromform->description = '';
        $new_popup = $DB->insert_record('popup', $fromform);
        
        
        if (!empty($fromform->description['itemid'])) { 

            $text = file_save_draft_area_files($fromform->description['itemid'], $context->id, 'local_popup', 'popup', $new_popup,
                    local_popup_write_form::editor_options(), $fromform->description['text']);
            $DB->set_field('popup', 'description', $text, array('id'=>$new_popup));
        }
    
        redirect($CFG->wwwroot."/ustmanaging/popup.php");
    }
}
    

?>

<?php
        
        $PAGE->set_heading("");
        
        echo $OUTPUT->header(); 
        echo $OUTPUT->box_start('generalbox');
        $mform->display();
        echo $OUTPUT->box_end();
        ?>


<?php 

  echo $OUTPUT->footer();
  
  ?>


