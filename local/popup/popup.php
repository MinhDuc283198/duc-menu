<?php


        //require_once('../../config.php');
        require_once(dirname(__FILE__).'/../../config.php');
    	require_once($CFG->dirroot .'/local/popup/lib.php');

        
        $id = optional_param('id', 0, PARAM_INT);
        
        $popup = $DB->get_record('popup', array('id'=>$id));
?>

<script  LANGUAGE="JavaScript">
    
    function setCookie( name, value, expiredays ) { 
        var todayDate = new Date(); 
        todayDate.setDate( todayDate.getDate() + expiredays ); 
        document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
    } 

    function closeWin(name, value, expiredays )  { 
        if ( document.popupform.confirmed.checked ) 
            setCookie(name, value , expiredays);
        self.close(); 
    } 

        
</script>

<html>
    <head>
        <title><?php echo $popup->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/aardvark_postit/style/postit_core.css" />
    </head>
    
    <body class="popup_body">
        <div class="popup_wrap">
            <div class="popup_text">
                <?php echo($popup->description); ?>
            </div>
            <?php
            if ($popup->cookieday > 0) {
                echo '<div class="popup_dontopen">
                    <form name="popupform"> 
                        <input type="checkbox" name="confirmed" onclick="closeWin(\'popup_' . $popup->id . '\', \'no\', \'' . $popup->cookieday . '\')">' . get_string("closepopupdays", "local_popup", $popup->cookieday) . '
                    </form>             
                </div>';
            }
            ?>
            
        </div>
    </body>
    
    
</html>