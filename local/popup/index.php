<?php


require_once('../../config.php');
//require_once $CFG->dirroot.'/mylearning/jinotech_lib.php';
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/local/popup/lib.php');

$page        = optional_param('page', 0, PARAM_INT);     // which page to show
$searchfield      = optional_param('searchfield', '', PARAM_CLEAN);// search string
$searchvalue      = optional_param('searchvalue', '', PARAM_CLEAN);// search string
$perpage			= optional_param('perpage', 15, PARAM_INT);    //한페이지에 보이는 글의 수
$pagerange			= optional_param('pagerange', 10, PARAM_INT);		//하단에 리스트에 보이는 페이지수
    
$params = array();
if ($page) {
    $params['page'] = $page;
}

$PAGE->set_url('/local/popup/view.php', $params);
$PAGE->set_pagelayout('admin');

$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('local'), null);
$PAGE->navbar->add(get_string('popup', 'local_popup'), new moodle_url('/local/popup/index.php'));

require_login();

$context = get_system_context();
require_capability('local/popup:managepopup', $context);
$PAGE->set_context($context);

$PAGE->set_title(get_string('popup','local_popup'));
$PAGE->set_heading(format_string("set_heading"));
 
echo $OUTPUT->header();

$totalcount= local_popup_get_allcount($searchfield, $searchvalue);
?>
<div class="mylearning_header">
    <h2 class="mylearning_title"><?php echo get_string('popup','local_popup'); ?></h2>
</div>

<div class="mylearning_header">
    <?php echo "
        <form name='searchForm' method='POST' action='".$CFG->wwwroot."/local/popup/index.php'>
            <select name='searchfield'>
                    <option value='1' $searchfield == 1 ? ' selected' : '')>".get_string('title', 'local_popup')." </option>
                    <option value='3' $searchfield == 3 ? ' selected' : '')>".get_string('description', 'local_popup')." </option>

            </select>
            <input type='text' name='searchvalue' class='board_search_input'  value='' />
            <input type='submit' value='".get_string('search')."'/>
        </form>";?>

</div>

<?php 
    
echo local_pupup_print_popups("id DESC ", $searchfield, $searchvalue, $page, $perpage, $totalcount);

echo $OUTPUT->footer();

?>