<?php

function local_popup_pluginfile($course, $cm, $context, $filearea,
        $args, $forcedownload, array $options=array()){
    
    
    global $DB;

    $context = get_system_context();
    //require_capability('local/popup:managepopup', $context);

    if ($filearea !== 'popup') {
        return false;
    }

    /*if (!has_capability('local/popup:managepopup', $context)) {
        return false;
    }*/

    $chid = (int)array_shift($args);

    if (!$popup = $DB->get_record('popup', array('id'=>$chid))) {
        return false;
    }

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/local_popup/popup/$chid/$relativepath";
    
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }
    
    // finally send the file
    send_stored_file($file, 360, 0, $forcedownload, $options);
    
}


function local_popup_get_allcount($searchfield="", $searchvalue="") {
    global $CFG, $DB, $USER;
    
    $params = array();
    $where = "";
    
    if(!isNullOrEmptyStr2($searchvalue)){
        switch ($searchfield) {
            case 1: 
                $where = " title LIKE ?"; //
                $params[] = '%'.$searchvalue.'%';
                    break;
            case 2:
                    break;
            case 3:
                $where = " description LIKE ?"; 
                $params[] = '%'.$searchvalue.'%';
                break;
        }
        
    }
    
    return $DB->count_records_select ("popup", $where, $params);
}


function local_pupup_print_popups($sort, $searchfield="1", $searchvalue="", $page, $perpage, $totalcount){
    global $CFG;
     
    echo '<div class="writing_content">';
    echo '<table class="mylearning_table">
	<tr >
		<th class="header">'.get_string("no", 'local_popup').'</th>
		<th class="header">'.get_string("title", 'local_popup').'</th>
		<th class="header">'.get_string("period", 'local_popup').'</th>
		<th class="header">'.get_string("isactive", 'local_popup').'</th>'.
            '</tr>';
    $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    
    $popups = local_pupup_get_popups($sort, $searchfield, $searchvalue, $page, $perpage);
    $num = $totalcount - $offset;
    if(!$popups){
        echo "<tr><td colspan=4 align=center>".get_string("emptycontent", 'local_popup')."</td></tr>";
         //return;
    }else {
        $rowi = 0;
        
        foreach ($popups as $popup) {
            echo("<tr  class=\"body_order ".($rowi%2==1?" r1 ": "")." \">
		<td align=center>".$num."</td>
                <td align=left><div  style='text-align:left'><a href='./view.php?id=".$popup->id."'>".$popup->title."</a></div></td>
                <td align=center>".date("Y-m-d",$popup->timeavailable)."~".date("Y-m-d",$popup->timedue)."</td>
                <td align=center>". ($popup->isactive==0?get_string("inactive","local_popup"):get_string("active","local_popup")) ."</td>
               </tr>");
            $num-- ; 
            $rowi ++;
        }
        
    }
    echo "</table>
        
        <input type='button'  id='writebtn'  onclick='document.location.href =\"./write.php\"'  class='listmenu_list' value='".get_string('addnewpopup', 'local_popup')."' />
    </div>";
    
    
}

function local_popup_get_popups($sort, $searchvalue = null, $page, $perpage) {
    
    global $CFG, $DB, $USER;
    
    $offset = 0;
    if ($page != 0) {
            $offset = $page * $perpage;
    }
    $where = '';
    $params = array();
    
    if(!$searchvalue){
        return $DB->get_records_select("popup",null, null,$sort,"*",$offset,$perpage);
    }else{
        $where .= $DB->sql_like('title', ':searchvalue');
        $params['searchvalue'] = '%'.$searchvalue.'%';
    } 
    return $DB->get_records_select("popup", $where, $params, $sort, "*", $offset, $perpage);
}

function local_popup_get_popups_count($searchvalue = null) {
    global $DB;
    
    $where = '';
    $params = array();
    if($searchvalue){
        $where .= $DB->sql_like('title', ':searchvalue');
        $params['searchvalue'] = '%'.$searchvalue.'%';
    } 
    return $DB->count_records_select('popup', $where, $params);
}

function local_pupup_get_paging_bar($totalcount, $page, $perpage, $baseurl, $maxdisplay=15, $separator="&nbsp;", $previousandnext = true, $prevpage, $nextpage) {

    $code = '';

    //If there are results (more than 1 page)
//    if ($totalcount > $perpage) {
        $code .= "<div class=\"pagingDiv\">";

        $maxpage = (int)(($totalcount-1)/$perpage);

        //Lower and upper limit of page
        if ($page < 0) {
            $page = 0;
        }
        if ($page > $maxpage) {
            $page = $maxpage;
        }

        //Calculate the window of pages
        $pagefrom = $page - ((int)($maxdisplay / 2));
        if ($pagefrom < 0) {
            $pagefrom = 0;
        }
        $pageto = $pagefrom + $maxdisplay - 1;
        if ($pageto > $maxpage) {
            $pageto = $maxpage;
        }

        //Some movements can be necessary if don't see enought pages
        if ($pageto - $pagefrom < $maxdisplay - 1) {
            if ($pageto - $maxdisplay + 1 > 0) {
                $pagefrom = $pageto - $maxdisplay + 1;
            }
        }

        //Calculate first and last if necessary
        $firstpagecode = '';
        $lastpagecode = '';
        if ($pagefrom > 0) {
            $firstpagecode = "$separator<a class=\"page\" href=\"{$baseurl}&page=0\">1</a>";
            if ($pagefrom > 1) {
                $firstpagecode .= "$separator...";
            }
        }
        if ($pageto < $maxpage) {
            if ($pageto < $maxpage -1) {
                $lastpagecode = "$separator...";
            }
            $lastpagecode .= "$separator<a class=\"page\" href=\"{$baseurl}&page=$maxpage\">".($maxpage+1)."</a>";
        }

        //Previous
        if ($page > 0 && $previousandnext) {
            $pagenum = $page - 1;
            $code .= "<span class=\"pagenavi\"><a href=\"{$baseurl}&page=$pagenum\">".$prevpage."</a></span>&nbsp;";
        } else {
            $code .= "<span class=\"pagenavi_disable\">".$prevpage."</span>&nbsp;";
        }

        //Add first
        $code .= $firstpagecode;

        $pagenum = $pagefrom;

        //List of maxdisplay pages
        while ($pagenum <= $pageto) {
            $pagetoshow = $pagenum +1;
            if ($pagenum == $page) {
                $code .= "$separator<span class=\"currentpage\">$pagetoshow</span>";
            } else {
                $code .= "$separator<a class=\"page\" href=\"{$baseurl}&page=$pagenum\">$pagetoshow</a>";
            }
            $pagenum++;
        }

        //Add last
        $code .= $lastpagecode;

        //Next
        if ($page < $maxpage && $previousandnext) {
            $pagenum = $page + 1;
            $code .= "&nbsp;&nbsp;<span class=\"pagenavi\"><a href=\"{$baseurl}&page=$pagenum\">".$nextpage."</a></span>";
        } else {
            $code .= "&nbsp;&nbsp;<span class=\"pagenavi_disable\">".$nextpage."</span>";
        }

        //End html
        $code .= "</div>";
//    }

    return $code;
}

 function isNullOrEmptyStr2($str) {
        if(!isset($str)||$str==NULL||$str=='') return true;
        else return false;
}

?>
