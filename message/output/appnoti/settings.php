<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//fcm 서버키
//defined('fcmsekey', 'AAAAwXytkcQ:APA91bEfeXrxMwL4pGVMaCq4SlSFOHyWr23uI4EJg2pl56d4pLt-lPVyreCA0T8z6Q-d4TnHUGgqxHEKBdcfpSi44sgHa06LI6IP5Wu5Tmllh0e4ekN5WJxovBFzeqLQbqo2dLb97Ymx');
//fcm 이전 서버키 
//defined('prefcmsekey', 'AIzaSyD_001rpG6-CTEh1QDv4XlhGVGcDle-muQ');
//api key 
//defined('webapi',  'AIzaSyBqLd5OPFSAjbIkey79rxjgwZew0gPMoDM');
//fcm url 
//defined('fcmurladd', 'https://fcm.googleapis.com/fcm/send');

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configtext('fcmserverkey', 'server key', 'Server Key.', '', PARAM_RAW));
    $settings->add(new admin_setting_configtext('fcmserverlegacykey', 'Server legacy key', 'Server legacy key', '', PARAM_RAW));
    $settings->add(new admin_setting_configtext('fcmurl', 'fcmurl', 'Fcm Url', '', PARAM_RAW));
    $settings->add(new admin_setting_configtext('brandname', 'Brand name', 'FPT Brand name ', '', PARAM_RAW));
    $settings->add(new admin_setting_configtext('clientid', 'Client ID', 'FPT Client ID' , '', PARAM_RAW));
    $settings->add(new admin_setting_configtext('clientsecret', 'Client Secret', 'FPT Client Secret', '', PARAM_RAW));
}
