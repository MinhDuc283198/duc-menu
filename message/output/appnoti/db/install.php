<?php
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//          http://moodle.com                                            //
//                                                                       //
// Copyright (C) 1999 onwards  Martin Dougiamas  http://moodle.com       //
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
/**
 * application notification - lib file
 *
 * @author seokkeun hong. JinoTech
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package message_appnoti
 */

/**
 * Install the appnoti message processor
 */
function xmldb_message_appnoti_install() {
    global $DB;
    
    $result = true;

    $provider = new stdClass();
    $provider->name  = 'appnoti';
    $DB->insert_record('message_processors', $provider);
    
    return $result;
}
