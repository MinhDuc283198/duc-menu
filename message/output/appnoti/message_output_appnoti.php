<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/message/output/lib.php');

/**
 * The application notification processor
 *
 * @package   message_appnoti
 * @copyright seokkeun hong Jinotech.
 */
class message_output_appnoti extends message_output {
    
    function send_message($eventdata) {
        
        global $CFG, $DB;
       
        // Ignore $CFG->nosmsever here because we want to test this code,
        // the message sending fails later in sms_to_user().
        // skip any messaging suspended and deleted users
        if ($eventdata->userto->auth === 'nologin' or $eventdata->userto->suspended or $eventdata->userto->deleted) {
            return true;
        }
        
        if (!empty($CFG->noemailever)) {
            // hidden setting for development sites, set in config.php if needed
            debugging('$CFG->noemailever is active, no sms message sent.', DEBUG_MINIMAL);
            return true;
        }
        
        if (PHPUNIT_TEST) {
            // No connection to external servers allowed in phpunit tests.
            return true;
        }
        

        $token = $DB->get_field('lmsdata_pushtoken','token', array('username'=>$eventdata->userto->id));

        $message = new stdClass();
        
        $message->title = $eventdata->subject;
        $message->fullmessage = $eventdata->fullmessage;
        $message->link = $eventdata->link;
        if($token){
            $this->send_visang_fcm($eventdata->smallmessage, $eventdata->fullmessage, $targetUrl, $token);
        }
        if(empty($eventdata->userto->phone1)){
            $this->send_visang_sms(preg_replace("/[^0-9]*/s", "", $eventdata->userto->phone1), $eventdata->fullmessage);
        }
        return true;
    }

    /**
     * Creates necessary fields in the messaging config form.
     *
     * @param array $preferences An array of user preferences
     */
    function config_form($preferences){
        global $USER, $OUTPUT, $CFG;
        error_log ("config_form\n", 3, "/var/www/moodle/error_log");  
//        if (empty($CFG->messagingallowsmsoverride)) {
//            return null;
//        }
        
//        $inputattributes = array('size'=>'30', 'name'=>'token', 'value'=>$preferences->sms_phone2);
//        $string = get_string('phone2','message_sms') . ': ' . html_writer::empty_tag('input', $inputattributes);
//
//        if (empty($preferences->sms_phone2) && !empty($preferences->userdefaultsms)) {
//            $string .= get_string('ifsmsleftempty', 'message_sms', $preferences->userdefaultsms);
//        }
//
//        if (!empty($preferences->sms_phone2)) {
//            $string .= $OUTPUT->container(get_string('invalidphonenumber', 'message_sms'), 'error');
//        }
        $string = 'test';
        return $string;
    }

    /**
     * Parses the submitted form data and saves it into preferences array.
     *
     * @param stdClass $form preferences form class
     * @param array $preferences preferences array
     */
    function process_form($form, &$preferences){
        error_log ("process_form\n", 3, "/var/www/moodle/error_log");  
        return true;
    }

    /**
     * Returns the default message output settings for this output
     *
     * @return int The default settings
     */
    public function get_default_messaging_settings() {
        error_log ("get_default_messaging_settings\n", 3, "/var/www/moodle/error_log");  
        return MESSAGE_PERMITTED + MESSAGE_DEFAULT_LOGGEDIN + MESSAGE_DEFAULT_LOGGEDOFF;
    }

    /**
     * Loads the config data from database to put on the form during initial form display
     *
     * @param array $preferences preferences array
     * @param int $userid the user id
     */
    function load_data(&$preferences, $userid){
        error_log ("load_data\n", 3, "/var/www/moodle/error_log");  
        return true;
    }

    /**
     * Returns true as message can be sent to internal support user.
     *
     * @return bool
     */
    public function can_send_to_any_users() {
        error_log ("can_send_to_any_users\n", 3, "/var/www/moodle/error_log");  
        return true;
    }
    
    public function send_visang_fcm($subject, $message, $targetUrl, $token) {

        $url = get_config('moodle', 'fcmurl');

        $fcmserverkey = get_config('moodle', 'fcmserverkey');
        $headers = array(
            'Authorization: key=' . $fcmserverkey,
            'Content-Type: application/json'
        );
        $fields = array(
            "to" => $token,
            "priority" => "high",
            "notification" => array(
                "title" => $subject,
                "body" => $message,
                "sound" => "",
//                "image" => "http://www.gokorea.kr/news/photo/201908/377541_907633_524.png"
            ),
            "data" => array(
//                "linkUrl" => "https://www.google.com",
//                "thumbUrl" => "http://www.gokorea.kr/news/photo/201908/377541_907633_524.png",
                "title" => $subject,
                "body" => $message
            )
        );
        print_object($url);
        print_object($fields);
        $fields['priority'] = "high";

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
        print_object($result);
        if ($result === FALSE) {
        }
        curl_close($ch);
        return $result;
    }

    public function send_visang_sms($phone, $message) {
        echo 333; die;
        $brandname = get_config('moodle', 'brandname');
        $clientid = get_config('moodle', 'clientid');
        $clientsecret = get_config('moodle', 'clientsecret');
        global $DB, $CFG;
        require_once $CFG->dirroot . '/local/lmsdata/hanbirosms_module/fptsms.php';
        $vsms = new fptSms();
        $smsinfos = $DB->get_records_sql('select name,value from {vi_settings} where name = ? or name = ? or name = ?', array("brandname", "clientid", "clientsecret"));
        $vsms->init($brandname,$clientid,$clientsecret);
        if (is_array($phone)) {
            $return = $vsms->send_multi_sms($phone, $message);
        } else {
            $return = $vsms->send_sms($phone, $message);
        }
        return $return;
    }
}
