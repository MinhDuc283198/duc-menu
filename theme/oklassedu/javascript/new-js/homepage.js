$(document).ready(function(){
    $('.course-popular-slider').slick({
        infinite: true,
        slidesToShow: 3,
        dots: true,
        customPaging : function(slider, i) {
            var thumb = $(slider.$slides[i]).data();
            return '<i class="fa fa-circle"></i>';
        },
        prevArrow: $('.course-popular-slider-pre'),
        nextArrow: $('.course-popular-slider-next'),
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 801,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 601,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });
});
$(document).ready(function(){
    $('.course-recommned-slider').slick({
        infinite: true,
        slidesToShow: 3,
        dots: true,
        customPaging : function(slider, i) {
            var thumb = $(slider.$slides[i]).data();
            return '<i class="fa fa-circle"></i>';
        },
        prevArrow: $('.course-recommend-slider-pre'),
        nextArrow: $('.course-recommend-slider-next'),
        responsive: [
            {
                breakpoint: 801,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 601,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });
});
$(document).ready(function () {
    $('.course-employer-slider').slick({
        infinite: true,
        slidesToShow: 3,
        dots: true,
        customPaging : function(slider, i) {
            var thumb = $(slider.$slides[i]).data();
            return '<i class="fa fa-circle"></i>';
        },
        prevArrow: $('.course-employer-slider-pre'),
        nextArrow: $('.course-employer-slider-next'),
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 801,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 601,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });
})
